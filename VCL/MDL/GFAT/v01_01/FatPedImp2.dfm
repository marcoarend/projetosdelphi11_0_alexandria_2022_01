object FmFatPedImp2: TFmFatPedImp2
  Left = 339
  Top = 185
  Caption = 'FAT-IMPRI-000 :: Vendas por Faturamento'
  ClientHeight = 712
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 299
        Height = 32
        Caption = 'Vendas por Faturamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 299
        Height = 32
        Caption = 'Vendas por Faturamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 299
        Height = 32
        Caption = 'Vendas por Faturamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 550
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 550
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      ExplicitHeight = 467
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 550
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 812
        ExplicitHeight = 467
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 517
          Height = 533
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitHeight = 616
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 517
            Height = 105
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object LaPVIEmp: TLabel
              Left = 8
              Top = 8
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object LaPVICli: TLabel
              Left = 8
              Top = 32
              Width = 35
              Height = 13
              Caption = 'Cliente:'
            end
            object LaPVIRep: TLabel
              Left = 8
              Top = 56
              Width = 73
              Height = 13
              Caption = 'Representante:'
            end
            object LaPVIPrd: TLabel
              Left = 8
              Top = 80
              Width = 74
              Height = 13
              Caption = 'Grupo de prod.:'
            end
            object EdPVIPrd: TdmkEditCB
              Left = 96
              Top = 76
              Width = 52
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBPVIPrd
              IgnoraDBLookupComboBox = False
            end
            object EdPVIRep: TdmkEditCB
              Left = 96
              Top = 52
              Width = 52
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Represen'
              UpdCampo = 'Represen'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBPVIRep
              IgnoraDBLookupComboBox = False
            end
            object EdPVICli: TdmkEditCB
              Left = 96
              Top = 28
              Width = 52
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Cliente'
              UpdCampo = 'Cliente'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBPVICli
              IgnoraDBLookupComboBox = False
            end
            object EdPVIEmp: TdmkEditCB
              Left = 96
              Top = 4
              Width = 52
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBPVIEmp
              IgnoraDBLookupComboBox = False
            end
            object CBPVIEmp: TdmkDBLookupComboBox
              Left = 152
              Top = 4
              Width = 356
              Height = 21
              KeyField = 'Filial'
              ListField = 'NOMEFILIAL'
              ListSource = DModG.DsEmpresas
              TabOrder = 1
              dmkEditCB = EdPVIEmp
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CBPVICli: TdmkDBLookupComboBox
              Left = 152
              Top = 28
              Width = 356
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENT'
              ListSource = DmPediVda.DsPVICliCad
              TabOrder = 3
              dmkEditCB = EdPVICli
              QryCampo = 'Cliente'
              UpdType = utNil
              LocF7SQLMasc = '$#'
            end
            object CBPVIRep: TdmkDBLookupComboBox
              Left = 152
              Top = 52
              Width = 356
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENT'
              ListSource = DmPediVda.DsPVIRepCad
              TabOrder = 5
              dmkEditCB = EdPVIRep
              QryCampo = 'Represen'
              UpdType = utNil
              LocF7SQLMasc = '$#'
            end
            object CBPVIPrd: TdmkDBLookupComboBox
              Left = 152
              Top = 76
              Width = 356
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DmPediVda.DsPVIPrdCad
              TabOrder = 7
              dmkEditCB = EdPVIPrd
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object Panel9: TPanel
            Left = 0
            Top = 150
            Width = 517
            Height = 383
            Align = alClient
            TabOrder = 1
            ExplicitTop = 425
            ExplicitHeight = 108
            object RGOrdemIts: TRadioGroup
              Left = 1
              Top = 1
              Width = 515
              Height = 41
              Align = alTop
              Caption = ' Ordena'#231#227'o dos itens: '
              Columns = 2
              Items.Strings = (
                'Quantidade'
                'Valor')
              TabOrder = 0
              ExplicitLeft = 4
              ExplicitTop = 48
              ExplicitWidth = 501
            end
            object RGAgrupa: TRadioGroup
              Left = 1
              Top = 42
              Width = 515
              Height = 53
              Align = alTop
              Caption = ' Sintetizar por: '
              Columns = 2
              Items.Strings = (
                'Grupo de produto'
                'Cliente'
                'Representante'
                'Produto')
              TabOrder = 1
              ExplicitLeft = 4
              ExplicitTop = 92
              ExplicitWidth = 501
            end
            object RGOrdemAgr_: TRadioGroup
              Left = 1
              Top = 95
              Width = 515
              Height = 287
              Align = alClient
              Caption = ' Ordena'#231#227'o dos agrupamentos: '
              Columns = 2
              Items.Strings = (
                'Nome / descri'#231#227'o'
                'C'#243'digo')
              TabOrder = 2
              Visible = False
              ExplicitLeft = 152
              ExplicitTop = 369
              ExplicitHeight = 381
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 105
            Width = 517
            Height = 45
            Align = alTop
            Caption = ' Per'#237'odo de faturamento: '
            TabOrder = 2
            ExplicitLeft = 12
            ExplicitTop = 121
            object TPIncluIni: TdmkEditDateTimePicker
              Left = 92
              Top = 18
              Width = 112
              Height = 21
              Date = 39892.420627939810000000
              Time = 39892.420627939810000000
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object TPIncluFim: TdmkEditDateTimePicker
              Left = 290
              Top = 18
              Width = 112
              Height = 21
              Date = 39892.420627939810000000
              Time = 39892.420627939810000000
              TabOrder = 3
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object CkIncluIni: TCheckBox
              Left = 12
              Top = 18
              Width = 77
              Height = 17
              Caption = 'Data inicial:'
              Checked = True
              Enabled = False
              State = cbChecked
              TabOrder = 0
            end
            object CkIncluFim: TCheckBox
              Left = 215
              Top = 18
              Width = 77
              Height = 17
              Caption = 'Data final:'
              Checked = True
              Enabled = False
              State = cbChecked
              TabOrder = 2
            end
          end
        end
        object Panel10: TPanel
          Left = 519
          Top = 15
          Width = 487
          Height = 533
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitLeft = 517
          ExplicitTop = 0
          ExplicitWidth = 468
          ExplicitHeight = 616
          object DBGrid2: TDBGrid
            Left = 0
            Top = 384
            Width = 487
            Height = 149
            Align = alBottom
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
            Columns = <
              item
                Expanded = False
                FieldName = 'Tabela'
                Title.Caption = 'Filtro'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodUsu1'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome1'
                Title.Caption = 'Descri'#231#227'o'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodUsu2'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome2'
                Title.Caption = 'Descri'#231#227'o'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodUsu3'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome3'
                Title.Caption = 'Descri'#231#227'o'
                Width = 140
                Visible = True
              end>
          end
          object DBGrid1: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 487
            Height = 384
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'CODI_AGRUP'
                Title.Caption = 'Codigo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME_AGRUP'
                Title.Caption = 'Descri'#231#227'o / nome'
                Width = 247
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_COR'
                Title.Caption = 'Cor'
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TAM'
                Title.Caption = 'Tamanho'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantP'
                Title.Caption = 'Q.Ped.'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantC'
                Title.Caption = 'Q.Can.'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantV'
                Title.Caption = 'Q.Fat.'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantF'
                Title.Caption = 'Q.Pen.'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValLiq'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SEQ'
                Width = 36
                Visible = True
              end>
            Color = clWindow
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'CODI_AGRUP'
                Title.Caption = 'Codigo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME_AGRUP'
                Title.Caption = 'Descri'#231#227'o / nome'
                Width = 247
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_COR'
                Title.Caption = 'Cor'
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TAM'
                Title.Caption = 'Tamanho'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantP'
                Title.Caption = 'Q.Ped.'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantC'
                Title.Caption = 'Q.Can.'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantV'
                Title.Caption = 'Q.Fat.'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantF'
                Title.Caption = 'Q.Pen.'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValLiq'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SEQ'
                Width = 36
                Visible = True
              end>
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 598
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 642
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
    end
  end
end
