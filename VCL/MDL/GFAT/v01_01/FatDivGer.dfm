object FmFatDivGer: TFmFatDivGer
  Left = 368
  Top = 194
  Caption = 'NFe-Geral-001 :: Emiss'#227'o de NF-e(s) Diversas'
  ClientHeight = 666
  ClientWidth = 993
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 90
    Width = 993
    Height = 576
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 993
      Height = 362
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object GroupBox9: TGroupBox
        Left = 0
        Top = 0
        Width = 993
        Height = 57
        Align = alTop
        TabOrder = 0
        object Label56: TLabel
          Left = 8
          Top = 12
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label2: TLabel
          Left = 8
          Top = 35
          Width = 36
          Height = 13
          Caption = 'Pedido:'
        end
        object Label64: TLabel
          Left = 368
          Top = 35
          Width = 50
          Height = 13
          Caption = 'Prioridade:'
        end
        object Label65: TLabel
          Left = 482
          Top = 35
          Width = 101
          Height = 13
          Caption = 'Previs'#227'o entrega:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label33: TLabel
          Left = 819
          Top = 12
          Width = 43
          Height = 13
          Caption = 'Abertura:'
          Enabled = False
        end
        object Label31: TLabel
          Left = 717
          Top = 35
          Width = 58
          Height = 13
          Caption = 'ID Faturam.:'
        end
        object Label32: TLabel
          Left = 835
          Top = 35
          Width = 80
          Height = 13
          Caption = 'C'#243'digo Faturam.:'
        end
        object SpeedButton5: TSpeedButton
          Left = 150
          Top = 31
          Width = 24
          Height = 24
          Caption = '?'
          OnClick = SpeedButton5Click
        end
        object DBEdit3: TDBEdit
          Left = 55
          Top = 8
          Width = 52
          Height = 21
          DataField = 'Filial'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit2: TDBEdit
          Left = 112
          Top = 8
          Width = 683
          Height = 21
          DataField = 'NOMEEMP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object EdPedido: TdmkEdit
          Left = 55
          Top = 31
          Width = 91
          Height = 24
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdPedidoExit
        end
        object DBEdit12: TDBEdit
          Left = 422
          Top = 31
          Width = 52
          Height = 21
          DataField = 'Prioridade'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 3
        end
        object DBEdit11: TDBEdit
          Left = 586
          Top = 31
          Width = 127
          Height = 21
          DataField = 'DtaPrevi'
          DataSource = DsPediVda
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
        end
        object EdCodigo: TdmkEdit
          Left = 776
          Top = 31
          Width = 55
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdCodUsu: TdmkEdit
          Left = 918
          Top = 31
          Width = 55
          Height = 24
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAbertura: TdmkEdit
          Left = 862
          Top = 8
          Width = 111
          Height = 21
          Enabled = False
          TabOrder = 7
          FormatType = dmktfDateTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '30/12/1899 00:00:00'
          QryCampo = 'Abertura'
          UpdCampo = 'Abertura'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 57
        Width = 993
        Height = 134
        Align = alTop
        TabOrder = 1
        object Label8: TLabel
          Left = 8
          Top = 12
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object DBText1: TDBText
          Left = 819
          Top = 12
          Width = 44
          Height = 17
          DataField = 'NOME_TIPO_DOC'
          DataSource = DsCli
        end
        object Label3: TLabel
          Left = 8
          Top = 82
          Width = 40
          Height = 13
          Caption = 'Entrega:'
        end
        object DBEdit1: TDBEdit
          Left = 55
          Top = 8
          Width = 71
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsCli
          Enabled = False
          TabOrder = 0
        end
        object DBEdit6: TDBEdit
          Left = 130
          Top = 8
          Width = 682
          Height = 21
          DataField = 'NOME_ENT'
          DataSource = DsCli
          Enabled = False
          TabOrder = 1
        end
        object DBMemo1: TDBMemo
          Left = 55
          Top = 31
          Width = 918
          Height = 47
          DataField = 'E_ALL'
          DataSource = DsCli
          Enabled = False
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 862
          Top = 8
          Width = 111
          Height = 21
          DataField = 'CNPJ_TXT'
          DataSource = DsCli
          Enabled = False
          TabOrder = 3
        end
        object DBMemo2: TDBMemo
          Left = 55
          Top = 82
          Width = 918
          Height = 47
          DataField = 'E_ALL'
          DataSource = DsEntrega
          Enabled = False
          TabOrder = 4
        end
      end
      object GroupBox12: TGroupBox
        Left = 0
        Top = 191
        Width = 993
        Height = 34
        Align = alTop
        TabOrder = 2
        object Label16: TLabel
          Left = 8
          Top = 12
          Width = 73
          Height = 13
          Caption = 'Representante:'
        end
        object Label17: TLabel
          Left = 630
          Top = 11
          Width = 117
          Height = 13
          Caption = '% comiss'#227'o faturamento:'
        end
        object Label18: TLabel
          Left = 806
          Top = 11
          Width = 119
          Height = 13
          Caption = '% comiss'#227'o recebimento:'
        end
        object DBEdit44: TDBEdit
          Left = 82
          Top = 8
          Width = 57
          Height = 21
          DataField = 'CODUSU_ACC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit45: TDBEdit
          Left = 142
          Top = 8
          Width = 481
          Height = 21
          DataField = 'NOMEACC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit46: TDBEdit
          Left = 748
          Top = 8
          Width = 47
          Height = 21
          DataField = 'ComisFat'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBEdit47: TDBEdit
          Left = 925
          Top = 8
          Width = 47
          Height = 21
          DataField = 'ComisRec'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 3
        end
      end
      object GroupBox15: TGroupBox
        Left = 0
        Top = 225
        Width = 993
        Height = 42
        Align = alTop
        Caption = ' Fiscal:'
        TabOrder = 3
        object Label34: TLabel
          Left = 8
          Top = 20
          Width = 73
          Height = 13
          Caption = 'Movimenta'#231#227'o:'
        end
        object Label35: TLabel
          Left = 630
          Top = 20
          Width = 55
          Height = 13
          Caption = 'Modelo NF:'
        end
        object DBEdit48: TDBEdit
          Left = 82
          Top = 16
          Width = 57
          Height = 21
          DataField = 'CODUSU_FRC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit49: TDBEdit
          Left = 142
          Top = 16
          Width = 481
          Height = 21
          DataField = 'NOMEFISREGCAD'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit50: TDBEdit
          Left = 690
          Top = 16
          Width = 282
          Height = 21
          DataField = 'NOMEMODELONF'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
      end
      object GroupBox14: TGroupBox
        Left = 0
        Top = 267
        Width = 993
        Height = 80
        Align = alTop
        TabOrder = 4
        object Label52: TLabel
          Left = 8
          Top = 35
          Width = 75
          Height = 13
          Caption = 'Transportadora:'
        end
        object Label53: TLabel
          Left = 8
          Top = 59
          Width = 64
          Height = 13
          Caption = 'Redespacho:'
        end
        object Label51: TLabel
          Left = 8
          Top = 12
          Width = 45
          Height = 13
          Caption = 'Frete por:'
        end
        object Label66: TLabel
          Left = 264
          Top = 12
          Width = 98
          Height = 13
          Caption = '% Desp. acess'#243'rias: '
          Visible = False
        end
        object Label67: TLabel
          Left = 398
          Top = 12
          Width = 96
          Height = 13
          Caption = '$ Desp. acess'#243'rias: '
          Visible = False
        end
        object Label68: TLabel
          Left = 563
          Top = 12
          Width = 38
          Height = 13
          Caption = '% Frete:'
          Visible = False
        end
        object Label70: TLabel
          Left = 642
          Top = 12
          Width = 36
          Height = 13
          Caption = '$ Frete:'
          Visible = False
        end
        object Label71: TLabel
          Left = 760
          Top = 12
          Width = 48
          Height = 13
          Caption = '% Seguro:'
          Visible = False
        end
        object Label72: TLabel
          Left = 850
          Top = 12
          Width = 46
          Height = 13
          Caption = '$ Seguro:'
          Visible = False
        end
        object DBEdit34: TDBEdit
          Left = 86
          Top = 31
          Width = 56
          Height = 21
          DataField = 'CODUSU_TRA'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit35: TDBEdit
          Left = 86
          Top = 55
          Width = 56
          Height = 21
          DataField = 'CODUSU_RED'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit36: TDBEdit
          Left = 146
          Top = 31
          Width = 826
          Height = 21
          DataField = 'NOMETRANSP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBEdit37: TDBEdit
          Left = 146
          Top = 55
          Width = 826
          Height = 21
          DataField = 'NOMEREDESP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 3
        end
        object DBEdit16: TDBEdit
          Left = 55
          Top = 8
          Width = 21
          Height = 21
          DataField = 'FretePor'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 4
        end
        object DBEdit23: TDBEdit
          Left = 75
          Top = 8
          Width = 185
          Height = 21
          DataField = 'NOMEFRETEPOR'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 5
        end
        object DBEdit39: TDBEdit
          Left = 358
          Top = 8
          Width = 36
          Height = 21
          DataField = 'DesoAces_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 6
          Visible = False
        end
        object DBEdit38: TDBEdit
          Left = 496
          Top = 8
          Width = 63
          Height = 21
          DataField = 'DesoAces_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 7
          Visible = False
        end
        object DBEdit41: TDBEdit
          Left = 602
          Top = 8
          Width = 36
          Height = 21
          DataField = 'Frete_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 8
          Visible = False
        end
        object DBEdit40: TDBEdit
          Left = 686
          Top = 8
          Width = 70
          Height = 21
          DataField = 'Frete_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 9
          Visible = False
        end
        object DBEdit43: TDBEdit
          Left = 811
          Top = 8
          Width = 35
          Height = 21
          DataField = 'Seguro_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 10
          Visible = False
        end
        object DBEdit42: TDBEdit
          Left = 902
          Top = 8
          Width = 70
          Height = 21
          DataField = 'Seguro_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 11
          Visible = False
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 513
      Width = 993
      Height = 63
      Align = alBottom
      TabOrder = 1
      object Panel8: TPanel
        Left = 2
        Top = 14
        Width = 989
        Height = 48
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 846
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 118
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 90
    Width = 993
    Height = 576
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Panel4: TPanel
      Left = 0
      Top = 451
      Width = 993
      Height = 62
      Align = alBottom
      TabOrder = 0
      object DBGFatPedVol: TDBGrid
        Left = 1
        Top = 1
        Width = 153
        Height = 60
        Align = alLeft
        DataSource = DsFatPedVol
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_UnidMed'
            Title.Caption = 'Volume'
            Width = 54
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cnta'
            Title.Caption = 'N'#250'mero'
            Width = 42
            Visible = True
          end>
      end
      object DBGFatPedIts: TDBGrid
        Left = 154
        Top = 1
        Width = 838
        Height = 60
        Align = alClient
        DataSource = DsFatPedIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CU_NIVEL1'
            Title.Caption = 'Produto'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_NIVEL1'
            Title.Caption = 'Descri'#231#227'o'
            Width = 171
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CU_COR'
            Title.Caption = 'Cor'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_COR'
            Title.Caption = 'Descri'#231#227'o'
            Width = 178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TAM'
            Title.Caption = 'Tamanho'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Caption = 'Quantidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CALC_TXT'
            Title.Caption = 'C'#225'lculo'
            Width = 49
            Visible = True
          end>
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 993
      Height = 434
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 993
        Height = 57
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 12
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label5: TLabel
          Left = 8
          Top = 35
          Width = 36
          Height = 13
          Caption = 'Pedido:'
        end
        object Label6: TLabel
          Left = 368
          Top = 35
          Width = 50
          Height = 13
          Caption = 'Prioridade:'
        end
        object Label9: TLabel
          Left = 482
          Top = 35
          Width = 101
          Height = 13
          Caption = 'Previs'#227'o entrega:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label10: TLabel
          Left = 717
          Top = 35
          Width = 58
          Height = 13
          Caption = 'ID Faturam.:'
        end
        object Label11: TLabel
          Left = 835
          Top = 35
          Width = 80
          Height = 13
          Caption = 'C'#243'digo Faturam.:'
        end
        object Label36: TLabel
          Left = 807
          Top = 12
          Width = 52
          Height = 13
          Caption = 'Encerrado:'
        end
        object Label37: TLabel
          Left = 646
          Top = 12
          Width = 43
          Height = 13
          Caption = 'Abertura:'
        end
        object DBEdit5: TDBEdit
          Left = 59
          Top = 8
          Width = 52
          Height = 21
          DataField = 'Filial'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit7: TDBEdit
          Left = 112
          Top = 8
          Width = 531
          Height = 21
          DataField = 'NOMEEMP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit8: TDBEdit
          Left = 422
          Top = 31
          Width = 52
          Height = 21
          DataField = 'Prioridade'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBEdit9: TDBEdit
          Left = 586
          Top = 31
          Width = 127
          Height = 21
          DataField = 'DtaPrevi'
          DataSource = DsPediVda
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
        object DBEdit53: TDBEdit
          Left = 776
          Top = 31
          Width = 55
          Height = 21
          DataField = 'Codigo'
          DataSource = DsFatPedCab
          TabOrder = 4
        end
        object DBEdit54: TDBEdit
          Left = 918
          Top = 31
          Width = 55
          Height = 24
          DataField = 'CodUsu'
          DataSource = DsFatPedCab
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
        end
        object DBEdit57: TDBEdit
          Left = 59
          Top = 31
          Width = 120
          Height = 24
          DataField = 'PEDIDO_TXT'
          DataSource = DsFatPedCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
        end
        object DBEdit59: TDBEdit
          Left = 862
          Top = 8
          Width = 111
          Height = 21
          DataField = 'ENCERROU_TXT'
          DataSource = DsFatPedCab
          TabOrder = 7
          OnChange = DBEdit59Change
        end
        object DBEdit58: TDBEdit
          Left = 693
          Top = 8
          Width = 110
          Height = 21
          DataField = 'Abertura'
          DataSource = DsFatPedCab
          TabOrder = 8
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 57
        Width = 993
        Height = 134
        Align = alTop
        TabOrder = 1
        object Label12: TLabel
          Left = 8
          Top = 12
          Width = 59
          Height = 13
          Caption = 'Destinat'#225'rio:'
        end
        object DBText2: TDBText
          Left = 819
          Top = 12
          Width = 44
          Height = 17
          DataField = 'NOME_TIPO_DOC'
          DataSource = DsCli
        end
        object Label13: TLabel
          Left = 27
          Top = 82
          Width = 40
          Height = 13
          Caption = 'Entrega:'
        end
        object DBEdit10: TDBEdit
          Left = 75
          Top = 8
          Width = 71
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsCli
          Enabled = False
          TabOrder = 0
        end
        object DBEdit13: TDBEdit
          Left = 149
          Top = 8
          Width = 667
          Height = 21
          DataField = 'NOME_ENT'
          DataSource = DsCli
          Enabled = False
          TabOrder = 1
        end
        object DBMemo3: TDBMemo
          Left = 75
          Top = 31
          Width = 898
          Height = 47
          DataField = 'E_ALL'
          DataSource = DsCli
          Enabled = False
          TabOrder = 2
        end
        object DBEdit14: TDBEdit
          Left = 862
          Top = 8
          Width = 111
          Height = 21
          DataField = 'CNPJ_TXT'
          DataSource = DsCli
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object DBMemo4: TDBMemo
          Left = 75
          Top = 82
          Width = 898
          Height = 47
          DataField = 'E_ALL'
          DataSource = DsEntrega
          Enabled = False
          TabOrder = 4
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 191
        Width = 993
        Height = 34
        Align = alTop
        TabOrder = 2
        object Label14: TLabel
          Left = 8
          Top = 12
          Width = 73
          Height = 13
          Caption = 'Representante:'
        end
        object Label15: TLabel
          Left = 630
          Top = 11
          Width = 117
          Height = 13
          Caption = '% comiss'#227'o faturamento:'
        end
        object Label19: TLabel
          Left = 806
          Top = 11
          Width = 119
          Height = 13
          Caption = '% comiss'#227'o recebimento:'
        end
        object DBEdit15: TDBEdit
          Left = 82
          Top = 8
          Width = 57
          Height = 21
          DataField = 'CODUSU_ACC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit17: TDBEdit
          Left = 142
          Top = 8
          Width = 481
          Height = 21
          DataField = 'NOMEACC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit18: TDBEdit
          Left = 748
          Top = 8
          Width = 47
          Height = 21
          DataField = 'ComisFat'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBEdit19: TDBEdit
          Left = 925
          Top = 8
          Width = 47
          Height = 21
          DataField = 'ComisRec'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 3
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 306
        Width = 993
        Height = 128
        Align = alTop
        Caption = ' Fiscal:'
        TabOrder = 3
        object Label20: TLabel
          Left = 8
          Top = 20
          Width = 73
          Height = 13
          Caption = 'Movimenta'#231#227'o:'
        end
        object Label21: TLabel
          Left = 630
          Top = 20
          Width = 55
          Height = 13
          Caption = 'Modelo NF:'
        end
        object Label29: TLabel
          Left = 12
          Top = 86
          Width = 521
          Height = 13
          Caption = 
            'Indicador de presen'#231'a do comprador no estabelecimento comercial ' +
            'no momento da opera'#231#227'o (NFe 3.10) : [F4]'
        end
        object Label38: TLabel
          Left = 614
          Top = 86
          Width = 148
          Height = 13
          Caption = 'Finalidade de emiss'#227'o da NF-e:'
          FocusControl = DBEdfinNFe
        end
        object DBEdit20: TDBEdit
          Left = 85
          Top = 16
          Width = 57
          Height = 21
          DataField = 'CODUSU_FRC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit21: TDBEdit
          Left = 146
          Top = 16
          Width = 478
          Height = 21
          DataField = 'NOMEFISREGCAD'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit22: TDBEdit
          Left = 690
          Top = 16
          Width = 282
          Height = 21
          DataField = 'NOMEMODELONF'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBRGIndDest: TDBRadioGroup
          Left = 12
          Top = 42
          Width = 678
          Height = 38
          Caption = ' Local de Destino da Opera'#231#227'o: (v'#225'lida a partir da NFe 3.10) '
          Columns = 4
          DataField = 'idDest'
          DataSource = DsPediVda
          Items.Strings = (
            '0 - N'#227'o definido'
            '1 - Opera'#231#227'o interna'
            '2 - Opera'#231#227'o interestadual'
            '3 - Opera'#231#227'o com exterior')
          TabOrder = 3
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
        object RGDBIndFinal: TDBRadioGroup
          Left = 693
          Top = 42
          Width = 276
          Height = 38
          Caption = ' Opera'#231#227'o com consumidor (NFe 3.10): '
          Columns = 2
          DataField = 'indFinal'
          DataSource = DsPediVda
          Items.Strings = (
            '0 - Normal'
            '1 - Consumidor final')
          TabOrder = 4
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
        object DBEdindPres: TDBEdit
          Left = 12
          Top = 102
          Width = 28
          Height = 21
          DataField = 'indPres'
          DataSource = DsPediVda
          TabOrder = 5
          OnChange = DBEdindPresChange
        end
        object EdDBindPres_TXT: TEdit
          Left = 39
          Top = 102
          Width = 570
          Height = 21
          ReadOnly = True
          TabOrder = 6
        end
        object DBRadioGroup1: TDBRadioGroup
          Left = 768
          Top = 86
          Width = 201
          Height = 39
          Caption = ' Envio da NF-e ao Fisco (NFe 3.10):'
          Columns = 2
          DataField = 'indSinc'
          DataSource = DsPediVda
          Items.Strings = (
            'Ass'#237'ncrono'
            'S'#237'ncrono')
          TabOrder = 7
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
        object DBEdfinNFe: TDBEdit
          Left = 614
          Top = 102
          Width = 21
          Height = 21
          DataField = 'finNFe'
          DataSource = DsPediVda
          TabOrder = 8
          OnChange = DBEdfinNFeChange
        end
        object EdDBfinNFe_TXT: TEdit
          Left = 635
          Top = 102
          Width = 128
          Height = 21
          ReadOnly = True
          TabOrder = 9
        end
      end
      object GroupBox6: TGroupBox
        Left = 0
        Top = 225
        Width = 993
        Height = 81
        Align = alTop
        TabOrder = 4
        object Label23: TLabel
          Left = 8
          Top = 35
          Width = 75
          Height = 13
          Caption = 'Transportadora:'
        end
        object Label24: TLabel
          Left = 8
          Top = 59
          Width = 64
          Height = 13
          Caption = 'Redespacho:'
        end
        object Label4: TLabel
          Left = 8
          Top = 12
          Width = 45
          Height = 13
          Caption = 'Frete por:'
        end
        object Label7: TLabel
          Left = 264
          Top = 12
          Width = 98
          Height = 13
          Caption = '% Desp. acess'#243'rias: '
          Visible = False
        end
        object Label22: TLabel
          Left = 398
          Top = 12
          Width = 96
          Height = 13
          Caption = '$ Desp. acess'#243'rias: '
          Visible = False
        end
        object Label25: TLabel
          Left = 563
          Top = 12
          Width = 38
          Height = 13
          Caption = '% Frete:'
          Visible = False
        end
        object Label26: TLabel
          Left = 642
          Top = 12
          Width = 36
          Height = 13
          Caption = '$ Frete:'
          Visible = False
        end
        object Label27: TLabel
          Left = 760
          Top = 12
          Width = 48
          Height = 13
          Caption = '% Seguro:'
          Visible = False
        end
        object Label28: TLabel
          Left = 850
          Top = 12
          Width = 46
          Height = 13
          Caption = '$ Seguro:'
          Visible = False
        end
        object DBEdit26: TDBEdit
          Left = 86
          Top = 31
          Width = 56
          Height = 21
          DataField = 'CODUSU_TRA'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit27: TDBEdit
          Left = 86
          Top = 55
          Width = 56
          Height = 21
          DataField = 'CODUSU_RED'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit28: TDBEdit
          Left = 146
          Top = 31
          Width = 826
          Height = 21
          DataField = 'NOMETRANSP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBEdit29: TDBEdit
          Left = 146
          Top = 55
          Width = 826
          Height = 21
          DataField = 'NOMEREDESP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 3
        end
        object DBEdit24: TDBEdit
          Left = 55
          Top = 8
          Width = 21
          Height = 21
          DataField = 'FretePor'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 4
        end
        object DBEdit25: TDBEdit
          Left = 75
          Top = 8
          Width = 185
          Height = 21
          DataField = 'NOMEFRETEPOR'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 5
        end
        object DBEdit30: TDBEdit
          Left = 358
          Top = 8
          Width = 36
          Height = 21
          DataField = 'DesoAces_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 6
          Visible = False
        end
        object DBEdit31: TDBEdit
          Left = 496
          Top = 8
          Width = 63
          Height = 21
          DataField = 'DesoAces_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 7
          Visible = False
        end
        object DBEdit32: TDBEdit
          Left = 602
          Top = 8
          Width = 36
          Height = 21
          DataField = 'Frete_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 8
          Visible = False
        end
        object DBEdit33: TDBEdit
          Left = 686
          Top = 8
          Width = 70
          Height = 21
          DataField = 'Frete_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 9
          Visible = False
        end
        object DBEdit51: TDBEdit
          Left = 811
          Top = 8
          Width = 35
          Height = 21
          DataField = 'Seguro_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 10
          Visible = False
        end
        object DBEdit52: TDBEdit
          Left = 902
          Top = 8
          Width = 70
          Height = 21
          DataField = 'Seguro_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 11
          Visible = False
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 513
      Width = 993
      Height = 63
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 14
        Width = 169
        Height = 48
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 126
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 86
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 47
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 171
        Top = 14
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 281
        Top = 14
        Width = 710
        Height = 48
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 577
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 118
            Height = 39
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtFatura: TBitBtn
          Tag = 10102
          Left = 4
          Top = 4
          Width = 89
          Height = 39
          Cursor = crHandPoint
          Caption = '&Fatura'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtFaturaClick
        end
        object BtVolume: TBitBtn
          Tag = 10103
          Left = 94
          Top = 4
          Width = 89
          Height = 39
          Cursor = crHandPoint
          Caption = '&Volume'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtVolumeClick
        end
        object BtItens: TBitBtn
          Tag = 30
          Left = 185
          Top = 4
          Width = 89
          Height = 39
          Cursor = crHandPoint
          Caption = '&Itens'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtItensClick
        end
        object BtEncerra: TBitBtn
          Tag = 506
          Left = 276
          Top = 4
          Width = 88
          Height = 39
          Cursor = crHandPoint
          Caption = '&Encerrar'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtEncerraClick
        end
        object BtComissao: TBitBtn
          Tag = 10031
          Left = 366
          Top = 4
          Width = 89
          Height = 39
          Cursor = crHandPoint
          Caption = '&Comiss'#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          Visible = False
          OnClick = BtComissaoClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 206
      Height = 47
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 5
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 44
        Top = 5
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 84
        Top = 5
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object BtFisRegCad: TBitBtn
        Tag = 10104
        Left = 123
        Top = 5
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtFisRegCadClick
      end
      object BtGraGruN: TBitBtn
        Tag = 30
        Left = 162
        Top = 5
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtGraGruNClick
      end
    end
    object GB_M: TGroupBox
      Left = 206
      Top = 0
      Width = 740
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 339
        Height = 31
        Caption = 'Emiss'#227'o de NF-e(s) Diversas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 339
        Height = 31
        Caption = 'Emiss'#227'o de NF-e(s) Diversas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 339
        Height = 31
        Caption = 'Emiss'#227'o de NF-e(s) Diversas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 47
    Width = 993
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 14
      Width = 989
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Panel8
    CanUpd01 = BtEncerra
    Left = 192
    Top = 212
  end
  object PMFatura: TPopupMenu
    OnPopup = PMFaturaPopup
    Left = 536
    Top = 36
    object Sempedido1: TMenuItem
      Caption = 'Inclui faturamento &Sem pedido'
      OnClick = Sempedido1Click
    end
    object Pedido1: TMenuItem
      Caption = 'Inclui faturamento &Com Pedido'
      Enabled = False
      OnClick = Pedido1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Alterafaturamentoatual1: TMenuItem
      Caption = '&Altera faturamento atual'
      OnClick = Alterafaturamentoatual1Click
    end
    object Excluifaturamentoatual1: TMenuItem
      Caption = 'E&xclui faturamento atual'
      Enabled = False
      OnClick = Excluifaturamentoatual1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Duplicarfaturamentoatual1: TMenuItem
      Caption = '&Duplicar faturamento atual'
      OnClick = Duplicarfaturamentoatual1Click
    end
  end
  object QrFatPedCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFatPedCabBeforeOpen
    AfterOpen = QrFatPedCabAfterOpen
    BeforeClose = QrFatPedCabBeforeClose
    AfterScroll = QrFatPedCabAfterScroll
    OnCalcFields = QrFatPedCabCalcFields
    SQL.Strings = (
      'SELECT pvd.CodUsu CU_PediVda, pvd.Empresa, '
      'pvd.TabelaPrc, pvd.CondicaoPG, pvd.RegrFiscal,'
      'pvd.AFP_Sit, pvd.AFP_Per, pvd.Cliente,'
      'pvd.Codigo ID_Pedido, pvd.PedidoCli,'
      'ppc.JurosMes,'
      'frc.Nome NOMEFISREGCAD,'
      'ppc.MedDDReal, ppc.MedDDSimpl,'
      'par.TipMediaDD, par.Associada, par.FatSemEstq,'
      ''
      'par.CtaProdVen EMP_CtaProdVen,'
      'par.FaturaSeq EMP_FaturaSeq,'
      'par.FaturaSep EMP_FaturaSep,'
      'par.FaturaDta EMP_FaturaDta,'
      'par.TxtProdVen EMP_TxtProdVen,'
      'emp.Filial EMP_FILIAL,'
      'ufe.Codigo EMP_UF,'
      ''
      'ass.CtaProdVen ASS_CtaProdVen,'
      'ass.FaturaSeq ASS_FaturaSeq,'
      'ass.FaturaSep ASS_FaturaSep,'
      'ass.FaturaDta ASS_FaturaDta,'
      'ass.TxtProdVen ASS_TxtProdVen,'
      'ufa.Nome ASS_NO_UF,'
      'ufa.Codigo ASS_CO_UF,'
      'ase.Filial ASS_FILIAL,'
      ''
      'tpc.Nome NO_TabelaPrc,'
      '/*frm.TipoCalc,*/ fpc.*'
      'FROM fatpedcab fpc'
      'LEFT JOIN pedivda    pvd ON pvd.Codigo=fpc.Pedido'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc'
      'LEFT JOIN paramsemp  par ON par.Codigo=pvd.Empresa'
      'LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada'
      'LEFT JOIN entidades  ase ON ase.Codigo=ass.Codigo'
      'LEFT JOIN entidades  emp ON emp.Codigo=par.Codigo'
      
        'LEFT JOIN ufs        ufe ON ufe.Codigo=IF(emp.Tipo=0, emp.EUF, e' +
        'mp.PUF)'
      
        'LEFT JOIN ufs        ufa ON ufa.Codigo=IF(ase.Tipo=0, ase.EUF, a' +
        'se.PUF)'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      '/* N'#227'o tem como'
      'LEFT JOIN fisregmvt frm ON frm.Codigo=pvd.RegrFiscal'
      '  AND frm.StqCenCad=1'
      '  AND frm.Empresa=-11'
      '*/'
      'WHERE fpc.Codigo > -1000')
    Left = 220
    Top = 60
    object QrFatPedCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'pedivda.Empresa'
    end
    object QrFatPedCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fatpedcab.Codigo'
      Required = True
    end
    object QrFatPedCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'fatpedcab.CodUsu'
      Required = True
    end
    object QrFatPedCabPedido: TIntegerField
      FieldName = 'Pedido'
      Origin = 'fatpedcab.Pedido'
      Required = True
    end
    object QrFatPedCabCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
      Origin = 'pedivda.CondicaoPG'
    end
    object QrFatPedCabTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Origin = 'pedivda.TabelaPrc'
    end
    object QrFatPedCabMedDDReal: TFloatField
      FieldName = 'MedDDReal'
      Origin = 'pediprzcab.MedDDReal'
    end
    object QrFatPedCabMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
      Origin = 'pediprzcab.MedDDSimpl'
    end
    object QrFatPedCabRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Origin = 'pedivda.RegrFiscal'
    end
    object QrFatPedCabNO_TabelaPrc: TWideStringField
      FieldName = 'NO_TabelaPrc'
      Origin = 'tabeprccab.Nome'
      Required = True
      Size = 50
    end
    object QrFatPedCabTipMediaDD: TSmallintField
      FieldName = 'TipMediaDD'
      Origin = 'paramsemp.TipMediaDD'
      Required = True
    end
    object QrFatPedCabAFP_Sit: TSmallintField
      FieldName = 'AFP_Sit'
      Origin = 'pedivda.AFP_Sit'
      Required = True
    end
    object QrFatPedCabAFP_Per: TFloatField
      FieldName = 'AFP_Per'
      Origin = 'pedivda.AFP_Per'
      Required = True
    end
    object QrFatPedCabAssociada: TIntegerField
      FieldName = 'Associada'
      Origin = 'paramsemp.Associada'
      Required = True
    end
    object QrFatPedCabCU_PediVda: TIntegerField
      FieldName = 'CU_PediVda'
      Origin = 'pedivda.CodUsu'
      Required = True
    end
    object QrFatPedCabAbertura: TDateTimeField
      FieldName = 'Abertura'
      Origin = 'fatpedcab.Abertura'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFatPedCabEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Origin = 'fatpedcab.Encerrou'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFatPedCabFatSemEstq: TSmallintField
      FieldName = 'FatSemEstq'
      Origin = 'paramsemp.FatSemEstq'
      Required = True
    end
    object QrFatPedCabENCERROU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENCERROU_TXT'
      Size = 30
      Calculated = True
    end
    object QrFatPedCabEMP_CtaProdVen: TIntegerField
      FieldName = 'EMP_CtaProdVen'
      Origin = 'paramsemp.CtaProdVen'
    end
    object QrFatPedCabEMP_FaturaSeq: TSmallintField
      FieldName = 'EMP_FaturaSeq'
      Origin = 'paramsemp.FaturaSeq'
    end
    object QrFatPedCabEMP_FaturaSep: TWideStringField
      FieldName = 'EMP_FaturaSep'
      Origin = 'paramsemp.FaturaSep'
      Size = 1
    end
    object QrFatPedCabEMP_FaturaDta: TSmallintField
      FieldName = 'EMP_FaturaDta'
      Origin = 'paramsemp.FaturaDta'
    end
    object QrFatPedCabEMP_TxtProdVen: TWideStringField
      FieldName = 'EMP_TxtProdVen'
      Origin = 'paramsemp.TxtProdVen'
      Size = 100
    end
    object QrFatPedCabEMP_FILIAL: TIntegerField
      FieldName = 'EMP_FILIAL'
      Origin = 'entidades.Filial'
    end
    object QrFatPedCabASS_CtaProdVen: TIntegerField
      FieldName = 'ASS_CtaProdVen'
      Origin = 'paramsemp.CtaProdVen'
    end
    object QrFatPedCabASS_FaturaSeq: TSmallintField
      FieldName = 'ASS_FaturaSeq'
      Origin = 'paramsemp.FaturaSeq'
    end
    object QrFatPedCabASS_FaturaSep: TWideStringField
      FieldName = 'ASS_FaturaSep'
      Origin = 'paramsemp.FaturaSep'
      Size = 1
    end
    object QrFatPedCabASS_FaturaDta: TSmallintField
      FieldName = 'ASS_FaturaDta'
      Origin = 'paramsemp.FaturaDta'
    end
    object QrFatPedCabASS_TxtProdVen: TWideStringField
      FieldName = 'ASS_TxtProdVen'
      Origin = 'paramsemp.TxtProdVen'
      Size = 100
    end
    object QrFatPedCabASS_NO_UF: TWideStringField
      FieldName = 'ASS_NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrFatPedCabASS_CO_UF: TIntegerField
      FieldName = 'ASS_CO_UF'
      Origin = 'ufs.Codigo'
      Required = True
    end
    object QrFatPedCabASS_FILIAL: TIntegerField
      FieldName = 'ASS_FILIAL'
      Origin = 'entidades.Filial'
    end
    object QrFatPedCabCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'pedivda.Cliente'
    end
    object QrFatPedCabSerieDesfe: TIntegerField
      FieldName = 'SerieDesfe'
    end
    object QrFatPedCabNFDesfeita: TIntegerField
      FieldName = 'NFDesfeita'
    end
    object QrFatPedCabPEDIDO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PEDIDO_TXT'
      Size = 21
      Calculated = True
    end
    object QrFatPedCabID_Pedido: TIntegerField
      FieldName = 'ID_Pedido'
      Required = True
    end
    object QrFatPedCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrFatPedCabNOMEFISREGCAD: TWideStringField
      FieldName = 'NOMEFISREGCAD'
      Size = 50
    end
    object QrFatPedCabEMP_UF: TIntegerField
      FieldName = 'EMP_UF'
      Required = True
    end
    object QrFatPedCabPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
    end
  end
  object DsFatPedCab: TDataSource
    DataSet = QrFatPedCab
    Left = 248
    Top = 60
  end
  object QrFatPedIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,'
      'gti.Nome NO_TAM, ggx.GraGru1, smi.Qtde QTDE_POSITIVO, '
      'smi.*, ELT(smi.Baixa+1,"Nulo","Adiciona","Subtrai") CALC_TXT,'
      'gg1.Nivel1 CO_NIVEL1, pgt.MadeBy, gg1.IPI_Alq, smi.Pecas,'
      'smi.AreaM2, smi.AreaP2, smi.Peso, gg1.prod_indTot'
      'FROM stqmovitsa smi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE smi.Tipo=1'
      'AND smi.OriCodi=:P0'
      'AND smi.OriCnta=:P1'
      ''
      'UNION'
      ''
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,'
      'gti.Nome NO_TAM, ggx.GraGru1, smi.Qtde QTDE_POSITIVO, '
      'smi.*, ELT(smi.Baixa+1,"Nulo","Adiciona","Subtrai") CALC_TXT,'
      'gg1.Nivel1 CO_NIVEL1, pgt.MadeBy, gg1.IPI_Alq, smi.Pecas,'
      'smi.AreaM2, smi.AreaP2, smi.Peso, gg1.prod_indTot'
      'FROM stqmovitsb smi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE smi.Tipo=1'
      'AND smi.OriCodi=:P0'
      'AND smi.OriCnta=:P1'
      '')
    Left = 339
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFatPedItsCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
      Origin = 'CU_NIVEL1'
    end
    object QrFatPedItsNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Origin = 'NO_NIVEL1'
      Size = 30
    end
    object QrFatPedItsGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'GraCorCad'
    end
    object QrFatPedItsCU_COR: TIntegerField
      FieldName = 'CU_COR'
      Origin = 'CU_COR'
    end
    object QrFatPedItsNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Origin = 'NO_COR'
      Size = 30
    end
    object QrFatPedItsNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Origin = 'NO_TAM'
      Size = 5
    end
    object QrFatPedItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'GraGru1'
    end
    object QrFatPedItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'DataHora'
      Required = True
    end
    object QrFatPedItsTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'Tipo'
      Required = True
    end
    object QrFatPedItsOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'OriCodi'
      Required = True
    end
    object QrFatPedItsOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
      Origin = 'OriCtrl'
      Required = True
    end
    object QrFatPedItsOriCnta: TIntegerField
      FieldName = 'OriCnta'
      Origin = 'OriCnta'
      Required = True
    end
    object QrFatPedItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'Empresa'
      Required = True
    end
    object QrFatPedItsStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = 'StqCenCad'
      Required = True
    end
    object QrFatPedItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'GraGruX'
      Required = True
    end
    object QrFatPedItsQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'Qtde'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedItsQTDE_POSITIVO: TFloatField
      FieldName = 'QTDE_POSITIVO'
      Required = True
    end
    object QrFatPedItsIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object QrFatPedItsCALC_TXT: TWideStringField
      FieldName = 'CALC_TXT'
      Size = 8
    end
    object QrFatPedItsCO_NIVEL1: TIntegerField
      FieldName = 'CO_NIVEL1'
    end
    object QrFatPedItsOriPart: TIntegerField
      FieldName = 'OriPart'
    end
    object QrFatPedItsMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrFatPedItsIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrFatPedItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrFatPedItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrFatPedItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrFatPedItsPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrFatPedItsprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
  end
  object DsFatPedIts: TDataSource
    DataSet = QrFatPedIts
    Left = 368
    Top = 60
  end
  object DsEntrega: TDataSource
    DataSet = QrEntrega
    Left = 740
    Top = 8
  end
  object QrEntrega: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntregaCalcFields
    SQL.Strings = (
      'SELECT'
      'en.LRua     RUA,'
      'en.LNumero  NUMERO,'
      'en.LCompl   COMPL,'
      'en.LBairro  BAIRRO,'
      'en.LCidade  CIDADE,'
      'lll.Nome    NOMELOGRAD,'
      'ufl.Nome    NOMEUF,'
      'en.LPais    Pais,'
      'en.LLograd  Lograd,'
      'en.LCEP     CEP,'
      'en.LEndeRef ENDEREF,'
      'en.LTel     TE1,'
      'en.LFax     FAX'
      'FROM entidades en'
      'LEFT JOIN ufs ufl ON ufl.Codigo=en.LUF'
      'LEFT JOIN listalograd lll ON lll.Codigo=en.LLograd'
      'WHERE en.Codigo=:P0'
      ''
      '')
    Left = 712
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntregaE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrEntregaCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrEntregaTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEntregaRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEntregaNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QrEntregaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEntregaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEntregaCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEntregaNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEntregaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrEntregaPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrEntregaLograd: TSmallintField
      FieldName = 'Lograd'
      Required = True
    end
    object QrEntregaCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrEntregaENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrEntregaTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntregaFAX: TWideStringField
      FieldName = 'FAX'
    end
  end
  object DsCli: TDataSource
    DataSet = QrCli
    Left = 652
    Top = 8
  end
  object QrCli: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCliAfterOpen
    BeforeClose = QrCliBeforeClose
    OnCalcFields = QrCliCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu, IE, indIEDest,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END + 0' +
        '.000 NUMERO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL,'
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE,'
      'CASE WHEN en.Tipo=0 THEN en.EUF     ELSE en.PUF  END + 0.000 UF,'
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD,'
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF,'
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's,'
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END + 0' +
        '.000 Lograd,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END + 0' +
        '.000 CEP,'
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF,'
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ' '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 624
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrCliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCliCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrCliNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCliIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrCliCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrCliPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrCliFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrCliIE: TWideStringField
      FieldName = 'IE'
    end
    object QrCliNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrCliUF: TFloatField
      FieldName = 'UF'
    end
    object QrCliCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrCliLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrCliindIEDest: TSmallintField
      FieldName = 'indIEDest'
    end
  end
  object DsPediVda: TDataSource
    DataSet = QrPediVda
    Left = 592
    Top = 8
  end
  object QrPediVda: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPediVdaAfterOpen
    BeforeClose = QrPediVdaBeforeClose
    OnCalcFields = QrPediVdaCalcFields
    SQL.Strings = (
      'SELECT '
      'pvd.Codigo, pvd.CodUsu, pvd.Empresa, pvd.Cliente,'
      'pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi, '
      'pvd.Prioridade, pvd.CondicaoPG, pvd.Moeda, '
      'pvd.Situacao, pvd.TabelaPrc, pvd.MotivoSit, pvd.LoteProd,'
      'pvd.PedidoCli, pvd.FretePor, pvd.Transporta, pvd.Redespacho,'
      'pvd.RegrFiscal, pvd.DesoAces_V, pvd.DesoAces_P,'
      'pvd.Frete_V, pvd.Frete_P, pvd.Seguro_V, pvd.Seguro_P, '
      'pvd.TotalQtd, pvd.Total_Vlr, pvd.Total_Des, pvd.Total_Tot,'
      'pvd.Observa, tpc.Nome NOMETABEPRCCAD,mda.Nome NOMEMOEDA,'
      'pvd.Represen, pvd.ComisFat, pvd.ComisRec, pvd.CartEmis,'
      'pvd.AFP_Sit, pvd.AFP_Per, ppc.MedDDSimpl, MedDDReal,'
      'pvd.ValLiq, pvd.QuantP, pvd.UsaReferen,'
      'frc.Nome NOMEFISREGCAD, imp.Nome NOMEMODELONF,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      'IF(emp.Tipo=0, emp.EUF, emp.PUF) + 0.000 EMP_UF,'
      'IF(ven.Tipo=0, ven.RazaoSocial, ven.NOME) NOMEACC,'
      'IF(tra.Tipo=0, tra.RazaoSocial, tra.NOME) NOMETRANSP,'
      'IF(red.Tipo=0, red.RazaoSocial, red.NOME) NOMEREDESP,'
      'emp.Filial, car.Nome NOMECARTEMIS, car.Tipo TIPOCART,'
      'ppc.Nome NOMECONDICAOPG, mot.Nome NOMEMOTIVO,'
      'ven.CodUsu CODUSU_ACC, tra.CodUsu CODUSU_TRA, '
      'red.CodUsu CODUSU_RED, mot.CodUsu CODUSU_MOT, '
      'tpc.CodUsu CODUSU_TPC, mda.CodUsu CODUSU_MDA, '
      'ppc.CodUsu CODUSU_PPC, tpc.JurosMes,'
      'frc.CodUsu CODUSU_FRC, imp.Codigo MODELO_NF'
      'FROM pedivda pvd'
      'LEFT JOIN entidades  emp ON emp.Codigo=pvd.Empresa'
      'LEFT JOIN entidades  tra ON tra.Codigo=pvd.Transporta'
      'LEFT JOIN entidades  red ON red.Codigo=pvd.Redespacho'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc'
      'LEFT JOIN cambiomda  mda ON mda.Codigo=pvd.Moeda'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG'
      'LEFT JOIN motivos    mot ON mot.Codigo=pvd.MotivoSit'
      'LEFT JOIN pediacc    acc ON acc.Codigo=pvd.Represen'
      'LEFT JOIN entidades  ven ON ven.Codigo=acc.Codigo'
      'LEFT JOIN carteiras  car ON car.Codigo=pvd.CartEmis'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      'LEFT JOIN imprime    imp ON imp.Codigo=frc.ModeloNF'
      'WHERE pvd.CodUsu=:P0'
      '')
    Left = 564
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaNOMEFRETEPOR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEFRETEPOR'
      Size = 50
      Calculated = True
    end
    object QrPediVdaCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'pedivda.Codigo'
      Required = True
    end
    object QrPediVdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'pedivda.CodUsu'
      Required = True
    end
    object QrPediVdaEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'pedivda.Empresa'
      Required = True
    end
    object QrPediVdaCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'pedivda.Cliente'
      Required = True
    end
    object QrPediVdaDtaEmiss: TDateField
      FieldName = 'DtaEmiss'
      Origin = 'pedivda.DtaEmiss'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPediVdaDtaEntra: TDateField
      FieldName = 'DtaEntra'
      Origin = 'pedivda.DtaEntra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPediVdaDtaInclu: TDateField
      FieldName = 'DtaInclu'
      Origin = 'pedivda.DtaInclu'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPediVdaDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
      Origin = 'pedivda.DtaPrevi'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPediVdaPrioridade: TSmallintField
      FieldName = 'Prioridade'
      Origin = 'pedivda.Prioridade'
      Required = True
    end
    object QrPediVdaCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
      Origin = 'pedivda.CondicaoPG'
      Required = True
    end
    object QrPediVdaMoeda: TIntegerField
      FieldName = 'Moeda'
      Origin = 'pedivda.Moeda'
      Required = True
    end
    object QrPediVdaSituacao: TIntegerField
      FieldName = 'Situacao'
      Origin = 'pedivda.Situacao'
      Required = True
    end
    object QrPediVdaTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Origin = 'pedivda.TabelaPrc'
      Required = True
    end
    object QrPediVdaMotivoSit: TIntegerField
      FieldName = 'MotivoSit'
      Origin = 'pedivda.MotivoSit'
      Required = True
    end
    object QrPediVdaLoteProd: TIntegerField
      FieldName = 'LoteProd'
      Origin = 'pedivda.LoteProd'
      Required = True
    end
    object QrPediVdaPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
      Origin = 'pedivda.PedidoCli'
    end
    object QrPediVdaFretePor: TSmallintField
      FieldName = 'FretePor'
      Origin = 'pedivda.FretePor'
      Required = True
    end
    object QrPediVdaTransporta: TIntegerField
      FieldName = 'Transporta'
      Origin = 'pedivda.Transporta'
      Required = True
    end
    object QrPediVdaRedespacho: TIntegerField
      FieldName = 'Redespacho'
      Origin = 'pedivda.Redespacho'
      Required = True
    end
    object QrPediVdaRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Origin = 'pedivda.RegrFiscal'
      Required = True
    end
    object QrPediVdaDesoAces_V: TFloatField
      FieldName = 'DesoAces_V'
      Origin = 'pedivda.DesoAces_V'
      Required = True
    end
    object QrPediVdaDesoAces_P: TFloatField
      FieldName = 'DesoAces_P'
      Origin = 'pedivda.DesoAces_P'
      Required = True
    end
    object QrPediVdaFrete_V: TFloatField
      FieldName = 'Frete_V'
      Origin = 'pedivda.Frete_V'
      Required = True
    end
    object QrPediVdaFrete_P: TFloatField
      FieldName = 'Frete_P'
      Origin = 'pedivda.Frete_P'
      Required = True
    end
    object QrPediVdaSeguro_V: TFloatField
      FieldName = 'Seguro_V'
      Origin = 'pedivda.Seguro_V'
      Required = True
    end
    object QrPediVdaSeguro_P: TFloatField
      FieldName = 'Seguro_P'
      Origin = 'pedivda.Seguro_P'
      Required = True
    end
    object QrPediVdaTotalQtd: TFloatField
      FieldName = 'TotalQtd'
      Origin = 'pedivda.TotalQtd'
      Required = True
    end
    object QrPediVdaTotal_Vlr: TFloatField
      FieldName = 'Total_Vlr'
      Origin = 'pedivda.Total_Vlr'
      Required = True
    end
    object QrPediVdaTotal_Des: TFloatField
      FieldName = 'Total_Des'
      Origin = 'pedivda.Total_Des'
      Required = True
    end
    object QrPediVdaTotal_Tot: TFloatField
      FieldName = 'Total_Tot'
      Origin = 'pedivda.Total_Tot'
      Required = True
    end
    object QrPediVdaObserva: TWideMemoField
      FieldName = 'Observa'
      Origin = 'pedivda.Observa'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPediVdaRepresen: TIntegerField
      FieldName = 'Represen'
      Origin = 'pedivda.Represen'
      Required = True
    end
    object QrPediVdaComisFat: TFloatField
      FieldName = 'ComisFat'
      Origin = 'pedivda.ComisFat'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaComisRec: TFloatField
      FieldName = 'ComisRec'
      Origin = 'pedivda.ComisRec'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaCartEmis: TIntegerField
      FieldName = 'CartEmis'
      Origin = 'pedivda.CartEmis'
      Required = True
    end
    object QrPediVdaAFP_Sit: TSmallintField
      FieldName = 'AFP_Sit'
      Origin = 'pedivda.AFP_Sit'
      Required = True
    end
    object QrPediVdaAFP_Per: TFloatField
      FieldName = 'AFP_Per'
      Origin = 'pedivda.AFP_Per'
      Required = True
    end
    object QrPediVdaMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
      Origin = 'pediprzcab.MedDDSimpl'
    end
    object QrPediVdaMedDDReal: TFloatField
      FieldName = 'MedDDReal'
      Origin = 'pediprzcab.MedDDReal'
    end
    object QrPediVdaValLiq: TFloatField
      FieldName = 'ValLiq'
      Origin = 'pedivda.ValLiq'
      Required = True
    end
    object QrPediVdaQuantP: TFloatField
      FieldName = 'QuantP'
      Origin = 'pedivda.QuantP'
      Required = True
    end
    object QrPediVdaFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
    end
    object QrPediVdaNOMETABEPRCCAD: TWideStringField
      FieldName = 'NOMETABEPRCCAD'
      Origin = 'tabeprccab.Nome'
      Size = 50
    end
    object QrPediVdaNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Origin = 'cambiomda.Nome'
      Size = 30
    end
    object QrPediVdaNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrPediVdaNOMEACC: TWideStringField
      FieldName = 'NOMEACC'
      Size = 100
    end
    object QrPediVdaNOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrPediVdaNOMEREDESP: TWideStringField
      FieldName = 'NOMEREDESP'
      Size = 100
    end
    object QrPediVdaNOMEFISREGCAD: TWideStringField
      FieldName = 'NOMEFISREGCAD'
      Origin = 'fisregcad.Nome'
      Size = 50
    end
    object QrPediVdaNOMEMODELONF: TWideStringField
      FieldName = 'NOMEMODELONF'
      Origin = 'imprime.Nome'
      Size = 100
    end
    object QrPediVdaNOMECARTEMIS: TWideStringField
      FieldName = 'NOMECARTEMIS'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrPediVdaNOMECONDICAOPG: TWideStringField
      FieldName = 'NOMECONDICAOPG'
      Origin = 'pediprzcab.Nome'
      Size = 50
    end
    object QrPediVdaNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Origin = 'motivos.Nome'
      Size = 50
    end
    object QrPediVdaCODUSU_ACC: TIntegerField
      FieldName = 'CODUSU_ACC'
      Origin = 'entidades.CodUsu'
      Required = True
    end
    object QrPediVdaCODUSU_TRA: TIntegerField
      FieldName = 'CODUSU_TRA'
      Origin = 'entidades.CodUsu'
      Required = True
    end
    object QrPediVdaCODUSU_RED: TIntegerField
      FieldName = 'CODUSU_RED'
      Origin = 'entidades.CodUsu'
      Required = True
    end
    object QrPediVdaCODUSU_MOT: TIntegerField
      FieldName = 'CODUSU_MOT'
      Origin = 'motivos.CodUsu'
      Required = True
    end
    object QrPediVdaCODUSU_TPC: TIntegerField
      FieldName = 'CODUSU_TPC'
      Origin = 'tabeprccab.CodUsu'
      Required = True
    end
    object QrPediVdaCODUSU_MDA: TIntegerField
      FieldName = 'CODUSU_MDA'
      Origin = 'cambiomda.CodUsu'
      Required = True
    end
    object QrPediVdaCODUSU_PPC: TIntegerField
      FieldName = 'CODUSU_PPC'
      Origin = 'pediprzcab.CodUsu'
      Required = True
    end
    object QrPediVdaCODUSU_FRC: TIntegerField
      FieldName = 'CODUSU_FRC'
      Origin = 'fisregcad.CodUsu'
      Required = True
    end
    object QrPediVdaMODELO_NF: TIntegerField
      FieldName = 'MODELO_NF'
      Origin = 'imprime.Codigo'
      Required = True
    end
    object QrPediVdaTIPOCART: TIntegerField
      FieldName = 'TIPOCART'
      Origin = 'carteiras.Tipo'
      Required = True
    end
    object QrPediVdaUsaReferen: TSmallintField
      FieldName = 'UsaReferen'
      Origin = 'pedivda.UsaReferen'
    end
    object QrPediVdaEMP_UF: TFloatField
      FieldName = 'EMP_UF'
    end
    object QrPediVdaJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPediVdaidDest: TSmallintField
      FieldName = 'idDest'
    end
    object QrPediVdaindFinal: TSmallintField
      FieldName = 'indFinal'
    end
    object QrPediVdaindPres: TSmallintField
      FieldName = 'indPres'
    end
    object QrPediVdaIndSinc: TSmallintField
      FieldName = 'IndSinc'
    end
    object QrPediVdafinNFe: TSmallintField
      FieldName = 'finNFe'
    end
  end
  object QrFatPedVol: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFatPedVolAfterOpen
    BeforeClose = QrFatPedVolBeforeClose
    AfterScroll = QrFatPedVolAfterScroll
    SQL.Strings = (
      'SELECT med.Nome NO_UnidMed, fpv.* '
      'FROM fatpedvol fpv'
      'LEFT JOIN unidmed med ON med.Codigo=fpv.UnidMed'
      'WHERE fpv.Codigo=:P0')
    Left = 280
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatPedVolNO_UnidMed: TWideStringField
      FieldName = 'NO_UnidMed'
      Size = 30
    end
    object QrFatPedVolCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFatPedVolCnta: TIntegerField
      FieldName = 'Cnta'
      Required = True
    end
    object QrFatPedVolUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
  end
  object DsFatPedVol: TDataSource
    DataSet = QrFatPedVol
    Left = 308
    Top = 60
  end
  object PMVolume: TPopupMenu
    OnPopup = PMVolumePopup
    Left = 564
    Top = 36
    object Incluinovovolume1: TMenuItem
      Caption = '&Inclui novo volume'
      OnClick = Incluinovovolume1Click
    end
    object Alteravolumeatual1: TMenuItem
      Caption = '&Altera volume atual'
      OnClick = Alteravolumeatual1Click
    end
    object Excluivolumeatual1: TMenuItem
      Caption = '&Exclui volume atual'
      OnClick = Excluivolumeatual1Click
    end
  end
  object PMItens: TPopupMenu
    OnPopup = PMItensPopup
    Left = 592
    Top = 36
    object Incluiitenss1: TMenuItem
      Caption = '&Inclui item(ns)'
      OnClick = Incluiitenss1Click
    end
    object Excluiitemns1: TMenuItem
      Caption = '&Exclui item(ns)'
      OnClick = Excluiitemns1Click
    end
  end
  object QrSCCs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 256
    Top = 12
  end
  object PMImprime: TPopupMenu
    Left = 624
    Top = 36
    object Anlise1: TMenuItem
      Caption = '&An'#225'lise'
      OnClick = Anlise1Click
    end
    object Romaneioporgrupo1: TMenuItem
      Caption = '&Romaneio por grupo'
      OnClick = Romaneioporgrupo1Click
    end
  end
  object QrAnalise: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT smva.Preco, SUM(smva.Total) Total, '
      'smva.GraGruX, SUM(smva.Qtde) Qtde, ent.Filial,'
      'gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,'
      'gti.Nome NO_TAM, ggx.GraGru1 '
      'FROM stqmovvala smva'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      
        'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI LEFT JOIN en' +
        'tidades ent ON ent.Codigo=smva.Empresa      '
      ''
      ''
      'WHERE smva.Tipo=1'
      'AND smva.OriCodi=:P0'
      'GROUP BY GraGruX, SeqInReduz'
      'ORDER BY NO_NIVEL1, NO_COR, NO_TAM, Filial')
    Left = 776
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAnalisePreco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
    object QrAnaliseTotal: TFloatField
      FieldName = 'Total'
    end
    object QrAnaliseFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrAnaliseCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
    end
    object QrAnaliseNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrAnaliseGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrAnaliseCU_COR: TIntegerField
      FieldName = 'CU_COR'
    end
    object QrAnaliseNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrAnaliseNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrAnaliseGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrAnaliseGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrAnaliseQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object frxDsAnalise: TfrxDBDataset
    UserName = 'frxDsAnalise'
    CloseDataSource = False
    DataSet = QrAnalise
    BCDToCurrency = False
    
    Left = 800
    Top = 8
  end
  object frxFAT_PEDID_001_00: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    Left = 656
    Top = 36
    Datasets = <
      item
        DataSet = frxDsAnalise
        DataSetName = 'frxDsAnalise'
      end
      item
        DataSet = frxDsCli
        DataSetName = 'frxDsCli'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDSFatPedCab
        DataSetName = 'frxDSFatPedCab'
      end
      item
        DataSet = frxDsPediVda
        DataSetName = 'frxDsPediVda'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 124.724480240000000000
        Top = 56.692950000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 684.094930000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 684.094930000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 668.976810000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'AN'#193'LISE DO FATURAMENTO N'#186' [frxDSFatPedCab."CodUsu"]  - PEDIDO N'#186 +
              ' [frxDSFatPedCab."CU_PediVda"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 1.779530000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 544.251946540000000000
          Width = 132.283476770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708720000000000000
          Width = 37.795300000000000000
          Height = 30.236230240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Filial')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 90.708720000000000000
          Width = 56.692950000000000000
          Height = 30.236230240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Quantia')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 90.708720000000000000
          Width = 56.692950000000000000
          Height = 30.236230240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pre'#231'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 90.708720000000000000
          Width = 75.590600000000000000
          Height = 30.236230240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 90.708720000000000000
          Width = 120.944960000000000000
          Height = 30.236230240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 45.354360000000000000
          Width = 638.740570000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            
              '[frxDsCli."CodUsu"] - [frxDsCli."NOME_TIPO_DOC"] [frxDsCli."CNPJ' +
              '_TXT"] - [frxDsCli."NOME_ENT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 45.354360000000000000
          Width = 30.236240000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 64.252010000000000000
          Width = 612.283860000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPediVda."NOMEACC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 64.252010000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Representante:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsAnalise."GraGruX"'
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalise."CU_NIVEL1"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 3.779530000000000000
          Width = 302.362400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAnalise."NO_NIVEL1"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 3.779530000000000000
          Width = 166.299320000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAnalise."NO_COR"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Top = 3.779530000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAnalise."NO_TAM"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Top = 3.779530000000000000
          Width = 79.370130000000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalise."GraGruX"]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 283.464750000000000000
        Width = 680.315400000000000000
        ColumnGap = 37.795275590551200000
        DataSet = frxDsAnalise
        DataSetName = 'frxDsAnalise'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataField = 'Filial'
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAnalise."Filial"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataField = 'Qtde'
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalise."Qtde"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataField = 'Preco'
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalise."Preco"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataField = 'Total'
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalise."Total"]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 120.944960000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalise."Qtde">)/2]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalise."Preco">)]'
            '')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalise."Total">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 120.944960000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 396.850650000000000000
        Width = 680.315400000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 7.559059999999988000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalise."Qtde">)/2]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 7.559059999999988000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsAnalise."Qtde">)=0, 0, SUM(<frxDsAnalise."Total">' +
              ') / SUM(<frxDsAnalise."Qtde">) * 2)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 7.559059999999988000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalise."Total">)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559059999999988000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 7.559060000000000000
          Width = 120.944960000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDSFatPedCab: TfrxDBDataset
    UserName = 'frxDSFatPedCab'
    CloseDataSource = False
    DataSet = QrFatPedCab
    BCDToCurrency = False
    
    Left = 220
    Top = 88
  end
  object frxDsPediVda: TfrxDBDataset
    UserName = 'frxDsPediVda'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEFRETEPOR=NOMEFRETEPOR'
      'Codigo=Codigo'
      'CodUsu=CodUsu'
      'Empresa=Empresa'
      'Cliente=Cliente'
      'DtaEmiss=DtaEmiss'
      'DtaEntra=DtaEntra'
      'DtaInclu=DtaInclu'
      'DtaPrevi=DtaPrevi'
      'Prioridade=Prioridade'
      'CondicaoPG=CondicaoPG'
      'Moeda=Moeda'
      'Situacao=Situacao'
      'TabelaPrc=TabelaPrc'
      'MotivoSit=MotivoSit'
      'LoteProd=LoteProd'
      'PedidoCli=PedidoCli'
      'FretePor=FretePor'
      'Transporta=Transporta'
      'Redespacho=Redespacho'
      'RegrFiscal=RegrFiscal'
      'DesoAces_V=DesoAces_V'
      'DesoAces_P=DesoAces_P'
      'Frete_V=Frete_V'
      'Frete_P=Frete_P'
      'Seguro_V=Seguro_V'
      'Seguro_P=Seguro_P'
      'TotalQtd=TotalQtd'
      'Total_Vlr=Total_Vlr'
      'Total_Des=Total_Des'
      'Total_Tot=Total_Tot'
      'Observa=Observa'
      'Represen=Represen'
      'ComisFat=ComisFat'
      'ComisRec=ComisRec'
      'CartEmis=CartEmis'
      'AFP_Sit=AFP_Sit'
      'AFP_Per=AFP_Per'
      'MedDDSimpl=MedDDSimpl'
      'MedDDReal=MedDDReal'
      'ValLiq=ValLiq'
      'QuantP=QuantP'
      'Filial=Filial'
      'NOMETABEPRCCAD=NOMETABEPRCCAD'
      'NOMEMOEDA=NOMEMOEDA'
      'NOMEEMP=NOMEEMP'
      'NOMEACC=NOMEACC'
      'NOMETRANSP=NOMETRANSP'
      'NOMEREDESP=NOMEREDESP'
      'NOMEFISREGCAD=NOMEFISREGCAD'
      'NOMEMODELONF=NOMEMODELONF'
      'NOMECARTEMIS=NOMECARTEMIS'
      'NOMECONDICAOPG=NOMECONDICAOPG'
      'NOMEMOTIVO=NOMEMOTIVO'
      'CODUSU_ACC=CODUSU_ACC'
      'CODUSU_TRA=CODUSU_TRA'
      'CODUSU_RED=CODUSU_RED'
      'CODUSU_MOT=CODUSU_MOT'
      'CODUSU_TPC=CODUSU_TPC'
      'CODUSU_MDA=CODUSU_MDA'
      'CODUSU_PPC=CODUSU_PPC'
      'CODUSU_FRC=CODUSU_FRC'
      'MODELO_NF=MODELO_NF'
      'TIPOCART=TIPOCART'
      'UsaReferen=UsaReferen'
      'EMP_UF=EMP_UF'
      'JurosMes=JurosMes'
      'idDest=idDest'
      'indFinal=indFinal'
      'indPres=indPres'
      'IndSinc=IndSinc')
    DataSet = QrPediVda
    BCDToCurrency = False
    
    Left = 536
    Top = 8
  end
  object frxDsCli: TfrxDBDataset
    UserName = 'frxDsCli'
    CloseDataSource = False
    DataSet = QrCli
    BCDToCurrency = False
    
    Left = 680
    Top = 8
  end
  object PMEncerra: TPopupMenu
    OnPopup = PMEncerraPopup
    Left = 652
    Top = 488
    object Encerrafaturamento1: TMenuItem
      Caption = '&Encerra faturamento'
      OnClick = Encerrafaturamento1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Desfazencerramento1: TMenuItem
      Caption = '&Desfaz encerramento'
      OnClick = Desfazencerramento1Click
    end
  end
  object QrL_C: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, FatParcela, Carteira'
      'FROM lanctos'
      'WHERE FatID=1801'
      'AND FatNum=2066'
      '')
    Left = 300
    Top = 12
    object QrL_CFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrL_CFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrL_CFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrL_CCarteira: TIntegerField
      FieldName = 'Carteira'
    end
  end
  object PMComissao: TPopupMenu
    OnPopup = PMComissaoPopup
    Left = 744
    Top = 460
    object Incluicomisses1: TMenuItem
      Caption = '&Inclui comiss'#245'es'
      Enabled = False
      OnClick = Incluicomisses1Click
    end
    object Excluicomisses1: TMenuItem
      Caption = '&Exclui comiss'#245'es'
      Enabled = False
      OnClick = Excluicomisses1Click
    end
  end
end
