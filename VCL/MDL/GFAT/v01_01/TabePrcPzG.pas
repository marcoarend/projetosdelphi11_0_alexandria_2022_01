unit TabePrcPzG;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral, Variants, dmkImage,
  UnDmkEnums, DmkDAC_PF;

type
  TFmTabePrcPzG = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdVariacao: TdmkEdit;
    Label2: TLabel;
    QrJaExiste: TmySQLQuery;
    Panel3: TPanel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    CkContinuar: TCheckBox;
    EdDiasIni: TdmkEdit;
    EdDiasFim: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    EdControle: TdmkEdit;
    Label8: TLabel;
    QrJaExisteControle: TIntegerField;
    QrMinMax: TmySQLQuery;
    QrMinMaxMin: TSmallintField;
    QrMinMaxMax: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdDiasIniChange(Sender: TObject);
    procedure EdDiasFimChange(Sender: TObject);
    procedure EdDiasFimExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function ImpedePorDuplicidade(): Boolean;
    procedure CriaNome();
  public
    { Public declarations }
  end;

  var
  FmTabePrcPzG: TFmTabePrcPzG;

implementation

uses UnMyObjects, Module, TabePrcCab, UMySQLModule;

{$R *.DFM}

procedure TFmTabePrcPzG.BtOKClick(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  if ImpedePorDuplicidade() then Exit;
  if Trim(EdNome.Text) = '' then
  begin
    Application.MessageBox('Informe uma descri��o!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Codigo := FmTabePrcCab.QrTabePrcCabCodigo.Value;
  Controle := UMyMod.BuscaEmLivreY_Def('tabeprcpzg', 'Controle', ImgTipo.SQLType,
    FmTabePrcCab.QrTabePrcPzGControle.Value);
  EdControle.ValueVariant := Controle;
  if UMyMod.ExecSQLInsUpdFm(FmTabePrcPzG, ImgTipo.SQLType, 'TabePrcPzG', Codigo,
  Dmod.QrUpd) then
  begin
    FmTabePrcCab.ReopenTabePrcPzG(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant := 0;
      EdDiasIni.ValueVariant  := 0;
      EdDiasFim.ValueVariant  := 0;
      EdNome.ValueVariant     := '';
      EdVariacao.ValueVariant := 0;
      EdDiasIni.SetFocus;
    end else Close;
  end;
end;

procedure TFmTabePrcPzG.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTabePrcPzG.CriaNome;
var
  Nome: String;
  Ini, Fim, Casas: Integer;
begin
  if ImgTipo.SQLType <> stIns then Exit;
  Ini := Geral.IMV(EdDiasIni.Text);
  Fim := Geral.IMV(EdDiasFim.Text);
  if (Ini > 99) or (Fim > 99) then Casas := 3 else Casas := 2;
  Nome := Geral.FTX(Ini, Casas, 0, siPositivo) + '-' +
          Geral.FTX(Fim, Casas, 0, siPositivo);
  EdNome.Text := Nome;
end;

procedure TFmTabePrcPzG.EdDiasFimChange(Sender: TObject);
begin
  CriaNome();
end;

procedure TFmTabePrcPzG.EdDiasFimExit(Sender: TObject);
begin
  if Geral.IMV(EdDiasFim.Text) < Geral.IMV(EdDiasIni.Text) then
    EdDiasFim.Text := EdDiasIni.Text;
end;

procedure TFmTabePrcPzG.EdDiasIniChange(Sender: TObject);
begin
  CriaNome();
end;

function TFmTabePrcPzG.ImpedePorDuplicidade(): Boolean;
var
  i, Ini, Fim: Integer;
begin
  Result := False;
  if ImgTipo.SQLType <> stIns then Exit;
  Ini := Geral.IMV(EdDiasIni.Text);
  Fim := Geral.IMV(EdDiasFim.Text);
  QrMinMax.Close;
  QrMinMax.Params[0].AsInteger := FmTabePrcCab.QrTabePrcCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrMinMax, Dmod.MyDB);
  //
  if Ini < QrMinMaxMin.Value then
  begin
    if Fim < QrMinMaxMin.Value then
      Exit
    else Ini := QrMinMaxMin.Value;
  end;
  if Fim > QrMinMaxMax.Value then
  begin
    if Ini > QrMinMaxMax.Value then
      Exit
    else Fim := QrMinMaxMax.Value;
  end;
  //
  for i := Ini to Fim do
  begin
    QrJaExiste.Close;
    QrJaExiste.Params[00].AsInteger := FmTabePrcCab.QrTabePrcCabCodigo.Value;
    QrJaExiste.Params[01].AsInteger := i;
    QrJaExiste.Params[02].AsInteger := i;
    UnDmkDAC_PF.AbreQuery(QrJaExiste, Dmod.MyDB);
    //
    if QrJaExiste.RecordCount > 0 then
    begin
      Result := True;
      Application.MessageBox(PChar('O per�odo est� em conflito com o ' +
      'per�odo do ID ' + IntToStr(QrJaExisteControle.Value) + '!'), 'Aviso',
      MB_OK+MB_ICONWARNING);
      Break;
    end;
  end;
end;

procedure TFmTabePrcPzG.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTabePrcPzG.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmTabePrcPzG.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
