unit FatDivGruIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkEditCalc, dmkEdit,
  dmkImage, UnDmkEnums;

type
  TFmFatDivGruIts = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    EdNivel1: TdmkEdit;
    EdNomeNivel1: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    EdQtde: TdmkEdit;
    GBBaixa: TGroupBox;
    LaPeso: TLabel;
    LaAreaP2: TLabel;
    LaAreaM2: TLabel;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    EdPeso: TdmkEdit;
    EdAreaP2: TdmkEditCalc;
    EdAreaM2: TdmkEditCalc;
    Label5: TLabel;
    EdGraGruX: TdmkEdit;
    EdNomeTam: TdmkEdit;
    Label6: TLabel;
    EdNomeCor: TdmkEdit;
    Label7: TLabel;
    EdPreco: TdmkEdit;
    Label8: TLabel;
    EdDesco: TdmkEdit;
    Label9: TLabel;
    EdTipoCalc: TdmkEdit;
    EdTipo_TXT: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtDesiste: TBitBtn;
    SbDecimalSizePrc: TSpeedButton;
    SbDecimalSizeQtd: TSpeedButton;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdTipoCalcChange(Sender: TObject);
    procedure SbDecimalSizePrcClick(Sender: TObject);
    procedure SbDecimalSizeQtdClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConfirmou: Boolean;
    FCol, FRow, FHowBxaEstq: Integer;
  end;

  var
  FmFatDivGruIts: TFmFatDivGruIts;

implementation

{$R *.DFM}

uses UnMyObjects, Module, dmkGeral, UnGrade_PF;

procedure TFmFatDivGruIts.BtDesisteClick(Sender: TObject);
begin
  FConfirmou := False;
  Close;
end;

procedure TFmFatDivGruIts.BtOKClick(Sender: TObject);
var
  Msg: String;
begin
  if GBBaixa.Visible = True then
  begin
    if not Grade_PF.ValidaGrandeza(EdTipoCalc.ValueVariant, FHowBxaEstq,
      EdPecas.ValueVariant, EdAreaM2.ValueVariant, EdAreaP2.ValueVariant,
      EdPeso.ValueVariant, Msg) then
    begin
      Geral.MB_Aviso(Msg);
      Exit;
    end;
  end;
  //
  FConfirmou := True;
  //
  Close;
end;

procedure TFmFatDivGruIts.EdTipoCalcChange(Sender: TObject);
begin
  case EdTipoCalc.ValueVariant of
    0: EdTipo_Txt.Text := 'Nulo';
    1: EdTipo_Txt.Text := 'Adiciona';
    2: EdTipo_Txt.Text := 'Subtrai';
  end;
  GBBaixa.Visible := EdTipoCalc.ValueVariant > 0;
end;

procedure TFmFatDivGruIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  EdPreco.DecimalSize := Dmod.FFmtPrc;
end;

procedure TFmFatDivGruIts.FormCreate(Sender: TObject);
begin
  FConfirmou := False;
end;

procedure TFmFatDivGruIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatDivGruIts.SbDecimalSizePrcClick(Sender: TObject);
begin
  EdPreco.DecimalSize := 10;
end;

procedure TFmFatDivGruIts.SbDecimalSizeQtdClick(Sender: TObject);
begin
  EdQtde.DecimalSize := 4;
end;

end.
