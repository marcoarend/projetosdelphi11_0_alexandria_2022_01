object FmFatPedCab2: TFmFatPedCab2
  Left = 368
  Top = 194
  Caption = 'FAT-PEDID-001 :: Faturamento de Pedido'
  ClientHeight = 636
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 540
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 128
    ExplicitTop = 92
    object PainelConfirma: TPanel
      Left = 1
      Top = 491
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 871
        Top = 1
        Width = 134
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 3
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 368
      Align = alTop
      ParentBackground = False
      TabOrder = 1
      object GroupBox9: TGroupBox
        Left = 1
        Top = 1
        Width = 1004
        Height = 58
        Align = alTop
        TabOrder = 0
        object Label56: TLabel
          Left = 8
          Top = 12
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label2: TLabel
          Left = 8
          Top = 36
          Width = 36
          Height = 13
          Caption = 'Pedido:'
        end
        object Label64: TLabel
          Left = 374
          Top = 36
          Width = 50
          Height = 13
          Caption = 'Prioridade:'
        end
        object Label65: TLabel
          Left = 490
          Top = 36
          Width = 101
          Height = 13
          Caption = 'Previs'#227'o entrega:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label33: TLabel
          Left = 832
          Top = 12
          Width = 43
          Height = 13
          Caption = 'Abertura:'
          Enabled = False
        end
        object Label31: TLabel
          Left = 728
          Top = 36
          Width = 58
          Height = 13
          Caption = 'ID Faturam.:'
        end
        object Label32: TLabel
          Left = 848
          Top = 36
          Width = 80
          Height = 13
          Caption = 'C'#243'digo Faturam.:'
        end
        object SpeedButton5: TSpeedButton
          Left = 152
          Top = 32
          Width = 24
          Height = 24
          Caption = '?'
          OnClick = SpeedButton5Click
        end
        object DBEdit3: TDBEdit
          Left = 56
          Top = 8
          Width = 53
          Height = 21
          DataField = 'Filial'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit2: TDBEdit
          Left = 114
          Top = 8
          Width = 694
          Height = 21
          DataField = 'NOMEEMP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object EdPedido: TdmkEdit
          Left = 56
          Top = 32
          Width = 93
          Height = 24
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdPedidoExit
        end
        object DBEdit12: TDBEdit
          Left = 428
          Top = 32
          Width = 53
          Height = 21
          DataField = 'Prioridade'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 3
        end
        object DBEdit11: TDBEdit
          Left = 595
          Top = 32
          Width = 129
          Height = 21
          DataField = 'DtaPrevi'
          DataSource = DsPediVda
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
        end
        object EdCodigo: TdmkEdit
          Left = 788
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdCodUsu: TdmkEdit
          Left = 932
          Top = 32
          Width = 56
          Height = 24
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAbertura: TdmkEdit
          Left = 876
          Top = 8
          Width = 112
          Height = 21
          Enabled = False
          TabOrder = 7
          FormatType = dmktfDateTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '30/12/1899 00:00:00'
          QryCampo = 'Abertura'
          UpdCampo = 'Abertura'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox1: TGroupBox
        Left = 1
        Top = 59
        Width = 1004
        Height = 136
        Align = alTop
        TabOrder = 1
        object Label8: TLabel
          Left = 8
          Top = 12
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object DBText1: TDBText
          Left = 832
          Top = 12
          Width = 45
          Height = 17
          DataField = 'NOME_TIPO_DOC'
          DataSource = DsCli
        end
        object Label3: TLabel
          Left = 8
          Top = 84
          Width = 40
          Height = 13
          Caption = 'Entrega:'
        end
        object DBEdit1: TDBEdit
          Left = 60
          Top = 8
          Width = 72
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsCli
          Enabled = False
          TabOrder = 0
        end
        object DBEdit6: TDBEdit
          Left = 132
          Top = 8
          Width = 693
          Height = 21
          DataField = 'NOME_ENT'
          DataSource = DsCli
          Enabled = False
          TabOrder = 1
        end
        object DBMemo1: TDBMemo
          Left = 56
          Top = 32
          Width = 932
          Height = 47
          DataField = 'E_ALL'
          DataSource = DsCli
          Enabled = False
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 876
          Top = 8
          Width = 112
          Height = 21
          DataField = 'CNPJ_TXT'
          DataSource = DsCli
          Enabled = False
          TabOrder = 3
        end
        object MeEnderecoEntrega1: TMemo
          Left = 56
          Top = 80
          Width = 932
          Height = 53
          TabStop = False
          ReadOnly = True
          TabOrder = 4
        end
      end
      object GroupBox12: TGroupBox
        Left = 1
        Top = 195
        Width = 1004
        Height = 34
        Align = alTop
        TabOrder = 2
        object Label16: TLabel
          Left = 8
          Top = 12
          Width = 73
          Height = 13
          Caption = 'Representante:'
        end
        object Label17: TLabel
          Left = 640
          Top = 11
          Width = 117
          Height = 13
          Caption = '% comiss'#227'o faturamento:'
        end
        object Label18: TLabel
          Left = 819
          Top = 11
          Width = 119
          Height = 13
          Caption = '% comiss'#227'o recebimento:'
        end
        object DBEdit44: TDBEdit
          Left = 84
          Top = 8
          Width = 57
          Height = 21
          DataField = 'CODUSU_ACC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit45: TDBEdit
          Left = 144
          Top = 8
          Width = 489
          Height = 21
          DataField = 'NOMEACC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit46: TDBEdit
          Left = 760
          Top = 8
          Width = 48
          Height = 21
          DataField = 'ComisFat'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBEdit47: TDBEdit
          Left = 939
          Top = 8
          Width = 48
          Height = 21
          DataField = 'ComisRec'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 3
        end
      end
      object GroupBox15: TGroupBox
        Left = 1
        Top = 229
        Width = 1004
        Height = 43
        Align = alTop
        Caption = ' Fiscal:'
        TabOrder = 3
        object Label34: TLabel
          Left = 8
          Top = 20
          Width = 73
          Height = 13
          Caption = 'Movimenta'#231#227'o:'
        end
        object Label35: TLabel
          Left = 640
          Top = 20
          Width = 55
          Height = 13
          Caption = 'Modelo NF:'
        end
        object DBEdit48: TDBEdit
          Left = 84
          Top = 16
          Width = 57
          Height = 21
          DataField = 'CODUSU_FRC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit49: TDBEdit
          Left = 144
          Top = 16
          Width = 489
          Height = 21
          DataField = 'NOMEFISREGCAD'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit50: TDBEdit
          Left = 700
          Top = 16
          Width = 287
          Height = 21
          DataField = 'NOMEMODELONF'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
      end
      object GroupBox14: TGroupBox
        Left = 1
        Top = 272
        Width = 1004
        Height = 82
        Align = alTop
        TabOrder = 4
        object Label51: TLabel
          Left = 8
          Top = 13
          Width = 45
          Height = 13
          Caption = 'Frete por:'
        end
        object Label52: TLabel
          Left = 8
          Top = 36
          Width = 75
          Height = 13
          Caption = 'Transportadora:'
        end
        object Label53: TLabel
          Left = 8
          Top = 61
          Width = 64
          Height = 13
          Caption = 'Redespacho:'
        end
        object Label66: TLabel
          Left = 268
          Top = 12
          Width = 98
          Height = 13
          Caption = '% Desp. acess'#243'rias: '
          Visible = False
        end
        object Label67: TLabel
          Left = 404
          Top = 12
          Width = 96
          Height = 13
          Caption = '$ Desp. acess'#243'rias: '
          Visible = False
        end
        object Label68: TLabel
          Left = 572
          Top = 12
          Width = 38
          Height = 13
          Caption = '% Frete:'
          Visible = False
        end
        object Label70: TLabel
          Left = 652
          Top = 12
          Width = 36
          Height = 13
          Caption = '$ Frete:'
          Visible = False
        end
        object Label71: TLabel
          Left = 772
          Top = 12
          Width = 48
          Height = 13
          Caption = '% Seguro:'
          Visible = False
        end
        object Label72: TLabel
          Left = 864
          Top = 12
          Width = 46
          Height = 13
          Caption = '$ Seguro:'
          Visible = False
        end
        object DBEdit16: TDBEdit
          Left = 56
          Top = 8
          Width = 21
          Height = 21
          DataField = 'FretePor'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit23: TDBEdit
          Left = 76
          Top = 8
          Width = 188
          Height = 21
          DataField = 'NOMEFRETEPOR'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit34: TDBEdit
          Left = 88
          Top = 32
          Width = 56
          Height = 21
          DataField = 'CODUSU_TRA'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBEdit35: TDBEdit
          Left = 88
          Top = 56
          Width = 56
          Height = 21
          DataField = 'CODUSU_RED'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 3
        end
        object DBEdit36: TDBEdit
          Left = 148
          Top = 32
          Width = 839
          Height = 21
          DataField = 'NOMETRANSP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 4
        end
        object DBEdit37: TDBEdit
          Left = 148
          Top = 56
          Width = 839
          Height = 21
          DataField = 'NOMEREDESP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 5
        end
        object DBEdit38: TDBEdit
          Left = 504
          Top = 8
          Width = 64
          Height = 21
          DataField = 'DesoAces_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 6
          Visible = False
        end
        object DBEdit39: TDBEdit
          Left = 364
          Top = 8
          Width = 36
          Height = 21
          DataField = 'DesoAces_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 7
          Visible = False
        end
        object DBEdit40: TDBEdit
          Left = 696
          Top = 8
          Width = 72
          Height = 21
          DataField = 'Frete_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 8
          Visible = False
        end
        object DBEdit41: TDBEdit
          Left = 612
          Top = 8
          Width = 36
          Height = 21
          DataField = 'Frete_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 9
          Visible = False
        end
        object DBEdit42: TDBEdit
          Left = 916
          Top = 8
          Width = 71
          Height = 21
          DataField = 'Seguro_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 10
          Visible = False
        end
        object DBEdit43: TDBEdit
          Left = 824
          Top = 8
          Width = 36
          Height = 21
          DataField = 'Seguro_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 11
          Visible = False
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 1
      Top = 427
      Width = 1006
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 1002
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 6
        object PnSaiDesis: TPanel
          Left = 859
          Top = 0
          Width = 143
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 540
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 412
      Width = 1006
      Height = 63
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object DBGFatPedVol: TDBGrid
        Left = 0
        Top = 0
        Width = 156
        Height = 63
        Align = alLeft
        DataSource = DsFatPedVol
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_UnidMed'
            Title.Caption = 'Volume'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cnta'
            Title.Caption = 'N'#250'mero'
            Width = 52
            Visible = True
          end>
      end
      object DBGFatPedIts: TDBGrid
        Left = 156
        Top = 0
        Width = 850
        Height = 63
        Align = alClient
        DataSource = DsFatPedIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CU_NIVEL1'
            Title.Caption = 'Produto'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_NIVEL1'
            Title.Caption = 'Descri'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CU_COR'
            Title.Caption = 'Cor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_COR'
            Title.Caption = 'Descri'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TAM'
            Title.Caption = 'Tamanho'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Caption = 'Quantidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CALC_TXT'
            Title.Caption = 'C'#225'lculo'
            Width = 61
            Visible = True
          end>
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 368
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 1006
        Height = 58
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 12
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label5: TLabel
          Left = 8
          Top = 36
          Width = 36
          Height = 13
          Caption = 'Pedido:'
        end
        object Label6: TLabel
          Left = 374
          Top = 36
          Width = 50
          Height = 13
          Caption = 'Prioridade:'
        end
        object Label9: TLabel
          Left = 490
          Top = 36
          Width = 101
          Height = 13
          Caption = 'Previs'#227'o entrega:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label10: TLabel
          Left = 728
          Top = 36
          Width = 58
          Height = 13
          Caption = 'ID Faturam.:'
        end
        object Label11: TLabel
          Left = 848
          Top = 36
          Width = 80
          Height = 13
          Caption = 'C'#243'digo Faturam.:'
        end
        object Label36: TLabel
          Left = 820
          Top = 12
          Width = 52
          Height = 13
          Caption = 'Encerrado:'
        end
        object Label37: TLabel
          Left = 656
          Top = 12
          Width = 43
          Height = 13
          Caption = 'Abertura:'
        end
        object DBEdit5: TDBEdit
          Left = 56
          Top = 8
          Width = 53
          Height = 21
          DataField = 'Filial'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit7: TDBEdit
          Left = 114
          Top = 8
          Width = 539
          Height = 21
          DataField = 'NOMEEMP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit8: TDBEdit
          Left = 428
          Top = 32
          Width = 53
          Height = 21
          DataField = 'Prioridade'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBEdit9: TDBEdit
          Left = 595
          Top = 32
          Width = 129
          Height = 21
          DataField = 'DtaPrevi'
          DataSource = DsPediVda
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
        object DBEdit53: TDBEdit
          Left = 788
          Top = 32
          Width = 56
          Height = 21
          DataField = 'Codigo'
          DataSource = DsFatPedCab
          TabOrder = 4
        end
        object DBEdit54: TDBEdit
          Left = 932
          Top = 32
          Width = 56
          Height = 26
          DataField = 'CodUsu'
          DataSource = DsFatPedCab
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
        end
        object DBEdit57: TDBEdit
          Left = 56
          Top = 32
          Width = 93
          Height = 26
          DataField = 'CU_PediVda'
          DataSource = DsFatPedCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
        end
        object DBEdit59: TDBEdit
          Left = 876
          Top = 8
          Width = 112
          Height = 21
          DataField = 'ENCERROU_TXT'
          DataSource = DsFatPedCab
          TabOrder = 7
          OnChange = DBEdit59Change
        end
        object DBEdit58: TDBEdit
          Left = 704
          Top = 8
          Width = 112
          Height = 21
          DataField = 'Abertura'
          DataSource = DsFatPedCab
          TabOrder = 8
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 58
        Width = 1006
        Height = 136
        Align = alTop
        TabOrder = 1
        object Label12: TLabel
          Left = 8
          Top = 12
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object DBText2: TDBText
          Left = 832
          Top = 12
          Width = 45
          Height = 17
          DataField = 'NOME_TIPO_DOC'
          DataSource = DsCli
        end
        object Label13: TLabel
          Left = 8
          Top = 84
          Width = 40
          Height = 13
          Caption = 'Entrega:'
        end
        object DBEdit10: TDBEdit
          Left = 56
          Top = 8
          Width = 72
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsCli
          Enabled = False
          TabOrder = 0
        end
        object DBEdit13: TDBEdit
          Left = 132
          Top = 8
          Width = 693
          Height = 21
          DataField = 'NOME_ENT'
          DataSource = DsCli
          Enabled = False
          TabOrder = 1
        end
        object DBMemo3: TDBMemo
          Left = 56
          Top = 32
          Width = 932
          Height = 47
          DataField = 'E_ALL'
          DataSource = DsCli
          Enabled = False
          TabOrder = 2
        end
        object DBEdit14: TDBEdit
          Left = 876
          Top = 8
          Width = 112
          Height = 21
          DataField = 'CNPJ_TXT'
          DataSource = DsCli
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object MeEnderecoEntrega2: TMemo
          Left = 56
          Top = 80
          Width = 932
          Height = 53
          TabStop = False
          ReadOnly = True
          TabOrder = 4
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 194
        Width = 1006
        Height = 34
        Align = alTop
        TabOrder = 2
        object Label14: TLabel
          Left = 8
          Top = 12
          Width = 73
          Height = 13
          Caption = 'Representante:'
        end
        object Label15: TLabel
          Left = 640
          Top = 11
          Width = 117
          Height = 13
          Caption = '% comiss'#227'o faturamento:'
        end
        object Label19: TLabel
          Left = 819
          Top = 11
          Width = 119
          Height = 13
          Caption = '% comiss'#227'o recebimento:'
        end
        object DBEdit15: TDBEdit
          Left = 84
          Top = 8
          Width = 57
          Height = 21
          DataField = 'CODUSU_ACC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit17: TDBEdit
          Left = 144
          Top = 8
          Width = 489
          Height = 21
          DataField = 'NOMEACC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit18: TDBEdit
          Left = 760
          Top = 8
          Width = 48
          Height = 21
          DataField = 'ComisFat'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBEdit19: TDBEdit
          Left = 939
          Top = 8
          Width = 48
          Height = 21
          DataField = 'ComisRec'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 3
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 228
        Width = 1006
        Height = 43
        Align = alTop
        Caption = ' Fiscal:'
        TabOrder = 3
        object Label20: TLabel
          Left = 8
          Top = 20
          Width = 73
          Height = 13
          Caption = 'Movimenta'#231#227'o:'
        end
        object Label21: TLabel
          Left = 640
          Top = 20
          Width = 55
          Height = 13
          Caption = 'Modelo NF:'
        end
        object DBEdit20: TDBEdit
          Left = 84
          Top = 16
          Width = 57
          Height = 21
          DataField = 'CODUSU_FRC'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit21: TDBEdit
          Left = 144
          Top = 16
          Width = 489
          Height = 21
          DataField = 'NOMEFISREGCAD'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit22: TDBEdit
          Left = 700
          Top = 16
          Width = 287
          Height = 21
          DataField = 'NOMEMODELONF'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
      end
      object GroupBox6: TGroupBox
        Left = 0
        Top = 271
        Width = 1006
        Height = 82
        Align = alTop
        TabOrder = 4
        object Label22: TLabel
          Left = 8
          Top = 13
          Width = 45
          Height = 13
          Caption = 'Frete por:'
        end
        object Label23: TLabel
          Left = 8
          Top = 36
          Width = 75
          Height = 13
          Caption = 'Transportadora:'
        end
        object Label24: TLabel
          Left = 8
          Top = 61
          Width = 64
          Height = 13
          Caption = 'Redespacho:'
        end
        object Label25: TLabel
          Left = 268
          Top = 12
          Width = 98
          Height = 13
          Caption = '% Desp. acess'#243'rias: '
          Visible = False
        end
        object Label26: TLabel
          Left = 404
          Top = 12
          Width = 96
          Height = 13
          Caption = '$ Desp. acess'#243'rias: '
          Visible = False
        end
        object Label27: TLabel
          Left = 572
          Top = 12
          Width = 38
          Height = 13
          Caption = '% Frete:'
          Visible = False
        end
        object Label28: TLabel
          Left = 652
          Top = 12
          Width = 36
          Height = 13
          Caption = '$ Frete:'
          Visible = False
        end
        object Label29: TLabel
          Left = 772
          Top = 12
          Width = 48
          Height = 13
          Caption = '% Seguro:'
          Visible = False
        end
        object Label30: TLabel
          Left = 864
          Top = 12
          Width = 46
          Height = 13
          Caption = '$ Seguro:'
          Visible = False
        end
        object DBEdit24: TDBEdit
          Left = 56
          Top = 8
          Width = 21
          Height = 21
          DataField = 'FretePor'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 0
        end
        object DBEdit25: TDBEdit
          Left = 76
          Top = 8
          Width = 188
          Height = 21
          DataField = 'NOMEFRETEPOR'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 1
        end
        object DBEdit26: TDBEdit
          Left = 88
          Top = 32
          Width = 56
          Height = 21
          DataField = 'CODUSU_TRA'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 2
        end
        object DBEdit27: TDBEdit
          Left = 88
          Top = 56
          Width = 56
          Height = 21
          DataField = 'CODUSU_RED'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 3
        end
        object DBEdit28: TDBEdit
          Left = 148
          Top = 32
          Width = 839
          Height = 21
          DataField = 'NOMETRANSP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 4
        end
        object DBEdit29: TDBEdit
          Left = 148
          Top = 56
          Width = 839
          Height = 21
          DataField = 'NOMEREDESP'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 5
        end
        object DBEdit30: TDBEdit
          Left = 504
          Top = 8
          Width = 64
          Height = 21
          DataField = 'DesoAces_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 6
          Visible = False
        end
        object DBEdit31: TDBEdit
          Left = 364
          Top = 8
          Width = 36
          Height = 21
          DataField = 'DesoAces_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 7
          Visible = False
        end
        object DBEdit32: TDBEdit
          Left = 696
          Top = 8
          Width = 72
          Height = 21
          DataField = 'Frete_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 8
          Visible = False
        end
        object DBEdit33: TDBEdit
          Left = 612
          Top = 8
          Width = 36
          Height = 21
          DataField = 'Frete_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 9
          Visible = False
        end
        object DBEdit51: TDBEdit
          Left = 916
          Top = 8
          Width = 71
          Height = 21
          DataField = 'Seguro_V'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 10
          Visible = False
        end
        object DBEdit52: TDBEdit
          Left = 824
          Top = 8
          Width = 36
          Height = 21
          DataField = 'Seguro_P'
          DataSource = DsPediVda
          Enabled = False
          TabOrder = 11
          Visible = False
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 1
      Top = 475
      Width = 1006
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 482
        Top = 15
        Width = 522
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 390
          Top = 0
          Width = 132
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtFatura: TBitBtn
          Tag = 10102
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fatura'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtFaturaClick
        end
        object BtVolume: TBitBtn
          Tag = 10103
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Volume'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtVolumeClick
        end
        object BtItens: TBitBtn
          Tag = 30
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtItensClick
        end
        object BtEncerra: TBitBtn
          Tag = 506
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Encerrar'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtEncerraClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 290
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 45
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 85
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 125
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 165
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtFisRegCad: TBitBtn
        Tag = 10104
        Left = 205
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtFisRegCadClick
      end
      object BtGraGruN: TBitBtn
        Tag = 30
        Left = 245
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtGraGruNClick
      end
    end
    object GB_M: TGroupBox
      Left = 290
      Top = 0
      Width = 670
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 283
        Height = 32
        Caption = 'Faturamento de Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 283
        Height = 32
        Caption = 'Faturamento de Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 283
        Height = 32
        Caption = 'Faturamento de Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtFatura
    CanUpd01 = BtItens
    Left = 432
    Top = 156
  end
  object PMFatura: TPopupMenu
    OnPopup = PMFaturaPopup
    Left = 536
    Top = 36
    object Incluinovofaturamento1: TMenuItem
      Caption = '&Inclui novo faturamento'
      OnClick = Incluinovofaturamento1Click
    end
    object Alterafaturamentoatual1: TMenuItem
      Caption = '&Altera faturamento atual'
      OnClick = Alterafaturamentoatual1Click
    end
    object Excluifaturamentoatual1: TMenuItem
      Caption = 'E&xclui faturamento atual'
      Enabled = False
      OnClick = Excluifaturamentoatual1Click
    end
  end
  object QrFatPedCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFatPedCabBeforeOpen
    AfterOpen = QrFatPedCabAfterOpen
    BeforeClose = QrFatPedCabBeforeClose
    AfterScroll = QrFatPedCabAfterScroll
    OnCalcFields = QrFatPedCabCalcFields
    SQL.Strings = (
      'SELECT pvd.CodUsu CU_PediVda, pvd.Empresa, '
      'pvd.TabelaPrc, pvd.CondicaoPG, pvd.RegrFiscal, '
      'pvd.AFP_Sit, pvd.AFP_Per, pvd.Cliente,'
      'pvd.PedidoCli, ppc.MedDDReal, ppc.MedDDSimpl, '
      'par.TipMediaDD, par.Associada, par.FatSemEstq, '
      ''
      'par.CtaProdVen EMP_CtaProdVen, '
      'par.FaturaSeq EMP_FaturaSeq,'
      'par.FaturaSep EMP_FaturaSep,'
      'par.FaturaDta EMP_FaturaDta,'
      'par.TxtProdVen EMP_TxtProdVen,'
      'emp.Filial EMP_FILIAL,'
      ''
      'ass.CtaProdVen ASS_CtaProdVen, '
      'ass.FaturaSeq ASS_FaturaSeq,'
      'ass.FaturaSep ASS_FaturaSep,'
      'ass.FaturaDta ASS_FaturaDta,'
      'ass.TxtProdVen ASS_TxtProdVen,'
      'uf.Nome ASS_NO_UF, '
      'uf.Codigo ASS_CO_UF,'
      'ase.Filial ASS_FILIAL,'
      ''
      'tpc.Nome NO_TabelaPrc, fpc.*'
      'FROM fatpedcab fpc '
      'LEFT JOIN pedivda    pvd ON pvd.Codigo=fpc.Pedido '
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc'
      'LEFT JOIN paramsemp  par ON par.Codigo=pvd.Empresa'
      'LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada'
      'LEFT JOIN entidades ase ON ase.Codigo=ass.Codigo'
      'LEFT JOIN entidades emp ON emp.Codigo=par.Codigo'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(ase.Tipo=0, ase.EUF, ase.PUF)'
      'WHERE fpc.Codigo > -1000'
      '')
    Left = 100
    Top = 84
    object QrFatPedCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'pedivda.Empresa'
    end
    object QrFatPedCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fatpedcab.Codigo'
      Required = True
    end
    object QrFatPedCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'fatpedcab.CodUsu'
      Required = True
    end
    object QrFatPedCabPedido: TIntegerField
      FieldName = 'Pedido'
      Origin = 'fatpedcab.Pedido'
      Required = True
    end
    object QrFatPedCabCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
      Origin = 'pedivda.CondicaoPG'
    end
    object QrFatPedCabTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Origin = 'pedivda.TabelaPrc'
    end
    object QrFatPedCabMedDDReal: TFloatField
      FieldName = 'MedDDReal'
      Origin = 'pediprzcab.MedDDReal'
    end
    object QrFatPedCabMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
      Origin = 'pediprzcab.MedDDSimpl'
    end
    object QrFatPedCabRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Origin = 'pedivda.RegrFiscal'
    end
    object QrFatPedCabNO_TabelaPrc: TWideStringField
      FieldName = 'NO_TabelaPrc'
      Origin = 'tabeprccab.Nome'
      Required = True
      Size = 50
    end
    object QrFatPedCabTipMediaDD: TSmallintField
      FieldName = 'TipMediaDD'
      Origin = 'paramsemp.TipMediaDD'
      Required = True
    end
    object QrFatPedCabAFP_Sit: TSmallintField
      FieldName = 'AFP_Sit'
      Origin = 'pedivda.AFP_Sit'
      Required = True
    end
    object QrFatPedCabAFP_Per: TFloatField
      FieldName = 'AFP_Per'
      Origin = 'pedivda.AFP_Per'
      Required = True
    end
    object QrFatPedCabAssociada: TIntegerField
      FieldName = 'Associada'
      Origin = 'paramsemp.Associada'
      Required = True
    end
    object QrFatPedCabCU_PediVda: TIntegerField
      FieldName = 'CU_PediVda'
      Origin = 'pedivda.CodUsu'
      Required = True
    end
    object QrFatPedCabAbertura: TDateTimeField
      FieldName = 'Abertura'
      Origin = 'fatpedcab.Abertura'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFatPedCabEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Origin = 'fatpedcab.Encerrou'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFatPedCabFatSemEstq: TSmallintField
      FieldName = 'FatSemEstq'
      Origin = 'paramsemp.FatSemEstq'
      Required = True
    end
    object QrFatPedCabENCERROU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENCERROU_TXT'
      Size = 30
      Calculated = True
    end
    object QrFatPedCabEMP_CtaProdVen: TIntegerField
      FieldName = 'EMP_CtaProdVen'
      Origin = 'paramsemp.CtaProdVen'
    end
    object QrFatPedCabEMP_FaturaSeq: TSmallintField
      FieldName = 'EMP_FaturaSeq'
      Origin = 'paramsemp.FaturaSeq'
    end
    object QrFatPedCabEMP_FaturaSep: TWideStringField
      FieldName = 'EMP_FaturaSep'
      Origin = 'paramsemp.FaturaSep'
      Size = 1
    end
    object QrFatPedCabEMP_FaturaDta: TSmallintField
      FieldName = 'EMP_FaturaDta'
      Origin = 'paramsemp.FaturaDta'
    end
    object QrFatPedCabEMP_TxtProdVen: TWideStringField
      FieldName = 'EMP_TxtProdVen'
      Origin = 'paramsemp.TxtProdVen'
      Size = 100
    end
    object QrFatPedCabEMP_FILIAL: TIntegerField
      FieldName = 'EMP_FILIAL'
      Origin = 'entidades.Filial'
    end
    object QrFatPedCabASS_CtaProdVen: TIntegerField
      FieldName = 'ASS_CtaProdVen'
      Origin = 'paramsemp.CtaProdVen'
    end
    object QrFatPedCabASS_FaturaSeq: TSmallintField
      FieldName = 'ASS_FaturaSeq'
      Origin = 'paramsemp.FaturaSeq'
    end
    object QrFatPedCabASS_FaturaSep: TWideStringField
      FieldName = 'ASS_FaturaSep'
      Origin = 'paramsemp.FaturaSep'
      Size = 1
    end
    object QrFatPedCabASS_FaturaDta: TSmallintField
      FieldName = 'ASS_FaturaDta'
      Origin = 'paramsemp.FaturaDta'
    end
    object QrFatPedCabASS_TxtProdVen: TWideStringField
      FieldName = 'ASS_TxtProdVen'
      Origin = 'paramsemp.TxtProdVen'
      Size = 100
    end
    object QrFatPedCabASS_NO_UF: TWideStringField
      FieldName = 'ASS_NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrFatPedCabASS_CO_UF: TIntegerField
      FieldName = 'ASS_CO_UF'
      Origin = 'ufs.Codigo'
      Required = True
    end
    object QrFatPedCabASS_FILIAL: TIntegerField
      FieldName = 'ASS_FILIAL'
      Origin = 'entidades.Filial'
    end
    object QrFatPedCabCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'pedivda.Cliente'
    end
    object QrFatPedCabSerieDesfe: TIntegerField
      FieldName = 'SerieDesfe'
    end
    object QrFatPedCabNFDesfeita: TIntegerField
      FieldName = 'NFDesfeita'
    end
    object QrFatPedCabPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
    end
  end
  object DsFatPedCab: TDataSource
    DataSet = QrFatPedCab
    Left = 128
    Top = 84
  end
  object QrFatPedIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,'
      'gg1.Nivel1 CO_NIVEL1, gti.Nome NO_TAM, ggx.GraGru1, '
      'ELT(smi.Baixa+2,"Subtrai","Nulo","Adiciona") CALC_TXT,'
      '- smi.Qtde QTDE_POSITIVO, smi.*'
      'FROM stqmovitsa smi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'WHERE smi.Tipo=1'
      'AND smi.OriCodi=:P0'
      'AND smi.OriCnta=:P1'
      ''
      'UNION'
      ''
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,'
      'gg1.Nivel1 CO_NIVEL1, gti.Nome NO_TAM, ggx.GraGru1, '
      'ELT(smi.Baixa+2,"Subtrai","Nulo","Adiciona") CALC_TXT,'
      '- smi.Qtde QTDE_POSITIVO, smi.*'
      'FROM stqmovitsb smi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'WHERE smi.Tipo=1'
      'AND smi.OriCodi=:P0'
      'AND smi.OriCnta=:P1'
      '')
    Left = 372
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFatPedItsCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
      Origin = 'CU_NIVEL1'
    end
    object QrFatPedItsNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Origin = 'NO_NIVEL1'
      Size = 30
    end
    object QrFatPedItsGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'GraCorCad'
    end
    object QrFatPedItsCU_COR: TIntegerField
      FieldName = 'CU_COR'
      Origin = 'CU_COR'
    end
    object QrFatPedItsNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Origin = 'NO_COR'
      Size = 30
    end
    object QrFatPedItsNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Origin = 'NO_TAM'
      Size = 5
    end
    object QrFatPedItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'GraGru1'
    end
    object QrFatPedItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'DataHora'
      Required = True
    end
    object QrFatPedItsTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'Tipo'
      Required = True
    end
    object QrFatPedItsOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'OriCodi'
      Required = True
    end
    object QrFatPedItsOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
      Origin = 'OriCtrl'
      Required = True
    end
    object QrFatPedItsOriCnta: TIntegerField
      FieldName = 'OriCnta'
      Origin = 'OriCnta'
      Required = True
    end
    object QrFatPedItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'Empresa'
      Required = True
    end
    object QrFatPedItsStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = 'StqCenCad'
      Required = True
    end
    object QrFatPedItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'GraGruX'
      Required = True
    end
    object QrFatPedItsQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'Qtde'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedItsQTDE_POSITIVO: TFloatField
      FieldName = 'QTDE_POSITIVO'
      Required = True
    end
    object QrFatPedItsIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object QrFatPedItsCO_NIVEL1: TIntegerField
      FieldName = 'CO_NIVEL1'
    end
    object QrFatPedItsCALC_TXT: TWideStringField
      FieldName = 'CALC_TXT'
      Size = 8
    end
    object QrFatPedItsOriPart: TIntegerField
      FieldName = 'OriPart'
    end
  end
  object DsFatPedIts: TDataSource
    DataSet = QrFatPedIts
    Left = 400
    Top = 156
  end
  object DsEntrega: TDataSource
    DataSet = QrEntrega
    Left = 740
    Top = 8
  end
  object QrEntrega: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntregaCalcFields
    SQL.Strings = (
      'SELECT'
      'en.LRua     RUA,'
      'en.LNumero  NUMERO,'
      'en.LCompl   COMPL,'
      'en.LBairro  BAIRRO,'
      'en.LCidade  CIDADE,'
      'lll.Nome    NOMELOGRAD,'
      'ufl.Nome    NOMEUF,'
      'en.LPais    Pais,'
      'en.LLograd  Lograd,'
      'en.LCEP     CEP,'
      'en.LEndeRef ENDEREF,'
      'en.LTel     TE1,'
      'en.LFax     FAX'
      'FROM entidades en'
      'LEFT JOIN ufs ufl ON ufl.Codigo=en.LUF'
      'LEFT JOIN listalograd lll ON lll.Codigo=en.LLograd'
      'WHERE en.Codigo=:P0'
      ''
      '')
    Left = 712
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntregaE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrEntregaCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrEntregaTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEntregaRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEntregaNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QrEntregaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEntregaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEntregaCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEntregaNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEntregaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrEntregaPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrEntregaLograd: TSmallintField
      FieldName = 'Lograd'
      Required = True
    end
    object QrEntregaCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrEntregaENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrEntregaTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntregaFAX: TWideStringField
      FieldName = 'FAX'
    end
  end
  object DsCli: TDataSource
    DataSet = QrCli
    Left = 652
    Top = 8
  end
  object QrCli: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCliAfterOpen
    BeforeClose = QrCliBeforeClose
    OnCalcFields = QrCliCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu, IE, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END + 0' +
        '.000 NUMERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL, '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE, '
      
        'CASE WHEN en.Tipo=0 THEN en.EUF     ELSE en.PUF  END + 0.000 UF,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's, '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END + 0' +
        '.000 Lograd, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END + 0' +
        '.000 CEP, '
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ' '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 624
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrCliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCliCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrCliNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCliIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrCliCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrCliPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrCliFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrCliIE: TWideStringField
      FieldName = 'IE'
    end
    object QrCliUF: TFloatField
      FieldName = 'UF'
    end
    object QrCliLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrCliCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrCliNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
  end
  object DsPediVda: TDataSource
    DataSet = QrPediVda
    Left = 592
    Top = 8
  end
  object QrPediVda: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPediVdaAfterOpen
    BeforeClose = QrPediVdaBeforeClose
    OnCalcFields = QrPediVdaCalcFields
    SQL.Strings = (
      'SELECT pvd.Codigo, pvd.CodUsu, pvd.Empresa, pvd.Cliente,'
      'pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi,'
      'pvd.Prioridade, pvd.CondicaoPG, pvd.Moeda,'
      'pvd.Situacao, pvd.TabelaPrc, pvd.MotivoSit, pvd.LoteProd,'
      'pvd.PedidoCli, pvd.FretePor, pvd.Transporta, pvd.Redespacho,'
      'pvd.RegrFiscal, pvd.DesoAces_V, pvd.DesoAces_P,'
      'pvd.Frete_V, pvd.Frete_P, pvd.Seguro_V, pvd.Seguro_P,'
      'pvd.TotalQtd, pvd.Total_Vlr, pvd.Total_Des, pvd.Total_Tot,'
      'pvd.Observa, tpc.Nome NOMETABEPRCCAD,mda.Nome NOMEMOEDA,'
      'pvd.Represen, pvd.ComisFat, pvd.ComisRec, pvd.CartEmis,'
      'pvd.AFP_Sit, pvd.AFP_Per, ppc.MedDDSimpl, MedDDReal,'
      'pvd.ValLiq, pvd.QuantP, frc.Nome NOMEFISREGCAD,'
      'pvd.EntregaUsa, pvd.EntregaEnti,'
      'imp.Nome NOMEMODELONF,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      'IF(emp.Tipo=0, emp.EUF, emp.PUF) + 0.000  EMP_UF,'
      'IF(ven.Tipo=0, ven.RazaoSocial, ven.NOME) NOMEACC,'
      'IF(tra.Tipo=0, tra.RazaoSocial, tra.NOME) NOMETRANSP,'
      'IF(red.Tipo=0, red.RazaoSocial, red.NOME) NOMEREDESP,'
      'emp.Filial, car.Nome NOMECARTEMIS, car.Tipo TIPOCART,'
      'ppc.Nome NOMECONDICAOPG, mot.Nome NOMEMOTIVO,'
      'ven.CodUsu CODUSU_ACC, tra.CodUsu CODUSU_TRA,'
      'red.CodUsu CODUSU_RED, mot.CodUsu CODUSU_MOT,'
      'tpc.CodUsu CODUSU_TPC, mda.CodUsu CODUSU_MDA,'
      'ppc.CodUsu CODUSU_PPC,'
      'frc.CodUsu CODUSU_FRC, imp.Codigo MODELO_NF,'
      'cli.L_Ativo'
      'FROM pedivda pvd'
      'LEFT JOIN entidades  emp ON emp.Codigo=pvd.Empresa'
      'LEFT JOIN entidades  tra ON tra.Codigo=pvd.Transporta'
      'LEFT JOIN entidades  red ON red.Codigo=pvd.Redespacho'
      'LEFT JOIN entidades  cli ON cli.Codigo=pvd.Cliente'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc'
      'LEFT JOIN cambiomda  mda ON mda.Codigo=pvd.Moeda'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG'
      'LEFT JOIN motivos    mot ON mot.Codigo=pvd.MotivoSit'
      'LEFT JOIN pediacc    acc ON acc.Codigo=pvd.Represen'
      'LEFT JOIN entidades  ven ON ven.Codigo=acc.Codigo'
      'LEFT JOIN carteiras  car ON car.Codigo=pvd.CartEmis'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      'LEFT JOIN imprime    imp ON imp.Codigo=frc.ModeloNF'
      'WHERE pvd.CodUsu=:P0')
    Left = 564
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaNOMEFRETEPOR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEFRETEPOR'
      Size = 50
      Calculated = True
    end
    object QrPediVdaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediVdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrPediVdaEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPediVdaCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPediVdaDtaEmiss: TDateField
      FieldName = 'DtaEmiss'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPediVdaDtaEntra: TDateField
      FieldName = 'DtaEntra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPediVdaDtaInclu: TDateField
      FieldName = 'DtaInclu'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPediVdaDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPediVdaPrioridade: TSmallintField
      FieldName = 'Prioridade'
      Required = True
    end
    object QrPediVdaCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
      Required = True
    end
    object QrPediVdaMoeda: TIntegerField
      FieldName = 'Moeda'
      Required = True
    end
    object QrPediVdaSituacao: TIntegerField
      FieldName = 'Situacao'
      Required = True
    end
    object QrPediVdaTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Required = True
    end
    object QrPediVdaMotivoSit: TIntegerField
      FieldName = 'MotivoSit'
      Required = True
    end
    object QrPediVdaLoteProd: TIntegerField
      FieldName = 'LoteProd'
      Required = True
    end
    object QrPediVdaPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
    end
    object QrPediVdaFretePor: TSmallintField
      FieldName = 'FretePor'
      Required = True
    end
    object QrPediVdaTransporta: TIntegerField
      FieldName = 'Transporta'
      Required = True
    end
    object QrPediVdaRedespacho: TIntegerField
      FieldName = 'Redespacho'
      Required = True
    end
    object QrPediVdaRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Required = True
    end
    object QrPediVdaDesoAces_V: TFloatField
      FieldName = 'DesoAces_V'
      Required = True
    end
    object QrPediVdaDesoAces_P: TFloatField
      FieldName = 'DesoAces_P'
      Required = True
    end
    object QrPediVdaFrete_V: TFloatField
      FieldName = 'Frete_V'
      Required = True
    end
    object QrPediVdaFrete_P: TFloatField
      FieldName = 'Frete_P'
      Required = True
    end
    object QrPediVdaSeguro_V: TFloatField
      FieldName = 'Seguro_V'
      Required = True
    end
    object QrPediVdaSeguro_P: TFloatField
      FieldName = 'Seguro_P'
      Required = True
    end
    object QrPediVdaTotalQtd: TFloatField
      FieldName = 'TotalQtd'
      Required = True
    end
    object QrPediVdaTotal_Vlr: TFloatField
      FieldName = 'Total_Vlr'
      Required = True
    end
    object QrPediVdaTotal_Des: TFloatField
      FieldName = 'Total_Des'
      Required = True
    end
    object QrPediVdaTotal_Tot: TFloatField
      FieldName = 'Total_Tot'
      Required = True
    end
    object QrPediVdaObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPediVdaRepresen: TIntegerField
      FieldName = 'Represen'
      Required = True
    end
    object QrPediVdaComisFat: TFloatField
      FieldName = 'ComisFat'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaComisRec: TFloatField
      FieldName = 'ComisRec'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaCartEmis: TIntegerField
      FieldName = 'CartEmis'
      Required = True
    end
    object QrPediVdaAFP_Sit: TSmallintField
      FieldName = 'AFP_Sit'
      Required = True
    end
    object QrPediVdaAFP_Per: TFloatField
      FieldName = 'AFP_Per'
      Required = True
    end
    object QrPediVdaMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
    end
    object QrPediVdaMedDDReal: TFloatField
      FieldName = 'MedDDReal'
    end
    object QrPediVdaValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrPediVdaQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrPediVdaFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrPediVdaNOMETABEPRCCAD: TWideStringField
      FieldName = 'NOMETABEPRCCAD'
      Size = 50
    end
    object QrPediVdaNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 30
    end
    object QrPediVdaNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrPediVdaNOMEACC: TWideStringField
      FieldName = 'NOMEACC'
      Size = 100
    end
    object QrPediVdaNOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrPediVdaNOMEREDESP: TWideStringField
      FieldName = 'NOMEREDESP'
      Size = 100
    end
    object QrPediVdaNOMEFISREGCAD: TWideStringField
      FieldName = 'NOMEFISREGCAD'
      Size = 50
    end
    object QrPediVdaNOMEMODELONF: TWideStringField
      FieldName = 'NOMEMODELONF'
      Size = 100
    end
    object QrPediVdaNOMECARTEMIS: TWideStringField
      FieldName = 'NOMECARTEMIS'
      Size = 100
    end
    object QrPediVdaNOMECONDICAOPG: TWideStringField
      FieldName = 'NOMECONDICAOPG'
      Size = 50
    end
    object QrPediVdaNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Size = 50
    end
    object QrPediVdaCODUSU_ACC: TIntegerField
      FieldName = 'CODUSU_ACC'
      Required = True
    end
    object QrPediVdaCODUSU_TRA: TIntegerField
      FieldName = 'CODUSU_TRA'
      Required = True
    end
    object QrPediVdaCODUSU_RED: TIntegerField
      FieldName = 'CODUSU_RED'
      Required = True
    end
    object QrPediVdaCODUSU_MOT: TIntegerField
      FieldName = 'CODUSU_MOT'
      Required = True
    end
    object QrPediVdaCODUSU_TPC: TIntegerField
      FieldName = 'CODUSU_TPC'
      Required = True
    end
    object QrPediVdaCODUSU_PPC: TIntegerField
      FieldName = 'CODUSU_PPC'
      Required = True
    end
    object QrPediVdaCODUSU_FRC: TIntegerField
      FieldName = 'CODUSU_FRC'
      Required = True
    end
    object QrPediVdaEMP_UF: TFloatField
      FieldName = 'EMP_UF'
    end
    object QrPediVdaCODUSU_MDA: TIntegerField
      FieldName = 'CODUSU_MDA'
    end
    object QrPediVdaTIPOCART: TIntegerField
      FieldName = 'TIPOCART'
    end
    object QrPediVdaMODELO_NF: TIntegerField
      FieldName = 'MODELO_NF'
    end
    object QrPediVdaL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
    end
    object QrPediVdaEntregaEnti: TIntegerField
      FieldName = 'EntregaEnti'
    end
    object QrPediVdaEntregaUsa: TSmallintField
      FieldName = 'EntregaUsa'
    end
  end
  object QrFatPedVol: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFatPedVolAfterOpen
    BeforeClose = QrFatPedVolBeforeClose
    AfterScroll = QrFatPedVolAfterScroll
    SQL.Strings = (
      'SELECT med.Nome NO_UnidMed, fpv.* '
      'FROM fatpedvol fpv'
      'LEFT JOIN unidmed med ON med.Codigo=fpv.UnidMed'
      'WHERE fpv.Codigo=:P0')
    Left = 312
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatPedVolNO_UnidMed: TWideStringField
      FieldName = 'NO_UnidMed'
      Size = 30
    end
    object QrFatPedVolCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFatPedVolCnta: TIntegerField
      FieldName = 'Cnta'
      Required = True
    end
    object QrFatPedVolUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
  end
  object DsFatPedVol: TDataSource
    DataSet = QrFatPedVol
    Left = 340
    Top = 156
  end
  object PMVolume: TPopupMenu
    OnPopup = PMVolumePopup
    Left = 344
    Top = 60
    object Incluinovovolume1: TMenuItem
      Caption = '&Inclui novo volume'
      OnClick = Incluinovovolume1Click
    end
    object Alteravolumeatual1: TMenuItem
      Caption = '&Altera volume atual'
      OnClick = Alteravolumeatual1Click
    end
    object Excluivolumeatual1: TMenuItem
      Caption = '&Exclui volume atual'
      OnClick = Excluivolumeatual1Click
    end
  end
  object PMItens: TPopupMenu
    Left = 372
    Top = 60
    object Incluiitenss1: TMenuItem
      Caption = '&Inclui item(ns)'
      OnClick = Incluiitenss1Click
    end
    object Excluiitemns1: TMenuItem
      Caption = '&Exclui item(ns)'
      OnClick = Excluiitemns1Click
    end
  end
  object QrSCCs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 256
    Top = 60
  end
  object PMImprime: TPopupMenu
    Left = 404
    Top = 60
    object Anlise1: TMenuItem
      Caption = '&An'#225'lise'
      OnClick = Anlise1Click
    end
    object Romaneioporgrupo1: TMenuItem
      Caption = '&Romaneio por grupo'
      OnClick = Romaneioporgrupo1Click
    end
  end
  object QrAnalise: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT smva.Preco, SUM(smva.Total) Total, '
      'smva.GraGruX, SUM(smva.Qtde) Qtde, ent.Filial,'
      'gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,'
      'gti.Nome NO_TAM, ggx.GraGru1 '
      'FROM stqmovvala smva'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      
        'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI LEFT JOIN en' +
        'tidades ent ON ent.Codigo=smva.Empresa      '
      ''
      ''
      'WHERE smva.Tipo=1'
      'AND smva.OriCodi=:P0'
      'GROUP BY GraGruX, SeqInReduz'
      'ORDER BY NO_NIVEL1, NO_COR, NO_TAM, Filial')
    Left = 780
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAnalisePreco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
    object QrAnaliseTotal: TFloatField
      FieldName = 'Total'
    end
    object QrAnaliseFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrAnaliseCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
    end
    object QrAnaliseNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrAnaliseGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrAnaliseCU_COR: TIntegerField
      FieldName = 'CU_COR'
    end
    object QrAnaliseNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrAnaliseNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrAnaliseGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrAnaliseGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrAnaliseQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object frxDsAnalise: TfrxDBDataset
    UserName = 'frxDsAnalise'
    CloseDataSource = False
    DataSet = QrAnalise
    BCDToCurrency = False
    DataSetOptions = []
    Left = 816
    Top = 8
  end
  object frxFAT_PEDID_001_00: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    Left = 656
    Top = 36
    Datasets = <
      item
        DataSet = frxDsAnalise
        DataSetName = 'frxDsAnalise'
      end
      item
        DataSet = frxDsCli
        DataSetName = 'frxDsCli'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDSFatPedCab
        DataSetName = 'frxDSFatPedCab'
      end
      item
        DataSet = frxDsPediVda
        DataSetName = 'frxDsPediVda'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 124.724480240000000000
        Top = 56.692950000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 684.094930000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 684.094930000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 668.976810000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'AN'#193'LISE DO FATURAMENTO N'#186' [frxDSFatPedCab."CodUsu"]  - PEDIDO N'#186 +
              ' [frxDSFatPedCab."CU_PediVda"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 1.779530000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 544.251946540000000000
          Width = 132.283476770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 90.708720000000000000
          Width = 37.795300000000000000
          Height = 30.236230240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Filial')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 90.708720000000000000
          Width = 56.692950000000000000
          Height = 30.236230240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Quantia')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 90.708720000000000000
          Width = 56.692950000000000000
          Height = 30.236230240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pre'#231'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 90.708720000000000000
          Width = 75.590600000000000000
          Height = 30.236230240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 90.708720000000000000
          Width = 120.944960000000000000
          Height = 30.236230240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 45.354360000000000000
          Width = 638.740570000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            
              '[frxDsCli."CodUsu"] - [frxDsCli."NOME_TIPO_DOC"] [frxDsCli."CNPJ' +
              '_TXT"] - [frxDsCli."NOME_ENT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 45.354360000000000000
          Width = 30.236240000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 64.252010000000000000
          Width = 612.283860000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsPediVda."NOMEACC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 64.252010000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Representante:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsAnalise."GraGruX"'
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalise."CU_NIVEL1"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 3.779530000000000000
          Width = 302.362400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAnalise."NO_NIVEL1"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 3.779530000000000000
          Width = 166.299320000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAnalise."NO_COR"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 544.252320000000000000
          Top = 3.779530000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsAnalise."NO_TAM"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Top = 3.779530000000000000
          Width = 79.370130000000000000
          Height = 15.118110240000000000
          DataField = 'GraGruX'
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalise."GraGruX"]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Height = 15.118110240000000000
        ParentFont = False
        Top = 283.464750000000000000
        Width = 680.315400000000000000
        ColumnGap = 37.795275590551200000
        DataSet = frxDsAnalise
        DataSetName = 'frxDsAnalise'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataField = 'Filial'
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAnalise."Filial"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataField = 'Qtde'
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalise."Qtde"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataField = 'Preco'
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalise."Preco"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataField = 'Total'
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalise."Total"]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 120.944960000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalise."Qtde">)/2]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalise."Preco">)]'
            '')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalise."Total">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 120.944960000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 396.850650000000000000
        Width = 680.315400000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 7.559059999999988000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalise."Qtde">)/2]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 7.559059999999988000
          Width = 56.692950000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsAnalise."Qtde">)=0, 0, SUM(<frxDsAnalise."Total">' +
              ') / SUM(<frxDsAnalise."Qtde">) * 2)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 7.559059999999988000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalise."Total">)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559059999999988000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 7.559060000000000000
          Width = 120.944960000000000000
          Height = 15.118110240000000000
          DataSet = frxDsAnalise
          DataSetName = 'frxDsAnalise'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 306.141930000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDSFatPedCab: TfrxDBDataset
    UserName = 'frxDSFatPedCab'
    CloseDataSource = False
    DataSet = QrFatPedCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 100
    Top = 112
  end
  object frxDsPediVda: TfrxDBDataset
    UserName = 'frxDsPediVda'
    CloseDataSource = False
    DataSet = QrPediVda
    BCDToCurrency = False
    DataSetOptions = []
    Left = 536
    Top = 8
  end
  object frxDsCli: TfrxDBDataset
    UserName = 'frxDsCli'
    CloseDataSource = False
    DataSet = QrCli
    BCDToCurrency = False
    DataSetOptions = []
    Left = 680
    Top = 8
  end
  object PMEncerra: TPopupMenu
    OnPopup = PMEncerraPopup
    Left = 732
    Top = 488
    object Encerrafaturamento1: TMenuItem
      Caption = '&Encerra faturamento'
      OnClick = Encerrafaturamento1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Desfazencerramento1: TMenuItem
      Caption = '&Desfaz encerramento'
      OnClick = Desfazencerramento1Click
    end
  end
  object QrTemItens: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,'
      'gti.Nome NO_TAM, ggx.GraGru1, - smi.Qtde QTDE_POSITIVO, smi.*'
      'FROM stqmovitsa smi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'WHERE smi.Tipo=1'
      'AND smi.OriCodi=:P0'
      ''
      'UNION'
      ''
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,'
      'gti.Nome NO_TAM, ggx.GraGru1, - smi.Qtde QTDE_POSITIVO, smi.*'
      'FROM stqmovitsb smi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'WHERE smi.Tipo=1'
      'AND smi.OriCodi=:P0')
    Left = 764
    Top = 488
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrEntregaEnti: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntregaEntiCalcFields
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, R' +
        'espons2, '
      'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, '
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, '
      'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, '
      'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, '
      'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, '
      'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, '
      'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, '
      'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, '
      'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, '
      'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, '
      'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, '
      'mun.Nome CIDADE, '
      'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, '
      'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, '
      'pai.Nome Pais, '
      'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, '
      'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, '
      'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, '
      'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, '
      'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, '
      'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, '
      'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, '
      'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, '
      'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, '
      'RG, SSP, DataRG '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF '
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF '
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd '
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd '
      
        'LEFT JOIN locbdermall.dtb_munici mun ON mun.Codigo = IF(en.Tipo=' +
        '0, en.ECodMunici, en.PCodMunici) '
      
        'LEFT JOIN locbdermall.bacen_pais pai ON pai.Codigo = IF(en.Tipo=' +
        '0, en.ECodiPais, en.PCodiPais) '
      'ORDER BY NOME_ENT')
    Left = 612
    Top = 176
    object QrEntregaEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntregaEntiCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEntregaEntiENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEntregaEntiPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrEntregaEntiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntregaEntiRespons1: TWideStringField
      FieldName = 'Respons1'
      Size = 100
    end
    object QrEntregaEntiRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 100
    end
    object QrEntregaEntiENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEntregaEntiPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrEntregaEntiELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrEntregaEntiPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrEntregaEntiECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEntregaEntiPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrEntregaEntiNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrEntregaEntiNO_2_ENT: TWideStringField
      FieldName = 'NO_2_ENT'
      Size = 60
    end
    object QrEntregaEntiCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntregaEntiIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEntregaEntiNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 15
    end
    object QrEntregaEntiRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrEntregaEntiSITE: TWideStringField
      FieldName = 'SITE'
      Size = 100
    end
    object QrEntregaEntiNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QrEntregaEntiCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrEntregaEntiBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrEntregaEntiCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 100
    end
    object QrEntregaEntiNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrEntregaEntiNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEntregaEntiPais: TWideStringField
      FieldName = 'Pais'
      Size = 100
    end
    object QrEntregaEntiLograd: TSmallintField
      FieldName = 'Lograd'
      Required = True
    end
    object QrEntregaEntiCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrEntregaEntiTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntregaEntiFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEntregaEntiEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrEntregaEntiTRATO: TWideStringField
      FieldName = 'TRATO'
      Size = 10
    end
    object QrEntregaEntiENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrEntregaEntiCODMUNICI: TFloatField
      FieldName = 'CODMUNICI'
      Required = True
    end
    object QrEntregaEntiCODPAIS: TFloatField
      FieldName = 'CODPAIS'
      Required = True
    end
    object QrEntregaEntiRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrEntregaEntiSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrEntregaEntiDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrEntregaEntiIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntregaEntiE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 512
      Calculated = True
    end
    object QrEntregaEntiCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaEntiNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrEntregaEntiTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaEntiFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaEntiNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaEntiCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
  end
  object QrEntregaCli: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntregaCliCalcFields
    SQL.Strings = (
      'SELECT en.L_CNPJ, en.L_CPF,'
      'en.L_Nome,'
      'en.LLograd  Lograd,'
      'lll.Nome    NOMELOGRAD,'
      'en.LRua     RUA,'
      'en.LNumero  NUMERO,'
      'en.LCompl   COMPL,'
      'en.LBairro  BAIRRO,'
      'en.LCodMunici,'
      'mun.Nome NO_MUNICI, '
      'ufl.Nome    NOMEUF,'
      'en.LCEP     CEP,'
      'en.LCodiPais    CodiPais,'
      'pai.Nome NO_PAIS, '
      'en.LEndeRef ENDEREF,'
      'en.LTel     TE1,'
      'en.LEmail     Email,'
      'en.L_IE'
      'FROM entidades en'
      'LEFT JOIN ufs ufl ON ufl.Codigo=en.LUF'
      'LEFT JOIN listalograd lll ON lll.Codigo=en.LLograd'
      
        'LEFT JOIN locbdermall.dtb_munici mun ON mun.Codigo = IF(en.Tipo=' +
        '0, en.ECodMunici, en.PCodMunici) '
      
        'LEFT JOIN locbdermall.bacen_pais pai ON pai.Codigo = IF(en.Tipo=' +
        '0, en.ECodiPais, en.PCodiPais) '
      'WHERE en.Codigo=:P0')
    Left = 700
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntregaCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrEntregaCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrEntregaCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEntregaCliL_CNPJ: TWideStringField
      FieldName = 'L_CNPJ'
      Size = 14
    end
    object QrEntregaCliL_CPF: TWideStringField
      FieldName = 'L_CPF'
      Size = 18
    end
    object QrEntregaCliL_Nome: TWideStringField
      FieldName = 'L_Nome'
      Size = 60
    end
    object QrEntregaCliLograd: TSmallintField
      FieldName = 'Lograd'
    end
    object QrEntregaCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrEntregaCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrEntregaCliNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QrEntregaCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrEntregaCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrEntregaCliLCodMunici: TIntegerField
      FieldName = 'LCodMunici'
    end
    object QrEntregaCliNO_MUNICI: TWideStringField
      FieldName = 'NO_MUNICI'
      Size = 100
    end
    object QrEntregaCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrEntregaCliCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrEntregaCliCodiPais: TIntegerField
      FieldName = 'CodiPais'
    end
    object QrEntregaCliNO_PAIS: TWideStringField
      FieldName = 'NO_PAIS'
      Size = 100
    end
    object QrEntregaCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrEntregaCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntregaCliEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrEntregaCliL_IE: TWideStringField
      FieldName = 'L_IE'
    end
  end
end
