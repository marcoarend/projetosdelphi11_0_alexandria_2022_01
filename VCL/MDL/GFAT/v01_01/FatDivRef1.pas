unit FatDivRef1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, Mask, dmkDBGridDAC, dmkDBEdit, ComCtrls, UnInternalConsts,
  dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmFatDivRef1 = class(TForm)
    Panel1: TPanel;
    DsFatPedCab: TDataSource;
    DsPediVda: TDataSource;
    DsCli: TDataSource;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    DBEdit5: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit53: TDBEdit;
    DBEdit54: TDBEdit;
    DBEdit57: TDBEdit;
    DBEdit59: TDBEdit;
    DBEdit58: TDBEdit;
    GroupBox3: TGroupBox;
    Label12: TLabel;
    DBText2: TDBText;
    DBEdit10: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GroupBox4: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label19: TLabel;
    DBEdit15: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    GroupBox5: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    GroupBox6: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label22: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit51: TDBEdit;
    DBEdit52: TDBEdit;
    DBGFatDivRef: TdmkDBGridDAC;
    TbFatDivRef: TmySQLTable;
    DsFatDivRef: TDataSource;
    TbFatDivRefStqCenCad: TIntegerField;
    TbFatDivRefReferencia: TWideStringField;
    TbFatDivRefGraGruX: TIntegerField;
    TbFatDivRefQtde: TFloatField;
    TbFatDivRefPrecoAut: TFloatField;
    TbFatDivRefPrecoVen: TFloatField;
    TbFatDivRefAtivo: TSmallintField;
    QrLista: TmySQLQuery;
    QrListaGraCusPrc: TIntegerField;
    TbFatDivRefPrecoOri: TFloatField;
    TbFatDivRefPrecoPrz: TFloatField;
    TbFatDivRefValorTotBru: TFloatField;
    TbFatDivRefPerDescoVal: TFloatField;
    TbFatDivRefValorTotLiq: TFloatField;
    TbFatDivRefPerComissF: TFloatField;
    TbFatDivRefValComisLiqF: TFloatField;
    TbFatDivRefPerComissR: TFloatField;
    TbFatDivRefValComisLiqR: TFloatField;
    Panel3: TPanel;
    TbFatDivRefPerComissT: TFloatField;
    TbFatDivRefValComisBruT: TFloatField;
    TbFatDivRefValComisDesT: TFloatField;
    TbFatDivRefValComisLiqT: TFloatField;
    QrLocComiss1: TmySQLQuery;
    QrLocComiss1PerComissF: TFloatField;
    QrLocComiss1PerComissR: TFloatField;
    QrLocComiss1PerComissZ: TSmallintField;
    QrLocComissX: TmySQLQuery;
    QrLocComissXPerComissF: TFloatField;
    QrLocComissXPerComissR: TFloatField;
    GroupBox9: TGroupBox;
    GroupBox1: TGroupBox;
    Label18: TLabel;
    Label31: TLabel;
    DBEdit2: TDBEdit;
    DBEdit6: TDBEdit;
    GroupBox7: TGroupBox;
    Label32: TLabel;
    Label35: TLabel;
    DBEdit11: TDBEdit;
    DBEdit23: TDBEdit;
    GroupBox8: TGroupBox;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    GroupBox10: TGroupBox;
    DsSumRef: TDataSource;
    QrSumRef: TmySQLQuery;
    QrSumRefITENS: TLargeintField;
    QrSumRefQtde: TFloatField;
    QrSumRefValorTotBru: TFloatField;
    QrSumRefValorTotLiq: TFloatField;
    QrSumRefValComisLiqF: TFloatField;
    QrSumRefValComisLiqR: TFloatField;
    QrSumRefValComisBruT: TFloatField;
    QrSumRefValComisDesT: TFloatField;
    QrSumRefValComisLiqT: TFloatField;
    Label29: TLabel;
    DBEdit3: TDBEdit;
    Label30: TLabel;
    DBEdit4: TDBEdit;
    Label33: TLabel;
    DBEdit12: TDBEdit;
    Label34: TLabel;
    DBEdit16: TDBEdit;
    Label42: TLabel;
    DBEdit38: TDBEdit;
    Label43: TLabel;
    DBEdit39: TDBEdit;
    Label44: TLabel;
    DBEdit40: TDBEdit;
    Label45: TLabel;
    DBEdit41: TDBEdit;
    Label46: TLabel;
    DBEdit42: TDBEdit;
    TbFatDivRefCodigo: TIntegerField;
    TbFatDivRefIDCtrl: TIntegerField;
    TbFatDivRefPerComissNiv: TWideStringField;
    DBEdit43: TDBEdit;
    Label13: TLabel;
    QrSumRefPERCOMISSL: TFloatField;
    TbFatDivRefSEQ: TIntegerField;
    TbFatDivRefVolCnta: TIntegerField;
    QrFatPedVol: TmySQLQuery;
    TbFatDivRefVOLUME: TIntegerField;
    QrFatPedVolCnta: TIntegerField;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    TbFatDivRefCENTRO: TIntegerField;
    ST1: TStaticText;
    ST2: TStaticText;
    Panel4: TPanel;
    PB1: TProgressBar;
    TbFatDivRefTipoCalc: TSmallintField;
    QrFatDivRef: TmySQLQuery;
    QrFatDivRefNO_PRD_TAM_COR: TWideStringField;
    QrFatDivRefprod_indTot: TSmallintField;
    QrFatDivRefIPI_Alq: TFloatField;
    QrFatDivRefMadeBy: TSmallintField;
    TbFatDivRefPROD_INDTOT: TSmallintField;
    TbFatDivRefMADEBY: TSmallintField;
    TbFatDivRefIPI_ALQ: TFloatField;
    TbFatDivRefTIPOCALC_TXT: TWideStringField;
    TbFatDivRefNO_GGX: TWideStringField;
    QrFatDivRefGraGruX: TIntegerField;
    QrLocSMIA: TmySQLQuery;
    QrFatDivRefIDCtrl: TIntegerField;
    QrFatDivRefHowBxaEstq: TSmallintField;
    QrFatDivRefGerBxaEstq: TSmallintField;
    QrFatDivRefFracio: TSmallintField;
    TbFatDivRefHOWBXAESTQ: TSmallintField;
    TbFatDivRefPecas: TFloatField;
    TbFatDivRefPeso: TFloatField;
    TbFatDivRefAreaM2: TFloatField;
    TbFatDivRefAreaP2: TFloatField;
    TbFatDivRefGerBxaEstq: TSmallintField;
    DBMemo3: TDBMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    PainelConfirma: TPanel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    BtConverte: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PnJuros: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    EdCustoFin: TdmkEdit;
    EdJurosMes: TdmkEdit;
    Panel8: TPanel;
    EdKey: TdmkEdit;
    EdCampo: TdmkEdit;
    EdValor: TdmkEdit;
    EdConfirmou: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGFatDivRefKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGFatDivRefKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TbFatDivRefBeforePost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure DBGFatDivRefColEnter(Sender: TObject);
    procedure TbFatDivRefBeforeOpen(DataSet: TDataSet);
    procedure TbFatDivRefAfterPost(DataSet: TDataSet);
    procedure TbFatDivRefAfterDelete(DataSet: TDataSet);
    procedure TbFatDivRefCalcFields(DataSet: TDataSet);
    procedure QrSumRefCalcFields(DataSet: TDataSet);
    procedure QrSumRefBeforeClose(DataSet: TDataSet);
    procedure QrSumRefAfterOpen(DataSet: TDataSet);
    procedure BtConverteClick(Sender: TObject);
    procedure TbFatDivRefBeforeDelete(DataSet: TDataSet);
  private
    { Private declarations }
    FOriCtrl: Integer;
    //
    FLocNivel1, FLocNivel2, FLocNivel3, FLocPrdGrupTip, FLocControle: Integer;
    FLocReferencia: String;
    FLocPerComissF, FLocPerComissR: Double;
    function  InsereItem2(): Boolean;
    procedure ObtemPrecos(const Nivel1: Integer; var PrecoO, PrecoR: Double);
    procedure ObtemPercentuaisDeComissoes(const Nivel1, Nivel2, Nivel3,
              PrdGrupTip: Integer; var PerComissF, PerComissR: Double;
              var Nivel: String);
  public
    { Public declarations }
    FMultiGrandeza: Boolean;
    procedure ReopenFatDivRef(nItem: Integer);
    //procedure ReopenFatDivSum();
    procedure ReopenSumRef();
    procedure ReopenLookup();
  end;

  var
  FmFatDivRef1: TFmFatDivRef1;

implementation

uses
{$IfNDef SemNFe_0000} ModuleNFe_0000, {$EndIf}
UnMyObjects, FatDivGer1, Module, ModProd, ModPediVda, UCreate, GraGruPesq1,
MyDBCheck, UMySQLModule, UnGrade_Tabs, ModuleGeral, ModuleFatura;

{$R *.DFM}

procedure TFmFatDivRef1.BtConverteClick(Sender: TObject);
var
  Codigo: Integer;
  Tot, Qtd, PrecoO, PrecoR: Double;
  ErrPreco, AvisoPrc, QtdRed(*, Sim*), ErrCentro: Integer;
  //
  NeedQI, NeedQA, NeedQK: Boolean;
  Pecas, Peso, AreaM2(*, AreaP2*): Double;
  Erro: String;
begin
  Screen.Cursor := crHourGlass;
  TbFatDivRef.DisableControls;
  try
    Codigo := FmFatDivGer1.QrFatPedCabCodigo.Value;
    //Sim       := 0;
    Tot       := 0;
    ErrPreco  := 0;
    ErrCentro := 0;
    AvisoPrc  := 0;
    QtdRed    := 0;
    TbFatDivRef.First;
    while not TbFatDivRef.Eof do
    begin
      if TbFatDivRefGraGruX.Value = 0 then
      begin
        Geral.MB_Aviso('Defina o produto!');
        Exit;
      end;
      Qtd := TbFatDivRefQtde.Value;
      //Sim := Geral.IMV(GradeX.Cells[Col,Row]);
      Tot := Tot + Qtd (*+ Sim*);
      if Qtd > 0 then
        QtdRed := QtdRed + 1;
      PrecoO := TbFatDivRefPrecoOri.Value;
      PrecoR := TbFatDivRefPrecoPrz.Value;
      //
      // 2011-08-19
      if (PrecoR < 0.01) and (TbFatDivRefPrecoAut.Value > 0.01) then
        PrecoR := TbFatDivRefPrecoAut.Value;
      // fim 2011-08-19
      //
      if (PrecoO <>  PrecoR) and (Qtd = 0) then
        AvisoPrc := AvisoPrc + 1;
      if ((Qtd > 0) (*or (Sim > 0)*)) and (PrecoR = 0) then
        ErrPreco := ErrPreco + 1;
      if TbFatDivRefStqCenCad.Value = 0 then
        ErrCentro := ErrCentro + 1;
      //
      if FMultiGrandeza then
      begin
        NeedQI := Geral.IntInConjunto(1, TbFatDivRefHowBxaEstq.Value);
        NeedQA := Geral.IntInConjunto(2, TbFatDivRefHowBxaEstq.Value);
        NeedQK := Geral.IntInConjunto(4, TbFatDivRefHowBxaEstq.Value);
        //
        Pecas  := - TbFatDivRefPecas.Value;
        Peso   := - TbFatDivRefPeso.Value;
        AreaM2 := - TbFatDivRefAreaM2.Value;
        //AreaP2 := - TbFatDivRefAreaP2.Value;
        //
        Erro   := '';
        if (Pecas  = 0) and NeedQI then Erro := 'Pe�as';
        if (AreaM2 = 0) and NeedQA then Erro := '�rea';
        if (Peso   = 0) and NeedQK then Erro := 'Peso';
        if Erro <> '' then
        begin
          Geral.MB_Aviso('Grandeza obrigat�ria: "' + Erro + '" para o item '
          + Geral.FF0(TbFatDivRef.RecNo) + '.');
          Screen.Cursor := crHourGlass;
          TbFatDivRef.EnableControls;
          Exit;
        end;
      end;
      //
      TbFatDivRef.Next;
    end;
    if (Tot = 0) (*and (Sim = 0)*) then
    begin
      Screen.Cursor := crDefault;
      TbFatDivRef.EnableControls;
      Geral.MB_Aviso('N�o h� defini��o de quantidades!');
      Exit;
    end
    else if ErrPreco > 0 then
    begin
      Screen.Cursor := crDefault;
      TbFatDivRef.EnableControls;
      Geral.MB_Aviso('Existem ' + Geral.FF0(ErrPreco) + ' itens sem pre�o de faturamento definido!');
      //
      Exit;
    end else if ErrCentro > 0 then
    begin
      Screen.Cursor := crDefault;
      TbFatDivRef.EnableControls;
      Geral.MB_Aviso('Existem ' + Geral.FF0(ErrCentro) + ' itens sem centro de estoque definido!');
      //
      Exit;
    end
    else if AvisoPrc > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(AvisoPrc) +
        ' itens com pre�o de faturamento alterado, mas n�o h� defini��o de ' +
        'quantidades de itens!' + sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then
      begin
        Screen.Cursor := crDefault;
        TbFatDivRef.EnableControls;
        Exit;
      end;
    end;
    //
    PB1.Position := 0;
    PB1.Max := QtdRed;
    TbFatDivRef.First;
    while not TbFatDivRef.Eof do
    begin
      if not InsereItem2() then
      begin
        TbFatDivRef.EnableControls;
        Screen.Cursor := crDefault;
        Exit;
      end;
      //
      TbFatDivRef.Next;
    end;
    FmFatDivGer1.LocCod(Codigo, Codigo);
    FmFatDivGer1.ReopenFatPedIts(0);
    //
    Close;
  except
    TbFatDivRef.EnableControls;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFatDivRef1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatDivRef1.DBGFatDivRefColEnter(Sender: TObject);
begin
  EdCampo.Text := DBGFatDivRef.Columns[THackDBGrid(DBGFatDivRef).Col -1].FieldName;
end;

procedure TFmFatDivRef1.DBGFatDivRefKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Campo, Valor, Nivel: String;
  PerComissF,PerComissR, Flut: Double;
  (*Itens,*) GraGruX: Integer;
  //
  procedure SetaReduzido();
  var
    PrecoO, PrecoR: Double;
  begin
    ObtemPrecos(FLocNivel1, PrecoO, PrecoR);
    ObtemPercentuaisDeComissoes(FLocNivel1, FLocNivel2, FLocNivel3,
      FLocPrdGrupTip, PerComissF, PerComissR, Nivel);
    if PrecoR < 0.000001 then
      Geral.MB_Aviso('Pre�o n�o definido para o reduzido ' +
        FormatFloat('0', FLocControle) + '!');
    //
    TbFatDivRef.Edit;
    TbFatDivRefReferencia.Value   := FLocReferencia;
    TbFatDivRefGraGruX.Value      := FLocControle;
    //TbFatDivRefNO_GGX.Value       := QrLocRefNO_PRD_TAM_COR.Value;
    if (FmFatDivGer1.QrPediVdaComisFat.Value <> 0)
    or (FmFatDivGer1.QrPediVdaComisRec.Value <> 0) then
    begin
      TbFatDivRefPerComissF.Value   := FmFatDivGer1.QrPediVdaComisFat.Value;
      TbFatDivRefPerComissR.Value   := FmFatDivGer1.QrPediVdaComisRec.Value;
      TbFatDivRefPerComissNiv.Value := '';
    end else begin
      TbFatDivRefPerComissF.Value   := FLocPerComissF;
      TbFatDivRefPerComissR.Value   := FLocPerComissR;
      TbFatDivRefPerComissNiv.Value := Nivel;
    end;
    //TbFatDivRefNivel.Value        := Nivel;
    TbFatDivRefPrecoOri.Value     := PrecoO;
    TbFatDivRefPrecoPrz.Value     := PrecoR;
    TbFatDivRefPerDescoVal.Value  := 0;
    TbFatDivRefPrecoAut.Value     := PrecoR;
    TbFatDivRefPrecoVen.Value     := PrecoR;
    TbFatDivRef.Post;
    TbFatDivRef.Refresh;
  end;
  procedure SelecionaReduzido();
  begin
    // Parei Aqui!
  end;
  function Confirmou(): Boolean;
  begin
    Result := (Key in ([13(*,37*),38(*,39*),40])) and (Valor <> '')
              and DBGFatDivRef.EditorMode;
    if Result then
      EdConfirmou.Text := 'SIM'
    else
      EdConfirmou.Text := 'N�O'
  end;
  function ObtemGGXPesquisa(): Integer;
  //var
    //PGT: String;
  begin
    Result := 0;
    if DBCheck.CriaFm(TFmGraGruPesq1, FmGraGruPesq1, afmoNegarComAviso) then
    begin
      //PGT := FormatFloat('0', FmStqInnCad.QrStqInnCadPrdGrupTip.Value);
      FmGraGruPesq1.QrGraGru1.Close;
      FmGraGruPesq1.QrGraGru1.SQL.Clear;
      FmGraGruPesq1.QrGraGru1.SQL.Add('SELECT gg1.CodUsu, gg1.Nivel1, ');
      FmGraGruPesq1.QrGraGru1.SQL.Add('gg1.GraTamCad, gg1.Nome');
      FmGraGruPesq1.QrGraGru1.SQL.Add('FROM gragru1 gg1');
      //FmGraGruPesq1.QrGraGru1.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
      //FmGraGruPesq1.QrGraGru1.SQL.Add('WHERE pgt.Codigo=' + PGT);
      FmGraGruPesq1.QrGraGru1.SQL.Add('WHERE gg1.Nivel1>-900000');
      FmGraGruPesq1.QrGraGru1.SQL.Add('ORDER BY gg1.Nome');
      UnDmkDAC_PF.AbreQuery(FmGraGruPesq1.QrGraGru1, Dmod.MyDB);
      FmGraGruPesq1.ShowModal;
      Result := Geral.IMV(FmGraGruPesq1.FGraGruX);
      FmGraGruPesq1.Destroy;
    end;
  end;
begin
{
  if DBGrid1.Columns[THackDBGrid(DBGrid1).Col -1].FieldName = 'Preco' then
    DBGrid1.Options := DBGrid1.Options + [dgEditing] else
    DBGrid1.Options := DBGrid1.Options - [dgEditing];
}
  EdKey.ValueVariant := Key;
  EdCampo.Text := DBGFatDivRef.Columns[THackDBGrid(DBGFatDivRef).Col -1].FieldName;
  Campo := LowerCase(EdCampo.Text);
  Valor := Trim(EdValor.Text);
  //
{  ERRO!!!!
  Texto := TStringGrid(DBGFatDivRef).Cells[
    THackDBGrid(DBGFatDivRef).Col -1,
    THackDBGrid(DBGFatDivRef).Row];
  dmkEdit3.Text := Texto;
}
  if Campo = 'referencia' then
  begin
    if Confirmou() then
    begin
      if DmProd.ReopenLocRef(tpRef, Valor,
      FLocNivel1, FLocNivel2, FLocNivel3, FLocPrdGrupTip, FLocControle,
      FLocReferencia, FLocPerComissF, FLocPerComissR) = 1 then
        SetaReduzido();
    end else
    if Key = VK_F4 then
    begin
      GraGruX := ObtemGGXPesquisa();
      if GraGruX <> 0 then
      begin
        if DmProd.ReopenLocRef(tpGGX, GraGruX,
          FLocNivel1, FLocNivel2, FLocNivel3, FLocPrdGrupTip, FLocControle,
          FLocReferencia, FLocPerComissF, FLocPerComissR) = 1 then
        SetaReduzido();
      end;
    end;
  end else
  if Campo = 'perdescoval' then
  begin
    if Confirmou() then
    begin
      Flut := Geral.DMV(Valor);
      TbFatDivRef.Edit;
      TbFatDivRefPrecoAut.Value :=
      TbFatDivRefPrecoPrz.Value -
      (TbFatDivRefPrecoPrz.Value *
      (*TbFatDivRefPerDescoVal.Value*)Flut / 100);
      TbFatDivRefPerDescoVal.Value := Flut;
      TbFatDivRef.Post;
      TbFatDivRef.Refresh;
    end;
  end else
  if Campo = 'precoaut' then
  begin
    if Confirmou() then
    begin
      Flut := Geral.DMV(Valor);
      TbFatDivRef.Edit;
      TbFatDivRefPrecoAut.Value := Flut;
      if TbFatDivRefPrecoPrz.Value > 0 then
        TbFatDivRefPerDescoVal.Value :=
        (TbFatDivRefPrecoPrz.Value - Flut) /
        TbFatDivRefPrecoPrz.Value * 100
      else
        TbFatDivRefPerDescoVal.Value := 0;
      TbFatDivRef.Post;
      TbFatDivRef.Refresh;
    end;
  end else
  if Campo = 'precoven' then
  begin
    if Confirmou() then
    begin
      if TbFatDivRef.State in [dsInsert,dsEdit] then
      begin
        TbFatDivRef.Post;
        TbFatDivRef.Refresh;
      end;
    end;
  end else
  if Campo = 'qtde' then
  begin
    if Confirmou() then
    begin
      Flut := Geral.DMV(Valor);
      TbFatDivRefQtde.Value := Flut;
      case TbFatDivRefGerBxaEstq.Value of
        1: TbFatDivRefPecas.Value  := Flut;
        2: TbFatDivRefAreaM2.Value := Flut;
        3: TbFatDivRefPeso.Value   := Flut;
      end;
      TbFatDivRef.Post;
      TbFatDivRef.Refresh;
    end;
  end;
end;

procedure TFmFatDivRef1.DBGFatDivRefKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  I : word;
begin
  for I := 0 to -1 + DBGFatDivRef.ControlCount do
    if DBGFatDivRef.Controls[I] is TInPlaceEdit then
      with DBGFatDivRef.Controls[I] as TInPlaceEdit do
        EdValor.Text := Text;
end;

procedure TFmFatDivRef1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatDivRef1.FormCreate(Sender: TObject);
begin
  FLocNivel1 := 0;
  FLocNivel2 := 0;
  FLocNivel3 := 0;
  FLocPrdGrupTip := 0;
  FLocControle := 0;
  //
  ReopenLookup();
  {
  QrProds.Close;
  UMyMod.AbreQuery(QrProds, 'TFmFatDivRef.FormCreate()');
  }//
  QrStqCenCad.Close;
  QrStqCenCad.Params[00].AsInteger := FmFatDivGer1.QrFatPedCabRegrFiscal.Value;
  QrStqCenCad.Params[01].AsInteger := FmFatDivGer1.QrFatPedCabEmpresa.Value;
  UMyMod.AbreQuery(QrStqCenCad, Dmod.MyDB, 'TFmFatDivRef.FormCreate()');
  //
end;

procedure TFmFatDivRef1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmFatDivRef1.InsereItem2(): Boolean;
var
  NFe_FatID, Cliente, RegrFiscal, GraGruX,
  StqCenCad, FatSemEstq, AFP_Sit: Integer; AFP_Per: Double;
  Cli_Tipo: Integer; Cli_IE: String; Cli_UF, EMP_UF, EMP_FILIAL,
  ASS_CO_UF, ASS_FILIAL, Item_MadeBy: Integer;
  Item_IPI_ALq: Double;
  Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
  Preco_MedidaA, Preco_MedidaE: Double; Preco_MedOrdem: Integer;
  SQLType: TSQLType;
  PediVda: Integer;
  TabelaPrc, OriCodi, Empresa, OriCnta, Associada, OriPart, InfAdCuztm: Integer;
  NO_tablaPrc: String;
  NeedQI, NeedQA, NeedQK: Boolean;
  //
  Falta, Pecas, AreaM2, AreaP2, Peso: Double;
  Erro: String;
//var
  ErrPreco, AvisoPrc, QtdRed, Casas, Nivel1, Codigo, Controle: Integer;
  Tot, Qtd, PrecoO, PrecoR, PrecoF, QuantP, ValBru, ValLiq, DescoP, DescoV,
  DescoI: Double;
  //
// var
  Qtde: Double;
  TipoCalc, prod_indTot, IDCtrl: Integer;
  prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
begin
  InfAdCuztm := 0;
  if (TbFatDivRef.State = dsInactive) or (TbFatDivRef.RecordCount = 0) then
  begin
    Geral.MB_Aviso('Reduzido n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  NFe_FatID        := VAR_FATID_0001;
  Cliente          := FmFatDivGer1.QrFatPedCabCliente.Value; //QrCliCodigo.Value;
  RegrFiscal       := FmFatDivGer1.QrFatPedCabRegrFiscal.Value; //QrPediVdaRegrFiscal.Value;
  GraGruX          := TbFatDivRefGraGruX.Value;
  prod_indTot      := TbFatDivRefprod_indTot.Value;
  FatSemEstq       := FmFatDivGer1.QrFatPedCabFatSemEstq.Value;
  AFP_Sit          := FmFatDivGer1.QrFatPedCabAFP_Sit.Value;
  AFP_Per          := FmFatDivGer1.QrFatPedCabAFP_Per.Value;
  Cli_Tipo         := FmFatDivGer1.QrCliTipo.Value;
  Cli_IE           := FmFatDivGer1.QrCliIE.Value;
  Cli_UF           := Trunc(FmFatDivGer1.QrCliUF.Value);
  EMP_UF           := FmFatDivGer1.QrFatPedCabEMP_UF.Value;
  EMP_FILIAL       := FmFatDivGer1.QrFatPedCabEMP_FILIAL.Value;
  ASS_CO_UF        := FmFatDivGer1.QrFatPedCabASS_CO_UF.Value;
  ASS_FILIAL       := FmFatDivGer1.QrFatPedCabASS_FILIAL.Value;
  Item_MadeBy      := TbFatDivRefMadeBy.Value;
  ITEM_IPI_Alq     := TbFatDivRefIPI_Alq.Value;
  StqCenCad        := TbFatDivRefStqCenCad.Value;
  //
  TipoCalc         := TbFatDivRefTipoCalc.Value;
  IDCtrl           := TbFatDivRefIDCtrl.Value;
  //
  Casas  := Dmod.QrControleCasasProd.Value;
  PrecoO := TbFatDivRefPrecoOri.Value;
  PrecoR := TbFatDivRefPrecoPrz.Value;
  QuantP := TbFatDivRefQtde.Value;
  //ValCal := PrecoR;// * QuantP;
  DescoP := TbFatDivRefPerDescoVal.Value;
  DescoI := TbFatDivRefPrecoAut.Value - TbFatDivRefPrecoVen.Value;
  PrecoF := TbFatDivRefPrecoVen.Value;
  DescoV := DescoI * QuantP;
  ValBru := TbFatDivRefValorTotBru.Value;
  ValLiq := TbFatDivRefValorTotLiq.Value;
  //
  Preco_PrecoF     := (*QrPreco*)PrecoF(*.Value*);
  Preco_PercCustom := 0;//(*QrPreco*)PercCustom(*.Value*);
  Preco_MedidaC    := 0;//(*QrPreco*)MedidaC(*.Value*);
  Preco_MedidaL    := 0;//(*QrPreco*)MedidaL(*.Value*);
  Preco_MedidaA    := 0;//(*QrPreco*)MedidaA(*.Value*);
  Preco_MedidaE    := 0;//(*QrPreco*)MedidaE(*.Value*);
  Preco_MedOrdem   := 0;//(*QrPreco*)MedOrdem(*.Value*);
  //
  QrLocSMIA.Close;
  QrLocSMIA.Params[0].AsInteger := IDCtrl;
  UnDmkDAC_PF.AbreQuery(QrLocSMIA, Dmod.MyDB);
  if QrLocSMIA.RecordCount > 0 then
    SQLType := stUpd
  else
    SQLType := stIns;
  //
  PediVda     := FmFatDivGer1.QrFatPedCabPedido.Value;
  TabelaPrc   := FmFatDivGer1.QrFatPedCabTabelaPrc.Value;
  NO_tablaPrc := FmFatDivGer1.QrFatPedCabNO_TabelaPrc.Value;
  OriCodi     := FmFatDivGer1.QrFatPedCabCodigo.Value;
  Empresa     := FmFatDivGer1.QrFatPedCabEmpresa.Value;
  OriCnta     := TbFatDivRefVolCnta.Value;
  Associada   := FmFatDivGer1.QrFatPedCabAssociada.Value;
  //
  (* Parei aqui!  ver
  if QrPediGruFracio.Value <> QrItemFracio.Value then
  begin
    Geral.MB_Aviso('Fracionamento n�o confere! AVISE A DERMATEK');
    Screen.Cursor := crDefault;
    Exit;
  end;
  *)
  Qtde := QuantP;
  (* Parei Aqui! Precisa?
  if PediVda > 0 then
  begin
    OriPart := DmPediVda.SaldoRedudidoPed(
      PediVda, GraGruX, Trunc(Qtde + 0.00001), Falta);
    if OriPart = 0 then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end else
      ReopenPrecoFat(OriPart);
      Preco_PrecoF     := QrPrecoPrecoF.Value;
      Preco_PercCustom := QrPrecoPercCustom.Value;
      Preco_MedidaC    := QrPrecoMedidaC.Value;
      Preco_MedidaL    := QrPrecoMedidaL.Value;
      Preco_MedidaA    := QrPrecoMedidaA.Value;
      Preco_MedidaE    := QrPrecoMedidaE.Value;
      Preco_MedOrdem   := QrPrecoMedOrdem.Value;
    if (Qtde <= 0) then
    begin
      Geral.MB_Aviso('Informe a quantidade!');
      Screen.Cursor := crDefault;
      Exit;
    end;
    if Qtde > Falta then
    begin
      Geral.MB_Aviso('A quantidade informada � al�m do necess�rio ' +
        'para este reduzido!' + sLineBreak + 'Necess�rio: ' + FloatToStr(Falta) +
        sLineBreak + 'Informado: ' + FloatToStr(Qtde) + sLineBreak +
        'Inclus�o de item abortada!');
      Screen.Cursor := crDefault;
      Exit;
    end;
    if QrPreco.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Pre�o n�o definido!' + sLineBreak + 'Produto: ' +
      FloatToStr(QrItemCU_Nivel1.Value) + ' - ' + QrItemNOMENIVEL1.Value + sLineBreak +
      'Tabela: ' + FloatToStr(TabelaPrc) + ' - ' +
      FmFatDivGer1.QrFatPedCabNO_TabelaPrc.Value);
      Screen.Cursor := crDefault;
      Exit;
    end else InfAdCuztm := QrPrecoInfAdCuztm.Value;
  end else begin
    OriPart := 0;
    InfAdCuztm := 0;
  end;
  *)


  //if PnMultiGrandeza.Visible then
  if FMultiGrandeza then
  begin
    NeedQI := Geral.IntInConjunto(1, TbFatDivRefHowBxaEstq.Value);
    NeedQA := Geral.IntInConjunto(2, TbFatDivRefHowBxaEstq.Value);
    NeedQK := Geral.IntInConjunto(4, TbFatDivRefHowBxaEstq.Value);
    //
    Pecas  := - TbFatDivRefPecas.Value;
    Peso   := - TbFatDivRefPeso.Value;
    AreaM2 := - TbFatDivRefAreaM2.Value;
    AreaP2 := - TbFatDivRefAreaP2.Value;

    Erro   := '';
    if (Pecas  = 0) and NeedQI then Erro := 'Pe�as';
    if (AreaM2 = 0) and NeedQA then Erro := '�rea';
    if (Peso   = 0) and NeedQK then Erro := 'Peso';
    if Erro <> '' then
    begin
      Geral.MB_Aviso('Grandeza obrigat�ria: "' + Erro + '".');
      Exit;
    end;
  end else begin
    Pecas  := 0;
    AreaM2 := 0;
    AreaP2 := 0;
    Peso   := 0;
  end;

  //FOriCtrl := DmNFe.InsereItemStqMov(Tipo, OriCodi, OriCnta, Empresa, Cliente,
  //
  prod_vFrete := 0.00;
  prod_vSeg   := 0.00;
  prod_vDesc  := 0.00;
  prod_vOutro := 0.00;
  //
  //
  Result := DmFatura.InsereItemStqMov(VAR_FATID_0001, OriCodi, OriCnta, Empresa,
              Cliente, Associada, RegrFiscal, GraGruX, NO_tablaPrc, '',
              StqCenCad, FatSemEstq, AFP_Sit, AFP_Per, Qtde, Cli_Tipo, Cli_IE,
              Cli_UF, EMP_UF, EMP_FILIAL, ASS_CO_UF, ASS_FILIAL, Item_MadeBy,
              Item_IPI_ALq, Preco_PrecoF, Preco_PercCustom, Preco_MedidaC,
              Preco_MedidaL, Preco_MedidaA, Preco_MedidaE, Preco_MedOrdem,
              SQLType, PediVda, OriPart, InfAdCuztm, (*TipoNF*)0, (*modNF*)0,
              (*Serie*)0, (*nNF*)0, (*SitDevolu*)0, (*Servico*)0,(*refNFe*)'',
              0, FOriCtrl, Pecas, AreaM2, AreaP2, Peso, TipoCalc, prod_indTot,
              prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro,
              IDCtrl);
end;

procedure TFmFatDivRef1.ObtemPercentuaisDeComissoes(const Nivel1, Nivel2, Nivel3,
  PrdGrupTip: Integer; var PerComissF, PerComissR: Double; var Nivel: String);
begin
  Nivel := '?';
  PerComissF := 0;
  PerComissR := 0;
  //
  QrLocComiss1.Close;
  QrLocComiss1.Params[0].AsInteger := Nivel1;
  UnDmkDAC_PF.AbreQuery(QrLocComiss1, Dmod.MyDB);
  if (QrLocComiss1PerComissZ.Value = 1) OR ((QrLocComiss1PerComissF.Value <> 0) or
  (QrLocComiss1PerComissR.Value <> 0)) then
  begin
    Nivel := '1';
    PerComissF := QrLocComiss1PerComissF.Value;
    PerComissR := QrLocComiss1PerComissR.Value;
  end else
  begin
    QrLocComissX.Close;
    QrLocComissX.SQL.Clear;
    QrLocComissX.SQL.Add('SELECT PerComissF, PerComissR');
    QrLocComissX.SQL.Add('FROM gracomiss');
    QrLocComissX.SQL.Add('WHERE NivelX=2');
    QrLocComissX.SQL.Add('AND PrdGrupTip=:P0');
    QrLocComissX.SQL.Add('AND Nivel3=:P1');
    QrLocComissX.SQL.Add('AND Nivel2=:P2');
    QrLocComissX.Params[00].AsInteger := PrdGrupTip;
    QrLocComissX.Params[01].AsInteger := Nivel3;
    QrLocComissX.Params[02].AsInteger := Nivel2;
    UnDmkDAC_PF.AbreQuery(QrLocComissX, Dmod.MyDB);
    if QrLocComissX.RecordCount > 0 then
    begin
      Nivel := '2';
      PerComissF := QrLocComissXPerComissF.Value;
      PerComissR := QrLocComissXPerComissR.Value;
    end else
    begin
      QrLocComissX.Close;
      QrLocComissX.SQL.Clear;
      QrLocComissX.SQL.Add('SELECT PerComissF, PerComissR');
      QrLocComissX.SQL.Add('FROM gracomiss');
      QrLocComissX.SQL.Add('WHERE NivelX=2');
      QrLocComissX.SQL.Add('AND PrdGrupTip=:P0');
      QrLocComissX.SQL.Add('AND Nivel3=:P1');
      QrLocComissX.Params[00].AsInteger := PrdGrupTip;
      QrLocComissX.Params[01].AsInteger := Nivel3;
      UnDmkDAC_PF.AbreQuery(QrLocComissX, Dmod.MyDB);
      if QrLocComissX.RecordCount > 0 then
      begin
        Nivel := '3';
        PerComissF := QrLocComissXPerComissF.Value;
        PerComissR := QrLocComissXPerComissR.Value;
      end else
      begin
        QrLocComissX.Close;
        QrLocComissX.SQL.Clear;
        QrLocComissX.SQL.Add('SELECT PerComissF, PerComissR');
        QrLocComissX.SQL.Add('FROM prdgruptip');
        QrLocComissX.SQL.Add('WHERE Codigo=:P0');
        QrLocComissX.Params[00].AsInteger := PrdGrupTip;
        UnDmkDAC_PF.AbreQuery(QrLocComissX, Dmod.MyDB);
        if QrLocComissX.RecordCount > 0 then
        begin
          Nivel := 'T';
          PerComissF := QrLocComissXPerComissF.Value;
          PerComissR := QrLocComissXPerComissR.Value;
        end;
      end;
    end;
  end;
end;

procedure TFmFatDivRef1.ObtemPrecos(const Nivel1: Integer; var PrecoO, PrecoR: Double);
const
  SemPrazo = False;
var
  MedDDSimpl, MedDDReal, MediaSel, TaxaM, Juros: Double;
  Tabela, CondicaoPG, Lista, TipMediaDD, FatSemPrcL: Integer;
begin
  PnJuros.Visible := False;
  Tabela     := FmFatDivGer1.QrFatPedCabTabelaPrc.Value;
  MedDDSimpl := FmFatDivGer1.QrFatPedCabMedDDSimpl.Value;
  MedDDReal  := FmFatDivGer1.QrFatPedCabMedDDReal.Value;
  CondicaoPG := FmFatDivGer1.QrFatPedCabCondicaoPG.Value;
  TipMediaDD := DmPediVda.QrParamsEmpTipMediaDD.Value;
  FatSemPrcL := DmPediVda.QrParamsEmpFatSemPrcL.Value;
  //
  if Tabela > 0 then
  begin
    DmProd.AtualizaDBGridPrecos3(Nivel1, Tabela, CondicaoPG, MedDDSimpl,
    MedDDReal, ST1, ST2, SemPrazo, TipMediaDD, FatSemPrcL, PrecoO, PrecoR);
    //
  end else begin
    QrLista.Close;
    QrLista.Params[00].AsInteger := FmFatDivGer1.QrFatPedCabEmpresa.Value;
    QrLista.Params[01].AsInteger := FmFatDivGer1.QrFatPedCabRegrFiscal.Value;
    UnDmkDAC_PF.AbreQuery(QrLista, Dmod.MyDB);
    if QrLista.RecordCount = 1 then
    begin
      Lista := QrListaGraCusPrc.Value;
      case DmPediVda.QrParamsEmpTipMediaDD.Value of
        1: MediaSel := MedDDSimpl;
        2: MediaSel := MedDDReal;
        else MediaSel := 0;
      end;
      TaxaM := FmFatDivGer1.QrFatPedCabJurosMes.Value;
      case DmPediVda.QrParamsEmpTipCalcJuro.Value of
        1: Juros := dmkPF.CalculaJuroSimples(TaxaM, MediaSel);
        2: Juros := dmkPF.CalculaJuroComposto(TaxaM, MediaSel);
        else Juros := 0;
      end;
      EdJurosMes.ValueVariant := TaxaM;
      EdCustoFin.ValueVariant := Juros;
      PnJuros.Visible := True;
      DmProd.AtualizaDBGridPrecos4(Nivel1, Lista, FmFatDivGer1.QrFatPedCabEmpresa.Value, Juros, PrecoO, PrecoR);
    end else begin
      if QrLista.RecordCount = 0 then
        Geral.MB_Aviso('Nenhuma lista est� definida com o tipo ' +
          'de movimento "Sa�da" na regra fiscal "' + FmFatDivGer1.QrFatPedCabNOMEFISREGCAD.Value
          + '" para a empresa ' + FormatFloat('000', FmFatDivGer1.QrFatPedCabEMP_FILIAL.Value)
          + '!')
      else
        Geral.MB_Aviso('H� ' + Geral.FF0(QrLista.RecordCount) +
          'listas habilitadas para o movimento "Sa�da" na regra fiscal "' +
          FmFatDivGer1.QrFatPedCabNOMEFISREGCAD.Value + '" para a empresa ' +
          FormatFloat('000', FmFatDivGer1.QrFatPedCabEMP_FILIAL.Value) + '. Nenhuma ser�' +
          'considerada!');
    end;
  end;
end;

procedure TFmFatDivRef1.QrSumRefAfterOpen(DataSet: TDataSet);
begin
  BtConverte.Enabled := QrSumRef.RecordCount > 0;
end;

procedure TFmFatDivRef1.QrSumRefBeforeClose(DataSet: TDataSet);
begin
  BtConverte.Enabled := False;
end;

procedure TFmFatDivRef1.QrSumRefCalcFields(DataSet: TDataSet);
begin
  if QrSumRefValorTotLiq.Value = 0 then
    QrSumRefPERCOMISSL.Value := 0
  else
    QrSumRefPERCOMISSL.Value :=
      QrSumRefValComisLiqT.Value /
      QrSumRefValorTotLiq.Value * 100;
end;

procedure TFmFatDivRef1.ReopenFatDivRef(nItem: Integer);
begin
  QrFatPedVol.Close;
  QrFatPedVol.Params[0].AsInteger := FmFatDivGer1.QrFatPedCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrFatPedVol, Dmod.MyDB);
  //
  TbFatDivRef.Close;
  UnDmkDAC_PF.AbreTable(TbFatDivRef, Dmod.MyDB);
  //
  ReopenSumRef();
end;

{
procedure TFmFatDivRef.ReopenFatDivSum();
var
  Cod: Integer;
begin
  Cod := FmFatDivGer1.QrFatPedCabCodigo.Value;
  QrFatDivSum.Close;
  QrFatDivSum.Params[0].AsInteger := Cod;
  UnDmkDAC_PF.AbreQuery(QrFatDivSum, Dmod.MyDB);
  //
  if QrFatDivSum.RecordCount = 0 then
  begin
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'fatdivsum', False, [
    'FatPedCab'], [], [Cod], [], False);
  end;
  QrFatDivSum.Close;
  QrFatDivSum.Params[0].AsInteger := Cod;
  UnDmkDAC_PF.AbreQuery(QrFatDivSum, Dmod.MyDB);
end;
}
procedure TFmFatDivRef1.ReopenLookup();
begin
  QrFatDivRef.Close;
  QrFatDivRef.Params[0].AsInteger := FmFatDivGer1.QrFatPedCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrFatDivRef, Dmod.MyDB);
  //
{
  if TbFatDivref.State <> dsInactive then
    TbFatDivref.Refresh;
}
end;

procedure TFmFatDivRef1.ReopenSumRef();
begin
  QrSumRef.Close;
  QrSumRef.Params[0].AsInteger := FmFatDivGer1.QrFatPedCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSumRef, Dmod.MyDB);
end;

procedure TFmFatDivRef1.TbFatDivRefAfterDelete(DataSet: TDataSet);
begin
  ReopenSumRef();
  ReopenLookup();
end;

procedure TFmFatDivRef1.TbFatDivRefAfterPost(DataSet: TDataSet);
begin
  ReopenSumRef();
  ReopenLookup();
end;

procedure TFmFatDivRef1.TbFatDivRefBeforeDelete(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  try
    if TbFatDivRefIDCtrl.Value <> 0 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa ');
      Dmod.QrUpd.SQL.Add('WHERE IDCtrl=:P0');
      Dmod.QrUpd.Params[0].AsInteger := TbFatDivRefIDCtrl.Value;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM stqmovvala ');
      Dmod.QrUpd.SQL.Add('WHERE IDCtrl=:P0');
      Dmod.QrUpd.Params[0].AsInteger := TbFatDivRefIDCtrl.Value;
      Dmod.QrUpd.ExecSQL;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFatDivRef1.TbFatDivRefBeforeOpen(DataSet: TDataSet);
begin
  TbFatDivRef.Filter := 'Codigo=' +
    FormatFloat('0', FmFatDivGer1.QrFatPedCabCodigo.Value)
end;

procedure TFmFatDivRef1.TbFatDivRefBeforePost(DataSet: TDataSet);
var
  IDCtrl: Integer;
{
  Base: Double;
}
begin
  Screen.Cursor := crHourGlass;
  try
    if TbFatDivRefIDCtrl.Value = 0 then
    begin
      //Controle := DModG.BuscaProximoInteiro('fatdivref', 'Controle', '', 0);
      IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
      TbFatDivRefIDCtrl.Value := IDCtrl;
      TbFatDivRefCodigo.Value := FmFatDivGer1.QrFatPedCabCodigo.Value;
    end;
    //
    if TbFatDivRefStqCenCad.Value = 0 then
      TbFatDivRefStqCenCad.Value := 1;
    //
    TbFatDivRefValorTotBru.Value :=
      TbFatDivRefQtde.Value *
      TbFatDivRefPrecoVen.Value;
    //
{
    TbFatDivRefValorTotLiq.Value :=
      TbFatDivRefValorTotBru.Value -
      (TbFatDivRefPerDescoVal.Value *
      TbFatDivRefValorTotBru.Value / 100);
}
{
    if TbFatDivRefPrecoVen.Value > TbFatDivRefPrecoAut.Value then
      TbFatDivRefValorTotLiq.Value :=
        TbFatDivRefValorTotBru.Value -
        (TbFatDivRefPerDescoVal.Value *
        TbFatDivRefValorTotBru.Value / 100)
     else
      TbFatDivRefValorTotLiq.Value :=
        TbFatDivRefValorTotBru.Value -
        (TbFatDivRefPerDescoVal.Value *
        TbFatDivRefValorTotBru.Value / 100);
}
      TbFatDivRefValorTotLiq.Value :=
        TbFatDivRefValorTotBru.Value;

    // Comiss�o
    TbFatDivRefPerComissT.Value :=
    TbFatDivRefPerComissF.Value +
    TbFatDivRefPerComissR.Value;
    //
    TbFatDivRefValComisDesT.Value :=
      (TbFatDivRefPrecoAut.Value -
      TbFatDivRefPrecoVen.Value) *
      TbFatDivRefQtde.Value;
    if TbFatDivRefValComisDesT.Value < 0 then
      TbFatDivRefValComisDesT.Value := 0;

    TbFatDivRefValComisBruT.Value :=
      (TbFatDivRefPrecoVen.Value *
      TbFatDivRefQtde.Value) *
      TbFatDivRefPerComissT.Value / 100;
    //
    TbFatDivRefValComisLiqT.Value :=
      TbFatDivRefValComisBruT.Value -
      TbFatDivRefValComisDesT.Value;
    //
    if TbFatDivRefValComisLiqT.Value < 0 then
    begin
      TbFatDivRefValComisLiqF.Value := TbFatDivRefValComisLiqT.Value;
      TbFatDivRefValComisLiqR.Value := 0;
    end else begin
      if TbFatDivRefPerComissT.Value > 0 then
      begin
        TbFatDivRefValComisLiqF.Value :=
        (TbFatDivRefPerComissF.Value /
        TbFatDivRefPerComissT.Value) *
        TbFatDivRefValComisLiqT.Value;
        TbFatDivRefValComisLiqR.Value :=
        TbFatDivRefValComisLiqT.Value -
        TbFatDivRefValComisLiqF.Value;
      end else
      begin
        TbFatDivRefValComisLiqF.Value := 0;
        TbFatDivRefValComisLiqR.Value := 0;
      end;
    end;
    //
    if TbFatDivRefVolCnta.Value = 0 then
      TbFatDivRefVolCnta.Value := QrFatPedVolCnta.Value;
    //
    TbFatDivRefTipoCalc.Value := DmProd.DefineTipoCalc(
      FmFatDivGer1.QrFatPedCabRegrFiscal.Value, TbFatDivRefStqCenCad.Value,
      FmFatDivGer1.QrFatPedCabEmpresa.Value);
    //
    TbFatDivRefAreaP2.Value := Geral.ConverteValor(
      TbFatDivRefAreaM2.Value, ctM2toP2, cfQuarto);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFatDivRef1.TbFatDivRefCalcFields(DataSet: TDataSet);
begin
  TbFatDivRefSEQ.Value := TbFatDivRef.RecNo;
  case TbFatDivRefTipoCalc.Value of
    0: TbFatDivRefTIPOCALC_TXT.Value := 'N';
    1: TbFatDivRefTIPOCALC_TXT.Value := 'A';
    2: TbFatDivRefTIPOCALC_TXT.Value := 'S';
    else TbFatDivRefTIPOCALC_TXT.Value := '?';
  end;
end;

end.
