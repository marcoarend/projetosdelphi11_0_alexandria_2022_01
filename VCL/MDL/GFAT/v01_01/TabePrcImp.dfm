object FmTabePrcImp: TFmTabePrcImp
  Left = 339
  Top = 185
  Caption = 'TAB-PRECO-006 :: Impress'#227'o de Lista de Pre'#231'os'
  ClientHeight = 260
  ClientWidth = 475
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 475
    Height = 104
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 111
    object StaticText2: TStaticText
      Left = 0
      Top = 17
      Width = 475
      Height = 17
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 18
      ExplicitWidth = 473
    end
    object StaticText1: TStaticText
      Left = 0
      Top = 0
      Width = 475
      Height = 17
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 1
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 473
    end
    object StaticText3: TStaticText
      Left = 0
      Top = 34
      Width = 475
      Height = 17
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 2
      ExplicitLeft = 1
      ExplicitTop = 35
      ExplicitWidth = 473
    end
    object StaticText4: TStaticText
      Left = 0
      Top = 51
      Width = 475
      Height = 17
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 3
      ExplicitLeft = 1
      ExplicitTop = 52
      ExplicitWidth = 473
    end
    object StaticText5: TStaticText
      Left = 0
      Top = 68
      Width = 475
      Height = 17
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 4
      ExplicitLeft = 1
      ExplicitTop = 69
      ExplicitWidth = 473
    end
    object PB1: TProgressBar
      Left = 0
      Top = 87
      Width = 475
      Height = 17
      Align = alBottom
      TabOrder = 5
      ExplicitLeft = 1
      ExplicitTop = 86
      ExplicitWidth = 473
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 475
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = -160
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 427
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 379
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 361
        Height = 32
        Caption = 'Impress'#227'o de Lista de Pre'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 361
        Height = 32
        Caption = 'Impress'#227'o de Lista de Pre'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 361
        Height = 32
        Caption = 'Impress'#227'o de Lista de Pre'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 152
    Width = 475
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitLeft = -160
    ExplicitTop = 192
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 471
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 196
    Width = 475
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitLeft = -160
    ExplicitTop = 209
    ExplicitWidth = 635
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 471
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 327
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
      end
    end
  end
  object QrGraTamI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM gratamits'
      'WHERE Codigo=:P0')
    Left = 64
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraTamIControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrGraTamINome: TWideStringField
      Alignment = taCenter
      FieldName = 'Nome'
      Size = 5
    end
  end
  object DsGraTamI: TDataSource
    DataSet = QrGraTamI
    Left = 92
    Top = 52
  end
  object QrGraGruC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggc.Nivel1, ggc.Controle, ggc.GraCorCad,'
      'gtc.CodUsu CodUsuGTC, gtc.Nome NomeGTC'
      'FROM gragruc ggc'
      'LEFT JOIN gracorcad gtc ON gtc.Codigo=ggc.GraCorCad'
      'WHERE ggc.Nivel1=:P0')
    Left = 8
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGruCNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGruCControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGraGruCGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Required = True
    end
    object QrGraGruCCodUsuGTC: TIntegerField
      FieldName = 'CodUsuGTC'
      Required = True
    end
    object QrGraGruCNomeGTC: TWideStringField
      FieldName = 'NomeGTC'
      Size = 50
    end
  end
  object DsGraGruC: TDataSource
    DataSet = QrGraGruC
    Left = 36
    Top = 52
  end
end
