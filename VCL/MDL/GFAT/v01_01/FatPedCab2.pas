unit FatPedCab2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids,
  dmkDBGrid, Menus, dmkCheckGroup, frxClass, frxDBSet, dmkImage,
  UnDmkEnums, DmkDAC_PF, UnGrl_Geral, UnGFat_Jan;

type
  TFmFatPedCab2 = class(TForm)
    PainelDados: TPanel;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    PMFatura: TPopupMenu;
    Panel4: TPanel;
    Incluinovofaturamento1: TMenuItem;
    Alterafaturamentoatual1: TMenuItem;
    Excluifaturamentoatual1: TMenuItem;
    DsFatPedCab: TDataSource;
    QrFatPedIts: TmySQLQuery;
    DsFatPedIts: TDataSource;
    DBGFatPedVol: TDBGrid;
    DsEntrega: TDataSource;
    QrEntrega: TmySQLQuery;
    QrEntregaE_ALL: TWideStringField;
    QrEntregaCNPJ_TXT: TWideStringField;
    QrEntregaNOME_TIPO_DOC: TWideStringField;
    QrEntregaTE1_TXT: TWideStringField;
    QrEntregaFAX_TXT: TWideStringField;
    QrEntregaNUMERO_TXT: TWideStringField;
    QrEntregaCEP_TXT: TWideStringField;
    QrEntregaRUA: TWideStringField;
    QrEntregaNUMERO: TIntegerField;
    QrEntregaCOMPL: TWideStringField;
    QrEntregaBAIRRO: TWideStringField;
    QrEntregaCIDADE: TWideStringField;
    QrEntregaNOMELOGRAD: TWideStringField;
    QrEntregaNOMEUF: TWideStringField;
    QrEntregaPais: TWideStringField;
    QrEntregaLograd: TSmallintField;
    QrEntregaCEP: TIntegerField;
    QrEntregaENDEREF: TWideStringField;
    QrEntregaTE1: TWideStringField;
    QrEntregaFAX: TWideStringField;
    DsCli: TDataSource;
    QrCli: TmySQLQuery;
    QrCliE_ALL: TWideStringField;
    QrCliCNPJ_TXT: TWideStringField;
    QrCliNOME_TIPO_DOC: TWideStringField;
    QrCliTE1_TXT: TWideStringField;
    QrCliFAX_TXT: TWideStringField;
    QrCliNUMERO_TXT: TWideStringField;
    QrCliCEP_TXT: TWideStringField;
    QrCliCodigo: TIntegerField;
    QrCliTipo: TSmallintField;
    QrCliCodUsu: TIntegerField;
    QrCliNOME_ENT: TWideStringField;
    QrCliCNPJ_CPF: TWideStringField;
    QrCliIE_RG: TWideStringField;
    QrCliRUA: TWideStringField;
    QrCliCOMPL: TWideStringField;
    QrCliBAIRRO: TWideStringField;
    QrCliCIDADE: TWideStringField;
    QrCliNOMELOGRAD: TWideStringField;
    QrCliNOMEUF: TWideStringField;
    QrCliPais: TWideStringField;
    QrCliENDEREF: TWideStringField;
    QrCliTE1: TWideStringField;
    QrCliFAX: TWideStringField;
    DsPediVda: TDataSource;
    QrPediVda: TmySQLQuery;
    QrPediVdaNOMEFRETEPOR: TWideStringField;
    QrPediVdaCodigo: TIntegerField;
    QrPediVdaCodUsu: TIntegerField;
    QrPediVdaEmpresa: TIntegerField;
    QrPediVdaCliente: TIntegerField;
    QrPediVdaDtaEmiss: TDateField;
    QrPediVdaDtaEntra: TDateField;
    QrPediVdaDtaInclu: TDateField;
    QrPediVdaDtaPrevi: TDateField;
    QrPediVdaPrioridade: TSmallintField;
    QrPediVdaCondicaoPG: TIntegerField;
    QrPediVdaMoeda: TIntegerField;
    QrPediVdaSituacao: TIntegerField;
    QrPediVdaTabelaPrc: TIntegerField;
    QrPediVdaMotivoSit: TIntegerField;
    QrPediVdaLoteProd: TIntegerField;
    QrPediVdaPedidoCli: TWideStringField;
    QrPediVdaFretePor: TSmallintField;
    QrPediVdaTransporta: TIntegerField;
    QrPediVdaRedespacho: TIntegerField;
    QrPediVdaRegrFiscal: TIntegerField;
    QrPediVdaDesoAces_V: TFloatField;
    QrPediVdaDesoAces_P: TFloatField;
    QrPediVdaFrete_V: TFloatField;
    QrPediVdaFrete_P: TFloatField;
    QrPediVdaSeguro_V: TFloatField;
    QrPediVdaSeguro_P: TFloatField;
    QrPediVdaTotalQtd: TFloatField;
    QrPediVdaTotal_Vlr: TFloatField;
    QrPediVdaTotal_Des: TFloatField;
    QrPediVdaTotal_Tot: TFloatField;
    QrPediVdaObserva: TWideMemoField;
    QrPediVdaRepresen: TIntegerField;
    QrPediVdaComisFat: TFloatField;
    QrPediVdaComisRec: TFloatField;
    QrPediVdaCartEmis: TIntegerField;
    QrPediVdaAFP_Sit: TSmallintField;
    QrPediVdaAFP_Per: TFloatField;
    QrPediVdaMedDDSimpl: TFloatField;
    QrPediVdaMedDDReal: TFloatField;
    QrPediVdaValLiq: TFloatField;
    QrPediVdaQuantP: TFloatField;
    QrPediVdaFilial: TIntegerField;
    QrPediVdaNOMETABEPRCCAD: TWideStringField;
    QrPediVdaNOMEMOEDA: TWideStringField;
    QrPediVdaNOMEEMP: TWideStringField;
    QrPediVdaNOMEACC: TWideStringField;
    QrPediVdaNOMETRANSP: TWideStringField;
    QrPediVdaNOMEREDESP: TWideStringField;
    QrPediVdaNOMEFISREGCAD: TWideStringField;
    QrPediVdaNOMEMODELONF: TWideStringField;
    QrPediVdaNOMECARTEMIS: TWideStringField;
    QrPediVdaNOMECONDICAOPG: TWideStringField;
    QrPediVdaNOMEMOTIVO: TWideStringField;
    QrPediVdaCODUSU_ACC: TIntegerField;
    QrPediVdaCODUSU_TRA: TIntegerField;
    QrPediVdaCODUSU_RED: TIntegerField;
    QrPediVdaCODUSU_MOT: TIntegerField;
    QrPediVdaCODUSU_TPC: TIntegerField;
    QrPediVdaCODUSU_PPC: TIntegerField;
    QrPediVdaCODUSU_FRC: TIntegerField;
    PainelEdit: TPanel;
    GroupBox9: TGroupBox;
    Label56: TLabel;
    Label2: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    DBEdit3: TDBEdit;
    DBEdit2: TDBEdit;
    EdPedido: TdmkEdit;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    EdCodigo: TdmkEdit;
    EdCodUsu: TdmkEdit;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    DBText1: TDBText;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit6: TDBEdit;
    DBMemo1: TDBMemo;
    DBEdit4: TDBEdit;
    GroupBox12: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    GroupBox15: TGroupBox;
    Label34: TLabel;
    Label35: TLabel;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    GroupBox14: TGroupBox;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    DBEdit16: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    QrFatPedCabCodigo: TIntegerField;
    QrFatPedCabCodUsu: TIntegerField;
    QrFatPedCabPedido: TIntegerField;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit5: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    GroupBox3: TGroupBox;
    Label12: TLabel;
    DBText2: TDBText;
    Label13: TLabel;
    DBEdit10: TDBEdit;
    DBEdit13: TDBEdit;
    DBMemo3: TDBMemo;
    DBEdit14: TDBEdit;
    GroupBox4: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label19: TLabel;
    DBEdit15: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    GroupBox5: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    GroupBox6: TGroupBox;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit51: TDBEdit;
    DBEdit52: TDBEdit;
    DBEdit53: TDBEdit;
    DBEdit54: TDBEdit;
    DBEdit57: TDBEdit;
    QrFatPedVol: TmySQLQuery;
    DsFatPedVol: TDataSource;
    QrFatPedVolNO_UnidMed: TWideStringField;
    QrFatPedVolCodigo: TIntegerField;
    QrFatPedVolCnta: TIntegerField;
    QrFatPedVolUnidMed: TIntegerField;
    PMVolume: TPopupMenu;
    Incluinovovolume1: TMenuItem;
    Alteravolumeatual1: TMenuItem;
    DBGFatPedIts: TDBGrid;
    Excluivolumeatual1: TMenuItem;
    QrFatPedCabEmpresa: TIntegerField;
    QrFatPedCabTabelaPrc: TIntegerField;
    QrFatPedCabCondicaoPG: TIntegerField;
    QrFatPedCabMedDDReal: TFloatField;
    QrFatPedCabMedDDSimpl: TFloatField;
    QrFatPedCabRegrFiscal: TIntegerField;
    QrFatPedCabNO_TabelaPrc: TWideStringField;
    QrFatPedItsCU_NIVEL1: TIntegerField;
    QrFatPedItsNO_NIVEL1: TWideStringField;
    QrFatPedItsGraCorCad: TIntegerField;
    QrFatPedItsCU_COR: TIntegerField;
    QrFatPedItsNO_COR: TWideStringField;
    QrFatPedItsNO_TAM: TWideStringField;
    QrFatPedItsGraGru1: TIntegerField;
    QrFatPedItsDataHora: TDateTimeField;
    QrFatPedItsTipo: TSmallintField;
    QrFatPedItsOriCodi: TIntegerField;
    QrFatPedItsOriCtrl: TIntegerField;
    QrFatPedItsOriCnta: TIntegerField;
    QrFatPedItsEmpresa: TIntegerField;
    QrFatPedItsStqCenCad: TIntegerField;
    QrFatPedItsGraGruX: TIntegerField;
    QrFatPedItsQtde: TFloatField;
    QrFatPedCabTipMediaDD: TSmallintField;
    QrFatPedCabAFP_Sit: TSmallintField;
    QrFatPedCabAFP_Per: TFloatField;
    QrFatPedCabAssociada: TIntegerField;
    PMItens: TPopupMenu;
    Incluiitenss1: TMenuItem;
    Excluiitemns1: TMenuItem;
    QrFatPedItsQTDE_POSITIVO: TFloatField;
    QrFatPedCabCU_PediVda: TIntegerField;
    DBEdit59: TDBEdit;
    Label36: TLabel;
    DBEdit58: TDBEdit;
    Label37: TLabel;
    Label33: TLabel;
    EdAbertura: TdmkEdit;
    QrFatPedCabAbertura: TDateTimeField;
    QrFatPedCabEncerrou: TDateTimeField;
    QrFatPedCabFatSemEstq: TSmallintField;
    QrSCCs: TmySQLQuery;
    PMImprime: TPopupMenu;
    Anlise1: TMenuItem;
    QrAnalise: TmySQLQuery;
    frxDsAnalise: TfrxDBDataset;
    frxFAT_PEDID_001_00: TfrxReport;
    QrAnalisePreco: TFloatField;
    QrAnaliseTotal: TFloatField;
    QrAnaliseFilial: TIntegerField;
    QrAnaliseCU_NIVEL1: TIntegerField;
    QrAnaliseNO_NIVEL1: TWideStringField;
    QrAnaliseGraCorCad: TIntegerField;
    QrAnaliseCU_COR: TIntegerField;
    QrAnaliseNO_COR: TWideStringField;
    QrAnaliseNO_TAM: TWideStringField;
    QrAnaliseGraGru1: TIntegerField;
    QrAnaliseGraGruX: TIntegerField;
    QrAnaliseQtde: TFloatField;
    frxDSFatPedCab: TfrxDBDataset;
    QrFatPedCab: TmySQLQuery;
    frxDsPediVda: TfrxDBDataset;
    frxDsCli: TfrxDBDataset;
    QrFatPedCabENCERROU_TXT: TWideStringField;
    Label31: TLabel;
    Label32: TLabel;
    QrFatPedCabEMP_CtaProdVen: TIntegerField;
    QrFatPedCabASS_CtaProdVen: TIntegerField;
    QrFatPedCabEMP_FaturaSeq: TSmallintField;
    QrFatPedCabEMP_FaturaSep: TWideStringField;
    QrFatPedCabEMP_FaturaDta: TSmallintField;
    QrFatPedCabASS_FaturaSeq: TSmallintField;
    QrFatPedCabASS_FaturaSep: TWideStringField;
    QrFatPedCabASS_FaturaDta: TSmallintField;
    QrFatPedCabEMP_TxtProdVen: TWideStringField;
    QrFatPedCabASS_TxtProdVen: TWideStringField;
    QrFatPedItsIDCtrl: TIntegerField;
    QrCliIE: TWideStringField;
    QrFatPedCabEMP_FILIAL: TIntegerField;
    QrFatPedCabASS_NO_UF: TWideStringField;
    QrFatPedCabASS_CO_UF: TIntegerField;
    QrFatPedCabASS_FILIAL: TIntegerField;
    SpeedButton5: TSpeedButton;
    Romaneioporgrupo1: TMenuItem;
    QrFatPedCabCliente: TIntegerField;
    PMEncerra: TPopupMenu;
    Encerrafaturamento1: TMenuItem;
    N1: TMenuItem;
    Desfazencerramento1: TMenuItem;
    QrFatPedCabSerieDesfe: TIntegerField;
    QrFatPedCabNFDesfeita: TIntegerField;
    QrFatPedCabPedidoCli: TWideStringField;
    QrPediVdaEMP_UF: TFloatField;
    QrCliUF: TFloatField;
    QrCliLograd: TFloatField;
    QrCliCEP: TFloatField;
    QrCliNUMERO: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtFatura: TBitBtn;
    BtVolume: TBitBtn;
    BtItens: TBitBtn;
    BtEncerra: TBitBtn;
    QrTemItens: TmySQLQuery;
    BtFisRegCad: TBitBtn;
    BtGraGruN: TBitBtn;
    QrFatPedItsCO_NIVEL1: TIntegerField;
    QrPediVdaCODUSU_MDA: TIntegerField;
    QrPediVdaTIPOCART: TIntegerField;
    QrPediVdaMODELO_NF: TIntegerField;
    QrFatPedItsOriPart: TIntegerField;
    QrFatPedItsCALC_TXT: TWideStringField;
    MeEnderecoEntrega2: TMemo;
    QrPediVdaL_Ativo: TSmallintField;
    QrPediVdaEntregaEnti: TIntegerField;
    QrPediVdaEntregaUsa: TSmallintField;
    QrEntregaEnti: TmySQLQuery;
    QrEntregaEntiCodigo: TIntegerField;
    QrEntregaEntiCadastro: TDateField;
    QrEntregaEntiENatal: TDateField;
    QrEntregaEntiPNatal: TDateField;
    QrEntregaEntiTipo: TSmallintField;
    QrEntregaEntiRespons1: TWideStringField;
    QrEntregaEntiRespons2: TWideStringField;
    QrEntregaEntiENumero: TIntegerField;
    QrEntregaEntiPNumero: TIntegerField;
    QrEntregaEntiELograd: TSmallintField;
    QrEntregaEntiPLograd: TSmallintField;
    QrEntregaEntiECEP: TIntegerField;
    QrEntregaEntiPCEP: TIntegerField;
    QrEntregaEntiNOME_ENT: TWideStringField;
    QrEntregaEntiNO_2_ENT: TWideStringField;
    QrEntregaEntiCNPJ_CPF: TWideStringField;
    QrEntregaEntiIE_RG: TWideStringField;
    QrEntregaEntiNIRE_: TWideStringField;
    QrEntregaEntiRUA: TWideStringField;
    QrEntregaEntiSITE: TWideStringField;
    QrEntregaEntiNUMERO: TIntegerField;
    QrEntregaEntiCOMPL: TWideStringField;
    QrEntregaEntiBAIRRO: TWideStringField;
    QrEntregaEntiCIDADE: TWideStringField;
    QrEntregaEntiNOMELOGRAD: TWideStringField;
    QrEntregaEntiNOMEUF: TWideStringField;
    QrEntregaEntiPais: TWideStringField;
    QrEntregaEntiLograd: TSmallintField;
    QrEntregaEntiCEP: TIntegerField;
    QrEntregaEntiTE1: TWideStringField;
    QrEntregaEntiFAX: TWideStringField;
    QrEntregaEntiEMAIL: TWideStringField;
    QrEntregaEntiTRATO: TWideStringField;
    QrEntregaEntiENDEREF: TWideStringField;
    QrEntregaEntiCODMUNICI: TFloatField;
    QrEntregaEntiCODPAIS: TFloatField;
    QrEntregaEntiRG: TWideStringField;
    QrEntregaEntiSSP: TWideStringField;
    QrEntregaEntiDataRG: TDateField;
    QrEntregaEntiIE: TWideStringField;
    QrEntregaEntiE_ALL: TWideStringField;
    QrEntregaEntiCNPJ_TXT: TWideStringField;
    QrEntregaEntiNOME_TIPO_DOC: TWideStringField;
    QrEntregaEntiTE1_TXT: TWideStringField;
    QrEntregaEntiFAX_TXT: TWideStringField;
    QrEntregaEntiNUMERO_TXT: TWideStringField;
    QrEntregaEntiCEP_TXT: TWideStringField;
    QrEntregaCli: TmySQLQuery;
    QrEntregaCliE_ALL: TWideStringField;
    QrEntregaCliCNPJ_TXT: TWideStringField;
    QrEntregaCliNOME_TIPO_DOC: TWideStringField;
    QrEntregaCliTE1_TXT: TWideStringField;
    QrEntregaCliNUMERO_TXT: TWideStringField;
    QrEntregaCliCEP_TXT: TWideStringField;
    QrEntregaCliL_CNPJ: TWideStringField;
    QrEntregaCliL_CPF: TWideStringField;
    QrEntregaCliL_Nome: TWideStringField;
    QrEntregaCliLograd: TSmallintField;
    QrEntregaCliNOMELOGRAD: TWideStringField;
    QrEntregaCliRUA: TWideStringField;
    QrEntregaCliNUMERO: TIntegerField;
    QrEntregaCliCOMPL: TWideStringField;
    QrEntregaCliBAIRRO: TWideStringField;
    QrEntregaCliLCodMunici: TIntegerField;
    QrEntregaCliNO_MUNICI: TWideStringField;
    QrEntregaCliNOMEUF: TWideStringField;
    QrEntregaCliCEP: TIntegerField;
    QrEntregaCliCodiPais: TIntegerField;
    QrEntregaCliNO_PAIS: TWideStringField;
    QrEntregaCliENDEREF: TWideStringField;
    QrEntregaCliTE1: TWideStringField;
    QrEntregaCliEmail: TWideStringField;
    QrEntregaCliL_IE: TWideStringField;
    MeEnderecoEntrega1: TMemo;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFatPedCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFatPedCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtFaturaClick(Sender: TObject);
    procedure PMFaturaPopup(Sender: TObject);
    procedure QrFatPedCabBeforeClose(DataSet: TDataSet);
    procedure QrFatPedCabAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure Incluinovofaturamento1Click(Sender: TObject);
    procedure Alterafaturamentoatual1Click(Sender: TObject);
    procedure EdPedidoExit(Sender: TObject);
    procedure QrPediVdaAfterOpen(DataSet: TDataSet);
    procedure QrPediVdaBeforeClose(DataSet: TDataSet);
    procedure QrPediVdaCalcFields(DataSet: TDataSet);
    procedure QrCliAfterOpen(DataSet: TDataSet);
    procedure QrCliBeforeClose(DataSet: TDataSet);
    procedure QrCliCalcFields(DataSet: TDataSet);
    procedure QrEntregaCalcFields(DataSet: TDataSet);
    procedure QrFatPedVolAfterScroll(DataSet: TDataSet);
    procedure QrFatPedVolBeforeClose(DataSet: TDataSet);
    procedure Incluinovovolume1Click(Sender: TObject);
    procedure Alteravolumeatual1Click(Sender: TObject);
    procedure BtVolumeClick(Sender: TObject);
    procedure PMVolumePopup(Sender: TObject);
    procedure Excluivolumeatual1Click(Sender: TObject);
    procedure QrFatPedVolAfterOpen(DataSet: TDataSet);
    procedure Incluiitenss1Click(Sender: TObject);
    procedure Excluiitemns1Click(Sender: TObject);
    procedure Anlise1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrFatPedCabCalcFields(DataSet: TDataSet);
    procedure DBEdit59Change(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Romaneioporgrupo1Click(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure Encerrafaturamento1Click(Sender: TObject);
    procedure Desfazencerramento1Click(Sender: TObject);
    procedure PMEncerraPopup(Sender: TObject);
    procedure Excluifaturamentoatual1Click(Sender: TObject);
    procedure BtFisRegCadClick(Sender: TObject);
    procedure BtGraGruNClick(Sender: TObject);
    procedure QrEntregaEntiCalcFields(DataSet: TDataSet);
    procedure QrEntregaCliCalcFields(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure MostraEnderecoDeEntrega2();
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure EncerraFaturamento();
    procedure DesfazEncerramento();
  public
    { Public declarations }
    FThisFatID: Integer;
    FMultiGrandeza: Boolean;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenFatPedVol(Cnta: Integer);
    procedure ReopenFatPedIts(OriCtrl: Integer);
    procedure ReopenPediVda(Pedido: Integer);
    //function  Encerra(): Boolean;
  end;

var
  FmFatPedCab2: TFmFatPedCab2;

const
  FFormatFloat = '00000';
  SFaturamentoAberto = 'EM FATURAMENTO';

implementation

uses
  UnFinanceiro, GetValor,
{$IfNDef NAO_GPED} FatPedIts1, FatPedPes2, FatPedImp, {$EndIf}
{$IfNDef SemNFe_0000} ModuleNFe_0000, NFe_PF, {$EndIf}
  UnMyObjects, Module, MyDBCheck, MasterSelFilial, ModuleGeral,
  FatPedVol2, ModPediVda, ModProd, GetData, Principal, FisRegCad, GraGruN,
  FatPedIts2;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFatPedCab2.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFatPedCab2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFatPedCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFatPedCab2.DefParams;
begin
  VAR_GOTOTABELA := 'FatPedCab';
  VAR_GOTOMYSQLTABLE := QrFatPedCab;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT pvd.CodUsu CU_PediVda, pvd.Empresa, ');
  VAR_SQLx.Add('pvd.TabelaPrc, pvd.CondicaoPG, pvd.RegrFiscal, ');
  VAR_SQLx.Add('pvd.AFP_Sit, pvd.AFP_Per, pvd.Cliente, ');
  VAR_SQLx.Add('pvd.PedidoCli, ppc.MedDDReal, ppc.MedDDSimpl, ');
  VAR_SQLx.Add('par.TipMediaDD, par.Associada, par.FatSemEstq, ');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('par.CtaProdVen EMP_CtaProdVen, ');
  VAR_SQLx.Add('par.FaturaSeq EMP_FaturaSeq,');
  VAR_SQLx.Add('par.FaturaSep EMP_FaturaSep,');
  VAR_SQLx.Add('par.FaturaDta EMP_FaturaDta,');
  VAR_SQLx.Add('par.TxtProdVen EMP_TxtProdVen,');
  VAR_SQLx.Add('emp.Filial EMP_FILIAL,');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('ass.CtaProdVen ASS_CtaProdVen, ');
  VAR_SQLx.Add('ass.FaturaSeq ASS_FaturaSeq,');
  VAR_SQLx.Add('ass.FaturaSep ASS_FaturaSep,');
  VAR_SQLx.Add('ass.FaturaDta ASS_FaturaDta,');
  VAR_SQLx.Add('ass.TxtProdVen ASS_TxtProdVen,');
  VAR_SQLx.Add('uf.Nome ASS_NO_UF, ');
  VAR_SQLx.Add('uf.Codigo ASS_CO_UF,');
  VAR_SQLx.Add('ase.Filial ASS_FILIAL,');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('tpc.Nome NO_TabelaPrc, fpc.*');
  VAR_SQLx.Add('FROM fatpedcab fpc ');
  VAR_SQLx.Add('LEFT JOIN pedivda    pvd ON pvd.Codigo=fpc.Pedido ');
  VAR_SQLx.Add('LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG');
  VAR_SQLx.Add('LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc');
  VAR_SQLx.Add('LEFT JOIN paramsemp  par ON par.Codigo=pvd.Empresa');
  VAR_SQLx.Add('LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada');
  VAR_SQLx.Add('LEFT JOIN entidades ase ON ase.Codigo=ass.Codigo');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=par.Codigo');
  VAR_SQLx.Add('LEFT JOIN ufs uf ON uf.Codigo=IF(ase.Tipo=0, ase.EUF, ase.PUF)');
  VAR_SQLx.Add('WHERE fpc.Codigo > -1000');
  VAR_SQLx.Add('AND pvd.CodUsu > 0 ');
  VAR_SQLx.Add('AND pvd.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND fpc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND fpc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND fpc.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Pedido IN (SELECT Codigo FROM pedivda WHERE Empresa IN (' + VAR_LIB_EMPRESAS + ') AND CodUsu > 0 )';
end;

procedure TFmFatPedCab2.DesfazEncerramento();
{$IfNDef SemNFe_0000}
var
  FatNum, Empresa: Integer;
begin
  FatNum  := QrFatPedCabCodigo.Value;
  Empresa := QrFatPedCabEmpresa.Value;
  if DmNFe_0000.DesfazEncerramento('fatpedcab', FThisFatID, FatNum, Empresa) then
    LocCod(FatNum, FatNum);
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappNFe);
{$EndIf}
end;

procedure TFmFatPedCab2.Desfazencerramento1Click(Sender: TObject);
begin
  DesfazEncerramento()
end;

procedure TFmFatPedCab2.EdPedidoExit(Sender: TObject);
begin
  ReopenPediVda(EdPedido.ValueVariant);
  if EdPedido.ValueVariant <> 0 then
  begin
    if QrPediVda.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Pedido n�o localizado!');
      EdPedido.ValueVariant := 0;
    end;
  end;
end;

procedure TFmFatPedCab2.Excluifaturamentoatual1Click(Sender: TObject);
var
  Codigo, Pedido: Integer;
begin
  Codigo := QrFatPedCabCodigo.Value;
  Pedido := QrFatPedCabPedido.Value;
  //
  if Geral.MB_Pergunta('Confirma a exclus�o desta emiss�o?') <> ID_YES then Exit;
  //
  if QrFatPedCabCU_PediVda.Value > 0 then
  begin
    UMyMod.ExcluiRegistroInt1('', 'fatpedcab', 'Codigo', Codigo, Dmod.MyDB);
  end else begin
    UMyMod.ExcluiRegistroInt1('', 'pedivda', 'Codigo', Pedido, Dmod.MyDB);
    UMyMod.ExcluiRegistroInt1('', 'fatpedcab', 'Codigo', Codigo, Dmod.MyDB);
  end;
  LocCod(Codigo, Codigo);
end;

procedure TFmFatPedCab2.Excluiitemns1Click(Sender: TObject);
begin
  // excluir itens e sub itens das tabelas ativas e arquivos mortos
end;

procedure TFmFatPedCab2.Excluivolumeatual1Click(Sender: TObject);
begin
  // N�o pode varios, precisa verificar se tem sub-itens
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrFatPedVol, TDBGrid(DBGFatPedVol),
  'FatPedVol', ['Cnta'], ['Cnta'], istAtual, '');
end;

procedure TFmFatPedCab2.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else
      Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFatPedCab2.MostraEnderecoDeEntrega2();
var
  Entidade: Integer;
begin
  if (QrPediVdaL_Ativo.Value = 1) and (QrPediVdaEntregaEnti.Value = 0) then
  begin
    QrEntregaCli.Close;
    QrEntregaCli.Params[00].AsInteger := QrPediVdaCliente.Value;
    UMyMod.AbreQuery(QrEntregaCli, Dmod.MyDB, 'TFmFatDivGer2.QrCliAfterOpen()');
    //
    //DsEntregaEnti.DataSet := QrEntregaCli;
    MeEnderecoEntrega1.Text := QrEntregaCliE_ALL.Value;
    MeEnderecoEntrega2.Text := QrEntregaCliE_ALL.Value;
  end else
  begin
    if QrPediVdaEntregaEnti.Value = 0 then
      Entidade := QrPediVdaCliente.Value
    else
      Entidade := QrPediVdaEntregaEnti.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEntregaEnti, Dmod.MyDB, [
    'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, Respons2, ',
    'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, ',
    'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, ',
    'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, ',
    'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, ',
    'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, ',
    'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, ',
    'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, ',
    'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, ',
    'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, ',
    'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, ',
    'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, ',
    'mun.Nome CIDADE, ',
    'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, ',
    'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, ',
    'pai.Nome Pais, ',
    'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, ',
    'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, ',
    'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, ',
    'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, ',
    'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, ',
    'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, ',
    'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, ',
    'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, ',
    'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, ',
    'RG, SSP, DataRG, IE ',
    'FROM entidades en ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(en.Tipo=0, en.ECodMunici, en.PCodMunici) ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(en.Tipo=0, en.ECodiPais, en.PCodiPais) ',
    'WHERE en.Codigo=' + Geral.FF0(Entidade),
    'ORDER BY NOME_ENT',
    '']);
    //DsEntregaEnti.DataSet := QrEntregaEnti;
    MeEnderecoEntrega1.Text := QrEntregaEntiE_ALL.Value;
    MeEnderecoEntrega2.Text := QrEntregaEntiE_ALL.Value;
  end;
end;

procedure TFmFatPedCab2.PMEncerraPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := QrFatPedCabEncerrou.Value = 0;
  Encerrafaturamento1.Enabled := Habilita;
  Desfazencerramento1.Enabled := not Habilita;
end;

procedure TFmFatPedCab2.PMFaturaPopup(Sender: TObject);
var
  Habil1: Boolean;
begin
  Habil1 :=
    (QrFatPedCab.State <> dsInactive) and (QrFatPedCab.RecordCount > 0)
    and (QrFatPedCabEncerrou.Value = 0);
  Alterafaturamentoatual1.Enabled    := Habil1;
  Excluifaturamentoatual1.Enabled    := Habil1 and (QrFatPedVol.RecordCount = 0);
  //Encerrarofaturamentoatual1.Enabled := Habil1;
end;

procedure TFmFatPedCab2.PMVolumePopup(Sender: TObject);
begin
  Incluinovovolume1.Enabled :=
    (QrFatPedCab.State = dsBrowse)
  and
    (QrFatPedCab.RecordCount > 0);
  Alteravolumeatual1.Enabled :=
    (QrFatPedVol.State = dsBrowse)
  and
    (QrFatPedVol.RecordCount > 0);
  Excluivolumeatual1.Enabled :=
    Alteravolumeatual1.Enabled
  and
    (QrFatPedIts.RecordCount = 0);
end;

procedure TFmFatPedCab2.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  // anular linha depois
  //Va(vpLast);
end;

procedure TFmFatPedCab2.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFatPedCab2.DBEdit59Change(Sender: TObject);
begin
  if DBEdit59.Text = SFaturamentoAberto then
  begin
    DBEdit59.Font.Color := clRed;
    DBEdit59.Font.Style := [];//[fsBold];
  end else begin
    DBEdit59.Font.Color := clWindowText;
    DBEdit59.Font.Style := [];
  end;
end;

procedure TFmFatPedCab2.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFatPedCab2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFatPedCab2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFatPedCab2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFatPedCab2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFatPedCab2.SpeedButton5Click(Sender: TObject);
{$IfNDef NAO_GPED}
var
  Pedido: Integer;
begin
  if DBCheck.CriaFm(TFmFatPedPes2, FmFatPedPes2, afmoNegarComAviso) then
  begin
    Pedido := EdPedido.ValueVariant;
    FmFatPedPes2.FEdPedido := EdPedido;
    FmFatPedPes2.ShowModal;
    FmFatPedPes2.Destroy;
    if EdPedido.ValueVariant <> Pedido then
      BtConfirma.SetFocus;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
end;

procedure TFmFatPedCab2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFatPedCabCodigo.Value;
  Close;
end;

procedure TFmFatPedCab2.BtVolumeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMVolume, BtVolume);
end;

procedure TFmFatPedCab2.BtFaturaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFatura, BtFatura);
end;

procedure TFmFatPedCab2.BtFisRegCadClick(Sender: TObject);
var
  Codigo, FisRegCad: Integer;
begin
  Codigo    := QrFatPedCabCodigo.Value;
  FisRegCad := QrPediVdaRegrFiscal.Value;
  //
  if DBCheck.CriaFm(TFmFisRegCad, FmFisRegCad, afmoNegarComAviso) then
  begin
    FmFisRegCad.LocCod(FisRegCad, FisRegCad);
    FmFisRegCad.ShowModal;
    FmFisRegCad.Destroy;
    try
      Screen.Cursor := crHourGlass;
      LocCod(Codigo, Codigo);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmFatPedCab2.BtGraGruNClick(Sender: TObject);
var
  Codigo, Nivel1: Integer;
begin
  Codigo := QrFatPedCabCodigo.Value;
  Nivel1 := QrFatPedItsCO_NIVEL1.Value;
  //
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    FmGraGruN.LocalizaIDNivel1(Nivel1);
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
    try
      Screen.Cursor := crHourGlass;
      LocCod(Codigo, Codigo);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmFatPedCab2.Alterafaturamentoatual1Click(Sender: TObject);
begin
  QrPediVda.Close;
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrFatPedCab, [PainelDados],
  [PainelEdita], EdPedido, ImgTipo, 'fatpedcab');
end;

procedure TFmFatPedCab2.Alteravolumeatual1Click(Sender: TObject);
begin
  if UMyMod.FormInsUpd_Cria(TFmFatPedVol2, FmFatPedVol2, afmoNegarComAviso,
    QrFatPedVol, stUpd) then
  begin
    FmFatPedVol2.FChamouFatPedVol := cfpvFatPedCab;
    FmFatPedVol2.DBEdCodigo.DataSource := DsFatPedCab;
    FmFatPedVol2.DBEdit1.DataSource := DsFatPedCab;
    FmFatPedVol2.DBEdit2.DataSource := DsFatPedCab;
    FmFatPedVol2.ShowModal;
    FmFatPedVol2.Destroy;
  end;
end;

procedure TFmFatPedCab2.Anlise1Click(Sender: TObject);
begin
  QrAnalise.Close;
  QrAnalise.Params[0].AsInteger := QrFatPedCabCodigo.Value;
  UMyMod.AbreQuery(QrAnalise, Dmod.MyDB, 'TFmFatPedCab.Anlise1Click()');
  //
  MyObjects.frxMostra(frxFAT_PEDID_001_00, 'An�lise de faturamento');
end;

procedure TFmFatPedCab2.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmFatPedCab2.EncerraFaturamento();
{$IfNDef SemNFe_0000}
const
  Tipo = 1;
var
  Especie: String;
  Quantidade: Integer;
begin
  QrTemItens.Close;
  QrTemItens.Params[0].AsInteger := QrFatPedCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrTemItens, Dmod.MyDB);
  //
  if QrTemItens.RecordCount = 0 then
  begin
    Geral.MB_Aviso('Encerramento abortado! N�o h� itens no fauramento!');
    Exit;
  end;

  UnNFe_PF.EncerraFaturamento(QrFatPedCabFatSemEstq.Value,
    QrFatPedCabCodigo.Value, QrFatPedVolCnta.Value, Tipo,
    QrFatPedCabRegrFiscal.Value, 'fatpedcab');
  //
  LocCod(QrFatPedCabCodigo.Value, QrFatPedCabCodigo.Value);

{
  if not DmProd.VerificaEstoqueTodoFaturamento(QrFatPedCabFatSemEstq.Value,
    QrFatPedCabCodigo.Value, QrFatPedVolCnta.Value, 1, QrFatPedCabRegrFiscal.Value)
  then
    Exit;
  //
  QrTemItens.Close;
  QrTemItens.Params[0].AsInteger := QrFatPedCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrTemItens, Dmod.MyDB);
  //
  if QrTemItens.RecordCount = 0 then
  begin
    Geral.MB_Aviso('Encerramento abortado! N�o h� itens no fauramento!');
    Exit;
  end;

  if DBCheck.CriaFm(TFmNFaEdit, FmNFaEdit, afmoNegarComAviso) then
  begin
    DmPediVda.ReopenFatPedCab(QrFatPedCabCodigo.Value, True);
    //FmNFaEdit.ReopenFatPedCab(QrFatPedCabCodigo.Value);
    //
    FmNFaEdit.ReopenFatPedNFs(1,0);

    //

    if not DmNFe_0000.Obtem_Serie_e_NumNF_Novo(
    QrFatPedCabSerieDesfe.Value,
    QrFatPedCabNFDesfeita.Value,
    FmNFaEdit.QrImprimeSerieNF_Normal.Value,
    FmNFaEdit.QrImprimeCtrl_nfs.Value,
    QrFatPedCabEmpresa.Value,
    FmNFaEdit.QrFatPedNFs.FieldByName('Filial').AsInteger,
    FmNFaEdit.QrImprimeMaxSeqLib.Value,
    FmNFaEdit.EdSerieNF, FmNFaEdit.EdNumeroNF(*),
    SerieNFTxt, NumeroNF*)) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;

    Especie := '';
    Quantidade := 0;
    //
    QrVolumes.Close;
    QrVolumes.Params[00].AsInteger := QrFatPedCabCodigo.Value;
    UMyMod.AbreQuery(QrVolumes, Dmod.MyDB, 'TFmFatPedCab.BtEncerraClick()');
    QrVolumes.First;
    while not QrVolumes.Eof do
    begin
      Quantidade := Quantidade + QrVolumesVolumes.Value;
      if Especie <> '' then
        Especie := Especie + ' + ';
      Especie := Especie + ' ' +
        // tirado em 2010-08-13
        //Geral.FF0(QrVolumesVolumes.Value) + ' ' +
        QrVolumesNO_UnidMed.Value;
      //
      QrVolumes.Next;
    end;
    FmNFaEdit.EdQuantidade.ValueVariant := FloatToStr(Quantidade);
    FmNFaEdit.EdEspecie.ValueVariant := Especie;
    //
    FmNFaEdit.EdNumeroNF.Enabled := (FmNFaEdit.QrImprimeIncSeqAuto.Value = 0);
    //
    (*
    FmNFaEdit.QrCFOP.Close;
    FmNFaEdit.QrCFOP.Params[00].AsInteger := FThisFatID;
    FmNFaEdit.QrCFOP.Params[01].AsInteger := FmFatPedCab.QrFatPedCabCodigo.Value;
    FmNFaEdit.QrCFOP.Params[02].AsInteger := FmFatPedCab.QrFatPedCabEmpresa.Value;
    UMyMod.AbreQuery(FmNFaEdit.QrCFOP, 'TFmFatPedCab.BtEncerraClick()');
    FmNFaEdit.EdCFOP1.Text := FmNFaEdit.QrCFOPCFOP.Value;
    FmNFaEdit.CBCFOP1.KeyValue := FmNFaEdit.QrCFOPCFOP.Value;
    *)
    //
    // NF-e 2.00
    if (DModG.QrParamsEmpCRT.Value = 3) and
    (DModG.QrParamsEmpNFeNT2013_003LTT.Value < 2) then
      FmNFaEdit.PageControl1.ActivePageIndex := 0
    else
      FmNFaEdit.PageControl1.ActivePageIndex := 1;    
    //
    // NF-e 2.00
    FmNFaEdit.EdHrEntraSai.ValueVariant := Time();
    FmNFaEdit.Eddest_email.Text         := DmodG.ObtemPrimeiroEMail_NFe(QrFatPedCabEmpresa.Value, QrFatPedCabCliente.Value);
    // fazer aqui logo ap�s a reabertura do QrParamsEmp
    FmNFaEdit.RGCRT.ItemIndex           := DmodG.QrParamsEmpCRT.Value;
    FmNFaEdit.EdVagao.Text              := ''; // Parei aqui # 2.00
    FmNFaEdit.EdBalsa.Text              := ''; // Parei aqui # 2.00
    // fim NF-e 2.00
    //
    FmNFaEdit.EdCompra_XNEmp.Text := '';
    FmNFaEdit.EdCompra_XPed.Text := QrFatPedCabPedidoCli.Value;
    FmNFaEdit.EdCompra_XCont.Text := '';
    //
    FmNFaEdit.ReopenStqMovValX(0);
    FmNFaEdit.ImgTipo.SQLType := stIns;
    FmNFaEdit.ShowModal;
    FmNFaEdit.Destroy;
    // Reabrir de novo!
    LocCod(QrFatPedCabCodigo.Value, QrFatPedCabCodigo.Value);
    //
    if QrFatPedCabEncerrou.Value > 0 then
    begin
      FmPrincipal.MostraFatPedNFs(QrFatPedCabEMP_FILIAL.Value,
      QrFatPedCabCliente.Value, QrFatPedCabCU_PediVda.Value, True);
      (*
      if DBCheck.CriaFm(TFmFatPedNFs, FmFatPedNFs, afmoNegarComAviso) then
      begin
        FmFatPedNFs.EdFilial.ValueVariant  := QrFatPedCabEMP_FILIAL.Value;
        FmFatPedNFs.CBFilial.KeyValue      := QrFatPedCabEMP_FILIAL.Value;
        FmFatPedNFs.EdCliente.ValueVariant := QrFatPedCabCliente.Value;
        FmFatPedNFs.CBCliente.KeyValue     := QrFatPedCabCliente.Value;
        FmFatPedNFs.EdPedido.ValueVariant  := QrFatPedCabCU_PediVda.Value;
        FmFatPedNFs.ShowModal;
        FmFatPedNFs.Destroy;
      end;
      *)
    end;
  end;
&*)
}
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappNFe);
{$EndIf}
end;

procedure TFmFatPedCab2.Encerrafaturamento1Click(Sender: TObject);
begin
  EncerraFaturamento();
end;

procedure TFmFatPedCab2.BtConfirmaClick(Sender: TObject);
var
  Codigo, Pedido, CodUsu: Integer;
  // Balan�o aberto
  PrdGrupTip, Empresa, FisRegCad: Integer;
  Abertura: String;
begin
  Pedido := Geral.IMV(EdPedido.Text);
  if Pedido = 0 then
  begin
    Geral.MB_Aviso('Defina o pedido!');
    Exit;
  end else begin
    Pedido := QrPediVdaCodigo.Value;
  end;
  PrdGrupTip := 1;
  Empresa    := QrPediVdaEmpresa.Value;
  FisRegCad  := QrPediVdaRegrFiscal.Value;
  Abertura   := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
  // Verifica se existe balan�o aberto
  if DmProd.ExisteBalancoAberto_StqCen_Mul(PrdGrupTip, Empresa, FisRegCad) then Exit;
  Codigo := UMyMod.BuscaEmLivreY_Def('FatPedCab', 'Codigo', ImgTipo.SQLType,
    QrFatPedCabCodigo.Value);
  CodUsu := DModG.BuscaProximoCodigoInt('controle', 'StqMovUsu', '',
    EdCodUsu.ValueVariant);
  EdCodUsu.ValueVariant := CodUsu;
{
  N�o � poss�vel por causa do Pedido!
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmFatPedCab, PainelEdit,
    'FatPedCab', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
}
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'fatpedcab', False, [
  'CodUsu', 'Pedido', 'Abertura'
  {
  'Serie', 'NF', 'PIS#Per', 'PIS#Val',
  'COFINS#Per', 'COFINS#Val', 'IR#Per', 'IR#Val', 'CS#Per', 'CS#Val',
  'ISS#Per', 'ISS#Val'
  }
  ], ['Codigo'], [
  CodUsu, Pedido, Abertura
  {Serie, NF, PIS#Per, PIS#Val,
  COFINS#Per, COFINS#Val, IR#Per, IR#Val, CS#Per, CS#Val,
  ISS#Per, ISS#Val
  }
  ], [Codigo], True) then
  begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
    ImgTipo.SQLType := stLok;
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmFatPedCab2.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'FatPedCab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'FatPedCab', 'Codigo');
  MostraEdicao(0, stLok, 0);
  //
  ReopenPediVda(QrFatPedCabCU_PediVda.Value);
end;

procedure TFmFatPedCab2.BtItensClick(Sender: TObject);
begin
  Incluiitenss1Click(Self);
  //MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmFatPedCab2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FMultiGrandeza    := False;
  FThisFatID        := 1;
  PainelEdit.Align  := alClient;
  Panel4.Align      := alClient;
  CriaOForm;
end;

procedure TFmFatPedCab2.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFatPedCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFatPedCab2.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmFatPedCab2.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFatPedCab2.SbNovoClick(Sender: TObject);
var
  Cod: Variant;
  Codigo: Integer;
  Qry: TmySQLQuery;
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrFatPedCabCodUsu.Value, LaRegistro.Caption);
  //
  Cod := 0;
  //
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, Cod, 0, 0,
  '', '', True, 'C�digo do pedido', 'Informe o "C�digo do pedido": ', 0, Cod) then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT fpc.Codigo',
        'FROM fatpedcab fpc',
        'LEFT JOIN pedivda pvd ON fpc.Pedido = pvd.Codigo',
        'WHERE pvd.CodUsu=' + Geral.FF0(Cod),
        '']);
      if Qry.RecordCount > 0 then
      begin
        Codigo := Qry.FieldByName('Codigo').AsInteger;
        //
        LocCod(Codigo, Codigo);
      end else
        Geral.MB_Aviso('Nenhum registro foi localizado!');
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmFatPedCab2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFatPedCab2.QrCliAfterOpen(DataSet: TDataSet);
begin
  QrEntrega.Close;
  QrEntrega.Params[00].AsInteger := QrPediVdaCliente.Value;
  UMyMod.AbreQuery(QrEntrega, Dmod.MyDB, 'TFmFatPedCab.QrCliAfterOpen()');
end;

procedure TFmFatPedCab2.QrCliBeforeClose(DataSet: TDataSet);
begin
  QrEntrega.Close;
end;

procedure TFmFatPedCab2.QrCliCalcFields(DataSet: TDataSet);
begin
  QrCliTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrCliTe1.Value);
  QrCliFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrCliFax.Value);
  QrCliCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrCliCNPJ_CPF.Value);
  QrCliNOME_TIPO_DOC.Value :=
    dmkPF.ObtemTipoDocTxtDeCPFCNPJ(QrCliCNPJ_CPF.Value) + ':';
  QrCliNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrCliRua.Value, Trunc(QrCliNumero.Value), False);
  //
  QrCliE_ALL.Value := Uppercase(QrCliNOMELOGRAD.Value);
  if Trim(QrCliE_ALL.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ';
  QrCliE_ALL.Value := QrCliE_ALL.Value + Uppercase(QrCliRua.Value);
  if Trim(QrCliRua.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNUMERO_TXT.Value;
  if Trim(QrCliCompl.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ' + Uppercase(QrCliCompl.Value);
  if Trim(QrCliBairro.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliBairro.Value);
  if QrCliCEP.Value > 0 then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrCliCEP.Value);
  if Trim(QrCliCidade.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliCidade.Value);
  if Trim(QrCliNOMEUF.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNOMEUF.Value;
  if Trim(QrCliPais.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + QrCliPais.Value;
  //
  //QrCliCEP_TXT.Value :=Geral.FormataCEP_NT(QrCliCEP.Value);
  //
end;

procedure TFmFatPedCab2.QrEntregaCalcFields(DataSet: TDataSet);
begin
  QrEntregaTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaTe1.Value);
  QrEntregaFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaFax.Value);
  {
  QrEntregaCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEntregaCNPJ_CPF.Value);
  QrEntregaNOME_TIPO_DOC.Value :=
    dmkPF.ObtemTipoDocTxtDeCPFCNPJ(QrEntregaCNPJ_CPF.Value) + ':';
  }
  QrEntregaNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEntregaRua.Value, QrEntregaNumero.Value, False);
  //
  QrEntregaE_ALL.Value := Uppercase(QrEntregaNOMELOGRAD.Value);
  if Trim(QrEntregaE_ALL.Value) <> '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' ';
  QrEntregaE_ALL.Value := QrEntregaE_ALL.Value + Uppercase(QrEntregaRua.Value);
  if Trim(QrEntregaRua.Value) <> '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ', ' + QrEntregaNUMERO_TXT.Value;
  if Trim(QrEntregaCompl.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' ' + Uppercase(QrEntregaCompl.Value);
  if Trim(QrEntregaBairro.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' - ' + Uppercase(QrEntregaBairro.Value);
  if QrEntregaCEP.Value > 0 then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEntregaCEP.Value);
  if Trim(QrEntregaCidade.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' - ' + Uppercase(QrEntregaCidade.Value);
  if Trim(QrEntregaNOMEUF.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ', ' + QrEntregaNOMEUF.Value;
  if Trim(QrEntregaPais.Value) <>  '' then QrEntregaE_ALL.Value :=
    QrEntregaE_ALL.Value + ' - ' + QrEntregaPais.Value;
  //
  //QrEntregaCEP_TXT.Value :=Geral.FormataCEP_NT(QrEntregaCEP.Value);
  //
  if Trim(QrEntregaE_ALL.Value) = '' then
    QrEntregaE_ALL.Value := QrCliE_ALL.Value;
end;

procedure TFmFatPedCab2.QrEntregaCliCalcFields(DataSet: TDataSet);
begin
  QrEntregaCliTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaCliTe1.Value);
(*
  QrEntregaCliFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaCliFax.Value);
*)
  {
  QrEntregaCliCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEntregaCliCNPJ_CPF.Value);
  QrEntregaCliNOME_TIPO_DOC.Value :=
    dmkPF.ObtemTipoDocTxtDeCPFCNPJ(QrEntregaCliCNPJ_CPF.Value) + ':';
  }
  QrEntregaCliNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEntregaCliRua.Value, QrEntregaCliNumero.Value, False);
  //
/////////////////////////////////////////////////////////////////////////////////
  QrEntregaCliE_ALL.Value := QrEntregaCliL_Nome.Value;
  if QrEntregaCliL_CNPJ.Value <> '' then
    QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + ' CNPJ: ' +
    Geral.FormataCNPJ_TT(QrEntregaCliL_CNPJ.Value)
  else
    QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + ' CPF: ' +
    Geral.FormataCNPJ_TT(QrEntregaCliL_CPF.Value);
  if Trim(QrEntregaCliL_IE.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' I.E. ' + QrEntregaCliL_IE.Value;
  if Trim(QrEntregaCliTE1.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' Fone: ' + QrEntregaCliTE1_TXT.Value;
  if Trim(QrEntregaCliEMAIL.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' Email: ' + QrEntregaCliEMAIL.Value;

  QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + sLineBreak +
    Uppercase(QrEntregaCliNOMELOGRAD.Value);
  if Trim(QrEntregaCliE_ALL.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' ';
  QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + Uppercase(QrEntregaCliRua.Value);
  if Trim(QrEntregaCliRua.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ', ' + QrEntregaCliNUMERO_TXT.Value;
  if Trim(QrEntregaCliCompl.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' ' + Uppercase(QrEntregaCliCompl.Value);
  if Trim(QrEntregaCliBairro.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + Uppercase(QrEntregaCliBairro.Value);
  if QrEntregaCliCEP.Value > 0 then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEntregaCliCEP.Value);
  if Trim(QrEntregaCliNO_MUNICI.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + Uppercase(QrEntregaCliNO_MUNICI.Value);
  if Trim(QrEntregaCliNOMEUF.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ', ' + QrEntregaCliNOMEUF.Value;
  if Trim(QrEntregaCliNO_PAIS.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + QrEntregaCliNO_PAIS.Value;
  //
  //QrEntregaCliCEP_TXT.Value :=Geral.FormataCEP_NT(QrEntregaCliCEP.Value);
  //
  //if Trim(QrEntregaCliE_ALL.Value) = '' then
    //QrEntregaCliE_ALL.Value := QrCliE_ALL.Value;
end;

procedure TFmFatPedCab2.QrEntregaEntiCalcFields(DataSet: TDataSet);
begin
  QrEntregaEntiTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaEntiTe1.Value);
  QrEntregaEntiFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaEntiFax.Value);
  {
  QrEntregaEntiCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
  QrEntregaEntiNOME_TIPO_DOC.Value :=
    dmkPF.ObtemTipoDocTxtDeCPFCNPJ(QrEntregaEntiCNPJ_CPF.Value) + ':';
  }
  QrEntregaEntiNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEntregaEntiRua.Value, QrEntregaEntiNumero.Value, False);
  //
  QrEntregaEntiE_ALL.Value := QrEntregaEntiNOME_ENT.Value;
  case QrEntregaEntiTipo.Value of
    0: QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + ' CNPJ: ' +
       Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
    1: QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + ' CPF: ' +
       Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
  end;
  if Trim(QrEntregaEntiIE.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' I.E. ' + QrEntregaEntiIE.Value;
  if Trim(QrEntregaEntiTE1.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' Fone: ' + QrEntregaEntiTE1_TXT.Value;
  if Trim(QrEntregaEntiEMAIL.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' Email: ' + QrEntregaEntiEMAIL.Value;

  QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + sLineBreak +
    Uppercase(QrEntregaEntiNOMELOGRAD.Value);
  if Trim(QrEntregaEntiE_ALL.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' ';
  QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + Uppercase(QrEntregaEntiRua.Value);
  if Trim(QrEntregaEntiRua.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ', ' + QrEntregaEntiNUMERO_TXT.Value;
  if Trim(QrEntregaEntiCompl.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' ' + Uppercase(QrEntregaEntiCompl.Value);
  if Trim(QrEntregaEntiBairro.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + Uppercase(QrEntregaEntiBairro.Value);
  if QrEntregaEntiCEP.Value > 0 then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEntregaEntiCEP.Value);
  if Trim(QrEntregaEntiCidade.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + Uppercase(QrEntregaEntiCidade.Value);
  if Trim(QrEntregaEntiNOMEUF.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ', ' + QrEntregaEntiNOMEUF.Value;
  if Trim(QrEntregaEntiPais.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + QrEntregaEntiPais.Value;
  //
  //QrEntregaEntiCEP_TXT.Value :=Geral.FormataCEP_NT(QrEntregaEntiCEP.Value);
  //
  //if Trim(QrEntregaEntiE_ALL.Value) = '' then
    //QrEntregaEntiE_ALL.Value := QrCliE_ALL.Value;
end;

procedure TFmFatPedCab2.QrFatPedCabAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  QueryPrincipalAfterOpen;
  Habilita := QrFatPedCab.RecordCount > 0;
  BtVolume.Enabled  := Habilita and (QrFatPedCabEncerrou.Value = 0);
  BtEncerra.Enabled := Habilita;
end;

procedure TFmFatPedCab2.QrFatPedCabAfterScroll(DataSet: TDataSet);
begin
  ReopenFatPedVol(0);
  ReopenPediVda(QrFatPedCabCU_PediVda.Value);
end;

procedure TFmFatPedCab2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatPedCab2.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFatPedCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'FatPedCab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFatPedCab2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatPedCab2.Incluiitenss1Click(Sender: TObject);
begin
  if MyObjects.FIC(QrFatPedCabRegrFiscal.Value = 0, nil,
    'Regra fiscal n�o definida no pedido!') then Exit;
  //
{$IfNDef NAO_GPED}
  UmyMod.FormInsUpd_Show(TFmFatPedIts2, FmFatPedIts2, afmoNegarComAviso,
    QrFatPedIts, stIns);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
end;

procedure TFmFatPedCab2.Incluinovofaturamento1Click(Sender: TObject);
begin
  QrPediVda.Close;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrFatPedCab, [PainelDados],
  [PainelEdita], EdPedido, ImgTipo, 'fatpedcab');
  EdAbertura.ValueVariant :=  DModG.ObtemAgora();
end;

procedure TFmFatPedCab2.Incluinovovolume1Click(Sender: TObject);
var
  IncluiIts: Boolean;
begin
  if UMyMod.FormInsUpd_Cria(TFmFatPedVol2, FmFatPedVol2, afmoNegarComAviso,
    QrFatPedVol, stIns) then
  begin
    FmFatPedVol2.FChamouFatPedVol := cfpvFatPedCab;
    FmFatPedVol2.ShowModal;
    IncluiIts := FmFatPedVol2.FIncluiuVol;
    FmFatPedVol2.Destroy;
    if IncluiIts then
      Incluiitenss1Click(Self);
  end;
end;

procedure TFmFatPedCab2.QrFatPedCabBeforeClose(DataSet: TDataSet);
begin
  QrFatPedIts.Close;
  QrPediVda.Close;
  BtVolume.Enabled := False;
  BtEncerra.Enabled := False;
end;

procedure TFmFatPedCab2.QrFatPedCabBeforeOpen(DataSet: TDataSet);
begin
  QrFatPedCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFatPedCab2.QrFatPedCabCalcFields(DataSet: TDataSet);
begin
  if QrFatPedCabEncerrou.Value = 0 then
    QrFatPedCabENCERROU_TXT.Value := SFaturamentoAberto
  else
    QrFatPedCabENCERROU_TXT.Value := Geral.FDT(QrFatPedCabEncerrou.Value, 0);
end;

procedure TFmFatPedCab2.QrFatPedVolAfterOpen(DataSet: TDataSet);
begin
  BtItens.Enabled := (QrFatPedVol.RecordCount > 0) and
    (QrFatPedCabEncerrou.Value = 0);
end;

procedure TFmFatPedCab2.QrFatPedVolAfterScroll(DataSet: TDataSet);
begin
  ReopenFatPedIts(0);
end;

procedure TFmFatPedCab2.QrFatPedVolBeforeClose(DataSet: TDataSet);
begin
  QrFatPedIts.Close;
  BtItens.Enabled := False;
end;

procedure TFmFatPedCab2.QrPediVdaAfterOpen(DataSet: TDataSet);
begin
  QrCli.Close;
  QrCli.Params[00].AsInteger := QrPediVdaCliente.Value;
  UMyMod.AbreQuery(QrCli, Dmod.MyDB, 'TFmFatPedCab.QrPediVdaAfterOpen()');
  //
  MostraEnderecoDeEntrega2();
end;

procedure TFmFatPedCab2.QrPediVdaBeforeClose(DataSet: TDataSet);
begin
  QrCli.Close;
  MeEnderecoEntrega1.Text := '';
  MeEnderecoEntrega2.Text := '';
end;

procedure TFmFatPedCab2.QrPediVdaCalcFields(DataSet: TDataSet);
begin
  QrPediVdaNOMEFRETEPOR.Value := dmkPF.FretePor_Txt(QrPediVdaFretePor.Value);
end;

procedure TFmFatPedCab2.ReopenFatPedIts(OriCtrl: Integer);
begin
  QrFatPedIts.Close;
  QrFatPedIts.ParamByName('P0').AsInteger := QrFatPedCabCodigo.Value;
  QrFatPedIts.ParamByName('P1').AsInteger := QrFatPedVolCnta.Value;
  UMyMod.AbreQuery(QrFatPedIts, Dmod.MyDB, 'TFmFatPedCab.ReopenFatPedIts()');
  //
  if OriCtrl <> 0 then
    QrFatPedIts.Locate('OriCtrl', OriCtrl, []);
end;

procedure TFmFatPedCab2.ReopenFatPedVol(Cnta: Integer);
begin
  QrFatPedVol.Close;
  QrFatPedVol.Params[0].AsInteger := QrFatPedCabCodigo.Value;
  UMyMod.AbreQuery(QrFatPedVol, Dmod.MyDB, 'TFmFatPedCab.ReopenFatPedVol()');
  //
  if Cnta <> 0 then
    QrFatPedVol.Locate('Cnta', Cnta, []);
end;

procedure TFmFatPedCab2.ReopenPediVda(Pedido: Integer);
begin
(*
  QrPediVda.Close;
  QrPediVda.Params[0].AsInteger := Pedido;
  UMyMod.AbreQuery(QrPediVda, Dmod.MyDB, 'TFmFatPedCab.ReopenPediVda()');
*)
  UnDMkDAC_PF.AbreMySQLQuery0(QrPediVda, Dmod.MyDB, [
  'SELECT pvd.Codigo, pvd.CodUsu, pvd.Empresa, pvd.Cliente, ',
  'pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi, ',
  'pvd.Prioridade, pvd.CondicaoPG, pvd.Moeda, ',
  'pvd.Situacao, pvd.TabelaPrc, pvd.MotivoSit, pvd.LoteProd, ',
  'pvd.PedidoCli, pvd.FretePor, pvd.Transporta, pvd.Redespacho, ',
  'pvd.RegrFiscal, pvd.DesoAces_V, pvd.DesoAces_P, ',
  'pvd.Frete_V, pvd.Frete_P, pvd.Seguro_V, pvd.Seguro_P, ',
  'pvd.TotalQtd, pvd.Total_Vlr, pvd.Total_Des, pvd.Total_Tot, ',
  'pvd.Observa, tpc.Nome NOMETABEPRCCAD, ',
  'mda.Nome NOMEMOEDA, mda.CodUsu CODUSU_MDA, ',
  'pvd.Represen, pvd.ComisFat, pvd.ComisRec, pvd.CartEmis, ',
  'pvd.AFP_Sit, pvd.AFP_Per, ppc.MedDDSimpl, MedDDReal, ',
  'pvd.ValLiq, pvd.QuantP, frc.Nome NOMEFISREGCAD, ',
  'pvd.EntregaUsa, pvd.EntregaEnti, ',
  'imp.Nome NOMEMODELONF, imp.Codigo MODELO_NF, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP, ',
  'IF(emp.Tipo=0, emp.EUF, emp.PUF) + 0.000  EMP_UF, ',
  'IF(ven.Tipo=0, ven.RazaoSocial, ven.NOME) NOMEACC, ',
  'IF(tra.Tipo=0, tra.RazaoSocial, tra.NOME) NOMETRANSP, ',
  'IF(red.Tipo=0, red.RazaoSocial, red.NOME) NOMEREDESP, ',
  'emp.Filial, ',
  'car.Nome NOMECARTEMIS, car.Tipo TIPOCART, ',
  'ppc.Nome NOMECONDICAOPG, mot.Nome NOMEMOTIVO, ',
  'ven.CodUsu CODUSU_ACC, tra.CodUsu CODUSU_TRA, ',
  'red.CodUsu CODUSU_RED, mot.CodUsu CODUSU_MOT, ',
  'tpc.CodUsu CODUSU_TPC, ',
  'ppc.CodUsu CODUSU_PPC, ',
  'frc.CodUsu CODUSU_FRC, ',
  'cli.L_Ativo ',
  'FROM pedivda pvd ',
  'LEFT JOIN entidades  emp ON emp.Codigo=pvd.Empresa ',
  'LEFT JOIN entidades  tra ON tra.Codigo=pvd.Transporta ',
  'LEFT JOIN entidades  red ON red.Codigo=pvd.Redespacho ',
  'LEFT JOIN entidades  cli ON cli.Codigo=pvd.Cliente ',
  'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc ',
  'LEFT JOIN cambiomda  mda ON mda.Codigo=pvd.Moeda ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG ',
  'LEFT JOIN motivos    mot ON mot.Codigo=pvd.MotivoSit ',
  'LEFT JOIN pediacc    acc ON acc.Codigo=pvd.Represen ',
  'LEFT JOIN entidades  ven ON ven.Codigo=acc.Codigo ',
  'LEFT JOIN carteiras  car ON car.Codigo=pvd.CartEmis ',
  'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal ',
  'LEFT JOIN imprime    imp ON imp.Codigo=frc.ModeloNF ',
  'WHERE pvd.CodUsu=' + Geral.FF0(Pedido),
  'AND pvd.CodUsu >= 0',
  '']);
end;

procedure TFmFatPedCab2.Romaneioporgrupo1Click(Sender: TObject);
begin
{$IfNDef NAO_GPED}
  if DBCheck.CriaFm(TFmFatPedImp, FmFatPedImp, afmoNegarComAviso) then
  begin
    FmFatPedImp.FFatID          := FThisFatID;
    FmFatPedImp.FEmpresa        := QrFatPedCabEmpresa.Value;
    FmFatPedImp.FOriCodigo      := QrFatPedCabCodigo.Value;
    FmFatPedImp.FPedido         := QrPediVdaCodUsu.Value;
    FmFatPedImp.FTituloPedido   := 'Pedido Normal';
    FmFatPedImp.FCliente        := QrPediVdaCliente.Value;
    FmFatPedImp.FNomeCondicaoPg := QrPediVdaNOMECONDICAOPG.Value;
    FmFatPedImp.FTipoMov        := 1; // sempre saida
    FmFatPedImp.ShowModal;
    FmFatPedImp.Destroy;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
end;

end.

