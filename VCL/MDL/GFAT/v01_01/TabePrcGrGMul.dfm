object FmTabePrcGrGMul: TFmTabePrcGrGMul
  Left = 339
  Top = 185
  Caption = 'TAB-PRECO-004 :: Pre'#231'o por Grupo de Produtos (m'#250'ltiplo)'
  ClientHeight = 489
  ClientWidth = 589
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 589
    Height = 333
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 40
      Width = 589
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 4
        Width = 129
        Height = 13
        Caption = 'Informe parte da descri'#231#227'o:'
      end
      object Label11: TLabel
        Left = 16
        Top = 44
        Width = 287
        Height = 13
        Caption = 'Pesquisas com muitos itens podem ser demoradas!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdPesq: TEdit
        Left = 16
        Top = 20
        Width = 369
        Height = 21
        TabOrder = 0
      end
      object BtPesquisa: TBitBtn
        Tag = 22
        Left = 392
        Top = 8
        Width = 90
        Height = 40
        Caption = '&Pesquisar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtPesquisaClick
      end
    end
    object DBGrid1: TdmkDBGrid
      Left = 0
      Top = 104
      Width = 589
      Height = 212
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodUsu'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 323
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Caption = 'Pre'#231'o'
          Width = 80
          Visible = True
        end>
      Color = clWindow
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      OnColEnter = DBGrid1ColEnter
      OnColExit = DBGrid1ColExit
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'OK'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodUsu'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 323
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Caption = 'Pre'#231'o'
          Width = 80
          Visible = True
        end>
    end
    object StaticText1: TStaticText
      Left = 0
      Top = 316
      Width = 321
      Height = 17
      Align = alBottom
      Caption = 
        '  Produtos j'#225' adicionados anteriormente ter'#227'o seu pre'#231'o atualiza' +
        'do!'
      TabOrder = 2
    end
    object RGTipPrd: TRadioGroup
      Left = 0
      Top = 0
      Width = 589
      Height = 40
      Align = alTop
      Caption = 'Tipifica'#231#227'o: '
      Columns = 4
      ItemIndex = 1
      Items.Strings = (
        'Nenhum tipo'
        'Produto'
        'Mat'#233'ria-prima'
        'Uso e consumo')
      TabOrder = 3
      OnClick = RGTipPrdClick
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 589
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 541
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 493
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 473
        Height = 32
        Caption = 'Pre'#231'o por Grupo de Produtos (m'#250'ltiplo)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 473
        Height = 32
        Caption = 'Pre'#231'o por Grupo de Produtos (m'#250'ltiplo)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 473
        Height = 32
        Caption = 'Pre'#231'o por Grupo de Produtos (m'#250'ltiplo)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 381
    Width = 589
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 585
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 425
    Width = 589
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 585
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 441
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.CodUsu, gg1.Nome, tpg.Preco'
      'FROM gragru1 gg1'
      
        'LEFT JOIN tabeprcgrg tpg ON tpg.Nivel1=gg1.Nivel1 AND tpg.Codigo' +
        '=:P0'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pgt.TipPrd=:P1'
      'AND gg1.Nome LIKE :P2'
      '')
    Left = 228
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPesqNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPesqCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPesqPreco: TFloatField
      FieldName = 'Preco'
    end
  end
  object DataSource1: TDataSource
    Left = 200
    Top = 220
  end
end
