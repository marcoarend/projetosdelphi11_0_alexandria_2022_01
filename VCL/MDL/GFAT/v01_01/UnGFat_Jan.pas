unit UnGFat_Jan;

interface

uses
  Windows, Forms, SysUtils, Classes, Controls, DB, mySQLDbTables, DmkGeral,
  DmkDAC_PF, UnInternalConsts, UnDmkProcFunc, frxClass, frxDBSet, Menus,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, UnDmkEnums, UnGrl_Geral,
  UnProjGroup_Vars, UnGrl_Vars, UnProjGroup_Consts;

type
  TChamouFatPedVol = (cfpvFatPedCab, cfpvFatDivGer);
  TUnGFat_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    //
    procedure InsereEDefinePediPrzCab(Codigo: Integer; EdCondicaoPG: TdmkEditCB;
              CBCondicaoPG: TdmkDBLookupComboBox; QrPediPrzCab: TmySQLQuery);
    //
    procedure MostraFormFatDivCab1(SQLTipo: TSQLType;
              QryFatPedCab: TmySQLQuery = nil; QryPediVda: TmySQLQuery = nil;
              QryFatPedVol: TmySQLQuery = nil);
    procedure MostraFormFatDivCab2(SQLTipo: TSQLType;
              QryFatPedCab: TmySQLQuery = nil; QryPediVda: TmySQLQuery = nil;
              QryFatPedVol: TmySQLQuery = nil);
(*
    function  MostraFormFatPedVol(SQLTipo: TSQLType; QryFatPedVol: TmySQLQuery;
              DsFatPedCab: TDataSource = nil): Boolean;
*)
    function  MostraFormFatPedVol1(SQLTipo: TSQLType; QryFatPedVol: TmySQLQuery;
              DsFatPedCab: TDataSource = nil): Boolean;
    function  MostraFormFatPedVol2(SQLTipo: TSQLType; QryFatPedVol: TmySQLQuery;
              DsFatPedCab: TDataSource = nil): Boolean;
    procedure MostraFormFatDivGer(FatNum: Integer = 0);
    procedure MostraFormFatPedCab(FatNum: Integer = 0);
    procedure MostraFormFatPedImp();
    procedure MostraFormTabePrcCab(Codigo: Integer);
    procedure MostraFormFatGruFisRegCad(Cliente, Empresa, RegrFiscal, CliUF,
              EmpUF, GraGru1, MadeBy, OriCodi, OriCnta: Integer; CliIE,
              Tmp_FatPedFis: String);
    procedure MostraFormPediVda();
    procedure MostraFormPediVdaImp();
    procedure MostraFormPediPrzCab1(Codigo: Integer);
    procedure MostraFormFisRegEnPsq(Entidade: Integer; Ed: TdmkEditCB;
              CB: TdmkDBLookupComboBox);
    procedure MostraFormFatDivItemAlt(IDCtrl, GraGruX: Integer; NO_NIVEL1,
              NO_COR, NO_TAM: String; vProd, vFrete, vSeg, vDesc, vOutro:
              Double; obsCont_xCampo, obsCont_xTexto, obsFisco_xCampo,
              obsFisco_xTexto: String; Tipo, OriCodi, OriCtrl, Empresa: Integer);
    procedure MostraFormSoliComprCab(Codigo: Integer);

  end;

var
  GFat_Jan: TUnGFat_Jan;


implementation

uses
  {$IfNDef NAO_GPED} FatPedImp2, FatPedCab1, FatPedCab2, TabePrcCab, {$EndIf}
  {$IfNDef NAO_GFAT} FatPedVol1, FatPedVol2, FatDivGer1, FatDivGer2, FatDivCab1, FatDivCab2, FatGruFisRegCad, UnPraz_PF, FisRegEnPsq, {$EndIf}
  // ini 2021-04-23
  //{$IfNDef SemPediVda} PediVda1, PediVda2, PediVdaImp, PediVdaImp2, ModPediVda, PediPrzCab1, {$Else}{$IfNDef NAO_GFAT}ModPediVda,{$Endif}{$Endif}
  PediVda1, PediVda2, PediVdaImp, PediVdaImp2, ModPediVda, PediPrzCab1,
  // fim 2021-04-23
  // ini 2021-10-20
  FatDivItemAlt,
  // fim 2021-10-20
  SoliComprCab,
  MyDBCheck, ModuleGeral, ModProd, UnMyObjects, UMySQLModule,
  MyListas;

{ TUnGFat_Jan }

procedure TUnGFat_Jan.InsereEDefinePediPrzCab(Codigo: Integer;
  EdCondicaoPG: TdmkEditCB; CBCondicaoPG: TdmkDBLookupComboBox; QrPediPrzCab:
  TmySQLQuery);
begin
{$IfNDef NAO_GFAT}
  VAR_CADASTRO := 0;

  Praz_PF.MostraFormPediPrzCab1(EdCondicaoPG.ValueVariant);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCondicaoPG, CBCondicaoPG, QrPediPrzCab,
      VAR_CADASTRO, 'Codigo');
    EdCondicaoPG.SetFocus;
  end;
  {$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
  {$EndIf}
end;

procedure TUnGFat_Jan.MostraFormFatDivCab1(SQLTipo: TSQLType;
  QryFatPedCab: TmySQLQuery = nil; QryPediVda: TmySQLQuery = nil;
  QryFatPedVol: TmySQLQuery = nil);
{$IfNDef NAO_GFAT}
  procedure ReopenTabelas();
  begin
    Screen.Cursor := crHourGlass;
    try
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      DmPediVda.ReopenTabelasPedido();
    finally
      Screen.Cursor := crDefault;
    end;
  end;
var
  Habilita0, Habilita1, Habilita2: Boolean;
begin
  if SQLTipo = stIns then
  begin
    ReopenTabelas;
    //
    if DBCheck.CriaFm(TFmFatDivCab1, FmFatDivCab1, afmoNegarComAviso) then
    begin
      FmFatDivCab1.ImgTipo.SQLType := stIns;
      FmFatDivCab1.EdEmpresa.ValueVariant := DmodG.QrFiliLogFilial.Value;
      FmFatDivCab1.CBEmpresa.KeyValue     := DmodG.QrFiliLogFilial.Value;
      FmFatDivCab1.EdindPres.ValueVariant := 9;
      //
      FmFatDivCab1.ShowModal;
      FmFatDivCab1.Destroy;
    end;
  end else
  begin
    ReopenTabelas;
    //
    DmodG.ReopenParamsEmp(QryFatPedCab.FieldByName('Empresa').AsInteger);
    //
    if DBCheck.CriaFm(TFmFatDivCab1, FmFatDivCab1, afmoNegarComAviso) then
    begin
      FmFatDivCab1.ImgTipo.SQLType := stUpd;
      //
      FmFatDivCab1.VuFisRegCad.ValueVariant  := QryFatPedCab.FieldByName('RegrFiscal').AsInteger;
      FmFatDivCab1.VuMotivoSit.ValueVariant  := QryPediVda.FieldByName('MotivoSit').AsInteger;
      FmFatDivCab1.VuTabelaPrc.ValueVariant  := QryFatPedCab.FieldByName('TabelaPrc').AsInteger;
      FmFatDivCab1.VuCambioMda.ValueVariant  := QryPediVda.FieldByName('Moeda').AsInteger;
      FmFatDivCab1.VuCondicaoPG.ValueVariant := QryFatPedCab.FieldByName('CondicaoPG').AsInteger;
      FmFatDivCab1.VuEmpresa.ValueVariant    := QryFatPedCab.FieldByName('Empresa').AsInteger;

      FmFatDivCab1.EdCliente.ValueVariant    := QryFatPedCab.FieldByName('Cliente').AsInteger;
      FmFatDivCab1.CBCliente.KeyValue        := QryFatPedCab.FieldByName('Cliente').AsInteger;
      FmFatDivCab1.EdSituacao.ValueVariant   := QryPediVda.FieldByName('Situacao').AsInteger;
      FmFatDivCab1.CBSituacao.KeyValue       := QryPediVda.FieldByName('Situacao').AsInteger;
      FmFatDivCab1.EdCartEmis.ValueVariant   := QryPediVda.FieldByName('CartEmis').AsInteger;
      FmFatDivCab1.CBCartEmis.KeyValue       := QryPediVda.FieldByName('CartEmis').AsInteger;
      FmFatDivCab1.EdFretePor.ValueVariant   := QryPediVda.FieldByName('FretePor').AsInteger;
      FmFatDivCab1.CBFretePor.KeyValue       := QryPediVda.FieldByName('FretePor').AsInteger;
      FmFatDivCab1.EdTransporta.ValueVariant := QryPediVda.FieldByName('Transporta').AsInteger;
      FmFatDivCab1.CBTransporta.KeyValue     := QryPediVda.FieldByName('Transporta').AsInteger;
      FmFatDivCab1.EdRedespacho.ValueVariant := QryPediVda.FieldByName('Redespacho').AsInteger;
      FmFatDivCab1.CBRedespacho.KeyValue     := QryPediVda.FieldByName('Redespacho').AsInteger;

      FmFatDivCab1.TPDtaInclu.Date           := QryPediVda.FieldByName('DtaInclu').AsDateTime;
      FmFatDivCab1.TPDtaEmiss.Date           := QryPediVda.FieldByName('DtaEmiss').AsDateTime;
      FmFatDivCab1.TPDtaEntra.Date           := QryPediVda.FieldByName('DtaEntra').AsDateTime;
      FmFatDivCab1.TPDtaPrevi.Date           := QryPediVda.FieldByName('DtaPrevi').AsDateTime;

      FmFatDivCab1.EdCodigo.ValueVariant     := QryFatPedCab.FieldByName('Codigo').AsInteger;
      FmFatDivCab1.EdCodUsu_Fat.ValueVariant := QryFatPedCab.FieldByName('CodUsu').AsInteger;
      FmFatDivCab1.EdCodUsu_Ped.ValueVariant := QryPediVda.FieldByName('CodUsu').AsInteger;
      FmFatDivCab1.EdCodigo_Ped.ValueVariant := QryPediVda.FieldByName('Codigo').AsInteger;
      FmFatDivCab1.EdPedidoCli.ValueVariant  := QryPediVda.FieldByName('PedidoCli').AsString;
      FmFatDivCab1.EdPrioridade.ValueVariant := QryPediVda.FieldByName('Prioridade').AsInteger;

      FmFatDivCab1.EdRepresen.ValueVariant   := QryPediVda.FieldByName('Represen').AsInteger;
      FmFatDivCab1.CBRepresen.KeyValue       := QryPediVda.FieldByName('Represen').AsInteger;
      FmFatDivCab1.EdComisFat.ValueVariant   := QryPediVda.FieldByName('ComisFat').AsFloat;
      FmFatDivCab1.EdComisRec.ValueVariant   := QryPediVda.FieldByName('ComisRec').AsFloat;
      FmFatDivCab1.EdDesoAces_P.ValueVariant := QryPediVda.FieldByName('DesoAces_P').AsFloat;
      FmFatDivCab1.EdDesoAces_V.ValueVariant := QryPediVda.FieldByName('DesoAces_V').AsFloat;
      FmFatDivCab1.EdFrete_P.ValueVariant    := QryPediVda.FieldByName('Frete_P').AsFloat;
      FmFatDivCab1.EdFrete_V.ValueVariant    := QryPediVda.FieldByName('Frete_V').AsFloat;
      FmFatDivCab1.EdSeguro_P.ValueVariant   := QryPediVda.FieldByName('Seguro_P').AsFloat;
      FmFatDivCab1.EdSeguro_V.ValueVariant   := QryPediVda.FieldByName('Seguro_V').AsFloat;
      //
      FmFatDivCab1.CkUsaReferen.Checked      := QryPediVda.FieldByName('UsaReferen').AsInteger = 1;
      //
      FmFatDivCab1.RG_idDest.ItemIndex       := QryPediVda.FieldByName('idDest').AsInteger;
      FmFatDivCab1.RG_indFinal.ItemIndex     := QryPediVda.FieldByName('indFinal').AsInteger;
      FmFatDivCab1.EdindPres.ValueVariant    := QryPediVda.FieldByName('indPres').AsInteger;
      FmFatDivCab1.Edide_finNFe.ValueVariant := QryPediVda.FieldByName('finNFe').AsInteger;
      //
      Habilita0 := not (QryFatPedVol.RecordCount > 0);
      Habilita1 := False;
      //
      if not Habilita0 then
      begin
        case DModG.QrParamsEmpPedVdaMudPrazo.Value of
          0: Habilita1 := False;
          1: Habilita1 := VAR_USUARIO < 0;
          2: Habilita1 := True;
        end;
      end else
        Habilita1 := True;
      //
      Habilita2 := Habilita1 or (QryPediVda.FieldByName('CondicaoPG').AsInteger = 0);
      //
      FmFatDivCab1.LaCondicaoPG.Enabled := Habilita2;
      FmFatDivCab1.EdCondicaoPG.Enabled := Habilita2;
      FmFatDivCab1.CBCondicaoPG.Enabled := Habilita2;
      FmFatDivCab1.BtCondicaoPG.Enabled := Habilita2;
      //
      if not Habilita0 then
      begin
        case DModG.QrParamsEmpPedVdaMudLista.Value of
          0: Habilita1 := False;
          1: Habilita1 := VAR_USUARIO < 0;
          2: Habilita1 := True;
        end;
      end else
        Habilita1 := True;
      //
      Habilita2 := Habilita1 or (QryPediVda.FieldByName('TabelaPrc').AsInteger = 0);
      //
      FmFatDivCab1.LaTabelaPrc.Enabled := Habilita2;
      FmFatDivCab1.EdTabelaPrc.Enabled := Habilita2;
      FmFatDivCab1.CBTabelaPrc.Enabled := Habilita2;
      FmFatDivCab1.BtTabelaPrc.Enabled := Habilita2;
      //
      FmFatDivCab1.ShowModal;
      FmFatDivCab1.Destroy;
    end;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
end;

procedure TUnGFat_Jan.MostraFormFatDivCab2(SQLTipo: TSQLType; QryFatPedCab,
  QryPediVda, QryFatPedVol: TmySQLQuery);
{$IfNDef NAO_GFAT}
  procedure ReopenTabelas();
  begin
    Screen.Cursor := crHourGlass;
    try
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      DmPediVda.ReopenTabelasPedido();
    finally
      Screen.Cursor := crDefault;
    end;
  end;
var
  Habilita0, Habilita1, Habilita2: Boolean;
begin
  if SQLTipo = stIns then
  begin
    ReopenTabelas;
    //
    if DBCheck.CriaFm(TFmFatDivCab2, FmFatDivCab2, afmoNegarComAviso) then
    begin
      FmFatDivCab2.ImgTipo.SQLType := stIns;
      FmFatDivCab2.EdEmpresa.ValueVariant := DmodG.QrFiliLogFilial.Value;
      FmFatDivCab2.CBEmpresa.KeyValue     := DmodG.QrFiliLogFilial.Value;
      FmFatDivCab2.EdindPres.ValueVariant := 9;
      //
      FmFatDivCab2.ShowModal;
      FMFatDivCab2.Destroy;
    end;
  end else
  begin
    ReopenTabelas;
    //
    DmodG.ReopenParamsEmp(QryFatPedCab.FieldByName('Empresa').AsInteger);
    //
    if DBCheck.CriaFm(TFmFatDivCab2, FmFatDivCab2, afmoNegarComAviso) then
    begin
      FmFatDivCab2.ImgTipo.SQLType := stUpd;
      //
      FmFatDivCab2.VuFisRegCad.ValueVariant  := QryFatPedCab.FieldByName('RegrFiscal').AsInteger;
      FmFatDivCab2.VuMotivoSit.ValueVariant  := QryPediVda.FieldByName('MotivoSit').AsInteger;
      FmFatDivCab2.VuTabelaPrc.ValueVariant  := QryFatPedCab.FieldByName('TabelaPrc').AsInteger;
      FmFatDivCab2.VuCambioMda.ValueVariant  := QryPediVda.FieldByName('Moeda').AsInteger;
      FmFatDivCab2.VuCondicaoPG.ValueVariant := QryFatPedCab.FieldByName('CondicaoPG').AsInteger;
      FmFatDivCab2.VuEmpresa.ValueVariant    := QryFatPedCab.FieldByName('Empresa').AsInteger;

      FmFatDivCab2.EdCliente.ValueVariant    := QryFatPedCab.FieldByName('Cliente').AsInteger;
      FmFatDivCab2.CBCliente.KeyValue        := QryFatPedCab.FieldByName('Cliente').AsInteger;
      FmFatDivCab2.EdSituacao.ValueVariant   := QryPediVda.FieldByName('Situacao').AsInteger;
      FmFatDivCab2.CBSituacao.KeyValue       := QryPediVda.FieldByName('Situacao').AsInteger;
      FmFatDivCab2.EdCartEmis.ValueVariant   := QryPediVda.FieldByName('CartEmis').AsInteger;
      FmFatDivCab2.CBCartEmis.KeyValue       := QryPediVda.FieldByName('CartEmis').AsInteger;
      FmFatDivCab2.EdFretePor.ValueVariant   := QryPediVda.FieldByName('FretePor').AsInteger;
      FmFatDivCab2.CBFretePor.KeyValue       := QryPediVda.FieldByName('FretePor').AsInteger;
      FmFatDivCab2.EdTransporta.ValueVariant := QryPediVda.FieldByName('Transporta').AsInteger;
      FmFatDivCab2.CBTransporta.KeyValue     := QryPediVda.FieldByName('Transporta').AsInteger;
      FmFatDivCab2.EdRedespacho.ValueVariant := QryPediVda.FieldByName('Redespacho').AsInteger;
      FmFatDivCab2.CBRedespacho.KeyValue     := QryPediVda.FieldByName('Redespacho').AsInteger;

      FmFatDivCab2.TPDtaInclu.Date           := QryPediVda.FieldByName('DtaInclu').AsDateTime;
      FmFatDivCab2.TPDtaEmiss.Date           := QryPediVda.FieldByName('DtaEmiss').AsDateTime;
      FmFatDivCab2.TPDtaEntra.Date           := QryPediVda.FieldByName('DtaEntra').AsDateTime;
      FmFatDivCab2.TPDtaPrevi.Date           := QryPediVda.FieldByName('DtaPrevi').AsDateTime;

      FmFatDivCab2.EdCodigo.ValueVariant     := QryFatPedCab.FieldByName('Codigo').AsInteger;
      FmFatDivCab2.EdCodUsu_Fat.ValueVariant := QryFatPedCab.FieldByName('CodUsu').AsInteger;
      FmFatDivCab2.EdCodUsu_Ped.ValueVariant := QryPediVda.FieldByName('CodUsu').AsInteger;
      FmFatDivCab2.EdCodigo_Ped.ValueVariant := QryPediVda.FieldByName('Codigo').AsInteger;
      FmFatDivCab2.EdPedidoCli.ValueVariant  := QryPediVda.FieldByName('PedidoCli').AsString;
      FmFatDivCab2.EdPrioridade.ValueVariant := QryPediVda.FieldByName('Prioridade').AsInteger;

      FmFatDivCab2.EdRepresen.ValueVariant   := QryPediVda.FieldByName('Represen').AsInteger;
      FmFatDivCab2.CBRepresen.KeyValue       := QryPediVda.FieldByName('Represen').AsInteger;
      FmFatDivCab2.EdComisFat.ValueVariant   := QryPediVda.FieldByName('ComisFat').AsFloat;
      FmFatDivCab2.EdComisRec.ValueVariant   := QryPediVda.FieldByName('ComisRec').AsFloat;
      FmFatDivCab2.EdDesoAces_P.ValueVariant := QryPediVda.FieldByName('DesoAces_P').AsFloat;
      FmFatDivCab2.EdDesoAces_V.ValueVariant := QryPediVda.FieldByName('DesoAces_V').AsFloat;
      FmFatDivCab2.EdFrete_P.ValueVariant    := QryPediVda.FieldByName('Frete_P').AsFloat;
      FmFatDivCab2.EdFrete_V.ValueVariant    := QryPediVda.FieldByName('Frete_V').AsFloat;
      FmFatDivCab2.EdSeguro_P.ValueVariant   := QryPediVda.FieldByName('Seguro_P').AsFloat;
      FmFatDivCab2.EdSeguro_V.ValueVariant   := QryPediVda.FieldByName('Seguro_V').AsFloat;
      //
      FmFatDivCab2.CkUsaReferen.Checked      := QryPediVda.FieldByName('UsaReferen').AsInteger = 1;
      //
      FmFatDivCab2.RG_idDest.ItemIndex       := QryPediVda.FieldByName('idDest').AsInteger;
      FmFatDivCab2.RG_indFinal.ItemIndex     := QryPediVda.FieldByName('indFinal').AsInteger;
      FmFatDivCab2.EdindPres.ValueVariant    := QryPediVda.FieldByName('indPres').AsInteger;
      FmFatDivCab2.Edide_finNFe.ValueVariant := QryPediVda.FieldByName('finNFe').AsInteger;
      //
      FmFatDivCab2.CkRetiradaUsa.Checked       := QryPediVda.FieldByName('RetiradaUsa').AsInteger > 0;
      FmFatDivCab2.EdRetiradaEnti.ValueVariant := QryPediVda.FieldByName('RetiradaEnti').AsInteger;
      FmFatDivCab2.CBRetiradaEnti.KeyValue     := QryPediVda.FieldByName('RetiradaEnti').AsInteger;
      //
      FmFatDivCab2.CkEntregaUsa.Checked       := QryPediVda.FieldByName('EntregaUsa').AsInteger > 0;
      FmFatDivCab2.EdEntregaEnti.ValueVariant := QryPediVda.FieldByName('EntregaEnti').AsInteger;
      FmFatDivCab2.CBEntregaEnti.KeyValue     := QryPediVda.FieldByName('EntregaEnti').AsInteger;
      //
      Habilita0 := not (QryFatPedVol.RecordCount > 0);
      Habilita1 := False;
      //
      if not Habilita0 then
      begin
        case DModG.QrParamsEmpPedVdaMudPrazo.Value of
          0: Habilita1 := False;
          1: Habilita1 := VAR_USUARIO < 0;
          2: Habilita1 := True;
        end;
      end else
        Habilita1 := True;
      //
      Habilita2 := Habilita1 or (QryPediVda.FieldByName('CondicaoPG').AsInteger = 0);
      //
      FmFatDivCab2.LaCondicaoPG.Enabled := Habilita2;
      FmFatDivCab2.EdCondicaoPG.Enabled := Habilita2;
      FmFatDivCab2.CBCondicaoPG.Enabled := Habilita2;
      FmFatDivCab2.BtCondicaoPG.Enabled := Habilita2;
      //
      if not Habilita0 then
      begin
        case DModG.QrParamsEmpPedVdaMudLista.Value of
          0: Habilita1 := False;
          1: Habilita1 := VAR_USUARIO < 0;
          2: Habilita1 := True;
        end;
      end else
        Habilita1 := True;
      //
      Habilita2 := Habilita1 or (QryPediVda.FieldByName('TabelaPrc').AsInteger = 0);
      //
      FmFatDivCab2.LaTabelaPrc.Enabled := Habilita2;
      FmFatDivCab2.EdTabelaPrc.Enabled := Habilita2;
      FmFatDivCab2.CBTabelaPrc.Enabled := Habilita2;
      FmFatDivCab2.BtTabelaPrc.Enabled := Habilita2;
      //
      //
      FmFatDivCab2.ShowModal;
      FmFatDivCab2.Destroy;
    end;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
end;

procedure TUnGFat_Jan.MostraFormFatDivGer(FatNum: Integer = 0);
begin
  {$IfNDef NAO_GFAT}
  if VAR_NT2018_05v120 then
  begin
    if DBCheck.CriaFm(TFmFatDivGer2, FmFatDivGer2, afmoNegarComAviso) then
    begin
      if CO_DMKID_APP = 2 then //Bluederm
        FmFatDivGer2.FMultiGrandeza := True;
      //
      if FatNum <> 0 then
        FmFatDivGer2.LocCod(FatNum, FatNum);
      FmFatDivGer2.ShowModal;
      FmFatDivGer2.Destroy;
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmFatDivGer1, FmFatDivGer1, afmoNegarComAviso) then
    begin
      if CO_DMKID_APP = 2 then //Bluederm
        FmFatDivGer1.FMultiGrandeza := True;
      //
      if FatNum <> 0 then
        FmFatDivGer1.LocCod(FatNum, FatNum);
      FmFatDivGer1.ShowModal;
      FmFatDivGer1.Destroy;
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TUnGFat_Jan.MostraFormFatDivItemAlt(IDCtrl, GraGruX: Integer;
  NO_NIVEL1, NO_COR, NO_TAM: String; vProd, vFrete, vSeg, vDesc, vOutro: Double;
  obsCont_xCampo, obsCont_xTexto, obsFisco_xCampo, obsFisco_xTexto: String;
  Tipo, OriCodi, OriCtrl, Empresa: Integer);
begin
  {$IfNDef NAO_GPED}
  if DBCheck.CriaFm(TFmFatDivItemAlt, FmFatDivItemAlt, afmoNegarComAviso) then
  begin
    FmFatDivItemAlt.EdIDCtrl.ValueVariant    := IDCtrl;
    FmFatDivItemAlt.EdTipo.ValueVariant      := Tipo;
    FmFatDivItemAlt.EdOriCodi.ValueVariant   := OriCodi;
    FmFatDivItemAlt.EdOriCtrl.ValueVariant   := OriCtrl;
    FmFatDivItemAlt.EdEmpresa.ValueVariant   := Empresa;
    FmFatDivItemAlt.EdGraGruX.ValueVariant   := GraGruX;
    FmFatDivItemAlt.EdNO_NIVEL1.ValueVariant := NO_NIVEL1;
    FmFatDivItemAlt.EdNO_COR.ValueVariant    := NO_COR;
    FmFatDivItemAlt.EdNO_TAM.ValueVariant    := NO_TAM;
    FmFatDivItemAlt.EdvProd.ValueVariant     := vProd;
    FmFatDivItemAlt.EdvFrete.ValueVariant    := vFrete;
    FmFatDivItemAlt.EdvSeg.ValueVariant      := vSeg;
    FmFatDivItemAlt.EdvDesc.ValueVariant     := vDesc;
    FmFatDivItemAlt.EdvOutro.ValueVariant    := vOutro;
    //FmFatDivItemAlt.EdvBC.ValueVariant       := vBC;
    FmFatDivItemAlt.EdobsCont_xCampo.ValueVariant  := obsCont_xCampo ;
    FmFatDivItemAlt.EdobsCont_xTexto.ValueVariant  := obsCont_xTexto ;
    FmFatDivItemAlt.EdobsFisco_xCampo.ValueVariant := obsFisco_xCampo;
    FmFatDivItemAlt.EdobsFisco_xTexto.ValueVariant := obsFisco_xTexto;
    //
    FmFatDivItemAlt.ShowModal;
    FmFatDivItemAlt.Destroy;
  end;
  {$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
  {$EndIf}
end;

procedure TUnGFat_Jan.MostraFormFatGruFisRegCad(Cliente, Empresa, RegrFiscal,
  CliUF, EmpUF, GraGru1, MadeBy, OriCodi, OriCnta: Integer; CliIE,
  Tmp_FatPedFis: String);
begin
  {$IfNDef NAO_GPED}
  if DBCheck.CriaFm(TFmFatGruFisRegCad, FmFatGruFisRegCad, afmoNegarComAviso) then
  begin
    FmFatGruFisRegCad.FCliente       := Cliente;
    FmFatGruFisRegCad.FEmpresa       := Empresa;
    FmFatGruFisRegCad.FRegrFiscal    := RegrFiscal;
    FmFatGruFisRegCad.FCliIE         := CliIE;
    FmFatGruFisRegCad.FEmpUF         := EmpUF;
    FmFatGruFisRegCad.FCliUF         := CliUF;
    FmFatGruFisRegCad.FGraGru1       := GraGru1;
    FmFatGruFisRegCad.FOriCodi       := OriCodi;
    FmFatGruFisRegCad.FOriCnta       := OriCnta;
    FmFatGruFisRegCad.FMadeBy        := MadeBy;
    FmFatGruFisRegCad.FTmp_FatPedFis := Tmp_FatPedFis;
    FmFatGruFisRegCad.ShowModal;
    FmFatGruFisRegCad.Destroy;
  end;
  {$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
  {$EndIf}
end;

procedure TUnGFat_Jan.MostraFormFatPedCab(FatNum: Integer = 0);
begin
  {$IfNDef NAO_GPED}
  if VAR_NT2018_05v120 then
  begin
    if DBCheck.CriaFm(TFmFatPedCab2, FmFatPedCab2, afmoNegarComAviso) then
    begin
      if CO_DMKID_APP = 2 then //Bluederm
        FmFatPedCab2.FMultiGrandeza := True;
      //
      if FatNum <> 0 then
        FmFatPedCab2.LocCod(FatNum, FatNum);
      FmFatPedCab2.ShowModal;
      FmFatPedCab2.Destroy;
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmFatPedCab1, FmFatPedCab1, afmoNegarComAviso) then
    begin
      if CO_DMKID_APP = 2 then //Bluederm
        FmFatPedCab1.FMultiGrandeza := True;
      //
      if FatNum <> 0 then
        FmFatPedCab1.LocCod(FatNum, FatNum);
      FmFatPedCab1.ShowModal;
      FmFatPedCab1.Destroy;
    end;
  end;
  {$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
  {$EndIf}
end;

procedure TUnGFat_Jan.MostraFormFatPedImp;
begin
  {$IfNDef NAO_GPED}
  if DBCheck.CriaFm(TFmFatPedImp2, FmFatPedImp2, afmoNegarComAviso) then
  begin
    FmFatPedImp2.ShowModal;
    FmFatPedImp2.Destroy;
  end;
  {$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
  {$EndIf}
end;

(*
function TUnGFat_Jan.MostraFormFatPedVol(SQLTipo: TSQLType;
  QryFatPedVol: TmySQLQuery; DsFatPedCab: TDataSource = nil): Boolean;
var
  Continua: Boolean;
begin
  Continua := False;
  //
  if SQLTipo = stIns then
  begin
    if UMyMod.FormInsUpd_Cria(TFmFatPedVol, FmFatPedVol, afmoNegarComAviso,
      QryFatPedVol, stIns) then
    begin
      FmFatPedVol.FChamouFatPedVol := cfpvFatDivGer;
      FmFatPedVol.ShowModal;
      //
      Continua := FmFatPedVol.FIncluiuVol;
      //
      FmFatPedVol.Destroy;
    end;
  end else
  begin
    if UMyMod.FormInsUpd_Cria(TFmFatPedVol, FmFatPedVol, afmoNegarComAviso,
      QryFatPedVol, stUpd) then
    begin
      FmFatPedVol.FChamouFatPedVol      := cfpvFatDivGer;
      FmFatPedVol.DBEdCodigo.DataSource := DsFatPedCab;
      FmFatPedVol.DBEdit1.DataSource    := DsFatPedCab;
      FmFatPedVol.DBEdit2.DataSource    := DsFatPedCab;
      FmFatPedVol.ShowModal;
      FmFatPedVol.Destroy;
    end;
  end;
  Result := Continua;
end;
*)

function TUnGFat_Jan.MostraFormFatPedVol1(SQLTipo: TSQLType;
  QryFatPedVol: TmySQLQuery; DsFatPedCab: TDataSource): Boolean;
var
  Continua: Boolean;
begin
  {$IfNDef NAO_GPED}
  Continua := False;
  //
  if SQLTipo = stIns then
  begin
    if UMyMod.FormInsUpd_Cria(TFmFatPedVol1, FmFatPedVol1, afmoNegarComAviso,
      QryFatPedVol, stIns) then
    begin
      FmFatPedVol1.FChamouFatPedVol := cfpvFatDivGer;
      FmFatPedVol1.ShowModal;
      //
      Continua := FmFatPedVol1.FIncluiuVol;
      //
      FmFatPedVol1.Destroy;
    end;
  end else
  begin
    if UMyMod.FormInsUpd_Cria(TFmFatPedVol1, FmFatPedVol1, afmoNegarComAviso,
      QryFatPedVol, stUpd) then
    begin
      FmFatPedVol1.FChamouFatPedVol      := cfpvFatDivGer;
      FmFatPedVol1.DBEdCodigo.DataSource := DsFatPedCab;
      FmFatPedVol1.DBEdit1.DataSource    := DsFatPedCab;
      FmFatPedVol1.DBEdit2.DataSource    := DsFatPedCab;
      FmFatPedVol1.ShowModal;
      FmFatPedVol1.Destroy;
    end;
  end;
  Result := Continua;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
end;

function TUnGFat_Jan.MostraFormFatPedVol2(SQLTipo: TSQLType;
  QryFatPedVol: TmySQLQuery; DsFatPedCab: TDataSource): Boolean;
var
  Continua: Boolean;
begin
  {$IfNDef NAO_GPED}
  Continua := False;
  //
  if SQLTipo = stIns then
  begin
    if UMyMod.FormInsUpd_Cria(TFmFatPedVol2, FmFatPedVol2, afmoNegarComAviso,
      QryFatPedVol, stIns) then
    begin
      FmFatPedVol2.FChamouFatPedVol := cfpvFatDivGer;
      FmFatPedVol2.ShowModal;
      //
      Continua := FmFatPedVol2.FIncluiuVol;
      //
      FmFatPedVol2.Destroy;
    end;
  end else
  begin
    if UMyMod.FormInsUpd_Cria(TFmFatPedVol2, FmFatPedVol2, afmoNegarComAviso,
      QryFatPedVol, stUpd) then
    begin
      FmFatPedVol2.FChamouFatPedVol      := cfpvFatDivGer;
      FmFatPedVol2.DBEdCodigo.DataSource := DsFatPedCab;
      FmFatPedVol2.DBEdit1.DataSource    := DsFatPedCab;
      FmFatPedVol2.DBEdit2.DataSource    := DsFatPedCab;
      FmFatPedVol2.ShowModal;
      FmFatPedVol2.Destroy;
    end;
  end;
  Result := Continua;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
end;

procedure TUnGFat_Jan.MostraFormFisRegEnPsq(Entidade: Integer; Ed: TdmkEditCB;
  CB: TdmkDBLookupComboBox);
begin
  if DBCheck.CriaFm(TFmFisRegEnPsq, FmFisRegEnPsq, afmoNegarComAviso) then
  begin
    FmFisRegEnPsq.ReopenPesq();
    FmFisRegEnPsq.ShowModal;
    if FmFisRegEnPsq.FSelecionou then
    begin
      Ed.ValueVariant := FmFisRegEnPsq.FFisRegEnt;
      CB.KeyValue     := FmFisRegEnPsq.FFisRegEnt;
    end;
    FmFisRegEnPsq.Destroy;
  end;
end;

procedure TUnGFat_Jan.MostraFormTabePrcCab(Codigo: Integer);
begin
  {$IfNDef NAO_GPED}
  if DBCheck.CriaFm(TFmTabePrcCab, FmTabePrcCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmTabePrcCab.LocCod(Codigo, Codigo);
    FmTabePrcCab.ShowModal;
    FmTabePrcCab.Destroy;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
end;

procedure TUnGFat_Jan.MostraFormPediPrzCab1(Codigo: Integer);
begin
  {$IfNDef NAO_GPED}
  begin
    if DBCheck.CriaFm(TFmPediPrzCab1, FmPediPrzCab1, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmPediPrzCab1.LocCod(Codigo, Codigo);
      FmPediPrzCab1.ShowModal;
      FmPediPrzCab1.Destroy;
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
end;

procedure TUnGFat_Jan.MostraFormPediVda();
begin
  {$IfNDef NAO_GPED}
  Screen.Cursor := crHourGlass;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  DmPediVda.ReopenTabelasPedido();
  Screen.Cursor := crDefault;
  //
  if VAR_NT2018_05v120 then
  begin
    if DBCheck.CriaFm(TFmPediVda2, FmPediVda2, afmoNegarComAviso) then
    begin
      FmPediVda2.ShowModal;
      FmPediVda2.Destroy;
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmPediVda1, FmPediVda1, afmoNegarComAviso) then
    begin
      FmPediVda1.ShowModal;
      FmPediVda1.Destroy;
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
end;

procedure TUnGFat_Jan.MostraFormPediVdaImp();
var
  Ordem: Integer;
  Ord_Txt: String;
begin
  {$IfNDef NAO_GPED}
  Ordem := MyObjects.SelRadioGroup('Janela de Impress�o',
  'Selecione a janela de Impress�o', [
  'Antiga',
  'Nova'], 1);
  if Ordem > -1 then
  begin
    Screen.Cursor := crHourGlass;
    try
      DmodG.ReopenEmpresas(VAR_USUARIO, 0);
      // N�o precisa todas, Mudar?
      DmPediVda.ReopenTabelasPedido();
    finally
      Screen.Cursor := crDefault;
    end;
    //
    case Ordem of
      0:
      begin
        if DBCheck.CriaFm(TFmPediVdaImp, FmPediVdaImp, afmoNegarComAviso) then
        begin
          FmPediVdaImp.ShowModal;
          FmPediVdaImp.Destroy;
        end;
      end;
      1:
      begin
        if DBCheck.CriaFm(TFmPediVdaImp2, FmPediVdaImp2, afmoNegarComAviso) then
        begin
          FmPediVdaImp2.ShowModal;
          FmPediVdaImp2.Destroy;
        end;
      end;
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
end;

procedure TUnGFat_Jan.MostraFormSoliComprCab(Codigo: Integer);
begin
  {$IfNDef NAO_GPED}
  Screen.Cursor := crHourGlass;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  DmPediVda.ReopenTabelasPedido();
  Screen.Cursor := crDefault;
  //
  if DBCheck.CriaFm(TFmSoliComprCab, FmSoliComprCab, afmoNegarComAviso) then
  begin
    FmSoliComprCab.ShowModal;
    FmSoliComprCab.Destroy;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
end;

end.
