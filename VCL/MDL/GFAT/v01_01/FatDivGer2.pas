unit FatDivGer2;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, frxClass, frxDBSet, UnDmkProcFunc,
  DmkDAC_PF, dmkImage, UnDmkEnums, UnGrl_Geral, Vcl.ComCtrls, UnProjGroup_Consts;

type
  TFmFatDivGer2 = class(TForm)
    PainelDados: TPanel;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    PMFatura: TPopupMenu;
    Panel4: TPanel;
    Alterafaturamentoatual1: TMenuItem;
    Excluifaturamentoatual1: TMenuItem;
    DsFatPedCab: TDataSource;
    QrFatPedIts: TmySQLQuery;
    DsFatPedIts: TDataSource;
    DBGFatPedVol: TDBGrid;
    DsEntregaCli: TDataSource;
    QrEntregaCli: TmySQLQuery;
    QrEntregaCliE_ALL: TWideStringField;
    QrEntregaCliCNPJ_TXT: TWideStringField;
    QrEntregaCliNOME_TIPO_DOC: TWideStringField;
    QrEntregaCliTE1_TXT: TWideStringField;
    QrEntregaCliNUMERO_TXT: TWideStringField;
    QrEntregaCliCEP_TXT: TWideStringField;
    DsCli: TDataSource;
    QrCli: TmySQLQuery;
    QrCliE_ALL: TWideStringField;
    QrCliCNPJ_TXT: TWideStringField;
    QrCliNOME_TIPO_DOC: TWideStringField;
    QrCliTE1_TXT: TWideStringField;
    QrCliFAX_TXT: TWideStringField;
    QrCliNUMERO_TXT: TWideStringField;
    QrCliCEP_TXT: TWideStringField;
    QrCliCodigo: TIntegerField;
    QrCliTipo: TSmallintField;
    QrCliCodUsu: TIntegerField;
    QrCliNOME_ENT: TWideStringField;
    QrCliCNPJ_CPF: TWideStringField;
    QrCliIE_RG: TWideStringField;
    QrCliRUA: TWideStringField;
    QrCliCOMPL: TWideStringField;
    QrCliBAIRRO: TWideStringField;
    QrCliCIDADE: TWideStringField;
    QrCliNOMELOGRAD: TWideStringField;
    QrCliNOMEUF: TWideStringField;
    QrCliPais: TWideStringField;
    QrCliENDEREF: TWideStringField;
    QrCliTE1: TWideStringField;
    QrCliFAX: TWideStringField;
    DsPediVda: TDataSource;
    QrPediVda: TmySQLQuery;
    QrPediVdaNOMEFRETEPOR: TWideStringField;
    QrPediVdaCodigo: TIntegerField;
    QrPediVdaCodUsu: TIntegerField;
    QrPediVdaEmpresa: TIntegerField;
    QrPediVdaCliente: TIntegerField;
    QrPediVdaDtaEmiss: TDateField;
    QrPediVdaDtaEntra: TDateField;
    QrPediVdaDtaInclu: TDateField;
    QrPediVdaDtaPrevi: TDateField;
    QrPediVdaPrioridade: TSmallintField;
    QrPediVdaCondicaoPG: TIntegerField;
    QrPediVdaMoeda: TIntegerField;
    QrPediVdaSituacao: TIntegerField;
    QrPediVdaTabelaPrc: TIntegerField;
    QrPediVdaMotivoSit: TIntegerField;
    QrPediVdaLoteProd: TIntegerField;
    QrPediVdaPedidoCli: TWideStringField;
    QrPediVdaFretePor: TSmallintField;
    QrPediVdaTransporta: TIntegerField;
    QrPediVdaRedespacho: TIntegerField;
    QrPediVdaRegrFiscal: TIntegerField;
    QrPediVdaDesoAces_V: TFloatField;
    QrPediVdaDesoAces_P: TFloatField;
    QrPediVdaFrete_V: TFloatField;
    QrPediVdaFrete_P: TFloatField;
    QrPediVdaSeguro_V: TFloatField;
    QrPediVdaSeguro_P: TFloatField;
    QrPediVdaTotalQtd: TFloatField;
    QrPediVdaTotal_Vlr: TFloatField;
    QrPediVdaTotal_Des: TFloatField;
    QrPediVdaTotal_Tot: TFloatField;
    QrPediVdaObserva: TWideMemoField;
    QrPediVdaRepresen: TIntegerField;
    QrPediVdaComisFat: TFloatField;
    QrPediVdaComisRec: TFloatField;
    QrPediVdaCartEmis: TIntegerField;
    QrPediVdaAFP_Sit: TSmallintField;
    QrPediVdaAFP_Per: TFloatField;
    QrPediVdaMedDDSimpl: TFloatField;
    QrPediVdaMedDDReal: TFloatField;
    QrPediVdaValLiq: TFloatField;
    QrPediVdaQuantP: TFloatField;
    QrPediVdaFilial: TIntegerField;
    QrPediVdaNOMETABEPRCCAD: TWideStringField;
    QrPediVdaNOMEMOEDA: TWideStringField;
    QrPediVdaNOMEEMP: TWideStringField;
    QrPediVdaNOMEACC: TWideStringField;
    QrPediVdaNOMETRANSP: TWideStringField;
    QrPediVdaNOMEREDESP: TWideStringField;
    QrPediVdaNOMEFISREGCAD: TWideStringField;
    QrPediVdaNOMEMODELONF: TWideStringField;
    QrPediVdaNOMECARTEMIS: TWideStringField;
    QrPediVdaNOMECONDICAOPG: TWideStringField;
    QrPediVdaNOMEMOTIVO: TWideStringField;
    QrPediVdaCODUSU_ACC: TIntegerField;
    QrPediVdaCODUSU_TRA: TIntegerField;
    QrPediVdaCODUSU_RED: TIntegerField;
    QrPediVdaCODUSU_MOT: TIntegerField;
    QrPediVdaCODUSU_TPC: TIntegerField;
    QrPediVdaCODUSU_MDA: TIntegerField;
    QrPediVdaCODUSU_PPC: TIntegerField;
    QrPediVdaCODUSU_FRC: TIntegerField;
    QrPediVdaMODELO_NF: TIntegerField;
    PainelEdit: TPanel;
    GroupBox9: TGroupBox;
    Label56: TLabel;
    Label2: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    DBEdit3: TDBEdit;
    DBEdit2: TDBEdit;
    EdPedido: TdmkEdit;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    EdCodigo: TdmkEdit;
    EdCodUsu: TdmkEdit;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    DBText1: TDBText;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit6: TDBEdit;
    DBMemo1: TDBMemo;
    DBEdit4: TDBEdit;
    DBMemo2: TDBMemo;
    GroupBox12: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    GroupBox15: TGroupBox;
    Label34: TLabel;
    Label35: TLabel;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    GroupBox14: TGroupBox;
    Label52: TLabel;
    Label53: TLabel;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    QrFatPedCabCodigo: TIntegerField;
    QrFatPedCabCodUsu: TIntegerField;
    QrFatPedCabPedido: TIntegerField;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit5: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit53: TDBEdit;
    DBEdit54: TDBEdit;
    DBEdit57: TDBEdit;
    QrFatPedVol: TmySQLQuery;
    DsFatPedVol: TDataSource;
    QrFatPedVolNO_UnidMed: TWideStringField;
    QrFatPedVolCodigo: TIntegerField;
    QrFatPedVolCnta: TIntegerField;
    QrFatPedVolUnidMed: TIntegerField;
    PMVolume: TPopupMenu;
    Incluinovovolume1: TMenuItem;
    Alteravolumeatual1: TMenuItem;
    DBGFatPedIts: TDBGrid;
    Excluivolumeatual1: TMenuItem;
    QrFatPedCabEmpresa: TIntegerField;
    QrFatPedCabTabelaPrc: TIntegerField;
    QrFatPedCabCondicaoPG: TIntegerField;
    QrFatPedCabMedDDReal: TFloatField;
    QrFatPedCabMedDDSimpl: TFloatField;
    QrFatPedCabRegrFiscal: TIntegerField;
    QrFatPedCabNO_TabelaPrc: TWideStringField;
    QrFatPedItsCU_NIVEL1: TIntegerField;
    QrFatPedItsNO_NIVEL1: TWideStringField;
    QrFatPedItsGraCorCad: TIntegerField;
    QrFatPedItsCU_COR: TIntegerField;
    QrFatPedItsNO_COR: TWideStringField;
    QrFatPedItsNO_TAM: TWideStringField;
    QrFatPedItsGraGru1: TIntegerField;
    QrFatPedItsDataHora: TDateTimeField;
    QrFatPedItsTipo: TSmallintField;
    QrFatPedItsOriCodi: TIntegerField;
    QrFatPedItsOriCtrl: TIntegerField;
    QrFatPedItsOriCnta: TIntegerField;
    QrFatPedItsEmpresa: TIntegerField;
    QrFatPedItsStqCenCad: TIntegerField;
    QrFatPedItsGraGruX: TIntegerField;
    QrFatPedItsQtde: TFloatField;
    QrFatPedCabTipMediaDD: TSmallintField;
    QrFatPedCabAFP_Sit: TSmallintField;
    QrFatPedCabAFP_Per: TFloatField;
    QrFatPedCabAssociada: TIntegerField;
    PMItens: TPopupMenu;
    Incluiitenss1: TMenuItem;
    Excluiitemns1: TMenuItem;
    QrFatPedItsQTDE_POSITIVO: TFloatField;
    QrFatPedCabCU_PediVda: TIntegerField;
    DBEdit59: TDBEdit;
    Label36: TLabel;
    DBEdit58: TDBEdit;
    Label37: TLabel;
    Label33: TLabel;
    EdAbertura: TdmkEdit;
    QrFatPedCabAbertura: TDateTimeField;
    QrFatPedCabEncerrou: TDateTimeField;
    QrFatPedCabFatSemEstq: TSmallintField;
    QrSCCs: TmySQLQuery;
    PMImprime: TPopupMenu;
    Anlise1: TMenuItem;
    QrAnalise: TmySQLQuery;
    frxDsAnalise: TfrxDBDataset;
    frxFAT_PEDID_001_00: TfrxReport;
    QrAnalisePreco: TFloatField;
    QrAnaliseTotal: TFloatField;
    QrAnaliseFilial: TIntegerField;
    QrAnaliseCU_NIVEL1: TIntegerField;
    QrAnaliseNO_NIVEL1: TWideStringField;
    QrAnaliseGraCorCad: TIntegerField;
    QrAnaliseCU_COR: TIntegerField;
    QrAnaliseNO_COR: TWideStringField;
    QrAnaliseNO_TAM: TWideStringField;
    QrAnaliseGraGru1: TIntegerField;
    QrAnaliseGraGruX: TIntegerField;
    QrAnaliseQtde: TFloatField;
    frxDSFatPedCab: TfrxDBDataset;
    QrFatPedCab: TmySQLQuery;
    frxDsPediVda: TfrxDBDataset;
    frxDsCli: TfrxDBDataset;
    QrFatPedCabENCERROU_TXT: TWideStringField;
    Label31: TLabel;
    Label32: TLabel;
    QrFatPedCabEMP_CtaProdVen: TIntegerField;
    QrFatPedCabASS_CtaProdVen: TIntegerField;
    QrPediVdaTIPOCART: TIntegerField;
    QrFatPedCabEMP_FaturaSeq: TSmallintField;
    QrFatPedCabEMP_FaturaSep: TWideStringField;
    QrFatPedCabEMP_FaturaDta: TSmallintField;
    QrFatPedCabASS_FaturaSeq: TSmallintField;
    QrFatPedCabASS_FaturaSep: TWideStringField;
    QrFatPedCabASS_FaturaDta: TSmallintField;
    QrFatPedCabEMP_TxtProdVen: TWideStringField;
    QrFatPedCabASS_TxtProdVen: TWideStringField;
    QrFatPedItsIDCtrl: TIntegerField;
    QrCliIE: TWideStringField;
    QrFatPedCabEMP_FILIAL: TIntegerField;
    QrFatPedCabASS_NO_UF: TWideStringField;
    QrFatPedCabASS_CO_UF: TIntegerField;
    QrFatPedCabASS_FILIAL: TIntegerField;
    SpeedButton5: TSpeedButton;
    Romaneioporgrupo1: TMenuItem;
    QrFatPedCabCliente: TIntegerField;
    PMEncerra: TPopupMenu;
    Encerrafaturamento1: TMenuItem;
    N1: TMenuItem;
    Desfazencerramento1: TMenuItem;
    QrFatPedCabSerieDesfe: TIntegerField;
    QrFatPedCabNFDesfeita: TIntegerField;
    Pedido1: TMenuItem;
    Sempedido1: TMenuItem;
    QrFatPedCabPEDIDO_TXT: TWideStringField;
    QrFatPedCabID_Pedido: TIntegerField;
    QrFatPedCabJurosMes: TFloatField;
    QrFatPedCabNOMEFISREGCAD: TWideStringField;
    QrFatPedCabEMP_UF: TIntegerField;
    QrFatPedItsCALC_TXT: TWideStringField;
    Label51: TLabel;
    DBEdit16: TDBEdit;
    DBEdit23: TDBEdit;
    Label66: TLabel;
    DBEdit39: TDBEdit;
    Label67: TLabel;
    DBEdit38: TDBEdit;
    Label68: TLabel;
    DBEdit41: TDBEdit;
    Label70: TLabel;
    DBEdit40: TDBEdit;
    Label71: TLabel;
    DBEdit43: TDBEdit;
    Label72: TLabel;
    DBEdit42: TDBEdit;
    QrPediVdaUsaReferen: TSmallintField;
    QrL_C: TmySQLQuery;
    PMComissao: TPopupMenu;
    Incluicomisses1: TMenuItem;
    Excluicomisses1: TMenuItem;
    QrL_CFatID: TIntegerField;
    QrL_CFatNum: TFloatField;
    QrL_CFatParcela: TIntegerField;
    QrL_CCarteira: TIntegerField;
    QrFatPedCabPedidoCli: TWideStringField;
    QrCliNUMERO: TFloatField;
    QrCliUF: TFloatField;
    QrCliCEP: TFloatField;
    QrCliLograd: TFloatField;
    QrPediVdaEMP_UF: TFloatField;
    QrPediVdaJurosMes: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtFatura: TBitBtn;
    BtVolume: TBitBtn;
    BtItens: TBitBtn;
    BtEncerra: TBitBtn;
    BtComissao: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    QrPediVdaidDest: TSmallintField;
    QrPediVdaindFinal: TSmallintField;
    QrPediVdaindPres: TSmallintField;
    QrPediVdaIndSinc: TSmallintField;
    BtFisRegCad: TBitBtn;
    BtGraGruN: TBitBtn;
    QrFatPedItsCO_NIVEL1: TIntegerField;
    QrPediVdafinNFe: TSmallintField;
    QrFatPedItsOriPart: TIntegerField;
    QrCliindIEDest: TSmallintField;
    N2: TMenuItem;
    N3: TMenuItem;
    Duplicarfaturamentoatual1: TMenuItem;
    QrFatPedItsMadeBy: TSmallintField;
    QrFatPedItsIPI_Alq: TFloatField;
    QrFatPedItsPecas: TFloatField;
    QrFatPedItsAreaM2: TFloatField;
    QrFatPedItsAreaP2: TFloatField;
    QrFatPedItsPeso: TFloatField;
    QrFatPedItsprod_indTot: TSmallintField;
    QrPediVdaRetiradaUsa: TSmallintField;
    QrPediVdaRetiradaEnti: TIntegerField;
    QrPediVdaEntregaUsa: TSmallintField;
    QrPediVdaEntregaEnti: TIntegerField;
    QrPediVdaNOMEENTRETIRADA: TWideStringField;
    QrPediVdaNOMEENTENTREGA: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    GroupBox3: TGroupBox;
    Label12: TLabel;
    DBText2: TDBText;
    Label13: TLabel;
    DBEdit10: TDBEdit;
    DBEdit13: TDBEdit;
    DBMemo3: TDBMemo;
    DBEdit14: TDBEdit;
    DBMemo4: TDBMemo;
    GroupBox4: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label19: TLabel;
    DBEdit15: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    GroupBox6: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label22: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit51: TDBEdit;
    DBEdit52: TDBEdit;
    GroupBox5: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    Label29: TLabel;
    Label38: TLabel;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBRGIndDest: TDBRadioGroup;
    RGDBIndFinal: TDBRadioGroup;
    DBEdindPres: TDBEdit;
    EdDBindPres_TXT: TEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBEdfinNFe: TDBEdit;
    EdDBfinNFe_TXT: TEdit;
    QrEntregaEnti: TmySQLQuery;
    QrEntregaEntiCodigo: TIntegerField;
    QrEntregaEntiCadastro: TDateField;
    QrEntregaEntiENatal: TDateField;
    QrEntregaEntiPNatal: TDateField;
    QrEntregaEntiTipo: TSmallintField;
    QrEntregaEntiRespons1: TWideStringField;
    QrEntregaEntiRespons2: TWideStringField;
    QrEntregaEntiENumero: TIntegerField;
    QrEntregaEntiPNumero: TIntegerField;
    QrEntregaEntiELograd: TSmallintField;
    QrEntregaEntiPLograd: TSmallintField;
    QrEntregaEntiECEP: TIntegerField;
    QrEntregaEntiPCEP: TIntegerField;
    QrEntregaEntiNOME_ENT: TWideStringField;
    QrEntregaEntiNO_2_ENT: TWideStringField;
    QrEntregaEntiCNPJ_CPF: TWideStringField;
    QrEntregaEntiIE_RG: TWideStringField;
    QrEntregaEntiNIRE_: TWideStringField;
    QrEntregaEntiRUA: TWideStringField;
    QrEntregaEntiSITE: TWideStringField;
    QrEntregaEntiNUMERO: TIntegerField;
    QrEntregaEntiCOMPL: TWideStringField;
    QrEntregaEntiBAIRRO: TWideStringField;
    QrEntregaEntiCIDADE: TWideStringField;
    QrEntregaEntiNOMELOGRAD: TWideStringField;
    QrEntregaEntiNOMEUF: TWideStringField;
    QrEntregaEntiPais: TWideStringField;
    QrEntregaEntiLograd: TSmallintField;
    QrEntregaEntiCEP: TIntegerField;
    QrEntregaEntiTE1: TWideStringField;
    QrEntregaEntiFAX: TWideStringField;
    QrEntregaEntiEMAIL: TWideStringField;
    QrEntregaEntiTRATO: TWideStringField;
    QrEntregaEntiENDEREF: TWideStringField;
    QrEntregaEntiCODMUNICI: TFloatField;
    QrEntregaEntiCODPAIS: TFloatField;
    QrEntregaEntiRG: TWideStringField;
    QrEntregaEntiSSP: TWideStringField;
    QrEntregaEntiDataRG: TDateField;
    QrEntregaEntiIE: TWideStringField;
    DsEntregaEnti: TDataSource;
    QrEntregaEntiE_ALL: TWideStringField;
    QrEntregaEntiCNPJ_TXT: TWideStringField;
    QrEntregaEntiNOME_TIPO_DOC: TWideStringField;
    QrEntregaEntiTE1_TXT: TWideStringField;
    QrEntregaEntiFAX_TXT: TWideStringField;
    QrEntregaEntiNUMERO_TXT: TWideStringField;
    QrEntregaEntiCEP_TXT: TWideStringField;
    QrCliL_Ativo: TSmallintField;
    QrEntregaCliL_CNPJ: TWideStringField;
    QrEntregaCliL_CPF: TWideStringField;
    QrEntregaCliL_Nome: TWideStringField;
    QrEntregaCliLograd: TSmallintField;
    QrEntregaCliNOMELOGRAD: TWideStringField;
    QrEntregaCliRUA: TWideStringField;
    QrEntregaCliNUMERO: TIntegerField;
    QrEntregaCliCOMPL: TWideStringField;
    QrEntregaCliBAIRRO: TWideStringField;
    QrEntregaCliLCodMunici: TIntegerField;
    QrEntregaCliNO_MUNICI: TWideStringField;
    QrEntregaCliNOMEUF: TWideStringField;
    QrEntregaCliCEP: TIntegerField;
    QrEntregaCliCodiPais: TIntegerField;
    QrEntregaCliNO_PAIS: TWideStringField;
    QrEntregaCliENDEREF: TWideStringField;
    QrEntregaCliTE1: TWideStringField;
    QrEntregaCliEmail: TWideStringField;
    QrEntregaCliL_IE: TWideStringField;
    EncerrafaturamentoModoRpido1: TMenuItem;
    QrEndContratante: TMySQLQuery;
    QrEndContratanteCodigo: TIntegerField;
    QrEndContratanteTipo: TSmallintField;
    QrEndContratanteNOMELOGRAD: TWideStringField;
    QrEndContratanteNOMERUA: TWideStringField;
    QrEndContratanteCOMPL: TWideStringField;
    QrEndContratanteBAIRRO: TWideStringField;
    QrEndContratanteNOMEUF: TWideStringField;
    QrEndContratanteCNPJ_CPF: TWideStringField;
    QrEndContratanteIE_RG: TWideStringField;
    QrEndContratanteTEL: TWideStringField;
    QrEndContratanteFAX: TWideStringField;
    QrEndContratanteCEP: TFloatField;
    QrEndContratanteTEL_TXT: TWideStringField;
    QrEndContratanteCONTRATANTE: TWideStringField;
    QrEndContratanteFAX_TXT: TWideStringField;
    QrEndContratanteCNPJ_TXT: TWideStringField;
    QrEndContratanteCEP_TXT: TWideStringField;
    QrEndContratanteNUMERO: TFloatField;
    QrEndContratanteNO_CIDADE: TWideStringField;
    QrEndContratanteNO_PAIS: TWideStringField;
    QrSMVA: TMySQLQuery;
    QrSMVATotal: TFloatField;
    Alteradadosespecficosdoitemselecionado1: TMenuItem;
    QrFatPedItsvFrete: TFloatField;
    QrFatPedItsvDesc: TFloatField;
    QrFatPedItsvSeg: TFloatField;
    QrFatPedItsvOutro: TFloatField;
    N4: TMenuItem;
    DistribuirFrete1: TMenuItem;
    DBEdit55: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFatPedCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFatPedCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtFaturaClick(Sender: TObject);
    procedure PMFaturaPopup(Sender: TObject);
    procedure QrFatPedCabBeforeClose(DataSet: TDataSet);
    procedure QrFatPedCabAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure Alterafaturamentoatual1Click(Sender: TObject);
    procedure EdPedidoExit(Sender: TObject);
    procedure QrPediVdaAfterOpen(DataSet: TDataSet);
    procedure QrPediVdaBeforeClose(DataSet: TDataSet);
    procedure QrPediVdaCalcFields(DataSet: TDataSet);
    procedure QrCliAfterOpen(DataSet: TDataSet);
    procedure QrCliBeforeClose(DataSet: TDataSet);
    procedure QrCliCalcFields(DataSet: TDataSet);
    procedure QrEntregaCliCalcFields(DataSet: TDataSet);
    procedure QrFatPedVolAfterScroll(DataSet: TDataSet);
    procedure QrFatPedVolBeforeClose(DataSet: TDataSet);
    procedure Incluinovovolume1Click(Sender: TObject);
    procedure Alteravolumeatual1Click(Sender: TObject);
    procedure BtVolumeClick(Sender: TObject);
    procedure PMVolumePopup(Sender: TObject);
    procedure Excluivolumeatual1Click(Sender: TObject);
    procedure QrFatPedVolAfterOpen(DataSet: TDataSet);
    procedure Incluiitenss1Click(Sender: TObject);
    procedure Excluiitemns1Click(Sender: TObject);
    procedure Anlise1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrFatPedCabCalcFields(DataSet: TDataSet);
    procedure DBEdit59Change(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Romaneioporgrupo1Click(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure Encerrafaturamento1Click(Sender: TObject);
    procedure Desfazencerramento1Click(Sender: TObject);
    procedure PMEncerraPopup(Sender: TObject);
    procedure Pedido1Click(Sender: TObject);
    procedure Excluifaturamentoatual1Click(Sender: TObject);
    procedure BtComissaoClick(Sender: TObject);
    procedure Incluicomisses1Click(Sender: TObject);
    procedure PMComissaoPopup(Sender: TObject);
    procedure Excluicomisses1Click(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure DBEdindPresChange(Sender: TObject);
    procedure BtFisRegCadClick(Sender: TObject);
    procedure BtGraGruNClick(Sender: TObject);
    procedure DBEdfinNFeChange(Sender: TObject);
    procedure Sempedido1Click(Sender: TObject);
    procedure Duplicarfaturamentoatual1Click(Sender: TObject);
    procedure QrEntregaEntiCalcFields(DataSet: TDataSet);
    procedure EncerrafaturamentoModoRpido1Click(Sender: TObject);
    procedure Alteradadosespecficosdoitemselecionado1Click(Sender: TObject);
    procedure DistribuirFrete1Click(Sender: TObject);
  private
    F_indPres, F_finNFe: MyArrayLista;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure EncerraFaturamento();
    procedure DesfazEncerramento();
    procedure CriaLctsComissoes();
    procedure AlteraFaturamentoAtual();
    procedure EmiteNFe_ModoRapido();
    function  MovimentoPossuiNFAutorizadaOuEPEC(FatID, FatNum, Empresa: Integer;
              Avisa: Boolean): Boolean;
//?    procedure ReopenEndContratante();
    function  ObtemValorTotalFaturamento(): Double;

  public
    { Public declarations }
    FThisFatID: Integer;
    FMultiGrandeza: Boolean;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenFatPedVol(Cnta: Integer);
    procedure ReopenFatPedIts(OriCtrl: Integer);
    procedure ReopenPediVda(Pedido: Integer);
    //function  Encerra(): Boolean;
    procedure IncluiNovoVolume();
  end;

var
  FmFatDivGer2: TFmFatDivGer2;

const
  FFormatFloat = '00000';
  SFaturamentoAberto = 'EM FATURAMENTO';

implementation

uses
  {$IfNDef NAO_GPED} FatPedImp, {$EndIf}
  {$IfNDef SemNFe_0000} ModuleNFe_0000, NFe_PF, UnXXe_PF, {$EndIf}
  {$IfNDef SemPediVda} FatPedIts2, FatPedPes2, {$EndIf}
  {$IfNDef NAO_GFAT}UnGrade_Jan, {$EndIf}
  //FatDivCms,
  UnFinanceiro, UnMyObjects, Module, MyDBCheck, MasterSelFilial,
  ModuleGeral, ModPediVda, ModProd, GetData, Principal, FatDivGru2,
  FatDivRef2, UCreate, MyListas, UnGFat_Jan, ModuleFatura, UnGrade_PF,
  GetValor;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFatDivGer2.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFatDivGer2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFatPedCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFatDivGer2.DefParams;
begin
  VAR_GOTOTABELA := 'FatPedCab';
  VAR_GOTOMYSQLTABLE := QrFatPedCab;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT pvd.CodUsu CU_PediVda, pvd.Empresa, ');
  VAR_SQLx.Add('pvd.TabelaPrc, pvd.CondicaoPG, pvd.RegrFiscal,');
  VAR_SQLx.Add('pvd.AFP_Sit, pvd.AFP_Per, pvd.Cliente,');
  VAR_SQLx.Add('pvd.Codigo ID_Pedido, pvd.PedidoCli, ');
  // NFe 3.10
  VAR_SQLx.Add('pvd.idDest, pvd.indFinal, pvd.indPres, pvd.indSinc, ');
  //
  VAR_SQLx.Add('ppc.JurosMes, frc.Nome NOMEFISREGCAD,');
  VAR_SQLx.Add('ppc.MedDDReal, ppc.MedDDSimpl,');
  VAR_SQLx.Add('par.TipMediaDD, par.Associada, par.FatSemEstq,');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('par.CtaProdVen EMP_CtaProdVen,');
  VAR_SQLx.Add('par.FaturaSeq EMP_FaturaSeq,');
  VAR_SQLx.Add('par.FaturaSep EMP_FaturaSep,');
  VAR_SQLx.Add('par.FaturaDta EMP_FaturaDta,');
  VAR_SQLx.Add('par.TxtProdVen EMP_TxtProdVen,');
  VAR_SQLx.Add('emp.Filial EMP_FILIAL,');
  VAR_SQLx.Add('ufe.Codigo EMP_UF,');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('ass.CtaProdVen ASS_CtaProdVen,');
  VAR_SQLx.Add('ass.FaturaSeq ASS_FaturaSeq,');
  VAR_SQLx.Add('ass.FaturaSep ASS_FaturaSep,');
  VAR_SQLx.Add('ass.FaturaDta ASS_FaturaDta,');
  VAR_SQLx.Add('ass.TxtProdVen ASS_TxtProdVen,');
  VAR_SQLx.Add('ufa.Nome ASS_NO_UF,');
  VAR_SQLx.Add('ufa.Codigo ASS_CO_UF,');
  VAR_SQLx.Add('ase.Filial ASS_FILIAL,');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('tpc.Nome NO_TabelaPrc, fpc.*');
  VAR_SQLx.Add('FROM fatpedcab fpc');
  VAR_SQLx.Add('LEFT JOIN pedivda    pvd ON pvd.Codigo=fpc.Pedido');
  VAR_SQLx.Add('LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG');
  VAR_SQLx.Add('LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc');
  VAR_SQLx.Add('LEFT JOIN paramsemp  par ON par.Codigo=pvd.Empresa');
  VAR_SQLx.Add('LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada');
  VAR_SQLx.Add('LEFT JOIN entidades  ase ON ase.Codigo=ass.Codigo');
  VAR_SQLx.Add('LEFT JOIN entidades  emp ON emp.Codigo=par.Codigo');
  VAR_SQLx.Add('LEFT JOIN ufs        ufe ON ufe.Codigo=IF(emp.Tipo=0, emp.EUF, emp.PUF)');
  VAR_SQLx.Add('LEFT JOIN ufs        ufa ON ufa.Codigo=IF(ase.Tipo=0, ase.EUF, ase.PUF)');
  VAR_SQLx.Add('LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal');
  VAR_SQLx.Add('WHERE fpc.Codigo > -1000');
  VAR_SQLx.Add('AND pvd.CodUsu < 0 ');
  VAR_SQLx.Add('');
  //
  VAR_SQLx.Add('AND pvd.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND fpc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND fpc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND fpc.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Pedido IN (SELECT Codigo FROM pedivda WHERE Empresa IN (' + VAR_LIB_EMPRESAS + ') AND CodUsu < 0)';
end;

procedure TFmFatDivGer2.DesfazEncerramento();
{$IfNDef SemNFe_0000}
var
  FatNum, Empresa: Integer;
  AutorizarBoss: Boolean;
begin
  FatNum  := QrFatPedCabCodigo.Value;
  Empresa := QrFatPedCabEmpresa.Value;

  if MovimentoPossuiNFAutorizadaOuEPEC(FThisFatID, FatNum, Empresa, True) then
    AutorizarBoss := False
  else
    AutorizarBoss := True;
  //
  if DmNFe_0000.DesfazEncerramento('fatpedcab', FThisFatID, FatNum, Empresa) then
  begin
    LocCod(FatNum, FatNum);
    //
    if QrFatPedCabCodigo.Value = FatNum then
    begin
      if Geral.MB_Pergunta('Encerramento desfeito! Deseja encerrar novamente') = ID_YES then
        EncerraFaturamento();
    end;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappNFe);
{$EndIf}
end;

procedure TFmFatDivGer2.Desfazencerramento1Click(Sender: TObject);
begin
  DesfazEncerramento()
end;

procedure TFmFatDivGer2.DistribuirFrete1Click(Sender: TObject);
const
  FormCaption  = 'Valor Total do Frete';
  ValCaption   = 'Informe o Valor:';
  WidthCaption = Length(ValCaption) * 7;
var
  ValorTotal, QtdeTotal, FreteTotal, Restante, ValorItem, Fator, vFrete: Double;
  ValVar: Variant;
  IDCtrl, OriCodi, OriCtrl, Tipo, Empresa, iniID: Integer;
  Qry: TmySQLQuery;
begin
  iniID   := QrFatPedItsIDCtrl.Value;
  OriCodi := QrFatPedItsOriCodi.Value;
  OriCtrl := QrFatPedItsOriCtrl.Value;
  Tipo    := QrFatPedItsTipo.Value;
  Empresa := QrFatPedItsEmpresa.Value;
  //
  Grade_PF.ObtemStqMovValA_SUM(OriCodi, Tipo, Empresa, QtdeTotal, ValorTotal);
  //
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    0.00, 2, 0, '0,00', '', True, FormCaption, ValCaption, WidthCaption,
    ValVar) then
  begin
    Screen.Cursor := crHourGlass;
    try
      FreteTotal := Geral.DMV(ValVar);
      Restante := FreteTotal;
      //
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Empresa, ID, Tipo, OriCodi, ',
        'OriCtrl, IDCtrl, Qtde, Total ',
        'FROM stqmovvala ',
        'WHERE OriCodi=' + Geral.FF0(OriCodi),
        'AND Tipo =' + Geral.FF0(Tipo),
        'AND Empresa=' + Geral.FF0(Empresa),
        '']);
        if Qry.RecordCount > 0 then
        begin
          Qry.First;
          while not Qry.Eof do
          begin
            ValorItem := Qry.FieldByName('Total').AsInteger;
            IDCtrl    := Qry.FieldByName('IDCtrl').AsInteger;
            OriCodi   := Qry.FieldByName('OriCodi').AsInteger;
            OriCtrl   := Qry.FieldByName('OriCtrl').AsInteger;
            Tipo      := Qry.FieldByName('Tipo').AsInteger;
            Empresa   := Qry.FieldByName('Empresa').AsInteger;
            //
            if ValorTotal > 0 then
              Fator := ValorItem / ValorTotal
            else
              Fator := 0;
            //
            if Qry.RecNo = Qry.RecordCount then
              vFrete := Restante
            else
            begin
              vFrete := Round(Fator * FreteTotal * 100) / 100;
              Restante := Restante - vFrete;
            end;
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovvala', False, [
            'prod_vFrete'], [
            'IDCtrl', 'Tipo', 'OriCodi', 'OriCtrl', 'Empresa'], [
            vFrete], [
            IDCtrl, Tipo, OriCodi, OriCtrl, Empresa], False);
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovitsa', False, [
            'vFrete'], ['IDCtrl'], [
            vFrete], [
            IDCtrl], False);
            //
            Qry.Next;
          end;
          //
          ReopenFatPedIts(iniID);
        end else
          Geral.MB_Aviso('Nenhum registro de item foi localizado!');
      finally
        Qry.Free;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmFatDivGer2.Duplicarfaturamentoatual1Click(Sender: TObject);
var
  PediVda_Codigo, PediVda_CodUsu, FatPedCab_Codigo, FatPedCab_CodUsu,
  Pedido, Cnta, OriCodi, OriCnta, Empresa, Cliente, Associada, RegrFiscal,
  GraGruX, StqCenCad, FatSemEstq, AFP_Sit, Cli_UF, EMP_UF, EMP_FILIAL,
  ASS_CO_UF, ASS_FILIAL, Item_MadeBy, Preco_MedOrdem, PediVda, OriPart,
  InfAdCuztm, TipoCalc, prod_indTot, IDCtrl, OriCtrl, Cli_Tipo: Integer;
  AFP_Per, Qtde, Item_IPI_ALq, Preco_PrecoF, Preco_PercCustom, Preco_MedidaC,
  Preco_MedidaL, Preco_MedidaA, Preco_MedidaE, Pecas, AreaM2, AreaP2,
  Peso: Double;
  Abertura, NO_tablaPrc, Cli_IE: String;
  prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
begin
  if (QrPediVda.State = dsInactive) or (QrPediVda.RecordCount = 0) then Exit;
  //
  if Geral.MB_Pergunta('Confirma a duplica��o do faturamento atual?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      QrPediVda.DisableControls;
      QrFatPedCab.DisableControls;
      QrFatPedVol.DisableControls;
      QrFatPedIts.DisableControls;
      //
      //INSERE pedivda
      PediVda_Codigo := UMyMod.BuscaEmLivreY_Def('pedivda', 'Codigo', stIns, 0);
      PediVda_CodUsu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'pedivda', 'CodUsu', [], [], stIns, 0, siNegativo, nil);
      //
      if MyObjects.FIC(PediVda_Codigo = 0, nil, 'Falha ao definir C�digo para a tabela "pedivda"!') then Exit;
      if MyObjects.FIC(PediVda_CodUsu = 0, nil, 'Falha ao definir CodUsu para a tabela "pedivda"!') then Exit;
      //
      if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'pedivda', TMeuDB,
        ['Codigo'], [QrPediVdaCodigo.Value],
        ['Codigo', 'CodUsu'],
        [PediVda_Codigo, PediVda_CodUsu],
        '', True, LaAviso1, LaAviso2) then
      begin
        //INSERE fatpedcab
        FatPedCab_Codigo := UMyMod.BuscaEmLivreY_Def('fatpedcab', 'Codigo', stIns, 0);
        FatPedCab_CodUsu := DModG.BuscaProximoCodigoInt('controle', 'StqMovUsu', '', 0);
        //
        Abertura := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
        Pedido   := PediVda_Codigo;
        //
        if MyObjects.FIC(FatPedCab_Codigo = 0, nil, 'Falha ao definir C�digo para a tabela "fatpedcab"!') then Exit;
        if MyObjects.FIC(FatPedCab_CodUsu = 0, nil, 'Falha ao definir CodUsu para a tabela "fatpedcab"!') then Exit;
        //
        if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'fatpedcab', TMeuDB,
          ['Codigo'], [QrFatPedCabCodigo.Value],
          ['Codigo', 'CodUsu', 'Pedido', 'Abertura', 'Encerrou',
          'DataFat', 'Status', 'SerieDesfe', 'NFDesfeita'],
          [FatPedCab_Codigo, FatPedCab_CodUsu, Pedido, Abertura, 0,
          0, 0, 0, 0],
          '', True, LaAviso1, LaAviso2) then
        begin
          //INSERE fatpedcab
          ReopenFatPedVol(0);
          //
          if QrFatPedVol.RecordCount > 0 then
          begin
            QrFatPedVol.First;
            while not QrFatPedVol.Eof do
            begin
              Cnta := UMyMod.BuscaEmLivreY_Def('fatpedvol', 'Cnta', stIns, 0);
              //
              if MyObjects.FIC(Cnta = 0, nil, 'Falha ao definir Cnta para a tabela "fatpedvol"!') then Exit;
              //
              if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'fatpedvol', TMeuDB,
                ['Cnta'], [QrFatPedVolCnta.Value],
                ['Codigo', 'Cnta'],
                [FatPedCab_Codigo, Cnta],
                '', True, LaAviso1, LaAviso2) then
              begin
                //INSERE fatpedcab
                ReopenFatPedIts(0);
                //
                if QrFatPedIts.RecordCount > 0 then
                begin
                  QrFatPedIts.First;
                  while not QrFatPedIts.Eof do
                  begin
                    OriCodi          := FatPedCab_Codigo;
                    OriCnta          := Cnta;
                    Empresa          := QrFatPedCabEmpresa.Value;
                    Cliente          := QrFatPedCabCliente.Value;
                    Associada        := QrFatPedCabAssociada.Value;
                    RegrFiscal       := QrFatPedCabRegrFiscal.Value;
                    GraGruX          := QrFatPedItsGraGruX.Value;
                    NO_tablaPrc      := QrFatPedCabNO_TabelaPrc.Value;
                    StqCenCad        := QrFatPedItsStqCenCad.Value;
                    FatSemEstq       := QrFatPedCabFatSemEstq.Value;
                    AFP_Sit          := QrFatPedCabAFP_Sit.Value;
                    AFP_Per          := QrFatPedCabAFP_Per.Value;
                    Qtde             := QrFatPedItsQtde.Value;
                    Cli_Tipo         := QrCliTipo.Value;
                    Cli_IE           := QrCliIE.Value;
                    Cli_UF           := Trunc(QrCliUF.Value);
                    EMP_UF           := QrFatPedCabEMP_UF.Value;
                    EMP_FILIAL       := QrFatPedCabEMP_FILIAL.Value;
                    ASS_CO_UF        := QrFatPedCabASS_CO_UF.Value;
                    ASS_FILIAL       := QrFatPedCabASS_FILIAL.Value;
                    Item_MadeBy      := QrFatPedItsMadeBy.Value;
                    ITEM_IPI_Alq     := QrFatPedItsIPI_Alq.Value;
                    Preco_PercCustom := 0;
                    Preco_MedidaC    := 0;
                    Preco_MedidaL    := 0;
                    Preco_MedidaA    := 0;
                    Preco_MedidaE    := 0;
                    Preco_MedOrdem   := 0;
                    PediVda          := QrFatPedCabPedido.Value;
                    OriPart          := 0;
                    InfAdCuztm       := 0;
                    Pecas            := QrFatPedItsPecas.Value;
                    AreaM2           := QrFatPedItsAreaM2.Value;
                    AreaP2           := QrFatPedItsAreaP2.Value;
                    Peso             := QrFatPedItsPeso.Value;
                    TipoCalc         := Grade_PF.ObtemFisRegMvtTipoCalc(
                                        RegrFiscal, StqCenCad, Empresa);
                    prod_indTot      := QrFatPedItsprod_indTot.Value;
                    IDCtrl           := QrFatPedItsIDCtrl.Value;
                    OriCtrl          := QrFatPedItsOriCtrl.Value;
                    Preco_PrecoF     := Grade_PF.ObtemStqMovValAPrecoF(
                                        QrFatPedCabCodigo.Value, OriCtrl,
                                        QrFatPedVolCnta.Value, IDCtrl, Empresa);
                    //
                    if MyObjects.FIC(Preco_PrecoF = 0, nil,
                      'Falha ao definir pre�o do produto!') then Exit;
                    //
                    IDCtrl := 0; //Zerar para evitar erro
                    //
                    // ini 2021-10-20
                    {
                    prod_vFrete := 0.00;
                    prod_vSeg   := 0.00;
                    prod_vDesc  := 0.00;
                    prod_vOutro := 0.00;
                    }
                    prod_vFrete := QrFatPedItsvFrete.Value;
                    prod_vSeg   := QrFatPedItsvSeg.Value;
                    prod_vDesc  := QrFatPedItsvDesc.Value;
                    prod_vOutro := QrFatPedItsvOutro.Value;
                    // fim 2021-10-20
                    //
                    if not DmFatura.InsereItemStqMov(VAR_FATID_0001, OriCodi,
                      OriCnta, Empresa, Cliente, Associada, RegrFiscal, GraGruX,
                      NO_tablaPrc, '', StqCenCad, FatSemEstq, AFP_Sit, AFP_Per,
                      Qtde, Cli_Tipo, Cli_IE, Cli_UF, EMP_UF, EMP_FILIAL,
                      ASS_CO_UF, ASS_FILIAL, Item_MadeBy, Item_IPI_ALq,
                      Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
                      Preco_MedidaA, Preco_MedidaE, Preco_MedOrdem, stIns,
                      PediVda, OriPart, InfAdCuztm, (*TipoNF*)0, (*modNF*)0,
                      (*Serie*)0, (*nNF*)0, (*SitDevolu*)0, (*Servico*)0,
                      (*refNFe*)'', 0, OriCtrl, Pecas, AreaM2, AreaP2, Peso,
                      TipoCalc, prod_indTot,
                      prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro,
                      IDCtrl) then
                    begin
                      Geral.MB_Erro('Falha ao inserir produtos!');
                      Exit;
                    end else
                    begin
                      //INSERE fatpedfis
                      if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'fatpedfis', TMeuDB,
                        ['IDCtrl'], [QrFatPedItsIDCtrl.Value],
                        ['IDCtrl'],
                        [IDCtrl],
                        '', True, LaAviso1, LaAviso2) then
                    end;
                    //
                    QrFatPedIts.Next;
                  end;
                end;
              end;
              QrFatPedVol.Next;
            end;
          end;
        end;
      end;
    finally
      QrPediVda.EnableControls;
      QrFatPedCab.EnableControls;
      QrFatPedVol.EnableControls;
      QrFatPedIts.EnableControls;
      //
      Va(vpLast);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      Screen.Cursor := crDefault;
      Geral.MB_Aviso('Duplica��o finalizada!');
    end;
  end;
end;

procedure TFmFatDivGer2.EdPedidoExit(Sender: TObject);
begin
  ReopenPediVda(EdPedido.ValueVariant);
  if EdPedido.ValueVariant <> 0 then
  begin
    if QrPediVda.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Pedido n�o localizado!');
      EdPedido.ValueVariant := 0;
    end;
  end;
end;

procedure TFmFatDivGer2.EmiteNFe_ModoRapido();
{$IfNDef SemNFe_0000}
const
  OriTipo = 1;
//?  OEstoqueJaFoiBaixado = True;
  SemFinanceiro = 0;
var
{
  Especie, UFCli: String;
  Quantidade,} FatSemEstq, FatPedCab, VolCnta, RegrFiscal, PediVda{, CtrID,
  EntidadeTipo}: Integer;
  ValFrete, (*prod_vFrete,*) ValorTotal: Double;
  IDCtrl, Status: Integer;
{
  Achou: Boolean;
}
  //
  Cliente, Empresa, {Tipo, OriCodi, ValiStq,
  Codigo,} FatID{, FatNum, ide_mod, ide_nNF, NF_Stat, Baixa}: Integer;
  CodStatus: Integer;
  TxtStatus: String;
  //
  FretePor, Transporta: Integer;
begin
  ValFrete := 0;
  FatID      := VAR_FATID_0001;
  FatPedCab  := QrFatPedCabCodigo.Value;
  Empresa    := QrFatPedCabEmpresa.Value;
  if MovimentoPossuiNFAutorizadaOuEPEC(FatID, FatPedCab, Empresa, True) then Exit;
  //
  ValorTotal := ObtemValorTotalFaturamento(); //QrLocCConItsValorPedi.Value;
  //
//?  CtrID := QrLocCConItsCtrID.Value;
  //DModG.ReopenParamsEmp();
  //FatSemEstq := DModG.QrParamsEmpFatSemEstq.Value;
  FatSemEstq := 1; // Permite sem avisar
  Cliente    := QrFatPedCabCliente.Value;
  //FatPedCab  := QrFatPedCabCodigo.Value;
  PediVda    := QrFatPedCabID_Pedido.Value;
  VolCnta    := QrFatPedVolCnta.Value;
{$IfDef cLocacao}
  RegrFiscal := Dmod.QrOpcoesTRenLocRegrFisNFe.Value;
  FretePor   := 9;
  Transporta := 0;
{$Else}
  //ver o que fazer!
  RegrFiscal := QrPediVdaRegrFiscal.Value;
  FretePor   := QrPediVdaFretePor.Value;
  Transporta := QrPediVdaTransporta.Value;
{$EndIf}
  //
  Empresa    := QrFatPedCabEmpresa.Value;
//?  Codigo     := QrFatPedCabCodigo.Value;
  //
{?
  ReopenEndContratante();
  UFCli := (*Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(*)QrEndContratanteNOMEUF.Value;
  EntidadeTipo := QrEndContratanteTipo.Value;
?}
  //
{?
  // For�a convers�o de registros pr�-cadastrados de pedido e faturamento de NFCe
  // para NFe
  XXe_PF.AlteraPedido_TipoNF(Empresa, FatPedCab, Cliente, RegrFiscal,
  (*De*) VAR_FATID_0002, (*Para*) VAR_FATID_0001);
?}
//

(*
  try

    UnNFe_PF.NFe_StatusServicoCod(False, CodStatus, TxtStatus);
    if CodStatus <> 107 then
    begin
      Geral.MB_Info(TxtStatus + ' (C�digo ' + Geral.FF0(CodStatus) + ')');
      Exit;
    end;
  except
     UnWin.MostraMMC_SnapIn();
     Exit;
  end;
*)
  UnNFe_PF.NFe_StatusServicoCodMul(Self, CodStatus, TxtStatus);


  UnNFe_PF.EncerraFaturamento_Quiet(FatSemEstq, FatPedCab, PediVda, (*IDVolume*)VolCnta, (*Tipo*)OriTipo,
  RegrFiscal, (*Tabela*)'fatpedcab' (*, OEstoqueJaFoiBaixado: Boolean*),
  SemFinanceiro, ValorTotal, ValFrete, FretePor, Transporta);

{?
  //UnNFCe_PF.EncerraFaturamento(FatSemEstq, FatPedCab, PediVda, VolCnta, OriTipo,
  //  RegrFiscal, 'fatpedcab', ValFrete, UFCli, EntidadeTipo, ValorTotal,
  //  (* OEstoqueJaFoiBaixado*)True);
  //
  // Fazer aqui somas e atualizad��es!!!!!!!!!!!!
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT FatID, FatNum, ide_mod, ide_nNF, Status  ',
  'FROM nfecaba ',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_0001), // NFe
  'AND FatNum=' + Geral.FF0(FatPedCab),
  'AND Empresa=' + Geral.FF0(Empresa),
  'ORDER BY Status DESC ',
  ' ']);
  //Geral.MB_Teste(Dmod.QrAux.SQL.Text);
  NF_Stat := 0;
  if Dmod.QrAux.RecordCount > 0 then
  begin
    FatID    := DMod.QrAux.FieldByName('FatID').AsInteger;
    FatNum   := DMod.QrAux.FieldByName('FatNum').AsInteger;
    ide_mod  := DMod.QrAux.FieldByName('ide_mod').AsInteger;
    ide_nNF  := DMod.QrAux.FieldByName('ide_nNF').AsInteger;
    NF_Stat  := DMod.QrAux.FieldByName('Status').AsInteger;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconits', False, [
    'Codigo', 'FatID', 'FatNum',
    'ide_mod', 'ide_nNF', 'NF_Stat'], [
    'CtrID'], [
    Codigo, FatID, FatNum,
    ide_mod, ide_nNF, NF_Stat], [
    CtrID], True) then
    begin
    end;
  end;
  //
?}
//?  AppPF.VerificaEstoqueVenda(Empresa, FatPedCab, NF_Stat, QrFatPedCabStatus.Value, QrLocCConItsStatus.Value);
  //
  LocCod(QrFatPedCabCodigo.Value, QrFatPedCabCodigo.Value);
//?  QrLocCConIts.Locate('CtrID', CtrID, []);
  //
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappNFe);
{$EndIf}
end;

procedure TFmFatDivGer2.Excluicomisses1Click(Sender: TObject);
var
  Filial: Integer;
  TabLctA: String;
begin
(*
  if Geral.MB_Pergunta('Confirma a exclus�o dos ' + FormatFloat('0', QrL_C.RecordCount) +
    ' lan�amentos de comiss�o?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      QrL_C.First;
      while not QrL_C.Eof do
      begin
        UFinanceiro.ExcluiLct_FatParcela(DMod.MyDB, QrL_CFatID.Value,
        QrL_CFatNum.Value, QrL_CFatParcela.Value, QrL_CCarteira.Value);
        //
        QrL_C.Next;
      end;
      QrL_C.Close;
      UnDmkDAC_PF.AbreQuery(QrL_C, Dmod.MyDB);
      Screen.Cursor := crDefault;
      if QrL_C.RecordCount = 0 then
        Geral.MB_Aviso('Exclus�o realizada com sucesso!');
    finally
      Screen.Cursor := crDefault;
    end;
  end;
*)
  //
{$IfDef DEFINE_VARLCT}
  Filial := QrPediVdaFilial.Value;
  TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  if Geral.MB_Pergunta('Confirma a exclus�o dos ' +
    FormatFloat('0', QrL_C.RecordCount) + ' lan�amentos de comiss�o?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      if UFinanceiro.ExcluiLct_FatNum(nil, QrL_CFatID.Value,
      QrL_CFatNum.Value, QrPediVdaEmpresa.Value, QrL_CCarteira.Value,
      dmkPF.MotivDel_ValidaCodigo(311), False, TabLcta) then
      begin
        // Parei aqui! N�o testei da Associada!
        DModG.ReopenParamsEmp(QrPediVdaEmpresa.Value);
        if DModG.QrParamsEmpAssociada.Value <> 0 then
        begin
          UFinanceiro.ExcluiLct_FatNum(nil, QrL_CFatID.Value,
          QrL_CFatNum.Value, DModG.QrParamsEmpAssociada.Value,
          QrL_CCarteira.Value, dmkPF.MotivDel_ValidaCodigo(311),
          False, TabLcta);
        end;
      end;
    finally
      QrL_C.Close;
      UnDmkDAC_PF.AbreQuery(QrL_C, Dmod.MyDB);
      Screen.Cursor := crDefault;
      if QrL_C.RecordCount = 0 then
        Geral.MB_Aviso('Exclus�o realizada com sucesso!');
    end;
  end;
{$Else}
  if Geral.MB_Pergunta('Confirma a exclus�o dos ' + FormatFloat('0', QrL_C.RecordCount) +
    ' lan�amentos de comiss�o?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      if UFinanceiro.ExcluiLct_FatNum(DMod.MyDB, QrL_CFatID.Value,
      QrL_CFatNum.Value, QrPediVdaEmpresa.Value, QrL_CCarteira.Value, False) then
      begin
        // Parei aqui! N�o testei da Associada!
        DModG.ReopenParamsEmp(QrPediVdaEmpresa.Value);
        if DModG.QrParamsEmpAssociada.Value <> 0 then
        begin
          UFinanceiro.ExcluiLct_FatNum(DMod.MyDB, QrL_CFatID.Value,
          QrL_CFatNum.Value, DModG.QrParamsEmpAssociada.Value,
          QrL_CCarteira.Value, False);
        end;
      end;
    finally
      QrL_C.Close;
      UnDmkDAC_PF.AbreQuery(QrL_C, Dmod.MyDB);
      Screen.Cursor := crDefault;
      if QrL_C.RecordCount = 0 then
        Geral.MB_Aviso('Exclus�o realizada com sucesso!');
    end;
  end;
{$EndIf}
end;

procedure TFmFatDivGer2.Excluifaturamentoatual1Click(Sender: TObject);
var
  Codigo, Pedido: Integer;
begin
  Codigo := QrFatPedCabCodigo.Value;
  Pedido := QrFatPedCabPedido.Value;
  //
  if Geral.MB_Pergunta('Confirma a exclus�o desta emiss�o?') <> ID_YES then Exit;
  //
  if QrFatPedCabCU_PediVda.Value > 0 then
  begin
    UMyMod.ExcluiRegistroInt1('', 'fatpedcab', 'Codigo', Codigo, DMod.MyDB);
  end else begin
    UMyMod.ExcluiRegistroInt1('', 'pedivda', 'Codigo', Pedido, DMod.MyDB);
    UMyMod.ExcluiRegistroInt1('', 'fatpedcab', 'Codigo', Codigo, DMod.MyDB);
  end;
  LocCod(Codigo, Codigo);
end;

procedure TFmFatDivGer2.Excluiitemns1Click(Sender: TObject);
var
  Codi: String;
begin
  Codi := dmkPF.FFP(FmFatDivGer2.QrFatPedCabCodigo.Value, 0);
  //
  DmPediVda.ExcluiItensFaturamento(Codi, QrFatPedIts, DBGFatPedIts);
  //
  DmPediVda.AtualizaTodosItensPediVda_(FmFatDivGer2.QrPediVdaCodigo.Value);
  ReopenFatPedIts(0);
end;

procedure TFmFatDivGer2.Excluivolumeatual1Click(Sender: TObject);
begin
  // N�o pode varios, precisa verificar se tem sub-itens
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrFatPedVol, TDBGrid(DBGFatPedVol),
    'FatPedVol', ['Cnta'], ['Cnta'], istAtual, '');
end;

procedure TFmFatDivGer2.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

function TFmFatDivGer2.MovimentoPossuiNFAutorizadaOuEPEC(FatID, FatNum, Empresa:
  Integer; Avisa: Boolean): Boolean;
var
  NF_Stat, ide_mod, ide_serie, ide_nNF, ide_tpEmis: Integer;
  id: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT FatID, FatNum, id, ide_mod, ide_serie, ',
  'ide_nNF, Status, ide_tpEmis  ',
  'FROM nfecaba ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'ORDER BY Status DESC ',
  ' ']);
  NF_Stat := 0;
  if Dmod.QrAux.RecordCount > 0 then
  begin
    ide_mod    := DMod.QrAux.FieldByName('ide_mod').AsInteger;
    ide_Serie  := DMod.QrAux.FieldByName('ide_serie').AsInteger;
    ide_nNF    := DMod.QrAux.FieldByName('ide_nNF').AsInteger;
    NF_Stat    := DMod.QrAux.FieldByName('Status').AsInteger;
    id         := DMod.QrAux.FieldByName('id').AsString;
    ide_tpEmis := DMod.QrAux.FieldByName('ide_tpEmis').AsInteger;
    //
  end;
  Result := NF_Stat = 100;
  if Result and Avisa then
    Geral.MB_Aviso('Faturamento j� possui NF-e emitida e autorizada:' + sLineBreak +
    'Modelo: ' + Geral.FF0(ide_mod) + sLineBreak +
    'S�rie: ' + Geral.FF0(ide_serie) + sLineBreak +
    'N�mero: ' + Geral.FF0(ide_nNF) + sLineBreak +
    'Chave: ' + id + sLineBreak +
    '');
  if Result = False then
  begin
    if ide_tpEmis = 4 then // EPEC
    begin
      Result := True;
      if Avisa then
        Geral.MB_Aviso('A��o cancelada! NF-e em EPEC:' + sLineBreak +
        'Modelo: ' + Geral.FF0(ide_mod) + sLineBreak +
        'S�rie: ' + Geral.FF0(ide_serie) + sLineBreak +
        'N�mero: ' + Geral.FF0(ide_nNF) + sLineBreak +
        'Chave: ' + id + sLineBreak +
        '');
    end;
  end;
end;

function TFmFatDivGer2.ObtemValorTotalFaturamento(): Double;
var
  OriCodi, OriCnta: Integer;
begin
  Result := 0.00;
  //
  try
    OriCodi := QrFatPedCabCodigo.Value;
    OriCnta := QrFatPedVolCnta.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSMVA, Dmod.MyDB, [
    'SELECT SUM(Total) Total  ',
    'FROM stqmovvala  ',
    'WHERE Tipo=1  ',
    'AND OriCodi=' + Geral.FF0(OriCodi),
    'AND OriCnta=' + Geral.FF0(OriCnta),
    '  ']);
    Result := QrSMVATotal.Value;
  except
    //
  end;
end;

procedure TFmFatDivGer2.Pedido1Click(Sender: TObject);
begin
  QrPediVda.Close;
  //
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrFatPedCab, [PainelDados],
    [PainelEdita], EdPedido, ImgTipo, 'fatpedcab');
  //
  EdAbertura.ValueVariant :=  DModG.ObtemAgora();
end;

procedure TFmFatDivGer2.PMComissaoPopup(Sender: TObject);
var
  Filial: Integer;
  TabLctA: String;
begin
  if Filial <> 0 then
    TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrPediVdaFilial.Value)
  else
    TabLctA := sTabLctErr;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrL_C, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + TabLctA,
  'WHERE FatID=' + FormatFloat('0', VAR_FATID_1801),
  'AND FatNum=' + FormatFloat('0', QrFatPedCabCodigo.Value),
  '']);
  Incluicomisses1.Enabled := QrL_C.RecordCount = 0;
  Excluicomisses1.Enabled := QrL_C.RecordCount > 0;
end;

procedure TFmFatDivGer2.PMEncerraPopup(Sender: TObject);
var
  Habilita, Habil: Boolean;
begin
  Habil    := (QrFatPedIts.State <> dsInactive) and (QrFatPedIts.RecordCount > 0);
  Habilita := (QrFatPedCabEncerrou.Value = 0) and Habil;
  Encerrafaturamento1.Enabled := Habilita;
  Desfazencerramento1.Enabled := not Habilita;
end;

procedure TFmFatDivGer2.PMFaturaPopup(Sender: TObject);
var
  Habil1: Boolean;
begin
  {$IfDef SemPediVda}
    Pedido1.Visible := False;
  {$Else}
    Pedido1.Visible := True;
  {$EndIf}
  //
  Habil1 :=
    (QrFatPedCab.State <> dsInactive) and (QrFatPedCab.RecordCount > 0)
    and (QrFatPedCabEncerrou.Value = 0);
  Alterafaturamentoatual1.Enabled    := Habil1;
  Excluifaturamentoatual1.Enabled    := Habil1 and (QrFatPedVol.RecordCount = 0);
  //Encerrarofaturamentoatual1.Enabled := Habil1;
end;

procedure TFmFatDivGer2.PMItensPopup(Sender: TObject);
begin
  Excluiitemns1.Enabled := (QrFatPedIts.State <> dsInactive) and
    (QrFatPedIts.RecordCount > 0);
end;

procedure TFmFatDivGer2.PMVolumePopup(Sender: TObject);
begin
  Incluinovovolume1.Enabled :=
    (QrFatPedCab.State = dsBrowse)
  and
    (QrFatPedCab.RecordCount > 0);
  Alteravolumeatual1.Enabled :=
    (QrFatPedVol.State = dsBrowse)
  and
    (QrFatPedVol.RecordCount > 0);
  Excluivolumeatual1.Enabled :=
    Alteravolumeatual1.Enabled
  and
    (QrFatPedIts.RecordCount = 0);
end;

procedure TFmFatDivGer2.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFatDivGer2.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFatDivGer2.DBEdfinNFeChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(DBEdfinNFe.Text, F_finNFe, Texto, 0, 1);
  EdDBfinNFe_TXT.Text := Texto;
end;

procedure TFmFatDivGer2.DBEdindPresChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(DBEdindPres.Text, F_indPres, Texto, 0, 1);
  EdDBindPres_TXT.Text := Texto;
end;

procedure TFmFatDivGer2.DBEdit59Change(Sender: TObject);
begin
  if DBEdit59.Text = SFaturamentoAberto then
  begin
    DBEdit59.Font.Color := clRed;
    DBEdit59.Font.Style := [];//[fsBold];
  end else begin
    DBEdit59.Font.Color := clWindowText;
    DBEdit59.Font.Style := [];
  end;
end;

procedure TFmFatDivGer2.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFatDivGer2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFatDivGer2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFatDivGer2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFatDivGer2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFatDivGer2.SpeedButton5Click(Sender: TObject);
{$IfNDef SemPediVda}
var
  Pedido: Integer;
{$EndIf}
begin
{$IfNDef SemPediVda}
  if DBCheck.CriaFm(TFmFatPedPes2, FmFatPedPes2, afmoNegarComAviso) then
  begin
    Pedido := EdPedido.ValueVariant;
    FmFatPedPes2.FEdPedido := EdPedido;
    FmFatPedPes2.ShowModal;
    FmFatPedPes2.Destroy;
    if EdPedido.ValueVariant <> Pedido then
      BtConfirma.SetFocus;
  end;
{$EndIf}
end;

procedure TFmFatDivGer2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFatPedCabCodigo.Value;
  Close;
end;

procedure TFmFatDivGer2.BtVolumeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMVolume, BtVolume);
end;

procedure TFmFatDivGer2.BtFaturaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFatura, BtFatura);
end;

procedure TFmFatDivGer2.BtFisRegCadClick(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  Codigo, FisRegCad: Integer;
begin
  Codigo    := QrFatPedCabCodigo.Value;
  FisRegCad := QrPediVdaRegrFiscal.Value;
  //
  Grade_Jan.MostraFormFisRegCad(FisRegCad);
  try
    Screen.Cursor := crHourGlass;
    LocCod(Codigo, Codigo);
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmFatDivGer2.BtGraGruNClick(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  Codigo, Nivel1: Integer;
begin
  Codigo := QrFatPedCabCodigo.Value;
  Nivel1 := QrFatPedItsCO_NIVEL1.Value;
  //
  Grade_Jan.MostraFormGraGruN(Nivel1);
  try
    Screen.Cursor := crHourGlass;
    LocCod(Codigo, Codigo);
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmFatDivGer2.Alteradadosespecficosdoitemselecionado1Click(
  Sender: TObject);
var
  Empresa, Tipo, IDCtrl, OriCodi, OriCtrl: Integer;
  Qtde, Preco, Total: Double;
  obsCont_xCampo, obsCont_xTexto, obsFisco_xCampo, obsFisco_xTexto: String;
begin
  Empresa          := QrFatPedCabEmpresa.Value;
  Tipo             := QrFatPedItsTipo.Value;
  IDCtrl           := QrFatPedItsIDCtrl.Value;
  OriCodi          := QrFatPedItsOriCodi.Value;
  OriCtrl          := QrFatPedItsOriCtrl.Value;
  //if
  Grade_PF.ObtemStqMovValA_Many(QrFatPedCabCodigo.Value, OriCtrl,
  QrFatPedVolCnta.Value, IDCtrl, Empresa, Qtde, Preco, Total,
  obsCont_xCampo, obsCont_xTexto, obsFisco_xCampo, obsFisco_xTexto);
  //
  GFat_Jan.MostraFormFatDivItemAlt(QrFatPedItsIDCtrl.Value,
  QrFatPedItsGraGruX.Value, QrFatPedItsNO_NIVEL1.Value, QrFatPedItsNO_COR.Value,
  QrFatPedItsNO_TAM.Value, Total, QrFatPedItsvFrete.Value,
  QrFatPedItsvSeg.Value, QrFatPedItsvDesc.Value, QrFatPedItsvOutro.Value,
  obsCont_xCampo, obsCont_xTexto, obsFisco_xCampo, obsFisco_xTexto,
  Tipo, OriCodi, OriCtrl, Empresa);
  //
  ReopenFatPedIts(OriCtrl);
end;

procedure TFmFatDivGer2.AlteraFaturamentoAtual();
begin
  if QrFatPedCabCU_PediVda.Value > 0 then
  begin
    QrPediVda.Close;
    //
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrFatPedCab,
      [PainelDados], [PainelEdita], EdPedido, ImgTipo, 'fatpedcab');
  end else
  begin
    GFat_Jan.MostraFormFatDivCab2(stUpd, QrFatPedCab, QrPediVda, QrFatPedVol);
  end;
  QrPediVda.Close;
  UnDmkDAC_PF.AbreQuery(QrPediVda, Dmod.MyDB);
end;

procedure TFmFatDivGer2.Alterafaturamentoatual1Click(Sender: TObject);
begin
  AlteraFaturamentoAtual();
end;

procedure TFmFatDivGer2.Alteravolumeatual1Click(Sender: TObject);
begin
  GFat_Jan.MostraFormFatPedVol2(stUpd, QrFatPedVol, DsFatPedCab);
end;

procedure TFmFatDivGer2.Anlise1Click(Sender: TObject);
begin
  QrAnalise.Close;
  QrAnalise.Params[0].AsInteger := QrFatPedCabCodigo.Value;
  UMyMod.AbreQuery(QrAnalise, Dmod.MyDB, 'TFmFatDivGer2.Anlise1Click()');
  //
  MyObjects.frxMostra(frxFAT_PEDID_001_00, 'An�lise de faturamento');
end;

procedure TFmFatDivGer2.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmFatDivGer2.EncerraFaturamento();
{$IfNDef SemNFe_0000}
const
  Tipo = 1;
var
  Especie: String;
  Quantidade: Integer;
begin
  //
  UnNFe_PF.EncerraFaturamento(QrFatPedCabFatSemEstq.Value,
    QrFatPedCabCodigo.Value, QrFatPedVolCnta.Value, Tipo,
    QrFatPedCabRegrFiscal.Value, 'fatpedcab');
  //
  LocCod(QrFatPedCabCodigo.Value, QrFatPedCabCodigo.Value);
{
(*&*)
  if not DmProd.VerificaEstoqueTodoFaturamento(QrFatPedCabFatSemEstq.Value,
    QrFatPedCabCodigo.Value, QrFatPedVolCnta.Value, 1, QrFatPedCabRegrFiscal.Value)
  then
    Exit;
  //
  if DBCheck.CriaFm(TFmNFaEdit, FmNFaEdit, afmoNegarComAviso) then
  begin
    DmPediVda.ReopenFatPedCab(QrFatPedCabCodigo.Value, True);
    //FmNFaEdit.ReopenFatPedCab(QrFatPedCabCodigo.Value);
    //
    FmNFaEdit.ReopenFatPedNFs(1,0);

    //

    if not DmNFe_0000.Obtem_Serie_e_NumNF_Novo(
    QrFatPedCabSerieDesfe.Value,
    QrFatPedCabNFDesfeita.Value,
    FmNFaEdit.QrImprimeSerieNF_Normal.Value,
    FmNFaEdit.QrImprimeCtrl_nfs.Value,
    QrFatPedCabEmpresa.Value,
    FmNFaEdit.QrFatPedNFs.FieldByName('Filial').AsInteger,
    FmNFaEdit.QrImprimeMaxSeqLib.Value,
    FmNFaEdit.EdSerieNF, FmNFaEdit.EdNumeroNF(*),
    SerieNFTxt, NumeroNF*)) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;

    Especie := '';
    Quantidade := 0;
    //
    QrVolumes.Close;
    QrVolumes.Params[00].AsInteger := QrFatPedCabCodigo.Value;
    UMyMod.AbreQuery(QrVolumes, Dmod.MyDB, 'TFmFatDivGer2.BtEncerraClick()');
    QrVolumes.First;
    while not QrVolumes.Eof do
    begin
      Quantidade := Quantidade + Trunc(QrVolumesVolumes.Value);
      if Especie <> '' then
        Especie := Especie + ' + ';
      Especie := Especie + ' ' +
        // tirado em 2010-08-13
        //Geral.FF0(QrVolumesVolumes.Value) + ' ' +
        QrVolumesNO_UnidMed.Value;
      //
      QrVolumes.Next;
    end;
    FmNFaEdit.EdQuantidade.ValueVariant := FloatToStr(Quantidade);
    FmNFaEdit.EdEspecie.ValueVariant := Especie;
    //
    FmNFaEdit.EdNumeroNF.Enabled := (FmNFaEdit.QrImprimeIncSeqAuto.Value = 0);
    //
    (*
    FmNFaEdit.QrCFOP.Close;
    FmNFaEdit.QrCFOP.Params[00].AsInteger := FThisFatID;
    FmNFaEdit.QrCFOP.Params[01].AsInteger := FmFatPedCab.QrFatPedCabCodigo.Value;
    FmNFaEdit.QrCFOP.Params[02].AsInteger := FmFatPedCab.QrFatPedCabEmpresa.Value;
    UMyMod.AbreQuery(FmNFaEdit.QrCFOP, 'TFmFatDivGer2.BtEncerraClick()');
    FmNFaEdit.EdCFOP1.Text := FmNFaEdit.QrCFOPCFOP.Value;
    FmNFaEdit.CBCFOP1.KeyValue := FmNFaEdit.QrCFOPCFOP.Value;
    *)
    //
    //
    // NF-e 2.00
    if (DModG.QrParamsEmpCRT.Value = 3) and
    (DModG.QrParamsEmpNFeNT2013_003LTT.Value < 2) then
      FmNFaEdit.PageControl1.ActivePageIndex := 0
    else
      FmNFaEdit.PageControl1.ActivePageIndex := 1;    
    //
    // NF-e 2.00
    FmNFaEdit.EdHrEntraSai.ValueVariant := Time();
    FmNFaEdit.Eddest_email.Text         := DmodG.ObtemPrimeiroEMail_NFe(QrFatPedCabEmpresa.Value, QrFatPedCabCliente.Value);
    // fazer aqui logo ap�s a reabertura do QrParamsEmp
    FmNFaEdit.RGCRT.ItemIndex           := DmodG.QrParamsEmpCRT.Value;
    FmNFaEdit.EdVagao.Text              := ''; // Parei aqui # 2.00
    FmNFaEdit.EdBalsa.Text              := ''; // Parei aqui # 2.00
    // fim NF-e 2.00
    //
    FmNFaEdit.EdCompra_XNEmp.Text := '';
    FmNFaEdit.EdCompra_XPed.Text := QrFatPedCabPedidoCli.Value;
    FmNFaEdit.EdCompra_XCont.Text := '';
    //
    FmNFaEdit.ReopenStqMovValX(0);
    FmNFaEdit.ImgTipo.SQLType := stIns;
    FmNFaEdit.ShowModal;
    FmNFaEdit.Destroy;
    // Reabrir de novo!
    LocCod(QrFatPedCabCodigo.Value, QrFatPedCabCodigo.Value);
    //
    if QrFatPedCabEncerrou.Value > 0 then
    begin
      FmPrincipal.MostraFormFatPedNFs(QrFatPedCabEMP_FILIAL.Value,
      QrFatPedCabCliente.Value, QrFatPedCabCU_PediVda.Value, True);
      (*
      if DBCheck.CriaFm(TFmFatPedNFs, FmFatPedNFs, afmoNegarComAviso) then
      begin
        FmFatPedNFs.EdFilial.ValueVariant  := QrFatPedCabEMP_FILIAL.Value;
        FmFatPedNFs.CBFilial.KeyValue      := QrFatPedCabEMP_FILIAL.Value;
        FmFatPedNFs.EdCliente.ValueVariant := QrFatPedCabCliente.Value;
        FmFatPedNFs.CBCliente.KeyValue     := QrFatPedCabCliente.Value;
        FmFatPedNFs.EdPedido.ValueVariant  := QrFatPedCabCU_PediVda.Value;
        FmFatPedNFs.ShowModal;
        FmFatPedNFs.Destroy;
      end;
      *)
    end;
  end;
&*)
}
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappNFe);
{$EndIf}
end;

procedure TFmFatDivGer2.Encerrafaturamento1Click(Sender: TObject);
begin
  EncerraFaturamento();
end;

procedure TFmFatDivGer2.EncerrafaturamentoModoRpido1Click(Sender: TObject);
begin
  EmiteNFe_ModoRapido();
end;

procedure TFmFatDivGer2.BtComissaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMComissao, BtComissao);
end;

procedure TFmFatDivGer2.BtConfirmaClick(Sender: TObject);
var
  Codigo, Pedido, CodUsu: Integer;
  // Balan�o aberto
  PrdGrupTip, Empresa, FisRegCad: Integer;
  Abertura: String;
begin
  Pedido := Geral.IMV(EdPedido.Text);
  if Pedido = 0 then
  begin
    Geral.MB_Erro('Defina o pedido!');
    Exit;
  end else begin
    Pedido := QrPediVdaCodigo.Value;
  end;
  PrdGrupTip := 1;
  Empresa    := QrPediVdaEmpresa.Value;
  FisRegCad  := QrPediVdaRegrFiscal.Value;
  Abertura   := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
  // Verifica se existe balan�o aberto
  if DmProd.ExisteBalancoAberto_StqCen_Mul(PrdGrupTip, Empresa, FisRegCad) then Exit;
  Codigo := UMyMod.BuscaEmLivreY_Def('FatPedCab', 'Codigo', ImgTipo.SQLType,
    QrFatPedCabCodigo.Value);
  CodUsu := DModG.BuscaProximoCodigoInt('controle', 'StqMovUsu', '',
    EdCodUsu.ValueVariant);
  EdCodUsu.ValueVariant := CodUsu;
{
  N�o � poss�vel por causa do Pedido!
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmFatPedCab, PainelEdit,
    'FatPedCab', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
}
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'fatpedcab', False, [
  'CodUsu', 'Pedido', 'Abertura'
  {
  'Serie', 'NF', 'PIS#Per', 'PIS#Val',
  'COFINS#Per', 'COFINS#Val', 'IR#Per', 'IR#Val', 'CS#Per', 'CS#Val',
  'ISS#Per', 'ISS#Val'
  }
  ], ['Codigo'], [
  CodUsu, Pedido, Abertura
  {Serie, NF, PIS#Per, PIS#Val,
  COFINS#Per, COFINS#Val, IR#Per, IR#Val, CS#Per, CS#Val,
  ISS#Per, ISS#Val
  }
  ], [Codigo], True) then
  begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
    ImgTipo.SQLType := stLok;
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmFatDivGer2.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'FatPedCab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'FatPedCab', 'Codigo');
  MostraEdicao(0, stLok, 0);
  //
  ReopenPediVda(QrFatPedCabCU_PediVda.Value);
end;

procedure TFmFatDivGer2.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmFatDivGer2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  // Deve ser antes do CriaOForm!
{$IfNDef SemNFe_0000}
  F_indPres := UnNFe_PF.ListaIndicadorDePresencaComprador();
  F_finNFe  := UnNFe_PF.ListaFinalidadeDeEmiss�oDaNFe(True);
{$Else}
  F_indPres := 0;
  F_finNFe  := 0;
{$EndIf}
  //
  CriaOForm;
  //
  FMultiGrandeza   := False;
  FThisFatID       := 1;
  PainelEdit.Align := alClient;
  Panel4.Align     := alClient;
end;

procedure TFmFatDivGer2.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFatPedCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFatDivGer2.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmFatDivGer2.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFatDivGer2.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFatPedCabCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmFatDivGer2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFatDivGer2.QrCliAfterOpen(DataSet: TDataSet);
var
  Entidade: Integer;
begin
  if (QrCliL_Ativo.Value = 1) and (QrPediVdaEntregaEnti.Value = 0) then
  begin
    QrEntregaCli.Close;
    QrEntregaCli.Params[00].AsInteger := QrPediVdaCliente.Value;
    UMyMod.AbreQuery(QrEntregaCli, Dmod.MyDB, 'TFmFatDivGer2.QrCliAfterOpen()');
    //
    DsEntregaEnti.DataSet := QrEntregaCli;
  end else
  begin
    if QrPediVdaEntregaEnti.Value = 0 then
      Entidade := QrPediVdaCliente.Value
    else
      Entidade := QrPediVdaEntregaEnti.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEntregaEnti, Dmod.MyDB, [
    'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, Respons2, ',
    'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, ',
    'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, ',
    'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, ',
    'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, ',
    'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, ',
    'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, ',
    'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, ',
    'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, ',
    'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, ',
    'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, ',
    'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, ',
    'mun.Nome CIDADE, ',
    'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, ',
    'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, ',
    'pai.Nome Pais, ',
    'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, ',
    'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, ',
    'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, ',
    'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, ',
    'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, ',
    'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, ',
    'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, ',
    'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, ',
    'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, ',
    'RG, SSP, DataRG, IE ',
    'FROM entidades en ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(en.Tipo=0, en.ECodMunici, en.PCodMunici) ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(en.Tipo=0, en.ECodiPais, en.PCodiPais) ',
    'WHERE en.Codigo=' + Geral.FF0(Entidade),
    'ORDER BY NOME_ENT',
    '']);
    DsEntregaEnti.DataSet := QrEntregaEnti;
  end;
end;

procedure TFmFatDivGer2.QrCliBeforeClose(DataSet: TDataSet);
begin
  QrEntregaEnti.Close;
  QrEntregaCli.Close;
end;

procedure TFmFatDivGer2.QrCliCalcFields(DataSet: TDataSet);
begin
  QrCliTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrCliTe1.Value);
  QrCliFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrCliFax.Value);
  QrCliCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrCliCNPJ_CPF.Value);
  QrCliNOME_TIPO_DOC.Value :=
    dmkPF.ObtemTipoDocTxtDeCPFCNPJ(QrCliCNPJ_CPF.Value) + ':';
  QrCliNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrCliRua.Value, Trunc(QrCliNumero.Value), False);
  //
  QrCliE_ALL.Value := Uppercase(QrCliNOMELOGRAD.Value);
  if Trim(QrCliE_ALL.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ';
  QrCliE_ALL.Value := QrCliE_ALL.Value + Uppercase(QrCliRua.Value);
  if Trim(QrCliRua.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNUMERO_TXT.Value;
  if Trim(QrCliCompl.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ' + Uppercase(QrCliCompl.Value);
  if Trim(QrCliBairro.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliBairro.Value);
  if QrCliCEP.Value > 0 then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrCliCEP.Value);
  if Trim(QrCliCidade.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliCidade.Value);
  if Trim(QrCliNOMEUF.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNOMEUF.Value;
  if Trim(QrCliPais.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + QrCliPais.Value;
  //
  //QrCliCEP_TXT.Value :=Geral.FormataCEP_NT(QrCliCEP.Value);
  //
end;

procedure TFmFatDivGer2.QrEntregaEntiCalcFields(DataSet: TDataSet);
begin
  QrEntregaEntiTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaEntiTe1.Value);
  QrEntregaEntiFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaEntiFax.Value);
  {
  QrEntregaEntiCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
  QrEntregaEntiNOME_TIPO_DOC.Value :=
    dmkPF.ObtemTipoDocTxtDeCPFCNPJ(QrEntregaEntiCNPJ_CPF.Value) + ':';
  }
  QrEntregaEntiNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEntregaEntiRua.Value, QrEntregaEntiNumero.Value, False);
  //
  QrEntregaEntiE_ALL.Value := QrEntregaEntiNOME_ENT.Value;
  case QrEntregaEntiTipo.Value of
    0: QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + ' CNPJ: ' +
       Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
    1: QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + ' CPF: ' +
       Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
  end;
  if Trim(QrEntregaEntiIE.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' I.E. ' + QrEntregaEntiIE.Value;
  if Trim(QrEntregaEntiTE1.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' Fone: ' + QrEntregaEntiTE1_TXT.Value;
  if Trim(QrEntregaEntiEMAIL.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' Email: ' + QrEntregaEntiEMAIL.Value;

  QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + sLineBreak +
    Uppercase(QrEntregaEntiNOMELOGRAD.Value);
  if Trim(QrEntregaEntiE_ALL.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' ';
  QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + Uppercase(QrEntregaEntiRua.Value);
  if Trim(QrEntregaEntiRua.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ', ' + QrEntregaEntiNUMERO_TXT.Value;
  if Trim(QrEntregaEntiCompl.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' ' + Uppercase(QrEntregaEntiCompl.Value);
  if Trim(QrEntregaEntiBairro.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + Uppercase(QrEntregaEntiBairro.Value);
  if QrEntregaEntiCEP.Value > 0 then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEntregaEntiCEP.Value);
  if Trim(QrEntregaEntiCidade.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + Uppercase(QrEntregaEntiCidade.Value);
  if Trim(QrEntregaEntiNOMEUF.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ', ' + QrEntregaEntiNOMEUF.Value;
  if Trim(QrEntregaEntiPais.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + QrEntregaEntiPais.Value;
  //
  //QrEntregaEntiCEP_TXT.Value :=Geral.FormataCEP_NT(QrEntregaEntiCEP.Value);
  //
  //if Trim(QrEntregaEntiE_ALL.Value) = '' then
    //QrEntregaEntiE_ALL.Value := QrCliE_ALL.Value;
end;

procedure TFmFatDivGer2.QrEntregaCliCalcFields(DataSet: TDataSet);
begin
  QrEntregaCliTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaCliTe1.Value);
(*
  QrEntregaCliFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaCliFax.Value);
*)
  {
  QrEntregaCliCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEntregaCliCNPJ_CPF.Value);
  QrEntregaCliNOME_TIPO_DOC.Value :=
    dmkPF.ObtemTipoDocTxtDeCPFCNPJ(QrEntregaCliCNPJ_CPF.Value) + ':';
  }
  QrEntregaCliNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEntregaCliRua.Value, QrEntregaCliNumero.Value, False);
  //
/////////////////////////////////////////////////////////////////////////////////
  QrEntregaCliE_ALL.Value := QrEntregaCliL_Nome.Value;
  if QrEntregaCliL_CNPJ.Value <> '' then
    QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + ' CNPJ: ' +
    Geral.FormataCNPJ_TT(QrEntregaCliL_CNPJ.Value)
  else
    QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + ' CPF: ' +
    Geral.FormataCNPJ_TT(QrEntregaCliL_CPF.Value);
  if Trim(QrEntregaCliL_IE.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' I.E. ' + QrEntregaCliL_IE.Value;
  if Trim(QrEntregaCliTE1.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' Fone: ' + QrEntregaCliTE1_TXT.Value;
  if Trim(QrEntregaCliEMAIL.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' Email: ' + QrEntregaCliEMAIL.Value;

  QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + sLineBreak +
    Uppercase(QrEntregaCliNOMELOGRAD.Value);
  if Trim(QrEntregaCliE_ALL.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' ';
  QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + Uppercase(QrEntregaCliRua.Value);
  if Trim(QrEntregaCliRua.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ', ' + QrEntregaCliNUMERO_TXT.Value;
  if Trim(QrEntregaCliCompl.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' ' + Uppercase(QrEntregaCliCompl.Value);
  if Trim(QrEntregaCliBairro.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + Uppercase(QrEntregaCliBairro.Value);
  if QrEntregaCliCEP.Value > 0 then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEntregaCliCEP.Value);
  if Trim(QrEntregaCliNO_MUNICI.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + Uppercase(QrEntregaCliNO_MUNICI.Value);
  if Trim(QrEntregaCliNOMEUF.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ', ' + QrEntregaCliNOMEUF.Value;
  if Trim(QrEntregaCliNO_PAIS.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + QrEntregaCliNO_PAIS.Value;
  //
  //QrEntregaCliCEP_TXT.Value :=Geral.FormataCEP_NT(QrEntregaCliCEP.Value);
  //
  //if Trim(QrEntregaCliE_ALL.Value) = '' then
    //QrEntregaCliE_ALL.Value := QrCliE_ALL.Value;
end;

procedure TFmFatDivGer2.QrFatPedCabAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  QueryPrincipalAfterOpen;
  Habilita := QrFatPedCab.RecordCount > 0;
  BtVolume.Enabled  := Habilita and (QrFatPedCabEncerrou.Value = 0);
  BtEncerra.Enabled := Habilita;
  //
  if CO_DMKID_APP = 2 then //Bluederm
    BtComissao.Visible := Habilita and (QrFatPedCabEncerrou.Value = 0)
  else
    BtComissao.Visible := False;
  //
  //Geral.MB_Info(QrFatPedCab.SQL.Text);
end;

procedure TFmFatDivGer2.QrFatPedCabAfterScroll(DataSet: TDataSet);
begin
  ReopenFatPedVol(0);
  ReopenPediVda(QrFatPedCabCU_PediVda.Value);
end;

procedure TFmFatDivGer2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatDivGer2.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFatPedCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'FatPedCab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFatDivGer2.Sempedido1Click(Sender: TObject);
begin
  GFat_Jan.MostraFormFatDivCab2(stIns);
end;

procedure TFmFatDivGer2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatDivGer2.Incluicomisses1Click(Sender: TObject);
begin
  CriaLctsComissoes();
end;

procedure TFmFatDivGer2.Incluiitenss1Click(Sender: TObject);
begin
  if MyObjects.FIC(QrFatPedCabRegrFiscal.Value = 0, nil,
    'Regra fiscal n�o definida no pedido!') then Exit;
  //
  if QrFatPedCabCU_PediVda.Value > 0 then
  begin
{$IfNDef SemPediVda}
    UmyMod.FormInsUpd_Show(TFmFatPedIts2, FmFatPedIts2, afmoNegarComAviso,
      QrFatPedIts, stIns);
{$EndIf}
  end else
  if QrPediVdaUsaReferen.Value = 1 then
  begin
    DmPediVda.ReopenParamsEmp(QrPediVdaEmpresa.Value, True);
    if DBCheck.CriaFm(TFmFatDivRef2, FmFatDivRef2, afmoNegarComAviso) then
    begin
      FmFatDivRef2.FMultiGrandeza := FMultiGrandeza;
      FmFatDivRef2.ReopenFatDivRef(0);
      //FmFatDivRef2.TbFatDivRef.Database := DmodG.MyPID_DB;
      FmFatDivRef2.TbFatDivRef.Close;
      UnDmkDAC_PF.AbreTable(FmFatDivRef2.TbFatDivRef, Dmod.MyDB);
      //FmFatDivRef2.TbFatDivRef.Refresh;
      //FmFatDivRef2.ImgTipo.SQLType := stIns;
      //
      FmFatDivRef2.ShowModal;
      FmFatDivRef2.Destroy;
    end;
  end else
  if QrFatPedCabCU_PediVda.Value < 0 then
  begin
    DmPediVda.ReopenParamsEmp(QrPediVdaEmpresa.Value, True);
    if DBCheck.CriaFm(TFmFatDivGru2, FmFatDivGru2, afmoNegarComAviso) then
    begin
      FmFatDivGru2.ImgTipo.SQLType := stIns;
      //
      FmFatDivGru2.ShowModal;
      FmFatDivGru2.Destroy;
    end;
  end;
end;

procedure TFmFatDivGer2.IncluiNovoVolume();
var
  Continua: Boolean;
begin
  Continua := GFat_Jan.MostraFormFatPedVol2(stIns, QrFatPedVol);
  //
  if Continua = True then
    Incluiitenss1Click(Self);
end;

procedure TFmFatDivGer2.Incluinovovolume1Click(Sender: TObject);
begin
  IncluiNovoVolume();
end;

procedure TFmFatDivGer2.QrFatPedCabBeforeClose(DataSet: TDataSet);
begin
  QrFatPedIts.Close;
  QrPediVda.Close;
  //
  BtVolume.Enabled   := False;
  BtEncerra.Enabled  := False;
  BtComissao.Visible := False;
end;

procedure TFmFatDivGer2.QrFatPedCabBeforeOpen(DataSet: TDataSet);
begin
  QrFatPedCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFatDivGer2.QrFatPedCabCalcFields(DataSet: TDataSet);
begin
  if QrFatPedCabEncerrou.Value = 0 then
    QrFatPedCabENCERROU_TXT.Value := SFaturamentoAberto
  else
    QrFatPedCabENCERROU_TXT.Value := Geral.FDT(QrFatPedCabEncerrou.Value, 0);
  //
  if QrFatPedCabCU_PediVda.Value < 1 then
    QrFatPedCabPEDIDO_TXT.Value := 'SEM PEDIDO'
  else
    QrFatPedCabPEDIDO_TXT.Value :=
    FormatFloat('#,###,###,###', QrFatPedCabCU_PediVda.Value);
end;

procedure TFmFatDivGer2.QrFatPedVolAfterOpen(DataSet: TDataSet);
begin
  BtItens.Enabled := (QrFatPedVol.RecordCount > 0) and
    (QrFatPedCabEncerrou.Value = 0);
end;

procedure TFmFatDivGer2.QrFatPedVolAfterScroll(DataSet: TDataSet);
begin
  ReopenFatPedIts(0);
end;

procedure TFmFatDivGer2.QrFatPedVolBeforeClose(DataSet: TDataSet);
begin
  QrFatPedIts.Close;
  BtItens.Enabled := False;
end;

procedure TFmFatDivGer2.QrPediVdaAfterOpen(DataSet: TDataSet);
begin
  QrCli.Close;
  QrCli.Params[00].AsInteger := QrPediVdaCliente.Value;
  UMyMod.AbreQuery(QrCli, Dmod.MyDB, 'TFmFatDivGer2.QrPediVdaAfterOpen()');
end;

procedure TFmFatDivGer2.QrPediVdaBeforeClose(DataSet: TDataSet);
begin
  QrCli.Close;
end;

procedure TFmFatDivGer2.QrPediVdaCalcFields(DataSet: TDataSet);
begin
  QrPediVdaNOMEFRETEPOR.Value := dmkPF.FretePor_Txt(QrPediVdaFretePor.Value);
end;

{?
procedure TFmFatDivGer2.ReopenEndContratante(Cliente: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEndContratante, Dmod.MyDB, [
  'SELECT mun.Nome NO_CIDADE, pai.Nome NO_PAIS, ent.Codigo, ent.Tipo,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATANTE,',
  'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,',
  'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA,',
  'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO,',
  'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL,',
  'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO,',
  '/*IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,*/',
  'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,',
  'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,',
  'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,',
  'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL,',
  'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX,',
  'IF(ent.Tipo=0, ent.ECep, ent.PCEP) + 0.000 CEP',
  'FROM entidades ent',
  'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF',
  'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF',
  'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd',
  'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici)',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais)',
  'WHERE ent.Codigo=' + Geral.FF0(Cliente),
  'ORDER BY CONTRATANTE',
  '']);
end;
?}

procedure TFmFatDivGer2.ReopenFatPedIts(OriCtrl: Integer);
var
  OriCodi, OriCnta: Integer;
begin
  OriCodi := QrFatPedCabCodigo.Value;
  OriCnta := QrFatPedVolCnta.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFatPedIts, Dmod.MyDB, [
    'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1, ',
    'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR, ',
    'gti.Nome NO_TAM, ggx.GraGru1, smi.Qtde QTDE_POSITIVO, ',
    'smi.*, ELT(smi.Baixa+2,"Subtrai","Nulo","Adiciona") CALC_TXT, ',
    'gg1.Nivel1 CO_NIVEL1, pgt.MadeBy, gg1.IPI_Alq, smi.Pecas, ',
    'smi.AreaM2, smi.AreaP2, smi.Peso, gg1.prod_indTot ',
    'FROM stqmovitsa smi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX ',
    'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
    'WHERE smi.Tipo=1 ',
    'AND smi.OriCodi=' + Geral.FF0(OriCodi),
    'AND smi.OriCnta=' + Geral.FF0(OriCnta),
    ' ',
    'UNION ',
    ' ',
    'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1, ',
    'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR, ',
    'gti.Nome NO_TAM, ggx.GraGru1, smi.Qtde QTDE_POSITIVO, ',
    'smi.*, ELT(smi.Baixa+2,"Subtrai","Nulo","Adiciona") CALC_TXT, ',
    'gg1.Nivel1 CO_NIVEL1, pgt.MadeBy, gg1.IPI_Alq, smi.Pecas, ',
    'smi.AreaM2, smi.AreaP2, smi.Peso, gg1.prod_indTot ',
    'FROM stqmovitsb smi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX ',
    'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
    'WHERE smi.Tipo=1 ',
    'AND smi.OriCodi=' + Geral.FF0(OriCodi),
    'AND smi.OriCnta=' + Geral.FF0(OriCnta),
    '']);
    //
  if OriCtrl <> 0 then
    QrFatPedIts.Locate('OriCtrl', OriCtrl, []);
end;

procedure TFmFatDivGer2.ReopenFatPedVol(Cnta: Integer);
begin
  QrFatPedVol.Close;
  QrFatPedVol.Params[0].AsInteger := QrFatPedCabCodigo.Value;
  UMyMod.AbreQuery(QrFatPedVol, Dmod.MyDB, 'TFmFatDivGer2.ReopenFatPedVol()');
  //
  if Cnta <> 0 then
    QrFatPedVol.Locate('Cnta', Cnta, []);
end;

procedure TFmFatDivGer2.ReopenPediVda(Pedido: Integer);
begin
(*
  QrPediVda.Close;
  QrPediVda.Params[0].AsInteger := Pedido;
  UMyMod.AbreQuery(QrPediVda, Dmod.MyDB, 'TFmFatDivGer2.ReopenPediVda()');
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediVda, Dmod.MyDB, [
  'SELECT ',
  'pvd.Codigo, pvd.CodUsu, pvd.Empresa, pvd.Cliente, ',
  'pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi, ',
  'pvd.Prioridade, pvd.CondicaoPG, pvd.Moeda, ',
  'pvd.Situacao, pvd.TabelaPrc, pvd.MotivoSit, pvd.LoteProd, ',
  'pvd.PedidoCli, pvd.FretePor, pvd.Transporta, pvd.Redespacho, ',
  'pvd.RegrFiscal, pvd.DesoAces_V, pvd.DesoAces_P, ',
  'pvd.Frete_V, pvd.Frete_P, pvd.Seguro_V, pvd.Seguro_P, ',
  'pvd.TotalQtd, pvd.Total_Vlr, pvd.Total_Des, pvd.Total_Tot, ',
  'pvd.Observa, tpc.Nome NOMETABEPRCCAD, ',
  'mda.Nome NOMEMOEDA, mda.CodUsu CODUSU_MDA, ',
  'pvd.Represen, pvd.ComisFat, pvd.ComisRec, pvd.CartEmis, ',
  'pvd.AFP_Sit, pvd.AFP_Per, ppc.MedDDSimpl, MedDDReal, ',
  'pvd.ValLiq, pvd.QuantP, pvd.UsaReferen, ',
  // NFe 3.10
  'pvd.idDest, pvd.indFinal, pvd.indPres, pvd.indSinc, pvd.finNFe, ',
  // NFe 4.00 NT 2018/05
  'pvd.RetiradaUsa, pvd.RetiradaEnti,',
  'pvd.EntregaUsa, pvd.EntregaEnti,',
  //
  'frc.Nome NOMEFISREGCAD, ',
  'imp.Nome NOMEMODELONF, imp.Codigo MODELO_NF, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP, ',
  'IF(emp.Tipo=0, emp.EUF, emp.PUF) + 0.000 EMP_UF, ',
  'IF(ven.Tipo=0, ven.RazaoSocial, ven.NOME) NOMEACC, ',
  'IF(tra.Tipo=0, tra.RazaoSocial, tra.NOME) NOMETRANSP, ',
  'IF(red.Tipo=0, red.RazaoSocial, red.NOME) NOMEREDESP, ',
  // NFe 4.00 NT 2018/05
  'IF(ret.Tipo=0, ret.RazaoSocial, ret.NOME) NOMEENTRETIRADA, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.NOME) NOMEENTENTREGA, ',
  //
  'emp.Filial, ',
  'car.Nome NOMECARTEMIS, car.Tipo TIPOCART, ',
  'ppc.Nome NOMECONDICAOPG, mot.Nome NOMEMOTIVO, ',
  'ven.CodUsu CODUSU_ACC, tra.CodUsu CODUSU_TRA, ',
  'red.CodUsu CODUSU_RED, mot.CodUsu CODUSU_MOT, ',
  'tpc.CodUsu CODUSU_TPC, ',
  'ppc.CodUsu CODUSU_PPC, tpc.JurosMes, ',
  'frc.CodUsu CODUSU_FRC ',
  'FROM pedivda pvd ',
  'LEFT JOIN entidades  emp ON emp.Codigo=pvd.Empresa ',
  'LEFT JOIN entidades  tra ON tra.Codigo=pvd.Transporta ',
  'LEFT JOIN entidades  red ON red.Codigo=pvd.Redespacho ',
  'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc ',
  'LEFT JOIN cambiomda  mda ON mda.Codigo=pvd.Moeda ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG ',
  'LEFT JOIN motivos    mot ON mot.Codigo=pvd.MotivoSit ',
  'LEFT JOIN pediacc    acc ON acc.Codigo=pvd.Represen ',
  'LEFT JOIN entidades  ven ON ven.Codigo=acc.Codigo ',
  'LEFT JOIN carteiras  car ON car.Codigo=pvd.CartEmis ',
  'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal ',
  'LEFT JOIN imprime    imp ON imp.Codigo=frc.ModeloNF ',
  // NFe 4.00 NT 2018/05
  'LEFT JOIN entidades  ret ON ret.Codigo=pvd.RetiradaEnti ',
  'LEFT JOIN entidades  ent ON ent.Codigo=pvd.EntregaEnti',
  //
  'WHERE pvd.CodUsu=' + Geral.FF0(Pedido),
  '']);
end;

procedure TFmFatDivGer2.Romaneioporgrupo1Click(Sender: TObject);
begin
{$IfNDef NAO_GPED}
  if DBCheck.CriaFm(TFmFatPedImp, FmFatPedImp, afmoNegarComAviso) then
  begin
    FmFatPedImp.FFatID          := FThisFatID;
    FmFatPedImp.FEmpresa        := QrFatPedCabEmpresa.Value;
    FmFatPedImp.FOriCodigo      := QrFatPedCabCodigo.Value;
    FmFatPedImp.FPedido         := QrPediVdaCodUsu.Value;
    FmFatPedImp.FTituloPedido   := 'Pedido Normal';
    FmFatPedImp.FCliente        := QrPediVdaCliente.Value;
    FmFatPedImp.FNomeCondicaoPg := QrPediVdaNOMECONDICAOPG.Value;
    FmFatPedImp.FTipoMov        := 1; // sempre saida
    FmFatPedImp.ShowModal;
    FmFatPedImp.Destroy;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
end;

procedure TFmFatDivGer2.CriaLctsComissoes();
begin
{
  if MyObjects.CriaForm_AcessoTotal(TFmFatDivCms, FmFatDivCms) then
  begin
    FmFatDivCms.FFatNum := QrFatPedCabCodigo.Value;
    FmFatDivCms.FIDDuplicata := QrFatPedCabCodUsu.Value;
    FmFatDivCms.FVendedor := QrPediVdaRepresen.Value;
    FmFatDivCms.FEntidade := QrFatPedCabEmpresa.Value;
    FmFatDivCms.FCodUsu := QrFatPedCabCodUsu.Value;
    FmFatDivCms.FCondicaoPG := QrFatPedCabCondicaoPG.Value;
    FmFatDivCms.FAssociada := QrFatPedCabAssociada.Value;
    if QrFatPedCabEncerrou.Value > 2 then
      FmFatDivCms.FDataFat := QrFatPedCabEncerrou.Value
    else
      FmFatDivCms.FDataFat := QrFatPedCabAbertura.Value;
    //
    FmFatDivCms.ReopenPediAcc();
    //
    if FmFatDivCms.CriaLcts() then
      FmFatDivCms.ShowModal;
    FmFatDivCms.Destroy;
}
  Geral.MB_Info('Deprecado! Desabilitado por falta de uso');
end;

{ TODO : criar campo para ver se lan ctos de comiss�o foram crados! }
{ TODO : fazer verifica�ao de desbloqueio! }

end.

