object FmFatPedPes1: TFmFatPedPes1
  Left = 339
  Top = 185
  Caption = 'FAT-PEDID-007 :: Pesquisa de Pedidos Pendentes'
  ClientHeight = 577
  ClientWidth = 584
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 584
    Height = 89
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object Label2: TLabel
      Left = 12
      Top = 44
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object SpeedButton1: TSpeedButton
      Left = 69
      Top = 60
      Width = 22
      Height = 21
      Caption = '?'
      OnClick = SpeedButton1Click
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 68
      Top = 20
      Width = 500
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 1
      dmkEditCB = EdEmpresa
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdEmpresa: TdmkEditCB
      Left = 12
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEmpresaChange
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdCliente: TdmkEditCB
      Left = 12
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdClienteChange
      DBLookupComboBox = CBCliente
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 92
      Top = 60
      Width = 476
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENT'
      ListSource = DsClientes
      TabOrder = 3
      dmkEditCB = EdCliente
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object dmkDBGrid1: TdmkDBGrid
    Left = 0
    Top = 137
    Width = 584
    Height = 332
    Align = alClient
    Columns = <
      item
        Expanded = False
        FieldName = 'CodUsu'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Title.Caption = 'C'#243'digo'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DtaEmiss'
        Title.Caption = 'Emiss'#227'o'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DtaPrevi'
        Title.Caption = 'Prev. entrega'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PedidoCli'
        Title.Caption = 'Pedido cliente'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Prioridade'
        Width = 56
        Visible = True
      end>
    Color = clWindow
    DataSource = DsPedidos
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = dmkDBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CodUsu'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Title.Caption = 'C'#243'digo'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DtaEmiss'
        Title.Caption = 'Emiss'#227'o'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DtaPrevi'
        Title.Caption = 'Prev. entrega'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PedidoCli'
        Title.Caption = 'Pedido cliente'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Prioridade'
        Width = 56
        Visible = True
      end>
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 584
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 536
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 488
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 391
        Height = 32
        Caption = 'Pesquisa de Pedidos Pendentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 391
        Height = 32
        Caption = 'Pesquisa de Pedidos Pendentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 391
        Height = 32
        Caption = 'Pesquisa de Pedidos Pendentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 469
    Width = 584
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 580
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 513
    Width = 584
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 580
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 436
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPedidos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pvd.CodUsu, pvd.DtaEmiss, pvd.DtaPrevi, '
      'pvd.Prioridade, pvd.PedidoCli'
      'FROM pedivda pvd'
      'LEFT JOIN pedivdaits pvi ON pvi.Codigo=pvd.Codigo'
      'WHERE pvi.QuantP-pvi.QuantC-pvi.QuantV > 0'
      'AND pvd.Empresa=:P0'
      'AND pvd.Cliente=:P1'
      'ORDER BY pvd.DtaPrevi')
    Left = 8
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPedidosCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
      DisplayFormat = '000000'
    end
    object QrPedidosDtaEmiss: TDateField
      FieldName = 'DtaEmiss'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrPedidosDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrPedidosPrioridade: TSmallintField
      FieldName = 'Prioridade'
      Required = True
      DisplayFormat = '00;-00; '
    end
    object QrPedidosPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
    end
  end
  object DsPedidos: TDataSource
    DataSet = QrPedidos
    Left = 36
    Top = 12
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT cli.Codigo, '
      'IF(cli.Tipo=0,RazaoSocial,Cli.Nome) NOMEENT '
      'FROM pedivda pvd'
      'LEFT JOIN entidades cli ON cli.Codigo=pvd.Cliente'
      'WHERE pvd.Codigo IN (    '
      '     SELECT DISTINCT Codigo '
      '     FROM pedivdaits'
      '     WHERE QuantP-QuantC-QuantV > 0'
      ')'
      'ORDER BY NOMEENT')
    Left = 204
    Top = 252
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 232
    Top = 252
  end
end
