unit FatGruFisRegCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditCB, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkRadioGroup;

type
  TFmFatGruFisRegCad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnDados: TPanel;
    RGMostra: TdmkRadioGroup;
    PnFisRegCad: TPanel;
    Label4: TLabel;
    SpeedButton1: TSpeedButton;
    EdCFOP: TdmkEditCB;
    CBCFOP: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    EdCST_B: TdmkEdit;
    EdTextoB: TdmkEdit;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    BtNaoSel: TBitBtn;
    RGmodBC: TdmkRadioGroup;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    EdpRedBC: TdmkEdit;
    Panel10: TPanel;
    GroupBox5: TGroupBox;
    Panel11: TPanel;
    Label24: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    EdUFEmit: TdmkEdit;
    EdICMSAliq: TdmkEdit;
    EdPISAliq: TdmkEdit;
    EdCOFINSAliq: TdmkEdit;
    EdpICMSInter: TdmkEdit;
    GroupBox6: TGroupBox;
    Panel12: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    LapICMSInterPart: TLabel;
    EdUFDest: TdmkEdit;
    EdpICMSUFDest: TdmkEdit;
    EdpFCPUFDest: TdmkEdit;
    EdpBCUFDest: TdmkEdit;
    EdpICMSInterPart: TdmkEdit;
    CkUsaInterPartLei: TCheckBox;
    DsCFOP: TDataSource;
    QrCFOP: TmySQLQuery;
    QrCFOPCodigo: TWideStringField;
    QrCFOPNome: TWideStringField;
    QrCFOPDescricao: TWideMemoField;
    QrCFOPComplementacao: TWideMemoField;
    QrTmp_FatPedFis: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdCST_BChange(Sender: TObject);
    procedure EdCST_BKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtNaoSelClick(Sender: TObject);
    procedure CkUsaInterPartLeiClick(Sender: TObject);
    procedure RGMostraClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FConfig: Boolean;
    procedure ConfiguraPainelFisRegCad(Cliente, Empresa, RegrFiscal: Integer;
              IE: String; Mostra, GraGru1, MadeBy, UFEmit, UFDest: Integer);
    procedure ReopenCFOP2003();
  public
    { Public declarations }
    FCliente, FEmpresa, FRegrFiscal, FCliUF, FEmpUF, FMadeBy, FOriCodi,
    FOriCnta, FGraGru1: Integer;
    FCliIE, FTmp_FatPedFis: String;
  end;

  var
  FmFatGruFisRegCad: TFmFatGruFisRegCad;

implementation

uses
  {$IfNDef NO_FINANCEIRO}UnFinanceiro, {$EndIf}
  UnMyObjects, Module, UnGrade_Jan, DmkDAC_PF, ModuleGeral, UMySQLModule;

{$R *.DFM}

procedure TFmFatGruFisRegCad.BtNaoSelClick(Sender: TObject);
begin
  RGmodBC.ItemIndex := -1;
end;

procedure TFmFatGruFisRegCad.BtOKClick(Sender: TObject);
var
  OriCodi, OriCnta, Gragru1, modBC, UsaInterPartLei: Integer;
  pRedBC, ICMSAliq, pICMSUFDest, pICMSInter, pFCPUFDest, pBCUFDest,
  pICMSInterPart: Double;
  CFOP, CST_B, UFEmit, UFDest: String;
begin
  OriCodi := FOriCodi;
  OriCnta := FOriCnta;
  Gragru1 := FGragru1;
  //
  CFOP              := EdCFOP.ValueVariant;
  CST_B             := EdCST_B.ValueVariant;
  modBC             := RGmodBC.ItemIndex;
  pRedBC            := EdpRedBC.ValueVariant;
  UFEmit            := EdUFEmit.ValueVariant;
  UFDest            := EdUFDest.ValueVariant;
  ICMSAliq          := EdICMSAliq.ValueVariant;
  pICMSUFDest       := EdpICMSUFDest.ValueVariant;
  pICMSInter        := EdpICMSInter.ValueVariant;
  pFCPUFDest        := EdpFCPUFDest.ValueVariant;
  pBCUFDest         := EdpBCUFDest.ValueVariant;
  pICMSInterPart    := EdpICMSInterPart.ValueVariant;
  UsaInterPartLei   := Geral.BoolToInt(CkUsaInterPartLei.Checked);
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DModG.MyPID_DB, [
    'DELETE FROM ' + FTmp_FatPedFis,
    'WHERE OriCodi=' + Geral.FF0(OriCodi),
    'AND OriCnta=' + Geral.FF0(OriCnta),
    'AND GraGru1=' + Geral.FF0(GraGru1),
    '']);
  //
  if not UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FTmp_FatPedFis, False,
    ['CFOP', 'CST_B', 'modBC', 'pRedBC', 'UFEmit', 'UFDest', 'ICMSAliq',
    'pICMSUFDest', 'pICMSInter', 'pFCPUFDest', 'pBCUFDest', 'pICMSInterPart',
    'UsaInterPartLei'], ['OriCodi', 'OriCnta', 'Gragru1'],
    [CFOP, CST_B, modBC, pRedBC, UFEmit, UFDest, ICMSAliq,
    pICMSUFDest, pICMSInter, pFCPUFDest, pBCUFDest, pICMSInterPart,
    UsaInterPartLei], [OriCodi, OriCnta, Gragru1], False)
  then
    Geral.MB_Erro('Falha ao atualizar dados fiscais!');
  Close;
end;

procedure TFmFatGruFisRegCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatGruFisRegCad.CkUsaInterPartLeiClick(Sender: TObject);
begin
  LapICMSInterPart.Enabled := not CkUsaInterPartLei.Checked;
  EdpICMSInterPart.Enabled := not CkUsaInterPartLei.Checked;
end;

procedure TFmFatGruFisRegCad.ConfiguraPainelFisRegCad(Cliente, Empresa,
  RegrFiscal: Integer; IE: String; Mostra, GraGru1, MadeBy, UFEmit, UFDest: Integer);
const
  CFOP_Servico = 0;
var
  QryCli, QryEmp, QryCFOP, QryAliq: TmySQLQuery;
  Cli_Tipo, Cli_UF, Emp_UF, CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, FisRegCad: Integer;
  UF_Cli, UF_Emp: String;
begin
  if Mostra = -1 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrTmp_FatPedFis, DModG.MyPID_DB, [
      'SELECT * ',
      'FROM ' + FTmp_FatPedFis,
      'WHERE OriCodi=' + Geral.FF0(FOriCodi),
      'AND OriCnta=' + Geral.FF0(FOriCnta),
      'AND GraGru1=' + Geral.FF0(FGraGru1),
      '']);
    if QrTmp_FatPedFis.RecordCount > 0 then
    begin
      RGMostra.ItemIndex            := 1;
      EdCFOP.ValueVariant           := QrTmp_FatPedFis.FieldByName('CFOP').AsString;
      CBCFOP.KeyValue               := QrTmp_FatPedFis.FieldByName('CFOP').AsString;
      EdCST_B.ValueVariant          := QrTmp_FatPedFis.FieldByName('CST_B').AsString;
      RGmodBC.ItemIndex             := QrTmp_FatPedFis.FieldByName('modBC').AsInteger;
      EdpRedBC.ValueVariant         := QrTmp_FatPedFis.FieldByName('pRedBC').AsFloat;
      CkUsaInterPartLei.Checked     := Geral.IntToBool(QrTmp_FatPedFis.FieldByName('UsaInterPartLei').AsInteger);
      EdUFEmit.ValueVariant         := QrTmp_FatPedFis.FieldByName('UFEmit').AsString;
      EdICMSAliq.ValueVariant       := QrTmp_FatPedFis.FieldByName('ICMSAliq').AsFloat;
      EdpICMSInter.ValueVariant     := QrTmp_FatPedFis.FieldByName('pICMSInter').AsFloat;
      EdUFDest.ValueVariant         := QrTmp_FatPedFis.FieldByName('UFDest').AsString;
      EdpICMSUFDest.ValueVariant    := QrTmp_FatPedFis.FieldByName('pICMSUFDest').AsFloat;
      EdpFCPUFDest.ValueVariant     := QrTmp_FatPedFis.FieldByName('pFCPUFDest').AsFloat;
      EdpBCUFDest.ValueVariant      := QrTmp_FatPedFis.FieldByName('pBCUFDest').AsFloat;
      EdpICMSInterPart.ValueVariant := QrTmp_FatPedFis.FieldByName('pICMSInterPart').AsFloat;
      //
      PnFisRegCad.Visible := True;
      PnDados.Visible     := True;
    end else
    begin
      RGMostra.ItemIndex  := 0;
      PnFisRegCad.Visible := False;
      PnDados.Visible     := True;
    end;
  end else
  if Mostra = 1 then
  begin
    RGMostra.ItemIndex            := 1;
    EdCST_B.ValueVariant          := '';
    RGmodBC.ItemIndex             := -1;
    EdpRedBC.ValueVariant         := 0;
    EdUFEmit.ValueVariant         := Geral.GetSiglaUF_do_CodigoUF(UFEmit);
    EdICMSAliq.ValueVariant       := 0;
    EdpICMSInter.ValueVariant     := 0;
    EdUFDest.ValueVariant         := Geral.GetSiglaUF_do_CodigoUF(UFDest);
    EdpICMSUFDest.ValueVariant    := 0;
    EdpFCPUFDest.ValueVariant     := 0;
    EdpBCUFDest.ValueVariant      := 0;
    EdpICMSInterPart.ValueVariant := 0;
    CkUsaInterPartLei.Checked     := True;
    //
    if GraGru1 = 0 then
    begin
      Geral.MB_Aviso('O produto n�o foi informado!');
      Close;
    end;
    Screen.Cursor := crHourGlass;
    //
    QryCli  := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    QryEmp  := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    QryCFOP := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    QryAliq := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QryCli, Dmod.MyDB, [
        'SELECT IF(ent.Tipo=0, ent.EUF, ent.PUF) UF, ent.Tipo ',
        'FROM entidades ent ',
        'WHERE ent.Codigo=' + Geral.FF0(Cliente),
        '']);
      UnDmkDAC_PF.AbreMySQLQuery0(QryEmp, Dmod.MyDB, [
        'SELECT IF(ent.Tipo=0, ent.EUF, ent.PUF) UF, ent.Tipo ',
        'FROM entidades ent ',
        'WHERE ent.Codigo=' + Geral.FF0(Empresa),
        '']);
      if (QryCli.RecordCount > 0) and (QryEmp.RecordCount > 0) then
      begin
        Cli_Tipo := QryCli.FieldByName('Tipo').AsInteger;
        Cli_UF   := QryCli.FieldByName('UF').AsInteger;
        Emp_UF   := QryEmp.FieldByName('UF').AsInteger;
        //
        //Contribuinte UF (I.E.)
        if Cli_Tipo = 1 then
        begin
          CFOP_Contrib := 0;
        end else
        begin
         if Uppercase(Trim(IE)) = 'ISENTO' then
           CFOP_Contrib := 0
         else
           CFOP_Contrib := 1;
        end;
        //Venda/compra mesma UF
        if Cli_UF = EMP_UF then
          CFOP_MesmaUF := 1
        else
          CFOP_MesmaUF := 0;
        //Fabrica��o pr�pria
        if MadeBy = 1 then
          CFOP_Proprio := 1
        else
          CFOP_Proprio := 0;
        //
        FisRegCad := RegrFiscal;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QryCFOP, Dmod.MyDB, [
          'SELECT CFOP ',
          'FROM fisregcfop ',
          'WHERE Codigo=' + Geral.FF0(FisRegCad),
          'AND Contribui=' + Geral.FF0(CFOP_Contrib),
          'AND Interno=' + Geral.FF0(CFOP_MesmaUF),
          'AND Proprio=' + Geral.FF0(CFOP_Proprio),
          'AND Servico=' + Geral.FF0(CFOP_Servico),
          '']);
        if QryCFOP.RecordCount > 0 then
        begin
          EdCFOP.ValueVariant := QryCFOP.FieldByName('CFOP').AsString;
          CBCFOP.KeyValue     := QryCFOP.FieldByName('CFOP').AsString;
          //
          UF_Cli := Geral.GetSiglaUF_do_CodigoUF(Cli_UF);
          UF_Emp := Geral.GetSiglaUF_do_CodigoUF(Emp_UF);
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QryAliq, Dmod.MyDB, [
            'SELECT frc.ICMS_Usa, frc.PIS_Usa, COFINS_Usa, fru.* ',
            'FROM fisregufs fru ',
            'LEFT JOIN fisregcad frc ON frc.Codigo=fru.Codigo ',
            'WHERE fru.Codigo=' + Geral.FF0(FisRegCad),
            'AND fru.Interno=' + Geral.FF0(CFOP_MesmaUF),
            'AND fru.Contribui=' + Geral.FF0(CFOP_Contrib),
            'AND fru.Proprio=' + Geral.FF0(CFOP_Proprio),
            'AND fru.UFEmit="' + UF_Emp + '" ',
            'AND fru.UFDest="' + UF_Cli + '" ',
            '']);
          if QryAliq.RecordCount > 0 then
          begin
            EdCST_B.ValueVariant          := QryAliq.FieldByName('CST_B').AsString;
            RGmodBC.ItemIndex             := QryAliq.FieldByName('modBC').AsInteger;
            EdpRedBC.ValueVariant         := QryAliq.FieldByName('pRedBC').AsFloat;
            EdUFEmit.ValueVariant         := QryAliq.FieldByName('UFEmit').AsString;
            EdICMSAliq.ValueVariant       := QryAliq.FieldByName('ICMSAliq').AsFloat;
            EdpICMSInter.ValueVariant     := QryAliq.FieldByName('pICMSInter').AsFloat;
            EdUFDest.ValueVariant         := QryAliq.FieldByName('UFDest').AsString;
            EdpICMSUFDest.ValueVariant    := QryAliq.FieldByName('pICMSUFDest').AsFloat;
            EdpFCPUFDest.ValueVariant     := QryAliq.FieldByName('pFCPUFDest').AsFloat;
            EdpBCUFDest.ValueVariant      := QryAliq.FieldByName('pBCUFDest').AsFloat;
            EdpICMSInterPart.ValueVariant := QryAliq.FieldByName('pICMSInterPart').AsFloat;
            CkUsaInterPartLei.Checked     := Geral.IntToBool(QryAliq.FieldByName('UsaInterPartLei').AsInteger);
          end;
        end;
      end;
    finally
      QryAliq.Free;
      QryCFOP.Free;
      QryCli.Free;
      QryEmp.Free;
      //
      Screen.Cursor := crDefault;
      //
      PnFisRegCad.Visible := True;
      PnDados.Visible     := True;
    end;
  end else
  begin
    RGMostra.ItemIndex  := 0;
    PnFisRegCad.Visible := False;
    PnDados.Visible     := True;
  end;
end;

procedure TFmFatGruFisRegCad.EdCST_BChange(Sender: TObject);
begin
  {$IfNDef NO_FINANCEIRO}
  EdTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdCST_B.Text));
  {$EndIf}
end;

procedure TFmFatGruFisRegCad.EdCST_BKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCST_B.Text := UFinanceiro.ListaDeTributacaoPeloICMS();
  {$EndIf}
end;

procedure TFmFatGruFisRegCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatGruFisRegCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FConfig         := False;
  //
  ReopenCFOP2003();
end;

procedure TFmFatGruFisRegCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatGruFisRegCad.FormShow(Sender: TObject);
begin
  ConfiguraPainelFisRegCad(FCliente, FEmpresa, FRegrFiscal, FCliIE, -1, 0, 0,
    FEmpUF, FCliUF);
  //
  FConfig := True;
end;

procedure TFmFatGruFisRegCad.ReopenCFOP2003;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCFOP, Dmod.MyDB, [
    'SELECT * ',
    'FROM cfop2003 ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmFatGruFisRegCad.RGMostraClick(Sender: TObject);
var
  Mostra: Integer;
begin
  if FConfig = True then
  begin
    if RGMostra.ItemIndex = 1 then
      Mostra := 1
    else
      Mostra := 0;
    //
    ConfiguraPainelFisRegCad(FCliente, FEmpresa, FRegrFiscal, FCliIE, Mostra,
      FGraGru1, FMadeBy, FEmpUF, FCliUF);
  end;
end;

procedure TFmFatGruFisRegCad.SpeedButton1Click(Sender: TObject);
begin
{$IfNDef NAO_GFAT}
  VAR_CADTEXTO := '';
  //
  Grade_Jan.MostraFormCFOP2003();
  //
  if VAR_CADTEXTO <> '' then
  begin
    ReopenCFOP2003();
    //
    EdCFOP.ValueVariant := VAR_CADTEXTO;
    CBCFOP.KeyValue     := VAR_CADTEXTO;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

end.
