unit FatDivCab2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, ComCtrls, DmkDAC_PF,
  dmkEditDateTimePicker, Mask, DBCtrls, dmkDBLookupComboBox, dmkEditCB, dmkEdit,
  dmkCheckBox, dmkLabel, dmkGeral, dmkValUsu, dmkPermissoes, dmkImage, UnDmkEnums,
  dmkRadioGroup, mySQLDbTables, UnGrl_Geral, dmkDBEdit, Vcl.Menus;

type
  TFmFatDivCab2 = class(TForm)
    PainelEdita: TPanel;
    GroupBox1: TGroupBox;
    Label9: TLabel;
    Label2: TLabel;
    SpeedButton5: TSpeedButton;
    Label23: TLabel;
    Label26: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    SpeedButton7: TSpeedButton;
    EdCliente: TdmkEditCB;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    CBCliente: TdmkDBLookupComboBox;
    DBEdCidade: TDBEdit;
    DBEdNOMEUF: TDBEdit;
    EdSituacao: TdmkEditCB;
    CBSituacao: TdmkDBLookupComboBox;
    EdMotivoSit: TdmkEditCB;
    CBMotivoSit: TdmkDBLookupComboBox;
    EdCodigo: TdmkEdit;
    TPDtaInclu: TdmkEditDateTimePicker;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    Label27: TLabel;
    TPDtaEmiss: TdmkEditDateTimePicker;
    TPDtaEntra: TdmkEditDateTimePicker;
    EdPrioridade: TdmkEdit;
    TPDtaPrevi: TdmkEditDateTimePicker;
    EdPedidoCli: TdmkEdit;
    GroupBox3: TGroupBox;
    BtTabelaPrc: TSpeedButton;
    LaTabelaPrc: TLabel;
    SpeedButton9: TSpeedButton;
    Label22: TLabel;
    BtCondicaoPG: TSpeedButton;
    Label24: TLabel;
    LaCondicaoPG: TLabel;
    SpeedButton13: TSpeedButton;
    CBTabelaPrc: TdmkDBLookupComboBox;
    EdTabelaPrc: TdmkEditCB;
    CBCartEmis: TdmkDBLookupComboBox;
    CBMoeda: TdmkDBLookupComboBox;
    EdMoeda: TdmkEditCB;
    EdCartEmis: TdmkEditCB;
    CBCondicaoPG: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    GroupBox4: TGroupBox;
    Label28: TLabel;
    DBEdit15: TDBEdit;
    GroupBox5: TGroupBox;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    SpeedButton6: TSpeedButton;
    SpeedButton11: TSpeedButton;
    Label20: TLabel;
    Label32: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    EdFretePor: TdmkEditCB;
    CBFretePor: TdmkDBLookupComboBox;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    EdRedespacho: TdmkEditCB;
    CBRedespacho: TdmkDBLookupComboBox;
    MeEnderecoEntrega1: TMemo;
    EdDesoAces_V: TdmkEdit;
    EdFrete_V: TdmkEdit;
    EdSeguro_P: TdmkEdit;
    EdDesoAces_P: TdmkEdit;
    EdFrete_P: TdmkEdit;
    EdSeguro_V: TdmkEdit;
    GBAgenteVenda: TGroupBox;
    Label46: TLabel;
    SpeedButton12: TSpeedButton;
    Label47: TLabel;
    Label48: TLabel;
    EdRepresen: TdmkEditCB;
    CBRepresen: TdmkDBLookupComboBox;
    EdComisFat: TdmkEdit;
    EdComisRec: TdmkEdit;
    GroupBox6: TGroupBox;
    Label25: TLabel;
    SbRegrFiscal: TSpeedButton;
    Label33: TLabel;
    EdRegrFiscal: TdmkEditCB;
    CBRegrFiscal: TdmkDBLookupComboBox;
    EdModeloNF: TdmkEdit;
    Label1: TLabel;
    EdCodUsu_Fat: TdmkEdit;
    EdCodUsu_Ped: TdmkEdit;
    Label8: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    VuEmpresa: TdmkValUsu;
    VuCondicaoPG: TdmkValUsu;
    VuCambioMda: TdmkValUsu;
    VuTabelaPrc: TdmkValUsu;
    VuMotivoSit: TdmkValUsu;
    VuFisRegCad: TdmkValUsu;
    Label3: TLabel;
    EdCodigo_Ped: TdmkEdit;
    CkUsaReferen: TdmkCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    BtConfirma: TBitBtn;
    CkAFP_Sit: TdmkCheckBox;
    EdAFP_Per: TdmkEdit;
    Label49: TLabel;
    BtDesiste: TBitBtn;
    RG_idDest: TdmkRadioGroup;
    RG_indFinal: TdmkRadioGroup;
    Label58: TLabel;
    EdindPres: TdmkEdit;
    EdindPres_TXT: TEdit;
    RGIndSinc: TdmkRadioGroup;
    Label208: TLabel;
    Edide_finNFe_TXT: TdmkEdit;
    Edide_finNFe: TdmkEdit;
    SbDestEnt: TSpeedButton;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Panel3: TPanel;
    CkRetiradaUsa: TdmkCheckBox;
    PnRetiradaEntiAll: TPanel;
    PnRetiradaEntiSel: TPanel;
    Label4: TLabel;
    EdRetiradaEnti: TdmkEditCB;
    CBRetiradaEnti: TdmkDBLookupComboBox;
    SbRetiradaEnti: TSpeedButton;
    PnRetiradaEntiViw: TPanel;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    Label10: TLabel;
    DBEdit2: TDBEdit;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label16: TLabel;
    DBEdit5: TDBEdit;
    Label17: TLabel;
    DBEdit6: TDBEdit;
    Label21: TLabel;
    DBEdit9: TDBEdit;
    Label18: TLabel;
    DBEdit7: TDBEdit;
    Label34: TLabel;
    DBEdit10: TDBEdit;
    DBEdit8: TDBEdit;
    Label35: TLabel;
    DBEdit11: TDBEdit;
    Label36: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    Label38: TLabel;
    DBEdit14: TDBEdit;
    Label39: TLabel;
    DBEdit16: TDBEdit;
    Label40: TLabel;
    DBEdit17: TDBEdit;
    Panel5: TPanel;
    CkEntregaUsa: TdmkCheckBox;
    PnEntregaEntiAll: TPanel;
    PnEntregaEntiSel: TPanel;
    Label19: TLabel;
    SbEntregaEnti: TSpeedButton;
    EdEntregaEnti: TdmkEditCB;
    CBEntregaEnti: TdmkDBLookupComboBox;
    PnEntregaEntiViw: TPanel;
    Label37: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    PMRegrFiscal: TPopupMenu;
    Estries1: TMenuItem;
    Cadastro1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBClienteExit(Sender: TObject);
    procedure EdTabelaPrcChange(Sender: TObject);
    procedure EdCondicaoPGChange(Sender: TObject);
    procedure EdDesoAces_PChange(Sender: TObject);
    procedure EdFrete_PChange(Sender: TObject);
    procedure EdSeguro_PChange(Sender: TObject);
    procedure EdRegrFiscalChange(Sender: TObject);
    procedure EdRegrFiscalExit(Sender: TObject);
    procedure BtCondicaoPGClick(Sender: TObject);
    procedure BtTabelaPrcClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure SpeedButton12Click(Sender: TObject);
    procedure SbRegrFiscalClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdRepresenChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdindPresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdindPresChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure Edide_finNFeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edide_finNFeChange(Sender: TObject);
    procedure SbDestEntClick(Sender: TObject);
    procedure EdClienteExit(Sender: TObject);
    procedure SbRetiradaEntiClick(Sender: TObject);
    procedure CkRetiradaUsaClick(Sender: TObject);
    procedure EdRetiradaEntiRedefinido(Sender: TObject);
    procedure CkEntregaUsaClick(Sender: TObject);
    procedure SbEntregaEntiClick(Sender: TObject);
    procedure EdEntregaEntiRedefinido(Sender: TObject);
    procedure Estries1Click(Sender: TObject);
    procedure Cadastro1Click(Sender: TObject);
  private
    { Private declarations }
    F_indPres, F_finNFe: MyArrayLista;
    procedure MostraFormEntidade2(Entidade: Integer; Query: TmySQLQuery;
              EditCliente: TdmkEditCB; ComboCliente: TdmkDBLookupComboBox);
  public
    { Public declarations }
  end;

  var
  FmFatDivCab2: TFmFatDivCab2;

implementation

uses
  {$IfNDef SemPediVda}PediAcc, {$EndIf}
  {$IfNDef SemNFe_0000} NFe_PF, {$EndIf}
  {$IfNDef SemCotacoes} CambioMda, {$EndIf}
  {$IfNDef NAO_GFAT}UnGrade_Jan, UnGFat_Jan, {$EndIf}
  {$IfNDef NO_FINANCEIRO}UnFinanceiroJan, {$EndIf}
  UnMyObjects, ModuleGeral, UMySQLModule, FatDivGer2, Module, Principal,
  ModProd, ModPediVda, UnInternalConsts, MyDBCheck, UnPraz_PF, UnDmkProcFunc,
  UnEntities;

{$R *.DFM}

procedure TFmFatDivCab2.BtCondicaoPGClick(Sender: TObject);
var
  CondicaoPG: Integer;
begin
  VAR_CADASTRO := 0;
  //
(*
  pode ser que ainda nao tenha nenhum cadastro!
  if not UMyMod.ObtemCodigoDeCodUsu(EdCondicaoPG, CondicaoPG,
    'Informe a condi��o de pagamento!', 'Codigo', 'CodUsu') then
    Exit;
*)
  UMyMod.ObtemCodigoDeCodUsu(EdCondicaoPG, CondicaoPG, '', 'Codigo', 'CodUsu');
  //
  Praz_PF.MostraFormPediPrzCab1(CondicaoPG);
  //
  if VAR_CADASTRO <> 0 then
  begin
    DmPediVda.QrPediPrzCab.Close;
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrPediPrzCab, Dmod.MyDB);
    if DmPediVda.QrPediPrzCab.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdCondicaoPg.ValueVariant := DmPediVda.QrPediPrzCabCodUsu.Value;
      CBCondicaoPg.KeyValue     := DmPediVda.QrPediPrzCabCodUsu.Value;
      EdCondicaoPG.SetFocus;
    end;
  end;
end;

procedure TFmFatDivCab2.BtConfirmaClick(Sender: TObject);
var
// Pedido
  Codigo: Integer;
//Faturamento
  //Codigo,
  Pedido, CodUsu, RegrFiscal, indPres, finNFe: Integer;
  // Balan�o aberto
  //PrdGrupTip, Empresa, FisRegCad: Integer;
  Abertura, Mensagem: String;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
  //
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa,
    'Informe a empresa!') then Exit;
  if MyObjects.FIC(EdCliente.ValueVariant = 0, EdCliente,
    'Informe o destinat�rio!') then Exit;
  if MyObjects.FIC(TPDtaPrevi.Date < 2, TPDtaPrevi,
    'Informe a data de previs�o de entrega!') then Exit;
  if MyObjects.FIC(EdCondicaoPG.ValueVariant = 0, EdCondicaoPG,
    'Informe a condi��o de pagamento!') then Exit;
  if MyObjects.FIC(EdRegrFiscal.ValueVariant = 0, EdRegrFiscal,
    'Informe a movimenta��o (fiscal)!') then Exit;

  if DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0 then
  begin
    if MyObjects.FIC(RG_indFinal.ItemIndex < 0, RG_indFinal,
      'Informe a opera��o com o consumidor final!') then Exit;

    {$IfNDef SemNFe_0000}
      if MyObjects.FIC(EdindPres.Text = '', EdindPres,
        'Informe o indicador de presen�a do comprador!') then Exit;
      indPres := Geral.IMV(EdindPres.Text);
      if MyObjects.FIC(not (indPres in [0,1,2,3,4,9]), EdindPres,
        'Informe o indicador de presen�a do comprador!') then Exit;

      if MyObjects.FIC(Edide_finNFe.Text = '', Edide_finNFe,
        'Informe a finalidade de emiss�o da NF-e!') then Exit;
      finNFe := Geral.IMV(Edide_finNFe.Text);
      if MyObjects.FIC(not (finNFe in [1,2,3,4]), Edide_finNFe,
        'Informe a finalidade de emiss�o da NF-e!') then Exit;
    {$EndIf}
  end;

  if DmPediVda.QrFisRegCadFinanceiro.Value > 0 then
  begin
    if MyObjects.FIC(EdCartEmis.ValueVariant = 0, EdCartEmis,
      'Informe a carteira!') then Exit;
  end;
  // 2019-03-25 Erro! informando filial como vari�vel mas o certo � o c�digo da entidade!
  //DmPediVda.ReopenParamsEmp(EdEmpresa.ValueVariant, True); /
  DmPediVda.ReopenParamsEmp(DModG.QrEmpresasCodigo.Value, True);
  // Fim 2019-03-25
  case DmPediVda.QrParamsEmpFatSemPrcL.Value of
    0: if MyObjects.FIC(EdTabelaPrc.ValueVariant = 0, EdTabelaPrc,
    'Informe a tabela de pre�os!') then Exit;
    1: if MyObjects.FIC(EdTabelaPrc.ValueVariant = 0, EdTabelaPrc,
      'ATEN��O! N�o foi informada a tabela de pre�os!') then (*Exit*);
  end;
  //if MyObjects.FIC(EdFretePor.ValueVariant = 0, EdFretePor,
    //'Informe a o respons�vel pelo frete!') then Exit;
(*
  if MyObjects.FIC((EdFretePor.ValueVariant > 1) and (DmPediVda.QrParamsEmpversao.Value < 2),
    EdFretePor, 'Tipo de frete inv�lido na vers�o atual da NFe! Selecione 0 ou 1') then Exit;
*)
  //ErroFatura := FaturaParcialIncorreta(Txt);
  //if MyObjects.FIC(ErroFatura, CkAFP_Sit,
    //'Fatura parcial n�o permitida!' + sLineBreak + Txt) then Exit;
  //Valida regra fiscal com os dados do cliente
  if not UMyMod.ObtemCodigoDeCodUsu(EdRegrFiscal, RegrFiscal,
    'Informe a regra fiscal!', 'Codigo', 'CodUsu') then Exit;

  if DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0 then
  begin
    if MyObjects.FIC(DmPediVda.QrClientesDOCENT.Value = '', EdCliente,
      'O Destinat�rio n�o possui CNPJ / CPF cadastrado!') then
    begin
      if Geral.MB_Pergunta('Deseja editar agora?' + sLineBreak +
        'AVISO: Verifique se os demais campos est�o devidamente preenchidos!') = ID_YES then
      begin
        MostraFormEntidade2(EdCliente.ValueVariant, DmPediVda.QrClientes,
          EdCliente, CBCliente);
      end;
      Exit;
    end;
    // NFe 3.10
    if MyObjects.FIC(RG_idDest.ItemIndex <= 0, RG_idDest,
      'Informe o Local de Destino da Opera��o!') then Exit;
    // NFe 3.10
    if not Entities.ValidaIndicadorDeIEEntidade_2(DmPediVda.QrClientesIE.Value,
      DmPediVda.QrClientesindIEDest.Value, DmPediVda.QrClientesTipo.Value,
      RG_idDest.ItemIndex, RG_indFinal.ItemIndex, False, Mensagem) then
    begin
      Geral.MB_Aviso(Mensagem);
      Exit;
    end;
  end;
  if CkEntregaUsa.Checked = False then
  begin
    EdEntregaEnti.ValueVariant := 0;
    CBEntregaEnti.KeyValue := 0;
  end;
  if CkRetiradaUsa.Checked = False then
  begin
    EdRetiradaEnti.ValueVariant := 0;
    CBRetiradaEnti.KeyValue := 0;
  end;
  //
  (*
  //////////////////////////////////////////////////////////////////////////////
  ////  Desativo em 14/09/2016                                              ////
  ////  Motivo: foi implementado a op��o de preencher os dados fiscais no   ////
  ////  momento da inclus�o do produto                                      ////
  //////////////////////////////////////////////////////////////////////////////

  if DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0 then
  begin
    if not DmPediVda.ValidaRegraFiscalCFOPCliente(RG_idDest.ItemIndex,
      RG_indFinal.ItemIndex, DmPediVda.QrClientesindIEDest.Value, RegrFiscal,
      DmPediVda.QrFisRegCadNome.Value, DmPediVda.QrClientesIE.Value,
      DmPediVda.QrClientesNOMEUF.Value, DmPediVda.QrParamsEmpUF_WebServ.Value,
      Mensagem) then
    begin
      Geral.MB_Aviso(Mensagem);
      //
      if Geral.MB_Pergunta('Deseja configurar a Regra Fiscal agora?') = ID_YES then
        Grade_Jan.MostraFormFisRegCad(RegrFiscal);
      //
      Exit;
    end;
  end;
  *)
  Codigo := UMyMod.BuscaEmLivreY_Def('pedivda', 'Codigo', SQLType,
    FmFatDivGer2.QrPediVdaCodigo.Value);
  EdCodigo_Ped.ValueVariant := Codigo;  
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'pedivda', 'CodUsu', [], [],
    SQLType, EdCodUsu_Ped.ValueVariant, siNegativo, EdCodUsu_Ped);
  if UMyMod.ExecSQLInsUpdPanel(SQLType, FmFatDivCab2, PainelEdita,
  'PediVda', Codigo, Dmod.QrUpd, [(*PainelEdita*)], [(*PainelEdita*)], nil, True) then
  begin
    {
    Pedido := Geral.IMV(EdPedido.Text);
    if Pedido = 0 then
    begin
      Geral.MB_Erro('Defina o pedido!');
      Exit;
    end else begin
      Pedido := QrPediVdaCodigo.Value;
    end;
    }
    Pedido := Codigo;
    //PrdGrupTip := 0; // 1
    //Empresa    := DModG.QrEmpresasCodigo.Value;//QrPediVdaEmpresa.Value;
    //FisRegCad  := DmPediVda.QrFisRegCadCodigo.Value;//QrPediVdaRegrFiscal.Value;
    Abertura   := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    // Verifica se existe balan�o aberto
    // Fazer como?
    {
    if DmProd.ExisteBalancoAberto_StqCen_Mul(PrdGrupTip, Empresa, FisRegCad) then Exit;
    }
    Codigo := UMyMod.BuscaEmLivreY_Def('fatpedcab', 'Codigo', SQLType,
      FmFatDivGer2.QrFatPedCabCodigo.Value);
    CodUsu := DModG.BuscaProximoCodigoInt('controle', 'StqMovUsu', '',
      EdCodUsu_Fat.ValueVariant);
    EdCodUsu_Fat.ValueVariant := CodUsu;
  {
    N�o � poss�vel por causa do Pedido!
    if UMyMod.ExecSQLInsUpdPanel(SQLType, FmFatPedCab, PainelEdit,
      'FatPedCab', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  }
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'fatpedcab', False, [
    'CodUsu', 'Pedido', 'Abertura'
    {
    'Serie', 'NF', 'PIS#Per', 'PIS#Val',
    'COFINS#Per', 'COFINS#Val', 'IR#Per', 'IR#Val', 'CS#Per', 'CS#Val',
    'ISS#Per', 'ISS#Val'
    }
    ], ['Codigo'], [
    CodUsu, Pedido, Abertura
    {Serie, NF, PIS#Per, PIS#Val,
    COFINS#Per, COFINS#Val, IR#Per, IR#Val, CS#Per, CS#Val,
    ISS#Per, ISS#Val
    }
    ], [Codigo], True) then
    begin
{
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      ImgTipo.SQLType := stLok;
}
      FmFatDivGer2.LocCod(Codigo, Codigo);
      if (SQLType = stIns) and (FmFatDivGer2.QrFatPedCabCodigo.Value = Codigo) then
        FmFatDivGer2.IncluiNovoVolume();
    end;
    Close;
  end;
end;

procedure TFmFatDivCab2.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatDivCab2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatDivCab2.BtTabelaPrcClick(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  TabelaPrc: Integer;
begin
  VAR_CADASTRO := 0;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdTabelaPrc, TabelaPrc,
    'Informe a tabela de pre�o!', 'Codigo', 'CodUsu') then Exit;
  //
  GFat_Jan.MostraFormTabePrcCab(TabelaPrc);
  //
  if VAR_CADASTRO <> 0 then
  begin
    DmPediVda.QrTabePrcCab.Close;
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrTabePrcCab, Dmod.MyDB);
    if DmPediVda.QrTabePrcCab.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdTabelaPrc.ValueVariant := DmPediVda.QrTabePrcCabCodUsu.Value;
      CBTabelaPrc.KeyValue     := DmPediVda.QrTabePrcCabCodUsu.Value;
      EdTabelaPrc.SetFocus;
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmFatDivCab2.Cadastro1Click(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  FisRegCad: Integer;
begin
  VAR_CADASTRO := 0;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdRegrFiscal, FisRegCad,
    'Informe a regra fiscal!', 'Codigo', 'CodUsu') then Exit;
  //
  Grade_Jan.MostraFormFisRegCad(FisRegCad);
  //
  if VAR_CADASTRO <> 0 then
  begin
    DmPediVda.QrFisRegCad.Close;
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrFisRegCad, Dmod.MyDB);
    if DmPediVda.QrFisRegCad.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdRegrFiscal.ValueVariant := DmPediVda.QrFisRegCadCodUsu.Value;
      CBRegrFiscal.KeyValue     := DmPediVda.QrFisRegCadCodUsu.Value;
      EdRegrFiscal.SetFocus;
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmFatDivCab2.CBClienteExit(Sender: TObject);
begin
  EdPedidoCli.SetFocus;
end;

procedure TFmFatDivCab2.CkEntregaUsaClick(Sender: TObject);
begin
  PnEntregaEntiAll.Visible := CkEntregaUsa.Checked;
end;

procedure TFmFatDivCab2.CkRetiradaUsaClick(Sender: TObject);
begin
  PnRetiradaEntiAll.Visible := CkRetiradaUsa.Checked;
end;

procedure TFmFatDivCab2.EdClienteChange(Sender: TObject);
var
  Linha1, Linha2, Linha3: String;
begin
  MeEnderecoEntrega1.Text :=
    DModG.ObtemEnderecoEntrega3Linhas(EdCliente.ValueVariant,
      Linha1, Linha2, Linha3);
  if EdCliente.ValueVariant = 0 then
  begin
    DBEdCidade.DataField := '';
    DBEdNOMEUF.DataField := '';
  end else begin
    DBEdCidade.DataField := 'CIDADE';
    DBEdNOMEUF.DataField := 'NOMEUF';
  end;
{$IfNDef SemNFe_0000}
  (*
  if ImgTipo.SQLType = stIns then
  begin
  Sempre atualiza para evitar erros
  *)
    UnNFe_PF.Configura_idDest(EdEmpresa, RG_idDest);
    UnNFe_PF.Configura_indFinal(RG_indFinal);
  (*
  end;
  *)
{$EndIf}
end;

procedure TFmFatDivCab2.EdClienteExit(Sender: TObject);
begin
  if DmPediVda.QrClientesObservacoes.Value <> '' then
    Geral.MB_Aviso('OBSERVA��ES:' + sLineBreak + DmPediVda.QrClientesObservacoes.Value);
end;

procedure TFmFatDivCab2.EdCondicaoPGChange(Sender: TObject);
begin
  CkAFP_Sit.Checked := DmPediVda.QrPediPrzCabPercent2.Value > 0;
  EdAfp_Per.ValueVariant := DmPediVda.QrPediPrzCabPercent2.Value;
end;

procedure TFmFatDivCab2.EdDesoAces_PChange(Sender: TObject);
begin
  if EdDesoAces_P.ValueVariant <> 0 then
  begin
    EdDesoAces_V.Enabled := False;
    //Parei Aqui! Falta fazer
    //CalculaDesoAces_V();
  end else EdDesoAces_V.Enabled := True;
end;

procedure TFmFatDivCab2.EdEmpresaChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    DModG.ReopenParamsEmp(DModG.QrEmpresasCodigo.Value);
    FmFatDivCab2.CkUsaReferen.Checked := DModG.QrParamsEmpUsaReferen.Value = 1;
{$IfNDef SemNFe_0000}
    UnNFe_PF.Configura_idDest(EdEmpresa, RG_idDest);
{$EndIf}
  end;
  DmPediVda.ReopenCartEmis(VuEmpresa.ValueVariant);
end;

procedure TFmFatDivCab2.EdEntregaEntiRedefinido(Sender: TObject);
begin
  PnEntregaEntiViw.Visible := EdEntregaEnti.ValueVariant <> 0;
end;

procedure TFmFatDivCab2.EdFrete_PChange(Sender: TObject);
begin
  if EdFrete_P.ValueVariant <> 0 then
  begin
    EdFrete_V.Enabled := False;
    //Parei Aqui! Falta fazer
    //CalculaFrete_V();
  end else EdFrete_V.Enabled := True;
end;

procedure TFmFatDivCab2.Edide_finNFeChange(Sender: TObject);
begin
  Edide_finNFe_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_finNFe,  Edide_finNFe.ValueVariant);
end;

procedure TFmFatDivCab2.Edide_finNFeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    Edide_finNFe.Text := Geral.SelecionaItem(F_finNFe, 0,
      'SEL-LISTA-000 :: Finalidade de emiss�o da NF-e',
      TitCols, Screen.Width)
  end;
end;

procedure TFmFatDivCab2.EdindPresChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdindPres.Text, F_indPres, Texto, 0, 1);
  EdindPres_TXT.Text := Texto;
end;

procedure TFmFatDivCab2.EdindPresKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdindPres.Text := Geral.SelecionaItem(F_indPres, 0,
    'SEL-LISTA-000 :: Indicador de presen�a do comprador no estabelecimento comercial no momento da opera��o',
    TitCols, Screen.Width)
  end;
end;

procedure TFmFatDivCab2.EdRegrFiscalChange(Sender: TObject);
begin
  EdModeloNF.Text := '';
  if not EdRegrFiscal.Focused then
    EdModeloNF.Text := DmPediVda.QrFisRegCadNO_MODELO_NF.Value;
end;

procedure TFmFatDivCab2.EdRegrFiscalExit(Sender: TObject);
begin
  EdModeloNF.Text := DmPediVda.QrFisRegCadNO_MODELO_NF.Value;
end;

procedure TFmFatDivCab2.EdRepresenChange(Sender: TObject);
begin
  EdComisFat.ValueVariant := DmPediVda.QrPediAccPerComissF.Value;
  EdComisRec.ValueVariant := DmPediVda.QrPediAccPerComissR.Value;
{ TODO : Ver o que fazer quando j� tem itens! }
end;

procedure TFmFatDivCab2.EdRetiradaEntiRedefinido(Sender: TObject);
begin
  PnRetiradaEntiViw.Visible := EdRetiradaEnti.ValueVariant <> 0;
end;

procedure TFmFatDivCab2.EdSeguro_PChange(Sender: TObject);
begin
  if EdSeguro_P.ValueVariant <> 0 then
  begin
    EdSeguro_V.Enabled := False;
    //Parei Aqui! Falta fazer
    //CalculaSeguro_V();
  end else EdSeguro_V.Enabled := True;
end;

procedure TFmFatDivCab2.EdTabelaPrcChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    EdMoeda.ValueVariant := DmPediVda.QrTabePrcCabMoeda.Value;
    CBMoeda.KeyValue     := DmPediVda.QrTabePrcCabMoeda.Value;
  end;
end;

procedure TFmFatDivCab2.Estries1Click(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := EdCliente.ValueVariant;
  if Entidade <> 0 then
    GFat_Jan.MostraFormFisRegEnPsq(Entidade, EdRegrFiscal, CBRegrFiscal)
  else
    Geral.MB_Aviso('Defina o destinat�rio!');
end;

procedure TFmFatDivCab2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatDivCab2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl2.Align := alClient;
  PageControl2.ActivePageIndex := 0;
  DBEdCidade.DataField := '';
  DBEdNOMEUF.DataField := '';
  //
  TPDtaInclu.Date := Date;
  TPDtaEmiss.Date := 0;
  TPDtaEntra.Date := 0;
  TPDtaPrevi.Date := Date;
  //
  RGIndSinc.ItemIndex := 1;
  RGIndSinc.Enabled   := False;
  //
  {$IfDef SemPediVda}
    GBAgenteVenda.Visible := False;
  {$Else}
    GBAgenteVenda.Visible := True;
  {$EndIf}
  CBEmpresa.ListSource := DModG.DsEmpresas;
{$IfNDef SemNFe_0000}
  F_indPres := UnNFe_PF.ListaIndicadorDePresencaComprador();
  F_finNFe  := UnNFe_PF.ListaFinalidadeDeEmiss�oDaNFe(True);
{$Else}
  F_indPres := 0;
  F_finNFe  := 0;
{$EndIf}
  CBEmpresa.ListSource := DModG.DsEmpresas;
  CBCliente.ListSource := DmPediVda.DsClientes;
  CBSituacao.ListSource := DModG.DsSituacao;
  CBMotivoSit.ListSource := DmPediVda.DsMotivos;
  CBTabelaPrc.ListSource := DmPediVda.DsTabePrcCab;
  CBCartEmis.ListSource := DmPediVda.DsCartEmis;
  CBMoeda.ListSource := DModG.DsCambioMda;
  CBCondicaoPG.ListSource := DmPediVda.DsPediPrzCab;
  CBFretePor.ListSource := DmPediVda.DsFretePor;
  CBTransporta.ListSource := DmPediVda.DsTransportas;
  CBRedespacho.ListSource := DmPediVda.DsRedespachos;
  CBRepresen.ListSource := DmPediVda.DsPediAcc;
  CBRegrFiscal.ListSource := DmPediVda.DsFisRegCad;
  CBEntregaEnti.ListSource := DmPediVda.DsEntregaEnti;
  CBRetiradaEnti.ListSource := DmPediVda.DsRetiradaEnti;
end;

procedure TFmFatDivCab2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatDivCab2.FormShow(Sender: TObject);
begin
  EdCliente.SetFocus;
end;

procedure TFmFatDivCab2.SbEntregaEntiClick(Sender: TObject);
begin
  MostraFormEntidade2(EdEntregaEnti.ValueVariant, DmPediVda.QrEntregaEnti,
    EdEntregaEnti, CBEntregaEnti);
end;

procedure TFmFatDivCab2.SbRegrFiscalClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMRegrFiscal, SbRegrFiscal)
end;

procedure TFmFatDivCab2.SbRetiradaEntiClick(Sender: TObject);
begin
  MostraFormEntidade2(EdRetiradaEnti.ValueVariant, DmPediVda.QrRetiradaEnti,
    EdRetiradaEnti, CBRetiradaEnti);
end;

procedure TFmFatDivCab2.SpeedButton11Click(Sender: TObject);
begin
  MostraFormEntidade2(EdRedespacho.ValueVariant, DmPediVda.QrRedespachos,
    EdRedespacho, CBRedespacho);
end;

procedure TFmFatDivCab2.SpeedButton12Click(Sender: TObject);
{$IfNDef SemPediVda}
var
  PediAcc: Integer;
{$EndIF}
begin
{$IfNDef SemPediVda}
  VAR_CADASTRO := 0;
  PediAcc      := EdRepresen.ValueVariant;
  //
  if DBCheck.CriaFm(TFmPediAcc, FmPediAcc, afmoNegarComAviso) then
  begin
    if PediAcc <> 0 then
      FmPediAcc.LocCod(PediAcc, PediAcc);
    FmPediAcc.ShowModal;
    FmPediAcc.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodigoPesquisado(EdRepresen, CBRepresen, DmPediVda.QrPediAcc, VAR_CADASTRO);
      EdRepresen.SetFocus;
    end;
  end;
{$EndIF}
end;

procedure TFmFatDivCab2.SpeedButton13Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  CartEmis: Integer;
begin
  VAR_CADASTRO := 0;
  CartEmis     := EdCartEmis.ValueVariant;
  //
  FinanceiroJan.CadastroDeCarteiras(CartEmis);
  //
  if VAR_CADASTRO <> 0 then
  begin
    DmPediVda.ReopenCartEmis(VuEmpresa.ValueVariant);
    //
    EdCartEmis.ValueVariant := VAR_CADASTRO;
    CBCartEmis.KeyValue     := VAR_CADASTRO;
    EdCartEmis.SetFocus;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmFatDivCab2.SbDestEntClick(Sender: TObject);
begin
  if EdCliente.ValueVariant <> 0 then
  begin
    Geral.MB_Aviso('OBSERVA��ES:' + sLineBreak + DmPediVda.QrClientesObservacoes.Value);
  end else
    Geral.MB_Aviso('Cliente n�o informado!');
end;

procedure TFmFatDivCab2.SpeedButton5Click(Sender: TObject);
begin
  MostraFormEntidade2(EdCliente.ValueVariant, DmPediVda.QrClientes,
    EdCliente, CBCliente);
end;

procedure TFmFatDivCab2.SpeedButton6Click(Sender: TObject);
begin
  MostraFormEntidade2(EdTransporta.ValueVariant, DmPediVda.QrTransportas,
    EdTransporta, CBTransporta);
end;

procedure TFmFatDivCab2.MostraFormEntidade2(Entidade: Integer; Query: TmySQLQuery;
  EditCliente: TdmkEditCB; ComboCliente: TdmkDBLookupComboBox);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
    //
    if Query.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EditCliente.ValueVariant := VAR_ENTIDADE;
      ComboCliente.KeyValue    := VAR_ENTIDADE;
      EditCliente.SetFocus;
    end;
  end;
end;

procedure TFmFatDivCab2.SpeedButton9Click(Sender: TObject);
{$IfNDef SemCotacoes}
var
  Moeda: Integer;
begin
  VAR_CADASTRO := 0;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdMoeda, Moeda,
    'Informe a moeda!', 'Codigo', 'CodUsu') then Exit;
  //
  if DBCheck.CriaFm(TFmCambioMda, FmCambioMda, afmoNegarComAviso) then
  begin
    if Moeda <> 0 then
      FmCambioMda.LocCod(Moeda, Moeda);
    FmCambioMda.ShowModal;
    FmCambioMda.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DModG.QrCambioMda.Close;
      UnDmkDAC_PF.AbreQuery(DModG.QrCambioMda, Dmod.MyDB);
      if DModG.QrCambioMda.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdMoeda.ValueVariant := DModG.QrCambioMdaCodUsu.Value;
        CBMoeda.KeyValue     := DModG.QrCambioMdaCodUsu.Value;
        EdMoeda.SetFocus;
      end;
    end;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

end.
