unit FatDivGru1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, Grids, ComCtrls, DmkDAC_PF,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, Variants,
  dmkLabel, Mask, dmkDBEdit, Menus, dmkImage, UnDmkEnums, UnDmkProcFunc,
  dmkRadioGroup, UnGrl_Geral;

type
  TFmFatDivGru1 = class(TForm)
    Panel1: TPanel;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraGru1Codusu: TIntegerField;
    QrGraGru1Nivel3: TIntegerField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru1GraTamCad: TIntegerField;
    QrGraGru1NOMEGRATAMCAD: TWideStringField;
    QrGraGru1CODUSUGRATAMCAD: TIntegerField;
    QrGraGru1CST_A: TSmallintField;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1UnidMed: TIntegerField;
    QrGraGru1NCM: TWideStringField;
    QrGraGru1Peso: TFloatField;
    QrGraGru1SIGLAUNIDMED: TWideStringField;
    QrGraGru1CODUSUUNIDMED: TIntegerField;
    QrGraGru1NOMEUNIDMED: TWideStringField;
    PnSeleciona: TPanel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    Label1: TLabel;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    TabSheet6: TTabSheet;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    TabSheet9: TTabSheet;
    GradeX: TStringGrid;
    QrLoc: TmySQLQuery;
    QrLocUltimo: TIntegerField;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GradeL: TStringGrid;
    GradeF: TStringGrid;
    ST3: TStaticText;
    ST2: TStaticText;
    ST1: TStaticText;
    TabSheet4: TTabSheet;
    GradeD: TStringGrid;
    StaticText3: TStaticText;
    QrGraGru1NOME_EX: TWideStringField;
    QrLista: TmySQLQuery;
    QrListaGraCusPrc: TIntegerField;
    PnJuros: TPanel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    EdCustoFin: TdmkEdit;
    Label3: TLabel;
    QrGraGru1Fracio: TSmallintField;
    DsFatPedVol: TDataSource;
    DsFatPedCab: TDataSource;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    Panel3: TPanel;
    Panel7: TPanel;
    Label5: TLabel;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    Panel8: TPanel;
    Label6: TLabel;
    Label10: TLabel;
    DBEdit5: TdmkDBEdit;
    DBEdit6: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    QrItem: TmySQLQuery;
    QrItemNOMENIVEL1: TWideStringField;
    QrItemGraCorCad: TIntegerField;
    QrItemNOMECOR: TWideStringField;
    QrItemNOMETAM: TWideStringField;
    QrItemGraGruX: TIntegerField;
    QrItemGraGru1: TIntegerField;
    QrItemCU_Nivel1: TIntegerField;
    QrItemIPI_Alq: TFloatField;
    QrItemMadeBy: TSmallintField;
    QrItemFracio: TSmallintField;
    QrItemHowBxaEstq: TSmallintField;
    QrItemGerBxaEstq: TSmallintField;
    DsItem: TDataSource;
    QrPreco_: TmySQLQuery;
    QrPreco_PrecoF: TFloatField;
    QrPreco_QuantF: TFloatField;
    QrPreco_InfAdCuztm: TIntegerField;
    QrPreco_PercCustom: TFloatField;
    QrPreco_MedidaC: TFloatField;
    QrPreco_MedidaL: TFloatField;
    QrPreco_MedidaA: TFloatField;
    QrPreco_MedidaE: TFloatField;
    QrPreco_MedOrdem: TIntegerField;
    DsPreco_: TDataSource;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    TabSheet10: TTabSheet;
    TabSheet11: TTabSheet;
    Grade1: TStringGrid;
    Grade2: TStringGrid;
    Grade3: TStringGrid;
    Grade4: TStringGrid;
    CkNome_ex: TCheckBox;
    TabSheet12: TTabSheet;
    Grade0: TStringGrid;
    QrGraGru1prod_indTot: TSmallintField;
    QrItemprod_indTot: TSmallintField;
    Label7: TLabel;
    Panel4: TPanel;
    LaAviso2B: TLabel;
    LaAviso2A: TLabel;
    SBMenu: TSpeedButton;
    PMMenu: TPopupMenu;
    Produtos1: TMenuItem;
    abelasdepreos1: TMenuItem;
    Regrasfiscais1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1B: TLabel;
    LaAviso1A: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    PB1: TProgressBar;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    DBEdit2: TDBEdit;
    Label14: TLabel;
    QrGraGru1MadeBy: TSmallintField;
    BtDadosFiscais: TBitBtn;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    QrGraGru1UsaSubsTrib: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure EdGraGru1Exit(Sender: TObject);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BtOKClick(Sender: TObject);
    procedure GradeQDblClick(Sender: TObject);
    procedure GradeQKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GradeLDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeFDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeFKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdGraGru1Enter(Sender: TObject);
    procedure GradeDDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GradeDClick(Sender: TObject);
    procedure GradeDSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure CBGraGru1Exit(Sender: TObject);
    procedure QrStqCenCadAfterOpen(DataSet: TDataSet);
    procedure QrItemBeforeClose(DataSet: TDataSet);
    procedure Grade1DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure Grade2DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure Grade3DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure Grade4DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure Grade1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Grade2KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Grade3KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Grade4KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CkNome_exClick(Sender: TObject);
    procedure Grade0DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure EdGraGru1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGru1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBMenuClick(Sender: TObject);
    procedure Produtos1Click(Sender: TObject);
    procedure abelasdepreos1Click(Sender: TObject);
    procedure Regrasfiscais1Click(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
    procedure BtDadosFiscaisClick(Sender: TObject);
  private
    { Private declarations }
    FOriCtrl,
    FOldNivel1: Integer;
    FTmp_FatPedFis: String;
    procedure ReconfiguraGradeQ();
    // Qtde de itens
    //function QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
    //function QtdeVariosItensCorTam(GridP, GridA, GridC, GridX: TStringGrid): Boolean;
    //function ObtemQtde(var Qtde: Double): Boolean;
    //
    function DescontoVariosItensCorTam(GridD, GridA, GridC, GridX:
             TStringGrid): Boolean;
    function ObtemDesconto(var Desconto: Double): Boolean;
    //
    function  ReopenItem(GraGruX: Integer): Boolean;
    procedure InsereItem2(Col, Row: Integer);
    procedure MostraFatDivGruIts(Col, Row: Integer);
    procedure BuscaPorReferencia();
    //
    procedure MostraFormGraGruN(SubForm: Integer);
    function  ValidaDadosProduto(): Boolean;
    procedure ReopenGraGru1(ZeraProduto: Boolean = True);
  public
    { Public declarations }
    FMultiGrandeza: Boolean;
  end;

  var
  FmFatDivGru1: TFmFatDivGru1;

implementation

uses
  {$IfNDef SemNFe_0000}ModuleNFe_0000, {$EndIf}
  {$IfNDef NAO_GFAT}UnGrade_Jan, UnGFat_Jan, {$EndIf}
  UnMyObjects, Module, ModProd, MyVCLSkin, GetValor, UMySQLModule,
  UnInternalConsts, GraGruVal, MyDBCheck, ModPediVda, FatDivGer1, UnGrade_Tabs,
  Principal, FatDivGruIts, ModuleGeral, ModuleFatura, UnGrade_PF, UCreate;

{$R *.DFM}

procedure TFmFatDivGru1.abelasdepreos1Click(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  TabPrc: Integer;
begin
  TabPrc := FmFatDivGer1.QrPediVdaTabelaPrc.Value;
  //
  GFat_Jan.MostraFormTabePrcCab(TabPrc);
  try
    Screen.Cursor := crHourGlass;
    ReconfiguraGradeQ();
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmFatDivGru1.BtDadosFiscaisClick(Sender: TObject);
var
  GraGru1: Integer;
begin
  if EdGraGru1.ValueVariant <> 0 then
    GraGru1 := QrGraGru1Nivel1.Value
  else
    GraGru1 := 0;
  //
  GFat_Jan.MostraFormFatGruFisRegCad(FmFatDivGer1.QrCliCodigo.Value,
    FmFatDivGer1.QrPediVdaEmpresa.Value, FmFatDivGer1.QrPediVdaRegrFiscal.Value,
    Trunc(FmFatDivGer1.QrCliUF.Value), FmFatDivGer1.QrFatPedCabEMP_UF.Value,
    GraGru1, QrGraGru1MadeBy.Value, FmFatDivGer1.QrFatPedCabCodigo.Value,
    FmFatDivGer1.QrFatPedVolCnta.Value, FmFatDivGer1.QrCliIE.Value, FTmp_FatPedFis);
end;

procedure TFmFatDivGru1.BtOKClick(Sender: TObject);

  function ValidaDadosFiscais(var Mensagem: String): Boolean;
  var
    QryTmpTable: TmySQLQuery;
    OriCodi, OriCnta, GraGru1, idDest, indFinal, indIEDest, RegrFiscal: Integer;
    CFOP, UFEmit, UFDest, CST_B, NO_RegrFiscal, Cli_IE, Cli_UF, Emp_UF: String;
  begin
    Result      := True;
    Mensagem    := '';
    QryTmpTable := TmySQLQuery.Create(TDataModule(DModG.MyPID_DB.Owner));
    try
      OriCodi := FmFatDivGer1.QrFatPedCabCodigo.Value;
      OriCnta := FmFatDivGer1.QrFatPedVolCnta.Value;
      //
      if EdGraGru1.ValueVariant <> 0 then
        GraGru1 := QrGraGru1Nivel1.Value
      else
        GraGru1 := 0;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QryTmpTable, DModG.MyPID_DB, [
        'SELECT * ',
        'FROM ' + FTmp_FatPedFis,
        'WHERE OriCodi=' + Geral.FF0(OriCodi),
        'AND OriCnta=' + Geral.FF0(OriCnta),
        'AND GraGru1=' + Geral.FF0(GraGru1),
        '']);
      if QryTmpTable.RecordCount > 0 then
      begin
        CFOP   := QryTmpTable.FieldByName('CFOP').AsString;
        UFEmit := QryTmpTable.FieldByName('UFEmit').AsString;
        UFDest := QryTmpTable.FieldByName('UFDest').AsString;
        CST_B  := QryTmpTable.FieldByName('CST_B').AsString;
        //
        if not Grade_PF.ValidaPainelFisRegCad(CFOP, UFEmit, UFDest, CST_B, Mensagem)
        then
          Result := False;
      end else
      begin
        idDest        := FmFatDivGer1.QrPediVdaidDest.Value;
        indFinal      := FmFatDivGer1.QrPediVdaindFinal.Value;
        indIEDest     := FmFatDivGer1.QrCliindIEDest.Value;
        RegrFiscal    := FmFatDivGer1.QrPediVdaRegrFiscal.Value;
        NO_RegrFiscal := FmFatDivGer1.QrPediVdaNOMEFISREGCAD.Value;
        Cli_IE        := FmFatDivGer1.QrCliIE.Value;
        Cli_UF        := FmFatDivGer1.QrCliNOMEUF.Value;
        Emp_UF        := Geral.GetSiglaUF_do_CodigoUF(FmFatDivGer1.QrFatPedCabEMP_UF.Value);
        //
        if not DmPediVda.ValidaRegraFiscalCFOPCliente(idDest, indFinal, indIEDest,
          RegrFiscal, NO_RegrFiscal, Cli_IE, Cli_UF, Emp_UF, Mensagem)
        then
          Result := False
        else
        if not DmPediVda.ValidaRegraFiscalCFOPProdutos(QrGraGru1PrdGrupTip.Value,
          RegrFiscal, NO_RegrFiscal, Mensagem)
        then
          Result := False;
      end;
    finally
      QryTmpTable.Free;
    end;
  end;

var
  Col, Row, GraGruX, Codigo: Integer;
  Tot, Qtd, PrecoO, PrecoR: Double;
  ErrPreco, AvisoPrc, QtdRed, Sim: Integer;
  Mensagem: String;
begin
  if EdGraGru1.ValueVariant = 0 then
  begin
    Geral.MB_Aviso('Defina o produto!');
    Exit;
  end;
  //
  if not ValidaDadosProduto() then Exit;
  //
  if not ValidaDadosFiscais(Mensagem) then
  begin
    Geral.MB_Aviso(Mensagem);
    Exit;
  end;
  //
  //Nivel1 := QrGraGru1Nivel1.Value;
  Screen.Cursor := crHourGlass;
  try
    Codigo := FmFatDivGer1.QrFatPedCabCodigo.Value;
    Sim       := 0;
    Tot       := 0;
    ErrPreco  := 0;
    AvisoPrc  := 0;
    QtdRed    := 0;
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      for Row := 1 to GradeQ.RowCount - 1 do
      begin
        Qtd := Geral.DMV(GradeQ.Cells[Col,Row]);
        Sim := Geral.IMV(GradeX.Cells[Col,Row]);
        Tot := Tot + Qtd + Sim;
        if Qtd > 0 then
          QtdRed := QtdRed + 1;
        PrecoO := Geral.DMV(GradeL.Cells[Col,Row]);
        PrecoR := Geral.DMV(GradeF.Cells[Col,Row]);
        if (PrecoO <>  PrecoR) and (Qtd = 0) then
          AvisoPrc := AvisoPrc + 1;
        if ((Qtd > 0) or (Sim > 0)) and (PrecoR = 0) then
          ErrPreco := ErrPreco + 1;
      end;
    end;
    if (Tot = 0) and (Sim = 0) then
    begin
      Screen.Cursor := crDefault;
      Geral.MB_Aviso('N�o h� defini��o de quantidades!');
      Exit;
    end
    else if ErrPreco > 0 then
    begin
      Screen.Cursor := crDefault;
      Geral.MB_Aviso('Existem ' + Geral.FF0(ErrPreco) +
        ' itens sem pre�o de faturamento definido!');
      //
      Exit;
    end
    else if AvisoPrc > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(AvisoPrc) +
        ' itens com pre�o de faturamento alterado, mas n�o h� defini��o de ' +
        'quantidades de itens!' + sLineBreak +
        'Deseja continuar assim mesmo?') <> ID_YES then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    PB1.Position := 0;
    PB1.Max := QtdRed;
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      if GradeX.Cells[Col,0] <> '' then
      begin
        for Row := 1 to GradeQ.RowCount - 1 do
        begin
          if Geral.IMV(GradeX.Cells[0,Row]) > 0 then
          begin
            Qtd := Geral.DMV(GradeQ.Cells[Col, Row]);
            Sim := Geral.IMV(GradeX.Cells[Col, Row]);
            if (Qtd > 0) or (Sim > 0) then
            begin
              //ShowMessage('Col: ' + Geral.FF0(Col) + '  Row: ' + Geral.FF0(Row) + sLineBreak + Geral.FF0(Qtd));
              GraGruX := Geral.IMV(GradeC.Cells[Col, Row]);
              if GraGruX > 0 then
              begin
                //Casas  := Dmod.QrControleCasasProd.Value;
                //PrecoR := Geral.DMV(GradeF.Cells[Col, Row]);
                //ValCal := PrecoR;// * QuantP;
                //DescoP := Geral.DMV(GradeD.Cells[Col, Row]);
                //
{
                PrecoO := Geral.DMV(GradeL.Cells[Col, Row]);
                QuantP := Geral.DMV(GradeQ.Cells[Col, Row]);
                DescoI := ???.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
                PrecoF := PrecoR - DescoI;
                DescoV := DescoI * QuantP;
                ValBru := PrecoR * QuantP;
                ValLiq := ValBru - DescoV;
}
                //
{
                Controle := UMyMod.BuscaEmLivreY_Def('pedivdaits', 'Controle',
                  ImgTipo.SQLType, 0);
                if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pedivdaits', False,[
                'Codigo', 'GraGruX', 'PrecoO',
                'PrecoR', 'QuantP', 'ValBru',
                'DescoP', 'DescoV', 'ValLiq',
                'PrecoF'  ], [
                'Controle'], [
                Codigo, GraGruX, PrecoO,
                PrecoR, QuantP, ValBru,
                DescoP, DescoV, ValLiq,
                PrecoF], [
                Controle], True) then
                DmPediVda.AtzSdosPedido(Codigo);
}
                if ReopenItem(GraGruX) then
                begin
                  InsereItem2(Col, Row);
                  //Parei Aqui Acho que n�o precisa, pois n�o tem pedido.
                  //ReopenPediGru(QrPediGruNivel1.Value);
                  FmFatDivGer1.ReopenFatPedIts(FOriCtrl);
                end;
              end;
            end;
          end;
        end;
      end;
    end;
    FmFatDivGer1.LocCod(Codigo, Codigo);
    FmFatDivGer1.ReopenFatPedIts(0);// Parei aqui! ver
    EdGraGru1.ValueVariant := 0;
    //
    PageControl1.ActivePageIndex := 0;
    //
    if EdGraGru1.Enabled then
      EdGraGru1.SetFocus;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFatDivGru1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatDivGru1.BuscaPorReferencia();
var
  Nivel1, Nivel2, Nivel3, PrdGrupTip, Controle: Integer;
  Referencia: String;
  PerComissF, PerComissR: Double;
  //
  Ref: String;
begin
  Ref := '';
  if InputQuery('Pesquisa por Refer�ncia', 'Informe a refer�ncia', Ref) then
  if Trim(Ref) <> '' then
  begin
    if DmProd.ReopenLocRef(tpRef,  Ref,
      Nivel1, Nivel2, Nivel3, PrdGrupTip, Controle,
      Referencia, PerComissF, PerComissR) = 1 then
    begin
      EdGraGru1.ValueVariant := Nivel1;
      CBGraGru1.KeyValue     := Nivel1;
    end;
  end;
end;

procedure TFmFatDivGru1.CBGraGru1Exit(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  //GradeQ.SetFocus; //D� erro sen�o selecionar nenhum produto
  GradeQ.Col := 1;
  GradeQ.Row := 1;
end;

procedure TFmFatDivGru1.CBGraGru1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
    BuscaPorReferencia();
end;

procedure TFmFatDivGru1.CkNome_exClick(Sender: TObject);
begin
  if CkNome_ex.Checked then
    CBGraGru1.ListField := 'NOME_EX'
  else
    CBGraGru1.ListField := 'Nome';
end;

function TFmFatDivGru1.DescontoVariosItensCorTam(GridD, GridA, GridC,
  GridX: TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL: Integer;
  Desconto: Double;
  DescontoTxt, ItensTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridD.Col;
  Linha  := GridD.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridD.ColCount - 1;
    RowI := 1;
    RowF := GridD.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridD.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridD.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  //Nivel1 := QrGraGru1Nivel1.Value;
  Desconto  := Geral.DMV(GridD.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Geral.MB_Aviso('N�o h� nenhum item com c�digo na sele��o ' +
      'para que se possa incluir / alterar o pre�o!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if ObtemDesconto(Desconto) then
  begin
    DescontoTxt := Geral.FFT(Desconto, 2, siNegativo);
    if (Coluna = 0) and (Linha = 0) then
    begin
      ItensTxt := 'todo grupo';
    end else if Coluna = 0 then
    begin
      ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
    end else if Linha = 0 then
    begin
      ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
    end;
    if Geral.MB_Pergunta('Confirma o valor de ' + DescontoTxt + ' para ' +
      ItensTxt + '?') <> ID_YES then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    for c := ColI to ColF do
      for l := RowI to RowF do
        GridD.Cells[c, l] := DescontoTxt;
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmFatDivGru1.EdGraGru1Change(Sender: TObject);
begin
  if EdGraGru1.Focused = False then
    ReconfiguraGradeQ();
end;

procedure TFmFatDivGru1.EdGraGru1Enter(Sender: TObject);
begin
  FOldNivel1 := EdGraGru1.ValueVariant;
end;

procedure TFmFatDivGru1.EdGraGru1Exit(Sender: TObject);
begin
  if FOldNivel1 <> EdGraGru1.ValueVariant then
  begin
    FOldNivel1 := EdGraGru1.ValueVariant;
    ReconfiguraGradeQ();
  end;
end;

procedure TFmFatDivGru1.EdGraGru1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
    BuscaPorReferencia();
end;

procedure TFmFatDivGru1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatDivGru1.FormCreate(Sender: TObject);
(*
var
  SQL: TStringList;
*)
begin
  ImgTipo.SQLType := stLok;
  //
  BtDadosFiscais.Visible := True;
  //
  FMultiGrandeza := FmFatDivGer1.FMultiGrandeza;
  ST1.Caption := '';
  ST2.Caption := '';
  ST3.Caption := '';
  ST1.Font.Color := clRed;
  ST2.Font.Color := clRed;
  ST3.Font.Color := clRed;
  ST1.Color := PnSeleciona.Color;
  ST2.Color := PnSeleciona.Color;
  ST3.Color := PnSeleciona.Color;
  //
  PageControl1.ActivePageIndex := 0;
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  GradeL.ColWidths[0] := 128;
  GradeF.ColWidths[0] := 128;
  GradeD.ColWidths[0] := 128;
  //
  if not Grade_PF.ReopenFatStqCenCad(FmFatDivGer1.QrFatPedCabRegrFiscal.Value,
    FmFatDivGer1.QrFatPedCabEmpresa.Value, DModG.QrFiliLogNomeEmp.Value,
    FmFatDivGer1.QrPediVdaNOMEFISREGCAD.Value, QrStqCenCad) then
  begin
    if Geral.MB_Pergunta('Deseja configurar a Regra Fiscal agora?') = ID_YES then
    begin
      Grade_Jan.MostraFormFisRegCad(FmFatDivGer1.QrFatPedCabRegrFiscal.Value);
      UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
    end;
  end;
  //
  ReopenGraGru1();
  //
  FTmp_FatPedFis := UCriar.RecriaTempTableNovo(ntrttFatPedFis, DmodG.QrUpdPID1, False);
  //
  TabSheet12.TabVisible := False;
end;

procedure TFmFatDivGru1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1A, LaAviso1B], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatDivGru1.Grade1DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(Grade1, GradeA, nil, ACol, ARow, Rect, State,
  dmkPF.FormataCasas(QrGraGru1Fracio.Value), 0, 0, False);
end;

procedure TFmFatDivGru1.Grade1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInputEx(Grade1, GradeC, Key, Shift, BtOK);
end;

procedure TFmFatDivGru1.Grade2DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(Grade2, GradeA, nil, ACol, ARow, Rect, State,
  '#,###,###,##0.000', 0, 0, False);
end;

procedure TFmFatDivGru1.Grade2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInputEx(Grade2, GradeC, Key, Shift, BtOK);
end;

procedure TFmFatDivGru1.Grade3DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(Grade3, GradeA, nil, ACol, ARow, Rect, State,
  '#,###,###,##0.00', 0, 0, False);
end;

procedure TFmFatDivGru1.Grade3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInputEx(Grade3, GradeC, Key, Shift, BtOK);
end;

procedure TFmFatDivGru1.Grade4DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(Grade4, GradeA, nil, ACol, ARow, Rect, State,
  '#,###,###,##0.00', 0, 0, False);
end;

procedure TFmFatDivGru1.Grade4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInputEx(Grade4, GradeC, Key, Shift, BtOK);
end;

procedure TFmFatDivGru1.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmFatDivGru1.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);;
end;

procedure TFmFatDivGru1.GradeDClick(Sender: TObject);
begin
  if (GradeD.Col = 0) or (GradeD.Row = 0) then
    DescontoVariosItensCorTam(GradeD, GradeA, GradeC, GradeX)
end;

procedure TFmFatDivGru1.GradeDDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeD, GradeA, nil, ACol, ARow, Rect, State,
  '0.0000' + ' %', 0, 0, False);
end;

procedure TFmFatDivGru1.GradeDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInput(GradeD, GradeC, Key, Shift);
end;

procedure TFmFatDivGru1.GradeDSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
{
  if (ACol = 0) or (ARow = 0) then
    GradeD.Options := GradeD.Options - [goEditing]
  else
    GradeD.Options := GradeD.Options + [goEditing];
}
end;

procedure TFmFatDivGru1.GradeFDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeF, GradeA, GradeL, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmFatDivGru1.GradeFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInput(GradeF, GradeC, Key, Shift);
end;

procedure TFmFatDivGru1.GradeLDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeL, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, True);;
end;

procedure TFmFatDivGru1.GradeQDblClick(Sender: TObject);
begin
{
  if (GradeQ.Col = 0) or (GradeQ.Row = 0) then
    QtdeVariosItensCorTam(GradeQ, GradeA, GradeC, GradeX)
  else
    QtdeItemCorTam(GradeQ, GradeC, GradeX);
}
  if (GradeQ.Col = 0) or (GradeQ.Row = 0) then
    Exit;
  MostraFatDivGruIts(GradeQ.Col, GradeQ.Row);
end;

procedure TFmFatDivGru1.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQ, GradeA, nil, ACol, ARow, Rect, State,
  dmkPF.FormataCasas(QrGraGru1Fracio.Value), 0, 0, False);
end;

procedure TFmFatDivGru1.GradeQKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInputEx(GradeQ, GradeC, Key, Shift, BtOK);
end;

procedure TFmFatDivGru1.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, nil, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);;
end;

procedure TFmFatDivGru1.MostraFatDivGruIts(Col, Row: Integer);
var
  GraGruX, StqCenCad, RegrFiscal, Empresa, TipoCalc: Integer;
begin
  RegrFiscal := FmFatDivGer1.QrFatPedCabRegrFiscal.Value;
  Empresa    := FmFatDivGer1.QrFatPedCabEmpresa.Value;
  StqCenCad  := EdStqCenCad.ValueVariant;
  //
  if MyObjects.FIC(StqCenCad = 0, EdStqCenCad, 'Informe o centro de estque!') then
    Exit
  else
    StqCenCad := QrStqCenCadCodigo.Value;
  //
  if not ValidaDadosProduto() then Exit;
  //
  TipoCalc := Grade_PF.ObtemFisRegMvtTipoCalc(RegrFiscal, StqCenCad, Empresa);
  //
  if MyObjects.FIC(StqCenCad = 0, EdStqCenCad, 'Informe o centro de estoque!') then
    Exit;

  if DBCheck.CriaFm(TFmFatDivGruIts, FmFatDivGruIts, afmoNegarComAviso) then
  begin
    GraGruX := Geral.IMV(GradeC.Cells[Col, Row]);
    //
    ReopenItem(GraGruX);
    //
    FmFatDivGruIts.FCol        := Col;
    FmFatDivGruIts.FRow        := Row;
    FmFatDivGruIts.FHowBxaEstq := QrItemHowBxaEstq.Value;
    //
    FmFatDivGruIts.EdNivel1.ValueVariant   := EdGraGru1.ValueVariant;
    FmFatDivGruIts.EdNomeNivel1.Text       := CBGraGru1.Text;
    FmFatDivGruIts.EdGraGruX.Text          := Geral.FF0(GraGruX);
    FmFatDivGruIts.EdNomeTam.Text          := GradeQ.Cells[Col,0];
    FmFatDivGruIts.EdNomeCor.Text          := GradeQ.Cells[0, Row];
    //
    FmFatDivGruIts.EdQtde.ValueVariant     := Geral.DMV(GradeQ.Cells[Col,Row]);
    FmFatDivGruIts.EdPreco.ValueVariant    := Geral.DMV(GradeF.Cells[Col,Row]);
    FmFatDivGruIts.EdDesco.ValueVariant    := Geral.DMV(GradeD.Cells[Col,Row]);
    FmFatDivGruIts.EdTipoCalc.ValueVariant := TipoCalc;
    //
    FmFatDivGruIts.EdPecas.ValueVariant    := Geral.DMV(Grade1.Cells[Col,Row]);
    FmFatDivGruIts.EdPeso.ValueVariant     := Geral.DMV(Grade2.Cells[Col,Row]);
    FmFatDivGruIts.EdAreaM2.ValueVariant   := Geral.DMV(Grade3.Cells[Col,Row]);
    FmFatDivGruIts.EdAreaP2.ValueVariant   := Geral.DMV(Grade4.Cells[Col,Row]);
    //
    FmFatDivGruIts.ShowModal;
    //
    if FmFatDivGruIts.FConfirmou then
    begin
      GradeQ.Cells[Col,Row] := FmFatDivGruIts.EdQtde.Text;
      GradeF.Cells[Col,Row] := FmFatDivGruIts.EdPreco.Text;
      GradeD.Cells[Col,Row] := FmFatDivGruIts.EdDesco.Text;

      GradeX.Cells[Col,Row] := '1';

      Grade1.Cells[Col,Row] := FmFatDivGruIts.EdPecas.Text;
      Grade2.Cells[Col,Row] := FmFatDivGruIts.EdPeso.Text;
      Grade3.Cells[Col,Row] := FmFatDivGruIts.EdAreaM2.Text;
      Grade4.Cells[Col,Row] := FmFatDivGruIts.EdAreaP2.Text;
    end;
    //
    FmFatDivGruIts.Destroy;
  end;
end;

procedure TFmFatDivGru1.ReconfiguraGradeQ();
var
  (*
  MediaSel, TaxaM, Juros: Double;
  Lista: Integer;
  *)
  MedDDSimpl, MedDDReal: Double;
  Tabela, Grade, Nivel1, CondicaoPG: Integer;
begin
  PnJuros.Visible := False;
  Tabela     := FmFatDivGer1.QrFatPedCabTabelaPrc.Value;
  MedDDSimpl := FmFatDivGer1.QrFatPedCabMedDDSimpl.Value;
  MedDDReal  := FmFatDivGer1.QrFatPedCabMedDDReal.Value;
  Grade      := QrGraGru1GraTamCad.Value;
  Nivel1     := QrGraGru1Nivel1.Value;
  CondicaoPG := FmFatDivGer1.QrFatPedCabCondicaoPG.Value;
  //
  if Tabela > 0 then
  begin
    PageControl1.Visible := True;
    //
    DmProd.ConfigGrades5(Grade, Nivel1, Tabela, CondicaoPG,
      GradeA, GradeX, GradeC, GradeL, GradeF, GradeQ, GradeD,
      MedDDSimpl, MedDDReal, ST1, ST2, ST3, ImgTipo, False,
      DmPediVda.QrParamsEmpTipMediaDD.Value,
      DmPediVda.QrParamsEmpFatSemPrcL.Value);
    DmProd.PreparaGrades(Grade, Nivel1, [Grade1, Grade2, Grade3, Grade4]);
  end else
  begin
    PageControl1.Visible := False;
    //
    MyObjects.LimpaGrade(GradeQ, 0, 0, True);
    MyObjects.LimpaGrade(GradeF, 0, 0, True);
    MyObjects.LimpaGrade(GradeD, 0, 0, True);
    MyObjects.LimpaGrade(GradeL, 0, 0, True);
    MyObjects.LimpaGrade(GradeC, 0, 0, True);
    MyObjects.LimpaGrade(GradeA, 0, 0, True);
    MyObjects.LimpaGrade(GradeX, 0, 0, True);
    MyObjects.LimpaGrade(Grade1, 0, 0, True);
    MyObjects.LimpaGrade(Grade2, 0, 0, True);
    MyObjects.LimpaGrade(Grade3, 0, 0, True);
    MyObjects.LimpaGrade(Grade4, 0, 0, True);
    //
    Geral.MB_Aviso('Tabela de pre�o n�o definida!');
    (*
    2017-08-23 => N�o existe mais a op��o de selecionar a tabela na movimenta��o da Regra Fiscal

    QrLista.Close;
    QrLista.Params[00].AsInteger := FmFatDivGer1.QrFatPedCabEmpresa.Value;
    QrLista.Params[01].AsInteger := FmFatDivGer1.QrFatPedCabRegrFiscal.Value;
    UnDmkDAC_PF.AbreQuery(QrLista, Dmod.MyDB);
    if QrLista.RecordCount = 1 then
    begin
      PageControl1.Visible := True;
      Lista := QrListaGraCusPrc.Value;
      case DmPediVda.QrParamsEmpTipMediaDD.Value of
        1: MediaSel := MedDDSimpl;
        2: MediaSel := MedDDReal;
        else MediaSel := 0;
      end;
      TaxaM := FmFatDivGer1.QrFatPedCabJurosMes.Value;
      case DmPediVda.QrParamsEmpTipCalcJuro.Value of
        1: Juros := dmkPF.CalculaJuroSimples(TaxaM, MediaSel);
        2: Juros := dmkPF.CalculaJuroComposto(TaxaM, MediaSel);
        else Juros := 0;
      end;
      EdCustoFin.ValueVariant := Juros;
      PnJuros.Visible := True;
      DmProd.ConfigGrades9(Grade, Nivel1, Lista, FmFatDivGer1.QrFatPedCabEmpresa.Value,
      GradeA, GradeX, GradeC, GradeL, GradeF, GradeQ, GradeD,
      Grade0, Grade1, Grade2, grade3, Grade4,
      Juros, ST1, ST2, ST3, ImgTipo);
    end else begin
      PageControl1.Visible := False;
      //
      MyObjects.LimpaGrade(GradeQ, 0, 0, True);
      MyObjects.LimpaGrade(GradeF, 0, 0, True);
      MyObjects.LimpaGrade(GradeD, 0, 0, True);
      MyObjects.LimpaGrade(GradeL, 0, 0, True);
      MyObjects.LimpaGrade(GradeC, 0, 0, True);
      MyObjects.LimpaGrade(GradeA, 0, 0, True);
      MyObjects.LimpaGrade(GradeX, 0, 0, True);
      MyObjects.LimpaGrade(Grade0, 0, 0, True);
      MyObjects.LimpaGrade(Grade1, 0, 0, True);
      MyObjects.LimpaGrade(Grade2, 0, 0, True);
      MyObjects.LimpaGrade(Grade3, 0, 0, True);
      MyObjects.LimpaGrade(Grade4, 0, 0, True);
      //
      if QrLista.RecordCount = 0 then
        Geral.MB_Aviso('Nenhuma lista est� definida com o tipo ' +
        'de movimento "Sa�da" na regra fiscal "' + FmFatDivGer1.QrFatPedCabNOMEFISREGCAD.Value
        + '" para a empresa ' + FormatFloat('000', FmFatDivGer1.QrFatPedCabEMP_FILIAL.Value)
        + '!')
      else
        Geral.MB_Aviso('H� ' + Geral.FF0(QrLista.RecordCount) +
          'listas habilitadas para o movimento "Sa�da" na regra fiscal "' +
          FmFatDivGer1.QrFatPedCabNOMEFISREGCAD.Value + '" para a empresa ' +
          FormatFloat('000', FmFatDivGer1.QrFatPedCabEMP_FILIAL.Value) +
          '. Nenhuma ser� considerada!');
    end;
    *)
  end;
end;

procedure TFmFatDivGru1.Regrasfiscais1Click(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  FisRegCad: Integer;
begin
  FisRegCad := FmFatDivGer1.QrPediVdaRegrFiscal.Value;
  //
  Grade_Jan.MostraFormFisRegCad(FisRegCad);
  try
    Screen.Cursor := crHourGlass;
    UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
    ReconfiguraGradeQ();
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmFatDivGru1.ReopenGraGru1(ZeraProduto: Boolean = True);
begin
  DmProd.ReopenGraGru1(QrGraGru1, -1, FmFatDivGer1.QrFatPedCabID_Pedido.Value,
    '>', CBGraGru1);
  //
  if ZeraProduto then
  begin
    EdGraGru1.ValueVariant := 0;
    CBGraGru1.KeyValue     := 0;
  end;
end;

function TFmFatDivGru1.ReopenItem(GraGruX: Integer): Boolean;
begin
  Result := False;
  QrItem.Close;
  if GraGruX > 0 then
  begin
    QrItem.Params[0].AsInteger := GraGrux;
    UMyMod.AbreQuery(QrItem, Dmod.MyDB, 'TFmFatPedIts.ReopenItem()');
    //
    if QrItem.RecordCount > 0 then
    begin
      Result := True;
      //if PnLeitura.Enabled then
        //EdQtdLei.SetFocus;
      //ReopenPrecoSel(FmFatDivGer1.QrFatPedCabID_Pedido.Value, GraGruX);
    end else
      Geral.MB_Aviso('Reduzido n�o localizado!');
  end;
end;

procedure TFmFatDivGru1.SBMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMenu, SBMenu);
end;

procedure TFmFatDivGru1.MostraFormGraGruN(SubForm: Integer);
{$IfNDef NAO_GFAT}
var
  SubFrm: TSubForm;
  Nivel1: Integer;
begin
  if EdGraGru1.ValueVariant <> 0 then
    Nivel1 := QrGraGru1Nivel1.Value
  else
    Nivel1 := 0;
  //
  if SubForm = 1 then
    SubFrm := tsfDadosGerais
  else if SubForm = 2 then
    SubFrm := tsfDadosFiscais
  else
    SubFrm := tsfNenhum;
  //
  VAR_CADASTRO2 := 0;
  //
  Grade_Jan.MostraFormGraGruN(Nivel1, 0, tpGraCusPrc, SubFrm);
  try
    Screen.Cursor := crHourGlass;
    //
    if VAR_CADASTRO2 <> 0 then
      Nivel1 := VAR_CADASTRO2;
    //
    UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
    QrGraGru1.Locate('Nivel1', Nivel1, []);
    //
    ReconfiguraGradeQ();
    ReopenGraGru1(False);
    //
    UMyMod.SetaCodUsuDeCodigo(EdGraGru1, CBGraGru1, QrGraGru1, Nivel1, 'Nivel1',
      'CodUsu');
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

function TFmFatDivGru1.ValidaDadosProduto: Boolean;
begin
  Result := True;
  //
  if (QrGraGru1NCM.Value = '') and (DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0) then
  begin
    Result := False;
    //
    Geral.MB_Aviso('O produto selecionado n�o possui NCM cadastrado!');
    //
    if Geral.MB_Pergunta('Deseja cadastrar agora?') = ID_YES then
    begin
      MostraFormGraGruN(Integer(tsfDadosFiscais));
    end;
    Exit;
  end;
  if (QrGraGru1UnidMed.Value = 0) and (DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0) then
  begin
    Result := False;
    //
    Geral.MB_Aviso('O produto selecionado n�o possui Unidade de Medida cadastrada!');
    //
    if Geral.MB_Pergunta('Deseja cadastrar agora?') = ID_YES then
    begin
      MostraFormGraGruN(Integer(tsfDadosGerais));
    end;
    Exit;
  end;
end;

procedure TFmFatDivGru1.Grade0DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(Grade0, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, False);
end;

function TFmFatDivGru1.ObtemDesconto(var Desconto: Double): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmGraGruVal, FmGraGruVal, afmoNegarComAviso) then
  begin
    FmGraGruVal.EdValor.ValueVariant := Desconto;
    FmGraGruVal.ShowModal;
    Result   := FmGraGruVal.FConfirmou;
    Desconto := FmGraGruVal.EdValor.ValueVariant;
    FmGraGruVal.Destroy;
  end;
end;

procedure TFmFatDivGru1.Produtos1Click(Sender: TObject);
begin
  MostraFormGraGruN(Integer(tsfNenhum));
end;

{
function TFmFatDivGru.ObtemQtde(var Qtde: Double): Boolean;
var
  ResVar: Variant;
  CasasDecimais: Integer;
begin
  Qtde          := 0;
  Result        := False;
  CasasDecimais := QrGraGru1Fracio.Value;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  0, CasasDecimais, 0, '', '', True, 'Itens', 'Informe a quantidade de itens: ',
  0, ResVar) then
  begin
    Qtde := Geral.DMV(ResVar);
    Result := True;
  end;
end;
}

procedure TFmFatDivGru1.QrItemBeforeClose(DataSet: TDataSet);
begin
  QrPreco_.Close;
end;

procedure TFmFatDivGru1.QrStqCenCadAfterOpen(DataSet: TDataSet);
begin
  EdStqCenCad.ValueVariant := QrStqCenCadCodUsu.Value;
  CBStqCenCad.KeyValue     := QrStqCenCadCodUsu.Value;
  PageControl1.ActivePageIndex := 0;
end;

{
function TFmFatDivGru.QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
var
  Qtde: Double;
  c, l, GraGruX, Coluna, Linha: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  c := Geral.IMV(GridX.Cells[Coluna, 0]);
  l := Geral.IMV(GridX.Cells[0, Linha]);
  GraGruX := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if GraGruX = 0 then
  begin
    Geral.MB_Aviso('O item nunca foi ativado!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = 0) or (l = 0) or (GraGruX = 0) then Exit;
  //
  //Nivel1 := QrGraGru1Nivel1.Value;
  Qtde  := Geral.DMV(GridP.Cells[Coluna, Linha]);
  if ObtemQtde(Qtde) then
    GridP.Cells[Coluna, Linha] := FloatToStr(Qtde);
  Result := True;
  Screen.Cursor := crDefault;
end;
}

{
function TFmFatDivGru.QtdeVariosItensCorTam(GridP, GridA, GridC, GridX:
TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL, GraGruX: Integer;
  Qtde: Double;
  QtdeTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Geral.MB_Aviso('Tamanho n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Geral.MB_Aviso('Cor n�o definida!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  //Nivel1 := QrGraGru1Nivel1.Value;
  Qtde   := Geral.IMV(GridP.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Geral.MB_Aviso('N�o h� nenhum item com c�digo na sele��o ' +
      'para que se possa incluir / alterar o pre�o!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if ObtemQtde(Qtde) then
  begin
    QtdeTxt := FloatToStr(Qtde);
    (*
    if (Coluna = 0) and (Linha = 0) then
    begin
      ItensTxt := 'todo grupo';
    end else if Coluna = 0 then
    begin
      ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
    end else if Linha = 0 then
    begin
      ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
    end;
    if Geral.MB_Pergunta('Confirma a quantidade de ' + QtdeTxt +
      ' para ' + ItensTxt + '?') <> ID_YES then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    *)
    for c := ColI to ColF do
      for l := RowI to RowF do
    begin
      GraGruX := Geral.IMV(GridC.Cells[c, l]);
      //
      if GraGruX > 0 then
        GradeQ.Cells[c,l] := QtdeTxt;
    end;
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;
}

procedure TFmFatDivGru1.ImgTipoChange(Sender: TObject);
begin
  if ImgTipo.SQLType in ([stIns, stUpd]) then
  begin
    BtOK.Enabled           := True;
    BtDadosFiscais.Enabled := True;
  { s� por duplo clique!
    GradeQ.Options := GradeQ.Options + [goEditing];
    GradeD.Options := GradeD.Options + [goEditing];
}
  end else
  begin
    BtOK.Enabled           := False;
    BtDadosFiscais.Enabled := False;
{    GradeQ.Options := GradeQ.Options - [goEditing];
    GradeD.Options := GradeQ.Options - [goEditing];
  }
  end;
end;

procedure TFmFatDivGru1.InsereItem2(Col, Row: Integer);
var
  NFe_FatID, Cliente, RegrFiscal, GraGruX,
  StqCenCad, FatSemEstq, AFP_Sit: Integer;
  AFP_Per: Double;
  Cli_Tipo: Integer;
  Cli_IE: String;
  Cli_UF, EMP_UF, EMP_FILIAL,
  ASS_CO_UF, ASS_FILIAL, Item_MadeBy: Integer;
  Item_IPI_ALq: Double;
  Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
  Preco_MedidaA, Preco_MedidaE: Double;
  Preco_MedOrdem: Integer;
  SQLType: TSQLType;
  PediVda: Integer;
  TabelaPrc, OriCodi, Empresa, OriCnta, Associada, OriPart, InfAdCuztm: Integer;
  NO_tablaPrc: String;
  //
  Falta, Pecas, AreaM2, AreaP2, Peso: Double;
  Msg: String;
//var
  ErrPreco, AvisoPrc, QtdRed, Casas, Nivel1, Codigo, Controle: Integer;
  Tot, Qtd, PrecoO, PrecoR, PrecoF, QuantP, ValBru, ValLiq, DescoP, DescoV,
  DescoI: Double;
  //
// var
  Qtde: Double;
  TipoCalc, prod_indTot, IDCtrl, GraGru1: Integer;
  prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
  UsaSubsTrib: Integer;
begin
  InfAdCuztm := 0;
  //StqCenCad        := Geral.IMV(EdStqCenCad.Text);
  if not UMyMod.ObtemCodigoDeCodUsu(EdStqCenCad, StqCencad,
    'Informe o Centro de estoque!', 'Codigo', 'CodUsu') then Exit;
  //
  NFe_FatID        := VAR_FATID_0001;
  Cliente          := FmFatDivGer1.QrFatPedCabCliente.Value; //QrCliCodigo.Value;
  RegrFiscal       := FmFatDivGer1.QrFatPedCabRegrFiscal.Value; //QrPediVdaRegrFiscal.Value;
  GraGruX          := QrItemGraGruX.Value;
  prod_indTot      := QrItemprod_indTot.Value;
  FatSemEstq       := FmFatDivGer1.QrFatPedCabFatSemEstq.Value;
  AFP_Sit          := FmFatDivGer1.QrFatPedCabAFP_Sit.Value;
  AFP_Per          := FmFatDivGer1.QrFatPedCabAFP_Per.Value;
  Cli_Tipo         := FmFatDivGer1.QrCliTipo.Value;
  Cli_IE           := FmFatDivGer1.QrCliIE.Value;
  Cli_UF           := Trunc(FmFatDivGer1.QrCliUF.Value);
  EMP_UF           := FmFatDivGer1.QrFatPedCabEMP_UF.Value;
  EMP_FILIAL       := FmFatDivGer1.QrFatPedCabEMP_FILIAL.Value;
  ASS_CO_UF        := FmFatDivGer1.QrFatPedCabASS_CO_UF.Value;
  ASS_FILIAL       := FmFatDivGer1.QrFatPedCabASS_FILIAL.Value;
  Item_MadeBy      := QrItemMadeBy.Value;
  ITEM_IPI_Alq     := QrItemIPI_Alq.Value;
  Empresa          := FmFatDivGer1.QrFatPedCabEmpresa.Value;
  //
  //TipoCalc := Geral.IMV(Grade0.Cells[Col, Row]);
  TipoCalc := DmProd.DefineTipoCalc(FmFatDivGer1.QrFatPedCabRegrFiscal.Value,
                StqCenCad, Empresa);
  //
  if TipoCalc = -1 then
    Exit;
  //
  Casas    := Dmod.QrControleCasasProd.Value;
  PrecoO   := Geral.DMV(GradeL.Cells[Col, Row]);
  PrecoR   := Geral.DMV(GradeF.Cells[Col, Row]);
  QuantP   := Geral.DMV(GradeQ.Cells[Col, Row]);
  //ValCal   := PrecoR;// * QuantP;
  DescoP   := Geral.DMV(GradeD.Cells[Col, Row]);
  DescoI   := dmkPF.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
  PrecoF   := PrecoR - DescoI;
  DescoV   := DescoI * QuantP;
  ValBru   := PrecoR * QuantP;
  ValLiq   := ValBru - DescoV;
//
  Preco_PrecoF     := (*QrPreco*)PrecoF(*.Value*);
  Preco_PercCustom := 0;//(*QrPreco*)PercCustom(*.Value*);
  Preco_MedidaC    := 0;//(*QrPreco*)MedidaC(*.Value*);
  Preco_MedidaL    := 0;//(*QrPreco*)MedidaL(*.Value*);
  Preco_MedidaA    := 0;//(*QrPreco*)MedidaA(*.Value*);
  Preco_MedidaE    := 0;//(*QrPreco*)MedidaE(*.Value*);
  Preco_MedOrdem   := 0;//(*QrPreco*)MedOrdem(*.Value*);
//
  //
  SQLType          := ImgTipo.SQLType;
  PediVda          := FmFatDivGer1.QrFatPedCabPedido.Value;
  TabelaPrc        := FmFatDivGer1.QrFatPedCabTabelaPrc.Value;
  NO_tablaPrc      := FmFatDivGer1.QrFatPedCabNO_TabelaPrc.Value;
  OriCodi          := FmFatDivGer1.QrFatPedCabCodigo.Value;
  OriCnta          := FmFatDivGer1.QrFatPedVolCnta.Value;
  Associada        := FmFatDivGer1.QrFatPedCabAssociada.Value;
  //
  if (QrItem.State = dsInactive) or (QrItem.RecordCount = 0) then
  begin
    Geral.MB_Aviso('Reduzido n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  { Parei aqui!  ver
  if QrPediGruFracio.Value <> QrItemFracio.Value then
  begin
    Geral.MB_Aviso('Fracionamento n�o confere! AVISE A DERMATEK');
    Screen.Cursor := crDefault;
    Exit;
  end;
  }
  Qtde := QuantP;
  { Parei Aqui! Precisa?
  if PediVda > 0 then
  begin
    OriPart := DmPediVda.SaldoRedudidoPed(
      PediVda, GraGruX, Trunc(Qtde + 0.00001), Falta);
    if OriPart = 0 then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end else
      ReopenPrecoFat(OriPart);
      Preco_PrecoF     := QrPrecoPrecoF.Value;
      Preco_PercCustom := QrPrecoPercCustom.Value;
      Preco_MedidaC    := QrPrecoMedidaC.Value;
      Preco_MedidaL    := QrPrecoMedidaL.Value;
      Preco_MedidaA    := QrPrecoMedidaA.Value;
      Preco_MedidaE    := QrPrecoMedidaE.Value;
      Preco_MedOrdem   := QrPrecoMedOrdem.Value;
    if (Qtde <= 0) then
    begin
      Geral.MB_Aviso('Informe a quantidade!');
      Screen.Cursor := crDefault;
      Exit;
    end;
    if Qtde > Falta then
    begin
      Geral.MB_Aviso('A quantidade informada � al�m do necess�rio ' +
        'para este reduzido!' + sLineBreak + 'Necess�rio: ' + FloatToStr(Falta) +
        sLineBreak + 'Informado: ' + FloatToStr(Qtde) + sLineBreak +
        'Inclus�o de item abortada!');
      Screen.Cursor := crDefault;
      Exit;
    end;
    if QrPreco.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Pre�o n�o definido!' + sLineBreak + 'Produto: ' +
        FloatToStr(QrItemCU_Nivel1.Value) + ' - ' + QrItemNOMENIVEL1.Value + sLineBreak +
        'Tabela: ' + FloatToStr(TabelaPrc) + ' - ' +
        FmFatDivGer1.QrFatPedCabNO_TabelaPrc.Value);
      Screen.Cursor := crDefault;
      Exit;
    end else InfAdCuztm := QrPrecoInfAdCuztm.Value;
  end else begin
    OriPart := 0;
    InfAdCuztm := 0;
  end;
  }


  //if PnMultiGrandeza.Visible then
  if FMultiGrandeza then
  begin
    Pecas  := - Geral.DMV(Grade1.Cells[Col, Row]);
    Peso   := - Geral.DMV(Grade2.Cells[Col, Row]);
    AreaM2 := - Geral.DMV(Grade3.Cells[Col, Row]);
    AreaP2 := - Geral.DMV(Grade4.Cells[Col, Row]);
    //
    if not Grade_PF.ValidaGrandeza(TipoCalc, QrItemHowBxaEstq.Value, Pecas,
      AreaM2, AreaP2, Peso, Msg) then
    begin
      Geral.MB_Aviso(Msg);
      Exit;
    end;
  end else begin
    Pecas  := 0;
    AreaM2 := 0;
    AreaP2 := 0;
    Peso   := 0;
  end;
  //FOriCtrl := DmNFe.InsereItemStqMov(Tipo, OriCodi, OriCnta, Empresa, Cliente,
  IDCtrl := 0;
  //
  //
  prod_vFrete := 0.00;
  prod_vSeg   := 0.00;
  prod_vDesc  := 0.00;
  prod_vOutro := 0.00;
  //
  UsaSubsTrib := QrGraGru1UsaSubsTrib.Value;
  //
  if DmFatura.InsereItemStqMov(VAR_FATID_0001, OriCodi, OriCnta, Empresa, Cliente,
    Associada, RegrFiscal, GraGruX, NO_tablaPrc, FTmp_FatPedFis, StqCenCad,
    FatSemEstq, AFP_Sit, AFP_Per, Qtde, Cli_Tipo, Cli_IE, Cli_UF, EMP_UF,
    EMP_FILIAL, ASS_CO_UF, ASS_FILIAL, Item_MadeBy, Item_IPI_ALq, Preco_PrecoF,
    Preco_PercCustom, Preco_MedidaC, Preco_MedidaL, Preco_MedidaA, Preco_MedidaE,
    Preco_MedOrdem, SQLType, PediVda, OriPart, InfAdCuztm, (*TipoNF*)0,
    (*modNF*)0, (*Serie*)0, (*nNF*)0, (*SitDevolu*)0, (*Servico*)0, (*refNFe*)'',
    0, FOriCtrl, Pecas, AreaM2, AreaP2, Peso, TipoCalc, prod_indTot,
    prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro,
    IDCtrl) then
  begin
    if EdGraGru1.ValueVariant <> 0 then
      GraGru1 := QrGraGru1Nivel1.Value
    else
      GraGru1 := 0;
    //
    Grade_PF.InsereFatPedFis(FTmp_FatPedFis, GraGru1, IDCtrl, VAR_FATID_0001,
      OriCodi, FOriCtrl, OriCnta, OriPart, RegrFiscal);
(*
    EdLeitura.Text := '';
    EdQtdLei.ValueVariant := 1;
    if PnLeitura.Enabled then
      EdLeitura.SetFocus;
*)
  end;
end;

end.

