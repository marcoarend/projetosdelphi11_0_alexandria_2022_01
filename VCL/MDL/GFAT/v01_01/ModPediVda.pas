unit ModPediVda;

interface

uses
  Winapi.Windows, Vcl.Forms, System.SysUtils, System.Classes, Data.DB,
  Vcl.Dialogs, Vcl.ComCtrls, Vcl.Controls, Vcl.StdCtrls, Vcl.DBGrids, (*&ABSMain,*)
  mySQLDbTables, DmkDAC_PF, UnDmkEnums, UnDmkProcFunc, UnGrl_Geral,
  dmkEditCB, dmkDBLookupComboBox, UnProjGroup_Consts, ABSMain;

type
  TDmPediVda = class(TDataModule)
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENT: TWideStringField;
    QrClientesCIDADE: TWideStringField;
    QrClientesNOMEUF: TWideStringField;
    DsClientes: TDataSource;
    QrTabePrcCab: TmySQLQuery;
    DsTabePrcCab: TDataSource;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    DsPediPrzCab: TDataSource;
    DsFretePor: TDataSource;
    QrTabePrcCabCodigo: TIntegerField;
    QrTabePrcCabCodUsu: TIntegerField;
    QrTabePrcCabNome: TWideStringField;
    QrTabePrcCabSigla: TWideStringField;
    QrTabePrcCabDataI: TDateField;
    QrTabePrcCabDataF: TDateField;
    QrTabePrcCabDescoMax: TFloatField;
    QrTabePrcCabJurosMes: TFloatField;
    QrTabePrcCabPrazoMed: TFloatField;
    QrMotivos: TmySQLQuery;
    DsMotivos: TDataSource;
    QrMotivosCodigo: TIntegerField;
    QrMotivosCodUsu: TIntegerField;
    QrMotivosNome: TWideStringField;
    QrTransportas: TMySQLQuery;
    DsTransportas: TDataSource;
    QrTabePrcCabMoeda: TIntegerField;
    DsRedespachos: TDataSource;
    QrPediAcc: TmySQLQuery;
    DsPediAcc: TDataSource;
    QrCartEmis: TmySQLQuery;
    DsCartEmis: TDataSource;
    QrCartEmisCodigo: TIntegerField;
    QrCartEmisNome: TWideStringField;
    QrRedespachos: TmySQLQuery;
    QrRedespachosCodigo: TIntegerField;
    QrRedespachosNOMEENT: TWideStringField;
    QrRedespachosCIDADE: TWideStringField;
    QrRedespachosNOMEUF: TWideStringField;
    QrParamsEmp: TmySQLQuery;
    QrParamsEmpNOMEMOEDA: TWideStringField;
    QrParamsEmpNOMEFILIAL: TWideStringField;
    QrParamsEmpFilial: TIntegerField;
    QrParamsEmpEntidade: TIntegerField;
    QrParamsEmpCodigo: TIntegerField;
    QrParamsEmpMoeda: TIntegerField;
    QrParamsEmpSituacao: TIntegerField;
    QrParamsEmpRegrFiscal: TIntegerField;
    QrParamsEmpFretePor: TSmallintField;
    QrParamsEmpTipMediaDD: TSmallintField;
    QrParamsEmpFatSemPrcL: TSmallintField;
    QrItemPVI: TmySQLQuery;
    QrItemPVICodigo: TIntegerField;
    QrItemPVIControle: TIntegerField;
    QrItemPVIGraGruX: TIntegerField;
    QrItemPVIPrecoO: TFloatField;
    QrItemPVIPrecoR: TFloatField;
    QrItemPVIValBru: TFloatField;
    QrItemPVIDescoP: TFloatField;
    QrItemPVIValLiq: TFloatField;
    QrItemPVIDescoV: TFloatField;
    QrItemPVIQuantP: TFloatField;
    QrItemPVIQuantC: TFloatField;
    QrItemPVIQuantV: TFloatField;
    QrSumPed: TmySQLQuery;
    QrSumPedValLiq: TFloatField;
    QrSumPedQuantP: TFloatField;
    DsPVICliIts: TDataSource;
    QrLocNiv1: TmySQLQuery;
    QrLocNiv1GraGru1: TIntegerField;
    QrPVIPrdCad: TmySQLQuery;
    QrPVIPrdCadCodusu: TIntegerField;
    QrPVIPrdCadNivel1: TIntegerField;
    QrPVIPrdCadNome: TWideStringField;
    DsPVIPrdCad: TDataSource;
    QrPVIEmpCad: TmySQLQuery;
    QrPVIEmpCadCodigo: TIntegerField;
    QrPVIEmpCadFilial: TIntegerField;
    DsPVIEmpCad: TDataSource;
    QrPVIEmpCadNOMEENT: TWideStringField;
    QrPVICliCad: TmySQLQuery;
    QrPVICliCadCodUsu: TIntegerField;
    QrPVICliCadCodigo: TIntegerField;
    DsPVICliCad: TDataSource;
    QrPVICliCadNOMEENT: TWideStringField;
    QrPVIRepCad: TmySQLQuery;
    QrPVIRepCadCodUsu: TIntegerField;
    QrPVIRepCadCodigo: TIntegerField;
    DsPVIRepCad: TDataSource;
    QrPVIRepCadNOMEENT: TWideStringField;
    QrPVIAtrPrdCad: TmySQLQuery;
    QrPVIAtrPrdCadCodigo: TIntegerField;
    QrPVIAtrPrdCadCodUsu: TIntegerField;
    QrPVIAtrPrdCadNome: TWideStringField;
    DsPVIAtrPrdCad: TDataSource;
    QrPVIAtrCliCad: TmySQLQuery;
    QrPVIAtrCliCadCodigo: TIntegerField;
    QrPVIAtrCliCadCodUsu: TIntegerField;
    QrPVIAtrCliCadNome: TWideStringField;
    DsPVIAtrCliCad: TDataSource;
    QrAtrPrdSubIts: TmySQLQuery;
    QrAtrPrdSubItsCodigo: TIntegerField;
    QrAtrPrdSubItsCodUsu: TIntegerField;
    QrAtrPrdSubItsNome: TWideStringField;
    QrAtrCliSubIts: TmySQLQuery;
    QrAtrCliSubItsCodigo: TIntegerField;
    QrAtrCliSubItsCodUsu: TIntegerField;
    QrAtrCliSubItsNome: TWideStringField;
    QrPVIAtrRepCad: TmySQLQuery;
    DsPVIAtrRepCad: TDataSource;
    QrAtrRepSubIts: TmySQLQuery;
    QrPVIAtrRepCadCodigo: TIntegerField;
    QrPVIAtrRepCadCodUsu: TIntegerField;
    QrPVIAtrRepCadNome: TWideStringField;
    QrAtrRepSubItsCodigo: TIntegerField;
    QrAtrRepSubItsCodUsu: TIntegerField;
    QrAtrRepSubItsNome: TWideStringField;
    DsFRC_TMov: TDataSource;
    QrFisRegCad: TmySQLQuery;
    DsFisRegCad: TDataSource;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadModeloNF: TIntegerField;
    QrFisRegCadNO_MODELO_NF: TWideStringField;
    QrPrazo: TmySQLQuery;
    QrPrazoVariacao: TFloatField;
    DsPrazo: TDataSource;
    QrFisco: TmySQLQuery;
    QrFiscoICMS_Usa: TSmallintField;
    QrFiscoICMS_Alq: TFloatField;
    QrFiscoIPI_Usa: TSmallintField;
    QrFiscoIPI_Alq: TFloatField;
    QrFiscoPIS_Usa: TSmallintField;
    QrFiscoPIS_Alq: TFloatField;
    QrFiscoCOFINS_Usa: TSmallintField;
    QrFiscoCOFINS_Alq: TFloatField;
    QrFiscoIR_Usa: TSmallintField;
    QrFiscoIR_Alq: TFloatField;
    QrFiscoCS_Usa: TSmallintField;
    QrFiscoCS_Alq: TFloatField;
    QrFiscoISS_Usa: TSmallintField;
    QrFiscoISS_Alq: TFloatField;
    DsFisco: TDataSource;
    QrICMSV: TmySQLQuery;
    DsICMSV: TDataSource;
    QrICMSVICMS_V: TFloatField;
    QrICMSVNome: TWideStringField;
    QrICMSVUFDest: TWideStringField;
    QrSumVdaA: TMySQLQuery;
    QrEhItemPed: TmySQLQuery;
    QrEhItemPedControle: TIntegerField;
    QrSdoRedPed: TmySQLQuery;
    QrSdoRedPedQuantF: TFloatField;
    QrParamsEmpTipCalcJuro: TSmallintField;
    QrPvix: TmySQLQuery;
    QrClientesCodUsu: TIntegerField;
    QrSdoRedPedControle: TIntegerField;
    QrSdoRedPedMedidaC: TFloatField;
    QrSdoRedPedMedidaL: TFloatField;
    QrSdoRedPedMedidaA: TFloatField;
    QrSdoRedPedMedidaE: TFloatField;
    QrSdoRedPedMedida1: TWideStringField;
    QrSdoRedPedMedida2: TWideStringField;
    QrSdoRedPedMedida3: TWideStringField;
    QrSdoRedPedMedida4: TWideStringField;
    DsSdoRedPed: TDataSource;
    QrPvixControle: TIntegerField;
    QrSumVda_: TmySQLQuery;
    FloatField1: TFloatField;
    QrSumVdaAQtde: TFloatField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    QrPP: TmySQLQuery;
    QrPPPartePrinc: TIntegerField;
    QrSumCuz: TmySQLQuery;
    QrSumCuzValBru: TFloatField;
    QrSumCuzValLiq: TFloatField;
    QrSumCuzDescoV: TFloatField;
    QrIts: TmySQLQuery;
    QrItsQuantP: TFloatField;
    QrItsGraGru1: TIntegerField;
    QrCustomiz: TmySQLQuery;
    QrCustomizSiglaCustm: TWideStringField;
    QrPesInfCuz: TmySQLQuery;
    QrPesInfCuzCodigo: TIntegerField;
    QrSumCuzPercCustom: TFloatField;
    TbPVICliIts: TmySQLTable;
    TbPVICliItsCodigo: TIntegerField;
    TbPVICliItsCodUsu: TIntegerField;
    TbPVICliItsNome: TWideStringField;
    TbPVICliItsAtivo: TSmallintField;
    QrPVICliAti: TmySQLQuery;
    QrPVICliAtiItens: TLargeintField;
    QrFatPedCab: TmySQLQuery;
    QrFatPedCabCodigo: TIntegerField;
    QrFatPedCabEmpresa: TIntegerField;
    QrFatPedCabCliente: TIntegerField;
    QrFatPedCabTransporta: TIntegerField;
    QrFatPedCabFretePor: TSmallintField;
    QrFatPedCabModeloNF: TIntegerField;
    QrFatPedCabEMP_FILIAL: TIntegerField;
    QrFatPedCabAFP_Sit: TSmallintField;
    QrFatPedCabAFP_Per: TFloatField;
    QrFatPedCabCartEmis: TIntegerField;
    QrFatPedCabAssociada: TIntegerField;
    QrFatPedCabCondicaoPg: TIntegerField;
    QrFatPedCabAbertura: TDateTimeField;
    QrFatPedCabCodUsu: TIntegerField;
    QrFatPedCabTIPOCART: TIntegerField;
    QrFatPedCabRepresen: TIntegerField;
    QrFatPedCabEMP_CtaProdVen: TIntegerField;
    QrFatPedCabEMP_FaturaSep: TWideStringField;
    QrFatPedCabEMP_FaturaDta: TSmallintField;
    QrFatPedCabEMP_FaturaSeq: TSmallintField;
    QrFatPedCabEMP_TxtProdVen: TWideStringField;
    QrFatPedCabASS_CtaProdVen: TIntegerField;
    QrFatPedCabASS_FILIAL: TIntegerField;
    QrFatPedCabASS_FaturaDta: TSmallintField;
    QrFatPedCabASS_FaturaSeq: TSmallintField;
    QrFatPedCabASS_FaturaSep: TWideStringField;
    QrFatPedCabASS_TxtProdVen: TWideStringField;
    QrTransportasCodigo: TIntegerField;
    QrTransportasNOMEENT: TWideStringField;
    QrTransportasCIDADE: TWideStringField;
    QrTransportasNOMEUF: TWideStringField;
    QrFatPedCabEMP_DupProdVen: TWideStringField;
    QrFatPedCabASS_DupProdVen: TWideStringField;
    QrSumConCad: TmySQLQuery;
    QrFatConLot: TmySQLQuery;
    QrFatConLotCodigo: TIntegerField;
    QrSumConCadValLiq: TFloatField;
    QrSumConCadQuantP: TFloatField;
    QrSumConCadQuantC: TFloatField;
    QrSumConCadQuantV: TFloatField;
    QrSumConCadValFat: TFloatField;
    QrFatConRet: TmySQLQuery;
    QrFatConRetDescoGer: TFloatField;
    QrSumC: TmySQLQuery;
    QrSumCCredito: TFloatField;
    QrFatConRetComisPer: TFloatField;
    QrFatConRetValFat: TFloatField;
    QrItemPVIPrecoF: TFloatField;
    QrFatPedCabFinanceiro: TSmallintField;
    QrFatConRetPercRedu: TFloatField;
    QrFatPedCabtpNF: TLargeintField;
    QrFatPedCabEMP_FaturaNum: TSmallintField;
    QrFatPedCabASS_FaturaNum: TSmallintField;
    QrFisRegCadFinanceiro: TSmallintField;
    QrFretePor: TmySQLQuery;
    QrFRC_TMov: TmySQLQuery;
    QrCorrGrade: TmySQLQuery;
    QrCorrGradeCorrGrad01: TSmallintField;
    QrCorrGradeCorrGrad02: TSmallintField;
    QrNFeCtrl: TmySQLQuery;
    QrPediAccCodigo: TIntegerField;
    QrPediAccNOMEACC: TWideStringField;
    QrPediAccPerComissF: TFloatField;
    QrPediAccPerComissR: TFloatField;
    QrLoc: TmySQLQuery;
    QrParamsEmpUF_WebServ: TWideStringField;
    QrClientesIE: TWideStringField;
    QrParamsEmpversao: TFloatField;
    QrParamsEmpPediVdaNElertas: TIntegerField;
    QrSumVdaB: TmySQLQuery;
    QrSumVdaBQtde: TFloatField;
    QrFPC1: TmySQLQuery;
    QrFPC1Empresa: TIntegerField;
    QrFPC1Codigo: TIntegerField;
    QrFPC1CodUsu: TIntegerField;
    QrFPC1Pedido: TIntegerField;
    QrFPC1CondicaoPG: TIntegerField;
    QrFPC1TabelaPrc: TIntegerField;
    QrFPC1MedDDReal: TFloatField;
    QrFPC1MedDDSimpl: TFloatField;
    QrFPC1RegrFiscal: TIntegerField;
    QrFPC1NO_TabelaPrc: TWideStringField;
    QrFPC1TipMediaDD: TSmallintField;
    QrFPC1AFP_Sit: TSmallintField;
    QrFPC1AFP_Per: TFloatField;
    QrFPC1Associada: TIntegerField;
    QrFPC1CU_PediVda: TIntegerField;
    QrFPC1Abertura: TDateTimeField;
    QrFPC1Encerrou: TDateTimeField;
    QrFPC1FatSemEstq: TSmallintField;
    QrFPC1ENCERROU_TXT: TWideStringField;
    QrFPC1EMP_CtaProdVen: TIntegerField;
    QrFPC1EMP_FaturaSeq: TSmallintField;
    QrFPC1EMP_FaturaSep: TWideStringField;
    QrFPC1EMP_FaturaDta: TSmallintField;
    QrFPC1EMP_TxtProdVen: TWideStringField;
    QrFPC1EMP_FILIAL: TIntegerField;
    QrFPC1ASS_CtaProdVen: TIntegerField;
    QrFPC1ASS_FaturaSeq: TSmallintField;
    QrFPC1ASS_FaturaSep: TWideStringField;
    QrFPC1ASS_FaturaDta: TSmallintField;
    QrFPC1ASS_TxtProdVen: TWideStringField;
    QrFPC1ASS_NO_UF: TWideStringField;
    QrFPC1ASS_CO_UF: TIntegerField;
    QrFPC1ASS_FILIAL: TIntegerField;
    QrFPC1Cliente: TIntegerField;
    QrFPC1SerieDesfe: TIntegerField;
    QrFPC1NFDesfeita: TIntegerField;
    QrFPC1PEDIDO_TXT: TWideStringField;
    QrFPC1ID_Pedido: TIntegerField;
    QrFPC1JurosMes: TFloatField;
    QrFPC1NOMEFISREGCAD: TWideStringField;
    QrFPC1EMP_UF: TIntegerField;
    QrFPC1PedidoCli: TWideStringField;
    QrVolumes: TmySQLQuery;
    QrVolumesVolumes: TLargeintField;
    QrVolumesNO_UnidMed: TWideStringField;
    QrNFeCtrlVerNFeLay: TIntegerField;
    QrClientesindIEDest: TIntegerField;
    QrClientesTipo: TIntegerField;
    QrClientesDOCENT: TWideStringField;
    QrClientesObservacoes: TWideMemoField;
    QrRetiradaEnti: TmySQLQuery;
    DsRetiradaEnti: TDataSource;
    QrEntregaEnti: TmySQLQuery;
    DsEntregaEnti: TDataSource;
    QrEntregaEntiCodigo: TIntegerField;
    QrEntregaEntiCadastro: TDateField;
    QrEntregaEntiENatal: TDateField;
    QrEntregaEntiPNatal: TDateField;
    QrEntregaEntiTipo: TSmallintField;
    QrEntregaEntiRespons1: TWideStringField;
    QrEntregaEntiRespons2: TWideStringField;
    QrEntregaEntiENumero: TIntegerField;
    QrEntregaEntiPNumero: TIntegerField;
    QrEntregaEntiELograd: TSmallintField;
    QrEntregaEntiPLograd: TSmallintField;
    QrEntregaEntiECEP: TIntegerField;
    QrEntregaEntiPCEP: TIntegerField;
    QrEntregaEntiNOME_ENT: TWideStringField;
    QrEntregaEntiNO_2_ENT: TWideStringField;
    QrEntregaEntiCNPJ_CPF: TWideStringField;
    QrEntregaEntiIE_RG: TWideStringField;
    QrEntregaEntiNIRE_: TWideStringField;
    QrEntregaEntiRUA: TWideStringField;
    QrEntregaEntiSITE: TWideStringField;
    QrEntregaEntiCOMPL: TWideStringField;
    QrEntregaEntiBAIRRO: TWideStringField;
    QrEntregaEntiCIDADE: TWideStringField;
    QrEntregaEntiNOMELOGRAD: TWideStringField;
    QrEntregaEntiNOMEUF: TWideStringField;
    QrEntregaEntiPais: TWideStringField;
    QrEntregaEntiLograd: TSmallintField;
    QrEntregaEntiTE1: TWideStringField;
    QrEntregaEntiFAX: TWideStringField;
    QrEntregaEntiEMAIL: TWideStringField;
    QrEntregaEntiTRATO: TWideStringField;
    QrEntregaEntiENDEREF: TWideStringField;
    QrEntregaEntiCODMUNICI: TFloatField;
    QrEntregaEntiCODPAIS: TFloatField;
    QrEntregaEntiRG: TWideStringField;
    QrEntregaEntiSSP: TWideStringField;
    QrEntregaEntiDataRG: TDateField;
    QrRetiradaEntiCodigo: TIntegerField;
    QrRetiradaEntiCadastro: TDateField;
    QrRetiradaEntiENatal: TDateField;
    QrRetiradaEntiPNatal: TDateField;
    QrRetiradaEntiTipo: TSmallintField;
    QrRetiradaEntiRespons1: TWideStringField;
    QrRetiradaEntiRespons2: TWideStringField;
    QrRetiradaEntiENumero: TIntegerField;
    QrRetiradaEntiPNumero: TIntegerField;
    QrRetiradaEntiELograd: TSmallintField;
    QrRetiradaEntiPLograd: TSmallintField;
    QrRetiradaEntiECEP: TIntegerField;
    QrRetiradaEntiPCEP: TIntegerField;
    QrRetiradaEntiNOME_ENT: TWideStringField;
    QrRetiradaEntiNO_2_ENT: TWideStringField;
    QrRetiradaEntiCNPJ_CPF: TWideStringField;
    QrRetiradaEntiIE_RG: TWideStringField;
    QrRetiradaEntiNIRE_: TWideStringField;
    QrRetiradaEntiRUA: TWideStringField;
    QrRetiradaEntiSITE: TWideStringField;
    QrRetiradaEntiCOMPL: TWideStringField;
    QrRetiradaEntiBAIRRO: TWideStringField;
    QrRetiradaEntiCIDADE: TWideStringField;
    QrRetiradaEntiNOMELOGRAD: TWideStringField;
    QrRetiradaEntiNOMEUF: TWideStringField;
    QrRetiradaEntiPais: TWideStringField;
    QrRetiradaEntiLograd: TSmallintField;
    QrRetiradaEntiTE1: TWideStringField;
    QrRetiradaEntiFAX: TWideStringField;
    QrRetiradaEntiEMAIL: TWideStringField;
    QrRetiradaEntiTRATO: TWideStringField;
    QrRetiradaEntiENDEREF: TWideStringField;
    QrRetiradaEntiCODMUNICI: TFloatField;
    QrRetiradaEntiCODPAIS: TFloatField;
    QrRetiradaEntiRG: TWideStringField;
    QrRetiradaEntiSSP: TWideStringField;
    QrRetiradaEntiDataRG: TDateField;
    QrRetiradaEntiIE: TWideStringField;
    QrEntregaEntiIE: TWideStringField;
    QrEntregaEntiNUMERO: TFloatField;
    QrRetiradaEntiNUMERO: TFloatField;
    QrEntregaEntiCEP: TFloatField;
    QrRetiradaEntiCEP: TFloatField;
    QrInfIntermedEnti: TMySQLQuery;
    QrInfIntermedEntiCodigo: TIntegerField;
    QrInfIntermedEntiNOMEENT: TWideStringField;
    QrInfIntermedEntiCIDADE: TWideStringField;
    QrInfIntermedEntiNOMEUF: TWideStringField;
    DsInfIntermedEnti: TDataSource;
    QrFisRegCadTipoMov: TSmallintField;
    QrPVIPrdIts: TABSQuery;
    QrPVIPrdItsCodigo: TIntegerField;
    QrPVIPrdItsCodUsu: TIntegerField;
    QrPVIPrdItsNome: TWideStringField;
    QrPVIPrdItsAtivo: TSmallintField;
    DsPVIPrdIts: TDataSource;
    QrFatPedCabCtbCadMoF: TIntegerField;
    QrFatPedCabCtbPlaCta: TIntegerField;
    QrItemPVIMedidaC: TFloatField;
    QrItemPVIMedidaL: TFloatField;
    QrItemPVIMedidaA: TFloatField;
    QrItemPVIMedidaE: TFloatField;
    QrItemPVIPercCustom: TFloatField;
    QrItemPVICustomizad: TSmallintField;
    QrItemPVIInfAdCuztm: TIntegerField;
    QrItemPVIOrdem: TIntegerField;
    QrItemPVIReferencia: TWideStringField;
    QrItemPVIMulti: TIntegerField;
    QrItemPVILk: TIntegerField;
    QrItemPVIDataCad: TDateField;
    QrItemPVIDataAlt: TDateField;
    QrItemPVIUserCad: TIntegerField;
    QrItemPVIUserAlt: TIntegerField;
    QrItemPVIAlterWeb: TSmallintField;
    QrItemPVIAWServerID: TIntegerField;
    QrItemPVIAWStatSinc: TSmallintField;
    QrItemPVIAtivo: TSmallintField;
    QrItemPVIvProd: TFloatField;
    QrItemPVIvFrete: TFloatField;
    QrItemPVIvSeg: TFloatField;
    QrItemPVIvOutro: TFloatField;
    QrItemPVIvDesc: TFloatField;
    QrItemPVIvBC: TFloatField;
    QrSumVdaUC: TMySQLQuery;
    QrSumVdaUCQtde: TFloatField;
    QrAux: TMySQLQuery;
    QrPQFor: TMySQLQuery;
    QrPQForIQ: TIntegerField;
    QrPQForRelevancia: TSmallintField;
    QrItemSCI: TMySQLQuery;
    QrItemSCICodigo: TIntegerField;
    QrItemSCIControle: TIntegerField;
    QrItemSCIGraGruX: TIntegerField;
    QrItemSCIPrecoO: TFloatField;
    QrItemSCIPrecoR: TFloatField;
    QrItemSCIQuantP: TFloatField;
    QrItemSCIQuantC: TFloatField;
    QrItemSCIQuantV: TFloatField;
    QrItemSCIValBru: TFloatField;
    QrItemSCIDescoP: TFloatField;
    QrItemSCIDescoV: TFloatField;
    QrItemSCIValLiq: TFloatField;
    QrItemSCIPrecoF: TFloatField;
    QrItemSCIMedidaC: TFloatField;
    QrItemSCIMedidaL: TFloatField;
    QrItemSCIMedidaA: TFloatField;
    QrItemSCIMedidaE: TFloatField;
    QrItemSCIPercCustom: TFloatField;
    QrItemSCICustomizad: TSmallintField;
    QrItemSCIInfAdCuztm: TIntegerField;
    QrItemSCIOrdem: TIntegerField;
    QrItemSCIReferencia: TWideStringField;
    QrItemSCIMulti: TIntegerField;
    QrItemSCIvProd: TFloatField;
    QrItemSCIvFrete: TFloatField;
    QrItemSCIvSeg: TFloatField;
    QrItemSCIvOutro: TFloatField;
    QrItemSCIvDesc: TFloatField;
    QrItemSCIvBC: TFloatField;
    QrItemSCIQuantS: TFloatField;
    QrSCIx: TMySQLQuery;
    QrSCIxControle: TIntegerField;
    QrClientesNOME_E_DOCENT: TWideStringField;
    QrCartEmisTipo: TIntegerField;
    QrPediPrzCabCondPg: TSmallintField;
    QrClientesMadeBy: TSmallintField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
  protected  // Alexandria -> Tokyo
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo
    //
  public
    { Public declarations }
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
    //
    procedure ReopenSumC(FatID, FatNum: Integer);
    procedure ReopenClientes(Query: TmySQLQuery = nil; SaiEnt: Integer = -1);
    procedure ReopenTransportas();
    procedure ReopenInfIntermedEnti();
    procedure ReopenTabelasPedido(SaiEnt: Integer = -1);
    procedure ReopenCartEmis(Empresa: Integer);
    procedure ReopenParamsEmp(CodPesq: Integer;
              AvisaDaFaltaDeCadastro: Boolean);
    procedure AtzSdosSoliCompr(SoliComprCab: Integer);
    procedure AtzSdosPedido(Pedido: Integer);
    procedure AtzSdosCondicional_Saida(FatConCad: Integer);
    procedure AtzSdosCondicional_Devol(FatConRet: Integer);
    procedure AtzCondicional_Devol_Comis(FatConRet: Integer);
    procedure AtzCondicional_Devol_Pgtos(FatConRet, FatID: Integer);
    procedure AtualizaTodosItensPediVda_(PediVda: Integer; PB: TProgressBar = nil);
    procedure AtualizaUmItemPediVda(OriPart: Integer);
    procedure AtualizaTodosItensSoliCompr_(SoliCompr: Integer; PB: TProgressBar = nil);
    procedure AtualizaUmItemSoliCompr(SoliPart: Integer);
    procedure ExcluiItensFaturamento(FatPedCabCodigo: String;
              QueryFatPedIts: TmySQLQuery; DBGridFatPedIts: TDBGrid;
              PB: TProgressBar = nil);
    procedure ReopenTabePrcCab(Aplicacao: Integer; Data: String);
    //
    //function  EhItemDoPedido(PediVda, GraGruX: Integer): Boolean;
    function  ReopenICMCV(Nome, UFDest: String; TipoEntidade: Integer;
              IE: String): Boolean;
    {
    function  SaldoSuficiente(Empresa: Integer; QtdeSdo, QtdeLei: Double;
              GraGruX: Integer; NO_Prd, NO_Cor, NO_Tam: String;
              FatSemEstq, TipoCalc: Integer): Integer;
    }
    procedure ReopenFatPedCab(FatPedCab: Integer; SetaVars: Boolean; EhNFCe:
              Boolean = False; Quiet: Boolean = False; Financeiro: Integer = -1);
    procedure ReopenNFeCtrl();
    procedure ReopenFisRegCad(SaiEnt: Integer);
    procedure BaixaWebServicesNFe(Memo: TMemo);
    procedure BaixaLayoutNFe(Memo: TMemo);
    //procedure BaixaLayoutSPED_EFD(Memo: TMemo);
    function  ValidaRegraFiscalCFOPCliente(idDest, indFinal, dest_indIEDest,
              CodRegraFiscal: Integer; NomeRegraFiscal, IEDest, UFDest,
              UFEmiss: String; var Mensagem: String): Boolean;
    function  ValidaRegraFiscalCFOPProdutos(PrdGrupTip, CodRegraFiscal: Integer;
              NomeRegraFiscal: String; var Mensagem: String): Boolean;
    function  SaldoRedudidoPed(PediVda, GraGruX, QtdBaixar: Integer; var Qtde:
              Double): Integer;
    procedure MostraFormEntidade2(Entidade: Integer; Query: TmySQLQuery;
              EditCliente: TdmkEditCB; ComboCliente: TdmkDBLookupComboBox);
    procedure InsereFornecedoresEmSoliComprFor(SoliComprCab, Nivel1: Integer);
    //NFe multi CFOPs
  end;

var
  DmPediVda: TDmPediVda;

implementation

uses
{$IfNDef NAO_GPED} FatPedCuzSel, {$EndIf}
{$IfNDef SemNFe_0000} NFe_PF, {$EndIf}
{$IfNDef SemNFCe_0000} NFCe_PF, {$EndIf}
  UnMyObjects, Module, ModuleGeral, MyDBCheck, dmkGeral, UnInternalConsts,
  UMySQLModule, Restaura2, UnGrade_Tabs, UnDmkWeb, MyListas, QuaisItens,
  UMySQLDB;

{$R *.dfm}

function TDmPediVda.ReopenICMCV(Nome, UFDest: String; TipoEntidade: Integer;
IE: String): Boolean;
var
  Nom2: String;
begin
  Result := False;
  QrICMSV.Close;
  if (Trim(Nome) = '' ) or (Trim(UFDest) = '') then
  begin
    Geral.MB_Aviso('N�o � possivel obter o percentual de ICMS da UF "'
      + Nome + '" (empresa) para a UF "' + UFDest +'" (Cliente) !');
    Exit;
  end;
  if (TipoEntidade = 0) and (Uppercase(IE) <> 'ISENTO') then
    Nom2 := UFDest
  else
    Nom2 := Nome;
  QrICMSV.Params[00].AsString := Nome;
  QrICMSV.Params[01].AsString := Nom2;
  UnDmkDAC_PF.AbreQuery(QrICMSV, Dmod.MyDB);
  Result := QrICMSV.RecordCount > 0;
  if not Result then
    Geral.MB_Aviso('Percentual de ICMS n�o definido!' + sLineBreak +
    'UF origem: ' + Nome + sLineBreak + 'UF destino: ' + UFDest + sLineBreak +
    ' Busca usada: "' + Nome + '" > "' + Nom2 + '"' + sLineBreak);

  //
end;

procedure TDmPediVda.ReopenInfIntermedEnti();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrInfIntermedEnti, Dmod.MyDB, [
  'SELECT ent.Codigo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, ',
  'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE, ',
  'ufs.Nome NOMEUF ',
  'FROM entidades ent ',
  'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF) ',
  'WHERE ent.Fornece1="V" OR ent.Fornece2="V"  ',
  'OR ent.Fornece3="V" OR ent.Fornece4="V" ',
  'OR ent.Fornece5="V" OR ent.Fornece6="V" ',
  'OR ent.Fornece7="V" OR ent.Fornece8="V" ',
  'AND ent.Ativo=1 ',
  'ORDER BY NOMEENT ',
  ' ']);
end;

procedure TDmPediVda.ReopenNFeCtrl;
begin
  QrNFeCtrl.Close;
  UnDmkDAC_PF.AbreQuery(QrNFeCtrl, Dmod.MyDB);
end;

procedure TDmPediVda.ReopenParamsEmp(CodPesq: Integer;
AvisaDaFaltaDeCadastro: Boolean);
var
  Filial: Integer;
begin
  QrParamsEmp.Close;
  QrParamsEmp.SQL.Clear;
  QrParamsEmp.SQL.Add('SELECT CONCAT(mda.Sigla," - ",mda.Nome) NOMEMOEDA,');
  QrParamsEmp.SQL.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEFILIAL,');
  QrParamsEmp.SQL.Add('ent.Filial, ent.Codigo Entidade, pem.*');
  QrParamsEmp.SQL.Add('FROM entidades ent');
  QrParamsEmp.SQL.Add('LEFT JOIN paramsemp pem ON pem.Codigo=ent.Codigo');
  QrParamsEmp.SQL.Add('LEFT JOIN cambiomda mda ON mda.Codigo=pem.Moeda');
  if CodPesq < -10 then
  begin
    QrParamsEmp.SQL.Add('WHERE ent.Codigo = :P0');
    QrParamsEmp.Params[0].AsInteger := CodPesq;
  end else begin
    QrParamsEmp.SQL.Add('WHERE ent.Codigo<-10');
    QrParamsEmp.SQL.Add('AND ent.Filial=:P0');
    QrParamsEmp.Params[0].AsInteger := CodPesq;
  end;
  UnDmkDAC_PF.AbreQuery(QrParamsEmp, Dmod.MyDB);
  if CodPesq < 0 then
    Filial := QrParamsEmpFilial.Value
  else
    Filial := CodPesq;
  //
  if (QrParamsEmpCodigo.Value = 0) and AvisaDaFaltaDeCadastro then
    Geral.MB_Aviso('Os par�metros da filial ' + IntToStr(Filial) +
    ' ainda n�o foram definidos!' + sLineBreak + 'Avise o ADMINISTRADOR!');
  //
  DModG.ReopenParamsEmp(CodPesq);
end;

procedure TDmPediVda.ReopenSumC(FatID, FatNum: Integer);
begin
  QrSumC.Close;
  QrSumC.Params[00].AsInteger := FatID;
  QrSumC.Params[01].AsInteger := FatNum;
  UnDmkDAC_PF.AbreQuery(QrSumC, Dmod.MyDB);
end;

procedure TDmPediVda.ReopenClientes(Query: TmySQLQuery = nil; SaiEnt: Integer = -1);
var
  Qry: TmySQLQuery;
begin
  if Query <> nil then
    Qry := Query
  else
    Qry := QrClientes;
  //
  if SaiEnt = 0 then // Entrada
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ent.Codigo, ent.CodUsu, ent.IE, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, ',
    'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCENT, ',
    'CONCAT(',
    '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome),',
    ' " - ", ',
    '  IF(ent.Tipo=0, ent.CNPJ, ent.CPF)',
    ') NOME_E_DOCENT, ',
    'mun.Nome CIDADE, ent.Observacoes,  ',
    'ufs.Nome NOMEUF, ent.indIEDest, ent.Tipo, ent.MadeBy  ',
    'FROM entidades ent ',
    'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF) ',
    'LEFT JOIN locbdermall.dtb_munici mun ON mun.Codigo = IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) ',
    'WHERE ent.Fornece1="V" ',
    'OR ent.Fornece2="V" ',
    'AND ent.Ativo=1 ',
    'ORDER BY NOMEENT ',
    '']);
  end else
  begin
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT ent.Codigo, ent.CodUsu, ent.IE,');
    Qry.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
    Qry.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCENT,');
    Qry.SQL.Add(   'CONCAT(');
    Qry.SQL.Add(   '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome),');
    Qry.SQL.Add(   '  " - ", ');
    Qry.SQL.Add(   '  IF(ent.Tipo=0, ent.CNPJ, ent.CPF)');
    Qry.SQL.Add(   ') NOME_E_DOCENT, ');
    Qry.SQL.Add('mun.Nome CIDADE, ent.Observacoes, ');
    Qry.SQL.Add('ufs.Nome NOMEUF, ent.indIEDest, ent.Tipo, ent.MadeBy ');
    Qry.SQL.Add('');
    Qry.SQL.Add('FROM entidades ent');
    Qry.SQL.Add('LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)');
    Qry.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici)');
    //
    if (CO_DMKID_APP = 6)  //Planning
    or (CO_DMKID_APP = 24) //Bugstrol
    or (CO_DMKID_APP = 28) then //Toolrent
      Qry.SQL.Add('WHERE ent.Cliente1="V"')
    else if CO_DMKID_APP = 2 then //Bluederm
    begin
      Qry.SQL.Add('WHERE ent.Cliente1="V"');
      Qry.SQL.Add('OR ent.Cliente2="V"');
    end else if CO_DMKID_APP = 10 then //Safecar
    begin
      Qry.SQL.Add('WHERE ent.Cliente1="V"');
      Qry.SQL.Add('OR ent.Cliente2="V"');
    end else
    begin
      Geral.MensagemBox('Tipos de clientes n�o definidos no ' +
        'aplicativo "' + Application.Title +
        '" Ser�o usados os 4 tipos (1, 2, 3, 4). AVISE A DERMATEK', 'Aviso',
        MB_OK+MB_ICONWARNING);
      Qry.SQL.Add('WHERE ent.Cliente1="V"');
      Qry.SQL.Add('OR ent.Cliente2="V"');
      Qry.SQL.Add('OR ent.Cliente3="V"');
      Qry.SQL.Add('OR ent.Cliente4="V"');
    end;
    Qry.SQL.Add('AND ent.Ativo=1');
    Qry.SQL.Add('ORDER BY NOMEENT');
    //
    //Adicionar a transportadora padr�o e o prazo padrao
    //dmkPF.LeMeuTexto(Qry.SQL.Text);
    //
    UnDmkDAC_PF.AbreQuery(Qry, Dmod.MyDB);
    //Geral.MB_Teste(Qry.SQL.Text);
  end;
end;

procedure TDmPediVda.ReopenFatPedCab(FatPedCab: Integer; SetaVars: Boolean;
  EhNFCe: Boolean = False; Quiet: Boolean = False; Financeiro: Integer = -1);
begin
  QrFatPedCab.Close;
  QrFatPedCab.Params[0].AsInteger := FatPedCab;
  UnDmkDAC_PF.AbreQuery(QrFatPedCab, Dmod.MyDB);
  //
  if SetaVars then
  begin
    if EhNFCe then
    {$IfNDef SemNFCe_0000}
      UnNFCe_PF.DefineVarsFormNFCeEmiss_XXXX()
    {$Else}
      Geral.MB_Info('NFC-e n�o habilitado para este ERP')
    {$EndIf}
    else
    begin
      if Quiet then
        UnNFe_PF.DefineVarsFormNFaEdit_Quiet(Financeiro)
      else
        UnNFe_PF.DefineVarsFormNFaEdit_XXXX();
    end;
(*&
    // Configura��o do tipo
    FmNFaEdit.F_Tipo            := VAR_FATID_0001;
    FmNFaEdit.F_OriCodi         := QrFatPedCabCodigo        .Value;
    // Integer
    FmNFaEdit.F_Empresa         := QrFatPedCabEmpresa       .Value;
    FmNFaEdit.F_ModeloNF        := QrFatPedCabModeloNF      .Value;
    FmNFaEdit.F_Cliente         := QrFatPedCabCliente       .Value;
    FmNFaEdit.F_EMP_FILIAL      := QrFatPedCabEMP_FILIAL    .Value;
    FmNFaEdit.F_AFP_Sit         := QrFatPedCabAFP_Sit       .Value;
    FmNFaEdit.F_Associada       := QrFatPedCabAssociada     .Value;
    FmNFaEdit.F_ASS_FILIAL      := QrFatPedCabASS_FILIAL    .Value;
    FmNFaEdit.F_EMP_CtaFaturas  := QrFatPedCabEMP_CtaProdVen.Value;
    FmNFaEdit.F_ASS_CtaFaturas  := QrFatPedCabASS_CtaProdVen.Value;
    FmNFaEdit.F_CartEmis        := QrFatPedCabCartEmis      .Value;
    FmNFaEdit.F_CondicaoPG      := QrFatPedCabCondicaoPG    .Value;
    FmNFaEdit.F_Financeiro      := QrfatPedCabFinanceiro    .Value; // define na regra fiscal
    FmNFaEdit.F_EMP_FaturaDta   := QrFatPedCabEMP_FaturaDta .Value;
    FmNFaEdit.F_EMP_IDDuplicata := QrFatPedCabCodUsu        .Value;
    FmNFaEdit.F_EMP_FaturaSeq   := QrFatPedCabEMP_FaturaSeq .Value;
    FmNFaEdit.F_EMP_FaturaNum   := QrFatPedCabEMP_FaturaNum .Value;
    FmNFaEdit.F_TIPOCART        := QrFatPedCabTIPOCART      .Value;
    FmNFaEdit.F_Represen        := QrFatPedCabRepresen      .Value;
    FmNFaEdit.F_ASS_IDDuplicata := QrFatPedCabCodUsu        .Value;
    FmNFaEdit.F_ASS_FaturaSeq   := QrFatPedCabASS_FaturaSeq .Value;
    FmNFaEdit.F_ASS_FaturaNum   := QrFatPedCabASS_FaturaNum .Value;
    // String
    FmNFaEdit.F_EMP_FaturaSep   := QrFatPedCabEMP_FaturaSep .Value;
    FmNFaEdit.F_EMP_TxtFaturas  := QrFatPedCabEMP_TxtProdVen.Value;
    FmNFaEdit.F_EMP_TpDuplicata := QrFatPedCabEMP_DupProdVen.Value;
    FmNFaEdit.F_ASS_FaturaSep   := QrFatPedCabASS_FaturaSep .Value;
    FmNFaEdit.F_ASS_TxtFaturas  := QrFatPedCabASS_TxtProdVen.Value;
    FmNFaEdit.F_ASS_TpDuplicata := QrFatPedCabASS_DupProdVen.Value;
    // Double
    FmNFaEdit.F_AFP_Per         := QrFatPedCabAFP_Per       .Value;
    // TDateTime
    FmNFaEdit.F_Abertura        := QrFatPedCabAbertura      .Value;
&*)
  end;
end;

procedure TDmPediVda.ReopenFisRegCad(SaiEnt: Integer);
var
  SQL_TipoMov: String;
begin
(*
  if SaiEnt = 0 then
    SQL_TipoMov := 'WHERE frc.TipoMov=0'
  else
    SQL_TipoMov := 'WHERE frc.TipoMov=1';
*)
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFisRegCad, Dmod.MyDB, [
  'SELECT frc.Codigo, frc.CodUsu, frc.ModeloNF, ',
  'frc.Nome, frc.Financeiro, imp.Nome NO_MODELO_NF, ',
  'frc.TipoMov ',
  'FROM fisregcad frc ',
  'LEFT JOIN imprime imp ON imp.Codigo=frc.ModeloNF ',
  SQL_TipoMov,
  'ORDER BY Nome ',
  '']);
end;

procedure TDmPediVda.ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

procedure TDmPediVda.ReopenCartEmis(Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartEmis, Dmod.MyDB, [
    'SELECT car.Codigo, car.Nome, car.Tipo ',
    'FROM carteiras car ',
    //'WHERE Tipo=2 ',
    //'AND Ativo=1 ',
    'WHERE Ativo=1',
    'AND ForneceI=' + Geral.FF0(Empresa),
    'ORDER BY Nome ',
    '']);
end;

procedure TDmPediVda.ReopenTabelasPedido(SaiEnt: Integer = -1);
begin
  ReopenClientes(nil, SaiEnt);
  //
  ReopenTransportas();
  ReopenInfIntermedEnti();
  //
  QrRedespachos.Close;
  UnDmkDAC_PF.AbreQuery(QrRedespachos, Dmod.MyDB);
  //
  QrPediPrzCab.Close;
  QrPediPrzCab.Params[00].AsInteger := 1;
  UnDmkDAC_PF.AbreQuery(QrPediPrzCab, Dmod.MyDB);
  //
{$IfNDef SemCotacoes}
  DmodG.QrCambioMda.Close;
  UnDmkDAC_PF.AbreQuery(DmodG.QrCambioMda, Dmod.MyDB);
{$EndIf}
  //
  ReopenTabePrcCab(1, FormatDateTime('YYYY-MM-DD', Date));
  //
  QrMotivos.Close;
  QrMotivos.Params[00].AsInteger := 2; // Pedido
  UnDmkDAC_PF.AbreQuery(QrMotivos, Dmod.MyDB);
  //
  QrPediAcc.Close;
  UnDmkDAC_PF.AbreQuery(QrPediAcc, Dmod.MyDB);
  //
  ReopenFisRegCad(SaiEnt);
  //
  DModG.ReopenEndereco3(QrEntregaEnti);
  DModG.ReopenEndereco3(QrRetiradaEnti);
end;

procedure TDmPediVda.ReopenTabePrcCab(Aplicacao: Integer; Data: String);
begin
  QrTabePrcCab.Close;
  QrTabePrcCab.Params[00].AsInteger := Aplicacao;
  // o mysql n�o aceita (neste caso) data como yyyy/mm/yy deve ser yyyy-mm-dd
  QrTabePrcCab.Params[01].AsString  := Data;
  UnDmkDAC_PF.AbreQuery(QrTabePrcCab, Dmod.MyDB);
end;

procedure TDmPediVda.ReopenTransportas();
begin
  QrTransportas.Close;
  QrTransportas.SQL.Clear;
  QrTransportas.SQL.Add('SELECT ent.Codigo,');
  QrTransportas.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
  QrTransportas.SQL.Add('IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,');
  QrTransportas.SQL.Add('ufs.Nome NOMEUF');
  QrTransportas.SQL.Add('FROM entidades ent');
  QrTransportas.SQL.Add('LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)');
  if VAR_FORNECET <> '' then
    QrTransportas.SQL.Add('WHERE ent.Fornece' + VAR_FORNECET + '="V"')
  else
  begin
    QrTransportas.SQL.Add('WHERE ent.Fornece1="V" OR ent.Fornece2="V" ');
    QrTransportas.SQL.Add('OR ent.Fornece3="V" OR ent.Fornece4="V"');
    QrTransportas.SQL.Add('OR ent.Fornece5="V" OR ent.Fornece6="V"');
    QrTransportas.SQL.Add('OR ent.Fornece7="V" OR ent.Fornece8="V"');
  end;
  QrTransportas.SQL.Add('AND ent.Ativo=1');
  QrTransportas.SQL.Add('ORDER BY NOMEENT');
  QrTransportas.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrTransportas, Dmod.MyDB);
end;

function TDmPediVda.SaldoRedudidoPed(PediVda, GraGruX, QtdBaixar: Integer; var Qtde:
Double): Integer;
begin
  Result := 0;
  QrSdoRedPed.Close;
  QrSdoRedPed.Params[00].AsInteger := PediVda;
  QrSdoRedPed.Params[01].AsInteger := GraGruX;
  //QrSdoRedPed.Params[02].AsInteger := QtdBaixar;
  UnDmkDAC_PF.AbreQuery(QrSdoRedPed, Dmod.MyDB);
  //
  Qtde := QrSdoRedPedQuantF.Value;
  case QrSdoRedPed.RecordCount of
    0: Geral.MB_Aviso('O reduzido ' + Geral.FF0(GraGruX) +
       ' n�o pertence a este pedido, ou n�o h� quantidade pendente suficiente!'
       +sLineBreak + 'Caso sejam customizados, informe apenas a quantidade do tamanho desejado!');
    1: Result := QrSdoRedPedControle.Value;
    else
    begin
      Geral.MB_Aviso('O reduzido ' + IntToStr(GraGruX) +
      ' possui mais de um lan�amento de itens neste pedido!' + sLineBreak +
      'Caso sejamm customizados, selecione a forma pr�pria de faturamento!');
      //
{$IfNDef NAO_GPED}
      if DBCheck.CriaFm(TFmFatPedCuzSel, FmFatPedCuzSel, afmoNegarComAviso) then
      begin
        FmFatPedCuzSel.ShowModal;
        Result := FmFatPedCuzSel.FControle;
        Qtde := QrSdoRedPedQuantF.Value;
        FmFatPedCuzSel.Destroy;
      end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGPed);
{$EndIf}
    end;
  end;
end;

function TDmPediVda.ValidaRegraFiscalCFOPCliente(idDest, indFinal, dest_indIEDest,
  CodRegraFiscal: Integer; NomeRegraFiscal, IEDest, UFDest, UFEmiss: String;
  var Mensagem: String): Boolean;

  function ValidaDados(Regra, Contribui, Interno: Integer): Boolean;
  begin
    Result := False;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT * ',
      'FROM fisregcfop ',
      'WHERE Codigo = ' + Geral.FF0(Regra),
      'AND Contribui = ' + Geral.FF0(Contribui),
      'AND Interno = ' + Geral.FF0(Interno),
      '']);
    if QrLoc.RecordCount > 0 then
      Result := True;
  end;

  function ValidaDadosGrupoTributacaoICMS(): Boolean;
  begin
    Result := True;
    //
    // Operacao interestadual
    // Consumidor final
    // Operacao com nao contribuinte
    if (idDest = 2) and (indFinal = 1) and (dest_indIEDest = 2) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
        'SELECT * ',
        'FROM Fisregufs ',
        'WHERE Codigo=' + Geral.FF0(CodRegraFiscal),
        'AND ((UsaInterPartLei = 0 OR pICMSInterPart = 0) ',
        'OR (pICMSInter = 0)) ',
        '']);
      if QrLoc.RecordCount > 0 then
        Result := False;
    end;
  end;

var
  Interno, Contribui: Integer;
  Interno_Txt, Contribui_Txt: String;
begin
  if QrParamsEmpPediVdaNElertas.Value = 0 then
  begin
    //True  = Existe regra para os valores informados
    //False = N�o existe regra para os valorer informados
    //
    if (IEDest <> '') and (UpperCase(IEDest) <> 'ISENTO') then
    begin
      Contribui     := 1;
      Contribui_Txt := 'MARCAR';
    end else
    begin
      Contribui     := 0;
      Contribui_Txt := 'DESMARCAR';
    end;
    if Trim(UFEmiss) = Trim(UFDest) then
    begin
      Interno     := 1;
      Interno_Txt := 'MARCAR';
    end else
    begin
      Interno     := 0;
      Interno_Txt := 'DESMARCAR';
    end;
    //
    Result := ValidaDados(CodRegraFiscal, Contribui, Interno);
    //
    if not Result then
      Mensagem := 'A regra fiscal ' + NomeRegraFiscal +
        ' n�o possui nenhum CFOP configurado que corresponda �s seguintes especifica��es:'
        + sLineBreak + sLineBreak + 'Campo = Contribuinte UF (I.E.) => Preenchimento = '
        + Contribui_Txt + sLineBreak + 'Campo = Venda/compra mesma UF => Preenchimento = '
        + Interno_Txt
    else
      Mensagem := '';
  end else
  begin
    Mensagem := '';
    Result   := True;
  end;

  if Result = True then
  begin
    Mensagem := '';
    //
    Result := ValidaDadosGrupoTributacaoICMS();
    //
    if Result = False then
    begin
      Mensagem := 'Os dados do grupo de tributa��o do ICMS para UF de destino n�o foram definidos!' +
        sLineBreak + sLineBreak + 'Observa��o:' + sLineBreak + 'Voc� dever� configurar estes dados para cada CFOP da regra.' +
        sLineBreak + 'Para isso no cadastro da regra fiscal clique no bot�o "Emit/Dest"';
    end;
  end;
end;

function TDmPediVda.ValidaRegraFiscalCFOPProdutos(PrdGrupTip,
  CodRegraFiscal: Integer; NomeRegraFiscal: String; var Mensagem: String): Boolean;

  function ObtemTipoFabricacao(PrdGrupTip: Integer): Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT MadeBy ',
    'FROM prdgruptip ',
    'WHERE Codigo=' + Geral.FF0(PrdGrupTip),
    '']);
    if QrLoc.RecordCount > 0 then
      Result := QrLoc.FieldByName('MadeBy').AsInteger
    else
      Result := 0;
  end;

  function ValidaDados(Regra, Proprio: Integer): Boolean;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT * ',
    'FROM fisregcfop ',
    'WHERE Codigo = ' + Geral.FF0(Regra),
    'AND Proprio = ' + Geral.FF0(Proprio),
    '']);
    if QrLoc.RecordCount > 0 then
      Result := True
    else
      Result := False;
  end;

var
  MadeBy, Proprio: Integer;
  Proprio_Txt: String;
begin
  if QrParamsEmpPediVdaNElertas.Value = 0 then
  begin
    //True  = Existe regra para os valores informados
    //False = N�o existe regra para os valorer informados
    //
    MadeBy := ObtemTipoFabricacao(PrdGrupTip);
    //
    if MadeBy = 1 then
    begin
      Proprio     := 1;
      Proprio_Txt := 'MARCAR';
    end else
    begin
      Proprio     := 0;
      Proprio_Txt := 'DESMARCAR';
    end;
    //
    Result := ValidaDados(CodRegraFiscal, Proprio);
    //
    if not Result then
      Mensagem := 'A regra fiscal ' + NomeRegraFiscal +
        ' n�o possui nenhum CFOP configurado que corresponda �s seguintes especifica��es:'
        + sLineBreak + sLineBreak + 'Campo = [PFP] Fabrica��o pr�pria => Preenchimento = '
        + Proprio_Txt
    else
      Mensagem := '';
  end else
  begin
    Mensagem := '';
    Result   := True;
  end;
end;

procedure TDmPediVda.WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

procedure TDmPediVda.AtualizaTodosItensPediVda_(PediVda: Integer;
  PB: TProgressBar = nil);
begin
  Screen.Cursor := crHourGlass;
  try
    QrPvix.Close;
    QrPvix.Params[0].AsInteger := PediVda;
    UnDmkDAC_PF.AbreQuery(QrPvix, Dmod.MyDB);
    //
    if QrPvix.RecordCount > 0 then
    begin
      if PB <> nil then
      begin
        PB.Position := 0;
        PB.Max      := QrPvix.RecordCount;
      end;
      //
      while not QrPvix.Eof do
      begin
        AtualizaUmItemPediVda(QrPvixControle.Value);
        //
        if PB <> nil then
        begin
          PB.Position := PB.Position + 1;
          PB.Update;
          Application.ProcessMessages;
        end;
        //
        QrPvix.Next;
      end;
      // Nada a ver
      //AtzSdosPedido(PediVda);
    end;
  finally
    Screen.Cursor := crDefault;
    //
    if PB <> nil then
      PB.Position := 0;
  end;
end;

procedure TDmPediVda.AtualizaTodosItensSoliCompr_(SoliCompr: Integer;
  PB: TProgressBar);
begin
  Screen.Cursor := crHourGlass;
  try
    QrSCIx.Close;
    QrSCIx.Params[0].AsInteger := SoliCompr;
    UnDmkDAC_PF.AbreQuery(QrSCIx, Dmod.MyDB);
    //
    if QrSCIx.RecordCount > 0 then
    begin
      if PB <> nil then
      begin
        PB.Position := 0;
        PB.Max      := QrSCIx.RecordCount;
      end;
      //
      while not QrSCIx.Eof do
      begin
        AtualizaUmItemSoliCompr(QrSCIxControle.Value);
        //
        if PB <> nil then
        begin
          PB.Position := PB.Position + 1;
          PB.Update;
          Application.ProcessMessages;
        end;
        //
        QrSCIx.Next;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
    //
    if PB <> nil then
      PB.Position := 0;
  end;
end;

procedure TDmPediVda.AtualizaUmItemPediVda(OriPart: Integer);
var
  Qtde, QtdeA, QtdeB, UCQtde: Double;
begin
  if OriPart = 0 then
    Exit;
  //
  QrSumVdaA.Close;
  QrSumVdaA.Params[00].AsInteger := OriPart;
  UnDmkDAC_PF.AbreQuery(QrSumVdaA, Dmod.MyDB);
  //
  QtdeA := QrSumVdaAQtde.Value;
  //
  QrSumVdaB.Close;
  QrSumVdaB.Params[00].AsInteger := OriPart;
  UnDmkDAC_PF.AbreQuery(QrSumVdaB, Dmod.MyDB);
  //
  QtdeB := QrSumVdaBQtde.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumVdaUC, Dmod.MyDB, [
  'SELECT SUM(Volumes * PesoVL) Qtde ',
  'FROM pqeits ',
  'WHERE OriPart=' + Geral.FF0(OriPart),
  '']);
  //
  UCQtde := QrSumVdaUCQtde.Value;
  Qtde := QtdeA + QtdeB + UCQtde;
  //
  if Qtde < 0 then
    Qtde := Qtde * -1;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE pedivdaits SET QuantV=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa');
  Dmod.QrUpd.Params[00].AsFloat   := Qtde;
  //Dmod.QrUpd.Params[00].AsFloat   := -QrSumVdaQtde.Value;
  Dmod.QrUpd.Params[01].AsInteger := OriPart;
  Dmod.QrUpd.ExecSQL;
  // Nada a ver
  //if AtzTotaisPedido then
    //AtzSdosPedido(PediVda);
end;

procedure TDmPediVda.AtualizaUmItemSoliCompr(SoliPart: Integer);
var
  QuantP: Double;
begin
  if SoliPart = 0 then
    Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT SUM(smi.QuantP) QuantP ',
  'FROM pedivdaits smi ',
  'WHERE smi.SoliPart=' + Geral.FF0(SoliPart),
  '']);
  //
  QuantP := QrAux.FieldByName('QuantP').AsFloat;
  if QuantP < 0 then
    QuantP := QuantP * -1;
  //
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'solicomprits', False, [
  'QuantP'], [
  'Controle'], [
  QuantP], [
  SoliPart], True);
end;

procedure TDmPediVda.AtzSdosPedido(Pedido: Integer);
begin
  QrSumPed.Close;
  QrSumPed.Params[0].AsInteger := Pedido;
  UnDmkDAC_PF.AbreQuery(QrSumPed, Dmod.MyDB);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE pedivda SET QuantP=:P0, ValLiq=:P1 ');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  Dmod.QrUpd.Params[00].AsFloat   := QrSumPedQuantP.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPedValLiq.Value;
  Dmod.QrUpd.Params[02].AsInteger := Pedido;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TDmPediVda.AtzSdosSoliCompr(SoliComprCab: Integer);
var
  ValLiq, QuantS, QuantP, QuantC: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT SUM(ValLiq) ValLiq, SUM(QuantS) QuantS, ',
  'SUM(QuantP) QuantP, SUM(QuantC) QuantC ',
  'FROM solicomprits pvi ',
  'WHERE pvi.Codigo=' + Geral.FF0(SoliComprCab),
  '']);
  ValLiq := QrAux.FieldByName('ValLiq').AsFloat;
  QuantS := QrAux.FieldByName('QuantS').AsFloat;
  QuantP := QrAux.FieldByName('QuantP').AsFloat;
  QuantC := QrAux.FieldByName('QuantC').AsFloat;
  //
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'solicomprcab', False, [
  'ValLiq', 'QuantS', 'QuantP', 'QuantC'], [
  'Codigo'], [
  ValLiq, QuantS, QuantP, QuantC], [
  SoliComprCab], True);
  //
end;

procedure TDmPediVda.BaixaWebServicesNFe(Memo: TMemo);
var
  Res: Boolean;
  ArqNome: String;
  Versao: Int64;
begin
  ReopenNFeCtrl;
  Res := DmkWeb.VerificaAtualizacaoVersao2(True, True, CO_WEBID_NFEWS,
           'Lista de Web Services NF-e', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value),
           0, 0, DModG.ObtemAgora(), Memo,
           dtSQLs, Versao, ArqNome, False);
  if (Res) and (VAR_DOWNLOAD_OK) then
  begin
    //C:\Projetos\Delphi 2007\Aplicativos\Auxiliares\DermaBK\Restaura.pas
    Application.CreateForm(TFmRestaura2, FmRestaura2);
    FmRestaura2.Show;
    FmRestaura2.EdBackFile.Text := CO_DIR_RAIZ_DMK + '\' + ArqNome + '.SQL';
    //FmRestaura2.BeRestore.DatabaseName := DMod.MyDB.DatabaseName;
    FmRestaura2.FDB := DModG.AllID_DB;
    FmRestaura2.BtDiretoClick(Self);
    FmRestaura2.Close;
    //
    ReopenNFeCtrl;
  end;
end;

procedure TDmPediVda.BaixaLayoutNFe(Memo: TMemo);
var
  Res: Boolean;
  ArqNome: String;
  Versao: Int64;
begin
  ReopenNFeCtrl;
  Res := DmkWeb.VerificaAtualizacaoVersao2(True, True, CO_WEBID_NFELAY,
           'Layout NFe', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value),
           DmPediVda.QrNFeCtrlVerNFeLay.Value, 0, DModG.ObtemAgora(), Memo,
           dtSQLs, Versao, ArqNome, False);
  if (Res) and (VAR_DOWNLOAD_OK) then
  begin
    //C:\Projetos\Delphi 2007\Aplicativos\Auxiliares\DermaBK\Restaura.pas
    Application.CreateForm(TFmRestaura2, FmRestaura2);
    FmRestaura2.Show;
    FmRestaura2.EdBackFile.Text := CO_DIR_RAIZ_DMK + '\' + ArqNome + '.SQL';
    //FmRestaura2.BeRestore.DatabaseName := DMod.MyDB.DatabaseName;
    FmRestaura2.FDB := DMod.MyDB;
    FmRestaura2.BtDiretoClick(Self);
    FmRestaura2.Close;
    //
    //Atualiza vers�o no banco de dados
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE nfectrl SET VerNFeLay=:P0');
    Dmod.QrUpd.Params[00].AsInteger := Versao;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenNFeCtrl;
  end;
end;

{
procedure TDmPediVda.BaixaLayoutSPED_EFD(Memo: TMemo);
var
  Res: Boolean;
  ArqNome: String;
  Versao: Integer;
begin
  DmPediVda.ReopenNFeCtrl;
  Res := DmkWeb.VerificaAtualizacaoVersao2(True, True, CO_WEBID_SPEDEFD,
           'Layout SPED EFD', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value),
           0(*DmPediVda.QrNFeCtrlVerSPEDEFDLay.Value*), 0, DModG.ObtemAgora(),
           Memo, dtSQLs, Versao, ArqNome, False);
  if (Res) and (VAR_DOWNLOAD_OK) then
  begin
    //C:\Projetos\Delphi 2007\Aplicativos\Auxiliares\DermaBK\Restaura.pas
    Application.CreateForm(TFmRestaura2, FmRestaura2);
    FmRestaura2.Show;
    FmRestaura2.EdBackFile.Text := 'C:\Dermatek\' + ArqNome + '.SQL';
    //FmRestaura2.BeRestore.DatabaseName := DMod.MyDB.DatabaseName;
    //FmRestaura2.FDB := DMod.MyDB;
    FmRestaura2.FDB := DModG.AllID_DB;
    FmRestaura2.BtDiretoClick(Self);
    FmRestaura2.Close;
  end;
end;
}

procedure TDmPediVda.ExcluiItensFaturamento(FatPedCabCodigo: String;
  QueryFatPedIts: TmySQLQuery; DBGridFatPedIts: TDBGrid; PB: TProgressBar = nil);

  procedure ExcluiRegistroFatPedFis();
  var
    IDCtrl, Tipo, OriCodi, OriCtrl, OriCnta, OriPart: Integer;
  begin
    IDCtrl  := QueryFatPedIts.FieldByName('IDCtrl').AsInteger;
    Tipo    := QueryFatPedIts.FieldByName('Tipo').AsInteger;
    OriCodi := QueryFatPedIts.FieldByName('OriCodi').AsInteger;
    OriCtrl := QueryFatPedIts.FieldByName('OriCtrl').AsInteger;
    OriCnta := QueryFatPedIts.FieldByName('OriCnta').AsInteger;
    OriPart := QueryFatPedIts.FieldByName('OriPart').AsInteger;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM fatpedfis ',
      'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
      '']);
    //
    Application.ProcessMessages;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM stqmovvala ',
      'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
      'AND Tipo=' + Geral.FF0(Tipo),
      'AND OriCodi=' + Geral.FF0(OriCodi),
      'AND OriCtrl=' + Geral.FF0(OriCtrl),
      'AND OriCnta=' + Geral.FF0(OriCnta),
      '']);
    //
    Application.ProcessMessages;
  end;

var
  i: Integer;
  q: TSelType;
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM stqmovvala');
  Dmod.QrUpd.SQL.Add('WHERE Tipo=1');
  Dmod.QrUpd.SQL.Add('AND OriCodi=' + FatPedCabCodigo);
  Dmod.QrUpd.SQL.Add('AND NOT (OriCtrl IN');
  Dmod.QrUpd.SQL.Add('  (');
  Dmod.QrUpd.SQL.Add('  SELECT DISTINCT OriCtrl');
  Dmod.QrUpd.SQL.Add('  FROM stqmovitsa');
  Dmod.QrUpd.SQL.Add('  WHERE Tipo=1');
  Dmod.QrUpd.SQL.Add('  AND OriCodi=' + FatPedCabCodigo);
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('  UNION');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('  SELECT DISTINCT OriCtrl');
  Dmod.QrUpd.SQL.Add('  FROM stqmovitsb');
  Dmod.QrUpd.SQL.Add('  WHERE Tipo=1');
  Dmod.QrUpd.SQL.Add('  AND OriCodi=' + FatPedCabCodigo);
  Dmod.QrUpd.SQL.Add('  )');
  Dmod.QrUpd.SQL.Add(') ');
  //
  if MyObjects.CriaForm_AcessoTotal(TFmQuaisItens, FmQuaisItens) then
  begin
    with FmQuaisItens do
    begin
      ShowModal;
      if not FSelecionou then
      begin
        Geral.MB_Aviso('Exclus�o cancelada pelo usu�rio!');
        q := istDesiste;
      end else
        q := FEscolha;
      Destroy;
    end;
  end;
  //
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QueryFatPedIts, DBGridFatPedIts,
    'stqmovitsa', ['IDCtrl'], ['IDCtrl'], q, Dmod.QrUpd.SQL.Text, True, False);
  //
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QueryFatPedIts, DBGridFatPedIts,
    'stqmovitsb', ['IDCtrl'], ['IDCtrl'], q, '', True, False);
  //
  if q = istNenhum then Exit;
  //
  if (q = istSelecionados) and (DBGridFatPedIts.SelectedRows.Count < 2) then
    q := istAtual;
  //
  case q of
    istAtual:
    begin
      ExcluiRegistroFatPedFis();
    end;
    istSelecionados:
    begin
      if PB <> nil then
      begin
        PB.Position := 0;
        PB.Max      := DBGridFatPedIts.SelectedRows.Count;
      end;
      Screen.Cursor := crHourGlass;
      try
        with DBGridFatPedIts.DataSource.DataSet do
        begin
          for i := 0 to DBGridFatPedIts.SelectedRows.Count - 1 do
          begin
            //GotoBookmark(pointer(DBGridFatPedIts.SelectedRows.Items[i]));
            GotoBookmark(DBGridFatPedIts.SelectedRows.Items[i]);
            ExcluiRegistroFatPedFis();
            //
            if PB <> nil then
            begin
              PB.Position := PB.Position + 1;
              PB.Update;
            end;
            Application.ProcessMessages;
          end;
        end;
      finally
        if PB <> nil then
          PB.Position := 0;
        //
        Screen.Cursor := crDefault;
      end;
    end;
    istTodos:
    begin
      if PB <> nil then
      begin
        PB.Position := 0;
        PB.Max      := QueryFatPedIts.RecordCount;
      end;
      Screen.Cursor := crHourGlass;
      try
        QueryFatPedIts.First;
        while not QueryFatPedIts.Eof do
        begin
          ExcluiRegistroFatPedFis();
          //
          if PB <> nil then
          begin
            PB.Position := PB.Position + 1;
            PB.Update;
          end;
          Application.ProcessMessages;
          //
          QueryFatPedIts.Next;
        end;
      finally
        if PB <> nil then
          PB.Position := 0;
        //
        Screen.Cursor := crDefault;
      end;
    end;
  end;
  UnDmkDAC_PF.AbreQueryApenas(QueryFatPedIts);
end;

procedure TDmPediVda.InsereFornecedoresEmSoliComprFor(SoliComprCab,
  Nivel1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPQFor, Dmod.MyDB, [
  'SELECT IQ, Relevancia  ',
  'FROM pqfor ',
  'WHERE PQ=' + Geral.FF0(Nivel1),
  '']);
  QrPQFor.First;
  while not QrPQFor.Eof do
  begin
    UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, stIns, 'solicomprfor', False, [
    'Relevancia'], [
    'Codigo', 'Fornece'], [
    QrPQForRelevancia.Value], [
    SoliComprCab, QrPQForIQ.Value], True);
    //
    QrPQFor.Next;
  end;
end;

procedure TDmPediVda.MostraFormEntidade2(Entidade: Integer; Query: TmySQLQuery;
  EditCliente: TdmkEditCB; ComboCliente: TdmkDBLookupComboBox);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
    //
    if Query.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EditCliente.ValueVariant := VAR_ENTIDADE;
      ComboCliente.KeyValue    := VAR_ENTIDADE;
      EditCliente.SetFocus;
    end;
  end;
end;

procedure TDmPediVda.AtzSdosCondicional_Saida(FatConCad: Integer);
var
  QuantP, QuantC, QuantV, ValLiq, ValFat: Double;
begin
  QrSumConCad.Close;
  QrSumConCad.Params[0].AsInteger := FatConCad;
  UnDmkDAC_PF.AbreQuery(QrSumConCad, Dmod.MyDB);
  //
  QuantP := QrSumConCadQuantP.Value;
  QuantC := QrSumConCadQuantC.Value;
  QuantV := QrSumConCadQuantV.Value;
  ValLiq := QrSumConCadValLiq.Value;
  ValFat := QrSumConCadValFat.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'fatconcad', False, [
    'QuantP', 'QuantC', 'QuantV', 'ValLiq', 'ValFat'
  ], ['Codigo'], [
    QuantP, QuantC, QuantV, ValLiq, ValFat
  ], [FatConCad], True) then
  begin
    // N�o Fechar QrSumConCad!! Usa no AtzSdosCondicional_Devol
  end;
end;

procedure TDmPediVda.AtzCondicional_Devol_Comis(FatConRet: Integer);
var
  BaseVal, ComisVal: Double;
begin
  QrFatConRet.Close;
  QrFatConRet.Params[0].AsInteger := FatConRet;
  UnDmkDAC_PF.AbreQuery(QrFatConRet, Dmod.MyDB);
  //
  BaseVal  := QrFatConRetValFat.Value - QrFatConRetDescoGer.Value;
  BaseVal  := BaseVal * (1 - (QrFatConRetPercRedu.Value / 100));
  ComisVal := Round(BaseVal * QrFatConRetComisPer.Value) / 100;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'fatconret', False, [
    'ComisVal'
  ], ['Codigo'], [
    ComisVal
  ], [FatConRet], True) then
  begin
    // Nada
  end;
end;

procedure TDmPediVda.AtzCondicional_Devol_Pgtos(FatConRet, FatID: Integer);
var
  ValorRec: Double;
begin
  ReopenSumC(FatID, FatConRet);
  //
  ValorRec := QrSumCCredito.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'fatconret', False, [
    'ValorRec'
  ], ['Codigo'], [
    ValorRec
  ], [FatConRet], True) then
  begin
    // Nada
  end;
end;

procedure TDmPediVda.AtzSdosCondicional_Devol(FatConRet: Integer);
var
  QuantP, QuantC, QuantV, ValLiq, ValFat: Double;
begin
  QuantP := 0;
  QuantC := 0;
  QuantV := 0;
  ValLiq := 0;
  ValFat := 0;
  //
  QrFatConLot.Close;
  QrFatConLot.Params[0].AsInteger := FatConRet;
  UnDmkDAC_PF.AbreQuery(QrFatConLot, Dmod.MyDB);
  //
  QrFatConLot.First;
  while not QrFatConLot.Eof do
  begin
    AtzSdosCondicional_Saida(QrFatConLotCodigo.Value);
    //
    QuantP := QuantP + QrSumConCadQuantP.Value;
    QuantC := QuantC + QrSumConCadQuantC.Value;
    QuantV := QuantV + QrSumConCadQuantV.Value;
    ValLiq := ValLiq + QrSumConCadValLiq.Value;
    ValFat := ValFat + QrSumConCadValFat.Value;
    //
    QrFatConLot.Next;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'fatconret', False, [
    'QuantP', 'QuantC', 'QuantV', 'ValLiq', 'ValFat'
  ], ['Codigo'], [
    QuantP, QuantC, QuantV, ValLiq, ValFat
  ], [FatConRet], True) then
  begin
    QrSumConCad.Close;
  end;
end;

procedure TDmPediVda.DataModuleCreate(Sender: TObject);
{
var
  Campos: String;
}
begin
  if Dmod.MyDB.Connected = True then
  begin
    /////////////////// F R E T E   P O R ////////////////////////////////////////
    QrFretePor.Close;
    {
    QrFretePor.SQL.Add('DROP TABLE fretepor; ');
    QrFretePor.SQL.Add('CREATE TABLE fretepor (');
    QrFretePor.SQL.Add('  Codigo integer     ,');
    QrFretePor.SQL.Add('  Nome   varchar(30) ');
    QrFretePor.SQL.Add(');');
    //
    Campos := 'INSERT INTO fretepor (Codigo,Nome) VALUES (';
    QrFretePor.SQL.Add(Campos + '1,"Emitente");');
    QrFretePor.SQL.Add(Campos + '2,"Destinat�rio/remetente");');
    QrFretePor.SQL.Add(Campos + '3,"Terceiros");');
    QrFretePor.SQL.Add(Campos + '10,"SemFrete");');
    //
    QrFretePor.SQL.Add('SELECT * FROM fretepor ORDER BY Nome;');
    }
    UnDmkDAC_PF.AbreQuery(QrFretePor, Dmod.MyDB);
    //

    /////////////////// TIPO MOVIMENTO REGRA FISCAL //////////////////////////////
    QrFRC_TMov.Close;
    {
    QrFRC_TMov.SQL.Add('DROP TABLE FRC_TMov; ');
    QrFRC_TMov.SQL.Add('CREATE TABLE FRC_TMov (');
    QrFRC_TMov.SQL.Add('  Codigo integer     ,');
    QrFRC_TMov.SQL.Add('  Nome   varchar(30) ');
    QrFRC_TMov.SQL.Add(');');
    //
    Campos := 'INSERT INTO frc_tmov (Codigo,Nome) VALUES (';
    QrFRC_TMov.SQL.Add(Campos + '1,"Entrada");');
    QrFRC_TMov.SQL.Add(Campos + '5,"Sa�da");');
    //
    QrFRC_TMov.SQL.Add('SELECT * FROM frc_tmov ORDER BY Nome;');
    }
    UnDmkDAC_PF.AbreQuery(QrFRC_TMov, Dmod.MyDB);
    //
    //
    //////////////////////// CORRE��ES ///////////////////////////////////////////
    ///
    //try
      QrCorrGrade.Close;
      UnDmkDAC_PF.AbreQuery(QrCorrGrade, Dmod.MyDB);
      if QrCorrGradeCorrGrad01.Value = 0 then
      begin
        if USQLDB.TabelaExiste('pedivda', Dmod.MyDB) then
        begin
          DMod.QrUpd.SQL.Clear;
          DMod.QrUpd.SQL.Add('UPDATE pedivda SET FretePor=FretePor-1');
          DMod.QrUpd.ExecSQL;
        end;
        try
          if USQLDB.TabelaExiste('fatvencab', Dmod.MyDB) then
          begin
            DMod.QrUpd.SQL.Clear;
            DMod.QrUpd.SQL.Add('UPDATE fatvencab SET FretePor=FretePor-1');
            DMod.QrUpd.ExecSQL;
          end;
        except
          ;
        end;
        //
        try
          if USQLDB.TabelaExiste('cmppvai', Dmod.MyDB) then
          begin
            DMod.QrUpd.SQL.Clear;
            DMod.QrUpd.SQL.Add('UPDATE cmppvai SET FretePor=FretePor-1');
            DMod.QrUpd.ExecSQL;
          end;

          if USQLDB.TabelaExiste('cmptout', Dmod.MyDB) then
          begin
            DMod.QrUpd.SQL.Clear;
            DMod.QrUpd.SQL.Add('UPDATE cmptout SET FretePor=FretePor-1');
            DMod.QrUpd.ExecSQL;
          end;

          if USQLDB.TabelaExiste('cectout', Dmod.MyDB) then
          begin
            DMod.QrUpd.SQL.Clear;
            DMod.QrUpd.SQL.Add('UPDATE cectout SET FretePor=FretePor-1');
            DMod.QrUpd.ExecSQL;
          end;

          if USQLDB.TabelaExiste('nfempinn', Dmod.MyDB) then
          begin
            DMod.QrUpd.SQL.Clear;
            DMod.QrUpd.SQL.Add('UPDATE nfempinn SET FretePor=FretePor-1');
            DMod.QrUpd.ExecSQL;
          end;
        except
          ;
        end;
        //  ATUALIZAR TABELA CONTROLE
        //
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('UPDATE controle SET CorrGrad01=1');
        DMod.QrUpd.ExecSQL;
        //
      end;

      if QrCorrGradeCorrGrad01.Value = 0 then
      begin
        if USQLDB.TabelaExiste('fisregcad', Dmod.MyDB) then
        begin
          DMod.QrAux.Close;
          DMod.QrAux.SQL.Clear;
          DMod.QrAux.SQL.Add('SELECT * FROM fisregcad WHERE TipoMov=5');
          UnDmkDAC_PF.AbreQuery(DMod.QrAux, Dmod.MyDB);
          if DMod.QrAux.RecordCount > 0 then
          begin
            DMod.QrUpd.SQL.Clear;
            DMod.QrUpd.SQL.Add('UPDATE fisregcad SET TipoMov=0 WHERE TipoMov=1');
            DMod.QrUpd.ExecSQL;

            DMod.QrUpd.SQL.Clear;
            DMod.QrUpd.SQL.Add('UPDATE FisRegCad SET TipoMov=1 WHERE TipoMov=5');
            DMod.QrUpd.ExecSQL;

            DMod.QrUpd.SQL.Clear;
            DMod.QrUpd.SQL.Add('UPDATE controle SET CorrGrad02=1');
            DMod.QrUpd.ExecSQL;
          end;
        end;
      end;
  {
    except

     //Halt(0);
    end;
  }
  end;
end;

procedure TDmPediVda.DefineProperties(Filer: TFiler);  // Alexandria -> Tokyo
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

{
function TDmPediVda.EhItemDoPedido(PediVda, GraGruX: Integer): Boolean;
begin
  QrEhItemPed.Close;
  QrEhItemPed.Params[00].AsInteger := PediVda;
  QrEhItemPed.Params[01].AsInteger := GraGruX;
  UnDmkDAC_PF.AbreQuery(QrEhItemPed, Dmod.MyDB);
  //
  Result := QrEhItemPedControle.Value > 0;
  //
  if not result then
    Geral.MB_Aviso('O reduzido ' + IntToStr(GraGruX) +
    ' n�o pertence a este pedido!');
end;
}




{
(*&&
object QrPVIPrdAti: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  SQL.Strings = (
    'SELECT Count(*)  Itens'
    'FROM pviprd'
    'WHERE Ativo=1'
    '')
  Left = 620
  Top = 24
  object QrPVIPrdAtiItens: TIntegerField
    FieldName = 'Itens'
  end
end
object QrPVIEmpIts: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  Left = 476
  Top = 120
  object QrPVIEmpItsCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrPVIEmpItsCodUsu: TIntegerField
    FieldName = 'CodUsu'
    DisplayFormat = '00000000'
  end
  object QrPVIEmpItsNome: TWideStringField
    FieldName = 'Nome'
    Size = 30
  end
  object QrPVIEmpItsAtivo: TSmallintField
    FieldName = 'Ativo'
    MaxValue = 1
  end
end
object DsPVIEmpIts: TDataSource
  Left = 552
  Top = 120
end
object QrPVIEmpAti: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  SQL.Strings = (
    'SELECT Count(*)  Itens'
    'FROM pviemp'
    'WHERE Ativo=1'
    '')
  Left = 620
  Top = 120
  object QrPVIEmpAtiItens: TIntegerField
    FieldName = 'Itens'
  end
end
object QrPVIRepIts: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  Left = 476
  Top = 168
  object QrPVIRepItsCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrPVIRepItsCodUsu: TIntegerField
    FieldName = 'CodUsu'
    DisplayFormat = '00000000'
  end
  object QrPVIRepItsNome: TWideStringField
    FieldName = 'Nome'
    Size = 30
  end
  object QrPVIRepItsAtivo: TSmallintField
    FieldName = 'Ativo'
    MaxValue = 1
  end
end
object DsPVIRepIts: TDataSource
  Left = 552
  Top = 168
end
object QrPVIRepAti: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  SQL.Strings = (
    'SELECT Count(*)  Itens'
    'FROM pvirep'
    'WHERE Ativo=1'
    '')
  Left = 620
  Top = 168
  object QrPVIRepAtiItens: TIntegerField
    FieldName = 'Itens'
  end
end
object QrPVIAtrPrdIts: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  Left = 476
  Top = 216
  object QrPVIAtrPrdItsCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrPVIAtrPrdItsCodUsu: TIntegerField
    FieldName = 'CodUsu'
    DisplayFormat = '00000000'
  end
  object QrPVIAtrPrdItsNome: TWideStringField
    FieldName = 'Nome'
    Size = 30
  end
  object QrPVIAtrPrdItsAtivo: TSmallintField
    FieldName = 'Ativo'
    MaxValue = 1
  end
end
object DsPVIAtrPrdIts: TDataSource
  Left = 548
  Top = 216
end
object QrPVIAtrPrdAti: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  SQL.Strings = (
    'SELECT Count(*)  Itens'
    'FROM pviatrprd'
    'WHERE Ativo=1'
    '')
  Left = 620
  Top = 216
  object QrPVIAtrPrdAtiItens: TIntegerField
    FieldName = 'Itens'
  end
end
object QrPVIAtrCliIts: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  Left = 476
  Top = 264
  object QrPVIAtrCliItsCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrPVIAtrCliItsCodUsu: TIntegerField
    FieldName = 'CodUsu'
    DisplayFormat = '00000000'
  end
  object QrPVIAtrCliItsNome: TWideStringField
    FieldName = 'Nome'
    Size = 30
  end
  object QrPVIAtrCliItsAtivo: TSmallintField
    FieldName = 'Ativo'
    MaxValue = 1
  end
end
object DsPVIAtrCliIts: TDataSource
  Left = 552
  Top = 264
end
object QrPVIAtrCliAti: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  SQL.Strings = (
    'SELECT Count(*)  Itens'
    'FROM pviatrcli'
    'WHERE Ativo=1'
    '')
  Left = 620
  Top = 264
  object QrPVIAtrCliAtiItens: TIntegerField
    FieldName = 'Itens'
  end
end
object QrPVIAtrRepIts: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  Left = 476
  Top = 312
  object QrPVIAtrRepItsCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object QrPVIAtrRepItsCodUsu: TIntegerField
    FieldName = 'CodUsu'
    DisplayFormat = '00000000'
  end
  object QrPVIAtrRepItsNome: TWideStringField
    FieldName = 'Nome'
    Size = 30
  end
  object QrPVIAtrRepItsAtivo: TSmallintField
    FieldName = 'Ativo'
    MaxValue = 1
  end
end
object DsPVIAtrRepIts: TDataSource
  Left = 552
  Top = 312
end
object QrPVIAtrRepAti: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  SQL.Strings = (
    'SELECT Count(*)  Itens'
    'FROM pviatrcli'
    'WHERE Ativo=1'
    '')
  Left = 620
  Top = 312
  object IntegerField6: TIntegerField
    FieldName = 'Itens'
  end
end
*)
}
end.


