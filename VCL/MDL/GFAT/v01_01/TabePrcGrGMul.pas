unit TabePrcGrGMul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB, DBGrids, dmkDBGrid, DBCtrls,
  mySQLDbTables, UnDmkProcFunc, dmkGeral, dmkImage, UnDmkEnums, DmkDAC_PF;
  (*&&UnDmkABS_PF, ABSMain;*)

type
  THackDBGrid = class(TDBGrid);
  TFmTabePrcGrGMul = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    BtPesquisa: TBitBtn;
    QrPesq: TmySQLQuery;
    Label11: TLabel;
    QrPesqNivel1: TIntegerField;
    QrPesqCodUsu: TIntegerField;
    QrPesqNome: TWideStringField;
    DataSource1: TDataSource;
    DBGrid1: TdmkDBGrid;
    QrPesqPreco: TFloatField;
    StaticText1: TStaticText;
    RGTipPrd: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure DBGrid1ColExit(Sender: TObject);
    procedure RGTipPrdClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCadastrar: Boolean;
  end;

  var
  FmTabePrcGrGMul: TFmTabePrcGrGMul;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab;

{$R *.DFM}

procedure TFmTabePrcGrGMul.BtOKClick(Sender: TObject);
var
  Codigo, Nivel1: Integer;
begin
(*&&
  Screen.Cursor := crHourGlass;
  //
  FCadastrar := True;
  Query.Filter   := 'Ativo=1';
  Query.Filtered := True;
  //
  if Query.RecordCount > 0 then
  begin
    Codigo := FmTabePrcCab.QrTabePrcCabCodigo.Value;
    Query.First;
    while not Query.Eof do
    begin
      UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'tabeprcgrg', False, ['Preco'],
        ['Codigo', 'Nivel1'], ['Preco'], [QueryPreco.Value],
        [Codigo, QueryNivel1.Value], [QueryPreco.Value], True);
      Query.Next;
    end;
    //
    Nivel1 := FmTabePrcCab.QrTabePrcGrGNivel1.Value;
    FmTabePrcCab.ReopenTabePrcGrG(Nivel1);
    //
    Close;
  end else begin
    Query.Filtered := False;
    Application.MessageBox('Nenhum item foi selecionado!', 'Avido',
    MB_OK+MB_ICONWARNING);
  end;
  Screen.Cursor := crDefault;
*)
end;

procedure TFmTabePrcGrGMul.BtPesquisaClick(Sender: TObject);
var
  Lin: String;
begin
(*&&
  Screen.Cursor := crHourGlass;
  QrPesq.Close;
  QrPesq.Params[00].AsInteger := FmTabePrcCab.QrTabePrcCabCodigo.Value;
  QrPesq.Params[01].AsInteger := RGTipPrd.ItemIndex;
  QrPesq.Params[02].AsString  := '%' + EdPesq.Text + '%';
  UnDmkDAC_PF.AbreQuery(QrPesq, Dmod.MyDB);

  //

  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DELETE FROM prcgrgmul;');
  Lin := 'INSERT INTO prcgrgmul (Nivel1,CodUsu,Nome,Preco,Ativo) Values (';
  QrPesq.First;
  while not QrPesq.Eof do
  begin
    Query.SQL.Add(Lin + //'(' +
      dmkPF.FFP(QrPesqNivel1.Value, 0) + ',' +
      dmkPF.FFP(QrPesqCodUsu.Value, 0) + ',' +
      '"' + QrPesqNome.Value + '",' +
      dmkPF.FFP(QrPesqPreco.Value , 6) + ',' +
    '1);');
    //
    QrPesq.Next;
  end;
  //
  Query.SQL.Add('SELECT * FROM prcgrgmul ORDER BY CodUsu');
  DmkABS_PF.AbreQuery(Query);

  //

  Screen.Cursor := crDefault;
*)
end;

procedure TFmTabePrcGrGMul.BtSaidaClick(Sender: TObject);
begin
  FCadastrar := False;
  Close;
end;

procedure TFmTabePrcGrGMul.DBGrid1CellClick(Column: TColumn);
var
  Status: Byte;
begin
(*&&
  if Column.FieldName = 'Ativo' then
  begin
    if Query.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    Query.Edit;
    Query.FieldByName('Ativo').Value := Status;
    Query.Post;
  end;
*)
end;

procedure TFmTabePrcGrGMul.DBGrid1ColEnter(Sender: TObject);
begin
  if DBGrid1.Columns[THackDBGrid(DBGrid1).Col -1].FieldName = 'Preco' then
    DBGrid1.Options := DBGrid1.Options + [dgEditing] else
    DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

procedure TFmTabePrcGrGMul.DBGrid1ColExit(Sender: TObject);
begin
  DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

procedure TFmTabePrcGrGMul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTabePrcGrGMul.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
(*&&
  FCadastrar := False;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE PrcGrGMul; ');
  Query.SQL.Add('CREATE TABLE PrcGrGMul (');
  Query.SQL.Add('  Nivel1  integer      ,');
  Query.SQL.Add('  CodUsu  integer      ,');
  Query.SQL.Add('  Nome    varchar(30)  ,');
  Query.SQL.Add('  Preco   float        ,');
  Query.SQL.Add('  Ativo   smallint      ');
  Query.SQL.Add(');');
  //
  Query.SQL.Add('SELECT * FROM prcgrgmul');
  DmkABS_PF.AbreQuery(Query);
  QueryPreco.DisplayFormat := dmkPF.FormataCasas(
    DMod.QrControleCasasProd.Value);
*)
end;

procedure TFmTabePrcGrGMul.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTabePrcGrGMul.RGTipPrdClick(Sender: TObject);
begin
(*&&
  Query.Close;
*)
end;

(*&&
object Query: TABSQuery
  CurrentVersion = '7.91 '
  DatabaseName = 'MEMORY'
  InMemory = True
  ReadOnly = False
  RequestLive = True
  Left = 12
  Top = 12
  object QueryNivel1: TIntegerField
    FieldName = 'Nivel1'
  end
  object QueryCodUsu: TIntegerField
    FieldName = 'CodUsu'
  end
  object QueryPreco: TFloatField
    FieldName = 'Preco'
  end
  object QueryAtivo: TSmallintField
    FieldName = 'Ativo'
    MaxValue = 1
  end
  object QueryNome: TWideStringField
    FieldName = 'Nome'
    Size = 30
  end
end
*)

end.

