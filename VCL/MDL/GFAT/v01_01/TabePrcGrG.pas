unit TabePrcGrG;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral, Variants, dmkValUsu,
  dmkRadioGroup, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmTabePrcGrG = class(TForm)
    Panel1: TPanel;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    EdNivel1: TdmkEditCB;
    CBNivel1: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdPreco: TdmkEdit;
    Label2: TLabel;
    QrPesqPrc: TmySQLQuery;
    QrPesqPrcPreco: TFloatField;
    Panel3: TPanel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    QrGraGru1CodUsu: TIntegerField;
    dmkValUsu1: TdmkValUsu;
    Panel4: TPanel;
    CkContinuar: TCheckBox;
    RGTipPrd: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdNivel1Change(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdNivel1Exit(Sender: TObject);
    procedure RGTipPrdClick(Sender: TObject);
  private
    { Private declarations }
    procedure PesquisaPreco(Altera: Boolean);
    procedure ReopenGraGru1(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmTabePrcGrG: TFmTabePrcGrG;

implementation

uses UnMyObjects, Module, TabePrcCab, UMySQLModule;

{$R *.DFM}

procedure TFmTabePrcGrG.BtOKClick(Sender: TObject);
var
  Resul: Boolean;
  Codigo, Nivel1: Integer;
begin
  PesquisaPreco(False);
  if QrPesqPrc.RecordCount > 0 then
  begin
    if Application.MessageBox(PChar('O grupo de produtos "' + CBNivel1.Text +
    '" j� pertence a esta lista. Deseja alterar seu pre�o?'), 'Aviso',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      ImgTipo.SQLType := stUpd
    else
      Exit;
  end;
  Codigo := FmTabePrcCab.QrTabePrcCabCodigo.Value;
  Nivel1 := Geral.IMV(EdNivel1.Text);
  if Nivel1 = 0 then
  begin
    Application.MessageBox('Informe o grupo de produtos!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdNivel1.SetFocus;
    Exit;             
  end;
  if ImgTipo.SQLType = stUpd then
    Resul := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'tabeprcgrg', False, [
    'Preco'], ['Codigo', 'Nivel1'], [EdPreco.ValueVariant], [Codigo,
    dmkValUsu1.ValueVariant], True)
  else
    Resul := UMyMod.ExecSQLInsUpdFm(FmTabePrcGrG, ImgTipo.SQLType, 'tabeprcgrg', Codigo,
      Dmod.QrUpd);
  if Resul then
  begin
    FmTabePrcCab.ReopenTabePrcGrG(Nivel1);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType        := stIns;
      EdNivel1.ValueVariant := 0;
      CBNivel1.KeyValue     := Null;
      EdPreco.ValueVariant  := 0;
      EdNivel1.SetFocus;
    end else Close;
  end;
end;

procedure TFmTabePrcGrG.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTabePrcGrG.PesquisaPreco(Altera: Boolean);
var
  Codigo, Nivel1: Integer;
begin
  Codigo := FmTabePrcCab.QrTabePrcCabCodigo.Value;
  if EdNivel1.ValueVariant <> 0 then
    //Nivel1 := Geral.IMV(EdNivel1.Text)
    Nivel1 := dmkValUsu1.ValueVariant
  else
    Nivel1 := 0;
  //
  QrPesqPrc.Close;
  QrPesqPrc.Params[0].AsInteger := Nivel1;
  QrPesqPrc.Params[1].AsInteger := Codigo;
  UnDmkDAC_PF.AbreQuery(QrPesqPrc, Dmod.MyDB);
  //
  if Altera then
    if QrPesqPrcPreco.Value > 0 then
      EdPreco.ValueVariant := QrPesqPrcPreco.Value;
end;

procedure TFmTabePrcGrG.ReopenGraGru1(Controle: Integer);
begin
  QrGraGru1.Close;
  QrGraGru1.Params[0].AsInteger := RGTipPrd.ItemIndex;
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  //
  EdNivel1.ValueVariant := 0;
  CBNivel1.KeyValue     := Null;
end;

procedure TFmTabePrcGrG.RGTipPrdClick(Sender: TObject);
begin
  ReopenGraGru1(0);
end;

procedure TFmTabePrcGrG.EdNivel1Change(Sender: TObject);
begin
  if not EdNivel1.Focused then
    PesquisaPreco(True);
end;

procedure TFmTabePrcGrG.EdNivel1Exit(Sender: TObject);
begin
  PesquisaPreco(True);
end;

procedure TFmTabePrcGrG.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTabePrcGrG.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  RGTipPrd.ItemIndex := 1;
  ReopenGraGru1(0);
  EdPreco.DecimalSize := Dmod.QrControleCasasProd.Value;
end;

procedure TFmTabePrcGrG.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
