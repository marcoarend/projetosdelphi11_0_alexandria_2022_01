unit FatDivCms;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkRadioGroup, Mask, DmkDAC_PF, UnDmkEnums;

type
  TFmFatDivCms = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    QrLcts: TmySQLQuery;
    DsLcts: TDataSource;
    QrLct: TmySQLQuery;
    QrFatDivRef: TmySQLQuery;
    QrFatDivRefValComisLiqF: TFloatField;
    QrFatDivRefValComisLiqR: TFloatField;
    QrLctControle: TIntegerField;
    QrLctSerieNF: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctCredito: TFloatField;
    QrLctVencimento: TDateField;
    QrLctFatParcela: TIntegerField;
    TbLct: TmySQLTable;
    TbLctData: TDateField;
    TbLctTipo: TSmallintField;
    TbLctCarteira: TIntegerField;
    TbLctGenero: TIntegerField;
    TbLctDescricao: TWideStringField;
    TbLctNotaFiscal: TIntegerField;
    TbLctDebito: TFloatField;
    TbLctCredito: TFloatField;
    TbLctSerieCH: TWideStringField;
    TbLctDocumento: TFloatField;
    TbLctVencimento: TDateField;
    TbLctFatID: TIntegerField;
    TbLctFatID_Sub: TIntegerField;
    TbLctFatNum: TFloatField;
    TbLctFatParcela: TIntegerField;
    TbLctMez: TIntegerField;
    TbLctFornecedor: TIntegerField;
    TbLctCliente: TIntegerField;
    TbLctCliInt: TIntegerField;
    TbLctForneceI: TIntegerField;
    TbLctMoraDia: TFloatField;
    TbLctMulta: TFloatField;
    TbLctDataDoc: TDateField;
    TbLctVendedor: TIntegerField;
    TbLctAccount: TIntegerField;
    TbLctDuplicata: TWideStringField;
    TbLctDepto: TIntegerField;
    TbLctUnidade: TIntegerField;
    TbLctSit: TIntegerField;
    DsLct: TDataSource;
    DBGrid1: TDBGrid;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    QrPediAcc: TmySQLQuery;
    DsPediAcc: TDataSource;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label3: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label4: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    QrNFeA: TmySQLQuery;
    QrNFeAide_nNF: TIntegerField;
    QrPediPrzIts: TmySQLQuery;
    QrPediPrzItsDias: TIntegerField;
    QrPediPrzItsPercent1: TFloatField;
    QrPediPrzItsPercent2: TFloatField;
    QrSumPPI: TmySQLQuery;
    QrSumPPITotal: TFloatField;
    dmkRadioGroup1: TDBRadioGroup;
    QrLctDuplicata: TWideStringField;
    QrSumPPIPercent1: TFloatField;
    QrSumPPIPercent2: TFloatField;
    QrPediAccCodigo: TIntegerField;
    QrPediAccSupervisor: TIntegerField;
    QrPediAccAtivo: TSmallintField;
    QrPediAccNOMEACC: TWideStringField;
    QrPediAccNOMESUP: TWideStringField;
    QrPediAccCartFin: TIntegerField;
    QrPediAccContaFin: TIntegerField;
    QrPediAccTpEmiCoRec: TIntegerField;
    QrPediAccNO_CART: TWideStringField;
    QrPediAccTIPO_CART: TIntegerField;
    QrPediAccNO_CONTA: TWideStringField;
    TbLctAtrelado: TIntegerField;
    TbLctOrdem: TIntegerField;
    QrSumRef: TmySQLQuery;
    QrSumRefLiquido: TFloatField;
    QrSumLct: TmySQLQuery;
    QrSumLctDebito: TFloatField;
    PB1: TProgressBar;
    TbLctSerieNF: TWideStringField;
    QrNFeAide_Serie: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Flct: String;
  public
    { Public declarations }
    FFatNum, FIDDuplicata, FVendedor, FEntidade, FCodUsu, FCondicaoPG, FAssociada:
    Integer;
    FDataFat: TDateTime;
    //
    function  CriaLcts(): Boolean;
    function  ConfirmaLancamento(): Boolean;
    procedure ReopenPediAcc();
  end;

  var
  FmFatDivCms: TFmFatDivCms;

implementation

uses
  UnFinanceiro,
  UnMyObjects, Module, UMySQLModule, ModuleGeral, ModProd, UCreate,
  ModuleFatura;

{$R *.DFM}

procedure TFmFatDivCms.BtOKClick(Sender: TObject);
var
  Ref, Lct: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumRef, Dmod.MyDB, [
  'SELECT SUM(ValComisLiqF + ValComisLiqR) Liquido',
  'FROM fatdivref',
  'WHERE Codigo=' + FormatFloat('0', FFatNum),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumLct, DModG.MyPID_DB, [
  'SELECT SUM(Debito) Debito',
  'FROM _lct_',
  '']);
  //
  Ref := FormatFloat('#,###,##0.00', QrSumRefLiquido.Value);
  Lct := FormatFloat('#,###,##0.00', QrSumLctDebito.Value);
  if MyObjects.FIC(Ref <> Lct, nil, 'Inclus�o cancelada! Valores n�o conferem:'
  + sLineBreak + 'Comiss�es: $ ' + Ref + sLineBreak + 'A Lan�ar: $' + Lct) then
    Exit;
  //
  if MyObjects.FIC(QrSumLctDebito.Value < 0.01, nil,
  'Inclus�o cancelada! N�o h� comiss�o a ser lan�ada!') then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    TbLct.DisableControls;
    PB1.Position := 0;
    PB1.Max := TbLct.RecordCount;
    while not TbLct.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      'Incluindo lan�amento ' + FormatFloat('0', TbLct.RecNo) + ' de ' +
      FormatFloat('0', TbLct.RecordCount));
      //
      ConfirmaLancamento();
      //
      TbLct.Next;
    end;
    TbLct.EnableControls;
    //
    Screen.Cursor := crDefault;
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFatDivCms.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmFatDivCms.ConfirmaLancamento(): Boolean;
var
  Filial: Integer;
  TabLctA: String;
begin
  Result := False;
  //
  UFinanceiro.LancamentoDefaultVARS();
  //
  FLAN_Data          := Geral.FDT(TbLctData.Value, 1);
  FLAN_Vencimento    := Geral.FDT(TbLctVencimento.Value, 1);
  //FLAN_Mez           := TbLctMez.Value;
  FLAN_Descricao     := TbLctDescricao.Value;
  if TbLctTipo.Value <> 2 then
    FLAN_Compensado    := FLAN_DATA
  else
    FLAN_Compensado    := Geral.FDT(0, 1);
  FLAN_Duplicata     := TbLctDuplicata.Value;
  //FLAN_Doc2          := TbLctDoc2.Value;
  FLAN_SerieCH       := TbLctSerieCH.Value;
  //
  FLAN_Documento     := TbLctDocumento.Value;
  FLAN_Tipo          := TbLctTipo.Value;
  FLAN_Carteira      := TbLctCarteira.Value;
  FLAN_Credito       := TbLctCredito.Value;
  FLAN_Debito        := TbLctDebito.Value;
  FLAN_Genero        := TbLctGenero.Value;
{ Ver o que Fazer!
  FLAN_GenCtb     := GenCtb;
  FLAN_GenCtbD    := GenCtbD;
  FLAN_GenCtbC    := GenCtbC;
}
 FLAN_SerieNF       := TbLctSerieNF.Value;
  FLAN_NotaFiscal    := TbLctNotaFiscal.Value;
  FLAN_Sit           := TbLctSit.Value;
  //FLAN_Sub           := TbLctSub.Value;
  //FLAN_ID_Pgto       := TbLctID_Pgto.Value;
  //FLAN_Cartao        := TbLctCartao.Value;
  //FLAN_Linha         := TbLctLinha.Value;
  FLAN_Fornecedor    := TbLctFornecedor.Value;
  FLAN_Cliente       := TbLctCliente.Value;
  FLAN_MoraDia       := TbLctMoraDia.Value;
  FLAN_Multa         := TbLctMulta.Value;
  //FLAN_UserCad       := TbLctUserCad.Value;
  FLAN_DataDoc       := Geral.FDT(TbLctDataDoc.Value, 1);
  FLAN_Vendedor      := TbLctVendedor.Value;
  FLAN_Account       := TbLctAccount.Value;
  //FLAN_ICMS_P        := TbLctICMS_P.Value;
  //FLAN_ICMS_V        := TbLctICMS_V.Value;
  FLAN_CliInt        := TbLctCliInt.Value;
  FLAN_Depto         := TbLctDepto.Value;
  //FLAN_DescoPor      := TbLctDescoPor.Value;
  FLAN_ForneceI      := TbLctForneceI.Value;
  //FLAN_DescoVal      := TbLctDescoVal.Value;
  //FLAN_NFVal         := TbLctNFVal.Value;
  FLAN_Unidade       := TbLctUnidade.Value;
  //FLAN_Qtde          := TbLctQtde.Value;
  //FLAN_Qtd2          := TbLctQtd2.Value;
  FLAN_FatID         := TbLctFatID.Value;
  FLAN_FatID_Sub     := TbLctFatID_Sub.Value;
  FLAN_FatNum        := TbLctFatNum.Value;
  FLAN_FatParcela    := TbLctFatParcela.Value;
  //FLAN_Pago          := TbLctPago.Value;
  //                   
  //FLAN_MultaVal      := TbLctMultaVal.Value;
  //FLAN_MoraVal       := TbLctMoraVal.Value;
  //FLAN_CtrlIni       := TbLctCtrlIni.Value;
  //FLAN_Nivel         := TbLctNivel.Value;
  //FLAN_CNAB_Sit      := TbLctCNAB_Sit.Value;
  //FLAN_TipoCH        := TbLctTipoCH.Value;
  FLAN_Atrelado      := TbLctAtrelado.Value;
  //FLAN_SubPgto1      := TbLctSubPgto1.Value;
  //FLAN_MultiPgto     := TbLctMultiPgto.Value;
  //FLAN_Protocolo     := TbLctProtocolo.Value;
  //
  //FLAN_Emitente      := TbLctEmitente.Value;
  //FLAN_CNPJCPF       := TbLctCNPJCPF.Value;
  //FLAN_Banco         := TbLctBanco.Value;
  //FLAN_Agencia       := TbLctAgencia.Value;
  //FLAN_ContaCorrente := TbLctContaCorrente.Value;
  //
  FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
    'Controle', VAR_LCT, VAR_LCT, 'Controle');
  //
  {$IFDEF DEFINE_VARLCT}
  Filial := DmFatura.ObtemFilialDeEntidade(TbLctCliInt.Value);
  TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  if UFinanceiro.InsereLancamento(TabLctA) then
  begin
    Result := True;
  end;
  {$Else}
  if UFinanceiro.InsereLancamento() then
  begin
    Result := True;
  end;
  {$EndIf}
end;

function TFmFatDivCms.CriaLcts(): Boolean;
var
  FatID_TXT, FatNum_TXT, DataFat_TXT,
  Data, DataDoc, Vencimento, Descricao, Duplicata, SerieNF: String;
  //
  FatID, FatID_Sub, FatParcela, Fornecedor, (*Empresa,*) ForneceI, Vendedor, Account,
  Tipo, Carteira, Genero, NotaFiscal, Sit, Atrelado, Ordem: Integer;
  //
  FatNum, Debito, Documento: Double;
  //
  Continua: Boolean;
  //
  Sobra: Double;
  procedure InsereItem(CliInt: Integer);
  begin
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, Flct, False, [
    'Data', 'Tipo', 'Carteira',
    'Genero', 'Descricao', 'NotaFiscal',
    'Debito',
    'Documento', 'Sit', 'Vencimento',
    'FatID', 'FatID_Sub', 'FatNum',
    'FatParcela', 'Fornecedor',
    'CliInt', 'ForneceI',
    'DataDoc',
    'Vendedor', 'Account', 'Duplicata',
    'Atrelado', 'SerieNF', 'Ordem'], [
    ], [
    Data, Tipo, Carteira,
    Genero, Descricao, NotaFiscal,
    Debito,
    Documento, Sit, Vencimento,
    FatID, FatID_Sub, FatNum,
    FatParcela, Fornecedor,
    CliInt, ForneceI,
    DataDoc,
    Vendedor, Account, Duplicata,
    Atrelado, SerieNF, Ordem], [
    ], False);
  end;
  //
var
  Filial: Integer;
  TabLctA: String;
begin
  Result := False;
  //
  Filial := DModG.ObtemFilialDeEntidade(FEntidade);
  if Filial <> 0 then
    TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial)
  else
    TabLctA := sTabLctErr;
  //
  Flct := UCriar.RecriaTempTableNovo(ntrttLct, DmodG.QrUpdPID1, False);
  FatID_TXT := FormatFloat('0', VAR_FATID_0001);
  FatNum_TXT := FormatFloat('0', FFatNum);
  DataFat_TXT := Geral.FDT(FDataFat, 1);

  // Abre NFeCabA > Dados da NF
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeA, Dmod.MyDB, [
  'SELECT ide_Serie, ide_nNF  ',
  'FROM nfecaba ',
  'WHERE FatID=' + FatID_TXT,
  'AND FatNum=' + FatNum_TXT,
  ' ']);
  if MyObjects.FIC(QrNFeA.RecordCount = 0, nil, 'CUIDADO!' + sLineBreak +
  'N�o foi localizada nenhuma NF-e!') then
    (*Exit - s� aviso*);


  UnDmkDAC_PF.AbreMySQLQuery0(QrLct, Dmod.MyDB, [
  'SELECT Controle, SerieNF, NotaFiscal, Credito, ',
  'Vencimento, FatParcela, Duplicata ',
  'FROM ' + TabLctA,
  'WHERE FatID=' + FatID_TXT,
  'AND FatNum=' + FatNum_TXT,
  ' ']);
  if MyObjects.FIC(QrNFeA.RecordCount = 0, nil, 'CUIDADO!' + sLineBreak +
  'N�o foi localizado nenhum lan�amento financeiro de cobran�a do destinat�rio!') then
    (*Exit - s� aviso*);
  //

  // Abre FatDivRef > itens do pedido com os dados de comiss�o
  UnDmkDAC_PF.AbreMySQLQuery0(QrFatDivRef, Dmod.MyDB, [
  'SELECT  ',
  'SUM(ValComisLiqF) ValComisLiqF, ',
  'SUM(ValComisLiqR) ValComisLiqR ',
  'FROM fatdivref ',
  'WHERE Codigo=' + FatNum_TXT,
  ' ']);
  if MyObjects.FIC(
  QrFatDivRefValComisLiqF.Value + QrFatDivRefValComisLiqR.Value < 0.01, nil,
  'N�o h� valor de comiss�o nos itens a ser lan�ado!' + sLineBreak +
  'Verifique nos itens do faturamento se h� valores de comiss�es!') then
    Exit;
  //
  // Verifica se a condi��o de pagamento foi bem setada
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPPI, Dmod.MyDB, [
  'SELECT SUM(Percent1 + Percent2) Total,',
  'SUM(Percent1) Percent1, SUM(Percent2) Percent2  ',
  'FROM pediprzits ppi',
  'WHERE Codigo=' + FormatFloat('0', FCondicaoPG),
  'ORDER BY Dias ',
  ' ']);
  Continua := (QrSumPPITotal.Value < 100.0001)
          and (QrSumPPITotal.Value >  99.9999);
  if MyObjects.FIC(not Continua, nil, 'Condi��o de pagamento mal definida!' +
  sLineBreak + 'Corrija os percentuais de cada parcela para que juntos somem 100%!' +
  'A soma atual � %: ' + FormatFloat('0.000000000000000', QrSumPPITotal.Value)) then
    Exit;
  //
  // Abre PediPrzIts > Percentuais do parcelamento por parcela
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzIts, Dmod.MyDB, [
  'SELECT Dias, Percent1, Percent2  ',
  'FROM pediprzits ',
  'WHERE Codigo=' + FormatFloat('0', FCondicaoPG),
  'ORDER BY Dias ',
  ' ']);
  //
  DModG.ReopenParamsEmp(FEntidade);
  // Verifica como paga comiss�es
  if QrFatDivRef.RecordCount > 0 then
  begin
    DataDoc    := DataFat_TXT;
    SerieNF    := Geral.FF0(QrNFeAide_Serie.Value);
    NotaFiscal := QrNFeAide_nNF.Value;
    Fornecedor := FVendedor;
    //CliInt     := FEntidade;
    Vendedor   := FVendedor;
    Account    := FVendedor;
    Tipo       := QrPediAccTIPO_CART.Value;
    Carteira   := QrPediAccCartFin.Value;
    Genero     := QrPediAccContaFin.Value;
    Documento  := FCodUsu;

    // Comiss�o no Faturamento
    if QrFatDivRefValComisLiqF.Value >= 0.01 then
    begin
      Data       := DataFat_TXT;
      Vencimento := DataFat_TXT;
      Descricao  := 'CVF1';
      //
      FatID      := VAR_FATID_1801;
      FatID_Sub  := 1; // Faturamento
      FatNum     := FFatNum;
      FatParcela := 0;
      // Deveria ser s� caixa, mas acaso o usu�rio consiga mudar:
      case Tipo of
        0: Sit := 3;
        1: Sit := 3;
        2: Sit := 0;
        else Sit := -1;
      end;
      //
      Atrelado   := 0;
      //
      Duplicata := DmProd.MontaDuplicata(
        FIDDuplicata, NotaFiscal, FatParcela,
        DModG.QrParamsEmpFaturaNum.Value,
        DModG.QrParamsEmpFaturaSeq.Value,
        DModG.QrParamsEmpTxtProdVen.Value,
        DModG.QrParamsEmpFaturaSep.Value);
      //
      Sobra := QrFatDivRefValComisLiqF.Value;
      if QrSumPPIPercent1.Value > 0 then
      begin
        Ordem := Ordem + 1;
        Debito := Geral.RoundC(Sobra * (QrSumPPIPercent1.Value / 100), 2);
        Sobra := Sobra - Debito;
        //
        InsereItem(FEntidade);
      end;
      if QrSumPPIPercent2.Value > 0 then
      begin
        Ordem := Ordem + 1;
        Debito := Sobra;
        //
        InsereItem(FAssociada);
      end;
    end;

    // Comiss�o no Recebimento
    if QrFatDivRefValComisLiqR.Value >= 0.01 then
    begin
      // Se receber somente ap�s receber tudo...
      if (QrPediAccTpEmiCoRec.Value = 1)
      // ou se for apenas uma parcela.
      or (QrPediPrzIts.RecordCount = 1) then
      begin
        // caso receber somente ap�s ser tudo pago, pegar o �ltimo vencimento!
        QrPediPrzIts.Last;
        //
        Data       := Geral.FDT(FDataFat + QrPediPrzItsDias.Value, 1);
        Vencimento := Data;
        Descricao  := 'CVR2';
        FatID      := VAR_FATID_1801;
        FatID_Sub  := 2; // Recebimento total
        FatNum     := FFatNum;
        FatParcela := 0;
        // Deve ser bloqueado at� ser pago!
        Sit := 5;
        //
        Atrelado   := 0;
        //
        //Duplicata  := FormatFloat('0', FFatNum);
        Duplicata := DmProd.MontaDuplicata(
          FIDDuplicata, NotaFiscal, FatParcela,
          DModG.QrParamsEmpFaturaNum.Value,
          DModG.QrParamsEmpFaturaSeq.Value,
          DModG.QrParamsEmpTxtProdVen.Value,
          DModG.QrParamsEmpFaturaSep.Value);
        //
        Sobra := QrFatDivRefValComisLiqR.Value;
        if QrSumPPIPercent1.Value > 0 then
        begin
          Ordem := Ordem + 1;
          Debito := Geral.RoundC(Sobra * (QrSumPPIPercent1.Value / 100), 2);
          Sobra := Sobra - Debito;
          //
          InsereItem(FEntidade);
        end;
        if QrSumPPIPercent2.Value > 0 then
        begin
          Ordem := Ordem + 1;
          Debito := Sobra;
          //
          InsereItem(FAssociada);
        end;
      end else
      begin
        // Fazer um lan�amento para cada parcela!
        QrPediPrzIts.First;
        Sobra := QrFatDivRefValComisLiqR.Value;
        while not QrPediPrzIts.Eof do
        begin
          Data       := Geral.FDT(FDataFat + QrPediPrzItsDias.Value, 1);
          Vencimento := Data;
          Descricao  := 'CVR3';
          if QrLct.Locate('FatParcela', QrPediPrzIts.RecNo, []) then
          begin
            Duplicata := QrLctDuplicata.Value;
            Atrelado   := QrLctControle.Value;
          end else begin
            Atrelado   := 0;
            Geral.MB_Aviso(
              'N�o foi poss�vel localizar o n�mero da duplicata nos lan�amentos!' +
              sLineBreak +
              'Ela ser� redefinida para informar no lan�amento de comiss�o!' +
              sLineBreak +
              'N�o haver� atrelamento para verifica��o de libera��o de pagamento!');
            Duplicata := DmProd.MontaDuplicata(
              FIDDuplicata, NotaFiscal, QrPediPrzIts.RecNo,
              DModG.QrParamsEmpFaturaNum.Value,
              DModG.QrParamsEmpFaturaSeq.Value,
              DModG.QrParamsEmpTxtProdVen.Value,
              DModG.QrParamsEmpFaturaSep.Value);
          end;
          //
          FatID      := VAR_FATID_1801;
          FatID_Sub  := 3; // Recebimento parcial
          FatNum     := FFatNum;
          FatParcela := QrPediPrzIts.RecNo;
          // Deve ser bloqueado at� ser pago!
          Sit := 5;
          //

          if QrPediPrzItsPercent1.Value >= 0.0001 then
          begin
            Ordem := Ordem + 1;
            if (QrPediPrzIts.RecNo = QrPediPrzIts.RecordCount) and
            (QrPediPrzItsPercent2.Value < 0.0001) then
              Debito := Sobra
            else
              Debito := Geral.RoundC(QrFatDivRefValComisLiqR.Value *
              (QrPediPrzItsPercent1.Value / 100), 2);
            InsereItem(FEntidade);
            Sobra := Sobra - Debito;
          end;
          if QrPediPrzItsPercent2.Value >= 0.0001 then
          begin
            Ordem := Ordem + 1;
            if (QrPediPrzIts.RecNo = QrPediPrzIts.RecordCount) then
              Debito := Sobra
            else
              Debito := Geral.RoundC(QrFatDivRefValComisLiqR.Value *
              (QrPediPrzItsPercent2.Value / 100), 2);
            InsereItem(FAssociada);
            Sobra := Sobra - Debito;
          end;
          //
          QrPediPrzIts.Next;
        end;
      end;
    end;
    //
    TbLct.Close;
    TbLct.TableName := Flct;
    UnDmkDAC_PF.AbreTable(TbLct, DModG.MyPID_DB);
    //
    Result := True;
  end;
end;

procedure TFmFatDivCms.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmFatDivCms.FormCreate(Sender: TObject);
begin
  TbLct.Close;
  TbLct.Database := DModG.MyPID_DB;
  //
  QrSumLct.Close;
  QrSumLct.Database := DModG.MyPID_DB;
  //
end;

procedure TFmFatDivCms.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatDivCms.ReopenPediAcc();
begin
  QrPediAcc.Close;
  QrPediAcc.Params[0].AsInteger := FVendedor;
  UnDmkDAC_PF.AbreQuery(QrPediAcc, Dmod.MyDB);
end;

end.
