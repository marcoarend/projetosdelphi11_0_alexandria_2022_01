unit FatPedPes2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, dmkEdit, dmkEditCB, DmkDAC_PF,
  DBCtrls, dmkDBLookupComboBox, Grids, DBGrids, dmkDBGrid, DB, mySQLDbTables,
  dmkImage, UnDmkEnums;

type
  TFmFatPedPes2 = class(TForm)
    Panel1: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    Label1: TLabel;
    EdCliente: TdmkEditCB;
    Label2: TLabel;
    CBCliente: TdmkDBLookupComboBox;
    QrPedidos: TmySQLQuery;
    DsPedidos: TDataSource;
    QrPedidosCodUsu: TIntegerField;
    QrPedidosDtaEmiss: TDateField;
    QrPedidosDtaPrevi: TDateField;
    QrPedidosPrioridade: TSmallintField;
    QrPedidosPedidoCli: TWideStringField;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENT: TWideStringField;
    DsClientes: TDataSource;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure dmkDBGrid1DblClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  procedure ReabrePedidos();
  public
    { Public declarations }
    FEdPedido: TdmkEdit;
  end;

  var
  FmFatPedPes2: TFmFatPedPes2;

implementation

uses UnMyObjects, Module, ModuleGeral, ModPediVda, UnInternalConsts, FatPedCab2,
  UnMySQLCuringa;

{$R *.DFM}

procedure TFmFatPedPes2.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatPedPes2.BtOKClick(Sender: TObject);
begin
  if FEdPedido <> nil then
    FEdPedido.ValueVariant := QrPedidosCodUsu.Value
  else
    FmFatPedCab2.EdPedido.ValueVariant := QrPedidosCodUsu.Value;
  Close;
end;

procedure TFmFatPedPes2.dmkDBGrid1DblClick(Sender: TObject);
begin
  if QrPedidos.RecordCount > 0 then
    BtOKClick(Self);
end;

procedure TFmFatPedPes2.EdClienteChange(Sender: TObject);
begin
  ReabrePedidos();
end;

procedure TFmFatPedPes2.EdEmpresaChange(Sender: TObject);
begin
  ReabrePedidos();
end;

procedure TFmFatPedPes2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatPedPes2.FormCreate(Sender: TObject);
begin
  FEdPedido := nil;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
end;

procedure TFmFatPedPes2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatPedPes2.ReabrePedidos;
var
  Empresa, Cliente: Integer;
begin
  Screen.Cursor := crHourGlass;
  QrPedidos.Close;
  Empresa := Geral.IMV(EdEmpresa.Text);
  Cliente := Geral.IMV(EdCliente.Text);
  if (Empresa <> 0) and (Cliente <> 0) then
  begin
    Empresa := DModG.QrEmpresasCodigo.Value;
    QrPedidos.Params[00].AsInteger := Empresa;
    QrPedidos.Params[01].AsInteger := Cliente;
    UnDmkDAC_PF.AbreQuery(QrPedidos, Dmod.MyDB);
    BtOK.Enabled := QrPedidos.RecordCount > 0;
  end else BtOK.Enabled := False;
  Screen.Cursor := crDefault;
end;

procedure TFmFatPedPes2.SpeedButton1Click(Sender: TObject);
var
  Cliente: Integer;
begin
  Cliente := CuringaLoc.CriaForm('Codigo', 'Nome', 'Entidades', Dmod.MyDB, '', True);
  if Cliente <> 0 then
  begin
    EdCliente.ValueVariant := Cliente;
    CBCliente.KeyValue     := Cliente;
  end;
end;

end.
