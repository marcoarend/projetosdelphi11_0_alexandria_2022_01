object FmFatDivCms: TFmFatDivCms
  Left = 339
  Top = 185
  Caption = 'NFe-GERAL-007 :: Lan'#231'amentos de Comiss'#245'es'
  ClientHeight = 592
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 344
        Height = 32
        Caption = 'Lan'#231'amentos de Comiss'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 344
        Height = 32
        Caption = 'Lan'#231'amentos de Comiss'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 344
        Height = 32
        Caption = 'Lan'#231'amentos de Comiss'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 522
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 417
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 417
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 105
        Width = 1008
        Height = 312
        Align = alClient
        Caption = ' Pr'#233'via dos Lan'#231'amentos: '
        TabOrder = 0
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 1004
          Height = 295
          Align = alClient
          DataSource = DsLct
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              ReadOnly = True
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Carteira'
              ReadOnly = True
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Genero'
              ReadOnly = True
              Title.Caption = 'Conta'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              ReadOnly = True
              Title.Caption = 'Descri'#231#227'o'
              Width = 121
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SerieNF'
              Title.Caption = 'S'#233'rie'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              ReadOnly = True
              Title.Caption = 'N.F.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              ReadOnly = True
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sit'
              ReadOnly = True
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fornecedor'
              ReadOnly = True
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CliInt'
              ReadOnly = True
              Title.Caption = 'Empresa'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataDoc'
              ReadOnly = True
              Title.Caption = 'Data doc.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duplicata'
              ReadOnly = True
              Width = 94
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Atrelado'
              ReadOnly = True
              Visible = True
            end>
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 105
        Align = alTop
        Caption = ' Dados do Representante: '
        TabOrder = 1
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 88
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 12
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Codigo'
            FocusControl = DBEdit1
          end
          object Label2: TLabel
            Left = 460
            Top = 4
            Width = 53
            Height = 13
            Caption = 'Supervisor:'
            FocusControl = DBEdit3
          end
          object Label3: TLabel
            Left = 12
            Top = 44
            Width = 36
            Height = 13
            Caption = 'Carteira'
            FocusControl = DBEdit5
          end
          object Label4: TLabel
            Left = 460
            Top = 44
            Width = 131
            Height = 13
            Caption = 'Conta (do plano de contas):'
            FocusControl = DBEdit7
          end
          object DBEdit1: TDBEdit
            Left = 12
            Top = 20
            Width = 44
            Height = 21
            DataField = 'Codigo'
            DataSource = DsPediAcc
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 56
            Top = 20
            Width = 400
            Height = 21
            DataField = 'NOMEACC'
            DataSource = DsPediAcc
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 460
            Top = 20
            Width = 44
            Height = 21
            DataField = 'Supervisor'
            DataSource = DsPediAcc
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 504
            Top = 20
            Width = 308
            Height = 21
            DataField = 'NOMESUP'
            DataSource = DsPediAcc
            TabOrder = 3
          end
          object DBEdit5: TDBEdit
            Left = 12
            Top = 60
            Width = 44
            Height = 21
            DataField = 'CartFin'
            DataSource = DsPediAcc
            TabOrder = 4
          end
          object DBEdit6: TDBEdit
            Left = 56
            Top = 60
            Width = 400
            Height = 21
            DataField = 'NO_CART'
            DataSource = DsPediAcc
            TabOrder = 5
          end
          object DBEdit7: TDBEdit
            Left = 460
            Top = 60
            Width = 44
            Height = 21
            DataField = 'ContaFin'
            DataSource = DsPediAcc
            TabOrder = 6
          end
          object DBEdit8: TDBEdit
            Left = 504
            Top = 60
            Width = 308
            Height = 21
            DataField = 'NO_CONTA'
            DataSource = DsPediAcc
            TabOrder = 7
          end
          object dmkRadioGroup1: TDBRadioGroup
            Left = 821
            Top = 9
            Width = 176
            Height = 72
            Caption = ' Pag. da comiss'#227'o  no receb.: '
            DataField = 'TpEmiCoRec'
            DataSource = DsPediAcc
            Items.Strings = (
              'N'#227'o definido'
              'Quando receber tudo'
              'Proporcional a cada quita'#231#227'o')
            TabOrder = 8
            Values.Strings = (
              '0'
              '1'
              '2')
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 465
    Width = 1008
    Height = 57
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 562
        Height = 16
        Caption = 
          'Os lan'#231'amentos aqui mostrados s'#227'o apenas uma pr'#233'via. Clique no b' +
          'ot'#227'o "OK" para confirm'#225'-los!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 562
        Height = 16
        Caption = 
          'Os lan'#231'amentos aqui mostrados s'#227'o apenas uma pr'#233'via. Clique no b' +
          'ot'#227'o "OK" para confirm'#225'-los!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 12
        Top = 20
        Width = 981
        Height = 17
        TabOrder = 0
      end
    end
  end
  object QrLcts: TMySQLQuery
    Database = Dmod.MyDB
    Left = 168
    Top = 112
  end
  object DsLcts: TDataSource
    DataSet = QrLcts
    Left = 196
    Top = 112
  end
  object QrLct: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, SerieNF, NotaFiscal, Credito, '
      'Vencimento, FatParcela, Duplicata '
      'FROM lanctos '
      'WHERE FatID=1 '
      'AND FatNum=2065 '
      ' ')
    Left = 308
    Top = 92
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
  end
  object QrFatDivRef: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(ValComisLiqF) ValComisLiqF,'
      'SUM(ValComisLiqR) ValComisLiqR'
      'FROM fatdivref'
      'WHERE Codigo=2065'
      '')
    Left = 336
    Top = 92
    object QrFatDivRefValComisLiqF: TFloatField
      FieldName = 'ValComisLiqF'
    end
    object QrFatDivRefValComisLiqR: TFloatField
      FieldName = 'ValComisLiqR'
    end
  end
  object TbLct: TMySQLTable
    Database = Dmod.MyDB
    SortFieldNames = 'Ordem'
    TableName = '_lct_'
    Left = 108
    Top = 272
    object TbLctOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object TbLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object TbLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object TbLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object TbLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object TbLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object TbLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbLctSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object TbLctDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000'
    end
    object TbLctVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object TbLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object TbLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object TbLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object TbLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object TbLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object TbLctCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object TbLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object TbLctForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object TbLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object TbLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object TbLctDataDoc: TDateField
      FieldName = 'DataDoc'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object TbLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object TbLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object TbLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object TbLctUnidade: TIntegerField
      FieldName = 'Unidade'
    end
    object TbLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object TbLctAtrelado: TIntegerField
      FieldName = 'Atrelado'
      DisplayFormat = '0;-0; '
    end
    object TbLctSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
  end
  object DsLct: TDataSource
    DataSet = TbLct
    Left = 136
    Top = 272
  end
  object QrPediAcc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pac.Codigo, pac.Supervisor, pac.Ativo,'
      'IF(acc.Tipo=0, acc.RazaoSocial, acc.Nome) NOMEACC,'
      'IF(sup.Tipo=0, sup.RazaoSocial, sup.Nome) NOMESUP,'
      'pac.CartFin, pac.ContaFin, pac.TpEmiCoRec,'
      'crt.Nome NO_CART,  crt.Tipo TIPO_CART,'
      'cta.Nome NO_CONTA'
      'FROM pediacc pac'
      'LEFT JOIN entidades acc ON acc.Codigo=pac.Codigo'
      'LEFT JOIN entidades sup ON sup.Codigo=pac.Supervisor'
      'LEFT JOIN carteiras crt ON crt.Codigo=pac.CartFin'
      'LEFT JOIN contas    cta ON cta.Codigo=pac.ContaFin'
      'WHERE pac.Codigo=:P0')
    Left = 408
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediAccCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediAccSupervisor: TIntegerField
      FieldName = 'Supervisor'
    end
    object QrPediAccAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPediAccNOMEACC: TWideStringField
      FieldName = 'NOMEACC'
      Size = 100
    end
    object QrPediAccNOMESUP: TWideStringField
      FieldName = 'NOMESUP'
      Size = 100
    end
    object QrPediAccCartFin: TIntegerField
      FieldName = 'CartFin'
    end
    object QrPediAccContaFin: TIntegerField
      FieldName = 'ContaFin'
    end
    object QrPediAccTpEmiCoRec: TIntegerField
      FieldName = 'TpEmiCoRec'
    end
    object QrPediAccNO_CART: TWideStringField
      FieldName = 'NO_CART'
      Size = 100
    end
    object QrPediAccTIPO_CART: TIntegerField
      FieldName = 'TIPO_CART'
      Required = True
    end
    object QrPediAccNO_CONTA: TWideStringField
      FieldName = 'NO_CONTA'
      Size = 50
    end
  end
  object DsPediAcc: TDataSource
    DataSet = QrPediAcc
    Left = 436
    Top = 12
  end
  object QrNFeA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ide_Serie, ide_nNF '
      'FROM nfecaba'
      'WHERE FatID=1'
      'AND FatNum=2065')
    Left = 364
    Top = 92
    object QrNFeAide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFeAide_Serie: TIntegerField
      FieldName = 'ide_Serie'
    end
  end
  object QrPediPrzIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Dias, Percent1, Percent2  '
      'FROM pediprzits '
      'WHERE Codigo=1 '
      'ORDER BY Dias ')
    Left = 392
    Top = 92
    object QrPediPrzItsDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPediPrzItsPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPediPrzItsPercent2: TFloatField
      FieldName = 'Percent2'
    end
  end
  object QrSumPPI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Percent1 + Percent2) Total,'
      'SUM(Percent1) Percent1, SUM(Percent2) Percent2  '
      'FROM pediprzits ppi'
      'WHERE Codigo=1 '
      'ORDER BY Dias ')
    Left = 420
    Top = 92
    object QrSumPPITotal: TFloatField
      FieldName = 'Total'
    end
    object QrSumPPIPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrSumPPIPercent2: TFloatField
      FieldName = 'Percent2'
    end
  end
  object QrSumRef: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ValComisLiqF + ValComisLiqR) Liquido'
      'FROM fatdivref'
      'WHERE Codigo=2065'
      '')
    Left = 108
    Top = 300
    object QrSumRefLiquido: TFloatField
      FieldName = 'Liquido'
    end
  end
  object QrSumLct: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Debito) Debito'
      'FROM _lct_')
    Left = 136
    Top = 300
    object QrSumLctDebito: TFloatField
      FieldName = 'Debito'
    end
  end
end
