object FmTabePrcNiv: TFmTabePrcNiv
  Left = 339
  Top = 185
  Caption = 'TAB-PRECO-005 :: Pre'#231'o por N'#237'vel de Produto'
  ClientHeight = 352
  ClientWidth = 630
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 630
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 582
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 534
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 324
        Height = 32
        Caption = 'Pre'#231'o por N'#237'vel de Produto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 324
        Height = 32
        Caption = 'Pre'#231'o por N'#237'vel de Produto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 324
        Height = 32
        Caption = 'Pre'#231'o por N'#237'vel de Produto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 282
    Width = 630
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 484
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 482
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 630
    Height = 190
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 630
      Height = 190
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 630
        Height = 190
        Align = alClient
        TabOrder = 0
        object RGNivel: TdmkRadioGroup
          Left = 2
          Top = 59
          Width = 626
          Height = 54
          Align = alTop
          Caption = ' N'#237'vel de Produto: '
          Columns = 5
          ItemIndex = 0
          Items.Strings = (
            'Nenhum'
            'Produto'
            'Nivel 2'
            'Nivel 3'
            'Tipo')
          TabOrder = 0
          OnClick = RGNivelClick
          QryCampo = 'Nivel'
          UpdCampo = 'Nivel'
          UpdType = utIdx
          OldValor = 0
        end
        object Panel9: TPanel
          Left = 2
          Top = 113
          Width = 626
          Height = 75
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Label5: TLabel
            Left = 8
            Top = 4
            Width = 175
            Height = 13
            Caption = 'C'#243'digo do item do n'#237'vel selecionado:'
          end
          object Label1: TLabel
            Left = 472
            Top = 4
            Width = 31
            Height = 13
            Caption = 'Pre'#231'o:'
          end
          object EdCodNiv: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CodNiv'
            UpdCampo = 'CodNiv'
            UpdType = utIdx
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCodNiv
            IgnoraDBLookupComboBox = False
          end
          object CBCodNiv: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 405
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCodNivel
            TabOrder = 1
            dmkEditCB = EdCodNiv
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdPreco: TdmkEdit
            Left = 472
            Top = 20
            Width = 110
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Preco'
            UpdCampo = 'Preco'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object CkContinuar: TCheckBox
            Left = 8
            Top = 48
            Width = 117
            Height = 17
            Caption = 'Continuar inserindo.'
            Checked = True
            State = cbChecked
            TabOrder = 3
          end
        end
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 626
          Height = 44
          Align = alTop
          Caption = ' Tabela: '
          Enabled = False
          TabOrder = 2
          object EdCodigo: TdmkEdit
            Left = 8
            Top = 16
            Width = 44
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utIdx
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNO_TAB: TEdit
            Left = 52
            Top = 16
            Width = 553
            Height = 21
            TabOrder = 1
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 238
    Width = 630
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 626
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrCodNivel: TmySQLQuery
    Database = Dmod.MyDB
    Left = 400
    Top = 24
  end
  object DsCodNivel: TDataSource
    DataSet = QrCodNivel
    Left = 428
    Top = 24
  end
end
