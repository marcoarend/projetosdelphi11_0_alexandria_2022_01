object DmPediVda: TDmPediVda
  OnCreate = DataModuleCreate
  Height = 618
  Width = 1023
  PixelsPerInch = 96
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCENT,'
      'CONCAT('
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome),'
      '  '#39' - '#39','
      '  IF(ent.Tipo=0, ent.CNPJ, ent.CPF)'
      ') NOME_E_DOCENT,'
      'mun.Nome CIDADE, ent.Observacoes,'
      'ufs.Nome NOMEUF, ent.indIEDest, ent.Tipo'
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      
        'LEFT JOIN locbdermall.dtb_munici mun ON mun.Codigo = IF(ent.Tipo' +
        '=0, ent.ECodMunici, ent.PCodMunici)'
      'WHERE ent.Fornece1="V"'
      'OR ent.Fornece2="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT'
      '')
    Left = 24
    Top = 12
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClientesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrClientesCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrClientesNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrClientesCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrClientesIE: TWideStringField
      FieldName = 'IE'
    end
    object QrClientesindIEDest: TIntegerField
      FieldName = 'indIEDest'
    end
    object QrClientesTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrClientesObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      Origin = 'entidades.Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrClientesDOCENT: TWideStringField
      FieldName = 'DOCENT'
      Size = 18
    end
    object QrClientesNOME_E_DOCENT: TWideStringField
      FieldName = 'NOME_E_DOCENT'
      Size = 121
    end
    object QrClientesMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 96
    Top = 12
  end
  object QrTabePrcCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, Sigla, DataI, DataF, '
      'DescoMax, JurosMes, PrazoMed, Moeda'
      'FROM tabeprccab'
      'WHERE Codigo=0'
      'OR'
      '('
      '  :P0 & Aplicacao'
      '  AND :P1 BETWEEN DataI and DataF'
      ')'
      'ORDER BY Nome')
    Left = 20
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTabePrcCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTabePrcCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrTabePrcCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrTabePrcCabSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 8
    end
    object QrTabePrcCabDataI: TDateField
      FieldName = 'DataI'
    end
    object QrTabePrcCabDataF: TDateField
      FieldName = 'DataF'
    end
    object QrTabePrcCabDescoMax: TFloatField
      FieldName = 'DescoMax'
    end
    object QrTabePrcCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrTabePrcCabPrazoMed: TFloatField
      FieldName = 'PrazoMed'
    end
    object QrTabePrcCabMoeda: TIntegerField
      FieldName = 'Moeda'
    end
  end
  object DsTabePrcCab: TDataSource
    DataSet = QrTabePrcCab
    Left = 92
    Top = 288
  end
  object QrPediPrzCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, MaxDesco,'
      'JurosMes, Parcelas, Percent1, Percent2,'
      'CondPg'
      'FROM pediprzcab'
      'WHERE Aplicacao & :P0 <> 0'
      'ORDER BY Nome')
    Left = 24
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrPediPrzCabCondPg: TSmallintField
      FieldName = 'CondPg'
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 96
    Top = 204
  end
  object DsFretePor: TDataSource
    DataSet = QrFretePor
    Left = 240
    Top = 60
  end
  object QrMotivos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM motivos'
      'WHERE Aplicacao & :P0 <> 0'#13)
    Left = 20
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMotivosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMotivosCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrMotivosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsMotivos: TDataSource
    DataSet = QrMotivos
    Left = 92
    Top = 336
  end
  object QrTransportas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'#13
      'ufs.Nome NOMEUF'
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Fornece2="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 24
    Top = 60
    object QrTransportasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportasNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrTransportasCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrTransportasNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
  end
  object DsTransportas: TDataSource
    DataSet = QrTransportas
    Left = 96
    Top = 60
  end
  object DsRedespachos: TDataSource
    DataSet = QrRedespachos
    Left = 96
    Top = 108
  end
  object QrPediAcc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT acc.Codigo, acc.PerComissF, acc.PerComissR,'
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEACC'
      'FROM pediacc acc'
      'LEFT JOIN entidades ent ON ent.Codigo=acc.Codigo'
      'ORDER BY NOMEACC')
    Left = 20
    Top = 384
    object QrPediAccCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediAccNOMEACC: TWideStringField
      FieldName = 'NOMEACC'
      Size = 100
    end
    object QrPediAccPerComissF: TFloatField
      FieldName = 'PerComissF'
    end
    object QrPediAccPerComissR: TFloatField
      FieldName = 'PerComissR'
    end
  end
  object DsPediAcc: TDataSource
    DataSet = QrPediAcc
    Left = 92
    Top = 384
  end
  object QrCartEmis: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome, car.Tipo'
      'FROM carteiras car'
      'WHERE Tipo=2'
      'ORDER BY Nome')
    Left = 20
    Top = 432
    object QrCartEmisCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCartEmisNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCartEmisTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsCartEmis: TDataSource
    DataSet = QrCartEmis
    Left = 92
    Top = 432
  end
  object QrRedespachos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'#13
      'ufs.Nome NOMEUF'
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Fornece2="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 24
    Top = 108
    object QrRedespachosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRedespachosNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrRedespachosCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrRedespachosNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT(mda.Sigla," - ",mda.Nome) NOMEMOEDA,'
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEFILIAL,'
      'ent.Filial, ent.Codigo Entidade, pem.*'
      'FROM entidades ent'
      'LEFT JOIN paramsemp pem ON pem.Codigo=ent.Codigo'
      'LEFT JOIN cambiomda mda ON mda.Codigo=pem.Moeda'
      'WHERE ent.Codigo = :P0')
    Left = 168
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsEmpNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 38
    end
    object QrParamsEmpNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Required = True
      Size = 100
    end
    object QrParamsEmpFilial: TIntegerField
      FieldName = 'Filial'
      Required = True
    end
    object QrParamsEmpEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrParamsEmpCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrParamsEmpMoeda: TIntegerField
      FieldName = 'Moeda'
    end
    object QrParamsEmpSituacao: TIntegerField
      FieldName = 'Situacao'
    end
    object QrParamsEmpRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
    end
    object QrParamsEmpFretePor: TSmallintField
      FieldName = 'FretePor'
    end
    object QrParamsEmpTipMediaDD: TSmallintField
      FieldName = 'TipMediaDD'
    end
    object QrParamsEmpFatSemPrcL: TSmallintField
      FieldName = 'FatSemPrcL'
    end
    object QrParamsEmpTipCalcJuro: TSmallintField
      FieldName = 'TipCalcJuro'
    end
    object QrParamsEmpUF_WebServ: TWideStringField
      FieldName = 'UF_WebServ'
      Size = 2
    end
    object QrParamsEmpversao: TFloatField
      FieldName = 'versao'
    end
    object QrParamsEmpPediVdaNElertas: TIntegerField
      FieldName = 'PediVdaNElertas'
    end
  end
  object QrItemPVI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM pedivdaits'
      'WHERE Codigo=:P0'
      'AND GraGruX=:P1'
      '')
    Left = 168
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrItemPVICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrItemPVIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrItemPVIGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrItemPVIPrecoO: TFloatField
      FieldName = 'PrecoO'
    end
    object QrItemPVIPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrItemPVIValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrItemPVIDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrItemPVIValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrItemPVIDescoV: TFloatField
      FieldName = 'DescoV'
    end
    object QrItemPVIQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrItemPVIQuantC: TFloatField
      FieldName = 'QuantC'
    end
    object QrItemPVIQuantV: TFloatField
      FieldName = 'QuantV'
    end
    object QrItemPVIPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrItemPVIMedidaC: TFloatField
      FieldName = 'MedidaC'
      Required = True
    end
    object QrItemPVIMedidaL: TFloatField
      FieldName = 'MedidaL'
      Required = True
    end
    object QrItemPVIMedidaA: TFloatField
      FieldName = 'MedidaA'
      Required = True
    end
    object QrItemPVIMedidaE: TFloatField
      FieldName = 'MedidaE'
      Required = True
    end
    object QrItemPVIPercCustom: TFloatField
      FieldName = 'PercCustom'
      Required = True
    end
    object QrItemPVICustomizad: TSmallintField
      FieldName = 'Customizad'
      Required = True
    end
    object QrItemPVIInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Required = True
    end
    object QrItemPVIOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrItemPVIReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 11
    end
    object QrItemPVIMulti: TIntegerField
      FieldName = 'Multi'
      Required = True
    end
    object QrItemPVILk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrItemPVIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrItemPVIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrItemPVIUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrItemPVIUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrItemPVIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrItemPVIAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrItemPVIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrItemPVIAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrItemPVIvProd: TFloatField
      FieldName = 'vProd'
      Required = True
    end
    object QrItemPVIvFrete: TFloatField
      FieldName = 'vFrete'
      Required = True
    end
    object QrItemPVIvSeg: TFloatField
      FieldName = 'vSeg'
      Required = True
    end
    object QrItemPVIvOutro: TFloatField
      FieldName = 'vOutro'
      Required = True
    end
    object QrItemPVIvDesc: TFloatField
      FieldName = 'vDesc'
      Required = True
    end
    object QrItemPVIvBC: TFloatField
      FieldName = 'vBC'
      Required = True
    end
  end
  object QrSumPed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ValLiq) ValLiq, SUM(QuantP) QuantP'
      'FROM pedivdaits pvi'
      'WHERE pvi.Codigo=:P0')
    Left = 168
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPedValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrSumPedQuantP: TFloatField
      FieldName = 'QuantP'
    end
  end
  object DsPVICliIts: TDataSource
    DataSet = TbPVICliIts
    Left = 552
    Top = 72
  end
  object QrLocNiv1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraGru1 '
      'FROM gragrux'#13
      'WHERE Controle=:P0')
    Left = 168
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocNiv1GraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
  end
  object QrPVIPrdCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Codusu, gg1.Nivel1, '
      'gg1.Nome'
      'FROM gragru1 gg1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pgt.TipPrd=1'
      'ORDER BY gg1.Nome')
    Left = 332
    Top = 24
    object QrPVIPrdCadCodusu: TIntegerField
      FieldName = 'Codusu'
      Required = True
    end
    object QrPVIPrdCadNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrPVIPrdCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
  end
  object DsPVIPrdCad: TDataSource
    DataSet = QrPVIPrdCad
    Left = 404
    Top = 24
  end
  object QrPVIEmpCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Filial, Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NOMEENT'
      'FROM entidades'
      'WHERE Codigo < -10'
      'ORDER BY NOMEENT')
    Left = 336
    Top = 124
    object QrPVIEmpCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPVIEmpCadFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrPVIEmpCadNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsPVIEmpCad: TDataSource
    DataSet = QrPVIEmpCad
    Left = 404
    Top = 120
  end
  object QrPVICliCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu, Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NOMEENT'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMEENT'
      '')
    Left = 332
    Top = 72
    object QrPVICliCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPVICliCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPVICliCadNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsPVICliCad: TDataSource
    DataSet = QrPVICliCad
    Left = 404
    Top = 72
  end
  object QrPVIRepCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodUsu, Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NOMEENT'
      'FROM entidades'
      'WHERE Fornece4="V"'
      'ORDER BY NOMEENT')
    Left = 332
    Top = 168
    object QrPVIRepCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPVIRepCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPVIRepCadNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsPVIRepCad: TDataSource
    DataSet = QrPVIRepCad
    Left = 404
    Top = 168
  end
  object QrPVIAtrPrdCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM graatrcad'
      'ORDER BY Nome')
    Left = 332
    Top = 216
    object QrPVIAtrPrdCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPVIAtrPrdCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPVIAtrPrdCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPVIAtrPrdCad: TDataSource
    DataSet = QrPVIAtrPrdCad
    Left = 404
    Top = 216
  end
  object QrPVIAtrCliCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM entatrcad'
      'WHERE Cliente1="V"'
      'ORDER BY Nome')
    Left = 332
    Top = 264
    object QrPVIAtrCliCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPVIAtrCliCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPVIAtrCliCadNome: TWideStringField
      DisplayWidth = 100
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPVIAtrCliCad: TDataSource
    DataSet = QrPVIAtrCliCad
    Left = 404
    Top = 264
  end
  object QrAtrPrdSubIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle Codigo, CodUsu, Nome'
      'FROM graatrits'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 688
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAtrPrdSubItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAtrPrdSubItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrAtrPrdSubItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrAtrCliSubIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle Codigo, CodUsu, Nome'
      'FROM entatrits'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 688
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAtrCliSubItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAtrCliSubItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrAtrCliSubItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrPVIAtrRepCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM entatrcad'
      'WHERE Fornece4="V"'
      'ORDER BY Nome')
    Left = 332
    Top = 316
    object QrPVIAtrRepCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPVIAtrRepCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPVIAtrRepCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPVIAtrRepCad: TDataSource
    DataSet = QrPVIAtrRepCad
    Left = 404
    Top = 312
  end
  object QrAtrRepSubIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle Codigo, CodUsu, Nome'
      'FROM entatrits'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 688
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAtrRepSubItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAtrRepSubItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrAtrRepSubItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsFRC_TMov: TDataSource
    DataSet = QrFRC_TMov
    Left = 240
    Top = 108
  end
  object QrFisRegCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.ModeloNF,'
      'frc.Nome, frc.Financeiro, imp.Nome NO_MODELO_NF'
      'FROM fisregcad frc'
      'LEFT JOIN imprime imp ON imp.Codigo=frc.ModeloNF'
      'ORDER BY Nome')
    Left = 20
    Top = 480
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFisRegCadModeloNF: TIntegerField
      FieldName = 'ModeloNF'
      Required = True
    end
    object QrFisRegCadNO_MODELO_NF: TWideStringField
      FieldName = 'NO_MODELO_NF'
      Size = 100
    end
    object QrFisRegCadFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
    object QrFisRegCadTipoMov: TSmallintField
      FieldName = 'TipoMov'
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 92
    Top = 480
  end
  object QrPrazo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tpp.Variacao'
      'FROM tabeprcpzg tpp'
      'WHERE tpp.Codigo=:P0'
      'AND :P1 BETWEEN DiasIni AND DiasFim'
      'ORDER BY Variacao DESC'#10)
    Left = 832
    Top = 20
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPrazoVariacao: TFloatField
      FieldName = 'Variacao'
      EditFormat = '0.00'
    end
  end
  object DsPrazo: TDataSource
    DataSet = QrPrazo
    Left = 900
    Top = 20
  end
  object QrFisco: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ICMS_Usa, ICMS_Alq, IPI_Usa, IPI_Alq,'
      'PIS_Usa, PIS_Alq, COFINS_Usa, COFINS_Alq,'
      'IR_Usa, IR_Alq, CS_Usa, CS_Alq, ISS_Usa, ISS_Alq        '#10
      ''
      'FROM fisregcad '
      'WHERE Codigo=:P0  '
      '')
    Left = 832
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFiscoICMS_Usa: TSmallintField
      FieldName = 'ICMS_Usa'
    end
    object QrFiscoICMS_Alq: TFloatField
      FieldName = 'ICMS_Alq'
    end
    object QrFiscoIPI_Usa: TSmallintField
      FieldName = 'IPI_Usa'
    end
    object QrFiscoIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrFiscoPIS_Usa: TSmallintField
      FieldName = 'PIS_Usa'
    end
    object QrFiscoPIS_Alq: TFloatField
      FieldName = 'PIS_Alq'
    end
    object QrFiscoCOFINS_Usa: TSmallintField
      FieldName = 'COFINS_Usa'
    end
    object QrFiscoCOFINS_Alq: TFloatField
      FieldName = 'COFINS_Alq'
    end
    object QrFiscoIR_Usa: TSmallintField
      FieldName = 'IR_Usa'
    end
    object QrFiscoIR_Alq: TFloatField
      FieldName = 'IR_Alq'
    end
    object QrFiscoCS_Usa: TSmallintField
      FieldName = 'CS_Usa'
    end
    object QrFiscoCS_Alq: TFloatField
      FieldName = 'CS_Alq'
    end
    object QrFiscoISS_Usa: TSmallintField
      FieldName = 'ISS_Usa'
    end
    object QrFiscoISS_Alq: TFloatField
      FieldName = 'ISS_Alq'
    end
  end
  object DsFisco: TDataSource
    DataSet = QrFisco
    Left = 900
    Top = 68
  end
  object QrICMSV: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, UFDest, ICMS_V'
      'FROM ufsicms '
      'WHERE Nome=:P0'#13
      'AND UFDest=:P1'#10
      '')
    Left = 832
    Top = 116
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrICMSVICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '0.00'
    end
    object QrICMSVNome: TWideStringField
      FieldName = 'Nome'
      Size = 2
    end
    object QrICMSVUFDest: TWideStringField
      FieldName = 'UFDest'
      Size = 2
    end
  end
  object DsICMSV: TDataSource
    DataSet = QrICMSV
    Left = 900
    Top = 116
  end
  object QrSumVdaA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smi.Qtde) Qtde'
      'FROM stqmovitsa smi'
      'WHERE smi.Tipo=1'
      'AND smi.OriPart=:P0')
    Left = 328
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumVdaAQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrEhItemPed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM pedivdaits'
      'WHERE Codigo=:P0'
      'AND  GraGruX=:P1')
    Left = 404
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEhItemPedControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSdoRedPed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvi.Controle, (pvi.QuantP-pvi.QuantC-pvi.QuantV) QuantF,'
      'pvi.MedidaC, pvi.MedidaL, pvi.MedidaA, pvi.MedidaE,'
      'med.Medida1, med.Medida2, med.Medida3, med.Medida4'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN medordem med ON med.Codigo=gg1.MedOrdem'
      'WHERE  pvi.Codigo=:P0'
      'AND pvi.GraGruX=:P1')
    Left = 404
    Top = 480
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSdoRedPedQuantF: TFloatField
      FieldName = 'QuantF'
      Required = True
    end
    object QrSdoRedPedControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSdoRedPedMedidaC: TFloatField
      FieldName = 'MedidaC'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrSdoRedPedMedidaL: TFloatField
      FieldName = 'MedidaL'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrSdoRedPedMedidaA: TFloatField
      FieldName = 'MedidaA'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrSdoRedPedMedidaE: TFloatField
      FieldName = 'MedidaE'
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrSdoRedPedMedida1: TWideStringField
      FieldName = 'Medida1'
    end
    object QrSdoRedPedMedida2: TWideStringField
      FieldName = 'Medida2'
    end
    object QrSdoRedPedMedida3: TWideStringField
      FieldName = 'Medida3'
    end
    object QrSdoRedPedMedida4: TWideStringField
      FieldName = 'Medida4'
    end
  end
  object QrPvix: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvi.Controle'
      'FROM pedivdaits pvi'
      'WHERE pvi.Codigo=:P0'
      '')
    Left = 168
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPvixControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsSdoRedPed: TDataSource
    DataSet = QrSdoRedPed
    Left = 476
    Top = 480
  end
  object QrSumVda_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smi.Qtde) Qtde'
      'FROM stqmovitsa smi'
      'LEFT JOIN fatpedcab fpc ON fpc.Codigo=smi.OriCodi and smi.Tipo=1'
      'LEFT JOIN pedivda pva ON pva.Codigo=fpc.Pedido     '
      'LEFT JOIN pedivdaits pvi ON pvi.Codigo=pva.Codigo'
      'WHERE pvi.Controle=:P0'
      '')
    Left = 424
    Top = 536
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object FloatField1: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrPP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT PartePrinc'
      'FROM gragru1'
      'WHERE Nivel1=:P0')
    Left = 168
    Top = 480
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPPPartePrinc: TIntegerField
      FieldName = 'PartePrinc'
    end
  end
  object QrSumCuz: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ValBru) ValBru,'
      'SUM(ValLiq) ValLiq, '
      'SUM(DescoV) DescoV,'
      'SUM(PrecoR* PerCustom)  / SUM(PrecoR) PercCustom'
      ''
      'FROM pedivdacuz'
      'WHERE Controle=:P0')
    Left = 832
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumCuzValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrSumCuzValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrSumCuzDescoV: TFloatField
      FieldName = 'DescoV'
    end
    object QrSumCuzPercCustom: TFloatField
      FieldName = 'PercCustom'
    end
  end
  object QrIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvi.QuantP, ggx.GraGru1'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGruX'
      'WHERE pvi.Controle=:P0'
      '')
    Left = 904
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItsQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
  end
  object QrCustomiz: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.SiglaCustm'
      'FROM pedivdacuz pvc'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvc.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE pvc.Controle=:P0'
      'AND gg1.SiglaCustm <> '#39#39)
    Left = 836
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCustomizSiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Size = 15
    end
  end
  object QrPesInfCuz: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM nfeinfcuz'
      'WHERE Nome=:P0')
    Left = 904
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesInfCuzCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object TbPVICliIts: TMySQLTable
    Database = Dmod.MyDB
    TableName = 'pvicli'
    Left = 476
    Top = 72
    object TbPVICliItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'svpclie.Codigo'
    end
    object TbPVICliItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'svpclie.CodUsu'
    end
    object TbPVICliItsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'svpclie.Nome'
      Size = 100
    end
    object TbPVICliItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'svpclie.Ativo'
      MaxValue = 1
    end
  end
  object QrPVICliAti: TMySQLQuery
    Database = Dmod.MyDB
    RequestLive = True
    SQL.Strings = (
      'SELECT Count(*)  Itens'
      'FROM pvicli'
      'WHERE Ativo=1'
      '')
    Left = 620
    Top = 72
    object QrPVICliAtiItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrFatPedCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fpc.Codigo,fpc.Abertura, fpc.CodUsu,'
      'pvd.Empresa, pvd.Cliente, pvd.Transporta, '
      'pvd.FretePor, pvd.CartEmis, pvd.CondicaoPg, '
      'pvd.AFP_Sit, pvd.AFP_Per, pvd.Represen,'
      'frc.ModeloNF, frc.Financeiro,'
      'IF(frc.TipoMov=1,0,1) tpNF,'
      'car.Tipo TIPOCART,'
      ''
      'par.Associada, ase.Filial ASS_FILIAL,'
      ''
      'par.CtaProdVen EMP_CtaProdVen, '
      'par.FaturaSeq EMP_FaturaSeq,'
      'par.FaturaSep EMP_FaturaSep,'
      'par.FaturaDta EMP_FaturaDta,'
      'par.FaturaNum EMP_FaturaNum,'
      'par.TxtProdVen EMP_TxtProdVen,'
      'par.DupProdVen EMP_DupProdVen,'
      'emp.Filial EMP_FILIAL,'
      ''
      'ass.CtaProdVen ASS_CtaProdVen,'
      'ass.FaturaSeq ASS_FaturaSeq,'
      'ass.FaturaSep ASS_FaturaSep,'
      'ass.FaturaDta ASS_FaturaDta,'
      'ass.FaturaNum ASS_FaturaNum,'
      'ass.TxtProdVen ASS_TxtProdVen,'
      'ass.DupServico ASS_DupProdVen,'
      ''
      'frc.CtbCadMoF, frc.CtbPlaCta'
      ''
      'FROM fatpedcab fpc'
      'LEFT JOIN pedivda pvd ON pvd.Codigo=fpc.Pedido'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      'LEFT JOIN entidades emp ON emp.Codigo=pvd.Empresa'
      'LEFT JOIN paramsemp  par ON par.Codigo=pvd.Empresa'
      'LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada'
      'LEFT JOIN entidades ase ON ase.Codigo=ass.Codigo'
      'LEFT JOIN carteiras  car ON car.Codigo=pvd.CartEmis'
      ''
      'WHERE fpc.Codigo=:P0')
    Left = 20
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatPedCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFatPedCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrFatPedCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrFatPedCabTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrFatPedCabFretePor: TSmallintField
      FieldName = 'FretePor'
    end
    object QrFatPedCabModeloNF: TIntegerField
      FieldName = 'ModeloNF'
    end
    object QrFatPedCabEMP_FILIAL: TIntegerField
      FieldName = 'EMP_FILIAL'
    end
    object QrFatPedCabAFP_Sit: TSmallintField
      FieldName = 'AFP_Sit'
    end
    object QrFatPedCabAFP_Per: TFloatField
      FieldName = 'AFP_Per'
    end
    object QrFatPedCabCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
    object QrFatPedCabAssociada: TIntegerField
      FieldName = 'Associada'
    end
    object QrFatPedCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrFatPedCabAbertura: TDateTimeField
      FieldName = 'Abertura'
      Required = True
    end
    object QrFatPedCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrFatPedCabTIPOCART: TIntegerField
      FieldName = 'TIPOCART'
      Required = True
    end
    object QrFatPedCabRepresen: TIntegerField
      FieldName = 'Represen'
    end
    object QrFatPedCabEMP_CtaProdVen: TIntegerField
      FieldName = 'EMP_CtaProdVen'
    end
    object QrFatPedCabEMP_FaturaSep: TWideStringField
      FieldName = 'EMP_FaturaSep'
      Size = 1
    end
    object QrFatPedCabEMP_FaturaDta: TSmallintField
      FieldName = 'EMP_FaturaDta'
    end
    object QrFatPedCabEMP_FaturaSeq: TSmallintField
      FieldName = 'EMP_FaturaSeq'
    end
    object QrFatPedCabEMP_TxtProdVen: TWideStringField
      FieldName = 'EMP_TxtProdVen'
      Size = 100
    end
    object QrFatPedCabEMP_DupProdVen: TWideStringField
      FieldName = 'EMP_DupProdVen'
      Size = 3
    end
    object QrFatPedCabASS_CtaProdVen: TIntegerField
      FieldName = 'ASS_CtaProdVen'
    end
    object QrFatPedCabASS_FILIAL: TIntegerField
      FieldName = 'ASS_FILIAL'
    end
    object QrFatPedCabASS_FaturaDta: TSmallintField
      FieldName = 'ASS_FaturaDta'
    end
    object QrFatPedCabASS_FaturaSeq: TSmallintField
      FieldName = 'ASS_FaturaSeq'
    end
    object QrFatPedCabASS_FaturaSep: TWideStringField
      FieldName = 'ASS_FaturaSep'
      Size = 1
    end
    object QrFatPedCabASS_TxtProdVen: TWideStringField
      FieldName = 'ASS_TxtProdVen'
      Size = 100
    end
    object QrFatPedCabASS_DupProdVen: TWideStringField
      FieldName = 'ASS_DupProdVen'
      Size = 3
    end
    object QrFatPedCabFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
    object QrFatPedCabtpNF: TLargeintField
      FieldName = 'tpNF'
      Required = True
    end
    object QrFatPedCabEMP_FaturaNum: TSmallintField
      FieldName = 'EMP_FaturaNum'
    end
    object QrFatPedCabASS_FaturaNum: TSmallintField
      FieldName = 'ASS_FaturaNum'
    end
    object QrFatPedCabCtbCadMoF: TIntegerField
      FieldName = 'CtbCadMoF'
    end
    object QrFatPedCabCtbPlaCta: TIntegerField
      FieldName = 'CtbPlaCta'
    end
  end
  object QrSumConCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ValLiq) ValLiq, SUM(QuantP) QuantP,'
      'SUM(QuantC) QuantC, SUM(QuantV) QuantV,'
      'SUM(ValFat) ValFat'
      'FROM fatconits fci'
      'WHERE fci.Codigo=:P0')
    Left = 236
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumConCadValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrSumConCadQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrSumConCadQuantC: TFloatField
      FieldName = 'QuantC'
    end
    object QrSumConCadQuantV: TFloatField
      FieldName = 'QuantV'
    end
    object QrSumConCadValFat: TFloatField
      FieldName = 'ValFat'
    end
  end
  object QrFatConLot: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fcc.Codigo'
      'FROM fatconcad fcc'
      'WHERE fcc.FatConRet=:P0')
    Left = 236
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatConLotCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrFatConRet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DescoGer, ComisPer, ValFat, PercRedu'
      'FROM fatconret'
      'WHERE Codigo=:P0')
    Left = 236
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatConRetDescoGer: TFloatField
      FieldName = 'DescoGer'
    end
    object QrFatConRetComisPer: TFloatField
      FieldName = 'ComisPer'
    end
    object QrFatConRetValFat: TFloatField
      FieldName = 'ValFat'
    end
    object QrFatConRetPercRedu: TFloatField
      FieldName = 'PercRedu'
    end
  end
  object QrSumC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito'
      'FROM lanctos'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      '')
    Left = 236
    Top = 480
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumCCredito: TFloatField
      FieldName = 'Credito'
    end
  end
  object QrFretePor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM lfretepor')
    Left = 172
    Top = 60
  end
  object QrFRC_TMov: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM lfrc_tmov'
      'ORDER BY Nome')
    Left = 172
    Top = 108
  end
  object QrCorrGrade: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CorrGrad01, CorrGrad02'
      'FROM controle')
    Left = 852
    Top = 456
    object QrCorrGradeCorrGrad01: TSmallintField
      FieldName = 'CorrGrad01'
    end
    object QrCorrGradeCorrGrad02: TSmallintField
      FieldName = 'CorrGrad02'
    end
  end
  object QrNFeCtrl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT VerNFeLay'
      'FROM nfectrl')
    Left = 680
    Top = 460
    object QrNFeCtrlVerNFeLay: TIntegerField
      FieldName = 'VerNFeLay'
    end
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 864
    Top = 364
  end
  object QrSumVdaB: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smi.Qtde) Qtde'
      'FROM stqmovitsb smi'
      'WHERE smi.Tipo=1'
      'AND smi.OriPart=:P0')
    Left = 328
    Top = 456
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumVdaBQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrFPC1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvd.CodUsu CU_PediVda, pvd.Empresa, '
      'pvd.TabelaPrc, pvd.CondicaoPG, pvd.RegrFiscal,'
      'pvd.AFP_Sit, pvd.AFP_Per, pvd.Cliente,'
      'pvd.Codigo ID_Pedido, pvd.PedidoCli,'
      'ppc.JurosMes,'
      'frc.Nome NOMEFISREGCAD,'
      'ppc.MedDDReal, ppc.MedDDSimpl,'
      'par.TipMediaDD, par.Associada, par.FatSemEstq,'
      ''
      'par.CtaProdVen EMP_CtaProdVen,'
      'par.FaturaSeq EMP_FaturaSeq,'
      'par.FaturaSep EMP_FaturaSep,'
      'par.FaturaDta EMP_FaturaDta,'
      'par.TxtProdVen EMP_TxtProdVen,'
      'emp.Filial EMP_FILIAL,'
      'ufe.Codigo EMP_UF,'
      ''
      'ass.CtaProdVen ASS_CtaProdVen,'
      'ass.FaturaSeq ASS_FaturaSeq,'
      'ass.FaturaSep ASS_FaturaSep,'
      'ass.FaturaDta ASS_FaturaDta,'
      'ass.TxtProdVen ASS_TxtProdVen,'
      'ufa.Nome ASS_NO_UF,'
      'ufa.Codigo ASS_CO_UF,'
      'ase.Filial ASS_FILIAL,'
      ''
      'tpc.Nome NO_TabelaPrc,'
      '/*frm.TipoCalc,*/ fpc.*'
      'FROM fatpedcab fpc'
      'LEFT JOIN pedivda    pvd ON pvd.Codigo=fpc.Pedido'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc'
      'LEFT JOIN paramsemp  par ON par.Codigo=pvd.Empresa'
      'LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada'
      'LEFT JOIN entidades  ase ON ase.Codigo=ass.Codigo'
      'LEFT JOIN entidades  emp ON emp.Codigo=par.Codigo'
      
        'LEFT JOIN ufs        ufe ON ufe.Codigo=IF(emp.Tipo=0, emp.EUF, e' +
        'mp.PUF)'
      
        'LEFT JOIN ufs        ufa ON ufa.Codigo=IF(ase.Tipo=0, ase.EUF, a' +
        'se.PUF)'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      '/* N'#227'o tem como'
      'LEFT JOIN fisregmvt frm ON frm.Codigo=pvd.RegrFiscal'
      '  AND frm.StqCenCad=1'
      '  AND frm.Empresa=-11'
      '*/'
      'WHERE fpc.Codigo > -1000')
    Left = 88
    Top = 532
    object QrFPC1Empresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'pedivda.Empresa'
    end
    object QrFPC1Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fatpedcab.Codigo'
      Required = True
    end
    object QrFPC1CodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'fatpedcab.CodUsu'
      Required = True
    end
    object QrFPC1Pedido: TIntegerField
      FieldName = 'Pedido'
      Origin = 'fatpedcab.Pedido'
      Required = True
    end
    object QrFPC1CondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
      Origin = 'pedivda.CondicaoPG'
    end
    object QrFPC1TabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Origin = 'pedivda.TabelaPrc'
    end
    object QrFPC1MedDDReal: TFloatField
      FieldName = 'MedDDReal'
      Origin = 'pediprzcab.MedDDReal'
    end
    object QrFPC1MedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
      Origin = 'pediprzcab.MedDDSimpl'
    end
    object QrFPC1RegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Origin = 'pedivda.RegrFiscal'
    end
    object QrFPC1NO_TabelaPrc: TWideStringField
      FieldName = 'NO_TabelaPrc'
      Origin = 'tabeprccab.Nome'
      Required = True
      Size = 50
    end
    object QrFPC1TipMediaDD: TSmallintField
      FieldName = 'TipMediaDD'
      Origin = 'paramsemp.TipMediaDD'
      Required = True
    end
    object QrFPC1AFP_Sit: TSmallintField
      FieldName = 'AFP_Sit'
      Origin = 'pedivda.AFP_Sit'
      Required = True
    end
    object QrFPC1AFP_Per: TFloatField
      FieldName = 'AFP_Per'
      Origin = 'pedivda.AFP_Per'
      Required = True
    end
    object QrFPC1Associada: TIntegerField
      FieldName = 'Associada'
      Origin = 'paramsemp.Associada'
      Required = True
    end
    object QrFPC1CU_PediVda: TIntegerField
      FieldName = 'CU_PediVda'
      Origin = 'pedivda.CodUsu'
      Required = True
    end
    object QrFPC1Abertura: TDateTimeField
      FieldName = 'Abertura'
      Origin = 'fatpedcab.Abertura'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFPC1Encerrou: TDateTimeField
      FieldName = 'Encerrou'
      Origin = 'fatpedcab.Encerrou'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFPC1FatSemEstq: TSmallintField
      FieldName = 'FatSemEstq'
      Origin = 'paramsemp.FatSemEstq'
      Required = True
    end
    object QrFPC1ENCERROU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENCERROU_TXT'
      Size = 30
      Calculated = True
    end
    object QrFPC1EMP_CtaProdVen: TIntegerField
      FieldName = 'EMP_CtaProdVen'
      Origin = 'paramsemp.CtaProdVen'
    end
    object QrFPC1EMP_FaturaSeq: TSmallintField
      FieldName = 'EMP_FaturaSeq'
      Origin = 'paramsemp.FaturaSeq'
    end
    object QrFPC1EMP_FaturaSep: TWideStringField
      FieldName = 'EMP_FaturaSep'
      Origin = 'paramsemp.FaturaSep'
      Size = 1
    end
    object QrFPC1EMP_FaturaDta: TSmallintField
      FieldName = 'EMP_FaturaDta'
      Origin = 'paramsemp.FaturaDta'
    end
    object QrFPC1EMP_TxtProdVen: TWideStringField
      FieldName = 'EMP_TxtProdVen'
      Origin = 'paramsemp.TxtProdVen'
      Size = 100
    end
    object QrFPC1EMP_FILIAL: TIntegerField
      FieldName = 'EMP_FILIAL'
      Origin = 'entidades.Filial'
    end
    object QrFPC1ASS_CtaProdVen: TIntegerField
      FieldName = 'ASS_CtaProdVen'
      Origin = 'paramsemp.CtaProdVen'
    end
    object QrFPC1ASS_FaturaSeq: TSmallintField
      FieldName = 'ASS_FaturaSeq'
      Origin = 'paramsemp.FaturaSeq'
    end
    object QrFPC1ASS_FaturaSep: TWideStringField
      FieldName = 'ASS_FaturaSep'
      Origin = 'paramsemp.FaturaSep'
      Size = 1
    end
    object QrFPC1ASS_FaturaDta: TSmallintField
      FieldName = 'ASS_FaturaDta'
      Origin = 'paramsemp.FaturaDta'
    end
    object QrFPC1ASS_TxtProdVen: TWideStringField
      FieldName = 'ASS_TxtProdVen'
      Origin = 'paramsemp.TxtProdVen'
      Size = 100
    end
    object QrFPC1ASS_NO_UF: TWideStringField
      FieldName = 'ASS_NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrFPC1ASS_CO_UF: TIntegerField
      FieldName = 'ASS_CO_UF'
      Origin = 'ufs.Codigo'
      Required = True
    end
    object QrFPC1ASS_FILIAL: TIntegerField
      FieldName = 'ASS_FILIAL'
      Origin = 'entidades.Filial'
    end
    object QrFPC1Cliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'pedivda.Cliente'
    end
    object QrFPC1SerieDesfe: TIntegerField
      FieldName = 'SerieDesfe'
    end
    object QrFPC1NFDesfeita: TIntegerField
      FieldName = 'NFDesfeita'
    end
    object QrFPC1PEDIDO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PEDIDO_TXT'
      Size = 21
      Calculated = True
    end
    object QrFPC1ID_Pedido: TIntegerField
      FieldName = 'ID_Pedido'
      Required = True
    end
    object QrFPC1JurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrFPC1NOMEFISREGCAD: TWideStringField
      FieldName = 'NOMEFISREGCAD'
      Size = 50
    end
    object QrFPC1EMP_UF: TIntegerField
      FieldName = 'EMP_UF'
      Required = True
    end
    object QrFPC1PedidoCli: TWideStringField
      FieldName = 'PedidoCli'
    end
  end
  object QrVolumes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(fpv.Ativo) Volumes, med.Nome NO_UnidMed'
      'FROM fatpedvol fpv'
      'LEFT JOIN unidmed med ON med.Codigo=fpv.UnidMed'
      'WHERE fpv.Codigo=:P0'
      'AND fpv.Ativo=1'
      'GROUP BY med.Codigo')
    Left = 164
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVolumesVolumes: TLargeintField
      FieldName = 'Volumes'
      Required = True
    end
    object QrVolumesNO_UnidMed: TWideStringField
      FieldName = 'NO_UnidMed'
      Size = 30
    end
  end
  object QrRetiradaEnti: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, R' +
        'espons2, '
      'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, '
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, '
      'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, '
      'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, '
      'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, '
      'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, '
      'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, '
      'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, '
      'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, '
      'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, '
      'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, '
      'mun.Nome CIDADE, '
      'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, '
      'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, '
      'pai.Nome Pais, '
      'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, '
      'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, '
      'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, '
      'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, '
      'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, '
      'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, '
      'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, '
      'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, '
      'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, '
      'RG, SSP, DataRG '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF '
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF '
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd '
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd '
      
        'LEFT JOIN locbdermall.dtb_munici mun ON mun.Codigo = IF(en.Tipo=' +
        '0, en.ECodMunici, en.PCodMunici) '
      
        'LEFT JOIN locbdermall.bacen_pais pai ON pai.Codigo = IF(en.Tipo=' +
        '0, en.ECodiPais, en.PCodiPais) '
      'ORDER BY NOME_ENT')
    Left = 544
    Top = 396
    object QrRetiradaEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRetiradaEntiCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrRetiradaEntiENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrRetiradaEntiPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrRetiradaEntiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrRetiradaEntiRespons1: TWideStringField
      FieldName = 'Respons1'
      Size = 100
    end
    object QrRetiradaEntiRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 100
    end
    object QrRetiradaEntiENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrRetiradaEntiPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrRetiradaEntiELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrRetiradaEntiPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrRetiradaEntiECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrRetiradaEntiPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrRetiradaEntiNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrRetiradaEntiNO_2_ENT: TWideStringField
      FieldName = 'NO_2_ENT'
      Size = 60
    end
    object QrRetiradaEntiCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrRetiradaEntiIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrRetiradaEntiNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 15
    end
    object QrRetiradaEntiRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrRetiradaEntiSITE: TWideStringField
      FieldName = 'SITE'
      Size = 100
    end
    object QrRetiradaEntiCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrRetiradaEntiBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrRetiradaEntiCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 100
    end
    object QrRetiradaEntiNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrRetiradaEntiNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrRetiradaEntiPais: TWideStringField
      FieldName = 'Pais'
      Size = 100
    end
    object QrRetiradaEntiLograd: TSmallintField
      FieldName = 'Lograd'
      Required = True
    end
    object QrRetiradaEntiTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrRetiradaEntiFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrRetiradaEntiEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrRetiradaEntiTRATO: TWideStringField
      FieldName = 'TRATO'
      Size = 10
    end
    object QrRetiradaEntiENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrRetiradaEntiCODMUNICI: TFloatField
      FieldName = 'CODMUNICI'
      Required = True
    end
    object QrRetiradaEntiCODPAIS: TFloatField
      FieldName = 'CODPAIS'
      Required = True
    end
    object QrRetiradaEntiRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrRetiradaEntiSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrRetiradaEntiDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrRetiradaEntiIE: TWideStringField
      FieldName = 'IE'
    end
    object QrRetiradaEntiNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrRetiradaEntiCEP: TFloatField
      FieldName = 'CEP'
    end
  end
  object DsRetiradaEnti: TDataSource
    DataSet = QrRetiradaEnti
    Left = 624
    Top = 396
  end
  object QrEntregaEnti: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, R' +
        'espons2, '
      'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, '
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, '
      'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, '
      'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, '
      'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, '
      'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, '
      'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, '
      'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, '
      'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, '
      'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, '
      'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, '
      'mun.Nome CIDADE, '
      'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, '
      'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, '
      'pai.Nome Pais, '
      'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, '
      'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, '
      'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, '
      'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, '
      'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, '
      'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, '
      'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, '
      'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, '
      'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, '
      'RG, SSP, DataRG '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF '
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF '
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd '
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd '
      
        'LEFT JOIN locbdermall.dtb_munici mun ON mun.Codigo = IF(en.Tipo=' +
        '0, en.ECodMunici, en.PCodMunici) '
      
        'LEFT JOIN locbdermall.bacen_pais pai ON pai.Codigo = IF(en.Tipo=' +
        '0, en.ECodiPais, en.PCodiPais) '
      'ORDER BY NOME_ENT')
    Left = 544
    Top = 448
    object QrEntregaEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntregaEntiCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEntregaEntiENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEntregaEntiPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrEntregaEntiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntregaEntiRespons1: TWideStringField
      FieldName = 'Respons1'
      Size = 100
    end
    object QrEntregaEntiRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 100
    end
    object QrEntregaEntiENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEntregaEntiPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrEntregaEntiELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrEntregaEntiPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrEntregaEntiECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEntregaEntiPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrEntregaEntiNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrEntregaEntiNO_2_ENT: TWideStringField
      FieldName = 'NO_2_ENT'
      Size = 60
    end
    object QrEntregaEntiCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntregaEntiIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEntregaEntiNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 15
    end
    object QrEntregaEntiRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrEntregaEntiSITE: TWideStringField
      FieldName = 'SITE'
      Size = 100
    end
    object QrEntregaEntiCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrEntregaEntiBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrEntregaEntiCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 100
    end
    object QrEntregaEntiNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrEntregaEntiNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEntregaEntiPais: TWideStringField
      FieldName = 'Pais'
      Size = 100
    end
    object QrEntregaEntiLograd: TSmallintField
      FieldName = 'Lograd'
      Required = True
    end
    object QrEntregaEntiTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntregaEntiFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEntregaEntiEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrEntregaEntiTRATO: TWideStringField
      FieldName = 'TRATO'
      Size = 10
    end
    object QrEntregaEntiENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrEntregaEntiCODMUNICI: TFloatField
      FieldName = 'CODMUNICI'
      Required = True
    end
    object QrEntregaEntiCODPAIS: TFloatField
      FieldName = 'CODPAIS'
      Required = True
    end
    object QrEntregaEntiRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrEntregaEntiSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrEntregaEntiDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrEntregaEntiIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntregaEntiNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrEntregaEntiCEP: TFloatField
      FieldName = 'CEP'
    end
  end
  object DsEntregaEnti: TDataSource
    DataSet = QrEntregaEnti
    Left = 624
    Top = 448
  end
  object QrInfIntermedEnti: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'#13
      'ufs.Nome NOMEUF'
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Fornece2="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 24
    Top = 156
    object QrInfIntermedEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrInfIntermedEntiNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrInfIntermedEntiCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrInfIntermedEntiNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
  end
  object DsInfIntermedEnti: TDataSource
    DataSet = QrInfIntermedEnti
    Left = 96
    Top = 156
  end
  object QrPVIPrdIts: TABSQuery
    CurrentVersion = '7.93 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    RequestLive = True
    Left = 476
    Top = 24
    object QrPVIPrdItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPVIPrdItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      DisplayFormat = '00000000'
    end
    object QrPVIPrdItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPVIPrdItsAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsPVIPrdIts: TDataSource
    Left = 552
    Top = 24
  end
  object QrSumVdaUC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(smi.Qtde) Qtde'
      'FROM stqmovitsa smi'
      'WHERE smi.Tipo=1'
      'AND smi.OriPart=:P0')
    Left = 324
    Top = 512
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumVdaUCQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrAux: TMySQLQuery
    Database = Dmod.MyDB
    Left = 692
    Top = 556
  end
  object QrPQFor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IQ, Relevancia  '
      'FROM pqfor '
      'WHERE PQ=:P0')
    Left = 952
    Top = 420
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQForIQ: TIntegerField
      FieldName = 'IQ'
      Required = True
    end
    object QrPQForRelevancia: TSmallintField
      FieldName = 'Relevancia'
      Required = True
    end
  end
  object QrItemSCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM solicomprits'
      'WHERE Codigo=:P0'
      'AND GraGruX=:P1'
      '')
    Left = 952
    Top = 476
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrItemSCICodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrItemSCIControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrItemSCIGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrItemSCIPrecoO: TFloatField
      FieldName = 'PrecoO'
      Required = True
    end
    object QrItemSCIPrecoR: TFloatField
      FieldName = 'PrecoR'
      Required = True
    end
    object QrItemSCIQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrItemSCIQuantC: TFloatField
      FieldName = 'QuantC'
      Required = True
    end
    object QrItemSCIQuantV: TFloatField
      FieldName = 'QuantV'
      Required = True
    end
    object QrItemSCIValBru: TFloatField
      FieldName = 'ValBru'
      Required = True
    end
    object QrItemSCIDescoP: TFloatField
      FieldName = 'DescoP'
      Required = True
    end
    object QrItemSCIDescoV: TFloatField
      FieldName = 'DescoV'
      Required = True
    end
    object QrItemSCIValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrItemSCIPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrItemSCIMedidaC: TFloatField
      FieldName = 'MedidaC'
      Required = True
    end
    object QrItemSCIMedidaL: TFloatField
      FieldName = 'MedidaL'
      Required = True
    end
    object QrItemSCIMedidaA: TFloatField
      FieldName = 'MedidaA'
      Required = True
    end
    object QrItemSCIMedidaE: TFloatField
      FieldName = 'MedidaE'
      Required = True
    end
    object QrItemSCIPercCustom: TFloatField
      FieldName = 'PercCustom'
      Required = True
    end
    object QrItemSCICustomizad: TSmallintField
      FieldName = 'Customizad'
      Required = True
    end
    object QrItemSCIInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Required = True
    end
    object QrItemSCIOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrItemSCIReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 11
    end
    object QrItemSCIMulti: TIntegerField
      FieldName = 'Multi'
      Required = True
    end
    object QrItemSCIvProd: TFloatField
      FieldName = 'vProd'
      Required = True
    end
    object QrItemSCIvFrete: TFloatField
      FieldName = 'vFrete'
      Required = True
    end
    object QrItemSCIvSeg: TFloatField
      FieldName = 'vSeg'
      Required = True
    end
    object QrItemSCIvOutro: TFloatField
      FieldName = 'vOutro'
      Required = True
    end
    object QrItemSCIvDesc: TFloatField
      FieldName = 'vDesc'
      Required = True
    end
    object QrItemSCIvBC: TFloatField
      FieldName = 'vBC'
      Required = True
    end
    object QrItemSCIQuantS: TFloatField
      FieldName = 'QuantS'
      Required = True
    end
  end
  object QrSCIx: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sci.Controle'
      'FROM solicomprits sci'
      'WHERE sci.Codigo=:P0'
      '')
    Left = 952
    Top = 528
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCIxControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
end
