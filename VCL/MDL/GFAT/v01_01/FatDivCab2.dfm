object FmFatDivCab2: TFmFatDivCab2
  Left = 339
  Top = 185
  Caption = 'NFe-GERAL-002 :: Emiss'#227'o de NF-e'
  ClientHeight = 733
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 685
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBAvisos1: TGroupBox
      Left = 0
      Top = 577
      Width = 1008
      Height = 44
      Align = alBottom
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 27
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 96
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 8
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label2: TLabel
        Left = 8
        Top = 44
        Width = 59
        Height = 13
        Caption = 'Destinat'#225'rio:'
      end
      object SpeedButton5: TSpeedButton
        Left = 605
        Top = 40
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label23: TLabel
        Left = 8
        Top = 72
        Width = 95
        Height = 13
        Caption = 'Situa'#231#227'o do pedido:'
        Enabled = False
      end
      object Label26: TLabel
        Left = 280
        Top = 72
        Width = 93
        Height = 13
        Caption = 'Motivo da situa'#231#227'o:'
        Enabled = False
      end
      object Label7: TLabel
        Left = 920
        Top = 72
        Width = 14
        Height = 13
        Caption = 'ID:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 736
        Top = 72
        Width = 68
        Height = 13
        Caption = 'Data inclus'#227'o:'
        Enabled = False
      end
      object SpeedButton7: TSpeedButton
        Left = 712
        Top = 67
        Width = 21
        Height = 21
        Caption = '...'
        Enabled = False
      end
      object Label1: TLabel
        Left = 856
        Top = 20
        Width = 80
        Height = 13
        Caption = 'C'#243'digo Faturam.:'
      end
      object SbDestEnt: TSpeedButton
        Left = 626
        Top = 40
        Width = 20
        Height = 21
        Caption = '!'
        OnClick = SbDestEntClick
      end
      object EdCliente: TdmkEditCB
        Left = 73
        Top = 40
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        OnExit = EdClienteExit
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdEmpresa: TdmkEditCB
        Left = 73
        Top = 16
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 127
        Top = 16
        Width = 725
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 127
        Top = 40
        Width = 475
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        TabOrder = 4
        OnExit = CBClienteExit
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object DBEdCidade: TDBEdit
        Left = 648
        Top = 40
        Width = 313
        Height = 21
        DataField = 'CIDADE'
        Enabled = False
        TabOrder = 5
      end
      object DBEdNOMEUF: TDBEdit
        Left = 964
        Top = 40
        Width = 32
        Height = 21
        DataField = 'NOMEUF'
        Enabled = False
        TabOrder = 6
      end
      object EdSituacao: TdmkEditCB
        Left = 112
        Top = 67
        Width = 21
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Situacao'
        UpdCampo = 'Situacao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSituacao
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSituacao: TdmkDBLookupComboBox
        Left = 136
        Top = 67
        Width = 141
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'Nome'
        TabOrder = 8
        dmkEditCB = EdSituacao
        QryCampo = 'Situacao'
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdMotivoSit: TdmkEditCB
        Left = 380
        Top = 67
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMotivoSit
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMotivoSit: TdmkDBLookupComboBox
        Left = 440
        Top = 67
        Width = 269
        Height = 21
        Enabled = False
        KeyField = 'CodUsu'
        ListField = 'Nome'
        TabOrder = 10
        dmkEditCB = EdMotivoSit
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCodigo: TdmkEdit
        Left = 940
        Top = 68
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDtaInclu: TdmkEditDateTimePicker
        Left = 808
        Top = 68
        Width = 105
        Height = 21
        Date = 39789.000000000000000000
        Time = 0.688972615738748600
        Enabled = False
        TabOrder = 11
        TabStop = False
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtaInclu'
        UpdCampo = 'DtaInclu'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdCodUsu_Fat: TdmkEdit
        Left = 940
        Top = 13
        Width = 56
        Height = 24
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object PageControl2: TPageControl
      Left = 0
      Top = 96
      Width = 1008
      Height = 453
      ActivePage = TabSheet3
      Align = alTop
      TabOrder = 1
      object TabSheet3: TTabSheet
        Caption = ' Dados do Gerais do faturamento: '
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 425
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 36
            Align = alTop
            TabOrder = 0
            object Label12: TLabel
              Left = 8
              Top = 12
              Width = 67
              Height = 13
              Caption = 'Data emiss'#227'o:'
              Enabled = False
            end
            object Label13: TLabel
              Left = 188
              Top = 12
              Width = 71
              Height = 13
              Caption = 'Data chegada:'
              Enabled = False
            end
            object Label15: TLabel
              Left = 636
              Top = 12
              Width = 50
              Height = 13
              Caption = 'Prioridade:'
              Enabled = False
            end
            object Label14: TLabel
              Left = 752
              Top = 12
              Width = 101
              Height = 13
              Caption = 'Previs'#227'o entrega:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label27: TLabel
              Left = 392
              Top = 12
              Width = 70
              Height = 13
              Caption = 'Pedido cliente:'
            end
            object TPDtaEmiss: TdmkEditDateTimePicker
              Left = 80
              Top = 8
              Width = 105
              Height = 21
              Date = 39789.000000000000000000
              Time = 0.688972615738748600
              Enabled = False
              TabOrder = 0
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaEmiss'
              UpdCampo = 'DtaEmiss'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object TPDtaEntra: TdmkEditDateTimePicker
              Left = 264
              Top = 8
              Width = 105
              Height = 21
              Date = 39789.000000000000000000
              Time = 0.688972615738748600
              Enabled = False
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaEntra'
              UpdCampo = 'DtaEntra'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdPrioridade: TdmkEdit
              Left = 688
              Top = 8
              Width = 53
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00'
              QryCampo = 'Prioridade'
              UpdCampo = 'Prioridade'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object TPDtaPrevi: TdmkEditDateTimePicker
              Left = 856
              Top = 8
              Width = 129
              Height = 21
              Date = 39789.000000000000000000
              Time = 0.688972615738748600
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 4
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaPrevi'
              UpdCampo = 'DtaPrevi'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdPedidoCli: TdmkEdit
              Left = 468
              Top = 8
              Width = 145
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'PedidoCli'
              UpdCampo = 'PedidoCli'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 36
            Width = 1000
            Height = 60
            Align = alTop
            TabOrder = 1
            object BtTabelaPrc: TSpeedButton
              Left = 684
              Top = 8
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = BtTabelaPrcClick
            end
            object LaTabelaPrc: TLabel
              Left = 8
              Top = 13
              Width = 81
              Height = 13
              Caption = 'Tabela de pre'#231'o:'
            end
            object SpeedButton9: TSpeedButton
              Left = 964
              Top = 8
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton9Click
            end
            object Label22: TLabel
              Left = 714
              Top = 13
              Width = 36
              Height = 13
              Caption = 'Moeda:'
            end
            object BtCondicaoPG: TSpeedButton
              Left = 504
              Top = 32
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = BtCondicaoPGClick
            end
            object Label24: TLabel
              Left = 530
              Top = 37
              Width = 39
              Height = 13
              Caption = 'Carteira:'
            end
            object LaCondicaoPG: TLabel
              Left = 8
              Top = 37
              Width = 119
              Height = 13
              Caption = 'Condi'#231#227'o de pagamento:'
            end
            object SpeedButton13: TSpeedButton
              Left = 964
              Top = 32
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton13Click
            end
            object CBTabelaPrc: TdmkDBLookupComboBox
              Left = 188
              Top = 8
              Width = 493
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              TabOrder = 1
              dmkEditCB = EdTabelaPrc
              QryCampo = 'TabelaPrc'
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdTabelaPrc: TdmkEditCB
              Left = 128
              Top = 8
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdTabelaPrcChange
              DBLookupComboBox = CBTabelaPrc
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCartEmis: TdmkDBLookupComboBox
              Left = 620
              Top = 32
              Width = 341
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              TabOrder = 7
              dmkEditCB = EdCartEmis
              QryCampo = 'CartEmis'
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object CBMoeda: TdmkDBLookupComboBox
              Left = 804
              Top = 8
              Width = 157
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              TabOrder = 3
              dmkEditCB = EdMoeda
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdMoeda: TdmkEditCB
              Left = 758
              Top = 8
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBMoeda
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdCartEmis: TdmkEditCB
              Left = 574
              Top = 32
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'CartEmis'
              UpdCampo = 'CartEmis'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCartEmis
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCondicaoPG: TdmkDBLookupComboBox
              Left = 184
              Top = 32
              Width = 313
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              TabOrder = 5
              dmkEditCB = EdCondicaoPG
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdCondicaoPG: TdmkEditCB
              Left = 128
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdCondicaoPGChange
              DBLookupComboBox = CBCondicaoPG
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
          end
          object GroupBox4: TGroupBox
            Left = 0
            Top = 96
            Width = 1000
            Height = 36
            Align = alTop
            TabOrder = 2
            object Label28: TLabel
              Left = 8
              Top = 12
              Width = 87
              Height = 13
              Caption = 'Lote de produ'#231#227'o:'
              FocusControl = DBEdit15
            end
            object Label8: TLabel
              Left = 796
              Top = 12
              Width = 110
              Height = 13
              Caption = 'CodUsu do Pedido >>>'
              Enabled = False
              Visible = False
            end
            object Label3: TLabel
              Left = 592
              Top = 12
              Width = 105
              Height = 13
              Caption = 'C'#243'digo do Pedido >>>'
              Enabled = False
              Visible = False
            end
            object DBEdit15: TDBEdit
              Left = 100
              Top = 8
              Width = 92
              Height = 21
              TabStop = False
              Color = clGradientInactiveCaption
              DataField = 'LoteProd'
              TabOrder = 0
            end
            object EdCodUsu_Ped: TdmkEdit
              Left = 913
              Top = 8
              Width = 80
              Height = 25
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000000'
              QryCampo = 'CodUsu'
              UpdCampo = 'CodUsu'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdCodigo_Ped: TdmkEdit
              Left = 708
              Top = 7
              Width = 80
              Height = 25
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000000'
              QryCampo = 'Codigo'
              UpdCampo = 'Codigo'
              UpdType = utIdx
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object CkUsaReferen: TdmkCheckBox
              Left = 216
              Top = 12
              Width = 205
              Height = 17
              Caption = 'Inclus'#227'o de itens pela sua refer'#234'ncia.'
              TabOrder = 3
              QryCampo = 'UsaReferen'
              UpdCampo = 'UsaReferen'
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
          end
          object GroupBox5: TGroupBox
            Left = 0
            Top = 132
            Width = 1000
            Height = 106
            Align = alTop
            TabOrder = 3
            object Label29: TLabel
              Left = 8
              Top = 13
              Width = 45
              Height = 13
              Caption = 'Frete por:'
            end
            object Label30: TLabel
              Left = 8
              Top = 36
              Width = 75
              Height = 13
              Caption = 'Transportadora:'
            end
            object Label31: TLabel
              Left = 8
              Top = 61
              Width = 64
              Height = 13
              Caption = 'Redespacho:'
            end
            object SpeedButton6: TSpeedButton
              Left = 524
              Top = 32
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton6Click
            end
            object SpeedButton11: TSpeedButton
              Left = 524
              Top = 56
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton11Click
            end
            object Label20: TLabel
              Left = 456
              Top = 13
              Width = 92
              Height = 13
              Caption = 'Local de entrega: >'
              Enabled = False
            end
            object Label32: TLabel
              Left = 176
              Top = 84
              Width = 115
              Height = 13
              Caption = '$ Despesas acess'#243'rias: '
              Enabled = False
            end
            object Label41: TLabel
              Left = 552
              Top = 84
              Width = 36
              Height = 13
              Caption = '$ Frete:'
              Enabled = False
            end
            object Label42: TLabel
              Left = 736
              Top = 84
              Width = 48
              Height = 13
              Caption = '% Seguro:'
              Enabled = False
            end
            object Label43: TLabel
              Left = 8
              Top = 84
              Width = 117
              Height = 13
              Caption = '% Despesas acess'#243'rias: '
              Enabled = False
            end
            object Label44: TLabel
              Left = 452
              Top = 84
              Width = 38
              Height = 13
              Caption = '% Frete:'
              Enabled = False
            end
            object Label45: TLabel
              Left = 844
              Top = 84
              Width = 46
              Height = 13
              Caption = '$ Seguro:'
              Enabled = False
            end
            object EdFretePor: TdmkEditCB
              Left = 56
              Top = 8
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FretePor'
              UpdCampo = 'FretePor'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBFretePor
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBFretePor: TdmkDBLookupComboBox
              Left = 80
              Top = 8
              Width = 369
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              TabOrder = 1
              dmkEditCB = EdFretePor
              QryCampo = 'FretePor'
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdTransporta: TdmkEditCB
              Left = 88
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Transporta'
              UpdCampo = 'Transporta'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBTransporta
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBTransporta: TdmkDBLookupComboBox
              Left = 148
              Top = 32
              Width = 373
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENT'
              TabOrder = 3
              dmkEditCB = EdTransporta
              QryCampo = 'Transporta'
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdRedespacho: TdmkEditCB
              Left = 88
              Top = 56
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Redespacho'
              UpdCampo = 'Redespacho'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBRedespacho
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBRedespacho: TdmkDBLookupComboBox
              Left = 148
              Top = 56
              Width = 373
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENT'
              TabOrder = 5
              dmkEditCB = EdRedespacho
              QryCampo = 'Redespacho'
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object MeEnderecoEntrega1: TMemo
              Left = 556
              Top = 8
              Width = 433
              Height = 69
              TabStop = False
              ReadOnly = True
              TabOrder = 6
            end
            object EdDesoAces_V: TdmkEdit
              Left = 300
              Top = 80
              Width = 80
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 8
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'DesoAces_V'
              UpdCampo = 'DesoAces_V'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdFrete_V: TdmkEdit
              Left = 600
              Top = 80
              Width = 80
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 10
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Frete_V'
              UpdCampo = 'Frete_V'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdSeguro_P: TdmkEdit
              Left = 788
              Top = 80
              Width = 48
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 11
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Seguro_P'
              UpdCampo = 'Seguro_P'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdSeguro_PChange
            end
            object EdDesoAces_P: TdmkEdit
              Left = 124
              Top = 80
              Width = 48
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 7
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'DesoAces_P'
              UpdCampo = 'DesoAces_P'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdDesoAces_PChange
            end
            object EdFrete_P: TdmkEdit
              Left = 496
              Top = 80
              Width = 48
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 9
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Frete_P'
              UpdCampo = 'Frete_P'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdFrete_PChange
            end
            object EdSeguro_V: TdmkEdit
              Left = 904
              Top = 80
              Width = 80
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 12
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Seguro_V'
              UpdCampo = 'Seguro_V'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object GBAgenteVenda: TGroupBox
            Left = 0
            Top = 238
            Width = 1000
            Height = 40
            Align = alTop
            TabOrder = 4
            object Label46: TLabel
              Left = 12
              Top = 16
              Width = 70
              Height = 13
              Caption = 'Agente venda:'
            end
            object SpeedButton12: TSpeedButton
              Left = 616
              Top = 12
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton12Click
            end
            object Label47: TLabel
              Left = 640
              Top = 15
              Width = 117
              Height = 13
              Caption = '% comiss'#227'o faturamento:'
              Enabled = False
            end
            object Label48: TLabel
              Left = 816
              Top = 15
              Width = 119
              Height = 13
              Caption = '% comiss'#227'o recebimento:'
              Enabled = False
            end
            object EdRepresen: TdmkEditCB
              Left = 88
              Top = 12
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Represen'
              UpdCampo = 'Represen'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdRepresenChange
              DBLookupComboBox = CBRepresen
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBRepresen: TdmkDBLookupComboBox
              Left = 148
              Top = 12
              Width = 465
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEACC'
              TabOrder = 1
              dmkEditCB = EdRepresen
              QryCampo = 'Represen'
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdComisFat: TdmkEdit
              Left = 760
              Top = 11
              Width = 48
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'ComisFat'
              UpdCampo = 'ComisFat'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdComisRec: TdmkEdit
              Left = 936
              Top = 11
              Width = 48
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'ComisRec'
              UpdCampo = 'ComisRec'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object GroupBox6: TGroupBox
            Left = 0
            Top = 278
            Width = 1000
            Height = 143
            Align = alTop
            Caption = ' Fiscal:'
            TabOrder = 5
            object Label25: TLabel
              Left = 12
              Top = 20
              Width = 73
              Height = 13
              Caption = 'Movimenta'#231#227'o:'
            end
            object SbRegrFiscal: TSpeedButton
              Left = 616
              Top = 16
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbRegrFiscalClick
            end
            object Label33: TLabel
              Left = 640
              Top = 20
              Width = 55
              Height = 13
              Caption = 'Modelo NF:'
              Enabled = False
            end
            object Label58: TLabel
              Left = 11
              Top = 89
              Width = 518
              Height = 13
              Caption = 
                'Indicador de presen'#231'a do comprador no estabelecimento comercial ' +
                'no momento da opera'#231#227'o (NFe 3.10): [F4]'
            end
            object Label208: TLabel
              Left = 624
              Top = 88
              Width = 134
              Height = 13
              Caption = 'Finalidade emis'#227'o NF-e: [F4]'
              FocusControl = Edide_finNFe
            end
            object EdRegrFiscal: TdmkEditCB
              Left = 88
              Top = 16
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdRegrFiscalChange
              OnExit = EdRegrFiscalExit
              DBLookupComboBox = CBRegrFiscal
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBRegrFiscal: TdmkDBLookupComboBox
              Left = 148
              Top = 16
              Width = 465
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              TabOrder = 1
              dmkEditCB = EdRegrFiscal
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdModeloNF: TdmkEdit
              Left = 696
              Top = 16
              Width = 287
              Height = 21
              Enabled = False
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object RG_idDest: TdmkRadioGroup
              Left = 9
              Top = 43
              Width = 689
              Height = 39
              Caption = ' Local de Destino da Opera'#231#227'o:'
              Columns = 4
              ItemIndex = 0
              Items.Strings = (
                '0 - N'#227'o definido'
                '1 - Opera'#231#227'o interna'
                '2 - Opera'#231#227'o interestadual'
                '3 - Opera'#231#227'o com exterior')
              TabOrder = 3
              QryCampo = 'idDest'
              UpdCampo = 'idDest'
              UpdType = utYes
              OldValor = 0
            end
            object RG_indFinal: TdmkRadioGroup
              Left = 704
              Top = 42
              Width = 280
              Height = 39
              Caption = ' Opera'#231#227'o com consumidor: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                '0 - Normal'
                '1 - Consumidor final')
              TabOrder = 4
              QryCampo = 'indFinal'
              UpdCampo = 'indFinal'
              UpdType = utYes
              OldValor = 0
            end
            object EdindPres: TdmkEdit
              Left = 12
              Top = 104
              Width = 32
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'indPres'
              UpdCampo = 'indPres'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdindPresChange
              OnKeyDown = EdindPresKeyDown
            end
            object EdindPres_TXT: TEdit
              Left = 45
              Top = 104
              Width = 576
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 6
            end
            object RGIndSinc: TdmkRadioGroup
              Left = 780
              Top = 88
              Width = 204
              Height = 39
              Caption = ' Envio da NF-e ao Fisco:'
              Columns = 2
              Enabled = False
              ItemIndex = 1
              Items.Strings = (
                'Ass'#237'ncrono'
                'S'#237'ncrono')
              TabOrder = 9
              QryCampo = 'IndSinc'
              UpdCampo = 'IndSinc'
              UpdType = utYes
              OldValor = 0
            end
            object Edide_finNFe_TXT: TdmkEdit
              Left = 645
              Top = 104
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object Edide_finNFe: TdmkEdit
              Left = 624
              Top = 104
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '4'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'finNFe'
              UpdCampo = 'finNFe'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = Edide_finNFeChange
              OnKeyDown = Edide_finNFeKeyDown
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Local de entrega diferente'
        ImageIndex = 2
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 29
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object CkEntregaUsa: TdmkCheckBox
            Left = 8
            Top = 4
            Width = 185
            Height = 17
            Caption = 'Definir local de entrega diferente:'
            TabOrder = 0
            OnClick = CkEntregaUsaClick
            QryCampo = 'EntregaUsa'
            UpdCampo = 'EntregaUsa'
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
        end
        object PnEntregaEntiAll: TPanel
          Left = 0
          Top = 29
          Width = 1000
          Height = 360
          Align = alTop
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object PnEntregaEntiSel: TPanel
            Left = 1
            Top = 1
            Width = 998
            Height = 28
            Align = alTop
            Caption = 'PnRetiradaEntiSel'
            TabOrder = 0
            object Label19: TLabel
              Left = 8
              Top = 8
              Width = 45
              Height = 13
              Caption = 'Entidade:'
            end
            object SbEntregaEnti: TSpeedButton
              Left = 605
              Top = 4
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbEntregaEntiClick
            end
            object EdEntregaEnti: TdmkEditCB
              Left = 73
              Top = 4
              Width = 52
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'EntregaEnti'
              UpdCampo = 'EntregaEnti'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdEntregaEntiRedefinido
              DBLookupComboBox = CBEntregaEnti
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBEntregaEnti: TdmkDBLookupComboBox
              Left = 127
              Top = 4
              Width = 475
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOME_ENT'
              TabOrder = 1
              dmkEditCB = EdEntregaEnti
              QryCampo = 'EntregaEnti'
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
          object PnEntregaEntiViw: TPanel
            Left = 1
            Top = 29
            Width = 998
            Height = 330
            Align = alClient
            TabOrder = 1
            Visible = False
            object Label37: TLabel
              Left = 8
              Top = 4
              Width = 61
              Height = 13
              Caption = 'CNPJ / CPF:'
              FocusControl = DBEdit18
            end
            object Label50: TLabel
              Left = 248
              Top = 4
              Width = 75
              Height = 13
              Caption = 'Nome entidade:'
              FocusControl = DBEdit19
            end
            object Label51: TLabel
              Left = 8
              Top = 44
              Width = 57
              Height = 13
              Caption = 'Logradouro:'
              FocusControl = DBEdit20
            end
            object Label52: TLabel
              Left = 212
              Top = 44
              Width = 23
              Height = 13
              Caption = 'Rua:'
              FocusControl = DBEdit22
            end
            object Label53: TLabel
              Left = 616
              Top = 44
              Width = 40
              Height = 13
              Caption = 'N'#250'mero:'
              FocusControl = DBEdit23
            end
            object Label54: TLabel
              Left = 752
              Top = 44
              Width = 67
              Height = 13
              Caption = 'Complemento:'
              FocusControl = DBEdit24
            end
            object Label55: TLabel
              Left = 8
              Top = 84
              Width = 30
              Height = 13
              Caption = 'Bairro:'
              FocusControl = DBEdit25
            end
            object Label56: TLabel
              Left = 252
              Top = 84
              Width = 50
              Height = 13
              Caption = 'Munic'#237'pio:'
              FocusControl = DBEdit26
            end
            object Label57: TLabel
              Left = 652
              Top = 84
              Width = 17
              Height = 13
              Caption = 'UF:'
              FocusControl = DBEdit28
            end
            object Label59: TLabel
              Left = 684
              Top = 84
              Width = 25
              Height = 13
              Caption = 'Pa'#237's:'
              FocusControl = DBEdit29
            end
            object Label60: TLabel
              Left = 8
              Top = 128
              Width = 102
              Height = 13
              Caption = 'Telefone principal (1):'
              FocusControl = DBEdit31
            end
            object Label61: TLabel
              Left = 276
              Top = 128
              Width = 28
              Height = 13
              Caption = 'Email:'
              FocusControl = DBEdit32
            end
            object Label62: TLabel
              Left = 728
              Top = 128
              Width = 87
              Height = 13
              Caption = 'Inscr'#231#227'o estadual:'
              FocusControl = DBEdit33
            end
            object DBEdit18: TDBEdit
              Left = 8
              Top = 20
              Width = 238
              Height = 21
              DataField = 'CNPJ_CPF'
              TabOrder = 0
            end
            object DBEdit19: TDBEdit
              Left = 248
              Top = 20
              Width = 741
              Height = 21
              DataField = 'NOME_ENT'
              TabOrder = 1
            end
            object DBEdit20: TDBEdit
              Left = 8
              Top = 60
              Width = 37
              Height = 21
              DataField = 'Lograd'
              TabOrder = 2
            end
            object DBEdit21: TDBEdit
              Left = 48
              Top = 60
              Width = 161
              Height = 21
              DataField = 'NOMELOGRAD'
              TabOrder = 3
            end
            object DBEdit22: TDBEdit
              Left = 212
              Top = 60
              Width = 400
              Height = 21
              DataField = 'RUA'
              TabOrder = 4
            end
            object DBEdit23: TDBEdit
              Left = 616
              Top = 60
              Width = 134
              Height = 21
              DataField = 'NUMERO'
              TabOrder = 5
            end
            object DBEdit24: TDBEdit
              Left = 752
              Top = 60
              Width = 237
              Height = 21
              DataField = 'COMPL'
              TabOrder = 6
            end
            object DBEdit25: TDBEdit
              Left = 8
              Top = 100
              Width = 241
              Height = 21
              DataField = 'BAIRRO'
              TabOrder = 7
            end
            object DBEdit26: TDBEdit
              Left = 252
              Top = 100
              Width = 134
              Height = 21
              DataField = 'CODMUNICI'
              TabOrder = 8
            end
            object DBEdit27: TDBEdit
              Left = 390
              Top = 100
              Width = 259
              Height = 21
              DataField = 'CIDADE'
              TabOrder = 9
            end
            object DBEdit28: TDBEdit
              Left = 652
              Top = 100
              Width = 30
              Height = 21
              DataField = 'NOMEUF'
              TabOrder = 10
            end
            object DBEdit29: TDBEdit
              Left = 684
              Top = 100
              Width = 61
              Height = 21
              DataField = 'CODPAIS'
              TabOrder = 11
            end
            object DBEdit30: TDBEdit
              Left = 748
              Top = 100
              Width = 240
              Height = 21
              DataField = 'Pais'
              TabOrder = 12
            end
            object DBEdit31: TDBEdit
              Left = 8
              Top = 144
              Width = 264
              Height = 21
              DataField = 'TE1'
              TabOrder = 13
            end
            object DBEdit32: TDBEdit
              Left = 276
              Top = 144
              Width = 449
              Height = 21
              DataField = 'EMAIL'
              TabOrder = 14
            end
            object DBEdit33: TDBEdit
              Left = 728
              Top = 144
              Width = 264
              Height = 21
              DataField = 'IE'
              TabOrder = 15
            end
          end
        end
      end
      object TabSheet1: TTabSheet
        Caption = 'Local de retirada diferente'
        ImageIndex = 2
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 425
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitLeft = 160
          ExplicitTop = -48
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 29
            Align = alTop
            ParentBackground = False
            TabOrder = 0
            object CkRetiradaUsa: TdmkCheckBox
              Left = 8
              Top = 4
              Width = 185
              Height = 17
              Caption = 'Definir local de retirada diferente:'
              TabOrder = 0
              OnClick = CkRetiradaUsaClick
              QryCampo = 'RetiradaUsa'
              UpdCampo = 'RetiradaUsa'
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
          end
          object PnRetiradaEntiAll: TPanel
            Left = 0
            Top = 29
            Width = 1000
            Height = 360
            Align = alTop
            TabOrder = 1
            Visible = False
            object PnRetiradaEntiSel: TPanel
              Left = 1
              Top = 1
              Width = 998
              Height = 28
              Align = alTop
              Caption = 'PnRetiradaEntiSel'
              TabOrder = 0
              object Label4: TLabel
                Left = 8
                Top = 8
                Width = 45
                Height = 13
                Caption = 'Entidade:'
              end
              object SbRetiradaEnti: TSpeedButton
                Left = 605
                Top = 4
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbRetiradaEntiClick
              end
              object EdRetiradaEnti: TdmkEditCB
                Left = 73
                Top = 4
                Width = 52
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'RetiradaEnti'
                UpdCampo = 'RetiradaEnti'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnRedefinido = EdRetiradaEntiRedefinido
                DBLookupComboBox = CBRetiradaEnti
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBRetiradaEnti: TdmkDBLookupComboBox
                Left = 127
                Top = 4
                Width = 475
                Height = 21
                KeyField = 'Codigo'
                ListField = 'NOME_ENT'
                ParentShowHint = False
                ShowHint = False
                TabOrder = 1
                dmkEditCB = EdRetiradaEnti
                QryCampo = 'RetiradaEnti'
                UpdType = utNil
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
            end
            object PnRetiradaEntiViw: TPanel
              Left = 1
              Top = 29
              Width = 998
              Height = 330
              Align = alClient
              TabOrder = 1
              Visible = False
              object Label5: TLabel
                Left = 8
                Top = 4
                Width = 61
                Height = 13
                Caption = 'CNPJ / CPF:'
                FocusControl = DBEdit1
              end
              object Label10: TLabel
                Left = 248
                Top = 4
                Width = 75
                Height = 13
                Caption = 'Nome entidade:'
                FocusControl = DBEdit2
              end
              object Label11: TLabel
                Left = 8
                Top = 44
                Width = 57
                Height = 13
                Caption = 'Logradouro:'
                FocusControl = DBEdit3
              end
              object Label16: TLabel
                Left = 212
                Top = 44
                Width = 23
                Height = 13
                Caption = 'Rua:'
                FocusControl = DBEdit5
              end
              object Label17: TLabel
                Left = 616
                Top = 44
                Width = 40
                Height = 13
                Caption = 'N'#250'mero:'
                FocusControl = DBEdit6
              end
              object Label21: TLabel
                Left = 752
                Top = 44
                Width = 67
                Height = 13
                Caption = 'Complemento:'
                FocusControl = DBEdit9
              end
              object Label18: TLabel
                Left = 8
                Top = 84
                Width = 30
                Height = 13
                Caption = 'Bairro:'
                FocusControl = DBEdit7
              end
              object Label34: TLabel
                Left = 252
                Top = 84
                Width = 50
                Height = 13
                Caption = 'Munic'#237'pio:'
                FocusControl = DBEdit10
              end
              object Label35: TLabel
                Left = 652
                Top = 84
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = DBEdit11
              end
              object Label36: TLabel
                Left = 684
                Top = 84
                Width = 25
                Height = 13
                Caption = 'Pa'#237's:'
                FocusControl = DBEdit12
              end
              object Label38: TLabel
                Left = 8
                Top = 128
                Width = 102
                Height = 13
                Caption = 'Telefone principal (1):'
                FocusControl = DBEdit14
              end
              object Label39: TLabel
                Left = 276
                Top = 128
                Width = 28
                Height = 13
                Caption = 'Email:'
                FocusControl = DBEdit16
              end
              object Label40: TLabel
                Left = 728
                Top = 128
                Width = 87
                Height = 13
                Caption = 'Inscr'#231#227'o estadual:'
                FocusControl = DBEdit17
              end
              object DBEdit1: TDBEdit
                Left = 8
                Top = 20
                Width = 238
                Height = 21
                DataField = 'CNPJ_CPF'
                TabOrder = 0
              end
              object DBEdit2: TDBEdit
                Left = 248
                Top = 20
                Width = 741
                Height = 21
                DataField = 'NOME_ENT'
                TabOrder = 1
              end
              object DBEdit3: TDBEdit
                Left = 8
                Top = 60
                Width = 37
                Height = 21
                DataField = 'Lograd'
                TabOrder = 2
              end
              object DBEdit4: TDBEdit
                Left = 48
                Top = 60
                Width = 161
                Height = 21
                DataField = 'NOMELOGRAD'
                TabOrder = 3
              end
              object DBEdit5: TDBEdit
                Left = 212
                Top = 60
                Width = 400
                Height = 21
                DataField = 'RUA'
                TabOrder = 4
              end
              object DBEdit6: TDBEdit
                Left = 616
                Top = 60
                Width = 134
                Height = 21
                DataField = 'NUMERO'
                TabOrder = 5
              end
              object DBEdit9: TDBEdit
                Left = 752
                Top = 60
                Width = 237
                Height = 21
                DataField = 'COMPL'
                TabOrder = 6
              end
              object DBEdit7: TDBEdit
                Left = 8
                Top = 100
                Width = 241
                Height = 21
                DataField = 'BAIRRO'
                TabOrder = 7
              end
              object DBEdit10: TDBEdit
                Left = 252
                Top = 100
                Width = 134
                Height = 21
                DataField = 'CODMUNICI'
                TabOrder = 8
              end
              object DBEdit8: TDBEdit
                Left = 390
                Top = 100
                Width = 259
                Height = 21
                DataField = 'CIDADE'
                TabOrder = 9
              end
              object DBEdit11: TDBEdit
                Left = 652
                Top = 100
                Width = 30
                Height = 21
                DataField = 'NOMEUF'
                TabOrder = 10
              end
              object DBEdit12: TDBEdit
                Left = 684
                Top = 100
                Width = 61
                Height = 21
                DataField = 'CODPAIS'
                TabOrder = 11
              end
              object DBEdit13: TDBEdit
                Left = 748
                Top = 100
                Width = 240
                Height = 21
                DataField = 'Pais'
                TabOrder = 12
              end
              object DBEdit14: TDBEdit
                Left = 8
                Top = 144
                Width = 264
                Height = 21
                DataField = 'TE1'
                TabOrder = 13
              end
              object DBEdit16: TDBEdit
                Left = 276
                Top = 144
                Width = 449
                Height = 21
                DataField = 'EMAIL'
                TabOrder = 14
              end
              object DBEdit17: TDBEdit
                Left = 728
                Top = 144
                Width = 264
                Height = 21
                DataField = 'IE'
                TabOrder = 15
              end
            end
          end
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 621
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label49: TLabel
          Left = 244
          Top = 17
          Width = 78
          Height = 13
          Caption = '% Fatura parcial:'
          Visible = False
        end
        object PnSaiDesis: TPanel
          Left = 859
          Top = 0
          Width = 145
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
        object CkAFP_Sit: TdmkCheckBox
          Left = 152
          Top = 15
          Width = 89
          Height = 17
          Caption = 'Fatura parcial?'
          TabOrder = 2
          Visible = False
          QryCampo = 'AFP_Sit'
          UpdCampo = 'AFP_Sit'
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object EdAFP_Per: TdmkEdit
          Left = 324
          Top = 12
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          Visible = False
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'AFP_Per'
          UpdCampo = 'AFP_Per'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 210
        Height = 32
        Caption = 'Emiss'#227'o de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 210
        Height = 32
        Caption = 'Emiss'#227'o de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 210
        Height = 32
        Caption = 'Emiss'#227'o de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 116
    Top = 12
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 144
    Top = 12
  end
  object VuCondicaoPG: TdmkValUsu
    dmkEditCB = EdCondicaoPG
    Panel = PainelEdita
    QryCampo = 'CondicaoPG'
    UpdCampo = 'CondicaoPG'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 172
    Top = 12
  end
  object VuCambioMda: TdmkValUsu
    dmkEditCB = EdMoeda
    Panel = PainelEdita
    QryCampo = 'Moeda'
    UpdCampo = 'Moeda'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 200
    Top = 12
  end
  object VuTabelaPrc: TdmkValUsu
    dmkEditCB = EdTabelaPrc
    Panel = PainelEdita
    QryCampo = 'TabelaPrc'
    UpdCampo = 'TabelaPrc'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 228
    Top = 12
  end
  object VuMotivoSit: TdmkValUsu
    dmkEditCB = EdMotivoSit
    Panel = PainelEdita
    QryCampo = 'MotivoSit'
    UpdCampo = 'MotivoSit'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 256
    Top = 12
  end
  object VuFisRegCad: TdmkValUsu
    dmkEditCB = EdRegrFiscal
    Panel = PainelEdita
    QryCampo = 'RegrFiscal'
    UpdCampo = 'RegrFiscal'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 284
    Top = 12
  end
  object PMRegrFiscal: TPopupMenu
    Left = 644
    Top = 460
    object Estries1: TMenuItem
      Caption = '&Estri'#231#245'es'
      OnClick = Estries1Click
    end
    object Cadastro1: TMenuItem
      Caption = '&Cadastro'
      OnClick = Cadastro1Click
    end
  end
end
