object FmFatDivGru: TFmFatDivGru
  Left = 339
  Top = 185
  Caption = 'NFe-GERAL-003 :: Itens de Venda - Por Grupo'
  ClientHeight = 827
  ClientWidth = 999
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 999
    Height = 619
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnSeleciona: TPanel
      Left = 0
      Top = 60
      Width = 999
      Height = 148
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 15
        Top = 5
        Width = 50
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Produto:'
      end
      object Label7: TLabel
        Left = 89
        Top = 55
        Width = 153
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '[F4] Busca por refer'#234'ncia:'
      end
      object SBMenu: TSpeedButton
        Left = 592
        Top = 26
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SBMenuClick
      end
      object Label14: TLabel
        Left = 622
        Top = 5
        Width = 33
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'NCM:'
      end
      object Label4: TLabel
        Left = 750
        Top = 5
        Width = 49
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'U. Med.:'
      end
      object EdGraGru1: TdmkEditCB
        Left = 15
        Top = 26
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGru1Change
        OnEnter = EdGraGru1Enter
        OnExit = EdGraGru1Exit
        OnKeyDown = EdGraGru1KeyDown
        DBLookupComboBox = CBGraGru1
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGru1: TdmkDBLookupComboBox
        Left = 89
        Top = 26
        Width = 501
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGraGru1
        TabOrder = 1
        OnExit = CBGraGru1Exit
        OnKeyDown = CBGraGru1KeyDown
        dmkEditCB = EdGraGru1
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object ST3: TStaticText
        Left = 0
        Top = 126
        Width = 999
        Height = 22
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Alignment = taCenter
        Caption = 'ST3'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 6
        Transparent = False
        ExplicitWidth = 32
      end
      object ST2: TStaticText
        Left = 0
        Top = 104
        Width = 999
        Height = 22
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Alignment = taCenter
        Caption = 'ST2'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
        ExplicitWidth = 32
      end
      object ST1: TStaticText
        Left = 0
        Top = 82
        Width = 999
        Height = 22
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Alignment = taCenter
        Caption = 'ST1'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        Transparent = False
        ExplicitWidth = 32
      end
      object PnJuros: TPanel
        Left = 806
        Top = 0
        Width = 193
        Height = 82
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvLowered
        Enabled = False
        TabOrder = 3
        object Label2: TLabel
          Left = 10
          Top = 5
          Width = 80
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% Juros m'#234's:'
          FocusControl = DBEdit1
        end
        object Label3: TLabel
          Left = 94
          Top = 5
          Width = 93
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% Custo financ.:'
          FocusControl = DBEdit1
        end
        object DBEdit1: TDBEdit
          Left = 10
          Top = 25
          Width = 80
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'JurosMes'
          DataSource = FmFatDivGer.DsPediVda
          TabOrder = 0
        end
        object EdCustoFin: TdmkEdit
          Left = 94
          Top = 25
          Width = 93
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object CkNome_ex: TCheckBox
        Left = 89
        Top = 4
        Width = 440
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Mostrar c'#243'digo na frente do nome na lista suspensa dos produtos.'
        TabOrder = 2
        OnClick = CkNome_exClick
      end
      object DBEdit2: TDBEdit
        Left = 622
        Top = 26
        Width = 123
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NCM'
        DataSource = DsGraGru1
        TabOrder = 7
      end
      object DBEdit3: TDBEdit
        Left = 750
        Top = 26
        Width = 48
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'SIGLAUNIDMED'
        DataSource = DsGraGru1
        TabOrder = 8
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 208
      Width = 999
      Height = 411
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet5
      Align = alClient
      MultiLine = True
      TabHeight = 25
      TabOrder = 1
      Visible = False
      object TabSheet5: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Quantidade '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeQ: TStringGrid
          Left = 0
          Top = 0
          Width = 991
          Height = 347
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDblClick = GradeQDblClick
          OnDrawCell = GradeQDrawCell
          OnKeyDown = GradeQKeyDown
        end
        object Panel4: TPanel
          Left = 0
          Top = 347
          Width = 991
          Height = 29
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object LaAviso2B: TLabel
            Left = 6
            Top = 2
            Width = 764
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
              'a incluir / alterar a quantidade.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -18
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2A: TLabel
            Left = 5
            Top = 1
            Width = 764
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
              'a incluir / alterar a quantidade.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -18
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Valor unit'#225'rio '
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeF: TStringGrid
          Left = 0
          Top = 0
          Width = 991
          Height = 376
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 2
          DefaultColWidth = 100
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDrawCell = GradeFDrawCell
          OnKeyDown = GradeFKeyDown
        end
      end
      object TabSheet4: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Desconto '
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeD: TStringGrid
          Left = 0
          Top = 0
          Width = 991
          Height = 356
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
          ParentFont = False
          TabOrder = 0
          OnClick = GradeDClick
          OnDrawCell = GradeDDrawCell
          OnKeyDown = GradeDKeyDown
          OnSelectCell = GradeDSelectCell
        end
        object StaticText3: TStaticText
          Left = 0
          Top = 356
          Width = 653
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 
            'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
            'a incluir / alterar o desconto.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Pre'#231'o da lista'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeL: TStringGrid
          Left = 0
          Top = 0
          Width = 991
          Height = 376
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 2
          DefaultColWidth = 100
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDrawCell = GradeLDrawCell
        end
      end
      object TabSheet6: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' C'#243'digos '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeC: TStringGrid
          Left = 0
          Top = 0
          Width = 991
          Height = 356
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 1
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 1
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = GradeCDrawCell
          RowHeights = (
            18)
        end
        object StaticText6: TStaticText
          Left = 0
          Top = 356
          Width = 603
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 
            'Para gerar o c'#243'digo, clique na c'#233'lula, coluna ou linha correspon' +
            'dente na guia "Ativos".'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
      object TabSheet3: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Ativos '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeA: TStringGrid
          Left = 0
          Top = 0
          Width = 991
          Height = 356
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = GradeADrawCell
          RowHeights = (
            18
            18)
        end
        object StaticText2: TStaticText
          Left = 0
          Top = 356
          Width = 571
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 
            'Clique na c'#233'lula, coluna ou linha correspondente para ativar / d' +
            'esativar o produto.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
      object TabSheet9: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Inclui? '
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeX: TStringGrid
          Left = 0
          Top = 0
          Width = 991
          Height = 376
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = GradeXDrawCell
          RowHeights = (
            18
            18)
        end
      end
      object TabSheet7: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Pe'#231'as '
        ImageIndex = 7
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Grade1: TStringGrid
          Left = 0
          Top = 0
          Width = 991
          Height = 376
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDrawCell = Grade1DrawCell
          OnKeyDown = Grade1KeyDown
        end
      end
      object TabSheet8: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Peso '
        ImageIndex = 8
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Grade2: TStringGrid
          Left = 0
          Top = 0
          Width = 991
          Height = 376
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDrawCell = Grade2DrawCell
          OnKeyDown = Grade2KeyDown
        end
      end
      object TabSheet10: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' '#193'rea m'#178' '
        ImageIndex = 9
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Grade3: TStringGrid
          Left = 0
          Top = 0
          Width = 991
          Height = 376
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDrawCell = Grade3DrawCell
          OnKeyDown = Grade3KeyDown
        end
      end
      object TabSheet11: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' '#193'rea ft'#178' '
        ImageIndex = 10
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Grade4: TStringGrid
          Left = 0
          Top = 0
          Width = 991
          Height = 376
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDrawCell = Grade4DrawCell
          OnKeyDown = Grade4KeyDown
        end
      end
      object TabSheet12: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Fator baixa '
        ImageIndex = 11
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Grade0: TStringGrid
          Left = 0
          Top = 0
          Width = 991
          Height = 376
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = Grade0DrawCell
          RowHeights = (
            18
            18)
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 999
      Height = 60
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Panel7: TPanel
        Left = 316
        Top = 0
        Width = 683
        Height = 60
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvLowered
        TabOrder = 0
        object Label5: TLabel
          Left = 15
          Top = 5
          Width = 599
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Centro de estoque (Cadastrados nos itens de movimenta'#231#227'o da regr' +
            'a fiscal selecionada no pedido):'
        end
        object EdStqCenCad: TdmkEditCB
          Left = 15
          Top = 25
          Width = 69
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBStqCenCad
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBStqCenCad: TdmkDBLookupComboBox
          Left = 84
          Top = 25
          Width = 508
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsStqCenCad
          TabOrder = 1
          dmkEditCB = EdStqCenCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 316
        Height = 60
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 1
        object Label6: TLabel
          Left = 123
          Top = 5
          Width = 49
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Volume:'
          FocusControl = DBEdit5
        end
        object Label10: TLabel
          Left = 10
          Top = 5
          Width = 52
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID fatura:'
          FocusControl = DBEdit6
        end
        object DBEdit5: TdmkDBEdit
          Left = 123
          Top = 25
          Width = 69
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Cnta'
          DataSource = DsFatPedVol
          TabOrder = 0
          UpdCampo = 'Cnta'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit6: TdmkDBEdit
          Left = 10
          Top = 25
          Width = 109
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Codigo'
          DataSource = DsFatPedCab
          TabOrder = 1
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 197
          Top = 25
          Width = 109
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NO_UnidMed'
          DataSource = DsFatPedVol
          TabOrder = 2
          UpdCampo = 'Cnta'
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 999
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 940
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 881
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 391
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Itens de Venda - Por Grupo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 391
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Itens de Venda - Por Grupo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 391
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Itens de Venda - Por Grupo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 678
    Width = 999
    Height = 70
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 18
      Width = 995
      Height = 50
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1B: TLabel
        Left = 16
        Top = 2
        Width = 705
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Verifique se o produto selecionado tem pre'#231'o cadastrado na lista' +
          ' de pre'#231'os selecionada.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso1A: TLabel
        Left = 15
        Top = 1
        Width = 705
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Verifique se o produto selecionado tem pre'#231'o cadastrado na lista' +
          ' de pre'#231'os selecionada.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 30
        Width = 995
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 748
    Width = 999
    Height = 79
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 18
      Width = 995
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 819
        Top = 0
        Width = 176
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 25
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object BtDadosFiscais: TBitBtn
        Tag = 502
        Left = 178
        Top = 4
        Width = 185
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Editar Dados fiscais'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtDadosFiscaisClick
      end
    end
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,'
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, pgt.MadeBy,'
      'gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD,'
      'gg1.CST_A, gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED,'
      'unm.Nome NOMEUNIDMED,  pgt.Fracio,'
      'CONCAT(LPAD(gg1.CodUsu, 8, "0"), " - ", gg1.Nome) NOME_EX,'
      'prod_indTot '
      'FROM gragru1 gg1'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      ''
      '/*WHERE pgt.TipPrd=1*/'
      'WHERE pgt.TipPrd>-1'
      'AND gg1.Nivel1 > 0'
      'AND gg1.Nivel1 NOT IN ('
      '  SELECT DISTINCT gg1.Nivel1'
      '  FROM pedivdaits pvi'
      '  LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      '  LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      '  WHERE pvi.Codigo=:P0'
      ')'
      ''
      'ORDER BY gg1.Nome')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru1Codusu: TIntegerField
      FieldName = 'Codusu'
      Required = True
    end
    object QrGraGru1Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGru1Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Required = True
    end
    object QrGraGru1NOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGru1CODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGru1CST_A: TSmallintField
      FieldName = 'CST_A'
      Required = True
    end
    object QrGraGru1CST_B: TSmallintField
      FieldName = 'CST_B'
      Required = True
    end
    object QrGraGru1UnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
    object QrGraGru1NCM: TWideStringField
      FieldName = 'NCM'
      Required = True
      Size = 10
    end
    object QrGraGru1Peso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrGraGru1SIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 3
    end
    object QrGraGru1CODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGru1NOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGru1NOME_EX: TWideStringField
      FieldName = 'NOME_EX'
      Required = True
      Size = 44
    end
    object QrGraGru1Fracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrGraGru1prod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
    object QrGraGru1MadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrGraGru1UsaSubsTrib: TSmallintField
      FieldName = 'UsaSubsTrib'
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 36
    Top = 8
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Sequencia) Ultimo'
      'FROM etqgeraits'
      'WHERE GraGruX = :P0'
      '')
    Left = 64
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocUltimo: TIntegerField
      FieldName = 'Ultimo'
      Required = True
    end
  end
  object QrLista: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraCusPrc'
      'FROM fisregmvt'
      'WHERE TipoMov=1'
      'AND Empresa=:P0'
      'AND Codigo=:P1')
    Left = 96
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrListaGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
  end
  object DsFatPedVol: TDataSource
    DataSet = FmFatDivGer.QrFatPedVol
    Left = 492
    Top = 12
  end
  object DsFatPedCab: TDataSource
    DataSet = FmFatDivGer.QrFatPedCab
    Left = 464
    Top = 12
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrStqCenCadAfterOpen
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad scc'
      'WHERE Codigo IN '
      '('
      '  SELECT StqCenCad'
      '  FROM fisregmvt'
      '  WHERE Codigo=:P0'
      '  AND Empresa=:P1'
      ') '
      'ORDER BY Nome')
    Left = 520
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 548
    Top = 12
  end
  object QrItem: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrItemBeforeClose
    SQL.Strings = (
      'SELECT gg1.Nome NOMENIVEL1, ggc.GraCorCad, '
      'gcc.Nome NOMECOR,  gti.Nome NOMETAM, '
      'ggx.Controle GraGruX, ggx.GraGru1, '
      'gg1.CodUsu CU_Nivel1, gg1.IPI_Alq, '
      'pgt.MadeBy, pgt.Fracio, '
      'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.prod_indTot'
      'FROM gragrux ggx '
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 576
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItemNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrItemGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrItemNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrItemNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragrux.Controle'
      Required = True
    end
    object QrItemGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
      Required = True
    end
    object QrItemCU_Nivel1: TIntegerField
      FieldName = 'CU_Nivel1'
      Required = True
    end
    object QrItemIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrItemMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrItemFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrItemHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrItemGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrItemprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
  end
  object DsItem: TDataSource
    DataSet = QrItem
    Left = 604
    Top = 12
  end
  object QrPreco_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvi.PrecoF, InfAdCuztm,'
      '(QuantP-QuantC-QuantV) QuantF,'
      'PercCustom, MedidaC, MedidaL,'
      'MedidaA, MedidaE, MedOrdem'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE pvi.Controle=:P0'
      '')
    Left = 632
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPreco_PrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrPreco_QuantF: TFloatField
      FieldName = 'QuantF'
      Required = True
    end
    object QrPreco_InfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrPreco_PercCustom: TFloatField
      FieldName = 'PercCustom'
    end
    object QrPreco_MedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrPreco_MedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrPreco_MedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrPreco_MedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrPreco_MedOrdem: TIntegerField
      FieldName = 'MedOrdem'
    end
  end
  object DsPreco_: TDataSource
    DataSet = QrPreco_
    Left = 660
    Top = 12
  end
  object PMMenu: TPopupMenu
    Left = 584
    Top = 168
    object Produtos1: TMenuItem
      Caption = '&Produtos'
      OnClick = Produtos1Click
    end
    object abelasdepreos1: TMenuItem
      Caption = '&Tabelas de pre'#231'os'
      OnClick = abelasdepreos1Click
    end
    object Regrasfiscais1: TMenuItem
      Caption = '&Regras fiscais'
      OnClick = Regrasfiscais1Click
    end
  end
end
