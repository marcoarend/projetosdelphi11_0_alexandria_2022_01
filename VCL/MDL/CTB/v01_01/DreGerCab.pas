unit DreGerCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, Vcl.Mask, Variants,
  dmkDBLookupComboBox, dmkEditCB, dmkDBGrid, Vcl.Menus, frxClass, frxDBSet,
  dmkLabelRotate;

type
  TFormaOpenCPC = (focpcTudo, focpcItem);
  TFmDreGerCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel5: TPanel;
    Panel10: TPanel;
    DBGCab: TDBGrid;
    Panel29: TPanel;
    BtPeriodo: TBitBtn;
    PMPeriodo: TPopupMenu;
    Incluinovoperiodo1: TMenuItem;
    Excluiperiodoselecionado1: TMenuItem;
    QrDreGerCab: TMySQLQuery;
    DsDreGerCab: TDataSource;
    QrDreGerCabAno: TIntegerField;
    QrDreGerCabAnoMes: TIntegerField;
    QrDreGerCabEmpresa: TIntegerField;
    QrDreGerCabNO_ENT: TWideStringField;
    QrDreGerCabMES_ANO: TWideStringField;
    PMObter: TPopupMenu;
    ObterFaturamento1: TMenuItem;
    QrVen: TMySQLQuery;
    QrDreGerVen: TMySQLQuery;
    DsDreGerVen: TDataSource;
    QrPLOrfaos: TMySQLQuery;
    QrNFeOrfaos: TMySQLQuery;
    QrPLOrfaosMovimCod: TIntegerField;
    QrPLOrfaosSerieV: TIntegerField;
    QrPLOrfaosNFV: TIntegerField;
    QrPLOrfaosDATA: TDateField;
    QrPLOrfaosHORA: TTimeField;
    QrPLOrfaosCliente: TIntegerField;
    QrPLOrfaosQtde: TFloatField;
    frxDRE_GEREN_001_Orfaos: TfrxReport;
    frxDsPLOrfaos: TfrxDBDataset;
    frxDsNFeOrfaos: TfrxDBDataset;
    QrNFeOrfaosMovimCod: TIntegerField;
    QrNFeOrfaosFatNum: TIntegerField;
    QrNFeOrfaoside_serie: TIntegerField;
    QrNFeOrfaoside_nNF: TIntegerField;
    QrNFeOrfaoside_dEmi: TDateField;
    QrNFeOrfaoside_hEmi: TTimeField;
    QrNFeOrfaosICMSTot_vNF: TFloatField;
    QrNFeOrfaosCodInfoDest: TIntegerField;
    QrNFeOrfaosNO_Cliente: TWideStringField;
    QrPLOrfaosNO_Cliente: TWideStringField;
    QrVenMovimCod: TIntegerField;
    QrVenValorT: TFloatField;
    QrVenQtde: TFloatField;
    QrVenFatID: TIntegerField;
    QrVenFatNum: TIntegerField;
    QrVenide_serie: TIntegerField;
    QrVenide_nNF: TIntegerField;
    QrVenide_dEmi: TDateField;
    QrVenide_hEmi: TTimeField;
    QrVenICMSTot_vNF: TFloatField;
    QrVenCodInfoDest: TIntegerField;
    QrVenNO_Cliente: TWideStringField;
    QrVenId: TWideStringField;
    QrVenICMSTot_vICMS: TFloatField;
    QrVenICMSTot_vIPI: TFloatField;
    QrVenICMSTot_vPIS: TFloatField;
    QrVenICMSTot_vCOFINS: TFloatField;
    QrDreGerVenAno: TIntegerField;
    QrDreGerVenAnoMes: TIntegerField;
    QrDreGerVenEmpresa: TIntegerField;
    QrDreGerVenLinha: TIntegerField;
    QrDreGerVenFatID: TIntegerField;
    QrDreGerVenFatNum: TIntegerField;
    QrDreGerVenId: TWideStringField;
    QrDreGerVenide_serie: TIntegerField;
    QrDreGerVenide_nNF: TIntegerField;
    QrDreGerVenide_dEmi: TDateField;
    QrDreGerVenide_hEmi: TTimeField;
    QrDreGerVenICMSTot_vNF: TFloatField;
    QrDreGerVenICMSTot_vCOFINS: TFloatField;
    QrDreGerVenICMSTot_vPIS: TFloatField;
    QrDreGerVenICMSTot_vIPI: TFloatField;
    QrDreGerVenICMSTot_vICMS: TFloatField;
    QrDreGerVenFormaAdd: TSmallintField;
    QrDreGerVenMovimCod: TIntegerField;
    QrDreGerVenQtde: TFloatField;
    QrDreGerVenCustoT: TFloatField;
    QrSemFRC: TMySQLQuery;
    QrSemFRCide_nNF: TIntegerField;
    ObterLanamentos1: TMenuItem;
    Panel9: TPanel;
    PCDados: TPageControl;
    TabSheet1: TTabSheet;
    Panel7: TPanel;
    DBGrid1: TDBGrid;
    TabSheet2: TTabSheet;
    Panel11: TPanel;
    BtObter: TBitBtn;
    QrDreCfgEst: TMySQLQuery;
    QrDreCfgEstCodigo: TIntegerField;
    QrDreCfgEstControle: TIntegerField;
    QrDreCfgEstNome: TWideStringField;
    QrDreCfgEstDreCfgTp1: TIntegerField;
    QrDreCfgEstOrdem: TIntegerField;
    QrDreCfgCtd: TMySQLQuery;
    QrDreCfgCtdCodigo: TIntegerField;
    QrDreCfgCtdControle: TIntegerField;
    QrDreCfgCtdConta: TIntegerField;
    QrDreCfgCtdNome: TWideStringField;
    QrDreCfgCtdDreCfgTp2: TIntegerField;
    QrDreCfgCtdOrdem: TIntegerField;
    QrDreCfgCpc: TMySQLQuery;
    QrDreCfgCpcIDXSUB4: TLargeintField;
    QrDreCfgCpcEXCLUSO: TIntegerField;
    QrDreCfgCpcStatus: TIntegerField;
    QrDreCfgCpcPlaAllcad: TIntegerField;
    QrDreCfgCpcNome: TWideStringField;
    QrDreCfgCpcOrdens: TWideStringField;
    QrDCC: TMySQLQuery;
    QrDCCPlaAllCad: TIntegerField;
    QrDCCExcluso: TIntegerField;
    QrLanctos: TMySQLQuery;
    QrLanctosCliInt: TIntegerField;
    QrLanctosData: TDateField;
    QrLanctosTipo: TSmallintField;
    QrLanctosCarteira: TIntegerField;
    QrLanctosControle: TIntegerField;
    QrLanctosSub: TSmallintField;
    QrLanctosGenero: TIntegerField;
    QrLanctosDebito: TFloatField;
    QrLanctosCredito: TFloatField;
    QrLanctosCompensado: TDateField;
    QrLanctosSit: TIntegerField;
    QrLanctosVencimento: TDateField;
    QrLanctosGenCtb: TIntegerField;
    QrLanctosGenCtbD: TIntegerField;
    QrLanctosGenCtbC: TIntegerField;
    PB1: TProgressBar;
    QrLanctosCodNiv1: TIntegerField;
    QrLanctosCodNiv2: TIntegerField;
    QrLanctosCodNiv3: TIntegerField;
    QrLanctosCodNiv4: TIntegerField;
    QrLanctosCodNiv5: TIntegerField;
    QrLanctosCodNiv6: TIntegerField;
    QrDreGerFinAut: TMySQLQuery;
    DsDreGerFinAut: TDataSource;
    QrDreGerFinAutAno: TIntegerField;
    QrDreGerFinAutAnoMes: TIntegerField;
    QrDreGerFinAutEmpresa: TIntegerField;
    QrDreGerFinAutLinha: TIntegerField;
    QrDreGerFinAutDreCfgEst: TIntegerField;
    QrDreGerFinAutDreCfgCtd: TIntegerField;
    QrDreGerFinAutDreCfgCpc: TIntegerField;
    QrDreGerFinAutDreCfgTp1: TIntegerField;
    QrDreGerFinAutDreCfgTp2: TIntegerField;
    QrDreGerFinAutData: TDateField;
    QrDreGerFinAutTipo: TSmallintField;
    QrDreGerFinAutCarteira: TIntegerField;
    QrDreGerFinAutControle: TIntegerField;
    QrDreGerFinAutSub: TSmallintField;
    QrDreGerFinAutGenero: TIntegerField;
    QrDreGerFinAutDebito: TFloatField;
    QrDreGerFinAutCredito: TFloatField;
    QrDreGerFinAutCompensado: TDateField;
    QrDreGerFinAutSit: TIntegerField;
    QrDreGerFinAutVencimento: TDateField;
    QrDreGerFinAutGenCtb: TIntegerField;
    QrDreGerFinAutGenCtbD: TIntegerField;
    QrDreGerFinAutGenCtbC: TIntegerField;
    QrDreGerFinAutLk: TIntegerField;
    QrDreGerFinAutDataCad: TDateField;
    QrDreGerFinAutDataAlt: TDateField;
    QrDreGerFinAutUserCad: TIntegerField;
    QrDreGerFinAutUserAlt: TIntegerField;
    QrDreGerFinAutAlterWeb: TSmallintField;
    QrDreGerFinAutAWServerID: TIntegerField;
    QrDreGerFinAutAWStatSinc: TSmallintField;
    QrDreGerFinAutAtivo: TSmallintField;
    QrDreGerFinAutGenNiv1: TIntegerField;
    QrDreGerFinAutGenNiv2: TIntegerField;
    QrDreGerFinAutGenNiv3: TIntegerField;
    QrDreGerFinAutGenNiv4: TIntegerField;
    QrDreGerFinAutGenNiv5: TIntegerField;
    QrDreGerFinAutGenNiv6: TIntegerField;
    QrDreGerFinAutNO_DreCfgTp1: TWideStringField;
    QrDreGerFinAutNO_DreCfgTp2: TWideStringField;
    QrLanctosCliente: TIntegerField;
    QrLanctosFornecedor: TIntegerField;
    QrDreGerFinAutCpcStatus: TSmallintField;
    QrDreGerFinAutCliente: TIntegerField;
    QrDreGerFinAutFornecedor: TIntegerField;
    QrDreGerFinAutNO_PAC_Genero: TWideStringField;
    QrDreGerFinAutNO_PAC_Cpc: TWideStringField;
    QrDreGerFinAutNO_TERCEIRO: TWideStringField;
    N2ObterDevolues1: TMenuItem;
    TabSheet3: TTabSheet;
    QrDev: TMySQLQuery;
    QrDevNO_Cliente: TWideStringField;
    QrDevMovFatID: TIntegerField;
    QrDevMovFatNum: TIntegerField;
    QrDevMovimCod: TIntegerField;
    QrDevValorT: TFloatField;
    QrDevQtde: TFloatField;
    QrDevSER: TIntegerField;
    QrDevNUM_DOC: TIntegerField;
    QrDevDT_DOC: TDateField;
    QrDevDT_E_S: TDateField;
    QrDevCHV_NFE: TWideStringField;
    QrDevVL_ICMS: TFloatField;
    QrDevVL_IPI: TFloatField;
    QrDevVL_PIS: TFloatField;
    QrDevVL_COFINS: TFloatField;
    QrDevVL_DOC: TFloatField;
    QrDreGerDev: TMySQLQuery;
    DsDreGerDev: TDataSource;
    QrDreGerDevAno: TIntegerField;
    QrDreGerDevAnoMes: TIntegerField;
    QrDreGerDevEmpresa: TIntegerField;
    QrDreGerDevLinha: TIntegerField;
    QrDreGerDevMovFatID: TIntegerField;
    QrDreGerDevMovFatNum: TIntegerField;
    QrDreGerDevCHV_NFE: TWideStringField;
    QrDreGerDevSER: TIntegerField;
    QrDreGerDevNUM_DOC: TIntegerField;
    QrDreGerDevDT_DOC: TDateField;
    QrDreGerDevDT_E_S: TDateField;
    QrDreGerDevVL_ICMS: TFloatField;
    QrDreGerDevVL_COFINS: TFloatField;
    QrDreGerDevVL_PIS: TFloatField;
    QrDreGerDevVL_IPI: TFloatField;
    QrDreGerDevVL_DOC: TFloatField;
    QrDreGerDevFormaAdd: TSmallintField;
    QrDreGerDevMovimCod: TIntegerField;
    QrDreGerDevQtde: TFloatField;
    QrDreGerDevCustoT: TFloatField;
    DBGrid3: TDBGrid;
    PMAvulso: TPopupMenu;
    IncluiLanamento1: TMenuItem;
    LctInclui1: TMenuItem;
    LctAltera1: TMenuItem;
    LctExclui1: TMenuItem;
    QrDreGerCabReceitaBruta: TFloatField;
    QrDreGerCabDeducAbatim: TFloatField;
    QrDreGerCabReceitaLiquida: TFloatField;
    QrDreGerCabCPV_Fixo: TFloatField;
    QrDreGerCabCPV_Variavel: TFloatField;
    QrDreGerCabLucroBruto: TFloatField;
    QrDreGerCabDespesVendas: TFloatField;
    QrDreGerCabDespesAdmnst: TFloatField;
    QrDreGerCabDespesFinance: TFloatField;
    QrDreGerCabEBITDA: TFloatField;
    QrDreGerCabProvImpostos: TFloatField;
    QrDreGerCabResultLiquid: TFloatField;
    QrDreGerCabDreCfgCab: TIntegerField;
    QrDreGerFinAutCtrlAvul: TIntegerField;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    QrDreCfgCpcMesCompet: TIntegerField;
    QrSeq: TMySQLQuery;
    QrDreGerFinAvu: TMySQLQuery;
    QrDreGerFinAvuAno: TIntegerField;
    QrDreGerFinAvuAnoMes: TIntegerField;
    QrDreGerFinAvuEmpresa: TIntegerField;
    QrDreGerFinAvuLinha: TIntegerField;
    QrDreGerFinAvuDreCfgEst: TIntegerField;
    QrDreGerFinAvuDreCfgCtd: TIntegerField;
    QrDreGerFinAvuDreCfgCpc: TIntegerField;
    QrDreGerFinAvuDreCfgTp1: TIntegerField;
    QrDreGerFinAvuDreCfgTp2: TIntegerField;
    QrDreGerFinAvuData: TDateField;
    QrDreGerFinAvuTipo: TSmallintField;
    QrDreGerFinAvuCarteira: TIntegerField;
    QrDreGerFinAvuControle: TIntegerField;
    QrDreGerFinAvuSub: TSmallintField;
    QrDreGerFinAvuGenero: TIntegerField;
    QrDreGerFinAvuDebito: TFloatField;
    QrDreGerFinAvuCredito: TFloatField;
    QrDreGerFinAvuCompensado: TDateField;
    QrDreGerFinAvuSit: TIntegerField;
    QrDreGerFinAvuVencimento: TDateField;
    QrDreGerFinAvuGenCtb: TIntegerField;
    QrDreGerFinAvuGenCtbD: TIntegerField;
    QrDreGerFinAvuGenCtbC: TIntegerField;
    QrDreGerFinAvuLk: TIntegerField;
    QrDreGerFinAvuDataCad: TDateField;
    QrDreGerFinAvuDataAlt: TDateField;
    QrDreGerFinAvuUserCad: TIntegerField;
    QrDreGerFinAvuUserAlt: TIntegerField;
    QrDreGerFinAvuAlterWeb: TSmallintField;
    QrDreGerFinAvuAWServerID: TIntegerField;
    QrDreGerFinAvuAWStatSinc: TSmallintField;
    QrDreGerFinAvuAtivo: TSmallintField;
    QrDreGerFinAvuGenNiv1: TIntegerField;
    QrDreGerFinAvuGenNiv2: TIntegerField;
    QrDreGerFinAvuGenNiv3: TIntegerField;
    QrDreGerFinAvuGenNiv4: TIntegerField;
    QrDreGerFinAvuGenNiv5: TIntegerField;
    QrDreGerFinAvuGenNiv6: TIntegerField;
    QrDreGerFinAvuNO_DreCfgTp1: TWideStringField;
    QrDreGerFinAvuNO_DreCfgTp2: TWideStringField;
    QrDreGerFinAvuCpcStatus: TSmallintField;
    QrDreGerFinAvuCliente: TIntegerField;
    QrDreGerFinAvuFornecedor: TIntegerField;
    QrDreGerFinAvuNO_PAC_Genero: TWideStringField;
    QrDreGerFinAvuNO_PAC_Cpc: TWideStringField;
    QrDreGerFinAvuNO_TERCEIRO: TWideStringField;
    QrDreGerFinAvuCtrlAvul: TIntegerField;
    DsDreGerFinAvu: TDataSource;
    BtAvulso: TBitBtn;
    TabSheet4: TTabSheet;
    DBGrid4: TDBGrid;
    DBGrid2: TDBGrid;
    MySQLQuery3: TMySQLQuery;
    LargeintField1: TLargeintField;
    IntegerField10: TIntegerField;
    IntegerField11: TIntegerField;
    IntegerField12: TIntegerField;
    WideStringField3: TWideStringField;
    WideStringField4: TWideStringField;
    IntegerField13: TIntegerField;
    QrDreCfgCpcNO_MesCompet: TWideStringField;
    QrLanctosSerieNF: TWideStringField;
    QrLanctosNotaFiscal: TIntegerField;
    QrLanctosDescricao: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure BtPeriodoClick(Sender: TObject);
    procedure Incluinovoperiodo1Click(Sender: TObject);
    procedure Excluiperiodoselecionado1Click(Sender: TObject);
    procedure PMPeriodoPopup(Sender: TObject);
    procedure QrDreGerCabAfterOpen(DataSet: TDataSet);
    procedure ObterFaturamento1Click(Sender: TObject);
    procedure QrDreGerCabAfterScroll(DataSet: TDataSet);
    procedure QrDreGerCabBeforeClose(DataSet: TDataSet);
    procedure frxDRE_GEREN_001_OrfaosGetValue(const VarName: string;
      var Value: Variant);
    procedure ObterLanamentos1Click(Sender: TObject);
    procedure BtObterClick(Sender: TObject);
    procedure N2ObterDevolues1Click(Sender: TObject);
    procedure LctInclui1Click(Sender: TObject);
    procedure LctAltera1Click(Sender: TObject);
    procedure LctExclui1Click(Sender: TObject);
    procedure BtAvulsoClick(Sender: TObject);
    procedure PMAvulsoPopup(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure PCDadosChange(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    { Private declarations }
    FDataIni_Dta, FDataFim_Dta: TDateTime;
    FAno_Int, FEmpresa_Int, FFilial_Int, FAnoMes_Int: Integer;
    FEmpresa_Txt, FAnoMes_Txt, FDataIni_Txt, FDataFim_Txt,
    FTabLctA, FIntervaloPeriodo, FIntervaloSeguinte: String;
    //
    procedure CalculaTotais();
    function  DefineEmpresaEAnoMes(): Boolean;
    procedure ReopenDreGerVen(Linha: Integer);
    procedure ReopenDreGerDev(Linha: Integer);
    procedure ReopenDreGerFin(LinAut, LinAvu: Integer);
    //function  SelecionaDreCfgCab(var DreCfgCab: Integer): Boolean;
    //
    procedure ReopenDreCfgCtd();
    procedure ReopenDreCfgEst(DreCfgCab: Integer);
    procedure ReopenLanctos();
    //
    procedure MostraFormDreGerPer(SQLType: TSQLType);
    procedure MostraFormDreGerFin(SQLType: TSQLType);
    //
  public
    { Public declarations }
    procedure ReopenDreGerCab(AnoMes: Integer);
    procedure ReopenDreCfgCpc(Forma: TFormaOpenCPC; DreCfgCab, Conta: Integer);
    function  ObterLinha(): Integer;
  end;

  var
  FmDreGerCab: TFmDreGerCab;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, Periodo, UMySQLModule,
  UnDmkProcFunc, MyDBCheck, DreGerPer, DreGerFin, UnContabil_Jan,
  DreGerImp;

{$R *.DFM}

procedure TFmDreGerCab.LctAltera1Click(Sender: TObject);
begin
  MostraFormDreGerFin(stUpd);
end;

procedure TFmDreGerCab.LctExclui1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do lan�amento avulso selecionado?') = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'dregerfin', [
    'AnoMes', 'Empresa', 'Linha'], ['=','=', '='],
    [QrDreGerFinAvuAnoMes.Value, QrDreGerFinAvuEmpresa.Value, QrDreGerFinAvuLinha.Value], '') then
    begin
      CalculaTotais();
      ReopenDreGerFin(0, 0);
    end;
  end;
end;

procedure TFmDreGerCab.BtAvulsoClick(Sender: TObject);
begin
  PCDados.ActivePageIndex := 2;
  MyObjects.MostraPopupDeBotao(PMAvulso, BtAvulso);
end;

procedure TFmDreGerCab.BtObterClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMObter, BtObter);
end;

procedure TFmDreGerCab.BtPeriodoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPeriodo, BtPeriodo);
end;

procedure TFmDreGerCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDreGerCab.CalculaTotais();
begin
//
end;

function TFmDreGerCab.DefineEmpresaEAnoMes(): Boolean;
  function ValidaPeriodoInteiroDeUmMes(const DtI, DtF:
  TDateTime): Boolean;
  var
    AnoIi, MesIi, DiaIi,
    AnoFf, AnoFd, MesFf, MesFd, DiaFf, DiaFd: Word;
    DtSeguinteI, DtSeguinteF: TDateTime;
    sSeguinteI, sSeguinteF: String;
  begin
    Result := False;
    FAno_Int := 0;
    FAnoMes_Int := 0;
    DecodeDate(DtI, AnoIi, MesIi, DiaIi);
    DecodeDate(DtF, AnoFf, MesFf, DiaFf);
    DecodeDate(DtF + 1, AnoFd, MesFd, DiaFd);
    FAnoMes_Int := AnoIi * 100 + MesIi;
    FAnoMes_Txt := Geral.FF0(FAnoMes_Int);
    FAno_Int := AnoIi;

    FDataIni_Txt := Geral.FDT(DtI, 1);
    FDataFim_Txt := Geral.FDT(DtF, 1);
    FIntervaloPeriodo := ' "' + FDataIni_Txt + '" AND "' + FDataFim_Txt + '"';
    //
    DtSeguinteI := IncMonth(DtI, 1);
    DtSeguinteF := IncMonth(DtI, 2) - 1;
    sSeguinteI  := Geral.FDT(DtSeguinteI, 1);
    sSeguinteF  := Geral.FDT(DtSeguinteF, 1);
    FIntervaloSeguinte := ' "' + sSeguinteI + '" AND "' + sSeguinteF + '"';
    //
    if DiaIi <> 1 then
      Geral.MB_Aviso(
      'A��o cancelada! Dia inicial deve ser o primeiro dia (dia 1) do m�s,'
      + sLineBreak +
      'mesmo que as atividades tenham se iniciado ap�s o dia primeiro!')
    else if DiaFd <> 1 then
      Geral.MB_Aviso(
      'A��o cancelada! Dia final deve ser o �ltimo dia do m�s,'
      + sLineBreak +
      'mesmo que as atividades tenham sido finalizados antes do �ltimo dia do m�s!')
    else if MesIi <> MesFf then
      Geral.MB_Aviso(
      'A��o cancelada! O per�odo deve ser de um �nico m�s!' + sLineBreak +
      'Data inicial selecionada:  ' + Geral.FDT(DtI, 3) + sLineBreak +
      'Data Final selecionada:   ' + Geral.FDT(DtF, 3))
    else if FAnoMes_Int < 200001 then
      Geral.MB_Aviso(
      'A��o cancelada! O per�odo deve ser de um ano ap�s 2000!' + sLineBreak +
      'Data inicial selecionada:  ' + Geral.FDT(DtI, 3) + sLineBreak +
      'Data Final selecionada:   ' + Geral.FDT(DtF, 3))
    else
      Result := True;
  end;
begin
  Result := False;
  //
  FEmpresa_Int := 0;
  FEmpresa_Txt := '0';
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Empresa n�o definida!') then
    Exit;
  FEmpresa_Int := DModG.QrEmpresasCodigo.Value;
  FEmpresa_Txt := Geral.FF0(FEmpresa_Int);
  //DmSPED.ReopenParamsEmp(FEmpresa_Txt);
  //
  DModG.ReopenParamsSPED(FEmpresa_Int);
  FDataIni_Dta := dmkPF.DatadeAnoMes(QrDreGerCabAnoMes.Value, 1, 0);
  FDataFim_Dta := IncMonth(FDataIni_Dta) - 1;
  Result := ValidaPeriodoInteiroDeUmMes(FDataIni_Dta, FDataFim_Dta);
end;

procedure TFmDreGerCab.EdEmpresaRedefinido(Sender: TObject);
begin
  FEmpresa_Int := 0;
  FFilial_Int  := 0;
  if EdEmpresa.ValueVariant <> 0 then
  begin
    FEmpresa_Int := DModG.QrEmpresasCodigo.Value;
    FFilial_Int  := DModG.QrEmpresasFilial.Value;
  end;
  BtPeriodo.Enabled := EdEmpresa.ValueVariant <> 0;
  if FFilial_Int <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, FFilial_Int)
  else
    FTabLctA := sTabLctErr;
  //
  ReopenDreGerCab(0);
end;

procedure TFmDreGerCab.Excluiperiodoselecionado1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do per�odo selecionado?') = ID_YES then
  begin
    if UMyMod.ExcluiRegistros('', Dmod.QrUpd, 'dregercab', [
    'AnoMes', 'Empresa'], ['=','='],
    [QrDreGerCabAnoMes.Value,
    QrDreGerCabEmpresa.Value], '') then
    ReopenDreGerCab(0);
  end;
end;

procedure TFmDreGerCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDreGerCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FEmpresa_Int := 0;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  FDataIni_Dta := 0;
  FDataFim_Dta := 0;
  FDataIni_Txt := '';
  FDataFim_Txt := '';
  FIntervaloPeriodo := '';
  //
  BtAvulso.Visible := False;
  //
end;

procedure TFmDreGerCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDreGerCab.frxDRE_GEREN_001_OrfaosGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_NO_EMPRESA' then
    Value := CBEmpresa.Text
  else
  if VarName = 'VARF_PERIODO' then
    Value := QrDreGerCabMES_ANO.Value
  else
end;

procedure TFmDreGerCab.LctInclui1Click(Sender: TObject);
begin
  MostraFormDreGerFin(stIns);
end;

procedure TFmDreGerCab.Incluinovoperiodo1Click(Sender: TObject);
begin
  MostraFormDreGerPer(stIns);
end;

procedure TFmDreGerCab.MostraFormDreGerFin(SQLType: TSQLType);
begin
  if not DefineEmpresaEAnoMes() then
    Exit;
  if DBCheck.CriaFm(TFmDreGerFin, FmDreGerFin, afmoNegarComAviso) then
  begin
    FmDreGerFin.ImgTipo.SQLType := SQLType;
    FmDreGerFin.ReopenDreCfgEst(QrDreGerCabDreCfgCab.Value);
    //
    FmDreGerFin.FQrIts       := QrDreGerFinAvu;
    FmDreGerFin.FDreCfgCab   := QrDreGerCabDreCfgCab.Value;
    FmDreGerFin.FAno         := FAno_Int;
    FmDreGerFin.FDataIni_Dta := FDataIni_Dta;
    FmDreGerFin.FDataFim_Dta := FDataFim_Dta;
    //
    FmDreGerFin.EdEmpresa.ValueVariant := FEmpresa_Int;
    FmDreGerFin.EdAnoMes.ValueVariant  := FAnoMes_Int;
    //
    if SQLType = stUpd then
    begin
      FmDreGerFin.EdLinha.ValueVariant     := QrDreGerFinAvuLinha.Value;
      FmDreGerFin.EdCtrlAvul.ValueVariant  := QrDreGerFinAvuCtrlAvul.Value;
      FmDreGerFin.EdDreCfgEst.ValueVariant := QrDreGerFinAvuDreCfgEst.Value;
      FmDreGerFin.CBDreCfgEst.KeyValue     := QrDreGerFinAvuDreCfgEst.Value;
      FmDreGerFin.EdDreCfgCtd.ValueVariant := QrDreGerFinAvuDreCfgCtd.Value;
      FmDreGerFin.CBDreCfgCtd.KeyValue     := QrDreGerFinAvuDreCfgCtd.Value;
      //FmDreGerFin.EdDreCfgCpc.ValueVariant := QrDreGerFinAvuDreCfgCpc.Value;
      FmDreGerFin.TPData.Date              := QrDreGerFinAvuData.Value;
      FmDreGerFin.EdGenero.ValueVariant    := QrDreGerFinAvuGenero.Value;
      FmDreGerFin.CBGenero.KeyValue        := QrDreGerFinAvuGenero.Value;
      FmDreGerFin.EdDebito.ValueVariant    := QrDreGerFinAvuDebito.Value;
      FmDreGerFin.EdCredito.ValueVariant   := QrDreGerFinAvuCredito.Value;
    end;
    //
    FmDreGerFin.ShowModal;
    FmDreGerFin.Destroy;
    //
  end;
end;

procedure TFmDreGerCab.MostraFormDreGerPer(SQLType: TSQLType);
var
  Ano, Mes, Dia: Word;
  AnoMes, J: Integer;
begin
  DecodeDate(Date, Ano, Mes, Dia);
  if DBCheck.CriaFm(TFmDreGerPer, FmDreGerPer, afmoNegarComAviso) then
  begin
    with FmDreGerPer do
    begin
      ImgTipo.SQLType := SQLType;
      //
      CBMes.ItemIndex := Mes -1;
      for J := 0 to CBAno.Items.Count do
      begin
        if Geral.IMV(CBAno.Items[j]) = Ano then
        begin
          CBAno.ItemIndex := j;
          Break;
        end;
      end;
    end;
    //
(*
    if SQLType = stUpd then
    begin
      FmDreGerPer.EdEmpresa.ValueVariant     := QrDreGerCabEmpresa.Value;
      FmDreGerPer.CBEmpresa.KeyValue         := QrDreGerCabEmpresa.Value;
      FmDreGerPer.EdDreCfgCab.ValueVariant   := QrDreGerCabDreCfgCab.Value;
      FmDreGerPer.CBDreCfgCab.KeyValue       := QrDreGerCabDreCfgCab.Value;
    end;
*)
    //
    FmDreGerPer.ShowModal;
    FmDreGerPer.Destroy;
    //
  end;
end;

procedure TFmDreGerCab.N2ObterDevolues1Click(Sender: TObject);
  procedure InsereNFAtual(var Linha: Integer);
  const
    FormaAdd = 1; //=Autom�tico
  var
    CHV_NFE, DT_DOC, DT_E_S: String;
    Ano, AnoMes, Empresa, MovFatID, MovFatNum, SER, NUM_DOC, MovimCod: Integer;
    VL_ICMS, VL_COFINS, VL_PIS, VL_IPI, VL_DOC, Qtde, CustoT: Double;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    Ano            := FAno_Int;
    AnoMes         := FAnoMes_Int;
    Empresa        := FEmpresa_Int;
    Linha          := Linha + 1;
    MovFatID       := QrDevMovFatID.Value;
    MovFatNum      := QrDevMovFatNum.Value;
    CHV_NFE        := QrDevCHV_NFE.Value;
    SER            := QrDevSER.Value;
    NUM_DOC        := QrDevNUM_DOC.Value;
    DT_DOC         := Geral.FDT(QrDevDT_DOC.Value, 1);
    DT_E_S         := Geral.FDT(QrDevDT_E_S.Value, 1);
    VL_ICMS        := QrDevVL_ICMS.Value;
    VL_COFINS      := QrDevVL_COFINS.Value;
    VL_PIS         := QrDevVL_PIS.Value;
    VL_IPI         := QrDevVL_IPI.Value;
    VL_DOC         := QrDevVL_DOC.Value;
    //FormaAdd       :=
    MovimCod       := QrDevMovimCod.Value;
    Qtde           := QrDevQtde.Value;
    CustoT         := VL_DOC;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'dregerdev', False, [
    'Ano', 'MovFatID', 'MovFatNum',
    'CHV_NFE', 'SER', 'NUM_DOC',
    'DT_DOC', 'DT_E_S', 'VL_ICMS',
    'VL_COFINS', 'VL_PIS', 'VL_IPI',
    'VL_DOC', 'FormaAdd', 'MovimCod',
    'Qtde', 'CustoT'], [
    'AnoMes', 'Empresa', 'Linha'], [
    Ano, MovFatID, MovFatNum,
    CHV_NFE, SER, NUM_DOC,
    DT_DOC, DT_E_S, VL_ICMS,
    VL_COFINS, VL_PIS, VL_IPI,
    VL_DOC, FormaAdd, MovimCod,
    Qtde, CustoT], [
    AnoMes, Empresa, Linha], True) then
    begin
      //
    end;
  end;
var
  Linha: Integer;
  Corda: String;
begin
  PCDados.ActivePageIndex := 1;
  Screen.Cursor := crHourGlass;
  try
    Linha := 0;
    //
    if not DefineEmpresaEAnoMes() then
      Exit;
    //
    // se j� existe um per�odo criado
    if QrDreGerDev.State <> dsInactive then
    begin
      // verifica se tem NFe sem FisRegCad (importadads para teste)
      UnDmkDAC_PF.AbreMySQLQuery0(QrSemFRC, Dmod.MyDB, [
      'SELECT einc.NUM_DOC NF ',
      'FROM efdinnnfscab einc',
      'WHERE DT_E_S BETWEEN ' + FIntervaloPeriodo,
      'AND NFeStatus=100',
      'AND RegrFiscal=0',
      '']);
      if QrSemFRC.RecordCount > 0 then
      begin
        Corda := MyObjects.CordaDeQuery(QrSemFRC, 'NF', '');
        Geral.MB_Aviso('NF-es de entrada sem Regra Fiscal definida:' + slineBreak + Corda);
        Exit;
      end;
      // verifica se tem Packing List ou NFe orf�o:
      if QrDreGerDev.RecordCount > 0 then
      begin
      if Geral.MB_Pergunta('Os itens de devolu��es de vendas previamente inseridos ser�o excluidos!' +
      sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'DELETE FROM dregerdev ',
          'WHERE AnoMes=' + Geral.FF0(QrDreGerCabAnoMes.Value),
          'AND Empresa=' + Geral.FF0(QrDreGerCabEmpresa.Value),
          '']);
        end else
        begin
          Geral.MB_Info('A��o cancelada!');
          Exit;
        end
      end;
      // inclus�es autom�ticas
      UnDmkDAC_PF.AbreMySQLQuery0(QrDev, Dmod.MyDB, [
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Cliente,',
      'einc.MovFatID, einc.MovFatNum, einc.MovimCod,',
      'einc.ValorT, ',
      'IF(einc.AreaM2 <> 0, einc.AreaM2, IF(einc.PesoKg <> 0, ',
      '      einc.PesoKg, einc.Pecas)) Qtde,',
      'einc.SER, einc.NUM_DOC,',
      'einc.DT_DOC, einc.DT_E_S, einc.CHV_NFE, einc.VL_ICMS,',
      'einc.VL_IPI, einc.VL_PIS, einc.VL_COFINS, einc.VL_DOC',
      'FROM efdinnnfscab einc',
      'LEFT JOIN fisregcad frc ON frc.Codigo=einc.RegrFiscal',
      'LEFT JOIN entidades ent ON ent.Codigo=einc.Terceiro',
      'WHERE einc.DT_E_S BETWEEN ' + FIntervaloPeriodo,
      'AND einc.NFeStatus=100',
      'AND frc.IntegraDRE=' + Geral.FF0(Integer(TRegFisCadIntDRE.rfcidDevolVenda)),
      '']);
      QrDev.First;
      while not QrDev.Eof do
      begin
        InsereNFAtual(Linha);
        //
        QrDev.Next;
      end;
    end;
    //
    ReopenDreGerDev(0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDreGerCab.ObterFaturamento1Click(Sender: TObject);
  procedure InsereNFAtual(var Linha: Integer);
  const
    FormaAdd = 1; //=Autom�tico
  var
    Id, ide_dEmi, ide_hEmi: String;
    Ano, AnoMes, Empresa, FatID, FatNum, MovimCod, ide_serie, ide_nNF: Integer;
    ICMSTot_vNF, ICMSTot_vCOFINS, ICMSTot_vPIS, ICMSTot_vIPI, ICMSTot_vICMS,
    Qtde, CustoT: Double;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    Ano            := FAno_Int;
    AnoMes         := FAnoMes_Int;
    Empresa        := FEmpresa_Int;
    Linha          := Linha + 1;
    FatID          := QrVenFatID.Value;
    FatNum         := QrVenFatNum.Value;
    Id             := QrVenId.Value;
    ICMSTot_vNF    := QrVenICMSTot_vNF.Value;
    ICMSTot_vCOFINS:= QrVenICMSTot_vCOFINS.Value;
    ICMSTot_vPIS   := QrVenICMSTot_vPIS.Value;
    ICMSTot_vIPI   := QrVenICMSTot_vIPI.Value;
    ICMSTot_vICMS  := QrVenICMSTot_vICMS.Value;
    ide_serie      := QrVenide_serie.Value;
    ide_nNF        := QrVenide_nNF.Value;
    ide_dEmi       := Geral.FDT(QrVenide_dEmi.Value, 1);
    ide_hEmi       := Geral.FDT(QrVenide_hEmi.Value, 100);
    //
    MovimCod       := QrVenMovimCod.Value;
    Qtde           := QrVenQtde.Value;
    CustoT         := -QrVenValorT.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'dregerven', False, [
    'Ano', 'FatID', 'FatNum',
    'Id', 'ide_serie', 'ide_nNF',
    'ide_dEmi', 'ide_hEmi',
    'ICMSTot_vNF', 'ICMSTot_vCOFINS',
    'ICMSTot_vPIS', 'ICMSTot_vIPI', 'ICMSTot_vICMS',
    'FormaAdd', 'MovimCod', 'Qtde', 'CustoT'], [
    'AnoMes', 'Empresa', 'Linha'], [
    Ano, FatID, FatNum,
    Id, ide_serie, ide_nNF,
    ide_dEmi, ide_hEmi,
    ICMSTot_vNF, ICMSTot_vCOFINS,
    ICMSTot_vPIS, ICMSTot_vIPI, ICMSTot_vICMS,
    FormaAdd, MovimCod, Qtde, CustoT], [
    AnoMes, Empresa, Linha], True) then
    begin
      //
    end;
  end;
var
  Linha: Integer;
  Corda: String;
begin
  Screen.Cursor := crHourGlass;
  try
    PCDados.ActivePageIndex := 0;
    Linha := 0;
    if not DefineEmpresaEAnoMes() then
      Exit;
    //
    // se j� existe um per�odo criado
    if QrDreGerVen.State <> dsInactive then
    begin
      // verifica se tem NFe sem FisRegCad (importadads para teste)
      UnDmkDAC_PF.AbreMySQLQuery0(QrSemFRC, Dmod.MyDB, [
      'SELECT nfea.ide_nNF NF ',
      'FROM nfecaba nfea ',
      'WHERE nfea.ide_dEmi BETWEEN ' + FIntervaloPeriodo,
      'AND nfea.FatID IN (' +
        Geral.FF0(VAR_FATID_0001) + ', ' +
        Geral.FF0(VAR_FATID_0050) +
        ') ',
      'AND nfea.Status=100 ',
      'AND nfea.FisRegCad=0 ',
      '']);
      if QrSemFRC.RecordCount > 0 then
      begin
        Corda := MyObjects.CordaDeQuery(QrSemFRC, 'NF', '');
        Geral.MB_Aviso('NF-es emitidas sem Regra Fiscal definida:' + slineBreak + Corda);
        Exit;
      end;
      // verifica se tem Packing List ou NFe orf�o:
      UnDmkDAC_PF.AbreMySQLQuery0(QrPLOrfaos, Dmod.MyDB, [
      'SELECT voc.MovimCod, voc.SerieV, voc.NFV, ',
      'DATE(voc.DtVenda) DATA, TIME(voc.DtVenda) HORA, ',
      'voc.Cliente, voc.ValorT, ',
      'IF(voc.AreaM2 <> 0, -voc.AreaM2, IF(voc.PesoKg <> 0, ',
      '-voc.PesoKg, -voc.Pecas)) Qtde, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Cliente ',
      'FROM vsoutcab voc',
      'LEFT JOIN entidades ent ON ent.Codigo=voc.Cliente',
      'WHERE voc.NFV < 1 ',
      'AND DATE(voc.DtVenda) BETWEEN ' + FIntervaloPeriodo,
      '']);
      UnDmkDAC_PF.AbreMySQLQuery0(QrNFeOrfaos, Dmod.MyDB, [
      'SELECT voc.MovimCod, nfea.FatNum, nfea.ide_serie, nfea.ide_nNF, ',
      'nfea.ide_dEmi, nfea.ide_hEmi, nfea.ICMSTot_vNF,',
      'nfea.CodInfoDest, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Cliente ',
      'FROM nfecaba nfea',
      'LEFT JOIN fisregcad frc ON frc.Codigo=nfea.FisRegCad',
      'LEFT JOIN entidades ent ON ent.Codigo=nfea.CodInfoDest',
      'LEFT JOIN vsoutcab voc ON voc.SerieV=nfea.ide_serie',
      '  AND voc.NFV=nfea.ide_nNF',
      '  AND DATE(voc.DtVenda) BETWEEN ' + FIntervaloPeriodo,
      'WHERE nfea.ide_dEmi BETWEEN ' + FIntervaloPeriodo,
      'AND nfea.FatID IN (' +
        Geral.FF0(VAR_FATID_0001) + ', ' +
        Geral.FF0(VAR_FATID_0050) +
        ') ',
      'AND nfea.Status=100',
      'AND frc.IntegraDRE=' + Geral.FF0(Integer(TRegFisCadIntDRE.rfcidVenda)),
      'AND voc.MovimCod IS NULL',
      '']);
      //Geral.MB_Teste(QrNFeOrfaos.SQL.Text);
      if (QrPLOrfaos.RecordCount > 0) or (QrNFeOrfaos.RecordCount > 0) then
      begin
        MyObjects.frxDefineDataSets(frxDRE_GEREN_001_Orfaos, [
          DModG.frxDsDono,
          frxDsPLOrfaos,
          frxDsNFeOrfaos
          ]);
        MyObjects.frxMostra(frxDRE_GEREN_001_Orfaos, 'DRE - PL e/ou NFe orf�os');
        //
        Exit;
      end;
      if QrDreGerVen.RecordCount > 0 then
      begin
        if Geral.MB_Pergunta('Os itens de de vendas previamente inseridos ser�o excluidos!' +
        sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'DELETE FROM dregerven ',
          'WHERE AnoMes=' + Geral.FF0(QrDreGerCabAnoMes.Value),
          'AND Empresa=' + Geral.FF0(QrDreGerCabEmpresa.Value),
          '']);
        end else
        begin
          Geral.MB_Info('A��o cancelada!');
          Exit;
        end
      end;
      // inclus�es autom�ticas
      UnDmkDAC_PF.AbreMySQLQuery0(QrVen, Dmod.MyDB, [

      'SELECT voc.MovimCod, voc.ValorT, ',
      'IF(voc.AreaM2 <> 0, -voc.AreaM2, IF(voc.PesoKg <> 0, ',
      '-voc.PesoKg, -voc.Pecas)) Qtde, ',
      'nfea.FatID, nfea.FatNum, nfea.ide_serie, nfea.ide_nNF, ',
      'nfea.ide_dEmi, nfea.ide_hEmi, ',
      'nfea.Id, nfea.ICMSTot_vICMS,',
      'nfea.ICMSTot_vIPI, nfea.ICMSTot_vPIS,',
      'nfea.ICMSTot_vCOFINS, nfea.ICMSTot_vNF,',
      'nfea.CodInfoDest, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Cliente ',
      'FROM nfecaba nfea',
      'LEFT JOIN fisregcad frc ON frc.Codigo=nfea.FisRegCad',
      'LEFT JOIN entidades ent ON ent.Codigo=nfea.CodInfoDest',
      'LEFT JOIN vsoutcab voc ON voc.SerieV=nfea.ide_serie',
      '  AND voc.NFV=nfea.ide_nNF',
      //'  AND DATE(voc.DtVenda) BETWEEN ' + FIntervaloPeriodo,
      'WHERE nfea.ide_dEmi BETWEEN ' + FIntervaloPeriodo,
      'AND nfea.FatID IN (' +
        Geral.FF0(VAR_FATID_0001) + ', ' +
        Geral.FF0(VAR_FATID_0050) +
        ') ',
      'AND nfea.Status=100',
      'AND frc.IntegraDRE=' + Geral.FF0(Integer(TRegFisCadIntDRE.rfcidVenda)),
      //'AND voc.MovimCod IS NULL',
      '']);
      QrVen.First;
      while not QrVen.Eof do
      begin
        InsereNFAtual(Linha);
        //
        QrVen.Next;
      end;
    end;
    //
    ReopenDreGerVen(0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDreGerCab.ObterLanamentos1Click(Sender: TObject);
var
  DataCad_Txt, UserCad_Txt, FormaAdd: String;

  procedure PreparaLanctoAtual(const Linha, DreCfgCpc, CpcStatus: Integer; var Vals: String);
  begin
    if Vals <> EmptyStr then
      Vals := Vals + ', ';
    Vals := Vals + '(' +
      IntToStr(FAno_Int) + ',' +
      IntToStr(QrDreCfgEstControle.Value) + ',' +
      IntToStr(QrDreCfgCtdConta.Value) + ',' +
      IntToStr(DreCfgCpc) + ',' +
      IntToStr(CpcStatus) + ',' +
      IntToStr(QrDreCfgEstDreCfgTp1.Value) + ',' +
      IntToStr(QrDreCfgCtdDreCfgTp2.Value) + ',' +
      Geral.FDT_Aspas(QrLanctosData.Value, 1) + ',' +
      IntToStr(QrLanctosTipo.Value) + ',' +
      IntToStr(QrLanctosCarteira.Value) + ',' +
      IntToStr(QrLanctosControle.Value) + ',' +
      IntToStr(QrLanctosSub.Value) + ',' +
      IntToStr(QrLanctosGenero.Value) + ',' +
      IntToStr(QrLanctosCliente.Value) + ',' +
      IntToStr(QrLanctosFornecedor.Value) + ',' +
      Geral.FFT_Dot(QrLanctosDebito.Value, 2, siNegativo) + ',' +
      Geral.FFT_Dot(QrLanctosCredito.Value, 2, siNegativo) + ',' +
      Geral.FDT_Aspas(QrLanctosCompensado.Value, 1) + ',' +
      IntToStr(QrLanctosSit.Value) + ',' +
      Geral.FDT_Aspas(QrLanctosVencimento.Value, 1) + ',' +
      IntToStr(QrLanctosGenCtb.Value) + ',' +
      IntToStr(QrLanctosGenCtbD.Value) + ',' +
      IntToStr(QrLanctosGenCtbC.Value) + ',' +
      IntToStr(QrLanctosCodNiv1.Value) + ',' +
      IntToStr(QrLanctosCodNiv2.Value) + ',' +
      IntToStr(QrLanctosCodNiv3.Value) + ',' +
      IntToStr(QrLanctosCodNiv4.Value) + ',' +
      IntToStr(QrLanctosCodNiv5.Value) + ',' +
      IntToStr(QrLanctosCodNiv6.Value) + ',' +
      FormaAdd + ',' +
      //
      FAnoMes_Txt + ',' +
      FEmpresa_Txt + ',' +
      IntToStr(Linha) + ',' +
      //
      Geral.Aspas(QrLanctosSerieNF.Value) + ',' +
      IntToStr(QrLanctosNotaFiscal.Value) + ',' +
      Geral.Aspas(QrLanctosDescricao.Value) + ',' +
      //
      DataCad_Txt + ',' +
      UserCad_Txt + //',' +
    ')';
  end;
const
  SQL_Flds = 'INSERT INTO dregerfin (' +
    'Ano, DreCfgEst, DreCfgCtd, ' +
    'DreCfgCpc, CpCStatus, DreCfgTp1, ' +
    'DreCfgTp2, Data, Tipo, ' +
    'Carteira, Controle, Sub, ' +
    'Genero, Cliente, Fornecedor, ' +
    'Debito, Credito, Compensado, ' +
    'Sit, Vencimento, GenCtb, ' +
    'GenCtbD, GenCtbC, GenNiv1, ' +
    'GenNiv2, GenNiv3, GenNiv4, ' +
    'GenNiv5, GenNiv6, FormaAdd, ' +
    'AnoMes, Empresa, Linha, ' +
    'SerieNF, NotaFiscal, Descricao, ' +
    'DataCad, UserCad ' +
    ') VALUES ';//(';
var
  DreCfgCab, Linha, DreCfgCpc: Integer;
  SQL_Vals: String;
begin
  PCDados.ActivePageIndex := 2;
  PB1.Position := 0;
  DreCfgCpc := 0;
  //Linha     := ObterLinha();
  Linha     := 0;
  FormaAdd  := Geral.FF0(Integer(TdmkModoExec.dmodexAutomatico));
  if not DefineEmpresaEAnoMes() then
    Exit;
  // J� existe um periodo?
  if QrDreGerCab.RecordCount = 0 then Exit;
  // Define qual configura��o de DRE ir� usar!

  //if SelecionaDreCfgCab(DreCfgCab) then
  DreCfgCab := QrDreGerCabDreCfgCab.Value;
  if MyObjects.FIC(DreCfgCab = 0, nil, 'Configura��o de DRE n�o definida!') then Exit;
  //
  ReopenDreCfgCpc(TFormaOpenCPC.focpcTudo, DreCfgCab, 0);
  PB1.Position := 0;
  PB1.Max := QrDreCfgCpc.RecordCount;
  DataCad_Txt := Geral.FDT_Aspas(DModG.ObtemAgora(), 1);
  UserCad_Txt := Geral.FF0(VAR_USUARIO);
  //
  if QrDreGerVen.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('Os lan�amentos autom�ticos previamente inseridos ser�o excluidos!' +
    sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM dregerfin ',
      'WHERE AnoMes=' + Geral.FF0(QrDreGerCabAnoMes.Value),
      'AND Empresa=' + Geral.FF0(QrDreGerCabEmpresa.Value),
      'AND FormaAdd=' + FormaAdd,
      '']);
      // preparar lan�amentos avulsos para seq superior aos autom�ticos
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE dregerfin ',
      'SET Linha = Linha * -1 ',
      'WHERE AnoMes=' + Geral.FF0(QrDreGerCabAnoMes.Value),
      'AND Empresa=' + Geral.FF0(QrDreGerCabEmpresa.Value),
      '']);
    end else
    begin
      Geral.MB_Info('A��o cancelada!');
      Exit;
    end
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    ReopenDreCfgEst(DreCfgCab);
    QrDreCfgEst.First;
    while not QrDreCfgEst.Eof do
    begin
      ReopenDreCfgCtd();
      QrDreCfgCtd.First;
      while not QrDreCfgCtd.Eof do
      begin
        //if QrDreCfgCtdConta.Value = 14336 then
          //Geral.MB_Teste('14336');
        ReopenDreCfgCpc(TFormaOpenCPC.focpcItem, DreCfgCab, QrDreCfgCtdConta.Value);
        QrDreCfgCpc.First;
        while not QrDreCfgCpc.Eof do
        begin
          MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
          //if QrDreCfgCpcStatus.Value in ([1,3]) then
          if TDreCfgCpcStatus(QrDreCfgCpcStatus.Value) in ([dccsAutoIncluso, dccsAutoInclusoFilho]) then
          begin
            if QrDreCfgCpcIDXSUB4.Value <> 0 then
              DreCfgCpc := QrDreCfgCpcIDXSUB4.Value;
            //
            SQL_Vals := EmptyStr;
            ReopenLanctos();
            while not QrLanctos.Eof do
            begin
              Linha := Linha + 1;
              //
              //InsereLanctoAtual(Linha, DreCfgCpc);
              PreparaLanctoAtual(Linha, DreCfgCpc, QrDreCfgCpcStatus.Value, SQL_Vals);
              //
              QrLanctos.Next;
            end;
            if SQL_Vals <> EmptyStr then
            begin
              UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, SQL_Flds + SQL_Vals);
            end;
          end;
          //
          QrDreCfgCpc.Next;
        end;
        //
        QrDreCfgCtd.Next;
      end;
      //
      QrDreCfgEst.Next;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Linha ',
    'FROM dregerfin ',
    'WHERE AnoMes=' + Geral.FF0(QrDreGerCabAnoMes.Value),
    'AND Empresa=' + Geral.FF0(QrDreGerCabEmpresa.Value),
    'AND Linha < 0',
    'ORDER BY Linha DESC ',
    '']);
    PB1.Position := 0;
    PB1.Max := Dmod.QrAux.RecordCount;
    while not Dmod.QrAux.Eof do
    begin
      Linha := Linha + 1;
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE dregerfin ',
      'SET Linha = ' + Geral.FF0(Linha),
      'WHERE AnoMes=' + Geral.FF0(QrDreGerCabAnoMes.Value),
      'AND Empresa=' + Geral.FF0(QrDreGerCabEmpresa.Value),
      'AND Linha=' + Geral.FF0(Dmod.QrAux.Fields[0].AsInteger),
      '']);
      //
      Dmod.QrAux.Next;
    end;
    //
    CalculaTotais();
    ReopenDreGerFin(0, 0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmDreGerCab.ObterLinha(): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT MAX(Linha) Linha ',
  'FROM dregerfin',
  'WHERE Empresa=' + Geral.FF0(FEmpresa_Int),
  'AND AnoMes=' + Geral.FF0(FAnoMes_Int),
  '']);
  //
  Result := Dmod.QrAux.Fields[0].AsInteger + 1;
end;

procedure TFmDreGerCab.PCDadosChange(Sender: TObject);
begin
  BtAvulso.Visible := PCDados.ActivePageIndex = 3;
end;

procedure TFmDreGerCab.PMAvulsoPopup(Sender: TObject);
var
  EhAvulso: Boolean;
begin
  MyObjects.HabilitaMenuItemItsIns(LctInclui1, QrDreGerCab);

  EhAvulso := (QrDreGerFinAvu.State <> dsInactive) and (QrDreGerFinAvu.RecordCount > 0)
    and (TDreCfgCpcStatus(QrDreGerFinAvuCpcStatus.Value) = dccsAvulso);
  MyObjects.HabilitaMenuItemItsUpd(LctAltera1, QrDreGerFinAvu, EhAvulso);
  MyObjects.HabilitaMenuItemItsDel(LctExclui1, QrDreGerFinAvu, EhAvulso);
end;

procedure TFmDreGerCab.PMPeriodoPopup(Sender: TObject);
begin
{
  Excluiperiodoselecionado1.Enabled :=
    (QrDreGerCab.State <> dsInactive) and (QrDreGerCab.RecordCount > 0)
  and
    (DfSEII_v03_0_2_a.QrE100.State <> dsInactive) and
    (DfSEII_v03_0_2_a.QrE100.RecordCount = 0);
}
end;

procedure TFmDreGerCab.QrDreGerCabAfterOpen(DataSet: TDataSet);
begin
  if QrDreGerCab.RecordCount > 0 then
    DefineEmpresaEAnoMes();
  //
  BtObter.Enabled := QrDreGerCab.RecordCount > 0;
end;

procedure TFmDreGerCab.QrDreGerCabAfterScroll(DataSet: TDataSet);
begin
  DefineEmpresaEAnoMes();
  ReopenDreGerVen(0);
  ReopenDreGerDev(0);
  ReopenDreGerFin(0, 0);
end;

procedure TFmDreGerCab.QrDreGerCabBeforeClose(DataSet: TDataSet);
begin
  QrDreGerVen.Close;
  QrDreGerDev.Close;
  QrDreGerFinAut.Close;
  QrDreGerFinAvu.Close;
end;

procedure TFmDreGerCab.ReopenDreCfgCpc(Forma: TFormaOpenCPC; DreCfgCab, Conta: Integer);
var
  sPAC, sEx0, sEx1, sConta, sCodigo: String;
  //
  function DefineCodigosSQLs(): String;
  var
    I: Integer;
    s: String;
  begin
    //
    case Forma of
      TFormaOpenCPC.focpcTudo:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrDCC, Dmod.MyDB, [
        'SELECT cpc.PlaAllCad, cpc.Excluso',
        'FROM drecfgcpc cpc',
        'WHERE cpc.Codigo=' + Geral.FF0(DreCfgCab),
        '']);
      end;
      TFormaOpenCPC.focpcItem:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrDCC, Dmod.MyDB, [
        'SELECT cpc.PlaAllCad, cpc.Excluso',
        'FROM drecfgcpc cpc',
        'WHERE cpc.Conta=' + Geral.FF0(Conta),
        '']);
      end;
    end;
    //
    //Geral.MB_Teste(QrDCC.SQL.Text);
    //
    if QrDCC.RecordCount > 0 then
    begin
      sPAC := EmptyStr;
      sEx0 := EmptyStr;
      sEx1 := EmptyStr;
      try
        QrDCC.DisableControls;
        QrDCC.First;
        while not QrDCC.Eof do
        begin
          s := Geral.FF0(QrDCCPlaAllCad.Value);
          //
          if sPAC <> '' then
            sPAC := sPAC + ', ';
          sPAC := sPAC + s;
          //
          if QrDCCExcluso.Value = 0 then
          begin
            if sEx0 <> '' then
              sEx0 := sEx0 + ', ';
            sEx0 := sEx0 + s;
          end else
          begin
            if sEx1 <> '' then
              sEx1 := sEx1 + ', ';
            sEx1 := sEx1 + s;
          end;
          QrDCC.Next;
        end;
      finally
        QrDCC.EnableControls;
      end;
    end;
    if sPAC = EmptyStr then
      sPAC := '-999999999';
    if sEx0 = EmptyStr then
      sEx0 := '-999999999';
    if sEx1 = EmptyStr then
      sEx1 := '-999999999';
  end;
  //
begin
  DefineCodigosSQLs();
  //
  sConta  :=  Geral.FF0(Conta);
  sCodigo :=  Geral.FF0(DreCfgCab);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgCpc, Dmod.MyDB, [
  //  'SELECT cpc.IdxSub4, cpc.Excluso, ',
  'SELECT ',
  ' IF(cpc.Conta=' + sConta + ', cpc.IdxSub4, NULL) IDXSUB4, ',
  ' IF(cpc.Conta=' + sConta + ', cpc.Excluso, NULL) EXCLUSO, ',
  'IF(cpc.MesCompet IS NULL, 0, cpc.MesCompet) MesCompet,  ',
  //
  'IF(pac.Codigo IN (' + sEx0 + '), 1,',
  '  IF(pac.Codigo IN (' + sEx1 + '), 2, ',
  '  IF(pac.CodNiv5 IN (' + sEx1 + '), 4,',
  '  IF(pac.CodNiv5 IN (' + sEx0 + '), 3,',
  '  IF(pac.CodNiv4 IN (' + sEx1 + '), 4,',
  '  IF(pac.CodNiv4 IN (' + sEx0 + '), 3,',
  '  IF(pac.CodNiv3 IN (' + sEx1 + '), 4,',
  '  IF(pac.CodNiv3 IN (' + sEx0 + '), 3,',
  '  IF(pac.CodNiv2 IN (' + sEx1 + '), 4,',
  '  IF(pac.CodNiv2 IN (' + sEx0 + '), 3,',
  '  IF(pac.CodNiv1 IN (' + sEx1 + '), 4,',
  '  IF(pac.CodNiv1 IN (' + sEx0 + '), 3,',
  '  9))))))))))))',
  'Status,',
  'pac.Codigo PlaAllcad, pac.Nome, pac.Ordens ',
  'FROM plaallcad pac ',
  'LEFT JOIN drecfgcpc cpc ON pac.Codigo=cpc.PlaAllCad ',
  'WHERE ',
  '( ',
  '  pac.Codigo IN (' + sPAC + ')',
  '  OR pac.CodNiv1 IN (' + sPAC + ')',
  '  OR pac.CodNiv2 IN (' + sPAC + ')',
  '  OR pac.CodNiv3 IN (' + sPAC + ')',
  '  OR pac.CodNiv4 IN (' + sPAC + ')',
  '  OR pac.CodNiv5 IN (' + sPAC + ')',
  ') ',
  'AND ( ',
  '  (cpc.Conta=' + sConta + ')',
  '  OR (cpc.Conta IS NULL) ',
  '  OR (cpc.Conta<>' + sConta + ' AND cpc.Codigo <> ' + sCodigo,
  '    AND NOT (pac.Codigo IN (' + sPAC + ')))  ',
  ') ',
  //
  'ORDER BY Ordens',
  '']);
  //if sConta = '13' then
    //Geral.MB_Teste(QrDreCfgCpc.SQL.Text);
end;

procedure TFmDreGerCab.ReopenDreCfgCtd();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgCtd, Dmod.MyDB, [
  'SELECT * ',
  'FROM drecfgctd ',
  'WHERE Controle=' + Geral.FF0(QrDreCfgEstControle.Value),
  '']);
  //Geral.MB_Teste(QrDreCfgCtd.SQL.Text);
end;

procedure TFmDreGerCab.ReopenDreCfgEst(DreCfgCab: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgEst, Dmod.MyDB, [
  'SELECT * ',
  'FROM drecfgest ',
  'WHERE Codigo=' + Geral.FF0(DreCfgCab),
  'ORDER BY Ordem, Controle ',
  '']);
  //Geral.MB_Teste(QrDreCfgEst.SQL.Text);
end;

procedure TFmDreGerCab.ReopenDreGerCab(AnoMes: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreGerCab, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
  'CONCAT(RIGHT(dgc.AnoMes, 2), "/", ',
  'LEFT(LPAD(dgc.AnoMes, 6, "0"), 4)) MES_ANO,',
  //'dcc.Nome NO_DreCfgCab, dgc.* ',
  'dgc.* ',
  'FROM dregercab dgc',
  'LEFT JOIN entidades ent ON ent.Codigo=dgc.Empresa',
  //'LEFT JOIN drecfgcab dcc ON dcc.Codigo=dgc.DreCfgCab',
  'ORDER BY AnoMes DESC ',
  '']);
end;

procedure TFmDreGerCab.ReopenDreGerDev(Linha: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreGerDev, Dmod.MyDB, [
  'SELECT dgd.* ',
  'FROM dregerdev dgd',
  'WHERE AnoMes=' + Geral.FF0(QrDreGerCabAnoMes.Value),
  'AND Empresa=' + Geral.FF0(QrDreGerCabEmpresa.Value),
  'ORDER BY dgd.Linha ',
  '']);
  if Linha > 0 then
    QrDreGerDev.Locate('Linha', Linha, []);
end;

procedure TFmDreGerCab.ReopenDreGerFin(LinAut, LinAvu: Integer);
  procedure Abre(Qry: TmySQLQuery; FormaAdd: TdmkModoExec; Linha: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT dgf.*,',
    'tp1.Nome NO_DreCfgTp1, tp2.Nome NO_DreCfgTp2, ',
    'pa1.Nome NO_PAC_Genero, pa2.Nome NO_PAC_Cpc,',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO ',
    'FROM dregerfin dgf',
    'LEFT JOIN drecfgtp1 tp1 ON tp1.Codigo=dgf.DreCfgTp1',
    'LEFT JOIN drecfgtp2 tp2 ON tp2.Controle=dgf.DreCfgTp2',
    'LEFT JOIN drecfgcpc dcc ON dcc.IdxSub4=dgf.DreCfgCpc',
    'LEFT JOIN plaallcad pa1 ON pa1.Codigo=dgf.Genero',
    'LEFT JOIN plaallcad pa2 ON pa2.Codigo=dcc.PlaAllCad',
    'LEFT JOIN entidades ent ON ent.Codigo=IF(dgf.Fornecedor>0, ',
    '  dgf.Fornecedor, dgf.Cliente)',
    'WHERE dgf.AnoMes=' + Geral.FF0(QrDreGerCabAnoMes.Value),
    'AND dgf.Empresa=' + Geral.FF0(QrDreGerCabEmpresa.Value),
    'AND dgf.FormaAdd=' + Geral.FF0(Integer(FormaAdd)),
    'ORDER BY dgf.Linha ',
    '']);
    //
    if Linha > 0 then
      Qry.Locate('Linha', Linha, []);
  end;
begin
  Abre(QrDreGerFinAut, dmodexAutomatico, LinAut);
  Abre(QrDreGerFinAvu, dmodexManual, LinAvu);
end;

procedure TFmDreGerCab.ReopenDreGerVen(Linha: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreGerVen, Dmod.MyDB, [
  'SELECT dgv.* ',
  'FROM dregerven dgv',
  'WHERE AnoMes=' + Geral.FF0(QrDreGerCabAnoMes.Value),
  'AND Empresa=' + Geral.FF0(QrDreGerCabEmpresa.Value),
  'ORDER BY dgv.Linha ',
  '']);
  if Linha > 0 then
    QrDreGerVen.Locate('Linha', Linha, []);
end;

procedure TFmDreGerCab.ReopenLanctos();
var
  SQL_Periodo: String;
begin
  if QrDreCfgCpcMesCompet.Value = 0 then
    SQL_Periodo := FIntervaloPeriodo
  else
    SQL_Periodo := FIntervaloSeguinte;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLanctos, Dmod.MyDB, [
  'SELECT lct.CliInt, lct.Data, lct.Tipo, lct.Carteira, ',
  'lct.Controle, lct.Sub, lct.Genero, ',
  'lct.Debito, lct.Credito, lct.Compensado, ',
  'lct.Sit, lct.Vencimento, lct.GenCtb, ',
  'lct.GenCtbD, lct.GenCtbC, lct.Cliente, lct.Fornecedor, ',
  'lct.SerieNF, lct.NotaFiscal, lct.Descricao, ',
  'pac.CodNiv1, pac.CodNiv2, pac.CodNiv3, ',
  'pac.CodNiv4, pac.CodNiv5, pac.CodNiv6',
  'FROM ' + FTabLctA + ' lct ',
  'LEFT JOIN plaallcad pac ON pac.Codigo=lct.Genero',
  'WHERE lct.CliInt=' + FEmpresa_txt,
  'AND lct.Genero=' + Geral.FF0(QrDreCfgCpcPlaAllcad.Value),
  'AND lct.Data BETWEEN ' + SQL_Periodo,
  'AND lct.ID_Pgto=0 ',
  ' ']);
  //if QrDreCfgCpcPlaAllcad.Value = 833 then
    //Geral.MB_Teste(QrLanctos.SQL.Text);
end;

procedure TFmDreGerCab.SbImprimeClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmDreGerImp, FmDreGerImp, afmoSoBoss) then
  begin
    FmDreGerImp.ShowModal;
    FmDreGerImp.Destroy;
  end;
end;

procedure TFmDreGerCab.SbNovoClick(Sender: TObject);
begin
  Contabil_Jan.MostraFormDreCfgCab();
end;

(*
function TFmDreGerCab.SelecionaDreCfgCab(var DreCfgCab: Integer): Boolean;
const
  Aviso  = '...';
  Titulo = 'Sele��o de Modelo de DRE';
  Prompt = 'Informe o Modelo';
  Campo  = 'Descricao';
var
  Resp: Variant;
begin
  Result := False;
  DreCfgCab := 0;
  //
  Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo Codigo, Nome Descricao ',
    'FROM drecfgcab ',
    'ORDER BY Nome ',
    ''], Dmod.MyDB, True);
  //
  if Resp <> Null then
  begin
    Result := True;
    DreCfgCab := Resp;
  end;
end;
*)

end.
