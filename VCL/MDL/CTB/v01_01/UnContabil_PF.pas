unit UnContabil_PF;


interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, UnMsgInt, Db,
  DbCtrls, Buttons, ZCF2, mySQLDbTables, ComCtrls, Grids, DBGrids, CommCtrl,
  Consts, UnDmkProcFunc, Variants, MaskUtils, frxClass, frxPreview,
  mySQLExceptions,
  // Dermatek
  dmkGeral, dmkEdit, dmkDBEdit, dmkEditF7, dmkEditDateTimePicker, dmkCheckGroup,
  dmkDBLookupCombobox, dmkEditCB, dmkDBGrid, dmkDBGridDAC, dmkImage, UnDmkEnums;

type
  TUnContabil_PF = class(TObject)
  private
    { Private declarations }
    procedure AtualizaPagoParcial(Controle, Sub: Integer; OriCredito, OriDebito:
              Double; DtCompensado: TDateTime; FTabLctA, sDiaSeguinte: String);
    procedure MostraFormCtbBalPLctEdit(FTabLctA, SQL: String);
    procedure InsereLctOpnGeneroAtual(FCodigo: Integer; QrLcts: TmySQLQuery);
    procedure InsereLctOpnGenCtbAtual(FCodigo: Integer; QrLcts: TmySQLQuery);
    function  InsereCtbBalPIts(SQLType: TSQLType; Codigo, Controle, TpBalanc,
              TpAmbito: Integer; Credito, Debito: Double; APlaCod, ACjtCod,
              AGruCod, ASgrCod, ACtaCod, BPlaCod, BCjtCod, BGruCod, BSgrCod,
              BCtaCod: Integer; APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
              BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt: String): Boolean;
    procedure ReopenCtbBalPIts(QrCtbBalPIts: TmySQLQuery; SQL_Extra: String; Controle: Integer);
  public
    { Public declarations }
    procedure DefineContasCredor(TypCtbCadMoF: TTypCtbCadMoF; Credor:
              Integer; EdGenCtbD: TdmkEditCB; CBGenCtbD: TdmkDBLookupComboBox;
              EdGenCtbC: TdmkEditCB; CBGenCtbC: TdmkDBLookupComboBox);
    procedure DefineContasDevedor(TypCtbCadMoF: TTypCtbCadMoF; Devedor:
              Integer; EdGenCtbD: TdmkEditCB; CBGenCtbD: TdmkDBLookupComboBox;
              EdGenCtbC: TdmkEditCB; CBGenCtbC: TdmkDBLookupComboBox);
    procedure RecriaItensFinanceiro(QrOrfaos, QrLcts, QrAux, QrCtbBalPIts:
              TmySQLQuery; LaAviso1, LaAviso2: TLabel; PB1: TProgressBar;
              FCodigo: Integer; FTabLctA, FsCodigo, FsEmpresa, FsPrimeiroDia,
              FsDiaSeguinte: String);
    function  DefineGenCtbsPeloGrupo(TypCtbCadMoF: TTypCtbCadMoF; const
              CtbCadGru: Integer; var GenCtbD, GenCtbC: Integer): Boolean;

    (*
    procedure ReopenDreCfgEst(QrDreCfgEst: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenDreCfgCtd(QrDreCfgCtd: TmySQLQuery; Controle, Conta: Integer);
    procedure ReopenDreCfgCpc(QrDreCfgCpc: TmySQLQuery; Conta, IdxSub4: Integer);
    *)
    end;

var
  Contabil_PF: TUnContabil_PF;

implementation

uses MyDBCheck, Module, DmkDAC_PF, UMySQLModule, UnMyObjects, CtbBalPLctEdit,
  ModuleFin, UnFinanceiro;


{ TUnContabil_PF }

procedure TUnContabil_PF.AtualizaPagoParcial(Controle, Sub: Integer; OriCredito,
  OriDebito: Double; DtCompensado: TDateTime; FTabLctA, sDiaSeguinte: String);
var
  SaldoCred, SaldoDeb, Pago: Double;
  Multa, Juros, Desconto: Double;
  Sit, QtDtPg: Integer;
  Compensado: String;
  //
  QrSoma: TmySQLQuery;
begin
  QrSoma := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrSoma, Dmod.MyDB, [
    'SELECT SUM(Credito) Credito, SUM(Debito) Debito, ',
    'SUM(MultaVal) Multa, SUM(MoraVal) Juros, SUM(DescoVal) Desconto, ',
    '(IF(SUM(Credito) - SUM(Debito) >= 0, SUM(Credito) - SUM(Debito), ',
    '(SUM(Credito) - SUM(Debito)) * -1) - SUM(MultaVal) - SUM(MoraVal) + SUM(DescoVal)) ValLiq, ',
    'COUNT(DISTINCT(Data)) QtDtPg ',
    'FROM ' + FTabLctA,
    'WHERE ID_Pgto=' + Geral.FF0(Controle),
    'AND Data < "' + sDiaSeguinte + '"',
    '']);
    //
    Multa    := QrSoma.FieldByName('Multa').AsFloat;
    Juros    := QrSoma.FieldByName('Juros').AsFloat;
    Desconto := QrSoma.FieldByName('Desconto').AsFloat;
    QtDtPg   := QrSoma.FieldByName('QtDtPg').AsInteger;
    //
    if OriDebito = 0 then
      SaldoDeb := 0
    else
      SaldoDeb := QrSoma.FieldByName('ValLiq').AsFloat - OriDebito;
    //
    if OriCredito = 0 then
      SaldoCred := 0
    else
      SaldoDeb := QrSoma.FieldByName('ValLiq').AsFloat - OriCredito;
    //
    Pago := QrSoma.FieldByName('Credito').AsFloat - QrSoma.FieldByName('Debito').AsFloat;
    //
    if (SaldoCred >= 0) and (SaldoDeb >= 0) then
      Sit := 2
    else if (QrSoma.FieldByName('Debito').AsFloat <> 0) or (QrSoma.FieldByName('Credito').AsFloat <> 0) then
      Sit := 1
    else
      Sit := 0;
    //
    if Sit < 2 then
      Compensado := '0000-00-00'
    else
      Compensado := Geral.FDT(DtCompensado, 1);
    //
    QrSoma.Close;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctbbalplctopn', False, [
    'Sit', 'Pago', 'PagJur',
    'PagMul', 'RecDes', 'QtDtPg',
    'Compensado'], [
    'Controle'], [
    Sit, Pago, Juros,
    Multa, Desconto, QtDtPg, Compensado], [
    Controle], True);
    //
  finally
    QrSoma.Free;
  end;
end;

procedure TUnContabil_PF.DefineContasCredor(TypCtbCadMoF: TTypCtbCadMoF;
  Credor: Integer; EdGenCtbD: TdmkEditCB; CBGenCtbD: TdmkDBLookupComboBox;
  EdGenCtbC: TdmkEditCB; CBGenCtbC: TdmkDBLookupComboBox);
var
  CtbPlaCta, CtbCadGru, Conta, ContC: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrGenCtbs, Dmod.MyDB, [
  'SELECT ent.CtbPlaCta, ent.CtbCadGru ',
  'FROM entidades ent ',
  'WHERE ent.Codigo=' + Geral.FF0(Credor),
  '']);
  CtbPlaCta := DModFin.QrGenCtbsCtbPlaCta.Value;
  CtbCadGru := DModFin.QrGenCtbsCtbCadGru.Value;
  if CtbCadGru > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrGruCtbs, Dmod.MyDB, [
    'SELECT cci.Conta, cci.ContC',
    'FROM ctbcadits cci',
    'WHERE cci.Codigo=' + Geral.FF0(CtbCadGru),
    'AND ctbcadmof=' + Geral.FF0(Integer(TypCtbCadMoF)),
    '']);
    Conta := DModFin.QrGruCtbsConta.Value;
    ContC := DModFin.QrGruCtbsContC.Value;
    if Conta <> 0 then
    begin
      EdGenCtbD.ValueVariant := Conta;
      CBGenCtbD.KeyValue := Conta;
    end;
    //
    if ContC <> 0 then
    begin
      EdGenCtbC.ValueVariant := ContC;
      CBGenCtbC.KeyValue := ContC;
    end;
  end else
  if CtbPlaCta > 0 then
  begin
    EdGenCtbC.ValueVariant := DModFin.QrGenCtbsCtbPlaCta.Value;
    CBGenCtbC.KeyValue := DModFin.QrGenCtbsCtbPlaCta.Value;
  end;
end;

procedure TUnContabil_PF.DefineContasDevedor(TypCtbCadMoF: TTypCtbCadMoF;
  Devedor: Integer; EdGenCtbD: TdmkEditCB; CBGenCtbD: TdmkDBLookupComboBox;
  EdGenCtbC: TdmkEditCB; CBGenCtbC: TdmkDBLookupComboBox);
var
  CtbPlaCta, CtbCadGru, Conta, ContC, TipoCtb: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrGenCtbs, Dmod.MyDB, [
  'SELECT ent.CtbPlaCta, ent.CtbCadGru ',
  'FROM entidades ent ',
  'WHERE ent.Codigo=' + Geral.FF0(Devedor),
  '']);
  CtbPlaCta := DModFin.QrGenCtbsCtbPlaCta.Value;
  CtbCadGru := DModFin.QrGenCtbsCtbCadGru.Value;
  if CtbCadGru > 0 then
  begin
    if TypCtbCadMoF = TTypCtbCadMoF.tccmfIndef then
    begin
      TipoCtb := MyObjects.SelRadioGroup('Tipo de item de Grupo de Cont�bil',
      'Escolha o tipo de lan�amento',
      sTypCtbCadMoF, 2, -1, (*SelOnClick*)True);
    end else
      TipoCtb := Integer(TypCtbCadMoF);
    if TipoCtb > 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrGruCtbs, Dmod.MyDB, [
      'SELECT cci.Conta, cci.ContC',
      'FROM ctbcadits cci',
      'WHERE cci.Codigo=' + Geral.FF0(CtbCadGru),
      'AND ctbcadmof=' + Geral.FF0(TipoCtb),
      '']);
      Conta := DModFin.QrGruCtbsConta.Value;
      ContC := DModFin.QrGruCtbsContC.Value;
    end;
    if Conta <> 0 then
    begin
      EdGenCtbD.ValueVariant := Conta;
      CBGenCtbD.KeyValue := Conta;
    end;
    //
    if ContC <> 0 then
    begin
      EdGenCtbC.ValueVariant := ContC;
      CBGenCtbC.KeyValue := ContC;
    end;
    if (Conta <> 0) or (ContC <> 0) then
      Exit;
  end;
  //
  if CtbPlaCta > 0 then
  begin
    EdGenCtbD.ValueVariant := DModFin.QrGenCtbsCtbPlaCta.Value;
    CBGenCtbD.KeyValue := DModFin.QrGenCtbsCtbPlaCta.Value;
  end;
end;

function TUnContabil_PF.DefineGenCtbsPeloGrupo(TypCtbCadMoF: TTypCtbCadMoF;
  const CtbCadGru: Integer; var GenCtbD, GenCtbC: Integer): Boolean;
begin
  Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrGruCtbs, Dmod.MyDB, [
  'SELECT cci.Conta, cci.ContC',
  'FROM ctbcadits cci',
  'WHERE cci.Codigo=' + Geral.FF0(CtbCadGru),
  'AND ctbcadmof=' + Geral.FF0(Integer(TypCtbCadMoF)),
  '']);
  GenCtbD := DModFin.QrGruCtbsConta.Value;
  GenCtbC := DModFin.QrGruCtbsContC.Value;
  if (GenCtbD <> 0) or (GenCtbC <> 0) then
    Result := True;
end;

function TUnContabil_PF.InsereCtbBalPIts(SQLType: TSQLType; Codigo, Controle,
  TpBalanc, TpAmbito: Integer; Credito, Debito: Double; APlaCod, ACjtCod,
  AGruCod, ASgrCod, ACtaCod, BPlaCod, BCjtCod, BGruCod, BSgrCod,
  BCtaCod: Integer; APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt, BPlaTxt,
  BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt: String): Boolean;
begin
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctbbalpits', False, [
  'Codigo', 'TpBalanc', 'TpAmbito',
  'Credito', 'Debito',
  'APlaCod', 'ACjtCod', 'AGruCod',
  'ASgrCod', 'ACtaCod', 'BPlaCod',
  'BCjtCod', 'BGruCod', 'BSgrCod',
  'BCtaCod', 'APlaTxt', 'ACjtTxt',
  'AGruTxt', 'ASgrTxt', 'ACtaTxt',
  'BPlaTxt', 'BCjtTxt', 'BGruTxt',
  'BSgrTxt', 'BCtaTxt'], [
  'Controle'], [
  Codigo, TpBalanc, TpAmbito,
  Credito, Debito,
  APlaCod, ACjtCod, AGruCod,
  ASgrCod, ACtaCod, BPlaCod,
  BCjtCod, BGruCod, BSgrCod,
  BCtaCod, APlaTxt, ACjtTxt,
  AGruTxt, ASgrTxt, ACtaTxt,
  BPlaTxt, BCjtTxt, BGruTxt,
  BSgrTxt, BCtaTxt], [
  Controle], True) then
  begin
    (*
    n�o d�!!!
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      'UPDATE ' + FTabLctA + ' SET BalPatr=' + Geral.FF0(
      FBalPatr) + ' WHERE Controle'
    *)
  end;
end;

procedure TUnContabil_PF.InsereLctOpnGenCtbAtual(FCodigo: Integer; QrLcts: TmySQLQuery);
var
  APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
  BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt: String;
  Codigo, Controle, TpBalanc, TpAmbito,
  APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod,
  BPlaCod, BCjtCod, BGruCod, BSgrCod, BCtaCod: Integer;
  Credito, Debito: Double;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := FCodigo;
  Controle       := UMyMod.BPGS1I32('ctbbalpits', 'Controle', '', '', tsPos, SQLType, 0);
  TpBalanc       := Integer(TContabTpBalanc.ctbLctGenCtb);
  TpAmbito       := Integer(TContabTpAmbito.ctaLctGenOpn);
  Credito        :=  QrLCts.FieldByName('Debito').AsFloat;  // Inverte???
  Debito         :=  QrLCts.FieldByName('Credito').AsFloat; // Inverte???
  APlaCod        := 0; // QrLCts.FieldByName('APlaCod').AsInteger;
  ACjtCod        := 0; // QrLCts.FieldByName('ACjtCod').AsInteger;
  AGruCod        := 0; // QrLCts.FieldByName('AGruCod').AsInteger;
  ASgrCod        := 0; // QrLCts.FieldByName('ASgrCod').AsInteger;
  ACtaCod        := 0; // QrLCts.FieldByName('ACtaCod').AsInteger;
  BPlaCod        :=  QrLCts.FieldByName('BPlaCod').AsInteger;
  BCjtCod        :=  QrLCts.FieldByName('BCjtCod').AsInteger;
  BGruCod        :=  QrLCts.FieldByName('BGruCod').AsInteger;
  BSgrCod        :=  QrLCts.FieldByName('BSgrCod').AsInteger;
  BCtaCod        :=  QrLCts.FieldByName('BCtaCod').AsInteger;
  APlaTxt        := ''; // QrLCts.FieldByName('APlaTxt').AsString;
  ACjtTxt        := ''; // QrLCts.FieldByName('ACjtTxt').AsString;
  AGruTxt        := ''; // QrLCts.FieldByName('AGruTxt').AsString;
  ASgrTxt        := ''; // QrLCts.FieldByName('ASgrTxt').AsString;
  ACtaTxt        := ''; // QrLCts.FieldByName('ACtaTxt').AsString;
  BPlaTxt        :=  QrLCts.FieldByName('BPlaTxt').AsString;
  BCjtTxt        :=  QrLCts.FieldByName('BCjtTxt').AsString;
  BGruTxt        :=  QrLCts.FieldByName('BGruTxt').AsString;
  BSgrTxt        :=  QrLCts.FieldByName('BSgrTxt').AsString;
  BCtaTxt        :=  QrLCts.FieldByName('BCtaTxt').AsString;
  //
  InsereCtbBalPIts(SQLType, Codigo, Controle, TpBalanc, TpAmbito, Credito,
  Debito, APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod, BPlaCod, BCjtCod,
  BGruCod, BSgrCod, BCtaCod, APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
  BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt);
end;

procedure TUnContabil_PF.InsereLctOpnGeneroAtual(FCodigo: Integer; QrLcts: TmySQLQuery);
var
  APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
  BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt: String;
  Codigo, Controle, TpBalanc, TpAmbito,
  APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod,
  BPlaCod, BCjtCod, BGruCod, BSgrCod, BCtaCod: Integer;
  Credito, Debito: Double;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := FCodigo;
  Controle       := UMyMod.BPGS1I32('ctbbalpits', 'Controle', '', '', tsPos, SQLType, 0);
  TpBalanc       := Integer(TContabTpBalanc.ctbLctGenero);
  TpAmbito       := Integer(TContabTpAmbito.ctaLctGenOpn);
  Credito        := QrLCts.FieldByName('Credito').AsFloat;
  Debito         := QrLCts.FieldByName('Debito').AsFloat;
  APlaCod        := QrLCts.FieldByName('APlaCod').AsInteger;
  ACjtCod        := QrLCts.FieldByName('ACjtCod').AsInteger;
  AGruCod        := QrLCts.FieldByName('AGruCod').AsInteger;
  ASgrCod        := QrLCts.FieldByName('ASgrCod').AsInteger;
  ACtaCod        := QrLCts.FieldByName('ACtaCod').AsInteger;
  BPlaCod        := 0; // QrLCts.FieldByName('ABPlaCod').AsInteger;
  BCjtCod        := 0; // QrLCts.FieldByName('ABCjtCod').AsInteger;
  BGruCod        := 0; // QrLCts.FieldByName('ABGruCod').AsInteger;
  BSgrCod        := 0; // QrLCts.FieldByName('ABSgrCod').AsInteger;
  BCtaCod        := 0; // QrLCts.FieldByName('ABCtaCod').AsInteger;
  APlaTxt        := QrLCts.FieldByName('APlaTxt').AsString;
  ACjtTxt        := QrLCts.FieldByName('ACjtTxt').AsString;
  AGruTxt        := QrLCts.FieldByName('AGruTxt').AsString;
  ASgrTxt        := QrLCts.FieldByName('ASgrTxt').AsString;
  ACtaTxt        := QrLCts.FieldByName('ACtaTxt').AsString;
  BPlaTxt        := ''; // QrLCts.FieldByName('ABPlaTxt').AsString;
  BCjtTxt        := ''; // QrLCts.FieldByName('ABCjtTxt').AsString;
  BGruTxt        := ''; // QrLCts.FieldByName('ABGruTxt').AsString;
  BSgrTxt        := ''; // QrLCts.FieldByName('ABSgrTxt').AsString;
  BCtaTxt        := ''; // QrLCts.FieldByName('ABCtaTxt').AsString;
  //
  InsereCtbBalPIts(SQLType, Codigo, Controle, TpBalanc, TpAmbito, Credito,
  Debito, APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod, BPlaCod, BCjtCod,
  BGruCod, BSgrCod, BCtaCod, APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
  BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt);
end;

procedure TUnContabil_PF.MostraFormCtbBalPLctEdit(FTabLctA, SQL: String);
begin
  if DBCheck.CriaFm(TFmCtbBalPLctEdit, FmCtbBalPLctEdit, afmoNegarComAviso) then
  begin
    FmCtbBalPLctEdit.ImgTipo.SQLType := stUpd;
    FmCtbBalPLctEdit.FSQL            := SQL;
    FmCtbBalPLctEdit.FTabLctA        := FTabLctA;
    FmCtbBalPLctEdit.ReopenOrfaos();
    FmCtbBalPLctEdit.ShowModal;
    FmCtbBalPLctEdit.Destroy;
  end;
end;

procedure TUnContabil_PF.RecriaItensFinanceiro(QrOrfaos, QrLcts, QrAux,
  QrCtbBalPIts: TmySQLQuery; LaAviso1, LaAviso2: TLabel; PB1: TProgressBar; FCodigo: Integer;
  FTabLctA, FsCodigo, FsEmpresa, FsPrimeiroDia, FsDiaSeguinte: String);
const
  sInserindoFin = 'Inserindo lan�amentos financeiros. ';
var
  QtdFlds: Integer;
  TabTxt, FldGen, CamposDst: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ctbbalpits WHERE Codigo=' + FsCodigo +
    ' AND TpAmbito=' + Geral.FF0(Integer(TContabTpAmbito.ctaLctGenOpn))
  );
  //
  //
  //
  TabTxt := 'ctbbalplctopn';
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt + '. Copiando dados da origem (dados atuais).');
  CamposDst := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabTxt, 'lct.', QtdFlds);
  CamposDst := StringReplace(CamposDst, ' lct.Codigo', ' ' + FsCodigo + ' Codigo', [rfReplaceAll, rfIgnoreCase]);
  //
  //////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando se h� lan�amentos orf�os.');
  //////////////////////////////////////////////////////////////////////////////
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrfaos, Dmod.MyDB, [
  'SELECT cjt.Plano, gru.Conjunto, sgr.Grupo, ',
  'cta.Subgrupo, lct.Genero, cta.Nome NO_Cta, ',
  'lct.Data, lct.Tipo, lct.Carteira, lct.Controle, ',
  'lct.Sub, lct.Debito, lct.Credito, ',
  'lct.Compensado, lct.Sit, lct.Vencimento, lct.ID_Pgto, ',
  'lct.ID_Quit, lct.ID_Sub, lct.Pago, lct.GenCtb, ',
  'lct.QtDtPg, lct.PagMul, lct.PagJur , lct.RecDes,',
  'pla.Nome NO_Pla, cjt.Nome NO_Cjt, ',
  'gru.Nome NO_Gru, sgr.Nome NO_Sgr,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,',
  'crt.Nome NO_CARTEIRA',
  'FROM ' + FTabLctA + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=',
  '  IF(lct.Cliente <> 0, lct.Cliente, lct.Fornecedor)',
  'LEFT JOIN ' + TMeuDB + '.carteiras crt ON crt.Codigo=lct.Carteira  ',
  'LEFT JOIN ' + TMeuDB + '.contas    cta ON cta.Codigo=lct.Genero ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=cta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     pla ON pla.Codigo=cjt.Plano ',
  'WHERE crt.ForneceI=' + FsEmpresa,
  'AND lct.Genero > 0 ',
  'AND ( ',
  // Lan�amentos a prazo
  '  lct.Tipo = 2  ',
  '  AND lct.Data < "' + FsDiaSeguinte + '"  ',
  '  AND (  ',
  '    lct.Sit < 2  ',
  '    OR  ',
  '    lct.Compensado >= "' + FsDiaSeguinte + '"  ',
  '  ) ',
  // lancamentos � vista
  '  OR ( ',
  '  lct.Tipo < 2 ',
  '  )',
  ') ',
  // lan�amentos sem GenCtb
  'AND lct.GenCtb = 0 ',
  'ORDER BY NO_Cta, NO_ENT',
  '']);
  if QrOrfaos.RecordCount > 0 then
  begin
    // Mostra form para edira lan�amentos orf�os
    //Geral.MB_Teste(QrOrfaos.SQL.Text);
    MostraFormCtbBalPLctEdit(FTabLctA, QrOrfaos.SQL.Text);
    Exit;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt + '. Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ' + TabTxt + ' WHERE Codigo=' + FsCodigo);
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt + '. Gerando lan�amentos de emiss�es em aberto no final do per�odo.');
  //
  //////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  Geral.ATS([
  'INSERT INTO ' + TabTxt,
  'SELECT ',
  CamposDst,
  'FROM ' + FTabLctA + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.carteiras crt ON crt.Codigo=lct.Carteira  ',
  'WHERE crt.ForneceI=' + FsEmpresa,
  'AND lct.Genero > 0 ',
  'AND ( ',
  '  lct.Tipo = 2  ',
  '  AND lct.Data < "' + FsDiaSeguinte + '"  ',
  '  AND (  ',
  '    lct.Sit < 2  ',
  '    OR  ',
  '    lct.Compensado >= "' + FsDiaSeguinte + '"  ',
  '  ) ',
  ') ',
  '']));
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt + '. Alterando dados para ficarem na situa��o que estavam no fim do per�odo do balan�o.');
  //
  //////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + TabTxt,
  'WHERE QtDtPg > 1 ',
  '']);
  QrAux.First;
  while not QrAux.Eof do
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt +
    '. Controle ' + Geral.FF0(QrAux.FieldByName('Controle').AsInteger) +
    '. Alterando dados para ficarem na situa��o no fim do per�odo do balan�o.');
    //
    AtualizaPagoParcial(
      QrAux.FieldByName('Controle').AsInteger,
      QrAux.FieldByName('Sub').AsInteger,
      QrAux.FieldByName('Credito').AsFloat,
      QrAux.FieldByName('Debito').AsFloat,
      QrAux.FieldByName('Compensado').AsDateTime,
      FTabLctA, FsDiaSeguinte
    );
    QrAux.First;
    //
    QrAux.Next;
  end;

  //////////////////////////////////////////////////////////////////////////////
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Totais de lan�amentos financeiros');
  //
  //////////////////////////////////////////////////////////////////////////////
  FldGen := 'Genero';
  UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, Dmod.MyDB, [
  'SELECT SUM(lct.Credito) Credito, SUM(lct.Debito) Debito,  ',
  'acjt.Plano APlaCod, agru.Conjunto ACjtCod, ',
  'asgr.Grupo AGruCod, acta.Subgrupo ASgrCod,  ',
  'lct.Genero ACtaCod, bcjt.Plano BPlaCod,  ',
  'bgru.Conjunto BCjtCod, bsgr.Grupo BGruCod,  ',
  'bcta.SubGrupo BSgrCod, lct.GenCtb BCtaCod,  ',
  'apla.Nome APlaTxt, acjt.Nome ACjtTxt,  ',
  'agru.Nome AGruTxt, asgr.Nome ASgrTxt,  ',
  'acta.Nome ACtaTxt, bpla.Nome BPlaTxt,  ',
  'bcjt.Nome BCjtTxt, bgru.Nome BGruTxt,  ',
  'bsgr.Nome BSgrTxt, bcta.Nome BCtaTxt ',
  'FROM ' + TabTxt + ' lct  ',
  'LEFT JOIN ' + TMeuDB + '.carteiras crt ON crt.Codigo=lct.Carteira ',

  'LEFT JOIN ' + TMeuDB + '.contas    acta ON acta.Codigo=lct.Genero ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos asgr ON asgr.Codigo=acta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    agru ON agru.Codigo=asgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos acjt ON acjt.Codigo=agru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     apla ON apla.Codigo=acjt.Plano ',

  'LEFT JOIN ' + TMeuDB + '.contas    bcta ON bcta.Codigo=lct.GenCtb ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos bsgr ON bsgr.Codigo=bcta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    bgru ON bgru.Codigo=bsgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos bcjt ON bcjt.Codigo=bgru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     bpla ON bpla.Codigo=bcjt.Plano ',

  'WHERE lct.Codigo=' + FsCodigo,
  'AND crt.ForneceI=' + FsEmpresa,
  'AND lct.Codigo=' + FsCodigo,
  'AND lct.Data < "' + FsDiaSeguinte + '" ',
  'AND lct.Tipo = 2 ',
  'AND ( ',
  '  lct.Sit < 2 ',
  '  OR ',
  '  lct.Compensado >= "' + FsDiaSeguinte + '" ',
  ') ',
  'GROUP BY lct.' + FldGen,
  'ORDER BY lct.' + FldGen,
  '']);
  //
  //Geral.MB_Teste(QrLCts.SQL.Text);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, sInserindoFin);
  PB1.Position := 0;
  PB1.Max := QrLCts.RecordCount;
  QrLCts.First;
  while not QrLCts.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, sInserindoFin +
    IntToStr(QrLCts.RecNo));
    //
    InsereLctOpnGeneroAtual(FCodigo, QrLcts);
    //
    QrLCts.Next;
  end;
  //
(*
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ctbbalpits WHERE Codigo=' + sCodigo +
    ' AND TpAmbito=' + Geral.FF0(Integer(TContabTpAmbito.ctaLct...))
  );
*)
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Totais de lan�amentos cont�beis');
  //
  //////////////////////////////////////////////////////////////////////////////
  FldGen := 'GenCtb';
  UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, Dmod.MyDB, [
  'SELECT SUM(lct.Credito) Credito, SUM(lct.Debito) Debito,  ',
  'acjt.Plano APlaCod, agru.Conjunto ACjtCod, ',
  'asgr.Grupo AGruCod, acta.Subgrupo ASgrCod,  ',
  'lct.Genero ACtaCod, bcjt.Plano BPlaCod,  ',
  'bgru.Conjunto BCjtCod, bsgr.Grupo BGruCod,  ',
  'bcta.SubGrupo BSgrCod, lct.GenCtb BCtaCod,  ',
  'apla.Nome APlaTxt, acjt.Nome ACjtTxt,  ',
  'agru.Nome AGruTxt, asgr.Nome ASgrTxt,  ',
  'acta.Nome ACtaTxt, bpla.Nome BPlaTxt,  ',
  'bcjt.Nome BCjtTxt, bgru.Nome BGruTxt,  ',
  'bsgr.Nome BSgrTxt, bcta.Nome BCtaTxt ',
  'FROM ' + TabTxt + ' lct  ',
  'LEFT JOIN ' + TMeuDB + '.carteiras crt ON crt.Codigo=lct.Carteira ',

  'LEFT JOIN ' + TMeuDB + '.contas    acta ON acta.Codigo=lct.Genero ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos asgr ON asgr.Codigo=acta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    agru ON agru.Codigo=asgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos acjt ON acjt.Codigo=agru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     apla ON apla.Codigo=acjt.Plano ',

  'LEFT JOIN ' + TMeuDB + '.contas    bcta ON bcta.Codigo=lct.GenCtb ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos bsgr ON bsgr.Codigo=bcta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    bgru ON bgru.Codigo=bsgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos bcjt ON bcjt.Codigo=bgru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     bpla ON bpla.Codigo=bcjt.Plano ',

  'WHERE lct.Codigo=' + FsCodigo,
  'AND crt.ForneceI=' + FsEmpresa,
  'AND lct.Codigo=' + FsCodigo,
  'AND lct.Data < "' + FsDiaSeguinte + '" ',
  'AND lct.Tipo = 2 ',
  'AND ( ',
  '  lct.Sit < 2 ',
  '  OR ',
  '  lct.Compensado >= "' + FsDiaSeguinte + '" ',
  ') ',
  'GROUP BY lct.' + FldGen,
  'ORDER BY lct.' + FldGen,
  '']);
  //
  //Geral.MB_Teste(QrLCts.SQL.Text);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, sInserindoFin);
  PB1.Position := 0;
  PB1.Max := QrLCts.RecordCount;
  QrLCts.First;
  while not QrLCts.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, sInserindoFin +
    IntToStr(QrLCts.RecNo));
    //
    InsereLctOpnGenCtbAtual(FCodigo, QrLcts);
    //
    QrLCts.Next;
  end;
  // ...
  //
  ReopenCtbBalPIts(QrCtbBalPIts, '', 0);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TUnContabil_PF.ReopenCtbBalPIts(QrCtbBalPIts: TmySQLQuery;
  SQL_Extra: String; Controle: Integer);
begin
// Fazer?
end;

{
procedure TUnContabil_PF.ReopenDreCfgCpc(QrDreCfgCpc: TmySQLQuery; IdxSub4: Integer);
var
  sPAC, sEx0, sEx1, sConta, sCodigo: String;
  //
  function DefineCodigosSQLs(): String;
  var
    I: Integer;
    s: String;
  begin
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrDCC, Dmod.MyDB, [
    'SELECT cpc.PlaAllCad, cpc.Excluso',
    'FROM drecfgcpc cpc',
    'WHERE cpc.Conta=' + Geral.FF0(QrDreCfgCtdConta.Value),
    '']);
    //
    //Geral.MB_Teste(QrDCC.SQL.Text);
    //
    if QrDCC.RecordCount > 0 then
    begin
      sPAC := EmptyStr;
      sEx0 := EmptyStr;
      sEx1 := EmptyStr;
      try
        QrDCC.DisableControls;
        QrDCC.First;
        while not QrDCC.Eof do
        begin
          s := Geral.FF0(QrDCCPlaAllCad.Value);
          //
          if sPAC <> '' then
            sPAC := sPAC + ', ';
          sPAC := sPAC + s;
          //
          if QrDCCExcluso.Value = 0 then
          begin
            if sEx0 <> '' then
              sEx0 := sEx0 + ', ';
            sEx0 := sEx0 + s;
          end else
          begin
            if sEx1 <> '' then
              sEx1 := sEx1 + ', ';
            sEx1 := sEx1 + s;
          end;
          QrDCC.Next;
        end;
      finally
        QrDCC.EnableControls;
      end;
    end;
    if sPAC = EmptyStr then
      sPAC := '-999999999';
    if sEx0 = EmptyStr then
      sEx0 := '-999999999';
    if sEx1 = EmptyStr then
      sEx1 := '-999999999';
  end;
  //
begin
  DefineCodigosSQLs();
  //
  sConta  :=  Geral.FF0(QrDreCfgCtdConta.Value);
  sCodigo :=  Geral.FF0(QrDreCfgCabCodigo.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgCpc, Dmod.MyDB, [
(*
  'SELECT cpc.IdxSub4, cpc.Excluso, ',
  'IF(pac.Codigo IN (' + sEx0 + '), 0,',
  '  IF(pac.Codigo IN (' + sEx1 + '), 1, ',
  '  IF(pac.CodNiv5 IN (' + sEx1 + '), 3,',
  '  IF(pac.CodNiv5 IN (' + sEx0 + '), 2,',
  '  IF(pac.CodNiv4 IN (' + sEx1 + '), 3,',
  '  IF(pac.CodNiv4 IN (' + sEx0 + '), 2,',
  '  IF(pac.CodNiv3 IN (' + sEx1 + '), 3,',
  '  IF(pac.CodNiv3 IN (' + sEx0 + '), 2,',
  '  IF(pac.CodNiv2 IN (' + sEx1 + '), 3,',
  '  IF(pac.CodNiv2 IN (' + sEx0 + '), 2,',
  '  IF(pac.CodNiv1 IN (' + sEx1 + '), 3,',
  '  IF(pac.CodNiv1 IN (' + sEx0 + '), 2,',
  '  -1))))))))))))',
  'Status,',
  'pac.Codigo PlaAllcad, pac.Nome, pac.Ordens ',
  'FROM plaallcad pac ',
  'LEFT JOIN drecfgcpc cpc ON pac.Codigo=cpc.PlaAllCad ',
  'WHERE pac.Codigo IN (' + sPAC + ')',
  'OR pac.CodNiv1 IN (' + sPAC + ')',
  'OR pac.CodNiv2 IN (' + sPAC + ')',
  'OR pac.CodNiv3 IN (' + sPAC + ')',
  'OR pac.CodNiv4 IN (' + sPAC + ')',
  'OR pac.CodNiv5 IN (' + sPAC + ')',
  'ORDER BY Ordens',
*)
//  'SELECT cpc.IdxSub4, cpc.Excluso, ',
  'SELECT ',
  ' IF(cpc.Conta=' + sConta + ', cpc.IdxSub4, NULL) IDXSUB4, ',
  ' IF(cpc.Conta=' + sConta + ', cpc.Excluso, NULL) EXCLUSO, ',
  //
  'IF(pac.Codigo IN (' + sEx0 + '), 0,',
  '  IF(pac.Codigo IN (' + sEx1 + '), 1, ',
  '  IF(pac.CodNiv5 IN (' + sEx1 + '), 3,',
  '  IF(pac.CodNiv5 IN (' + sEx0 + '), 2,',
  '  IF(pac.CodNiv4 IN (' + sEx1 + '), 3,',
  '  IF(pac.CodNiv4 IN (' + sEx0 + '), 2,',
  '  IF(pac.CodNiv3 IN (' + sEx1 + '), 3,',
  '  IF(pac.CodNiv3 IN (' + sEx0 + '), 2,',
  '  IF(pac.CodNiv2 IN (' + sEx1 + '), 3,',
  '  IF(pac.CodNiv2 IN (' + sEx0 + '), 2,',
  '  IF(pac.CodNiv1 IN (' + sEx1 + '), 3,',
  '  IF(pac.CodNiv1 IN (' + sEx0 + '), 2,',
  '  -1))))))))))))',
  'Status,',
  'pac.Codigo PlaAllcad, pac.Nome, pac.Ordens ',
  'FROM plaallcad pac ',
  'LEFT JOIN drecfgcpc cpc ON pac.Codigo=cpc.PlaAllCad ',
  'WHERE ',
  '( ',
  '  pac.Codigo IN (' + sPAC + ')',
  '  OR pac.CodNiv1 IN (' + sPAC + ')',
  '  OR pac.CodNiv2 IN (' + sPAC + ')',
  '  OR pac.CodNiv3 IN (' + sPAC + ')',
  '  OR pac.CodNiv4 IN (' + sPAC + ')',
  '  OR pac.CodNiv5 IN (' + sPAC + ')',
  ') ',
(*
  'AND (cpc.Conta=' + Geral.FF0(QrDreCfgCtdConta.Value) +
  ' OR (cpc.Conta IS NULL) ',
  ' OR ((NOT(cpc.Conta IS NULL)) AND (cpc.Codigo <> ' + Geral.FF0(QrDreCfgCabCodigo.Value) + '))) ',
*)
  'AND ( ',
  '  (cpc.Conta=' + sConta + ')',
  '  OR (cpc.Conta IS NULL) ',
  '  OR (cpc.Conta<>' + sConta + ' AND cpc.Codigo <> ' + sCodigo,
  '    AND NOT (pac.Codigo IN (' + sPAC + ')))  ',
  ') ',
  //
  'ORDER BY Ordens',
  '']);
  //
  //Geral.MB_Teste(QrDreCfgCpc.SQL.Text);
  if IdxSub4 > 0 then
    QrDreCfgCpc.Locate('IdxSub4', IdxSub4, []);
end;

procedure TUnContabil_PF.ReopenDreCfgCtd(QrDreCfgCtd: TmySQLQuery; Controle, Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgCtd, Dmod.MyDB, [
  'SELECT * ',
  'FROM drecfgctd ',
  'WHERE Controle=' + Geral.FF0(QrDreCfgEstControle.Value),
  '']);
  //
  QrDreCfgCtd.Locate('Conta',Conta, []);
end;

procedure TUnContabil_PF.ReopenDreCfgEst(QrDreCfgEst: TmySQLQuery; Codigo, Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgEst, Dmod.MyDB, [
  'SELECT * ',
  'FROM drecfgest ',
  'WHERE Codigo=' + Geral.FF0(QrDreCfgCabCodigo.Value),
  'ORDER BY Ordem, Controle ',
  '']);
  //
  QrDreCfgEst.Locate('Controle',Controle, []);
end;
}

end.
