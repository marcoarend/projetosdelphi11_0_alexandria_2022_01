unit CtbBalPIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmCtbBalPIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrContasA: TMySQLQuery;
    QrContasACodigo: TIntegerField;
    QrContasANome: TWideStringField;
    DsContasA: TDataSource;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    EdCtaPlaA: TdmkEditCB;
    CBCtaPlaA: TdmkDBLookupComboBox;
    SbCtaPlaA: TSpeedButton;
    QrContasB: TMySQLQuery;
    QrContasBCodigo: TIntegerField;
    QrContasBNome: TWideStringField;
    DsContasB: TDataSource;
    Label2: TLabel;
    EdCtaPlaB: TdmkEditCB;
    CBCtaPlaB: TdmkDBLookupComboBox;
    SbCtaPlaB: TSpeedButton;
    EdCredito: TdmkEdit;
    LaCredito: TLabel;
    LaDebito: TLabel;
    EdDebito: TdmkEdit;
    QrContasACredito: TWideStringField;
    QrContasADebito: TWideStringField;
    RGTpAmbito: TRadioGroup;
    QrContasASubgrupo: TIntegerField;
    QrContasANO_SubGrupo: TWideStringField;
    QrContasAGrupo: TIntegerField;
    QrContasANO_Grupo: TWideStringField;
    QrContasAConjunto: TIntegerField;
    QrContasANO_Conjunto: TWideStringField;
    QrContasAPlano: TIntegerField;
    QrContasANO_Plano: TWideStringField;
    QrContasBCredito: TWideStringField;
    QrContasBDebito: TWideStringField;
    QrContasBSubgrupo: TIntegerField;
    QrContasBNO_SubGrupo: TWideStringField;
    QrContasBGrupo: TIntegerField;
    QrContasBNO_Grupo: TWideStringField;
    QrContasBConjunto: TIntegerField;
    QrContasBNO_Conjunto: TWideStringField;
    QrContasBPlano: TIntegerField;
    QrContasBNO_Plano: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbCtaPlaAClick(Sender: TObject);
    procedure SbCtaPlaBClick(Sender: TObject);
    procedure EdCtaPlaARedefinido(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCtbBalPIts(Controle: Integer);
  public
    { Public declarations }
    FCodigo: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmCtbBalPIts: TFmCtbBalPIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnFinanceiroJan;

{$R *.DFM}

procedure TFmCtbBalPIts.BtOKClick(Sender: TObject);
var
  APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
  BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt: String;
  Codigo, Controle, TpBalanc, TpAmbito,
  APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod,
  BPlaCod, BCjtCod, BGruCod, BSgrCod, BCtaCod: Integer;
  Credito, Debito: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  Controle       := EdControle.ValueVariant;
  TpBalanc       := Integer(TContabTpBalanc.ctbOutrCtb);
  TpAmbito       := RGTpAmbito.ItemIndex;
  Credito        := EdCredito.ValueVariant;
  Debito         := EdDebito.ValueVariant;
  //
  ACtaCod        := EdCtaPlaA.ValueVariant;
  if ACtaCod <> 0 then
  begin
    APlaCod        := QrContasAPlano.Value;
    ACjtCod        := QrContasAConjunto.Value;
    AGruCod        := QrContasAGrupo.Value;
    ASgrCod        := QrContasASubgrupo.Value;
    APlaTxt        := QrContasANO_Plano.Value;
    ACjtTxt        := QrContasANO_Conjunto.Value;
    AGruTxt        := QrContasANO_Grupo.Value;
    ASgrTxt        := QrContasANO_SubGrupo.Value;
    ACtaTxt        := QrContasBNome.Value;
  end else
  begin
    APlaCod        := 0;
    ACjtCod        := 0;
    AGruCod        := 0;
    ASgrCod        := 0;
    APlaTxt        := '';
    ACjtTxt        := '';
    AGruTxt        := '';
    ASgrTxt        := '';
    ACtaTxt        := '';
  end;
  BCtaCod        := EdCtaPlaB.ValueVariant;
  if BCtaCod <> 0 then
  begin
    BPlaCod        := QrContasBPlano.Value;
    BCjtCod        := QrContasBConjunto.Value;
    BGruCod        := QrContasBGrupo.Value;
    BSgrCod        := QrContasBSubgrupo.Value;
    BPlaTxt        := QrContasBNO_Plano.Value;
    BCjtTxt        := QrContasBNO_Conjunto.Value;
    BGruTxt        := QrContasBNO_Grupo.Value;
    BSgrTxt        := QrContasBNO_SubGrupo.Value;
    BCtaTxt        := QrContasBNome.Value;
  end else
  begin
    BPlaCod        := 0;
    BCjtCod        := 0;
    BGruCod        := 0;
    BSgrCod        := 0;
    BPlaTxt        := '';
    BCjtTxt        := '';
    BGruTxt        := '';
    BSgrTxt        := '';
    BCtaTxt        := '';
  end;
  //
  Controle := UMyMod.BPGS1I32('ctbbalpits', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctbbalpits', False, [
  'Codigo', 'TpBalanc', 'TpAmbito',
  'APlaCod', 'ACjtCod', 'AGruCod',
  'ASgrCod', 'ACtaCod', 'Credito',
  'Debito', 'BPlaCod', 'BCjtCod',
  'BGruCod', 'BSgrCod', 'BCtaCod',
  'APlaTxt', 'ACjtTxt', 'AGruTxt',
  'ASgrTxt', 'ACtaTxt', 'BPlaTxt',
  'BCjtTxt', 'BGruTxt', 'BSgrTxt',
  'BCtaTxt'], [
  'Controle'], [
  Codigo, TpBalanc, TpAmbito,
  APlaCod, ACjtCod, AGruCod,
  ASgrCod, ACtaCod, Credito,
  Debito, BPlaCod, BCjtCod,
  BGruCod, BSgrCod, BCtaCod,
  APlaTxt, ACjtTxt, AGruTxt,
  ASgrTxt, ACtaTxt, BPlaTxt,
  BCjtTxt, BGruTxt, BSgrTxt,
  BCtaTxt], [
  Controle], True) then
  begin
    ReopenCtbBalPIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      //
      EdCtaPlaA.ValueVariant     := 0;
      CBCtaPlaA.KeyValue         := Null;
      EdCtaPlaB.ValueVariant     := 0;
      CBCtaPlaB.KeyValue         := Null;
      EdNome.ValueVariant        := '';
      EdCredito.ValueVariant     := 0.00;
      EdDebito.ValueVariant      := 0.00;
      RGTpAmbito.ItemIndex       := 0;
      //
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmCtbBalPIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtbBalPIts.EdCtaPlaARedefinido(Sender: TObject);
begin
  LaCredito.Enabled := QrContasADebito.Value  <> 'V';
  //EdCredito.Enabled := QrContasADebito.Value  <> 'V';
  LaDebito.Enabled  := QrContasACredito.Value <> 'V';
  //EdDebito.Enabled  := QrContasACredito.Value <> 'V';
end;

procedure TFmCtbBalPIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCtbBalPIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrContasA, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContasB, Dmod.MyDB);
end;

procedure TFmCtbBalPIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtbBalPIts.FormShow(Sender: TObject);
begin
  TRadioButton(RGTpAmbito.Controls[1]).Enabled := False;
  TRadioButton(RGTpAmbito.Controls[2]).Enabled := False;
  TRadioButton(RGTpAmbito.Controls[3]).Enabled := False;
end;

procedure TFmCtbBalPIts.ReopenCtbBalPIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmCtbBalPIts.SbCtaPlaAClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := EdCtaPlaA.ValueVariant;
  VAR_CADASTRO := 0;
  FinanceiroJan.CadastroDeContas(Codigo);
  UMyMod.SetaCodigoPesquisado(EdCtaPlaA, CBCtaPlaA, QrContasA, VAR_CADASTRO);
end;

procedure TFmCtbBalPIts.SbCtaPlaBClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := EdCtaPlaB.ValueVariant;
  VAR_CADASTRO := 0;
  FinanceiroJan.CadastroDeContas(Codigo);
  UMyMod.SetaCodigoPesquisado(EdCtaPlaB, CBCtaPlaB, QrContasB, VAR_CADASTRO);
end;

end.
