object FmCtbCadIts: TFmCtbCadIts
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTB-003 :: Item de Grupo Cont'#225'bil'
  ClientHeight = 376
  ClientWidth = 615
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 231
    Width = 615
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 615
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 567
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 519
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 282
        Height = 32
        Caption = 'Item de Grupo Cont'#225'bil'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 282
        Height = 32
        Caption = 'Item de Grupo Cont'#225'bil'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 282
        Height = 32
        Caption = 'Item de Grupo Cont'#225'bil'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 262
    Width = 615
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 611
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 306
    Width = 615
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 469
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 467
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 615
    Height = 169
    Align = alTop
    TabOrder = 0
    object Label6: TLabel
      Left = 8
      Top = 4
      Width = 36
      Height = 13
      Caption = 'ID item:'
    end
    object Label7: TLabel
      Left = 92
      Top = 4
      Width = 78
      Height = 13
      Caption = 'Nome do Grupo:'
    end
    object Label1: TLabel
      Left = 8
      Top = 44
      Width = 133
      Height = 13
      Caption = 'Tipo de movimento cont'#225'bil:'
    end
    object Label2: TLabel
      Left = 8
      Top = 84
      Width = 166
      Height = 13
      Caption = 'Conta a d'#233'bito do plano de contas:'
    end
    object SbCtbCadMoF: TSpeedButton
      Left = 580
      Top = 60
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbCtbCadMoFClick
    end
    object SbGenCtbD: TSpeedButton
      Left = 580
      Top = 100
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbGenCtbDClick
    end
    object Label4: TLabel
      Left = 8
      Top = 124
      Width = 169
      Height = 13
      Caption = 'Conta a cr'#233'dito do plano de contas:'
    end
    object SbContC: TSpeedButton
      Left = 580
      Top = 140
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbContCClick
    end
    object EdControle: TdmkEdit
      Left = 8
      Top = 20
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 92
      Top = 20
      Width = 509
      Height = 21
      Enabled = False
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdCtbCadMoF: TdmkEditCB
      Left = 8
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCtbCadMoF
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCtbCadMoF: TdmkDBLookupComboBox
      Left = 64
      Top = 60
      Width = 513
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCtbCadMoF
      TabOrder = 3
      dmkEditCB = EdCtbCadMoF
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object CBConta: TdmkDBLookupComboBox
      Left = 64
      Top = 100
      Width = 513
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      TabOrder = 4
      dmkEditCB = EdConta
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdConta: TdmkEditCB
      Left = 8
      Top = 100
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBConta
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdContC: TdmkEditCB
      Left = 8
      Top = 140
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBContC
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBContC: TdmkDBLookupComboBox
      Left = 64
      Top = 140
      Width = 513
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      TabOrder = 7
      dmkEditCB = EdContC
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object QrCtbCadMoF: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  Nome '
      'FROM ctbcadmof'
      'ORDER BY Nome')
    Left = 216
    Top = 44
    object QrCtbCadMoFCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtbCadMoFNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCtbCadMoF: TDataSource
    DataSet = QrCtbCadMoF
    Left = 216
    Top = 92
  end
  object DsPlaAllCad_D: TDataSource
    DataSet = QrPlaAllCad_D
    Left = 304
    Top = 96
  end
  object QrPlaAllCad_D: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM plaallcad'
      'WHERE AnaliSinte=2'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 304
    Top = 44
    object QrPlaAllCad_DCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPlaAllCad_DNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrPlaAllCad_DNivel: TSmallintField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPlaAllCad_DAnaliSinte: TIntegerField
      FieldName = 'AnaliSinte'
      Required = True
    end
    object QrPlaAllCad_DCredito: TSmallintField
      FieldName = 'Credito'
      Required = True
    end
    object QrPlaAllCad_DDebito: TSmallintField
      FieldName = 'Debito'
      Required = True
    end
    object QrPlaAllCad_DOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object DsPlaAllCad_C: TDataSource
    DataSet = QrPlaAllCad_C
    Left = 392
    Top = 96
  end
  object QrPlaAllCad_C: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM plaallcad'
      'WHERE AnaliSinte=2'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 392
    Top = 44
    object QrPlaAllCad_CCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPlaAllCad_CNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrPlaAllCad_CNivel: TSmallintField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPlaAllCad_CAnaliSinte: TIntegerField
      FieldName = 'AnaliSinte'
      Required = True
    end
    object QrPlaAllCad_CCredito: TSmallintField
      FieldName = 'Credito'
      Required = True
    end
    object QrPlaAllCad_CDebito: TSmallintField
      FieldName = 'Debito'
      Required = True
    end
    object QrPlaAllCad_COrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
end
