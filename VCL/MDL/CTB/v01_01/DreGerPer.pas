unit DreGerPer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB;

type
  TFmDreGerPer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    CBMes: TComboBox;
    LaMes: TLabel;
    LaAno: TLabel;
    CBAno: TComboBox;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    function  PeriodoJaExiste(AnoMes, Empresa: Integer; Avisa:
              Boolean): Boolean;
    //procedure ReopenDreCfgCab(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmDreGerPer: TFmDreGerPer;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule, ModuleGeral, DreGerCab;

{$R *.DFM}

procedure TFmDreGerPer.BtOKClick(Sender: TObject);
var
  Ano, Mes, Empresa, AnoMes, DreCfgCab, Filial: Integer;
  SQLType: TSQLType;
begin
  SQLType   := ImgTipo.SQLType;
  Filial    := EdEmpresa.ValueVariant;
  Empresa   := DModG.QrEmpresasCodigo.Value;
  DreCfgCab := Empresa; // EdDreCfgCab.ValueVariant;
  Ano       := Geral.IMV(CBAno.Text);
  Mes       := CBMes.ItemIndex + 1;
  AnoMes    := (Ano * 100) + Mes;
  //
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Selecione a Empresa!') then Exit;
  //if MyObjects.FIC(DreCfgCab = 0, EdDreCfgCab, 'Selecione a configura��o de DRE!') then Exit;
  //
  if not PeriodoJaExiste(AnoMes, Empresa, True) then
  begin
(* /////////////////////////////////////////////////////////////////////////////
      Permitir abrir pois n�o atrapalha e na verdade impede de lan�ar
      mais info n�o retroativo corretamente!

      if not EfdIcmsIpi_PF.LiberaAcaoVS_SPED(AnoMes, DfSEII_v03_0_2_a.FEmpresa_Int,
      TEstagioVS_SPED.evsspedEncerraVS) then
        Exit;
//////////////////////////////////////////////////////////////////////////////*)
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'dregercab', False, [
    'Ano', 'DreCfgCab'], [
    'AnoMes', 'Empresa'], [
    Ano, DreCfgCab], [
    AnoMes, Empresa], True) then
    begin
      FmDreGerCab.EdEmpresa.ValueVariant := Empresa;
      FmDreGerCab.CBEmpresa.KeyValue     := Empresa;
      FmDreGerCab.ReopenDreGerCab(AnoMes);
      //
      Close;
    end;
  end;
end;

procedure TFmDreGerPer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDreGerPer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDreGerPer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  //ReopenDreCfgCab(0);
end;

procedure TFmDreGerPer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmDreGerPer.PeriodoJaExiste(AnoMes, Empresa: Integer;
  Avisa: Boolean): Boolean;
var
  Qry1: TmySQLQuery;
begin
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry1.Close;
    Qry1.Database := Dmod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT AnoMes ',
    'FROM dregercab ',
    'WHERE AnoMes=' + Geral.FF0(AnoMes),
    'AND Empresa=' + Geral.FF0(Empresa),
    '']);
    Result := Qry1.RecordCount > 0;
    if Result and Avisa then
    begin
      Geral.MB_Aviso('O periodo selecionado j� existe!');
    end;
  finally
    Qry1.Free;
  end;
end;

{
procedure TFmDreGerPer.ReopenDreCfgCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgCab, Dmod.MyDB, [
  'SELECT *',
  'FROM drecfgcab',
  'ORDER BY Nome',
  '']);
end;
}
(*
object LaPrompt: TLabel
  Left = 12
  Top = 96
  Width = 107
  Height = 13
  Caption = 'Configura'#231#227'o de DRE:'
  Enabled = False
end
object EdDreCfgCab: TdmkEditCB
  Left = 12
  Top = 112
  Width = 65
  Height = 21
  Alignment = taRightJustify
  Enabled = False
  TabOrder = 4
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ValMin = '-2147483647'
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
  ValWarn = False
  DBLookupComboBox = CBDreCfgCab
  IgnoraDBLookupComboBox = False
  AutoSetIfOnlyOneReg = setregOnlyManual
end
object CBDreCfgCab: TdmkDBLookupComboBox
  Left = 80
  Top = 112
  Width = 437
  Height = 21
  Enabled = False
  KeyField = 'Codigo'
  ListField = 'Nome'
  ListSource = DsDreCfgCab
  TabOrder = 5
  dmkEditCB = EdDreCfgCab
  UpdType = utYes
  LocF7SQLMasc = '$#'
  LocF7PreDefProc = f7pNone
end

object QrDreCfgCab: TMySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT *'
    'FROM drecfgcab'
    'ORDER BY Nome')
  Left = 328
  Top = 44
  object QrDreCfgCabCodigo: TIntegerField
    FieldName = 'Codigo'
    Required = True
  end
  object QrDreCfgCabNome: TWideStringField
    FieldName = 'Nome'
    Required = True
    Size = 60
  end
end
object DsDreCfgCab: TDataSource
  DataSet = QrDreCfgCab
  Left = 328
  Top = 100
end

*)
end.
