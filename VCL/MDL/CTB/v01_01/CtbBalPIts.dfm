object FmCtbBalPIts: TFmCtbBalPIts
  Left = 339
  Top = 185
  Caption = 'CTB-BALAN-002 :: Item de Balan'#231'o Patrimonial'
  ClientHeight = 498
  ClientWidth = 614
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 353
    Width = 614
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 614
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 566
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 518
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 347
        Height = 32
        Caption = ' Item de Balan'#231'o Patrimonial'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 347
        Height = 32
        Caption = ' Item de Balan'#231'o Patrimonial'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 347
        Height = 32
        Caption = ' Item de Balan'#231'o Patrimonial'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 384
    Width = 614
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 610
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 428
    Width = 614
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 468
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 466
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 614
    Height = 285
    Align = alTop
    TabOrder = 0
    object Label6: TLabel
      Left = 8
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label7: TLabel
      Left = 92
      Top = 4
      Width = 99
      Height = 13
      Caption = 'Descri'#231#227'o (hist'#243'rico):'
    end
    object Label1: TLabel
      Left = 8
      Top = 44
      Width = 41
      Height = 13
      Caption = 'Conta A:'
    end
    object SbCtaPlaA: TSpeedButton
      Left = 580
      Top = 60
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbCtaPlaAClick
    end
    object Label2: TLabel
      Left = 8
      Top = 84
      Width = 41
      Height = 13
      Caption = 'Conta B:'
    end
    object SbCtaPlaB: TSpeedButton
      Left = 580
      Top = 100
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbCtaPlaBClick
    end
    object LaCredito: TLabel
      Left = 8
      Top = 124
      Width = 76
      Height = 13
      Caption = 'Cr'#233'dito conta A:'
    end
    object LaDebito: TLabel
      Left = 120
      Top = 124
      Width = 74
      Height = 13
      Caption = 'D'#233'bito conta A:'
    end
    object EdControle: TdmkEdit
      Left = 8
      Top = 20
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 92
      Top = 20
      Width = 509
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdCtaPlaA: TdmkEditCB
      Left = 8
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdCtaPlaARedefinido
      DBLookupComboBox = CBCtaPlaA
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCtaPlaA: TdmkDBLookupComboBox
      Left = 64
      Top = 60
      Width = 513
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContasA
      TabOrder = 3
      dmkEditCB = EdCtaPlaA
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCtaPlaB: TdmkEditCB
      Left = 8
      Top = 100
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCtaPlaB
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCtaPlaB: TdmkDBLookupComboBox
      Left = 64
      Top = 100
      Width = 513
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContasB
      TabOrder = 5
      dmkEditCB = EdCtaPlaB
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCredito: TdmkEdit
      Left = 8
      Top = 140
      Width = 108
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdDebito: TdmkEdit
      Left = 120
      Top = 140
      Width = 108
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object RGTpAmbito: TRadioGroup
      Left = 8
      Top = 164
      Width = 593
      Height = 105
      Caption = ' '#194'mbito: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Nenhum'
        'Lan'#231'amento financeiro'
        ' ---------'
        'Saldo carteira'
        'Valor estoque'
        'Recupera'#231#227'o de impostos'
        'Imobilizado'
        'Patrim'#244'nio l'#237'quido')
      TabOrder = 8
    end
  end
  object QrContasA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.Codigo, cta.Nome, cta.Credito, cta.Debito,'
      'cta.Subgrupo, sgr.Nome NO_SubGrupo, '
      'sgr.Grupo, gru.Nome NO_Grupo, '
      'gru.Conjunto, cjt.Nome NO_Conjunto, '
      'cjt.Plano, pla.Nome NO_Plano '
      'FROM contas cta'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano'
      'WHERE cta.Codigo > 0'
      'ORDER BY cta.Nome')
    Left = 216
    Top = 44
    object QrContasACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasANome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
    object QrContasACredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasADebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasASubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContasANO_SubGrupo: TWideStringField
      FieldName = 'NO_SubGrupo'
      Size = 50
    end
    object QrContasAGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrContasANO_Grupo: TWideStringField
      FieldName = 'NO_Grupo'
      Size = 50
    end
    object QrContasAConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object QrContasANO_Conjunto: TWideStringField
      FieldName = 'NO_Conjunto'
      Size = 50
    end
    object QrContasAPlano: TIntegerField
      FieldName = 'Plano'
    end
    object QrContasANO_Plano: TWideStringField
      FieldName = 'NO_Plano'
      Size = 50
    end
  end
  object DsContasA: TDataSource
    DataSet = QrContasA
    Left = 216
    Top = 92
  end
  object QrContasB: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.Codigo, cta.Nome, cta.Credito, cta.Debito,'
      'cta.Subgrupo, sgr.Nome NO_SubGrupo, '
      'sgr.Grupo, gru.Nome NO_Grupo, '
      'gru.Conjunto, cjt.Nome NO_Conjunto, '
      'cjt.Plano, pla.Nome NO_Plano '
      'FROM contas cta'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano'
      'WHERE cta.Codigo > 0'
      'ORDER BY cta.Nome')
    Left = 284
    Top = 44
    object QrContasBCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasBNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
    object QrContasBCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasBDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasBSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContasBNO_SubGrupo: TWideStringField
      FieldName = 'NO_SubGrupo'
      Size = 50
    end
    object QrContasBGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrContasBNO_Grupo: TWideStringField
      FieldName = 'NO_Grupo'
      Size = 50
    end
    object QrContasBConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object QrContasBNO_Conjunto: TWideStringField
      FieldName = 'NO_Conjunto'
      Size = 50
    end
    object QrContasBPlano: TIntegerField
      FieldName = 'Plano'
    end
    object QrContasBNO_Plano: TWideStringField
      FieldName = 'NO_Plano'
      Size = 50
    end
  end
  object DsContasB: TDataSource
    DataSet = QrContasB
    Left = 284
    Top = 92
  end
end
