unit DreGerFin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmDreGerFin = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label6: TLabel;
    EdEmpresa: TdmkEdit;
    Label1: TLabel;
    EdDreCfgEst: TdmkEditCB;
    CBDreCfgEst: TdmkDBLookupComboBox;
    EdAnoMes: TdmkEdit;
    Label2: TLabel;
    EdLinha: TdmkEdit;
    Label3: TLabel;
    QrDreCfgEst: TMySQLQuery;
    QrDreCfgEstCodigo: TIntegerField;
    QrDreCfgEstControle: TIntegerField;
    QrDreCfgEstNome: TWideStringField;
    QrDreCfgEstDreCfgTp1: TIntegerField;
    QrDreCfgEstOrdem: TIntegerField;
    DsDreCfgEst: TDataSource;
    QrDreCfgCtd: TMySQLQuery;
    QrDreCfgCtdCodigo: TIntegerField;
    QrDreCfgCtdControle: TIntegerField;
    QrDreCfgCtdConta: TIntegerField;
    QrDreCfgCtdNome: TWideStringField;
    QrDreCfgCtdDreCfgTp2: TIntegerField;
    QrDreCfgCtdOrdem: TIntegerField;
    DsDreCfgCtd: TDataSource;
    Label4: TLabel;
    EdDreCfgCtd: TdmkEditCB;
    CBDreCfgCtd: TdmkDBLookupComboBox;
    TPData: TdmkEditDateTimePicker;
    Label5: TLabel;
    QrContas1: TMySQLQuery;
    QrContas1Codigo: TIntegerField;
    QrContas1Nome: TWideStringField;
    QrContas1Nome2: TWideStringField;
    QrContas1Nome3: TWideStringField;
    QrContas1ID: TWideStringField;
    QrContas1Subgrupo: TIntegerField;
    QrContas1Empresa: TIntegerField;
    QrContas1Credito: TWideStringField;
    QrContas1Debito: TWideStringField;
    QrContas1Mensal: TWideStringField;
    QrContas1Exclusivo: TWideStringField;
    QrContas1Mensdia: TSmallintField;
    QrContas1Mensdeb: TFloatField;
    QrContas1Mensmind: TFloatField;
    QrContas1Menscred: TFloatField;
    QrContas1Mensminc: TFloatField;
    QrContas1Lk: TIntegerField;
    QrContas1Terceiro: TIntegerField;
    QrContas1Excel: TWideStringField;
    QrContas1DataCad: TDateField;
    QrContas1DataAlt: TDateField;
    QrContas1UserCad: TSmallintField;
    QrContas1UserAlt: TSmallintField;
    QrContas1NOMESUBGRUPO: TWideStringField;
    QrContas1NOMEGRUPO: TWideStringField;
    QrContas1NOMECONJUNTO: TWideStringField;
    QrContas1NOMEEMPRESA: TWideStringField;
    QrContas1NOMEPLANO: TWideStringField;
    QrContas1Niveis: TWideStringField;
    QrContas1Ordens: TWideStringField;
    DsContas1: TDataSource;
    DBNiveis1: TDBEdit;
    Label7: TLabel;
    EdGenero: TdmkEditCB;
    CBGenero: TdmkDBLookupComboBox;
    LaDeb: TLabel;
    EdDebito: TdmkEdit;
    EdCredito: TdmkEdit;
    LaCred: TLabel;
    EdCtrlAvul: TdmkEdit;
    Label8: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdDreCfgEstRedefinido(Sender: TObject);
    procedure EdGeneroChange(Sender: TObject);
    procedure EdDreCfgCtdRedefinido(Sender: TObject);
    procedure EdGeneroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGeneroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenContas1(Filtra: Boolean);
    procedure ReopenDreGerFin(Linha: Integer);
    function  CordaDeQuery_Inclusos(): String;
  public
    { Public declarations }
    FQrIts: TmySQLQuery;
    FAno, FDreCfgCab: Integer;
    FDataIni_Dta, FDataFim_Dta: TDateTime;
    (*
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    *)
    procedure ReopenDreCfgEst(Codigo: Integer);
  end;

  var
  FmDreGerFin: TFmDreGerFin;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral, DreGerCab;

{$R *.DFM}

procedure TFmDreGerFin.BtOKClick(Sender: TObject);
var
  Data, Compensado, Vencimento: String;
  Ano, AnoMes, Empresa, Linha, DreCfgEst, DreCfgCtd, DreCfgCpc, CpcStatus,
  DreCfgTp1, DreCfgTp2, Tipo, Carteira, Controle, Sub, Genero, Cliente,
  Fornecedor, Sit, GenCtb, GenCtbD, GenCtbC, GenNiv1, GenNiv2, GenNiv3,
  GenNiv4, GenNiv5, GenNiv6, CtrlAvul, FormaAdd: Integer;
  Debito, Credito: Double;
  SQLType: TSQLType;
  //
  Data_Dta: TDateTime;
  Erro: Boolean;
begin
  SQLType        := ImgTipo.SQLType;
  Ano            := FAno;
  AnoMes         := EdAnoMes.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  Linha          := EdLinha.ValueVariant;
  DreCfgEst      := EdDreCfgEst.ValueVariant;
  DreCfgCtd      := EdDreCfgCtd.ValueVariant;
  DreCfgCpc      := 0; //EdDreCfgCpc .ValueVariant;
  CpcStatus      := Integer(TDreCfgCpcStatus.dccsAvulso); // 5; // Manual
  DreCfgTp1      := QrDreCfgEstDreCfgTp1.Value;
  DreCfgTp2      := QrDreCfgCtdDreCfgTp2.Value;
  Data           := Geral.FDT(TPData.Date, 1);
  Tipo           := 0;
  Carteira       := 0;
  Controle       := 0;
  Sub            := 0;
  Genero         := EdGenero.ValueVariant;
  Cliente        := 0;
  Fornecedor     := 0;
  Debito         := EdDebito.ValueVariant;
  Credito        := EdCredito.ValueVariant;
  Compensado     := '0000-00-00';
  Sit            := -1;
  Vencimento     := '0000-00-00';
  GenCtb         := 0;
  GenCtbD        := 0;
  GenCtbC        := 0;
  GenNiv1        := 0;
  GenNiv2        := 0;
  GenNiv3        := 0;
  GenNiv4        := 0;
  GenNiv5        := 0;
  GenNiv6        := 0;
  CtrlAvul       := EdCtrlAvul.ValueVariant;
  FormaAdd       := Integer(TdmkModoExec.dmodexManual);
  //
  if MyObjects.FIC(DreCfgEst = 0, EdDreCfgEst, 'Informe a estrutura!') then Exit;
  if MyObjects.FIC(DreCfgCtd = 0, EdDreCfgEst, 'Informe o Conte�do!') then Exit;
  //
  Data_Dta := Trunc(TPData.Date);
  Erro := (Data_Dta < FDataIni_Dta) or (Data_Dta > FDataFim_Dta);
  if MyObjects.FIC(Erro, TpData, 'Informe a data dentro do per�odo!') then Exit;
  //
  Erro := (Credito <> 0) and (Debito <> 0);
  if MyObjects.FIC(Erro, TpData, 'Informe somente o d�bito ou o cr�dito!') then Exit;
  //
  if SQLType = stIns then
    Linha := FmDreGerCab.ObterLinha();
  //
  CtrlAvul := UMyMod.BPGS1I32('dregerfin', 'CtrlAvul', '', '', tsPos, SQLType, CtrlAvul);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'dregerfin', False, [
  'Ano', 'DreCfgEst', 'DreCfgCtd',
  'DreCfgCpc', 'CpcStatus', 'DreCfgTp1',
  'DreCfgTp2', 'Data', 'Tipo',
  'Carteira', 'Controle', 'Sub',
  'Genero', 'Cliente', 'Fornecedor',
  'Debito', 'Credito', 'Compensado',
  'Sit', 'Vencimento', 'GenCtb',
  'GenCtbD', 'GenCtbC', 'GenNiv1',
  'GenNiv2', 'GenNiv3', 'GenNiv4',
  'GenNiv5', 'GenNiv6', 'CtrlAvul',
  'FormaAdd'], [
  'AnoMes', 'Empresa', 'Linha'], [
  Ano, DreCfgEst, DreCfgCtd,
  DreCfgCpc, CpcStatus, DreCfgTp1,
  DreCfgTp2, Data, Tipo,
  Carteira, Controle, Sub,
  Genero, Cliente, Fornecedor,
  Debito, Credito, Compensado,
  Sit, Vencimento, GenCtb,
  GenCtbD, GenCtbC, GenNiv1,
  GenNiv2, GenNiv3, GenNiv4,
  GenNiv5, GenNiv6, CtrlAvul,
  FormaAdd], [
  AnoMes, Empresa, Linha], True) then
  begin
    ReopenDreGerFin(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdDreCfgEst.ValueVariant  := 0;
      CBDreCfgEst.KeyValue      := 0;
      EdDreCfgCtd.ValueVariant  := 0;
      CBDreCfgCtd.KeyValue      := 0;
      EdDebito.ValueVariant     := 0;
      EdCredito.ValueVariant    := 0;
      EdGenero.ValueVariant     := 0;
      CBGenero.KeyValue         := Null;
      //
      EdDreCfgEst.SetFocus;
    end else Close;
  end;
end;

procedure TFmDreGerFin.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDreGerFin.CBGeneroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    ReopenContas1(False);
end;

function TFmDreGerFin.CordaDeQuery_Inclusos: String;
var
  I: Integer;
begin
  with FmDreGerCab.QrDreCfgCpc do
  begin
    if RecordCount > 0 then
    begin
      Result := '';
      try
        DisableControls;
        First;
        while not Eof do
        begin
          if TDreCfgCpcStatus(FmDreGerCab.QrDreCfgCpcStatus.Value) in ([dccsAutoIncluso,dccsAutoInclusoFilho]) then
          begin
            if Result <> '' then
              Result := Result + ', ';
            Result := Result + Geral.FF0(FmDreGerCab.QrDreCfgCpcPlaAllCad.Value);
          end;
          //
          Next;
        end;
      finally
        EnableControls;
      end;
    end;
  end;
  if Result = EmptyStr then
    Result := '-999999999';
end;

procedure TFmDreGerFin.EdDreCfgCtdRedefinido(Sender: TObject);
begin
  ReopenContas1(True);
end;

procedure TFmDreGerFin.EdDreCfgEstRedefinido(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgCtd, Dmod.MyDB, [
  'SELECT * ',
  'FROM drecfgctd ',
  'WHERE Controle=' + Geral.FF0(QrDreCfgEstControle.Value),
  'ORDER BY Nome ',
  '']);
  //
  EdDreCfgCtd.ValueVariant := 0;
  CBDreCfgCtd.KeyValue     := 0;
end;

procedure TFmDreGerFin.EdGeneroChange(Sender: TObject);
begin
  if EdGenero.ValueVariant = 0 then
    DBNiveis1.DataField := ''
  else
  begin
    if DModG.QrCtrlGeralConcatNivOrdCta.Value = 1 then
      DBNiveis1.DataField := 'Ordens'
    else
      DBNiveis1.DataField := 'Niveis';
  end;
end;

procedure TFmDreGerFin.EdGeneroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    ReopenContas1(False);
end;

procedure TFmDreGerFin.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDreGerFin.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DBNiveis1.DataField := '';
  //
end;

procedure TFmDreGerFin.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDreGerFin.ReopenContas1(Filtra: Boolean);
var
  _WHERE, Corda: String;
begin
  FmDreGerCab.ReopenDreCfgCpc(TFormaOpenCPC.focpcItem, FDreCfgCab, QrDreCfgCtdConta.Value);
  //Corda := MyObjects.CordaDeQuery(FmDreGerCab.QrDreCfgCpc, 'PlaAllCad', '-999999999');
  Corda := CordaDeQuery_Inclusos();
  //ReopenLanctos(Corda);
  if Filtra then
    _WHERE := 'WHERE co.Codigo IN (' + Corda + ') '
  else
    _WHERE := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrContas1, Dmod.MyDB, [
  ' SELECT CONCAT( ',
  '  pl.Codigo, ".", ',
  '  cj.Codigo, ".", ',
  '  gr.Codigo, ".", ',
  '  sg.Codigo, ".", ',
  '  co.Codigo) Niveis, ',
  'CONCAT( ',
  '  pl.OrdemLista, ".", ',
  '  cj.OrdemLista, ".", ',
  '  gr.OrdemLista, ".", ',
  '  sg.OrdemLista, ".", ',
  '  co.OrdemLista) Ordens, ',
  'co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO, ',
  'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO, ',
  'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA ',
  'FROM contas co ',
  'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo ',
  'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo ',
  'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto ',
  'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano ',
  'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa ',
  _WHERE,
  'ORDER BY co.Nome ',
  '']);
end;

procedure TFmDreGerFin.ReopenDreCfgEst(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgEst, Dmod.MyDB, [
  'SELECT *',
  'FROM drecfgest',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'ORDER BY Nome',
  '']);
end;

procedure TFmDreGerFin.ReopenDreGerFin(Linha: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Linha <> 0 then
      FQrIts.Locate('Linha', Linha, []);
  end;
end;

end.
