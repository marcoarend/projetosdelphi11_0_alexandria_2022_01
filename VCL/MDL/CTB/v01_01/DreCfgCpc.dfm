object FmDreCfgCpc: TFmDreCfgCpc
  Left = 339
  Top = 185
  Caption = 'DRE-CONFG-003 :: DRE - Configura'#231#227'o - Item do Plano'
  ClientHeight = 509
  ClientWidth = 740
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 345
    Width = 740
    Height = 50
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 740
    Height = 145
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object Label2: TLabel
      Left = 12
      Top = 60
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label4: TLabel
      Left = 72
      Top = 60
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label9: TLabel
      Left = 12
      Top = 100
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label10: TLabel
      Left = 72
      Top = 100
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object EdCodigo: TdmkEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = '0'
      ValWarn = False
    end
    object EdNO_Empresa: TdmkEdit
      Left = 72
      Top = 36
      Width = 661
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdNomCtrl: TdmkEdit
      Left = 72
      Top = 76
      Width = 661
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 76
      Width = 56
      Height = 21
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = '0'
      ValWarn = False
    end
    object EdConta: TdmkEdit
      Left = 12
      Top = 116
      Width = 56
      Height = 21
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = '0'
      ValWarn = False
    end
    object EdNomCta: TdmkEdit
      Left = 72
      Top = 116
      Width = 661
      Height = 21
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 193
    Width = 740
    Height = 152
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label1: TLabel
      Left = 12
      Top = 56
      Width = 117
      Height = 13
      Caption = 'Item do plano de contas:'
    end
    object SbContas: TSpeedButton
      Left = 712
      Top = 72
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbContasClick
    end
    object Label7: TLabel
      Left = 96
      Top = 16
      Width = 26
      Height = 13
      Caption = 'N'#237'vel'
      Enabled = False
      FocusControl = DBEdNivel
    end
    object Label8: TLabel
      Left = 132
      Top = 16
      Width = 133
      Height = 13
      Caption = 'Filia'#231#227'o no plano de contas:'
      Enabled = False
    end
    object SpeedButton1: TSpeedButton
      Left = 712
      Top = 32
      Width = 23
      Height = 22
      Caption = '<'
    end
    object EdIdxSub4: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'IdxSub3'
      UpdCampo = 'IdxSub3'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CBPlaAllCad: TdmkDBLookupComboBox
      Left = 72
      Top = 72
      Width = 637
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPlaAllCad
      TabOrder = 2
      dmkEditCB = EdPlaAllCad
      QryCampo = 'Topico'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdPlaAllCad: TdmkEditCB
      Left = 12
      Top = 72
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Topico'
      UpdCampo = 'Topico'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdPlaAllCadRedefinido
      DBLookupComboBox = CBPlaAllCad
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object DBOrdens: TDBEdit
      Left = 132
      Top = 32
      Width = 577
      Height = 21
      DataField = 'Ordens'
      DataSource = DsPlaAllCad
      Enabled = False
      TabOrder = 3
    end
    object DBEdNivel: TDBEdit
      Left = 96
      Top = 32
      Width = 33
      Height = 21
      DataField = 'Nivel'
      DataSource = DsPlaAllCad
      Enabled = False
      TabOrder = 4
    end
    object RGExcluso: TRadioGroup
      Left = 12
      Top = 100
      Width = 237
      Height = 45
      Caption = ' Importar dos lan'#231'amentos financeiros: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Sim'
        'N'#227'o')
      TabOrder = 5
    end
    object RGMesCompet: TRadioGroup
      Left = 252
      Top = 100
      Width = 313
      Height = 45
      Caption = ' M'#234's de refer'#234'ncia (lan'#231'amentos autom'#225'ticos): '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'M'#234's do exerc'#237'cio'
        'M'#234's seguinte ao exerc'#237'cio')
      TabOrder = 6
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 740
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 692
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 644
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 353
        Height = 32
        Caption = 'Configura'#231#227'o - Item do Plano'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 353
        Height = 32
        Caption = 'Configura'#231#227'o - Item do Plano'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 353
        Height = 32
        Caption = 'Configura'#231#227'o - Item do Plano'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 395
    Width = 740
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 736
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 439
    Width = 740
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 594
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 592
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPlaAllCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Ordens, Nome  '
      'FROM plaallcad '
      'ORDER BY Nome ')
    Left = 268
    Top = 92
    object QrPlaAllCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPlaAllCadOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
    object QrPlaAllCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrPlaAllCadNivel: TSmallintField
      FieldName = 'Nivel'
    end
  end
  object DsPlaAllCad: TDataSource
    DataSet = QrPlaAllCad
    Left = 268
    Top = 144
  end
  object PMContas: TPopupMenu
    Left = 700
    Top = 297
    object odosNveis1: TMenuItem
      Caption = '&Todos N'#237'veis'
      OnClick = odosNveis1Click
    end
    object Lista1: TMenuItem
      Caption = '&Lista'
      OnClick = Lista1Click
    end
  end
end
