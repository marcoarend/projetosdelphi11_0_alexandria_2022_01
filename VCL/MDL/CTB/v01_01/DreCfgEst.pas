unit DreCfgEst;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmDreCfgEst = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrDreCfgTp1: TMySQLQuery;
    DsDreCfgTp1: TDataSource;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    EdDreCfgTp1: TdmkEditCB;
    CBDreCfgTp1: TdmkDBLookupComboBox;
    EdOrdem: TdmkEdit;
    Label2: TLabel;
    QrDreCfgTp1Codigo: TIntegerField;
    QrDreCfgTp1NOM_COD: TWideStringField;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    EdNomCod: TdmkEdit;
    QrDreCfgTp1Nome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdDreCfgTp1Redefinido(Sender: TObject);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenIts(Controle: Integer);
    procedure ReopenDreCfgTp1();
  public
    { Public declarations }
    //FCodigo: Integer;
    FQrIts: TmySQLQuery;
  end;

  var
  FmDreCfgEst: TFmDreCfgEst;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmDreCfgEst.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Controle, DreCfgTp1, Ordem: Integer;
  SQLType: TSQLType;
begin
{
http://www.portaldecontabilidade.com.br/guia/demonstracaodoresultado.htm#:~:text=DEMONSTRA%C3%87%C3%83O%20DO%20RESULTADO%20DO%20EXERC%C3%8DCIO%20(DRE)&text=O%20artigo%20187%20da%20Lei,obrigat%C3%B3rio%20conforme%20seu%20artigo%201.179.

DEMONSTRA��O DO RESULTADO DO EXERC�CIO (DRE)

O artigo 187 da Lei 6.404/1976 (Lei das Sociedades por A��es), instituiu a Demonstra��o do Resultado do Exerc�cio - DRE. No atual C�digo Civil Brasileiro, a DRE corresponde ao "resultado econ�mico", cujo levantamento � obrigat�rio conforme seu artigo 1.179.
A DRE tem como objetivo principal apresentar de forma vertical resumida o resultado apurado em rela��o ao conjunto de opera��es realizadas num determinado per�odo, normalmente, de doze meses.
De acordo com a legisla��o mencionada, as empresas dever�o na Demonstra��o do Resultado do Exerc�cio discriminar:
- a receita bruta das vendas e servi�os, as dedu��es das vendas, os abatimentos e os impostos;
- a receita l�quida das vendas e servi�os, o custo das mercadorias e servi�os vendidos e o lucro bruto;
- as despesas com as vendas, as despesas financeiras, deduzidas das receitas, as despesas gerais e administrativas, e outras despesas operacionais;
- o lucro ou preju�zo operacional, as outras receitas e as outras despesas;
- o resultado do exerc�cio antes do Imposto sobre a Renda e a provis�o para o imposto;
- as participa��es de deb�ntures, empregados, administradores e partes benefici�rias, mesmo na forma de instrumentos financeiros, e de institui��es ou fundos de assist�ncia ou previd�ncia de empregados, que n�o se caracterizem como despesa;
-  o lucro ou preju�zo l�quido do exerc�cio e o seu montante por a��o do capital social.
Na determina��o da apura��o do resultado do exerc�cio ser�o computados em obedi�ncia ao princ�pio da compet�ncia:
a) as receitas e os rendimentos ganhos no per�odo, independentemente de sua realiza��o em moeda; e
b) os custos, despesas, encargos e perdas, pagos ou incorridos, correspondentes a essas receitas e rendimentos.

MODELO DA DEMONSTRA��O DO RESULTADO DO EXERC�CIO

RECEITA OPERACIONAL BRUTA
Vendas de Produtos
Vendas de Mercadorias
Presta��o de Servi�os

(-) DEDU��ES DA RECEITA BRUTA
Devolu��es de Vendas
Abatimentos
Impostos e Contribui��es Incidentes sobre Vendas

= RECEITA OPERACIONAL L�QUIDA

(-) CUSTOS DAS VENDAS
Custo dos Produtos Vendidos
Custo das Mercadorias
Custo dos Servi�os Prestados

= RESULTADO OPERACIONAL BRUTO

(-) DESPESAS OPERACIONAIS
Despesas Com Vendas
Despesas Administrativas

(-) DESPESAS FINANCEIRAS L�QUIDAS
Despesas Financeiras
(-) Receitas Financeiras
Varia��es Monet�rias e Cambiais Passivas
(-) Varia��es Monet�rias e Cambiais Ativas

OUTRAS RECEITAS E DESPESAS
Resultado da Equival�ncia Patrimonial
Venda de Bens e Direitos do Ativo N�o Circulante
(-) Custo da Venda de Bens e Direitos do Ativo N�o Circulante

= RESULTADO OPERACIONAL ANTES DO IMPOSTO DE RENDA E DA CONTRIBUI��O SOCIAL E SOBRE O LUCRO
(-) Provis�o para Imposto de Renda e Contribui��o Social Sobre o Lucro

= LUCRO L�QUIDO ANTES DAS PARTICIPA��ES
(-) Deb�ntures, Empregados, Participa��es de Administradores, Partes Benefici�rias, Fundos de Assist�ncia e Previd�ncia para Empregados

(=) RESULTADO L�QUIDO DO EXERC�CIO

}
  //
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  Nome           := EdNome.Text;
  DreCfgTp1      := EdDreCfgTp1.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  //
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  if MyObjects.FIC(DreCfgTp1 = 0, EdDreCfgTp1, 'Informe o item da estrutura!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('drecfgest', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'drecfgest', False, [
  'Codigo', 'Nome', 'DreCfgTp1',
  'Ordem'], [
  'Controle'], [
  Codigo, Nome, DreCfgTp1,
  Ordem], [
  Controle], True) then
  begin
    ReopenIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType             := stIns;
      EdControle.ValueVariant     := 0;
      EdDreCfgTp1.ValueVariant    := 0;
      CBDreCfgTp1.KeyValue        := Null;
      EdOrdem.ValueVariant        := 0;
      EdNome.ValueVariant         := '';
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmDreCfgEst.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDreCfgEst.EdDreCfgTp1Redefinido(Sender: TObject);
begin
  //if EdOrdem.ValueVariant = 0 then
    EdOrdem.ValueVariant := EdDreCfgTp1.ValueVariant;
end;

procedure TFmDreCfgEst.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdNome.ValueVariant := QrDreCfgTp1Nome.Value;
end;

procedure TFmDreCfgEst.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDreCfgEst.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenDreCfgTp1();
end;

procedure TFmDreCfgEst.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDreCfgEst.ReopenIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmDreCfgEst.ReopenDreCfgTp1();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgTp1, Dmod.MyDB, [
  'SELECT Codigo, Nome, CONCAT(Codigo, " - ", Nome) NOM_COD',
  'FROM drecfgtp1',
  'ORDER BY Codigo, Nome',
  ' ']);
end;

end.
