unit CtbGruPsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmCtbGruPsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TdmkEdit;
    QrCtbCadGru: TMySQLQuery;
    QrCtbCadGruCodigo: TIntegerField;
    QrCtbCadGruNome: TWideStringField;
    DsCtbCadGru: TDataSource;
    QrCtbCadIts: TMySQLQuery;
    QrCtbCadItsCodigo: TIntegerField;
    QrCtbCadItsControle: TIntegerField;
    QrCtbCadItsCtbCadMoF: TIntegerField;
    QrCtbCadItsConta: TIntegerField;
    QrCtbCadItsNO_CtbCadMoF: TWideStringField;
    QrCtbCadItsNO_Conta: TWideStringField;
    DsCtbCadIts: TDataSource;
    DGDados: TDBGrid;
    Splitter1: TSplitter;
    CkIncluiCtas: TCheckBox;
    QrCtbCadItsCtbCadMoC: TIntegerField;
    QrCtbCadItsContC: TIntegerField;
    QrCtbCadItsNO_CtbCadMoC: TWideStringField;
    QrCtbCadItsNO_ContC: TWideStringField;
    DBGrid1: TDBGrid;
    CkUsarAsDuasContas: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure CkIncluiCtasClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure QrCtbCadGruAfterScroll(DataSet: TDataSet);
    procedure QrCtbCadGruBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenCtbCadIts(Controle: Integer);
    procedure ReopenPesquisa();
    procedure DefineCtas();
  public
    { Public declarations }
    FCtaFin, FCtaCtb: Integer;
    FUsarAsDuas: Boolean;
  end;

  var
  FmCtbGruPsq: TFmCtbGruPsq;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmCtbGruPsq.BtOKClick(Sender: TObject);
begin
  DefineCtas();
end;

procedure TFmCtbGruPsq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtbGruPsq.CkIncluiCtasClick(Sender: TObject);
begin
  ReopenPesquisa();
end;

procedure TFmCtbGruPsq.DBGrid1DblClick(Sender: TObject);
begin
  DefineCtas();
end;

procedure TFmCtbGruPsq.DefineCtas();
begin
  if (QrCtbCadIts.State <> DsInactive) and (QrCtbCadIts.RecordCount > 0) then
  begin
    FCtaFin := QrCtbCadItsConta.Value;
    FCtaCtb := QrCtbCadItsContC.Value;
    FUsarAsDuas := CkUsarAsDuasContas.Checked;
    //
    Close;
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

procedure TFmCtbGruPsq.EdPesqChange(Sender: TObject);
begin
  ReopenPesquisa();
end;

procedure TFmCtbGruPsq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCtbGruPsq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCtaFin := 0;
  FCtaCtb := 0;
  FUsarAsDuas := False;
end;

procedure TFmCtbGruPsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtbGruPsq.QrCtbCadGruAfterScroll(DataSet: TDataSet);
begin
  ReopenCtbCadIts(0);
end;

procedure TFmCtbGruPsq.QrCtbCadGruBeforeClose(DataSet: TDataSet);
begin
  QrCtbCadIts.Close;
end;

procedure TFmCtbGruPsq.ReopenCtbCadIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtbCadIts, Dmod.MyDB, [
  'SELECT cci.*,  ',
  'ccm.Nome NO_CtbCadMoF, cta.Nome NO_Conta, ',
  'ccc.Nome NO_CtbCadMoC, cts.Nome NO_ContC  ',
  'FROM ctbcadits cci  ',
  'LEFT JOIN ctbcadmof ccm ON ccm.Codigo=cci.CtbCadMoF  ',
  'LEFT JOIN contas cta ON cta.Codigo=cci.Conta  ',
  'LEFT JOIN ctbcadmof ccc ON ccc.Codigo=cci.CtbCadMoC  ',
  'LEFT JOIN contas cts ON cts.Codigo=cci.ContC  ',
  'WHERE cci.Codigo=' + Geral.FF0(QrCtbCadGruCodigo.Value),
  '']);
end;

procedure TFmCtbGruPsq.ReopenPesquisa;
var
  Texto: String;
begin

  Screen.Cursor := crHourGlass;
  try
    Texto := EdPesq.ValueVariant;
    if (Length(Texto) > 2) then
    begin
      Texto := '%' + Texto + '%';
      //
      if CkIncluiCtas.Checked then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrCtbCadGru, Dmod.MyDB, [
        'SELECT DISTINCT ccg.Codigo, ccg.Nome',
        'FROM ctbcadgru ccg  ',
        'LEFT JOIN ctbcadits cci ON cci.Codigo=ccg.Codigo ',
        'LEFT JOIN contas ct1 ON ct1.Codigo=cci.Conta ',
        'LEFT JOIN contas ct2 ON ct2.Codigo=cci.ContC ',
        'WHERE ccg.Nome LIKE "' + Texto + '"',
        'OR ct1.Nome LIKE "' + Texto + '"',
        'OR ct1.Nome2 LIKE "' + Texto + '"',
        'OR ct1.Nome3 LIKE "' + Texto + '"',
        'OR ct2.Nome LIKE "' + Texto + '"',
        'OR ct2.Nome2 LIKE "' + Texto + '"',
        'OR ct1.Nome3 LIKE "' + Texto + '"',
        'ORDER BY ccg.Nome',
        '']);
      end else
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrCtbCadGru, Dmod.MyDB, [
        'SELECT DISTINCT ccg.Codigo, ccg.Nome',
        'FROM ctbcadgru ccg  ',
        'WHERE ccg.Nome LIKE "' + Texto + '"',
        'ORDER BY ccg.Nome',
        '']);
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
