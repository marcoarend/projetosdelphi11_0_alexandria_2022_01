object FmDreGerFin: TFmDreGerFin
  Left = 339
  Top = 185
  Caption = 'DRE-GEREN-003 :: DRE - Lan'#231'amentos Financeiros'
  ClientHeight = 372
  ClientWidth = 745
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 232
    Width = 745
    Height = 26
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 745
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 697
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 649
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 395
        Height = 32
        Caption = 'DRE - Lan'#231'amentos Financeiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 395
        Height = 32
        Caption = 'DRE - Lan'#231'amentos Financeiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 395
        Height = 32
        Caption = 'DRE - Lan'#231'amentos Financeiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 258
    Width = 745
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 741
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 302
    Width = 745
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 599
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 597
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 745
    Height = 173
    Align = alTop
    TabOrder = 0
    object Label6: TLabel
      Left = 8
      Top = 4
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object Label1: TLabel
      Left = 8
      Top = 44
      Width = 45
      Height = 13
      Caption = 'Estrutura:'
    end
    object Label2: TLabel
      Left = 92
      Top = 4
      Width = 46
      Height = 13
      Caption = 'Ano/m'#234's:'
    end
    object Label3: TLabel
      Left = 176
      Top = 4
      Width = 36
      Height = 13
      Caption = 'ID Seq:'
    end
    object Label4: TLabel
      Left = 372
      Top = 44
      Width = 49
      Height = 13
      Caption = 'Conte'#250'do:'
    end
    object Label5: TLabel
      Left = 8
      Top = 87
      Width = 26
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 8
      Top = 128
      Width = 238
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Conta a d'#233'bito - [F7] pesquisa - [F4] Habilita todos:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LaDeb: TLabel
      Left = 135
      Top = 87
      Width = 34
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'D'#233'bito:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LaCred: TLabel
      Left = 218
      Top = 87
      Width = 36
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Cr'#233'dito:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 260
      Top = 4
      Width = 48
      Height = 13
      Caption = 'ID avulso:'
    end
    object EdEmpresa: TdmkEdit
      Left = 8
      Top = 20
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdDreCfgEst: TdmkEditCB
      Left = 8
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdDreCfgEstRedefinido
      DBLookupComboBox = CBDreCfgEst
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBDreCfgEst: TdmkDBLookupComboBox
      Left = 64
      Top = 60
      Width = 304
      Height = 21
      KeyField = 'Controle'
      ListField = 'Nome'
      ListSource = DsDreCfgEst
      TabOrder = 4
      dmkEditCB = EdDreCfgEst
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdAnoMes: TdmkEdit
      Left = 92
      Top = 20
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdLinha: TdmkEdit
      Left = 176
      Top = 20
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdDreCfgCtd: TdmkEditCB
      Left = 372
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdDreCfgCtdRedefinido
      DBLookupComboBox = CBDreCfgCtd
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBDreCfgCtd: TdmkDBLookupComboBox
      Left = 428
      Top = 60
      Width = 304
      Height = 21
      KeyField = 'Conta'
      ListField = 'Nome'
      ListSource = DsDreCfgCtd
      TabOrder = 6
      dmkEditCB = EdDreCfgCtd
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object TPData: TdmkEditDateTimePicker
      Left = 8
      Top = 103
      Width = 123
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 44748.000000000000000000
      Time = 0.655720300899702100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'Data'
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object DBNiveis1: TDBEdit
      Left = 9
      Top = 144
      Width = 98
      Height = 21
      TabStop = False
      DataField = 'Niveis'
      DataSource = DsContas1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
    end
    object EdGenero: TdmkEditCB
      Left = 111
      Top = 144
      Width = 55
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'GenCtbD'
      UpdCampo = 'GenCtbD'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGeneroChange
      OnKeyDown = EdGeneroKeyDown
      DBLookupComboBox = CBGenero
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBGenero: TdmkDBLookupComboBox
      Left = 170
      Top = 144
      Width = 562
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas1
      ParentFont = False
      TabOrder = 12
      OnKeyDown = CBGeneroKeyDown
      dmkEditCB = EdGenero
      QryCampo = 'GenCtbD'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdDebito: TdmkEdit
      Left = 135
      Top = 103
      Width = 80
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Debito'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdCredito: TdmkEdit
      Left = 218
      Top = 103
      Width = 80
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Credito'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdCtrlAvul: TdmkEdit
      Left = 260
      Top = 20
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 13
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object QrDreCfgEst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM drecfgest'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 284
    Top = 1
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDreCfgEstCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDreCfgEstControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDreCfgEstNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrDreCfgEstDreCfgTp1: TIntegerField
      FieldName = 'DreCfgTp1'
      Required = True
    end
    object QrDreCfgEstOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
  end
  object DsDreCfgEst: TDataSource
    DataSet = QrDreCfgEst
    Left = 280
    Top = 53
  end
  object QrDreCfgCtd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM drecfgctd'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 384
    Top = 1
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDreCfgCtdCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDreCfgCtdControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDreCfgCtdConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrDreCfgCtdNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrDreCfgCtdDreCfgTp2: TIntegerField
      FieldName = 'DreCfgTp2'
      Required = True
    end
    object QrDreCfgCtdOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
  end
  object DsDreCfgCtd: TDataSource
    DataSet = QrDreCfgCtd
    Left = 384
    Top = 53
  end
  object QrContas1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ' SELECT CONCAT('
      '  pl.Codigo, ".",'
      '  cj.Codigo, ".",'
      '  gr.Codigo, ".",'
      '  sg.Codigo, ".",'
      '  co.Codigo) Niveis,'
      'CONCAT('
      '  pl.OrdemLista, ".",'
      '  cj.OrdemLista, ".",'
      '  gr.OrdemLista, ".",'
      '  sg.OrdemLista, ".",'
      '  co.OrdemLista) Ordens,'
      'co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Codigo in (:P0)'
      'ORDER BY co.Nome'
      ''
      '')
    Left = 472
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContas1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContas1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContas1Nome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContas1Nome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContas1ID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContas1Subgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContas1Empresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContas1Credito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContas1Debito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContas1Mensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContas1Exclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContas1Mensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContas1Mensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContas1Mensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContas1Menscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContas1Mensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContas1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContas1Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContas1Excel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContas1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContas1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContas1UserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContas1UserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContas1NOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContas1NOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContas1NOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContas1NOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrContas1NOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrContas1Niveis: TWideStringField
      FieldName = 'Niveis'
      Size = 100
    end
    object QrContas1Ordens: TWideStringField
      FieldName = 'Ordens'
      Size = 100
    end
  end
  object DsContas1: TDataSource
    DataSet = QrContas1
    Left = 472
    Top = 48
  end
end
