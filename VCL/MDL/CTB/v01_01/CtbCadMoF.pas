unit CtbCadMoF;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums;

type
  TFmCtbCadMoF = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCtbCadMoF: TMySQLQuery;
    QrCtbCadMoFCodigo: TIntegerField;
    QrCtbCadMoFNome: TWideStringField;
    DsCtbCadMoF: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    RGMoviFisc: TdmkRadioGroup;
    DBRGMoviFisc: TDBRadioGroup;
    QrCtbCadMoFMoviFisc: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCtbCadMoFAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCtbCadMoFBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCtbCadMoF: TFmCtbCadMoF;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnFinanceiro;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCtbCadMoF.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCtbCadMoF.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCtbCadMoFCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCtbCadMoF.DefParams;
var
  ATT_MovimID: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('ccm.MoviFisc', 'NO_MoviFisc', pvPos, True,
    sTypCtbCadMoF);

  VAR_GOTOTABELA := 'ctbcadmof';
  VAR_GOTOMYSQLTABLE := QrCtbCadMoF;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ' + ATT_MovimID + 'ccm.*');
  VAR_SQLx.Add('FROM ctbcadmof ccm');
  VAR_SQLx.Add('WHERE ccm.Codigo > 0');
  //
  VAR_SQL1.Add('AND ccm.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ccm.Nome Like :P0');
  //
end;

procedure TFmCtbCadMoF.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCtbCadMoF.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCtbCadMoF.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmCtbCadMoF.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCtbCadMoF.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCtbCadMoF.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCtbCadMoF.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCtbCadMoF.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtbCadMoF.BtAlteraClick(Sender: TObject);
begin
  if (QrCtbCadMoF.State <> dsInactive) and (QrCtbCadMoF.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCtbCadMoF, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'ctbcadmof');
  end;
end;

procedure TFmCtbCadMoF.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCtbCadMoFCodigo.Value;
  Close;
end;

procedure TFmCtbCadMoF.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, MoviFisc: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  MoviFisc       := RGMoviFisc.ItemIndex;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ctbcadmof', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctbcadmof', False, [
  'Nome', 'MoviFisc'], [
  'Codigo'], [
  Nome, MoviFisc], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCtbCadMoF.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ctbcadmof', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCtbCadMoF.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCtbCadMoF, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ctbcadmof');
end;

procedure TFmCtbCadMoF.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  MyObjects.PreencheComponente(RGMoviFisc, sTypCtbCadMoF, 3);
  MyObjects.PreencheComponente(DBRGMoviFisc, sTypCtbCadMoF, 3);
end;

procedure TFmCtbCadMoF.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCtbCadMoFCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCtbCadMoF.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCtbCadMoF.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCtbCadMoFCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCtbCadMoF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCtbCadMoF.QrCtbCadMoFAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCtbCadMoF.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCtbCadMoF.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCtbCadMoFCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ctbcadmof', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCtbCadMoF.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtbCadMoF.QrCtbCadMoFBeforeOpen(DataSet: TDataSet);
begin
  QrCtbCadMoFCodigo.DisplayFormat := FFormatFloat;
end;

end.

