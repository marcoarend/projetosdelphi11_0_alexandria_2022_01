unit DreGerImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, frxClass, frxDBSet,
  dmkDBLookupComboBox, dmkEditCB, Vcl.Menus;

type
  TFmDreGerImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    LaAno: TLabel;
    CBAno: TComboBox;
    LaMes: TLabel;
    CBMesI: TComboBox;
    Label3: TLabel;
    CBMesF: TComboBox;
    EdEmpresa: TdmkEditCB;
    Label1: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    QrDre: TMySQLQuery;
    QrDreSorcInfoGru: TIntegerField;
    QrDreSorcInfoItm: TIntegerField;
    QrDreAnoMes: TIntegerField;
    QrDreEmpresa: TIntegerField;
    QrDreLinha: TIntegerField;
    QrDreDreCfgEst: TIntegerField;
    QrDreDreCfgCtd: TIntegerField;
    QrDreDreCfgCpc: TIntegerField;
    QrDreCpcStatus: TSmallintField;
    QrDreDreCfgTp1: TIntegerField;
    QrDreDreCfgTp2: TIntegerField;
    QrDreData: TDateField;
    QrDreGenero: TIntegerField;
    QrDreDebito: TFloatField;
    QrDreCredito: TFloatField;
    QrDreGenCtb: TIntegerField;
    QrDreGenCtbD: TIntegerField;
    QrDreGenCtbC: TIntegerField;
    QrDreCfgCab: TMySQLQuery;
    QrDreCfgCabCodigo: TIntegerField;
    QrDreCfgCabEmpresa: TIntegerField;
    QrDreCfgCabRecVenProd: TIntegerField;
    QrDreCfgCabRecVenMerc: TIntegerField;
    QrDreCfgCabRecVenServ: TIntegerField;
    QrDreCfgCabDevVendas: TIntegerField;
    QrDreCfgCabTriVenICMS: TIntegerField;
    QrDreCfgCabTriVenPIS: TIntegerField;
    QrDreCfgCabTriVenCOFINS: TIntegerField;
    QrDreCfgCabTriVenIPI: TIntegerField;
    QrDreCfgCabTriDevICMS: TIntegerField;
    QrDreCfgCabTriDevPIS: TIntegerField;
    QrDreCfgCabTriDevCOFINS: TIntegerField;
    QrDreCfgCabTriDevIPI: TIntegerField;
    QrDreCfgCabCusFixVen: TIntegerField;
    QrTP1: TMySQLQuery;
    QrTP1DreCfgTp1: TIntegerField;
    QrTP1NO_DreCfgTp1: TWideStringField;
    QrTP1Valor: TFloatField;
    QrTP2: TMySQLQuery;
    QrTP2DreCfgTp2: TIntegerField;
    QrTP2NO_DreCfgTp2: TWideStringField;
    QrTP2Valor: TFloatField;
    QrGen: TMySQLQuery;
    QrGenGenero: TIntegerField;
    QrGenNO_PlaAllCad: TWideStringField;
    QrGenValor: TFloatField;
    frxDRE_GEREN_001_A1: TfrxReport;
    frxDsTP1: TfrxDBDataset;
    frxDsTP2: TfrxDBDataset;
    frxDsGen: TfrxDBDataset;
    QrSumCalc: TMySQLQuery;
    QrSumCalcValr1024: TFloatField;
    QrSumCalcValr2048: TFloatField;
    QrSumCalcValr4096: TFloatField;
    QrSumCalcValr6144: TFloatField;
    QrSumCalcValr8192: TFloatField;
    QrSumCalcValr9216: TFloatField;
    QrSumCalcValr11264: TFloatField;
    QrSumCalcValr13312: TFloatField;
    QrTP1TpUso: TIntegerField;
    frxDRE_GEREN_001_B1: TfrxReport;
    QrGenOrdens: TWideStringField;
    frxDRE_GEREN_001_C1: TfrxReport;
    PopupMenu1: TPopupMenu;
    RGRelatorio: TRadioGroup;
    frxDsDre: TfrxDBDataset;
    QrDreSerieNF: TWideStringField;
    QrDreNotaFiscal: TIntegerField;
    QrDreDescricao: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxDRE_GEREN_001_AGetValue(const VarName: string;
      var Value: Variant);
    procedure QrDCEstAfterScroll(DataSet: TDataSet);
    procedure QrTP1BeforeClose(DataSet: TDataSet);
    procedure QrTP1AfterScroll(DataSet: TDataSet);
    procedure QrGenAfterScroll(DataSet: TDataSet);
    procedure QrTP2AfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    FTabDre: String;
    FRelatorio, FEmpresa, FAnoMesI, FAnoMesF: Integer;
    //
    procedure GeraDados();
    procedure ImprimeDreAnalitico();
    procedure InsereSubTotal(SorcInfoGru, (*SorcInfoItm,*) DreCfgTp1: Integer;
              Valor: Double);
    //procedure ReopenDCEst();
    //procedure ReopenDCCtd();
    //procedure ReopenDCCpc();
    //
    procedure ReopenTP1();
    procedure ReopenTP2();
    procedure ReopenGen();
    procedure ReopenDre();
  public
    { Public declarations }
  end;

  var
  FmDreGerImp: TFmDreGerImp;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, DreCfgCab, UCreateFin,
  UMySQLModule;

{$R *.DFM}

procedure TFmDreGerImp.BtOKClick(Sender: TObject);
var
  Report: TfrxReport;
begin
  FRelatorio := RGRelatorio.ItemIndex;
  if MyObjects.FIC(FRelatorio = -1, RGRelatorio, 'Selecione o relat�rio!') then Exit;
  //
  GeraDados();
  //
  ReopenTP1();
  //
  case RGRelatorio.ItemIndex of
    0: Report := frxDRE_GEREN_001_A1;
    1: Report := frxDRE_GEREN_001_B1;
    2: Report := frxDRE_GEREN_001_C1;
    else
    begin
      Geral.MB_Aviso('Relat�rio n�o implementado!');
      Exit;
    end;
  end;
  MyObjects.frxDefineDatasets(Report, [
    DModG.frxDsDono,
    frxDsTP1,
    frxDsTP2,
    frxDsGen,
    frxDsDre
  ]);
  //
  MyObjects.frxMostra(Report, 'DRE');
end;

procedure TFmDreGerImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDreGerImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDreGerImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FTabDre  := '';
  FEmpresa := 0;
  FAnoMesI := 0;
  FAnoMesF := 0;
  FRelatorio := -1;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  MyObjects.PreencheCBAnoECBMes2(CBAno, CBMesI, CBMesF, -1, -1);
end;

procedure TFmDreGerImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDreGerImp.frxDRE_GEREN_001_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_NO_EMPRESA') = 0 then
    Value := DModG.QrDonoNOMEDONO.Value
  else if AnsiCompareText(VarName, 'VAR_FILIAL') = 0 then
    Value := DModG.QrEmpresasFilial.Value
  else if AnsiCompareText(VarName, 'VAR_CINOME') = 0 then
    Value := CBEmpresa.Text
  else if AnsiCompareText(VarName, 'VAR_PERIODO') = 0 then
  begin
    if CBMesI.Text = CBMesF.Text then
      Value := CBMesI.Text + ' de ' + CBAno.Text
    else
      Value := CBMesI.Text + ' a ' + CBMesF.Text + ' de ' + CBAno.Text
  end
  else
(*
  if AnsiCompareText(VarName, 'VFR_AGR1NOME') = 0 then
  begin
    case PCRelatorio.ActivePageIndex of
      PCRelatorio_PAGEINDEX_00_Estoque: //0:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrEstqNOMECI.Value;
          1: Value := QrEstqNOMESE.Value;
          2: Value := QrEstqNOMEPQ.Value;
          3: Value := QrEstqNOMEFO.Value;
          else Value := '';
        end;
      end;
      PCRelatorio_PAGEINDEX_01_UsoTerc: //2:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrUsoTNOMEORI.Value;
          1: Value := QrUsoTNOMESE.Value;
          2: Value := QrUsoTNOMEPQ.Value;
          3: Value := QrUsoTNOMEFO.Value;
          else Value := '';
        end;
      end;
      PCRelatorio_PAGEINDEX_02_ConsSetr,
      PCRelatorio_PAGEINDEX_03_EstoquEm: //   3,5:
      begin
        if CkNovaForma6.Checked and (PCRelatorio.ActivePageIndex = 5) then
        begin
          case RGOrdem1.ItemIndex of
            0: Value := QrMoviPQNomeCI.Value;
            1: Value := QrMoviPQNomeSE.Value;
            2: Value := QrMoviPQNomePQ.Value;
            3: Value := QrMoviPQNomeFO.Value;
            else Value := '';
          end;
        end else
        begin
          case RGOrdem1.ItemIndex of
            0: Value := QrMoviPQ3NomeCI.Value;
            1: Value := QrMoviPQ3NomeSE.Value;
            2: Value := QrMoviPQ3NomePQ.Value;
            3: Value := QrMoviPQ3NomeFO.Value;
            else Value := '';
          end;
        end;
      end;
      PCRelatorio_PAGEINDEX_07_EstqNfCC: //     9:
      begin
        case RGOrdem1.ItemIndex of
          0: Value := QrPQRSumNOMECI.Value;
          1: Value := QrPQRSumNOMESE.Value;
          2: Value := QrPQRSumNOMEPQ.Value;
          3: Value := QrPQRSumNOMEFO.Value;
          else Value := '';
        end;
      end;
    end;
  end else if AnsiCompareText(VarName, 'VFR_AGR2NOME') = 0 then
  begin
    case PCRelatorio.ActivePageIndex of
      PCRelatorio_PAGEINDEX_00_Estoque: //0:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrEstqNOMECI.Value;
          1: Value := QrEstqNOMESE.Value;
          2: Value := QrEstqNOMEPQ.Value;
          3: Value := QrEstqNOMEFO.Value;
          else Value := '';
        end;
      end;
      PCRelatorio_PAGEINDEX_01_UsoTerc: //2:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrUsoTNOMEORI.Value;
          1: Value := QrUsoTNOMESE.Value;
          2: Value := QrUsoTNOMEPQ.Value;
          3: Value := QrUsoTNOMEFO.Value;
          else Value := '';
        end;
      end;
      PCRelatorio_PAGEINDEX_02_ConsSetr,
      PCRelatorio_PAGEINDEX_03_EstoquEm: //   3,5:
      begin
        if CkNovaForma6.Checked and (PCRelatorio.ActivePageIndex = 5) then
        begin
          case RGOrdem1.ItemIndex of
            0: Value := QrMoviPQ3NomeCI.Value;
            1: Value := QrMoviPQ3NomeSE.Value;
            2: Value := QrMoviPQ3NomePQ.Value;
            3: Value := QrMoviPQ3NomeFO.Value;
            else Value := '';
          end;
        end else
        begin
          case RGOrdem2.ItemIndex of
            0: Value := QrMoviPQNomeCI.Value;
            1: Value := QrMoviPQNomeSE.Value;
            2: Value := QrMoviPQNomePQ.Value;
            3: Value := QrMoviPQNomeFO.Value;
            else Value := '';
          end;
        end;
      end;
      PCRelatorio_PAGEINDEX_07_EstqNfCC: //     9:
      begin
        case RGOrdem2.ItemIndex of
          0: Value := QrPQRSumNOMECI.Value;
          1: Value := QrPQRSumNOMESE.Value;
          2: Value := QrPQRSumNOMEPQ.Value;
          3: Value := QrPQRSumNOMEFO.Value;
          else Value := '';
        end;
      end;
    end;
  end


  else if AnsiCompareText(VarName, 'VFR_AGR')  = 0 then Value := RGAgrupa.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD1') = 0 then Value := RGOrdem1.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD2') = 0 then Value := RGOrdem2.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_ORD3') = 0 then Value := RGOrdem3.ItemIndex
  else if AnsiCompareText(VarName, 'VFR_GRD') = 0 then
    Value := dmkPF.BoolToInt2(CkGrade.Checked, 15, 0)
  else
*)
end;

procedure TFmDreGerImp.GeraDados();
var
  Filial, Ano: Integer;
  Gru1, Gru2, Gru3, Gru4, Gru5, Gru8,
  Itm1, Itm2, Itm3(*, itm4*), Itm0,
  SQL_AnoMes,
  RecVenProd, RecVenMerc, RecVenServ, DevVendas, TriVenICMS, TriVenPIS,
  TriVenCOFINS, TriVenIPI, TriDevICMS, TriDevPIS, TriDevCOFINS, TriDevIPI,
  CusFixVen: String;
  //
  SorcInfoGru, SorcInfoItm, DreCfgTp1: Integer;
  Valor: Double;
begin
//definir padroes nas opcoes para itens sem conta do plano!
  Filial   := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Informe a ermpresa!') then Exit;
  FEmpresa := DMOdG.QrEmpresasCodigo.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM drecfgcab ',
  'WHERE Codigo=' + Geral.FF0(FEmpresa),
  '']);
  RecVenProd   := Geral.FF0(QrDreCfgCabRecVenProd.Value);
  RecVenMerc   := Geral.FF0(QrDreCfgCabRecVenMerc.Value);
  RecVenServ   := Geral.FF0(QrDreCfgCabRecVenServ.Value);
  DevVendas    := Geral.FF0(QrDreCfgCabDevVendas.Value);
  TriVenICMS   := Geral.FF0(QrDreCfgCabTriVenICMS.Value);
  TriVenPIS    := Geral.FF0(QrDreCfgCabTriVenPIS.Value);
  TriVenCOFINS := Geral.FF0(QrDreCfgCabTriVenCOFINS.Value);
  TriVenIPI    := Geral.FF0(QrDreCfgCabTriVenIPI.Value);
  TriDevICMS   := Geral.FF0(QrDreCfgCabTriDevICMS.Value);
  TriDevPIS    := Geral.FF0(QrDreCfgCabTriDevPIS.Value);
  TriDevCOFINS := Geral.FF0(QrDreCfgCabTriDevCOFINS.Value);
  TriDevIPI    := Geral.FF0(QrDreCfgCabTriDevIPI.Value);
  CusFixVen    := Geral.FF0(QrDreCfgCabCusFixVen.Value);
  //
  Ano      := Geral.IMV(CBAno.Text);
  FAnoMesI := (Ano * 100) + CBMesI.ItemIndex + 1;
  FAnoMesF := (Ano * 100) + CBMesF.ItemIndex + 1;
  SQL_AnoMes :=  'WHERE AnoMes BETWEEN ' + Geral.FF0(FAnoMesI) + ' AND ' + Geral.FF0(FAnoMesF);
  //
  FTabDre := UCriarFin.RecriaTempTableNovo(ntrtt_Dre, DModG.QrUpdPID1, False);
  //
  Gru1 := Geral.FF0(Integer(TDreGerSorcInfoGru.dgsiReceita));
  Gru2 := Geral.FF0(Integer(TDreGerSorcInfoGru.dgsiDevolRec));
  Gru3 := Geral.FF0(Integer(TDreGerSorcInfoGru.dgsiTribRec));
  Gru4 := Geral.FF0(Integer(TDreGerSorcInfoGru.dgsiTribDev));
  Gru5 := Geral.FF0(Integer(TDreGerSorcInfoGru.dgsiCustDirVar));
  Gru8 := Geral.FF0(Integer(TDreGerSorcInfoGru.dgsiLancFin));
  //
  Itm0 := Geral.FF0(Integer(TDreGerSorcInfoItm.dgsiND));
  Itm1 := Geral.FF0(Integer(TDreGerSorcInfoItm.dgsiICMS));
  Itm2 := Geral.FF0(Integer(TDreGerSorcInfoItm.dgsiPIS));
  Itm3 := Geral.FF0(Integer(TDreGerSorcInfoItm.dgsiCOFINS));
  //Itm4 := Geral.FF0(Integer(TDreGerSorcInfoItm.dgsiIPI));
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumCalc, DModG.MyPID_DB, [
  'DELETE FROM _dre_; ',
  ' ',
(*
  'INSERT INTO _dre_ ',
  'SELECT ' + Gru1 + '  SorcInfoGru, ' + Itm0 + ' SorcInfoItm, ',
  'AnoMes, Empresa, Linha,  ',
  '0 DreCfgEst, 0 DreCfgCtd,  ',
  '0 DreCfgCpc, 0 CpcStatus, 1024 DreCfgTp1,  ',
  '1024 DreCfgTp2, ide_dEmi Data, ' + RecVenProd + ' Genero,  ',
  '0 Debito, ICMSTot_vNF Credito, 0 GenCtb,  ',
  '0 GenCtbD, 0 GenCtbC, ide_serie SerieNF, ide_nNF NotaFiscal, ',
  '"" Descricao ',
  'FROM ' + TMeuDB + '.dregerven ',
  SQL_AnoMes,
  '; ',
  ' ',
  'INSERT INTO _dre_ ',
  'SELECT ' + Gru2 + '  SorcInfoGru, ' + Itm0 + ' SorcInfoItm, ',
  'AnoMes, Empresa, Linha, ',
  '0 DreCfgEst, 0 DreCfgCtd,  ',
  '0 DreCfgCpc, 0 CpcStatus, 2048 DreCfgTp1,  ',
  '4096 DreCfgTp2, DT_E_S Data, ' + DevVendas + ' Genero,  ',
  'VL_DOC Debito, 0 Credito, 0 GenCtb, ',
  '0 GenCtbD, 0 GenCtbC, SER SerieNF, NUM_DOC NotaFiscal, ',
  '"" Descricao ',
  'FROM ' + TMeuDB + '.dregerdev ',
  SQL_AnoMes,
  '; ',
*)
  ' ',
  'INSERT INTO _dre_ ',
  'SELECT ' + Gru3 + '  SorcInfoGru, ' + Itm1 + ' SorcInfoItm, ',
  'AnoMes, Empresa, Linha,  ',
  '0 DreCfgEst, 0 DreCfgCtd,  ',
  '0 DreCfgCpc, 0 CpcStatus, 2048 DreCfgTp1,  ',
  '6144 DreCfgTp2, ide_dEmi Data, ' + TriVenICMS + ' Genero,  ',
  'ICMSTot_vICMS Debito, 0 Credito, 0 GenCtb,  ',
  '0 GenCtbD, 0 GenCtbC, ide_serie SerieNF, ide_nNF NotaFiscal, ',
  '"ICMS" Descricao ',
  'FROM ' + TMeuDB + '.dregerven ',
  SQL_AnoMes,
  'AND ICMSTot_vICMS > 0 ',
  '; ',
  ' ',
  'INSERT INTO _dre_ ',
  'SELECT ' + Gru3 + '  SorcInfoGru, ' + Itm2 + ' SorcInfoItm, ',
  'AnoMes, Empresa, Linha,  ',
  '0 DreCfgEst, 0 DreCfgCtd,  ',
  '0 DreCfgCpc, 0 CpcStatus, 2048 DreCfgTp1,  ',
  '6144 DreCfgTp2, ide_dEmi Data, ' + TriVenPIS + ' Genero,  ',
  'ICMSTot_vPIS Debito, 0 Credito, 0 GenCtb,  ',
  '0 GenCtbD, 0 GenCtbC, ide_serie SerieNF, ide_nNF NotaFiscal, ',
  '"PIS" Descricao ',
  'FROM ' + TMeuDB + '.dregerven ',
  SQL_AnoMes,
  'AND ICMSTot_vPIS > 0 ',
  '; ',
  ' ',
  'INSERT INTO _dre_ ',
  'SELECT ' + Gru3 + '  SorcInfoGru, ' + Itm3 + ' SorcInfoItm, ',
  'AnoMes, Empresa, Linha,  ',
  '0 DreCfgEst, 0 DreCfgCtd,  ',
  '0 DreCfgCpc, 0 CpcStatus, 2048 DreCfgTp1,  ',
  '6144 DreCfgTp2, ide_dEmi Data, ' + TriVenCOFINS + ' Genero,  ',
  'ICMSTot_vCOFINS Debito, 0 Credito, 0 GenCtb,  ',
  '0 GenCtbD, 0 GenCtbC, ide_serie SerieNF, ide_nNF NotaFiscal, ',
  '"COFINS" Descricao ',
  'FROM ' + TMeuDB + '.dregerven ',
  SQL_AnoMes,
  'AND ICMSTot_vCOFINS > 0 ',
  '; ',
  ' ',
  'INSERT INTO _dre_ ',
  'SELECT ' + Gru4 + '  SorcInfoGru, ' + Itm1 + ' SorcInfoItm, ',
  'AnoMes, Empresa, Linha,  ',
  '0 DreCfgEst, 0 DreCfgCtd,  ',
  '0 DreCfgCpc, 0 CpcStatus, 2048 DreCfgTp1,  ',
  '6144 DreCfgTp2, DT_E_S Data, ' + TriDevICMS + ' Genero,  ',
  '0 Debito, VL_ICMS  Credito, 0 GenCtb, ',
  '0 GenCtbD, 0 GenCtbC, SER SerieNF, NUM_DOC NotaFiscal, ',
  '"ICMS" Descricao ',
  'FROM ' + TMeuDB + '.dregerdev ',
  SQL_AnoMes,
  'AND VL_ICMS > 0 ',
  '; ',
  ' ',
  'INSERT INTO _dre_ ',
  'SELECT ' + Gru4 + '  SorcInfoGru, ' + Itm2 + ' SorcInfoItm, ',
  'AnoMes, Empresa, Linha,  ',
  '0 DreCfgEst, 0 DreCfgCtd,  ',
  '0 DreCfgCpc, 0 CpcStatus, 2048 DreCfgTp1,  ',
  '6144 DreCfgTp2, DT_E_S Data, ' + TriDevPIS + ' Genero,  ',
  '0 Debito, VL_PIS  Credito, 0 GenCtb, ',
  '0 GenCtbD, 0 GenCtbC, SER SerieNF, NUM_DOC NotaFiscal, ',
  '"PIS" Descricao ',
  'FROM ' + TMeuDB + '.dregerdev ',
  SQL_AnoMes,
  'AND VL_PIS > 0 ',
  '; ',
  ' ',
  'INSERT INTO _dre_ ',
  'SELECT ' + Gru4 + '  SorcInfoGru, ' + Itm3 + ' SorcInfoItm, ',
  'AnoMes, Empresa, Linha,  ',
  '0 DreCfgEst, 0 DreCfgCtd,  ',
  '0 DreCfgCpc, 0 CpcStatus, 2048 DreCfgTp1,  ',
  '6144 DreCfgTp2, DT_E_S Data, ' + TriDevCOFINS + ' Genero,  ',
  '0 Debito, VL_COFINS  Credito, 0 GenCtb, ',
  '0 GenCtbD, 0 GenCtbC, SER SerieNF, NUM_DOC NotaFiscal, ',
  '"COFINS" Descricao ',
  'FROM ' + TMeuDB + '.dregerdev ',
  SQL_AnoMes,
  'AND VL_COFINS > 0 ',
  '; ',
  ' ',
  'INSERT INTO _dre_ ',
  'SELECT ' + Gru5 + '  SorcInfoGru, ' + Itm0 + ' SorcInfoItm, ',
  'AnoMes, Empresa, Linha,  ',
  '0 DreCfgEst, 0 DreCfgCtd,  ',
  '0 DreCfgCpc, 0 CpcStatus, 4096 DreCfgTp1,  ',
  '9216 DreCfgTp2, ide_dEmi Data, ' + CusFixVen + ' Genero,  ',
  'CustoT Debito, 0 Credito, 0 GenCtb,  ',
  '0 GenCtbD, 0 GenCtbC, ide_serie SerieNF, ide_nNF NotaFiscal, ',
  '"Custo fixo dos produtos vendidos" Descricao ',
  'FROM ' + TMeuDB + '.dregerven ',
  SQL_AnoMes,
  'AND CustoT <> 0 ',
  '; ',
  ' ',
  'INSERT INTO _dre_ ',
  'SELECT ' + Gru8 + '  SorcInfoGru, ' + Itm0 + ' SorcInfoItm, ',
  'AnoMes, Empresa, Linha,  ',
  'DreCfgEst, DreCfgCtd,  ',
  'DreCfgCpc, CpcStatus, DreCfgTp1,  ',
  'DreCfgTp2, Data, Genero,  ',
  'Debito, Credito, GenCtb, ',
  'GenCtbD, GenCtbC, SerieNF, NotaFiscal, ',
  'Descricao ',
  'FROM ' + TMeuDB + '.dregerfin ',
  SQL_AnoMes,
  '; ',
  ' ',
  'SELECT ',
  'SUM(IF(DreCfgTp1=1024, Credito-Debito, 0)) Valr1024,  ',
  'SUM(IF(DreCfgTp1=2048, Credito-Debito, 0)) Valr2048,  ',
  'SUM(IF(DreCfgTp1=4096, Credito-Debito, 0)) Valr4096,  ',
  'SUM(IF(DreCfgTp1=6144, Credito-Debito, 0)) Valr6144,',
  'SUM(IF(DreCfgTp1=8192, Credito-Debito, 0)) Valr8192,',
  'SUM(IF(DreCfgTp1=9216, Credito-Debito, 0)) Valr9216,',
  'SUM(IF(DreCfgTp1=11264, Credito-Debito, 0)) Valr11264,',
  'SUM(IF(DreCfgTp1=13312, Credito-Debito, 0)) Valr13312',
  '  ',
  'FROM _dre_ ',
  '']);
  //Geral.MB_Teste(QrDre.SQL.Text);
  //
  SorcInfoGru := Integer(TDreGerSorcInfoGru.dgsiSubTotais);
  //DreCfgTp1:
  //Integer; Valor: Double);
  Valor := QrSumCalcValr1024.Value + QrSumCalcValr2048.Value;
  InsereSubTotal(SorcInfoGru, 3072, Valor);
  //
  Valor := Valor +  QrSumCalcValr4096.Value;
  InsereSubTotal(SorcInfoGru, 5120, Valor);
  //
  Valor := Valor +  QrSumCalcValr6144.Value;
  InsereSubTotal(SorcInfoGru, 7168, Valor);
  //
  Valor := Valor +  QrSumCalcValr8192.Value + QrSumCalcValr9216.Value;
  InsereSubTotal(SorcInfoGru, 10240, Valor);
  //
  Valor := Valor +  QrSumCalcValr11264.Value;
  InsereSubTotal(SorcInfoGru, 12288, Valor);
  //
  Valor := Valor +  QrSumCalcValr13312.Value;
  InsereSubTotal(SorcInfoGru, 14336, Valor);
  //
end;

procedure TFmDreGerImp.ImprimeDreAnalitico();
begin
  GeraDados();
  //
(*
  ReopenDCEst();
  //
  MyObjects.frxDefineDatasets(frxDRE_GEREN_001_A, [
    DModG.frxDsDono,
    frxDsAnali
  ]);
  //
  MyObjects.frxMostra(frxDRE_GEREN_001_A, 'DRE');
*)
  ReopenTP1();
  //
  MyObjects.frxDefineDatasets(frxDRE_GEREN_001_A1, [
    DModG.frxDsDono,
    frxDsTP1,
    frxDsTP2,
    frxDsGen
  ]);
  //
  MyObjects.frxMostra(frxDRE_GEREN_001_A1, 'DRE anal�tico');
end;

procedure TFmDreGerImp.InsereSubTotal(SorcInfoGru, (*SorcInfoItm,*) DreCfgTp1:
  Integer; Valor: Double);
var
  Data, SerieNF, Descricao: String;
  AnoMes, Empresa, Linha, DreCfgEst, DreCfgCtd, DreCfgCpc, CpcStatus, DreCfgTp2,
  Genero, GenCtb, GenCtbD, GenCtbC, NotaFiscal: Integer;
  Debito, Credito: Double;
  SQLType: TSQLType;
  SorcInfoItm: Integer;
begin
  SQLType        := stIns;
  //SorcInfoGru    := ;
  SorcInfoItm    := DreCfgTp1;
  AnoMes         := 0;
  Empresa        := 0;
  Linha          := 0;
  DreCfgEst      := 0;
  DreCfgCtd      := 0;
  DreCfgCpc      := 0;
  CpcStatus      := 0;
  //DreCfgTp1      := ;
  DreCfgTp2      := 0;
  Data           := '0000-00-00';
  Genero         := 0;
  Debito         := 0.00;
  Credito        := Valor;
  GenCtb         := 0;
  GenCtbD        := 0;
  GenCtbC        := 0;
  SerieNF        := '';
  NotaFiscal     := 0;
  Descricao      := '';
  //
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, SQLType, '_dre_', False, [
  'DreCfgEst', 'DreCfgCtd', 'DreCfgCpc',
  'CpcStatus', 'DreCfgTp1', 'DreCfgTp2',
  'Data', 'Genero', 'Debito',
  'Credito', 'GenCtb', 'GenCtbD',
  'GenCtbC', 'SerieNF', 'NotaFiscal',
  'Descricao'], [
  'SorcInfoGru', 'SorcInfoItm', 'AnoMes', 'Empresa', 'Linha'], [
  DreCfgEst, DreCfgCtd, DreCfgCpc,
  CpcStatus, DreCfgTp1, DreCfgTp2,
  Data, Genero, Debito,
  Credito, GenCtb, GenCtbD,
  GenCtbC, SerieNF, NotaFiscal,
  Descricao], [
  SorcInfoGru, SorcInfoItm, AnoMes, Empresa, Linha], False);
end;

procedure TFmDreGerImp.QrDCEstAfterScroll(DataSet: TDataSet);
begin
  //ReopenDCCtd();
end;

procedure TFmDreGerImp.QrGenAfterScroll(DataSet: TDataSet);
begin
  if FRelatorio > 1 then
    ReopenDre();
end;

procedure TFmDreGerImp.QrTP1AfterScroll(DataSet: TDataSet);
begin
  ReopenTP2();
end;

procedure TFmDreGerImp.QrTP1BeforeClose(DataSet: TDataSet);
begin
  QrTP2.Close;
end;

procedure TFmDreGerImp.QrTP2AfterScroll(DataSet: TDataSet);
begin
  if FRelatorio > 0 then
    ReopenGen();
end;

(*
procedure TFmDreGerImp.ReopenDCCpc();
begin
  //
end;
*)
{
procedure TFmDreGerImp.ReopenDCCtd();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDCCtd, Dmod.MyDB, [
  'SELECT * ',
  'FROM drecfgctd ',
  'WHERE Controle=' + Geral.FF0(QrDCEstControle.Value),
  '']);
end;

procedure TFmDreGerImp.ReopenDCEst();
begin
  // Parei aqui!
  //buscar todas estruturas de todos meses selecionados.
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDCEst, Dmod.MyDB, [
  'SELECT * ',
  'FROM drecfgest ',
  //'WHERE Codigo=' + Geral.FF0(DreCfgCab),
  'ORDER BY Ordem, Controle ',
  '']);
end;
}

procedure TFmDreGerImp.ReopenDre();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDre, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM _dre_ ',
  'WHERE DreCfgTp1=' + Geral.FF0(QrTP1DreCfgTp1.Value),
  'AND DreCfgTp2=' + Geral.FF0(QrTP2DreCfgTp2.Value),
  'AND Genero=' + Geral.FF0(QrGenGenero.Value),
  '']);
end;

procedure TFmDreGerImp.ReopenGen();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGen, DModG.MyPID_DB, [
  'SELECT dre.Genero, pac.Ordens, pac.Nome NO_PlaAllCad,  ',
  'ABS(SUM(dre.Credito-dre.Debito)) Valor  ',
  'FROM _dre_ dre ',
  'LEFT JOIN ' + TMeuDB + '.plaallcad pac ON pac.Codigo=dre.Genero ',
  'WHERE dre.DreCfgTp1=' + Geral.FF0(QrTP1DreCfgTp1.Value),
  'AND dre.DreCfgTp2=' + Geral.FF0(QrTP2DreCfgTp2.Value),
  'GROUP BY dre.Genero ',
  '']);
end;

procedure TFmDreGerImp.ReopenTP1();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTP1, DModG.MyPID_DB, [
  'SELECT dre.DreCfgTp1, tp1.TpUso,  tp1.Nome NO_DreCfgTp1,  ',
  //'ABS(SUM(dre.Credito-dre.Debito)) Valor  ',
  '(SUM(dre.Credito-dre.Debito)) Valor  ',
  'FROM _dre_ dre ',
  'LEFT JOIN ' + TMeuDB + '.drecfgtp1 tp1 ON tp1.Codigo=dre.DreCfgTp1 ',
  'GROUP BY dre.DreCfgTp1 ',
  '']);
end;

procedure TFmDreGerImp.ReopenTP2();
var
  DreCfgTp1: Integer;
  SQL_ABS: String;
begin
  if QrTP1TpUso.Value = 2 then
  begin
    DreCfgTp1 := -999999999;
    SQL_ABS := '(SUM(dre.Credito-dre.Debito)) Valor ';
  end else
  begin
    DreCfgTp1 := QrTP1DreCfgTp1.Value;
    SQL_ABS := 'ABS(SUM(dre.Credito-dre.Debito)) Valor ';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTP2, DModG.MyPID_DB, [
  'SELECT dre.DreCfgTp2, tp2.Nome NO_DreCfgTp2,  ',
  SQL_ABS,
  'FROM _dre_ dre ',
  'LEFT JOIN ' + TMeuDB + '.drecfgtp2 tp2 ON tp2.Controle=dre.DreCfgTp2 ',
  'WHERE dre.DreCfgTp1=' + Geral.FF0(DreCfgTp1),
  'GROUP BY dre.DreCfgTp2 ',
  '']);
  //
end;

end.
