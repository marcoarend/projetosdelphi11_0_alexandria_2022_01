object FmDreGerCab: TFmDreGerCab
  Left = 339
  Top = 185
  Caption = 'DRE-GEREN-001 :: DRE - Demonstrativo de Resultado do Exerc'#237'cio'
  ClientHeight = 628
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 1264
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 1216
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 90
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
    end
    object GB_M: TGroupBox
      Left = 90
      Top = 0
      Width = 870
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 1126
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 499
        Height = 32
        Caption = 'Demonstrativo de Resultado do Exerc'#237'cio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 499
        Height = 32
        Caption = 'Demonstrativo de Resultado do Exerc'#237'cio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 499
        Height = 32
        Caption = 'Demonstrativo de Resultado do Exerc'#237'cio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 558
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 1
    ExplicitWidth = 1264
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 1118
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 1116
      object GBAvisos1: TGroupBox
        Left = 0
        Top = 0
        Width = 860
        Height = 53
        Align = alClient
        Caption = ' Avisos: '
        TabOrder = 0
        ExplicitWidth = 1116
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 856
          Height = 36
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 1112
          object LaAviso1: TLabel
            Left = 13
            Top = -2
            Width = 120
            Height = 17
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 12
            Top = -3
            Width = 120
            Height = 17
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object PB1: TProgressBar
            Left = 0
            Top = 19
            Width = 856
            Height = 17
            Align = alBottom
            TabOrder = 0
            ExplicitWidth = 1112
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 510
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 1264
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 45
      Align = alTop
      TabOrder = 0
      ExplicitWidth = 1264
      object Panel6: TPanel
        Left = 1
        Top = -1
        Width = 1006
        Height = 45
        Align = alBottom
        BevelOuter = bvLowered
        TabOrder = 0
        ExplicitWidth = 1262
        object Label1: TLabel
          Left = 8
          Top = 2
          Width = 23
          Height = 13
          Caption = 'Filial:'
        end
        object EdEmpresa: TdmkEditCB
          Left = 8
          Top = 18
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdEmpresaRedefinido
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 64
          Top = 18
          Width = 301
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 45
      Width = 1008
      Height = 465
      Align = alClient
      TabOrder = 1
      ExplicitWidth = 1264
      object Panel10: TPanel
        Left = 1
        Top = 1
        Width = 102
        Height = 463
        Align = alLeft
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object DBGCab: TDBGrid
          Left = 0
          Top = 41
          Width = 102
          Height = 422
          Align = alClient
          DataSource = DsDreGerCab
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'MES_ANO'
              Title.Caption = 'MES / ANO'
              Width = 63
              Visible = True
            end>
        end
        object Panel29: TPanel
          Left = 0
          Top = 0
          Width = 102
          Height = 41
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object BtPeriodo: TBitBtn
            Tag = 10
            Left = 0
            Top = 0
            Width = 102
            Height = 41
            Cursor = crHandPoint
            Align = alClient
            Caption = '&Per'#237'odo'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtPeriodoClick
          end
        end
      end
      object Panel9: TPanel
        Left = 103
        Top = 1
        Width = 904
        Height = 463
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitWidth = 1160
        object PCDados: TPageControl
          Left = 0
          Top = 0
          Width = 904
          Height = 419
          ActivePage = TabSheet4
          Align = alClient
          TabOrder = 0
          OnChange = PCDadosChange
          ExplicitWidth = 1160
          object TabSheet1: TTabSheet
            Caption = ' Faturamento '
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 896
              Height = 391
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 1152
              object DBGrid1: TDBGrid
                Left = 0
                Top = 0
                Width = 896
                Height = 391
                Align = alClient
                DataSource = DsDreGerVen
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Linha'
                    Title.Caption = 'Item'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'FatNum'
                    Title.Caption = 'Fatura'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ide_dEmi'
                    Title.Caption = 'Dta. Emi.'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ide_serie'
                    Title.Caption = 'S'#233'rie'
                    Width = 28
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ide_nNF'
                    Title.Caption = 'N'#176' NFe'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMSTot_vNF'
                    Title.Caption = '$ NFe'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMSTot_vICMS'
                    Title.Caption = '$ ICMS'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMSTot_vPIS'
                    Title.Caption = '$ PIS'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMSTot_vCOFINS'
                    Title.Caption = '$ COFINS'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMSTot_vIPI'
                    Title.Caption = '$ IPI'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MovimCod'
                    Title.Caption = 'IME-C'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Qtde'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CustoT'
                    Title.Caption = '$ Custo'
                    Width = 72
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet3: TTabSheet
            Caption = ' Devolu'#231#245'es de Vendas'
            ImageIndex = 1
            object DBGrid3: TDBGrid
              Left = 0
              Top = 0
              Width = 896
              Height = 391
              Align = alClient
              DataSource = DsDreGerDev
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Linha'
                  Title.Caption = 'Item'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MovFatID'
                  Title.Caption = 'Fat.ID'
                  Width = 24
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MovFatNum'
                  Title.Caption = 'Fat.Num'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DT_DOC'
                  Title.Caption = 'Dta. Emi.'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SER'
                  Title.Caption = 'S'#233'rie'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NUM_DOC'
                  Title.Caption = 'N'#176' NFe'
                  Width = 62
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VL_DOC'
                  Title.Caption = '$ NFe'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VL_ICMS'
                  Title.Caption = '$ ICMS'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VL_PIS'
                  Title.Caption = '$ PIS'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VL_COFINS'
                  Title.Caption = '$ COFINS'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VL_IPI'
                  Title.Caption = '$ IPI'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MovimCod'
                  Title.Caption = 'IME-C'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Qtde'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CustoT'
                  Title.Caption = '$ Valor'
                  Width = 72
                  Visible = True
                end>
            end
          end
          object TabSheet2: TTabSheet
            Caption = ' Lan'#231'amentos Financeiros Autom'#225'ticos'
            ImageIndex = 2
            object DBGrid2: TDBGrid
              Left = 0
              Top = 0
              Width = 896
              Height = 391
              Align = alClient
              DataSource = DsDreGerFinAut
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Linha'
                  Title.Caption = 'Seq'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DreCfgTp1'
                  Title.Caption = 'Estrutura'
                  Width = 26
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_DreCfgTp1'
                  Title.Caption = 'Nome estrutura'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DreCfgTp2'
                  Title.Caption = 'Conte'#250'do'
                  Width = 26
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_DreCfgTp2'
                  Title.Caption = 'Nome conte'#250'do'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Carteira'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PAC_Genero'
                  Title.Caption = 'Descri'#231#227'o G'#234'nero'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Caption = 'D'#233'bito'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Caption = 'Cr'#233'dito'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PAC_Cpc'
                  Title.Caption = 'Item do n'#237'vel selecionado'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TERCEIRO'
                  Title.Caption = 'Terceiro'
                  Width = 140
                  Visible = True
                end>
            end
          end
          object TabSheet4: TTabSheet
            Caption = ' Lan'#231'amentos Financeiros Avulsos'
            ImageIndex = 3
            object DBGrid4: TDBGrid
              Left = 0
              Top = 0
              Width = 896
              Height = 391
              Align = alClient
              DataSource = DsDreGerFinAvu
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Linha'
                  Title.Caption = 'Seq'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DreCfgTp1'
                  Title.Caption = 'Estrutura'
                  Width = 26
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_DreCfgTp1'
                  Title.Caption = 'Nome estrutura'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DreCfgTp2'
                  Title.Caption = 'Conte'#250'do'
                  Width = 26
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_DreCfgTp2'
                  Title.Caption = 'Nome conte'#250'do'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Carteira'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PAC_Genero'
                  Title.Caption = 'Descri'#231#227'o G'#234'nero'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Caption = 'D'#233'bito'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Caption = 'Cr'#233'dito'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PAC_Cpc'
                  Title.Caption = 'Item do n'#237'vel selecionado'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TERCEIRO'
                  Title.Caption = 'Terceiro'
                  Width = 140
                  Visible = True
                end>
            end
          end
        end
        object Panel11: TPanel
          Left = 0
          Top = 419
          Width = 904
          Height = 44
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitWidth = 1160
          object BtObter: TBitBtn
            Tag = 1000378
            Left = 16
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Obter'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtObterClick
          end
          object BtAvulso: TBitBtn
            Tag = 1000151
            Left = 140
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Avulso'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAvulsoClick
          end
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 84
    Top = 39
  end
  object PMPeriodo: TPopupMenu
    OnPopup = PMPeriodoPopup
    Left = 40
    Top = 196
    object Incluinovoperiodo1: TMenuItem
      Caption = 'Inclui novo periodo'
      OnClick = Incluinovoperiodo1Click
    end
    object Excluiperiodoselecionado1: TMenuItem
      Caption = '&Exclui periodo selecionado'
      Enabled = False
      OnClick = Excluiperiodoselecionado1Click
    end
  end
  object QrDreGerCab: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrDreGerCabAfterOpen
    BeforeClose = QrDreGerCabBeforeClose
    AfterScroll = QrDreGerCabAfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, '
      'CONCAT(RIGHT(dgc.AnoMes, 2), "/", '
      'LEFT(LPAD(dgc.AnoMes, 6, "0"), 4)) MES_ANO,'
      'dcc.Nome NO_DreCfgCab, dgc.* '
      'FROM dregercab dgc'
      'LEFT JOIN entidades ent ON ent.Codigo=dgc.Empresa'
      'LEFT JOIN drecfgcab dcc ON dcc.Codigo=dgc.DreCfgCab'
      'ORDER BY AnoMes DESC ')
    Left = 36
    Top = 253
    object QrDreGerCabAno: TIntegerField
      FieldName = 'Ano'
    end
    object QrDreGerCabAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrDreGerCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrDreGerCabNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrDreGerCabMES_ANO: TWideStringField
      FieldName = 'MES_ANO'
      Required = True
      Size = 7
    end
    object QrDreGerCabReceitaBruta: TFloatField
      FieldName = 'ReceitaBruta'
      Required = True
    end
    object QrDreGerCabDeducAbatim: TFloatField
      FieldName = 'DeducAbatim'
      Required = True
    end
    object QrDreGerCabReceitaLiquida: TFloatField
      FieldName = 'ReceitaLiquida'
      Required = True
    end
    object QrDreGerCabCPV_Fixo: TFloatField
      FieldName = 'CPV_Fixo'
      Required = True
    end
    object QrDreGerCabCPV_Variavel: TFloatField
      FieldName = 'CPV_Variavel'
      Required = True
    end
    object QrDreGerCabLucroBruto: TFloatField
      FieldName = 'LucroBruto'
      Required = True
    end
    object QrDreGerCabDespesVendas: TFloatField
      FieldName = 'DespesVendas'
      Required = True
    end
    object QrDreGerCabDespesAdmnst: TFloatField
      FieldName = 'DespesAdmnst'
      Required = True
    end
    object QrDreGerCabDespesFinance: TFloatField
      FieldName = 'DespesFinance'
      Required = True
    end
    object QrDreGerCabEBITDA: TFloatField
      FieldName = 'EBITDA'
      Required = True
    end
    object QrDreGerCabProvImpostos: TFloatField
      FieldName = 'ProvImpostos'
      Required = True
    end
    object QrDreGerCabResultLiquid: TFloatField
      FieldName = 'ResultLiquid'
      Required = True
    end
    object QrDreGerCabDreCfgCab: TIntegerField
      FieldName = 'DreCfgCab'
      Required = True
    end
  end
  object DsDreGerCab: TDataSource
    DataSet = QrDreGerCab
    Left = 36
    Top = 297
  end
  object PMObter: TPopupMenu
    Left = 123
    Top = 526
    object ObterFaturamento1: TMenuItem
      Caption = '&1. Obter Faturamentos'
      OnClick = ObterFaturamento1Click
    end
    object N2ObterDevolues1: TMenuItem
      Caption = '&2. Obter Devolu'#231#245'es de Vendas'
      OnClick = N2ObterDevolues1Click
    end
    object ObterLanamentos1: TMenuItem
      Caption = '3. Obter Lan'#231'amentos'
      OnClick = ObterLanamentos1Click
    end
  end
  object QrVen: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT voc.MovimCod, voc.ValorT,'
      'IF(voc.AreaM2 <> 0, -voc.AreaM2, IF(voc.PesoKg <> 0,'
      '-voc.PesoKg, -voc.Pecas)) Qtde,'
      'nfea.FatNum, nfea.ide_serie, nfea.ide_nNF,'
      'nfea.ide_dEmi, nfea.ide_hEmi,'
      'nfea.Id, nfea.ICMSTot_vICMS,'
      'nfea.ICMSTot_vIPI, nfea.ICMSTot_vPIS,'
      'nfea.ICMSTot_vCOFINS, nfea.ICMSTot_vNF,'
      'nfea.CodInfoDest,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Cliente'
      'FROM nfecaba nfea'
      'LEFT JOIN fisregcad frc ON frc.Codigo=nfea.FisRegCad'
      'LEFT JOIN entidades ent ON ent.Codigo=nfea.CodInfoDest'
      'LEFT JOIN vsoutcab voc ON voc.SerieV=nfea.ide_serie'
      '  AND voc.NFV=nfea.ide_nNF'
      'WHERE nfea.ide_dEmi BETWEEN  "2022-02-01" AND "2022-02-28"'
      'AND nfea.FatID IN (1, 50)'
      'AND nfea.Status=100'
      'AND frc.IntegraDRE=2')
    Left = 125
    Top = 254
    object QrVenMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVenValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVenQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrVenFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrVenFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrVenide_serie: TIntegerField
      FieldName = 'ide_serie'
      Required = True
    end
    object QrVenide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Required = True
    end
    object QrVenide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrVenide_hEmi: TTimeField
      FieldName = 'ide_hEmi'
      Required = True
    end
    object QrVenICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      Required = True
    end
    object QrVenCodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
      Required = True
    end
    object QrVenNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object QrVenId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrVenICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
      Required = True
    end
    object QrVenICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
      Required = True
    end
    object QrVenICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
      Required = True
    end
    object QrVenICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
      Required = True
    end
  end
  object QrDreGerVen: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT dgv.* '
      'FROM dregerven dgv'
      'WHERE AnoMes=202202'
      'AND Empresa=-11'
      'ORDER BY dgv.Linha ')
    Left = 123
    Top = 158
    object QrDreGerVenAno: TIntegerField
      FieldName = 'Ano'
      Required = True
    end
    object QrDreGerVenAnoMes: TIntegerField
      FieldName = 'AnoMes'
      Required = True
    end
    object QrDreGerVenEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrDreGerVenLinha: TIntegerField
      FieldName = 'Linha'
      Required = True
    end
    object QrDreGerVenFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrDreGerVenFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrDreGerVenId: TWideStringField
      FieldName = 'Id'
      Required = True
      Size = 44
    end
    object QrDreGerVenide_serie: TIntegerField
      FieldName = 'ide_serie'
      Required = True
    end
    object QrDreGerVenide_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Required = True
    end
    object QrDreGerVenide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDreGerVenide_hEmi: TTimeField
      FieldName = 'ide_hEmi'
      Required = True
      DisplayFormat = 'hh:nn'
    end
    object QrDreGerVenICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerVenICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerVenICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerVenICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerVenICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerVenFormaAdd: TSmallintField
      FieldName = 'FormaAdd'
      Required = True
    end
    object QrDreGerVenMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrDreGerVenQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrDreGerVenCustoT: TFloatField
      FieldName = 'CustoT'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsDreGerVen: TDataSource
    DataSet = QrDreGerVen
    Left = 123
    Top = 206
  end
  object QrPLOrfaos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT voc.MovimCod, voc.SerieV, voc.NFV, '
      'DATE(voc.DtVenda) DATA, TIME(voc.DtVenda) HORA, '
      'voc.Cliente, '
      'IF(voc.AreaM2 <> 0, -voc.AreaM2, IF(voc.PesoKg <> 0, '
      '-voc.PesoKg, -voc.Pecas)) Qtde, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Cliente '
      'FROM vsoutcab voc'
      'LEFT JOIN entidades ent ON ent.Codigo=voc.Cliente'
      'WHERE voc.NFV < 1 '
      'AND DATE(voc.DtVenda) BETWEEN "2000-01-01" AND "2999-01-01" ')
    Left = 207
    Top = 158
    object QrPLOrfaosMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrPLOrfaosSerieV: TIntegerField
      FieldName = 'SerieV'
      Required = True
    end
    object QrPLOrfaosNFV: TIntegerField
      FieldName = 'NFV'
      Required = True
    end
    object QrPLOrfaosDATA: TDateField
      FieldName = 'DATA'
    end
    object QrPLOrfaosHORA: TTimeField
      FieldName = 'HORA'
    end
    object QrPLOrfaosCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPLOrfaosQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrPLOrfaosNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
  end
  object QrNFeOrfaos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT voc.MovimCod, nfea.FatNum, nfea.ide_serie, nfea.ide_nNF, '
      'nfea.ide_dEmi, nfea.ide_hEmi, nfea.ICMSTot_vNF,'
      'nfea.CodInfoDest, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Cliente '
      'FROM nfecaba nfea'
      'LEFT JOIN fisregcad frc ON frc.Codigo=nfea.FisRegCad'
      'LEFT JOIN entidades ent ON ent.Codigo=nfea.CodInfoDest'
      'LEFT JOIN vsoutcab voc ON voc.SerieV=nfea.ide_serie'
      '  AND voc.NFV=nfea.ide_nNF'
      '  AND DATE(voc.DtVenda) BETWEEN "2000-01-01" AND "2999-01-01"'
      'WHERE nfea.ide_dEmi BETWEEN "2000-01-01" AND "2999-01-01"'
      'AND nfea.FatID IN (1,50)'
      'AND nfea.Status=100'
      'AND frc.IntegraDRE=2'
      'AND voc.MovimCod IS NULL')
    Left = 207
    Top = 258
    object QrNFeOrfaosMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrNFeOrfaosFatNum: TIntegerField
      FieldName = 'FatNum'
      Required = True
    end
    object QrNFeOrfaoside_serie: TIntegerField
      FieldName = 'ide_serie'
      Required = True
    end
    object QrNFeOrfaoside_nNF: TIntegerField
      FieldName = 'ide_nNF'
      Required = True
    end
    object QrNFeOrfaoside_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFeOrfaoside_hEmi: TTimeField
      FieldName = 'ide_hEmi'
      Required = True
    end
    object QrNFeOrfaosICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      Required = True
    end
    object QrNFeOrfaosCodInfoDest: TIntegerField
      FieldName = 'CodInfoDest'
      Required = True
    end
    object QrNFeOrfaosNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
  end
  object frxDRE_GEREN_001_Orfaos: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42668.603069016200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxDRE_GEREN_001_OrfaosGetValue
    Left = 208
    Top = 360
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPLOrfaos
        DataSetName = 'frxDsPLOrfaos'
      end
      item
        DataSet = frxDsNFeOrfaos
        DataSetName = 'frxDsNFeOrfaos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 68.031525350000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        Stretched = True
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 56.692950000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 34.015770000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 34.015770000000010000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 34.015770000000000000
          Width = 468.661720000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Packing List e/ou NF-e Orf'#227'os')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 34.015770000000000000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 34.015770000000010000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677165350000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPLOrfaos
        DataSetName = 'frxDsPLOrfaos'
        RowCount = 0
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 68.031510710000000000
          Height = 22.677165350000000000
          DataField = 'MovimCod'
          DataSet = frxDsPLOrfaos
          DataSetName = 'frxDsPLOrfaos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPLOrfaos."MovimCod"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Width = 34.015740710000000000
          Height = 22.677165350000000000
          DataField = 'SerieV'
          DataSet = frxDsPLOrfaos
          DataSetName = 'frxDsPLOrfaos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPLOrfaos."SerieV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Width = 68.031510710000000000
          Height = 22.677165350000000000
          DataField = 'NFV'
          DataSet = frxDsPLOrfaos
          DataSetName = 'frxDsPLOrfaos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPLOrfaos."NFV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 75.590570710000000000
          Height = 22.677165350000000000
          DataField = 'DATA'
          DataSet = frxDsPLOrfaos
          DataSetName = 'frxDsPLOrfaos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPLOrfaos."DATA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 68.031510710000000000
          Height = 22.677165350000000000
          DataField = 'HORA'
          DataSet = frxDsPLOrfaos
          DataSetName = 'frxDsPLOrfaos'
          DisplayFormat.FormatStr = 'hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPLOrfaos."HORA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          DataField = 'Qtde'
          DataSet = frxDsPLOrfaos
          DataSetName = 'frxDsPLOrfaos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPLOrfaos."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Width = 68.031510710000000000
          Height = 22.677165350000000000
          DataField = 'Cliente'
          DataSet = frxDsPLOrfaos
          DataSetName = 'frxDsPLOrfaos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPLOrfaos."Cliente"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 207.874015750000000000
          Height = 22.677165350000000000
          DataField = 'NO_Cliente'
          DataSet = frxDsPLOrfaos
          DataSetName = 'frxDsPLOrfaos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPLOrfaos."NO_Cliente"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 147.401670000000000000
        Width = 680.315400000000000000
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Packing Lists Orf'#227'os')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-C')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Top = 22.677180000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Top = 22.677180000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 22.677180000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 22.677180000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Top = 22.677180000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Top = 22.677180000000000000
          Width = 275.905690000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 257.008040000000000000
        Width = 680.315400000000000000
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 283.464750000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'NF-es Orf'#227's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Faturam.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Top = 22.677180000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Top = 22.677180000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 22.677180000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 22.677180000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Top = 22.677180000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Top = 22.677180000000000000
          Width = 275.905690000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677165350000000000
        Top = 347.716760000000000000
        Width = 680.315400000000000000
        DataSet = frxDsNFeOrfaos
        DataSetName = 'frxDsNFeOrfaos'
        RowCount = 0
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Width = 68.031510710000000000
          Height = 22.677165350000000000
          DataField = 'FatNum'
          DataSet = frxDsNFeOrfaos
          DataSetName = 'frxDsNFeOrfaos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeOrfaos."FatNum"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Width = 34.015740710000000000
          Height = 22.677165350000000000
          DataField = 'ide_serie'
          DataSet = frxDsNFeOrfaos
          DataSetName = 'frxDsNFeOrfaos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeOrfaos."ide_serie"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Width = 68.031510710000000000
          Height = 22.677165350000000000
          DataField = 'ide_nNF'
          DataSet = frxDsNFeOrfaos
          DataSetName = 'frxDsNFeOrfaos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeOrfaos."ide_nNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 75.590570710000000000
          Height = 22.677165350000000000
          DataField = 'ide_dEmi'
          DataSet = frxDsNFeOrfaos
          DataSetName = 'frxDsNFeOrfaos'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeOrfaos."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 68.031510710000000000
          Height = 22.677165350000000000
          DataField = 'ide_hEmi'
          DataSet = frxDsNFeOrfaos
          DataSetName = 'frxDsNFeOrfaos'
          DisplayFormat.FormatStr = 'hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeOrfaos."ide_hEmi"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          DataField = 'ICMSTot_vNF'
          DataSet = frxDsNFeOrfaos
          DataSetName = 'frxDsNFeOrfaos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeOrfaos."ICMSTot_vNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Width = 68.031510710000000000
          Height = 22.677165350000000000
          DataField = 'CodInfoDest'
          DataSet = frxDsNFeOrfaos
          DataSetName = 'frxDsNFeOrfaos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeOrfaos."CodInfoDest"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 207.874015750000000000
          Height = 22.677165350000000000
          DataField = 'NO_Cliente'
          DataSet = frxDsNFeOrfaos
          DataSetName = 'frxDsNFeOrfaos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeOrfaos."NO_Cliente"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 393.071120000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsPLOrfaos: TfrxDBDataset
    UserName = 'frxDsPLOrfaos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'MovimCod=MovimCod'
      'SerieV=SerieV'
      'NFV=NFV'
      'DATA=DATA'
      'HORA=HORA'
      'Cliente=Cliente'
      'Qtde=Qtde'
      'NO_Cliente=NO_Cliente')
    DataSet = QrPLOrfaos
    BCDToCurrency = False
    DataSetOptions = []
    Left = 208
    Top = 208
  end
  object frxDsNFeOrfaos: TfrxDBDataset
    UserName = 'frxDsNFeOrfaos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'MovimCod=MovimCod'
      'FatNum=FatNum'
      'ide_serie=ide_serie'
      'ide_nNF=ide_nNF'
      'ide_dEmi=ide_dEmi'
      'ide_hEmi=ide_hEmi'
      'ICMSTot_vNF=ICMSTot_vNF'
      'CodInfoDest=CodInfoDest'
      'NO_Cliente=NO_Cliente')
    DataSet = QrNFeOrfaos
    BCDToCurrency = False
    DataSetOptions = []
    Left = 208
    Top = 308
  end
  object QrSemFRC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfea.ide_nNF'
      'FROM nfecaba nfea'
      'WHERE nfea.ide_dEmi BETWEEN  "2022-02-01" AND "2022-02-28"'
      'AND nfea.FatID IN (1, 50) '
      'AND nfea.Status=100'
      'AND nfea.FisRegCad=0')
    Left = 395
    Top = 450
    object QrSemFRCide_nNF: TIntegerField
      FieldName = 'NF'
      Required = True
    end
  end
  object QrDreCfgEst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM drecfgest'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 292
    Top = 257
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDreCfgEstCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDreCfgEstControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDreCfgEstNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrDreCfgEstDreCfgTp1: TIntegerField
      FieldName = 'DreCfgTp1'
      Required = True
    end
    object QrDreCfgEstOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
  end
  object QrDreCfgCtd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM drecfgctd'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 292
    Top = 305
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDreCfgCtdCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDreCfgCtdControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDreCfgCtdConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrDreCfgCtdNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrDreCfgCtdDreCfgTp2: TIntegerField
      FieldName = 'DreCfgTp2'
      Required = True
    end
    object QrDreCfgCtdOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
  end
  object QrDreCfgCpc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT CAST(IF(cpc.Conta=5, cpc.IdxSub4, NULL) AS SIGNED) IDXSUB' +
        '4,'
      'CAST(IF(cpc.Conta=5, cpc.Excluso, NULL) AS SIGNED) EXCLUSO,'
      'IF(cpc.MesCompet IS NULL, 0, cpc.MesCompet) MesCompet,'
      'IF(pac.Codigo IN (761), 0,'
      '  IF(pac.Codigo IN (-999999999), 1, '
      '  IF(pac.CodNiv5 IN (-999999999), 3,'
      '  IF(pac.CodNiv5 IN (761), 2,'
      '  IF(pac.CodNiv4 IN (-999999999), 3,'
      '  IF(pac.CodNiv4 IN (761), 2,'
      '  IF(pac.CodNiv3 IN (-999999999), 3,'
      '  IF(pac.CodNiv3 IN (761), 2,'
      '  IF(pac.CodNiv2 IN (-999999999), 3,'
      '  IF(pac.CodNiv2 IN (761), 2,'
      '  IF(pac.CodNiv1 IN (-999999999), 3,'
      '  IF(pac.CodNiv1 IN (761), 2,'
      '  -1))))))))))))'
      'Status,'
      'pac.Codigo PlaAllcad, pac.Nome, pac.Ordens '
      'FROM plaallcad pac '
      'LEFT JOIN drecfgcpc cpc ON pac.Codigo=cpc.PlaAllCad '
      'WHERE '
      '( '
      '  pac.Codigo IN (761)'
      '  OR pac.CodNiv1 IN (761)'
      '  OR pac.CodNiv2 IN (761)'
      '  OR pac.CodNiv3 IN (761)'
      '  OR pac.CodNiv4 IN (761)'
      '  OR pac.CodNiv5 IN (761)'
      ') '
      'AND ( '
      '  (cpc.Conta=5)  '
      '  OR (cpc.Conta IS NULL)  '
      '  OR (cpc.Conta<>5 AND cpc.Codigo <> 4'
      '    AND NOT (pac.Codigo IN (761)))  '
      ') '
      'ORDER BY Ordens')
    Left = 292
    Top = 353
    object QrDreCfgCpcIDXSUB4: TLargeintField
      FieldName = 'IDXSUB4'
    end
    object QrDreCfgCpcEXCLUSO: TIntegerField
      FieldName = 'EXCLUSO'
    end
    object QrDreCfgCpcStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object QrDreCfgCpcPlaAllcad: TIntegerField
      FieldName = 'PlaAllcad'
      Required = True
    end
    object QrDreCfgCpcNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrDreCfgCpcOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
    object QrDreCfgCpcMesCompet: TIntegerField
      FieldName = 'MesCompet'
    end
  end
  object QrDCC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cpc.PlaAllCad, cpc.Excluso'
      'FROM drecfgcpc cpc'
      'WHERE cpc.Controle=:P0')
    Left = 290
    Top = 401
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDCCPlaAllCad: TIntegerField
      FieldName = 'PlaAllCad'
      Required = True
    end
    object QrDCCExcluso: TIntegerField
      FieldName = 'Excluso'
      Required = True
    end
  end
  object QrLanctos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lct.CliInt, lct.Data, lct.Tipo, lct.Carteira, '
      'lct.Controle, lct.Sub, lct.Genero, '
      'lct.Debito, lct.Credito, lct.Compensado, '
      'lct.Sit, lct.Vencimento, lct.GenCtb, '
      'lct.GenCtbD, lct.GenCtbC, lct.Cliente, lct.Fornecedor,  '
      'lct.SerieNF, lct.NotaFiscal,'
      'pac.CodNiv1, pac.CodNiv2, pac.CodNiv3, '
      'pac.CodNiv4, pac.CodNiv5, pac.CodNiv6'
      'FROM lct0001a lct '
      'LEFT JOIN plaallcad pac ON pac.Codigo=lct.Genero'
      'WHERE lct.CliInt=-11 '
      'AND lct.Genero <> 0  '
      'AND lct.Data BETWEEN "1899-12-31" AND  "2999-12-31"'
      'AND lct.ID_Pgto=0 ')
    Left = 291
    Top = 450
    object QrLanctosCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrLanctosData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrLanctosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrLanctosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrLanctosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLanctosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrLanctosGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrLanctosDebito: TFloatField
      FieldName = 'Debito'
      Required = True
    end
    object QrLanctosCredito: TFloatField
      FieldName = 'Credito'
      Required = True
    end
    object QrLanctosCompensado: TDateField
      FieldName = 'Compensado'
      Required = True
    end
    object QrLanctosSit: TIntegerField
      FieldName = 'Sit'
      Required = True
    end
    object QrLanctosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrLanctosGenCtb: TIntegerField
      FieldName = 'GenCtb'
      Required = True
    end
    object QrLanctosGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
      Required = True
    end
    object QrLanctosGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
      Required = True
    end
    object QrLanctosCodNiv1: TIntegerField
      FieldName = 'CodNiv1'
    end
    object QrLanctosCodNiv2: TIntegerField
      FieldName = 'CodNiv2'
    end
    object QrLanctosCodNiv3: TIntegerField
      FieldName = 'CodNiv3'
    end
    object QrLanctosCodNiv4: TIntegerField
      FieldName = 'CodNiv4'
    end
    object QrLanctosCodNiv5: TIntegerField
      FieldName = 'CodNiv5'
    end
    object QrLanctosCodNiv6: TIntegerField
      FieldName = 'CodNiv6'
    end
    object QrLanctosCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrLanctosFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Required = True
    end
    object QrLanctosSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrLanctosNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Required = True
    end
    object QrLanctosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object QrDreGerFinAut: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT dgf.*,'
      'tp1.Nome NO_DreCfgTp1, tp2.Nome NO_DreCfgTp2, '
      'pa1.Nome NO_PAC_Genero, pa2.Nome NO_PAC_Cpc,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO '
      'FROM dregerfin dgf'
      'LEFT JOIN drecfgtp1 tp1 ON tp1.Codigo=dgf.DreCfgTp1'
      'LEFT JOIN drecfgtp2 tp2 ON tp2.Controle=dgf.DreCfgTp2'
      'LEFT JOIN drecfgcpc dcc ON dcc.IdxSub4=dgf.DreCfgCpc'
      'LEFT JOIN plaallcad pa1 ON pa1.Codigo=dgf.Genero'
      'LEFT JOIN plaallcad pa2 ON pa2.Codigo=dcc.PlaAllCad'
      'LEFT JOIN entidades ent ON ent.Codigo=IF(dgf.Fornecedor>0, '
      '  dgf.Fornecedor, dgf.Cliente)')
    Left = 291
    Top = 162
    object QrDreGerFinAutAno: TIntegerField
      FieldName = 'Ano'
      Required = True
    end
    object QrDreGerFinAutAnoMes: TIntegerField
      FieldName = 'AnoMes'
      Required = True
    end
    object QrDreGerFinAutEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrDreGerFinAutLinha: TIntegerField
      FieldName = 'Linha'
      Required = True
      EditFormat = '000;-000; '
    end
    object QrDreGerFinAutDreCfgEst: TIntegerField
      FieldName = 'DreCfgEst'
      Required = True
    end
    object QrDreGerFinAutDreCfgCtd: TIntegerField
      FieldName = 'DreCfgCtd'
      Required = True
    end
    object QrDreGerFinAutDreCfgCpc: TIntegerField
      FieldName = 'DreCfgCpc'
      Required = True
    end
    object QrDreGerFinAutDreCfgTp1: TIntegerField
      FieldName = 'DreCfgTp1'
      Required = True
    end
    object QrDreGerFinAutDreCfgTp2: TIntegerField
      FieldName = 'DreCfgTp2'
      Required = True
    end
    object QrDreGerFinAutData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDreGerFinAutTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrDreGerFinAutCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDreGerFinAutControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDreGerFinAutSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrDreGerFinAutGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrDreGerFinAutDebito: TFloatField
      FieldName = 'Debito'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerFinAutCredito: TFloatField
      FieldName = 'Credito'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerFinAutCompensado: TDateField
      FieldName = 'Compensado'
      Required = True
    end
    object QrDreGerFinAutSit: TIntegerField
      FieldName = 'Sit'
      Required = True
    end
    object QrDreGerFinAutVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrDreGerFinAutGenCtb: TIntegerField
      FieldName = 'GenCtb'
      Required = True
    end
    object QrDreGerFinAutGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
      Required = True
    end
    object QrDreGerFinAutGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
      Required = True
    end
    object QrDreGerFinAutLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrDreGerFinAutDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDreGerFinAutDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDreGerFinAutUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrDreGerFinAutUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrDreGerFinAutAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrDreGerFinAutAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrDreGerFinAutAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrDreGerFinAutAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrDreGerFinAutGenNiv1: TIntegerField
      FieldName = 'GenNiv1'
      Required = True
    end
    object QrDreGerFinAutGenNiv2: TIntegerField
      FieldName = 'GenNiv2'
      Required = True
    end
    object QrDreGerFinAutGenNiv3: TIntegerField
      FieldName = 'GenNiv3'
      Required = True
    end
    object QrDreGerFinAutGenNiv4: TIntegerField
      FieldName = 'GenNiv4'
      Required = True
    end
    object QrDreGerFinAutGenNiv5: TIntegerField
      FieldName = 'GenNiv5'
      Required = True
    end
    object QrDreGerFinAutGenNiv6: TIntegerField
      FieldName = 'GenNiv6'
      Required = True
    end
    object QrDreGerFinAutNO_DreCfgTp1: TWideStringField
      FieldName = 'NO_DreCfgTp1'
      Size = 100
    end
    object QrDreGerFinAutNO_DreCfgTp2: TWideStringField
      FieldName = 'NO_DreCfgTp2'
      Size = 255
    end
    object QrDreGerFinAutCpcStatus: TSmallintField
      FieldName = 'CpcStatus'
      Required = True
    end
    object QrDreGerFinAutCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrDreGerFinAutFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Required = True
    end
    object QrDreGerFinAutNO_PAC_Genero: TWideStringField
      FieldName = 'NO_PAC_Genero'
      Size = 60
    end
    object QrDreGerFinAutNO_PAC_Cpc: TWideStringField
      FieldName = 'NO_PAC_Cpc'
      Size = 60
    end
    object QrDreGerFinAutNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrDreGerFinAutCtrlAvul: TIntegerField
      FieldName = 'CtrlAvul'
    end
  end
  object DsDreGerFinAut: TDataSource
    DataSet = QrDreGerFinAut
    Left = 291
    Top = 210
  end
  object QrDev: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Cliente,'
      'einc.MovFatID, einc.MovFatNum, einc.MovimCod,'
      'einc.ValorT,'
      'IF(einc.AreaM2 <> 0, -einc.AreaM2, IF(einc.PesoKg <> 0,'
      '      -einc.PesoKg, -einc.Pecas)) Qtde,'
      'einc.SER, einc.NUM_DOC,'
      'einc.DT_DOC, einc.DT_E_S, einc.CHV_NFE, einc.VL_ICMS,'
      'einc.VL_IPI, einc.VL_PIS, einc.VL_COFINS, einc.VL_DOC'
      'FROM efdinnnfscab einc'
      'LEFT JOIN fisregcad frc ON frc.Codigo=einc.RegrFiscal'
      'LEFT JOIN entidades ent ON ent.Codigo=einc.Terceiro'
      'WHERE einc.DT_E_S BETWEEN "2000-01-01" AND "2999-12-31"'
      'AND einc.NFeStatus=100'
      'AND frc.IntegraDRE<>-1'
      '')
    Left = 123
    Top = 406
    object QrDevNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object QrDevMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrDevMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrDevMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrDevValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrDevQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrDevSER: TIntegerField
      FieldName = 'SER'
      Required = True
    end
    object QrDevNUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Required = True
    end
    object QrDevDT_DOC: TDateField
      FieldName = 'DT_DOC'
      Required = True
    end
    object QrDevDT_E_S: TDateField
      FieldName = 'DT_E_S'
      Required = True
    end
    object QrDevCHV_NFE: TWideStringField
      FieldName = 'CHV_NFE'
      Size = 44
    end
    object QrDevVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
    end
    object QrDevVL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Required = True
    end
    object QrDevVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
    end
    object QrDevVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
    end
    object QrDevVL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Required = True
    end
  end
  object QrDreGerDev: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT dgd.* '
      'FROM dregerdev dgd'
      'WHERE AnoMes=202202'
      'AND Empresa=-11'
      'ORDER BY dgd.Linha ')
    Left = 123
    Top = 306
    object QrDreGerDevAno: TIntegerField
      FieldName = 'Ano'
      Required = True
    end
    object QrDreGerDevAnoMes: TIntegerField
      FieldName = 'AnoMes'
      Required = True
    end
    object QrDreGerDevEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrDreGerDevLinha: TIntegerField
      FieldName = 'Linha'
      Required = True
    end
    object QrDreGerDevMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrDreGerDevMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrDreGerDevCHV_NFE: TWideStringField
      FieldName = 'CHV_NFE'
      Required = True
      Size = 44
    end
    object QrDreGerDevSER: TIntegerField
      FieldName = 'SER'
      Required = True
    end
    object QrDreGerDevNUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Required = True
    end
    object QrDreGerDevDT_DOC: TDateField
      FieldName = 'DT_DOC'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDreGerDevDT_E_S: TDateField
      FieldName = 'DT_E_S'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDreGerDevVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerDevVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerDevVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerDevVL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerDevVL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerDevFormaAdd: TSmallintField
      FieldName = 'FormaAdd'
      Required = True
    end
    object QrDreGerDevMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrDreGerDevQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrDreGerDevCustoT: TFloatField
      FieldName = 'CustoT'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsDreGerDev: TDataSource
    DataSet = QrDreGerDev
    Left = 123
    Top = 354
  end
  object PMAvulso: TPopupMenu
    OnPopup = PMAvulsoPopup
    Left = 247
    Top = 534
    object IncluiLanamento1: TMenuItem
      Caption = '&Lan'#231'amento'
      object LctInclui1: TMenuItem
        Caption = '&Inclui'
        OnClick = LctInclui1Click
      end
      object LctAltera1: TMenuItem
        Caption = '&Altera'
        OnClick = LctAltera1Click
      end
      object LctExclui1: TMenuItem
        Caption = '&Exclui'
        OnClick = LctExclui1Click
      end
    end
  end
  object QrSeq: TMySQLQuery
    Left = 387
    Top = 259
  end
  object QrDreGerFinAvu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT dgf.*,'
      'tp1.Nome NO_DreCfgTp1, tp2.Nome NO_DreCfgTp2, '
      'pa1.Nome NO_PAC_Genero, pa2.Nome NO_PAC_Cpc,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO '
      'FROM dregerfin dgf'
      'LEFT JOIN drecfgtp1 tp1 ON tp1.Codigo=dgf.DreCfgTp1'
      'LEFT JOIN drecfgtp2 tp2 ON tp2.Controle=dgf.DreCfgTp2'
      'LEFT JOIN drecfgcpc dcc ON dcc.IdxSub4=dgf.DreCfgCpc'
      'LEFT JOIN plaallcad pa1 ON pa1.Codigo=dgf.Genero'
      'LEFT JOIN plaallcad pa2 ON pa2.Codigo=dcc.PlaAllCad'
      'LEFT JOIN entidades ent ON ent.Codigo=IF(dgf.Fornecedor>0, '
      '  dgf.Fornecedor, dgf.Cliente)')
    Left = 387
    Top = 162
    object QrDreGerFinAvuAno: TIntegerField
      FieldName = 'Ano'
      Required = True
    end
    object QrDreGerFinAvuAnoMes: TIntegerField
      FieldName = 'AnoMes'
      Required = True
    end
    object QrDreGerFinAvuEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrDreGerFinAvuLinha: TIntegerField
      FieldName = 'Linha'
      Required = True
      EditFormat = '000;-000; '
    end
    object QrDreGerFinAvuDreCfgEst: TIntegerField
      FieldName = 'DreCfgEst'
      Required = True
    end
    object QrDreGerFinAvuDreCfgCtd: TIntegerField
      FieldName = 'DreCfgCtd'
      Required = True
    end
    object QrDreGerFinAvuDreCfgCpc: TIntegerField
      FieldName = 'DreCfgCpc'
      Required = True
    end
    object QrDreGerFinAvuDreCfgTp1: TIntegerField
      FieldName = 'DreCfgTp1'
      Required = True
    end
    object QrDreGerFinAvuDreCfgTp2: TIntegerField
      FieldName = 'DreCfgTp2'
      Required = True
    end
    object QrDreGerFinAvuData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDreGerFinAvuTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrDreGerFinAvuCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDreGerFinAvuControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDreGerFinAvuSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrDreGerFinAvuGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrDreGerFinAvuDebito: TFloatField
      FieldName = 'Debito'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerFinAvuCredito: TFloatField
      FieldName = 'Credito'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDreGerFinAvuCompensado: TDateField
      FieldName = 'Compensado'
      Required = True
    end
    object QrDreGerFinAvuSit: TIntegerField
      FieldName = 'Sit'
      Required = True
    end
    object QrDreGerFinAvuVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrDreGerFinAvuGenCtb: TIntegerField
      FieldName = 'GenCtb'
      Required = True
    end
    object QrDreGerFinAvuGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
      Required = True
    end
    object QrDreGerFinAvuGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
      Required = True
    end
    object QrDreGerFinAvuLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrDreGerFinAvuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDreGerFinAvuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDreGerFinAvuUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrDreGerFinAvuUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrDreGerFinAvuAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrDreGerFinAvuAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrDreGerFinAvuAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrDreGerFinAvuAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrDreGerFinAvuGenNiv1: TIntegerField
      FieldName = 'GenNiv1'
      Required = True
    end
    object QrDreGerFinAvuGenNiv2: TIntegerField
      FieldName = 'GenNiv2'
      Required = True
    end
    object QrDreGerFinAvuGenNiv3: TIntegerField
      FieldName = 'GenNiv3'
      Required = True
    end
    object QrDreGerFinAvuGenNiv4: TIntegerField
      FieldName = 'GenNiv4'
      Required = True
    end
    object QrDreGerFinAvuGenNiv5: TIntegerField
      FieldName = 'GenNiv5'
      Required = True
    end
    object QrDreGerFinAvuGenNiv6: TIntegerField
      FieldName = 'GenNiv6'
      Required = True
    end
    object QrDreGerFinAvuNO_DreCfgTp1: TWideStringField
      FieldName = 'NO_DreCfgTp1'
      Size = 100
    end
    object QrDreGerFinAvuNO_DreCfgTp2: TWideStringField
      FieldName = 'NO_DreCfgTp2'
      Size = 255
    end
    object QrDreGerFinAvuCpcStatus: TSmallintField
      FieldName = 'CpcStatus'
      Required = True
    end
    object QrDreGerFinAvuCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrDreGerFinAvuFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Required = True
    end
    object QrDreGerFinAvuNO_PAC_Genero: TWideStringField
      FieldName = 'NO_PAC_Genero'
      Size = 60
    end
    object QrDreGerFinAvuNO_PAC_Cpc: TWideStringField
      FieldName = 'NO_PAC_Cpc'
      Size = 60
    end
    object QrDreGerFinAvuNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrDreGerFinAvuCtrlAvul: TIntegerField
      FieldName = 'CtrlAvul'
    end
  end
  object DsDreGerFinAvu: TDataSource
    DataSet = QrDreGerFinAvu
    Left = 387
    Top = 210
  end
  object MySQLQuery3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT CAST(IF(cpc.Conta=5, cpc.IdxSub4, NULL) AS SIGNED) IDXSUB' +
        '4,'
      'CAST(IF(cpc.Conta=5, cpc.Excluso, NULL) AS SIGNED) EXCLUSO,'
      'IF(cpc.MesCompet IS NULL, 0, cpc.MesCompet) MesCompet,'
      'IF(cpc.MesCompet = 1, "S", "E") NO_MesCompet,'
      'IF(pac.Codigo IN (761), 1,'
      '  IF(pac.Codigo IN (-999999999), 2,'
      '  IF(pac.CodNiv5 IN (-999999999), 4,'
      '  IF(pac.CodNiv5 IN (761), 3,'
      '  IF(pac.CodNiv4 IN (-999999999), 4,'
      '  IF(pac.CodNiv4 IN (761), 3,'
      '  IF(pac.CodNiv3 IN (-999999999), 4,'
      '  IF(pac.CodNiv3 IN (761), 3,'
      '  IF(pac.CodNiv2 IN (-999999999), 4,'
      '  IF(pac.CodNiv2 IN (761), 3,'
      '  IF(pac.CodNiv1 IN (-999999999), 4,'
      '  IF(pac.CodNiv1 IN (761), 3,'
      '  9))))))))))))'
      'Status,'
      'pac.Codigo PlaAllcad, pac.Nome, pac.Ordens'
      'FROM plaallcad pac'
      'LEFT JOIN drecfgcpc cpc ON pac.Codigo=cpc.PlaAllCad'
      'WHERE'
      '('
      '  pac.Codigo IN (761)'
      '  OR pac.CodNiv1 IN (761)'
      '  OR pac.CodNiv2 IN (761)'
      '  OR pac.CodNiv3 IN (761)'
      '  OR pac.CodNiv4 IN (761)'
      '  OR pac.CodNiv5 IN (761)'
      ')'
      'AND ('
      '  (cpc.Conta=5)'
      '  OR (cpc.Conta IS NULL)'
      '  OR (cpc.Conta<>5 AND cpc.Codigo <> 4'
      '    AND NOT (pac.Codigo IN (761)))'
      ')'
      'ORDER BY Ordens')
    Left = 716
    Top = 209
    object LargeintField1: TLargeintField
      FieldName = 'IDXSUB4'
    end
    object IntegerField10: TIntegerField
      FieldName = 'EXCLUSO'
    end
    object IntegerField11: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object IntegerField12: TIntegerField
      FieldName = 'PlaAllcad'
      Required = True
    end
    object WideStringField3: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object WideStringField4: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
    object IntegerField13: TIntegerField
      FieldName = 'MesCompet'
    end
    object QrDreCfgCpcNO_MesCompet: TWideStringField
      FieldName = 'NO_MesCompet'
      Required = True
      Size = 1
    end
  end
end
