unit DreCfgCpc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Menus;

type
  TFmDreCfgCpc = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdIdxSub4: TdmkEdit;
    Label6: TLabel;
    CBPlaAllCad: TdmkDBLookupComboBox;
    EdPlaAllCad: TdmkEditCB;
    Label1: TLabel;
    SbContas: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrPlaAllCad: TMySQLQuery;
    DsPlaAllCad: TDataSource;
    Label2: TLabel;
    Label4: TLabel;
    QrPlaAllCadCodigo: TIntegerField;
    QrPlaAllCadOrdens: TWideStringField;
    QrPlaAllCadNome: TWideStringField;
    DBOrdens: TDBEdit;
    EdCodigo: TdmkEdit;
    EdNO_Empresa: TdmkEdit;
    EdNomCtrl: TdmkEdit;
    EdControle: TdmkEdit;
    QrPlaAllCadNivel: TSmallintField;
    Label7: TLabel;
    DBEdNivel: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    EdConta: TdmkEdit;
    EdNomCta: TdmkEdit;
    Label10: TLabel;
    RGExcluso: TRadioGroup;
    SpeedButton1: TSpeedButton;
    PMContas: TPopupMenu;
    odosNveis1: TMenuItem;
    Lista1: TMenuItem;
    RGMesCompet: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbContasClick(Sender: TObject);
    procedure EdPlaAllCadRedefinido(Sender: TObject);
    procedure odosNveis1Click(Sender: TObject);
    procedure Lista1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenIts(IdxSub4: Integer);
    procedure ReopenPlaAllCad();
  public
    { Public declarations }
  end;

  var
  FmDreCfgCpc: TFmDreCfgCpc;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, DmkDAC_PF,
  UnFinanceiroJan, ModuleGeral, DreCfgCab;

{$R *.DFM}

procedure TFmDreCfgCpc.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta, IdxSub4, PlaAllCad, Excluso, MesCompet: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  Conta          := EdConta.ValueVariant;
  IdxSub4        := EdIdxSub4.ValueVariant;
  PlaAllCad      := EdPlaAllCad.ValueVariant;
  Excluso        := RGExcluso.ItemIndex;
  MesCompet      := RGMesCompet.ItemIndex;
  //
  if MyObjects.FIC(PlaAllCad = 0, EdPlaAllCad, 'Informe a conta!') then
    Exit;
  //
  IdxSub4 := UMyMod.BPGS1I32('drecfgcpc', 'IdxSub4', '', '', tsPos, SQLType, IdxSub4);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'drecfgcpc', False, [
  'Codigo', 'Controle', 'Conta',
  'PlaAllCad', 'Excluso', 'MesCompet'], [
  'IdxSub4'], [
  Codigo, Controle, Conta,
  PlaAllCad, Excluso, MesCompet], [
  IdxSub4], True) then
  begin
    ReopenIts(IdxSub4);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType              := stIns;
      EdIdxSub4.ValueVariant       := 0;
      EdPlaAllCad.ValueVariant     := 0;
      CBPlaAllCad.KeyValue         := Null;
      EdPlaAllCad.SetFocus;
    end else Close;
  end;
end;

procedure TFmDreCfgCpc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDreCfgCpc.EdPlaAllCadRedefinido(Sender: TObject);
begin
  if EdPlaAllCad.ValueVariant = 0 then
  begin
    DBOrdens.DataField := '';
    DBEdNivel.DataField := '';
  end else
  begin
    (*if DModG.QrCtrlGeralConcatNivOrdCta.Value = 0 then
      DBOrdens.DataField := 'Niveis'
    else*)
      DBOrdens.DataField := 'Ordens';
      DBEdNivel.DataField := 'Nivel';
  end;
end;

procedure TFmDreCfgCpc.FormActivate(Sender: TObject);
begin
  //
  MyObjects.CorIniComponente();
end;

procedure TFmDreCfgCpc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DBOrdens.DataField := '';
  DBEdNivel.DataField := '';
  ReopenPlaAllCad();
end;

procedure TFmDreCfgCpc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDreCfgCpc.Lista1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FinanceiroJan.MostraFormPlaAllCad(EdPlaAllCad.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdPlaAllCad, CBPlaAllCad, QrPlaAllCad, VAR_CADASTRO);
end;

procedure TFmDreCfgCpc.odosNveis1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FinanceiroJan.CadastroDeNiveisPlano(QrPlaAllCadNivel.Value, QrPlaAllCadCodigo.Value);
  UMyMod.SetaCodigoPesquisado(EdPlaAllCad, CBPlaAllCad, QrPlaAllCad, VAR_CADASTRO);
end;

procedure TFmDreCfgCpc.ReopenIts(IdxSub4: Integer);
begin
  FmDreCfgCab.ReopenDreCfgCpc(IdxSub4);
end;

procedure TFmDreCfgCpc.ReopenPlaAllCad();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPlaAllCad, DMod.MyDB, [
  'SELECT Codigo, Nivel, Ordens, Nome  ',
  'FROM plaallcad ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmDreCfgCpc.SbContasClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMContas, SbContas);
end;

end.
