unit CtbBalPLctEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, Variants, dmkDBGridZTO;

type
  TFmCtbBalPLctEdit = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAltera: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DBGOrfaos: TdmkDBGridZTO;
    QrOrfaos: TMySQLQuery;
    DsOrfaos: TDataSource;
    QrOrfaosPlano: TIntegerField;
    QrOrfaosConjunto: TIntegerField;
    QrOrfaosGrupo: TIntegerField;
    QrOrfaosSubgrupo: TIntegerField;
    QrOrfaosGenero: TIntegerField;
    QrOrfaosNO_Cta: TWideStringField;
    QrOrfaosData: TDateField;
    QrOrfaosTipo: TSmallintField;
    QrOrfaosCarteira: TIntegerField;
    QrOrfaosControle: TIntegerField;
    QrOrfaosSub: TSmallintField;
    QrOrfaosDebito: TFloatField;
    QrOrfaosCredito: TFloatField;
    QrOrfaosCompensado: TDateField;
    QrOrfaosSit: TIntegerField;
    QrOrfaosVencimento: TDateField;
    QrOrfaosID_Pgto: TIntegerField;
    QrOrfaosID_Quit: TIntegerField;
    QrOrfaosID_Sub: TSmallintField;
    QrOrfaosPago: TFloatField;
    QrOrfaosGenCtb: TIntegerField;
    QrOrfaosQtDtPg: TIntegerField;
    QrOrfaosPagMul: TFloatField;
    QrOrfaosPagJur: TFloatField;
    QrOrfaosRecDes: TFloatField;
    QrOrfaosNO_Pla: TWideStringField;
    QrOrfaosNO_Cjt: TWideStringField;
    QrOrfaosNO_Gru: TWideStringField;
    QrOrfaosNO_Sgr: TWideStringField;
    QrOrfaosNO_ENT: TWideStringField;
    QrOrfaosNO_CARTEIRA: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrOrfaosAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }

  public
    { Public declarations }
    FSQL, FTabLctA: String;
    //
    procedure ReopenOrfaos();
  end;

  var
  FmCtbBalPLctEdit: TFmCtbBalPLctEdit;

implementation

uses UnMyObjects, DmkDAC_PF, Module, MyDBCheck;

{$R *.DFM}

procedure TFmCtbBalPLctEdit.BtAlteraClick(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de G�nero Cont�bil';
  Prompt1 = 'Informe o G�nero Financeiro:';
  Prompt2 = 'Informe o G�nero Cont�bil:';
  Campo  = 'Descricao';
var
  vGenero, vGenCtb: Variant;
  Genero, GenCtb: Integer;
  Corda: String;
begin
  if DBGOrfaos.SelectedRows.Count > 0 then
  begin
    Corda := MyObjects.CordaDeZTOChecks(DBGOrfaos, QrOrfaos, 'Controle', True);
    //
{
    Genero := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo, Nome ' + Campo,
    'FROM contas ',
    'WHERE Codigo > 0 ',
    'ORDER BY ' + Campo,
    ''], Dmod.MyDB, True);
}
    if DBCheck.Escolhe2CodigoUnicos(vGenero, vGenCtb, Aviso, Titulo, Prompt1,
    Prompt2, nil, nil, nil, nil, Campo, Campo, QrOrfaosGenero.Value, QrOrfaosGenero.Value, [
    'SELECT Codigo, Nome ' + Campo,
    'FROM contas ',
    'WHERE Codigo > 0 ',
    'ORDER BY ' + Campo,
    ''],  [
    'SELECT Codigo, Nome ' + Campo,
    'FROM contas ',
    'WHERE Codigo > 0 ',
    'ORDER BY ' + Campo,
    ''], Dmod.MyDB, True) then
    begin
      if (vGenero <> Null) and (vGenCtb <> Null) then
      begin
        Genero := Integer(vGenero);
        GenCtb := Integer(vGenCtb);
        UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
        'UPDATE ' + FTabLctA + ' SET ' +
        ' Genero=' + Geral.FF0(Genero) +
        ', GenCtb=' + Geral.FF0(GenCtb) +
        ' WHERE Controle IN (' + Corda + ')');
        //
        ReopenOrfaos();
      end;
    end;
  end;
end;

procedure TFmCtbBalPLctEdit.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtbBalPLctEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCtbBalPLctEdit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCtbBalPLctEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtbBalPLctEdit.QrOrfaosAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrOrfaos.RecordCount > 0;
end;

procedure TFmCtbBalPLctEdit.ReopenOrfaos();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrfaos, Dmod.MyDB, [FSQL]);
end;

end.
