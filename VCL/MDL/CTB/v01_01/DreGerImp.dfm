object FmDreGerImp: TFmDreGerImp
  Left = 339
  Top = 185
  Caption = 'DRE-GEREN-006 :: DRE - Impress'#227'o'
  ClientHeight = 314
  ClientWidth = 491
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 491
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 443
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 395
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 210
        Height = 32
        Caption = 'DRE - Impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 210
        Height = 32
        Caption = 'DRE - Impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 210
        Height = 32
        Caption = 'DRE - Impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 200
    Width = 491
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 487
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 244
    Width = 491
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 345
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 343
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 491
    Height = 152
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 812
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 491
      Height = 93
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      object LaAno: TLabel
        Left = 12
        Top = 44
        Width = 22
        Height = 13
        Caption = 'Ano:'
      end
      object LaMes: TLabel
        Left = 108
        Top = 44
        Width = 52
        Height = 13
        Caption = 'M'#234's inicial:'
      end
      object Label3: TLabel
        Left = 296
        Top = 44
        Width = 45
        Height = 13
        Caption = 'M'#234's final:'
      end
      object Label1: TLabel
        Left = 8
        Top = 2
        Width = 23
        Height = 13
        Caption = 'Filial:'
      end
      object CBAno: TComboBox
        Left = 12
        Top = 61
        Width = 90
        Height = 21
        Color = clWhite
        DropDownCount = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        Text = 'CBAno'
      end
      object CBMesI: TComboBox
        Left = 108
        Top = 61
        Width = 182
        Height = 21
        Color = clWhite
        DropDownCount = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        Text = 'CBMesI'
      end
      object CBMesF: TComboBox
        Left = 296
        Top = 61
        Width = 182
        Height = 21
        Color = clWhite
        DropDownCount = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Text = 'CBMes'
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 18
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 18
        Width = 301
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 4
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object RGRelatorio: TRadioGroup
      Left = 12
      Top = 88
      Width = 465
      Height = 49
      Caption = ' Relat'#243'rio: '
      Columns = 3
      Items.Strings = (
        'DRE sint'#233'tico'
        'DRE semi sint'#233'tico'
        'DRE anal'#237'tico')
      TabOrder = 1
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65523
  end
  object QrDre: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT *'
      'FROM _dre_')
    Left = 220
    Top = 140
    object QrDreSorcInfoGru: TIntegerField
      FieldName = 'SorcInfoGru'
      Required = True
    end
    object QrDreSorcInfoItm: TIntegerField
      FieldName = 'SorcInfoItm'
      Required = True
    end
    object QrDreAnoMes: TIntegerField
      FieldName = 'AnoMes'
      Required = True
    end
    object QrDreEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrDreLinha: TIntegerField
      FieldName = 'Linha'
      Required = True
    end
    object QrDreDreCfgEst: TIntegerField
      FieldName = 'DreCfgEst'
      Required = True
    end
    object QrDreDreCfgCtd: TIntegerField
      FieldName = 'DreCfgCtd'
      Required = True
    end
    object QrDreDreCfgCpc: TIntegerField
      FieldName = 'DreCfgCpc'
      Required = True
    end
    object QrDreCpcStatus: TSmallintField
      FieldName = 'CpcStatus'
      Required = True
    end
    object QrDreDreCfgTp1: TIntegerField
      FieldName = 'DreCfgTp1'
      Required = True
    end
    object QrDreDreCfgTp2: TIntegerField
      FieldName = 'DreCfgTp2'
      Required = True
    end
    object QrDreData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrDreGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrDreDebito: TFloatField
      FieldName = 'Debito'
      Required = True
    end
    object QrDreCredito: TFloatField
      FieldName = 'Credito'
      Required = True
    end
    object QrDreGenCtb: TIntegerField
      FieldName = 'GenCtb'
      Required = True
    end
    object QrDreGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
      Required = True
    end
    object QrDreGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
      Required = True
    end
    object QrDreSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrDreNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Required = True
    end
    object QrDreDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object QrDreCfgCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT dcc.*'
      'FROM drecfgcab dcc'
      'WHERE dcc.Codigo > 0')
    Left = 280
    Top = 9
    object QrDreCfgCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDreCfgCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrDreCfgCabRecVenProd: TIntegerField
      FieldName = 'RecVenProd'
      Required = True
    end
    object QrDreCfgCabRecVenMerc: TIntegerField
      FieldName = 'RecVenMerc'
      Required = True
    end
    object QrDreCfgCabRecVenServ: TIntegerField
      FieldName = 'RecVenServ'
      Required = True
    end
    object QrDreCfgCabDevVendas: TIntegerField
      FieldName = 'DevVendas'
      Required = True
    end
    object QrDreCfgCabTriVenICMS: TIntegerField
      FieldName = 'TriVenICMS'
      Required = True
    end
    object QrDreCfgCabTriVenPIS: TIntegerField
      FieldName = 'TriVenPIS'
      Required = True
    end
    object QrDreCfgCabTriVenCOFINS: TIntegerField
      FieldName = 'TriVenCOFINS'
      Required = True
    end
    object QrDreCfgCabTriVenIPI: TIntegerField
      FieldName = 'TriVenIPI'
      Required = True
    end
    object QrDreCfgCabTriDevICMS: TIntegerField
      FieldName = 'TriDevICMS'
      Required = True
    end
    object QrDreCfgCabTriDevPIS: TIntegerField
      FieldName = 'TriDevPIS'
      Required = True
    end
    object QrDreCfgCabTriDevCOFINS: TIntegerField
      FieldName = 'TriDevCOFINS'
      Required = True
    end
    object QrDreCfgCabTriDevIPI: TIntegerField
      FieldName = 'TriDevIPI'
      Required = True
    end
    object QrDreCfgCabCusFixVen: TIntegerField
      FieldName = 'CusFixVen'
      Required = True
    end
  end
  object QrTP1: TMySQLQuery
    Database = DModG.MyPID_DB
    BeforeClose = QrTP1BeforeClose
    AfterScroll = QrTP1AfterScroll
    SQL.Strings = (
      'SELECT dre.DreCfgTp1, tp1.Nome NO_DreCfgTp1, '
      'ABS(SUM(dre.Credito-dre.Debito)) Valor '
      'FROM _dre_ dre'
      
        'LEFT JOIN bluederm_colosso.drecfgtp1 tp1 ON tp1.Codigo=dre.DreCf' +
        'gTp1'
      'GROUP BY dre.DreCfgTp1')
    Left = 28
    Top = 140
    object QrTP1DreCfgTp1: TIntegerField
      FieldName = 'DreCfgTp1'
      Required = True
    end
    object QrTP1NO_DreCfgTp1: TWideStringField
      FieldName = 'NO_DreCfgTp1'
      Size = 100
    end
    object QrTP1Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrTP1TpUso: TIntegerField
      FieldName = 'TpUso'
    end
  end
  object QrTP2: TMySQLQuery
    Database = DModG.MyPID_DB
    AfterScroll = QrTP2AfterScroll
    SQL.Strings = (
      'SELECT dre.DreCfgTp2, tp2.Nome NO_DreCfgTp2, '
      'ABS(SUM(dre.Credito-dre.Debito)) Valor '
      'FROM _dre_ dre'
      
        'LEFT JOIN bluederm_colosso.drecfgtp2 tp2 ON tp2.Controle=dre.Dre' +
        'CfgTp2'
      'WHERE dre.DreCfgTp1=1024'
      'GROUP BY dre.DreCfgTp2')
    Left = 88
    Top = 140
    object QrTP2DreCfgTp2: TIntegerField
      FieldName = 'DreCfgTp2'
      Required = True
    end
    object QrTP2NO_DreCfgTp2: TWideStringField
      FieldName = 'NO_DreCfgTp2'
      Size = 255
    end
    object QrTP2Valor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrGen: TMySQLQuery
    Database = DModG.MyPID_DB
    AfterScroll = QrGenAfterScroll
    SQL.Strings = (
      'SELECT dre.Genero, pac.Ordens, pac.Nome NO_PlaAllCad, '
      'ABS(SUM(dre.Credito-dre.Debito)) Valor '
      'FROM _dre_ dre'
      
        'LEFT JOIN bluederm_colosso.plaallcad pac ON pac.Codigo=dre.Gener' +
        'o'
      'GROUP BY dre.Genero')
    Left = 152
    Top = 140
    object QrGenGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrGenNO_PlaAllCad: TWideStringField
      FieldName = 'NO_PlaAllCad'
      Size = 60
    end
    object QrGenValor: TFloatField
      FieldName = 'Valor'
    end
    object QrGenOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object frxDRE_GEREN_001_A1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    OldStyleProgress = True
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxDRE_GEREN_001_AGetValue
    Left = 268
    Top = 140
    Datasets = <
      item
        DataSet = frxDsTP1
        DataSetName = 'frxDsTP1'
      end
      item
        DataSet = frxDsTP2
        DataSetName = 'frxDsTP2'
      end
      item
        DataSet = frxDsGen
        DataSetName = 'frxDsGen'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsDre
        DataSetName = 'frxDsDre'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 302.362400000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050900
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Empresa [VAR_FILIAL] - [VAR_CINOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 18.897650000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'DRE - Demonstrativo de Resultados do Exerc'#237'cio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 18.897650000000000000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
      end
      object MD1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795275590000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsTP1
        DataSetName = 'frxDsTP1'
        PrintIfDetailEmpty = True
        RowCount = 0
        object MeGH1_1: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 536.692820630000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 6579200
          Font.Height = -13
          Font.Name = 'calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsTP1."NO_DreCfgTp1"]')
          ParentFont = False
        end
        object MeGH1_2: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 7.559060000000000000
          Width = 151.181102360000000000
          Height = 18.000000000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 6579200
          Font.Height = -13
          Font.Name = 'calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTP1."Valor"]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 30.236240000000000000
          Width = 680.314960630000000000
          Color = clBlack
          Frame.Color = 251900
          Frame.Typ = [ftTop]
          Frame.Width = 3.000000000000000000
        end
      end
      object DD1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        DataSet = frxDsTP2
        DataSetName = 'frxDsTP2'
        RowCount = 0
        object MeGH2_01: TfrxMemoView
          AllowVectorExport = True
          Width = 536.692820630000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 6579200
          Memo.UTF8W = (
            '[frxDsTP2."NO_DreCfgTp2"]')
          ParentFont = False
        end
        object MeGH2_02: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 151.180760630000000000
          Height = 18.000000000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 6579200
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTP2."Valor"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsTP1: TfrxDBDataset
    UserName = 'frxDsTP1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DreCfgTp1=DreCfgTp1'
      'NO_DreCfgTp1=NO_DreCfgTp1'
      'Valor=Valor')
    DataSet = QrTP1
    BCDToCurrency = False
    DataSetOptions = []
    Left = 28
    Top = 188
  end
  object frxDsTP2: TfrxDBDataset
    UserName = 'frxDsTP2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DreCfgTp2=DreCfgTp2'
      'NO_DreCfgTp2=NO_DreCfgTp2'
      'Valor=Valor')
    DataSet = QrTP2
    BCDToCurrency = False
    DataSetOptions = []
    Left = 88
    Top = 188
  end
  object frxDsGen: TfrxDBDataset
    UserName = 'frxDsGen'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Genero=Genero'
      'NO_PlaAllCad=NO_PlaAllCad'
      'Valor=Valor'
      'Ordens=Ordens')
    DataSet = QrGen
    BCDToCurrency = False
    DataSetOptions = []
    Left = 152
    Top = 188
  end
  object QrSumCalc: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT '
      'SUM(IF(DreCfgTp1=1024, Credito-Debito, 0)) Valr1024,  '
      'SUM(IF(DreCfgTp1=2048, Credito-Debito, 0)) Valr2048,  '
      'SUM(IF(DreCfgTp1=4096, Credito-Debito, 0)) Valr4096,  '
      'SUM(IF(DreCfgTp1=6144, Credito-Debito, 0)) Valr6144,'
      'SUM(IF(DreCfgTp1=8192, Credito-Debito, 0)) Valr8192,'
      'SUM(IF(DreCfgTp1=9216, Credito-Debito, 0)) Valr9216,'
      'SUM(IF(DreCfgTp1=11264, Credito-Debito, 0)) Valr11264,'
      'SUM(IF(DreCfgTp1=13312, Credito-Debito, 0)) Valr13312'
      '  '
      'FROM _dre_ ')
    Left = 28
    Top = 240
    object QrSumCalcValr1024: TFloatField
      FieldName = 'Valr1024'
    end
    object QrSumCalcValr2048: TFloatField
      FieldName = 'Valr2048'
    end
    object QrSumCalcValr4096: TFloatField
      FieldName = 'Valr4096'
    end
    object QrSumCalcValr6144: TFloatField
      FieldName = 'Valr6144'
    end
    object QrSumCalcValr8192: TFloatField
      FieldName = 'Valr8192'
    end
    object QrSumCalcValr9216: TFloatField
      FieldName = 'Valr9216'
    end
    object QrSumCalcValr11264: TFloatField
      FieldName = 'Valr11264'
    end
    object QrSumCalcValr13312: TFloatField
      FieldName = 'Valr13312'
    end
  end
  object frxDRE_GEREN_001_B1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    OldStyleProgress = True
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxDRE_GEREN_001_AGetValue
    Left = 268
    Top = 188
    Datasets = <
      item
        DataSet = frxDsTP1
        DataSetName = 'frxDsTP1'
      end
      item
        DataSet = frxDsTP2
        DataSetName = 'frxDsTP2'
      end
      item
        DataSet = frxDsGen
        DataSetName = 'frxDsGen'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsDre
        DataSetName = 'frxDsDre'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050900
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Empresa [VAR_FILIAL] - [VAR_CINOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 18.897650000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'DRE - Demonstrativo de Resultados do Exerc'#237'cio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 18.897650000000000000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
      end
      object MD1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795275590000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsTP1
        DataSetName = 'frxDsTP1'
        PrintIfDetailEmpty = True
        RowCount = 0
        object MeGH1_1: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 536.692820630000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 6579200
          Font.Height = -13
          Font.Name = 'calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsTP1."NO_DreCfgTp1"]')
          ParentFont = False
        end
        object MeGH1_2: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 7.559060000000000000
          Width = 151.181102360000000000
          Height = 18.000000000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 6579200
          Font.Height = -13
          Font.Name = 'calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTP1."Valor"]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 30.236240000000000000
          Width = 680.314960630000000000
          Color = clBlack
          Frame.Color = 251900
          Frame.Typ = [ftTop]
          Frame.Width = 3.000000000000000000
        end
      end
      object DD1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        DataSet = frxDsTP2
        DataSetName = 'frxDsTP2'
        RowCount = 0
        object MeGH2_01: TfrxMemoView
          AllowVectorExport = True
          Width = 536.692820630000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 6579200
          Memo.UTF8W = (
            '[frxDsTP2."NO_DreCfgTp2"]')
          ParentFont = False
        end
        object MeGH2_02: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 151.180760630000000000
          Height = 18.000000000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 6579200
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTP2."Valor"]')
          ParentFont = False
        end
      end
      object SD1: TfrxSubdetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.755905510000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        DataSet = frxDsGen
        DataSetName = 'frxDsGen'
        RowCount = 0
        object MeGH3_01: TfrxMemoView
          AllowVectorExport = True
          Top = 0.755905510000000000
          Width = 113.385460630000000000
          Height = 18.000000000000000000
          DataField = 'Ordens'
          DataSet = frxDsGen
          DataSetName = 'frxDsGen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 15657701
          Memo.UTF8W = (
            '[frxDsGen."Ordens"]')
          ParentFont = False
        end
        object MeGH3_02: TfrxMemoView
          AllowVectorExport = True
          Left = 117.165430000000000000
          Top = 0.755905510000000000
          Width = 438.425040630000000000
          Height = 18.000000000000000000
          DataField = 'NO_PlaAllCad'
          DataSet = frxDsGen
          DataSetName = 'frxDsGen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 15657701
          Memo.UTF8W = (
            '[frxDsGen."NO_PlaAllCad"]')
          ParentFont = False
        end
        object MeGH3_03: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 0.755905510000000000
          Width = 120.944520630000000000
          Height = 18.000000000000000000
          DataField = 'Valor'
          DataSet = frxDsGen
          DataSetName = 'frxDsGen'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 15657701
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGen."Valor"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDRE_GEREN_001_C1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    OldStyleProgress = True
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxDRE_GEREN_001_AGetValue
    Left = 268
    Top = 236
    Datasets = <
      item
        DataSet = frxDsTP1
        DataSetName = 'frxDsTP1'
      end
      item
        DataSet = frxDsTP2
        DataSetName = 'frxDsTP2'
      end
      item
        DataSet = frxDsGen
        DataSetName = 'frxDsGen'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsDre
        DataSetName = 'frxDsDre'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 396.850650000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050900
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Empresa [VAR_FILIAL] - [VAR_CINOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 18.897650000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'DRE - Demonstrativo de Resultados do Exerc'#237'cio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 18.897650000000000000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_PERIODO]')
          ParentFont = False
        end
      end
      object MD1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795275590000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsTP1
        DataSetName = 'frxDsTP1'
        PrintIfDetailEmpty = True
        RowCount = 0
        object MeGH1_1: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 536.692820630000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 6579200
          Font.Height = -13
          Font.Name = 'calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsTP1."NO_DreCfgTp1"]')
          ParentFont = False
        end
        object MeGH1_2: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 7.559060000000000000
          Width = 151.181102360000000000
          Height = 18.000000000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 6579200
          Font.Height = -13
          Font.Name = 'calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTP1."Valor"]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 30.236240000000000000
          Width = 680.314960630000000000
          Color = clBlack
          Frame.Color = 251900
          Frame.Typ = [ftTop]
          Frame.Width = 3.000000000000000000
        end
      end
      object DD1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        DataSet = frxDsTP2
        DataSetName = 'frxDsTP2'
        RowCount = 0
        object MeGH2_01: TfrxMemoView
          AllowVectorExport = True
          Width = 536.692820630000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 6579200
          Memo.UTF8W = (
            '[frxDsTP2."NO_DreCfgTp2"]')
          ParentFont = False
        end
        object MeGH2_02: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 151.180760630000000000
          Height = 18.000000000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 6579200
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTP2."Valor"]')
          ParentFont = False
        end
      end
      object SD1: TfrxSubdetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015760240000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        DataSet = frxDsGen
        DataSetName = 'frxDsGen'
        RowCount = 0
        object MeGH3_01: TfrxMemoView
          AllowVectorExport = True
          Top = 0.755905510000000000
          Width = 113.385460630000000000
          Height = 18.000000000000000000
          DataField = 'Ordens'
          DataSet = frxDsGen
          DataSetName = 'frxDsGen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 15657701
          Memo.UTF8W = (
            '[frxDsGen."Ordens"]')
          ParentFont = False
        end
        object MeGH3_02: TfrxMemoView
          AllowVectorExport = True
          Left = 117.165430000000000000
          Top = 0.755905510000000000
          Width = 438.425040630000000000
          Height = 18.000000000000000000
          DataField = 'NO_PlaAllCad'
          DataSet = frxDsGen
          DataSetName = 'frxDsGen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 15657701
          Memo.UTF8W = (
            '[frxDsGen."NO_PlaAllCad"]')
          ParentFont = False
        end
        object MeGH3_03: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 0.755905510000000000
          Width = 120.944520630000000000
          Height = 18.000000000000000000
          DataField = 'Valor'
          DataSet = frxDsGen
          DataSetName = 'frxDsGen'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 15657701
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGen."Valor"]')
          ParentFont = False
        end
        object MeGH3_12: TfrxMemoView
          AllowVectorExport = True
          Left = 64.251975830000000000
          Top = 18.897650000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeGH3_11: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeGH3_13: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Top = 18.897650000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NF')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeGH3_16: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Top = 18.897650000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeGH3_14: TfrxMemoView
          AllowVectorExport = True
          Left = 192.756030000000000000
          Top = 18.897650000000000000
          Width = 298.582828500000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hist'#243'rico')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeGH3_15: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 18.897650000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DataBand41: TfrxDataBand4
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        DataSet = frxDsDre
        DataSetName = 'frxDsDre'
        RowCount = 0
        object MeMD1_02: TfrxMemoView
          AllowVectorExport = True
          Left = 64.251975830000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataField = 'SerieNF'
          DataSet = frxDsDre
          DataSetName = 'frxDsDre'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDre."SerieNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeMD1_01: TfrxMemoView
          AllowVectorExport = True
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = frxDsDre
          DataSetName = 'frxDsDre'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDre."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeMD1_03: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataField = 'NotaFiscal'
          DataSet = frxDsDre
          DataSetName = 'frxDsDre'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDre."NotaFiscal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeMD1_05: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataField = 'Debito'
          DataSet = frxDsDre
          DataSetName = 'frxDsDre'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDre."Debito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeMD1_06: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataField = 'Credito'
          DataSet = frxDsDre
          DataSetName = 'frxDsDre'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDre."Credito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeMD1_04: TfrxMemoView
          AllowVectorExport = True
          Left = 192.756030000000000000
          Width = 298.582828500000000000
          Height = 15.118110240000000000
          DataField = 'Descricao'
          DataSet = frxDsDre
          DataSetName = 'frxDsDre'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Calibri'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDre."Descricao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 104
    Top = 484
  end
  object frxDsDre: TfrxDBDataset
    UserName = 'frxDsDre'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SorcInfoGru=SorcInfoGru'
      'SorcInfoItm=SorcInfoItm'
      'AnoMes=AnoMes'
      'Empresa=Empresa'
      'Linha=Linha'
      'DreCfgEst=DreCfgEst'
      'DreCfgCtd=DreCfgCtd'
      'DreCfgCpc=DreCfgCpc'
      'CpcStatus=CpcStatus'
      'DreCfgTp1=DreCfgTp1'
      'DreCfgTp2=DreCfgTp2'
      'Data=Data'
      'Genero=Genero'
      'Debito=Debito'
      'Credito=Credito'
      'GenCtb=GenCtb'
      'GenCtbD=GenCtbD'
      'GenCtbC=GenCtbC'
      'SerieNF=SerieNF'
      'NotaFiscal=NotaFiscal'
      'Descricao=Descricao')
    DataSet = QrDre
    BCDToCurrency = False
    DataSetOptions = []
    Left = 220
    Top = 188
  end
end
