unit DreCfgCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls, dmkDBLookupComboBox, dmkEditCB;

type
  TFmDreCfgCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrDreCfgCab: TMySQLQuery;
    DsDreCfgCab: TDataSource;
    QrDreCfgCtd: TMySQLQuery;
    DsDreCfgCtd: TDataSource;
    PMCtd: TPopupMenu;
    CtdInclui1: TMenuItem;
    CtdExclui1: TMenuItem;
    CtdAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtEst: TBitBtn;
    QrDreCfgCpc: TMySQLQuery;
    DsDreCfgCpc: TDataSource;
    PMCpc: TPopupMenu;
    CpcInclui1: TMenuItem;
    CpcAltera1: TMenuItem;
    CpcExclui1: TMenuItem;
    BtCtd: TBitBtn;
    BtCpc: TBitBtn;
    PMEst: TPopupMenu;
    EstInclui1: TMenuItem;
    EstAltera1: TMenuItem;
    EstExclui1: TMenuItem;
    QrDreCfgEst: TMySQLQuery;
    DsDreCfgEst: TDataSource;
    QrDreCfgEstCodigo: TIntegerField;
    QrDreCfgEstControle: TIntegerField;
    QrDreCfgEstNome: TWideStringField;
    QrDreCfgEstDreCfgTp1: TIntegerField;
    QrDreCfgEstOrdem: TIntegerField;
    QrDreCfgCtdCodigo: TIntegerField;
    QrDreCfgCtdControle: TIntegerField;
    QrDreCfgCtdConta: TIntegerField;
    QrDreCfgCtdNome: TWideStringField;
    QrDreCfgCtdDreCfgTp2: TIntegerField;
    QrDreCfgCtdOrdem: TIntegerField;
    QrDCC: TMySQLQuery;
    QrDCCPlaAllCad: TIntegerField;
    QrDCCExcluso: TIntegerField;
    QrDreCfgCpcIDXSUB4: TLargeintField;
    QrDreCfgCpcEXCLUSO: TIntegerField;
    QrDreCfgCpcStatus: TIntegerField;
    QrDreCfgCpcPlaAllcad: TIntegerField;
    QrDreCfgCpcNome: TWideStringField;
    QrDreCfgCpcOrdens: TWideStringField;
    QrDreCfgCpcMesCompet: TIntegerField;
    QrDreCfgCpcNO_MesCompet: TWideStringField;
    QrDreCfgCabNO_Empresa: TWideStringField;
    QrDreCfgCabEmpresa: TIntegerField;
    PCDados: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel6: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Panel7: TPanel;
    DGDados: TDBGrid;
    Panel10: TPanel;
    Panel8: TPanel;
    DBGCpc: TDBGrid;
    Panel9: TPanel;
    Panel11: TPanel;
    DBGrid2: TDBGrid;
    Panel12: TPanel;
    Panel13: TPanel;
    QrDreCfgCabRecVenProd: TIntegerField;
    QrDreCfgCabRecVenMerc: TIntegerField;
    QrDreCfgCabRecVenServ: TIntegerField;
    QrDreCfgCabDevVendas: TIntegerField;
    QrDreCfgCabTriVenICMS: TIntegerField;
    QrDreCfgCabTriVenPIS: TIntegerField;
    QrDreCfgCabTriVenCOFINS: TIntegerField;
    QrDreCfgCabTriVenIPI: TIntegerField;
    QrDreCfgCabTriDevICMS: TIntegerField;
    QrDreCfgCabTriDevPIS: TIntegerField;
    QrDreCfgCabTriDevCOFINS: TIntegerField;
    QrDreCfgCabTriDevIPI: TIntegerField;
    QrDreCfgCabCusFixVen: TIntegerField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label10: TLabel;
    DBEdit6: TDBEdit;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    Label12: TLabel;
    DBEdit8: TDBEdit;
    Label13: TLabel;
    DBEdit9: TDBEdit;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    Label15: TLabel;
    DBEdit11: TDBEdit;
    Label16: TLabel;
    DBEdit12: TDBEdit;
    Label17: TLabel;
    DBEdit13: TDBEdit;
    QrPlaAllCad: TMySQLQuery;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    QrPlaAllCadCodigo: TIntegerField;
    QrPlaAllCadNome: TWideStringField;
    QrPlaAllCadOrdens: TWideStringField;
    QrDreCfgCabNO_RecVenProd: TWideStringField;
    QrDreCfgCabNO_RecVenMerc: TWideStringField;
    QrDreCfgCabNO_RecVenServ: TWideStringField;
    QrDreCfgCabNO_DevVendas: TWideStringField;
    QrDreCfgCabNO_TriVenICMS: TWideStringField;
    QrDreCfgCabNO_TriVenPIS: TWideStringField;
    QrDreCfgCabNO_TriVenCOFINS: TWideStringField;
    QrDreCfgCabNO_TriVenIPI: TWideStringField;
    QrDreCfgCabNO_TriDevICMS: TWideStringField;
    QrDreCfgCabNO_TriDevPIS: TWideStringField;
    QrDreCfgCabNO_TriDevCOFINS: TWideStringField;
    QrDreCfgCabNO_TriDevIPI: TWideStringField;
    QrDreCfgCabNO_CusFixVen: TWideStringField;
    QrDreCfgCabORD_RecVenProd: TWideStringField;
    QrDreCfgCabORD_RecVenMerc: TWideStringField;
    QrDreCfgCabORD_RecVenServ: TWideStringField;
    QrDreCfgCabORD_DevVendas: TWideStringField;
    QrDreCfgCabORD_TriVenICMS: TWideStringField;
    QrDreCfgCabORD_TriVenPIS: TWideStringField;
    QrDreCfgCabORD_TriVenCOFINS: TWideStringField;
    QrDreCfgCabORD_TriVenIPI: TWideStringField;
    QrDreCfgCabORD_TriDevICMS: TWideStringField;
    QrDreCfgCabORD_TriDevPIS: TWideStringField;
    QrDreCfgCabORD_TriDevCOFINS: TWideStringField;
    QrDreCfgCabORD_TriDevIPI: TWideStringField;
    QrDreCfgCabORD_CusFixVen: TWideStringField;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    EdRecVenProd: TdmkEditCB;
    CBRecVenProd: TdmkDBLookupComboBox;
    EdRecVenMerc: TdmkEditCB;
    CBRecVenMerc: TdmkDBLookupComboBox;
    EdRecVenServ: TdmkEditCB;
    CBRecVenServ: TdmkDBLookupComboBox;
    CBTriVenPIS: TdmkDBLookupComboBox;
    EdTriVenPIS: TdmkEditCB;
    EdDevVendas: TdmkEditCB;
    CBDevVendas: TdmkDBLookupComboBox;
    EdTriVenICMS: TdmkEditCB;
    CBTriVenICMS: TdmkDBLookupComboBox;
    EdTriVenCOFINS: TdmkEditCB;
    CBTriVenCOFINS: TdmkDBLookupComboBox;
    EdTriVenIPI: TdmkEditCB;
    CbTriVenIPI: TdmkDBLookupComboBox;
    QrRecVenProd: TMySQLQuery;
    QrRecVenProdCodigo: TIntegerField;
    QrRecVenProdNome: TWideStringField;
    QrRecVenProdOrdens: TWideStringField;
    QrRecVenMerc: TMySQLQuery;
    QrRecVenMercCodigo: TIntegerField;
    QrRecVenMercNome: TWideStringField;
    QrRecVenMercOrdens: TWideStringField;
    QrRecVenServ: TMySQLQuery;
    QrRecVenServCodigo: TIntegerField;
    QrRecVenServNome: TWideStringField;
    QrRecVenServOrdens: TWideStringField;
    QrDevVendas: TMySQLQuery;
    QrDevVendasCodigo: TIntegerField;
    QrDevVendasNome: TWideStringField;
    QrDevVendasOrdens: TWideStringField;
    QrTriVenPIS: TMySQLQuery;
    QrTriVenPISCodigo: TIntegerField;
    QrTriVenPISNome: TWideStringField;
    QrTriVenPISOrdens: TWideStringField;
    QrTriVenICMS: TMySQLQuery;
    QrTriVenICMSCodigo: TIntegerField;
    QrTriVenICMSNome: TWideStringField;
    QrTriVenICMSOrdens: TWideStringField;
    QrTriVenCOFINS: TMySQLQuery;
    QrTriVenCOFINSCodigo: TIntegerField;
    QrTriVenCOFINSNome: TWideStringField;
    QrTriVenCOFINSOrdens: TWideStringField;
    QrTriVenIPI: TMySQLQuery;
    QrTriVenIPICodigo: TIntegerField;
    QrTriVenIPINome: TWideStringField;
    QrTriVenIPIOrdens: TWideStringField;
    EdTriDevICMS: TdmkEditCB;
    CBTriDevICMS: TdmkDBLookupComboBox;
    EdTriDevPIS: TdmkEditCB;
    CBTriDevPIS: TdmkDBLookupComboBox;
    EdTriDevCOFINS: TdmkEditCB;
    CBTriDevCOFINS: TdmkDBLookupComboBox;
    EdTriDevIPI: TdmkEditCB;
    CbTriDevIPI: TdmkDBLookupComboBox;
    QrTriDevICMS: TMySQLQuery;
    QrTriDevICMSCodigo: TIntegerField;
    QrTriDevICMSNome: TWideStringField;
    QrTriDevICMSOrdens: TWideStringField;
    QrTriDevPIS: TMySQLQuery;
    QrTriDevPISCodigo: TIntegerField;
    QrTriDevPISNome: TWideStringField;
    QrTriDevPISOrdens: TWideStringField;
    QrTriDevCOFINS: TMySQLQuery;
    QrTriDevCOFINSCodigo: TIntegerField;
    QrTriDevCOFINSNome: TWideStringField;
    QrTriDevCOFINSOrdens: TWideStringField;
    QrTriDevIPI: TMySQLQuery;
    QrTriDevIPICodigo: TIntegerField;
    QrTriDevIPINome: TWideStringField;
    QrTriDevIPIOrdens: TWideStringField;
    QrCusFixVen: TMySQLQuery;
    IntegerField1: TIntegerField;
    WideStringField1: TWideStringField;
    WideStringField2: TWideStringField;
    DsRecVenProd: TDataSource;
    DsRecVenMerc: TDataSource;
    DsRecVenServ: TDataSource;
    DsDevVendas: TDataSource;
    DsTriVenICMS: TDataSource;
    DsTriVenPIS: TDataSource;
    DsTriVenCOFINS: TDataSource;
    DsTriVenIPI: TDataSource;
    DsTriDevICMS: TDataSource;
    DsTriDevPIS: TDataSource;
    DsTriDevCOFINS: TDataSource;
    DsTriDevIPI: TDataSource;
    DsCusFixVen: TDataSource;
    Label31: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdCusFixVen: TdmkEditCB;
    CBCusFixVen: TdmkDBLookupComboBox;
    Label32: TLabel;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    QrDreCfgCabCodigo: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrDreCfgCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrDreCfgCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrDreCfgCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure CtdInclui1Click(Sender: TObject);
    procedure CtdExclui1Click(Sender: TObject);
    procedure CtdAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMCtdPopup(Sender: TObject);
    procedure QrDreCfgCabBeforeClose(DataSet: TDataSet);
    procedure QrDreCfgCtdAfterScroll(DataSet: TDataSet);
    procedure PMCpcPopup(Sender: TObject);
    procedure EstInclui1Click(Sender: TObject);
    procedure EstAltera1Click(Sender: TObject);
    procedure EstExclui1Click(Sender: TObject);
    procedure QrDreCfgEstAfterScroll(DataSet: TDataSet);
    procedure QrDreCfgCtdBeforeClose(DataSet: TDataSet);
    procedure QrDreCfgEstBeforeClose(DataSet: TDataSet);
    procedure PMEstPopup(Sender: TObject);
    procedure BtEstClick(Sender: TObject);
    procedure BtCtdClick(Sender: TObject);
    procedure BtCpcClick(Sender: TObject);
    procedure CpcInclui1Click(Sender: TObject);
    procedure CpcAltera1Click(Sender: TObject);
    procedure CpcExclui1Click(Sender: TObject);
    procedure DBGCpcDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraFormDreCfgEst(SQLType: TSQLType);
    procedure MostraFormDreCfgCtd(SQLType: TSQLType);
    procedure MostraFormDreCfgCpc(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenDreCfgEst(Controle: Integer);
    procedure ReopenDreCfgCtd(Conta: Integer);
    procedure ReopenDreCfgCpc(IdxSub4: Integer);

  end;

var
  FmDreCfgCab: TFmDreCfgCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, DreCfgEst, DreCfgCtd, DreCfgCpc,
  ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmDreCfgCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmDreCfgCab.MostraFormDreCfgCpc(SQLType: TSQLType);
var
  IdxSub4: Integer;
begin
  if DBCheck.CriaFm(TFmDreCfgCpc, FmDreCfgCpc, afmoNegarComAviso) then
  begin
    FmDreCfgCpc.ImgTipo.SQLType := SQLType;
    //FmDreCfgCpc.FQrIts     := QrDreCfgCpc;
    FmDreCfgCpc.EdCodigo.ValueVariant     := QrDreCfgCabEmpresa.Value;
    FmDreCfgCpc.EdNo_Empresa.ValueVariant := QrDreCfgCabNO_Empresa.Value;
    FmDreCfgCpc.EdControle.ValueVariant   := QrDreCfgEstControle.Value;
    FmDreCfgCpc.EdNomCtrl.ValueVariant    := QrDreCfgEstNome.Value;
    FmDreCfgCpc.EdConta.ValueVariant      := QrDreCfgCtdConta.Value;
    FmDreCfgCpc.EdNomCta.ValueVariant     := QrDreCfgCtdNome.Value;
    if SQLType = stIns then
    begin
      // Facilitar defini��o de Excluso
      FmDreCfgCpc.EdPlaAllCad.ValueVariant  := QrDreCfgCpcPlaAllCad.Value;
      FmDreCfgCpc.CBPlaAllCad.KeyValue      := QrDreCfgCpcPlaAllCad.Value;
    end else
    begin
      FmDreCfgCpc.EdIdxSub4.ValueVariant    := QrDreCfgCpcIdxSub4.Value;
      //
      FmDreCfgCpc.RGExcluso.ItemIndex       := QrDreCfgCpcExcluso.Value;
      FmDreCfgCpc.RGMesCompet.ItemIndex     := QrDreCfgCpcMesCompet.Value;
    end;
    FmDreCfgCpc.ShowModal;
    FmDreCfgCpc.Destroy;
  end;
  //if (QrDreCfgCpc.State <> dsInactive) and (QrDreCfgCpc.RecordCount > 0) then
  if (QrDreCfgCpc.State <> dsInactive) then
  begin
    IdxSub4 :=  QrDreCfgCpcIdxSub4.Value;
    ReopenDreCfgCpc(IdxSub4);
  end;
end;

procedure TFmDreCfgCab.MostraFormDreCfgCtd(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmDreCfgCtd, FmDreCfgCtd, afmoNegarComAviso) then
  begin
    FmDreCfgCtd.ImgTipo.SQLType := SQLType;
    FmDreCfgCtd.FQrIts     := QrDreCfgCtd;
    FmDreCfgCtd.FDreCfgTp1 := QrDreCfgEstDreCfgTp1.Value;
    //
    FmDreCfgCtd.EdCodigo.ValueVariant   := QrDreCfgCabCodigo.Value;
    FmDreCfgCtd.EdControle.ValueVariant := QrDreCfgEstControle.Value;
    FmDreCfgCtd.EdCodigo.ValueVariant   := QrDreCfgCabCodigo.Value;
    FmDreCfgCtd.EdNomCod.ValueVariant   := QrDreCfgCabNO_Empresa.Value;
    FmDreCfgCtd.EdControle.ValueVariant := QrDreCfgEstControle.Value;
    FmDreCfgCtd.EdNomCtrl.ValueVariant  := QrDreCfgEstNome.Value;
    FmDreCfgCtd.ReopenDreCfgTp2();
    if SQLType = stIns then
      //
    else
    begin
      FmDreCfgCtd.EdConta.ValueVariant  := QrDreCfgCtdConta.Value;
      //
      FmDreCfgCtd.EdNome.ValueVariant       := QrDreCfgCtdNome.Value;
      FmDreCfgCtd.EdDreCfgTp2.ValueVariant  := QrDreCfgCtdDreCfgTp2.Value;
      FmDreCfgCtd.CBDreCfgTp2.KeyValue      := QrDreCfgCtdDreCfgTp2.Value;
      FmDreCfgCtd.EdOrdem.ValueVariant      := QrDreCfgCtdOrdem.Value;
    end;
    FmDreCfgCtd.ShowModal;
    FmDreCfgCtd.Destroy;
  end;
end;

procedure TFmDreCfgCab.MostraFormDreCfgEst(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmDreCfgEst, FmDreCfgEst, afmoNegarComAviso) then
  begin
    FmDreCfgEst.ImgTipo.SQLType := SQLType;
    FmDreCfgEst.FQrIts := QrDreCfgEst;
    FmDreCfgEst.EdCodigo.ValueVariant := QrDreCfgCabCodigo.Value;
    FmDreCfgEst.EdNomCod.ValueVariant := QrDreCfgCabNO_Empresa.Value;
    if SQLType = stIns then
      //
    else
    begin
      FmDreCfgEst.EdControle.ValueVariant  := QrDreCfgEstControle.Value;
      //
      FmDreCfgEst.EdNome.ValueVariant       := QrDreCfgEstNome.Value;
      FmDreCfgEst.EdDreCfgTp1.ValueVariant  := QrDreCfgEstDreCfgTp1.Value;
      FmDreCfgEst.CBDreCfgTp1.KeyValue      := QrDreCfgEstDreCfgTp1.Value;
      FmDreCfgEst.EdOrdem.ValueVariant      := QrDreCfgEstOrdem.Value;
    end;
    FmDreCfgEst.ShowModal;
    FmDreCfgEst.Destroy;
  end;
end;

procedure TFmDreCfgCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrDreCfgCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrDreCfgCab, QrDreCfgCtd);
end;

procedure TFmDreCfgCab.PMCpcPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(CpcInclui1, QrDreCfgCtd);
  MyObjects.HabilitaMenuItemItsUpd(CpcAltera1, QrDreCfgCpc);
  MyObjects.HabilitaMenuItemItsDel(CpcExclui1, QrDreCfgCpc);
  if (QrDreCfgCpc.State <> dsInactive) and
  //(not (QrDreCfgCpcStatus.Value in ([1,2]))) then
  (not (TDreCfgCpcStatus(QrDreCfgCpcStatus.Value) in ([dccsAutoIncluso, dccsAutoExcluso]))) then
  begin
    CpcAltera1.Enabled := False;
    CpcExclui1.Enabled := False;
  end;
end;

procedure TFmDreCfgCab.PMCtdPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(CtdInclui1, QrDreCfgEst);
  MyObjects.HabilitaMenuItemItsUpd(CtdAltera1, QrDreCfgCtd);
  MyObjects.HabilitaMenuItemCabDel(CtdExclui1, QrDreCfgCtd, QrDreCfgCpc);
end;

procedure TFmDreCfgCab.PMEstPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(EstInclui1, QrDreCfgCab);
  MyObjects.HabilitaMenuItemItsUpd(EstAltera1, QrDreCfgEst);
  MyObjects.HabilitaMenuItemCabDel(EstExclui1, QrDreCfgEst, QrDreCfgCtd);
end;

procedure TFmDreCfgCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrDreCfgCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmDreCfgCab.DefParams;
begin
  VAR_GOTOTABELA := 'drecfgcab';
  VAR_GOTOMYSQLTABLE := QrDreCfgCab;
  VAR_GOTONEG := gotoNeg;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_Empresa,');
  VAR_SQLx.Add('dcc.*');
  VAR_SQLx.Add('FROM drecfgcab dcc');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=dcc.Empresa');
  VAR_SQLx.Add('WHERE dcc.Codigo <> 0');
  //
  VAR_SQL1.Add('AND dcc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND dcc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND dcc.Nome Like :P0');
  //
end;

procedure TFmDreCfgCab.EstAltera1Click(Sender: TObject);
begin
  MostraFormDreCfgEst(stUpd);
end;

procedure TFmDreCfgCab.EstExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item de estrutura selecionado?',
  'DreCfgEst', 'Controle', QrDreCfgEstControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrDreCfgEst,
      QrDreCfgEstControle, QrDreCfgEstControle.Value);
    ReopenDreCfgEst(Controle);
  end;
end;

procedure TFmDreCfgCab.EstInclui1Click(Sender: TObject);
begin
  MostraFormDreCfgEst(stIns);
end;

procedure TFmDreCfgCab.CtdAltera1Click(Sender: TObject);
begin
  MostraFormDreCfgCtd(stUpd);
end;

procedure TFmDreCfgCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmDreCfgCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmDreCfgCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmDreCfgCab.CtdExclui1Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item de conte�do selecionado?',
  'DreCfgCtd', 'Conta', QrDreCfgCtdConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrDreCfgCtd,
      QrDreCfgCtdConta, QrDreCfgCtdConta.Value);
    ReopenDreCfgCtd(Conta);
  end;
end;

procedure TFmDreCfgCab.ReopenDreCfgCpc(IdxSub4: Integer);
var
  sPAC, sEx0, sEx1, sConta, sCodigo: String;
  //
  function DefineCodigosSQLs(): String;
  var
    I: Integer;
    s: String;
  begin
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrDCC, Dmod.MyDB, [
    'SELECT cpc.PlaAllCad, cpc.Excluso',
    'FROM drecfgcpc cpc',
    'WHERE cpc.Conta=' + Geral.FF0(QrDreCfgCtdConta.Value),
    '']);
    //
    //Geral.MB_Teste(QrDCC.SQL.Text);
    //
    if QrDCC.RecordCount > 0 then
    begin
      sPAC := EmptyStr;
      sEx0 := EmptyStr;
      sEx1 := EmptyStr;
      try
        QrDCC.DisableControls;
        QrDCC.First;
        while not QrDCC.Eof do
        begin
          s := Geral.FF0(QrDCCPlaAllCad.Value);
          //
          if sPAC <> '' then
            sPAC := sPAC + ', ';
          sPAC := sPAC + s;
          //
          if QrDCCExcluso.Value = 0 then
          begin
            if sEx0 <> '' then
              sEx0 := sEx0 + ', ';
            sEx0 := sEx0 + s;
          end else
          begin
            if sEx1 <> '' then
              sEx1 := sEx1 + ', ';
            sEx1 := sEx1 + s;
          end;
          QrDCC.Next;
        end;
      finally
        QrDCC.EnableControls;
      end;
    end;
    if sPAC = EmptyStr then
      sPAC := '-999999999';
    if sEx0 = EmptyStr then
      sEx0 := '-999999999';
    if sEx1 = EmptyStr then
      sEx1 := '-999999999';
  end;
  //
begin
  DefineCodigosSQLs();
  //
  sConta  :=  Geral.FF0(QrDreCfgCtdConta.Value);
  sCodigo :=  Geral.FF0(QrDreCfgCabCodigo.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgCpc, Dmod.MyDB, [
  'SELECT ',
  ' IF(cpc.Conta=' + sConta + ', cpc.IdxSub4, NULL) IDXSUB4, ',
  ' IF(cpc.Conta=' + sConta + ', cpc.Excluso, NULL) EXCLUSO, ',
  'IF(cpc.MesCompet IS NULL, 0, cpc.MesCompet) MesCompet,  ',
  'IF(cpc.MesCompet = 1, "S", "E") NO_MesCompet,  ',
  //
  'IF(pac.Codigo IN (' + sEx0 + '), 1,',
  '  IF(pac.Codigo IN (' + sEx1 + '), 2, ',
  '  IF(pac.CodNiv5 IN (' + sEx1 + '), 4,',
  '  IF(pac.CodNiv5 IN (' + sEx0 + '), 3,',
  '  IF(pac.CodNiv4 IN (' + sEx1 + '), 4,',
  '  IF(pac.CodNiv4 IN (' + sEx0 + '), 3,',
  '  IF(pac.CodNiv3 IN (' + sEx1 + '), 4,',
  '  IF(pac.CodNiv3 IN (' + sEx0 + '), 3,',
  '  IF(pac.CodNiv2 IN (' + sEx1 + '), 4,',
  '  IF(pac.CodNiv2 IN (' + sEx0 + '), 3,',
  '  IF(pac.CodNiv1 IN (' + sEx1 + '), 4,',
  '  IF(pac.CodNiv1 IN (' + sEx0 + '), 3,',
  '  9))))))))))))',
  'Status,',
  'pac.Codigo PlaAllcad, pac.Nome, pac.Ordens ',
  'FROM plaallcad pac ',
  'LEFT JOIN drecfgcpc cpc ON pac.Codigo=cpc.PlaAllCad ',
  'WHERE ',
  '( ',
  '  pac.Codigo IN (' + sPAC + ')',
  '  OR pac.CodNiv1 IN (' + sPAC + ')',
  '  OR pac.CodNiv2 IN (' + sPAC + ')',
  '  OR pac.CodNiv3 IN (' + sPAC + ')',
  '  OR pac.CodNiv4 IN (' + sPAC + ')',
  '  OR pac.CodNiv5 IN (' + sPAC + ')',
  ') ',
  'AND ( ',
  '  (cpc.Conta=' + sConta + ')',
  '  OR (cpc.Conta IS NULL) ',
  '  OR (cpc.Conta<>' + sConta + ' AND cpc.Codigo <> ' + sCodigo,
  '    AND NOT (pac.Codigo IN (' + sPAC + ')))  ',
  ') ',
  //
  'ORDER BY Ordens',
  '']);
  //
  //Geral.MB_Teste(QrDreCfgCpc.SQL.Text);
  if IdxSub4 > 0 then
    QrDreCfgCpc.Locate('IdxSub4', IdxSub4, []);
end;

procedure TFmDreCfgCab.ReopenDreCfgCtd(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgCtd, Dmod.MyDB, [
  'SELECT * ',
  'FROM drecfgctd ',
  'WHERE Controle=' + Geral.FF0(QrDreCfgEstControle.Value),
  '']);
  //
  QrDreCfgCtd.Locate('Conta',Conta, []);
end;

procedure TFmDreCfgCab.ReopenDreCfgEst(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDreCfgEst, Dmod.MyDB, [
  'SELECT * ',
  'FROM drecfgest ',
  'WHERE Codigo=' + Geral.FF0(QrDreCfgCabCodigo.Value),
  'ORDER BY Ordem, Controle ',
  '']);
  //
  QrDreCfgEst.Locate('Controle',Controle, []);
end;

procedure TFmDreCfgCab.DBGCpcDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
  Negrito: Integer;
begin
  case TDreCfgCpcStatus(QrDreCfgCpcStatus.Value) of
    (*0*)dccsIndef            : Cor := clGray;
    (*1*)dccsAutoIncluso      : Cor := clGreen;
    (*2*)dccsAutoExcluso      : Cor := clRed;
    (*3*)dccsAutoInclusoFilho : Cor := clBlue;
    (*4*)dccsAutoExclusoFilho : Cor := clLaranja;
    //dccsManual=5,
    //dccsErro=9
    else                        Cor := clFuchsia;
  end;
  //
  //if QrDreCfgCpcStatus.Value in ([1, 2]) then
  if (TDreCfgCpcStatus(QrDreCfgCpcStatus.Value) in ([dccsAutoIncluso, dccsAutoExcluso])) then
    Negrito := 1
  else
    Negrito := 0;
  MyObjects.DesenhaTextoEmDBGrid(DBGCpc, Rect, Cor, clWhite,
    Column.Alignment, Column.Field.DisplayText, Negrito);
end;

procedure TFmDreCfgCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmDreCfgCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmDreCfgCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmDreCfgCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmDreCfgCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmDreCfgCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDreCfgCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrDreCfgCabCodigo.Value;
  Close;
end;

procedure TFmDreCfgCab.CtdInclui1Click(Sender: TObject);
begin
  MostraFormDreCfgCtd(stIns);
end;

procedure TFmDreCfgCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrDreCfgCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'drecfgcab');
  DModG.ObtemFilialDeEntidade(QrDreCfgCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmDreCfgCab.BtConfirmaClick(Sender: TObject);
var
  //Nome: String;
  Codigo, Empresa, RecVenProd, RecVenMerc, RecVenServ, DevVendas, TriVenICMS,
  TriVenPIS, TriVenCOFINS, TriVenIPI, TriDevICMS, TriDevPIS, TriDevCOFINS,
  TriDevIPI, CusFixVen: Integer;
  SQLType: TSQLType;
  Filial: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  //Nome           := EdNome.ValueVariant;
  Filial         := EdEmpresa.ValueVariant;
  Empresa        := DModG.QrEmpresasCodigo.Value;
  RecVenProd     := EdRecVenProd.ValueVariant;
  RecVenMerc     := EdRecVenMerc.ValueVariant;
  RecVenServ     := EdRecVenServ.ValueVariant;
  DevVendas      := EdDevVendas.ValueVariant;
  TriVenICMS     := EdTriVenICMS.ValueVariant;
  TriVenPIS      := EdTriVenPIS.ValueVariant;
  TriVenCOFINS   := EdTriVenCOFINS.ValueVariant;
  TriVenIPI      := EdTriVenIPI.ValueVariant;
  TriDevICMS     := EdTriDevICMS.ValueVariant;
  TriDevPIS      := EdTriDevPIS.ValueVariant;
  TriDevCOFINS   := EdTriDevCOFINS.ValueVariant;
  TriDevIPI      := EdTriDevIPI.ValueVariant;
  CusFixVen      := EdCusFixVen.ValueVariant;
  //
  //if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Defina a Empresa!') then Exit;
  //
  Codigo         := Empresa;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'drecfgcab', False, [
  //'Nome', 'Empresa',
  'RecVenProd',
  'RecVenMerc', 'RecVenServ', 'DevVendas',
  'TriVenICMS', 'TriVenPIS', 'TriVenCOFINS',
  'TriVenIPI', 'TriDevICMS', 'TriDevPIS',
  'TriDevCOFINS', 'TriDevIPI', 'CusFixVen'], [
  'Empresa'], [
  //Nome, Empresa,
  RecVenProd,
  RecVenMerc, RecVenServ, DevVendas,
  TriVenICMS, TriVenPIS, TriVenCOFINS,
  TriVenIPI, TriDevICMS, TriDevPIS,
  TriDevCOFINS, TriDevIPI, CusFixVen], [
  Empresa], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmDreCfgCab.BtCpcClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCpc, BtCpc);
end;

procedure TFmDreCfgCab.BtCtdClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCtd, BtCtd);
end;

procedure TFmDreCfgCab.BtDesisteClick(Sender: TObject);
(*
var
  Codigo : Integer;
*)
begin
(*
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'drecfgcab', Codigo);
*)
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
(*
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'drecfgcab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
*)
end;

procedure TFmDreCfgCab.BtEstClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEst, BtEst);
end;

procedure TFmDreCfgCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmDreCfgCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  PCDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  UnDmkDAC_PF.AbreQuery(QrRecVenProd, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrRecVenMerc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrRecVenServ, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDevVendas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTriVenICMS, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTriVenPIS, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTriVenCOFINS, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTriVenIPI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTriDevICMS, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTriDevPIS, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTriDevCOFINS, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTriDevIPI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCusFixVen, Dmod.MyDB);
end;

procedure TFmDreCfgCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrDreCfgCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmDreCfgCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmDreCfgCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrDreCfgCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmDreCfgCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmDreCfgCab.QrDreCfgCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmDreCfgCab.QrDreCfgCabAfterScroll(DataSet: TDataSet);
begin
  ReopenDreCfgEst(0);
end;

procedure TFmDreCfgCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrDreCfgCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmDreCfgCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrDreCfgCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'drecfgcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmDreCfgCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDreCfgCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrDreCfgCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'drecfgcab');
end;

procedure TFmDreCfgCab.CpcAltera1Click(Sender: TObject);
begin
  MostraFormDreCfgCpc(stUpd);
end;

procedure TFmDreCfgCab.CpcExclui1Click(Sender: TObject);
var
  IdxSub4: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item do plano de contas selecionado?',
  'DreCfgCpc', 'IdxSub4', QrDreCfgCpcIdxSub4.Value, Dmod.MyDB) = ID_YES then
  begin
    IdxSub4 := GOTOy.LocalizaPriorNextIntQr(QrDreCfgCpc,
      QrDreCfgCpcIdxSub4, QrDreCfgCpcIdxSub4.Value);
    ReopenDreCfgCpc(IdxSub4);
  end;
end;

procedure TFmDreCfgCab.CpcInclui1Click(Sender: TObject);
begin
  MostraFormDreCfgCpc(stIns);
end;

procedure TFmDreCfgCab.QrDreCfgCabBeforeClose(
  DataSet: TDataSet);
begin
  QrDreCfgEst.Close;
end;

procedure TFmDreCfgCab.QrDreCfgCabBeforeOpen(DataSet: TDataSet);
begin
  QrDreCfgCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmDreCfgCab.QrDreCfgCtdAfterScroll(DataSet: TDataSet);
begin
  ReopenDreCfgCpc(0);
end;

procedure TFmDreCfgCab.QrDreCfgCtdBeforeClose(DataSet: TDataSet);
begin
  QrDreCfgCpc.Close;
end;

procedure TFmDreCfgCab.QrDreCfgEstAfterScroll(DataSet: TDataSet);
begin
  ReopenDreCfgCtd(0);
end;

procedure TFmDreCfgCab.QrDreCfgEstBeforeClose(DataSet: TDataSet);
begin
  QrDreCfgCtd.Close;
end;

end.

