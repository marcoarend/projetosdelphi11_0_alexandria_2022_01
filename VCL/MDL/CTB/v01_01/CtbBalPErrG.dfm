object FmCtbBalPErrG: TFmCtbBalPErrG
  Left = 339
  Top = 185
  Caption = 'CTB-BALAN-004 :: Lan'#231'amentos de G'#234'nero Cont'#225'bil Invertidos'
  ClientHeight = 629
  ClientWidth = 1073
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1073
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1025
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 977
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 533
        Height = 32
        Caption = 'Lan'#231'amentos de G'#234'nero Cont'#225'bil Invertidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 533
        Height = 32
        Caption = 'Lan'#231'amentos de G'#234'nero Cont'#225'bil Invertidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 533
        Height = 32
        Caption = 'Lan'#231'amentos de G'#234'nero Cont'#225'bil Invertidos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1073
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1069
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1073
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 927
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 925
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtAltera: TBitBtn
        Tag = 11
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAlteraClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1073
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object DBGErrsG: TDBGrid
      Left = 0
      Top = 0
      Width = 1073
      Height = 467
      Align = alClient
      DataSource = DsErrsG
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGErrsGDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Lancamento'
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodWarning'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MsgWarning'
          Width = 584
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GenCtb'
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 24
    Top = 67
  end
  object QrErrsG: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrErrsGAfterOpen
    SQL.Strings = (
      'SELECT emi.Controle Lancamento, 1 CodWarning,'
      'emi.Genero, emi.GenCtb,  '
      
        '"Lan'#231'amento com conta cont'#225'bil no campo financeiro!" MsgWarning ' +
        ' '
      'FROM ctbbalplctemi  emi '
      'LEFT JOIN carteiras car ON car.Codigo=emi.Carteira '
      'LEFT JOIN contas    cta ON cta.Codigo=emi.Genero '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo '
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo '
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto '
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano '
      'WHERE  emi.Codigo=3 '
      'AND pla.FinContab<>3 '
      ' '
      'UNION '
      ' '
      'SELECT emi.Controle Lancamento, 2 CodWarning, '
      'emi.Genero, emi.GenCtb,  '
      '"Lan'#231'amento com conta finaneira no campo cont'#225'bil!" MsgWarning  '
      'FROM ctbbalplctemi  emi '
      'LEFT JOIN carteiras car ON car.Codigo=emi.Carteira '
      'LEFT JOIN contas    cta ON cta.Codigo=emi.GenCtb '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo '
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo '
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto '
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano '
      'WHERE emi.Codigo=3 '
      'AND emi.GenCtb <> 0 '
      'AND pla.FinContab=3 '
      ' '
      'UNION  '
      ' '
      'SELECT emi.Controle Lancamento, 3 CodWarning,  '
      'emi.Genero, emi.GenCtb,  '
      
        '"Lan'#231'amento com conta cont'#225'bil no campo financeiro!" MsgWarning ' +
        ' '
      'FROM ctbbalplctopn  emi '
      'LEFT JOIN carteiras car ON car.Codigo=emi.Carteira '
      'LEFT JOIN contas    cta ON cta.Codigo=emi.Genero '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo '
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo '
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto '
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano '
      'WHERE  emi.Codigo=3 '
      'AND pla.FinContab<>3 '
      ' '
      'UNION '
      ' '
      'SELECT emi.Controle Lancamento, 4 CodWarning, '
      'emi.Genero, emi.GenCtb,  '
      '"Lan'#231'amento com conta finaneira no campo cont'#225'bil!" MsgWarning  '
      'FROM ctbbalplctopn  emi '
      'LEFT JOIN carteiras car ON car.Codigo=emi.Carteira '
      'LEFT JOIN contas    cta ON cta.Codigo=emi.GenCtb '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo '
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo '
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto '
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano '
      'WHERE emi.Codigo=3 '
      'AND emi.GenCtb <> 0 '
      'AND pla.FinContab=3 '
      ''
      'UNION '
      ' '
      'SELECT emi.Controle Lancamento, 5 CodWarning, '
      'emi.Genero, emi.GenCtb,  '
      
        '"Lan'#231'amento Financeiro com Plano sem defini'#231#227'o Cont'#225'bil" MsgWarn' +
        'ing  '
      'FROM ctbbalplctopn  emi '
      'LEFT JOIN carteiras car ON car.Codigo=emi.Carteira '
      'LEFT JOIN contas    cta ON cta.Codigo=emi.Genero '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo '
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo '
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto '
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano '
      'WHERE emi.Codigo=3 '
      'AND pla.FinContab=0 '
      ' '
      'UNION '
      ' '
      'SELECT emi.Controle Lancamento, 6 CodWarning, '
      'emi.Genero, emi.GenCtb,  '
      
        '"Lan'#231'amento Cont'#225'bil com Plano sem defini'#231#227'o Cont'#225'bil" MsgWarnin' +
        'g  '
      'FROM ctbbalplctopn  emi '
      'LEFT JOIN carteiras car ON car.Codigo=emi.Carteira '
      'LEFT JOIN contas    cta ON cta.Codigo=emi.GenCtb '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo '
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo '
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto '
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano '
      'WHERE emi.Codigo=3 '
      'AND emi.GenCtb <> 0 '
      'AND pla.FinContab=0'
      ''
      'ORDER BY CodWarning, Genero, GenCtb'
      ' '
      ' ')
    Left = 156
    Top = 141
    object QrErrsGLancamento: TIntegerField
      FieldName = 'Lancamento'
      Required = True
    end
    object QrErrsGCodWarning: TLargeintField
      FieldName = 'CodWarning'
      Required = True
    end
    object QrErrsGMsgWarning: TWideStringField
      FieldName = 'MsgWarning'
      Required = True
      Size = 54
    end
    object QrErrsGGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrErrsGGenCtb: TIntegerField
      FieldName = 'GenCtb'
      Required = True
    end
  end
  object DsErrsG: TDataSource
    DataSet = QrErrsG
    Left = 156
    Top = 192
  end
end
