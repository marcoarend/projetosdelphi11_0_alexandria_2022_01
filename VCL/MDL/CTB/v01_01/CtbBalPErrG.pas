unit CtbBalPErrG;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, Variants, dmkDBGridZTO;

type
  TFmCtbBalPErrG = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAltera: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DBGErrsG: TDBGrid;
    QrErrsG: TMySQLQuery;
    QrErrsGLancamento: TIntegerField;
    QrErrsGCodWarning: TLargeintField;
    QrErrsGMsgWarning: TWideStringField;
    DsErrsG: TDataSource;
    QrErrsGGenero: TIntegerField;
    QrErrsGGenCtb: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrErrsGAfterOpen(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure DBGErrsGDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTabLctA: String;
  end;

  var
  FmCtbBalPErrG: TFmCtbBalPErrG;

implementation

uses UnMyObjects, MyDBCheck, DmkDAC_PF, Module, UnFinanceiro,
  Principal, ModuleLct2, LctGer2;

{$R *.DFM}

procedure TFmCtbBalPErrG.BtAlteraClick(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de G�nero Cont�bil';
  Prompt1 = 'Informe o G�nero Financeiro:';
  Prompt2 = 'Informe o G�nero Cont�bil:';
  Campo  = 'Descricao';
var
  vGenero, vGenCtb: Variant;
  N, Genero, GenCtb: Integer;
  //
  Tabela: String;
  //
  procedure AlteraAtual();
  begin
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'UPDATE ' + FTabLctA + ' SET ' +
    ' Genero=' + Geral.FF0(Genero) +
    ', GenCtb=' + Geral.FF0(GenCtb) +
    ' WHERE Controle=' + Geral.FF0(QrErrsGLancamento.Value));
    //
    if QrErrsGCodWarning.Value in ([1, 2]) then
      Tabela := 'ctbbalplctemi'
    else
      Tabela := 'ctbbalplctopn';
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'UPDATE ' + Tabela + ' SET ' +
    ' Genero=' + Geral.FF0(Genero) +
    ', GenCtb=' + Geral.FF0(GenCtb) +
    ' WHERE Controle=' + Geral.FF0(QrErrsGLancamento.Value));
    //
  end;
begin
  if DBGErrsG.SelectedRows.Count > 0 then
  begin
    if DBCheck.Escolhe2CodigoUnicos(vGenero, vGenCtb, Aviso, Titulo, Prompt1,
    Prompt2, nil, nil, nil, nil, Campo, Campo, QrErrsGGenero.Value, QrErrsGGenCtb.Value, [
    'SELECT co.Codigo, CONCAT( ',
    '  pl.Codigo, ".", ',
    '  LPAD(cj.Codigo, 2, "0"), ".", ',
    '  LPAD(gr.Codigo, 2, "0"), ".", ',
    '  LPAD(sg.Codigo, 3, "0"), ".", ',
    '  LPAD(co.Codigo, 4, "0"), "    -    ", co.Nome) ' + Campo,
    'FROM contas co ',
    'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo ',
    'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo ',
    'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto ',
    'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano ',
    'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa ',
    'WHERE co.Codigo>0 ',
    'AND pl.FinContab IN (3) ',
    'ORDER BY pl.Codigo, co.Nome ',
    ''],  [
    'SELECT co.Codigo, CONCAT( ',
    '  pl.Codigo, ".", ',
    '  LPAD(cj.Codigo, 2, "0"), ".", ',
    '  LPAD(gr.Codigo, 2, "0"), ".", ',
    '  LPAD(sg.Codigo, 3, "0"), ".", ',
    '  LPAD(co.Codigo, 4, "0"), "    -    ", co.Nome) ' + Campo,
    'FROM contas co ',
    'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo ',
    'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo ',
    'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto ',
    'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano ',
    'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa ',
    'WHERE co.Codigo>0 ',
    'AND pl.FinContab IN (1,2) ',
    'ORDER BY pl.Codigo, co.Nome ',
(*
    'SELECT Codigo, Nome ' + Campo,
    'FROM contas ',
    'WHERE Codigo > 0 ',
    'ORDER BY ' + Campo,
*)
    ''], Dmod.MyDB, True) then
    begin
      if (vGenero <> Null) and (vGenCtb <> Null) then
      begin
        Genero := Integer(vGenero);
        GenCtb := Integer(vGenCtb);
        with DBGErrsG.DataSource.DataSet do
        for N := 0 to DBGErrsG.SelectedRows.Count-1 do
        begin
          GotoBookmark(DBGErrsG.SelectedRows.Items[n]);
          AlteraAtual();
        end;
        UnDmkDAC_PF.AbreQuery(FmCtbBalPErrG.QrErrsG, Dmod.MyDB);
      end;
    end;
  end;
(*
  if QrErrsG.RecordCount > 0 then
  begin
    //Corda := MyObjects.CordaDeZTOChecks(DBGErrsG, QrErrsG, 'Controle', True);
    //
    if DBCheck.Escolhe2CodigoUnicos(vGenero, vGenCtb, Aviso, Titulo, Prompt1,
    Prompt2, nil, nil, nil, nil, Campo, Campo, QrErrsGGenero.Value, QrErrsGGenCtb.Value, [
    'SELECT Codigo, Nome ' + Campo,
    'FROM contas ',
    'WHERE Codigo > 0 ',
    'ORDER BY ' + Campo,
    ''],  [
    'SELECT Codigo, Nome ' + Campo,
    'FROM contas ',
    'WHERE Codigo > 0 ',
    'ORDER BY ' + Campo,
    ''], Dmod.MyDB, True) then
    begin
      if (vGenero <> Null) and (vGenCtb <> Null) then
      begin
        Genero := Integer(vGenero);
        GenCtb := Integer(vGenCtb);
        UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
        'UPDATE ' + FTabLctA + ' SET ' +
        ' Genero=' + Geral.FF0(Genero) +
        ', GenCtb=' + Geral.FF0(GenCtb) +
        ' WHERE Controle=' + Geral.FF0(QrErrsGLancamento.Value));
        //
        UnDmkDAC_PF.AbreQuery(FmCtbBalPErrG.QrErrsG, Dmod.MyDB);
      end;
    end;
  end;
*)
end;

procedure TFmCtbBalPErrG.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtbBalPErrG.DBGErrsGDblClick(Sender: TObject);
var
  DmLctX: TDataModule;
begin
  VAR_FORM_VerificaSeTabFormExiste_LOCALIZADO := nil;
  if MyObjects.VerificaSeTabFormExiste(VAR_PageControlFormsTabPrincipal, TFmLctGer2) > -1 then
  begin
    DmLctX := TFmLctGer2(VAR_FORM_VerificaSeTabFormExiste_LOCALIZADO).FModuleLctX;
    UFinanceiro.LocalizarLancamento((*TPDataIni*)nil, (*TPDataFim*)nil,
    (*RGTipoData.ItemIndex*)-1,
    TDmLct2(DmLctX).QrCrt, TDmLct2(DmLctX).QrLct, True, 0,
    FTabLctA, DmLctX, QrErrsGLancamento.Value, False);
  end else
    Geral.MB_Info('N�o h� guia de financeiro aberta na janela principal!');
end;

procedure TFmCtbBalPErrG.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCtbBalPErrG.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCtbBalPErrG.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtbBalPErrG.QrErrsGAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrErrsG.RecordCount > 0;
end;

end.
