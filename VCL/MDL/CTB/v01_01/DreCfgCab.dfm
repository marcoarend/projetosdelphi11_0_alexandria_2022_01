object FmDreCfgCab: TFmDreCfgCab
  Left = 368
  Top = 194
  Caption = 'DRE-CONFG-001 :: DRE - Configura'#231#227'o - Modelo'
  ClientHeight = 581
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 620
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 485
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label32: TLabel
        Left = 24
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit40
      end
      object DBEdit40: TDBEdit
        Left = 24
        Top = 32
        Width = 40
        Height = 21
        DataField = 'Empresa'
        DataSource = DsDreCfgCab
        TabOrder = 0
      end
      object DBEdit41: TDBEdit
        Left = 64
        Top = 32
        Width = 721
        Height = 21
        DataField = 'NO_Empresa'
        DataSource = DsDreCfgCab
        TabOrder = 1
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 421
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 186
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 360
        Top = 15
        Width = 646
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 513
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 1000350
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Modelo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtEst: TBitBtn
          Tag = 1000351
          Left = 126
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Estrutura'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtEstClick
        end
        object BtCtd: TBitBtn
          Tag = 1000352
          Left = 248
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'Conte&'#250'do'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtCtdClick
        end
        object BtCpc: TBitBtn
          Tag = 1000353
          Left = 370
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'Cont&as'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtCpcClick
        end
      end
    end
    object PCDados: TPageControl
      Left = 0
      Top = 65
      Width = 1008
      Height = 344
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Padr'#245'es'
        object Panel13: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 316
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label3: TLabel
            Left = 20
            Top = 8
            Width = 147
            Height = 13
            Caption = 'Receita de venda de produtos:'
            FocusControl = DBEdit1
          end
          object Label4: TLabel
            Left = 20
            Top = 32
            Width = 163
            Height = 13
            Caption = 'Receita de venda de mercadorias:'
            FocusControl = DBEdit2
          end
          object Label5: TLabel
            Left = 20
            Top = 56
            Width = 145
            Height = 13
            Caption = 'Receita de venda de servi'#231'os:'
            FocusControl = DBEdit3
          end
          object Label6: TLabel
            Left = 20
            Top = 80
            Width = 108
            Height = 13
            Caption = 'Devolu'#231#227'o de vendas:'
            FocusControl = DBEdit4
          end
          object Label8: TLabel
            Left = 20
            Top = 104
            Width = 82
            Height = 13
            Caption = 'ICMS de vendas:'
            FocusControl = DBEdit5
          end
          object Label10: TLabel
            Left = 20
            Top = 128
            Width = 73
            Height = 13
            Caption = 'PIS de vendas:'
            FocusControl = DBEdit6
          end
          object Label11: TLabel
            Left = 20
            Top = 152
            Width = 95
            Height = 13
            Caption = 'COFINS de vendas:'
            FocusControl = DBEdit7
          end
          object Label12: TLabel
            Left = 20
            Top = 176
            Width = 69
            Height = 13
            Caption = 'IPI de vendas:'
            FocusControl = DBEdit8
          end
          object Label13: TLabel
            Left = 20
            Top = 200
            Width = 155
            Height = 13
            Caption = 'ICMS de devolu'#231#245'es de vendas:'
            FocusControl = DBEdit9
          end
          object Label14: TLabel
            Left = 20
            Top = 224
            Width = 149
            Height = 13
            Caption = 'PIS  de devolu'#231#245'es de vendas:'
            FocusControl = DBEdit10
          end
          object Label15: TLabel
            Left = 20
            Top = 248
            Width = 168
            Height = 13
            Caption = 'COFINS de devolu'#231#245'es de vendas:'
            FocusControl = DBEdit11
          end
          object Label16: TLabel
            Left = 20
            Top = 272
            Width = 145
            Height = 13
            Caption = 'IPI  de devolu'#231#245'es de vendas:'
            FocusControl = DBEdit12
          end
          object Label17: TLabel
            Left = 20
            Top = 296
            Width = 102
            Height = 13
            Caption = 'Custo fixo de vendas:'
            FocusControl = DBEdit13
          end
          object DBEdit1: TDBEdit
            Left = 196
            Top = 4
            Width = 56
            Height = 21
            DataField = 'RecVenProd'
            DataSource = DsDreCfgCab
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 196
            Top = 28
            Width = 56
            Height = 21
            DataField = 'RecVenMerc'
            DataSource = DsDreCfgCab
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 196
            Top = 52
            Width = 56
            Height = 21
            DataField = 'RecVenServ'
            DataSource = DsDreCfgCab
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 196
            Top = 76
            Width = 56
            Height = 21
            DataField = 'DevVendas'
            DataSource = DsDreCfgCab
            TabOrder = 3
          end
          object DBEdit5: TDBEdit
            Left = 196
            Top = 100
            Width = 56
            Height = 21
            DataField = 'TriVenICMS'
            DataSource = DsDreCfgCab
            TabOrder = 4
          end
          object DBEdit6: TDBEdit
            Left = 196
            Top = 124
            Width = 56
            Height = 21
            DataField = 'TriVenPIS'
            DataSource = DsDreCfgCab
            TabOrder = 5
          end
          object DBEdit7: TDBEdit
            Left = 196
            Top = 148
            Width = 56
            Height = 21
            DataField = 'TriVenCOFINS'
            DataSource = DsDreCfgCab
            TabOrder = 6
          end
          object DBEdit8: TDBEdit
            Left = 196
            Top = 172
            Width = 56
            Height = 21
            DataField = 'TriVenIPI'
            DataSource = DsDreCfgCab
            TabOrder = 7
          end
          object DBEdit9: TDBEdit
            Left = 196
            Top = 196
            Width = 56
            Height = 21
            DataField = 'TriDevICMS'
            DataSource = DsDreCfgCab
            TabOrder = 8
          end
          object DBEdit10: TDBEdit
            Left = 196
            Top = 220
            Width = 56
            Height = 21
            DataField = 'TriDevPIS'
            DataSource = DsDreCfgCab
            TabOrder = 9
          end
          object DBEdit11: TDBEdit
            Left = 196
            Top = 244
            Width = 56
            Height = 21
            DataField = 'TriDevCOFINS'
            DataSource = DsDreCfgCab
            TabOrder = 10
          end
          object DBEdit12: TDBEdit
            Left = 196
            Top = 268
            Width = 56
            Height = 21
            DataField = 'TriDevIPI'
            DataSource = DsDreCfgCab
            TabOrder = 11
          end
          object DBEdit13: TDBEdit
            Left = 196
            Top = 292
            Width = 56
            Height = 21
            DataField = 'CusFixVen'
            DataSource = DsDreCfgCab
            TabOrder = 12
          end
          object DBEdit27: TDBEdit
            Left = 380
            Top = 4
            Width = 400
            Height = 21
            DataField = 'NO_RecVenProd'
            DataSource = DsDreCfgCab
            TabOrder = 13
          end
          object DBEdit28: TDBEdit
            Left = 380
            Top = 28
            Width = 400
            Height = 21
            DataField = 'NO_RecVenMerc'
            DataSource = DsDreCfgCab
            TabOrder = 14
          end
          object DBEdit29: TDBEdit
            Left = 380
            Top = 52
            Width = 400
            Height = 21
            DataField = 'NO_RecVenServ'
            DataSource = DsDreCfgCab
            TabOrder = 15
          end
          object DBEdit30: TDBEdit
            Left = 380
            Top = 76
            Width = 400
            Height = 21
            DataField = 'NO_DevVendas'
            DataSource = DsDreCfgCab
            TabOrder = 16
          end
          object DBEdit31: TDBEdit
            Left = 380
            Top = 100
            Width = 400
            Height = 21
            DataField = 'NO_TriVenICMS'
            DataSource = DsDreCfgCab
            TabOrder = 17
          end
          object DBEdit32: TDBEdit
            Left = 380
            Top = 124
            Width = 400
            Height = 21
            DataField = 'NO_TriVenPIS'
            DataSource = DsDreCfgCab
            TabOrder = 18
          end
          object DBEdit33: TDBEdit
            Left = 380
            Top = 148
            Width = 400
            Height = 21
            DataField = 'NO_TriVenCOFINS'
            DataSource = DsDreCfgCab
            TabOrder = 19
          end
          object DBEdit34: TDBEdit
            Left = 380
            Top = 172
            Width = 400
            Height = 21
            DataField = 'NO_TriVenIPI'
            DataSource = DsDreCfgCab
            TabOrder = 20
          end
          object DBEdit35: TDBEdit
            Left = 380
            Top = 196
            Width = 400
            Height = 21
            DataField = 'NO_TriDevICMS'
            DataSource = DsDreCfgCab
            TabOrder = 21
          end
          object DBEdit36: TDBEdit
            Left = 380
            Top = 220
            Width = 400
            Height = 21
            DataField = 'NO_TriDevPIS'
            DataSource = DsDreCfgCab
            TabOrder = 22
          end
          object DBEdit37: TDBEdit
            Left = 380
            Top = 244
            Width = 400
            Height = 21
            DataField = 'NO_TriDevCOFINS'
            DataSource = DsDreCfgCab
            TabOrder = 23
          end
          object DBEdit38: TDBEdit
            Left = 380
            Top = 268
            Width = 400
            Height = 21
            DataField = 'NO_TriDevIPI'
            DataSource = DsDreCfgCab
            TabOrder = 24
          end
          object DBEdit39: TDBEdit
            Left = 380
            Top = 291
            Width = 400
            Height = 21
            DataField = 'NO_CusFixVen'
            DataSource = DsDreCfgCab
            TabOrder = 25
          end
          object DBEdit14: TDBEdit
            Left = 256
            Top = 4
            Width = 120
            Height = 21
            DataField = 'ORD_RecVenProd'
            DataSource = DsDreCfgCab
            TabOrder = 26
          end
          object DBEdit15: TDBEdit
            Left = 256
            Top = 28
            Width = 120
            Height = 21
            DataField = 'ORD_RecVenMerc'
            DataSource = DsDreCfgCab
            TabOrder = 27
          end
          object DBEdit16: TDBEdit
            Left = 256
            Top = 52
            Width = 120
            Height = 21
            DataField = 'ORD_RecVenServ'
            DataSource = DsDreCfgCab
            TabOrder = 28
          end
          object DBEdit17: TDBEdit
            Left = 256
            Top = 76
            Width = 120
            Height = 21
            DataField = 'ORD_DevVendas'
            DataSource = DsDreCfgCab
            TabOrder = 29
          end
          object DBEdit18: TDBEdit
            Left = 256
            Top = 100
            Width = 120
            Height = 21
            DataField = 'ORD_TriVenICMS'
            DataSource = DsDreCfgCab
            TabOrder = 30
          end
          object DBEdit19: TDBEdit
            Left = 256
            Top = 124
            Width = 120
            Height = 21
            DataField = 'ORD_TriVenPIS'
            DataSource = DsDreCfgCab
            TabOrder = 31
          end
          object DBEdit20: TDBEdit
            Left = 256
            Top = 148
            Width = 120
            Height = 21
            DataField = 'ORD_TriVenCOFINS'
            DataSource = DsDreCfgCab
            TabOrder = 32
          end
          object DBEdit21: TDBEdit
            Left = 256
            Top = 172
            Width = 120
            Height = 21
            DataField = 'ORD_TriVenIPI'
            DataSource = DsDreCfgCab
            TabOrder = 33
          end
          object DBEdit22: TDBEdit
            Left = 256
            Top = 196
            Width = 120
            Height = 21
            DataField = 'ORD_TriDevICMS'
            DataSource = DsDreCfgCab
            TabOrder = 34
          end
          object DBEdit23: TDBEdit
            Left = 256
            Top = 220
            Width = 120
            Height = 21
            DataField = 'ORD_TriDevPIS'
            DataSource = DsDreCfgCab
            TabOrder = 35
          end
          object DBEdit24: TDBEdit
            Left = 256
            Top = 244
            Width = 120
            Height = 21
            DataField = 'ORD_TriDevCOFINS'
            DataSource = DsDreCfgCab
            TabOrder = 36
          end
          object DBEdit25: TDBEdit
            Left = 256
            Top = 268
            Width = 120
            Height = 21
            DataField = 'ORD_TriDevIPI'
            DataSource = DsDreCfgCab
            TabOrder = 37
          end
          object DBEdit26: TDBEdit
            Left = 256
            Top = 291
            Width = 120
            Height = 21
            DataField = 'ORD_CusFixVen'
            DataSource = DsDreCfgCab
            TabOrder = 38
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Itens'
        ImageIndex = 1
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 316
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitTop = 65
          ExplicitWidth = 1008
          ExplicitHeight = 226
          object Splitter1: TSplitter
            Left = 253
            Top = 0
            Width = 5
            Height = 200
            ExplicitLeft = 185
            ExplicitHeight = 226
          end
          object Splitter2: TSplitter
            Left = 557
            Top = 0
            Width = 5
            Height = 200
            ExplicitLeft = 556
            ExplicitHeight = 226
          end
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 253
            Height = 200
            Align = alLeft
            TabOrder = 0
            object DGDados: TDBGrid
              Left = 1
              Top = 21
              Width = 251
              Height = 178
              Align = alClient
              DataSource = DsDreCfgEst
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ordem'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 180
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end>
            end
            object Panel10: TPanel
              Left = 1
              Top = 1
              Width = 251
              Height = 20
              Align = alTop
              BevelOuter = bvNone
              Caption = 'Estrutura'
              TabOrder = 1
            end
          end
          object Panel8: TPanel
            Left = 562
            Top = 0
            Width = 438
            Height = 200
            Align = alClient
            TabOrder = 1
            object DBGCpc: TDBGrid
              Left = 1
              Top = 21
              Width = 436
              Height = 178
              Align = alClient
              DataSource = DsDreCfgCpc
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDrawColumnCell = DBGCpcDrawColumnCell
              Columns = <
                item
                  Expanded = False
                  FieldName = 'IDXSUB4'
                  Title.Caption = 'ID'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ordens'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 270
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'EXCLUSO'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Status'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PlaAllcad'
                  Title.Caption = 'Conta'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_MesCompet'
                  Title.Caption = 'M'#234's'
                  Visible = True
                end>
            end
            object Panel9: TPanel
              Left = 1
              Top = 1
              Width = 436
              Height = 20
              Align = alTop
              BevelOuter = bvNone
              Caption = 'Itens do plano de contas do conte'#250'do  selecionado'
              TabOrder = 1
            end
          end
          object Panel11: TPanel
            Left = 258
            Top = 0
            Width = 299
            Height = 200
            Align = alLeft
            TabOrder = 2
            object DBGrid2: TDBGrid
              Left = 1
              Top = 21
              Width = 297
              Height = 178
              Align = alClient
              DataSource = DsDreCfgCtd
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ordem'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 220
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Width = 40
                  Visible = True
                end>
            end
            object Panel12: TPanel
              Left = 1
              Top = 1
              Width = 297
              Height = 20
              Align = alTop
              BevelOuter = bvNone
              Caption = 'Conte'#250'do da estrutura selecionada'
              TabOrder = 1
            end
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 485
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 413
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label18: TLabel
        Left = 16
        Top = 64
        Width = 147
        Height = 13
        Caption = 'Receita de venda de produtos:'
      end
      object Label19: TLabel
        Left = 16
        Top = 88
        Width = 163
        Height = 13
        Caption = 'Receita de venda de mercadorias:'
      end
      object Label20: TLabel
        Left = 16
        Top = 112
        Width = 145
        Height = 13
        Caption = 'Receita de venda de servi'#231'os:'
      end
      object Label21: TLabel
        Left = 16
        Top = 136
        Width = 108
        Height = 13
        Caption = 'Devolu'#231#227'o de vendas:'
      end
      object Label22: TLabel
        Left = 16
        Top = 160
        Width = 82
        Height = 13
        Caption = 'ICMS de vendas:'
      end
      object Label23: TLabel
        Left = 16
        Top = 184
        Width = 73
        Height = 13
        Caption = 'PIS de vendas:'
      end
      object Label24: TLabel
        Left = 16
        Top = 208
        Width = 95
        Height = 13
        Caption = 'COFINS de vendas:'
      end
      object Label25: TLabel
        Left = 16
        Top = 232
        Width = 69
        Height = 13
        Caption = 'IPI de vendas:'
      end
      object Label26: TLabel
        Left = 16
        Top = 256
        Width = 155
        Height = 13
        Caption = 'ICMS de devolu'#231#245'es de vendas:'
      end
      object Label27: TLabel
        Left = 16
        Top = 280
        Width = 149
        Height = 13
        Caption = 'PIS  de devolu'#231#245'es de vendas:'
      end
      object Label28: TLabel
        Left = 16
        Top = 304
        Width = 168
        Height = 13
        Caption = 'COFINS de devolu'#231#245'es de vendas:'
      end
      object Label29: TLabel
        Left = 16
        Top = 328
        Width = 145
        Height = 13
        Caption = 'IPI  de devolu'#231#245'es de vendas:'
      end
      object Label30: TLabel
        Left = 16
        Top = 352
        Width = 102
        Height = 13
        Caption = 'Custo fixo de vendas:'
      end
      object Label31: TLabel
        Left = 16
        Top = 16
        Width = 23
        Height = 13
        Caption = 'Filial:'
      end
      object EdRecVenProd: TdmkEditCB
        Left = 192
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'RecVenProd'
        UpdCampo = 'RecVenProd'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBRecVenProd
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBRecVenProd: TdmkDBLookupComboBox
        Left = 248
        Top = 60
        Width = 565
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsRecVenProd
        TabOrder = 3
        dmkEditCB = EdRecVenProd
        QryCampo = 'RecVenProd'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdRecVenMerc: TdmkEditCB
        Left = 192
        Top = 84
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'RecVenMerc'
        UpdCampo = 'RecVenMerc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBRecVenMerc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBRecVenMerc: TdmkDBLookupComboBox
        Left = 248
        Top = 84
        Width = 565
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsRecVenMerc
        TabOrder = 5
        dmkEditCB = EdRecVenMerc
        QryCampo = 'RecVenMerc'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdRecVenServ: TdmkEditCB
        Left = 192
        Top = 108
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'RecVenServ'
        UpdCampo = 'RecVenServ'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBRecVenServ
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBRecVenServ: TdmkDBLookupComboBox
        Left = 248
        Top = 108
        Width = 565
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsRecVenServ
        TabOrder = 7
        dmkEditCB = EdRecVenServ
        QryCampo = 'RecVenServ'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CBTriVenPIS: TdmkDBLookupComboBox
        Left = 248
        Top = 180
        Width = 565
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTriVenPIS
        TabOrder = 13
        dmkEditCB = EdTriVenPIS
        QryCampo = 'TriVenPIS'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTriVenPIS: TdmkEditCB
        Left = 192
        Top = 180
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TriVenPIS'
        UpdCampo = 'TriVenPIS'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTriVenPIS
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdDevVendas: TdmkEditCB
        Left = 192
        Top = 132
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'DevVendas'
        UpdCampo = 'DevVendas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBDevVendas
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBDevVendas: TdmkDBLookupComboBox
        Left = 248
        Top = 132
        Width = 565
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsDevVendas
        TabOrder = 9
        dmkEditCB = EdDevVendas
        QryCampo = 'DevVendas'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTriVenICMS: TdmkEditCB
        Left = 192
        Top = 156
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TriVenICMS'
        UpdCampo = 'TriVenICMS'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTriVenICMS
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTriVenICMS: TdmkDBLookupComboBox
        Left = 248
        Top = 156
        Width = 565
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTriVenICMS
        TabOrder = 11
        dmkEditCB = EdTriVenICMS
        QryCampo = 'TriVenICMS'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTriVenCOFINS: TdmkEditCB
        Left = 192
        Top = 204
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TriVenCOFINS'
        UpdCampo = 'TriVenCOFINS'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTriVenCOFINS
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTriVenCOFINS: TdmkDBLookupComboBox
        Left = 248
        Top = 204
        Width = 565
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTriVenCOFINS
        TabOrder = 15
        dmkEditCB = EdTriVenCOFINS
        QryCampo = 'TriVenCOFINS'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTriVenIPI: TdmkEditCB
        Left = 192
        Top = 228
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TriVenIPI'
        UpdCampo = 'TriVenIPI'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CbTriVenIPI
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CbTriVenIPI: TdmkDBLookupComboBox
        Left = 248
        Top = 228
        Width = 565
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTriVenIPI
        TabOrder = 17
        dmkEditCB = EdTriVenIPI
        QryCampo = 'TriVenIPI'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTriDevICMS: TdmkEditCB
        Left = 192
        Top = 252
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TriDevICMS'
        UpdCampo = 'TriDevICMS'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTriDevICMS
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTriDevICMS: TdmkDBLookupComboBox
        Left = 248
        Top = 252
        Width = 565
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTriDevICMS
        TabOrder = 19
        dmkEditCB = EdTriDevICMS
        QryCampo = 'TriDevICMS'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTriDevPIS: TdmkEditCB
        Left = 192
        Top = 276
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 20
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TriDevPIS'
        UpdCampo = 'TriDevPIS'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTriDevPIS
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTriDevPIS: TdmkDBLookupComboBox
        Left = 248
        Top = 276
        Width = 565
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTriDevPIS
        TabOrder = 21
        dmkEditCB = EdTriDevPIS
        QryCampo = 'TriDevPIS'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTriDevCOFINS: TdmkEditCB
        Left = 192
        Top = 300
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 22
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TriDevCOFINS'
        UpdCampo = 'TriDevCOFINS'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTriDevCOFINS
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTriDevCOFINS: TdmkDBLookupComboBox
        Left = 248
        Top = 300
        Width = 565
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTriDevCOFINS
        TabOrder = 23
        dmkEditCB = EdTriDevCOFINS
        QryCampo = 'TriDevCOFINS'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTriDevIPI: TdmkEditCB
        Left = 192
        Top = 324
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 24
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TriDevIPI'
        UpdCampo = 'TriDevIPI'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CbTriDevIPI
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CbTriDevIPI: TdmkDBLookupComboBox
        Left = 248
        Top = 324
        Width = 565
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTriDevIPI
        TabOrder = 25
        dmkEditCB = EdTriDevIPI
        QryCampo = 'TriDevIPI'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEmpresa: TdmkEditCB
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 72
        Top = 32
        Width = 741
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCusFixVen: TdmkEditCB
        Left = 192
        Top = 348
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 26
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CusFixVen'
        UpdCampo = 'CusFixVen'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCusFixVen
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCusFixVen: TdmkDBLookupComboBox
        Left = 248
        Top = 348
        Width = 565
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCusFixVen
        TabOrder = 27
        dmkEditCB = EdCusFixVen
        QryCampo = 'CusFixVen'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 422
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 358
        Height = 32
        Caption = 'DRE - Configura'#231#227'o - Modelo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 358
        Height = 32
        Caption = 'DRE - Configura'#231#227'o - Modelo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 358
        Height = 32
        Caption = 'DRE - Configura'#231#227'o - Modelo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrDreCfgCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrDreCfgCabBeforeOpen
    AfterOpen = QrDreCfgCabAfterOpen
    BeforeClose = QrDreCfgCabBeforeClose
    AfterScroll = QrDreCfgCabAfterScroll
    SQL.Strings = (
      'SELECT IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_Empresa,'
      'dcc.*'
      'FROM drecfgcab dcc'
      'LEFT JOIN entidades emp ON emp.Codigo=dcc.Empresa'
      'WHERE dcc.Codigo > 0')
    Left = 608
    Top = 9
    object QrDreCfgCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDreCfgCabNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
    object QrDreCfgCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrDreCfgCabRecVenProd: TIntegerField
      FieldName = 'RecVenProd'
      Required = True
    end
    object QrDreCfgCabRecVenMerc: TIntegerField
      FieldName = 'RecVenMerc'
      Required = True
    end
    object QrDreCfgCabRecVenServ: TIntegerField
      FieldName = 'RecVenServ'
      Required = True
    end
    object QrDreCfgCabDevVendas: TIntegerField
      FieldName = 'DevVendas'
      Required = True
    end
    object QrDreCfgCabTriVenICMS: TIntegerField
      FieldName = 'TriVenICMS'
      Required = True
    end
    object QrDreCfgCabTriVenPIS: TIntegerField
      FieldName = 'TriVenPIS'
      Required = True
    end
    object QrDreCfgCabTriVenCOFINS: TIntegerField
      FieldName = 'TriVenCOFINS'
      Required = True
    end
    object QrDreCfgCabTriVenIPI: TIntegerField
      FieldName = 'TriVenIPI'
      Required = True
    end
    object QrDreCfgCabTriDevICMS: TIntegerField
      FieldName = 'TriDevICMS'
      Required = True
    end
    object QrDreCfgCabTriDevPIS: TIntegerField
      FieldName = 'TriDevPIS'
      Required = True
    end
    object QrDreCfgCabTriDevCOFINS: TIntegerField
      FieldName = 'TriDevCOFINS'
      Required = True
    end
    object QrDreCfgCabTriDevIPI: TIntegerField
      FieldName = 'TriDevIPI'
      Required = True
    end
    object QrDreCfgCabCusFixVen: TIntegerField
      FieldName = 'CusFixVen'
      Required = True
    end
    object QrDreCfgCabNO_RecVenProd: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_RecVenProd'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'RecVenProd'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabNO_RecVenMerc: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_RecVenMerc'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'RecVenMerc'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabNO_RecVenServ: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_RecVenServ'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'RecVenServ'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabNO_DevVendas: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_DevVendas'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'DevVendas'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabNO_TriVenICMS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_TriVenICMS'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'TriVenICMS'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabNO_TriVenPIS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_TriVenPIS'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'TriVenPIS'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabNO_TriVenCOFINS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_TriVenCOFINS'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'TriVenCOFINS'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabNO_TriVenIPI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_TriVenIPI'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'TriVenIPI'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabNO_TriDevICMS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_TriDevICMS'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'TriDevICMS'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabNO_TriDevPIS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_TriDevPIS'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'TriDevPIS'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabNO_TriDevCOFINS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_TriDevCOFINS'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'TriDevCOFINS'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabNO_TriDevIPI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_TriDevIPI'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'TriDevIPI'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabNO_CusFixVen: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_CusFixVen'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'CusFixVen'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabORD_RecVenProd: TWideStringField
      FieldKind = fkLookup
      FieldName = 'ORD_RecVenProd'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Ordens'
      KeyFields = 'RecVenProd'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabORD_RecVenMerc: TWideStringField
      FieldKind = fkLookup
      FieldName = 'ORD_RecVenMerc'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Ordens'
      KeyFields = 'RecVenMerc'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabORD_RecVenServ: TWideStringField
      FieldKind = fkLookup
      FieldName = 'ORD_RecVenServ'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Ordens'
      KeyFields = 'RecVenServ'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabORD_DevVendas: TWideStringField
      FieldKind = fkLookup
      FieldName = 'ORD_DevVendas'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Ordens'
      KeyFields = 'DevVendas'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabORD_TriVenICMS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'ORD_TriVenICMS'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Ordens'
      KeyFields = 'TriVenICMS'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabORD_TriVenPIS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'ORD_TriVenPIS'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Ordens'
      KeyFields = 'TriVenPIS'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabORD_TriVenCOFINS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'ORD_TriVenCOFINS'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Ordens'
      KeyFields = 'TriVenCOFINS'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabORD_TriVenIPI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'ORD_TriVenIPI'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Ordens'
      KeyFields = 'TriVenIPI'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabORD_TriDevICMS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'ORD_TriDevICMS'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Ordens'
      KeyFields = 'TriDevICMS'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabORD_TriDevPIS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'ORD_TriDevPIS'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Ordens'
      KeyFields = 'TriDevPIS'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabORD_TriDevCOFINS: TWideStringField
      FieldKind = fkLookup
      FieldName = 'ORD_TriDevCOFINS'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Ordens'
      KeyFields = 'TriDevCOFINS'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabORD_TriDevIPI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'ORD_TriDevIPI'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Ordens'
      KeyFields = 'TriDevIPI'
      Size = 120
      Lookup = True
    end
    object QrDreCfgCabORD_CusFixVen: TWideStringField
      FieldKind = fkLookup
      FieldName = 'ORD_CusFixVen'
      LookupDataSet = QrPlaAllCad
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Ordens'
      KeyFields = 'CusFixVen'
      Size = 120
      Lookup = True
    end
  end
  object DsDreCfgCab: TDataSource
    DataSet = QrDreCfgCab
    Left = 608
    Top = 61
  end
  object QrDreCfgCtd: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrDreCfgCtdBeforeClose
    AfterScroll = QrDreCfgCtdAfterScroll
    SQL.Strings = (
      'SELECT * FROM drecfgctd'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 804
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDreCfgCtdCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDreCfgCtdControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDreCfgCtdConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrDreCfgCtdNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrDreCfgCtdDreCfgTp2: TIntegerField
      FieldName = 'DreCfgTp2'
      Required = True
    end
    object QrDreCfgCtdOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
  end
  object DsDreCfgCtd: TDataSource
    DataSet = QrDreCfgCtd
    Left = 804
    Top = 61
  end
  object PMCtd: TPopupMenu
    OnPopup = PMCtdPopup
    Left = 872
    Top = 232
    object CtdInclui1: TMenuItem
      Caption = '&Adiciona conte'#250'do'
      Enabled = False
      OnClick = CtdInclui1Click
    end
    object CtdAltera1: TMenuItem
      Caption = '&Edita o conte'#250'do selecionado'
      Enabled = False
      OnClick = CtdAltera1Click
    end
    object CtdExclui1: TMenuItem
      Caption = '&Remove o conte'#250'do selecionado'
      Enabled = False
      OnClick = CtdExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 140
    Top = 512
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrDreCfgCpc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT CAST(IF(cpc.Conta=5, cpc.IdxSub4, NULL) AS SIGNED) IDXSUB' +
        '4,'
      'CAST(IF(cpc.Conta=5, cpc.Excluso, NULL) AS SIGNED) EXCLUSO,'
      'IF(cpc.MesCompet IS NULL, 0, cpc.MesCompet) MesCompet,'
      'IF(cpc.MesCompet = 1, "S", "E") NO_MesCompet,'
      'IF(pac.Codigo IN (761), 1,'
      '  IF(pac.Codigo IN (-999999999), 2,'
      '  IF(pac.CodNiv5 IN (-999999999), 4,'
      '  IF(pac.CodNiv5 IN (761), 3,'
      '  IF(pac.CodNiv4 IN (-999999999), 4,'
      '  IF(pac.CodNiv4 IN (761), 3,'
      '  IF(pac.CodNiv3 IN (-999999999), 4,'
      '  IF(pac.CodNiv3 IN (761), 3,'
      '  IF(pac.CodNiv2 IN (-999999999), 4,'
      '  IF(pac.CodNiv2 IN (761), 3,'
      '  IF(pac.CodNiv1 IN (-999999999), 4,'
      '  IF(pac.CodNiv1 IN (761), 3,'
      '  9))))))))))))'
      'Status,'
      'pac.Codigo PlaAllcad, pac.Nome, pac.Ordens'
      'FROM plaallcad pac'
      'LEFT JOIN drecfgcpc cpc ON pac.Codigo=cpc.PlaAllCad'
      'WHERE'
      '('
      '  pac.Codigo IN (761)'
      '  OR pac.CodNiv1 IN (761)'
      '  OR pac.CodNiv2 IN (761)'
      '  OR pac.CodNiv3 IN (761)'
      '  OR pac.CodNiv4 IN (761)'
      '  OR pac.CodNiv5 IN (761)'
      ')'
      'AND ('
      '  (cpc.Conta=5)'
      '  OR (cpc.Conta IS NULL)'
      '  OR (cpc.Conta<>5 AND cpc.Codigo <> 4'
      '    AND NOT (pac.Codigo IN (761)))'
      ')'
      'ORDER BY Ordens')
    Left = 892
    Top = 9
    object QrDreCfgCpcIDXSUB4: TLargeintField
      FieldName = 'IDXSUB4'
    end
    object QrDreCfgCpcEXCLUSO: TIntegerField
      FieldName = 'EXCLUSO'
    end
    object QrDreCfgCpcStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object QrDreCfgCpcPlaAllcad: TIntegerField
      FieldName = 'PlaAllcad'
      Required = True
    end
    object QrDreCfgCpcNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrDreCfgCpcOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
    object QrDreCfgCpcMesCompet: TIntegerField
      FieldName = 'MesCompet'
    end
    object QrDreCfgCpcNO_MesCompet: TWideStringField
      FieldName = 'NO_MesCompet'
      Required = True
      Size = 1
    end
  end
  object DsDreCfgCpc: TDataSource
    DataSet = QrDreCfgCpc
    Left = 892
    Top = 61
  end
  object PMCpc: TPopupMenu
    OnPopup = PMCpcPopup
    Left = 884
    Top = 380
    object CpcInclui1: TMenuItem
      Caption = '&Adiciona Conta'
      OnClick = CpcInclui1Click
    end
    object CpcAltera1: TMenuItem
      Caption = '&Edita a conta selecionada'
      OnClick = CpcAltera1Click
    end
    object CpcExclui1: TMenuItem
      Caption = '&Remove a conta selecionada'
      OnClick = CpcExclui1Click
    end
  end
  object PMEst: TPopupMenu
    OnPopup = PMEstPopup
    Left = 848
    Top = 168
    object EstInclui1: TMenuItem
      Caption = '&Inclui item de estrutura'
      OnClick = EstInclui1Click
    end
    object EstAltera1: TMenuItem
      Caption = '&Altera item de estrutura selecionado'
      OnClick = EstAltera1Click
    end
    object EstExclui1: TMenuItem
      Caption = '&Exclui item de estrutura selecionado'
      OnClick = EstExclui1Click
    end
  end
  object QrDreCfgEst: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrDreCfgEstBeforeClose
    AfterScroll = QrDreCfgEstAfterScroll
    SQL.Strings = (
      'SELECT * FROM drecfgest'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 704
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDreCfgEstCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDreCfgEstControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDreCfgEstNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrDreCfgEstDreCfgTp1: TIntegerField
      FieldName = 'DreCfgTp1'
      Required = True
    end
    object QrDreCfgEstOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
  end
  object DsDreCfgEst: TDataSource
    DataSet = QrDreCfgEst
    Left = 700
    Top = 61
  end
  object QrDCC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cpc.PlaAllCad, cpc.Excluso'
      'FROM drecfgcpc cpc'
      'WHERE cpc.Controle=:P0')
    Left = 898
    Top = 177
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDCCPlaAllCad: TIntegerField
      FieldName = 'PlaAllCad'
      Required = True
    end
    object QrDCCExcluso: TIntegerField
      FieldName = 'Excluso'
      Required = True
    end
  end
  object QrPlaAllCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM plaallcad')
    Left = 894
    Top = 121
    object QrPlaAllCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPlaAllCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrPlaAllCadOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object QrRecVenProd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ordens'
      'FROM plaallcad')
    Left = 534
    Top = 149
    object QrRecVenProdCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRecVenProdNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrRecVenProdOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object QrRecVenMerc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ordens'
      'FROM plaallcad')
    Left = 594
    Top = 173
    object QrRecVenMercCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRecVenMercNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrRecVenMercOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object QrRecVenServ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ordens'
      'FROM plaallcad')
    Left = 536
    Top = 196
    object QrRecVenServCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRecVenServNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrRecVenServOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object QrDevVendas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ordens'
      'FROM plaallcad')
    Left = 602
    Top = 221
    object QrDevVendasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDevVendasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrDevVendasOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object QrTriVenPIS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ordens'
      'FROM plaallcad')
    Left = 604
    Top = 268
    object QrTriVenPISCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTriVenPISNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrTriVenPISOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object QrTriVenICMS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ordens'
      'FROM plaallcad')
    Left = 538
    Top = 245
    object QrTriVenICMSCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTriVenICMSNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrTriVenICMSOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object QrTriVenCOFINS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ordens'
      'FROM plaallcad')
    Left = 538
    Top = 293
    object QrTriVenCOFINSCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTriVenCOFINSNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrTriVenCOFINSOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object QrTriVenIPI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ordens'
      'FROM plaallcad')
    Left = 598
    Top = 317
    object QrTriVenIPICodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTriVenIPINome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrTriVenIPIOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object QrTriDevICMS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ordens'
      'FROM plaallcad')
    Left = 538
    Top = 341
    object QrTriDevICMSCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTriDevICMSNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrTriDevICMSOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object QrTriDevPIS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ordens'
      'FROM plaallcad')
    Left = 604
    Top = 364
    object QrTriDevPISCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTriDevPISNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrTriDevPISOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object QrTriDevCOFINS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ordens'
      'FROM plaallcad')
    Left = 538
    Top = 389
    object QrTriDevCOFINSCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTriDevCOFINSNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrTriDevCOFINSOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object QrTriDevIPI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ordens'
      'FROM plaallcad')
    Left = 598
    Top = 413
    object QrTriDevIPICodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTriDevIPINome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrTriDevIPIOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object QrCusFixVen: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Ordens'
      'FROM plaallcad')
    Left = 538
    Top = 437
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object WideStringField1: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object WideStringField2: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
  end
  object DsRecVenProd: TDataSource
    DataSet = QrRecVenProd
    Left = 464
    Top = 148
  end
  object DsRecVenMerc: TDataSource
    DataSet = QrRecVenMerc
    Left = 680
    Top = 172
  end
  object DsRecVenServ: TDataSource
    DataSet = QrRecVenServ
    Left = 464
    Top = 196
  end
  object DsDevVendas: TDataSource
    DataSet = QrDevVendas
    Left = 680
    Top = 220
  end
  object DsTriVenICMS: TDataSource
    DataSet = QrTriVenICMS
    Left = 464
    Top = 244
  end
  object DsTriVenPIS: TDataSource
    DataSet = QrTriVenPIS
    Left = 680
    Top = 268
  end
  object DsTriVenCOFINS: TDataSource
    DataSet = QrTriVenCOFINS
    Left = 464
    Top = 292
  end
  object DsTriVenIPI: TDataSource
    DataSet = QrTriVenIPI
    Left = 680
    Top = 316
  end
  object DsTriDevICMS: TDataSource
    DataSet = QrTriDevICMS
    Left = 464
    Top = 340
  end
  object DsTriDevPIS: TDataSource
    DataSet = QrTriDevPIS
    Left = 680
    Top = 364
  end
  object DsTriDevCOFINS: TDataSource
    DataSet = QrTriDevCOFINS
    Left = 464
    Top = 388
  end
  object DsTriDevIPI: TDataSource
    DataSet = QrTriDevIPI
    Left = 680
    Top = 412
  end
  object DsCusFixVen: TDataSource
    DataSet = QrCusFixVen
    Left = 464
    Top = 440
  end
end
