object FmCtbBalPLctEdit: TFmCtbBalPLctEdit
  Left = 339
  Top = 185
  Caption = 'CTB-BALAN-003 :: Edi'#231#227'o de Lan'#231'amentos Orf'#227'os de G'#234'nero Cont'#225'bil'
  ClientHeight = 629
  ClientWidth = 1073
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1073
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1025
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 977
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 625
        Height = 32
        Caption = 'Edi'#231#227'o de Lan'#231'amentos Orf'#227'os de G'#234'nero Cont'#225'bil'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 625
        Height = 32
        Caption = 'Edi'#231#227'o de Lan'#231'amentos Orf'#227'os de G'#234'nero Cont'#225'bil'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 625
        Height = 32
        Caption = 'Edi'#231#227'o de Lan'#231'amentos Orf'#227'os de G'#234'nero Cont'#225'bil'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1073
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1069
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1073
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 927
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 925
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtAltera: TBitBtn
        Tag = 11
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAlteraClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1073
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object DBGOrfaos: TdmkDBGridZTO
      Left = 0
      Top = 0
      Width = 1073
      Height = 467
      Align = alClient
      DataSource = DsOrfaos
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'Cta Fin'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Cta'
          Title.Caption = 'Nome conta financeiro'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Compensado'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ENT'
          Title.Caption = 'Terceiro'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Carteira'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CARTEIRA'
          Title.Caption = 'Nome Carteira'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Subgrupo'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Sgr'
          Title.Caption = 'Descri'#231#227'o Subgrupo'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Grupo'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Gru'
          Title.Caption = 'Descri'#231#227'o Grupo'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conjunto'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Cjt'
          Title.Caption = 'Descri'#231#227'o Conjunto'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Plano'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Pla'
          Title.Caption = 'Descri'#231#227'o Plano'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pago'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Tipo'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sit'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtDtPg'
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 16
    Top = 27
  end
  object QrOrfaos: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrOrfaosAfterOpen
    SQL.Strings = (
      'SELECT cjt.Plano, gru.Conjunto, sgr.Grupo, '
      'cta.Subgrupo, lct.Genero, cta.Nome NO_Cta, '
      'lct.Data, lct.Tipo, lct.Carteira, lct.Controle, '
      'lct.Sub, lct.Debito, lct.Credito, '
      'lct.Compensado, lct.Sit, lct.Vencimento, lct.ID_Pgto, '
      'lct.ID_Quit, lct.ID_Sub, lct.Pago, lct.GenCtb, '
      'lct.QtDtPg, lct.PagMul, lct.PagJur , lct.RecDes,'
      'pla.Nome NO_Pla, cjt.Nome NO_Cjt, '
      'gru.Nome NO_Gru, sgr.Nome NO_Sgr,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'crt.Nome NO_CARTEIRA'
      'FROM bluederm_cialeather.lct0001a lct  '
      'LEFT JOIN bluederm_cialeather.entidades ent ON ent.Codigo='
      '  IF(lct.Cliente <> 0, lct.Cliente, lct.Fornecedor)'
      
        'LEFT JOIN bluederm_cialeather.carteiras crt ON crt.Codigo=lct.Ca' +
        'rteira '
      
        'LEFT JOIN bluederm_cialeather.contas    cta ON cta.Codigo=lct.Ge' +
        'nero '
      
        'LEFT JOIN bluederm_cialeather.subgrupos sgr ON sgr.Codigo=cta.Su' +
        'bGrupo '
      
        'LEFT JOIN bluederm_cialeather.grupos    gru ON gru.Codigo=sgr.Gr' +
        'upo '
      
        'LEFT JOIN bluederm_cialeather.conjuntos cjt ON cjt.Codigo=gru.Co' +
        'njunto '
      
        'LEFT JOIN bluederm_cialeather.plano     pla ON pla.Codigo=cjt.Pl' +
        'ano '
      'WHERE crt.ForneceI=-11'
      'AND lct.Genero > 0 '
      'AND ( '
      '  lct.Tipo = 2  '
      '  AND lct.Data < "2021-01-01"  '
      '  AND (  '
      '    lct.Sit < 2  '
      '    OR  '
      '    lct.Compensado >= "2021-01-01"  '
      '  ) '
      '  OR ( '
      '  lct.Tipo < 2 '
      '  )'
      ') '
      'AND lct.GenCtb = 0 '
      'ORDER BY NO_Cta, NO_ENT')
    Left = 272
    Top = 73
    object QrOrfaosPlano: TIntegerField
      FieldName = 'Plano'
    end
    object QrOrfaosConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object QrOrfaosGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrOrfaosSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
    end
    object QrOrfaosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrOrfaosNO_Cta: TWideStringField
      FieldName = 'NO_Cta'
      Size = 50
    end
    object QrOrfaosData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOrfaosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrOrfaosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrOrfaosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOrfaosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrOrfaosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOrfaosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOrfaosCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOrfaosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrOrfaosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOrfaosID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrOrfaosID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrOrfaosID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrOrfaosPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOrfaosGenCtb: TIntegerField
      FieldName = 'GenCtb'
      Required = True
    end
    object QrOrfaosQtDtPg: TIntegerField
      FieldName = 'QtDtPg'
      Required = True
    end
    object QrOrfaosPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOrfaosPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOrfaosRecDes: TFloatField
      FieldName = 'RecDes'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOrfaosNO_Pla: TWideStringField
      FieldName = 'NO_Pla'
      Size = 50
    end
    object QrOrfaosNO_Cjt: TWideStringField
      FieldName = 'NO_Cjt'
      Size = 50
    end
    object QrOrfaosNO_Gru: TWideStringField
      FieldName = 'NO_Gru'
      Size = 50
    end
    object QrOrfaosNO_Sgr: TWideStringField
      FieldName = 'NO_Sgr'
      Size = 50
    end
    object QrOrfaosNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrOrfaosNO_CARTEIRA: TWideStringField
      FieldName = 'NO_CARTEIRA'
      Size = 100
    end
  end
  object DsOrfaos: TDataSource
    DataSet = QrOrfaos
    Left = 272
    Top = 120
  end
end
