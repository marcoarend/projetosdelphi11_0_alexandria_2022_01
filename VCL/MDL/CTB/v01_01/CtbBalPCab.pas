unit CtbBalPCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  frxClass, frxDBSet;

type
  TArr2Double = array of array[0..1] of double;
  //
  TFmCtbBalPCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrCtbBalPCab: TMySQLQuery;
    DsCtbBalPCab: TDataSource;
    QrCtbBalPIts: TMySQLQuery;
    DsCtbBalPIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label3: TLabel;
    CBMes: TComboBox;
    Label4: TLabel;
    CBAno: TComboBox;
    QrCtbBalPCabCodigo: TIntegerField;
    QrCtbBalPCabEmpresa: TIntegerField;
    QrCtbBalPCabAnoMes: TIntegerField;
    QrCtbBalPCabNome: TWideStringField;
    QrCtbBalPCabNO_EMPRESA: TWideStringField;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    N1: TMenuItem;
    Recriaitens1: TMenuItem;
    PB1: TProgressBar;
    QrCtbBalPItsNO_CtaPlaA: TWideStringField;
    QrCtbBalPItsNO_CtaPlaB: TWideStringField;
    QrCtbBalPItsNO_TpBalanc: TWideStringField;
    QrCtbBalPItsCodigo: TIntegerField;
    QrCtbBalPItsControle: TIntegerField;
    QrCtbBalPItsCredito: TFloatField;
    QrCtbBalPItsDebito: TFloatField;
    QrCtbBalPItsLk: TIntegerField;
    QrCtbBalPItsDataCad: TDateField;
    QrCtbBalPItsDataAlt: TDateField;
    QrCtbBalPItsUserCad: TIntegerField;
    QrCtbBalPItsUserAlt: TIntegerField;
    QrCtbBalPItsAlterWeb: TSmallintField;
    QrCtbBalPItsAWServerID: TIntegerField;
    QrCtbBalPItsAWStatSinc: TSmallintField;
    QrCtbBalPItsAtivo: TSmallintField;
    QrCtbBalPItsAPlaCod: TIntegerField;
    QrCtbBalPItsACjtCod: TIntegerField;
    QrCtbBalPItsAGruCod: TIntegerField;
    QrCtbBalPItsASgrCod: TIntegerField;
    QrCtbBalPItsACtaCod: TIntegerField;
    QrCtbBalPItsBPlaCod: TIntegerField;
    QrCtbBalPItsBCjtCod: TIntegerField;
    QrCtbBalPItsBGruCod: TIntegerField;
    QrCtbBalPItsBSgrCod: TIntegerField;
    QrCtbBalPItsBCtaCod: TIntegerField;
    QrCtbBalPItsAPlaTxt: TWideStringField;
    QrCtbBalPItsACjtTxt: TWideStringField;
    QrCtbBalPItsAGruTxt: TWideStringField;
    QrCtbBalPItsASgrTxt: TWideStringField;
    QrCtbBalPItsACtaTxt: TWideStringField;
    QrCtbBalPItsBPlaTxt: TWideStringField;
    QrCtbBalPItsBCjtTxt: TWideStringField;
    QrCtbBalPItsBGruTxt: TWideStringField;
    QrCtbBalPItsBSgrTxt: TWideStringField;
    QrCtbBalPItsBCtaTxt: TWideStringField;
    QrAux: TMySQLQuery;
    QrSoma: TMySQLQuery;
    QrSomaCredito: TFloatField;
    QrSomaDebito: TFloatField;
    QrSomaMulta: TFloatField;
    QrSomaJuros: TFloatField;
    QrSomaDesconto: TFloatField;
    QrSomaQtDtPg: TLargeintField;
    QrSomaValLiq: TFloatField;
    QrOrfaos: TMySQLQuery;
    QrLcts: TMySQLQuery;
    QrLctsCredito: TFloatField;
    QrLctsDebito: TFloatField;
    QrLctsAPlaCod: TIntegerField;
    QrLctsACjtCod: TIntegerField;
    QrLctsAGruCod: TIntegerField;
    QrLctsASgrCod: TIntegerField;
    QrLctsACtaCod: TIntegerField;
    QrLctsBPlaCod: TIntegerField;
    QrLctsBCjtCod: TIntegerField;
    QrLctsBGruCod: TIntegerField;
    QrLctsBSgrCod: TIntegerField;
    QrLctsBCtaCod: TIntegerField;
    QrLctsAPlaTxt: TWideStringField;
    QrLctsACjtTxt: TWideStringField;
    QrLctsAGruTxt: TWideStringField;
    QrLctsASgrTxt: TWideStringField;
    QrLctsACtaTxt: TWideStringField;
    QrLctsBPlaTxt: TWideStringField;
    QrLctsBCjtTxt: TWideStringField;
    QrLctsBGruTxt: TWideStringField;
    QrLctsBSgrTxt: TWideStringField;
    QrLctsBCtaTxt: TWideStringField;
    QrCtbBalPItsNO_TpAmbito: TWideStringField;
    QrAu2: TMySQLQuery;
    QrCrts: TMySQLQuery;
    QrCrtsBPlaCod: TIntegerField;
    QrCrtsBCjtCod: TIntegerField;
    QrCrtsBGruCod: TIntegerField;
    QrCrtsBSgrCod: TIntegerField;
    QrCrtsBCtaCod: TIntegerField;
    QrCrtsBPlaTxt: TWideStringField;
    QrCrtsBCjtTxt: TWideStringField;
    QrCrtsBGruTxt: TWideStringField;
    QrCrtsBSgrTxt: TWideStringField;
    QrCrtsBCtaTxt: TWideStringField;
    QrCrtsCarteira: TIntegerField;
    odosrecriveis1: TMenuItem;
    N2: TMenuItem;
    Soslanamentosdofinanceiro1: TMenuItem;
    Sossaldosdascarteiras1: TMenuItem;
    QrCtbBalPItsTpBalanc: TSmallintField;
    QrCtbBalPItsTpAmbito: TSmallintField;
    QrCtbBalPItsNiveisTXT: TWideStringField;
    frxCTB_BALAN_001_01: TfrxReport;
    frxReport1: TfrxReport;
    frxDsCtbBalPIts: TfrxDBDataset;
    QrCtbBalPItsXPlaTxt: TWideStringField;
    QrCtbBalPItsXCjtTxt: TWideStringField;
    QrCtbBalPItsXGruTxt: TWideStringField;
    QrCtbBalPItsXSgrTxt: TWideStringField;
    QrCtbBalPItsXCtaTxt: TWideStringField;
    SossaldosdasCarteirasTipoEmisso1: TMenuItem;
    SossaldosdasCarteiras2: TMenuItem;
    PMImprime: TPopupMenu;
    Balancete1: TMenuItem;
    Balano1: TMenuItem;
    Soslanamentosemitidosnoperodo1: TMenuItem;
    TodoBalanco1: TMenuItem;
    N3: TMenuItem;
    Lanamentosfinanceirosabertos1: TMenuItem;
    SaldosdeCarteiras1: TMenuItem;
    Lanamentosfinanceirosemitidos1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DGDados: TDBGrid;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    QrCtbBalPLctOpn: TMySQLQuery;
    DsCtbBalPLctOpn: TDataSource;
    QrCtbBalPLctOpnCodigo: TIntegerField;
    QrCtbBalPLctOpnData: TDateField;
    QrCtbBalPLctOpnTipo: TSmallintField;
    QrCtbBalPLctOpnCarteira: TIntegerField;
    QrCtbBalPLctOpnControle: TIntegerField;
    QrCtbBalPLctOpnSub: TSmallintField;
    QrCtbBalPLctOpnGenero: TIntegerField;
    QrCtbBalPLctOpnDebito: TFloatField;
    QrCtbBalPLctOpnCredito: TFloatField;
    QrCtbBalPLctOpnCompensado: TDateField;
    QrCtbBalPLctOpnSit: TIntegerField;
    QrCtbBalPLctOpnVencimento: TDateField;
    QrCtbBalPLctOpnID_Pgto: TIntegerField;
    QrCtbBalPLctOpnID_Quit: TIntegerField;
    QrCtbBalPLctOpnID_Sub: TSmallintField;
    QrCtbBalPLctOpnPago: TFloatField;
    QrCtbBalPLctOpnGenCtb: TIntegerField;
    QrCtbBalPLctOpnQtDtPg: TIntegerField;
    QrCtbBalPLctOpnLk: TIntegerField;
    QrCtbBalPLctOpnDataCad: TDateField;
    QrCtbBalPLctOpnDataAlt: TDateField;
    QrCtbBalPLctOpnUserCad: TIntegerField;
    QrCtbBalPLctOpnUserAlt: TIntegerField;
    QrCtbBalPLctOpnAlterWeb: TSmallintField;
    QrCtbBalPLctOpnAWServerID: TIntegerField;
    QrCtbBalPLctOpnAWStatSinc: TSmallintField;
    QrCtbBalPLctOpnAtivo: TSmallintField;
    QrCtbBalPLctOpnPagMul: TFloatField;
    QrCtbBalPLctOpnPagJur: TFloatField;
    QrCtbBalPLctOpnRecDes: TFloatField;
    QrCtbBalPLctEmi: TMySQLQuery;
    QrCtbBalPLctEmiCodigo: TIntegerField;
    QrCtbBalPLctEmiData: TDateField;
    QrCtbBalPLctEmiTipo: TSmallintField;
    QrCtbBalPLctEmiCarteira: TIntegerField;
    QrCtbBalPLctEmiControle: TIntegerField;
    QrCtbBalPLctEmiSub: TSmallintField;
    QrCtbBalPLctEmiGenero: TIntegerField;
    QrCtbBalPLctEmiDebito: TFloatField;
    QrCtbBalPLctEmiCredito: TFloatField;
    QrCtbBalPLctEmiCompensado: TDateField;
    QrCtbBalPLctEmiSit: TIntegerField;
    QrCtbBalPLctEmiVencimento: TDateField;
    QrCtbBalPLctEmiID_Pgto: TIntegerField;
    QrCtbBalPLctEmiID_Quit: TIntegerField;
    QrCtbBalPLctEmiID_Sub: TSmallintField;
    QrCtbBalPLctEmiPago: TFloatField;
    QrCtbBalPLctEmiGenCtb: TIntegerField;
    QrCtbBalPLctEmiQtDtPg: TIntegerField;
    QrCtbBalPLctEmiLk: TIntegerField;
    QrCtbBalPLctEmiDataCad: TDateField;
    QrCtbBalPLctEmiDataAlt: TDateField;
    QrCtbBalPLctEmiUserCad: TIntegerField;
    QrCtbBalPLctEmiUserAlt: TIntegerField;
    QrCtbBalPLctEmiAlterWeb: TSmallintField;
    QrCtbBalPLctEmiAWServerID: TIntegerField;
    QrCtbBalPLctEmiAWStatSinc: TSmallintField;
    QrCtbBalPLctEmiAtivo: TSmallintField;
    QrCtbBalPLctEmiPagMul: TFloatField;
    QrCtbBalPLctEmiPagJur: TFloatField;
    QrCtbBalPLctEmiRecDes: TFloatField;
    DsCtbBalPLctEmi: TDataSource;
    DBGrid2: TDBGrid;
    Panel6: TPanel;
    RGTpAmbito: TRadioGroup;
    QrCtbBalPItsXPlaCod: TLargeintField;
    QrCtbBalPItsXCtaCod: TLargeintField;
    QrCtbBalPItsXSgrCod: TLargeintField;
    QrCtbBalPItsXGruCod: TLargeintField;
    QrCtbBalPItsXCjtCod: TLargeintField;
    QrCBPIO: TMySQLQuery;
    QrCBPIONiveisTXT: TWideStringField;
    QrCBPIOXPlaCod: TLargeintField;
    QrCBPIOXCjtCod: TLargeintField;
    QrCBPIOXGruCod: TLargeintField;
    QrCBPIOXSgrCod: TLargeintField;
    QrCBPIOXCtaCod: TLargeintField;
    QrCBPIOORD_PLA: TLargeintField;
    QrCBPIOORD_CJT: TLargeintField;
    QrCBPIOORD_GRU: TLargeintField;
    QrCBPIOORD_SGR: TLargeintField;
    QrCBPIOXPlaTxt: TWideStringField;
    QrCBPIOXCjtTxt: TWideStringField;
    QrCBPIOXGruTxt: TWideStringField;
    QrCBPIOXSgrTxt: TWideStringField;
    QrCBPIOXCtaTxt: TWideStringField;
    QrCBPIONO_CtaPlaA: TWideStringField;
    QrCBPIONO_CtaPlaB: TWideStringField;
    QrCBPIONO_TpBalanc: TWideStringField;
    QrCBPIONO_TpAmbito: TWideStringField;
    QrCBPIOCREDITO: TFloatField;
    QrCBPIODEBITO: TFloatField;
    frxDsCBPIO: TfrxDBDataset;
    frxCTB_BALAN_001_02: TfrxReport;
    QrCBPIE: TMySQLQuery;
    QrCBPIENO_CtaPlaA: TWideStringField;
    QrCBPIENO_CtaPlaB: TWideStringField;
    QrCBPIENO_TpBalanc: TWideStringField;
    QrCBPIECodigo: TIntegerField;
    QrCBPIEControle: TIntegerField;
    QrCBPIECredito: TFloatField;
    QrCBPIEDebito: TFloatField;
    QrCBPIELk: TIntegerField;
    QrCBPIEDataCad: TDateField;
    QrCBPIEDataAlt: TDateField;
    QrCBPIEUserCad: TIntegerField;
    QrCBPIEUserAlt: TIntegerField;
    QrCBPIEAlterWeb: TSmallintField;
    QrCBPIEAWServerID: TIntegerField;
    QrCBPIEAWStatSinc: TSmallintField;
    QrCBPIEAtivo: TSmallintField;
    QrCBPIEAPlaCod: TIntegerField;
    QrCBPIEACjtCod: TIntegerField;
    QrCBPIEAGruCod: TIntegerField;
    QrCBPIEASgrCod: TIntegerField;
    QrCBPIEACtaCod: TIntegerField;
    QrCBPIEBPlaCod: TIntegerField;
    QrCBPIEBCjtCod: TIntegerField;
    QrCBPIEBGruCod: TIntegerField;
    QrCBPIEBSgrCod: TIntegerField;
    QrCBPIEBCtaCod: TIntegerField;
    QrCBPIEAPlaTxt: TWideStringField;
    QrCBPIEACjtTxt: TWideStringField;
    QrCBPIEAGruTxt: TWideStringField;
    QrCBPIEASgrTxt: TWideStringField;
    QrCBPIEACtaTxt: TWideStringField;
    QrCBPIEBPlaTxt: TWideStringField;
    QrCBPIEBCjtTxt: TWideStringField;
    QrCBPIEBGruTxt: TWideStringField;
    QrCBPIEBSgrTxt: TWideStringField;
    QrCBPIEBCtaTxt: TWideStringField;
    QrCBPIENO_TpAmbito: TWideStringField;
    QrCBPIETpBalanc: TSmallintField;
    QrCBPIETpAmbito: TSmallintField;
    QrCBPIENiveisTXT: TWideStringField;
    QrCBPIEXPlaTxt: TWideStringField;
    QrCBPIEXCjtTxt: TWideStringField;
    QrCBPIEXGruTxt: TWideStringField;
    QrCBPIEXSgrTxt: TWideStringField;
    QrCBPIEXCtaTxt: TWideStringField;
    QrCBPIEXPlaCod: TLargeintField;
    QrCBPIEXCjtCod: TLargeintField;
    QrCBPIEXGruCod: TLargeintField;
    QrCBPIEXSgrCod: TLargeintField;
    QrCBPIEXCtaCod: TLargeintField;
    frxDsCBPIE: TfrxDBDataset;
    frxCTB_BALAN_001_03: TfrxReport;
    Balancete2: TMenuItem;
    QrCBPIEORD_PLA: TLargeintField;
    QrCBPIEORD_CJT: TLargeintField;
    QrCBPIEORD_GRU: TLargeintField;
    QrCBPIEORD_SGR: TLargeintField;
    QrErrsG: TMySQLQuery;
    QrErrsGLancamento: TIntegerField;
    QrErrsGCodWarning: TLargeintField;
    QrErrsGMsgWarning: TWideStringField;
    VerificaGneros1: TMenuItem;
    QrErrsGGenero: TIntegerField;
    QrErrsGGenCtb: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCtbBalPCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCtbBalPCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrCtbBalPCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrCtbBalPCabBeforeClose(DataSet: TDataSet);
    procedure Soslanamentosdofinanceiro1Click(Sender: TObject);
    procedure Sossaldosdascarteiras1Click(Sender: TObject);
    procedure odosrecriveis1Click(Sender: TObject);
    procedure frxCTB_BALAN_001_01GetValue(const VarName: string;
      var Value: Variant);
    procedure SossaldosdasCarteirasTipoEmisso1Click(Sender: TObject);
    procedure SossaldosdasCarteiras2Click(Sender: TObject);
    procedure Balancete1Click(Sender: TObject);
    procedure Balano1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Soslanamentosemitidosnoperodo1Click(Sender: TObject);
    procedure Lanamentosfinanceirosabertos1Click(Sender: TObject);
    procedure SaldosdeCarteiras1Click(Sender: TObject);
    procedure Lanamentosfinanceirosemitidos1Click(Sender: TObject);
    procedure TodoBalanco1Click(Sender: TObject);
    procedure QrCtbBalPItsBeforeClose(DataSet: TDataSet);
    procedure QrCtbBalPItsAfterScroll(DataSet: TDataSet);
    procedure RGTpAmbitoClick(Sender: TObject);
    procedure frxCTB_BALAN_001_02GetValue(const VarName: string;
      var Value: Variant);
    procedure Balancete2Click(Sender: TObject);
    procedure frxCTB_BALAN_001_03GetValue(const VarName: string;
      var Value: Variant);
    procedure VerificaGneros1Click(Sender: TObject);
  private
    FEmpresa, FFilial, FBalPatr: Integer;
    //FEntidade_TXT,
    FTabLctA, FTabLctB, FTabLctD: String;
    FDtEncer, FDtMorto: TDateTime;
    //
    FsCodigo, FsEmpresa, FsDiaSeguinte, FsPrimeiroDia: String;
    FAMSeguinte, FAMAtual: Integer;
    FDiaSeguinte, FPrimeiroDia: TDateTime;
    FTituloImp: String;
    //
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraFormCtbBalPIts(SQLType: TSQLType);
    procedure MostraFormCtbBalPLctEdit(SQL: String);
    //
    procedure AtualizaPagoParcial(Controle, Sub: Integer; OriCredito, OriDebito:
              Double; DtCompensado: TDateTime; sDiaSeguinte: String);
    procedure ImprimeBalanco(Forma: Integer);
    procedure ImprimeBalancete(Forma: Integer);
    function  InsereCtbBalPIts(SQLType: TSQLType; Codigo, Controle, TpBalanc,
              TpAmbito: Integer; Credito, Debito: Double; APlaCod, ACjtCod,
              AGruCod, ASgrCod, ACtaCod, BPlaCod, BCjtCod, BGruCod, BSgrCod,
              BCtaCod: Integer; APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
              BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt: String): Boolean;
    procedure InsereLctOpnGeneroAtual();
    procedure InsereLctOpnGenCtbAtual();
    procedure InsereLctEmiGeneroAtual();
    procedure RecriaItens_Todos();
    procedure RecriaItensCarteiras();
    procedure RecriaItensCarteirasTipo0e1();
    procedure RecriaItensCarteirasTipo2();
    procedure RecriaItensFinanceiro();
    procedure RecriaTotaisGeneroPeriodo();
    procedure ReopenCBPIO(SQL_Extra: String);
    procedure ReopenCBPIE(SQL_Extra: String; Integrado: Boolean = False);

    function  PreparaSomasA(): Boolean;
    function  PreparaSomasB(): Boolean;
    //function  PreparaSomasC(): Boolean;
    //
    function ObtemValB_PLa(Tabela, Campo: String): Double;
    function ObtemValB_Cjt(Tabela, Campo: String): Double;
    function ObtemValB_Gru(Tabela, Campo: String): Double;
    function ObtemValB_Sgr(Tabela, Campo: String): Double;

    function ObtemValC_PLa(Tabela, Campo: String): Double;
    function ObtemValC_Cjt(Tabela, Campo: String): Double;
    function ObtemValC_Gru(Tabela, Campo: String): Double;
    function ObtemValC_Sgr(Tabela, Campo: String): Double;
    //
    procedure VerificaErrosGenero_e_GenCtb(SoImportantes: Boolean);
    //
    function MesAnoTxt(AnoMes: Integer): String;
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenCtbBalPIts(SQL_Extra: String; Controle: Integer;
              IgnoraFiltro: Boolean = False; Integrado: Boolean = False);

  end;

var
  FmCtbBalPCab: TFmCtbBalPCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, CtbBalPIts, ModuleGeral,
  UnContabil_Jan, UnFinanceiro, CtbBalPLctEdit;

{$R *.DFM}

var
  ArrPla, ArrCjt, ArrGru, ArrSgr, ArrCta: TArr2Double;
  FIsOK: Boolean;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCtbBalPCab.Lanamentosfinanceirosabertos1Click(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ctbbalpits WHERE Codigo=' + FsCodigo +
    ' AND TpAmbito=' + Geral.FF0(Integer(TContabTpAmbito.ctaLctGenOpn))
  );
  // ...
  //
  ReopenCtbBalPIts('', 0);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmCtbBalPCab.Lanamentosfinanceirosemitidos1Click(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ctbbalpits WHERE Codigo=' + FsCodigo +
    ' AND TpAmbito=' + Geral.FF0(Integer(TContabTpAmbito.ctaLctGenEmi))
  );
  // ...
  //
  ReopenCtbBalPIts('', 0);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmCtbBalPCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

function TFmCtbBalPCab.MesAnoTxt(AnoMes: Integer): String;
const
  Upper = True;
  Sigla = False;
var
  Ano, Mes: Integer;
begin
  Ano := AnoMes div 100;
  Mes := AnoMes mod 100;
  Result := dmkPF.VerificaMes(Mes, Upper, Sigla) + ' / ' + Geral.FF0(Ano);
end;

procedure TFmCtbBalPCab.MostraFormCtbBalPIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmCtbBalPIts, FmCtbBalPIts, afmoNegarComAviso) then
  begin
    FmCtbBalPIts.ImgTipo.SQLType := SQLType;
    FmCtbBalPIts.FQrCab  := QrCtbBalPCab;
    FmCtbBalPIts.FDsCab  := DsCtbBalPCab;
    FmCtbBalPIts.FQrIts  := QrCtbBalPIts;
    FmCtbBalPIts.FCodigo := QrCtbBalPCabCodigo.Value;
    if SQLType = stIns then
      //
    else
    begin
      FmCtbBalPIts.EdControle.ValueVariant := QrCtbBalPItsControle.Value;
      //
      FmCtbBalPIts.EdCtaPlaA.ValueVariant  := QrCtbBalPItsACtaCod.Value;
      FmCtbBalPIts.CBCtaPlaA.KeyValue      := QrCtbBalPItsACtaCod.Value;
      FmCtbBalPIts.EdCtaPlaB.ValueVariant  := QrCtbBalPItsBCtaCod.Value;
      FmCtbBalPIts.CBCtaPlaB.KeyValue      := QrCtbBalPItsBCtaCod.Value;
      FmCtbBalPIts.EdCredito.ValueVariant  := QrCtbBalPItsCredito.Value;
      FmCtbBalPIts.EdDebito.ValueVariant   := QrCtbBalPItsDebito.Value;
      FmCtbBalPIts.RGTpAmbito.ItemIndex    := QrCtbBalPItsTpAmbito.Value;
    end;
    FmCtbBalPIts.ShowModal;
    FmCtbBalPIts.Destroy;
  end;
end;

procedure TFmCtbBalPCab.MostraFormCtbBalPLctEdit(SQL: String);
begin
  if DBCheck.CriaFm(TFmCtbBalPLctEdit, FmCtbBalPLctEdit, afmoNegarComAviso) then
  begin
    FmCtbBalPLctEdit.ImgTipo.SQLType := stUpd;
    FmCtbBalPLctEdit.FSQL            := SQL;
    FmCtbBalPLctEdit.FTabLctA        := FTabLcta;
    FmCtbBalPLctEdit.ReopenOrfaos();
    FmCtbBalPLctEdit.ShowModal;
    FmCtbBalPLctEdit.Destroy;
  end;
end;

function TFmCtbBalPCab.ObtemValB_PLa(Tabela, Campo: String): Double;
begin
  Result := Dmod.MyDB.SelectDouble(
  'SELECT SUM(' + Campo + ') Valor FROM ' + Tabela + ' WHERE ORD_PLA=' +
  IntToStr(QrCBPIOORD_PLA.Value), FIsOK, 'Valor');
end;

function TFmCtbBalPCab.ObtemValB_Cjt(Tabela, Campo: String): Double;
begin
  Result := Dmod.MyDB.SelectDouble(
  'SELECT SUM(' + Campo + ') Valor FROM ' + Tabela + ' WHERE ORD_PLA=' +
  IntToStr(QrCBPIOORD_PLA.Value) + ' AND ORD_CJT=' +
  IntToStr(QrCBPIOORD_CJT.Value), FIsOK, 'Valor');
end;

function TFmCtbBalPCab.ObtemValB_Gru(Tabela, Campo: String): Double;
begin
  Result := Dmod.MyDB.SelectDouble(
  'SELECT SUM(' + Campo + ') Valor FROM ' + Tabela + ' WHERE ORD_PLA=' +
  IntToStr(QrCBPIOORD_PLA.Value) + ' AND ORD_CJT=' +
  IntToStr(QrCBPIOORD_CJT.Value) + ' AND ORD_GRU=' +
  IntToStr(QrCBPIOORD_GRU.Value), FIsOK, 'Valor');
end;

function TFmCtbBalPCab.ObtemValB_Sgr(Tabela, Campo: String): Double;
begin
  Result := Dmod.MyDB.SelectDouble(
  'SELECT SUM(' + Campo + ') Valor FROM ' + Tabela + ' WHERE ORD_PLA=' +
  IntToStr(QrCBPIOORD_PLA.Value) + ' AND ORD_CJT=' +
  IntToStr(QrCBPIOORD_CJT.Value) + ' AND ORD_GRU=' +
  IntToStr(QrCBPIOORD_GRU.Value) + ' AND ORD_SGR=' +
  IntToStr(QrCBPIOORD_SGR.Value), FIsOK, 'Valor');
end;

function TFmCtbBalPCab.ObtemValC_PLa(Tabela, Campo: String): Double;
begin
  Result := Dmod.MyDB.SelectDouble(
  'SELECT SUM(' + Campo + ') Valor FROM ' + Tabela + ' WHERE XPlaCod=' +
  IntToStr(QrCBPIEXPlaCod.Value), FIsOK, 'Valor');
end;

function TFmCtbBalPCab.ObtemValC_Cjt(Tabela, Campo: String): Double;
begin
  Result := Dmod.MyDB.SelectDouble(
  'SELECT SUM(' + Campo + ') Valor FROM ' + Tabela + ' WHERE XPlaCod=' +
  IntToStr(QrCBPIEXPlaCod.Value) + ' AND XCjtCod=' +
  IntToStr(QrCBPIEXCjtCod.Value), FIsOK, 'Valor');
end;

function TFmCtbBalPCab.ObtemValC_Gru(Tabela, Campo: String): Double;
begin
  Result := Dmod.MyDB.SelectDouble(
  'SELECT SUM(' + Campo + ') Valor FROM ' + Tabela + ' WHERE XPlaCod=' +
  IntToStr(QrCBPIEXPlaCod.Value) + ' AND XCjtCod=' +
  IntToStr(QrCBPIEXCjtCod.Value) + ' AND XGruCod=' +
  IntToStr(QrCBPIEXGruCod.Value), FIsOK, 'Valor');
end;

function TFmCtbBalPCab.ObtemValC_Sgr(Tabela, Campo: String): Double;
begin
  Result := Dmod.MyDB.SelectDouble(
  'SELECT SUM(' + Campo + ') Valor FROM ' + Tabela + ' WHERE XPlaCod=' +
  IntToStr(QrCBPIEXPlaCod.Value) + ' AND XCjtCod=' +
  IntToStr(QrCBPIEXCjtCod.Value) + ' AND XGruCod=' +
  IntToStr(QrCBPIEXGruCod.Value) + ' AND XSgrCod=' +
  IntToStr(QrCBPIEXSgrCod.Value), FIsOK, 'Valor');
end;

procedure TFmCtbBalPCab.odosrecriveis1Click(Sender: TObject);
begin
  RecriaItens_Todos();
  //
  VerificaErrosGenero_e_GenCtb(True);
  //
end;

procedure TFmCtbBalPCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrCtbBalPCab);
  MyObjects.HabilitaMenuItemItsDel(CabExclui1, QrCtbBalPCab);
  MyObjects.HabilitaMenuItemCabDel(TodoBalanco1, QrCtbBalPCab, QrCtbBalPIts);
end;

procedure TFmCtbBalPCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrCtbBalPCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrCtbBalPIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrCtbBalPIts);

end;

function TFmCtbBalPCab.PreparaSomasA(): Boolean;
var
  I, K, N, Pla, Cjt, Gru, Sgr, Cta, Controle: Integer;
  Credito, Debito: Double;
begin
  Result := False;
  QrCtbBalPIts.DisableControls;
  try
    Controle := QrCtbBalPItsControle.Value;
    ArrPla := nil;
    ArrCjt := nil;
    ArrGru := nil;
    ArrSgr := nil;
    ArrCta := nil;
    QrCtbBalPIts.First;
    while not QrCtbBalPIts.Eof do
    begin
      Pla := QrCtbBalPItsXPlaCod.Value;
      Cjt := QrCtbBalPItsXCjtCod.Value;
      Gru := QrCtbBalPItsXGruCod.Value;
      Sgr := QrCtbBalPItsXSgrCod.Value;
      Cta := QrCtbBalPItsXCtaCod.Value;
      // Plano
      K := Length(ArrPla);
      if K <= Pla then
      begin
        SetLength(ArrPla, Pla + 1);
        for I := K to Pla do
        begin
          ArrPla[I][0] := 0;
          ArrPla[I][1] := 0;
        end;
      end;
      // Cunjunto
      K := Length(ArrCjt);
      if K <= Cjt then
      begin
        SetLength(ArrCjt, Cjt + 1);
        for I := K to Cjt do
        begin
          ArrCjt[I][0] := 0;
          ArrCjt[I][1] := 0;
        end;
      end;
      // Grupo
      K := Length(ArrGru);
      if K <= Gru then
      begin
        SetLength(ArrGru, Gru + 1);
        for I := K to Gru do
        begin
          ArrGru[I][0] := 0;
          ArrGru[I][1] := 0;
        end;
      end;
      // SubSgrpo
      K := Length(ArrSgr);
      if K <= Sgr then
      begin
        SetLength(ArrSgr, Sgr + 1);
        for I := K to Sgr do
        begin
          ArrSgr[I][0] := 0;
          ArrSgr[I][1] := 0;
        end;
      end;
      // Conta
      K := Length(ArrCta);
      if K <= Cta then
      begin
        SetLength(ArrCta, Cta + 1);
        for I := K to Cta do
        begin
          ArrCta[I][0] := 0;
          ArrCta[I][1] := 0;
        end;
      end;
      //
      Credito := QrCtbBalPItsCredito.Value;
      Debito  := QrCtbBalPItsDebito.Value;
      //
      ArrPla[Pla][0] := ArrPla[Pla][0] + Credito;
      ArrPla[Pla][1] := ArrPla[Pla][1] + Debito;
      //
      ArrCjt[Cjt][0] := ArrCjt[Cjt][0] + Credito;
      ArrCjt[Cjt][1] := ArrCjt[Cjt][1] + Debito;
      //
      ArrGru[Gru][0] := ArrGru[Gru][0] + Credito;
      ArrGru[Gru][1] := ArrGru[Gru][1] + Debito;
      //
      ArrSgr[Sgr][0] := ArrSgr[Sgr][0] + Credito;
      ArrSgr[Sgr][1] := ArrSgr[Sgr][1] + Debito;
      //
      ArrCta[Cta][0] := ArrCta[Cta][0] + Credito;
      ArrCta[Cta][1] := ArrCta[Cta][1] + Debito;
      // ...
      QrCtbBalPIts.Next;
    end;
    QrCtbBalPIts.Locate('Controle', Controle, []);
    //
    Result := True;
  finally
    QrCtbBalPIts.EnableControls;
  end;
end;

function TFmCtbBalPCab.PreparaSomasB(): Boolean;
var
  //Controle
  I, K, N, Pla, Cjt, Gru, Sgr, Cta: Integer;
  Credito, Debito: Double;
begin
  Result := False;
  QrCBPIO.DisableControls;
  try
    //Controle := QrCBPIOControle.Value;
    ArrPla := nil;
    ArrCjt := nil;
    ArrGru := nil;
    ArrSgr := nil;
    ArrCta := nil;
    QrCBPIO.First;
    while not QrCBPIO.Eof do
    begin
      Pla := QrCBPIOORD_PLA.Value;
      Cjt := QrCBPIOORD_CJT.Value;
      Gru := QrCBPIOORD_GRU.Value;
      Sgr := QrCBPIOORD_SGR.Value;
      Cta := QrCBPIOXCtaCod.Value;
      // Plano
      K := Length(ArrPla);
      if K <= Pla then
      begin
        SetLength(ArrPla, Pla + 1);
        for I := K to Pla do
        begin
          ArrPla[I][0] := 0;
          ArrPla[I][1] := 0;
        end;
      end;
      // Cunjunto
      K := Length(ArrCjt);
      if K <= Cjt then
      begin
        SetLength(ArrCjt, Cjt + 1);
        for I := K to Cjt do
        begin
          ArrCjt[I][0] := 0;
          ArrCjt[I][1] := 0;
        end;
      end;
      // Grupo
      K := Length(ArrGru);
      if K <= Gru then
      begin
        SetLength(ArrGru, Gru + 1);
        for I := K to Gru do
        begin
          ArrGru[I][0] := 0;
          ArrGru[I][1] := 0;
        end;
      end;
      // SubSgrpo
      K := Length(ArrSgr);
      if K <= Sgr then
      begin
        SetLength(ArrSgr, Sgr + 1);
        for I := K to Sgr do
        begin
          ArrSgr[I][0] := 0;
          ArrSgr[I][1] := 0;
        end;
      end;
      // Conta
      K := Length(ArrCta);
      if K <= Cta then
      begin
        SetLength(ArrCta, Cta + 1);
        for I := K to Cta do
        begin
          ArrCta[I][0] := 0;
          ArrCta[I][1] := 0;
        end;
      end;
      //
      Credito := QrCBPIOCredito.Value;
      Debito  := QrCBPIODebito.Value;
      //
      ArrPla[Pla][0] := ArrPla[Pla][0] + Credito;
      ArrPla[Pla][1] := ArrPla[Pla][1] + Debito;
      //
      ArrCjt[Cjt][0] := ArrCjt[Cjt][0] + Credito;
      ArrCjt[Cjt][1] := ArrCjt[Cjt][1] + Debito;
      //
      ArrGru[Gru][0] := ArrGru[Gru][0] + Credito;
      ArrGru[Gru][1] := ArrGru[Gru][1] + Debito;
      //
      ArrSgr[Sgr][0] := ArrSgr[Sgr][0] + Credito;
      ArrSgr[Sgr][1] := ArrSgr[Sgr][1] + Debito;
      //
      ArrCta[Cta][0] := ArrCta[Cta][0] + Credito;
      ArrCta[Cta][1] := ArrCta[Cta][1] + Debito;
      // ...
      QrCBPIO.Next;
    end;
    //QrCBPIO.Locate('Controle', Controle, []);
    //
    Result := True;
  finally
    QrCBPIO.EnableControls;
  end;
end;

{
function TFmCtbBalPCab.PreparaSomasC(): Boolean;
var
  I, K, N, Pla, Cjt, Gru, Sgr, Cta, Controle: Integer;
  Credito, Debito: Double;
begin
  Result := False;
  QrCBPIE.DisableControls;
  try
    Controle := QrCBPIEControle.Value;
    ArrPla := nil;
    ArrCjt := nil;
    ArrGru := nil;
    ArrSgr := nil;
    ArrCta := nil;
    QrCBPIE.First;
    while not QrCBPIE.Eof do
    begin
      Pla := QrCBPIEXPlaCod.Value;
      Cjt := QrCBPIEXCjtCod.Value;
      Gru := QrCBPIEXGruCod.Value;
      Sgr := QrCBPIEXSgrCod.Value;
      Cta := QrCBPIEXCtaCod.Value;
      // Plano
      K := Length(ArrPla);
      if K <= Pla then
      begin
        SetLength(ArrPla, Pla + 1);
        for I := K to Pla do
        begin
          ArrPla[I][0] := 0;
          ArrPla[I][1] := 0;
        end;
      end;
      // Cunjunto
      K := Length(ArrCjt);
      if K <= Cjt then
      begin
        SetLength(ArrCjt, Cjt + 1);
        for I := K to Cjt do
        begin
          ArrCjt[I][0] := 0;
          ArrCjt[I][1] := 0;
        end;
      end;
      // Grupo
      K := Length(ArrGru);
      if K <= Gru then
      begin
        SetLength(ArrGru, Gru + 1);
        for I := K to Gru do
        begin
          ArrGru[I][0] := 0;
          ArrGru[I][1] := 0;
        end;
      end;
      // SubSgrpo
      K := Length(ArrSgr);
      if K <= Sgr then
      begin
        SetLength(ArrSgr, Sgr + 1);
        for I := K to Sgr do
        begin
          ArrSgr[I][0] := 0;
          ArrSgr[I][1] := 0;
        end;
      end;
      // Conta
      K := Length(ArrCta);
      if K <= Cta then
      begin
        SetLength(ArrCta, Cta + 1);
        for I := K to Cta do
        begin
          ArrCta[I][0] := 0;
          ArrCta[I][1] := 0;
        end;
      end;
      //
      Credito := QrCBPIECredito.Value;
      Debito  := QrCBPIEDebito.Value;
      //
      ArrPla[Pla][0] := ArrPla[Pla][0] + Credito;
      ArrPla[Pla][1] := ArrPla[Pla][1] + Debito;
      //
      ArrCjt[Cjt][0] := ArrCjt[Cjt][0] + Credito;
      ArrCjt[Cjt][1] := ArrCjt[Cjt][1] + Debito;
      //
      ArrGru[Gru][0] := ArrGru[Gru][0] + Credito;
      ArrGru[Gru][1] := ArrGru[Gru][1] + Debito;
      //
      ArrSgr[Sgr][0] := ArrSgr[Sgr][0] + Credito;
      ArrSgr[Sgr][1] := ArrSgr[Sgr][1] + Debito;
      //
      ArrCta[Cta][0] := ArrCta[Cta][0] + Credito;
      ArrCta[Cta][1] := ArrCta[Cta][1] + Debito;
      // ...
      QrCBPIE.Next;
    end;
    QrCBPIE.Locate('Controle', Controle, []);
    //
    Result := True;
  finally
    QrCBPIE.EnableControls;
  end;
end;
}

procedure TFmCtbBalPCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCtbBalPCabCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmCtbBalPCab.VerificaErrosGenero_e_GenCtb(SoImportantes: Boolean);
var
  SQL: String;
  //
  function AddSQL(Union: Boolean; Erro: Integer; Campo, SQL_Extra, Mensagem: String): String;
  begin
    if Union then
      Result := sLineBreak + 'UNION ' + sLineBreak
    else
      Result := EmptyStr;
    //
    Result := Result + Geral.ATS([
    'SELECT emi.Controle Lancamento, ' + Geral.FF0(Erro) + ' CodWarning,  ',
    'emi.Genero, emi.GenCtb, ',
    '"' + Mensagem + '" MsgWarning  ',
    'FROM ctbbalplctemi  emi ',
    'LEFT JOIN carteiras car ON car.Codigo=emi.Carteira ',
    'LEFT JOIN contas    cta ON cta.Codigo=emi.' + Campo,
    'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo ',
    'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo ',
    'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto ',
    'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano ',
    'WHERE  emi.Codigo=' + Geral.FF0(QrCtbBalPCabCodigo.Value),
    SQL_Extra
    ]);
  end;
var
  SQL_NaoImportantes: String;
begin
  if SoImportantes then
    SQL_NaoImportantes := ''
  else
    SQL_NaoImportantes := Geral.ATS([
    AddSQL(True,  3, 'Genero', 'AND pla.FinContab<>3 ',
      'Lan�amento com conta cont�bil no campo financeiro!'),
    AddSQL(True,  4, 'GenCtb', Geral.ATS(['AND emi.GenCtb <> 0 ', 'AND pla.FinContab=3 ']),
      'Lan�amento com conta finaneira no campo cont�bil!')]);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrsG, Dmod.MyDB, [
  AddSQL(False, 1, 'Genero', 'AND pla.FinContab<>3 ',
    'Lan�amento com conta cont�bil no campo financeiro!'),
  AddSQL(True,  2, 'GenCtb', Geral.ATS(['AND emi.GenCtb <> 0 ', 'AND pla.FinContab=3 ']),
    'Lan�amento com conta finaneira no campo cont�bil!'),
  SQL_NaoImportantes,
  AddSQL(True,  5, 'Genero', 'AND pla.FinContab=0 ',
    'Lan�amento Financeiro com Plano sem defini��o Cont�bil'),
  AddSQL(True,  6, 'GenCtb', Geral.ATS(['AND emi.GenCtb <> 0 ', 'AND pla.FinContab=0']),
    'Lan�amento Cont�bil com Plano sem defini��o Cont�bil'),
  'ORDER BY CodWarning, Genero, GenCtb ',
  ' ']);
  //
  if QrErrsG.RecordCount > 0 then
  begin
    Contabil_Jan.MostraFormCtbBalPErrG(FTabLctA, QrErrsG.SQL.Text);
  end
  else if SoImportantes = False then
    Geral.MB_Info('N�o foram encontrados lan�amentos divergentes.');
end;

procedure TFmCtbBalPCab.VerificaGneros1Click(Sender: TObject);
begin
  VerificaErrosGenero_e_GenCtb(False);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCtbBalPCab.DefParams;
begin
  VAR_GOTOTABELA := 'ctbbalpcab';
  VAR_GOTOMYSQLTABLE := QrCtbBalPCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ent.RazaoSocial NO_EMPRESA, cbc.*   ');
  VAR_SQLx.Add('FROM ctbbalpcab cbc  ');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=cbc.Empresa  ');
  VAR_SQLx.Add('WHERE cbc.Codigo > 0');
  //
  VAR_SQL1.Add('AND cbc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cbc.Nome Like :P0');
  //
end;

procedure TFmCtbBalPCab.ImprimeBalancete(Forma: Integer);
var
  SQL_Balanco: String;
begin
  SQL_Balanco := Geral.ATS(['']);
  //
  if Forma > 0 then
  begin
    ReopenCBPIE(SQL_Balanco, Forma = 2);
    //
    //if PreparaSomasC() then
    begin
      MyObjects.frxDefineDataSets(frxCTB_BALAN_001_03, [
        frxDsCBPIE
      ]);
      //
      MyObjects.frxMostra(frxCTB_BALAN_001_03, FTituloImp);
    end;
  end else
  begin
    ReopenCBPIO(SQL_Balanco);
    //
    //if PreparaSomasC() then
    begin
      MyObjects.frxDefineDataSets(frxCTB_BALAN_001_02, [
        frxDsCBPIO
      ]);
      //
      MyObjects.frxMostra(frxCTB_BALAN_001_02, FTituloImp);
    end;
  end;
end;

procedure TFmCtbBalPCab.ImprimeBalanco(Forma: Integer);
var
  SQL_Balanco: String;
begin
  SQL_Balanco := Geral.ATS([
  'AND ( ',
  '  (pla.FinContab IN (1,2)) ',
  '  OR ',
  '  (plb.FinContab IN (1,2)) ',
  ') ',
  //'AND cbi.TpAmbito = ' + Geral.FF0(Integer(TContabTpAmbito.ctaLctGenOpn))
  'AND cbi.TpAmbito <> ' + Geral.FF0(Integer(TContabTpAmbito.ctaLctGenEmi))
  ]);
  //
  if Forma > 0 then
  begin
{
    ReopenCtbBalPIts(SQL_Balanco, 0, True, Forma = 2);
    //
    //Geral.MB_Teste(QrCtbBalPIts.SQL.Text);
    //
    if PreparaSomasA() then
    begin
      QrCtbBalPIts.DisableControls;
      try
        MyObjects.frxDefineDataSets(frxCTB_BALAN_001_01, [
          frxDsCtbBalPIts
        ]);
        //
        MyObjects.frxMostra(frxCTB_BALAN_001_01, 'Balan�o');
      finally
        QrCtbBalPIts.EnableControls;
        ReopenCtbBalPIts(EmptyStr, 0);
      end;
    end;
}
    ReopenCBPIE(SQL_Balanco, Forma = 2);
    //
    //if PreparaSomasC() then
    begin
      MyObjects.frxDefineDataSets(frxCTB_BALAN_001_03, [
        frxDsCBPIE
      ]);
      //
      MyObjects.frxMostra(frxCTB_BALAN_001_03, FTituloImp);
    end;
  end else
  begin
    ReopenCBPIO(SQL_Balanco);
    //
    if PreparaSomasB() then
    begin
      MyObjects.frxDefineDataSets(frxCTB_BALAN_001_02, [
        frxDsCBPIO
      ]);
      //
      MyObjects.frxMostra(frxCTB_BALAN_001_02, FTituloImp);
    end;
  end;
end;

function TFmCtbBalPCab.InsereCtbBalPIts(SQLType: TSQLType; Codigo, Controle,
  TpBalanc, TpAmbito: Integer; Credito, Debito: Double; APlaCod, ACjtCod,
  AGruCod, ASgrCod, ACtaCod, BPlaCod, BCjtCod, BGruCod, BSgrCod, BCtaCod:
  Integer; APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt, BPlaTxt, BCjtTxt,
  BGruTxt, BSgrTxt, BCtaTxt: String): Boolean;
begin
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctbbalpits', False, [
  'Codigo', 'TpBalanc', 'TpAmbito',
  'Credito', 'Debito',
  'APlaCod', 'ACjtCod', 'AGruCod',
  'ASgrCod', 'ACtaCod', 'BPlaCod',
  'BCjtCod', 'BGruCod', 'BSgrCod',
  'BCtaCod', 'APlaTxt', 'ACjtTxt',
  'AGruTxt', 'ASgrTxt', 'ACtaTxt',
  'BPlaTxt', 'BCjtTxt', 'BGruTxt',
  'BSgrTxt', 'BCtaTxt'], [
  'Controle'], [
  Codigo, TpBalanc, TpAmbito,
  Credito, Debito,
  APlaCod, ACjtCod, AGruCod,
  ASgrCod, ACtaCod, BPlaCod,
  BCjtCod, BGruCod, BSgrCod,
  BCtaCod, APlaTxt, ACjtTxt,
  AGruTxt, ASgrTxt, ACtaTxt,
  BPlaTxt, BCjtTxt, BGruTxt,
  BSgrTxt, BCtaTxt], [
  Controle], True) then
  begin
    (*
    n�o d�!!!
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      'UPDATE ' + FTabLctA + ' SET BalPatr=' + Geral.FF0(
      FBalPatr) + ' WHERE Controle'
    *)
  end;
end;

procedure TFmCtbBalPCab.InsereLctEmiGeneroAtual;
var
  APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
  BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt: String;
  Codigo, Controle, TpBalanc, TpAmbito,
  APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod,
  BPlaCod, BCjtCod, BGruCod, BSgrCod, BCtaCod: Integer;
  Credito, Debito: Double;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := QrCtbBalPCabCodigo.Value;
  Controle       := UMyMod.BPGS1I32('ctbbalpits', 'Controle', '', '', tsPos, SQLType, 0);
  TpBalanc       := Integer(TContabTpBalanc.ctbLctGenero);
  TpAmbito       := Integer(TContabTpAmbito.ctaLctGenEmi);
  Credito        := QrLCtsCredito.Value;
  Debito         := QrLCtsDebito .Value;
  APlaCod        := QrLCtsAPlaCod.Value;
  ACjtCod        := QrLCtsACjtCod.Value;
  AGruCod        := QrLCtsAGruCod.Value;
  ASgrCod        := QrLCtsASgrCod.Value;
  ACtaCod        := QrLCtsACtaCod.Value;
  BPlaCod        := 0; //QrLCtsABPlaCod.Value;
  BCjtCod        := 0; //QrLCtsABCjtCod.Value;
  BGruCod        := 0; //QrLCtsABGruCod.Value;
  BSgrCod        := 0; //QrLCtsABSgrCod.Value;
  BCtaCod        := 0; //QrLCtsABCtaCod.Value;
  APlaTxt        := QrLCtsAPlaTxt.Value;
  ACjtTxt        := QrLCtsACjtTxt.Value;
  AGruTxt        := QrLCtsAGruTxt.Value;
  ASgrTxt        := QrLCtsASgrTxt.Value;
  ACtaTxt        := QrLCtsACtaTxt.Value;
  BPlaTxt        := ''; //QrLCtsABPlaTxt.Value;
  BCjtTxt        := ''; //QrLCtsABCjtTxt.Value;
  BGruTxt        := ''; //QrLCtsABGruTxt.Value;
  BSgrTxt        := ''; //QrLCtsABSgrTxt.Value;
  BCtaTxt        := ''; //QrLCtsABCtaTxt.Value;
  //
  InsereCtbBalPIts(SQLType, Codigo, Controle, TpBalanc, TpAmbito, Credito,
  Debito, APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod, BPlaCod, BCjtCod,
  BGruCod, BSgrCod, BCtaCod, APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
  BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt);
end;

procedure TFmCtbBalPCab.InsereLctOpnGenCtbAtual();
var
  APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
  BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt: String;
  Codigo, Controle, TpBalanc, TpAmbito,
  APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod,
  BPlaCod, BCjtCod, BGruCod, BSgrCod, BCtaCod: Integer;
  Credito, Debito: Double;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := QrCtbBalPCabCodigo.Value;
  Controle       := UMyMod.BPGS1I32('ctbbalpits', 'Controle', '', '', tsPos, SQLType, 0);
  TpBalanc       := Integer(TContabTpBalanc.ctbLctGenCtb);
  TpAmbito       := Integer(TContabTpAmbito.ctaLctGenOpn);
  Credito        := QrLCtsDebito.Value;  // Inverte???
  Debito         := QrLCtsCredito.Value; // Inverte???
  APlaCod        := 0; //QrLCtsAPlaCod.Value;
  ACjtCod        := 0; //QrLCtsACjtCod.Value;
  AGruCod        := 0; //QrLCtsAGruCod.Value;
  ASgrCod        := 0; //QrLCtsASgrCod.Value;
  ACtaCod        := 0; //QrLCtsACtaCod.Value;
  BPlaCod        := QrLCtsBPlaCod.Value;
  BCjtCod        := QrLCtsBCjtCod.Value;
  BGruCod        := QrLCtsBGruCod.Value;
  BSgrCod        := QrLCtsBSgrCod.Value;
  BCtaCod        := QrLCtsBCtaCod.Value;
  APlaTxt        := ''; //QrLCtsAPlaTxt.Value;
  ACjtTxt        := ''; //QrLCtsACjtTxt.Value;
  AGruTxt        := ''; //QrLCtsAGruTxt.Value;
  ASgrTxt        := ''; //QrLCtsASgrTxt.Value;
  ACtaTxt        := ''; //QrLCtsACtaTxt.Value;
  BPlaTxt        := QrLCtsBPlaTxt.Value;
  BCjtTxt        := QrLCtsBCjtTxt.Value;
  BGruTxt        := QrLCtsBGruTxt.Value;
  BSgrTxt        := QrLCtsBSgrTxt.Value;
  BCtaTxt        := QrLCtsBCtaTxt.Value;
  //
  InsereCtbBalPIts(SQLType, Codigo, Controle, TpBalanc, TpAmbito, Credito,
  Debito, APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod, BPlaCod, BCjtCod,
  BGruCod, BSgrCod, BCtaCod, APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
  BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt);
end;

procedure TFmCtbBalPCab.InsereLctOpnGeneroAtual();
var
  APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
  BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt: String;
  Codigo, Controle, TpBalanc, TpAmbito,
  APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod,
  BPlaCod, BCjtCod, BGruCod, BSgrCod, BCtaCod: Integer;
  Credito, Debito: Double;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := QrCtbBalPCabCodigo.Value;
  Controle       := UMyMod.BPGS1I32('ctbbalpits', 'Controle', '', '', tsPos, SQLType, 0);
  TpBalanc       := Integer(TContabTpBalanc.ctbLctGenero);
  TpAmbito       := Integer(TContabTpAmbito.ctaLctGenOpn);
  Credito        := QrLCtsCredito.Value;
  Debito         := QrLCtsDebito .Value;
  APlaCod        := QrLCtsAPlaCod.Value;
  ACjtCod        := QrLCtsACjtCod.Value;
  AGruCod        := QrLCtsAGruCod.Value;
  ASgrCod        := QrLCtsASgrCod.Value;
  ACtaCod        := QrLCtsACtaCod.Value;
  BPlaCod        := 0; //QrLCtsABPlaCod.Value;
  BCjtCod        := 0; //QrLCtsABCjtCod.Value;
  BGruCod        := 0; //QrLCtsABGruCod.Value;
  BSgrCod        := 0; //QrLCtsABSgrCod.Value;
  BCtaCod        := 0; //QrLCtsABCtaCod.Value;
  APlaTxt        := QrLCtsAPlaTxt.Value;
  ACjtTxt        := QrLCtsACjtTxt.Value;
  AGruTxt        := QrLCtsAGruTxt.Value;
  ASgrTxt        := QrLCtsASgrTxt.Value;
  ACtaTxt        := QrLCtsACtaTxt.Value;
  BPlaTxt        := ''; //QrLCtsABPlaTxt.Value;
  BCjtTxt        := ''; //QrLCtsABCjtTxt.Value;
  BGruTxt        := ''; //QrLCtsABGruTxt.Value;
  BSgrTxt        := ''; //QrLCtsABSgrTxt.Value;
  BCtaTxt        := ''; //QrLCtsABCtaTxt.Value;
  //
  InsereCtbBalPIts(SQLType, Codigo, Controle, TpBalanc, TpAmbito, Credito,
  Debito, APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod, BPlaCod, BCjtCod,
  BGruCod, BSgrCod, BCtaCod, APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
  BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt);
end;

procedure TFmCtbBalPCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormCtbBalPIts(stUpd);
end;

procedure TFmCtbBalPCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCtbBalPCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCtbBalPCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'CtbBalPIts', 'Controle', QrCtbBalPItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrCtbBalPIts,
      QrCtbBalPItsControle, QrCtbBalPItsControle.Value);
    ReopenCtbBalPIts('', Controle);
  end;
end;

procedure TFmCtbBalPCab.RGTpAmbitoClick(Sender: TObject);
begin
  ReopenCtbBalPIts('', 0);
end;

procedure TFmCtbBalPCab.RecriaItensCarteiras();
var
  Carteira, GenCtb, Controle: Integer;
  Credito, Debito: Double;
  //
  procedure InsereLctCtbCartAtual();
  var
    APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
    BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt: String;
    Codigo, TpBalanc, TpAmbito,
    APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod,
    BPlaCod, BCjtCod, BGruCod, BSgrCod, BCtaCod: Integer;
    //Credito, Debito: Double;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    Codigo         := QrCtbBalPCabCodigo.Value;
    Controle       := UMyMod.BPGS1I32('ctbbalpits', 'Controle', '', '', tsPos, SQLType, 0);
    TpBalanc       := Integer(TContabTpBalanc.ctbOutrCtb);
    TpAmbito       := Integer(TContabTpAmbito.ctaSdoCart);
    //Credito        := ;
    //Debito         := ;
    APlaCod        := 0; //QrCrtsAPlaCod.Value;
    ACjtCod        := 0; //QrCrtsACjtCod.Value;
    AGruCod        := 0; //QrCrtsAGruCod.Value;
    ASgrCod        := 0; //QrCrtsASgrCod.Value;
    ACtaCod        := 0; //QrCrtsACtaCod.Value;
    BPlaCod        := QrCrtsBPlaCod.Value;
    BCjtCod        := QrCrtsBCjtCod.Value;
    BGruCod        := QrCrtsBGruCod.Value;
    BSgrCod        := QrCrtsBSgrCod.Value;
    BCtaCod        := QrCrtsBCtaCod.Value;
    APlaTxt        := ''; //QrCrtsAPlaTxt.Value;
    ACjtTxt        := ''; //QrCrtsACjtTxt.Value;
    AGruTxt        := ''; //QrCrtsAGruTxt.Value;
    ASgrTxt        := ''; //QrCrtsASgrTxt.Value;
    ACtaTxt        := ''; //QrCrtsACtaTxt.Value;
    BPlaTxt        := QrCrtsBPlaTxt.Value;
    BCjtTxt        := QrCrtsBCjtTxt.Value;
    BGruTxt        := QrCrtsBGruTxt.Value;
    BSgrTxt        := QrCrtsBSgrTxt.Value;
    BCtaTxt        := QrCrtsBCtaTxt.Value;
    //
    InsereCtbBalPIts(SQLType, Codigo, Controle, TpBalanc, TpAmbito, Credito,
    Debito, APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod, BPlaCod, BCjtCod,
    BGruCod, BSgrCod, BCtaCod, APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
    BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt);
  end;
  //
  procedure ReopenCarteirasNao(sTipos: String);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
    'SELECT ',
    '   GROUP_CONCAT(Codigo SEPARATOR ", ") Corda',
    'FROM carteiras',
    'WHERE Tipo IN (' + sTipos + ')',
    'AND Codigo > 0',
    'AND GenCtb=0',
    '']);
  end;
  //
  procedure ReopenCarteirasSim(sTipos: String);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCrts, Dmod.MyDB, [
    'SELECT crt.Codigo Carteira, ',
    'crt.GenCtb BCtaCod, bcjt.Plano BPlaCod,  ',
    'bgru.Conjunto BCjtCod, bsgr.Grupo BGruCod,  ',
    'bcta.SubGrupo BSgrCod, ',
    'bpla.Nome BPlaTxt,  ',
    'bcjt.Nome BCjtTxt, bgru.Nome BGruTxt,  ',
    'bsgr.Nome BSgrTxt, bcta.Nome BCtaTxt ',
    'FROM ' + TMeuDB + '.carteiras crt ',
    'LEFT JOIN ' + TMeuDB + '.contas    bcta ON bcta.Codigo=crt.GenCtb ',
    'LEFT JOIN ' + TMeuDB + '.subgrupos bsgr ON bsgr.Codigo=bcta.SubGrupo ',
    'LEFT JOIN ' + TMeuDB + '.grupos    bgru ON bgru.Codigo=bsgr.Grupo ',
    'LEFT JOIN ' + TMeuDB + '.conjuntos bcjt ON bcjt.Codigo=bgru.Conjunto ',
    'LEFT JOIN ' + TMeuDB + '.plano     bpla ON bpla.Codigo=bcjt.Plano ',
    'WHERE crt.Tipo IN (' + sTipos + ')',
    'AND crt.Codigo > 0',
    'AND crt.GenCtb <> 0',
    'ORDER BY crt.GenCtb ',
    '']);
  end;
var
  Corda, sCarteira, sGenCt: String;
  Saldo: Double;
begin
  Controle := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ctbbalpits WHERE Codigo=' + FsCodigo +
    ' AND TpAmbito=' + Geral.FF0(Integer(TContabTpAmbito.ctaSdoCart))
  );
  //
  //////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando lan�amentos de saldos de carteiras.');
  //////////////////////////////////////////////////////////////////////////////
  //
/////////////////// Caretiras 0 e 1 - Bancos e Caixas //////////////////////////
  ReopenCarteirasNao('0,1');
  if QrAux.RecordCount > 0 then
  begin
    Corda := QrAux.FieldByName('Corda').AsString;
    if Trim(Corda) <> EmptyStr then
      Geral.MB_Aviso('As carteiras do tipo Banco e Caixa n� ' + Corda +
      ' n�o tem a conta cont�bil do plano de contas definido em seu cadastro!' +
      sLineBreak +
      '� necess�rio completar o cadastro par se ter um relat�rio completo!');
  end;
  ReopenCarteirasSim('0,1');
  QrCrts.First;
  while not QrCrts.Eof do
  begin
    Carteira := QrCrtsCarteira.Value;
    GenCtb   := QrCrtsBCtaCod.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
    'SELECT SUM(Credito - Debito) Saldo ',
    'FROM ' + FTabLctA,
    'WHERE Carteira=' + Geral.FF0(Carteira),
    'AND Data < "' + FsDiaSeguinte + '"',
    '']);
    //
    Saldo := QrAux.FieldByName('Saldo').AsFloat;
    if Saldo > 0 then
    begin
      Debito  := Saldo;
      Credito := 0;
    end else
    begin
      Debito  := 0;
      Credito := -Saldo;
    end;
    InsereLctCtbCartAtual();
    //
    QrCrts.Next;
  end;
  //
/////////////////// FIM Caretiras 0 e 1 - Bancos e Caixas //////////////////////
  //
//////////////////////// Inicio Caretiras 2 - Emissoes /////////////////////////
  //
  ReopenCarteirasNao('2');
  if QrAux.RecordCount > 0 then
  begin
    Corda := QrAux.FieldByName('Corda').AsString;
    if Trim(Corda) <> EmptyStr then
      Geral.MB_Aviso('As carteiras do tipo emiss�o n� ' + Corda +
      ' n�o tem a conta cont�bil do plano de contas definido em seu cadastro!'
      );
  end;
  ReopenCarteirasSim('2');
  QrCrts.First;
  while not QrCrts.Eof do
  begin
    Carteira := QrCrtsCarteira.Value;
    GenCtb   := QrCrtsBCtaCod.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
    'SELECT SUM(Credito - Debito - Pago) Saldo ',  // muda aqui
    'FROM ctbbalplctopn ', // muda aqui
    'WHERE Carteira=' + Geral.FF0(Carteira),
    //'AND Data < "' + FsDiaSeguinte + '"',  // muda aqui
    '']);
    //
    Saldo := QrAux.FieldByName('Saldo').AsFloat;
    if Saldo > 0 then
    begin
      Debito  := Saldo;
      Credito := 0;
    end else
    begin
      Debito  := 0;
      Credito := -Saldo;
    end;
    InsereLctCtbCartAtual();
    //
    QrCrts.Next;
  end;
  //
///////////////////////// FIM Caretiras 2 - Emiss�es ///////////////////////////
  //
  ReopenCtbBalPIts('', Controle);
end;

procedure TFmCtbBalPCab.RecriaItensCarteirasTipo0e1();
var
  Carteira, GenCtb, Controle: Integer;
  Credito, Debito: Double;
  //
  procedure InsereLctCtbCartAtual();
  var
    APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
    BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt: String;
    Codigo, TpBalanc, TpAmbito,
    APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod,
    BPlaCod, BCjtCod, BGruCod, BSgrCod, BCtaCod: Integer;
    //Credito, Debito: Double;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    Codigo         := QrCtbBalPCabCodigo.Value;
    Controle       := UMyMod.BPGS1I32('ctbbalpits', 'Controle', '', '', tsPos, SQLType, 0);
    TpBalanc       := Integer(TContabTpBalanc.ctbOutrCtb);
    TpAmbito       := Integer(TContabTpAmbito.ctaSdoCart);
    //Credito        := ;
    //Debito         := ;
    APlaCod        := 0; //QrCrtsAPlaCod.Value;
    ACjtCod        := 0; //QrCrtsACjtCod.Value;
    AGruCod        := 0; //QrCrtsAGruCod.Value;
    ASgrCod        := 0; //QrCrtsASgrCod.Value;
    ACtaCod        := 0; //QrCrtsACtaCod.Value;
    BPlaCod        := QrCrtsBPlaCod.Value;
    BCjtCod        := QrCrtsBCjtCod.Value;
    BGruCod        := QrCrtsBGruCod.Value;
    BSgrCod        := QrCrtsBSgrCod.Value;
    BCtaCod        := QrCrtsBCtaCod.Value;
    APlaTxt        := ''; //QrCrtsAPlaTxt.Value;
    ACjtTxt        := ''; //QrCrtsACjtTxt.Value;
    AGruTxt        := ''; //QrCrtsAGruTxt.Value;
    ASgrTxt        := ''; //QrCrtsASgrTxt.Value;
    ACtaTxt        := ''; //QrCrtsACtaTxt.Value;
    BPlaTxt        := QrCrtsBPlaTxt.Value;
    BCjtTxt        := QrCrtsBCjtTxt.Value;
    BGruTxt        := QrCrtsBGruTxt.Value;
    BSgrTxt        := QrCrtsBSgrTxt.Value;
    BCtaTxt        := QrCrtsBCtaTxt.Value;
    //
    InsereCtbBalPIts(SQLType, Codigo, Controle, TpBalanc, TpAmbito, Credito,
    Debito, APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod, BPlaCod, BCjtCod,
    BGruCod, BSgrCod, BCtaCod, APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
    BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt);
  end;
var
  Corda, sCarteira, sGenCt: String;
  Saldo: Double;
begin
  Controle := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ctbbalpits WHERE Codigo=' + FsCodigo +
    ' AND TpAmbito=' + Geral.FF0(Integer(TContabTpAmbito.ctaSdoCart))
  );
  //
  //////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando lan�amentos de saldos de carteiras.');
  //////////////////////////////////////////////////////////////////////////////
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT ',
  '   GROUP_CONCAT(Codigo SEPARATOR ", ") Corda',
  'FROM carteiras',
  'WHERE Tipo IN (0, 1)',
  'AND Codigo > 0',
  'AND GenCtb=0',
  '']);
  if QrAux.RecordCount > 0 then
  begin
    Corda := QrAux.FieldByName('Corda').AsString;
    if Trim(Corda) <> EmptyStr then
      Geral.MB_Aviso('As carteiras ' + Corda +
      ' n�o tem a conta cont�bil do plano de contas definido em seu cadastro!' +
      sLineBreak +
      '� necess�rio completar o cadastro par se ter um relat�rio completo!');
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCrts, Dmod.MyDB, [
  'SELECT crt.Codigo Carteira, ',
  'crt.GenCtb BCtaCod, bcjt.Plano BPlaCod,  ',
  'bgru.Conjunto BCjtCod, bsgr.Grupo BGruCod,  ',
  'bcta.SubGrupo BSgrCod, ',
  'bpla.Nome BPlaTxt,  ',
  'bcjt.Nome BCjtTxt, bgru.Nome BGruTxt,  ',
  'bsgr.Nome BSgrTxt, bcta.Nome BCtaTxt ',
  'FROM ' + TMeuDB + '.carteiras crt ',
  'LEFT JOIN ' + TMeuDB + '.contas    bcta ON bcta.Codigo=crt.GenCtb ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos bsgr ON bsgr.Codigo=bcta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    bgru ON bgru.Codigo=bsgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos bcjt ON bcjt.Codigo=bgru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     bpla ON bpla.Codigo=bcjt.Plano ',
  'WHERE crt.Tipo IN (0, 1)',
  'AND crt.Codigo > 0',
  'AND crt.GenCtb <> 0',
  'ORDER BY crt.GenCtb ',
  '']);
  QrCrts.First;
  while not QrCrts.Eof do
  begin
    Carteira := QrCrtsCarteira.Value;
    GenCtb   := QrCrtsBCtaCod.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
    'SELECT SUM(Credito - Debito) Saldo ',
    'FROM ' + FTabLctA,
    'WHERE Carteira=' + Geral.FF0(Carteira),
    'AND Data < "' + FsDiaSeguinte + '"',
    '']);
    //
    Saldo := QrAux.FieldByName('Saldo').AsFloat;
    if Saldo > 0 then
    begin
      Debito  := 0; //Saldo;
      Credito := Saldo;
    end else
    begin
      Debito  := -Saldo; //0;
      Credito := 0; //-Saldo;
    end;
    InsereLctCtbCartAtual();
    //
    QrCrts.Next;
  end;
  ReopenCtbBalPIts('', Controle);
end;

procedure TFmCtbBalPCab.RecriaItensCarteirasTipo2();
var
  Carteira, GenCtb, Controle: Integer;
  Credito, Debito: Double;
  //
  procedure InsereLctCtbCartAtual();
  var
    APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
    BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt: String;
    Codigo, TpBalanc, TpAmbito,
    APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod,
    BPlaCod, BCjtCod, BGruCod, BSgrCod, BCtaCod: Integer;
    //Credito, Debito: Double;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    Codigo         := QrCtbBalPCabCodigo.Value;
    Controle       := UMyMod.BPGS1I32('ctbbalpits', 'Controle', '', '', tsPos, SQLType, 0);
    TpBalanc       := Integer(TContabTpBalanc.ctbOutrCtb);
    TpAmbito       := Integer(TContabTpAmbito.ctaSdoCart);
    //Credito        := ;
    //Debito         := ;
    APlaCod        := 0; //QrCrtsAPlaCod.Value;
    ACjtCod        := 0; //QrCrtsACjtCod.Value;
    AGruCod        := 0; //QrCrtsAGruCod.Value;
    ASgrCod        := 0; //QrCrtsASgrCod.Value;
    ACtaCod        := 0; //QrCrtsACtaCod.Value;
    BPlaCod        := QrCrtsBPlaCod.Value;
    BCjtCod        := QrCrtsBCjtCod.Value;
    BGruCod        := QrCrtsBGruCod.Value;
    BSgrCod        := QrCrtsBSgrCod.Value;
    BCtaCod        := QrCrtsBCtaCod.Value;
    APlaTxt        := ''; //QrCrtsAPlaTxt.Value;
    ACjtTxt        := ''; //QrCrtsACjtTxt.Value;
    AGruTxt        := ''; //QrCrtsAGruTxt.Value;
    ASgrTxt        := ''; //QrCrtsASgrTxt.Value;
    ACtaTxt        := ''; //QrCrtsACtaTxt.Value;
    BPlaTxt        := QrCrtsBPlaTxt.Value;
    BCjtTxt        := QrCrtsBCjtTxt.Value;
    BGruTxt        := QrCrtsBGruTxt.Value;
    BSgrTxt        := QrCrtsBSgrTxt.Value;
    BCtaTxt        := QrCrtsBCtaTxt.Value;
    //
    InsereCtbBalPIts(SQLType, Codigo, Controle, TpBalanc, TpAmbito, Credito,
    Debito, APlaCod, ACjtCod, AGruCod, ASgrCod, ACtaCod, BPlaCod, BCjtCod,
    BGruCod, BSgrCod, BCtaCod, APlaTxt, ACjtTxt, AGruTxt, ASgrTxt, ACtaTxt,
    BPlaTxt, BCjtTxt, BGruTxt, BSgrTxt, BCtaTxt);
  end;
var
  Corda, sCarteira, sGenCt: String;
  Saldo: Double;
begin
  Controle := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ctbbalpits WHERE Codigo=' + FsCodigo +
    ' AND TpAmbito=' + Geral.FF0(Integer(TContabTpAmbito.ctaSdoCart))
  );
  //
  //////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando lan�amentos de saldos de carteiras.');
  //////////////////////////////////////////////////////////////////////////////
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT ',
  '   GROUP_CONCAT(Codigo SEPARATOR ", ") Corda',
  'FROM carteiras',
  'WHERE Tipo IN (2)', // Difere aqui!
  'AND Codigo > 0',
  'AND GenCtb=0',
  '']);
  if QrAux.RecordCount > 0 then
  begin
    Corda := QrAux.FieldByName('Corda').AsString;
    if Trim(Corda) <> EmptyStr then
      Geral.MB_Aviso('As carteiras do tipo emiss�o n� ' + Corda +
      ' n�o tem a conta cont�bil do plano de contas definido em seu cadastro!');
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCrts, Dmod.MyDB, [
  'SELECT crt.Codigo Carteira, ',
  'crt.GenCtb BCtaCod, bcjt.Plano BPlaCod,  ',
  'bgru.Conjunto BCjtCod, bsgr.Grupo BGruCod,  ',
  'bcta.SubGrupo BSgrCod, ',
  'bpla.Nome BPlaTxt,  ',
  'bcjt.Nome BCjtTxt, bgru.Nome BGruTxt,  ',
  'bsgr.Nome BSgrTxt, bcta.Nome BCtaTxt ',
  'FROM ' + TMeuDB + '.carteiras crt ',
  'LEFT JOIN ' + TMeuDB + '.contas    bcta ON bcta.Codigo=crt.GenCtb ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos bsgr ON bsgr.Codigo=bcta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    bgru ON bgru.Codigo=bsgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos bcjt ON bcjt.Codigo=bgru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     bpla ON bpla.Codigo=bcjt.Plano ',
  'WHERE crt.Tipo IN (2)',  // Difere aqui!
  'AND crt.Codigo > 0',
  'AND crt.GenCtb <> 0',
  'ORDER BY crt.GenCtb ',
  '']);
  QrCrts.First;
  while not QrCrts.Eof do
  begin
    Carteira := QrCrtsCarteira.Value;
    GenCtb   := QrCrtsBCtaCod.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
    'SELECT SUM(Credito - Debito - Pago) Saldo ',  // muda aqui
    'FROM ctbbalplctopn ', // muda aqui
    'WHERE Carteira=' + Geral.FF0(Carteira),
    //'AND Data < "' + FsDiaSeguinte + '"',  // muda aqui
    '']);
    //
    Saldo := QrAux.FieldByName('Saldo').AsFloat;
    if Saldo > 0 then
    begin
      Debito  := Saldo;
      Credito := 0;
    end else
    begin
      Debito  := 0;
      Credito := -Saldo;
    end;
    InsereLctCtbCartAtual();
    //
    QrCrts.Next;
  end;
  ReopenCtbBalPIts('', Controle);
end;

procedure TFmCtbBalPCab.RecriaItensFinanceiro();
const
  sInserindoFin = 'Inserindo lan�amentos financeiros. ';
var
  QtdFlds: Integer;
  TabTxt, FldGen, CamposDst: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ctbbalpits WHERE Codigo=' + FsCodigo +
    ' AND TpAmbito=' + Geral.FF0(Integer(TContabTpAmbito.ctaLctGenOpn))
  );
  //
  //
  //
  TabTxt := 'ctbbalplctopn';
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt + '. Copiando dados da origem (dados atuais).');
  CamposDst := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabTxt, 'lct.', QtdFlds);
  CamposDst := StringReplace(CamposDst, ' lct.Codigo', ' ' + FsCodigo + ' Codigo', [rfReplaceAll, rfIgnoreCase]);
  //
  //////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando se h� lan�amentos orf�os.');
  //////////////////////////////////////////////////////////////////////////////
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrfaos, Dmod.MyDB, [
  'SELECT cjt.Plano, gru.Conjunto, sgr.Grupo, ',
  'cta.Subgrupo, lct.Genero, cta.Nome NO_Cta, ',
  'lct.Data, lct.Tipo, lct.Carteira, lct.Controle, ',
  'lct.Sub, lct.Debito, lct.Credito, ',
  'lct.Compensado, lct.Sit, lct.Vencimento, lct.ID_Pgto, ',
  'lct.ID_Quit, lct.ID_Sub, lct.Pago, lct.GenCtb, ',
  'lct.QtDtPg, lct.PagMul, lct.PagJur , lct.RecDes,',
  'pla.Nome NO_Pla, cjt.Nome NO_Cjt, ',
  'gru.Nome NO_Gru, sgr.Nome NO_Sgr,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,',
  'crt.Nome NO_CARTEIRA',
  'FROM ' + FTabLctA + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=',
  '  IF(lct.Cliente <> 0, lct.Cliente, lct.Fornecedor)',
  'LEFT JOIN ' + TMeuDB + '.carteiras crt ON crt.Codigo=lct.Carteira  ',
  'LEFT JOIN ' + TMeuDB + '.contas    cta ON cta.Codigo=lct.Genero ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=cta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     pla ON pla.Codigo=cjt.Plano ',
  'WHERE crt.ForneceI=' + FsEmpresa,
  'AND lct.Genero > 0 ',
  'AND ( ',
  // Lan�amentos a prazo
  '  lct.Tipo = 2  ',
  '  AND lct.Data < "' + FsDiaSeguinte + '"  ',
  //'  AND ((lct.Data >= "' + FsPrimeiroDia + '") AND (lct.Data < "' + FsDiaSeguinte + '")) ',
  '  AND (  ',
  '    lct.Sit < 2  ',
  '    OR  ',
  '    lct.Compensado >= "' + FsDiaSeguinte + '"  ',
  '  ) ',
  // lancamentos � vista
(*
  '  OR ( ',
  '    (lct.Tipo < 2) ',
  '    AND ',
  '    ((lct.Data >= "' + FsPrimeiroDia + '") AND (lct.Data < "' + FsDiaSeguinte + '")) ',
  '  )',
*)
  ') ',
  // lan�amentos sem GenCtb
  'AND lct.GenCtb = 0 ',
  'ORDER BY NO_Cta, NO_ENT',
  '']);
  if QrOrfaos.RecordCount > 0 then
  begin
    // Mostra form para editar lan�amentos orf�os
    //Geral.MB_Teste(QrOrfaos.SQL.Text);
    MostraFormCtbBalPLctEdit(QrOrfaos.SQL.Text);
    if VAR_USUARIO > 0 then
      Exit;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt + '. Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ' + TabTxt + ' WHERE Codigo=' + FsCodigo);
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt + '. Gerando lan�amentos de emiss�es em aberto no final do per�odo.');
  //
  //////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  Geral.ATS([
  'INSERT INTO ' + TabTxt,
  'SELECT ',
  CamposDst,
  'FROM ' + FTabLctA + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.carteiras crt ON crt.Codigo=lct.Carteira  ',
  'WHERE crt.ForneceI=' + FsEmpresa,
  'AND lct.Genero > 0 ',
  'AND ( ',
  '  lct.Tipo = 2  ',
  '  AND lct.Data < "' + FsDiaSeguinte + '"  ',
  '  AND (  ',
  '    lct.Sit < 2  ',
  '    OR  ',
  '    lct.Compensado >= "' + FsDiaSeguinte + '"  ',
  '  ) ',
  ') ',
  '']));
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt + '. Alterando dados para ficarem na situa��o que estavam no fim do per�odo do balan�o.');
  //
  //////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + TabTxt,
  'WHERE QtDtPg > 1 ',
  '']);
  QrAux.First;
  while not QrAux.Eof do
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt +
    '. Controle ' + Geral.FF0(QrAux.FieldByName('Controle').AsInteger) +
    '. Alterando dados para ficarem na situa��o no fim do per�odo do balan�o.');
    //
    AtualizaPagoParcial(
      QrAux.FieldByName('Controle').AsInteger,
      QrAux.FieldByName('Sub').AsInteger,
      QrAux.FieldByName('Credito').AsFloat,
      QrAux.FieldByName('Debito').AsFloat,
      QrAux.FieldByName('Compensado').AsDateTime,
      FsDiaSeguinte
    );
    QrAux.First;
    //
    QrAux.Next;
  end;

  //////////////////////////////////////////////////////////////////////////////
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Totais de lan�amentos financeiros');
  //
  //////////////////////////////////////////////////////////////////////////////
  FldGen := 'Genero';
  UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, Dmod.MyDB, [
  'SELECT SUM(lct.Credito) Credito, SUM(lct.Debito) Debito,  ',
  'acjt.Plano APlaCod, agru.Conjunto ACjtCod, ',
  'asgr.Grupo AGruCod, acta.Subgrupo ASgrCod,  ',
  'lct.Genero ACtaCod, bcjt.Plano BPlaCod,  ',
  'bgru.Conjunto BCjtCod, bsgr.Grupo BGruCod,  ',
  'bcta.SubGrupo BSgrCod, lct.GenCtb BCtaCod,  ',
  'apla.Nome APlaTxt, acjt.Nome ACjtTxt,  ',
  'agru.Nome AGruTxt, asgr.Nome ASgrTxt,  ',
  'acta.Nome ACtaTxt, bpla.Nome BPlaTxt,  ',
  'bcjt.Nome BCjtTxt, bgru.Nome BGruTxt,  ',
  'bsgr.Nome BSgrTxt, bcta.Nome BCtaTxt ',
  'FROM ' + TabTxt + ' lct  ',
  'LEFT JOIN ' + TMeuDB + '.carteiras crt ON crt.Codigo=lct.Carteira ',

  'LEFT JOIN ' + TMeuDB + '.contas    acta ON acta.Codigo=lct.Genero ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos asgr ON asgr.Codigo=acta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    agru ON agru.Codigo=asgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos acjt ON acjt.Codigo=agru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     apla ON apla.Codigo=acjt.Plano ',

  'LEFT JOIN ' + TMeuDB + '.contas    bcta ON bcta.Codigo=lct.GenCtb ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos bsgr ON bsgr.Codigo=bcta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    bgru ON bgru.Codigo=bsgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos bcjt ON bcjt.Codigo=bgru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     bpla ON bpla.Codigo=bcjt.Plano ',

  'WHERE lct.Codigo=' + FsCodigo,
  'AND crt.ForneceI=' + FsEmpresa,
  'AND lct.Codigo=' + FsCodigo,
  'AND lct.Data < "' + FsDiaSeguinte + '" ',
  'AND lct.Tipo = 2 ',
  'AND ( ',
  '  lct.Sit < 2 ',
  '  OR ',
  '  lct.Compensado >= "' + FsDiaSeguinte + '" ',
  ') ',
  'GROUP BY lct.' + FldGen,
  'ORDER BY lct.' + FldGen,
  '']);
  //
  //Geral.MB_Teste(QrLCts.SQL.Text);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, sInserindoFin);
  PB1.Position := 0;
  PB1.Max := QrLCts.RecordCount;
  QrLCts.First;
  while not QrLCts.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, sInserindoFin +
    IntToStr(QrLCts.RecNo));
    //
    InsereLctOpnGeneroAtual();
    //
    QrLCts.Next;
  end;
  //
(*
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ctbbalpits WHERE Codigo=' + sCodigo +
    ' AND TpAmbito=' + Geral.FF0(Integer(TContabTpAmbito.ctaLct...))
  );
*)
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Totais de lan�amentos cont�beis');
  //
  //////////////////////////////////////////////////////////////////////////////
  FldGen := 'GenCtb';
  UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, Dmod.MyDB, [
  'SELECT SUM(lct.Credito) Credito, SUM(lct.Debito) Debito,  ',
  'acjt.Plano APlaCod, agru.Conjunto ACjtCod, ',
  'asgr.Grupo AGruCod, acta.Subgrupo ASgrCod,  ',
  'lct.Genero ACtaCod, bcjt.Plano BPlaCod,  ',
  'bgru.Conjunto BCjtCod, bsgr.Grupo BGruCod,  ',
  'bcta.SubGrupo BSgrCod, lct.GenCtb BCtaCod,  ',
  'apla.Nome APlaTxt, acjt.Nome ACjtTxt,  ',
  'agru.Nome AGruTxt, asgr.Nome ASgrTxt,  ',
  'acta.Nome ACtaTxt, bpla.Nome BPlaTxt,  ',
  'bcjt.Nome BCjtTxt, bgru.Nome BGruTxt,  ',
  'bsgr.Nome BSgrTxt, bcta.Nome BCtaTxt ',
  'FROM ' + TabTxt + ' lct  ',
  'LEFT JOIN ' + TMeuDB + '.carteiras crt ON crt.Codigo=lct.Carteira ',

  'LEFT JOIN ' + TMeuDB + '.contas    acta ON acta.Codigo=lct.Genero ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos asgr ON asgr.Codigo=acta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    agru ON agru.Codigo=asgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos acjt ON acjt.Codigo=agru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     apla ON apla.Codigo=acjt.Plano ',

  'LEFT JOIN ' + TMeuDB + '.contas    bcta ON bcta.Codigo=lct.GenCtb ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos bsgr ON bsgr.Codigo=bcta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    bgru ON bgru.Codigo=bsgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos bcjt ON bcjt.Codigo=bgru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     bpla ON bpla.Codigo=bcjt.Plano ',

  'WHERE lct.Codigo=' + FsCodigo,
  'AND crt.ForneceI=' + FsEmpresa,
  'AND lct.Codigo=' + FsCodigo,
  'AND lct.Data < "' + FsDiaSeguinte + '" ',
  'AND lct.Tipo = 2 ',
  'AND ( ',
  '  lct.Sit < 2 ',
  '  OR ',
  '  lct.Compensado >= "' + FsDiaSeguinte + '" ',
  ') ',
  'GROUP BY lct.' + FldGen,
  'ORDER BY lct.' + FldGen,
  '']);
  //
  //Geral.MB_Teste(QrLCts.SQL.Text);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, sInserindoFin);
  PB1.Position := 0;
  PB1.Max := QrLCts.RecordCount;
  QrLCts.First;
  while not QrLCts.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, sInserindoFin +
    IntToStr(QrLCts.RecNo));
    //
    InsereLctOpnGenCtbAtual();
    //
    QrLCts.Next;
  end;
  // ...
  //
  ReopenCtbBalPIts('', 0);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmCtbBalPCab.RecriaItens_Todos();
begin
  RecriaItensFinanceiro();
  RecriaItensCarteirasTipo0e1();
  RecriaTotaisGeneroPeriodo();
end;

procedure TFmCtbBalPCab.RecriaTotaisGeneroPeriodo();
const
  sInserindoFin = 'Inserindo lan�amentos financeiros emitidos. ';
var
  QtdFlds: Integer;
  TabTxt, FldGen, CamposDst: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ctbbalpits WHERE Codigo=' + FsCodigo +
    ' AND TpAmbito=' + Geral.FF0(Integer(TContabTpAmbito.ctaLctGenEmi))
  );
  //
  //
  //
  TabTxt := 'ctbbalplctemi';
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt + '. Copiando dados da origem (dados atuais).');
  CamposDst := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabTxt, 'lct.', QtdFlds);
  CamposDst := StringReplace(CamposDst, ' lct.Codigo', ' ' + FsCodigo + ' Codigo', [rfReplaceAll, rfIgnoreCase]);
  //
  //////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando se h� lan�amentos orf�os.');
  //////////////////////////////////////////////////////////////////////////////
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrfaos, Dmod.MyDB, [
  'SELECT cjt.Plano, gru.Conjunto, sgr.Grupo, ',
  'cta.Subgrupo, lct.Genero, cta.Nome NO_Cta, ',
  'lct.Data, lct.Tipo, lct.Carteira, lct.Controle, ',
  'lct.Sub, lct.Debito, lct.Credito, ',
  'lct.Compensado, lct.Sit, lct.Vencimento, lct.ID_Pgto, ',
  'lct.ID_Quit, lct.ID_Sub, lct.Pago, lct.GenCtb, ',
  'lct.QtDtPg, lct.PagMul, lct.PagJur , lct.RecDes,',
  'pla.Nome NO_Pla, cjt.Nome NO_Cjt, ',
  'gru.Nome NO_Gru, sgr.Nome NO_Sgr,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,',
  'crt.Nome NO_CARTEIRA',
  'FROM ' + FTabLctA + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=',
  '  IF(lct.Cliente <> 0, lct.Cliente, lct.Fornecedor)',
  'LEFT JOIN ' + TMeuDB + '.carteiras crt ON crt.Codigo=lct.Carteira  ',
  'LEFT JOIN ' + TMeuDB + '.contas    cta ON cta.Codigo=lct.Genero ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=cta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     pla ON pla.Codigo=cjt.Plano ',
  'WHERE crt.ForneceI=' + FsEmpresa,
  'AND lct.Genero > 0 ',
  //
  '  AND (lct.Data >= "' + FsPrimeiroDia + '" AND  lct.Data < "' + FsDiaSeguinte + '")  ',
  // lan�amentos emitidos no m�s
  'AND ( ',
  '  (lct.Tipo = 2) ',
(*
  '  OR ',
  '  (lct.Tipo <> 2 AND lct.ID_Pgto=0) ',
*)
  ') ',
  //
  // lan�amentos sem GenCtb
  'AND lct.GenCtb = 0 ',
  'ORDER BY NO_Cta, NO_ENT',
  '']);
  if QrOrfaos.RecordCount > 0 then
  begin
    // Mostra form para edira lan�amentos orf�os
    //Geral.MB_Teste(QrOrfaos.SQL.Text);
    MostraFormCtbBalPLctEdit(QrOrfaos.SQL.Text);
    Exit;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt + '. Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ' + TabTxt + ' WHERE Codigo=' + FsCodigo);
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt + '. Gerando lan�amentos emitidos no per�odo.');
  //
  //////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  Geral.ATS([
  'INSERT INTO ' + TabTxt,
  'SELECT ',
  CamposDst,
  'FROM ' + FTabLctA + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.carteiras crt ON crt.Codigo=lct.Carteira  ',
  'WHERE crt.ForneceI=' + FsEmpresa,
  'AND lct.Genero > 0 ',
  //
  '  AND (lct.Data >= "' + FsPrimeiroDia + '" AND  lct.Data < "' + FsDiaSeguinte + '")  ',
  // lan�amentos emitidos no m�s
  'AND ( ',
  '  (lct.Tipo = 2) ',
  '  OR ',
  '  (lct.Tipo <> 2 AND lct.ID_Pgto=0) ',
  ') ',
  '']));
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt + '. Alterando dados para ficarem na situa��o que estavam no fim do per�odo do balan�o.');
  //
  //////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + TabTxt,
  'WHERE QtDtPg > 1 ',
  '']);
  QrAux.First;
  while not QrAux.Eof do
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, TabTxt +
    '. Controle ' + Geral.FF0(QrAux.FieldByName('Controle').AsInteger) +
    '. Alterando dados para ficarem na situa��o no fim do per�odo do balan�o.');
    //
    AtualizaPagoParcial(
      QrAux.FieldByName('Controle').AsInteger,
      QrAux.FieldByName('Sub').AsInteger,
      QrAux.FieldByName('Credito').AsFloat,
      QrAux.FieldByName('Debito').AsFloat,
      QrAux.FieldByName('Compensado').AsDateTime,
      FsDiaSeguinte
    );
    QrAux.First;
    //
    QrAux.Next;
  end;









  //////////////////////////////////////////////////////////////////////////////
  FldGen := 'Genero';
  UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, Dmod.MyDB, [
  'SELECT SUM(lct.Credito) Credito, SUM(lct.Debito) Debito,  ',
  'acjt.Plano APlaCod, agru.Conjunto ACjtCod, ',
  'asgr.Grupo AGruCod, acta.Subgrupo ASgrCod,  ',
  'lct.Genero ACtaCod, bcjt.Plano BPlaCod,  ',
  'bgru.Conjunto BCjtCod, bsgr.Grupo BGruCod,  ',
  'bcta.SubGrupo BSgrCod, lct.GenCtb BCtaCod,  ',
  'apla.Nome APlaTxt, acjt.Nome ACjtTxt,  ',
  'agru.Nome AGruTxt, asgr.Nome ASgrTxt,  ',
  'acta.Nome ACtaTxt, bpla.Nome BPlaTxt,  ',
  'bcjt.Nome BCjtTxt, bgru.Nome BGruTxt,  ',
  'bsgr.Nome BSgrTxt, bcta.Nome BCtaTxt ',
  'FROM ' + TabTxt + ' lct  ',
  'LEFT JOIN ' + TMeuDB + '.carteiras crt ON crt.Codigo=lct.Carteira ',

  'LEFT JOIN ' + TMeuDB + '.contas    acta ON acta.Codigo=lct.Genero ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos asgr ON asgr.Codigo=acta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    agru ON agru.Codigo=asgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos acjt ON acjt.Codigo=agru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     apla ON apla.Codigo=acjt.Plano ',

  'LEFT JOIN ' + TMeuDB + '.contas    bcta ON bcta.Codigo=lct.GenCtb ',
  'LEFT JOIN ' + TMeuDB + '.subgrupos bsgr ON bsgr.Codigo=bcta.SubGrupo ',
  'LEFT JOIN ' + TMeuDB + '.grupos    bgru ON bgru.Codigo=bsgr.Grupo ',
  'LEFT JOIN ' + TMeuDB + '.conjuntos bcjt ON bcjt.Codigo=bgru.Conjunto ',
  'LEFT JOIN ' + TMeuDB + '.plano     bpla ON bpla.Codigo=bcjt.Plano ',

  'WHERE lct.Codigo=' + FsCodigo,
  'AND crt.ForneceI=' + FsEmpresa,
  'AND lct.Genero > 0 ',
  //
  '  AND (lct.Data >= "' + FsPrimeiroDia + '" AND  lct.Data < "' + FsDiaSeguinte + '")  ',
  // lan�amentos emitidos no m�s
  'AND ( ',
  '  (lct.Tipo = 2) ',
  '  OR ',
  '  (lct.Tipo <> 2 AND lct.ID_Pgto=0) ',
  ') ',
  'GROUP BY lct.Genero',
  'ORDER BY lct.Genero',
  '']);
  //
  //Geral.MB_Teste(QrLCts.SQL.Text);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, sInserindoFin);
  PB1.Position := 0;
  PB1.Max := QrLCts.RecordCount;
  QrLCts.First;
  while not QrLCts.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, sInserindoFin +
    IntToStr(QrLCts.RecNo));
    //
    InsereLctEmiGeneroAtual();
    //
    QrLCts.Next;
  end;
  //
  // ...
  //
  ReopenCtbBalPIts('', 0);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

  //
procedure TFmCtbBalPCab.ReopenCBPIE(SQL_Extra: String; Integrado: Boolean = False);
var
  SQL_TpAmbito, FldIntegeradoA, FldIntegeradoB: String;
begin
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE plano     SET Nome2=Nome WHERE Nome2="" OR Nome2 IS NULL');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE conjuntos SET Nome2=Nome WHERE Nome2="" OR Nome2 IS NULL');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE grupos    SET Nome2=Nome WHERE Nome2="" OR Nome2 IS NULL');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE subgrupos SET Nome2=Nome WHERE Nome2="" OR Nome2 IS NULL');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE contas    SET Nome2=Nome WHERE Nome2="" OR Nome2="Nulo"');
  //
{
  if IgnoraFiltro or (RGTpAmbito.ItemIndex = 0) then
}
    SQL_TpAmbito := ''
{
  else
    SQL_TpAmbito := 'AND TpAmbito=' + Geral.FF0(RGTpAmbito.ItemIndex)};
  //
  if Integrado then
  begin
    FldIntegeradoA := 'pla.FinContab';
    FldIntegeradoB := 'plb.FinContab';
  end else
  begin
    FldIntegeradoA := 'APlaCod';
    FldIntegeradoB := 'BPlaCod';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCBPIE, Dmod.MyDB, [
  'DROP TABLE IF EXISTS _Teste_CBPIE_; ',
  'CREATE TABLE _Teste_CBPIE_ ',
  //
  'SELECT IF(TpBalanc<>2, ',
  ' CONCAT(' + FldIntegeradoB + ', ".", LPAD(BCjtCod, 2, "0"), ".", LPAD(BGruCod, 2, "0"), ".", LPAD(BSgrCod, 3, "0"), ".", LPAD(BCtaCod, 4, "0")), ',
  ' CONCAT(' + FldIntegeradoA + ', ".", LPAD(ACjtCod, 2, "0"), ".", LPAD(AGruCod, 2, "0"), ".", LPAD(ASgrCod, 3, "0"), ".", LPAD(ACtaCod, 4, "0"))',
  ') NiveisTXT, ',
  '',
  'CAST(IF(TpBalanc<>2, ' + FldIntegeradoB + ', ' + FldIntegeradoA + ') AS UNSIGNED) XPlaCod,',
  'CAST(IF(TpBalanc<>2, BCjtCod, ACjtCod) AS UNSIGNED) XCjtCod,',
  'CAST(IF(TpBalanc<>2, BGruCod, AGruCod) AS UNSIGNED) XGruCod,',
  'CAST(IF(TpBalanc<>2, BSgrCod, ASgrCod) AS UNSIGNED) XSgrCod,',
  'CAST(IF(TpBalanc<>2, BCtaCod, ACtaCod) AS UNSIGNED) XCtaCod,',
  '',
  'IF(TpBalanc<>2, BPlaTxt, APlaTxt) XPlaTxt,',
  'IF(TpBalanc<>2, BCjtTxt, ACjtTxt) XCjtTxt,',
  'IF(TpBalanc<>2, BGruTxt, AGruTxt) XGruTxt,',
  'IF(TpBalanc<>2, BSgrTxt, ASgrTxt) XSgrTxt,',
  'IF(TpBalanc<>2, BCtaTxt, ACtaTxt) XCtaTxt,',
  '',
  'CAST(IF(TpBalanc<>2, plb.FinContab, pla.FinContab) AS UNSIGNED) ORD_PLA,',
  'CAST(IF(TpBalanc<>2, cjb.OrdemLista, cja.OrdemLista) AS UNSIGNED) ORD_CJT,',
  'CAST(IF(TpBalanc<>2, grb.OrdemLista, gra.OrdemLista) AS UNSIGNED) ORD_GRU,',
  'CAST(IF(TpBalanc<>2, sgb.OrdemLista, sga.OrdemLista) AS UNSIGNED) ORD_SGR,',
  '',
  'coa.Nome NO_CtaPlaA, cob.Nome NO_CtaPlaB,  ',
  'ELT(cbi.TpBalanc, "Movimento Cont�bil", "Movimento Financeiro",',
  ' "Manual Cont�bil", "? ? ? ? ? ? ?") NO_TpBalanc, ',
  'ELT(cbi.TpAmbito, "Lcts Abertos", "Lcts Emitidos", "Saldo Carteira", ',
  '"Valor Estoque", "Recup.Impostos", "Imobilizado", "Patrim.L�q", "?????") ',
  'NO_TpAmbito, cbi.* ',
  'FROM ctbbalpits cbi ',
  'LEFT JOIN contas coa ON coa.Codigo=cbi.ACtaCod ',
  'LEFT JOIN contas cob ON cob.Codigo=cbi.BCtaCod ',
  'LEFT JOIN plano  pla ON pla.Codigo=cbi.APlaCod ',
  'LEFT JOIN plano  plb ON plb.Codigo=cbi.BPlaCod ',

  'LEFT JOIN conjuntos  cja ON cja.Codigo=cbi.ACjtCod ',
  'LEFT JOIN conjuntos  cjb ON cjb.Codigo=cbi.BCjtCod ',
  'LEFT JOIN grupos  gra ON gra.Codigo=cbi.AGruCod ',
  'LEFT JOIN grupos  grb ON grb.Codigo=cbi.BGruCod ',
  'LEFT JOIN subgrupos  sga ON sga.Codigo=cbi.ASGRCod ',
  'LEFT JOIN subgrupos  sgb ON sgb.Codigo=cbi.BSGRCod ',


  'WHERE cbi.Codigo=' + Geral.FF0(QrCtbBalPCabCodigo.Value),
  SQL_Extra,
  SQL_TpAmbito,
  'ORDER BY NiveisTXT; ',
  'SELECT * FROM _Teste_CBPIE_',
  ' ']);
  //
  //Geral.MB_Teste(QrCBPIE.SQL.Text);
end;

procedure TFmCtbBalPCab.ReopenCBPIO(SQL_Extra: String);
begin
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE plano     SET Nome2=Nome WHERE Nome2="" OR Nome2 IS NULL');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE conjuntos SET Nome2=Nome WHERE Nome2="" OR Nome2 IS NULL');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE grupos    SET Nome2=Nome WHERE Nome2="" OR Nome2 IS NULL');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE subgrupos SET Nome2=Nome WHERE Nome2="" OR Nome2 IS NULL');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE contas    SET Nome2=Nome WHERE Nome2="" OR Nome2="Nulo"');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCBPIO, Dmod.MyDB, [
  'DROP TABLE IF EXISTS _Teste_CBPIO_; ',
  'CREATE TABLE _Teste_CBPIO_ ',
  //
  'SELECT IF(TpBalanc<>2, ',
  ' CONCAT(plb.FinContab, ".", LPAD(cjb.OrdemLista, 2, "0"), ".", LPAD(grb.OrdemLista, 2, "0"), ".", LPAD(sgb.OrdemLista, 3, "0"), ".", LPAD(BCtaCod, 4, "0")), ',
  ' CONCAT(pla.FinContab, ".", LPAD(cja.OrdemLista, 2, "0"), ".", LPAD(gra.OrdemLista, 2, "0"), ".", LPAD(sgb.OrdemLista, 3, "0"), ".", LPAD(ACtaCod, 4, "0"))',
  ') NiveisTXT, ',
  'CAST(IF(TpBalanc<>2, plb.FinContab, pla.FinContab) AS UNSIGNED) XPlaCod,',
  'CAST(IF(TpBalanc<>2, BCjtCod, ACjtCod) AS UNSIGNED) XCjtCod,',
  'CAST(IF(TpBalanc<>2, BGruCod, AGruCod) AS UNSIGNED) XGruCod,',
  'CAST(IF(TpBalanc<>2, BSgrCod, ASgrCod) AS UNSIGNED) XSgrCod,',
  'CAST(IF(TpBalanc<>2, BCtaCod, ACtaCod) AS UNSIGNED) XCtaCod,',
  '',
  'CAST(IF(TpBalanc<>2, plb.FinContab, pla.FinContab) AS UNSIGNED) ORD_PLA,',
  'CAST(IF(TpBalanc<>2, cjb.OrdemLista, cja.OrdemLista) AS UNSIGNED) ORD_CJT,',
  'CAST(IF(TpBalanc<>2, grb.OrdemLista, gra.OrdemLista) AS UNSIGNED) ORD_GRU,',
  'CAST(IF(TpBalanc<>2, sgb.OrdemLista, sga.OrdemLista) AS UNSIGNED) ORD_SGR,',
  '',
  'IF(TpBalanc<>2, plb.Nome2, pla.Nome2) XPlaTxt,',
  'IF(TpBalanc<>2, cjb.Nome2, cja.Nome2) XCjtTxt,',
  'IF(TpBalanc<>2, grb.Nome2, gra.Nome2) XGruTxt,',
  'IF(TpBalanc<>2, sgb.Nome2, sga.Nome2) XSgrTxt,',
  //'IF(TpBalanc<>2, coa.Nome2, cob.Nome2) XCtaTxt,',
  'IF(TpBalanc<>2, BCtaTxt, ACtaTxt) XCtaTxt,',
  'coa.Nome NO_CtaPlaA, cob.Nome NO_CtaPlaB,  ',
  'ELT(cbi.TpBalanc, "Movimento Cont�bil", "Movimento Financeiro",',
  ' "Manual Cont�bil", "? ? ? ? ? ? ?") NO_TpBalanc, ',
  'ELT(cbi.TpAmbito, "Lcts Abertos", "Lcts Emitidos", "Saldo Carteira", ',
  '"Valor Estoque", "Recup.Impostos", "Imobilizado", "Patrim.L�q", "?????") ',
  'NO_TpAmbito, SUM(cbi.Credito) CREDITO, SUM(cbi.Debito) DEBITO ',
  'FROM ctbbalpits cbi ',
  'LEFT JOIN contas coa ON coa.Codigo=cbi.ACtaCod ',
  'LEFT JOIN contas cob ON cob.Codigo=cbi.BCtaCod ',
  'LEFT JOIN plano  pla ON pla.Codigo=cbi.APlaCod ',
  'LEFT JOIN plano  plb ON plb.Codigo=cbi.BPlaCod ',
  'LEFT JOIN conjuntos  cja ON cja.Codigo=cbi.ACjtCod ',
  'LEFT JOIN conjuntos  cjb ON cjb.Codigo=cbi.BCjtCod ',
  'LEFT JOIN grupos  gra ON gra.Codigo=cbi.AGruCod ',
  'LEFT JOIN grupos  grb ON grb.Codigo=cbi.BGruCod ',
  'LEFT JOIN subgrupos  sga ON sga.Codigo=cbi.ASGRCod ',
  'LEFT JOIN subgrupos  sgb ON sgb.Codigo=cbi.BSGRCod ',
  'WHERE cbi.Codigo=' + Geral.FF0(QrCtbBalPCabCodigo.Value),
  SQL_Extra,
  '',
  'GROUP BY ORD_PLA, ORD_CJT, ORD_GRU, ORD_SGR, XCtaCod',
  'ORDER BY ORD_PLA, ORD_CJT, ORD_GRU, ORD_SGR, XCtaCod;',
  //'ORDER BY NiveisTXT; ',
  'SELECT * FROM _Teste_CBPIO_',
  ' ']);
  //Geral.MB_Teste(QrCBPIO.SQL.Text);
end;

procedure TFmCtbBalPCab.ReopenCtbBalPIts(SQL_Extra: String; Controle: Integer;
  IgnoraFiltro: Boolean = False; Integrado: Boolean = False);
var
  SQL_TpAmbito, FldIntegeradoA, FldIntegeradoB: String;
begin
  if IgnoraFiltro or (RGTpAmbito.ItemIndex = 0) then
    SQL_TpAmbito := ''
  else
    SQL_TpAmbito := 'AND TpAmbito=' + Geral.FF0(RGTpAmbito.ItemIndex);
  //
  if Integrado then
  begin
    FldIntegeradoA := 'pla.FinContab';
    FldIntegeradoB := 'plb.FinContab';
  end else
  begin
    FldIntegeradoA := 'APlaCod';
    FldIntegeradoB := 'BPlaCod';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtbBalPIts, Dmod.MyDB, [
  'SELECT IF(TpBalanc<>2, ',
  ' CONCAT(' + FldIntegeradoB + ', ".", LPAD(BCjtCod, 2, "0"), ".", LPAD(BGruCod, 2, "0"), ".", LPAD(BSgrCod, 3, "0"), ".", LPAD(BCtaCod, 4, "0")), ',
  ' CONCAT(' + FldIntegeradoA + ', ".", LPAD(ACjtCod, 2, "0"), ".", LPAD(AGruCod, 2, "0"), ".", LPAD(ASgrCod, 3, "0"), ".", LPAD(ACtaCod, 4, "0"))',
  ') NiveisTXT, ',
  '',
  'CAST(IF(TpBalanc<>2, ' + FldIntegeradoB + ', ' + FldIntegeradoA + ') AS UNSIGNED) XPlaCod,',
  'CAST(IF(TpBalanc<>2, BCjtCod, ACjtCod) AS UNSIGNED) XCjtCod,',
  'CAST(IF(TpBalanc<>2, BGruCod, AGruCod) AS UNSIGNED) XGruCod,',
  'CAST(IF(TpBalanc<>2, BSgrCod, ASgrCod) AS UNSIGNED) XSgrCod,',
  'CAST(IF(TpBalanc<>2, BCtaCod, ACtaCod) AS UNSIGNED) XCtaCod,',
  '',
  'IF(TpBalanc<>2, BPlaTxt, APlaTxt) XPlaTxt,',
  'IF(TpBalanc<>2, BCjtTxt, ACjtTxt) XCjtTxt,',
  'IF(TpBalanc<>2, BGruTxt, AGruTxt) XGruTxt,',
  'IF(TpBalanc<>2, BSgrTxt, ASgrTxt) XSgrTxt,',
  'IF(TpBalanc<>2, BCtaTxt, ACtaTxt) XCtaTxt,',
  '',
  '',
  'coa.Nome NO_CtaPlaA, cob.Nome NO_CtaPlaB,  ',
  'ELT(cbi.TpBalanc, "Movimento Cont�bil", "Movimento Financeiro",',
  ' "Manual Cont�bil", "? ? ? ? ? ? ?") NO_TpBalanc, ',
  'ELT(cbi.TpAmbito, "Lcts Abertos", "Lcts Emitidos", "Saldo Carteira", ',
  '"Valor Estoque", "Recup.Impostos", "Imobilizado", "Patrim.L�q", "?????") ',
  'NO_TpAmbito, cbi.* ',
  'FROM ctbbalpits cbi ',
  'LEFT JOIN contas coa ON coa.Codigo=cbi.ACtaCod ',
  'LEFT JOIN contas cob ON cob.Codigo=cbi.BCtaCod ',
  'LEFT JOIN plano  pla ON pla.Codigo=cbi.APlaCod ',
  'LEFT JOIN plano  plb ON plb.Codigo=cbi.BPlaCod ',
  'WHERE cbi.Codigo=' + Geral.FF0(QrCtbBalPCabCodigo.Value),
  SQL_Extra,
  SQL_TpAmbito,
  'ORDER BY NiveisTXT ',
  '']);
  //
  //.MB_Teste(QrCtbBalPIts.SQL.Text);
  QrCtbBalPIts.Locate('Controle', Controle, []);
end;


procedure TFmCtbBalPCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCtbBalPCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCtbBalPCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCtbBalPCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCtbBalPCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCtbBalPCab.TodoBalanco1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Deseja realmente excluir todo bala��o?') = ID_YES then
  begin
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ctbbalplctemi ');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'ctbbalplctopn ');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'ctbbalpits ');
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'ctbbalpcab ');
    //
    LocCod(QrCtbBalPCabCodigo.Value, QrCtbBalPCabCodigo.Value);
  end;
end;

procedure TFmCtbBalPCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtbBalPCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCtbBalPCabCodigo.Value;
  Close;
end;

procedure TFmCtbBalPCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormCtbBalPIts(stIns);
end;

procedure TFmCtbBalPCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCtbBalPCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'ctbbalpcab');
end;

procedure TFmCtbBalPCab.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, Filial, Empresa, AnoMes: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Filial         := EdEmpresa.ValueVariant;
  AnoMes         := (Geral.IMV(CBAno.Text) * 100) + CBMes.ItemIndex + 1;
  Nome           := EdNome.Text;
  //
  Empresa        := DModG.ObtemEntidadeDeFilial(Filial);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a empresa!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ctbbalpcab', 'Codigo', '', '', tsPos, stIns, Codigo);
  if UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, SQLType, 'ctbbalpcab', False, [
  'Empresa', 'AnoMes', 'Nome'], [
  'Codigo'], [
  Empresa, AnoMes, Nome], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmCtbBalPCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ctbbalpcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ctbbalpcab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCtbBalPCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmCtbBalPCab.AtualizaPagoParcial(Controle, Sub: Integer; OriCredito,
  OriDebito: Double; DtCompensado: TDateTime; sDiaSeguinte: String);
var
  SaldoCred, SaldoDeb, Pago: Double;
  Multa, Juros, Desconto: Double;
  Sit, QtDtPg: Integer;
  Compensado: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoma, Dmod.MyDB, [
  'SELECT SUM(Credito) Credito, SUM(Debito) Debito, ',
  'SUM(MultaVal) Multa, SUM(MoraVal) Juros, SUM(DescoVal) Desconto, ',
  '(IF(SUM(Credito) - SUM(Debito) >= 0, SUM(Credito) - SUM(Debito), ',
  '(SUM(Credito) - SUM(Debito)) * -1) - SUM(MultaVal) - SUM(MoraVal) + SUM(DescoVal)) ValLiq, ',
  'COUNT(DISTINCT(Data)) QtDtPg ',
  'FROM ' + FTabLctA,
  'WHERE ID_Pgto=' + Geral.FF0(Controle),
  'AND Data < "' + sDiaSeguinte + '"',
  '']);
  //
  Multa    := QrSomaMulta.Value;
  Juros    := QrSomaJuros.Value;
  Desconto := QrSomaDesconto.Value;
  QtDtPg   := QrSomaQtDtPg.Value;
  //
  if OriDebito = 0 then
    SaldoDeb := 0
  else
    SaldoDeb := QrSomaValLiq.Value - OriDebito;
  //
  if OriCredito = 0 then
    SaldoCred := 0
  else
    SaldoDeb := QrSomaValLiq.Value - OriCredito;
  //
  Pago := QrSomaCredito.Value - QrSomaDebito.Value;
  //
  if (SaldoCred >= 0) and (SaldoDeb >= 0) then
    Sit := 2
  else if (QrSomaDebito.Value <> 0) or (QrSomaCredito.Value <> 0) then
    Sit := 1
  else
    Sit := 0;
  //
  if Sit < 2 then
    Compensado := '0000-00-00'
  else
    Compensado := Geral.FDT(DtCompensado, 1);
  //
  QrSoma.Close;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctbbalplctopn', False, [
  'Sit', 'Pago', 'PagJur',
  'PagMul', 'RecDes', 'QtDtPg',
  'Compensado'], [
  'Controle'], [
  Sit, Pago, Juros,
  Multa, Desconto, QtDtPg, Compensado], [
  Controle], True);
  //
end;

procedure TFmCtbBalPCab.Balancete1Click(Sender: TObject);
var
  SQL_Balanco: String;
begin
  SQL_Balanco := '';
  SQL_Balanco := Geral.ATS([
  'AND ( ',
  '  ( ',
  '    ( ',
  '      (pla.FinContab IN (1,2)) ',
  '      OR ',
  '      (plb.FinContab IN (1,2)) ',
  '    ) ',
  '    AND cbi.TpAmbito = ' + Geral.FF0(Integer(TContabTpAmbito.ctaLctGenOpn)),
  '  ) OR ( ',
  '    cbi.TpAmbito = ' + Geral.FF0(Integer(TContabTpAmbito.ctaLctGenEmi)),
  '  ) ',
  ') ',
  //
  '']);
  ReopenCtbBalPIts(SQL_Balanco, 0, True);
  if PreparaSomasA() then
  begin
    //Geral.MB_Teste(QrCtbBalPIts.SQL.Text);
    QrCtbBalPIts.DisableControls;
    try
      MyObjects.frxDefineDataSets(frxCTB_BALAN_001_01, [
        frxDsCtbBalPIts
      ]);
      //
      MyObjects.frxMostra(frxCTB_BALAN_001_01, 'Balancete Teste (1)');
    finally
      QrCtbBalPIts.EnableControls;
      ReopenCtbBalPIts(EmptyStr, 0);
    end;
  end;
end;

procedure TFmCtbBalPCab.Balancete2Click(Sender: TObject);
const
  CaptionFm  = 'Forma de Agrupamento';
  CaptionRG  = 'Selecione a forma de agrupamento:';
  Lista: array [0..2] of String = (
  'Balancente Pela Ordena��o',
  'Balancente Pelos Itens de Plano',
  'Balancente Pela Forma Cont�bil');
var
  Forma: Integer;
begin
  Forma := MyObjects.SelRadioGroup(CaptionFm, CaptionRG, [
  'Pela ordena��o',
  'Pelos itens de plano',
  'Pela forma cont�bil'
  ], 1, -1, True);
  //
  if Forma > - 1 then
  begin
    FTituloImp := Lista[Forma];
    ImprimeBalancete(Forma);
  end;
end;

procedure TFmCtbBalPCab.Balano1Click(Sender: TObject);
const
  CaptionFm  = 'Forma de Agrupamento';
  CaptionRG  = 'Selecione a forma de agrupamento:';
  Lista: array [0..2] of String = (
  'Balan�o Pela Ordena��o',
  'Balan�o Pelos Itens de Plano',
  'Balan�o Pela Forma Cont�bil');
var
  Forma: Integer;
begin
  Forma := MyObjects.SelRadioGroup(CaptionFm, CaptionRG, Lista, 1, -1, True);
  //
  if Forma > - 1 then
  begin
    FTituloImp := Lista[Forma];
    ImprimeBalanco(Forma);
  end;
end;

procedure TFmCtbBalPCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmCtbBalPCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  PageControl1.ActivePageIndex := 0;
{ N�o! ocorre depois do AfterScroll do QrCtbBalPCab!
  FEmpresa := 0;
  FFilial  := 0;
  //FEntidade_TXT
  FTabLctA := '';
  FTabLctB := '';
  FTabLctD := '';
  FDtEncer := 0;
  FDtMorto := 0;
}
end;

procedure TFmCtbBalPCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCtbBalPCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCtbBalPCab.SaldosdeCarteiras1Click(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando dados pr�vios.');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM ctbbalpits WHERE Codigo=' + FsCodigo +
    ' AND TpAmbito=' + Geral.FF0(Integer(TContabTpAmbito.ctaSdoCart))
  );
  // ...
  //
  ReopenCtbBalPIts('', 0);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmCtbBalPCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmCtbBalPCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCtbBalPCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCtbBalPCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCtbBalPCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
{
  ArrPla := nil;
  ArrCjt := nil;
  ArrGru := nil;
  ArrSgr := nil;
  ArrCta := nil;
}
  //
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCtbBalPCab.QrCtbBalPCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCtbBalPCab.QrCtbBalPCabAfterScroll(DataSet: TDataSet);
begin
  FBalPatr      := QrCtbBalPCabAnoMes.Value;
  FEmpresa      := QrCtbBalPCabEmpresa.Value;
  FFilial       := DModG.ObtemFilialDeEntidade(FEmpresa);
  DModG.Def_EM_ABD(TMeuDB, FEmpresa, FFilial, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  FsCodigo      := Geral.FF0(QrCtbBalPCabCodigo.Value);
  FsEmpresa     := Geral.FF0(FEmpresa);

  FAMAtual      := QrCtbBalPCabAnoMes.Value;
  FPrimeiroDia  := Geral.AnoMesToData(FAMAtual, 1);
  FsPrimeiroDia := Geral.FDT(FPrimeiroDia, 1);

  FAMSeguinte   := Geral.IncrementaMes_AnoMes(FAMAtual, 1);
  FDiaSeguinte  := Geral.AnoMesToData(FAMSeguinte, 1);
  FsDiaSeguinte := Geral.FDT(FDiaSeguinte, 1);
  //
  ReopenCtbBalPIts('', 0);
end;

procedure TFmCtbBalPCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrCtbBalPCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmCtbBalPCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCtbBalPCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ctbbalpcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCtbBalPCab.Soslanamentosdofinanceiro1Click(Sender: TObject);
begin
  RecriaItensFinanceiro();
  //
end;

procedure TFmCtbBalPCab.Soslanamentosemitidosnoperodo1Click(Sender: TObject);
begin
  RecriaTotaisGeneroPeriodo();
  //
  VerificaErrosGenero_e_GenCtb(True);
  //
end;

procedure TFmCtbBalPCab.Sossaldosdascarteiras1Click(Sender: TObject);
begin
  RecriaItensCarteirasTipo0e1();
end;

procedure TFmCtbBalPCab.SossaldosdasCarteiras2Click(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Para obter os saldos de carteiras de emiss�es � necess�rio que os lan�amentos fnanceiroas j� tenham sido criados !'
  + sLineBreak +
  'Caso n�o tenham sido importados ou estejam desatualizados, recrie-os antes.'
  + sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
    RecriaItensCarteiras();
end;

procedure TFmCtbBalPCab.SossaldosdasCarteirasTipoEmisso1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Para obter os saldos de carteiras de emiss�es � necess�rio que os lan�amentos fnanceiroas j� tenham sido criados !'
  + sLineBreak +
  'Caso n�o tenham sido importados ou estejam desatualizados, recrie-os antes.'
  + sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
    RecriaItensCarteirasTipo2();
end;

procedure TFmCtbBalPCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtbBalPCab.frxCTB_BALAN_001_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_PERIODO' then
    Value := MesAnoTxt(QrCtbBalPCabAnoMes.Value)
  else
  if VarName = 'VARF_DATA' then
    Value := Now() //Geral.FDT(Now, 11)
  else
  if VarName = 'VARF_NO_EMPRESA' then
    Value := QrCtbBalPCabNO_EMPRESA.Value
  else

  if VarName = 'VARF_PLA_C' then
    Value := ArrPla[QrCtbBalPItsXPlaCod.Value][0]
  else
  if VarName = 'VARF_PLA_D' then
    Value := ArrPla[QrCtbBalPItsXPlaCod.Value][1]
  else
  if VarName = 'VARF_PLA_R' then
    Value := ArrPla[QrCtbBalPItsXPlaCod.Value][0] - ArrPla[QrCtbBalPItsXPlaCod.Value][1]
  else

  if VarName = 'VARF_CJT_C' then
    Value := ArrCjt[QrCtbBalPItsXCjtCod.Value][0]
  else
  if VarName = 'VARF_CJT_D' then
    Value := ArrCjt[QrCtbBalPItsXCjtCod.Value][1]
  else
  if VarName = 'VARF_CJT_R' then
    Value := ArrCjt[QrCtbBalPItsXCjtCod.Value][0] - ArrCjt[QrCtbBalPItsXCjtCod.Value][1]
  else

  if VarName = 'VARF_GRU_C' then
    Value := ArrGru[QrCtbBalPItsXGruCod.Value][0]
  else
  if VarName = 'VARF_GRU_D' then
    Value := ArrGru[QrCtbBalPItsXGruCod.Value][1]
  else
  if VarName = 'VARF_GRU_R' then
    Value := ArrGru[QrCtbBalPItsXGruCod.Value][0] - ArrGru[QrCtbBalPItsXGruCod.Value][1]
  else

  if VarName = 'VARF_SGR_C' then
    Value := ArrSgr[QrCtbBalPItsXSgrCod.Value][0]
  else
  if VarName = 'VARF_SGR_D' then
    Value := ArrSgr[QrCtbBalPItsXSgrCod.Value][1]
  else
  if VarName = 'VARF_SGR_R' then
    Value := ArrSgr[QrCtbBalPItsXSgrCod.Value][0] - ArrSgr[QrCtbBalPItsXSgrCod.Value][1]
  else

  if VarName = 'VARF_CTA_C' then
    Value := ArrCta[QrCtbBalPItsXCtaCod.Value][0]
  else
  if VarName = 'VARF_CTA_D' then
    Value := ArrCta[QrCtbBalPItsXCtaCod.Value][1]
  else
  if VarName = 'VARF_CTA_R' then
    Value := ArrCta[QrCtbBalPItsXCtaCod.Value][0] - ArrCta[QrCtbBalPItsXCtaCod.Value][1]
  else

end;

procedure TFmCtbBalPCab.frxCTB_BALAN_001_02GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULO' then
    Value := FTituloImp
  else
  if VarName = 'VARF_PERIODO' then
    Value := MesAnoTxt(QrCtbBalPCabAnoMes.Value)
  else
  if VarName = 'VARF_DATA' then
    Value := Now() //Geral.FDT(Now, 11)
  else
  if VarName = 'VARF_NO_EMPRESA' then
    Value := QrCtbBalPCabNO_EMPRESA.Value
  else

  if VarName = 'VARF_PLA_C' then
    //Value := ArrPla[QrCBPIOORD_PLA.Value][0]
    Value := ObtemValB_Pla('_Teste_CBPIO_', 'Credito')
  else
  if VarName = 'VARF_PLA_D' then
    //Value := ArrPla[QrCBPIOORD_PLA.Value][1]
    Value := ObtemValB_Pla('_Teste_CBPIO_', 'Debito')
  else
  if VarName = 'VARF_PLA_R' then
    //Value := ArrPla[QrCBPIOORD_PLA.Value][0] - ArrPla[QrCBPIOORD_PLA.Value][1]
    Value := ObtemValB_Pla('_Teste_CBPIO_', 'Credito - Debito')
  else

  if VarName = 'VARF_CJT_C' then
    //Value := ArrCjt[QrCBPIOORD_CJT.Value][0]
    Value := ObtemValB_Cjt('_Teste_CBPIO_', 'Credito')
  else
  if VarName = 'VARF_CJT_D' then
    //Value := ArrCjt[QrCBPIOORD_CJT.Value][1]
    Value := ObtemValB_Cjt('_Teste_CBPIO_', 'Debito')
  else
  if VarName = 'VARF_CJT_R' then
    //Value := ArrCjt[QrCBPIOORD_CJT.Value][0] - ArrCjt[QrCBPIOORD_CJT.Value][1]
    Value := ObtemValB_Cjt('_Teste_CBPIO_', 'Credito - Debito')
  else

  if VarName = 'VARF_GRU_C' then
    //Value := ArrGru[QrCBPIOORD_GRU.Value][0]
    Value := ObtemValB_Gru('_Teste_CBPIO_', 'Credito')
  else
  if VarName = 'VARF_GRU_D' then
    //Value := ArrGru[QrCBPIOORD_GRU.Value][1]
    Value := ObtemValB_Gru('_Teste_CBPIO_', 'Debito')
  else
  if VarName = 'VARF_GRU_R' then
    //Value := ArrGru[QrCBPIOORD_GRU.Value][0] - ArrGru[QrCBPIOORD_GRU.Value][1]
    Value := ObtemValB_Gru('_Teste_CBPIO_', 'Credito - Debito')
  else

  if VarName = 'VARF_SGR_C' then
    //Value := ArrSgr[QrCBPIOORD_SGR.Value][0]
    Value := ObtemValB_Sgr('_Teste_CBPIO_', 'Credito')
  else
  if VarName = 'VARF_SGR_D' then
    //Value := ArrSgr[QrCBPIOORD_SGR.Value][1]
    Value := ObtemValB_Sgr('_Teste_CBPIO_', 'Debito')
  else
  if VarName = 'VARF_SGR_R' then
    //Value := ArrSgr[QrCBPIOORD_SGR.Value][0] - ArrSgr[QrCBPIOORD_SGR.Value][1]
    Value := ObtemValB_Sgr('_Teste_CBPIO_', 'Credito - Debito')
  else

end;

procedure TFmCtbBalPCab.frxCTB_BALAN_001_03GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULO' then
    Value := FTituloImp
  else
  if VarName = 'VARF_PERIODO' then
    Value := MesAnoTxt(QrCtbBalPCabAnoMes.Value)
  else
  if VarName = 'VARF_DATA' then
    Value := Now() //Geral.FDT(Now, 11)
  else
  if VarName = 'VARF_NO_EMPRESA' then
    Value := QrCtbBalPCabNO_EMPRESA.Value
  else

  if VarName = 'VARF_PLA_C' then
    //Value := ArrPla[QrCBPIEORD_PLA.Value][0]
    Value := ObtemValC_Pla('_Teste_CBPIE_', 'Credito')
  else
  if VarName = 'VARF_PLA_D' then
    //Value := ArrPla[QrCBPIEORD_PLA.Value][1]
    Value := ObtemValC_Pla('_Teste_CBPIE_', 'Debito')
  else
  if VarName = 'VARF_PLA_R' then
    //Value := ArrPla[QrCBPIEORD_PLA.Value][0] - ArrPla[QrCBPIEORD_PLA.Value][1]
    Value := ObtemValC_Pla('_Teste_CBPIE_', 'Credito - Debito')
  else

  if VarName = 'VARF_CJT_C' then
    //Value := ArrCjt[QrCBPIEORD_CJT.Value][0]
    Value := ObtemValC_Cjt('_Teste_CBPIE_', 'Credito')
  else
  if VarName = 'VARF_CJT_D' then
    //Value := ArrCjt[QrCBPIEORD_CJT.Value][1]
    Value := ObtemValC_Cjt('_Teste_CBPIE_', 'Debito')
  else
  if VarName = 'VARF_CJT_R' then
    //Value := ArrCjt[QrCBPIEORD_CJT.Value][0] - ArrCjt[QrCBPIEORD_CJT.Value][1]
    Value := ObtemValC_Cjt('_Teste_CBPIE_', 'Credito - Debito')
  else

  if VarName = 'VARF_GRU_C' then
    //Value := ArrGru[QrCBPIEORD_GRU.Value][0]
    Value := ObtemValC_Gru('_Teste_CBPIE_', 'Credito')
  else
  if VarName = 'VARF_GRU_D' then
    //Value := ArrGru[QrCBPIEORD_GRU.Value][1]
    Value := ObtemValC_Gru('_Teste_CBPIE_', 'Debito')
  else
  if VarName = 'VARF_GRU_R' then
    //Value := ArrGru[QrCBPIEORD_GRU.Value][0] - ArrGru[QrCBPIEORD_GRU.Value][1]
    Value := ObtemValC_Gru('_Teste_CBPIE_', 'Credito - Debito')
  else

  if VarName = 'VARF_SGR_C' then
    //Value := ArrSgr[QrCBPIEORD_SGR.Value][0]
    Value := ObtemValC_Sgr('_Teste_CBPIE_', 'Credito')
  else
  if VarName = 'VARF_SGR_D' then
    //Value := ArrSgr[QrCBPIEORD_SGR.Value][1]
    Value := ObtemValC_Sgr('_Teste_CBPIE_', 'Debito')
  else
  if VarName = 'VARF_SGR_R' then
    //Value := ArrSgr[QrCBPIEORD_SGR.Value][0] - ArrSgr[QrCBPIEORD_SGR.Value][1]
    Value := ObtemValC_Sgr('_Teste_CBPIE_', 'Credito - Debito')
  else

end;

procedure TFmCtbBalPCab.CabInclui1Click(Sender: TObject);
const
  IncremMes = -1;
  Nulo = False;
begin
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, IncremMes, Nulo);
  //
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCtbBalPCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'ctbbalpcab');
end;

procedure TFmCtbBalPCab.QrCtbBalPCabBeforeClose(
  DataSet: TDataSet);
begin
  QrCtbBalPIts.Close;
  FBalPatr      := 0;
  FEmpresa      := 0;
  FFilial       := 0;
  //DModG.Def_EM_ABD(TMeuDB, FEmpresa, FFilial, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  FsCodigo      := '?';
  FsEmpresa     := '?';

  FAMAtual      := 0;
  FPrimeiroDia  := 0;
  FsPrimeiroDia := '?/?/?';

  FAMSeguinte   := 0;
  FDiaSeguinte  := 0;
  FsDiaSeguinte := '?/?/?';
end;

procedure TFmCtbBalPCab.QrCtbBalPCabBeforeOpen(DataSet: TDataSet);
begin
  QrCtbBalPCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCtbBalPCab.QrCtbBalPItsAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtbBalPLctOpn, Dmod.MyDB, [
  'SELECT *  ',
  'FROM ctbbalplctopn ',
  'WHERE Codigo=' + Geral.FF0(QrCtbBalPCabCodigo.Value),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtbBalPLctEmi, Dmod.MyDB, [
  'SELECT *  ',
  'FROM ctbbalplctemi ',
  'WHERE Codigo=' + Geral.FF0(QrCtbBalPCabCodigo.Value),
  '']);
end;

procedure TFmCtbBalPCab.QrCtbBalPItsBeforeClose(DataSet: TDataSet);
begin
  QrCtbBalPLctOpn.Close;
  QrCtbBalPLctEmi.Close;
end;

end.

