object FmCtbBalPCab: TFmCtbBalPCab
  Left = 368
  Top = 194
  Caption = 'CTB-BALAN-001 :: Balan'#231'o Patrimonial'
  ClientHeight = 578
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 105
    Width = 784
    Height = 473
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 101
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label3: TLabel
        Left = 488
        Top = 15
        Width = 92
        Height = 13
        Caption = 'M'#234's do movimento:'
      end
      object Label4: TLabel
        Left = 676
        Top = 15
        Width = 91
        Height = 13
        Caption = 'Ano do movimento:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 72
        Width = 753
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 133
        Top = 32
        Width = 352
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CBMes: TComboBox
        Left = 489
        Top = 32
        Width = 182
        Height = 21
        Color = clWhite
        DropDownCount = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        Text = 'CBMes'
      end
      object CBAno: TComboBox
        Left = 675
        Top = 32
        Width = 95
        Height = 21
        Color = clWhite
        DropDownCount = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        Text = 'CBAno'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 410
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 105
    Width = 784
    Height = 473
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 101
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 16
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label6: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit2
      end
      object Label5: TLabel
        Left = 636
        Top = 16
        Width = 39
        Height = 13
        Caption = 'AnoMes'
        FocusControl = DBEdit3
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCtbBalPCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 16
        Top = 72
        Width = 753
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsCtbBalPCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 128
        Top = 32
        Width = 505
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsCtbBalPCab
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 76
        Top = 32
        Width = 52
        Height = 21
        DataField = 'Empresa'
        DataSource = DsCtbBalPCab
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 636
        Top = 32
        Width = 134
        Height = 21
        DataField = 'AnoMes'
        DataSource = DsCtbBalPCab
        TabOrder = 4
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 409
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Balan'#231'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 101
      Width = 784
      Height = 308
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Itens '
        object DGDados: TDBGrid
          Left = 0
          Top = 101
          Width = 776
          Height = 179
          Align = alClient
          DataSource = DsCtbBalPIts
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NiveisTXT'
              Title.Caption = 'N'#237'veis'
              Width = 98
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TpBalanc'
              Title.Caption = 'Movimenta'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TpAmbito'
              Title.Caption = #194'mbito'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'XCtaCod'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'XCtaTxt'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'XSgrCod'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'XSgrTxt'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'XGruCod'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'XGruTxt'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'XCjtCod'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'XCjtTxt'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'XPlaCod'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'XPlaTxt'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'APlaTxt'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ACjtTxt'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AGruTxt'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ASgrTxt'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ACtaTxt'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CtaPlaA'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BPlaTxt'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BCjtTxt'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BGruTxt'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BSgrTxt'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BCtaTxt'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CtaPlaB'
              Width = 140
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 101
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object RGTpAmbito: TRadioGroup
            Left = 0
            Top = 0
            Width = 776
            Height = 57
            Align = alTop
            Caption = ' Filtro por tipo de item: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              '0: TODOS'
              '1: Emiss'#245'es abertas'
              '2: Lan'#231'amentos emitidos'
              '3: Saldos de Carteiras '
              '4: Valor de estoque'
              '5: Recupera'#231#227'o de Impostos'
              '6: Imobilizado'
              '7: Patrim'#244'nio L'#237'quido')
            TabOrder = 0
            OnClick = RGTpAmbitoClick
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Abertos '
        ImageIndex = 1
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 280
          Align = alClient
          DataSource = DsCtbBalPLctOpn
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Emitidos '
        ImageIndex = 2
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 280
          Align = alClient
          DataSource = DsCtbBalPLctEmi
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 240
        Height = 32
        Caption = 'Balan'#231'o Patrimonial'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 240
        Height = 32
        Caption = 'Balan'#231'o Patrimonial'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 240
        Height = 32
        Caption = 'Balan'#231'o Patrimonial'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrCtbBalPCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCtbBalPCabBeforeOpen
    AfterOpen = QrCtbBalPCabAfterOpen
    BeforeClose = QrCtbBalPCabBeforeClose
    AfterScroll = QrCtbBalPCabAfterScroll
    SQL.Strings = (
      'SELECT ent.RazaoSocial NO_EMPRESA, cbc.*  '
      'FROM ctbbalpcab cbc '
      'LEFT JOIN entidades ent ON ent.Codigo=cbc.Empresa '
      'WHERE cbc.Codigo=0 ')
    Left = 240
    Top = 57
    object QrCtbBalPCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCtbBalPCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrCtbBalPCabAnoMes: TIntegerField
      FieldName = 'AnoMes'
      Required = True
    end
    object QrCtbBalPCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCtbBalPCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 255
    end
  end
  object DsCtbBalPCab: TDataSource
    DataSet = QrCtbBalPCab
    Left = 240
    Top = 101
  end
  object QrCtbBalPIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCtbBalPItsBeforeClose
    AfterScroll = QrCtbBalPItsAfterScroll
    SQL.Strings = (
      'SELECT IF(TpBalanc<>2, '
      
        ' CONCAT(BPlaCod, ".", LPAD(BCjtCod, 2, "0"), ".", LPAD(BGruCod, ' +
        '2, "0"), ".", LPAD(BSgrCod, 3, "0"), ".", LPAD(BCtaCod, 4, "0"))' +
        ', '
      
        ' CONCAT(APlaCod, ".", LPAD(ACjtCod, 2, "0"), ".", LPAD(AGruCod, ' +
        '2, "0"), ".", LPAD(ASgrCod, 3, "0"), ".", LPAD(ACtaCod, 4, "0"))'
      ') NiveisTXT, '
      ''
      'IF(TpBalanc<>2, BPlaCod, APlaCod) XPlaCod,'
      'IF(TpBalanc<>2, BCjtCod, ACjtCod) XCjtCod,'
      'IF(TpBalanc<>2, BGruCod, AGruCod) XGruCod,'
      'IF(TpBalanc<>2, BSgrCod, ASgrCod) XSgrCod,'
      'IF(TpBalanc<>2, BCtaCod, ACtaCod) XCtaCod,'
      ''
      'IF(TpBalanc<>2, BPlaTxt, APlaTxt) XPlaTxt,'
      'IF(TpBalanc<>2, BCjtTxt, ACjtTxt) XCjtTxt,'
      'IF(TpBalanc<>2, BGruTxt, AGruTxt) XGruTxt,'
      'IF(TpBalanc<>2, BSgrTxt, ASgrTxt) XSgrTxt,'
      'IF(TpBalanc<>2, BCtaTxt, ACtaTxt) XCtaTxt,'
      ''
      ''
      'coa.Nome NO_CtaPlaA, cob.Nome NO_CtaPlaB,  '
      'ELT(cbi.TpBalanc, "Movimento Cont'#225'bil", "Movimento Financeiro",'
      ' "Manual Cont'#225'bil", "? ? ? ? ? ? ?") NO_TpBalanc, '
      'ELT(cbi.TpAmbito, "Financeiro", "[Livre]", "Saldo Carteira", '
      
        '"Valor Estoque", "Recup.Impostos", "Imobilizado", "Patrim.L'#237'q", ' +
        '"?????") '
      'NO_TpAmbito, cbi.* '
      'FROM ctbbalpits cbi '
      'LEFT JOIN contas coa ON coa.Codigo=cbi.ACtaCod '
      'LEFT JOIN contas cob ON cob.Codigo=cbi.BCtaCod '
      'ORDER BY NiveisTXT')
    Left = 328
    Top = 57
    object QrCtbBalPItsNO_CtaPlaA: TWideStringField
      FieldName = 'NO_CtaPlaA'
      Size = 50
    end
    object QrCtbBalPItsNO_CtaPlaB: TWideStringField
      FieldName = 'NO_CtaPlaB'
      Size = 50
    end
    object QrCtbBalPItsNO_TpBalanc: TWideStringField
      FieldName = 'NO_TpBalanc'
      Size = 10
    end
    object QrCtbBalPItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCtbBalPItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCtbBalPItsCredito: TFloatField
      FieldName = 'Credito'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrCtbBalPItsDebito: TFloatField
      FieldName = 'Debito'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrCtbBalPItsLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrCtbBalPItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCtbBalPItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCtbBalPItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrCtbBalPItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrCtbBalPItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrCtbBalPItsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrCtbBalPItsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrCtbBalPItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrCtbBalPItsAPlaCod: TIntegerField
      FieldName = 'APlaCod'
      Required = True
    end
    object QrCtbBalPItsACjtCod: TIntegerField
      FieldName = 'ACjtCod'
      Required = True
    end
    object QrCtbBalPItsAGruCod: TIntegerField
      FieldName = 'AGruCod'
      Required = True
    end
    object QrCtbBalPItsASgrCod: TIntegerField
      FieldName = 'ASgrCod'
      Required = True
    end
    object QrCtbBalPItsACtaCod: TIntegerField
      FieldName = 'ACtaCod'
      Required = True
    end
    object QrCtbBalPItsBPlaCod: TIntegerField
      FieldName = 'BPlaCod'
      Required = True
    end
    object QrCtbBalPItsBCjtCod: TIntegerField
      FieldName = 'BCjtCod'
      Required = True
    end
    object QrCtbBalPItsBGruCod: TIntegerField
      FieldName = 'BGruCod'
      Required = True
    end
    object QrCtbBalPItsBSgrCod: TIntegerField
      FieldName = 'BSgrCod'
      Required = True
    end
    object QrCtbBalPItsBCtaCod: TIntegerField
      FieldName = 'BCtaCod'
      Required = True
    end
    object QrCtbBalPItsAPlaTxt: TWideStringField
      FieldName = 'APlaTxt'
      Size = 60
    end
    object QrCtbBalPItsACjtTxt: TWideStringField
      FieldName = 'ACjtTxt'
      Size = 60
    end
    object QrCtbBalPItsAGruTxt: TWideStringField
      FieldName = 'AGruTxt'
      Size = 60
    end
    object QrCtbBalPItsASgrTxt: TWideStringField
      FieldName = 'ASgrTxt'
      Size = 60
    end
    object QrCtbBalPItsACtaTxt: TWideStringField
      FieldName = 'ACtaTxt'
      Size = 60
    end
    object QrCtbBalPItsBPlaTxt: TWideStringField
      FieldName = 'BPlaTxt'
      Size = 60
    end
    object QrCtbBalPItsBCjtTxt: TWideStringField
      FieldName = 'BCjtTxt'
      Size = 60
    end
    object QrCtbBalPItsBGruTxt: TWideStringField
      FieldName = 'BGruTxt'
      Size = 60
    end
    object QrCtbBalPItsBSgrTxt: TWideStringField
      FieldName = 'BSgrTxt'
      Size = 60
    end
    object QrCtbBalPItsBCtaTxt: TWideStringField
      FieldName = 'BCtaTxt'
      Size = 60
    end
    object QrCtbBalPItsNO_TpAmbito: TWideStringField
      FieldName = 'NO_TpAmbito'
      Size = 30
    end
    object QrCtbBalPItsTpBalanc: TSmallintField
      FieldName = 'TpBalanc'
    end
    object QrCtbBalPItsTpAmbito: TSmallintField
      FieldName = 'TpAmbito'
    end
    object QrCtbBalPItsNiveisTXT: TWideStringField
      FieldName = 'NiveisTXT'
      Size = 59
    end
    object QrCtbBalPItsXPlaTxt: TWideStringField
      FieldName = 'XPlaTxt'
      Size = 60
    end
    object QrCtbBalPItsXCjtTxt: TWideStringField
      FieldName = 'XCjtTxt'
      Size = 60
    end
    object QrCtbBalPItsXGruTxt: TWideStringField
      FieldName = 'XGruTxt'
      Size = 60
    end
    object QrCtbBalPItsXSgrTxt: TWideStringField
      FieldName = 'XSgrTxt'
      Size = 60
    end
    object QrCtbBalPItsXCtaTxt: TWideStringField
      FieldName = 'XCtaTxt'
      Size = 60
    end
    object QrCtbBalPItsXPlaCod: TLargeintField
      FieldName = 'XPlaCod'
    end
    object QrCtbBalPItsXCjtCod: TLargeintField
      FieldName = 'XCjtCod'
    end
    object QrCtbBalPItsXGruCod: TLargeintField
      FieldName = 'XGruCod'
    end
    object QrCtbBalPItsXSgrCod: TLargeintField
      FieldName = 'XSgrCod'
    end
    object QrCtbBalPItsXCtaCod: TLargeintField
      FieldName = 'XCtaCod'
    end
  end
  object DsCtbBalPIts: TDataSource
    DataSet = QrCtbBalPIts
    Left = 328
    Top = 101
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 420
    Top = 484
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 340
    Top = 480
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      object TodoBalanco1: TMenuItem
        Caption = '&Todo Balan'#231'o'
        OnClick = TodoBalanco1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object Lanamentosfinanceirosabertos1: TMenuItem
        Caption = '&Lan'#231'amentos financeiros abertos'
        OnClick = Lanamentosfinanceirosabertos1Click
      end
      object SaldosdeCarteiras1: TMenuItem
        Caption = '&Saldos de Carteiras'
        OnClick = SaldosdeCarteiras1Click
      end
      object Lanamentosfinanceirosemitidos1: TMenuItem
        Caption = '&Lan'#231'amentos financeiros emitidos'
        OnClick = Lanamentosfinanceirosemitidos1Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Recriaitens1: TMenuItem
      Caption = 'Recria itens'
      object odosrecriveis1: TMenuItem
        Caption = 'Recria &Todos recri'#225'veis'
        OnClick = odosrecriveis1Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Soslanamentosdofinanceiro1: TMenuItem
        Caption = 'S'#243' os lan'#231'amentos do &Financeiro'
        OnClick = Soslanamentosdofinanceiro1Click
      end
      object Sossaldosdascarteiras1: TMenuItem
        Caption = 'S'#243' os saldos das &Carteiras Tipo Banco e Caixa'
        OnClick = Sossaldosdascarteiras1Click
      end
      object SossaldosdasCarteirasTipoEmisso1: TMenuItem
        Caption = 'S'#243' os saldos das Carteiras Tipo &Emiss'#227'o'
        Enabled = False
        OnClick = SossaldosdasCarteirasTipoEmisso1Click
      end
      object SossaldosdasCarteiras2: TMenuItem
        Caption = 'S'#243' os saldos das Carteiras'
        Enabled = False
        OnClick = SossaldosdasCarteiras2Click
      end
      object Soslanamentosemitidosnoperodo1: TMenuItem
        Caption = 'S'#243' os lan'#231'amentos emitidos no per'#237'odo'
        OnClick = Soslanamentosemitidosnoperodo1Click
      end
    end
    object VerificaGneros1: TMenuItem
      Caption = '&Verifica G'#234'neros'
      OnClick = VerificaGneros1Click
    end
  end
  object QrAux: TMySQLQuery
    Database = Dmod.MyDB
    Left = 492
    Top = 1
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    Left = 492
    Top = 49
    object QrSomaCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrSomaDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrSomaMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrSomaJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSomaDesconto: TFloatField
      FieldName = 'Desconto'
    end
    object QrSomaQtDtPg: TLargeintField
      FieldName = 'QtDtPg'
    end
    object QrSomaValLiq: TFloatField
      FieldName = 'ValLiq'
    end
  end
  object QrOrfaos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cjt.Plano, gru.Conjunto, sgr.Grupo, '
      'cta.Subgrupo, lct.Genero, cta.Nome NO_Cta, '
      'lct.Data, lct.Tipo, lct.Carteira, lct.Controle, '
      'lct.Sub, lct.Genero, lct.Debito, lct.Credito, '
      'lct.Compensado, lct.Sit, lct.Vencimento, lct.ID_Pgto, '
      'lct.ID_Quit, lct.ID_Sub, lct.Pago, lct.GenCtb, '
      'lct.QtDtPg, lct.PagMul, lct.PagJur , lct.RecDes,'
      'pla.Nome NO_Pla, cjt.Nome NO_Cjt, '
      'gru.Nome NO_Gru, sgr.Nome NO_Sgr,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT'
      'FROM bluederm_cialeather.lct0001a lct  '
      'LEFT JOIN bluederm_cialeather.entidades ent ON ent.Codigo='
      '  IF(lct.Cliente <> 0, lct.Cliente, lct.Fornecedor)'
      
        'LEFT JOIN bluederm_cialeather.carteiras crt ON crt.Codigo=lct.Ca' +
        'rteira '
      
        'LEFT JOIN bluederm_cialeather.contas    cta ON cta.Codigo=lct.Ge' +
        'nero '
      
        'LEFT JOIN bluederm_cialeather.subgrupos sgr ON sgr.Codigo=cta.Su' +
        'bGrupo '
      
        'LEFT JOIN bluederm_cialeather.grupos    gru ON gru.Codigo=sgr.Gr' +
        'upo '
      
        'LEFT JOIN bluederm_cialeather.conjuntos cjt ON cjt.Codigo=gru.Co' +
        'njunto '
      
        'LEFT JOIN bluederm_cialeather.plano     pla ON pla.Codigo=cjt.Pl' +
        'ano '
      'WHERE crt.ForneceI=-11'
      'AND lct.Genero > 0 '
      'AND ( '
      '  lct.Tipo = 2  '
      '  AND lct.Data < "2021-01-01"  '
      '  AND (  '
      '    lct.Sit < 2  '
      '    OR  '
      '    lct.Compensado >= "2021-01-01"  '
      '  ) '
      '  OR ( '
      '  lct.Tipo < 2 '
      '  )'
      ') '
      'AND lct.GenCtb = 0 '
      'ORDER BY NO_Cta, NO_ENT')
    Left = 492
    Top = 97
  end
  object QrLcts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ''
      'SELECT SUM(lct.Credito) Credito, SUM(lct.Debito) Debito,  '
      'acjt.Plano APlaCod, agru.Conjunto ACjtCod, '
      'asgr.Grupo AGruCod, acta.Subgrupo ASgrCod,  '
      'lct.Genero ACtaCod, bcjt.Plano BPlaCod,  '
      'bgru.Conjunto BCjtCod, bsgr.Grupo BGruCod,  '
      'bcta.SubGrupo BSgrCod, lct.GenCtb BCtaCod,  '
      'apla.Nome APlaTxt, acjt.Nome ACjtTxt,  '
      'agru.Nome AGruTxt, asgr.Nome ASgrTxt,  '
      'acta.Nome ACtaTxt, bpla.Nome BPlaTxt,  '
      'bcjt.Nome BCjtTxt, bgru.Nome BGruTxt,  '
      'bsgr.Nome BSgrTxt, bcta.Nome BCtaTxt '
      'FROM ctbbalplctopn lct  '
      
        'LEFT JOIN bluederm_cialeather.carteiras crt ON crt.Codigo=lct.Ca' +
        'rteira '
      
        'LEFT JOIN bluederm_cialeather.contas    acta ON acta.Codigo=lct.' +
        'Genero '
      
        'LEFT JOIN bluederm_cialeather.subgrupos asgr ON asgr.Codigo=acta' +
        '.SubGrupo '
      
        'LEFT JOIN bluederm_cialeather.grupos    agru ON agru.Codigo=asgr' +
        '.Grupo '
      
        'LEFT JOIN bluederm_cialeather.conjuntos acjt ON acjt.Codigo=agru' +
        '.Conjunto '
      
        'LEFT JOIN bluederm_cialeather.plano     apla ON apla.Codigo=acjt' +
        '.Plano '
      
        'LEFT JOIN bluederm_cialeather.contas    bcta ON bcta.Codigo=lct.' +
        'GenCtb '
      
        'LEFT JOIN bluederm_cialeather.subgrupos bsgr ON bsgr.Codigo=bcta' +
        '.SubGrupo '
      
        'LEFT JOIN bluederm_cialeather.grupos    bgru ON bgru.Codigo=bsgr' +
        '.Grupo '
      
        'LEFT JOIN bluederm_cialeather.conjuntos bcjt ON bcjt.Codigo=bgru' +
        '.Conjunto '
      
        'LEFT JOIN bluederm_cialeather.plano     bpla ON bpla.Codigo=bcjt' +
        '.Plano '
      'WHERE crt.ForneceI=-11'
      'AND lct.Codigo=2'
      'AND lct.Data < "2021-01-01" '
      'AND lct.Tipo = 2 '
      'AND ( '
      '  lct.Sit < 2 '
      '  OR '
      '  lct.Compensado >= "2021-01-01" '
      ') '
      'GROUP BY lct.GenCtb'
      'ORDER BY lct.GenCtb')
    Left = 76
    Top = 300
    object QrLctsCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctsDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLctsAPlaCod: TIntegerField
      FieldName = 'APlaCod'
    end
    object QrLctsACjtCod: TIntegerField
      FieldName = 'ACjtCod'
    end
    object QrLctsAGruCod: TIntegerField
      FieldName = 'AGruCod'
    end
    object QrLctsASgrCod: TIntegerField
      FieldName = 'ASgrCod'
    end
    object QrLctsACtaCod: TIntegerField
      FieldName = 'ACtaCod'
      Required = True
    end
    object QrLctsBPlaCod: TIntegerField
      FieldName = 'BPlaCod'
    end
    object QrLctsBCjtCod: TIntegerField
      FieldName = 'BCjtCod'
    end
    object QrLctsBGruCod: TIntegerField
      FieldName = 'BGruCod'
    end
    object QrLctsBSgrCod: TIntegerField
      FieldName = 'BSgrCod'
    end
    object QrLctsBCtaCod: TIntegerField
      FieldName = 'BCtaCod'
      Required = True
    end
    object QrLctsAPlaTxt: TWideStringField
      FieldName = 'APlaTxt'
      Size = 50
    end
    object QrLctsACjtTxt: TWideStringField
      FieldName = 'ACjtTxt'
      Size = 50
    end
    object QrLctsAGruTxt: TWideStringField
      FieldName = 'AGruTxt'
      Size = 50
    end
    object QrLctsASgrTxt: TWideStringField
      FieldName = 'ASgrTxt'
      Size = 50
    end
    object QrLctsACtaTxt: TWideStringField
      FieldName = 'ACtaTxt'
      Size = 50
    end
    object QrLctsBPlaTxt: TWideStringField
      FieldName = 'BPlaTxt'
      Size = 50
    end
    object QrLctsBCjtTxt: TWideStringField
      FieldName = 'BCjtTxt'
      Size = 50
    end
    object QrLctsBGruTxt: TWideStringField
      FieldName = 'BGruTxt'
      Size = 50
    end
    object QrLctsBSgrTxt: TWideStringField
      FieldName = 'BSgrTxt'
      Size = 50
    end
    object QrLctsBCtaTxt: TWideStringField
      FieldName = 'BCtaTxt'
      Size = 50
    end
  end
  object QrAu2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 492
    Top = 145
  end
  object QrCrts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT crt.Codigo Carteira, '
      'crt.GenCtb BCtaCod, bcjt.Plano BPlaCod,  '
      'bgru.Conjunto BCjtCod, bsgr.Grupo BGruCod,  '
      'bcta.SubGrupo BSgrCod, '
      'bpla.Nome BPlaTxt,  '
      'bcjt.Nome BCjtTxt, bgru.Nome BGruTxt,  '
      'bsgr.Nome BSgrTxt, bcta.Nome BCtaTxt '
      'FROM bluederm_cialeather.carteiras crt '
      
        'LEFT JOIN bluederm_cialeather.contas    bcta ON bcta.Codigo=crt.' +
        'GenCtb '
      
        'LEFT JOIN bluederm_cialeather.subgrupos bsgr ON bsgr.Codigo=bcta' +
        '.SubGrupo '
      
        'LEFT JOIN bluederm_cialeather.grupos    bgru ON bgru.Codigo=bsgr' +
        '.Grupo '
      
        'LEFT JOIN bluederm_cialeather.conjuntos bcjt ON bcjt.Codigo=bgru' +
        '.Conjunto '
      
        'LEFT JOIN bluederm_cialeather.plano     bpla ON bpla.Codigo=bcjt' +
        '.Plano '
      'WHERE crt.Tipo IN (0, 1)'
      'AND crt.Codigo > 0'
      'AND crt.GenCtb <> 0'
      'ORDER BY crt.GenCtb ')
    Left = 76
    Top = 348
    object QrCrtsCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrCrtsBPlaCod: TIntegerField
      FieldName = 'BPlaCod'
    end
    object QrCrtsBCjtCod: TIntegerField
      FieldName = 'BCjtCod'
    end
    object QrCrtsBGruCod: TIntegerField
      FieldName = 'BGruCod'
    end
    object QrCrtsBSgrCod: TIntegerField
      FieldName = 'BSgrCod'
    end
    object QrCrtsBCtaCod: TIntegerField
      FieldName = 'BCtaCod'
      Required = True
    end
    object QrCrtsBPlaTxt: TWideStringField
      FieldName = 'BPlaTxt'
      Size = 50
    end
    object QrCrtsBCjtTxt: TWideStringField
      FieldName = 'BCjtTxt'
      Size = 50
    end
    object QrCrtsBGruTxt: TWideStringField
      FieldName = 'BGruTxt'
      Size = 50
    end
    object QrCrtsBSgrTxt: TWideStringField
      FieldName = 'BSgrTxt'
      Size = 50
    end
    object QrCrtsBCtaTxt: TWideStringField
      FieldName = 'BCtaTxt'
      Size = 50
    end
  end
  object frxCTB_BALAN_001_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCTB_BALAN_001_01GetValue
    Left = 520
    Top = 264
    Datasets = <
      item
        DataSet = frxDsCtbBalPIts
        DataSetName = 'frxDsCtbBalPIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456710000000000000
        Top = 181.417440000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCtbBalPIts."XPlaCod"'
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCtbBalPIts."XPlaCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 3.779530000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCtbBalPIts."XPlaTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PLA_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PLA_C]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PLA_D]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCtbBalPIts."XCjtCod"'
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCtbBalPIts."XCjtCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCtbBalPIts."XCjtTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CJT_D]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CJT_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CJT_C]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 272.126160000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCtbBalPIts."XGruCod"'
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCtbBalPIts."XGruCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 340.157700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCtbBalPIts."XGruTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_GRU_D]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_GRU_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_GRU_C]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCtbBalPIts."XSgrCod"'
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCtbBalPIts."XSgrCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 302.362400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCtbBalPIts."XSgrTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SGR_D]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SGR_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SGR_C]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 347.716760000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCtbBalPIts
        DataSetName = 'frxDsCtbBalPIts'
        RowCount = 0
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataField = 'XCtaCod'
          DataSet = frxDsCtbBalPIts
          DataSetName = 'frxDsCtbBalPIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCtbBalPIts."XCtaCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          DataField = 'XCtaTxt'
          DataSet = frxDsCtbBalPIts
          DataSetName = 'frxDsCtbBalPIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCtbBalPIts."XCtaTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          DataField = 'Debito'
          DataSet = frxDsCtbBalPIts
          DataSetName = 'frxDsCtbBalPIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtbBalPIts."Debito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsCtbBalPIts."Credito">-<frxDsCtbBalPIts."Debito">]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          DataField = 'Credito'
          DataSet = frxDsCtbBalPIts
          DataSetName = 'frxDsCtbBalPIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtbBalPIts."Credito"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 100.157536460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 64.252010000000000000
          Width = 619.842920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Plano')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conjunto')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sub-grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 86.929190000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 86.929190000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 86.929190000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Resultado')
          ParentFont = False
          WordWrap = False
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'BALANCETE TESTE (1)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 86.929190000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015770000000000000
        Top = 423.307360000000000000
        Width = 680.315400000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Top = 7.559060000000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL PER'#205'DO:     ')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 480.000310000000000000
        Width = 680.315400000000000000
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxReport1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 416
    Top = 412
    Datasets = <
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456710000000000000
        Top = 181.417440000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupHeader1OnBeforePrint'
        Condition = '<frxDsTotais."Plano">'
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTotais."Plano"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 3.779530000000000000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTotais."NO_PLA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PLA_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PLA_C]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PLA_D]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupHeader2OnBeforePrint'
        Condition = '<frxDsTotais."Conjunto">'
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTotais."Conjunto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTotais."NO_CJT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CJT_D]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CJT_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CJT_C]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 272.126160000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupHeader3OnBeforePrint'
        Condition = '<frxDsTotais."Grupo">'
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTotais."Grupo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 340.157700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTotais."NO_GRU"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_GRU_D]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_GRU_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_GRU_C]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupHeader4OnBeforePrint'
        Condition = '<frxDsTotais."SubGrupo">'
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTotais."SubGrupo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 529.134200000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTotais."NO_SGR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SGR_D]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SGR_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SGR_C]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 347.716760000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        RowCount = 0
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataField = 'Genero'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsTotais."Genero"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          DataField = 'NO_CTA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTotais."NO_CTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          DataField = 'CREDITO'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTotais."CREDITO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          DataField = 'DEBITO'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTotais."DEBITO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTotais."CREDITO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 100.157536460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 64.252010000000000000
          Width = 619.842920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Plano')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conjunto')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sub-grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 86.929190000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 86.929190000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 86.929190000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Resultado')
          ParentFont = False
          WordWrap = False
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'PLANO DE CONTAS COM TOTAIS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 86.929190000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015770000000000000
        Top = 423.307360000000000000
        Width = 680.315400000000000000
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Top = 7.559060000000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL PER'#205'DO:     ')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 7.559060000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsTotais."DEBITO">,MasterData1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 7.559060000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsTotais."CREDITO">,MasterData1)) - (SUM(<frxDsTotais.' +
              '"DEBITO">,MasterData1))]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 7.559060000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsTotais."CREDITO">,MasterData1)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 480.000310000000000000
        Width = 680.315400000000000000
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxDsCtbBalPIts: TfrxDBDataset
    UserName = 'frxDsCtbBalPIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_CtaPlaA=NO_CtaPlaA'
      'NO_CtaPlaB=NO_CtaPlaB'
      'NO_TpBalanc=NO_TpBalanc'
      'Codigo=Codigo'
      'Controle=Controle'
      'Credito=Credito'
      'Debito=Debito'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'APlaCod=APlaCod'
      'ACjtCod=ACjtCod'
      'AGruCod=AGruCod'
      'ASgrCod=ASgrCod'
      'ACtaCod=ACtaCod'
      'BPlaCod=BPlaCod'
      'BCjtCod=BCjtCod'
      'BGruCod=BGruCod'
      'BSgrCod=BSgrCod'
      'BCtaCod=BCtaCod'
      'APlaTxt=APlaTxt'
      'ACjtTxt=ACjtTxt'
      'AGruTxt=AGruTxt'
      'ASgrTxt=ASgrTxt'
      'ACtaTxt=ACtaTxt'
      'BPlaTxt=BPlaTxt'
      'BCjtTxt=BCjtTxt'
      'BGruTxt=BGruTxt'
      'BSgrTxt=BSgrTxt'
      'BCtaTxt=BCtaTxt'
      'NO_TpAmbito=NO_TpAmbito'
      'TpBalanc=TpBalanc'
      'TpAmbito=TpAmbito'
      'NiveisTXT=NiveisTXT'
      'XPlaCod=XPlaCod'
      'XCjtCod=XCjtCod'
      'XGruCod=XGruCod'
      'XSgrCod=XSgrCod'
      'XCtaCod=XCtaCod'
      'XPlaTxt=XPlaTxt'
      'XCjtTxt=XCjtTxt'
      'XGruTxt=XGruTxt'
      'XSgrTxt=XSgrTxt'
      'XCtaTxt=XCtaTxt')
    DataSet = QrCtbBalPIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 404
    Top = 57
  end
  object PMImprime: TPopupMenu
    Left = 92
    Top = 21
    object Balancete1: TMenuItem
      Caption = 'Balancete'
      OnClick = Balancete1Click
    end
    object Balano1: TMenuItem
      Caption = 'Balan'#231'o'
      OnClick = Balano1Click
    end
    object Balancete2: TMenuItem
      Caption = 'Balancete'
      OnClick = Balancete2Click
    end
  end
  object QrCtbBalPLctOpn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctbbalplctopn'
      'WHERE Codigo=2')
    Left = 200
    Top = 298
    object QrCtbBalPLctOpnCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCtbBalPLctOpnData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrCtbBalPLctOpnTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCtbBalPLctOpnCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrCtbBalPLctOpnControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCtbBalPLctOpnSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrCtbBalPLctOpnGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrCtbBalPLctOpnDebito: TFloatField
      FieldName = 'Debito'
      Required = True
    end
    object QrCtbBalPLctOpnCredito: TFloatField
      FieldName = 'Credito'
      Required = True
    end
    object QrCtbBalPLctOpnCompensado: TDateField
      FieldName = 'Compensado'
      Required = True
    end
    object QrCtbBalPLctOpnSit: TIntegerField
      FieldName = 'Sit'
      Required = True
    end
    object QrCtbBalPLctOpnVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrCtbBalPLctOpnID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrCtbBalPLctOpnID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrCtbBalPLctOpnID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Required = True
    end
    object QrCtbBalPLctOpnPago: TFloatField
      FieldName = 'Pago'
      Required = True
    end
    object QrCtbBalPLctOpnGenCtb: TIntegerField
      FieldName = 'GenCtb'
      Required = True
    end
    object QrCtbBalPLctOpnQtDtPg: TIntegerField
      FieldName = 'QtDtPg'
      Required = True
    end
    object QrCtbBalPLctOpnLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrCtbBalPLctOpnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCtbBalPLctOpnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCtbBalPLctOpnUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrCtbBalPLctOpnUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrCtbBalPLctOpnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrCtbBalPLctOpnAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrCtbBalPLctOpnAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrCtbBalPLctOpnAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrCtbBalPLctOpnPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrCtbBalPLctOpnPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrCtbBalPLctOpnRecDes: TFloatField
      FieldName = 'RecDes'
      Required = True
    end
  end
  object DsCtbBalPLctOpn: TDataSource
    DataSet = QrCtbBalPLctOpn
    Left = 200
    Top = 346
  end
  object QrCtbBalPLctEmi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctbbalplctopn'
      'WHERE Codigo=2')
    Left = 308
    Top = 298
    object QrCtbBalPLctEmiCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCtbBalPLctEmiData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrCtbBalPLctEmiTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCtbBalPLctEmiCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrCtbBalPLctEmiControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCtbBalPLctEmiSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrCtbBalPLctEmiGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrCtbBalPLctEmiDebito: TFloatField
      FieldName = 'Debito'
      Required = True
    end
    object QrCtbBalPLctEmiCredito: TFloatField
      FieldName = 'Credito'
      Required = True
    end
    object QrCtbBalPLctEmiCompensado: TDateField
      FieldName = 'Compensado'
      Required = True
    end
    object QrCtbBalPLctEmiSit: TIntegerField
      FieldName = 'Sit'
      Required = True
    end
    object QrCtbBalPLctEmiVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrCtbBalPLctEmiID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrCtbBalPLctEmiID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrCtbBalPLctEmiID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Required = True
    end
    object QrCtbBalPLctEmiPago: TFloatField
      FieldName = 'Pago'
      Required = True
    end
    object QrCtbBalPLctEmiGenCtb: TIntegerField
      FieldName = 'GenCtb'
      Required = True
    end
    object QrCtbBalPLctEmiQtDtPg: TIntegerField
      FieldName = 'QtDtPg'
      Required = True
    end
    object QrCtbBalPLctEmiLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrCtbBalPLctEmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCtbBalPLctEmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCtbBalPLctEmiUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrCtbBalPLctEmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrCtbBalPLctEmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrCtbBalPLctEmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrCtbBalPLctEmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrCtbBalPLctEmiAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrCtbBalPLctEmiPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrCtbBalPLctEmiPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrCtbBalPLctEmiRecDes: TFloatField
      FieldName = 'RecDes'
      Required = True
    end
  end
  object DsCtbBalPLctEmi: TDataSource
    DataSet = QrCtbBalPLctEmi
    Left = 308
    Top = 346
  end
  object QrCBPIO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(TpBalanc<>2, '
      
        ' CONCAT(BPlaCod, ".", LPAD(BCjtCod, 2, "0"), ".", LPAD(BGruCod, ' +
        '2, "0"), ".", LPAD(BSgrCod, 3, "0"), ".", LPAD(BCtaCod, 4, "0"))' +
        ', '
      
        ' CONCAT(APlaCod, ".", LPAD(ACjtCod, 2, "0"), ".", LPAD(AGruCod, ' +
        '2, "0"), ".", LPAD(ASgrCod, 3, "0"), ".", LPAD(ACtaCod, 4, "0"))'
      ') NiveisTXT, '
      'CAST(IF(TpBalanc<>2, BPlaCod, APlaCod) AS UNSIGNED) XPlaCod,'
      'CAST(IF(TpBalanc<>2, BCjtCod, ACjtCod) AS UNSIGNED) XCjtCod,'
      'CAST(IF(TpBalanc<>2, BGruCod, AGruCod) AS UNSIGNED) XGruCod,'
      'CAST(IF(TpBalanc<>2, BSgrCod, ASgrCod) AS UNSIGNED) XSgrCod,'
      'CAST(IF(TpBalanc<>2, BCtaCod, ACtaCod) AS UNSIGNED) XCtaCod,'
      ''
      
        'CAST(IF(TpBalanc<>2, plb.FinContab, pla.FinContab) AS UNSIGNED) ' +
        'ORD_PLA,'
      
        'CAST(IF(TpBalanc<>2, cjb.OrdemLista, cja.OrdemLista) AS UNSIGNED' +
        ') ORD_CJT,'
      
        'CAST(IF(TpBalanc<>2, grb.OrdemLista, gra.OrdemLista) AS UNSIGNED' +
        ') ORD_GRU,'
      
        'CAST(IF(TpBalanc<>2, sgb.OrdemLista, sga.OrdemLista) AS UNSIGNED' +
        ') ORD_SGR,'
      ''
      'IF(TpBalanc<>2, BPlaTxt, APlaTxt) XPlaTxt,'
      'IF(TpBalanc<>2, BCjtTxt, ACjtTxt) XCjtTxt,'
      'IF(TpBalanc<>2, BGruTxt, AGruTxt) XGruTxt,'
      'IF(TpBalanc<>2, BSgrTxt, ASgrTxt) XSgrTxt,'
      'IF(TpBalanc<>2, BCtaTxt, ACtaTxt) XCtaTxt,'
      'coa.Nome NO_CtaPlaA, cob.Nome NO_CtaPlaB,  '
      'ELT(cbi.TpBalanc, "Movimento Cont'#225'bil", "Movimento Financeiro",'
      ' "Manual Cont'#225'bil", "? ? ? ? ? ? ?") NO_TpBalanc, '
      
        'ELT(cbi.TpAmbito, "Lcts Abertos", "Lcts Emitidos", "Saldo Cartei' +
        'ra", '
      
        '"Valor Estoque", "Recup.Impostos", "Imobilizado", "Patrim.L'#237'q", ' +
        '"?????") '
      'NO_TpAmbito, SUM(cbi.Credito) CREDITO, SUM(cbi.Debito) DEBITO '
      'FROM ctbbalpits cbi '
      'LEFT JOIN contas coa ON coa.Codigo=cbi.ACtaCod '
      'LEFT JOIN contas cob ON cob.Codigo=cbi.BCtaCod '
      'LEFT JOIN plano  pla ON pla.Codigo=cbi.APlaCod '
      'LEFT JOIN plano  plb ON plb.Codigo=cbi.BPlaCod '
      'LEFT JOIN conjuntos  cja ON cja.Codigo=cbi.ACjtCod '
      'LEFT JOIN conjuntos  cjb ON cjb.Codigo=cbi.BCjtCod '
      'LEFT JOIN grupos  gra ON gra.Codigo=cbi.AGruCod '
      'LEFT JOIN grupos  grb ON grb.Codigo=cbi.BGruCod '
      'LEFT JOIN subgrupos  sga ON sga.Codigo=cbi.ASGRCod '
      'LEFT JOIN subgrupos  sgb ON sgb.Codigo=cbi.BSGRCod '
      'WHERE cbi.Codigo=3'
      'AND ( '
      '  ( '
      '    ( '
      '      (pla.FinContab IN (1,2)) '
      '      OR '
      '      (plb.FinContab IN (1,2)) '
      '    ) '
      '    AND cbi.TpAmbito = 1'
      '  ) OR ( '
      '    cbi.TpAmbito = 2'
      '  ) '
      ') '
      ''
      'GROUP BY ORD_PLA, ORD_CJT, ORD_GRU, ORD_SGR, XCtaCod'
      'ORDER BY NiveisTXT '
      ' ')
    Left = 516
    Top = 365
    object QrCBPIONiveisTXT: TWideStringField
      FieldName = 'NiveisTXT'
      Size = 26
    end
    object QrCBPIOXPlaCod: TLargeintField
      FieldName = 'XPlaCod'
      Required = True
    end
    object QrCBPIOXCjtCod: TLargeintField
      FieldName = 'XCjtCod'
      Required = True
    end
    object QrCBPIOXGruCod: TLargeintField
      FieldName = 'XGruCod'
      Required = True
    end
    object QrCBPIOXSgrCod: TLargeintField
      FieldName = 'XSgrCod'
      Required = True
    end
    object QrCBPIOXCtaCod: TLargeintField
      FieldName = 'XCtaCod'
      Required = True
    end
    object QrCBPIOORD_PLA: TLargeintField
      FieldName = 'ORD_PLA'
    end
    object QrCBPIOORD_CJT: TLargeintField
      FieldName = 'ORD_CJT'
    end
    object QrCBPIOORD_GRU: TLargeintField
      FieldName = 'ORD_GRU'
    end
    object QrCBPIOORD_SGR: TLargeintField
      FieldName = 'ORD_SGR'
    end
    object QrCBPIOXPlaTxt: TWideStringField
      FieldName = 'XPlaTxt'
      Size = 60
    end
    object QrCBPIOXCjtTxt: TWideStringField
      FieldName = 'XCjtTxt'
      Size = 60
    end
    object QrCBPIOXGruTxt: TWideStringField
      FieldName = 'XGruTxt'
      Size = 60
    end
    object QrCBPIOXSgrTxt: TWideStringField
      FieldName = 'XSgrTxt'
      Size = 60
    end
    object QrCBPIOXCtaTxt: TWideStringField
      FieldName = 'XCtaTxt'
      Size = 60
    end
    object QrCBPIONO_CtaPlaA: TWideStringField
      FieldName = 'NO_CtaPlaA'
      Size = 50
    end
    object QrCBPIONO_CtaPlaB: TWideStringField
      FieldName = 'NO_CtaPlaB'
      Size = 50
    end
    object QrCBPIONO_TpBalanc: TWideStringField
      FieldName = 'NO_TpBalanc'
    end
    object QrCBPIONO_TpAmbito: TWideStringField
      FieldName = 'NO_TpAmbito'
      Size = 14
    end
    object QrCBPIOCREDITO: TFloatField
      FieldName = 'CREDITO'
    end
    object QrCBPIODEBITO: TFloatField
      FieldName = 'DEBITO'
    end
  end
  object frxDsCBPIO: TfrxDBDataset
    UserName = 'frxDsCBPIO'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NiveisTXT=NiveisTXT'
      'XPlaCod=XPlaCod'
      'XCjtCod=XCjtCod'
      'XGruCod=XGruCod'
      'XSgrCod=XSgrCod'
      'XCtaCod=XCtaCod'
      'ORD_PLA=ORD_PLA'
      'ORD_CJT=ORD_CJT'
      'ORD_GRU=ORD_GRU'
      'ORD_SGR=ORD_SGR'
      'XPlaTxt=XPlaTxt'
      'XCjtTxt=XCjtTxt'
      'XGruTxt=XGruTxt'
      'XSgrTxt=XSgrTxt'
      'XCtaTxt=XCtaTxt'
      'NO_CtaPlaA=NO_CtaPlaA'
      'NO_CtaPlaB=NO_CtaPlaB'
      'NO_TpBalanc=NO_TpBalanc'
      'NO_TpAmbito=NO_TpAmbito'
      'CREDITO=Credito'
      'DEBITO=Debito')
    DataSet = QrCBPIO
    BCDToCurrency = False
    DataSetOptions = []
    Left = 516
    Top = 413
  end
  object frxCTB_BALAN_001_02: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCTB_BALAN_001_02GetValue
    Left = 520
    Top = 316
    Datasets = <
      item
        DataSet = frxDsCBPIO
        DataSetName = 'frxDsCBPIO'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456710000000000000
        Top = 181.417440000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCBPIO."ORD_PLA"'
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCBPIO."XPlaCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 3.779530000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCBPIO."XPlaTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PLA_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PLA_C]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PLA_D]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCBPIO."ORD_CJT"'
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCBPIO."XCjtCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCBPIO."XCjtTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CJT_D]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CJT_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CJT_C]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 272.126160000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCBPIO."ORD_GRU"'
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCBPIO."XGruCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 340.157700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCBPIO."XGruTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_GRU_D]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_GRU_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_GRU_C]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCBPIO."ORD_SGR"'
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCBPIO."XSgrCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 302.362400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCBPIO."XSgrTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SGR_D]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SGR_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SGR_C]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 347.716760000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCBPIO
        DataSetName = 'frxDsCBPIO'
        RowCount = 0
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataField = 'XCtaCod'
          DataSet = frxDsCBPIO
          DataSetName = 'frxDsCBPIO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCBPIO."XCtaCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          DataField = 'XCtaTxt'
          DataSet = frxDsCBPIO
          DataSetName = 'frxDsCBPIO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCBPIO."XCtaTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          DataField = 'Debito'
          DataSet = frxDsCBPIO
          DataSetName = 'frxDsCBPIO'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCBPIO."Debito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsCBPIO."Credito">-<frxDsCBPIO."Debito">]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          DataField = 'Credito'
          DataSet = frxDsCBPIO
          DataSetName = 'frxDsCBPIO'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCBPIO."Credito"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 100.157536460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 64.252010000000000000
          Width = 619.842920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Plano')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conjunto')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sub-grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 86.929190000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 86.929190000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 86.929190000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Resultado')
          ParentFont = False
          WordWrap = False
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 86.929190000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015770000000000000
        Top = 423.307360000000000000
        Width = 680.315400000000000000
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 480.000310000000000000
        Width = 680.315400000000000000
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object QrCBPIE: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(TpBalanc<>2, '
      
        ' CONCAT(BPlaCod, ".", LPAD(BCjtCod, 2, "0"), ".", LPAD(BGruCod, ' +
        '2, "0"), ".", LPAD(BSgrCod, 3, "0"), ".", LPAD(BCtaCod, 4, "0"))' +
        ', '
      
        ' CONCAT(APlaCod, ".", LPAD(ACjtCod, 2, "0"), ".", LPAD(AGruCod, ' +
        '2, "0"), ".", LPAD(ASgrCod, 3, "0"), ".", LPAD(ACtaCod, 4, "0"))'
      ') NiveisTXT, '
      ''
      'IF(TpBalanc<>2, BPlaCod, APlaCod) XPlaCod,'
      'IF(TpBalanc<>2, BCjtCod, ACjtCod) XCjtCod,'
      'IF(TpBalanc<>2, BGruCod, AGruCod) XGruCod,'
      'IF(TpBalanc<>2, BSgrCod, ASgrCod) XSgrCod,'
      'IF(TpBalanc<>2, BCtaCod, ACtaCod) XCtaCod,'
      ''
      'IF(TpBalanc<>2, BPlaTxt, APlaTxt) XPlaTxt,'
      'IF(TpBalanc<>2, BCjtTxt, ACjtTxt) XCjtTxt,'
      'IF(TpBalanc<>2, BGruTxt, AGruTxt) XGruTxt,'
      'IF(TpBalanc<>2, BSgrTxt, ASgrTxt) XSgrTxt,'
      'IF(TpBalanc<>2, BCtaTxt, ACtaTxt) XCtaTxt,'
      ''
      ''
      'coa.Nome NO_CtaPlaA, cob.Nome NO_CtaPlaB,  '
      'ELT(cbi.TpBalanc, "Movimento Cont'#225'bil", "Movimento Financeiro",'
      ' "Manual Cont'#225'bil", "? ? ? ? ? ? ?") NO_TpBalanc, '
      'ELT(cbi.TpAmbito, "Financeiro", "[Livre]", "Saldo Carteira", '
      
        '"Valor Estoque", "Recup.Impostos", "Imobilizado", "Patrim.L'#237'q", ' +
        '"?????") '
      'NO_TpAmbito, cbi.* '
      'FROM ctbbalpits cbi '
      'LEFT JOIN contas coa ON coa.Codigo=cbi.ACtaCod '
      'LEFT JOIN contas cob ON cob.Codigo=cbi.BCtaCod '
      'ORDER BY NiveisTXT')
    Left = 672
    Top = 377
    object QrCBPIENO_CtaPlaA: TWideStringField
      FieldName = 'NO_CtaPlaA'
      Size = 50
    end
    object QrCBPIENO_CtaPlaB: TWideStringField
      FieldName = 'NO_CtaPlaB'
      Size = 50
    end
    object QrCBPIENO_TpBalanc: TWideStringField
      FieldName = 'NO_TpBalanc'
      Size = 10
    end
    object QrCBPIECodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCBPIEControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCBPIECredito: TFloatField
      FieldName = 'Credito'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrCBPIEDebito: TFloatField
      FieldName = 'Debito'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrCBPIELk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrCBPIEDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCBPIEDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCBPIEUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrCBPIEUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrCBPIEAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrCBPIEAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrCBPIEAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrCBPIEAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrCBPIEAPlaCod: TIntegerField
      FieldName = 'APlaCod'
      Required = True
    end
    object QrCBPIEACjtCod: TIntegerField
      FieldName = 'ACjtCod'
      Required = True
    end
    object QrCBPIEAGruCod: TIntegerField
      FieldName = 'AGruCod'
      Required = True
    end
    object QrCBPIEASgrCod: TIntegerField
      FieldName = 'ASgrCod'
      Required = True
    end
    object QrCBPIEACtaCod: TIntegerField
      FieldName = 'ACtaCod'
      Required = True
    end
    object QrCBPIEBPlaCod: TIntegerField
      FieldName = 'BPlaCod'
      Required = True
    end
    object QrCBPIEBCjtCod: TIntegerField
      FieldName = 'BCjtCod'
      Required = True
    end
    object QrCBPIEBGruCod: TIntegerField
      FieldName = 'BGruCod'
      Required = True
    end
    object QrCBPIEBSgrCod: TIntegerField
      FieldName = 'BSgrCod'
      Required = True
    end
    object QrCBPIEBCtaCod: TIntegerField
      FieldName = 'BCtaCod'
      Required = True
    end
    object QrCBPIEAPlaTxt: TWideStringField
      FieldName = 'APlaTxt'
      Size = 60
    end
    object QrCBPIEACjtTxt: TWideStringField
      FieldName = 'ACjtTxt'
      Size = 60
    end
    object QrCBPIEAGruTxt: TWideStringField
      FieldName = 'AGruTxt'
      Size = 60
    end
    object QrCBPIEASgrTxt: TWideStringField
      FieldName = 'ASgrTxt'
      Size = 60
    end
    object QrCBPIEACtaTxt: TWideStringField
      FieldName = 'ACtaTxt'
      Size = 60
    end
    object QrCBPIEBPlaTxt: TWideStringField
      FieldName = 'BPlaTxt'
      Size = 60
    end
    object QrCBPIEBCjtTxt: TWideStringField
      FieldName = 'BCjtTxt'
      Size = 60
    end
    object QrCBPIEBGruTxt: TWideStringField
      FieldName = 'BGruTxt'
      Size = 60
    end
    object QrCBPIEBSgrTxt: TWideStringField
      FieldName = 'BSgrTxt'
      Size = 60
    end
    object QrCBPIEBCtaTxt: TWideStringField
      FieldName = 'BCtaTxt'
      Size = 60
    end
    object QrCBPIENO_TpAmbito: TWideStringField
      FieldName = 'NO_TpAmbito'
      Size = 30
    end
    object QrCBPIETpBalanc: TSmallintField
      FieldName = 'TpBalanc'
    end
    object QrCBPIETpAmbito: TSmallintField
      FieldName = 'TpAmbito'
    end
    object QrCBPIENiveisTXT: TWideStringField
      FieldName = 'NiveisTXT'
      Size = 59
    end
    object QrCBPIEXPlaTxt: TWideStringField
      FieldName = 'XPlaTxt'
      Size = 60
    end
    object QrCBPIEXCjtTxt: TWideStringField
      FieldName = 'XCjtTxt'
      Size = 60
    end
    object QrCBPIEXGruTxt: TWideStringField
      FieldName = 'XGruTxt'
      Size = 60
    end
    object QrCBPIEXSgrTxt: TWideStringField
      FieldName = 'XSgrTxt'
      Size = 60
    end
    object QrCBPIEXCtaTxt: TWideStringField
      FieldName = 'XCtaTxt'
      Size = 60
    end
    object QrCBPIEXPlaCod: TLargeintField
      FieldName = 'XPlaCod'
    end
    object QrCBPIEXCjtCod: TLargeintField
      FieldName = 'XCjtCod'
    end
    object QrCBPIEXGruCod: TLargeintField
      FieldName = 'XGruCod'
    end
    object QrCBPIEXSgrCod: TLargeintField
      FieldName = 'XSgrCod'
    end
    object QrCBPIEXCtaCod: TLargeintField
      FieldName = 'XCtaCod'
    end
    object QrCBPIEORD_PLA: TLargeintField
      FieldName = 'ORD_PLA'
    end
    object QrCBPIEORD_CJT: TLargeintField
      FieldName = 'ORD_CJT'
    end
    object QrCBPIEORD_GRU: TLargeintField
      FieldName = 'ORD_GRU'
    end
    object QrCBPIEORD_SGR: TLargeintField
      FieldName = 'ORD_SGR'
    end
  end
  object frxDsCBPIE: TfrxDBDataset
    UserName = 'frxDsCBPIE'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_CtaPlaA=NO_CtaPlaA'
      'NO_CtaPlaB=NO_CtaPlaB'
      'NO_TpBalanc=NO_TpBalanc'
      'Codigo=Codigo'
      'Controle=Controle'
      'Credito=Credito'
      'Debito=Debito'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'APlaCod=APlaCod'
      'ACjtCod=ACjtCod'
      'AGruCod=AGruCod'
      'ASgrCod=ASgrCod'
      'ACtaCod=ACtaCod'
      'BPlaCod=BPlaCod'
      'BCjtCod=BCjtCod'
      'BGruCod=BGruCod'
      'BSgrCod=BSgrCod'
      'BCtaCod=BCtaCod'
      'APlaTxt=APlaTxt'
      'ACjtTxt=ACjtTxt'
      'AGruTxt=AGruTxt'
      'ASgrTxt=ASgrTxt'
      'ACtaTxt=ACtaTxt'
      'BPlaTxt=BPlaTxt'
      'BCjtTxt=BCjtTxt'
      'BGruTxt=BGruTxt'
      'BSgrTxt=BSgrTxt'
      'BCtaTxt=BCtaTxt'
      'NO_TpAmbito=NO_TpAmbito'
      'TpBalanc=TpBalanc'
      'TpAmbito=TpAmbito'
      'NiveisTXT=NiveisTXT'
      'XPlaTxt=XPlaTxt'
      'XCjtTxt=XCjtTxt'
      'XGruTxt=XGruTxt'
      'XSgrTxt=XSgrTxt'
      'XCtaTxt=XCtaTxt'
      'XPlaCod=XPlaCod'
      'XCjtCod=XCjtCod'
      'XGruCod=XGruCod'
      'XSgrCod=XSgrCod'
      'XCtaCod=XCtaCod'
      'ORD_PLA=ORD_PLA'
      'ORD_CJT=ORD_CJT'
      'ORD_GRU=ORD_GRU'
      'ORD_SGR=ORD_SGR')
    DataSet = QrCBPIE
    BCDToCurrency = False
    DataSetOptions = []
    Left = 672
    Top = 425
  end
  object frxCTB_BALAN_001_03: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCTB_BALAN_001_03GetValue
    Left = 672
    Top = 324
    Datasets = <
      item
        DataSet = frxDsCBPIE
        DataSetName = 'frxDsCBPIE'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456710000000000000
        Top = 181.417440000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCBPIE."XPlaCod"'
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCBPIE."XPlaCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 3.779530000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCBPIE."XPlaTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PLA_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PLA_C]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PLA_D]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCBPIE."XCjtCod"'
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCBPIE."XCjtCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCBPIE."XCjtTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CJT_D]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CJT_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CJT_C]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 272.126160000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCBPIE."XGruCod"'
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCBPIE."XGruCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 340.157700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCBPIE."XGruTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_GRU_D]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_GRU_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_GRU_C]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCBPIE."XSgrCod"'
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCBPIE."XSgrCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 302.362400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCBPIE."XSgrTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SGR_D]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SGR_R]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_SGR_C]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 347.716760000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCBPIE
        DataSetName = 'frxDsCBPIE'
        RowCount = 0
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataField = 'XCtaCod'
          DataSet = frxDsCBPIE
          DataSetName = 'frxDsCBPIE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCBPIE."XCtaCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          DataField = 'XCtaTxt'
          DataSet = frxDsCBPIE
          DataSetName = 'frxDsCBPIE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCBPIE."XCtaTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          DataField = 'Debito'
          DataSet = frxDsCBPIE
          DataSetName = 'frxDsCBPIE'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCBPIE."Debito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsCBPIE."Credito">-<frxDsCBPIE."Debito">]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          DataField = 'Credito'
          DataSet = frxDsCBPIE
          DataSetName = 'frxDsCBPIE'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCBPIE."Credito"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 100.157536460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 64.252010000000000000
          Width = 619.842920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Plano')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conjunto')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sub-grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 86.929190000000000000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 86.929190000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 86.929190000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Resultado')
          ParentFont = False
          WordWrap = False
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 86.929190000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015770000000000000
        Top = 423.307360000000000000
        Width = 680.315400000000000000
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 480.000310000000000000
        Width = 680.315400000000000000
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object QrErrsG: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emi.Controle Lancamento, 1 CodWarning, '
      'emi.Genero, emi.GenCtb,  '
      
        '"Lan'#231'amento com conta cont'#225'bil no campo financeiro!" MsgWarning ' +
        ' '
      'FROM ctbbalplctemi  emi '
      'LEFT JOIN carteiras car ON car.Codigo=emi.Carteira '
      'LEFT JOIN contas    cta ON cta.Codigo=emi.Genero '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo '
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo '
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto '
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano '
      'WHERE  emi.Codigo=3 '
      'AND pla.FinContab<>3 '
      ' '
      'UNION '
      ' '
      'SELECT emi.Controle Lancamento, 2 CodWarning, '
      'emi.Genero, emi.GenCtb,  '
      '"Lan'#231'amento com conta finaneira no campo cont'#225'bil!" MsgWarning  '
      'FROM ctbbalplctemi  emi '
      'LEFT JOIN carteiras car ON car.Codigo=emi.Carteira '
      'LEFT JOIN contas    cta ON cta.Codigo=emi.GenCtb '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo '
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo '
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto '
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano '
      'WHERE emi.Codigo=3 '
      'AND emi.GenCtb <> 0 '
      'AND pla.FinContab=3 '
      ' '
      'UNION  '
      ' '
      'SELECT emi.Controle Lancamento, 3 CodWarning,  '
      'emi.Genero, emi.GenCtb,  '
      
        '"Lan'#231'amento com conta cont'#225'bil no campo financeiro!" MsgWarning ' +
        ' '
      'FROM ctbbalplctopn  emi '
      'LEFT JOIN carteiras car ON car.Codigo=emi.Carteira '
      'LEFT JOIN contas    cta ON cta.Codigo=emi.Genero '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo '
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo '
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto '
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano '
      'WHERE  emi.Codigo=3 '
      'AND pla.FinContab<>3 '
      ' '
      'UNION '
      ' '
      'SELECT emi.Controle Lancamento, 4 CodWarning, '
      'emi.Genero, emi.GenCtb,  '
      '"Lan'#231'amento com conta finaneira no campo cont'#225'bil!" MsgWarning  '
      'FROM ctbbalplctopn  emi '
      'LEFT JOIN carteiras car ON car.Codigo=emi.Carteira '
      'LEFT JOIN contas    cta ON cta.Codigo=emi.GenCtb '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo '
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo '
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto '
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano '
      'WHERE emi.Codigo=3 '
      'AND emi.GenCtb <> 0 '
      'AND pla.FinContab=3 '
      ''
      'UNION '
      ' '
      'SELECT emi.Controle Lancamento, 5 CodWarning, '
      'emi.Genero, emi.GenCtb,  '
      
        '"Lan'#231'amento Financeiro com Plano sem defini'#231#227'o Cont'#225'bil" MsgWarn' +
        'ing  '
      'FROM ctbbalplctopn  emi '
      'LEFT JOIN carteiras car ON car.Codigo=emi.Carteira '
      'LEFT JOIN contas    cta ON cta.Codigo=emi.Genero '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo '
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo '
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto '
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano '
      'WHERE emi.Codigo=3 '
      'AND pla.FinContab=0 '
      ' '
      'UNION '
      ' '
      'SELECT emi.Controle Lancamento, 6 CodWarning, '
      'emi.Genero, emi.GenCtb,  '
      
        '"Lan'#231'amento Cont'#225'bil com Plano sem defini'#231#227'o Cont'#225'bil" MsgWarnin' +
        'g  '
      'FROM ctbbalplctopn  emi '
      'LEFT JOIN carteiras car ON car.Codigo=emi.Carteira '
      'LEFT JOIN contas    cta ON cta.Codigo=emi.GenCtb '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo '
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo '
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto '
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano '
      'WHERE emi.Codigo=3 '
      'AND emi.GenCtb <> 0 '
      'AND pla.FinContab=0'
      ' '
      ' ')
    Left = 336
    Top = 209
    object QrErrsGLancamento: TIntegerField
      FieldName = 'Lancamento'
      Required = True
    end
    object QrErrsGCodWarning: TLargeintField
      FieldName = 'CodWarning'
      Required = True
    end
    object QrErrsGMsgWarning: TWideStringField
      FieldName = 'MsgWarning'
      Required = True
      Size = 54
    end
    object QrErrsGGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrErrsGGenCtb: TIntegerField
      FieldName = 'GenCtb'
      Required = True
    end
  end
end
