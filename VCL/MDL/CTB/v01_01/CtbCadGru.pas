unit CtbCadGru;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmCtbCadGru = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrCtbCadGru: TMySQLQuery;
    DsCtbCadGru: TDataSource;
    QrCtbCadIts: TMySQLQuery;
    DsCtbCadIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrCtbCadGruCodigo: TIntegerField;
    QrCtbCadGruNome: TWideStringField;
    QrCtbCadItsCodigo: TIntegerField;
    QrCtbCadItsControle: TIntegerField;
    QrCtbCadItsCtbCadMoC: TIntegerField;
    QrCtbCadItsContC: TIntegerField;
    QrCtbCadItsNO_CtbCadMoC: TWideStringField;
    QrCtbCadItsNO_ContC: TWideStringField;
    QrCtbCadItsCtbCadMoF: TIntegerField;
    QrCtbCadItsConta: TIntegerField;
    QrCtbCadItsNO_CtbCadMoF: TWideStringField;
    QrCtbCadItsNO_Conta: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCtbCadGruAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCtbCadGruBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrCtbCadGruAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrCtbCadGruBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraCtbCadIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenCtbCadIts(Controle: Integer);

  end;

var
  FmCtbCadGru: TFmCtbCadGru;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, CtbCadIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCtbCadGru.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCtbCadGru.MostraCtbCadIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmCtbCadIts, FmCtbCadIts, afmoNegarComAviso) then
  begin
    FmCtbCadIts.ImgTipo.SQLType := SQLType;
    FmCtbCadIts.FQrCab := QrCtbCadGru;
    FmCtbCadIts.FDsCab := DsCtbCadGru;
    FmCtbCadIts.FQrIts := QrCtbCadIts;
    FmCtbCadIts.FCodigo := QrCtbCadGruCodigo.Value;
    FmCtbCadIts.EdNome.ValueVariant := QrCtbCadGruNome.Value;
    if SQLType = stIns then
      //
    else
    begin
      FmCtbCadIts.EdControle.ValueVariant := QrCtbCadItsControle.Value;
      //
      FmCtbCadIts.EdCtbCadMoF.ValueVariant := QrCtbCadItsCtbCadMoF.Value;
      FmCtbCadIts.CBCtbCadMoF.KeyValue     := QrCtbCadItsCtbCadMoF.Value;
      //
      FmCtbCadIts.EdConta.ValueVariant := QrCtbCadItsConta.Value;
      FmCtbCadIts.CBConta.KeyValue     := QrCtbCadItsConta.Value;
      //
(*
      FmCtbCadIts.EdCtbCadMoC.ValueVariant := QrCtbCadItsCtbCadMoC.Value;
      FmCtbCadIts.CBCtbCadMoC.KeyValue     := QrCtbCadItsCtbCadMoC.Value;
*)
      //
      FmCtbCadIts.EdContC.ValueVariant := QrCtbCadItsContC.Value;
      FmCtbCadIts.CBContC.KeyValue     := QrCtbCadItsContC.Value;
      //
    end;
    FmCtbCadIts.ShowModal;
    FmCtbCadIts.Destroy;
  end;
end;

procedure TFmCtbCadGru.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrCtbCadGru);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrCtbCadGru, QrCtbCadIts);
end;

procedure TFmCtbCadGru.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrCtbCadGru);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrCtbCadIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrCtbCadIts);
end;

procedure TFmCtbCadGru.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCtbCadGruCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCtbCadGru.DefParams;
begin
  VAR_GOTOTABELA := 'ctbcadgru';
  VAR_GOTOMYSQLTABLE := QrCtbCadGru;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM ctbcadgru');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCtbCadGru.ItsAltera1Click(Sender: TObject);
begin
  MostraCtbCadIts(stUpd);
end;

procedure TFmCtbCadGru.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmCtbCadGru.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCtbCadGru.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCtbCadGru.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'CtbCadIts', 'Controle', QrCtbCadItsControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrCtbCadIts,
      QrCtbCadItsControle, QrCtbCadItsControle.Value);
    ReopenCtbCadIts(Controle);
  end;
}
end;

procedure TFmCtbCadGru.ReopenCtbCadIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtbCadIts, Dmod.MyDB, [
  'SELECT cci.*, ',
  'ccm.Nome NO_CtbCadMoF, cta.Nome NO_Conta, ',
  'ccc.Nome NO_CtbCadMoC, cts.Nome NO_ContC ',
  'FROM ctbcadits cci ',
  'LEFT JOIN ctbcadmof ccm ON ccm.Codigo=cci.CtbCadMoF ',
  'LEFT JOIN plaallcad cta ON cta.Codigo=cci.Conta ',
  'LEFT JOIN ctbcadmof ccc ON ccc.Codigo=cci.CtbCadMoC ',
  'LEFT JOIN plaallcad cts ON cts.Codigo=cci.ContC ',
  'WHERE cci.Codigo=' + Geral.FF0(QrCtbCadGruCodigo.Value),
  '']);
  //
  QrCtbCadIts.Locate('Controle', Controle, []);
end;


procedure TFmCtbCadGru.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCtbCadGru.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCtbCadGru.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCtbCadGru.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCtbCadGru.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCtbCadGru.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtbCadGru.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCtbCadGruCodigo.Value;
  Close;
end;

procedure TFmCtbCadGru.ItsInclui1Click(Sender: TObject);
begin
  MostraCtbCadIts(stIns);
end;

procedure TFmCtbCadGru.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCtbCadGru, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ctbcadgru');
end;

procedure TFmCtbCadGru.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ctbcadgru', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctbcadgru', False, [
  'Nome'], [
  'Codigo'], [
  Nome], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmCtbCadGru.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ctbcadgru', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ctbcadgru', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCtbCadGru.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmCtbCadGru.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmCtbCadGru.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmCtbCadGru.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCtbCadGruCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCtbCadGru.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCtbCadGru.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCtbCadGruCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCtbCadGru.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCtbCadGru.QrCtbCadGruAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCtbCadGru.QrCtbCadGruAfterScroll(DataSet: TDataSet);
begin
  ReopenCtbCadIts(0);
end;

procedure TFmCtbCadGru.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrCtbCadGruCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmCtbCadGru.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCtbCadGruCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ctbcadgru', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCtbCadGru.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtbCadGru.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCtbCadGru, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ctbcadgru');
end;

procedure TFmCtbCadGru.QrCtbCadGruBeforeClose(
  DataSet: TDataSet);
begin
  QrCtbCadIts.Close;
end;

procedure TFmCtbCadGru.QrCtbCadGruBeforeOpen(DataSet: TDataSet);
begin
  QrCtbCadGruCodigo.DisplayFormat := FFormatFloat;
end;

end.

