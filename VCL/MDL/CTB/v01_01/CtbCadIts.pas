unit CtbCadIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmCtbCadIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrCtbCadMoF: TMySQLQuery;
    QrCtbCadMoFCodigo: TIntegerField;
    QrCtbCadMoFNome: TWideStringField;
    DsCtbCadMoF: TDataSource;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    EdCtbCadMoF: TdmkEditCB;
    CBCtbCadMoF: TdmkDBLookupComboBox;
    CBConta: TdmkDBLookupComboBox;
    EdConta: TdmkEditCB;
    Label2: TLabel;
    SbCtbCadMoF: TSpeedButton;
    SbGenCtbD: TSpeedButton;
    Label4: TLabel;
    EdContC: TdmkEditCB;
    CBContC: TdmkDBLookupComboBox;
    SbContC: TSpeedButton;
    DsPlaAllCad_D: TDataSource;
    QrPlaAllCad_D: TMySQLQuery;
    QrPlaAllCad_DCodigo: TIntegerField;
    QrPlaAllCad_DNome: TWideStringField;
    QrPlaAllCad_DNivel: TSmallintField;
    QrPlaAllCad_DAnaliSinte: TIntegerField;
    QrPlaAllCad_DCredito: TSmallintField;
    QrPlaAllCad_DDebito: TSmallintField;
    QrPlaAllCad_DOrdens: TWideStringField;
    DsPlaAllCad_C: TDataSource;
    QrPlaAllCad_C: TMySQLQuery;
    QrPlaAllCad_CCodigo: TIntegerField;
    QrPlaAllCad_CNome: TWideStringField;
    QrPlaAllCad_CNivel: TSmallintField;
    QrPlaAllCad_CAnaliSinte: TIntegerField;
    QrPlaAllCad_CCredito: TSmallintField;
    QrPlaAllCad_CDebito: TSmallintField;
    QrPlaAllCad_COrdens: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbCtbCadMoFClick(Sender: TObject);
    procedure SbCtbCadMoCClick(Sender: TObject);
    procedure SbContCClick(Sender: TObject);
    procedure SbGenCtbDClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCtbCadIts(Controle: Integer);
  public
    { Public declarations }
    FCodigo: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmCtbCadIts: TFmCtbCadIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnFinanceiroJan, ModuleFin;

{$R *.DFM}

procedure TFmCtbCadIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, CtbCadMoF, Conta, CtbCadMoC, ContC: Integer;
  SQLType: TSQLType;
begin
  SQLType   := ImgTipo.SQLType;
  Codigo    := FCodigo;
  Controle  := EdControle.ValueVariant;
  CtbCadMoF := EdCtbCadMoF.ValueVariant;
  Conta     := EdConta.ValueVariant;
  CtbCadMoC := EdCtbCadMoF.ValueVariant; //EdCtbCadMoC.ValueVariant;
  ContC     := EdContC.ValueVariant;
(*
  if MyObjects.FIC(CtbCadMoF = 0, EdCtbCadMoF, 'Informe o movimento fiscal no cont�bil!') then
    Exit;
  if MyObjects.FIC(Conta = 0, EdConta, 'Informe a conta do plano de contas!') then
    Exit;
*)
  Controle := UMyMod.BPGS1I32('ctbcadits', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctbcadits', False, [
  'Codigo',
  'CtbCadMoF', 'Conta',
  'CtbCadMoC', 'ContC'
  ], [
  'Controle'], [
  Codigo,
  CtbCadMoF, Conta,
  CtbCadMoC, ContC
  ], [
  Controle], True) then
  begin
    ReopenCtbCadIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;

      EdCtbCadMoF.ValueVariant := 0;
      CBCtbCadMoF.KeyValue     := Null;
      EdConta.ValueVariant     := 0;
      CBConta.KeyValue         := Null;

(*
      EdCtbCadMoC.ValueVariant := 0;
      CBCtbCadMoC.KeyValue     := Null;
*)
      EdContC.ValueVariant     := 0;
      CBContC.KeyValue         := Null;
      //
      EdCtbCadMoF.SetFocus;
      //
    end else Close;
  end;
end;

procedure TFmCtbCadIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtbCadIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCtbCadIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrCtbCadMoF, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPlaAllCad_D, Dmod.MyDB);
  //
  //UnDmkDAC_PF.AbreQuery(QrCtbCadMoC, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPlaAllCad_C, Dmod.MyDB);
end;

procedure TFmCtbCadIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtbCadIts.ReopenCtbCadIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmCtbCadIts.SbContCClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  //FinanceiroJan.MostraFormContas(EdCtbPlaCta.ValueVariant);
  DModFin.MostraFormPlaAny(EdContC, CBContC, QrPlaAllCad_C, SbContC);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdContC, CBContC, QrPlaAllCad_C, VAR_CADASTRO);
{$EndIf}
end;

procedure TFmCtbCadIts.SbCtbCadMoCClick(Sender: TObject);
begin
(*
  VAR_CADASTRO := 0;
  FinanceiroJan.MostraFormCtbCadMoF(EdCtbCadMoC.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCtbCadMoC, CBCtbCadMoC, QrCtbCadMoC, VAR_CADASTRO);
*)
end;

procedure TFmCtbCadIts.SbCtbCadMoFClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FinanceiroJan.MostraFormCtbCadMoF(EdCtbCadMoF.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdCtbCadMoF, CBCtbCadMoF, QrCtbCadMoF, VAR_CADASTRO);
end;

procedure TFmCtbCadIts.SbGenCtbDClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  //FinanceiroJan.MostraFormContas(EdCtbPlaCta.ValueVariant);
  DModFin.MostraFormPlaAny(EdConta, CBConta, QrPlaAllCad_D, SbGenCtbD);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdConta, CBConta, QrPlaAllCad_D, VAR_CADASTRO);
{$EndIf}
end;

end.
