object FmCtbGruPsq: TFmCtbGruPsq
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTB-005 :: Pesquisa de Grupo Cont'#225'bil'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 340
        Height = 32
        Caption = 'Pesquisa de Grupo Cont'#225'bil'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 340
        Height = 32
        Caption = 'Pesquisa de Grupo Cont'#225'bil'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 340
        Height = 32
        Caption = 'Pesquisa de Grupo Cont'#225'bil'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkUsarAsDuasContas: TCheckBox
        Left = 144
        Top = 16
        Width = 493
        Height = 17
        Caption = 
          'Usar as duas contas (financeiro e cont'#225'bil) do item da pesquisa ' +
          'selecionado.'
        TabOrder = 1
        OnClick = CkIncluiCtasClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 213
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 201
      ExplicitWidth = 812
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 65
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 85
        Height = 13
        Caption = 'Descri'#231#227'o parcial:'
      end
      object EdPesq: TdmkEdit
        Left = 8
        Top = 20
        Width = 648
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdPesqChange
      end
      object CkIncluiCtas: TCheckBox
        Left = 8
        Top = 44
        Width = 193
        Height = 17
        Caption = 'Inclui contas atreladas na pesquisa.'
        TabOrder = 1
        OnClick = CkIncluiCtasClick
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 65
      Width = 1008
      Height = 148
      Align = alTop
      DataSource = DsCtbCadGru
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Visible = True
        end>
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 218
      Width = 1008
      Height = 249
      Align = alClient
      DataSource = DsCtbCadIts
      TabOrder = 2
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CtbCadMoF'
          Title.Caption = 'ID mov.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CtbCadMoF'
          Title.Caption = 'Movimento fiscal no cont'#225'bil'
          Width = 147
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Conta'
          Title.Caption = 'Conta a d'#233'bito do plano de contas'
          Width = 308
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ContC'
          Title.Caption = 'Conta'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ContC'
          Title.Caption = 'Conta a cr'#233'dito do plano de contas'
          Width = 308
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrCtbCadGru: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCtbCadGruBeforeClose
    AfterScroll = QrCtbCadGruAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM ctbcadgru'
      'ORDER BY Nome')
    Left = 92
    Top = 233
    object QrCtbCadGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtbCadGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCtbCadGru: TDataSource
    DataSet = QrCtbCadGru
    Left = 92
    Top = 277
  end
  object QrCtbCadIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM ctbcadits'
      'ORDER BY Nome')
    Left = 188
    Top = 237
    object QrCtbCadItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtbCadItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCtbCadItsCtbCadMoF: TIntegerField
      FieldName = 'CtbCadMoF'
    end
    object QrCtbCadItsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCtbCadItsNO_CtbCadMoF: TWideStringField
      FieldName = 'NO_CtbCadMoF'
      Size = 60
    end
    object QrCtbCadItsNO_Conta: TWideStringField
      FieldName = 'NO_Conta'
      Size = 60
    end
    object QrCtbCadItsCtbCadMoC: TIntegerField
      FieldName = 'CtbCadMoC'
    end
    object QrCtbCadItsContC: TIntegerField
      DisplayLabel = 'ContC'#39
      FieldName = 'ContC'
    end
    object QrCtbCadItsNO_CtbCadMoC: TWideStringField
      FieldName = 'NO_CtbCadMoC'
      Size = 60
    end
    object QrCtbCadItsNO_ContC: TWideStringField
      FieldName = 'NO_ContC'
      Size = 60
    end
  end
  object DsCtbCadIts: TDataSource
    DataSet = QrCtbCadIts
    Left = 188
    Top = 281
  end
end
