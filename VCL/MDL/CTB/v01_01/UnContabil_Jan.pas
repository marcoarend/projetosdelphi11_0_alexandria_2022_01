unit UnContabil_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, Variants;

type
  TUnContabil_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormCtbBalPCab1();
    procedure MostraFormCtbBalPCab2();
    procedure MostraFormCtbBalPErrG(TabLctA, SQL: String);
    //
    procedure MostraFormDreCfgCab();
    procedure MostraFormDreGerCab();
    //
    procedure MostraFormCtbGruPsq(var CtaFin, CtaCtb: Integer; var UsarAsDuas:
              Boolean);
  end;

var
  Contabil_Jan: TUnContabil_Jan;


implementation

uses
  MyDBCheck, Module, CfgCadLista, DmkDAC_PF,
  CtbBalPCab, CtbBalPCab2, CtbBalPErrG, CtbGruPsq,
  DreGerCab, DreCfgCab;

{ TUnContabil_Jan }

procedure TUnContabil_Jan.MostraFormCtbBalPCab1();
begin
  if DBCheck.CriaFm(TFmCtbBalPCab, FmCtbBalPCab, afmoNegarComAviso) then
  begin
    FmCtbBalPCab.ShowModal;
    FmCtbBalPCab.Destroy;
  end;
end;

procedure TUnContabil_Jan.MostraFormCtbBalPCab2();
begin
  if DBCheck.CriaFm(TFmCtbBalPCab2, FmCtbBalPCab2, afmoNegarComAviso) then
  begin
    FmCtbBalPCab2.ShowModal;
    FmCtbBalPCab2.Destroy;
  end;
end;

procedure TUnContabil_Jan.MostraFormCtbBalPErrG(TabLctA, SQL: String);
begin
  if DBCheck.CriaFm(TFmCtbBalPErrG, FmCtbBalPErrG, afmoNegarComAviso) then
  begin
    FmCtbBalPErrG.FTabLctA := TabLctA;
    FmCtbBalPErrG.QrErrsG.SQL.Text := SQL;
    UnDmkDAC_PF.AbreQuery(FmCtbBalPErrG.QrErrsG, Dmod.MyDB);
    FmCtbBalPErrG.ShowModal;
    FmCtbBalPErrG.Destroy;
  end;
end;

procedure TUnContabil_Jan.MostraFormCtbGruPsq(var CtaFin, CtaCtb: Integer;
  var UsarAsDuas: Boolean);
begin
  if DBCheck.CriaFm(TFmCtbGruPsq, FmCtbGruPsq, afmoNegarComAviso) then
  begin
    FmCtbGruPsq.FCtaFin := CtaFin;
    FmCtbGruPsq.FCtaCtb := CtaCtb;
    FmCtbGruPsq.FUsarAsDuas := UsarAsDuas;
    FmCtbGruPsq.ShowModal;
    FmCtbGruPsq.Destroy;
    CtaFin := FmCtbGruPsq.FCtaFin;
    CtaCtb := FmCtbGruPsq.FCtaCtb;
    UsarAsDuas := FmCtbGruPsq.FUsarAsDuas;
  end;
end;

procedure TUnContabil_Jan.MostraFormDreCfgCab();
begin
  if DBCheck.CriaFm(TFmDreCfgCab, FmDreCfgCab, afmoNegarComAviso) then
  begin
    FmDreCfgCab.ShowModal;
    FmDreCfgCab.Destroy;
  end;
end;

procedure TUnContabil_Jan.MostraFormDreGerCab();
begin
  if DBCheck.CriaFm(TFmDreGerCab, FmDreGerCab, afmoNegarComAviso) then
  begin
    FmDreGerCab.ShowModal;
    FmDreGerCab.Destroy;
  end;
end;

end.
