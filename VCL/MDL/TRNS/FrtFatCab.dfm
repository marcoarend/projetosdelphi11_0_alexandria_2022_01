object FmFrtFatCab: TFmFrtFatCab
  Left = 368
  Top = 194
  Caption = 'FAT-FRETE-001 :: Faturamento de Frete'
  ClientHeight = 804
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 708
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 129
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 40
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label7: TLabel
          Left = 16
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label9: TLabel
          Left = 76
          Top = 0
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label3: TLabel
          Left = 548
          Top = 0
          Width = 45
          Height = 13
          Caption = 'Tomador:'
          Enabled = False
        end
        object SbTomador: TSpeedButton
          Left = 966
          Top = 16
          Width = 21
          Height = 21
          Caption = '...'
          Enabled = False
        end
        object EdCodigo: TdmkEdit
          Left = 16
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdEmpresa: TdmkEditCB
          Left = 80
          Top = 16
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdEmpresaRedefinido
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 132
          Top = 16
          Width = 409
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 2
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdTomador: TdmkEditCB
          Left = 548
          Top = 16
          Width = 52
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Tomador'
          UpdCampo = 'Tomador'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBTomador
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBTomador: TdmkDBLookupComboBox
          Left = 600
          Top = 16
          Width = 365
          Height = 21
          Enabled = False
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          ListSource = DsTomadores
          TabOrder = 4
          dmkEditCB = EdTomador
          QryCampo = 'Tomador'
          UpdType = utNil
          LocF7SQLMasc = '$#'
        end
      end
      object Panel9: TPanel
        Left = 2
        Top = 55
        Width = 1004
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object Label5: TLabel
          Left = 16
          Top = 4
          Width = 55
          Height = 13
          Caption = 'Remetente:'
        end
        object Label8: TLabel
          Left = 504
          Top = 4
          Width = 59
          Height = 13
          Caption = 'Destinatario:'
        end
        object SbDestinatario: TSpeedButton
          Left = 966
          Top = 0
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbDestinatarioClick
        end
        object SbRemetente: TSpeedButton
          Left = 478
          Top = 0
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbRemetenteClick
        end
        object EdRemetente: TdmkEditCB
          Left = 80
          Top = 0
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Remetente'
          UpdCampo = 'Remetente'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdRemetenteRedefinido
          DBLookupComboBox = CBRemetente
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBRemetente: TdmkDBLookupComboBox
          Left = 132
          Top = 0
          Width = 345
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          ListSource = DsRemetentes
          TabOrder = 1
          dmkEditCB = EdRemetente
          QryCampo = 'Remetente'
          UpdType = utNil
          LocF7SQLMasc = '$#'
        end
        object EdDestinatario: TdmkEditCB
          Left = 568
          Top = 0
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Destinatario'
          UpdCampo = 'Destinatario'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdDestinatarioRedefinido
          DBLookupComboBox = CBDestinatario
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBDestinatario: TdmkDBLookupComboBox
          Left = 620
          Top = 0
          Width = 345
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          ListSource = DsDestinatarios
          TabOrder = 3
          dmkEditCB = EdDestinatario
          QryCampo = 'Destinatario'
          UpdType = utNil
          LocF7SQLMasc = '$#'
        end
      end
      object Panel8: TPanel
        Left = 2
        Top = 79
        Width = 1004
        Height = 48
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 2
        object Label11: TLabel
          Left = 16
          Top = 4
          Width = 59
          Height = 13
          Caption = 'Regra fiscal:'
        end
        object SbFrtRegFisC: TSpeedButton
          Left = 478
          Top = 0
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbFrtRegFisCClick
        end
        object Label6: TLabel
          Left = 504
          Top = 4
          Width = 56
          Height = 13
          Caption = 'Recebedor:'
        end
        object SbRecebedor: TSpeedButton
          Left = 966
          Top = 0
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbRecebedorClick
        end
        object Label4: TLabel
          Left = 16
          Top = 28
          Width = 50
          Height = 13
          Caption = 'Expedidor:'
        end
        object SbExpedidor: TSpeedButton
          Left = 478
          Top = 24
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbExpedidorClick
        end
        object Label10: TLabel
          Left = 504
          Top = 28
          Width = 76
          Height = 13
          Caption = 'Outro (tomador):'
        end
        object SbToma4: TSpeedButton
          Left = 966
          Top = 24
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbToma4Click
        end
        object EdFrtRegFisC: TdmkEditCB
          Left = 80
          Top = 0
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FrtRegFisC'
          UpdCampo = 'FrtRegFisC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdFrtRegFisCRedefinido
          DBLookupComboBox = CBFrtRegFisC
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBFrtRegFisC: TdmkDBLookupComboBox
          Left = 132
          Top = 0
          Width = 345
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsFrtRegFisC
          TabOrder = 1
          dmkEditCB = EdFrtRegFisC
          QryCampo = 'FrtRegFisC'
          UpdType = utNil
          LocF7SQLMasc = '$#'
        end
        object EdRecebedor: TdmkEditCB
          Left = 568
          Top = 0
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Recebedor'
          UpdCampo = 'Recebedor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdRecebedorRedefinido
          DBLookupComboBox = CBRecebedor
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBRecebedor: TdmkDBLookupComboBox
          Left = 620
          Top = 0
          Width = 345
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          ListSource = DsRecebedores
          TabOrder = 3
          dmkEditCB = EdRecebedor
          QryCampo = 'Recebedor'
          UpdType = utNil
          LocF7SQLMasc = '$#'
        end
        object EdExpedidor: TdmkEditCB
          Left = 80
          Top = 24
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Expedidor'
          UpdCampo = 'Expedidor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdExpedidorRedefinido
          DBLookupComboBox = CBExpedidor
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBExpedidor: TdmkDBLookupComboBox
          Left = 132
          Top = 24
          Width = 345
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          ListSource = DsExpedidores
          TabOrder = 5
          dmkEditCB = EdExpedidor
          QryCampo = 'Expedidor'
          UpdType = utNil
          LocF7SQLMasc = '$#'
        end
        object EdToma4: TdmkEditCB
          Left = 588
          Top = 24
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Toma4'
          UpdCampo = 'Toma4'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdToma4Redefinido
          DBLookupComboBox = CBToma4
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBToma4: TdmkDBLookupComboBox
          Left = 640
          Top = 24
          Width = 325
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          ListSource = DsToma4s
          TabOrder = 7
          dmkEditCB = EdToma4
          QryCampo = 'Toma4'
          UpdType = utNil
          LocF7SQLMasc = '$#'
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 645
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 3
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 129
      Width = 1008
      Height = 84
      Align = alTop
      TabOrder = 1
      object Label13: TLabel
        Left = 8
        Top = 16
        Width = 75
        Height = 13
        Caption = 'Valor tot. carga:'
      end
      object Label22: TLabel
        Left = 92
        Top = 16
        Width = 80
        Height = 13
        Caption = 'V. tot.prest.serv.:'
      end
      object Label23: TLabel
        Left = 176
        Top = 16
        Width = 75
        Height = 13
        Caption = 'Valor a receber:'
      end
      object Label33: TLabel
        Left = 260
        Top = 16
        Width = 63
        Height = 13
        Caption = 'Valor original:'
        Enabled = False
      end
      object Label34: TLabel
        Left = 344
        Top = 16
        Width = 74
        Height = 13
        Caption = 'Valor desconto:'
      end
      object Label35: TLabel
        Left = 428
        Top = 16
        Width = 62
        Height = 13
        Caption = 'Valor l'#237'quido:'
        Enabled = False
      end
      object Label36: TLabel
        Left = 512
        Top = 16
        Width = 171
        Height = 13
        Caption = 'Data e hora prevista para a entrega:'
      end
      object Label37: TLabel
        Left = 696
        Top = 16
        Width = 88
        Height = 13
        Caption = 'CIOT (conta frete):'
      end
      object Label38: TLabel
        Left = 8
        Top = 60
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object CkRetira: TdmkCheckBox
        Left = 8
        Top = 0
        Width = 341
        Height = 17
        Caption = 
          'Recebedor retira no Aeroporto, Filial, Porto ou Esta'#231#227'o de Desti' +
          'no?'
        TabOrder = 0
        QryCampo = 'Retira'
        UpdCampo = 'Retira'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object EdvCarga: TdmkEdit
        Left = 8
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'vCarga'
        UpdCampo = 'vCarga'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdvTPrest: TdmkEdit
        Left = 92
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'vTPrest'
        UpdCampo = 'vTPrest'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdvTPrestRedefinido
      end
      object EdvRec: TdmkEdit
        Left = 176
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'vRec'
        UpdCampo = 'vRec'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdvRecRedefinido
      end
      object EdvOrig: TdmkEdit
        Left = 260
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'vOrig'
        UpdCampo = 'vOrig'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdvOrigRedefinido
      end
      object EdvDesc: TdmkEdit
        Left = 344
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'vDesc'
        UpdCampo = 'vDesc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdvDescRedefinido
      end
      object EdvLiq: TdmkEdit
        Left = 428
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'vLiq'
        UpdCampo = 'vLiq'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CkLota: TdmkCheckBox
        Left = 352
        Top = 0
        Width = 341
        Height = 17
        Caption = 'Indicador de Lota'#231#227'o: '#218'nico CT por ve'#237'culo e por viagem.'
        TabOrder = 1
        QryCampo = 'Lota'
        UpdCampo = 'Lota'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object TPDataPrev: TdmkEditDateTimePicker
        Left = 512
        Top = 32
        Width = 116
        Height = 21
        Date = 40137.793551006940000000
        Time = 40137.793551006940000000
        TabOrder = 8
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataPrev'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDataPrev: TdmkEdit
        Left = 632
        Top = 32
        Width = 60
        Height = 21
        TabOrder = 9
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DataPrev'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCIOT: TdmkEdit
        Left = 696
        Top = 32
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInt64
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CIOT'
        UpdCampo = 'CIOT'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 68
        Top = 56
        Width = 925
        Height = 21
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 587
      Width = 1008
      Height = 58
      Align = alBottom
      TabOrder = 2
      object BtTabelaPrc: TSpeedButton
        Left = 604
        Top = 8
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = BtTabelaPrcClick
      end
      object LaTabelaPrc: TLabel
        Left = 8
        Top = 13
        Width = 81
        Height = 13
        Caption = 'Tabela de pre'#231'o:'
      end
      object BtCondicaoPG: TSpeedButton
        Left = 504
        Top = 32
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = BtCondicaoPGClick
      end
      object Label24: TLabel
        Left = 530
        Top = 37
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object LaCondicaoPG: TLabel
        Left = 8
        Top = 37
        Width = 119
        Height = 13
        Caption = 'Condi'#231#227'o de pagamento:'
      end
      object SpeedButton13: TSpeedButton
        Left = 968
        Top = 32
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton13Click
      end
      object Label27: TLabel
        Left = 628
        Top = 9
        Width = 138
        Height = 13
        Caption = 'Funcion'#225'rio Emissor da CT-e:'
      end
      object CBTabelaPrc: TdmkDBLookupComboBox
        Left = 188
        Top = 8
        Width = 417
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFrtPrcCad
        TabOrder = 1
        dmkEditCB = EdTabelaPrc
        QryCampo = 'TabelaPrc'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdTabelaPrc: TdmkEditCB
        Left = 128
        Top = 8
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TabelaPrc'
        UpdCampo = 'TabelaPrc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTabelaPrc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCartEmiss: TdmkDBLookupComboBox
        Left = 624
        Top = 32
        Width = 343
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCartEmiss
        TabOrder = 5
        dmkEditCB = EdCartEmiss
        QryCampo = 'CartEmiss'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdCartEmiss: TdmkEditCB
        Left = 578
        Top = 32
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CartEmiss'
        UpdCampo = 'CartEmiss'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCartEmiss
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCondicaoPG: TdmkDBLookupComboBox
        Left = 184
        Top = 32
        Width = 320
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsPediPrzCab
        TabOrder = 3
        dmkEditCB = EdCondicaoPG
        QryCampo = 'CondicaoPg'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdCondicaoPG: TdmkEditCB
        Left = 128
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CondicaoPg'
        UpdCampo = 'CondicaoPg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCondicaoPG
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdEntiEmi: TdmkEditCB
        Left = 772
        Top = 5
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EntiEmi'
        UpdCampo = 'EntiEmi'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEntiEmi
        IgnoraDBLookupComboBox = True
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEntiEmi: TdmkDBLookupComboBox
        Left = 817
        Top = 5
        Width = 172
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsEntiEmi
        TabOrder = 7
        dmkEditCB = EdEntiEmi
        QryCampo = 'EntiEmi'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 227
      Width = 1008
      Height = 360
      ActivePage = TabSheet2
      Align = alBottom
      TabOrder = 4
      object TabSheet1: TTabSheet
        Caption = 'Fiscal'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 1000
          Height = 465
          Align = alTop
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object Label40: TLabel
            Left = 16
            Top = 16
            Width = 109
            Height = 13
            Caption = 'Natureza da opera'#231#227'o:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label41: TLabel
            Left = 496
            Top = 16
            Width = 31
            Height = 13
            Caption = 'CFOP:'
          end
          object SbCFOP: TSpeedButton
            Left = 935
            Top = 32
            Width = 21
            Height = 21
            Caption = '...'
          end
          object EdNatOp: TdmkEdit
            Left = 16
            Top = 32
            Width = 473
            Height = 21
            MaxLength = 60
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'NatOp'
            UpdCampo = 'NatOp'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCFOP: TdmkEditCB
            Left = 496
            Top = 32
            Width = 56
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CFOP'
            UpdCampo = 'CFOP'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '0'
            ValWarn = False
            DBLookupComboBox = CBCFOP
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCFOP: TdmkDBLookupComboBox
            Left = 555
            Top = 32
            Width = 382
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCFOP
            TabOrder = 2
            dmkEditCB = EdCFOP
            QryCampo = 'CFOP'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object RGTpCte: TdmkRadioGroup
            Left = 16
            Top = 56
            Width = 977
            Height = 41
            Caption = 'Tipo de CT-e:'
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'CT-e Normal'
              'CT-e de Complemento de Valores'
              'CT-e de Anula'#231#227'o'
              'CT-e Substituto')
            TabOrder = 3
            QryCampo = 'TpCTe'
            UpdCampo = 'TpCTe'
            UpdType = utYes
            OldValor = 0
          end
          object RGMyCST: TdmkRadioGroup
            Left = 16
            Top = 100
            Width = 977
            Height = 89
            Caption = 'CST:'
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o definido'
              '00-Tributa'#231#227'o Normal do ICMS'
              '20-tributa'#231#227'o com BC reduzida do ICMS'
              '40-ICMS isen'#231#227'o'
              '41-ICMS n'#227'o tributada'
              '51-ICMS diferido'
              '60-ICMS cobrado anteriormente por substitui'#231#227'o tribut'#225'ria'
              '90-ICMS outros - devido '#224' UF de origem da presta'#231#227'o'
              '90-ICMS outros'
              'Simples Nacional')
            TabOrder = 4
            QryCampo = 'MyCST'
            UpdCampo = 'MyCST'
            UpdType = utYes
            OldValor = 0
          end
          object GroupBox6: TGroupBox
            Left = 16
            Top = 196
            Width = 753
            Height = 57
            Caption = ' Percentuais de ICMS: '
            TabOrder = 5
            object Panel10: TPanel
              Left = 2
              Top = 15
              Width = 749
              Height = 40
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label42: TLabel
                Left = 8
                Top = 0
                Width = 40
                Height = 13
                Caption = '% ICMS:'
              end
              object Label43: TLabel
                Left = 112
                Top = 1
                Width = 75
                Height = 13
                Caption = '% Redu'#231#227'o BC:'
              end
              object Label44: TLabel
                Left = 216
                Top = 1
                Width = 91
                Height = 13
                Caption = '% ICMS ST Retido:'
              end
              object Label45: TLabel
                Left = 320
                Top = 1
                Width = 98
                Height = 13
                Caption = '% Red. BC outra UF:'
              end
              object Label46: TLabel
                Left = 424
                Top = 1
                Width = 84
                Height = 13
                Caption = '% ICMS outra UF:'
              end
              object EdICMS_pICMS: TdmkEdit
                Left = 8
                Top = 16
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'ICMS_pICMS'
                UpdCampo = 'ICMS_pICMS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdICMS_pRedBC: TdmkEdit
                Left = 112
                Top = 16
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'ICMS_pRedBC'
                UpdCampo = 'ICMS_pRedBC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdICMS_pICMSSTRet: TdmkEdit
                Left = 216
                Top = 16
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'ICMS_pICMSSTRet'
                UpdCampo = 'ICMS_pICMSSTRet'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdICMS_pRedBCOutraUF: TdmkEdit
                Left = 320
                Top = 16
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'ICMS_pRedBCOutraUF'
                UpdCampo = 'ICMS_pRedBCOutraUF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdICMS_pICMSOutraUF: TdmkEdit
                Left = 424
                Top = 16
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'ICMS_pICMSOutraUF'
                UpdCampo = 'ICMS_pICMSOutraUF'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
          object GroupBox7: TGroupBox
            Left = 16
            Top = 256
            Width = 753
            Height = 65
            Caption = 
              ' Valor de tributos federais, estaduais e municipais (Lei da tran' +
              'spar'#234'ncia - Lei n'#186' 12.741/2012): '
            TabOrder = 6
            object Panel11: TPanel
              Left = 2
              Top = 15
              Width = 749
              Height = 48
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object RGiTotTrib_Fonte: TdmkRadioGroup
                Left = 0
                Top = 0
                Width = 180
                Height = 48
                Align = alLeft
                Caption = ' Fonte do c'#225'lculo: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o informar'
                  'IBPTax')
                TabOrder = 0
                OnClick = RGiTotTrib_FonteClick
                QryCampo = 'iTotTrib_Fonte'
                UpdCampo = 'iTotTrib_Fonte'
                UpdType = utYes
                OldValor = 0
              end
              object PnIBPTax: TPanel
                Left = 180
                Top = 0
                Width = 569
                Height = 48
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                Visible = False
                object Label47: TLabel
                  Left = 412
                  Top = 5
                  Width = 17
                  Height = 13
                  Caption = 'EX:'
                end
                object Label48: TLabel
                  Left = 440
                  Top = 5
                  Width = 36
                  Height = 13
                  Caption = 'C'#243'digo:'
                end
                object RGiTotTrib_Tabela: TdmkRadioGroup
                  Left = 0
                  Top = 0
                  Width = 272
                  Height = 48
                  Align = alLeft
                  Caption = 'Tabela IBPTax:'
                  Columns = 4
                  ItemIndex = 3
                  Items.Strings = (
                    'NCM'
                    'NBS'
                    'LC116'
                    '?????')
                  TabOrder = 0
                  QryCampo = 'iTotTrib_Tabela'
                  UpdCampo = 'iTotTrib_Tabela'
                  UpdType = utYes
                  OldValor = 0
                end
                object EdiTotTrib_Ex: TdmkEdit
                  Left = 412
                  Top = 20
                  Width = 25
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'iTotTrib_Ex'
                  UpdCampo = 'iTotTrib_Ex'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object BtiTotTrib: TBitBtn
                  Tag = 20
                  Left = 280
                  Top = 5
                  Width = 120
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Pesquisar'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  NumGlyphs = 2
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                  OnClick = BtiTotTribClick
                end
                object EdiTotTrib_Codigo: TdmkEdit
                  Left = 440
                  Top = 20
                  Width = 116
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfInt64
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'iTotTrib_Codigo'
                  UpdCampo = 'iTotTrib_Codigo'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Comercial'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GBFrtRegFatC: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 332
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Label14: TLabel
            Left = 16
            Top = 172
            Width = 233
            Height = 13
            Caption = 'Caracter'#237'stica adicional do transporte (Opcional) :'
          end
          object Label15: TLabel
            Left = 256
            Top = 172
            Width = 217
            Height = 13
            Caption = 'Caracter'#237'stica adicional do servi'#231'o (Opcional):'
          end
          object Label16: TLabel
            Left = 240
            Top = 232
            Width = 150
            Height = 13
            Caption = 'Observa'#231#245'es Gerais (Opcional):'
          end
          object SbCaracAd: TSpeedButton
            Left = 232
            Top = 188
            Width = 23
            Height = 22
            OnClick = SbCaracAdClick
          end
          object SbCaracSer: TSpeedButton
            Left = 472
            Top = 188
            Width = 23
            Height = 22
            OnClick = SbCaracSerClick
          end
          object Label12: TLabel
            Left = 16
            Top = 9
            Width = 106
            Height = 13
            Caption = 'Regra comercial base:'
          end
          object SbFrtRegFatC: TSpeedButton
            Left = 676
            Top = 4
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbFrtRegFatCClick
          end
          object SpeedButton6: TSpeedButton
            Left = 700
            Top = 4
            Width = 21
            Height = 21
            Caption = '>'
            OnClick = SpeedButton6Click
          end
          object Label25: TLabel
            Left = 496
            Top = 172
            Width = 88
            Height = 13
            Caption = 'CT-e referenciada:'
          end
          object Label26: TLabel
            Left = 16
            Top = 232
            Width = 98
            Height = 13
            Caption = 'Detalhes da retirada:'
          end
          object Label28: TLabel
            Left = 772
            Top = 172
            Width = 190
            Height = 13
            Caption = 'Entrega (s'#243' se diferente do destinat'#225'rio):'
          end
          object Label29: TLabel
            Left = 772
            Top = 124
            Width = 76
            Height = 13
            Caption = 'Local de coleta:'
          end
          object Label30: TLabel
            Left = 16
            Top = 212
            Width = 170
            Height = 13
            Caption = 'Produto predominante (Obrigat'#243'rio) :'
          end
          object SbFrtProPred: TSpeedButton
            Left = 472
            Top = 212
            Width = 23
            Height = 22
            OnClick = SbFrtProPredClick
          end
          object Label31: TLabel
            Left = 500
            Top = 216
            Width = 300
            Height = 13
            Caption = 
              'Outras caracter'#237'sticas da carga (opcional) (ex.: fria, granel, e' +
              'tc):'
          end
          object Label32: TLabel
            Left = 728
            Top = 9
            Width = 65
            Height = 13
            Caption = 'S'#233'rie do CTe:'
          end
          object RGForPag: TdmkRadioGroup
            Left = 16
            Top = 28
            Width = 129
            Height = 73
            Caption = 'Forma de pag. do serv.:'
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              'Pago'
              'A pagar'
              'Outros')
            TabOrder = 4
            QryCampo = 'ForPag'
            UpdCampo = 'ForPag'
            UpdType = utYes
            OldValor = 0
          end
          object RGModal: TdmkRadioGroup
            Left = 148
            Top = 28
            Width = 285
            Height = 73
            Caption = ' Modal: '
            Columns = 3
            ItemIndex = 1
            Items.Strings = (
              '00-Indefinido'
              '01-Rodovi'#225'rio'
              '02-A'#233'reo'
              '03-Aquavi'#225'rio'
              '04-Ferrovi'#225'rio'
              '05-Dutovi'#225'rio'
              '06-Multimodal')
            TabOrder = 5
            QryCampo = 'Modal'
            UpdCampo = 'Modal'
            UpdType = utYes
            OldValor = 0
          end
          object RGTpServ: TdmkRadioGroup
            Left = 436
            Top = 28
            Width = 369
            Height = 73
            Caption = 'Tipo de Servi'#231'o (OPCIONAL): '
            Columns = 2
            Items.Strings = (
              '0-Normal'
              '1-Subcontrata'#231#227'o'
              '2-Redespacho'
              '3-Redespacho intermedi'#225'rio'
              '4-Servi'#231'o Vinculado a Multimodal')
            TabOrder = 6
            QryCampo = 'TpServ'
            UpdCampo = 'TpServ'
            UpdType = utYes
            OldValor = 0
          end
          object GroupBox2: TGroupBox
            Left = 268
            Top = 104
            Width = 248
            Height = 65
            Caption = 'Munic'#237'pio de in'#237'cio da presta'#231#227'o: '
            TabOrder = 9
            object Label17: TLabel
              Left = 9
              Top = 20
              Width = 17
              Height = 13
              Caption = 'UF:'
            end
            object Label105: TLabel
              Left = 40
              Top = 20
              Width = 36
              Height = 13
              Caption = 'Cidade:'
            end
            object CBCMunIni: TdmkDBLookupComboBox
              Left = 97
              Top = 36
              Width = 144
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsMunIni
              TabOrder = 2
              dmkEditCB = EdCMunIni
              QryCampo = 'CMunIni'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdUFIni: TdmkEdit
              Left = 9
              Top = 36
              Width = 28
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtUF
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'UFIni'
              UpdCampo = 'UFIni'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCMunIni: TdmkEditCB
              Left = 40
              Top = 36
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'CMunIni'
              UpdCampo = 'CMunIni'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCMunIni
              IgnoraDBLookupComboBox = True
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
          end
          object GroupBox4: TGroupBox
            Left = 516
            Top = 104
            Width = 248
            Height = 65
            Caption = 'Munic'#237'pio final da presta'#231#227'o: '
            TabOrder = 10
            object Label18: TLabel
              Left = 9
              Top = 20
              Width = 17
              Height = 13
              Caption = 'UF:'
            end
            object Label19: TLabel
              Left = 40
              Top = 20
              Width = 36
              Height = 13
              Caption = 'Cidade:'
            end
            object CBCMunFim: TdmkDBLookupComboBox
              Left = 97
              Top = 36
              Width = 144
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsMunFim
              TabOrder = 2
              dmkEditCB = EdCMunFim
              QryCampo = 'CMunFim'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdUFFim: TdmkEdit
              Left = 9
              Top = 36
              Width = 28
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtUF
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'UFFim'
              UpdCampo = 'UFFim'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCMunFim: TdmkEditCB
              Left = 40
              Top = 36
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'CMunFim'
              UpdCampo = 'CMunFim'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCMunFim
              IgnoraDBLookupComboBox = True
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
          end
          object RGToma: TdmkRadioGroup
            Left = 812
            Top = 28
            Width = 177
            Height = 73
            Caption = ' Tomador do servi'#231'o: '
            Columns = 2
            Items.Strings = (
              '0-Remetente'
              '1-Expedidor'
              '2-Recebedor'
              '3-Destinat'#225'rio'
              '4-Outros')
            TabOrder = 7
            OnClick = RGTomaClick
            QryCampo = 'Toma'
            UpdCampo = 'Toma'
            UpdType = utYes
            OldValor = 0
          end
          object EdCaracAd: TdmkEditCB
            Left = 16
            Top = 188
            Width = 44
            Height = 21
            Alignment = taRightJustify
            TabOrder = 13
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CaracAd'
            UpdCampo = 'CaracAd'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCaracAd
            IgnoraDBLookupComboBox = True
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCaracAd: TdmkDBLookupComboBox
            Left = 61
            Top = 188
            Width = 170
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCaracAd
            TabOrder = 14
            dmkEditCB = EdCaracAd
            QryCampo = 'CaracAd'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCaracSer: TdmkEditCB
            Left = 256
            Top = 188
            Width = 44
            Height = 21
            Alignment = taRightJustify
            TabOrder = 15
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CaracSer'
            UpdCampo = 'CaracSer'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCaracSer
            IgnoraDBLookupComboBox = True
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCaracSer: TdmkDBLookupComboBox
            Left = 301
            Top = 188
            Width = 170
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCaracSer
            TabOrder = 16
            dmkEditCB = EdCaracSer
            QryCampo = 'CaracSer'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object MexObs: TdmkMemo
            Left = 240
            Top = 248
            Width = 749
            Height = 89
            MaxLength = 2000
            TabOrder = 24
            OnKeyDown = MexObsKeyDown
            QryCampo = 'xObs'
            UpdCampo = 'xObs'
            UpdType = utYes
          end
          object GroupBox5: TGroupBox
            Left = 16
            Top = 104
            Width = 252
            Height = 65
            Caption = 'Munic'#237'pio de envio do CT-e (transmiss'#227'o do doc.): '
            TabOrder = 8
            object Label20: TLabel
              Left = 9
              Top = 20
              Width = 17
              Height = 13
              Caption = 'UF:'
            end
            object Label21: TLabel
              Left = 40
              Top = 20
              Width = 36
              Height = 13
              Caption = 'Cidade:'
            end
            object CBCMunEnv: TdmkDBLookupComboBox
              Left = 97
              Top = 36
              Width = 148
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsMunEnv
              TabOrder = 2
              dmkEditCB = EdCMunEnv
              QryCampo = 'CMunEnv'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdUFEnv: TdmkEdit
              Left = 9
              Top = 36
              Width = 28
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtUF
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'UFEnv'
              UpdCampo = 'UFEnv'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCMunEnv: TdmkEditCB
              Left = 40
              Top = 36
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'CMunEnv'
              UpdCampo = 'CMunEnv'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCMunEnv
              IgnoraDBLookupComboBox = True
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
          end
          object EdFrtRegFatC: TdmkEditCB
            Left = 136
            Top = 4
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FrtRegFatC'
            UpdCampo = 'FrtRegFatC'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdFrtRegFatCRedefinido
            DBLookupComboBox = CBFrtRegFatC
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFrtRegFatC: TdmkDBLookupComboBox
            Left = 196
            Top = 4
            Width = 477
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsFrtRegFat
            TabOrder = 1
            dmkEditCB = EdFrtRegFatC
            QryCampo = 'FrtRegFatC'
            UpdType = utNil
            LocF7SQLMasc = '$#'
          end
          object EdrefCTE: TdmkEdit
            Left = 496
            Top = 188
            Width = 272
            Height = 21
            TabOrder = 17
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'refCTE'
            UpdCampo = 'refCTE'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnRedefinido = EdrefCTERedefinido
          end
          object MeXDetRetira: TdmkMemo
            Left = 16
            Top = 248
            Width = 221
            Height = 89
            MaxLength = 160
            TabOrder = 23
            OnKeyDown = MeXDetRetiraKeyDown
            QryCampo = 'XDetRetira'
            UpdCampo = 'XDetRetira'
            UpdType = utYes
          end
          object EdLocEnt: TdmkEditCB
            Left = 772
            Top = 188
            Width = 44
            Height = 21
            Alignment = taRightJustify
            TabOrder = 18
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'LocEnt'
            UpdCampo = 'LocEnt'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBLocEnt
            IgnoraDBLookupComboBox = True
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBLocEnt: TdmkDBLookupComboBox
            Left = 817
            Top = 188
            Width = 172
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENT'
            ListSource = DsLocEnt
            TabOrder = 19
            dmkEditCB = EdLocEnt
            QryCampo = 'LocEnt'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdLocColeta: TdmkEditCB
            Left = 772
            Top = 140
            Width = 44
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'LocColeta'
            UpdCampo = 'LocColeta'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBLocColeta
            IgnoraDBLookupComboBox = True
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBLocColeta: TdmkDBLookupComboBox
            Left = 817
            Top = 140
            Width = 172
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENT'
            ListSource = DsLocColeta
            TabOrder = 12
            dmkEditCB = EdLocColeta
            QryCampo = 'LocColeta'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdFrtProPred: TdmkEditCB
            Left = 212
            Top = 212
            Width = 44
            Height = 21
            Alignment = taRightJustify
            TabOrder = 20
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'FrtProPred'
            UpdCampo = 'FrtProPred'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBFrtProPred
            IgnoraDBLookupComboBox = True
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFrtProPred: TdmkDBLookupComboBox
            Left = 256
            Top = 212
            Width = 215
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsFrtProPred
            TabOrder = 21
            dmkEditCB = EdFrtProPred
            QryCampo = 'FrtProPred'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdxOutCat: TdmkEdit
            Left = 816
            Top = 212
            Width = 172
            Height = 21
            MaxLength = 30
            TabOrder = 22
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'xOutCat'
            UpdCampo = 'xOutCat'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdParamsCTe: TdmkEditCB
            Left = 804
            Top = 4
            Width = 28
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ParamsCTe'
            UpdCampo = 'ParamsCTe'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdFrtRegFatCRedefinido
            DBLookupComboBox = CBParamsCTe
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBParamsCTe: TdmkDBLookupComboBox
            Left = 832
            Top = 4
            Width = 157
            Height = 21
            KeyField = 'Controle'
            ListField = 'NOME'
            ListSource = DsParamsCTe
            TabOrder = 3
            dmkEditCB = EdParamsCTe
            QryCampo = 'ParamsCTe'
            UpdType = utNil
            LocF7SQLMasc = '$#'
          end
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 708
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsFrtFatCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBGrid8: TDBGrid
        Left = 140
        Top = 15
        Width = 866
        Height = 48
        Align = alRight
        DataSource = DsFrtFatComp
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 598
      Width = 1008
      Height = 110
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 93
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 276
        Top = 15
        Width = 730
        Height = 93
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 648
          Top = 0
          Width = 82
          Height = 93
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 72
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 72
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtInfQ: TBitBtn
          Tag = 110
          Left = 76
          Top = 4
          Width = 72
          Height = 40
          Cursor = crHandPoint
          Caption = '&Quantidades'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtInfQClick
        end
        object BtInfDoc: TBitBtn
          Tag = 110
          Left = 148
          Top = 4
          Width = 72
          Height = 40
          Cursor = crHandPoint
          Caption = '&Documentos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtInfDocClick
        end
        object BtEncerra: TBitBtn
          Left = 572
          Top = 44
          Width = 72
          Height = 40
          Cursor = crHandPoint
          Caption = '&Encerra'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtEncerraClick
        end
        object BtFrtFOCAtrDef: TBitBtn
          Tag = 110
          Left = 220
          Top = 4
          Width = 72
          Height = 40
          Cursor = crHandPoint
          Caption = '&Obs.Cont.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtFrtFOCAtrDefClick
        end
        object BtPeri: TBitBtn
          Tag = 110
          Left = 292
          Top = 4
          Width = 72
          Height = 40
          Cursor = crHandPoint
          Caption = '&Prod.Peri'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = BtPeriClick
        end
        object BtAutXML: TBitBtn
          Tag = 110
          Left = 364
          Top = 4
          Width = 72
          Height = 40
          Cursor = crHandPoint
          Caption = '&Aut.XML'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          OnClick = BtAutXMLClick
        end
        object BtSeg: TBitBtn
          Tag = 110
          Left = 4
          Top = 44
          Width = 72
          Height = 40
          Cursor = crHandPoint
          Caption = '&Seguros'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
          OnClick = BtSegClick
        end
        object BtComp: TBitBtn
          Tag = 110
          Left = 76
          Top = 44
          Width = 72
          Height = 40
          Cursor = crHandPoint
          Caption = '&Comp.Vlr'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 9
          OnClick = BtCompClick
        end
        object BtVPed: TBitBtn
          Tag = 110
          Left = 148
          Top = 44
          Width = 72
          Height = 40
          Cursor = crHandPoint
          Caption = 'Ped'#225'&gio'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 10
          OnClick = BtVPedClick
        end
        object BtVeic: TBitBtn
          Tag = 110
          Left = 220
          Top = 44
          Width = 72
          Height = 40
          Cursor = crHandPoint
          Caption = '&Ve'#237'culos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 11
          OnClick = BtVeicClick
        end
        object BtLacr: TBitBtn
          Tag = 110
          Left = 292
          Top = 44
          Width = 72
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lacre'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 12
          OnClick = BtLacrClick
        end
        object BtMoto: TBitBtn
          Tag = 110
          Left = 364
          Top = 44
          Width = 72
          Height = 40
          Cursor = crHandPoint
          Caption = '&Motorista'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 13
          OnClick = BtMotoClick
        end
      end
    end
    object DGInfQ: TDBGrid
      Left = 0
      Top = 129
      Width = 377
      Height = 104
      Align = alLeft
      DataSource = DsFrtFatInfQ
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CUnid'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CUnid'
          Title.Caption = 'Unidade de medida'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_MyTpMed'
          Title.Caption = 'Tipo de medida'
          Width = 81
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QCarga'
          Title.Caption = 'Qtd. carga'
          Visible = True
        end>
    end
    object DGInfNFe: TDBGrid
      Left = 0
      Top = 65
      Width = 1008
      Height = 64
      Align = alTop
      DataSource = DsFrtFatInfDoc
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InfNFe_chave'
          Title.Caption = 'Chave NFe'
          Width = 272
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InfNF_nDoc'
          Title.Caption = 'NF1/1A ou Produtor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InfOutros_nDoc'
          Title.Caption = 'Outro documento'
          Visible = True
        end>
    end
    object DBGrid1: TDBGrid
      Left = 604
      Top = 129
      Width = 404
      Height = 104
      Align = alRight
      DataSource = DsFrtFOCAtrDef
      TabOrder = 4
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CU_CAD'
          Title.Caption = 'ID campo'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CAD'
          Title.Caption = 'Campo'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CU_ITS'
          Title.Caption = 'ID valor'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_ITS'
          Title.Caption = 'Valor'
          Width = 200
          Visible = True
        end>
    end
    object DBGrid2: TDBGrid
      Left = 0
      Top = 535
      Width = 1008
      Height = 63
      Align = alBottom
      DataSource = DsFrtFatPeri
      TabOrder = 5
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object DBGrid3: TDBGrid
      Left = 0
      Top = 475
      Width = 1008
      Height = 60
      Align = alBottom
      DataSource = DsFrtFatSeg
      TabOrder = 6
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object DBGrid4: TDBGrid
      Left = 0
      Top = 233
      Width = 1008
      Height = 58
      Align = alBottom
      DataSource = DsFrtFatMoRodoVPed
      TabOrder = 7
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object DBGrid5: TDBGrid
      Left = 0
      Top = 291
      Width = 1008
      Height = 64
      Align = alBottom
      DataSource = DsFrtFatMoRodoVeic
      TabOrder = 8
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object DBGrid6: TDBGrid
      Left = 0
      Top = 355
      Width = 1008
      Height = 60
      Align = alBottom
      DataSource = DsFrtFatMoRodoMoto
      TabOrder = 9
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object DBGrid7: TDBGrid
      Left = 0
      Top = 415
      Width = 1008
      Height = 60
      Align = alBottom
      DataSource = DsFrtFatMoRodoLacr
      TabOrder = 10
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 264
        Height = 32
        Caption = 'Faturamento de Frete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 264
        Height = 32
        Caption = 'Faturamento de Frete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 264
        Height = 32
        Caption = 'Faturamento de Frete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 40
    Top = 65520
  end
  object QrFrtFatCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFrtFatCabBeforeOpen
    AfterOpen = QrFrtFatCabAfterOpen
    BeforeClose = QrFrtFatCabBeforeClose
    AfterScroll = QrFrtFatCabAfterScroll
    SQL.Strings = (
      'SELECT rfc.Nome, rfc.NatOp, rfc.CFOP, rfc.TpCTe, rfc.MyCST, '
      'rfc.ICMS_pICMS, rfc.ICMS_pRedBC, rfc.ICMS_pICMSSTRet, '
      'rfc.ICMS_pRedBCOutraUF, rfc.ICMS_pICMSOutraUF, '
      'rfc.iTotTrib_Fonte, rfc.iTotTrib_Tabela, rfc.iTotTrib_Ex,'
      'rfc.iTotTrib_Codigo, ffc.* '
      'FROM frtfatcab ffc'
      'LEFT JOIN frtregfisc rfc ON rfc.Codigo=ffc.FrtRegFisC'
      'WHERE ffc.Codigo > 0')
    Left = 44
    Top = 33
    object QrFrtFatCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatCabFrtRegFisC: TIntegerField
      FieldName = 'FrtRegFisC'
    end
    object QrFrtFatCabTomador: TIntegerField
      FieldName = 'Tomador'
    end
    object QrFrtFatCabRemetente: TIntegerField
      FieldName = 'Remetente'
    end
    object QrFrtFatCabExpedidor: TIntegerField
      FieldName = 'Expedidor'
    end
    object QrFrtFatCabRecebedor: TIntegerField
      FieldName = 'Recebedor'
    end
    object QrFrtFatCabDestinatario: TIntegerField
      FieldName = 'Destinatario'
    end
    object QrFrtFatCabToma4: TIntegerField
      FieldName = 'Toma4'
    end
    object QrFrtFatCabTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrFrtFatCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrFrtFatCabCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrFrtFatCabFrtRegFatC: TIntegerField
      FieldName = 'FrtRegFatC'
    end
    object QrFrtFatCabForPag: TSmallintField
      FieldName = 'ForPag'
    end
    object QrFrtFatCabModal: TSmallintField
      FieldName = 'Modal'
    end
    object QrFrtFatCabTpServ: TSmallintField
      FieldName = 'TpServ'
    end
    object QrFrtFatCabCMunEnv: TIntegerField
      FieldName = 'CMunEnv'
    end
    object QrFrtFatCabUFEnv: TWideStringField
      FieldName = 'UFEnv'
      Size = 2
    end
    object QrFrtFatCabCMunIni: TIntegerField
      FieldName = 'CMunIni'
    end
    object QrFrtFatCabUFIni: TWideStringField
      FieldName = 'UFIni'
      Size = 2
    end
    object QrFrtFatCabCMunFim: TIntegerField
      FieldName = 'CMunFim'
    end
    object QrFrtFatCabUFFim: TWideStringField
      FieldName = 'UFFim'
      Size = 2
    end
    object QrFrtFatCabToma: TSmallintField
      FieldName = 'Toma'
    end
    object QrFrtFatCabCaracAd: TIntegerField
      FieldName = 'CaracAd'
    end
    object QrFrtFatCabCaracSer: TIntegerField
      FieldName = 'CaracSer'
    end
    object QrFrtFatCabxObs: TWideMemoField
      FieldName = 'xObs'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFrtFatCabRetira: TSmallintField
      FieldName = 'Retira'
    end
    object QrFrtFatCabvCarga: TFloatField
      FieldName = 'vCarga'
    end
    object QrFrtFatCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFrtFatCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrFrtFatCabAbertura: TDateTimeField
      FieldName = 'Abertura'
    end
    object QrFrtFatCabEncerrou: TDateTimeField
      FieldName = 'Encerrou'
    end
    object QrFrtFatCabvTPrest: TFloatField
      FieldName = 'vTPrest'
    end
    object QrFrtFatCabvRec: TFloatField
      FieldName = 'vRec'
    end
    object QrFrtFatCabrefCTE: TWideStringField
      FieldName = 'refCTE'
      Size = 44
    end
    object QrFrtFatCabXDetRetira: TWideStringField
      FieldName = 'XDetRetira'
      Size = 160
    end
    object QrFrtFatCabEntiEmi: TIntegerField
      FieldName = 'EntiEmi'
    end
    object QrFrtFatCabLocEnt: TIntegerField
      FieldName = 'LocEnt'
    end
    object QrFrtFatCabLocColeta: TIntegerField
      FieldName = 'LocColeta'
    end
    object QrFrtFatCabFrtProPred: TIntegerField
      FieldName = 'FrtProPred'
    end
    object QrFrtFatCabxOutCat: TWideStringField
      FieldName = 'xOutCat'
      Size = 30
    end
    object QrFrtFatCabDataFat: TDateTimeField
      FieldName = 'DataFat'
    end
    object QrFrtFatCabStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrFrtFatCabSerieDesfe: TIntegerField
      FieldName = 'SerieDesfe'
    end
    object QrFrtFatCabCTDesfeita: TIntegerField
      FieldName = 'CTDesfeita'
    end
    object QrFrtFatCabParamsCTe: TIntegerField
      FieldName = 'ParamsCTe'
    end
    object QrFrtFatCabvOrig: TFloatField
      FieldName = 'vOrig'
    end
    object QrFrtFatCabvDesc: TFloatField
      FieldName = 'vDesc'
    end
    object QrFrtFatCabvLiq: TFloatField
      FieldName = 'vLiq'
    end
    object QrFrtFatCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrFrtFatCabNatOp: TWideStringField
      FieldName = 'NatOp'
      Size = 60
    end
    object QrFrtFatCabCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 5
    end
    object QrFrtFatCabTpCTe: TSmallintField
      FieldName = 'TpCTe'
    end
    object QrFrtFatCabMyCST: TSmallintField
      FieldName = 'MyCST'
    end
    object QrFrtFatCabICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrFrtFatCabICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrFrtFatCabICMS_pICMSSTRet: TFloatField
      FieldName = 'ICMS_pICMSSTRet'
    end
    object QrFrtFatCabICMS_pRedBCOutraUF: TFloatField
      FieldName = 'ICMS_pRedBCOutraUF'
    end
    object QrFrtFatCabICMS_pICMSOutraUF: TFloatField
      FieldName = 'ICMS_pICMSOutraUF'
    end
    object QrFrtFatCabiTotTrib_Fonte: TIntegerField
      FieldName = 'iTotTrib_Fonte'
    end
    object QrFrtFatCabiTotTrib_Tabela: TIntegerField
      FieldName = 'iTotTrib_Tabela'
    end
    object QrFrtFatCabiTotTrib_Ex: TIntegerField
      FieldName = 'iTotTrib_Ex'
    end
    object QrFrtFatCabiTotTrib_Codigo: TLargeintField
      FieldName = 'iTotTrib_Codigo'
    end
    object QrFrtFatCabDataPrev: TDateTimeField
      FieldName = 'DataPrev'
    end
    object QrFrtFatCabLota: TSmallintField
      FieldName = 'Lota'
    end
    object QrFrtFatCabCIOT: TLargeintField
      FieldName = 'CIOT'
    end
  end
  object DsFrtFatCab: TDataSource
    DataSet = QrFrtFatCab
    Left = 40
    Top = 81
  end
  object QrFrtFatInfQ: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      
        ' ELT(fiq.CUnid + 1,"M3","KG","TON","UNIDADE","LITROS","MMBTU") N' +
        'O_CUnid,'
      'mtm.Nome NO_MyTpMed, '
      'fiq.* '
      'FROM frtfatinfq fiq'
      'LEFT JOIN trnsmytpmed mtm ON mtm.Codigo=fiq.MyTpMed '
      'WHERE fiq.Codigo=1')
    Left = 372
    Top = 49
    object QrFrtFatInfQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatInfQControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatInfQCUnid: TSmallintField
      FieldName = 'CUnid'
    end
    object QrFrtFatInfQMyTpMed: TIntegerField
      FieldName = 'MyTpMed'
    end
    object QrFrtFatInfQQCarga: TFloatField
      FieldName = 'QCarga'
    end
    object QrFrtFatInfQLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatInfQDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatInfQDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatInfQUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatInfQUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatInfQAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatInfQAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFrtFatInfQNO_CUnid: TWideStringField
      DisplayWidth = 15
      FieldName = 'NO_CUnid'
      Size = 15
    end
    object QrFrtFatInfQNO_MyTpMed: TWideStringField
      FieldName = 'NO_MyTpMed'
      Size = 60
    end
  end
  object DsFrtFatInfQ: TDataSource
    DataSet = QrFrtFatInfQ
    Left = 372
    Top = 93
  end
  object PMInfQ: TPopupMenu
    OnPopup = PMInfQPopup
    Left = 452
    Top = 560
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona informe de quantidade'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita  informe de quantidade'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove  informe de quantidade'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 316
    Top = 552
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CTeSubstitudo1: TMenuItem
      Caption = 'CT-e &Substitu'#237'do'
      OnClick = CTeSubstitudo1Click
    end
    object CTeComplementado1: TMenuItem
      Caption = 'CT-e &Complementado'
      OnClick = CTeComplementado1Click
    end
    object CTedeAnulacao1: TMenuItem
      Caption = 'CT-e de A&nula'#231#227'o'
      OnClick = CTedeAnulacao1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Duplicaconhecimento1: TMenuItem
      Caption = 'Duplica conhecimento'
      OnClick = Duplicaconhecimento1Click
    end
  end
  object QrFrtFatInfDoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatinfdoc'
      'WHERE Codigo =:P0')
    Left = 444
    Top = 49
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatInfDocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatInfDocControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatInfDocMyTpDocInf: TSmallintField
      FieldName = 'MyTpDocInf'
    end
    object QrFrtFatInfDocInfNFe_chave: TWideStringField
      FieldName = 'InfNFe_chave'
      Size = 44
    end
    object QrFrtFatInfDocInfNFe_PIN: TIntegerField
      FieldName = 'InfNFe_PIN'
    end
    object QrFrtFatInfDocInfNFe_dPrev: TDateField
      FieldName = 'InfNFe_dPrev'
    end
    object QrFrtFatInfDocInfNF_nRoma: TWideStringField
      FieldName = 'InfNF_nRoma'
    end
    object QrFrtFatInfDocInfNF_nPed: TWideStringField
      FieldName = 'InfNF_nPed'
    end
    object QrFrtFatInfDocInfNF_mod: TSmallintField
      FieldName = 'InfNF_mod'
    end
    object QrFrtFatInfDocInfNF_serie: TWideStringField
      FieldName = 'InfNF_serie'
      Size = 3
    end
    object QrFrtFatInfDocInfNF_nDoc: TWideStringField
      FieldName = 'InfNF_nDoc'
    end
    object QrFrtFatInfDocInfNF_dEmi: TDateField
      FieldName = 'InfNF_dEmi'
    end
    object QrFrtFatInfDocInfNF_vBC: TFloatField
      FieldName = 'InfNF_vBC'
    end
    object QrFrtFatInfDocInfNF_vICMS: TFloatField
      FieldName = 'InfNF_vICMS'
    end
    object QrFrtFatInfDocInfNF_vBCST: TFloatField
      FieldName = 'InfNF_vBCST'
    end
    object QrFrtFatInfDocInfNF_vST: TFloatField
      FieldName = 'InfNF_vST'
    end
    object QrFrtFatInfDocInfNF_vProd: TFloatField
      FieldName = 'InfNF_vProd'
    end
    object QrFrtFatInfDocInfNF_vNF: TFloatField
      FieldName = 'InfNF_vNF'
    end
    object QrFrtFatInfDocInfNF_nCFOP: TIntegerField
      FieldName = 'InfNF_nCFOP'
    end
    object QrFrtFatInfDocInfNF_nPeso: TFloatField
      FieldName = 'InfNF_nPeso'
    end
    object QrFrtFatInfDocInfNF_PIN: TIntegerField
      FieldName = 'InfNF_PIN'
    end
    object QrFrtFatInfDocInfNF_dPrev: TDateField
      FieldName = 'InfNF_dPrev'
    end
    object QrFrtFatInfDocInfOutros_tpDoc: TSmallintField
      FieldName = 'InfOutros_tpDoc'
    end
    object QrFrtFatInfDocInfOutros_descOutros: TWideStringField
      FieldName = 'InfOutros_descOutros'
      Size = 100
    end
    object QrFrtFatInfDocInfOutros_nDoc: TWideStringField
      FieldName = 'InfOutros_nDoc'
    end
    object QrFrtFatInfDocInfOutros_dEmi: TDateField
      FieldName = 'InfOutros_dEmi'
    end
    object QrFrtFatInfDocInfOutros_vDocFisc: TFloatField
      FieldName = 'InfOutros_vDocFisc'
    end
    object QrFrtFatInfDocInfOutros_dPrev: TDateField
      FieldName = 'InfOutros_dPrev'
    end
    object QrFrtFatInfDocLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatInfDocDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatInfDocDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatInfDocUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatInfDocUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatInfDocAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatInfDocAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtFatInfDoc: TDataSource
    DataSet = QrFrtFatInfDoc
    Left = 444
    Top = 93
  end
  object QrFrtRegFisC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtregfisc'
      'ORDER BY Nome'
      '')
    Left = 296
    Top = 141
    object QrFrtRegFisCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtRegFisCNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsFrtRegFisC: TDataSource
    DataSet = QrFrtRegFisC
    Left = 296
    Top = 185
  end
  object QrFrtRegFatC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtregfatc'
      'ORDER BY Nome')
    Left = 376
    Top = 141
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsFrtRegFat: TDataSource
    DataSet = QrFrtRegFatC
    Left = 372
    Top = 185
  end
  object QrTomadores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE, ent.indIEDest, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      ''
      'ufs.Nome NOMEUF'
      ''
      ''
      ''
      ''
      ''
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 500
    Top = 12
    object QrTomadoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTomadoresNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrTomadoresCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrTomadoresNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrTomadoresCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrTomadoresIE: TWideStringField
      FieldName = 'IE'
    end
    object QrTomadoresindIEDest: TIntegerField
      FieldName = 'indIEDest'
    end
    object QrTomadoresTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsTomadores: TDataSource
    DataSet = QrTomadores
    Left = 500
    Top = 60
  end
  object QrRemetentes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE, ent.indIEDest, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      ''
      'ufs.Nome NOMEUF'
      ''
      ''
      ''
      ''
      ''
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 580
    Top = 12
    object QrRemetentesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRemetentesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrRemetentesCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrRemetentesNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrRemetentesCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrRemetentesIE: TWideStringField
      FieldName = 'IE'
    end
    object QrRemetentesindIEDest: TIntegerField
      FieldName = 'indIEDest'
    end
    object QrRemetentesTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsRemetentes: TDataSource
    DataSet = QrRemetentes
    Left = 580
    Top = 60
  end
  object QrExpedidores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE, ent.indIEDest, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      ''
      'ufs.Nome NOMEUF'
      ''
      ''
      ''
      ''
      ''
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 660
    Top = 12
    object QrExpedidoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrExpedidoresNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrExpedidoresCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrExpedidoresNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrExpedidoresCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrExpedidoresIE: TWideStringField
      FieldName = 'IE'
    end
    object QrExpedidoresindIEDest: TIntegerField
      FieldName = 'indIEDest'
    end
    object QrExpedidoresTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsExpedidores: TDataSource
    DataSet = QrExpedidores
    Left = 660
    Top = 60
  end
  object QrRecebedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE, ent.indIEDest, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      ''
      'ufs.Nome NOMEUF'
      ''
      ''
      ''
      ''
      ''
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 744
    Top = 12
    object QrRecebedoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRecebedoresNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrRecebedoresCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrRecebedoresNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrRecebedoresCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrRecebedoresIE: TWideStringField
      FieldName = 'IE'
    end
    object QrRecebedoresindIEDest: TIntegerField
      FieldName = 'indIEDest'
    end
    object QrRecebedoresTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsRecebedores: TDataSource
    DataSet = QrRecebedores
    Left = 744
    Top = 60
  end
  object QrDestinatarios: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE, ent.indIEDest, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      ''
      'ufs.Nome NOMEUF'
      ''
      ''
      ''
      ''
      ''
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 828
    Top = 12
    object QrDestinatariosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDestinatariosNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrDestinatariosCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrDestinatariosNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrDestinatariosCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrDestinatariosIE: TWideStringField
      FieldName = 'IE'
    end
    object QrDestinatariosindIEDest: TIntegerField
      FieldName = 'indIEDest'
    end
    object QrDestinatariosTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsDestinatarios: TDataSource
    DataSet = QrDestinatarios
    Left = 828
    Top = 60
  end
  object QrToma4s: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE, ent.indIEDest, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      ''
      'ufs.Nome NOMEUF'
      ''
      ''
      ''
      ''
      ''
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 900
    Top = 12
    object QrToma4sCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrToma4sNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrToma4sCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrToma4sNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrToma4sCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrToma4sIE: TWideStringField
      FieldName = 'IE'
    end
    object QrToma4sindIEDest: TIntegerField
      FieldName = 'indIEDest'
    end
    object QrToma4sTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsToma4s: TDataSource
    DataSet = QrToma4s
    Left = 900
    Top = 60
  end
  object QrPediPrzCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, MaxDesco,'
      'JurosMes, Parcelas, Percent1, Percent2'
      'FROM pediprzcab'
      'WHERE Aplicacao & :P0 <> 0'
      'ORDER BY Nome')
    Left = 712
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 712
    Top = 180
  end
  object QrCartEmiss: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome'
      'FROM carteiras car'
      'WHERE Tipo=2'
      'ORDER BY Nome')
    Left = 788
    Top = 132
    object QrCartEmissCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCartEmissNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsCartEmiss: TDataSource
    DataSet = QrCartEmiss
    Left = 788
    Top = 180
  end
  object QrFrtPrcCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM frtprccad'
      'ORDER BY Nome')
    Left = 864
    Top = 136
    object QrFrtPrcCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtPrcCadNome: TWideStringField
      DisplayWidth = 100
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsFrtPrcCad: TDataSource
    DataSet = QrFrtPrcCad
    Left = 864
    Top = 184
  end
  object QrCaracAd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM frtcaracad'
      'ORDER BY Nome')
    Left = 108
    Top = 4
    object QrCaracAdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCaracAdNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCaracAd: TDataSource
    DataSet = QrCaracAd
    Left = 108
    Top = 48
  end
  object QrCaracSer: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM frtcaracser'
      'ORDER BY Nome')
    Left = 176
    Top = 4
    object QrCaracSerCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCaracSerNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCaracSer: TDataSource
    DataSet = QrCaracSer
    Left = 176
    Top = 48
  end
  object QrMunIni: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 776
    Top = 632
    object QrMunIniCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMunIniNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrMunFim: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 872
    Top = 584
    object QrMunFimCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMunFimNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrMunEnv: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 932
    Top = 584
    object QrMunEnvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMunEnvNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMunEnv: TDataSource
    DataSet = QrMunEnv
    Left = 932
    Top = 628
  end
  object DsMunFim: TDataSource
    DataSet = QrMunFim
    Left = 872
    Top = 628
  end
  object DsMunIni: TDataSource
    DataSet = QrMunIni
    Left = 816
    Top = 628
  end
  object QrFRFatC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM FrtRegFatC'
      'WHERE Codigo=1')
    Left = 156
    Top = 268
    object QrFRFatCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFRFatCNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrFRFatCForPag: TSmallintField
      FieldName = 'ForPag'
    end
    object QrFRFatCModal: TSmallintField
      FieldName = 'Modal'
    end
    object QrFRFatCTpServ: TSmallintField
      FieldName = 'TpServ'
    end
    object QrFRFatCCMunIni: TIntegerField
      FieldName = 'CMunIni'
    end
    object QrFRFatCUFIni: TWideStringField
      FieldName = 'UFIni'
      Size = 2
    end
    object QrFRFatCCMunFim: TIntegerField
      FieldName = 'CMunFim'
    end
    object QrFRFatCUFFim: TWideStringField
      FieldName = 'UFFim'
      Size = 2
    end
    object QrFRFatCToma: TSmallintField
      FieldName = 'Toma'
    end
    object QrFRFatCCaracAd: TIntegerField
      FieldName = 'CaracAd'
    end
    object QrFRFatCCaracSer: TIntegerField
      FieldName = 'CaracSer'
    end
    object QrFRFatCxObs: TWideMemoField
      FieldName = 'xObs'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFRFatCCMunEnv: TIntegerField
      FieldName = 'CMunEnv'
    end
    object QrFRFatCUFEnv: TWideStringField
      FieldName = 'UFEnv'
      Size = 2
    end
    object QrFRFatCTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrFRFatCCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrFRFatCCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
  end
  object PMInfDoc: TPopupMenu
    OnPopup = PMInfDocPopup
    Left = 572
    Top = 560
    object AdicionainformedeDocumento1: TMenuItem
      Caption = '&Adiciona informe de Documento'
      OnClick = AdicionainformedeDocumento1Click
    end
    object EditainformedeDocumento1: TMenuItem
      Caption = '&Edita informe de Documento'
      OnClick = EditainformedeDocumento1Click
    end
    object RemoveinformedeDocumento1: TMenuItem
      Caption = '&Remove informe de Documento'
      OnClick = RemoveinformedeDocumento1Click
    end
  end
  object PMEncerra: TPopupMenu
    OnPopup = PMEncerraPopup
    Left = 788
    Top = 560
    object Encerrafaturamento1: TMenuItem
      Caption = '&Encerra faturamento'
      OnClick = Encerrafaturamento1Click
    end
    object MenuItem1: TMenuItem
      Caption = '-'
    end
    object Desfazencerramento1: TMenuItem
      Caption = '&Desfaz encerramento'
      OnClick = Desfazencerramento1Click
    end
  end
  object QrEntiEmi: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE, ent.indIEDest, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      ''
      'ufs.Nome NOMEUF'
      ''
      ''
      ''
      ''
      ''
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 956
    Top = 12
    object QrEntiEmiCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiEmiNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrEntiEmiCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEntiEmiNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrEntiEmiCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrEntiEmiIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntiEmiindIEDest: TIntegerField
      FieldName = 'indIEDest'
    end
    object QrEntiEmiTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsEntiEmi: TDataSource
    DataSet = QrEntiEmi
    Left = 956
    Top = 60
  end
  object QrLocEnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE, ent.indIEDest, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      ''
      'ufs.Nome NOMEUF'
      ''
      ''
      ''
      ''
      ''
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 280
    Top = 232
    object QrLocEntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocEntNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrLocEntCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrLocEntNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrLocEntCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrLocEntIE: TWideStringField
      FieldName = 'IE'
    end
    object QrLocEntindIEDest: TIntegerField
      FieldName = 'indIEDest'
    end
    object QrLocEntTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsLocEnt: TDataSource
    DataSet = QrLocEnt
    Left = 236
    Top = 296
  end
  object DsLocColeta: TDataSource
    DataSet = QrLocColeta
    Left = 300
    Top = 296
  end
  object QrLocColeta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.IE, ent.indIEDest, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      ''
      'ufs.Nome NOMEUF'
      ''
      ''
      ''
      ''
      ''
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 300
    Top = 248
    object QrLocColetaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocColetaNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrLocColetaCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrLocColetaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrLocColetaCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrLocColetaIE: TWideStringField
      FieldName = 'IE'
    end
    object QrLocColetaindIEDest: TIntegerField
      FieldName = 'indIEDest'
    end
    object QrLocColetaTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrFrtProPred: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM frtpropred'
      'ORDER BY Nome')
    Left = 236
    Top = 4
    object QrFrtProPredCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtProPredNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsFrtProPred: TDataSource
    DataSet = QrFrtProPred
    Left = 236
    Top = 48
  end
  object QrParamsCTe: TmySQLQuery
    Database = Dmod.MyDB
    Left = 392
    Top = 296
    object QrParamsCTeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrParamsCTeNOME: TWideStringField
      FieldName = 'NOME'
    end
  end
  object DsParamsCTe: TDataSource
    DataSet = QrParamsCTe
    Left = 392
    Top = 340
  end
  object PMCTeIt2ObsCont: TPopupMenu
    OnPopup = PMCTeIt2ObsContPopup
    Left = 680
    Top = 556
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
    end
  end
  object QrCTeIt2ObsCont_0000: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cteit2obscont')
    Left = 700
    Top = 312
    object QrCTeIt2ObsCont_0000FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeIt2ObsCont_0000FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeIt2ObsCont_0000Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeIt2ObsCont_0000Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeIt2ObsCont_0000xCampo: TWideStringField
      FieldName = 'xCampo'
    end
    object QrCTeIt2ObsCont_0000xTexto: TWideStringField
      FieldName = 'xTexto'
      Size = 60
    end
    object QrCTeIt2ObsCont_0000Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeIt2ObsCont_0000DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeIt2ObsCont_0000DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeIt2ObsCont_0000UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeIt2ObsCont_0000UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeIt2ObsCont_0000AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeIt2ObsCont_0000Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsCTeIt2ObsCont_0000: TDataSource
    DataSet = QrCTeIt2ObsCont_0000
    Left = 704
    Top = 360
  end
  object PMFrtFocAtrDef: TPopupMenu
    Left = 740
    Top = 532
    object IncluiAtrAMovDef1: TMenuItem
      Caption = '&Inclui um novo atributo'
      OnClick = IncluiAtrAMovDef1Click
    end
    object AlteraAtrAMovDef1: TMenuItem
      Caption = '&Altera o atributo selecionado'
      OnClick = AlteraAtrAMovDef1Click
    end
    object ExcluiAtrAMovDef1: TMenuItem
      Caption = '&Exclui o atributo selecionado'
      OnClick = ExcluiAtrAMovDef1Click
    end
  end
  object QrFrtFOCAtrDef: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ''
      
        'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ' +
        'ATRITS, '
      'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,'
      'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp'
      'FROM frtfocatrdef def'
      'LEFT JOIN frtfocatrits its ON its.Controle=def.AtrIts '
      'LEFT JOIN frtfocatrcad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc=1'
      ' '
      'UNION  '
      ' '
      'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, '
      'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,'
      'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp '
      'FROM frtfocatrtxt def '
      'LEFT JOIN frtfocatrcad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc=1'
      ' '
      'ORDER BY NO_CAD, NO_ITS ')
    Left = 112
    Top = 412
    object QrFrtFOCAtrDefID_Item: TIntegerField
      FieldName = 'ID_Item'
      Required = True
    end
    object QrFrtFOCAtrDefID_Sorc: TIntegerField
      FieldName = 'ID_Sorc'
      Required = True
    end
    object QrFrtFOCAtrDefAtrCad: TIntegerField
      FieldName = 'AtrCad'
      Required = True
    end
    object QrFrtFOCAtrDefATRITS: TFloatField
      FieldName = 'ATRITS'
      Required = True
    end
    object QrFrtFOCAtrDefCU_CAD: TIntegerField
      FieldName = 'CU_CAD'
    end
    object QrFrtFOCAtrDefCU_ITS: TLargeintField
      FieldName = 'CU_ITS'
    end
    object QrFrtFOCAtrDefAtrTxt: TWideStringField
      FieldName = 'AtrTxt'
      Size = 255
    end
    object QrFrtFOCAtrDefNO_CAD: TWideStringField
      FieldName = 'NO_CAD'
      Size = 30
    end
    object QrFrtFOCAtrDefNO_ITS: TWideStringField
      FieldName = 'NO_ITS'
      Size = 255
    end
    object QrFrtFOCAtrDefAtrTyp: TSmallintField
      FieldName = 'AtrTyp'
    end
  end
  object DsFrtFOCAtrDef: TDataSource
    DataSet = QrFrtFOCAtrDef
    Left = 112
    Top = 460
  end
  object PMPeri: TPopupMenu
    OnPopup = PMPeriPopup
    Left = 680
    Top = 604
    object Incluiprodutoperigoso1: TMenuItem
      Caption = '&Inclui produto perigoso'
      OnClick = Incluiprodutoperigoso1Click
    end
    object Alteraprodutoperigoso1: TMenuItem
      Caption = '&Altera produto perigoso'
      OnClick = Alteraprodutoperigoso1Click
    end
    object Excluiprodutoperigoso1: TMenuItem
      Caption = '&Exclui produto perigoso'
      OnClick = Excluiprodutoperigoso1Click
    end
  end
  object QrFrtFatPeri: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ffp.Codigo, ffp.Controle, ffp.ProdutCad, '
      'ffp.qTotProd, ffp.qVolTipo,  '
      'prc.Nome, prc.nONU, prc.XNomeAE,'
      'prc.NClaRisco, prc.GrEmb PERI_grEmb, prc.PontoFulgor'
      'FROM frtfatperi ffp'
      'LEFT JOIN produtcad prc ON prc.Codigo=ffp.ProdutCad'
      'ORDER BY ffp.Controle')
    Left = 304
    Top = 4
    object QrFrtFatPeriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatPeriControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatPeriProdutCad: TIntegerField
      FieldName = 'ProdutCad'
    end
    object QrFrtFatPeriqTotProd: TWideStringField
      FieldName = 'qTotProd'
    end
    object QrFrtFatPeriqVolTipo: TWideStringField
      FieldName = 'qVolTipo'
      Size = 60
    end
    object QrFrtFatPeriNome: TWideStringField
      FieldName = 'Nome'
      Size = 150
    end
    object QrFrtFatPerinONU: TWideStringField
      FieldName = 'nONU'
      Size = 4
    end
    object QrFrtFatPeriXNomeAE: TWideStringField
      FieldName = 'XNomeAE'
      Size = 150
    end
    object QrFrtFatPeriXClaRisco: TWideStringField
      FieldName = 'XClaRisco'
      Size = 40
    end
    object QrFrtFatPeriPERI_grEmb: TWideStringField
      FieldName = 'PERI_grEmb'
      Size = 6
    end
    object QrFrtFatPeriPontoFulgor: TWideStringField
      FieldName = 'PontoFulgor'
      Size = 6
    end
  end
  object DsFrtFatPeri: TDataSource
    DataSet = QrFrtFatPeri
    Left = 304
    Top = 48
  end
  object QrFrtFatInfCTeSub: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatinfctesub'
      'WHERE Codigo=:P0')
    Left = 468
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatInfCTeSubCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatInfCTeSubChCTe: TWideStringField
      FieldName = 'ChCTe'
      Size = 44
    end
    object QrFrtFatInfCTeSubContribICMS: TSmallintField
      FieldName = 'ContribICMS'
    end
    object QrFrtFatInfCTeSubTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrFrtFatInfCTeSubrefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrFrtFatInfCTeSubCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrFrtFatInfCTeSubCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrFrtFatInfCTeSubmod_: TWideStringField
      FieldName = 'mod_'
      Size = 2
    end
    object QrFrtFatInfCTeSubserie: TIntegerField
      FieldName = 'serie'
    end
    object QrFrtFatInfCTeSubsubserie: TIntegerField
      FieldName = 'subserie'
    end
    object QrFrtFatInfCTeSubnro: TIntegerField
      FieldName = 'nro'
    end
    object QrFrtFatInfCTeSubvalor: TFloatField
      FieldName = 'valor'
    end
    object QrFrtFatInfCTeSubdEmi: TDateField
      FieldName = 'dEmi'
    end
    object QrFrtFatInfCTeSubrefCTe: TWideStringField
      FieldName = 'refCTe'
      Size = 44
    end
    object QrFrtFatInfCTeSubrefCTeAnu: TWideStringField
      FieldName = 'refCTeAnu'
      Size = 44
    end
    object QrFrtFatInfCTeSubLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatInfCTeSubDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatInfCTeSubDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatInfCTeSubUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatInfCTeSubUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatInfCTeSubAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatInfCTeSubAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtFatInfCTeSub: TDataSource
    DataSet = QrFrtFatInfCTeSub
    Left = 516
    Top = 332
  end
  object QrFrtFatInfCTeAnu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatinfctesub'
      'WHERE Codigo=:P0')
    Left = 572
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatInfCTeAnuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatInfCTeAnuChCTe: TWideStringField
      FieldName = 'ChCTe'
      Size = 44
    end
    object QrFrtFatInfCTeAnudEmi: TDateField
      FieldName = 'dEmi'
    end
  end
  object DsFrtFatInfCTeAnu: TDataSource
    DataSet = QrFrtFatInfCTeAnu
    Left = 572
    Top = 328
  end
  object QrFrtFatInfCTeComp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatinfctesub'
      'WHERE Codigo=:P0')
    Left = 796
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatInfCTeCompCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatInfCTeCompChave: TWideStringField
      FieldName = 'Chave'
      Size = 44
    end
  end
  object DsFrtFatInfCTeComp: TDataSource
    DataSet = QrFrtFatInfCTeComp
    Left = 796
    Top = 292
  end
  object QrFrtFatAutXml: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cga.*, '
      
        ' ELT(cga.AddForma + 1,"N'#227'o informado","Contatos","Entidade","Avu' +
        'lso") NO_AddForma,'
      'IF(cga.Tipo=0, "CNPJ", "CPF") TpDOC,'
      'CASE cga.AddForma '
      '  WHEN 0 THEN "(N/I)"  '
      '  WHEN 1 THEN eco.Nome '
      '  WHEN 2 THEN IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) '
      '  WHEN 3 THEN " "  '
      '  ELSE "???" END NO_ENT_ECO, '
      'IF(cga.Tipo=0, cga.CNPJ, cga.CPF) CNPJ_CPF '
      'FROM frtfatautxml cga '
      'LEFT JOIN entidades ent ON ent.Codigo=cga.AddIDCad'
      'LEFT JOIN enticontat eco ON eco.Controle=cga.AddIDCad'
      'WHERE cga.Codigo=7')
    Left = 900
    Top = 260
    object QrFrtFatAutXmlCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatAutXmlControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatAutXmlAddForma: TSmallintField
      FieldName = 'AddForma'
    end
    object QrFrtFatAutXmlAddIDCad: TIntegerField
      FieldName = 'AddIDCad'
    end
    object QrFrtFatAutXmlTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrFrtFatAutXmlCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrFrtFatAutXmlCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrFrtFatAutXmlLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatAutXmlDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatAutXmlDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatAutXmlUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatAutXmlUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatAutXmlAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatAutXmlAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFrtFatAutXmlNO_AddForma: TWideStringField
      FieldName = 'NO_AddForma'
      Size = 13
    end
    object QrFrtFatAutXmlTpDOC: TWideStringField
      FieldName = 'TpDOC'
      Required = True
      Size = 4
    end
    object QrFrtFatAutXmlNO_ENT_ECO: TWideStringField
      FieldName = 'NO_ENT_ECO'
      Size = 100
    end
    object QrFrtFatAutXmlCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 14
    end
  end
  object PMSeg: TPopupMenu
    OnPopup = PMSegPopup
    Left = 504
    Top = 608
    object Adicionainformedeseguro1: TMenuItem
      Caption = '&Adiciona informe de seguro'
      Enabled = False
      OnClick = Adicionainformedeseguro1Click
    end
    object Editainformedeseguro1: TMenuItem
      Caption = '&Edita  informe de seguro'
      Enabled = False
      OnClick = Editainformedeseguro1Click
    end
    object Removeinformedeseguro1: TMenuItem
      Caption = '&Remove  informe de seguro'
      Enabled = False
      OnClick = Removeinformedeseguro1Click
    end
  end
  object QrFrtFatSeg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'ffs.* '
      'FROM frtfatseg ffs'
      'WHERE ffs.Codigo=1')
    Left = 200
    Top = 413
    object QrFrtFatSegCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatSegControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatSegrespSeg: TSmallintField
      FieldName = 'respSeg'
    end
    object QrFrtFatSegxSeg: TWideStringField
      FieldName = 'xSeg'
      Size = 30
    end
    object QrFrtFatSegnApol: TWideStringField
      FieldName = 'nApol'
    end
    object QrFrtFatSegnAver: TWideStringField
      FieldName = 'nAver'
    end
    object QrFrtFatSegvCarga: TFloatField
      FieldName = 'vCarga'
    end
    object QrFrtFatSegLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatSegDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatSegDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatSegUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatSegUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatSegAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatSegAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtFatSeg: TDataSource
    DataSet = QrFrtFatSeg
    Left = 200
    Top = 457
  end
  object PMVPed: TPopupMenu
    OnPopup = PMVPedPopup
    Left = 40
    Top = 572
    object IncluiValePedgio1: TMenuItem
      Caption = '&Inclui Vale Ped'#225'gio'
      Enabled = False
      OnClick = IncluiValePedgio1Click
    end
    object AlteraValePedgio1: TMenuItem
      Caption = '&Altera Vale Ped'#225'gio'
      Enabled = False
      OnClick = AlteraValePedgio1Click
    end
    object ExcluiValePedgio1: TMenuItem
      Caption = '&Exclui Vale Ped'#225'gio'
      Enabled = False
      OnClick = ExcluiValePedgio1Click
    end
  end
  object QrFrtFatMoRodoVPed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatmorodovped'
      'WHERE Codigo=:P0')
    Left = 300
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatMoRodoVPedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatMoRodoVPedControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatMoRodoVPedCNPJForn: TWideStringField
      FieldName = 'CNPJForn'
      Size = 14
    end
    object QrFrtFatMoRodoVPednCompra: TWideStringField
      FieldName = 'nCompra'
    end
    object QrFrtFatMoRodoVPedCNPJPg: TWideStringField
      FieldName = 'CNPJPg'
      Size = 14
    end
    object QrFrtFatMoRodoVPedvValePed: TFloatField
      FieldName = 'vValePed'
    end
    object QrFrtFatMoRodoVPedLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatMoRodoVPedDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatMoRodoVPedDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatMoRodoVPedUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatMoRodoVPedUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatMoRodoVPedAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatMoRodoVPedAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtFatMoRodoVPed: TDataSource
    DataSet = QrFrtFatMoRodoVPed
    Left = 300
    Top = 460
  end
  object QrFrtFatMoRodoVeic: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatmorodoveic'
      'WHERE Codigo=:P0')
    Left = 412
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatMoRodoVeicCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatMoRodoVeicControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatMoRodoVeiccInt: TIntegerField
      FieldName = 'cInt'
    end
    object QrFrtFatMoRodoVeicLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatMoRodoVeicDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatMoRodoVeicDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatMoRodoVeicUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatMoRodoVeicUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatMoRodoVeicAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatMoRodoVeicAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtFatMoRodoVeic: TDataSource
    DataSet = QrFrtFatMoRodoVeic
    Left = 412
    Top = 460
  end
  object QrFrtFatMoRodoLacr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatmorodolacr'
      'WHERE Codigo=:P0')
    Left = 520
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatMoRodoLacrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatMoRodoLacrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatMoRodoLacrnLacre: TWideStringField
      FieldName = 'nLacre'
    end
    object QrFrtFatMoRodoLacrLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatMoRodoLacrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatMoRodoLacrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatMoRodoLacrUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatMoRodoLacrUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatMoRodoLacrAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatMoRodoLacrAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtFatMoRodoLacr: TDataSource
    DataSet = QrFrtFatMoRodoLacr
    Left = 520
    Top = 460
  end
  object QrFrtFatMoRodoMoto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatmorodomoto'
      'WHERE Codigo=:P0')
    Left = 632
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatMoRodoMotoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatMoRodoMotoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatMoRodoMotoxNome: TWideStringField
      FieldName = 'xNome'
    end
    object QrFrtFatMoRodoMotoCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrFrtFatMoRodoMotoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatMoRodoMotoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatMoRodoMotoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatMoRodoMotoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatMoRodoMotoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatMoRodoMotoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatMoRodoMotoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtFatMoRodoMoto: TDataSource
    DataSet = QrFrtFatMoRodoMoto
    Left = 632
    Top = 460
  end
  object PMVeic: TPopupMenu
    OnPopup = PMVeicPopup
    Left = 100
    Top = 572
    object IncluiVeculo1: TMenuItem
      Caption = '&Inclui Ve'#237'culo'
      Enabled = False
      OnClick = IncluiVeculo1Click
    end
    object AlteraVeculo1: TMenuItem
      Caption = '&Altera Ve'#237'culo'
      Enabled = False
      OnClick = AlteraVeculo1Click
    end
    object ExcluiVeculo1: TMenuItem
      Caption = '&Exclui Ve'#237'culo'
      Enabled = False
      OnClick = ExcluiVeculo1Click
    end
  end
  object PMLacr: TPopupMenu
    OnPopup = PMLacrPopup
    Left = 164
    Top = 576
    object IncluiLacre1: TMenuItem
      Caption = '&Inclui Lacre'
      Enabled = False
      OnClick = IncluiLacre1Click
    end
    object AlteraLacre1: TMenuItem
      Caption = '&Altera Lacre'
      Enabled = False
      OnClick = AlteraLacre1Click
    end
    object ExcluiLacre1: TMenuItem
      Caption = '&Exclui Lacre'
      Enabled = False
      OnClick = ExcluiLacre1Click
    end
  end
  object PMMoto: TPopupMenu
    OnPopup = PMMotoPopup
    Left = 232
    Top = 576
    object IncluiMotorista1: TMenuItem
      Caption = '&Inclui Motorista'
      Enabled = False
      OnClick = IncluiMotorista1Click
    end
    object AlteraMotorista1: TMenuItem
      Caption = '&Altera Motorista'
      Enabled = False
      OnClick = AlteraMotorista1Click
    end
    object ExcluiMotorista1: TMenuItem
      Caption = '&Exclui Motorista'
      Enabled = False
      OnClick = ExcluiMotorista1Click
    end
  end
  object PMComp: TPopupMenu
    OnPopup = PMCompPopup
    Left = 384
    Top = 552
    object Incluicomponentedovalordofrete1: TMenuItem
      Caption = '&Inclui componente do valor do frete'
      Enabled = False
      OnClick = Incluicomponentedovalordofrete1Click
    end
    object Alteracomponentedovalordofrete1: TMenuItem
      Caption = '&Altera o componente do valor do frete selecionado'
      Enabled = False
      OnClick = Alteracomponentedovalordofrete1Click
    end
    object Excluicomponentedovalordofrete1: TMenuItem
      Caption = '&Exclui o componente do valor do frete selecionado'
      Enabled = False
      OnClick = Excluicomponentedovalordofrete1Click
    end
  end
  object QrFrtFatComp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM frtfatcomp'
      'WHERE Codigo=1')
    Left = 764
    Top = 409
    object QrFrtFatCompCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatCompControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatCompCodComp: TIntegerField
      FieldName = 'CodComp'
    end
    object QrFrtFatCompxNome: TWideStringField
      FieldName = 'xNome'
      Size = 15
    end
    object QrFrtFatCompvComp: TFloatField
      FieldName = 'vComp'
    end
    object QrFrtFatCompLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatCompDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatCompDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatCompUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatCompUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatCompAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatCompAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtFatComp: TDataSource
    DataSet = QrFrtFatComp
    Left = 744
    Top = 453
  end
  object QrFRFisC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      
        ' ELT(fis.TpCTe + 1,"CT-e Normal","CT-e de Complemento de Valores' +
        '","CT-e de Anula'#231#227'o","CT-e Substituto") NO_TpCTe,'
      
        ' ELT(fis.TpCTe + 1,"N'#227'o definido","00-Tributa'#231#227'o Normal do ICMS"' +
        ',"20-tributa'#231#227'o com BC reduzida do ICMS","40-ICMS isen'#231#227'o;","41-' +
        'ICMS n'#227'o tributada;","51-ICMS diferido","60-ICMS cobrado anterio' +
        'rmente por substitui'#231#227'o tribut'#225'ria","90-ICMS outros - devido '#224' U' +
        'F de origem da presta'#231#227'o","90-ICMS outros","Simples Nacional") N' +
        'O_MyCST,'
      'fis.* '
      'FROM frtregfisc fis'
      'WHERE fis.Codigo > 0'
      'AND fis.Codigo=:P0')
    Left = 92
    Top = 272
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFRFisCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFRFisCNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrFRFisCNatOp: TWideStringField
      FieldName = 'NatOp'
      Size = 60
    end
    object QrFRFisCTpCTe: TSmallintField
      FieldName = 'TpCTe'
    end
    object QrFRFisCMyCST: TSmallintField
      FieldName = 'MyCST'
    end
    object QrFRFisCCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 5
    end
    object QrFRFisCICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrFRFisCICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrFRFisCICMS_pICMSSTRet: TFloatField
      FieldName = 'ICMS_pICMSSTRet'
    end
    object QrFRFisCICMS_pRedBCOutraUF: TFloatField
      FieldName = 'ICMS_pRedBCOutraUF'
    end
    object QrFRFisCICMS_pICMSOutraUF: TFloatField
      FieldName = 'ICMS_pICMSOutraUF'
    end
    object QrFRFisCiTotTrib_Fonte: TIntegerField
      FieldName = 'iTotTrib_Fonte'
    end
    object QrFRFisCiTotTrib_Tabela: TIntegerField
      FieldName = 'iTotTrib_Tabela'
    end
    object QrFRFisCiTotTrib_Ex: TIntegerField
      FieldName = 'iTotTrib_Ex'
    end
    object QrFRFisCiTotTrib_Codigo: TLargeintField
      FieldName = 'iTotTrib_Codigo'
    end
  end
  object PMNumero: TPopupMenu
    Left = 212
    Top = 108
    object CdigodoFaturamento1: TMenuItem
      Caption = 'C'#243'digo do Faturamento'
      OnClick = CdigodoFaturamento1Click
    end
    object NmerodoCTe1: TMenuItem
      Caption = 'N'#250'mero do CT-e'
      OnClick = NmerodoCTe1Click
    end
    object ChavedoCTe1: TMenuItem
      Caption = 'Chave do CT-e'
    end
  end
  object QrCFOP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cfop2003'
      'ORDER BY Nome')
    Left = 956
    Top = 344
    object QrCFOPCodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 5
    end
    object QrCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCFOPDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCFOPComplementacao: TWideMemoField
      FieldName = 'Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsCFOP: TDataSource
    DataSet = QrCFOP
    Left = 956
    Top = 392
  end
  object QrCTeCabA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCTeCabAAfterOpen
    Left = 32
    Top = 168
    object QrCTeCabAStatus: TIntegerField
      FieldName = 'Status'
    end
  end
end
