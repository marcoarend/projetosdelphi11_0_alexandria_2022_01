unit FrtFatSeg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmFrtFatSeg = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    VUUnidMed: TdmkValUsu;
    RGRespSeg: TdmkRadioGroup;
    Label8: TLabel;
    EdxSeg: TdmkEdit;
    Label9: TLabel;
    EdnApol: TdmkEdit;
    Label10: TLabel;
    EdnAver: TdmkEdit;
    EdvCarga: TdmkEdit;
    Label11: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenFrtFatSeg(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtFatSeg: TFmFrtFatSeg;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule(*, UnInternalConsts, MyDBCheck,
  UnMySQLCuringa, UnUMedi_PF, UnidMed, UnFrt_PF*);

{$R *.DFM}

procedure TFmFrtFatSeg.BtOKClick(Sender: TObject);
var
  xSeg, nApol, nAver: String;
  Codigo, Controle, respSeg: Integer;
  vCarga: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  respSeg        := RGrespSeg.ItemIndex;
  xSeg           := EdxSeg.Text;
  nApol          := EdnApol.Text;
  nAver          := EdnAver.Text;
  vCarga         := EdvCarga.ValueVariant;
  //
  if MyObjects.FIC(respSeg < 0, nil, 'Informe o respons�vel pelo seguro!') then Exit;
  if nAver <> '' then
  begin
    if MyObjects.FIC(nAver <> Geral.SoNumero_TT(nAver), EdnAver, 'A averba��o deve ter apenas n�meros!') then
      Exit;
    if MyObjects.FIC(Length(nAver) <> 20, EdnAver, 'O n�mero da averba��o deve ter 20 caracteres!') then
      Exit;
  end;
  //
  Controle := UMyMod.BPGS1I32('frtfatseg', 'Controle', '', '', tsPos, SQLTYpe, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtfatseg', False, [
  'Codigo', 'respSeg', 'xSeg',
  'nApol', 'nAver', 'vCarga'], [
  'Controle'], [
  Codigo, respSeg, xSeg,
  nApol, nAver, vCarga], [
  Controle], True) then  begin
    ReopenFrtFatSeg(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      EdControle.ValueVariant  := 0;
      RGrespSeg.ItemIndex      := 0;
      EdxSeg.Text              := '';
      EdnApol.Text             := '';
      EdnAver.Text             := '';
      EdvCarga.ValueVariant    := 0;
      //
      RGrespSeg.SetFocus;
    end else Close;
  end;
end;

procedure TFmFrtFatSeg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtFatSeg.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtFatSeg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmFrtFatSeg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtFatSeg.ReopenFrtFatSeg(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
