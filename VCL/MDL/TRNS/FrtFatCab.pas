unit FrtFatCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, dmkMemo,
  dmkCheckBox, Vcl.ComCtrls, dmkEditDateTimePicker, Variants;

type
  TFmFrtFatCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    QrFrtFatCab: TmySQLQuery;
    DsFrtFatCab: TDataSource;
    QrFrtFatInfQ: TmySQLQuery;
    DsFrtFatInfQ: TDataSource;
    PMInfQ: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtInfQ: TBitBtn;
    DGInfQ: TDBGrid;
    QrFrtFatInfDoc: TmySQLQuery;
    DsFrtFatInfDoc: TDataSource;
    QrFrtRegFisC: TmySQLQuery;
    DsFrtRegFisC: TDataSource;
    QrFrtRegFisCCodigo: TIntegerField;
    QrFrtRegFisCNome: TWideStringField;
    QrFrtRegFatC: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsFrtRegFat: TDataSource;
    QrTomadores: TmySQLQuery;
    QrTomadoresCodigo: TIntegerField;
    QrTomadoresNOMEENT: TWideStringField;
    QrTomadoresCIDADE: TWideStringField;
    QrTomadoresNOMEUF: TWideStringField;
    QrTomadoresCodUsu: TIntegerField;
    QrTomadoresIE: TWideStringField;
    QrTomadoresindIEDest: TIntegerField;
    QrTomadoresTipo: TIntegerField;
    DsTomadores: TDataSource;
    QrRemetentes: TmySQLQuery;
    QrRemetentesCodigo: TIntegerField;
    QrRemetentesNOMEENT: TWideStringField;
    QrRemetentesCIDADE: TWideStringField;
    QrRemetentesNOMEUF: TWideStringField;
    QrRemetentesCodUsu: TIntegerField;
    QrRemetentesIE: TWideStringField;
    QrRemetentesindIEDest: TIntegerField;
    QrRemetentesTipo: TIntegerField;
    DsRemetentes: TDataSource;
    QrExpedidores: TmySQLQuery;
    QrExpedidoresCodigo: TIntegerField;
    QrExpedidoresNOMEENT: TWideStringField;
    QrExpedidoresCIDADE: TWideStringField;
    QrExpedidoresNOMEUF: TWideStringField;
    QrExpedidoresCodUsu: TIntegerField;
    QrExpedidoresIE: TWideStringField;
    QrExpedidoresindIEDest: TIntegerField;
    QrExpedidoresTipo: TIntegerField;
    DsExpedidores: TDataSource;
    QrRecebedores: TmySQLQuery;
    QrRecebedoresCodigo: TIntegerField;
    QrRecebedoresNOMEENT: TWideStringField;
    QrRecebedoresCIDADE: TWideStringField;
    QrRecebedoresNOMEUF: TWideStringField;
    QrRecebedoresCodUsu: TIntegerField;
    QrRecebedoresIE: TWideStringField;
    QrRecebedoresindIEDest: TIntegerField;
    QrRecebedoresTipo: TIntegerField;
    DsRecebedores: TDataSource;
    QrDestinatarios: TmySQLQuery;
    QrDestinatariosCodigo: TIntegerField;
    QrDestinatariosNOMEENT: TWideStringField;
    QrDestinatariosCIDADE: TWideStringField;
    QrDestinatariosNOMEUF: TWideStringField;
    QrDestinatariosCodUsu: TIntegerField;
    QrDestinatariosIE: TWideStringField;
    QrDestinatariosindIEDest: TIntegerField;
    QrDestinatariosTipo: TIntegerField;
    DsDestinatarios: TDataSource;
    QrToma4s: TmySQLQuery;
    QrToma4sCodigo: TIntegerField;
    QrToma4sNOMEENT: TWideStringField;
    QrToma4sCIDADE: TWideStringField;
    QrToma4sNOMEUF: TWideStringField;
    QrToma4sCodUsu: TIntegerField;
    QrToma4sIE: TWideStringField;
    QrToma4sindIEDest: TIntegerField;
    QrToma4sTipo: TIntegerField;
    DsToma4s: TDataSource;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    DsPediPrzCab: TDataSource;
    QrCartEmiss: TmySQLQuery;
    QrCartEmissCodigo: TIntegerField;
    QrCartEmissNome: TWideStringField;
    DsCartEmiss: TDataSource;
    QrFrtPrcCad: TmySQLQuery;
    QrFrtPrcCadCodigo: TIntegerField;
    QrFrtPrcCadNome: TWideStringField;
    DsFrtPrcCad: TDataSource;
    Panel6: TPanel;
    CkRetira: TdmkCheckBox;
    EdvCarga: TdmkEdit;
    Label13: TLabel;
    QrCaracAd: TmySQLQuery;
    QrCaracAdCodigo: TIntegerField;
    QrCaracAdNome: TWideStringField;
    DsCaracAd: TDataSource;
    QrCaracSer: TmySQLQuery;
    QrCaracSerCodigo: TIntegerField;
    QrCaracSerNome: TWideStringField;
    DsCaracSer: TDataSource;
    QrFrtFatCabCodigo: TIntegerField;
    QrFrtFatCabFrtRegFisC: TIntegerField;
    QrFrtFatCabTomador: TIntegerField;
    QrFrtFatCabRemetente: TIntegerField;
    QrFrtFatCabExpedidor: TIntegerField;
    QrFrtFatCabRecebedor: TIntegerField;
    QrFrtFatCabDestinatario: TIntegerField;
    QrFrtFatCabToma4: TIntegerField;
    QrFrtFatCabTabelaPrc: TIntegerField;
    QrFrtFatCabCondicaoPg: TIntegerField;
    QrFrtFatCabCartEmiss: TIntegerField;
    QrFrtFatCabFrtRegFatC: TIntegerField;
    QrFrtFatCabForPag: TSmallintField;
    QrFrtFatCabModal: TSmallintField;
    QrFrtFatCabTpServ: TSmallintField;
    QrFrtFatCabCMunEnv: TIntegerField;
    QrFrtFatCabUFEnv: TWideStringField;
    QrFrtFatCabCMunIni: TIntegerField;
    QrFrtFatCabUFIni: TWideStringField;
    QrFrtFatCabCMunFim: TIntegerField;
    QrFrtFatCabUFFim: TWideStringField;
    QrFrtFatCabToma: TSmallintField;
    QrFrtFatCabCaracAd: TIntegerField;
    QrFrtFatCabCaracSer: TIntegerField;
    QrFrtFatCabxObs: TWideMemoField;
    QrFrtFatCabRetira: TSmallintField;
    QrFrtFatCabvCarga: TFloatField;
    QrFrtFatCabLk: TIntegerField;
    QrFrtFatCabDataCad: TDateField;
    QrFrtFatCabDataAlt: TDateField;
    QrFrtFatCabUserCad: TIntegerField;
    QrFrtFatCabUserAlt: TIntegerField;
    QrFrtFatCabAlterWeb: TSmallintField;
    QrFrtFatCabAtivo: TSmallintField;
    QrFrtFatCabEmpresa: TIntegerField;
    QrMunIni: TmySQLQuery;
    QrMunIniCodigo: TIntegerField;
    QrMunIniNome: TWideStringField;
    QrMunFim: TmySQLQuery;
    QrMunFimCodigo: TIntegerField;
    QrMunFimNome: TWideStringField;
    QrMunEnv: TmySQLQuery;
    QrMunEnvCodigo: TIntegerField;
    QrMunEnvNome: TWideStringField;
    DsMunEnv: TDataSource;
    DsMunFim: TDataSource;
    DsMunIni: TDataSource;
    GroupBox3: TGroupBox;
    BtTabelaPrc: TSpeedButton;
    LaTabelaPrc: TLabel;
    BtCondicaoPG: TSpeedButton;
    Label24: TLabel;
    LaCondicaoPG: TLabel;
    SpeedButton13: TSpeedButton;
    CBTabelaPrc: TdmkDBLookupComboBox;
    EdTabelaPrc: TdmkEditCB;
    CBCartEmiss: TdmkDBLookupComboBox;
    EdCartEmiss: TdmkEditCB;
    CBCondicaoPG: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    QrFRFatC: TmySQLQuery;
    QrFRFatCCodigo: TIntegerField;
    QrFRFatCNome: TWideStringField;
    QrFRFatCForPag: TSmallintField;
    QrFRFatCModal: TSmallintField;
    QrFRFatCTpServ: TSmallintField;
    QrFRFatCCMunIni: TIntegerField;
    QrFRFatCUFIni: TWideStringField;
    QrFRFatCCMunFim: TIntegerField;
    QrFRFatCUFFim: TWideStringField;
    QrFRFatCToma: TSmallintField;
    QrFRFatCCaracAd: TIntegerField;
    QrFRFatCCaracSer: TIntegerField;
    QrFRFatCxObs: TWideMemoField;
    QrFRFatCCMunEnv: TIntegerField;
    QrFRFatCUFEnv: TWideStringField;
    QrFRFatCTabelaPrc: TIntegerField;
    QrFRFatCCondicaoPg: TIntegerField;
    QrFRFatCCartEmiss: TIntegerField;
    Panel7: TPanel;
    Label7: TLabel;
    Label9: TLabel;
    Label3: TLabel;
    SbTomador: TSpeedButton;
    EdCodigo: TdmkEdit;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdTomador: TdmkEditCB;
    CBTomador: TdmkDBLookupComboBox;
    Panel9: TPanel;
    Label5: TLabel;
    Label8: TLabel;
    SbDestinatario: TSpeedButton;
    SbRemetente: TSpeedButton;
    EdRemetente: TdmkEditCB;
    CBRemetente: TdmkDBLookupComboBox;
    EdDestinatario: TdmkEditCB;
    CBDestinatario: TdmkDBLookupComboBox;
    Panel8: TPanel;
    Label11: TLabel;
    SbFrtRegFisC: TSpeedButton;
    Label6: TLabel;
    SbRecebedor: TSpeedButton;
    Label4: TLabel;
    SbExpedidor: TSpeedButton;
    Label10: TLabel;
    SbToma4: TSpeedButton;
    EdFrtRegFisC: TdmkEditCB;
    CBFrtRegFisC: TdmkDBLookupComboBox;
    EdRecebedor: TdmkEditCB;
    CBRecebedor: TdmkDBLookupComboBox;
    EdExpedidor: TdmkEditCB;
    CBExpedidor: TdmkDBLookupComboBox;
    EdToma4: TdmkEditCB;
    CBToma4: TdmkDBLookupComboBox;
    QrFrtFatInfQCodigo: TIntegerField;
    QrFrtFatInfQControle: TIntegerField;
    QrFrtFatInfQCUnid: TSmallintField;
    QrFrtFatInfQMyTpMed: TIntegerField;
    QrFrtFatInfQQCarga: TFloatField;
    QrFrtFatInfQLk: TIntegerField;
    QrFrtFatInfQDataCad: TDateField;
    QrFrtFatInfQDataAlt: TDateField;
    QrFrtFatInfQUserCad: TIntegerField;
    QrFrtFatInfQUserAlt: TIntegerField;
    QrFrtFatInfQAlterWeb: TSmallintField;
    QrFrtFatInfQAtivo: TSmallintField;
    QrFrtFatInfQNO_CUnid: TWideStringField;
    QrFrtFatInfQNO_MyTpMed: TWideStringField;
    BtInfDoc: TBitBtn;
    PMInfDoc: TPopupMenu;
    DGInfNFe: TDBGrid;
    N1: TMenuItem;
    BtEncerra: TBitBtn;
    QrFrtFatCabAbertura: TDateTimeField;
    QrFrtFatCabEncerrou: TDateTimeField;
    Label22: TLabel;
    EdvTPrest: TdmkEdit;
    EdvRec: TdmkEdit;
    Label23: TLabel;
    QrFrtFatCabvTPrest: TFloatField;
    QrFrtFatCabvRec: TFloatField;
    PMEncerra: TPopupMenu;
    Encerrafaturamento1: TMenuItem;
    MenuItem1: TMenuItem;
    Desfazencerramento1: TMenuItem;
    QrFrtFatCabrefCTE: TWideStringField;
    QrFrtFatCabXDetRetira: TWideStringField;
    QrEntiEmi: TmySQLQuery;
    DsEntiEmi: TDataSource;
    QrEntiEmiCodigo: TIntegerField;
    QrEntiEmiNOMEENT: TWideStringField;
    QrEntiEmiCIDADE: TWideStringField;
    QrEntiEmiNOMEUF: TWideStringField;
    QrEntiEmiCodUsu: TIntegerField;
    QrEntiEmiIE: TWideStringField;
    QrEntiEmiindIEDest: TIntegerField;
    QrEntiEmiTipo: TIntegerField;
    QrFrtFatCabEntiEmi: TIntegerField;
    QrLocEnt: TmySQLQuery;
    DsLocEnt: TDataSource;
    QrLocEntCodigo: TIntegerField;
    QrLocEntNOMEENT: TWideStringField;
    QrLocEntCIDADE: TWideStringField;
    QrLocEntNOMEUF: TWideStringField;
    QrLocEntCodUsu: TIntegerField;
    QrLocEntIE: TWideStringField;
    QrLocEntindIEDest: TIntegerField;
    QrLocEntTipo: TIntegerField;
    QrFrtFatCabLocEnt: TIntegerField;
    EdEntiEmi: TdmkEditCB;
    CBEntiEmi: TdmkDBLookupComboBox;
    Label27: TLabel;
    DsLocColeta: TDataSource;
    QrLocColeta: TmySQLQuery;
    QrLocColetaCodigo: TIntegerField;
    QrLocColetaNOMEENT: TWideStringField;
    QrLocColetaCIDADE: TWideStringField;
    QrLocColetaNOMEUF: TWideStringField;
    QrLocColetaCodUsu: TIntegerField;
    QrLocColetaIE: TWideStringField;
    QrLocColetaindIEDest: TIntegerField;
    QrLocColetaTipo: TIntegerField;
    QrFrtFatCabLocColeta: TIntegerField;
    QrFrtProPred: TmySQLQuery;
    DsFrtProPred: TDataSource;
    QrFrtProPredCodigo: TIntegerField;
    QrFrtProPredNome: TWideStringField;
    QrFrtFatCabFrtProPred: TIntegerField;
    QrFrtFatCabxOutCat: TWideStringField;
    AdicionainformedeDocumento1: TMenuItem;
    RemoveinformedeDocumento1: TMenuItem;
    EditainformedeDocumento1: TMenuItem;
    QrFrtFatInfDocCodigo: TIntegerField;
    QrFrtFatInfDocControle: TIntegerField;
    QrFrtFatInfDocMyTpDocInf: TSmallintField;
    QrFrtFatInfDocInfNFe_chave: TWideStringField;
    QrFrtFatInfDocInfNFe_PIN: TIntegerField;
    QrFrtFatInfDocInfNFe_dPrev: TDateField;
    QrFrtFatInfDocInfNF_nRoma: TWideStringField;
    QrFrtFatInfDocInfNF_nPed: TWideStringField;
    QrFrtFatInfDocInfNF_mod: TSmallintField;
    QrFrtFatInfDocInfNF_dEmi: TDateField;
    QrFrtFatInfDocInfNF_vBC: TFloatField;
    QrFrtFatInfDocInfNF_vICMS: TFloatField;
    QrFrtFatInfDocInfNF_vBCST: TFloatField;
    QrFrtFatInfDocInfNF_vST: TFloatField;
    QrFrtFatInfDocInfNF_vProd: TFloatField;
    QrFrtFatInfDocInfNF_vNF: TFloatField;
    QrFrtFatInfDocInfNF_nCFOP: TIntegerField;
    QrFrtFatInfDocInfNF_nPeso: TFloatField;
    QrFrtFatInfDocInfNF_PIN: TIntegerField;
    QrFrtFatInfDocInfNF_dPrev: TDateField;
    QrFrtFatInfDocInfOutros_tpDoc: TSmallintField;
    QrFrtFatInfDocInfOutros_descOutros: TWideStringField;
    QrFrtFatInfDocInfOutros_nDoc: TWideStringField;
    QrFrtFatInfDocInfOutros_dEmi: TDateField;
    QrFrtFatInfDocInfOutros_vDocFisc: TFloatField;
    QrFrtFatInfDocInfOutros_dPrev: TDateField;
    QrFrtFatInfDocLk: TIntegerField;
    QrFrtFatInfDocDataCad: TDateField;
    QrFrtFatInfDocDataAlt: TDateField;
    QrFrtFatInfDocUserCad: TIntegerField;
    QrFrtFatInfDocUserAlt: TIntegerField;
    QrFrtFatInfDocAlterWeb: TSmallintField;
    QrFrtFatInfDocAtivo: TSmallintField;
    QrFrtFatInfDocInfNF_serie: TWideStringField;
    QrFrtFatInfDocInfNF_nDoc: TWideStringField;
    QrFrtFatCabDataFat: TDateTimeField;
    QrFrtFatCabStatus: TSmallintField;
    QrFrtFatCabSerieDesfe: TIntegerField;
    QrFrtFatCabCTDesfeita: TIntegerField;
    QrFrtFatCabParamsCTe: TIntegerField;
    QrParamsCTe: TmySQLQuery;
    DsParamsCTe: TDataSource;
    QrParamsCTeControle: TIntegerField;
    QrParamsCTeNOME: TWideStringField;
    EdvOrig: TdmkEdit;
    Label33: TLabel;
    EdvDesc: TdmkEdit;
    Label34: TLabel;
    EdvLiq: TdmkEdit;
    Label35: TLabel;
    QrFrtFatCabvOrig: TFloatField;
    QrFrtFatCabvDesc: TFloatField;
    QrFrtFatCabvLiq: TFloatField;
    BtFrtFOCAtrDef: TBitBtn;
    PMCTeIt2ObsCont: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    QrCTeIt2ObsCont_0000: TmySQLQuery;
    DsCTeIt2ObsCont_0000: TDataSource;
    QrCTeIt2ObsCont_0000FatID: TIntegerField;
    QrCTeIt2ObsCont_0000FatNum: TIntegerField;
    QrCTeIt2ObsCont_0000Empresa: TIntegerField;
    QrCTeIt2ObsCont_0000Controle: TIntegerField;
    QrCTeIt2ObsCont_0000xCampo: TWideStringField;
    QrCTeIt2ObsCont_0000xTexto: TWideStringField;
    QrCTeIt2ObsCont_0000Lk: TIntegerField;
    QrCTeIt2ObsCont_0000DataCad: TDateField;
    QrCTeIt2ObsCont_0000DataAlt: TDateField;
    QrCTeIt2ObsCont_0000UserCad: TIntegerField;
    QrCTeIt2ObsCont_0000UserAlt: TIntegerField;
    QrCTeIt2ObsCont_0000AlterWeb: TSmallintField;
    QrCTeIt2ObsCont_0000Ativo: TSmallintField;
    DBGrid1: TDBGrid;
    PMFrtFocAtrDef: TPopupMenu;
    IncluiAtrAMovDef1: TMenuItem;
    AlteraAtrAMovDef1: TMenuItem;
    ExcluiAtrAMovDef1: TMenuItem;
    QrFrtFOCAtrDef: TmySQLQuery;
    DsFrtFOCAtrDef: TDataSource;
    QrFrtFOCAtrDefID_Item: TIntegerField;
    QrFrtFOCAtrDefID_Sorc: TIntegerField;
    QrFrtFOCAtrDefAtrCad: TIntegerField;
    QrFrtFOCAtrDefATRITS: TFloatField;
    QrFrtFOCAtrDefCU_CAD: TIntegerField;
    QrFrtFOCAtrDefCU_ITS: TLargeintField;
    QrFrtFOCAtrDefAtrTxt: TWideStringField;
    QrFrtFOCAtrDefNO_CAD: TWideStringField;
    QrFrtFOCAtrDefNO_ITS: TWideStringField;
    QrFrtFOCAtrDefAtrTyp: TSmallintField;
    BtPeri: TBitBtn;
    BtAutXML: TBitBtn;
    PMPeri: TPopupMenu;
    Incluiprodutoperigoso1: TMenuItem;
    Alteraprodutoperigoso1: TMenuItem;
    Excluiprodutoperigoso1: TMenuItem;
    QrFrtFatPeri: TmySQLQuery;
    DsFrtFatPeri: TDataSource;
    DBGrid2: TDBGrid;
    QrFrtFatPeriCodigo: TIntegerField;
    QrFrtFatPeriControle: TIntegerField;
    QrFrtFatPeriProdutCad: TIntegerField;
    QrFrtFatPeriqTotProd: TWideStringField;
    QrFrtFatPeriqVolTipo: TWideStringField;
    QrFrtFatPeriNome: TWideStringField;
    QrFrtFatPerinONU: TWideStringField;
    QrFrtFatPeriXNomeAE: TWideStringField;
    QrFrtFatPeriXClaRisco: TWideStringField;
    QrFrtFatPeriPERI_grEmb: TWideStringField;
    QrFrtFatPeriPontoFulgor: TWideStringField;
    CTeSubstitudo1: TMenuItem;
    QrFrtFatInfCTeSub: TmySQLQuery;
    DsFrtFatInfCTeSub: TDataSource;
    QrFrtFatInfCTeSubCodigo: TIntegerField;
    QrFrtFatInfCTeSubChCTe: TWideStringField;
    QrFrtFatInfCTeSubContribICMS: TSmallintField;
    QrFrtFatInfCTeSubTipoDoc: TSmallintField;
    QrFrtFatInfCTeSubrefNFe: TWideStringField;
    QrFrtFatInfCTeSubCNPJ: TWideStringField;
    QrFrtFatInfCTeSubCPF: TWideStringField;
    QrFrtFatInfCTeSubmod_: TWideStringField;
    QrFrtFatInfCTeSubserie: TIntegerField;
    QrFrtFatInfCTeSubsubserie: TIntegerField;
    QrFrtFatInfCTeSubnro: TIntegerField;
    QrFrtFatInfCTeSubvalor: TFloatField;
    QrFrtFatInfCTeSubdEmi: TDateField;
    QrFrtFatInfCTeSubrefCTe: TWideStringField;
    QrFrtFatInfCTeSubrefCTeAnu: TWideStringField;
    QrFrtFatInfCTeSubLk: TIntegerField;
    QrFrtFatInfCTeSubDataCad: TDateField;
    QrFrtFatInfCTeSubDataAlt: TDateField;
    QrFrtFatInfCTeSubUserCad: TIntegerField;
    QrFrtFatInfCTeSubUserAlt: TIntegerField;
    QrFrtFatInfCTeSubAlterWeb: TSmallintField;
    QrFrtFatInfCTeSubAtivo: TSmallintField;
    QrFrtFatCabNome: TWideStringField;
    QrFrtFatCabNatOp: TWideStringField;
    QrFrtFatCabCFOP: TWideStringField;
    QrFrtFatCabTpCTe: TSmallintField;
    QrFrtFatCabMyCST: TSmallintField;
    QrFrtFatCabICMS_pICMS: TFloatField;
    QrFrtFatCabICMS_pRedBC: TFloatField;
    QrFrtFatCabICMS_pICMSSTRet: TFloatField;
    QrFrtFatCabICMS_pRedBCOutraUF: TFloatField;
    QrFrtFatCabICMS_pICMSOutraUF: TFloatField;
    QrFrtFatCabiTotTrib_Fonte: TIntegerField;
    QrFrtFatCabiTotTrib_Tabela: TIntegerField;
    QrFrtFatCabiTotTrib_Ex: TIntegerField;
    QrFrtFatCabiTotTrib_Codigo: TLargeintField;
    CTeComplementado1: TMenuItem;
    CTedeAnulacao1: TMenuItem;
    QrFrtFatInfCTeAnu: TmySQLQuery;
    DsFrtFatInfCTeAnu: TDataSource;
    QrFrtFatInfCTeComp: TmySQLQuery;
    DsFrtFatInfCTeComp: TDataSource;
    QrFrtFatInfCTeAnuCodigo: TIntegerField;
    QrFrtFatInfCTeAnuChCTe: TWideStringField;
    QrFrtFatInfCTeAnudEmi: TDateField;
    QrFrtFatInfCTeCompCodigo: TIntegerField;
    QrFrtFatInfCTeCompChave: TWideStringField;
    QrFrtFatAutXml: TmySQLQuery;
    CkLota: TdmkCheckBox;
    TPDataPrev: TdmkEditDateTimePicker;
    EdDataPrev: TdmkEdit;
    Label36: TLabel;
    EdCIOT: TdmkEdit;
    Label37: TLabel;
    QrFrtFatCabDataPrev: TDateTimeField;
    QrFrtFatCabLota: TSmallintField;
    QrFrtFatCabCIOT: TLargeintField;
    BtSeg: TBitBtn;
    PMSeg: TPopupMenu;
    Adicionainformedeseguro1: TMenuItem;
    Removeinformedeseguro1: TMenuItem;
    Editainformedeseguro1: TMenuItem;
    QrFrtFatSeg: TmySQLQuery;
    DsFrtFatSeg: TDataSource;
    QrFrtFatSegCodigo: TIntegerField;
    QrFrtFatSegControle: TIntegerField;
    QrFrtFatSegrespSeg: TSmallintField;
    QrFrtFatSegxSeg: TWideStringField;
    QrFrtFatSegnApol: TWideStringField;
    QrFrtFatSegnAver: TWideStringField;
    QrFrtFatSegvCarga: TFloatField;
    QrFrtFatSegLk: TIntegerField;
    QrFrtFatSegDataCad: TDateField;
    QrFrtFatSegDataAlt: TDateField;
    QrFrtFatSegUserCad: TIntegerField;
    QrFrtFatSegUserAlt: TIntegerField;
    QrFrtFatSegAlterWeb: TSmallintField;
    QrFrtFatSegAtivo: TSmallintField;
    DBGrid3: TDBGrid;
    BtComp: TBitBtn;
    BtVPed: TBitBtn;
    BtVeic: TBitBtn;
    PMVPed: TPopupMenu;
    IncluiValePedgio1: TMenuItem;
    AlteraValePedgio1: TMenuItem;
    ExcluiValePedgio1: TMenuItem;
    QrFrtFatMoRodoVPed: TmySQLQuery;
    DsFrtFatMoRodoVPed: TDataSource;
    QrFrtFatMoRodoVeic: TmySQLQuery;
    DsFrtFatMoRodoVeic: TDataSource;
    QrFrtFatMoRodoLacr: TmySQLQuery;
    DsFrtFatMoRodoLacr: TDataSource;
    QrFrtFatMoRodoMoto: TmySQLQuery;
    DsFrtFatMoRodoMoto: TDataSource;
    QrFrtFatMoRodoVPedCodigo: TIntegerField;
    QrFrtFatMoRodoVPedControle: TIntegerField;
    QrFrtFatMoRodoVPedCNPJForn: TWideStringField;
    QrFrtFatMoRodoVPednCompra: TWideStringField;
    QrFrtFatMoRodoVPedCNPJPg: TWideStringField;
    QrFrtFatMoRodoVPedvValePed: TFloatField;
    QrFrtFatMoRodoVPedLk: TIntegerField;
    QrFrtFatMoRodoVPedDataCad: TDateField;
    QrFrtFatMoRodoVPedDataAlt: TDateField;
    QrFrtFatMoRodoVPedUserCad: TIntegerField;
    QrFrtFatMoRodoVPedUserAlt: TIntegerField;
    QrFrtFatMoRodoVPedAlterWeb: TSmallintField;
    QrFrtFatMoRodoVPedAtivo: TSmallintField;
    QrFrtFatMoRodoVeicCodigo: TIntegerField;
    QrFrtFatMoRodoVeicControle: TIntegerField;
    QrFrtFatMoRodoVeiccInt: TIntegerField;
    QrFrtFatMoRodoVeicLk: TIntegerField;
    QrFrtFatMoRodoVeicDataCad: TDateField;
    QrFrtFatMoRodoVeicDataAlt: TDateField;
    QrFrtFatMoRodoVeicUserCad: TIntegerField;
    QrFrtFatMoRodoVeicUserAlt: TIntegerField;
    QrFrtFatMoRodoVeicAlterWeb: TSmallintField;
    QrFrtFatMoRodoVeicAtivo: TSmallintField;
    QrFrtFatMoRodoLacrCodigo: TIntegerField;
    QrFrtFatMoRodoLacrControle: TIntegerField;
    QrFrtFatMoRodoLacrnLacre: TWideStringField;
    QrFrtFatMoRodoLacrLk: TIntegerField;
    QrFrtFatMoRodoLacrDataCad: TDateField;
    QrFrtFatMoRodoLacrDataAlt: TDateField;
    QrFrtFatMoRodoLacrUserCad: TIntegerField;
    QrFrtFatMoRodoLacrUserAlt: TIntegerField;
    QrFrtFatMoRodoLacrAlterWeb: TSmallintField;
    QrFrtFatMoRodoLacrAtivo: TSmallintField;
    QrFrtFatMoRodoMotoCodigo: TIntegerField;
    QrFrtFatMoRodoMotoControle: TIntegerField;
    QrFrtFatMoRodoMotoxNome: TWideStringField;
    QrFrtFatMoRodoMotoCPF: TWideStringField;
    QrFrtFatMoRodoMotoLk: TIntegerField;
    QrFrtFatMoRodoMotoDataCad: TDateField;
    QrFrtFatMoRodoMotoDataAlt: TDateField;
    QrFrtFatMoRodoMotoUserCad: TIntegerField;
    QrFrtFatMoRodoMotoUserAlt: TIntegerField;
    QrFrtFatMoRodoMotoAlterWeb: TSmallintField;
    QrFrtFatMoRodoMotoAtivo: TSmallintField;
    BtLacr: TBitBtn;
    BtMoto: TBitBtn;
    PMVeic: TPopupMenu;
    PMLacr: TPopupMenu;
    PMMoto: TPopupMenu;
    IncluiVeculo1: TMenuItem;
    AlteraVeculo1: TMenuItem;
    ExcluiVeculo1: TMenuItem;
    IncluiLacre1: TMenuItem;
    AlteraLacre1: TMenuItem;
    ExcluiLacre1: TMenuItem;
    IncluiMotorista1: TMenuItem;
    AlteraMotorista1: TMenuItem;
    ExcluiMotorista1: TMenuItem;
    DBGrid4: TDBGrid;
    DBGrid5: TDBGrid;
    DBGrid6: TDBGrid;
    DBGrid7: TDBGrid;
    PMComp: TPopupMenu;
    Incluicomponentedovalordofrete1: TMenuItem;
    Alteracomponentedovalordofrete1: TMenuItem;
    Excluicomponentedovalordofrete1: TMenuItem;
    QrFrtFatComp: TmySQLQuery;
    DsFrtFatComp: TDataSource;
    QrFrtFatCompCodigo: TIntegerField;
    QrFrtFatCompControle: TIntegerField;
    QrFrtFatCompCodComp: TIntegerField;
    QrFrtFatCompxNome: TWideStringField;
    QrFrtFatCompvComp: TFloatField;
    QrFrtFatCompLk: TIntegerField;
    QrFrtFatCompDataCad: TDateField;
    QrFrtFatCompDataAlt: TDateField;
    QrFrtFatCompUserCad: TIntegerField;
    QrFrtFatCompUserAlt: TIntegerField;
    QrFrtFatCompAlterWeb: TSmallintField;
    QrFrtFatCompAtivo: TSmallintField;
    DBGrid8: TDBGrid;
    N2: TMenuItem;
    Duplicaconhecimento1: TMenuItem;
    QrFrtFatAutXmlCodigo: TIntegerField;
    QrFrtFatAutXmlControle: TIntegerField;
    QrFrtFatAutXmlAddForma: TSmallintField;
    QrFrtFatAutXmlAddIDCad: TIntegerField;
    QrFrtFatAutXmlTipo: TSmallintField;
    QrFrtFatAutXmlCNPJ: TWideStringField;
    QrFrtFatAutXmlCPF: TWideStringField;
    QrFrtFatAutXmlLk: TIntegerField;
    QrFrtFatAutXmlDataCad: TDateField;
    QrFrtFatAutXmlDataAlt: TDateField;
    QrFrtFatAutXmlUserCad: TIntegerField;
    QrFrtFatAutXmlUserAlt: TIntegerField;
    QrFrtFatAutXmlAlterWeb: TSmallintField;
    QrFrtFatAutXmlAtivo: TSmallintField;
    QrFrtFatAutXmlNO_AddForma: TWideStringField;
    QrFrtFatAutXmlTpDOC: TWideStringField;
    QrFrtFatAutXmlNO_ENT_ECO: TWideStringField;
    QrFrtFatAutXmlCNPJ_CPF: TWideStringField;
    QrFRFisC: TmySQLQuery;
    QrFRFisCCodigo: TIntegerField;
    QrFRFisCNome: TWideStringField;
    QrFRFisCNatOp: TWideStringField;
    QrFRFisCTpCTe: TSmallintField;
    QrFRFisCMyCST: TSmallintField;
    QrFRFisCCFOP: TWideStringField;
    QrFRFisCICMS_pICMS: TFloatField;
    QrFRFisCICMS_pRedBC: TFloatField;
    QrFRFisCICMS_pICMSSTRet: TFloatField;
    QrFRFisCICMS_pRedBCOutraUF: TFloatField;
    QrFRFisCICMS_pICMSOutraUF: TFloatField;
    QrFRFisCiTotTrib_Fonte: TIntegerField;
    QrFRFisCiTotTrib_Tabela: TIntegerField;
    QrFRFisCiTotTrib_Ex: TIntegerField;
    QrFRFisCiTotTrib_Codigo: TLargeintField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GBFrtRegFatC: TPanel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    SbCaracAd: TSpeedButton;
    SbCaracSer: TSpeedButton;
    Label12: TLabel;
    SbFrtRegFatC: TSpeedButton;
    SpeedButton6: TSpeedButton;
    Label25: TLabel;
    Label26: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    SbFrtProPred: TSpeedButton;
    Label31: TLabel;
    Label32: TLabel;
    RGForPag: TdmkRadioGroup;
    RGModal: TdmkRadioGroup;
    RGTpServ: TdmkRadioGroup;
    GroupBox2: TGroupBox;
    Label17: TLabel;
    Label105: TLabel;
    CBCMunIni: TdmkDBLookupComboBox;
    EdUFIni: TdmkEdit;
    EdCMunIni: TdmkEditCB;
    GroupBox4: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    CBCMunFim: TdmkDBLookupComboBox;
    EdUFFim: TdmkEdit;
    EdCMunFim: TdmkEditCB;
    RGToma: TdmkRadioGroup;
    EdCaracAd: TdmkEditCB;
    CBCaracAd: TdmkDBLookupComboBox;
    EdCaracSer: TdmkEditCB;
    CBCaracSer: TdmkDBLookupComboBox;
    MexObs: TdmkMemo;
    GroupBox5: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    CBCMunEnv: TdmkDBLookupComboBox;
    EdUFEnv: TdmkEdit;
    EdCMunEnv: TdmkEditCB;
    EdFrtRegFatC: TdmkEditCB;
    CBFrtRegFatC: TdmkDBLookupComboBox;
    EdrefCTE: TdmkEdit;
    MeXDetRetira: TdmkMemo;
    EdLocEnt: TdmkEditCB;
    CBLocEnt: TdmkDBLookupComboBox;
    EdLocColeta: TdmkEditCB;
    CBLocColeta: TdmkDBLookupComboBox;
    EdFrtProPred: TdmkEditCB;
    CBFrtProPred: TdmkDBLookupComboBox;
    EdxOutCat: TdmkEdit;
    EdParamsCTe: TdmkEditCB;
    CBParamsCTe: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    Label40: TLabel;
    Label41: TLabel;
    SbCFOP: TSpeedButton;
    EdNatOp: TdmkEdit;
    EdCFOP: TdmkEditCB;
    CBCFOP: TdmkDBLookupComboBox;
    RGTpCte: TdmkRadioGroup;
    RGMyCST: TdmkRadioGroup;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    EdICMS_pICMS: TdmkEdit;
    EdICMS_pRedBC: TdmkEdit;
    EdICMS_pICMSSTRet: TdmkEdit;
    EdICMS_pRedBCOutraUF: TdmkEdit;
    EdICMS_pICMSOutraUF: TdmkEdit;
    GroupBox7: TGroupBox;
    Panel11: TPanel;
    RGiTotTrib_Fonte: TdmkRadioGroup;
    PnIBPTax: TPanel;
    Label47: TLabel;
    Label48: TLabel;
    RGiTotTrib_Tabela: TdmkRadioGroup;
    EdiTotTrib_Ex: TdmkEdit;
    BtiTotTrib: TBitBtn;
    EdiTotTrib_Codigo: TdmkEdit;
    Label38: TLabel;
    EdNome: TdmkEdit;
    PMNumero: TPopupMenu;
    CdigodoFaturamento1: TMenuItem;
    NmerodoCTe1: TMenuItem;
    ChavedoCTe1: TMenuItem;
    QrCFOP: TmySQLQuery;
    QrCFOPCodigo: TWideStringField;
    QrCFOPNome: TWideStringField;
    QrCFOPDescricao: TWideMemoField;
    QrCFOPComplementacao: TWideMemoField;
    DsCFOP: TDataSource;
    QrCTeCabA: TmySQLQuery;
    QrCTeCabAStatus: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFrtFatCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFrtFatCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrFrtFatCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtInfQClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMInfQPopup(Sender: TObject);
    procedure QrFrtFatCabBeforeClose(DataSet: TDataSet);
    procedure SbCaracAdClick(Sender: TObject);
    procedure SbCaracSerClick(Sender: TObject);
    procedure SbFrtRegFatCClick(Sender: TObject);
    procedure BtCondicaoPGClick(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure BtTabelaPrcClick(Sender: TObject);
    procedure MexObsKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdFrtRegFatCRedefinido(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SbRemetenteClick(Sender: TObject);
    procedure SbRecebedorClick(Sender: TObject);
    procedure SbToma4Click(Sender: TObject);
    procedure SbExpedidorClick(Sender: TObject);
    procedure SbDestinatarioClick(Sender: TObject);
    procedure SbFrtRegFisCClick(Sender: TObject);
    procedure RGTomaClick(Sender: TObject);
    procedure EdRemetenteRedefinido(Sender: TObject);
    procedure EdExpedidorRedefinido(Sender: TObject);
    procedure EdRecebedorRedefinido(Sender: TObject);
    procedure EdDestinatarioRedefinido(Sender: TObject);
    procedure EdToma4Redefinido(Sender: TObject);
    procedure BtInfDocClick(Sender: TObject);
    procedure PMInfDocPopup(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure PMEncerraPopup(Sender: TObject);
    procedure Encerrafaturamento1Click(Sender: TObject);
    procedure Desfazencerramento1Click(Sender: TObject);
    procedure EdrefCTERedefinido(Sender: TObject);
    procedure SbFrtProPredClick(Sender: TObject);
    procedure RemoveinformedeDocumento1Click(Sender: TObject);
    procedure EditainformedeDocumento1Click(Sender: TObject);
    procedure AdicionainformedeDocumento1Click(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure EdvRecRedefinido(Sender: TObject);
    procedure EdvOrigRedefinido(Sender: TObject);
    procedure EdvDescRedefinido(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtFrtFOCAtrDefClick(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure PMCTeIt2ObsContPopup(Sender: TObject);
    procedure IncluiAtrAMovDef1Click(Sender: TObject);
    procedure AlteraAtrAMovDef1Click(Sender: TObject);
    procedure ExcluiAtrAMovDef1Click(Sender: TObject);
    procedure BtPeriClick(Sender: TObject);
    procedure Incluiprodutoperigoso1Click(Sender: TObject);
    procedure Alteraprodutoperigoso1Click(Sender: TObject);
    procedure Excluiprodutoperigoso1Click(Sender: TObject);
    procedure PMPeriPopup(Sender: TObject);
    procedure CTeSubstitudo1Click(Sender: TObject);
    procedure CTeComplementado1Click(Sender: TObject);
    procedure CTedeAnulacao1Click(Sender: TObject);
    procedure BtAutXMLClick(Sender: TObject);
    procedure Adicionainformedeseguro1Click(Sender: TObject);
    procedure Removeinformedeseguro1Click(Sender: TObject);
    procedure Editainformedeseguro1Click(Sender: TObject);
    procedure PMSegPopup(Sender: TObject);
    procedure IncluiValePedgio1Click(Sender: TObject);
    procedure AlteraValePedgio1Click(Sender: TObject);
    procedure ExcluiValePedgio1Click(Sender: TObject);
    procedure BtVPedClick(Sender: TObject);
    procedure BtVeicClick(Sender: TObject);
    procedure BtLacrClick(Sender: TObject);
    procedure BtMotoClick(Sender: TObject);
    procedure IncluiVeculo1Click(Sender: TObject);
    procedure AlteraVeculo1Click(Sender: TObject);
    procedure ExcluiVeculo1Click(Sender: TObject);
    procedure ExcluiLacre1Click(Sender: TObject);
    procedure ExcluiMotorista1Click(Sender: TObject);
    procedure IncluiLacre1Click(Sender: TObject);
    procedure AlteraLacre1Click(Sender: TObject);
    procedure IncluiMotorista1Click(Sender: TObject);
    procedure AlteraMotorista1Click(Sender: TObject);
    procedure PMVPedPopup(Sender: TObject);
    procedure PMVeicPopup(Sender: TObject);
    procedure PMLacrPopup(Sender: TObject);
    procedure PMMotoPopup(Sender: TObject);
    procedure Incluicomponentedovalordofrete1Click(Sender: TObject);
    procedure Alteracomponentedovalordofrete1Click(Sender: TObject);
    procedure Excluicomponentedovalordofrete1Click(Sender: TObject);
    procedure PMCompPopup(Sender: TObject);
    procedure Duplicaconhecimento1Click(Sender: TObject);
    procedure BtSegClick(Sender: TObject);
    procedure BtCompClick(Sender: TObject);
    procedure EdFrtRegFisCRedefinido(Sender: TObject);
    procedure CdigodoFaturamento1Click(Sender: TObject);
    procedure NmerodoCTe1Click(Sender: TObject);
    procedure RGiTotTrib_FonteClick(Sender: TObject);
    procedure BtiTotTribClick(Sender: TObject);
    procedure EdvTPrestRedefinido(Sender: TObject);
    procedure MeXDetRetiraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrCTeCabAAfterOpen(DataSet: TDataSet);
  private
    FPreparandoEdicao: Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure HabilitaBotoes(Habilita: Boolean);
    procedure LocalizaFatNumPorCTe();
    procedure MostraFormFrtFatComp(SQLType: TSQLType);
    procedure MostraFormFrtFatInfQ(SQLType: TSQLType);
    procedure MostraFormFrtFatInfDoc(SQLType: TSQLType);
    procedure MostraFormCTeIt2ObsCon(SQLType: TSQLType);
    procedure MostraFormFrtFatPeri(SQLType: TSQLType);
    procedure MostraFormFrtFatSeg(SQLType: TSQLType);
    //
    procedure CalculaValorLiq();
    procedure PreencheCamposFrtRegFatC(Avisa: Boolean);
    procedure PreencheCamposFrtRegFisC(Avisa: Boolean);
    procedure DefineTomadorDoServico();
    procedure MostraFormAtrAMovDef(SQLType: TSQLType);
    procedure MostraFormFrtFatInfCTeSub();
    procedure MostraFormFrtFatInfCTeComp();
    procedure MostraFormFrtFatInfCTeAnu();
    procedure MostraFormFrtFatAutXml();

    procedure MostraFormFrtFatMoRodoLacr(SQLType: TSQLType);
    procedure MostraFormFrtFatMoRodoMoto(SQLType: TSQLType);
    procedure MostraFormFrtFatMoRodoVeic(SQLType: TSQLType);
    procedure MostraFormFrtFatMoRodoVPed(SQLType: TSQLType);

    procedure AcaoPosteriorTpCTe();
    procedure ReopenEntiAll(Qry: TmySQLQuery);
    procedure ReopenParamsCTe();
    procedure ReopenFrtFOCAtrDef(ID_Item: Integer);
    procedure ReopenFrtFatAutXml(Controle: Integer);

  public
    { Public declarations }
    FSeq, FCabIni, FTomadorAtual: Integer;
    FBtCab, FLocIni: Boolean;
    //
    procedure ReopenCTeCabA();
    //
    procedure ReopenFrtFatComp(Controle: Integer);
    procedure ReopenFrtFatInfQ(Controle: Integer);
    procedure ReopenFrtFatPeri(Controle: Integer);
    procedure ReopenFrtFatInfDoc(Controle: Integer);
    procedure ReopenFrtFatInfCTeSub();
    procedure ReopenFrtFatInfCTeComp();
    procedure ReopenFrtFatInfCTeAnu();
    procedure ReopenFrtFatSeg(Controle: Integer);
    procedure ReopenFrtFatMoRodoVPed(Controle: Integer);
    procedure ReopenFrtFatMoRodoVeic(Controle: Integer);
    procedure ReopenFrtFatMoRodoLacr(Controle: Integer);
    procedure ReopenFrtFatMoRodoMoto(Controle: Integer);

    //procedure ReopenCTeIt2ObsCont_0000(Controle: Integer);

  end;

var
  FmFrtFatCab: TFmFrtFatCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, FrtFatInfQ, ModuleGeral,
  UnFinanceiroJan, UnFrt_PF, UnPraz_PF, UnEntities, UnCTeListas, FrtFatInfDoc,
  UnCTe_PF, UnXXe_PF, CTeIt2ObsCont_0000, CfgAtributos, FrtFatPeri,
  FrtFatInfCTeSub, FrtFatInfCTeComp, FrtFatInfCTeAnu, FrtFatSeg, FrtFatComp,
  FrtFatMoRodoLacr, FrtFatMoRodoMoto, FrtFatMoRodoVeic, FrtFatMoRodoVPed,
  IBPTaxCad;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFrtFatCab.LocalizaFatNumPorCTe();
var
  CTeNum, FatNum: Integer;
  CTeTxt: String;
  Qry: TmySQLQuery;
begin
  CTeTxt := '';
  if InputQuery('Pesquisa por CT-e', 'Informe o n�mero do CT-e', CteTxt) then
  begin
// Por chave
    if Length(CTeTxt) = 44 then
      CteTxt := Copy(CteTxt, DEMONTA_CHV_INI_NumCTe,  DEMONTA_CHV_TAM_NumCTe);
//
    CteNum := Geral.IMV(CteTxt);
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT FatNum,',
      'IF(Status=100, 1, IF(Status=101, 2, 3)) Importancia ',
      'FROM ctecaba ',
      'WHERE ide_nCT=' + CteTxt,
      'AND FatID=' + Geral.FF0(VAR_FATID_5001),
      'ORDER BY Importancia',
      '']);
      if Qry.RecordCount > 0 then
      begin
        FatNum := Qry.FieldByName('FatNum').AsInteger;
        LocCod(FatNum, FatNum);
      end else
        Geral.MB_Info('Nenhuma CT-e foi localizada!');
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmFrtFatCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFrtFatCab.MeXDetRetiraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 13) then
  begin
    if (not (ssCtrl in Shift)) then
    begin
      MexObs.SetFocus;
      Key := 0;
    end;
  end;
end;

procedure TFmFrtFatCab.MexObsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 13) then
  begin
    if (not (ssCtrl in Shift)) then
    begin
      EdTabelaPrc.SetFocus;
      Key := 0;
    end;
  end;
end;

procedure TFmFrtFatCab.MostraFormAtrAMovDef(SQLType: TSQLType);
begin
  if (SQLType = stIns) and
  (QrFrtFOCAtrDef.RecordCount >= CO_MAX_ITENS_CTE_ObsFisco) then
  begin
    Geral.MB_Aviso('O limite m�ximo de itens para este CTe j� foi atingido');
    Exit;
  end;
  UnCfgAtributos.InsAltAtrDef_ITS(QrFrtFatCabCodigo.Value,
  QrFrtFOCAtrDefID_Item.Value, 'frtfocatrdef', 'frtfocatrcad', 'frtfocatrits', 'frtfocatrtxt',
  SQLType, QrFrtFOCAtrDefAtrCad.Value, Trunc(QrFrtFOCAtrDefAtrIts.Value), '',
  QrFrtFOCAtrDef, nil, True);
end;

procedure TFmFrtFatCab.MostraFormFrtFatAutXml();
var
  Codigo, Tomador: Integer;
begin
  if (QrFrtFatCab.State <> dsInactive) and (QrFrtFatCabCodigo.Value <> 0) then
  begin
    Codigo  := QrFrtFatCabCodigo.Value;
    Tomador := QrFrtFatCabTomador.Value;
    //
    Frt_PF.MostraFormFrtFatAutXml(Codigo, Tomador, True);
    //
    ReopenFrtFatAutXml(0);
  end;
end;

procedure TFmFrtFatCab.MostraFormFrtFatComp(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtFatComp, FmFrtFatComp, afmoNegarComAviso) then
  begin
    FmFrtFatComp.ImgTipo.SQLType := SQLType;
    FmFrtFatComp.FQrCab := QrFrtFatCab;
    FmFrtFatComp.FDsCab := DsFrtFatCab;
    FmFrtFatComp.FQrIts := QrFrtFatComp;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtFatComp.EdControle.ValueVariant   := QrFrtFatCompControle.Value;
      //
      FmFrtFatComp.EdCodComp.ValueVariant    := QrFrtFatCompCodComp.Value;
      FmFrtFatComp.CBCodComp.KeyValue        := QrFrtFatCompCodComp.Value;
      FmFrtFatComp.EdvComp.ValueVariant      := QrFrtFatCompvComp.Value;
    end;
    FmFrtFatComp.ShowModal;
    FmFrtFatComp.Destroy;
  end;
end;

procedure TFmFrtFatCab.MostraFormCTeIt2ObsCon(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmCTeIt2ObsCont_0000, FmCTeIt2ObsCont_0000, afmoNegarComAviso) then
  begin
    FmCTeIt2ObsCont_0000.ImgTipo.SQLType := SQLType;
    FmCTeIt2ObsCont_0000.FQrCab := QrFrtFatCab;
    FmCTeIt2ObsCont_0000.FDsCab := DsFrtFatCab;
    FmCTeIt2ObsCont_0000.FQrIts := QrCTeIt2ObsCont_0000;
    FmCTeIt2ObsCont_0000.EdFatID.ValueVariant      := VAR_FATID_5001;
    FmCTeIt2ObsCont_0000.EdFatNum.ValueVariant     := QrFrtFatCabCodigo.Value;
    FmCTeIt2ObsCont_0000.EdEmpresa.ValueVariant    := QrFrtFatCabEmpresa.Value;
    if SQLType = stIns then
      //
    else
    begin
      FmCTeIt2ObsCont_0000.EdControle.ValueVariant   := QrCTeIt2ObsCont_0000Controle.Value;
      //
      FmCTeIt2ObsCont_0000.EdxCampo.ValueVariant     := QrCTeIt2ObsCont_0000xCampo.Value;
      FmCTeIt2ObsCont_0000.EdxTexto.ValueVariant     := QrCTeIt2ObsCont_0000xTexto.Value;
    end;
    FmCTeIt2ObsCont_0000.ShowModal;
    FmCTeIt2ObsCont_0000.Destroy;
  end;
end;

procedure TFmFrtFatCab.MostraFormFrtFatInfCTeAnu();
var
  SQLType: TSQLType;
begin
  if QrFrtFatInfCTeAnu.RecordCount = 0 then
    SQLType := stIns
  else
    SQLType := stUpd;
  //
  if DBCheck.CriaFm(TFmFrtFatInfCTeAnu, FmFrtFatInfCTeAnu, afmoNegarComAviso) then
  begin
    FmFrtFatInfCTeAnu.ImgTipo.SQLType := SQLType;
    FmFrtFatInfCTeAnu.FQrCab := QrFrtFatCab;
    FmFrtFatInfCTeAnu.FDsCab := DsFrtFatCab;
    FmFrtFatInfCTeAnu.FQrIts := QrFrtFatInfCTeAnu;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtFatInfCTeAnu.EdChCTe.Text := QrFrtFatInfCTeAnuChCTe.Value;
      FmFrtFatInfCTeAnu.TPdEmi.Date  := QrFrtFatInfCTeAnudEmi.Value;
    end;
    FmFrtFatInfCTeAnu.ShowModal;
    FmFrtFatInfCTeAnu.Destroy;
  end;
end;

procedure TFmFrtFatCab.MostraFormFrtFatInfCTeComp();
var
  SQLType: TSQLType;
begin
  if QrFrtFatInfCTeComp.RecordCount = 0 then
    SQLType := stIns
  else
    SQLType := stUpd;
  //
  if DBCheck.CriaFm(TFmFrtFatInfCTeComp, FmFrtFatInfCTeComp, afmoNegarComAviso) then
  begin
    FmFrtFatInfCTeComp.ImgTipo.SQLType := SQLType;
    FmFrtFatInfCTeComp.FQrCab := QrFrtFatCab;
    FmFrtFatInfCTeComp.FDsCab := DsFrtFatCab;
    FmFrtFatInfCTeComp.FQrIts := QrFrtFatInfCTeComp;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtFatInfCTeComp.EdChave.Text := QrFrtFatInfCTeCompChave.Value;
    end;
    FmFrtFatInfCTeComp.ShowModal;
    FmFrtFatInfCTeComp.Destroy;
  end;
end;

procedure TFmFrtFatCab.MostraFormFrtFatInfCTeSub();
var
  SQLType: TSQLType;
begin
  if QrFrtFatInfCTeSub.RecordCount = 0 then
    SQLType := stIns
  else
    SQLType := stUpd;
  //
  if DBCheck.CriaFm(TFmFrtFatInfCTeSub, FmFrtFatInfCTeSub, afmoNegarComAviso) then
  begin
    FmFrtFatInfCTeSub.ImgTipo.SQLType := SQLType;
    FmFrtFatInfCTeSub.FQrCab := QrFrtFatCab;
    FmFrtFatInfCTeSub.FDsCab := DsFrtFatCab;
    FmFrtFatInfCTeSub.FQrIts := QrFrtFatInfCTeSub;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtFatInfCTeSub.EdChCTe.Text            := QrFrtFatInfCTeSubChCTe.Value;
      FmFrtFatInfCTeSub.RGContribICMS.ItemIndex := QrFrtFatInfCTeSubContribICMS.Value;
      FmFrtFatInfCTeSub.RGTipoDoc.ItemIndex     := QrFrtFatInfCTeSubTipoDoc.Value;
      FmFrtFatInfCTeSub.EdrefNFe.Text           := QrFrtFatInfCTeSubrefNFe.Value;
      FmFrtFatInfCTeSub.EdCNPJ.ValueVariant     := QrFrtFatInfCTeSubCNPJ.Value;
      FmFrtFatInfCTeSub.EdCPF.ValueVariant      := QrFrtFatInfCTeSubCPF.Value;
      FmFrtFatInfCTeSub.Edmod_.Text             := QrFrtFatInfCTeSubmod_.Value;
      FmFrtFatInfCTeSub.Edserie.ValueVariant    := QrFrtFatInfCTeSubserie.Value;
      FmFrtFatInfCTeSub.Edsubserie.ValueVariant := QrFrtFatInfCTeSubsubserie.Value;
      FmFrtFatInfCTeSub.Ednro.ValueVariant      := QrFrtFatInfCTeSubnro.Value;
      FmFrtFatInfCTeSub.Edvalor.ValueVariant    := QrFrtFatInfCTeSubvalor.Value;
      FmFrtFatInfCTeSub.TPdEmi.Date             := QrFrtFatInfCTeSubdEmi.Value;
      FmFrtFatInfCTeSub.EdrefCTe.Text           := QrFrtFatInfCTeSubrefCTe.Value;
      FmFrtFatInfCTeSub.EdrefCTeAnu.Text        := QrFrtFatInfCTeSubrefCTeAnu.Value;
    end;
    FmFrtFatInfCTeSub.ShowModal;
    FmFrtFatInfCTeSub.Destroy;
  end;
end;

procedure TFmFrtFatCab.MostraFormFrtFatInfDoc(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtFatInfDoc, FmFrtFatInfDoc, afmoNegarComAviso) then
  begin
    FmFrtFatInfDoc.ImgTipo.SQLType := SQLType;
    FmFrtFatInfDoc.FQrCab := QrFrtFatCab;
    FmFrtFatInfDoc.FDsCab := DsFrtFatCab;
    FmFrtFatInfDoc.FQrIts := QrFrtFatInfDoc;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtFatInfDoc.EdControle.ValueVariant := QrFrtFatInfDocControle.Value;
      //
      FmFrtFatInfDoc.RGMyTpDocInf.ItemIndex      := QrFrtFatInfDocMyTpDocInf.Value;
      //
      FmFrtFatInfDoc.EdNFe_Chave.ValueVariant             := QrFrtFatInfDocInfNFe_Chave.Value;
      FmFrtFatInfDoc.EdNFe_PIN.ValueVariant               := QrFrtFatInfDocInfNFe_PIN.Value;
      FmFrtFatInfDoc.TPNFe_dPrev.Date                     := QrFrtFatInfDocInfNFe_dPrev.Value;
      //
      FmFrtFatInfDoc.EdInfNF_nRoma.ValueVariant           := QrFrtFatInfDocInfNF_nRoma.Value;
      FmFrtFatInfDoc.EdInfNF_nPed.ValueVariant            := QrFrtFatInfDocInfNF_nPed.Value;
      FmFrtFatInfDoc.RGInfNF_mod.ItemIndex                := QrFrtFatInfDocInfNF_mod.Value;
      FmFrtFatInfDoc.EdInfNF_serie.ValueVariant           := QrFrtFatInfDocInfNF_serie.Value;
      FmFrtFatInfDoc.EdInfNF_nDoc.ValueVariant            := QrFrtFatInfDocInfNF_nDoc.Value;
      FmFrtFatInfDoc.TPInfNF_dEmi.Date                    := QrFrtFatInfDocInfNF_dEmi.Value;
      FmFrtFatInfDoc.EdInfNF_vBC.ValueVariant             := QrFrtFatInfDocInfNF_vBC.Value;
      FmFrtFatInfDoc.EdInfNF_vICMS.ValueVariant           := QrFrtFatInfDocInfNF_vICMS.Value;
      FmFrtFatInfDoc.EdInfNF_vBCST.ValueVariant           := QrFrtFatInfDocInfNF_vBCST.Value;
      FmFrtFatInfDoc.EdInfNF_vST.ValueVariant             := QrFrtFatInfDocInfNF_vST.Value;
      FmFrtFatInfDoc.EdInfNF_vProd.ValueVariant           := QrFrtFatInfDocInfNF_vProd.Value;
      FmFrtFatInfDoc.EdInfNF_vNF.ValueVariant             := QrFrtFatInfDocInfNF_vNF.Value;
      FmFrtFatInfDoc.EdInfNF_nCFOP.ValueVariant           := QrFrtFatInfDocInfNF_nCFOP.Value;
      FmFrtFatInfDoc.EdInfNF_nPeso.ValueVariant           := QrFrtFatInfDocInfNF_nPeso.Value;
      FmFrtFatInfDoc.EdInfNF_PIN.ValueVariant             := QrFrtFatInfDocInfNF_PIN.Value;
      FmFrtFatInfDoc.TPInfNF_dPrev.Date                   := QrFrtFatInfDocInfNF_dPrev.Value;
      //
      case QrFrtFatInfDocInfOutros_tpDoc.Value of
         0: FmFrtFatInfDoc.RGInfOutros_tpDoc.ItemIndex    := 1;
        10: FmFrtFatInfDoc.RGInfOutros_tpDoc.ItemIndex    := 2;
        99: FmFrtFatInfDoc.RGInfOutros_tpDoc.ItemIndex    := 3;
        else FmFrtFatInfDoc.RGInfOutros_tpDoc.ItemIndex   := 0;
      end;
      FmFrtFatInfDoc.EdInfOutros_descOutros.ValueVariant  := QrFrtFatInfDocInfOutros_descOutros.Value;
      FmFrtFatInfDoc.EdInfOutros_nDoc.ValueVariant        := QrFrtFatInfDocInfOutros_nDoc.Value;
      FmFrtFatInfDoc.TPInfOutros_dEmi.Date                := QrFrtFatInfDocInfOutros_dEmi.Value;
      FmFrtFatInfDoc.EdInfOutros_vDocFisc.ValueVariant    := QrFrtFatInfDocInfOutros_vDocFisc.Value;
      FmFrtFatInfDoc.TPInfOutros_dPrev.Date               := QrFrtFatInfDocInfOutros_dPrev.Value;
    end;
    FmFrtFatInfDoc.ShowModal;
    FmFrtFatInfDoc.Destroy;
  end;
end;

procedure TFmFrtFatCab.MostraFormFrtFatInfQ(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtFatInfQ, FmFrtFatInfQ, afmoNegarComAviso) then
  begin
    FmFrtFatInfQ.ImgTipo.SQLType := SQLType;
    FmFrtFatInfQ.FQrCab := QrFrtFatCab;
    FmFrtFatInfQ.FDsCab := DsFrtFatCab;
    FmFrtFatInfQ.FQrIts := QrFrtFatInfQ;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtFatInfQ.EdControle.ValueVariant := QrFrtFatInfQControle.Value;
      //
      FmFrtFatInfQ.RGCUnid.ItemIndex      := QrFrtFatInfQCUnid.Value;
      FmFrtFatInfQ.EdMyTpMed.ValueVariant := QrFrtFatInfQMyTpMed.Value;
      FmFrtFatInfQ.CBMyTpMed.KeyValue     := QrFrtFatInfQMyTpMed.Value;
      FmFrtFatInfQ.EdQCarga.ValueVariant  := QrFrtFatInfQQCarga.Value;
    end;
    FmFrtFatInfQ.ShowModal;
    FmFrtFatInfQ.Destroy;
  end;
end;

procedure TFmFrtFatCab.MostraFormFrtFatMoRodoLacr(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtFatMoRodoLacr, FmFrtFatMoRodoLacr, afmoNegarComAviso) then
  begin
    FmFrtFatMoRodoLacr.ImgTipo.SQLType := SQLType;
    FmFrtFatMoRodoLacr.FQrCab := QrFrtFatCab;
    FmFrtFatMoRodoLacr.FDsCab := DsFrtFatCab;
    FmFrtFatMoRodoLacr.FQrIts := QrFrtFatMoRodoLacr;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtFatMoRodoLacr.EdControle.ValueVariant := QrFrtFatMoRodoLacrControle.Value;
      //
      FmFrtFatMoRodoLacr.EdnLacre.ValueVariant    := QrFrtFatMoRodoLacrnLacre.Value;
    end;
    FmFrtFatMoRodoLacr.ShowModal;
    FmFrtFatMoRodoLacr.Destroy;
  end;
end;

procedure TFmFrtFatCab.MostraFormFrtFatMoRodoMoto(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtFatMoRodoMoto, FmFrtFatMoRodoMoto, afmoNegarComAviso) then
  begin
    FmFrtFatMoRodoMoto.ImgTipo.SQLType := SQLType;
    FmFrtFatMoRodoMoto.FQrCab := QrFrtFatCab;
    FmFrtFatMoRodoMoto.FDsCab := DsFrtFatCab;
    FmFrtFatMoRodoMoto.FQrIts := QrFrtFatMoRodoMoto;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtFatMoRodoMoto.EdControle.ValueVariant := QrFrtFatMoRodoMotoControle.Value;
      //
      FmFrtFatMoRodoMoto.EdxNome.Text             := QrFrtFatMoRodoMotoxNome.Value;
      FmFrtFatMoRodoMoto.EdCPF.ValueVariant       := QrFrtFatMoRodoMotoCPF.Value;
    end;
    FmFrtFatMoRodoMoto.ShowModal;
    FmFrtFatMoRodoMoto.Destroy;
  end;
end;

procedure TFmFrtFatCab.MostraFormFrtFatMoRodoVeic(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtFatMoRodoVeic, FmFrtFatMoRodoVeic, afmoNegarComAviso) then
  begin
    FmFrtFatMoRodoVeic.ImgTipo.SQLType := SQLType;
    FmFrtFatMoRodoVeic.FQrCab := QrFrtFatCab;
    FmFrtFatMoRodoVeic.FDsCab := DsFrtFatCab;
    FmFrtFatMoRodoVeic.FQrIts := QrFrtFatMoRodoVeic;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtFatMoRodoVeic.EdControle.ValueVariant := QrFrtFatMoRodoVeicControle.Value;
      //
      FmFrtFatMoRodoVeic.EdcInt.ValueVariant      := QrFrtFatMoRodoVeiccInt.Value;
      FmFrtFatMoRodoVeic.CBcInt.KeyValue          := QrFrtFatMoRodoVeiccInt.Value;
    end;
    FmFrtFatMoRodoVeic.ShowModal;
    FmFrtFatMoRodoVeic.Destroy;
  end;
end;

procedure TFmFrtFatCab.MostraFormFrtFatMoRodoVPed(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtFatMoRodoVPed, FmFrtFatMoRodoVPed, afmoNegarComAviso) then
  begin
    FmFrtFatMoRodoVPed.ImgTipo.SQLType := SQLType;
    FmFrtFatMoRodoVPed.FQrCab := QrFrtFatCab;
    FmFrtFatMoRodoVPed.FDsCab := DsFrtFatCab;
    FmFrtFatMoRodoVPed.FQrIts := QrFrtFatMoRodoVPed;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtFatMoRodoVPed.EdControle.ValueVariant := QrFrtFatMoRodoVPedControle.Value;
      //
      FmFrtFatMoRodoVPed.EdCNPJForn.ValueVariant  := QrFrtFatMoRodoVPedCNPJForn.Value;
      FmFrtFatMoRodoVPed.EdnCompra.Text           := QrFrtFatMoRodoVPednCompra.Value;
      FmFrtFatMoRodoVPed.EdCNPJPg.ValueVariant    := QrFrtFatMoRodoVPedCNPJPg.Value;
      FmFrtFatMoRodoVPed.EdvValePed.ValueVariant  := QrFrtFatMoRodoVPedvValePed.Value;
    end;
    FmFrtFatMoRodoVPed.ShowModal;
    FmFrtFatMoRodoVPed.Destroy;
  end;
end;

procedure TFmFrtFatCab.MostraFormFrtFatPeri(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtFatPeri, FmFrtFatPeri, afmoNegarComAviso) then
  begin
    FmFrtFatPeri.ImgTipo.SQLType := SQLType;
    FmFrtFatPeri.FQrCab := QrFrtFatCab;
    FmFrtFatPeri.FDsCab := DsFrtFatCab;
    FmFrtFatPeri.FQrIts := QrFrtFatPeri;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtFatPeri.EdControle.ValueVariant := QrFrtFatPeriControle.Value;
      //
      FmFrtFatPeri.EdProdutCad.ValueVariant := QrFrtFatPeriProdutCad.Value;
      FmFrtFatPeri.CBProdutCad.KeyValue     := QrFrtFatPeriProdutCad.Value;
      FmFrtFatPeri.EdqTotProd.Text          := QrFrtFatPeriqTotProd.Value;
      FmFrtFatPeri.EdqVolTipo.Text          := QrFrtFatPeriqVolTipo.Value;
      FmFrtFatPeri.EdgrEmb.Text             := QrFrtFatPeriPERI_grEmb.Value;
    end;
    FmFrtFatPeri.ShowModal;
    FmFrtFatPeri.Destroy;
  end;
end;

procedure TFmFrtFatCab.MostraFormFrtFatSeg(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtFatSeg, FmFrtFatSeg, afmoNegarComAviso) then
  begin
    FmFrtFatSeg.ImgTipo.SQLType := SQLType;
    FmFrtFatSeg.FQrCab := QrFrtFatCab;
    FmFrtFatSeg.FDsCab := DsFrtFatCab;
    FmFrtFatSeg.FQrIts := QrFrtFatSeg;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtFatSeg.EdControle.ValueVariant := QrFrtFatSegControle.Value;
      //
      FmFrtFatSeg.RGRespSeg.ItemIndex      := QrFrtFatSegrespSeg.Value;
      FmFrtFatSeg.EdxSeg.Text              := QrFrtFatSegxSeg.Value;
      FmFrtFatSeg.EdnApol.Text             := QrFrtFatSegnApol.Value;
      FmFrtFatSeg.EdnAver.Text             := QrFrtFatSegnAver.Value;
      FmFrtFatSeg.EdvCarga.ValueVariant    := QrFrtFatSegvCarga.Value;
    end;
    FmFrtFatSeg.ShowModal;
    FmFrtFatSeg.Destroy;
  end;
end;

procedure TFmFrtFatCab.NmerodoCTe1Click(Sender: TObject);
begin
  LocalizaFatNumPorCTe();
end;

procedure TFmFrtFatCab.PMCabPopup(Sender: TObject);
begin
  if not FBtCab then
  begin
    CabAltera1.Enabled := False;
    CabExclui1.Enabled := False;
    CTeSubstitudo1.Enabled := False;
    CTeComplementado1.Enabled := False;
    CTedeAnulacao1.Enabled := False;
  end else
  begin
    MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrFrtFatCab);
    MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrFrtFatCab, QrFrtFatInfQ);
    CTeSubstitudo1.Enabled := CabAltera1.Enabled and
      (QrFrtFatCabTpCTe.Value = Integer(TCTeTpCTe.ctetpcteSubstituto));
    CTeComplementado1.Enabled := CabAltera1.Enabled and
      (QrFrtFatCabTpCTe.Value = Integer(TCTeTpCTe.ctetpcteComplValrs));
    CTedeAnulacao1.Enabled := CabAltera1.Enabled and
      (QrFrtFatCabTpCTe.Value = Integer(TCTeTpCTe.ctetpcteAnulacao));
  end;
end;

procedure TFmFrtFatCab.PMCompPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluicomponentedovalordofrete1, QrFrtFatCab);
  MyObjects.HabilitaMenuItemItsUpd(Alteracomponentedovalordofrete1, QrFrtFatComp);
  MyObjects.HabilitaMenuItemItsDel(Excluicomponentedovalordofrete1, QrFrtFatComp);
end;

procedure TFmFrtFatCab.PMCTeIt2ObsContPopup(Sender: TObject);
begin
 { TODO 0 -cCTe : Popup ObsCont}
end;

procedure TFmFrtFatCab.PMEncerraPopup(Sender: TObject);
var
  Habilita, Habil: Boolean;
begin
  Habil    := (QrFrtFatCab.State <> dsInactive) and (QrFrtFatCab.RecordCount > 0);
  if MyObjects.FIC(not Habil, nil, 'Nenhum faturamento foi selecionado!') then exit;
  Habil    := (QrFrtFatInfDoc.State <> dsInactive) and (QrFrtFatInfDoc.RecordCount > 0);
  if MyObjects.FIC(not Habil, nil, 'Informe pelo menos um item de documento!') then exit;
  Habil    := (QrFrtFatInfQ.State <> dsInactive) and (QrFrtFatInfQ.RecordCount > 0);
  if MyObjects.FIC(not Habil, nil, 'Informe pelo menos um item de quantidade de carga!') then exit;
  //
  Habil    := (QrFrtFatCab.State <> dsInactive) and (QrFrtFatCab.RecordCount > 0);
  Habilita := (QrFrtFatCabEncerrou.Value = 0) and Habil;
  Encerrafaturamento1.Enabled := Habilita;
  Desfazencerramento1.Enabled := not Habilita
    and (QrFrtFatCabStatus.Value < Integer(ctemystatusCTeAddedLote(*50*)));
end;

procedure TFmFrtFatCab.PMInfDocPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(AdicionainformedeDocumento1, QrFrtFatCab);
  MyObjects.HabilitaMenuItemItsUpd(EditainformedeDocumento1, QrFrtFatInfDoc);
  MyObjects.HabilitaMenuItemItsDel(RemoveinformedeDocumento1, QrFrtFatInfDoc);
end;

procedure TFmFrtFatCab.PMInfQPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrFrtFatCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrFrtFatInfQ);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrFrtFatInfQ);
end;

procedure TFmFrtFatCab.PMLacrPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiLacre1, QrFrtFatCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraLacre1, QrFrtFatMoRodoLacr);
  MyObjects.HabilitaMenuItemItsDel(ExcluiLacre1, QrFrtFatMoRodoLacr);
end;

procedure TFmFrtFatCab.PMMotoPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiMotorista1, QrFrtFatCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraMotorista1, QrFrtFatMoRodoMoto);
  MyObjects.HabilitaMenuItemItsDel(ExcluiMotorista1, QrFrtFatMoRodoMoto);
end;

procedure TFmFrtFatCab.PMPeriPopup(Sender: TObject);
begin
 { TODO 0 -cCTe : Popup Peri}
end;

procedure TFmFrtFatCab.PMSegPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Adicionainformedeseguro1, QrFrtFatCab);
  MyObjects.HabilitaMenuItemItsUpd(Editainformedeseguro1,    QrFrtFatSeg);
  MyObjects.HabilitaMenuItemItsDel(Removeinformedeseguro1,   QrFrtFatSeg);
end;

procedure TFmFrtFatCab.PMVeicPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiVeculo1, QrFrtFatCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraVeculo1, QrFrtFatMoRodoVeic);
  MyObjects.HabilitaMenuItemItsDel(ExcluiVeculo1, QrFrtFatMoRodoVeic);
end;

procedure TFmFrtFatCab.PMVPedPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiValePedgio1, QrFrtFatCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraValePedgio1, QrFrtFatMoRodoVPed);
  MyObjects.HabilitaMenuItemItsDel(ExcluiValePedgio1, QrFrtFatMoRodoVPed);
end;

procedure TFmFrtFatCab.PreencheCamposFrtRegFatC(Avisa: Boolean);
var
  FrtRegFatC: Integer;
begin
  if FPreparandoEdicao then
    Exit;
  FrtRegFatC := EdFrtRegFatC.ValueVariant;
  if FrtRegFatC <> 0 then
  begin
    UnDmkdac_PF.AbreMySQLQuery0(QrFRFatC, Dmod.MyDB, [
    'SELECT * ',
    'FROM FrtRegFatC ',
    'WHERE Codigo=' + Geral.FF0(FrtRegFatC),
    '']);
    //
    RGForPag.ItemIndex        := QrFRFatCForPag.Value;
    RGModal.ItemIndex         := QrFRFatCModal.Value;
    RGTpServ.ItemIndex        := QrFRFatCTpServ.Value;
    RGToma.ItemIndex          := QrFRFatCToma.Value;
    EdUFIni.ValueVariant      := QrFRFatCUFIni.Value;
    EdCMunIni.ValueVariant    := QrFRFatCCMunIni.Value;
    CBCMunIni.KeyValue        := QrFRFatCCMunIni.Value;
    EdUFFim.ValueVariant      := QrFRFatCUFFim.Value;
    EdCMunFim.ValueVariant    := QrFRFatCCMunFim.Value;
    CBCMunFim.KeyValue        := QrFRFatCCMunFim.Value;
    EdUFEnv.ValueVariant      := QrFRFatCUFEnv.Value;
    EdCMunEnv.ValueVariant    := QrFRFatCCMunEnv.Value;
    CBCMunEnv.KeyValue        := QrFRFatCCMunEnv.Value;
    EdCaracAd.ValueVariant    := QrFRFatCCaracAd.Value;
    CBCaracAd.KeyValue        := QrFRFatCCaracAd.Value;
    EdCaracSer.ValueVariant   := QrFRFatCCaracSer.Value;
    CBCaracSer.KeyValue       := QrFRFatCCaracSer.Value;
    MexObs.Text               := QrFRFatCxObs.Value;
    EdTabelaPrc.ValueVariant  := QrFRFatCTabelaPrc.Value;
    CBTabelaPrc.KeyValue      := QrFRFatCTabelaPrc.Value;
    EdCondicaoPg.ValueVariant := QrFRFatCCondicaoPg.Value;
    CBCondicaoPg.KeyValue     := QrFRFatCCondicaoPg.Value;
    EdCartEmiss.ValueVariant  := QrFRFatCCartEmiss.Value;
    CBCartEmiss.KeyValue      := QrFRFatCCartEmiss.Value;
  end else
    if Avisa then
      Geral.MB_Aviso('Regra comercial n�o definida!');
end;

procedure TFmFrtFatCab.PreencheCamposFrtRegFisC(Avisa: Boolean);
var
  FrtRegFisC: Integer;
begin
  if FPreparandoEdicao then
    Exit;
  FrtRegFisC := EdFrtRegFisC.ValueVariant;
  if FrtRegFisC <> 0 then
  begin
    UnDmkdac_PF.AbreMySQLQuery0(QrFRFisC, Dmod.MyDB, [
    'SELECT * ',
    'FROM FrtRegFisC ',
    'WHERE Codigo=' + Geral.FF0(FrtRegFisC),
    '']);
    //
    RGTpCTe.ItemIndex                 := QrFRFisCTpCte.Value;
    EdNatOp.ValueVariant              := QrFRFisCNatOp.Value;
    EdCFOP.Text                       := QrFRFisCCFOP.Value;
    RGTpCTe.ItemIndex                 := QrFRFisCTpCTe.Value;
    RGMyCST.ItemIndex                 := QrFRFisCMyCST.Value;
    //
    EdICMS_pICMS.ValueVariant         := QrFRFisCICMS_pICMS.Value;
    EdICMS_pRedBC.ValueVariant        := QrFRFisCICMS_pRedBC.Value;
    EdICMS_pICMSSTRet.ValueVariant    := QrFRFisCICMS_pICMSSTRet.Value;
    EdICMS_pRedBCOutraUF.ValueVariant := QrFRFisCICMS_pRedBCOutraUF.Value;
    EdICMS_pICMSOutraUF.ValueVariant  := QrFRFisCICMS_pICMSOutraUF.Value;
    RGiTotTrib_Fonte.ItemIndex        := QrFRFisCiTotTrib_Fonte.Value;
    RGiTotTrib_Tabela.ItemIndex       := QrFRFisCiTotTrib_Tabela.Value;
    EdiTotTrib_Ex.ValueVariant        := QrFRFisCiTotTrib_Ex.Value;
    EdiTotTrib_Codigo.ValueVariant    := QrFRFisCiTotTrib_Codigo.Value;
    //
    if (QrParamsCTe.RecordCount = 1) and (EdParamsCte.ValueVariant = 0) then
      EdParamsCte.ValueVariant := QrParamsCTeControle.Value;
  end;
end;

procedure TFmFrtFatCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFrtFatCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFrtFatCab.DefParams;
begin
  VAR_GOTOTABELA := 'frtfatcab';
  VAR_GOTOMYSQLTABLE := QrFrtFatCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ffc.* ');
  VAR_SQLx.Add('FROM frtfatcab ffc');
  VAR_SQLx.Add('WHERE ffc.Codigo > 0');
  //
  VAR_SQL1.Add('AND ffc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND ffc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ffc.Nome Like :P0');
  //
end;

procedure TFmFrtFatCab.Desfazencerramento1Click(Sender: TObject);
//{$IfNDef SemNFe_0000}
var
  FatNum, Empresa: Integer;
begin
  FatNum  := QrFrtFatCabCodigo.Value;
  Empresa := QrFrtFatCabEmpresa.Value;
  if Frt_PF.DesfazEncerramentoFatura(VAR_FATID_5001, FatNum, Empresa) then
    LocCod(FatNum, FatNum);
(*
{$Else}
begin
  dmkPF.InfoSemModulo(TDmkModuloApp.mdlappNFe);
{$EndIf}
*)
end;

procedure TFmFrtFatCab.Duplicaconhecimento1Click(Sender: TObject);
const
  HabilitaForm = True;
  SerieDesfe   = 0;
  CTDesfeita   = 0;
  Status       = 0;
  DataFat      = '0000-00-00';
  Encerrou      = '0000-00-00';
var
  Controle, Codigo: Integer;
  Abertura: String;
begin
  Screen.Cursor := crHourGlass;
  // Evitar interfer�ncia do usu�rio nas grids (tabelas) durante o processo de duplica��o
  PnDados.Enabled := False;
  try
    //Result := False;
    //
    Codigo := UMyMod.BPGS1I32('frtfatcab', 'Codigo', '', '', tsPos, stIns, 0);
    Abertura := Geral.FDT(DModG.ObtemAgora, 109);
    //
    if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'frtfatcab', TMeuDB,
    ['Codigo'], [QrFrtFatCabCodigo.Value],
    ['Codigo', 'SerieDesfe', 'CTDesfeita',
    'Status', 'DataFat', 'Abertura',
    'Encerrou'],
    [Codigo, SerieDesfe, CTDesfeita,
    Status, DataFat, Abertura,
    Encerrou],
    '', True, LaAviso1, LaAviso2) then
    begin
      // FrtFatInfQ
      QrFrtFatInfQ.First;
      while not QrFrtFatInfQ.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('FrtFatInfQ', 'Controle', '', '',tsPos, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'FrtFatInfQ', TMeuDB,
        ['Controle'], [QrFrtFatInfQControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrFrtFatInfQ.Next;
      end;
      // FrtFatInfDoc
      QrFrtFatInfDoc.First;
      while not QrFrtFatInfDoc.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('FrtFatInfDoc', 'Controle', '', '',tsPos, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'FrtFatInfDoc', TMeuDB,
        ['Controle'], [QrFrtFatInfDocControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrFrtFatInfDoc.Next;
      end;
      // FrtFatPeri
      QrFrtFatPeri.First;
      while not QrFrtFatPeri.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('FrtFatPeri', 'Controle', '', '',tsPos, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'FrtFatPeri', TMeuDB,
        ['Controle'], [QrFrtFatPeriControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrFrtFatPeri.Next;
      end;
      // FrtFatInfCTeSub
      QrFrtFatInfCTeSub.First;
      while not QrFrtFatInfCTeSub.Eof do
      begin
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'FrtFatInfCTeSub', TMeuDB,
        ['Codigo'], [QrFrtFatInfCTeSubCodigo.Value],
        ['Codigo'], [Codigo], '', True, LaAviso1, LaAviso2);
        //
        QrFrtFatInfCTeSub.Next;
      end;
      // FrtFatInfCTeComp
      QrFrtFatInfCTeComp.First;
      while not QrFrtFatInfCTeComp.Eof do
      begin
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'FrtFatInfCTeComp', TMeuDB,
        ['Controle'], [QrFrtFatInfCTeCompCodigo.Value],
        ['Codigo'], [Codigo], '', True, LaAviso1, LaAviso2);
        //
        QrFrtFatInfCTeComp.Next;
      end;
      // FrtFatInfCTeAnu
      QrFrtFatInfCTeAnu.First;
      while not QrFrtFatInfCTeAnu.Eof do
      begin
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'FrtFatInfCTeAnu', TMeuDB,
        ['Controle'], [QrFrtFatInfCTeAnuCodigo.Value],
        ['Codigo'], [Codigo], '', True, LaAviso1, LaAviso2);
        //
        QrFrtFatInfCTeAnu.Next;
      end;
      // FrtFatAutXml
      QrFrtFatAutXml.First;
      while not QrFrtFatAutXml.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('FrtFatAutXml', 'Controle', '', '',tsPos, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'FrtFatAutXml', TMeuDB,
        ['Controle'], [QrFrtFatAutXmlControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrFrtFatAutXml.Next;
      end;
      // FrtFatSeg
      QrFrtFatSeg.First;
      while not QrFrtFatSeg.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('FrtFatSeg', 'Controle', '', '',tsPos, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'FrtFatSeg', TMeuDB,
        ['Controle'], [QrFrtFatSegControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrFrtFatSeg.Next;
      end;
      // FrtFatMoRodoLacr
      QrFrtFatMoRodoLacr.First;
      while not QrFrtFatMoRodoLacr.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('FrtFatMoRodoLacr', 'Controle', '', '',tsPos, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'FrtFatMoRodoLacr', TMeuDB,
        ['Controle'], [QrFrtFatMoRodoLacrControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrFrtFatMoRodoLacr.Next;
      end;
      // FrtFatMoRodoMoto
      QrFrtFatMoRodoMoto.First;
      while not QrFrtFatMoRodoMoto.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('FrtFatMoRodoMoto', 'Controle', '', '',tsPos, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'FrtFatMoRodoMoto', TMeuDB,
        ['Controle'], [QrFrtFatMoRodoMotoControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrFrtFatMoRodoMoto.Next;
      end;
      // FrtFatMoRodoVeic
      QrFrtFatMoRodoVeic.First;
      while not QrFrtFatMoRodoVeic.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('FrtFatMoRodoVeic', 'Controle', '', '',tsPos, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'FrtFatMoRodoVeic', TMeuDB,
        ['Controle'], [QrFrtFatMoRodoVeicControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrFrtFatMoRodoVeic.Next;
      end;
      // FrtFatMoRodoVPed
      QrFrtFatMoRodoVPed.First;
      while not QrFrtFatMoRodoVPed.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('FrtFatMoRodoVPed', 'Controle', '', '',tsPos, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'FrtFatMoRodoVPed', TMeuDB,
        ['Controle'], [QrFrtFatMoRodoVPedControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrFrtFatMoRodoVPed.Next;
      end;
      // FrtFatComp
      QrFrtFatComp.First;
      while not QrFrtFatComp.Eof do
      begin
        Controle  := UMyMod.BPGS1I32('FrtFatComp', 'Controle', '', '',tsPos, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'FrtFatComp', TMeuDB,
        ['Controle'], [QrFrtFatCompControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrFrtFatComp.Next;
      end;
      // FrtFOCAtrDef
      QrFrtFOCAtrDef.First;
      while not QrFrtFOCAtrDef.Eof do
      begin
        if QrFrtFOCAtrDefAtrTyp.Value = 1 then
        begin
          Controle  := UMyMod.BPGS1I32('frtfocatrdef', 'ID_Item', '', '',tsPos, stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'frtfocatrdef', TMeuDB,
          ['ID_Item'], [QrFrtFOCAtrDefID_Item.Value],
          ['ID_Sorc', 'ID_Item'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
          //
          QrFrtFOCAtrDef.Next;
        end else
        if QrFrtFOCAtrDefAtrTyp.Value = 2 then
        begin
          Controle  := UMyMod.BPGS1I32('frtfocatrtxt', 'ID_Item', '', '',tsPos, stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'frtfocatrtxt', TMeuDB,
          ['ID_Item'], [QrFrtFOCAtrDefID_Item.Value],
          ['ID_Sorc', 'ID_Item'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
          //
          QrFrtFOCAtrDef.Next;
        end else
          Geral.MB_Aviso('"FrtFOCAtrDefAtrTyp" indefinido!');
      end;
    end;
    //
    //Result := True;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    if Codigo <> 0 then
      LocCod(Codigo, Codigo);
    PnDados.Enabled := HabilitaForm;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFrtFatCab.EdDestinatarioRedefinido(Sender: TObject);
begin
  DefineTomadorDoServico();
end;

procedure TFmFrtFatCab.EdEmpresaRedefinido(Sender: TObject);
begin
  ReopenParamsCTe();
end;

procedure TFmFrtFatCab.EdExpedidorRedefinido(Sender: TObject);
begin
  DefineTomadorDoServico();
end;

procedure TFmFrtFatCab.EdFrtRegFatCRedefinido(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    PreencheCamposFrtRegFatC(False);
end;

procedure TFmFrtFatCab.EdFrtRegFisCRedefinido(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    PreencheCamposFrtRegFisC(False);
end;

procedure TFmFrtFatCab.EditainformedeDocumento1Click(Sender: TObject);
begin
  MostraFormFrtFatInfDoc(stUpd);
end;

procedure TFmFrtFatCab.Editainformedeseguro1Click(Sender: TObject);
begin
  MostraFormFrtFatSeg(stUpd);
end;

procedure TFmFrtFatCab.EdRecebedorRedefinido(Sender: TObject);
begin
  DefineTomadorDoServico();
end;

procedure TFmFrtFatCab.EdrefCTERedefinido(Sender: TObject);
begin
  if not XXe_PF.VerificaChaveAcessoXXe(EdrefCTe.Text, True) then
    EdrefCTe.SetFocus;
end;

procedure TFmFrtFatCab.EdRemetenteRedefinido(Sender: TObject);
begin
  DefineTomadorDoServico();
end;

procedure TFmFrtFatCab.EdToma4Redefinido(Sender: TObject);
begin
  DefineTomadorDoServico();
end;

procedure TFmFrtFatCab.EdvDescRedefinido(Sender: TObject);
begin
  CalculaValorLiq();
end;

procedure TFmFrtFatCab.EdvOrigRedefinido(Sender: TObject);
begin
  CalculaValorLiq();
end;

procedure TFmFrtFatCab.EdvRecRedefinido(Sender: TObject);
begin
  EdvOrig.ValueVariant := EdvRec.ValueVariant;
end;

procedure TFmFrtFatCab.EdvTPrestRedefinido(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (EdvRec.ValueVariant = 0) then
    EdvRec.ValueVariant := EdvTPrest.ValueVariant;
end;

procedure TFmFrtFatCab.Encerrafaturamento1Click(Sender: TObject);
var
  VersaoCTe, Empresa, SerieCTe, NumeroCTe, ParamsCTe, SerieDesfe,
  CTDesfeita, FatID, FatNum: Integer;
  vRec: Double;
  Tabela: String;
  Qry: TmySQLQuery;
begin
  if (TCTeModal(QrFrtFatCabModal.Value) = ctemodalRodoviario)
  and (QrFrtFatCabLota.Value = 1)
  and (QrFrtFatMoRodoVeic.RecordCount = 0) then
  begin
    Geral.MB_Aviso(
    'Dados dos ve�culos obrigat�rio em CT-e rodovi�rio de lota��o!');
    Exit;
  end;
  if (TCTeModal(QrFrtFatCabModal.Value) = ctemodalRodoviario)
  and (QrFrtFatSeg.RecordCount = 0) then
  begin
    Geral.MB_Aviso(
    'Dados de seguro obrigat�rio em CT-e rodovi�rio!');
    Exit;
  end;

  VersaoCTe    := CTe_PF.VersaoCTeEmUso();
  // Buscar valores se mudaram sem atualizar
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDb, [
    'SELECT * FROM frtfatcab ',
    'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
    '']);
    //
    SerieDesfe := Qry.FieldByName('SerieDesfe').AsInteger;
    CTDesfeita := Qry.FieldByName('CTDesfeita').AsInteger;
  finally
    Qry.Free;
  end;
  ParamsCTe  := QrFrtFatCabParamsCTe.Value;
  vRec       := QrFrtFatCabvRec.Value;
  FatID      := VAR_FATID_5001;
  FatNum     := QrFrtFatCabCodigo.Value;
  Empresa    := QrFrtFatCabEmpresa.Value;
  Tabela     := 'FrtFatCab';
  SerieCTe   := 0;
  NumeroCTe  := 0;
  //
  if not CTe_PF.Obtem_Serie_e_NumCT(SerieDesfe, CTDesfeita, (*SerieNormal,*)
  ParamsCTe, FatID, FatNum, Empresa(*, Filial, MaxSeq*), Tabela, SerieCTe, NumeroCTe) then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  case VersaoCTe of
    0200: CTe_PF.MostraFormCTeCTaEdit_0200a(QrFrtFatCabEmpresa.Value,
          QrFrtFatCabCodigo.Value,
          QrFrtFatCab, SerieCTe, NumeroCTe, vRec);
    else CTe_PF.AvisoNaoImplemVerCTe(VersaoCTe);
  end;
end;

procedure TFmFrtFatCab.ExcluiAtrAMovDef1Click(Sender: TObject);
var
  ID_Item: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do atributo do m�vel / autom�vel selecionado?',
  'frtfocatrdef', 'ID_Item', QrFrtFOCAtrDefID_Item.Value, Dmod.MyDB) = ID_YES then
  begin
    ID_Item := GOTOy.LocalizaPriorNextIntQr(QrFrtFOCAtrDef, QrFrtFOCAtrDefID_Item,
    QrFrtFOCAtrDefID_Item.Value);
    ReopenFrtFOCAtrDef(ID_Item);
  end;
end;

procedure TFmFrtFatCab.Excluicomponentedovalordofrete1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtFatComp', 'Controle', QrFrtFatCompControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtFatComp,
      QrFrtFatCompControle, QrFrtFatCompControle.Value);
    ReopenFrtFatComp(Controle);
  end;
end;

procedure TFmFrtFatCab.ExcluiLacre1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtFatMoRodoLacr', 'Controle', QrFrtFatMoRodoLacrControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtFatMoRodoLacr,
      QrFrtFatMoRodoLacrControle, QrFrtFatMoRodoLacrControle.Value);
    ReopenFrtFatMoRodoLacr(Controle);
  end;
end;

procedure TFmFrtFatCab.ExcluiMotorista1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtFatMoRodoMoto', 'Controle', QrFrtFatMoRodoMotoControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtFatMoRodoMoto,
      QrFrtFatMoRodoMotoControle, QrFrtFatMoRodoMotoControle.Value);
    ReopenFrtFatMoRodoMoto(Controle);
  end;
end;

procedure TFmFrtFatCab.Excluiprodutoperigoso1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtFatPeri', 'Controle', QrFrtFatPeriControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtFatPeri,
      QrFrtFatPeriControle, QrFrtFatPeriControle.Value);
    ReopenFrtFatPeri(Controle);
  end;
end;

procedure TFmFrtFatCab.ExcluiValePedgio1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtFatMoRodoVPed', 'Controle', QrFrtFatMoRodoVPedControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtFatMoRodoVPed,
      QrFrtFatMoRodoVPedControle, QrFrtFatMoRodoVPedControle.Value);
    ReopenFrtFatMoRodoVPed(Controle);
  end;
end;

procedure TFmFrtFatCab.ExcluiVeculo1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtFatMoRodoVeic', 'Controle', QrFrtFatMoRodoVeicControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtFatMoRodoVeic,
      QrFrtFatMoRodoVeicControle, QrFrtFatMoRodoVeicControle.Value);
    ReopenFrtFatMoRodoVeic(Controle);
  end;
end;

procedure TFmFrtFatCab.Inclui1Click(Sender: TObject);
begin
  MostraFormCTeIt2ObsCon(stIns);
end;

procedure TFmFrtFatCab.IncluiAtrAMovDef1Click(Sender: TObject);
begin
  MostraFormAtrAMovDef(stIns);
end;

procedure TFmFrtFatCab.Incluicomponentedovalordofrete1Click(Sender: TObject);
begin
  MostraFormFrtFatComp(stIns);
end;

procedure TFmFrtFatCab.IncluiLacre1Click(Sender: TObject);
begin
  MostraFormFrtFatMoRodoLacr(stIns);
end;

procedure TFmFrtFatCab.IncluiMotorista1Click(Sender: TObject);
begin
  MostraFormFrtFatMoRodoMoto(stIns);
end;

procedure TFmFrtFatCab.Incluiprodutoperigoso1Click(Sender: TObject);
begin
  MostraFormFrtFatPeri(stIns);
end;

procedure TFmFrtFatCab.IncluiValePedgio1Click(Sender: TObject);
begin
  MostraFormFrtFatMoRodoVPed(stIns);
end;

procedure TFmFrtFatCab.IncluiVeculo1Click(Sender: TObject);
begin
  MostraFormFrtFatMoRodoVeic(stIns);
end;

procedure TFmFrtFatCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormFrtFatInfQ(stUpd);
end;

procedure TFmFrtFatCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmFrtFatCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFrtFatCab.CTeComplementado1Click(Sender: TObject);
begin
  MostraFormFrtFatInfCTeComp();
end;

procedure TFmFrtFatCab.CTedeAnulacao1Click(Sender: TObject);
begin
  MostraFormFrtFatInfCTeAnu();
end;

procedure TFmFrtFatCab.CTeSubstitudo1Click(Sender: TObject);
begin
  MostraFormFrtFatInfCTeSub();
end;

procedure TFmFrtFatCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFrtFatCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtFatInfQ', 'Controle', QrFrtFatInfQControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtFatInfQ,
      QrFrtFatInfQControle, QrFrtFatInfQControle.Value);
    ReopenFrtFatInfQ(Controle);
  end;
end;

procedure TFmFrtFatCab.RemoveinformedeDocumento1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtFatInfDoc', 'Controle', QrFrtFatInfDocControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtFatInfDoc,
      QrFrtFatInfDocControle, QrFrtFatInfDocControle.Value);
    ReopenFrtFatInfDoc(Controle);
  end;
end;

procedure TFmFrtFatCab.Removeinformedeseguro1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtFatSeg', 'Controle', QrFrtFatSegControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtFatSeg,
      QrFrtFatSegControle, QrFrtFatSegControle.Value);
    ReopenFrtFatSeg(Controle);
  end;
end;

procedure TFmFrtFatCab.ReopenFrtFOCAtrDef(ID_Item: Integer);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrFrtFOCAtrDef, Dmod.MyDB, [
  'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ATRITS, ',
  'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,',
  'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp',
  'FROM frtfocatrdef def',
  'LEFT JOIN frtfocatrits its ON its.Controle=def.AtrIts ',
  'LEFT JOIN frtfocatrcad cad ON cad.Codigo=def.AtrCad ',
  'WHERE def.ID_Sorc=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  ' ',
  'UNION  ',
  ' ',
  'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, ',
  'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,',
  'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp ',
  'FROM frtfocatrtxt def ',
  'LEFT JOIN frtfocatrcad cad ON cad.Codigo=def.AtrCad ',
  'WHERE def.ID_Sorc=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  ' ',
  'ORDER BY NO_CAD, NO_ITS ',
  '']);
  QrFrtFOCAtrDef.Locate('ID_Item', ID_Item, []);
end;

(*
procedure TFmFrtFatCab.ReopenCTeIt2ObsCont_0000(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeIt2ObsCont_0000, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit2obscont ',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_5001),
  'AND FatNum=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  'AND Empresa=' + Geral.FF0(QrFrtFatCabEMpresa.Value),
  '']);
  if Controle <> 0 then
    QrCTeIt2ObsCont_0000.Locate('Controle', Controle, []);
end;
*)

procedure TFmFrtFatCab.ReopenFrtFatAutXml(Controle: Integer);
var
  ATT_AddForma: String;
begin
  ATT_AddForma := dmkPF.ArrayToTexto('cga.AddForma', 'NO_AddForma', pvPos, True,
    sNFeautXML);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatAutXml, Dmod.MyDB, [
  'SELECT cga.*, ',
  ATT_AddForma,
  'IF(cga.Tipo=0, "CNPJ", "CPF") TpDOC,',
  'CASE cga.AddForma ',
  '  WHEN 0 THEN "(N/I)"  ',
  '  WHEN 1 THEN eco.Nome ',
  '  WHEN 2 THEN IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ',
  '  WHEN 3 THEN " "  ',
  '  ELSE "???" END NO_ENT_ECO, ',
  'IF(cga.Tipo=0, cga.CNPJ, cga.CPF) CNPJ_CPF ',
  'FROM frtfatautxml cga ',
  'LEFT JOIN entidades ent ON ent.Codigo=cga.AddIDCad',
  'LEFT JOIN enticontat eco ON eco.Controle=cga.AddIDCad',
  'WHERE cga.Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
  //
  if Controle <> 0 then
    QrFrtFatAutXml.Locate('Controle', Controle, []);
end;

procedure TFmFrtFatCab.ReopenFrtFatComp(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatComp, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatcomp ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
  if Controle <> 0 then
    QrFrtFatComp.Locate('Controle', Controle, []);
end;

procedure TFmFrtFatCab.ReopenCTeCabA();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeCabA, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctecaba ',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_5001),
  'AND FatNum=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  'AND Empresa=' + Geral.FF0(QrFrtFatCabEmpresa.Value),
  '']);
end;

procedure TFmFrtFatCab.ReopenEntiAll(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT ent.Codigo, ent.CodUsu, ent.IE, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, ',
  'mun.Nome CIDADE, ',
  'ufs.Nome NOMEUF, ent.indIEDest, ent.Tipo  ',
  'FROM entidades ent ',
  'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo ',
  ' = IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) ',
  'ORDER BY NOMEENT ',
  '']);
end;

procedure TFmFrtFatCab.ReopenFrtFatInfCTeAnu();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatInfCTeAnu, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatinfcteanu ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
  //
  //QrFrtFatInfCTeAnu.Locate('Controle', Controle, []);
end;

procedure TFmFrtFatCab.ReopenFrtFatInfCTeComp();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatInfCTeComp, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatinfctecomp ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
  //
  //QrFrtFatInfCTeComp.Locate('Controle', Controle, []);
end;

procedure TFmFrtFatCab.ReopenFrtFatInfCTeSub();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatInfCTeSub, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatinfctesub ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
  //
  //QrFrtFatInfDoc.Locate('Controle', Controle, []);
end;

procedure TFmFrtFatCab.ReopenFrtFatInfDoc(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatInfDoc, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatinfdoc ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
  //
  QrFrtFatInfDoc.Locate('Controle', Controle, []);
end;

procedure TFmFrtFatCab.ReopenFrtFatInfQ(Controle: Integer);
var
  ATT_CTeCUnid, ATT_CTeModal, ATT_CTeTpServ, ATT_CTeToma: String;
begin
  ATT_CTeCUnid := dmkPF.ArrayToTexto('fiq.CUnid', 'NO_CUnid', pvPos, True,
    sCTeCUnid);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatInfQ, Dmod.MyDB, [
  'SELECT ',
  ATT_CTeCUnid,
  'mtm.Nome NO_MyTpMed, ',
  'fiq.* ',
  'FROM frtfatinfq fiq',
  'LEFT JOIN trnsmytpmed mtm ON mtm.Codigo=fiq.MyTpMed ',
  'WHERE fiq.Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
  //
  QrFrtFatInfQ.Locate('Controle', Controle, []);
end;


procedure TFmFrtFatCab.ReopenFrtFatMoRodoLacr(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatMoRodoLacr, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatmorodolacr ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
  //
  QrFrtFatMoRodoLacr.Locate('Controle', Controle, []);
end;

procedure TFmFrtFatCab.ReopenFrtFatMoRodoMoto(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatMoRodoMoto, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatmorodomoto ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
  //
  QrFrtFatMoRodoMoto.Locate('Controle', Controle, []);
end;

procedure TFmFrtFatCab.ReopenFrtFatMoRodoVeic(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatMoRodoVeic, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatmorodoveic ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
  //
  QrFrtFatMoRodoVeic.Locate('Controle', Controle, []);
end;

procedure TFmFrtFatCab.ReopenFrtFatMoRodoVPed(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatMoRodoVPed, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatmorodovped ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
  //
  QrFrtFatMoRodoVPed.Locate('Controle', Controle, []);
end;

procedure TFmFrtFatCab.ReopenFrtFatPeri(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatPeri, Dmod.MyDB, [
  'SELECT ffp.Codigo, ffp.Controle, ffp.ProdutCad,  ',
  'ffp.qTotProd, ffp.qVolTipo,   ',
  'prc.Nome, prc.nONU, prc.XNomeAE, ',
  'prc.XClaRisco, prc.GrEmb PERI_grEmb, prc.PontoFulgor ',
  'FROM frtfatperi ffp ',
  'LEFT JOIN produtcad prc ON prc.Codigo=ffp.ProdutCad ',
  'WHERE ffp.Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  'ORDER BY ffp.Controle ',
  '']);
  //
  QrFrtFatPeri.Locate('Controle', Controle, []);
end;

procedure TFmFrtFatCab.ReopenFrtFatSeg(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatSeg, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatseg ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
  //
  QrFrtFatSeg.Locate('Controle', Controle, []);
end;

procedure TFmFrtFatCab.ReopenParamsCTe();
var
  Empresa: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  if Empresa <> 0 then
  begin
    Empresa := DModG.QrEmpresasCodigo.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrParamsCTe, Dmod.MyDB, [
    'SELECT Controle, CONCAT("S�rie ", SerieCTe) NOME',
    'FROM paramscte ',
    'WHERE Codigo=' + Geral.FF0(Empresa),
    'AND MaxSeqLib > IncSeqAuto',
    'ORDER BY NOME',
    '']);
  end else
    QrParamsCTe.Close;
end;

procedure TFmFrtFatCab.RGiTotTrib_FonteClick(Sender: TObject);
begin
  PnIBPTax.Visible := RGiTotTrib_Fonte.ItemIndex = 1;
end;

procedure TFmFrtFatCab.RGTomaClick(Sender: TObject);
begin
  DefineTomadorDoServico();
end;

procedure TFmFrtFatCab.DefineONomeDoForm;
begin
end;

procedure TFmFrtFatCab.DefineTomadorDoServico();
var
  Toma: Integer;
begin
  if FPreparandoEdicao then Exit;
  //
  case RGToma.ItemIndex of
    0: Toma := EdRemetente.ValueVariant;
    1: Toma := EdExpedidor.ValueVariant;
    2: Toma := EdRecebedor.ValueVariant;
    3: Toma := EdDestinatario.ValueVariant;
    4: Toma := EdToma4.ValueVariant;
  end;
  EdTomador.ValueVariant := Toma;
  CBTomador.KeyValue     := Toma;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFrtFatCab.SpeedButton13Click(Sender: TObject);
begin
  FinanceiroJan.CadastroESelecaoDeCarteira(QrCartEmiss, EdCartEmiss, CBCartEmiss);
end;

procedure TFmFrtFatCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFrtFatCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFrtFatCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFrtFatCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFrtFatCab.SbFrtRegFisCClick(Sender: TObject);
begin
  Frt_PF.CadastroESelecaoDeFrtRegFisC(QrFrtRegFisC, EdFrtRegFisC, CBFrtRegFisC);
end;

procedure TFmFrtFatCab.SpeedButton6Click(Sender: TObject);
begin
  PreencheCamposFrtRegFatC(True);
end;

procedure TFmFrtFatCab.BtSegClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSeg, BtSeg);
end;

procedure TFmFrtFatCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtFatCab.BtTabelaPrcClick(Sender: TObject);
begin
  Frt_PF.CadastroESelecaoDeFrtPrcCad(QrFrtPrcCad, EdTabelaPrc, CBTabelaPrc);
end;

procedure TFmFrtFatCab.BtVeicClick(Sender: TObject);
begin
  if (TCTeModal(QrFrtFatCabModal.Value) = ctemodalRodoviario)
  and (QrFrtFatCabLota.Value = 0) then
  begin
    Geral.MB_Aviso(
    'Dados dos ve�culos s� preenchidos em CT-e rodovi�rio de lota��o!');
    Exit;
  end;
  MyObjects.MostraPopUpDeBotao(PMVeic, BtVeic);
end;

procedure TFmFrtFatCab.BtVPedClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMVPed, BtVPed);
end;

procedure TFmFrtFatCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFrtFatCabCodigo.Value;
  Close;
end;

procedure TFmFrtFatCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormFrtFatInfQ(stIns);
end;

procedure TFmFrtFatCab.CabAltera1Click(Sender: TObject);
var
  Filial: Integer;
begin
  FPreparandoEdicao := True;
  FTomadorAtual := QrFrtFatCabTomador.Value;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFrtFatCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'frtfatcab');
  //
  Filial := DModG.ObtemFilialDeEntidade(QrFrtFatCabEmpresa.Value);
  EdEmpresa.ValueVariant := Filial;
  CBEmpresa.KeyValue     := Filial;
  //
  FPreparandoEdicao := False;
end;

procedure TFmFrtFatCab.BtCompClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMComp, BtComp);
end;

procedure TFmFrtFatCab.BtCondicaoPGClick(Sender: TObject);
begin
  Praz_PF.CadastroESelecaoDePediPrzCab1(QrPediPrzCab, EdCondicaoPg, CBCondicaoPg);
end;

procedure TFmFrtFatCab.BtConfirmaClick(Sender: TObject);
var
  UFEnv, UFIni, UFFim, xObs, Abertura, refCTE, xOutCat, DataPrev,
  XDetRetira, Nome: String;
  Codigo, FrtRegFisC, Tomador, Remetente, Expedidor, Recebedor, Destinatario,
  Toma4, TabelaPrc, CondicaoPg, CartEmiss, FrtRegFatC, ForPag, Modal, TpServ,
  CMunEnv, CMunIni, CMunFim, Toma, CaracAd, CaracSer, Retira, Empresa, EntiEmi,
  LocEnt, FrtProPred, Filial, ParamsCTe, Lota, LocColeta: Integer;
  vCarga, vTPrest, vRec, vOrig, vDesc, vLiq: Double;
  CIOT: Int64;
  SQLType: TSQLType;
//var frtregfisc
  NatOp, CFOP: String;
  TpCTe, MyCST, iTotTrib_Fonte, iTotTrib_Tabela, iTotTrib_Ex: Integer;
  ICMS_pICMS, ICMS_pRedBC, ICMS_pICMSSTRet, ICMS_pRedBCOutraUF, ICMS_pICMSOutraUF, iTotTrib_Codigo: Double;
begin
  Nome           := EdNome.Text;
  Filial         := EdEmpresa.ValueVariant;
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);;
  FrtRegFisC     := EdFrtRegFisC.ValueVariant;
  Tomador        := EdTomador.ValueVariant;
  Remetente      := EdRemetente.ValueVariant;        // Nao obrigatorio
  Expedidor      := EdExpedidor.ValueVariant;        // Nao obrigatorio
  Recebedor      := EdRecebedor.ValueVariant;        // Nao obrigatorio
  Destinatario   := EdDestinatario.ValueVariant;     // Nao obrigatorio
  Toma4          := EdToma4.ValueVariant;
  //
  FrtRegFatC     := EdFrtRegFatC.ValueVariant;
  TabelaPrc      := EdTabelaPrc .ValueVariant;
  CondicaoPg     := EdCondicaoPg.ValueVariant;
  CartEmiss      := EdCartEmiss.ValueVariant;
  ForPag         := RGForPag.ItemIndex;
  Modal          := RGModal.ItemIndex;
  TpServ         := RGTpServ.ItemIndex;
  CMunIni        := EdCMunIni.ValueVariant;
  UFIni          := EdUFIni.ValueVariant;
  CMunFim        := EdCMunFim.ValueVariant;
  UFFim          := EdUFFim.ValueVariant;
  CMunEnv        := EdCMunEnv.ValueVariant;
  UFEnv          := EdUFEnv.ValueVariant;
  Toma           := RGToma.ItemIndex;
  CaracAd        := EdCaracAd.ValueVariant;
  CaracSer       := EdCaracSer.ValueVariant;
  xObs           := MexObs.Text;
  //
  Retira         := Geral.BoolToInt(CkRetira.Checked);
  vCarga         := EdvCarga.ValueVariant;
  //
  vTPrest        := EdvTPrest.ValueVariant;
  vRec           := EdvRec.ValueVariant;
  //
  vOrig          := EdvOrig.ValueVariant;
  vDesc          := EdvDesc.ValueVariant;
  vLiq           := EdvLiq.ValueVariant;
  //
  refCTE         := EdrefCTE.Text;
  EntiEmi        := EdEntiEmi.ValueVariant;
  LocEnt         := EdLocEnt.ValueVariant;
  FrtProPred     := EdFrtProPred.ValueVariant;
  xOutCat        := EdxOutCat.Text;
  //
  ParamsCTe      := EdParamsCTe.ValueVariant;
  //
  DataPrev       := Geral.FDT(Int(TPDataPrev.Date) + EdDataPrev.ValueVariant, 109);
  Lota           := Geral.BoolToInt(CkLota.Checked);
  CIOT           := EdCIOT.ValueVariant;
  XDetRetira     := MeXDetRetira.Text;
  LocColeta      := EdLocColeta.ValueVariant;
  // frtregfisc
  NatOp              := EdNatOp.ValueVariant;
  if CBCFOP.KeyValue = Null then
    CFOP             := ''
  else
    CFOP             := EdCFOP.Text;
  TpCTe              := RGTpCTe.ItemIndex;
  MyCST              := RGMyCST.ItemIndex;
  //
  ICMS_pICMS         := EdICMS_pICMS.ValueVariant;
  ICMS_pRedBC        := EdICMS_pRedBC.ValueVariant;
  ICMS_pICMSSTRet    := EdICMS_pICMSSTRet.ValueVariant;
  ICMS_pRedBCOutraUF := EdICMS_pRedBCOutraUF.ValueVariant;
  ICMS_pICMSOutraUF  := EdICMS_pICMSOutraUF.ValueVariant;
  iTotTrib_Fonte     := RGiTotTrib_Fonte.ItemIndex;
  iTotTrib_Tabela    := RGiTotTrib_Tabela.ItemIndex;
  iTotTrib_Ex        := EdiTotTrib_Ex.ValueVariant;
  iTotTrib_Codigo    := EdiTotTrib_Codigo.ValueVariant;
  //
  //if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Trim(CFOP) = '', EdCFOP, 'Informe o CFOP!') then Exit;
  if MyObjects.FIC(MyCST < 0, RGMyCST, 'Defina o CST!') then Exit;
  //
  //
  if SQLType = stIns then
    Abertura := Geral.FDT(DModG.ObtemAgora, 109)
  else
    Abertura := Geral.FDT(QrFrtFatCabAbertura.Value, 109);
  //
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Informe a Empresa!') then Exit;
  Empresa := DmodG.ObtemEntidadeDeFilial(Filial);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a Empresa!') then Exit;
  //
  if MyObjects.FIC(FrtRegFisC = 0, EdFrtRegFisC, 'Informe a regra fiscal!') then Exit;

  if MyObjects.FIC((Toma = 0) and (Remetente = 0), EdRemetente, 'Informe o Remetente!') then Exit;
  if MyObjects.FIC((Toma = 1) and (Expedidor = 0), EdExpedidor, 'Informe o Expedidor!') then Exit;
  if MyObjects.FIC((Toma = 2) and (Recebedor = 0), EdRecebedor, 'Informe o Recebedor!') then Exit;
  if MyObjects.FIC((Toma = 3) and (Destinatario = 0), EdDestinatario, 'Informe o Destinat�rio!') then Exit;
  if MyObjects.FIC((Toma = 4) and (Toma4 = 0), EdToma4, 'Informe o "Outro (tomador)"!') then Exit;
  if MyObjects.FIC(Toma < 0, RGToma, '"Tomador do servi�o" n�o definido !') then Exit;
  if MyObjects.FIC(Tomador = 0, nil, '"Tomador" indefinido. AVISE A DERMATEK!') then Exit;

  if MyObjects.FIC(vCarga = 0, EdTabelaPrc, 'Valor da carga n�o definido!') then ;//Exit;

  if MyObjects.FIC(ForPag < 0, RGForPag, 'Informe a forma de pagamento do servi�o!') then Exit;
  if MyObjects.FIC(Modal=0, RGModal, 'Defina o Modal!') then Exit;
  if MyObjects.FIC(Modal<>1, RGModal, 'Modal n�o implementado! SOLICITE � DERMATEK') then Exit;
  if MyObjects.FIC(TpServ < 0, RGTpServ, 'Informe o Tipo de Servi�o!') then Exit;

  if MyObjects.FIC(TabelaPrc = 0, EdTabelaPrc, 'Informe a tabela de pre�os!') then Exit;
  if MyObjects.FIC(CondicaoPg = 0, EdCondicaoPg, 'Informe a Condi��o de Pagamento!') then Exit;
  if MyObjects.FIC(CartEmiss = 0, EdCartEmiss, 'Informe a Carteira!') then Exit;
  //if MyObjects.FIC(FrtRegFatC = 0, EdFrtRegFatC, 'Informe a Regra Comercial Base!') then Exit;
  if MyObjects.FIC(CMunEnv = 0, EdCMunEnv, 'Informe o munic�pio de envio!') then Exit;
  if MyObjects.FIC(Trim(UFEnv) = '', EdUFEnv, 'Informe a UF de envio!') then Exit;
  if MyObjects.FIC(CMunIni = 0, EdCMunIni, 'Informe o munic�pio de In�cio!') then Exit;
  if MyObjects.FIC(Trim(UFIni) = '', EdUFIni, 'Informe a UF de In�cio!') then Exit;
  if MyObjects.FIC(CMunFim = 0, EdCMunFim, 'Informe o munic�pio Final!') then Exit;
  if MyObjects.FIC(Trim(UFFim) = '', EdUFFim, 'Informe a UF de Final!') then Exit;
  if MyObjects.FIC(FrtProPred = 0, EdFrtProPred, 'Informe o produto predominante!') then Exit;
  if MyObjects.FIC(ParamsCTe = 0, EdParamsCTe, 'Informe a s�rie do CT-e!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('frtfatcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtfatcab', False, [
  'FrtRegFisC', 'Tomador', 'Remetente',
  'Expedidor', 'Recebedor', 'Destinatario',
  'Toma4', 'TabelaPrc', 'CondicaoPg',
  'CartEmiss', 'FrtRegFatC', 'ForPag',
  'Modal', 'TpServ', 'CMunEnv',
  'UFEnv', 'CMunIni', 'UFIni',
  'CMunFim', 'UFFim', 'Toma',
  'CaracAd', 'CaracSer', 'xObs',
  'Retira', 'vCarga', 'Abertura',
  'vTPrest', 'vRec', 'Empresa',
  'refCTE', 'EntiEmi', 'LocEnt',
  'FrtProPred', 'xOutCat', 'ParamsCTe',
  'vOrig', 'vDesc', 'vLiq',
  'DataPrev', 'Lota', 'CIOT',
  'XDetRetira', 'LocColeta', 'Nome',
  // frtregfisc
  'NatOp', 'CFOP',
  'TpCTe', 'MyCST', 'ICMS_pICMS',
  'ICMS_pRedBC', 'ICMS_pICMSSTRet', 'ICMS_pRedBCOutraUF',
  'ICMS_pICMSOutraUF', 'iTotTrib_Fonte', 'iTotTrib_Tabela',
  'iTotTrib_Ex', 'iTotTrib_Codigo'], [
  'Codigo'], [
  FrtRegFisC, Tomador, Remetente,
  Expedidor, Recebedor, Destinatario,
  Toma4, TabelaPrc, CondicaoPg,
  CartEmiss, FrtRegFatC, ForPag,
  Modal, TpServ, CMunEnv,
  UFEnv, CMunIni, UFIni,
  CMunFim, UFFim, Toma,
  CaracAd, CaracSer, xObs,
  Retira, vCarga, Abertura,
  vTPrest, vRec, Empresa,
  refCTE, EntiEmi, LocEnt,
  FrtProPred, xOutCat, ParamsCTe,
  vOrig, vDesc, vLiq,
  DataPrev, Lota, CIOT,
  XDetRetira, LocColeta, Nome,
  // frtregfisc
  NatOp, CFOP,
  TpCTe, MyCST, ICMS_pICMS,
  ICMS_pRedBC, ICMS_pICMSSTRet, ICMS_pRedBCOutraUF,
  ICMS_pICMSOutraUF, iTotTrib_Fonte, iTotTrib_Tabela,
  iTotTrib_Ex, iTotTrib_Codigo], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //
    AcaoPosteriorTpCTe();
    if FSeq = 1 then Close;
  end;
end;

procedure TFmFrtFatCab.BtFrtFOCAtrDefClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFrtFocAtrDef, BtFrtFOCAtrDef);
(*
  fazer outra tabela e nao cteit2obscont!
  MyObjects.MostraPopUpDeBotao(PMCTeIt2ObsCont, BtCTeIt2ObsCont);
*)
end;

procedure TFmFrtFatCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'frtfatcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'frtfatcab', 'Codigo');
end;

procedure TFmFrtFatCab.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmFrtFatCab.BtInfQClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInfQ, BtInfQ);
end;

procedure TFmFrtFatCab.BtiTotTribClick(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmIBPTaxCad, FmIBPTaxCad, afmoLiberado) then
  begin
    FmIBPTaxCad.ShowModal;
    if VAR_CADASTRO <> 0 then
    begin
      RGiTotTrib_Tabela.ItemIndex    := FmIBPTaxCad.QrIBPTaxTabela.Value;
      EdiTotTrib_Ex.ValueVariant     := FmIBPTaxCad.QrIBPTaxEx.Value;
      EdiTotTrib_Codigo.ValueVariant := FmIBPTaxCad.QrIBPTaxCodigo.Value;
    end;
    FmIBPTaxCad.Destroy;
  end;
{$EndIf}
end;

procedure TFmFrtFatCab.BtLacrClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLacr, BtLacr);
end;

procedure TFmFrtFatCab.BtMotoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMoto, BtMoto);
end;

procedure TFmFrtFatCab.BtPeriClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPeri, BtPeri);
end;

procedure TFmFrtFatCab.AcaoPosteriorTpCTe();
var
  CodTxt: String;
begin
  CodTxt := Geral.FF0(QrFrtFatCabCodigo.Value);
  if TCTeTpCTe(QrFrtFatCabTpCTe.Value) <> ctetpcteSubstituto then
    Dmod.MyDB.Execute('DELETE FROM frtfatinfctesub WHERE Codigo=' + CodTxt);
  if TCTeTpCTe(QrFrtFatCabTpCTe.Value) <> ctetpcteComplValrs then
    Dmod.MyDB.Execute('DELETE FROM frtfatinfctecomp WHERE Codigo=' + CodTxt);
  if TCTeTpCTe(QrFrtFatCabTpCTe.Value) <> ctetpcteAnulacao then
    Dmod.MyDB.Execute('DELETE FROM frtfatinfcteanu WHERE Codigo=' + CodTxt);
  //
  case TCTeTpCTe(QrFrtFatCabTpCTe.Value) of
    ctetpcteSubstituto: MostraFormFrtFatInfCTeSub();
    ctetpcteComplValrs: MostraFormFrtFatInfCTeComp();
    ctetpcteAnulacao: MostraFormFrtFatInfCTeAnu();
  end;
  if FTomadorAtual <> QrFrtFatCabTomador.Value then
  begin
    FTomadorAtual := 0;
    //excluir emails AddForma=1
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE  ',
    'FROM frtfatautxml ',
(*
    'WHERE FatID=' + Geral.FF0(VAR_FATID_5001),
    'AND FatNum=' + Geral.FF0(QrFrtFatCabCodigo.Value),
    'AND Empresa=' + Geral.FF0(QrFrtFatCabEmpresa.Value),
*)
    'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
    //
    'AND AddForma=' + Geral.FF0(Integer(TNFeautXML.naxEntiContat)),
    '']);
    //mostrar janela AutXml
    MostraFormFrtFatAutXml();
  end;
end;

procedure TFmFrtFatCab.AdicionainformedeDocumento1Click(Sender: TObject);
begin
  MostraFormFrtFatInfDoc(stIns);
end;

procedure TFmFrtFatCab.Adicionainformedeseguro1Click(Sender: TObject);
begin
  MostraFormFrtFatSeg(stIns);
end;

procedure TFmFrtFatCab.BtInfDocClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInfDoc, BtInfDoc);
end;

procedure TFmFrtFatCab.Altera1Click(Sender: TObject);
begin
  MostraFormCTeIt2ObsCon(stUpd);
end;

procedure TFmFrtFatCab.AlteraAtrAMovDef1Click(Sender: TObject);
begin
  MostraFormAtrAMovDef(stUpd);
end;

procedure TFmFrtFatCab.Alteracomponentedovalordofrete1Click(Sender: TObject);
begin
  MostraFormFrtFatComp(stUpd);
end;

procedure TFmFrtFatCab.AlteraLacre1Click(Sender: TObject);
begin
  MostraFormFrtFatMoRodoLacr(stUpd);
end;

procedure TFmFrtFatCab.AlteraMotorista1Click(Sender: TObject);
begin
  MostraFormFrtFatMoRodoMoto(stUpd);
end;

procedure TFmFrtFatCab.Alteraprodutoperigoso1Click(Sender: TObject);
begin
  MostraFormFrtFatPeri(stUpd);
end;

procedure TFmFrtFatCab.AlteraValePedgio1Click(Sender: TObject);
begin
  MostraFormFrtFatMoRodoVPed(stUpd);
end;

procedure TFmFrtFatCab.AlteraVeculo1Click(Sender: TObject);
begin
  MostraFormFrtFatMoRodoVeic(stUpd);
end;

procedure TFmFrtFatCab.BtAutXMLClick(Sender: TObject);
begin
  MostraFormFrtFatAutXml();
end;

procedure TFmFrtFatCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmFrtFatCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FBtCab := True;
  //GBEdita.Align := alClient;
  FPreparandoEdicao := False;
  DGInfNFe.Align := alClient;
  PageControl1.Align := alClient;
  Panel6.BevelOuter := bvNone;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  UnDmkDAC_PF.AbreQuery(QrMunIni, DmodG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrMunFim, DmodG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrMunEnv, DmodG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrFrtRegFisC, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFrtRegFatC, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCFOP, Dmod.MyDB);
  ReopenEntiAll(QrTomadores);
  ReopenEntiAll(QrRemetentes);
  ReopenEntiAll(QrExpedidores);
  ReopenEntiAll(QrRecebedores);
  ReopenEntiAll(QrDestinatarios);
  ReopenEntiAll(QrToma4s);
  ReopenEntiAll(QrEntiEmi);
  ReopenEntiAll(QrLocColeta);
  ReopenEntiAll(QrLocEnt);
  UnDmkDAC_PF.AbreQuery(QrCaracAd, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCaracSer, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCartEmiss, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFrtProPred, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzCab, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome, MaxDesco, ',
  'JurosMes, Parcelas, Percent1, Percent2 ',
  'FROM pediprzcab ',
  'WHERE Aplicacao & ' + Geral.FF0(CO_APLIC_COND_PAG_FRETE) + ' <> 0 ',
  'ORDER BY Nome ',
  '']);
  UnDmkDAC_PF.AbreQuery(QrFrtPrcCad, Dmod.MyDB);
end;

procedure TFmFrtFatCab.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNumero, SbNumero);
end;

procedure TFmFrtFatCab.SbCaracAdClick(Sender: TObject);
begin
  Frt_PF.CadastroESelecaoDeFrtCaracAd(QrCaracAd, EdCaracAd, CBCaracAd);
end;

procedure TFmFrtFatCab.SbCaracSerClick(Sender: TObject);
begin
  Frt_PF.CadastroESelecaoDeFrtCaracAd(QrCaracAd, EdCaracAd, CBCaracAd);
end;

procedure TFmFrtFatCab.SbDestinatarioClick(Sender: TObject);
begin
  Entities.CadastroESelecaoDeEntidade2(QrDestinatarios, EdDestinatario, CBDestinatario);
end;

procedure TFmFrtFatCab.SbExpedidorClick(Sender: TObject);
begin
  Entities.CadastroESelecaoDeEntidade2(QrExpedidores, EdExpedidor, CBExpedidor);
end;

procedure TFmFrtFatCab.SbFrtProPredClick(Sender: TObject);
begin
  Frt_PF.CadastroESelecaoDeFrtProPred(QrFrtProPred, EdFrtProPred, CBFrtProPred);
end;

procedure TFmFrtFatCab.SbFrtRegFatCClick(Sender: TObject);
begin
  Frt_PF.CadastroESelecaoDeFrtRegFatC(QrFrtRegFatC, EdFrtRegFatC, CBFrtRegFatC);
end;

procedure TFmFrtFatCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFrtFatCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFrtFatCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFrtFatCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFrtFatCab.QrCTeCabAAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := XXe_PF.PerrmiteDesencerrarXXe(QrCTeCabAStatus.Value);
  //
  HabilitaBotoes(Habilita);
end;

procedure TFmFrtFatCab.QrFrtFatCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFrtFatCab.QrFrtFatCabAfterScroll(DataSet: TDataSet);
begin
  ReopenCTeCabA();
  //
  ReopenFrtFatInfQ(0);
  ReopenFrtFatInfDoc(0);
  ReopenFrtFOCAtrDef(0);
  ReopenFrtFatPeri(0);
  ReopenFrtFatInfCTeSub();
  ReopenFrtFatInfCTeComp();
  ReopenFrtFatInfCTeAnu();
  ReopenFrtFatAutXml(0);
  ReopenFrtFatSeg(0);
  ReopenFrtFatMoRodoLacr(0);
  ReopenFrtFatMoRodoMoto(0);
  ReopenFrtFatMoRodoVeic(0);
  ReopenFrtFatMoRodoVPed(0);
  ReopenFrtFatComp(0);
end;

procedure TFmFrtFatCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrFrtFatCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmFrtFatCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFrtFatCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'frtfatcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFrtFatCab.SbRecebedorClick(Sender: TObject);
begin
  Entities.CadastroESelecaoDeEntidade2(QrRecebedores, EdRecebedor, CBRecebedor);
end;

procedure TFmFrtFatCab.SbRemetenteClick(Sender: TObject);
begin
  Entities.CadastroESelecaoDeEntidade2(QrRemetentes, EdRemetente, CBRemetente);
end;

procedure TFmFrtFatCab.SbToma4Click(Sender: TObject);
begin
  Entities.CadastroESelecaoDeEntidade2(QrToma4s, EdToma4, CBToma4);
end;

procedure TFmFrtFatCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtFatCab.HabilitaBotoes(Habilita: Boolean);
begin
  FBtCab                 := Habilita;
  BtSeg.Enabled          := Habilita;
  BtInfQ.Enabled         := Habilita;
  BtComp.Enabled         := Habilita;
  BtVPed.Enabled         := Habilita;
  BtInfDoc.Enabled       := Habilita;
  BtFrtFOCAtrDef.Enabled := Habilita;
  BtVeic.Enabled         := Habilita;
  BtLacr.Enabled         := Habilita;
  BtPeri.Enabled         := Habilita;
  BtAutXML.Enabled       := Habilita;
  BtMoto.Enabled         := Habilita;
  BtEncerra.Enabled      := Habilita;
end;

procedure TFmFrtFatCab.CabInclui1Click(Sender: TObject);
begin
  FTomadorAtual := 0;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFrtFatCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'frtfatcab');
end;

procedure TFmFrtFatCab.CalculaValorLiq();
begin
  EdvLiq.ValueVariant := EdvOrig.ValueVariant - EdvDesc.ValueVariant;
end;

procedure TFmFrtFatCab.CdigodoFaturamento1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFrtFatCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFrtFatCab.QrFrtFatCabBeforeClose(
  DataSet: TDataSet);
begin
  QrCTeCabA.Close;
  //
  QrFrtFatInfQ.Close;
  QrFrtFatInfDoc.Close;
  QrFrtFOCAtrDef.Close;
  QrFrtFatPeri.Close;
  QrFrtFatInfCTeSub.Close;
  QrFrtFatInfCTeComp.Close;
  QrFrtFatInfCTeAnu.Close;
  QrFrtFatAutXml.Close;
  QrFrtFatSeg.Close;
  QrFrtFatMoRodoLacr.Close;
  QrFrtFatMoRodoMoto.Close;
  QrFrtFatMoRodoVeic.Close;
  QrFrtFatMoRodoVPed.Close;
  QrFrtFatComp.Close;
  //
  HabilitaBotoes(False);
end;

procedure TFmFrtFatCab.QrFrtFatCabBeforeOpen(DataSet: TDataSet);
begin
  QrFrtFatCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

