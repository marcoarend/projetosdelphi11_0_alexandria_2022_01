object FmFrtMnfAutXml: TFmFrtMnfAutXml
  Left = 339
  Top = 185
  Caption = 'MNF-CARGA-005 :: Manifesto - Autorizados XML'
  ClientHeight = 442
  ClientWidth = 655
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 655
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 607
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbEntidade: TBitBtn
        Tag = 101
        Left = 5
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbEntidadeClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 559
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 348
        Height = 32
        Caption = 'Manifesto - Autorizados XML'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 348
        Height = 32
        Caption = 'Manifesto - Autorizados XML'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 348
        Height = 32
        Caption = 'Manifesto - Autorizados XML'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 92
    Width = 655
    Height = 217
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 147
      Width = 655
      Height = 70
      Align = alBottom
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 509
        Top = 15
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 13
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 507
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtInclui: TBitBtn
          Tag = 10
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 136
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 260
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 655
      Height = 147
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 655
        Height = 147
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object DBGrid1: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 655
          Height = 147
          Align = alClient
          DataSource = DsFrtMnfAutXml
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'ITEM'
              ReadOnly = True
              Title.Caption = 'Item'
              Width = 27
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TpDOC'
              Title.Caption = 'Tipo'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJ_CPF'
              Title.Caption = 'CNPJ / CPF'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_AddForma'
              Title.Caption = 'Forma adi'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ENT_ECO'
              Title.Caption = 'Nome entidade / contato'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 655
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 651
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 309
    Width = 655
    Height = 133
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 655
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 99
        Height = 44
        Align = alLeft
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label7: TLabel
          Left = 4
          Top = 16
          Width = 23
          Height = 13
          Caption = 'Item:'
          Color = clBtnFace
          ParentColor = False
        end
        object EdControle: TdmkEdit
          Left = 36
          Top = 11
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object Panel5: TPanel
        Left = 101
        Top = 15
        Width = 552
        Height = 44
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object RGTipo: TdmkRadioGroup
          Left = 0
          Top = 0
          Width = 133
          Height = 44
          Align = alLeft
          Caption = '  Pessoa: '
          Columns = 2
          Items.Strings = (
            'Jur'#237'dica'
            'F'#237'sica')
          TabOrder = 0
          OnClick = RGTipoClick
          QryCampo = 'Tipo'
          UpdCampo = 'Tipo'
          UpdType = utYes
          OldValor = 0
        end
        object PnCPF: TPanel
          Left = 318
          Top = 0
          Width = 185
          Height = 44
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object Label8: TLabel
            Left = 8
            Top = 16
            Width = 23
            Height = 13
            Caption = 'CPF:'
          end
          object EdCPF: TdmkEdit
            Left = 40
            Top = 12
            Width = 112
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'CPF'
            UpdCampo = 'CPF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object PnCNPJ: TPanel
          Left = 133
          Top = 0
          Width = 185
          Height = 44
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          Visible = False
          object Label11: TLabel
            Left = 4
            Top = 16
            Width = 30
            Height = 13
            Caption = 'CNPJ:'
          end
          object EdCNPJ: TdmkEdit
            Left = 36
            Top = 12
            Width = 113
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'CNPJ'
            UpdCampo = 'CNPJ'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 70
      Width = 655
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel8: TPanel
        Left = 515
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object QrFrtMnfAutXml: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFrtMnfAutXmlAfterOpen
    BeforeClose = QrFrtMnfAutXmlBeforeClose
    OnCalcFields = QrFrtMnfAutXmlCalcFields
    SQL.Strings = (
      'SELECT cga.*, '
      'IF(cga.Tipo=0, "CNPJ", "CPF") TpDOC,'
      'IF(cga.Tipo=0, autXML_CNPJ, autXML_CPF) CNPJ_CPF, '
      'ELT(cga.AddForma + 1, "a", "b", "c", "d", "e") NO_AddForma'
      'FROM cteit1autxml cga '
      'LEFT JOIN entidades ent ON ent.Codigo=cga.AddIDCad'
      'LEFT JOIN enticontat eco ON eco.Controle=cga.AddIDCad')
    Left = 160
    Top = 112
    object QrFrtMnfAutXmlCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfAutXmlControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfAutXmlAddForma: TSmallintField
      FieldName = 'AddForma'
    end
    object QrFrtMnfAutXmlAddIDCad: TSmallintField
      FieldName = 'AddIDCad'
    end
    object QrFrtMnfAutXmlTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrFrtMnfAutXmlCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrFrtMnfAutXmlCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrFrtMnfAutXmlLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfAutXmlDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfAutXmlDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfAutXmlUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfAutXmlUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfAutXmlAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfAutXmlAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFrtMnfAutXmlCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 14
    end
    object QrFrtMnfAutXmlITEM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'ITEM'
      Calculated = True
    end
    object QrFrtMnfAutXmlTpDOC: TWideStringField
      FieldName = 'TpDOC'
    end
    object QrFrtMnfAutXmlNO_AddForma: TWideStringField
      FieldName = 'NO_AddForma'
      Size = 1
    end
    object QrFrtMnfAutXmlNO_ENT_ECO: TWideStringField
      FieldName = 'NO_ENT_ECO'
      Size = 100
    end
  end
  object DsFrtMnfAutXml: TDataSource
    DataSet = QrFrtMnfAutXml
    Left = 160
    Top = 164
  end
end
