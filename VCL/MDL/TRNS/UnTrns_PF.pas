unit UnTrns_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, Math, UnMsgInt,
  Db, DbCtrls,   Mask, Buttons, ZCF2, (*DBTables,*) mySQLDbTables, ComCtrls,
  (*DBIProcs,*) Registry, Grids, DBGrids, WinSkinStore, WinSkinData, CheckLst,
  printers, CommCtrl, TypInfo, comobj, ShlObj, RichEdit, ShellAPI, Consts,
  ActiveX, OleCtrls, SHDocVw, AdvToolBar, AdvGlowButton, UnDmkProcFunc,
  Variants, MaskUtils, RTLConsts, IniFiles, frxClass, frxPreview,
  mySQLExceptions,
  // Dermatek
  dmkGeral, dmkEdit, dmkDBEdit, dmkEditF7, dmkEditDateTimePicker, dmkCheckGroup,
  dmkDBLookupCombobox, dmkEditCB, dmkDBGrid, dmkDBGridDAC, dmkImage, UnDmkEnums;

type

  TUnTrns_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormTrnsCad(Codigo, Controle: Integer);
    //
    procedure CadastroESelecaoDeTrnsMyTpMed(QrTrnsMyTpMed: TmySQLQuery; EdTrnsMyTpMed:
              TdmkEditCB; CBTrnsMyTpMed: TdmkDBLookupCombobox);
    procedure CadastroESelecaoDeTrnsIts(QrTrnsIts: TmySQLQuery; EdTrnsIts:
              TdmkEditCB; CBTrnsIts: TdmkDBLookupCombobox);
  end;

var
  Trns_PF: TUnTrns_PF;

implementation

uses MyDBCheck, Module, DmkDAC_PF, UMySQLModule, CfgCadLista, ModuleGeral,
  TrnsCad;


{ TUnTrns_PF }

procedure TUnTrns_PF.CadastroESelecaoDeTrnsIts(QrTrnsIts: TmySQLQuery;
  EdTrnsIts: TdmkEditCB; CBTrnsIts: TdmkDBLookupCombobox);
begin
  VAR_CAD_ITEM := 0;
  MostraFormTrnsCad(0, EdTrnsIts.ValueVariant);
  if VAR_CAD_ITEM <> 0 then
  begin
    if QrTrnsIts <> nil then
      UnDmkDAC_PF.AbreQuery(QrTrnsIts, QrTrnsIts.Database);
    //
    EdTrnsIts.ValueVariant := VAR_CAD_ITEM;
    CBTrnsIts.KeyValue     := VAR_CAD_ITEM;
    EdTrnsIts.SetFocus;
  end;
end;

procedure TUnTrns_PF.CadastroESelecaoDeTrnsMyTpMed(QrTrnsMyTpMed: TmySQLQuery;
  EdTrnsMyTpMed: TdmkEditCB; CBTrnsMyTpMed: TdmkDBLookupCombobox);
begin
  VAR_CADASTRO := 0;
  //
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'TrnsMyTpMed', 20, ncGerlSeq1,
  'Tipos de Medida', [], False, Null, [], [], False);
  //
  if VAR_CADASTRO <> 0 then
  begin
    if QrTrnsMyTpMed <> nil then
      UnDmkDAC_PF.AbreQuery(QrTrnsMyTpMed, QrTrnsMyTpMed.Database);
    //
    EdTrnsMyTpMed.ValueVariant := VAR_CADASTRO;
    CBTrnsMyTpMed.KeyValue     := VAR_CADASTRO;
    EdTrnsMyTpMed.SetFocus;
  end;
end;


procedure TUnTrns_PF.MostraFormTrnsCad(Codigo, Controle: Integer);
//var
  //Qry: TmySQLQuery;
begin
  if DBCheck.CriaFm(TFmTrnsCad, FmTrnsCad, afmoNegarComAviso) then
  begin
    (*if (Codigo = 0) and (Controle <> 0) then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      UnDmkDAC_PFQry := TmySQLQuery.Create(Dmod);
      Qry := TmySQLQuery.Create(Dmod);
    end;
    *)
    FmTrnsCad.LocCod(Codigo, Codigo);
    if (FmTrnsCad.QrTrnsIts.State <> dsInactive) and (Controle <> 0) then
      FmTrnsCad.QrTrnsIts.Locate('Controle', Controle, []);
    //
    FmTrnsCad.ShowModal;
    FmTrnsCad.Destroy;
  end;
end;

end.
