object FmTrnsCad: TFmTrnsCad
  Left = 368
  Top = 194
  Caption = 'LGS-TRNSP-001 :: Cadastro de Transportador'
  ClientHeight = 504
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 408
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 101
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label7: TLabel
        Left = 16
        Top = 56
        Width = 89
        Height = 13
        Caption = 'Fantasia / Apelido:'
        FocusControl = DBEdit3
      end
      object Label8: TLabel
        Left = 584
        Top = 56
        Width = 61
        Height = 13
        Caption = 'CNPJ / CPF:'
        FocusControl = DBEdit4
      end
      object Label10: TLabel
        Left = 700
        Top = 56
        Width = 44
        Height = 13
        Caption = 'RNTRC: '
        Color = clBtnFace
        ParentColor = False
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTrnsCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 689
        Height = 21
        Color = clWhite
        DataField = 'NO_ENT'
        DataSource = DsTrnsCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit3: TDBEdit
        Left = 16
        Top = 72
        Width = 565
        Height = 21
        DataField = 'NO_FantApel'
        DataSource = DsTrnsCad
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 584
        Top = 72
        Width = 112
        Height = 21
        DataField = 'CNPJ_CPF'
        DataSource = DsTrnsCad
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 700
        Top = 72
        Width = 65
        Height = 21
        DataField = 'RNTRC'
        DataSource = DsTrnsCad
        TabOrder = 4
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 344
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Transportador'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Ve'#237'culos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 101
      Width = 784
      Height = 148
      Align = alTop
      DataSource = DsTrnsIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_TpVeic'
          Title.Caption = 'Tipo de ve'#237'culo'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_TpRod'
          Title.Caption = 'Tipo de rodado'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_TpCar'
          Title.Caption = 'Tipo de carroceria'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RENAVAM'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Placa'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TaraKg'
          Title.Caption = 'Tara Kg'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CapKg'
          Title.Caption = 'Capc. Kg'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CapM3'
          Title.Caption = 'Cap. m'#179
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UF'
          Visible = True
        end>
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 408
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 185
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label9: TLabel
        Left = 700
        Top = 56
        Width = 44
        Height = 13
        Caption = 'RNTRC: '
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = dmkDBEdit1
      end
      object Label4: TLabel
        Left = 76
        Top = 16
        Width = 105
        Height = 13
        Caption = 'Raz'#227'o Social / Nome:'
        FocusControl = dmkDBEdit2
      end
      object Label5: TLabel
        Left = 16
        Top = 56
        Width = 89
        Height = 13
        Caption = 'Fantasia / Apelido:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 584
        Top = 56
        Width = 61
        Height = 13
        Caption = 'CNPJ / CPF:'
        FocusControl = DBEdit2
      end
      object EdRNTRC: TdmkEdit
        Left = 700
        Top = 72
        Width = 66
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'RNTRC'
        UpdCampo = 'RNTRC'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTrnsCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taRightJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 689
        Height = 21
        Color = clWhite
        DataField = 'NO_ENT'
        DataSource = DsTrnsCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 565
        Height = 21
        DataField = 'NO_FantApel'
        DataSource = DsTrnsCad
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 584
        Top = 72
        Width = 112
        Height = 21
        DataField = 'CNPJ_CPF'
        DataSource = DsTrnsCad
        TabOrder = 4
      end
      object RGTpProp: TdmkRadioGroup
        Left = 16
        Top = 96
        Width = 749
        Height = 49
        Caption = ' Tipo de propriet'#225'rio:'
        Columns = 4
        ItemIndex = 3
        Items.Strings = (
          'TAC - Agregado'
          'TAC - Independente'
          'Outros'
          'Indefinido')
        TabOrder = 5
        QryCampo = 'TpProp'
        UpdCampo = 'TpProp'
        UpdType = utYes
        OldValor = 0
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 345
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 325
        Height = 32
        Caption = 'Cadastro de Transportador'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 325
        Height = 32
        Caption = 'Cadastro de Transportador'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 325
        Height = 32
        Caption = 'Cadastro de Transportador'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrTrnsCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTrnsCadBeforeOpen
    AfterOpen = QrTrnsCadAfterOpen
    BeforeClose = QrTrnsCadBeforeClose
    AfterScroll = QrTrnsCadAfterScroll
    SQL.Strings = (
      'SELECT '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, '
      'IF(ent.Tipo=0, ent.Fantasia, ent.Apelido) NO_FantApel, '
      'trn.*'
      'FROM trnscad trn '
      'LEFT JOIN entidades ent ON ent.Codigo=trn.Codigo')
    Left = 92
    Top = 233
    object QrTrnsCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTrnsCadRNTRC: TWideStringField
      FieldName = 'RNTRC'
      Size = 8
    end
    object QrTrnsCadLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTrnsCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTrnsCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTrnsCadUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTrnsCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTrnsCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTrnsCadAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTrnsCadNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrTrnsCadNO_FantApel: TWideStringField
      FieldName = 'NO_FantApel'
      Size = 60
    end
    object QrTrnsCadCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 14
    end
    object QrTrnsCadTpProp: TSmallintField
      FieldName = 'TpProp'
    end
  end
  object DsTrnsCad: TDataSource
    DataSet = QrTrnsCad
    Left = 92
    Top = 277
  end
  object QrTrnsIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM trnsits'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 188
    Top = 237
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTrnsItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'trnsits.Codigo'
    end
    object QrTrnsItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'trnsits.Controle'
    end
    object QrTrnsItsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'trnsits.Nome'
      Size = 100
    end
    object QrTrnsItsRENAVAM: TWideStringField
      FieldName = 'RENAVAM'
      Origin = 'trnsits.RENAVAM'
      Size = 11
    end
    object QrTrnsItsPlaca: TWideStringField
      FieldName = 'Placa'
      Origin = 'trnsits.Placa'
      Size = 11
    end
    object QrTrnsItsTaraKg: TIntegerField
      FieldName = 'TaraKg'
      Origin = 'trnsits.TaraKg'
    end
    object QrTrnsItsCapKg: TIntegerField
      FieldName = 'CapKg'
      Origin = 'trnsits.CapKg'
    end
    object QrTrnsItsTpVeic: TSmallintField
      FieldName = 'TpVeic'
      Origin = 'trnsits.TpVeic'
    end
    object QrTrnsItsTpRod: TSmallintField
      FieldName = 'TpRod'
      Origin = 'trnsits.TpRod'
    end
    object QrTrnsItsTpCar: TSmallintField
      FieldName = 'TpCar'
      Origin = 'trnsits.TpCar'
    end
    object QrTrnsItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'trnsits.Lk'
    end
    object QrTrnsItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'trnsits.DataCad'
    end
    object QrTrnsItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'trnsits.DataAlt'
    end
    object QrTrnsItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'trnsits.UserCad'
    end
    object QrTrnsItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'trnsits.UserAlt'
    end
    object QrTrnsItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'trnsits.AlterWeb'
    end
    object QrTrnsItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'trnsits.Ativo'
    end
    object QrTrnsItsNO_TpVeic: TWideStringField
      FieldName = 'NO_TpVeic'
      Size = 10
    end
    object QrTrnsItsNO_TpRod: TWideStringField
      FieldName = 'NO_TpRod'
      Size = 30
    end
    object QrTrnsItsNO_TpCar: TWideStringField
      FieldName = 'NO_TpCar'
      Size = 30
    end
    object QrTrnsItsCapM3: TIntegerField
      FieldName = 'CapM3'
    end
    object QrTrnsItsUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
  end
  object DsTrnsIts: TDataSource
    DataSet = QrTrnsIts
    Left = 188
    Top = 281
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      Default = True
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      Visible = False
      OnClick = CabExclui1Click
    end
  end
end
