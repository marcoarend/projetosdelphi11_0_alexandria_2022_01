object FmFrtFatInfCTeSub: TFmFrtFatInfCTeSub
  Left = 339
  Top = 185
  Caption = 'FAT-FRETE-005 :: Faturamento de Frete - CTe Substitu'#237'do'
  ClientHeight = 458
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 784
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 4
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object LaControle: TLabel
      Left = 72
      Top = 20
      Width = 42
      Height = 13
      Caption = 'Controle:'
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object EdControle: TdmkEdit
      Left = 72
      Top = 36
      Width = 85
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 479
        Height = 32
        Caption = 'Faturamento de Frete - CTe Substitu'#237'do'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 479
        Height = 32
        Caption = 'Faturamento de Frete - CTe Substitu'#237'do'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 479
        Height = 32
        Caption = 'Faturamento de Frete - CTe Substitu'#237'do'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 344
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 6
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 279
        Height = 16
        Caption = 'Informe apenas os dados de uma das tr'#234's abas.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 279
        Height = 16
        Caption = 'Informe apenas os dados de uma das tr'#234's abas.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 388
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object PCContribICMS: TPageControl
    Left = 0
    Top = 201
    Width = 784
    Height = 143
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 2
    object TabSheet0: TTabSheet
      Caption = 'Indefinido'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object TabSheet1: TTabSheet
      Caption = 'Tomador '#201' contribuinte do ICMS'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PCTipoDoc: TPageControl
        Left = 0
        Top = 41
        Width = 776
        Height = 74
        ActivePage = TabSheet12
        Align = alClient
        TabOrder = 1
        object TabSheet10: TTabSheet
          Caption = 'Indefinido'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
        object TabSheet11: TTabSheet
          Caption = 'NF-e'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 768
            Height = 46
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label15: TLabel
              Left = 4
              Top = 4
              Width = 160
              Height = 13
              Caption = 'Chave NF-e emitida pelo tomador:'
            end
            object SbrefCTe: TSpeedButton
              Left = 281
              Top = 20
              Width = 21
              Height = 21
              Caption = '...'
            end
            object EdrefNFe: TdmkEdit
              Left = 4
              Top = 20
              Width = 276
              Height = 21
              MaxLength = 44
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'refNFe'
              UpdCampo = 'refNFe'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnRedefinido = EdrefNFeRedefinido
            end
          end
        end
        object TabSheet12: TTabSheet
          Caption = 'NF/CT'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 768
            Height = 46
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label28: TLabel
              Left = 236
              Top = 4
              Width = 38
              Height = 13
              Caption = 'Modelo:'
            end
            object Label30: TLabel
              Left = 280
              Top = 4
              Width = 27
              Height = 13
              Caption = 'S'#233'rie:'
            end
            object Label31: TLabel
              Left = 320
              Top = 4
              Width = 44
              Height = 13
              Caption = 'Subs'#233'rie:'
            end
            object Label32: TLabel
              Left = 564
              Top = 4
              Width = 82
              Height = 13
              Caption = 'Data da emiss'#227'o:'
            end
            object Label38: TLabel
              Left = 460
              Top = 4
              Width = 98
              Height = 13
              Caption = 'Valor do documento:'
            end
            object Label120: TLabel
              Left = 4
              Top = 4
              Width = 42
              Height = 13
              Caption = 'CNPJ ...:'
              FocusControl = EdCNPJ
            end
            object Label136: TLabel
              Left = 120
              Top = 4
              Width = 50
              Height = 13
              Caption = '... ou CPF:'
              FocusControl = EdCPF
            end
            object Label1: TLabel
              Left = 372
              Top = 4
              Width = 71
              Height = 13
              Caption = 'N'#186' documento:'
            end
            object Edmod_: TdmkEdit
              Left = 236
              Top = 20
              Width = 41
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'mod_'
              UpdCampo = 'mod_'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object Edserie: TdmkEdit
              Left = 280
              Top = 20
              Width = 36
              Height = 21
              Alignment = taRightJustify
              MaxLength = 3
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'serie'
              UpdCampo = 'serie'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object Edsubserie: TdmkEdit
              Left = 320
              Top = 20
              Width = 49
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'subserie'
              UpdCampo = 'subserie'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object TPdEmi: TdmkEditDateTimePicker
              Left = 564
              Top = 20
              Width = 112
              Height = 21
              Date = 0.702857974538346800
              Time = 0.702857974538346800
              TabOrder = 7
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'dEmi'
              UpdCampo = 'dEmi'
              UpdType = utYes
            end
            object Edvalor: TdmkEdit
              Left = 460
              Top = 20
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'valor'
              UpdCampo = 'valor'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdCNPJ: TdmkEdit
              Left = 4
              Top = 20
              Width = 112
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtCPFJ_NFe
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'CNPJ'
              UpdCampo = 'CNPJ'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCPF: TdmkEdit
              Left = 120
              Top = 20
              Width = 112
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtCPFJ_NFe
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'CPF'
              UpdCampo = 'CPF'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object Ednro: TdmkEdit
              Left = 372
              Top = 20
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'nro'
              UpdCampo = 'nro'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
        end
        object TabSheet13: TTabSheet
          Caption = 'CT-e'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel13: TPanel
            Left = 0
            Top = 0
            Width = 768
            Height = 46
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label3: TLabel
              Left = 4
              Top = 4
              Width = 160
              Height = 13
              Caption = 'Chave CT-e emitida pelo tomador:'
            end
            object SpeedButton2: TSpeedButton
              Left = 281
              Top = 20
              Width = 21
              Height = 21
              Caption = '...'
            end
            object EdrefCTe: TdmkEdit
              Left = 4
              Top = 20
              Width = 276
              Height = 21
              MaxLength = 44
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'refCTe'
              UpdCampo = 'refCTe'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnRedefinido = EdrefCTeRedefinido
            end
          end
        end
      end
      object RGTipoDoc: TdmkRadioGroup
        Left = 0
        Top = 0
        Width = 776
        Height = 41
        Align = alTop
        Caption = ' Tipo de documento: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          'NF-e'
          'NF/CT'
          'CT-e')
        TabOrder = 0
        OnClick = RGTipoDocClick
        QryCampo = 'TipoDoc'
        UpdCampo = 'TipoDoc'
        UpdType = utYes
        OldValor = 0
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Tomador N'#195'O '#233' contribuinte do ICMS'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 115
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label2: TLabel
          Left = 4
          Top = 4
          Width = 122
          Height = 13
          Caption = 'Chave CT-e de anula'#231#227'o:'
        end
        object SpeedButton1: TSpeedButton
          Left = 281
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
        end
        object EdrefCTeAnu: TdmkEdit
          Left = 4
          Top = 20
          Width = 276
          Height = 21
          MaxLength = 44
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'refCTeAnu'
          UpdCampo = 'refCTeAnu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 112
    Width = 784
    Height = 44
    Align = alTop
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    object Panel8: TPanel
      Left = 0
      Top = 0
      Width = 305
      Height = 44
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Label27: TLabel
        Left = 4
        Top = 4
        Width = 60
        Height = 13
        Caption = 'Chave CT-e:'
      end
      object SBChCTe: TSpeedButton
        Left = 281
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
      end
      object EdChCTe: TdmkEdit
        Left = 4
        Top = 20
        Width = 276
        Height = 21
        MaxLength = 44
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'ChCTe'
        UpdCampo = 'ChCTe'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnRedefinido = EdChCTeRedefinido
      end
    end
  end
  object RGContribICMS: TdmkRadioGroup
    Left = 0
    Top = 156
    Width = 784
    Height = 45
    Align = alTop
    Caption = ' ICMS: '
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Indefinido'
      'Tomador '#201' contribuinte do ICMS'
      'Tomador N'#195'O '#233' contribuinte do ICMS')
    TabOrder = 1
    OnClick = RGContribICMSClick
    QryCampo = 'ContribICMS'
    UpdCampo = 'ContribICMS'
    UpdType = utYes
    OldValor = 0
  end
end
