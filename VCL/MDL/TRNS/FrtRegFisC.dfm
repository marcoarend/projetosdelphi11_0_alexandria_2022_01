object FmFrtRegFisC: TFmFrtRegFisC
  Left = 368
  Top = 194
  Caption = 'FRT-REGFI-001 :: Regras Fiscais de Frete'
  ClientHeight = 640
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 544
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 405
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 16
        Top = 56
        Width = 109
        Height = 13
        Caption = 'Natureza da opera'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 16
        Top = 96
        Width = 31
        Height = 13
        Caption = 'CFOP:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsFrtRegFisC
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsFrtRegFisC
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 752
        Height = 21
        DataField = 'NatOp'
        DataSource = DsFrtRegFisC
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 16
        Top = 112
        Width = 752
        Height = 21
        DataField = 'NO_CFOP'
        DataSource = DsFrtRegFisC
        TabOrder = 3
      end
      object RGDBTpCTe: TDBRadioGroup
        Left = 16
        Top = 136
        Width = 753
        Height = 61
        Caption = 'Tipo de CT-e:'
        Columns = 2
        DataField = 'TpCTe'
        DataSource = DsFrtRegFisC
        Items.Strings = (
          'CT-e Normal'
          'CT-e de Complemento de Valores'
          'CT-e de Anula'#231#227'o'
          'CT-e Substituto')
        ParentBackground = True
        TabOrder = 4
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9'
          '10')
      end
      object RGDBMyCST: TDBRadioGroup
        Left = 16
        Top = 200
        Width = 753
        Height = 129
        Caption = 'CST:'
        Columns = 2
        DataField = 'MyCST'
        DataSource = DsFrtRegFisC
        Items.Strings = (
          'N'#227'o definido'
          '00-Tributa'#231#227'o Normal do ICMS'
          '20-tributa'#231#227'o com BC reduzida do ICMS'
          '40-ICMS isen'#231#227'o'
          '41-ICMS n'#227'o tributada'
          '51-ICMS diferido'
          '60-ICMS cobrado anteriormente por substitui'#231#227'o tribut'#225'ria'
          '90-ICMS outros - devido '#224' UF de origem da presta'#231#227'o'
          '90-ICMS outros'
          'Simples Nacional')
        ParentBackground = True
        TabOrder = 5
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9'
          '10')
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 480
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 544
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 465
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 109
        Height = 13
        Caption = 'Natureza da opera'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 16
        Top = 96
        Width = 31
        Height = 13
        Caption = 'CFOP:'
      end
      object SbCFOP: TSpeedButton
        Left = 747
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbCFOPClick
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        MaxLength = 100
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNatOp: TdmkEdit
        Left = 16
        Top = 72
        Width = 752
        Height = 21
        MaxLength = 60
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NatOp'
        UpdCampo = 'NatOp'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCFOP: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CFOP'
        UpdCampo = 'CFOP'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '0'
        ValWarn = False
        DBLookupComboBox = CBCFOP
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCFOP: TdmkDBLookupComboBox
        Left = 71
        Top = 112
        Width = 674
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCFOP
        TabOrder = 4
        dmkEditCB = EdCFOP
        QryCampo = 'CFOP'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGTpCTe: TdmkRadioGroup
        Left = 16
        Top = 136
        Width = 753
        Height = 61
        Caption = 'Tipo de CT-e:'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'CT-e Normal'
          'CT-e de Complemento de Valores'
          'CT-e de Anula'#231#227'o'
          'CT-e Substituto')
        TabOrder = 5
        QryCampo = 'TpCTe'
        UpdCampo = 'TpCTe'
        UpdType = utYes
        OldValor = 0
      end
      object RGMyCST: TdmkRadioGroup
        Left = 16
        Top = 200
        Width = 753
        Height = 129
        Caption = 'CST:'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o definido'
          '00-Tributa'#231#227'o Normal do ICMS'
          '20-tributa'#231#227'o com BC reduzida do ICMS'
          '40-ICMS isen'#231#227'o'
          '41-ICMS n'#227'o tributada'
          '51-ICMS diferido'
          '60-ICMS cobrado anteriormente por substitui'#231#227'o tribut'#225'ria'
          '90-ICMS outros - devido '#224' UF de origem da presta'#231#227'o'
          '90-ICMS outros'
          'Simples Nacional')
        TabOrder = 6
        QryCampo = 'MyCST'
        UpdCampo = 'MyCST'
        UpdType = utYes
        OldValor = 0
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 332
        Width = 753
        Height = 57
        Caption = ' Percentuais de ICMS: '
        TabOrder = 7
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 749
          Height = 40
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label8: TLabel
            Left = 8
            Top = 0
            Width = 40
            Height = 13
            Caption = '% ICMS:'
          end
          object Label10: TLabel
            Left = 112
            Top = 1
            Width = 75
            Height = 13
            Caption = '% Redu'#231#227'o BC:'
          end
          object Label11: TLabel
            Left = 216
            Top = 1
            Width = 91
            Height = 13
            Caption = '% ICMS ST Retido:'
          end
          object Label12: TLabel
            Left = 320
            Top = 1
            Width = 98
            Height = 13
            Caption = '% Red. BC outra UF:'
          end
          object Label13: TLabel
            Left = 424
            Top = 1
            Width = 84
            Height = 13
            Caption = '% ICMS outra UF:'
          end
          object EdICMS_pICMS: TdmkEdit
            Left = 8
            Top = 16
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ICMS_pICMS'
            UpdCampo = 'ICMS_pICMS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdICMS_pRedBC: TdmkEdit
            Left = 112
            Top = 16
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ICMS_pRedBC'
            UpdCampo = 'ICMS_pRedBC'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdICMS_pICMSSTRet: TdmkEdit
            Left = 216
            Top = 16
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ICMS_pICMSSTRet'
            UpdCampo = 'ICMS_pICMSSTRet'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdICMS_pRedBCOutraUF: TdmkEdit
            Left = 320
            Top = 16
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ICMS_pRedBCOutraUF'
            UpdCampo = 'ICMS_pRedBCOutraUF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdICMS_pICMSOutraUF: TdmkEdit
            Left = 424
            Top = 16
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ICMS_pICMSOutraUF'
            UpdCampo = 'ICMS_pICMSOutraUF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 16
        Top = 392
        Width = 753
        Height = 65
        Caption = 
          ' Valor de tributos federais, estaduais e municipais (Lei da tran' +
          'spar'#234'ncia - Lei n'#186' 12.741/2012): '
        TabOrder = 8
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 749
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object RGiTotTrib_Fonte: TdmkRadioGroup
            Left = 0
            Top = 0
            Width = 180
            Height = 48
            Align = alLeft
            Caption = ' Fonte do c'#225'lculo: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o informar'
              'IBPTax')
            TabOrder = 0
            OnClick = RGiTotTrib_FonteClick
            QryCampo = 'iTotTrib_Fonte'
            UpdCampo = 'iTotTrib_Fonte'
            UpdType = utYes
            OldValor = 0
          end
          object PnIBPTax: TPanel
            Left = 180
            Top = 0
            Width = 569
            Height = 48
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            Visible = False
            object Label14: TLabel
              Left = 412
              Top = 5
              Width = 17
              Height = 13
              Caption = 'EX:'
            end
            object Label15: TLabel
              Left = 440
              Top = 5
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
            end
            object RGiTotTrib_Tabela: TdmkRadioGroup
              Left = 0
              Top = 0
              Width = 272
              Height = 48
              Align = alLeft
              Caption = 'Tabela IBPTax:'
              Columns = 4
              ItemIndex = 3
              Items.Strings = (
                'NCM'
                'NBS'
                'LC116'
                '?????')
              TabOrder = 0
              QryCampo = 'iTotTrib_Tabela'
              UpdCampo = 'iTotTrib_Tabela'
              UpdType = utYes
              OldValor = 0
            end
            object EdiTotTrib_Ex: TdmkEdit
              Left = 412
              Top = 20
              Width = 25
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'iTotTrib_Ex'
              UpdCampo = 'iTotTrib_Ex'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object BtiTotTrib: TBitBtn
              Tag = 20
              Left = 280
              Top = 5
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Pesquisar'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtiTotTribClick
            end
            object EdiTotTrib_Codigo: TdmkEdit
              Left = 440
              Top = 20
              Width = 116
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInt64
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'iTotTrib_Codigo'
              UpdCampo = 'iTotTrib_Codigo'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 481
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 292
        Height = 32
        Caption = 'Regras Fiscais de Frete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 292
        Height = 32
        Caption = 'Regras Fiscais de Frete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 292
        Height = 32
        Caption = 'Regras Fiscais de Frete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrFrtRegFisC: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFrtRegFisCBeforeOpen
    AfterOpen = QrFrtRegFisCAfterOpen
    SQL.Strings = (
      'SELECT '
      
        ' ELT(fis.TpCTe + 1,"CT-e Normal","CT-e de Complemento de Valores' +
        '","CT-e de Anula'#231#227'o","CT-e Substituto") NO_TpCTe,'
      
        ' ELT(fis.TpCTe + 1,"N'#227'o definido","00-Tributa'#231#227'o Normal do ICMS"' +
        ',"20-tributa'#231#227'o com BC reduzida do ICMS","40-ICMS isen'#231#227'o;","41-' +
        'ICMS n'#227'o tributada;","51-ICMS diferido","60-ICMS cobrado anterio' +
        'rmente por substitui'#231#227'o tribut'#225'ria","90-ICMS outros - devido '#224' U' +
        'F de origem da presta'#231#227'o","90-ICMS outros","Simples Nacional") N' +
        'O_MyCST,'
      'fis.* '
      'FROM frtregfisc fis'
      'WHERE fis.Codigo > 0'
      'AND fis.Codigo=:P0')
    Left = 236
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtRegFisCNO_TpCTe: TWideStringField
      FieldName = 'NO_TpCTe'
      Size = 60
    end
    object QrFrtRegFisCNO_MyCST: TWideStringField
      FieldName = 'NO_MyCST'
      Size = 60
    end
    object QrFrtRegFisCNO_CFOP: TWideStringField
      FieldName = 'NO_CFOP'
      Size = 255
    end
    object QrFrtRegFisCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtRegFisCNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrFrtRegFisCNatOp: TWideStringField
      FieldName = 'NatOp'
      Size = 60
    end
    object QrFrtRegFisCTpCTe: TSmallintField
      FieldName = 'TpCTe'
    end
    object QrFrtRegFisCMyCST: TSmallintField
      FieldName = 'MyCST'
    end
    object QrFrtRegFisCLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtRegFisCDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtRegFisCDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtRegFisCUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtRegFisCUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtRegFisCAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtRegFisCAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFrtRegFisCCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 5
    end
    object QrFrtRegFisCICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrFrtRegFisCICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrFrtRegFisCICMS_pICMSSTRet: TFloatField
      FieldName = 'ICMS_pICMSSTRet'
    end
    object QrFrtRegFisCICMS_pRedBCOutraUF: TFloatField
      FieldName = 'ICMS_pRedBCOutraUF'
    end
    object QrFrtRegFisCICMS_pICMSOutraUF: TFloatField
      FieldName = 'ICMS_pICMSOutraUF'
    end
    object QrFrtRegFisCiTotTrib_Fonte: TIntegerField
      FieldName = 'iTotTrib_Fonte'
    end
    object QrFrtRegFisCiTotTrib_Tabela: TIntegerField
      FieldName = 'iTotTrib_Tabela'
    end
    object QrFrtRegFisCiTotTrib_Ex: TIntegerField
      FieldName = 'iTotTrib_Ex'
    end
    object QrFrtRegFisCiTotTrib_Codigo: TLargeintField
      FieldName = 'iTotTrib_Codigo'
    end
  end
  object DsFrtRegFisC: TDataSource
    DataSet = QrFrtRegFisC
    Left = 236
    Top = 248
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrCFOP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cfop2003'
      'ORDER BY Nome')
    Left = 312
    Top = 200
    object QrCFOPCodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 5
    end
    object QrCFOPNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCFOPDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCFOPComplementacao: TWideMemoField
      FieldName = 'Complementacao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsCFOP: TDataSource
    DataSet = QrCFOP
    Left = 312
    Top = 248
  end
end
