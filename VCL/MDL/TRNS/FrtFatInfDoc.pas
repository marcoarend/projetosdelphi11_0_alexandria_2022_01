unit FrtFatInfDoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.ComCtrls,
  dmkEditDateTimePicker;

type
  TFmFrtFatInfDoc = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label6: TLabel;
    EdControle: TdmkEdit;
    PCMyTpDocInf: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    Label15: TLabel;
    SpeedButton1: TSpeedButton;
    EdNFe_Chave: TdmkEdit;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel7: TPanel;
    RGMyTpDocInf: TdmkRadioGroup;
    TabSheet0: TTabSheet;
    Panel6: TPanel;
    Label2: TLabel;
    EdInfNF_nRoma: TdmkEdit;
    EdInfNF_nPed: TdmkEdit;
    Label3: TLabel;
    RGInfNF_mod: TdmkRadioGroup;
    Label4: TLabel;
    EdInfNF_serie: TdmkEdit;
    Label7: TLabel;
    EdInfNF_nDoc: TdmkEdit;
    TPInfNF_dEmi: TdmkEditDateTimePicker;
    Label8: TLabel;
    EdInfNF_vBC: TdmkEdit;
    Label9: TLabel;
    Label10: TLabel;
    EdInfNF_vICMS: TdmkEdit;
    Label11: TLabel;
    EdInfNF_vBCST: TdmkEdit;
    Label12: TLabel;
    EdInfNF_vST: TdmkEdit;
    Label13: TLabel;
    EdInfNF_vProd: TdmkEdit;
    Label14: TLabel;
    EdInfNF_vNF: TdmkEdit;
    Label16: TLabel;
    EdInfNF_nPeso: TdmkEdit;
    Label17: TLabel;
    EdInfNF_nCFOP: TdmkEdit;
    Label18: TLabel;
    Label1: TLabel;
    EdNFe_PIN: TdmkEdit;
    Label23: TLabel;
    TPNFe_dPrev: TdmkEditDateTimePicker;
    Label19: TLabel;
    EdInfNF_PIN: TdmkEdit;
    TPInfNF_dPrev: TdmkEditDateTimePicker;
    Label20: TLabel;
    RGInfOutros_tpDoc: TdmkRadioGroup;
    EdInfOutros_nDoc: TdmkEdit;
    Label21: TLabel;
    Label22: TLabel;
    TPInfOutros_dEmi: TdmkEditDateTimePicker;
    Label24: TLabel;
    EdInfOutros_vDocFisc: TdmkEdit;
    Label25: TLabel;
    TPInfOutros_dPrev: TdmkEditDateTimePicker;
    EdInfOutros_descOutros: TdmkEdit;
    Label26: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdNFe_ChaveExit(Sender: TObject);
    procedure RGMyTpDocInfClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenFrtFatInfNFe(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtFatInfDoc: TFmFrtFatInfDoc;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnMySQLCuringa, UnUMedi_PF, UnidMed, UnFrt_PF, UnXXe_PF;

{$R *.DFM}

procedure TFmFrtFatInfDoc.BtOKClick(Sender: TObject);
var
  InfNFe_chave, InfNFe_dPrev, InfNF_nRoma, InfNF_nPed, InfNF_dEmi, InfNF_dPrev,
  InfOutros_descOutros, InfOutros_nDoc, InfNF_serie, InfNF_nDoc, InfOutros_dEmi,
  InfOutros_dPrev: String;
  Codigo, Controle, MyTpDocInf, InfNFe_PIN, InfNF_mod, InfNF_nCFOP, InfNF_PIN,
  InfOutros_tpDoc: Integer;
  InfNF_vBC, InfNF_vICMS, InfNF_vBCST, InfNF_vST, InfNF_vProd, InfNF_vNF,
  InfNF_nPeso, InfOutros_vDocFisc: Double;
  SQLType: TSQLType;
begin
  SQLType              := ImgTipo.SQLType;
  Codigo               := Geral.IMV(DBEdCodigo.Text);
  Controle             := EdControle.ValueVariant;
  //
  MyTpDocInf           := RGMyTpDocInf.ItemIndex;
  InfNFe_chave         := '';
  InfNFe_PIN           := 0;
  InfNFe_dPrev         := '0000-00-00';
  InfNF_nRoma          := '';
  InfNF_nPed           := '';
  InfNF_mod            := 0;
  InfNF_serie          := '';
  InfNF_nDoc           := '';
  InfNF_dEmi           := '0000-00-00';
  InfNF_vBC            := 0.00;
  InfNF_vICMS          := 0.00;
  InfNF_vBCST          := 0.00;
  InfNF_vST            := 0.00;
  InfNF_vProd          := 0.00;
  InfNF_vNF            := 0.00;
  InfNF_nCFOP          := 0;
  InfNF_nPeso          := 0.000;
  InfNF_PIN            := 0;
  InfNF_dPrev          := '0000-00-00';
  InfOutros_tpDoc      := 0;
  InfOutros_descOutros := '';
  InfOutros_nDoc       := '';
  InfOutros_dEmi       := '0000-00-00';
  InfOutros_vDocFisc   := 0.00;
  InfOutros_dPrev      := '0000-00-00';
  //
  case MyTpDocInf of
    0:
    begin
      Geral.MB_Aviso('Informe o tipo de documento!');
      Exit;
    end;
    1:
    begin
      InfNFe_Chave          := EdNFe_Chave.Text;
      InfNFe_PIN            := EdNFe_PIN.ValueVariant;
      InfNFe_DPrev          := Geral.FDT(TPNFe_dPrev.Date, 1, True);
      //
      if MyObjects.FIC(Trim(InfNFe_Chave) = '', EdNFe_Chave,
      'Informe a chave da NFe!') then Exit;
   end;
    2:
    begin
      InfNF_nRoma          := EdInfNF_nRoma.Text;
      InfNF_nPed           := EdInfNF_nPed.Text;
      InfNF_mod            := RGInfNF_mod.ItemIndex;
      InfNF_serie          := EdInfNF_serie.ValueVariant;
      InfNF_nDoc           := EdInfNF_nDoc.ValueVariant;
      InfNF_dEmi           := Geral.FDT(TPInfNF_dEmi.Date, 1);
      InfNF_vBC            := EdInfNF_vBC.ValueVariant;
      InfNF_vICMS          := EdInfNF_vICMS.ValueVariant;
      InfNF_vBCST          := EdInfNF_vBCST.ValueVariant;
      InfNF_vST            := EdInfNF_vST.ValueVariant;
      InfNF_vProd          := EdInfNF_vProd.ValueVariant;
      InfNF_vNF            := EdInfNF_vNF.ValueVariant;
      InfNF_nCFOP          := EdInfNF_nCFOP.ValueVariant;
      InfNF_nPeso          := EdInfNF_nPeso.ValueVariant;
      InfNF_PIN            := EdInfNF_PIN.ValueVariant;
      InfNF_dPrev          := Geral.FDT(TPInfNF_dPrev.Date, 1);
      //
      if MyObjects.FIC((InfNF_mod in ([1,4])) = False, RGInfNF_mod,
      'Informe o modelo fiscal!') then Exit;
      if MyObjects.FIC(Trim(InfNF_serie) = '', EdInfNF_serie,
      'Informe a s�rie da NF!') then Exit;
      if MyObjects.FIC(Trim(InfNF_nDoc) = '', EdInfNF_nDoc,
      'Informe o n�mero da NF!') then Exit;
      if MyObjects.FIC(TPInfNF_dEmi.Date < 2, TPInfNF_dEmi,
      'Informe a data de emiss�o da NF!') then Exit;
      if MyObjects.FIC(InfNF_nCFOP = 0, EdInfNF_nCFOP,
      'Informe a data de emiss�o da NF!') then Exit;
    end;
    3:
    begin
      case RGInfOutros_tpDoc.ItemIndex of
        1: InfOutros_tpDoc := 0;
        2: InfOutros_tpDoc := 10;
        3: InfOutros_tpDoc := 99;
        else InfOutros_tpDoc := -1;
      end;
      //
      InfOutros_descOutros := EdInfOutros_descOutros.Text;
      InfOutros_nDoc       := EdInfOutros_nDoc.Text;
      InfOutros_dEmi       := Geral.FDT(TPInfOutros_dEmi.Date, 1);
      InfOutros_vDocFisc   := EdInfOutros_vDocFisc.ValueVariant;
      InfOutros_dPrev      := Geral.FDT(TPInfOutros_dPrev.Date, 1);
      //
      if MyObjects.FIC(InfOutros_tpDoc = -1, RGInfOutros_tpDoc,
      'Informe o tipo de documento!') then Exit;
    end;
    else
    begin
      Geral.MB_Aviso('Tipo de documento n�o implementado!');
      Exit;
    end;
  end;

  //
  Controle := UMyMod.BPGS1I32('frtfatinfdoc', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtfatinfdoc', False, [
  'Codigo', 'MyTpDocInf', 'InfNFe_chave',
  'InfNFe_PIN', 'InfNFe_dPrev', 'InfNF_nRoma',
  'InfNF_nPed', 'InfNF_mod', 'InfNF_serie',
  'InfNF_nDoc', 'InfNF_dEmi', 'InfNF_vBC',
  'InfNF_vICMS', 'InfNF_vBCST', 'InfNF_vST',
  'InfNF_vProd', 'InfNF_vNF', 'InfNF_nCFOP',
  'InfNF_nPeso', 'InfNF_PIN', 'InfNF_dPrev',
  'InfOutros_tpDoc', 'InfOutros_descOutros', 'InfOutros_nDoc',
  'InfOutros_dEmi', 'InfOutros_vDocFisc', 'InfOutros_dPrev'], [
  'Controle'], [
  Codigo, MyTpDocInf, InfNFe_chave,
  InfNFe_PIN, InfNFe_dPrev, InfNF_nRoma,
  InfNF_nPed, InfNF_mod, InfNF_serie,
  InfNF_nDoc, InfNF_dEmi, InfNF_vBC,
  InfNF_vICMS, InfNF_vBCST, InfNF_vST,
  InfNF_vProd, InfNF_vNF, InfNF_nCFOP,
  InfNF_nPeso, InfNF_PIN, InfNF_dPrev,
  InfOutros_tpDoc, InfOutros_descOutros, InfOutros_nDoc,
  InfOutros_dEmi, InfOutros_vDocFisc, InfOutros_dPrev], [
  Controle], True) then
  begin
    ReopenFrtFatInfNFe(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType      := stIns;
      //
      EdNFe_Chave.ValueVariant             := '';
      EdNFe_PIN.ValueVariant               := '';
      TPNFe_dPrev.Date                     := 0;

      EdInfNF_nRoma.ValueVariant           := '';
      EdInfNF_nPed.ValueVariant            := '';
      RGInfNF_mod.ItemIndex                := 0;
      EdInfNF_serie.ValueVariant           := '';
      EdInfNF_nDoc.ValueVariant            := '';
      TPInfNF_dEmi.Date                    := 0;
      EdInfNF_vBC.ValueVariant             := 0.00;
      EdInfNF_vICMS.ValueVariant           := 0.00;
      EdInfNF_vBCST.ValueVariant           := 0.00;
      EdInfNF_vST.ValueVariant             := 0.00;
      EdInfNF_vProd.ValueVariant           := 0.00;
      EdInfNF_vNF.ValueVariant             := 0.00;
      EdInfNF_nCFOP.ValueVariant           := 0;
      EdInfNF_nPeso.ValueVariant           := 0.000;
      EdInfNF_PIN.ValueVariant             := 0;
      TPInfNF_dPrev.Date                   := 0;

      RGInfOutros_tpDoc.ItemIndex          := 0;
      EdInfOutros_descOutros.ValueVariant  := '';
      EdInfOutros_nDoc.ValueVariant        := '';
      TPInfOutros_dEmi.Date                := 0;
      EdInfOutros_vDocFisc.ValueVariant    := 0.00;
      TPInfOutros_dPrev.Date               := 0;
      //
    end else Close;
  end;
end;

procedure TFmFrtFatInfDoc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtFatInfDoc.EdNFe_ChaveExit(Sender: TObject);
begin
  if not XXe_PF.VerificaChaveAcessoXXe(EdNFe_Chave.Text, True) then
    EdNFe_Chave.SetFocus;
end;

procedure TFmFrtFatInfDoc.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtFatInfDoc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  RGMyTpDocInf.ItemIndex := 0;
end;

procedure TFmFrtFatInfDoc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtFatInfDoc.ReopenFrtFatInfNFe(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmFrtFatInfDoc.RGMyTpDocInfClick(Sender: TObject);
begin
  PCMyTpDocInf.Visible := False;
  //
  TabSheet0.TabVisible := True;
  TabSheet1.TabVisible := True;
  TabSheet2.TabVisible := True;
  TabSheet3.TabVisible := True;
  //
  PCMyTpDocInf.ActivePageIndex := RGMyTpDocInf.ItemIndex;
  case RGMyTpDocInf.ItemIndex of
    0: PCMyTpDocInf.ActivePage := TabSheet0;
    1: PCMyTpDocInf.ActivePage := TabSheet1;
    2: PCMyTpDocInf.ActivePage := TabSheet2;
    3: PCMyTpDocInf.ActivePage := TabSheet3;
  end;
  //
  TabSheet0.TabVisible := RGMyTpDocInf.ItemIndex = 0;
  TabSheet1.TabVisible := RGMyTpDocInf.ItemIndex = 1;
  TabSheet2.TabVisible := RGMyTpDocInf.ItemIndex = 2;
  TabSheet3.TabVisible := RGMyTpDocInf.ItemIndex = 3;
  //
  PCMyTpDocInf.Visible := True;
end;

end.
