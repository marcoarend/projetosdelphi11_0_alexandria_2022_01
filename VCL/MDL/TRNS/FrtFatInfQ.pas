unit FrtFatInfQ;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmFrtFatInfQ = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    Label1: TLabel;
    EdUnidMed: TdmkEditCB;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    VUUnidMed: TdmkValUsu;
    QrTrnsMyTpMed: TmySQLQuery;
    QrTrnsMyTpMedCodigo: TIntegerField;
    QrTrnsMyTpMedNome: TWideStringField;
    DsTrnsMyTpMed: TDataSource;
    Label2: TLabel;
    EdMyTpMed: TdmkEditCB;
    CBMyTpMed: TdmkDBLookupComboBox;
    SbCaracAd: TSpeedButton;
    EdQCarga: TdmkEdit;
    Label4: TLabel;
    RGCUnid: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbCaracAdClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopanFrtFatItsQ(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtFatInfQ: TFmFrtFatInfQ;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnMySQLCuringa, UnUMedi_PF, UnidMed, UnFrt_PF, UnTrns_PF;

{$R *.DFM}

procedure TFmFrtFatInfQ.BtOKClick(Sender: TObject);
var
  Codigo, Controle, CUnid, MyTpMed: Integer;
  QCarga: Double;
  SQLType: TSQLType;
begin
(*
  if MyObjects.FIC(VUUnidMed.ValueVariant = Null, EdUnidMed,
  'Informe a unidade de medida!') then Exit;
*)
  //
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  CUnid          := RGCUnid.ItemIndex;
  MyTpMed        := EdMyTpMed.ValueVariant;
  QCarga         := EdQCarga.ValueVariant;
  //
  if MyObjects.FIC(CUnid < 0, RGCUnid, 'Informe a unidade de medida!') then
    Exit;
  if MyObjects.FIC(CUnid > 5, RGCUnid, 'Informe a unidade de medida!') then
    Exit;
  if MyObjects.FIC(MyTpMed = 0, EdMyTpMed, 'Informe o tipo de medida!') then
    Exit;
  if MyObjects.FIC(QCarga < 0.0001, EdQCarga, 'Informe a quantidade da carga!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('frtfatinfq', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtfatinfq', False, [
  'Codigo', 'CUnid', 'MyTpMed',
  'QCarga'], [
  'Controle'], [
  Codigo, CUnid, MyTpMed,
  QCarga], [
  Controle], True) then
  begin
    ReopanFrtFatItsQ(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      EdControle.ValueVariant  := 0;
      //EdUnidMed.ValueVariant   := 0;
      //EdSigla.ValueVariant     := '';
      //CBUnidMed.KeyValue       := Null;
      RGCUnid.ItemIndex        := RGCUnid.Items.Count -1;
      EdMyTpMed.ValueVariant   := 0;
      CBMyTpMed.KeyValue       := Null;
      EdQCarga.ValueVariant    := 0;
    end else Close;
  end;
end;

procedure TFmFrtFatInfQ.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtFatInfQ.CBUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmFrtFatInfQ.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    UMedi_PF.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmFrtFatInfQ.EdSiglaExit(Sender: TObject);
begin
  UMedi_PF.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmFrtFatInfQ.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger)
end;

procedure TFmFrtFatInfQ.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    UMedi_PF.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmFrtFatInfQ.EdUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmFrtFatInfQ.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtFatInfQ.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTrnsMyTpMed, Dmod.MyDB);
end;

procedure TFmFrtFatInfQ.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtFatInfQ.ReopanFrtFatItsQ(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmFrtFatInfQ.SbCaracAdClick(Sender: TObject);
begin
  Trns_PF.CadastroESelecaoDeTrnsMyTpMed(QrTrnsMyTpMed, EdMyTpMed, CBMyTpMed);
end;

procedure TFmFrtFatInfQ.SpeedButton1Click(Sender: TObject);
begin
  UMedi_PF.CadastraESelecionaUnidMed(VUUnidMed, EdUnidMed, CBUnidMed, QrUnidMed);
end;

end.
