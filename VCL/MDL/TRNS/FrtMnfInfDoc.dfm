object FmFrtMnfInfDoc: TFmFrtMnfInfDoc
  Left = 339
  Top = 185
  Caption = 'MNF-CARGA-004 :: Manifesto - Documentos'
  ClientHeight = 353
  ClientWidth = 782
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 213
    Width = 782
    Height = 26
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 782
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 533
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 782
    Height = 101
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label2: TLabel
      Left = 380
      Top = 16
      Width = 150
      Height = 13
      Caption = 'Munic'#237'pio de descarregamento:'
    end
    object Label197: TLabel
      Left = 12
      Top = 56
      Width = 27
      Height = 13
      Caption = 'Mod.:'
      FocusControl = EdDocMod
    end
    object Label198: TLabel
      Left = 44
      Top = 56
      Width = 27
      Height = 13
      Caption = 'S'#233'rie:'
      FocusControl = EdDocSerie
    end
    object Label199: TLabel
      Left = 80
      Top = 56
      Width = 32
      Height = 13
      Caption = 'N'#186' NF:'
      FocusControl = EdDocNumero
    end
    object Label1: TLabel
      Left = 96
      Top = 16
      Width = 157
      Height = 13
      Caption = 'Chave de acesso do documento:'
    end
    object Label4: TLabel
      Left = 164
      Top = 56
      Width = 138
      Height = 13
      Caption = 'Ve'#237'culo onda estar'#225' a carga:'
    end
    object Label7: TLabel
      Left = 500
      Top = 56
      Width = 157
      Height = 13
      Caption = 'Chave de acesso do documento:'
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMunDescarga: TdmkEditCB
      Left = 380
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MunDescarga'
      UpdCampo = 'MunDescarga'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBMunDescarga
      IgnoraDBLookupComboBox = False
    end
    object CBMunDescarga: TdmkDBLookupComboBox
      Left = 436
      Top = 32
      Width = 333
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsDTB_Munici
      TabOrder = 3
      dmkEditCB = EdMunDescarga
      QryCampo = 'MunDescarga'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdDocMod: TdmkEdit
      Left = 12
      Top = 72
      Width = 29
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'DocMod'
      UpdCampo = 'DocMod'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdDocSerie: TdmkEdit
      Left = 44
      Top = 72
      Width = 33
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'ide_serie'
      UpdCampo = 'DocSerie'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdDocNumero: TdmkEdit
      Left = 80
      Top = 72
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'DocNumero'
      UpdCampo = 'DocNumero'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdTrnsIts: TdmkEditCB
      Left = 164
      Top = 72
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'TrnsIts'
      UpdCampo = 'TrnsIts'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBTrnsIts
      IgnoraDBLookupComboBox = True
    end
    object CBTrnsIts: TdmkDBLookupComboBox
      Left = 221
      Top = 72
      Width = 272
      Height = 21
      KeyField = 'Controle'
      ListField = 'DESCRI'
      ListSource = DsTrnsIts
      TabOrder = 8
      dmkEditCB = EdTrnsIts
      QryCampo = 'TrnsIts'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdDocChave: TdmkEdit
      Left = 96
      Top = 32
      Width = 280
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'DocChave'
      UpdCampo = 'DocChave'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnRedefinido = EdDocChaveRedefinido
    end
    object EdSegCodBarra: TdmkEdit
      Left = 500
      Top = 72
      Width = 269
      Height = 21
      TabOrder = 9
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'SegCodBarra'
      UpdCampo = 'SegCodBarra'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnRedefinido = EdSegCodBarraRedefinido
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 782
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 734
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 686
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 295
        Height = 32
        Caption = 'Manifesto - Documentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 295
        Height = 32
        Caption = 'Manifesto - Documentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 295
        Height = 32
        Caption = 'Manifesto - Documentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 239
    Width = 782
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 778
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 283
    Width = 782
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 636
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 634
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrDTB_Munici: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 196
    Top = 56
    object QrDTB_MuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDTB_MuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsDTB_Munici: TDataSource
    DataSet = QrDTB_Munici
    Left = 196
    Top = 104
  end
  object QrTrnsIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle,'
      'CONCAT(Placa, '#39'  '#39', Nome) DESCRI'
      'FROM trnsits'
      'WHERE tpVeic=0'
      'ORDER BY DESCRI')
    Left = 284
    Top = 52
    object QrTrnsItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTrnsItsDESCRI: TWideStringField
      FieldName = 'DESCRI'
      Size = 255
    end
  end
  object DsTrnsIts: TDataSource
    DataSet = QrTrnsIts
    Left = 284
    Top = 100
  end
end
