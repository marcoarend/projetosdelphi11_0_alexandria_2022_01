object FmFrtFatInfDoc: TFmFrtFatInfDoc
  Left = 339
  Top = 185
  Caption = 'FAT-FRETE-003 :: Faturamento de Frete - Rela'#231#227'o de Documentos'
  ClientHeight = 559
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 414
    Width = 784
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 784
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 2
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label6: TLabel
      Left = 72
      Top = 20
      Width = 36
      Height = 13
      Caption = 'ID item:'
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object EdControle: TdmkEdit
      Left = 72
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 584
        Height = 32
        Caption = 'Faturamento de Frete - Rela'#231#227'o de Documentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 584
        Height = 32
        Caption = 'Faturamento de Frete - Rela'#231#227'o de Documentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 584
        Height = 32
        Caption = 'Faturamento de Frete - Rela'#231#227'o de Documentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 445
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 5
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 279
        Height = 16
        Caption = 'Informe apenas os dados de uma das tr'#234's abas.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 279
        Height = 16
        Caption = 'Informe apenas os dados de uma das tr'#234's abas.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 489
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object PCMyTpDocInf: TPageControl
    Left = 0
    Top = 181
    Width = 784
    Height = 233
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object TabSheet0: TTabSheet
      Caption = 'Indefinido'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object TabSheet1: TTabSheet
      Caption = 'NF-e'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 205
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label15: TLabel
          Left = 4
          Top = 4
          Width = 60
          Height = 13
          Caption = 'Chave NF-e:'
        end
        object SpeedButton1: TSpeedButton
          Left = 281
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
        end
        object Label1: TLabel
          Left = 304
          Top = 4
          Width = 76
          Height = 13
          Caption = 'PIN SUFRAMA:'
        end
        object Label23: TLabel
          Left = 388
          Top = 4
          Width = 120
          Height = 13
          Caption = 'Data prevista de entrega:'
        end
        object EdNFe_Chave: TdmkEdit
          Left = 4
          Top = 20
          Width = 276
          Height = 21
          MaxLength = 44
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'NFe_Chave'
          UpdCampo = 'NFe_Chave'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdNFe_ChaveExit
        end
        object EdNFe_PIN: TdmkEdit
          Left = 304
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'NFe_PIN'
          UpdCampo = 'NFe_PIN'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object TPNFe_dPrev: TdmkEditDateTimePicker
          Left = 388
          Top = 20
          Width = 120
          Height = 21
          Date = 0.655720300899702100
          Time = 0.655720300899702100
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'NFe_dPrev'
          UpdCampo = 'NFe_dPrev'
          UpdType = utYes
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Nota Fiscal (NF modelo 1/1A/Produtor)'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 205
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label2: TLabel
          Left = 148
          Top = 4
          Width = 110
          Height = 13
          Caption = 'N'#186' romaneio (opcional):'
        end
        object Label3: TLabel
          Left = 332
          Top = 4
          Width = 131
          Height = 13
          Caption = 'N'#186' pedido da NF (opcional):'
        end
        object Label4: TLabel
          Left = 516
          Top = 4
          Width = 27
          Height = 13
          Caption = 'S'#233'rie:'
        end
        object Label7: TLabel
          Left = 556
          Top = 4
          Width = 71
          Height = 13
          Caption = 'N'#186' documento:'
        end
        object Label8: TLabel
          Left = 148
          Top = 44
          Width = 82
          Height = 13
          Caption = 'Data da emiss'#227'o:'
        end
        object Label9: TLabel
          Left = 264
          Top = 44
          Width = 73
          Height = 13
          Caption = 'Valor BC ICMS:'
        end
        object Label10: TLabel
          Left = 368
          Top = 44
          Width = 79
          Height = 13
          Caption = 'Valor total ICMS:'
        end
        object Label11: TLabel
          Left = 472
          Top = 44
          Width = 90
          Height = 13
          Caption = 'Valor BC ICMS ST:'
        end
        object Label12: TLabel
          Left = 576
          Top = 44
          Width = 73
          Height = 13
          Caption = 'Valor ICMS ST:'
        end
        object Label13: TLabel
          Left = 148
          Top = 84
          Width = 71
          Height = 13
          Caption = 'Valor produtos:'
        end
        object Label14: TLabel
          Left = 252
          Top = 84
          Width = 67
          Height = 13
          Caption = 'Valor total NF:'
        end
        object Label16: TLabel
          Left = 404
          Top = 84
          Width = 59
          Height = 13
          Caption = 'Peso em kg:'
        end
        object Label17: TLabel
          Left = 360
          Top = 84
          Width = 35
          Height = 13
          Caption = 'CFOP*:'
        end
        object Label18: TLabel
          Left = 152
          Top = 132
          Width = 136
          Height = 13
          Caption = 'CFOP*: CFOP predominante.'
        end
        object Label19: TLabel
          Left = 504
          Top = 84
          Width = 76
          Height = 13
          Caption = 'PIN SUFRAMA:'
        end
        object Label20: TLabel
          Left = 588
          Top = 84
          Width = 120
          Height = 13
          Caption = 'Data prevista de entrega:'
        end
        object EdInfNF_nRoma: TdmkEdit
          Left = 148
          Top = 20
          Width = 180
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'InfNF_nRoma'
          UpdCampo = 'InfNF_nRoma'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdInfNF_nPed: TdmkEdit
          Left = 332
          Top = 20
          Width = 180
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'InfNF_nPed'
          UpdCampo = 'InfNF_nPed'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RGInfNF_mod: TdmkRadioGroup
          Left = 0
          Top = 0
          Width = 141
          Height = 205
          Align = alLeft
          Caption = ' Modelo fiscal :'
          ItemIndex = 0
          Items.Strings = (
            '0-Indefinido'
            '1-NF 1/1A e Avulsa'
            '2-?????'
            '3-?????'
            '4-NF de produtor')
          TabOrder = 0
          QryCampo = 'InfNF_mod'
          UpdCampo = 'InfNF_mod'
          UpdType = utYes
          OldValor = 0
        end
        object EdInfNF_serie: TdmkEdit
          Left = 516
          Top = 20
          Width = 36
          Height = 21
          MaxLength = 3
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'InfNF_serie'
          UpdCampo = 'InfNF_serie'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdInfNF_nDoc: TdmkEdit
          Left = 556
          Top = 20
          Width = 180
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'InfNF_nDoc'
          UpdCampo = 'InfNF_nDoc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object TPInfNF_dEmi: TdmkEditDateTimePicker
          Left = 148
          Top = 60
          Width = 112
          Height = 21
          Date = 0.702857974538346800
          Time = 0.702857974538346800
          TabOrder = 5
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'InfNF_dEmi'
          UpdCampo = 'InfNF_dEmi'
          UpdType = utYes
        end
        object EdInfNF_vBC: TdmkEdit
          Left = 264
          Top = 60
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'InfNF_vBC'
          UpdCampo = 'InfNF_vBC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdInfNF_vICMS: TdmkEdit
          Left = 368
          Top = 60
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'InfNF_vICMS'
          UpdCampo = 'InfNF_vICMS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdInfNF_vBCST: TdmkEdit
          Left = 472
          Top = 60
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'InfNF_vBCST'
          UpdCampo = 'InfNF_vBCST'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdInfNF_vST: TdmkEdit
          Left = 576
          Top = 60
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'InfNF_vST'
          UpdCampo = 'InfNF_vST'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdInfNF_vProd: TdmkEdit
          Left = 148
          Top = 100
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'InfNF_vProd'
          UpdCampo = 'InfNF_vProd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdInfNF_vNF: TdmkEdit
          Left = 252
          Top = 100
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'InfNF_vNF'
          UpdCampo = 'InfNF_vNF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdInfNF_nPeso: TdmkEdit
          Left = 404
          Top = 100
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'InfNF_nPeso'
          UpdCampo = 'InfNF_nPeso'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdInfNF_nCFOP: TdmkEdit
          Left = 356
          Top = 100
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000'
          QryCampo = 'InfNF_nCFOP'
          UpdCampo = 'InfNF_nCFOP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdInfNF_PIN: TdmkEdit
          Left = 504
          Top = 100
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 14
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'InfNF_PIN'
          UpdCampo = 'InfNF_PIN'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object TPInfNF_dPrev: TdmkEditDateTimePicker
          Left = 588
          Top = 100
          Width = 120
          Height = 21
          Date = 0.655720300899702100
          Time = 0.655720300899702100
          TabOrder = 15
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'InfNF_dPrev'
          UpdCampo = 'InfNF_dPrev'
          UpdType = utYes
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Outro ducumento (Documento, Dutoviario, Outro)'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 205
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label21: TLabel
          Left = 148
          Top = 4
          Width = 71
          Height = 13
          Caption = 'N'#186' documento:'
        end
        object Label22: TLabel
          Left = 332
          Top = 4
          Width = 82
          Height = 13
          Caption = 'Data da emiss'#227'o:'
        end
        object Label24: TLabel
          Left = 448
          Top = 4
          Width = 98
          Height = 13
          Caption = 'Valor do documento:'
        end
        object Label25: TLabel
          Left = 552
          Top = 4
          Width = 120
          Height = 13
          Caption = 'Data prevista de entrega:'
        end
        object Label26: TLabel
          Left = 148
          Top = 44
          Width = 195
          Height = 13
          Caption = 'Descri'#231#227'o quando se tratar de 99-Outros:'
        end
        object RGInfOutros_tpDoc: TdmkRadioGroup
          Left = 0
          Top = 0
          Width = 141
          Height = 205
          Align = alLeft
          Caption = ' Tipo de documento:'
          ItemIndex = 0
          Items.Strings = (
            '??-Indefinido'
            '00-Declara'#231#227'o'
            '10-Dutovi'#225'rio'
            '99-Outros')
          TabOrder = 0
          QryCampo = 'InfOutros_tpDoc'
          UpdCampo = 'InfOutros_tpDoc'
          UpdType = utYes
          OldValor = 0
        end
        object EdInfOutros_nDoc: TdmkEdit
          Left = 148
          Top = 20
          Width = 180
          Height = 21
          MaxLength = 20
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'InfOutros_nDoc'
          UpdCampo = 'InfOutros_nDoc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object TPInfOutros_dEmi: TdmkEditDateTimePicker
          Left = 332
          Top = 20
          Width = 112
          Height = 21
          Date = 0.702857974538346800
          Time = 0.702857974538346800
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'InfOutros_dEmi'
          UpdCampo = 'InfOutros_dEmi'
          UpdType = utYes
        end
        object EdInfOutros_vDocFisc: TdmkEdit
          Left = 448
          Top = 20
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'InfOutros_vDocFisc'
          UpdCampo = 'InfOutros_vDocFisc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object TPInfOutros_dPrev: TdmkEditDateTimePicker
          Left = 552
          Top = 20
          Width = 120
          Height = 21
          Date = 0.655720300899702100
          Time = 0.655720300899702100
          TabOrder = 4
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'InfOutros_dPrev'
          UpdCampo = 'InfOutros_dPrev'
          UpdType = utYes
        end
        object EdInfOutros_descOutros: TdmkEdit
          Left = 148
          Top = 60
          Width = 525
          Height = 21
          MaxLength = 100
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'InfOutros_descOutros'
          UpdCampo = 'InfOutros_descOutros'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
  end
  object RGMyTpDocInf: TdmkRadioGroup
    Left = 0
    Top = 112
    Width = 784
    Height = 69
    Align = alTop
    Caption = ' Tipo de Documento: '
    Columns = 3
    Items.Strings = (
      'Indefinido'
      'NF-e'
      'Nota Fiscal (NF modelo 1/1A/Produtor)'
      'Outro ducumento (Documento, Dutoviario, Outro)')
    TabOrder = 6
    OnClick = RGMyTpDocInfClick
    QryCampo = 'MyTpDocInf'
    UpdCampo = 'MyTpDocInf'
    UpdType = utYes
    OldValor = 0
  end
end
