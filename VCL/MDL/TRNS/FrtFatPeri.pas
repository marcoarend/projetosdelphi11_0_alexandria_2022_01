unit FrtFatPeri;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmFrtFatPeri = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    VUUnidMed: TdmkValUsu;
    QrProdutCad: TmySQLQuery;
    QrProdutCadCodigo: TIntegerField;
    QrProdutCadNome: TWideStringField;
    DsProdutCad: TDataSource;
    Label2: TLabel;
    EdProdutCad: TdmkEditCB;
    CBProdutCad: TdmkDBLookupComboBox;
    SbCaracAd: TSpeedButton;
    EdqTotProd: TdmkEdit;
    Label4: TLabel;
    EdqVolTipo: TdmkEdit;
    Label1: TLabel;
    EdgrEmb: TdmkEdit;
    Label3: TLabel;
    QrProdutCadnONU: TWideStringField;
    Label7: TLabel;
    EdnONU: TdmkEdit;
    QrProdutCadGrEmb: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbCaracAdClick(Sender: TObject);
    procedure EdProdutCadRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ReopanFrtFatPeri(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtFatPeri: TFmFrtFatPeri;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnMySQLCuringa, UnUMedi_PF, UnidMed, UnFrt_PF;

{$R *.DFM}

procedure TFmFrtFatPeri.BtOKClick(Sender: TObject);
var
  qTotProd, qVolTipo, grEmb: String;
  Codigo, Controle, ProdutCad: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  ProdutCad      := EdProdutCad.ValueVariant;
  qTotProd       := EdqTotProd.Text;
  qVolTipo       := EdqVolTipo.Text;
  grEmb          := EdgrEmb.Text;
  //
  if MyObjects.FIC(ProdutCad = 0, EdProdutCad, 'Informe o produto perigoso!') then
    Exit;
  if MyObjects.FIC(Trim(QrProdutCadnOnu.Value) = '', EdProdutCad,
    'O produto informedo n�o tem n�mero ONU/UN informado!') then
    Exit;
  Controle := UMyMod.BPGS1I32('frtfatperi', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtfatperi', False, [
  'Codigo', 'ProdutCad', 'qTotProd',
  'qVolTipo', 'grEmb'], [
  'Controle'], [
  Codigo, ProdutCad, qTotProd,
  qVolTipo, grEmb], [
  Controle], True) then
  begin
    ReopanFrtFatPeri(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      EdControle.ValueVariant  := 0;
      EdProdutCad.ValueVariant := 0;
      CBProdutCad.KeyValue     := Null;
      EdqTotProd.Text          := '';
      EdqVolTipo.Text          := '';
      EdgrEmb.Text             := '';
      //
      EdProdutCad.SetFocus;
    end else Close;
  end;
end;

procedure TFmFrtFatPeri.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtFatPeri.EdProdutCadRedefinido(Sender: TObject);
begin
  EdnONU.Text  := QrProdutCadnONU.Value;
  EdgrEmb.Text := QrProdutCadGrEmb.Value;
end;

procedure TFmFrtFatPeri.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtFatPeri.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrProdutCad, Dmod.MyDB);
end;

procedure TFmFrtFatPeri.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtFatPeri.ReopanFrtFatPeri(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmFrtFatPeri.SbCaracAdClick(Sender: TObject);
begin
  Frt_PF.CadastroESelecaoDeProdutCad(QrProdutCad, EdProdutCad, CBProdutCad);
end;

end.
