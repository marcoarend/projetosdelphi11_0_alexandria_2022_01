unit TrnsIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmTrnsIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNO_ENT: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    CB_Sel_: TdmkDBLookupComboBox;
    Ed_Sel_: TdmkEditCB;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Qr_Sel_: TmySQLQuery;
    Qr_Sel_Codigo: TIntegerField;
    Qr_Sel_CodUsu: TIntegerField;
    Qr_Sel_Nome: TWideStringField;
    Ds_Sel_: TDataSource;
    VU_Sel_: TdmkValUsu;
    EdRENAVAM: TdmkEdit;
    Label2: TLabel;
    Label4: TLabel;
    EdPlaca: TdmkEdit;
    Label8: TLabel;
    EdTaraKg: TdmkEdit;
    Label9: TLabel;
    EdCapKg: TdmkEdit;
    RGTpVeic: TdmkRadioGroup;
    RGTpRod: TdmkRadioGroup;
    RGTpCar: TdmkRadioGroup;
    EdCapM3: TdmkEdit;
    Label10: TLabel;
    EdUF: TdmkEdit;
    Label11: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenTrnsIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmTrnsIts: TFmTrnsIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmTrnsIts.BtOKClick(Sender: TObject);
var
  Nome, RENAVAM, Placa, UF: String;
  TamRena, Codigo, Controle, TaraKg, CapKg, TpVeic, TpRod, TpCar, CapM3: Integer;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Nome           := EdNome.Text;
  RENAVAM        := EdRENAVAM.ValueVariant;
  Placa          := EdPlaca.ValueVariant;
  TaraKg         := EdTaraKg.ValueVariant;
  CapKg          := EdCapKg.ValueVariant;
  TpVeic         := RGTpVeic.ItemIndex;
  TpRod          := RGTpRod.ItemIndex;
  TpCar          := RGTpCar.ItemIndex;
  CapM3          := EdCapM3.ValueVariant;
  UF             := EdUF.Text;
  //
  TamRena := Length(RENAVAM);
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Informe o nome!') then Exit;
  if MyObjects.FIC((TamRena < 9) or (TamRena > 11), EdRENAVAM,
    'RENAVAM deve ter entre 9 e 11 caracteres!') then Exit;
  if MyObjects.FIC(Length(Placa) <> 7, EdPlaca,
    'Placa deve ter 7 caracteres!') then Exit;
  //
  Controle := UMyMod.BPGS1I32('trnsits', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'trnsits', False, [
  'Codigo', 'Nome', 'RENAVAM',
  'Placa', 'TaraKg', 'CapKg',
  'TpVeic', 'TpRod', 'TpCar',
  'CapM3', 'UF'], [
  'Controle'], [
  Codigo, Nome, RENAVAM,
  Placa, TaraKg, CapKg,
  TpVeic, TpRod, TpCar,
  CapM3, UF], [
  Controle], True) then
  begin
    ReopenTrnsIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdNome.Text              := '';
      EdRENAVAM.ValueVariant   := '';
      EdPlaca.ValueVariant     := '';
      EdTaraKg.ValueVariant    := 0;
      EdCapKg.ValueVariant     := 0;
      RGTpVeic.ItemIndex       := -1;
      RGTpRod.ItemIndex        := -1;
      RGTpCar.ItemIndex        := -1;
      Ed_SEL_.ValueVariant     := 0;
      CB_SEL_.KeyValue         := Null;
      EdCapM3.ValueVariant     := 0;
      EdUF.ValueVariant        := '';
      //
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmTrnsIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTrnsIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNO_ENT.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmTrnsIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //UnDmkDAC_PF.AbreQuery(Qr_Sel_, Dmod.MyDB?);
end;

procedure TFmTrnsIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTrnsIts.ReopenTrnsIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTrnsIts.SpeedButton1Click(Sender: TObject);
begin
(*
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFm_CadSel_, Fm_CadSel_, afmoNegarComAviso) then
  begin
    Fm_CadSel_.ShowModal;
    Fm_CadSel_.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdSel, CBSel, QrSel, VAR_CADASTRO);
*)
end;

end.
