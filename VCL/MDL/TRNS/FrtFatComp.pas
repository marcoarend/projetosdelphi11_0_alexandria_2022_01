unit FrtFatComp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmFrtFatComp = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrFrtSrvCad: TmySQLQuery;
    QrFrtSrvCadCodigo: TIntegerField;
    QrFrtSrvCadNome: TWideStringField;
    DsFrtSrvCad: TDataSource;
    Label2: TLabel;
    EdCodComp: TdmkEditCB;
    CBCodComp: TdmkDBLookupComboBox;
    SbCaracAd: TSpeedButton;
    EdvComp: TdmkEdit;
    Label4: TLabel;
    QrFrtSrvCadValor: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbCaracAdClick(Sender: TObject);
    procedure EdCodCompChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FCriando: Boolean;
    procedure ReopanFrtFatComp(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtFatComp: TFmFrtFatComp;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnMySQLCuringa, UnFrt_PF;

{$R *.DFM}

procedure TFmFrtFatComp.BtOKClick(Sender: TObject);
var
  xNome: String;
  Codigo, Controle, CodComp: Integer;
  vComp: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  CodComp        := EdCodComp.ValueVariant;
  xNome          := CBCodComp.Text;
  vComp          := EdvComp.ValueVariant;
  //
  if MyObjects.FIC(CodComp < 0, EdCodComp, 'Informe o componente do servi�o!') then
    Exit;
  if VComp < 0.0001 then
  begin
    if Geral.MB_Pergunta(
    'Valor do componente n�o definido. Deseja continuar assim mesmo?'
    ) <> ID_YES then
      Exit;
  end;
  //
  Controle := UMyMod.BPGS1I32('frtfatcomp', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtfatcomp', False, [
  'Codigo', 'CodComp', 'xNome',
  'vComp'], [
  'Controle'], [
  Codigo, CodComp, xNome,
  vComp], [
  Controle], True) then
  begin
    ReopanFrtFatComp(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      EdControle.ValueVariant  := 0;
      EdCodComp.ValueVariant   := 0;
      CBCodComp.KeyValue       := Null;
      EdvComp.ValueVariant     := 0;
    end else Close;
  end;
end;

procedure TFmFrtFatComp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtFatComp.EdCodCompChange(Sender: TObject);
begin
  if FCriando then
    Exit;
  EdvComp.ValueVariant := QrFrtSrvCadValor.Value;
end;

procedure TFmFrtFatComp.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtFatComp.FormCreate(Sender: TObject);
begin
  FCriando := True;
  ImgTipo.SQLType := stLok;
  //
  //UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFrtSrvCad, Dmod.MyDB);
end;

procedure TFmFrtFatComp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtFatComp.FormShow(Sender: TObject);
begin
  FCriando := False;
end;

procedure TFmFrtFatComp.ReopanFrtFatComp(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmFrtFatComp.SbCaracAdClick(Sender: TObject);
begin
  Frt_PF.CadastroESelecaoDeFrtSrvCad(QrFrtSrvCad, EdCodComp, CBCodComp);
end;

end.
