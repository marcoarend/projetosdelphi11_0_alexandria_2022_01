unit FrtRegFisC;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, UnCTeListas, dmkDBLookupComboBox, dmkEditCB,
  Variants;

type
  TFmFrtRegFisC = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrFrtRegFisC: TmySQLQuery;
    DsFrtRegFisC: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrFrtRegFisCNO_TpCTe: TWideStringField;
    QrFrtRegFisCNO_MyCST: TWideStringField;
    QrFrtRegFisCCodigo: TIntegerField;
    QrFrtRegFisCNome: TWideStringField;
    QrFrtRegFisCNatOp: TWideStringField;
    QrFrtRegFisCTpCTe: TSmallintField;
    QrFrtRegFisCMyCST: TSmallintField;
    QrFrtRegFisCLk: TIntegerField;
    QrFrtRegFisCDataCad: TDateField;
    QrFrtRegFisCDataAlt: TDateField;
    QrFrtRegFisCUserCad: TIntegerField;
    QrFrtRegFisCUserAlt: TIntegerField;
    QrFrtRegFisCAlterWeb: TSmallintField;
    QrFrtRegFisCAtivo: TSmallintField;
    Label3: TLabel;
    EdNatOp: TdmkEdit;
    QrCFOP: TmySQLQuery;
    QrCFOPCodigo: TWideStringField;
    QrCFOPNome: TWideStringField;
    QrCFOPDescricao: TWideMemoField;
    QrCFOPComplementacao: TWideMemoField;
    DsCFOP: TDataSource;
    Label4: TLabel;
    EdCFOP: TdmkEditCB;
    CBCFOP: TdmkDBLookupComboBox;
    SbCFOP: TSpeedButton;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    QrFrtRegFisCNO_CFOP: TWideStringField;
    DBEdit2: TDBEdit;
    RGTpCTe: TdmkRadioGroup;
    RGMyCST: TdmkRadioGroup;
    RGDBTpCTe: TDBRadioGroup;
    RGDBMyCST: TDBRadioGroup;
    QrFrtRegFisCCFOP: TWideStringField;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EdICMS_pICMS: TdmkEdit;
    EdICMS_pRedBC: TdmkEdit;
    EdICMS_pICMSSTRet: TdmkEdit;
    EdICMS_pRedBCOutraUF: TdmkEdit;
    EdICMS_pICMSOutraUF: TdmkEdit;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    RGiTotTrib_Fonte: TdmkRadioGroup;
    PnIBPTax: TPanel;
    RGiTotTrib_Tabela: TdmkRadioGroup;
    Label14: TLabel;
    EdiTotTrib_Ex: TdmkEdit;
    BtiTotTrib: TBitBtn;
    Label15: TLabel;
    EdiTotTrib_Codigo: TdmkEdit;
    QrFrtRegFisCICMS_pICMS: TFloatField;
    QrFrtRegFisCICMS_pRedBC: TFloatField;
    QrFrtRegFisCICMS_pICMSSTRet: TFloatField;
    QrFrtRegFisCICMS_pRedBCOutraUF: TFloatField;
    QrFrtRegFisCICMS_pICMSOutraUF: TFloatField;
    QrFrtRegFisCiTotTrib_Fonte: TIntegerField;
    QrFrtRegFisCiTotTrib_Tabela: TIntegerField;
    QrFrtRegFisCiTotTrib_Ex: TIntegerField;
    QrFrtRegFisCiTotTrib_Codigo: TLargeintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFrtRegFisCAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFrtRegFisCBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbCFOPClick(Sender: TObject);
    procedure RGiTotTrib_FonteClick(Sender: TObject);
    procedure BtiTotTribClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmFrtRegFisC: TFmFrtRegFisC;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, CFOP2003, MyDBCheck, DmkDAC_PF, IBPTaxCad;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFrtRegFisC.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFrtRegFisC.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFrtRegFisCCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFrtRegFisC.DefParams;
var
  ATT_CTeTpCTe, ATT_CTeMyCST: String;
begin
  ATT_CTeTpCTe := dmkPF.ArrayToTexto('fis.TpCTe', 'NO_TpCTe', pvPos, True,
    sCTeTpCTe);
  ATT_CTeMyCST := dmkPF.ArrayToTexto('fis.TpCTe', 'NO_MyCST', pvPos, True,
    sCTeMyCST);
  //
  VAR_GOTOTABELA := 'frtregfisc';
  VAR_GOTOMYSQLTABLE := QrFrtRegFisC;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add(ATT_CTeTpCTe);
  VAR_SQLx.Add(ATT_CTeMyCST);
  VAR_SQLx.Add('cfp.Nome NO_CFOP, ');
  VAR_SQLx.Add('fis.* ');
  VAR_SQLx.Add('FROM frtregfisc fis');
  VAR_SQLx.Add('LEFT JOIN cfop2003 cfp ON cfp.Codigo=fis.CFOP ');
  VAR_SQLx.Add('WHERE fis.Codigo > 0');
  //
  VAR_SQL1.Add('AND fis.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND fis.Nome Like :P0');
  //
end;

procedure TFmFrtRegFisC.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFrtRegFisC.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFrtRegFisC.RGiTotTrib_FonteClick(Sender: TObject);
begin
  PnIBPTax.Visible := RGiTotTrib_Fonte.ItemIndex = 1;
end;

procedure TFmFrtRegFisC.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFrtRegFisC.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFrtRegFisC.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFrtRegFisC.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFrtRegFisC.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFrtRegFisC.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtRegFisC.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFrtRegFisC, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'frtregfisc');
end;

procedure TFmFrtRegFisC.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFrtRegFisCCodigo.Value;
  Close;
end;

procedure TFmFrtRegFisC.BtConfirmaClick(Sender: TObject);
var
  Nome, NatOp, CFOP: String;
  Codigo, TpCTe, MyCST, iTotTrib_Fonte, iTotTrib_Tabela, iTotTrib_Ex: Integer;
  ICMS_pICMS, ICMS_pRedBC, ICMS_pICMSSTRet, ICMS_pRedBCOutraUF, ICMS_pICMSOutraUF, iTotTrib_Codigo: Double;
  SQLType: TSQLType;
begin
  SQLType            := ImgTipo.SQLType;
  Codigo             := EdCodigo.ValueVariant;
  Nome               := EdNome.ValueVariant;
  NatOp              := EdNatOp.ValueVariant;
  if CBCFOP.KeyValue = Null then
    CFOP             := ''
  else
    CFOP             := EdCFOP.Text;
  TpCTe              := RGTpCTe.ItemIndex;
  MyCST              := RGMyCST.ItemIndex;
  //
  ICMS_pICMS         := EdICMS_pICMS.ValueVariant;
  ICMS_pRedBC        := EdICMS_pRedBC.ValueVariant;
  ICMS_pICMSSTRet    := EdICMS_pICMSSTRet.ValueVariant;
  ICMS_pRedBCOutraUF := EdICMS_pRedBCOutraUF.ValueVariant;
  ICMS_pICMSOutraUF  := EdICMS_pICMSOutraUF.ValueVariant;
  iTotTrib_Fonte     := RGiTotTrib_Fonte.ItemIndex;
  iTotTrib_Tabela    := RGiTotTrib_Tabela.ItemIndex;
  iTotTrib_Ex        := EdiTotTrib_Ex.ValueVariant;
  iTotTrib_Codigo    := EdiTotTrib_Codigo.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Trim(CFOP) = '', EdCFOP, 'Informe o CFOP!') then Exit;
  if MyObjects.FIC(MyCST < 0, RGMyCST, 'Defina o CST!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('frtregfisc', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtregfisc', False, [
  'Nome', 'NatOp', 'CFOP',
  'TpCTe', 'MyCST', 'ICMS_pICMS',
  'ICMS_pRedBC', 'ICMS_pICMSSTRet', 'ICMS_pRedBCOutraUF',
  'ICMS_pICMSOutraUF', 'iTotTrib_Fonte', 'iTotTrib_Tabela',
  'iTotTrib_Ex', 'iTotTrib_Codigo'], [
  'Codigo'], [
  Nome, NatOp, CFOP,
  TpCTe, MyCST, ICMS_pICMS,
  ICMS_pRedBC, ICMS_pICMSSTRet, ICMS_pRedBCOutraUF,
  ICMS_pICMSOutraUF, iTotTrib_Fonte, iTotTrib_Tabela,
  iTotTrib_Ex, iTotTrib_Codigo], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmFrtRegFisC.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'frtregfisc', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFrtRegFisC.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFrtRegFisC, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'frtregfisc');
  RGiTotTrib_Fonte.ItemIndex := 1;
  RGiTotTrib_Tabela.ItemIndex := 1;
  EdiTotTrib_Ex.ValueVariant := 0;
  EdiTotTrib_Codigo.ValueVariant := 105011900; // Servi�os de transporte rodovi�rio de outros tipos de carga
end;

procedure TFmFrtRegFisC.BtiTotTribClick(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmIBPTaxCad, FmIBPTaxCad, afmoLiberado) then
  begin
    FmIBPTaxCad.ShowModal;
    if VAR_CADASTRO <> 0 then
    begin
      RGiTotTrib_Tabela.ItemIndex    := FmIBPTaxCad.QrIBPTaxTabela.Value;
      EdiTotTrib_Ex.ValueVariant     := FmIBPTaxCad.QrIBPTaxEx.Value;
      EdiTotTrib_Codigo.ValueVariant := FmIBPTaxCad.QrIBPTaxCodigo.Value;
    end;
    FmIBPTaxCad.Destroy;
  end;
{$EndIf}
end;

procedure TFmFrtRegFisC.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrCFOP, Dmod.MyDB);
end;

procedure TFmFrtRegFisC.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFrtRegFisCCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFrtRegFisC.SbCFOPClick(Sender: TObject);
begin
//{$IfNDef NAO_GFAT}
  VAR_CADTEXTO := '';
  if DBCheck.CriaFm(TFmCFOP2003, FmCFOP2003, afmoNegarComAviso) then
  begin
    FmCFOP2003.ShowModal;
    FmCFOP2003.Destroy;
    if VAR_CADTEXTO <> '' then
    begin
      QrCFOP.Close;
      UnDmkDAC_PF.AbreQuery(QrCFOP, Dmod.MyDB);
      EdCFOP.ValueVariant := VAR_CADTEXTO;
      CBCFOP.KeyValue     := VAR_CADTEXTO;
    end;
  end;
(*
{$Else}
  dmkPF.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
*)
end;

procedure TFmFrtRegFisC.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFrtRegFisC.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFrtRegFisCCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFrtRegFisC.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFrtRegFisC.QrFrtRegFisCAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFrtRegFisC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFrtRegFisC.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFrtRegFisCCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'frtregfisc', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFrtRegFisC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtRegFisC.QrFrtRegFisCBeforeOpen(DataSet: TDataSet);
begin
  QrFrtRegFisCCodigo.DisplayFormat := FFormatFloat;
end;

end.

