unit FrtRegFatC;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, UnCTeListas, dmkEditCB, dmkDBLookupComboBox,
  dmkMemo, Variants;

type
  TFmFrtRegFatC = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrFrtRegFatC: TmySQLQuery;
    DsFrtRegFatC: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrFrtRegFatCCodigo: TIntegerField;
    QrFrtRegFatCNome: TWideStringField;
    QrFrtRegFatCForPag: TSmallintField;
    QrFrtRegFatCModal: TSmallintField;
    QrFrtRegFatCTpServ: TSmallintField;
    QrFrtRegFatCCMunIni: TIntegerField;
    QrFrtRegFatCUFIni: TWideStringField;
    QrFrtRegFatCCMunFim: TIntegerField;
    QrFrtRegFatCUFFim: TWideStringField;
    QrFrtRegFatCToma: TSmallintField;
    QrFrtRegFatCCaracAd: TIntegerField;
    QrFrtRegFatCCaracSer: TIntegerField;
    QrFrtRegFatCxObs: TWideMemoField;
    QrFrtRegFatCLk: TIntegerField;
    QrFrtRegFatCDataCad: TDateField;
    QrFrtRegFatCDataAlt: TDateField;
    QrFrtRegFatCUserCad: TIntegerField;
    QrFrtRegFatCUserAlt: TIntegerField;
    QrFrtRegFatCAlterWeb: TSmallintField;
    QrFrtRegFatCAtivo: TSmallintField;
    RGForPag: TdmkRadioGroup;
    RGModal: TdmkRadioGroup;
    RGTpServ: TdmkRadioGroup;
    QrMunIni: TmySQLQuery;
    QrMunIniCodigo: TIntegerField;
    QrMunIniNome: TWideStringField;
    DsMunIni: TDataSource;
    QrMunFim: TmySQLQuery;
    QrMunFimCodigo: TIntegerField;
    QrMunFimNome: TWideStringField;
    DsMunFim: TDataSource;
    GroupBox1: TGroupBox;
    CBCMunIni: TdmkDBLookupComboBox;
    Label24: TLabel;
    EdUFIni: TdmkEdit;
    EdCMunIni: TdmkEditCB;
    Label105: TLabel;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    CBCMunFim: TdmkDBLookupComboBox;
    EdUFFim: TdmkEdit;
    EdCMunFim: TdmkEditCB;
    RGToma: TdmkRadioGroup;
    QrCaracAd: TmySQLQuery;
    DsCaracAd: TDataSource;
    QrCaracAdCodigo: TIntegerField;
    QrCaracAdNome: TWideStringField;
    EdCaracAd: TdmkEditCB;
    Label5: TLabel;
    CBCaracAd: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdCaracSer: TdmkEditCB;
    CBCaracSer: TdmkDBLookupComboBox;
    QrCaracSer: TmySQLQuery;
    DsCaracSer: TDataSource;
    QrCaracSerCodigo: TIntegerField;
    QrCaracSerNome: TWideStringField;
    Label8: TLabel;
    MexObs: TdmkMemo;
    QrFrtRegFatCNO_MunFim: TWideStringField;
    QrFrtRegFatCNO_MunIni: TWideStringField;
    SbCaracAd: TSpeedButton;
    SbCaracSer: TSpeedButton;
    GroupBox3: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    CBCMunEnv: TdmkDBLookupComboBox;
    EdUFEnv: TdmkEdit;
    EdCMunEnv: TdmkEditCB;
    QrMunEnv: TmySQLQuery;
    DsMunEnv: TDataSource;
    QrMunEnvCodigo: TIntegerField;
    QrMunEnvNome: TWideStringField;
    QrFrtRegFatCNO_MunEnv: TWideStringField;
    QrFrtRegFatCCMunEnv: TIntegerField;
    QrFrtRegFatCUFEnv: TWideStringField;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    DsPediPrzCab: TDataSource;
    QrCartEmiss: TmySQLQuery;
    QrCartEmissCodigo: TIntegerField;
    QrCartEmissNome: TWideStringField;
    DsCartEmiss: TDataSource;
    QrFrtPrcCad: TmySQLQuery;
    DsFrtPrcCad: TDataSource;
    GroupBox4: TGroupBox;
    BtTabelaPrc: TSpeedButton;
    LaTabelaPrc: TLabel;
    BtCondicaoPG: TSpeedButton;
    Label12: TLabel;
    LaCondicaoPG: TLabel;
    SpeedButton13: TSpeedButton;
    CBTabelaPrc: TdmkDBLookupComboBox;
    EdTabelaPrc: TdmkEditCB;
    CBCartEmiss: TdmkDBLookupComboBox;
    EdCartEmiss: TdmkEditCB;
    CBCondicaoPG: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    QrFrtPrcCadCodigo: TIntegerField;
    QrFrtPrcCadNome: TWideStringField;
    QrFrtRegFatCTabelaPrc: TIntegerField;
    QrFrtRegFatCCondicaoPg: TIntegerField;
    QrFrtRegFatCCartEmiss: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFrtRegFatCAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFrtRegFatCBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure EdUFIniRedefinido(Sender: TObject);
    procedure EdUFFimRedefinido(Sender: TObject);
    procedure SbCaracAdClick(Sender: TObject);
    procedure SbCaracSerClick(Sender: TObject);
    procedure EdUFEnvRedefinido(Sender: TObject);
    procedure BtTabelaPrcClick(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure BtCondicaoPGClick(Sender: TObject);
    procedure MexObsKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmFrtRegFatC: TFmFrtRegFatC;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, CfgCadLista, UnFinanceiroJan,
  UnFrt_PF, UnPraz_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFrtRegFatC.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFrtRegFatC.MexObsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 13) then
  begin
    if (not (ssCtrl in Shift)) then
    begin
      EdTabelaPrc.SetFocus;
      Key := 0;
    end;
  end;
end;

procedure TFmFrtRegFatC.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFrtRegFatCCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFrtRegFatC.DefParams;
var
  ATT_CTeForPag, ATT_CTeModal, ATT_CTeTpServ, ATT_CTeToma: String;
begin
  ATT_CTeForPag := dmkPF.ArrayToTexto('fat.ForPag', 'NO_ForPag', pvPos, True,
    sCTeForPag);
  ATT_CTeModal := dmkPF.ArrayToTexto('fat.Modal', 'NO_Modal', pvPos, True,
    sCTeModal);
  ATT_CTeTpServ := dmkPF.ArrayToTexto('fat.TpServ', 'NO_TpServ', pvPos, True,
    sCTeTpServ);
  ATT_CTeToma := dmkPF.ArrayToTexto('fat.Toma', 'NO_Toma', pvPos, True,
    sCTeToma);
  //
  VAR_GOTOTABELA := 'frtregfatc';
  VAR_GOTOMYSQLTABLE := QrFrtRegFatC;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT fca.Nome NO_CaracAd, fcs.Nome NO_CaracSer, ');
  VAR_SQLx.Add('mui.Nome NO_MunIni, muf.Nome NO_MunFim, mue.Nome NO_MunEnv,');
  VAR_SQLx.Add(ATT_CTeForPag);
  VAR_SQLx.Add(ATT_CTeModal);
  VAR_SQLx.Add(ATT_CTeTpServ);
  VAR_SQLx.Add(ATT_CTeToma);
  VAR_SQLx.Add('fat.* ');
  VAR_SQLx.Add('FROM frtregfatc fat');
  VAR_SQLx.Add('LEFT JOIN frtcaracad fca ON fca.Codigo=caracad ');
  VAR_SQLx.Add('LEFT JOIN frtcaracser fcs ON fcs.Codigo=caracser ');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mui ON mui.Codigo=fat.CMunIni');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici muf ON muf.Codigo=fat.CMunFim');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mue ON mue.Codigo=fat.CMunEnv');
  VAR_SQLx.Add('WHERE fat.Codigo > 0');
  //
  VAR_SQL1.Add('AND fat.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND fat.Nome Like :P0');
  //
end;

procedure TFmFrtRegFatC.EdUFEnvRedefinido(Sender: TObject);
begin
  DModG.ReopenMunici(QrMunEnv, EdUFEnv.ValueVariant);
end;

procedure TFmFrtRegFatC.EdUFFimRedefinido(Sender: TObject);
begin
  DModG.ReopenMunici(QrMunFim, EdUFFim.ValueVariant);
  //
end;

procedure TFmFrtRegFatC.EdUFIniRedefinido(Sender: TObject);
begin
  DModG.ReopenMunici(QrMunIni, EdUFIni.ValueVariant);
end;

procedure TFmFrtRegFatC.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFrtRegFatC.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFrtRegFatC.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFrtRegFatC.SpeedButton13Click(Sender: TObject);
begin
  FinanceiroJan.CadastroESelecaoDeCarteira(QrCartEmiss, EdCartEmiss, CBCartEmiss);
end;

procedure TFmFrtRegFatC.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFrtRegFatC.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFrtRegFatC.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFrtRegFatC.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFrtRegFatC.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtRegFatC.BtTabelaPrcClick(Sender: TObject);
begin
  Frt_PF.CadastroESelecaoDeFrtPrcCad(QrFrtPrcCad, EdTabelaPrc, CBTabelaPrc);
end;

procedure TFmFrtRegFatC.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFrtRegFatC, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'frtregfatc');
end;

procedure TFmFrtRegFatC.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFrtRegFatCCodigo.Value;
  Close;
end;

procedure TFmFrtRegFatC.BtCondicaoPGClick(Sender: TObject);
begin
  Praz_PF.CadastroESelecaoDePediPrzCab1(QrPediPrzCab, EdCondicaoPg, CBCondicaoPg);
end;

procedure TFmFrtRegFatC.BtConfirmaClick(Sender: TObject);
var
  Nome, UFEnv, UFIni, UFFim, xObs: String;
  Codigo, ForPag, Modal, TpServ, CMunIni, CMunFim, Toma, CaracAd, CaracSer,
  CMunEnv, TabelaPrc, CondicaoPg, CartEmiss: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  ForPag         := RGForPag.ItemIndex;
  Modal          := RGModal.ItemIndex;
  TpServ         := RGTpServ.ItemIndex;
  CMunIni        := EdCMunIni.ValueVariant;
  UFIni          := EdUFIni.ValueVariant;
  CMunFim        := EdCMunFim.ValueVariant;
  UFFim          := EdUFFim.ValueVariant;
  CMunEnv        := EdCMunEnv.ValueVariant;
  UFEnv          := EdUFEnv.ValueVariant;
  Toma           := RGToma.ItemIndex;
  CaracAd        := EdCaracAd.ValueVariant;
  CaracSer       := EdCaracSer.ValueVariant;
  xObs           := MexObs.Text;
  TabelaPrc      := EdTabelaPrc.ValueVariant;
  CondicaoPg     := EdCondicaoPg.ValueVariant;
  CartEmiss      := EdCartEmiss.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Modal=0, RGModal, 'Defina o Modal!') then Exit;
  if MyObjects.FIC(Modal<>1, RGModal, 'Modal n�o implementado! SOLICITE � DERMATEK') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('frtregfatc', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtregfatc', False, [
  'Nome', 'ForPag', 'Modal',
  'TpServ', 'CMunEnv', 'UFEnv',
  'CMunIni', 'UFIni', 'CMunFim',
  'UFFim', 'Toma', 'CaracAd',
  'CaracSer', 'xObs', 'TabelaPrc',
  'CondicaoPg', 'CartEmiss'], [
  'Codigo'], [
  Nome, ForPag, Modal,
  TpServ, CMunEnv, UFEnv,
  CMunIni, UFIni, CMunFim,
  UFFim, Toma, CaracAd,
  CaracSer, xObs, TabelaPrc,
  CondicaoPg, CartEmiss], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmFrtRegFatC.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'frtregfatc', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFrtRegFatC.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFrtRegFatC, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'frtregfatc');
end;

procedure TFmFrtRegFatC.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrMunIni, DmodG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrMunFim, DmodG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrMunEnv, DmodG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrCaracAd, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCaracSer, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCartEmiss, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzCab, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome, MaxDesco, ',
  'JurosMes, Parcelas, Percent1, Percent2 ',
  'FROM pediprzcab ',
  'WHERE Aplicacao & ' + Geral.FF0(CO_APLIC_COND_PAG_FRETE) + ' <> 0 ',
  'ORDER BY Nome ',
  '']);
  UnDmkDAC_PF.AbreQuery(QrFrtPrcCad, Dmod.MyDB);
end;

procedure TFmFrtRegFatC.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFrtRegFatCCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFrtRegFatC.SbCaracAdClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'FrtCaracAd', 60, ncGerlSeq1,
  'Caracter�sticas de Transporte', [], False, Null, [], [], False);
  //
  UMyMod.SetaCodigoPesquisado(
    EdCaracAd, CBCaracAd, QrCaracAd, VAR_CADASTRO);
end;

procedure TFmFrtRegFatC.SbCaracSerClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'FrtCaracSer', 60, ncGerlSeq1,
  'Caracter�sticas de Transporte', [], False, Null, [], [], False);
  //
  UMyMod.SetaCodigoPesquisado(
    EdCaracSer, CBCaracSer, QrCaracSer, VAR_CADASTRO);
end;

procedure TFmFrtRegFatC.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFrtRegFatC.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFrtRegFatCCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFrtRegFatC.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFrtRegFatC.QrFrtRegFatCAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFrtRegFatC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFrtRegFatC.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFrtRegFatCCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'frtregfatc', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFrtRegFatC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtRegFatC.QrFrtRegFatCBeforeOpen(DataSet: TDataSet);
begin
  QrFrtRegFatCCodigo.DisplayFormat := FFormatFloat;
end;

end.

