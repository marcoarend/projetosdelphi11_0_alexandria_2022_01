unit FrtMnfMunCarrega;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmFrtMnfMunCarrega = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBMunCarrega: TdmkDBLookupComboBox;
    EdMunCarrega: TdmkEditCB;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    DsDTB_Munici: TDataSource;
    QrDTB_Munici: TmySQLQuery;
    QrDTB_MuniciCodigo: TIntegerField;
    QrDTB_MuniciNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenFrtMnfMunCarrega(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtMnfMunCarrega: TFmFrtMnfMunCarrega;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral;

{$R *.DFM}

procedure TFmFrtMnfMunCarrega.BtOKClick(Sender: TObject);
var
  xMunCarrega: String;
  Codigo, Controle, cMunCarrega: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  cMunCarrega    := EdMunCarrega.ValueVariant;
  xMunCarrega    := CBMunCarrega.Text;
  //
  if MyObjects.FIC(cMunCarrega = 0, EdMunCarrega, 'Informe o município!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('frtmnfmuncarrega', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtmnfmuncarrega', False, [
  'Codigo', 'cMunCarrega', 'xMunCarrega'], [
  'Controle'], [
  Codigo, cMunCarrega, xMunCarrega], [
  Controle], True) then
  begin
    ReopenFrtMnfMunCarrega(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdControle.ValueVariant   := 0;
      EdMunCarrega.ValueVariant := 0;
      CBMunCarrega.KeyValue     := Null;
      //
      EdMunCarrega.SetFocus;
    end else Close;
  end;
end;

procedure TFmFrtMnfMunCarrega.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtMnfMunCarrega.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtMnfMunCarrega.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrDTB_Munici, DModG.AllID_DB);
end;

procedure TFmFrtMnfMunCarrega.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtMnfMunCarrega.ReopenFrtMnfMunCarrega(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
