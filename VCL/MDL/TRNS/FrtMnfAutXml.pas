unit FrtMnfAutXml;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, Clipbrd, Variants, dmkDBGridZTO, UnDmkEnums,
  dmkRadioGroup, UnDmkProcFunc;

type
  TFmFrtMnfAutXml = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    QrFrtMnfAutXml: TmySQLQuery;
    DsFrtMnfAutXml: TDataSource;
    PnDados: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGrid1: TdmkDBGridZTO;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnEdita: TPanel;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel8: TPanel;
    BtDesiste: TBitBtn;
    Panel6: TPanel;
    Label7: TLabel;
    EdControle: TdmkEdit;
    Panel5: TPanel;
    RGTipo: TdmkRadioGroup;
    PnCPF: TPanel;
    Label8: TLabel;
    EdCPF: TdmkEdit;
    PnCNPJ: TPanel;
    Label11: TLabel;
    EdCNPJ: TdmkEdit;
    QrFrtMnfAutXmlCodigo: TIntegerField;
    QrFrtMnfAutXmlControle: TIntegerField;
    QrFrtMnfAutXmlAddForma: TSmallintField;
    QrFrtMnfAutXmlAddIDCad: TSmallintField;
    QrFrtMnfAutXmlTipo: TSmallintField;
    QrFrtMnfAutXmlCNPJ: TWideStringField;
    QrFrtMnfAutXmlCPF: TWideStringField;
    QrFrtMnfAutXmlLk: TIntegerField;
    QrFrtMnfAutXmlDataCad: TDateField;
    QrFrtMnfAutXmlDataAlt: TDateField;
    QrFrtMnfAutXmlUserCad: TIntegerField;
    QrFrtMnfAutXmlUserAlt: TIntegerField;
    QrFrtMnfAutXmlAlterWeb: TSmallintField;
    QrFrtMnfAutXmlAtivo: TSmallintField;
    QrFrtMnfAutXmlCNPJ_CPF: TWideStringField;
    QrFrtMnfAutXmlITEM: TIntegerField;
    QrFrtMnfAutXmlTpDOC: TWideStringField;
    QrFrtMnfAutXmlNO_AddForma: TWideStringField;
    QrFrtMnfAutXmlNO_ENT_ECO: TWideStringField;
    SbEntidade: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrFrtMnfAutXmlAfterOpen(DataSet: TDataSet);
    procedure QrFrtMnfAutXmlBeforeClose(DataSet: TDataSet);
    procedure RGTipoClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrFrtMnfAutXmlCalcFields(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbEntidadeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function AtualizaListaContatos(): Boolean;
  public
    { Public declarations }
    FInserePadroes: Boolean;
    FCodigo, FTerceiro: Integer;
    //
    procedure ReopenFrtMnfAutXml(Controle: Integer);
  end;

  var
  FmFrtMnfAutXml: TFmFrtMnfAutXml;

implementation

uses UnMyObjects, Module, UMySQLModule, CfgCadLista, UnGOTOy, ModuleGeral;

{$R *.DFM}

procedure TFmFrtMnfAutXml.BtAlteraClick(Sender: TObject);
begin
  if QrFrtMnfAutXml.RecordCount > 0 then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFrtMnfAutXml, [PnDados],
    [PnEdita], RGTipo, ImgTipo,  'frtmnfautxml');
  end;
end;

procedure TFmFrtMnfAutXml.BtConfirmaClick(Sender: TObject);
var
  CNPJ, CPF: String;
  Codigo, Controle, AddForma, AddIDCad, Tipo: Integer;
begin
  Codigo   := FCodigo;
  Controle := EdControle.ValueVariant;
  Tipo     := RGTipo.ItemIndex;
  AddForma := Integer(TNFeautXML.naxAvulso);
  AddIDCad := 0;
  //
  if Tipo = 0 then
  begin
    CNPJ := Geral.SoNumero_TT(EdCNPJ.Text);
    CPF  := '';
    //
    if MyObjects.FIC(CNPJ = '', EdCNPJ, 'Informe o CNPJ!') then
      Exit;
    if MyObjects.FIC(Length(CNPJ) <> 14, EdCNPJ, 'O CNPJ deve conter 14 algarismos!') then
      Exit;
  end else begin
    CNPJ := '';
    CPF  := Geral.SoNumero_TT(EdCPF.Text);
    //
    if MyObjects.FIC(CPF = '', EdCPF, 'Informe o CPF!') then
      Exit;
    if MyObjects.FIC(Length(CPF) <> 11, EdCPF, 'O CPF deve conter 11 algarismos!') then
      Exit;
  end;
  //
  Controle := UMyMod.BPGS1I32('frtmnfautxml', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'frtmnfautxml', False, [
    'Codigo',
    'AddForma', 'AddIDCad', 'Tipo',
    'CNPJ', 'CPF'], [
    'Controle'], [
    Codigo,
    AddForma, AddIDCad, Tipo,
    CNPJ, CPF], [
    Controle], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    //
    ReopenFrtMnfAutXml(Controle);
  end;
end;

procedure TFmFrtMnfAutXml.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
end;

procedure TFmFrtMnfAutXml.BtExcluiClick(Sender: TObject);
var
  Controle: Integer;
begin
  if QrFrtMnfAutXml.RecordCount > 0 then
  begin
    if UMyMod.ExcluiRegistroInt1('Confirma a retirada do setor selecionado?',
    'frtmnfautxml', 'Controle', QrFrtMnfAutXmlControle.Value, Dmod.MyDB) = ID_YES then
    begin
      Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtMnfAutXml,
        QrFrtMnfAutXmlControle, QrFrtMnfAutXmlControle.Value);
      ReopenFrtMnfAutXml(Controle);
    end;
  end;
end;

procedure TFmFrtMnfAutXml.BtIncluiClick(Sender: TObject);
begin
  if QrFrtMnfAutXml.RecordCount < 10 then
  begin
    UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFrtMnfAutXml, [PnDados],
    [PnEdita], RGTipo, ImgTipo,  'frtmnfautxml');
  end else
    Geral.MB_Aviso('Limite de itens (10) atingido!');
end;

procedure TFmFrtMnfAutXml.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtMnfAutXml.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtMnfAutXml.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Align   := alClient;
end;

procedure TFmFrtMnfAutXml.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtMnfAutXml.FormShow(Sender: TObject);
begin
  if FInserePadroes then
  begin
    SbEntidade.Visible := True;
    //
    Screen.Cursor := crHourGlass;
    try
      if not AtualizaListaContatos() then
        Geral.MB_Aviso('Falha ao atualizar lista de contatos!');
    finally
      Screen.Cursor := crDefault;
    end;
  end else
    SbEntidade.Visible := False;
end;

function TFmFrtMnfAutXml.AtualizaListaContatos(): Boolean;
var
  Qry, QryLoc: TmySQLQuery;
  SQLCompl, CPF, CNPJ: String;
  Tipo: Integer;

  procedure InsereAtual();
  var
    CNPJ, CPF: String;
    Controle, AddForma, AddIDCad, Tipo: Integer;
  begin
    Controle := 0;
    Tipo     := Qry.FieldByName('Tipo').AsInteger;
    AddForma := Integer(TNFeautXML.naxEntiContat); // EntiContat.Controle
    AddIDCad := Qry.FieldByName('Controle').AsInteger;
    //
    if Tipo = 0 then
    begin
      CNPJ := Geral.SoNumero_TT(Qry.FieldByName('CNPJ').AsString);
      CPF  := '';
      //
      if MyObjects.FIC(Length(CNPJ) <> 14, nil,
      'O CNPJ ' + CNPJ + ' n�o foi inclu�do pois n�p possui 14 algarismos!') then
        Exit;
    end else begin
      CNPJ := '';
      CPF  := Geral.SoNumero_TT(Qry.FieldByName('CPF').AsString);
      //
      if MyObjects.FIC(Length(CPF) <> 11, nil,
      'O CPF ' + CPF + ' n�o foi inclu�do pois n�p possui 11 algarismos!') then
        Exit;
    end;
    //
    Controle := UMyMod.BPGS1I32('frtmnfautxml', 'Controle', '', '', tsPos, stIns, Controle);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'frtmnfautxml', False, [
      'Codigo',
      'CNPJ', 'CPF', 'AddForma',
      'AddIDCad', 'Tipo'], [
      'Controle'], [
      FCodigo,
      CNPJ, CPF, AddForma,
      AddIDCad, Tipo], [
      Controle], True);
  end;

begin
  if FTerceiro = 0 then
    Exit;
  try
    Result := True;
    //
    Qry    := TmySQLQuery.Create(Dmod);
    QryLoc := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Controle, Tipo, CNPJ, CPF ',
        'FROM enticontat ',
        'WHERE Codigo=' + Geral.FF0(FTerceiro),
        'AND Aplicacao & 1 ',
        'And Ativo=1 ',
        'LIMIT 10 ',
        '']);
      if Qry.RecordCount = 0 then
        Geral.MB_Aviso(
          'Destinat�rio / remetente sem emails cadastrados para autoriza��o de obten��o de XML!')
      else
      begin
        Qry.First;
        //
        while not Qry.Eof do
        begin
          Tipo := Qry.FieldByName('Tipo').AsInteger;
          CPF  := Qry.FieldByName('CPF').AsString;
          CNPJ := Qry.FieldByName('CNPJ').AsString;
          //
          if Tipo = 0 then
            SQLCompl := 'AND CNPJ="' + CNPJ + '"'
          else
            SQLCompl := 'AND CPF="' + CPF + '"';
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QryLoc, Dmod.MyDB, [
            'SELECT * FROM frtmnfautxml ',
            'WHERE Codigo=' + Geral.FF0(FCodigo),
            SQLCompl,
            '']);
          if QryLoc.RecordCount = 0 then
            InsereAtual();
          //
          Qry.Next;
        end;
        ReopenFrtMnfAutXml(0);
      end;
    finally
      Qry.Free;
      QryLoc.Free;
    end;
  except
    Result := False;
  end;
end;

procedure TFmFrtMnfAutXml.QrFrtMnfAutXmlAfterOpen(DataSet: TDataSet);
begin
  BtInclui.Enabled := QrFrtMnfAutXml.RecordCount < 10;
end;

procedure TFmFrtMnfAutXml.QrFrtMnfAutXmlBeforeClose(DataSet: TDataSet);
begin
  BtInclui.Enabled := False;
end;

procedure TFmFrtMnfAutXml.QrFrtMnfAutXmlCalcFields(DataSet: TDataSet);
begin
  QrFrtMnfAutXmlITEM.Value := QrFrtMnfAutXml.RecNo;
end;

procedure TFmFrtMnfAutXml.ReopenFrtMnfAutXml(Controle: Integer);
var
  ATT_AddForma: String;
begin
  ATT_AddForma := dmkPF.ArrayToTexto('cga.AddForma', 'NO_AddForma', pvPos, True,
    sNFeautXML);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfAutXml, Dmod.MyDB, [
  'SELECT cga.*, ',
  ATT_AddForma,
  'IF(cga.Tipo=0, "CNPJ", "CPF") TpDOC, ',
  'CASE cga.AddForma ',
  '  WHEN 0 THEN "(N/I)"  ',
  '  WHEN 1 THEN eco.Nome ',
  '  WHEN 2 THEN IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ',
  '  WHEN 3 THEN " "  ',
  '  ELSE "???" END NO_ENT_ECO, ',
  'IF(cga.Tipo=0, cga.CNPJ, cga.CPF) CNPJ_CPF ',
  'FROM frtmnfautxml cga ',
  'LEFT JOIN entidades ent ON ent.Codigo=cga.AddIDCad',
  'LEFT JOIN enticontat eco ON eco.Controle=cga.AddIDCad',
  'WHERE cga.Codigo=' + Geral.FF0(FCodigo),
  '']);
  //
  if Controle <> 0 then
    QrFrtMnfAutXml.Locate('Controle', Controle, []);
end;

procedure TFmFrtMnfAutXml.RGTipoClick(Sender: TObject);
begin
  PnCNPJ.Visible := RGTipo.ItemIndex = 0;
  PnCPF.Visible  := RGTipo.ItemIndex = 1;
end;

procedure TFmFrtMnfAutXml.SbEntidadeClick(Sender: TObject);
begin
  if FTerceiro <> 0 then
  begin
    DModG.CadastroDeEntidade(FTerceiro, fmcadEntidade2, fmcadEntidade2);
    //
    Screen.Cursor := crHourGlass;
    try
      if not AtualizaListaContatos() then
        Geral.MB_Aviso('Falha ao atualizar lista de contatos!');
    finally
      Screen.Cursor := crDefault;
    end;
    //
  end;
  ReopenFrtMnfAutXml(0);
end;

end.
