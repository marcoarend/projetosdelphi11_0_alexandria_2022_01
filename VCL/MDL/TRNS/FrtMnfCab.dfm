object FmFrtMnfCab: TFmFrtMnfCab
  Left = 368
  Top = 194
  Caption = 'MNF-CARGA-001 :: Manifesto de Carga'
  ClientHeight = 747
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 651
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 429
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 162
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label7: TLabel
          Left = 16
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label3: TLabel
          Left = 76
          Top = 0
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label9: TLabel
          Left = 444
          Top = 0
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label36: TLabel
          Left = 16
          Top = 40
          Width = 162
          Height = 13
          Caption = 'Data e hora prevista para a sa'#237'da:'
        end
        object Label4: TLabel
          Left = 200
          Top = 40
          Width = 177
          Height = 13
          Caption = 'Data e hora prevista para a chegada:'
        end
        object Label5: TLabel
          Left = 384
          Top = 40
          Width = 88
          Height = 13
          Caption = 'Ve'#237'culo de tra'#231#227'o:'
        end
        object Label32: TLabel
          Left = 812
          Top = 41
          Width = 77
          Height = 13
          Caption = 'S'#233'rie do MDF-e:'
        end
        object Label13: TLabel
          Left = 836
          Top = 0
          Width = 161
          Height = 13
          Caption = 'C'#243'digo de agendamento no porto:'
        end
        object EdCodigo: TdmkEdit
          Left = 16
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdEmpresa: TdmkEditCB
          Left = 76
          Top = 16
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdEmpresaRedefinido
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 128
          Top = 16
          Width = 313
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 2
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdNome: TdmkEdit
          Left = 444
          Top = 16
          Width = 389
          Height = 21
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object TpDataViagIniPrv: TdmkEditDateTimePicker
          Left = 16
          Top = 56
          Width = 116
          Height = 21
          Date = 40137.793551006940000000
          Time = 40137.793551006940000000
          TabOrder = 5
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataViagIniPrv'
          UpdCampo = 'DataViagIniPrv'
          UpdType = utYes
        end
        object EdDataViagIniPrv: TdmkEdit
          Left = 136
          Top = 56
          Width = 60
          Height = 21
          TabOrder = 6
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfLong
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'DataViagIniPrv'
          UpdCampo = 'DataViagIniPrv'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object TPDataViagFimPrv: TdmkEditDateTimePicker
          Left = 200
          Top = 56
          Width = 116
          Height = 21
          Date = 40137.793551006940000000
          Time = 40137.793551006940000000
          TabOrder = 7
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataViagFimPrv'
          UpdCampo = 'DataViagFimPrv'
          UpdType = utYes
        end
        object EdDataViagFimPrv: TdmkEdit
          Left = 320
          Top = 56
          Width = 60
          Height = 21
          TabOrder = 8
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfLong
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'DataViagFimPrv'
          UpdCampo = 'DataViagFimPrv'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdTrnsIts: TdmkEditCB
          Left = 384
          Top = 56
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'TrnsIts'
          UpdCampo = 'TrnsIts'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBTrnsIts
          IgnoraDBLookupComboBox = True
        end
        object CBTrnsIts: TdmkDBLookupComboBox
          Left = 441
          Top = 56
          Width = 272
          Height = 21
          KeyField = 'Controle'
          ListField = 'DESCRI'
          ListSource = DsTrnsIts
          TabOrder = 10
          dmkEditCB = EdTrnsIts
          QryCampo = 'TrnsIts'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdParamsMDFe: TdmkEditCB
          Left = 812
          Top = 56
          Width = 28
          Height = 21
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ParamsMDFe'
          UpdCampo = 'ParamsMDFe'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBParamsMDFe
          IgnoraDBLookupComboBox = False
        end
        object CBParamsMDFe: TdmkDBLookupComboBox
          Left = 840
          Top = 56
          Width = 157
          Height = 21
          KeyField = 'Controle'
          ListField = 'NOME'
          ListSource = DsParamsMDFe
          TabOrder = 12
          dmkEditCB = EdParamsMDFe
          QryCampo = 'ParamsMDFe'
          UpdType = utNil
          LocF7SQLMasc = '$#'
        end
        object RGTpEmit: TdmkRadioGroup
          Left = 16
          Top = 80
          Width = 193
          Height = 77
          Caption = 'Tipo do Emitente:'
          ItemIndex = 0
          Items.Strings = (
            'Indefinido'
            'Prestador de servi'#231'o de transporte'
            'Transportador de carga pr'#243'pria')
          TabOrder = 13
          QryCampo = 'TpEmit'
          UpdCampo = 'TpEmit'
          UpdType = utYes
          OldValor = 0
        end
        object RGModal: TdmkRadioGroup
          Left = 212
          Top = 80
          Width = 173
          Height = 77
          Caption = 'Modal: '
          Columns = 2
          Enabled = False
          ItemIndex = 1
          Items.Strings = (
            'Indefinido'
            'Rodovi'#225'rio'
            'A'#233'reo'
            'Aquavi'#225'rio'
            'Ferrovi'#225'rio')
          TabOrder = 14
          OnClick = RGModalClick
          QryCampo = 'TpEmit'
          UpdCampo = 'TpEmit'
          UpdType = utYes
          OldValor = 0
        end
        object GroupBox1: TGroupBox
          Left = 388
          Top = 80
          Width = 141
          Height = 77
          Caption = ' Carga e descarga '
          TabOrder = 15
          object Label24: TLabel
            Left = 9
            Top = 28
            Width = 77
            Height = 13
            Caption = 'UF que carrega:'
          end
          object Label6: TLabel
            Left = 9
            Top = 52
            Width = 79
            Height = 13
            Caption = 'UF da descarga:'
          end
          object EdUFIni: TdmkEdit
            Left = 105
            Top = 24
            Width = 28
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtUF
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'UFIni'
            UpdCampo = 'UFIni'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdUFFim: TdmkEdit
            Left = 105
            Top = 48
            Width = 28
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtUF
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'UFFim'
            UpdCampo = 'UFFim'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object GroupBox2: TGroupBox
          Left = 532
          Top = 80
          Width = 401
          Height = 77
          Caption = ' Totais deste MDF-e:'
          TabOrder = 16
          object Label8: TLabel
            Left = 9
            Top = 28
            Width = 104
            Height = 13
            Caption = 'Quantidade de CT-es:'
          end
          object Label10: TLabel
            Left = 9
            Top = 52
            Width = 104
            Height = 13
            Caption = 'Quantidade de NF-es:'
          end
          object Label11: TLabel
            Left = 181
            Top = 28
            Width = 58
            Height = 13
            Caption = 'Peso bruto*:'
          end
          object Label12: TLabel
            Left = 181
            Top = 52
            Width = 95
            Height = 13
            Caption = 'Valor total da carga:'
          end
          object EdTot_qCTe: TdmkEdit
            Left = 120
            Top = 24
            Width = 48
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Tot_qCTe'
            UpdCampo = 'Tot_qCTe'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdTot_qNFe: TdmkEdit
            Left = 120
            Top = 48
            Width = 48
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Tot_qNFe'
            UpdCampo = 'Tot_qNFe'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdTot_vCarga: TdmkEdit
            Left = 284
            Top = 48
            Width = 108
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'Tot_vCarga'
            UpdCampo = 'Tot_vCarga'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTot_qCarga: TdmkEdit
            Left = 284
            Top = 24
            Width = 108
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryCampo = 'Tot_qCarga'
            UpdCampo = 'Tot_qCarga'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object RGTot_cUnid: TdmkRadioGroup
          Left = 932
          Top = 80
          Width = 65
          Height = 77
          Caption = '*Unidade:'
          ItemIndex = 0
          Items.Strings = (
            '??'
            'Kg'
            'Ton')
          TabOrder = 17
          OnClick = RGModalClick
          QryCampo = 'Tot_cUnid'
          UpdCampo = 'Tot_cUnid'
          UpdType = utYes
          OldValor = 0
        end
        object EdCodAgPorto: TdmkEdit
          Left = 836
          Top = 16
          Width = 161
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'CodAgPorto'
          UpdCampo = 'CodAgPorto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object Panel6: TPanel
        Left = 2
        Top = 177
        Width = 1004
        Height = 242
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label179: TLabel
          Left = 16
          Top = 0
          Width = 211
          Height = 13
          Caption = 'Informa'#231#245'es adicionais de interesse do fisco:'
        end
        object Label180: TLabel
          Left = 16
          Top = 104
          Width = 274
          Height = 13
          Caption = 'Informa'#231#245'es complementares de interesse do contribuinte:'
          FocusControl = MeInfCpl
        end
        object MeInfAdFisco: TdmkMemo
          Left = 16
          Top = 16
          Width = 981
          Height = 85
          MaxLength = 2000
          TabOrder = 0
          QryCampo = 'InfAdFisco'
          UpdCampo = 'InfAdFisco'
          UpdType = utYes
        end
        object MeInfCpl: TdmkMemo
          Left = 16
          Top = 120
          Width = 981
          Height = 117
          MaxLength = 5000
          TabOrder = 1
          QryCampo = 'InfCpl'
          UpdCampo = 'InfCpl'
          UpdType = utYes
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 588
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 651
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsFrtMnfCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 689
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsFrtMnfCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 546
      Width = 1008
      Height = 105
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 88
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 86
        Height = 88
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 260
        Top = 15
        Width = 746
        Height = 88
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 616
          Top = 0
          Width = 130
          Height = 88
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 6
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 44
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtSaidaClick
          end
          object BtEncerra: TBitBtn
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Encerra'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtEncerraClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Manifesto'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCabClick
        end
        object BtMunCarrega: TBitBtn
          Tag = 110
          Left = 126
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Carrega'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtMunCarregaClick
        end
        object BtInfPercurso: TBitBtn
          Tag = 110
          Left = 248
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Percurso'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtInfPercursoClick
        end
        object BtInfDoc: TBitBtn
          Tag = 110
          Left = 370
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Docs'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtInfDocClick
        end
        object BtAutXML: TBitBtn
          Tag = 110
          Left = 4
          Top = 44
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Aut. XML'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtAutXMLClick
        end
        object BtLacres: TBitBtn
          Tag = 110
          Left = 492
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lacres'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtLacresClick
        end
        object BtCondutor: TBitBtn
          Tag = 110
          Left = 126
          Top = 44
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'C&ondutor'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          OnClick = BtCondutorClick
        end
        object BtReboque: TBitBtn
          Tag = 110
          Left = 248
          Top = 44
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Reboque'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
          OnClick = BtReboqueClick
        end
        object BtValePedagio: TBitBtn
          Tag = 110
          Left = 370
          Top = 44
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Vale ped'#225'gio'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 9
          OnClick = BtValePedagioClick
        end
        object BtCIOT: TBitBtn
          Tag = 110
          Left = 492
          Top = 44
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&CIOT'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 10
          OnClick = BtCIOTClick
        end
      end
    end
    object DBGrid3: TDBGrid
      Left = 0
      Top = 189
      Width = 1008
      Height = 112
      Align = alTop
      DataSource = DsFrtMnfInfDoc
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'cMunDescarga'
          Title.Caption = 'Munic'#237'pio'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'xMunDescarga'
          Title.Caption = 'Descri'#231#227'o munic'#237'pio'
          Width = 147
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DocMod'
          Title.Caption = 'Mod.Doc.'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DocSerie'
          Title.Caption = 'S'#233'rie'
          Width = 29
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DocNumero'
          Title.Caption = 'N'#250'mero Doc.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DocChave'
          Title.Caption = 'Chave de acesso do documento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TrnsIts'
          Title.Caption = 'ID ve'#237'culo'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Placa'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_TrnsIts'
          Title.Caption = 'Descri'#231#227'o ve'#237'culo'
          Width = 269
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SegCodBarra'
          Title.Caption = 'Segundo c'#243'digo de barra'
          Visible = True
        end>
    end
    object Panel8: TPanel
      Left = 0
      Top = 65
      Width = 1008
      Height = 124
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object DBGrid2: TDBGrid
        Left = 0
        Top = 0
        Width = 356
        Height = 124
        Align = alLeft
        DataSource = DsFrtMnfMunCarrega
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'cMunCarrega'
            Title.Caption = 'Munic'#237'pio'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'xMunCarrega'
            Title.Caption = 'Descri'#231#227'o'
            Width = 196
            Visible = True
          end>
      end
      object DBGrid1: TDBGrid
        Left = 356
        Top = 0
        Width = 129
        Height = 124
        Align = alLeft
        DataSource = DsFrtMnfInfPercurso
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UFPer'
            Title.Caption = 'UF'
            Width = 22
            Visible = True
          end>
      end
      object DBGrid4: TDBGrid
        Left = 485
        Top = 0
        Width = 523
        Height = 124
        Align = alClient
        DataSource = DsFrtMnfLacres
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nLacre'
            Title.Caption = 'Lacre'
            Width = 360
            Visible = True
          end>
      end
    end
    object Panel9: TPanel
      Left = 0
      Top = 301
      Width = 1008
      Height = 120
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
      object DBGrid5: TDBGrid
        Left = 489
        Top = 0
        Width = 519
        Height = 120
        Align = alClient
        DataSource = DsFrtMnfAutXml
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_AddForma'
            Title.Caption = 'Forma Add'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TpDOC'
            Title.Caption = 'Tipo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CNPJ_CPF'
            Title.Caption = 'CNPJ/CPF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_ENT_ECO'
            Title.Caption = 'Nome do Autorizado a baixar o XML'
            Width = 234
            Visible = True
          end>
      end
      object DBGrid6: TDBGrid
        Left = 0
        Top = 0
        Width = 489
        Height = 120
        Align = alLeft
        DataSource = DsFrtMnfMoRodoCondutor
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Entidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CPF'
            Title.Caption = 'CNPJ/CPF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'xNome'
            Title.Caption = 'Nome do condutor'
            Width = 247
            Visible = True
          end>
      end
    end
    object Panel10: TPanel
      Left = 0
      Top = 421
      Width = 1008
      Height = 100
      Align = alTop
      TabOrder = 5
      object DBGrid7: TDBGrid
        Left = 1
        Top = 1
        Width = 384
        Height = 98
        Align = alLeft
        DataSource = DsFrtMnfMoRodoVeicRebo
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'cInt'
            Title.Caption = 'Reboque'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Placa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_VEIC'
            Title.Caption = 'Decri'#231#227'o do reboque'
            Width = 146
            Visible = True
          end>
      end
      object DBGrid8: TDBGrid
        Left = 385
        Top = 1
        Width = 456
        Height = 98
        Align = alLeft
        DataSource = DsFrtMnfMoRodoValePedg
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CNPJForn'
            Title.Caption = 'CNPJ fornecedor'
            Width = 112
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nCompra'
            Title.Caption = 'N'#186' Compra'
            Width = 128
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CNPJPg'
            Title.Caption = 'CNPJ pagador'
            Width = 112
            Visible = True
          end>
      end
      object DBGrid9: TDBGrid
        Left = 841
        Top = 1
        Width = 166
        Height = 98
        Align = alClient
        DataSource = DsFrtMnfMoRodoCIOT
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CIOT'
            Width = 80
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 237
        Height = 32
        Caption = 'Manifesto de Carga'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 237
        Height = 32
        Caption = 'Manifesto de Carga'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 237
        Height = 32
        Caption = 'Manifesto de Carga'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 20
    Top = 65520
  end
  object QrFrtMnfCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFrtMnfCabBeforeOpen
    AfterOpen = QrFrtMnfCabAfterOpen
    BeforeClose = QrFrtMnfCabBeforeClose
    AfterScroll = QrFrtMnfCabAfterScroll
    SQL.Strings = (
      'SELECT * FROM frtmnfcab'
      'WHERE Codigo > 0')
    Left = 80
    Top = 1
    object QrFrtMnfCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrFrtMnfCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrFrtMnfCabAbertura: TDateTimeField
      FieldName = 'Abertura'
    end
    object QrFrtMnfCabEncerrou: TDateTimeField
      FieldName = 'Encerrou'
    end
    object QrFrtMnfCabDataViagIniPrv: TDateTimeField
      FieldName = 'DataViagIniPrv'
    end
    object QrFrtMnfCabDataViagIniSai: TDateTimeField
      FieldName = 'DataViagIniSai'
    end
    object QrFrtMnfCabDataViagFimPrv: TDateTimeField
      FieldName = 'DataViagFimPrv'
    end
    object QrFrtMnfCabDataViagFimChg: TDateTimeField
      FieldName = 'DataViagFimChg'
    end
    object QrFrtMnfCabFrtRegMnf: TIntegerField
      FieldName = 'FrtRegMnf'
    end
    object QrFrtMnfCabTrnsIts: TIntegerField
      FieldName = 'TrnsIts'
    end
    object QrFrtMnfCabTpEmit: TSmallintField
      FieldName = 'TpEmit'
    end
    object QrFrtMnfCabModal: TSmallintField
      FieldName = 'Modal'
    end
    object QrFrtMnfCabTpEmis: TSmallintField
      FieldName = 'TpEmis'
    end
    object QrFrtMnfCabFrtRegPth: TIntegerField
      FieldName = 'FrtRegPth'
    end
    object QrFrtMnfCabUFIni: TWideStringField
      FieldName = 'UFIni'
      Size = 2
    end
    object QrFrtMnfCabUFFim: TWideStringField
      FieldName = 'UFFim'
      Size = 2
    end
    object QrFrtMnfCabParamsMDFe: TIntegerField
      FieldName = 'ParamsMDFe'
    end
    object QrFrtMnfCabTot_qCTe: TIntegerField
      FieldName = 'Tot_qCTe'
    end
    object QrFrtMnfCabTot_qNFe: TIntegerField
      FieldName = 'Tot_qNFe'
    end
    object QrFrtMnfCabTot_qMDFe: TIntegerField
      FieldName = 'Tot_qMDFe'
    end
    object QrFrtMnfCabTot_vCarga: TFloatField
      FieldName = 'Tot_vCarga'
    end
    object QrFrtMnfCabTot_cUnid: TSmallintField
      FieldName = 'Tot_cUnid'
    end
    object QrFrtMnfCabTot_qCarga: TFloatField
      FieldName = 'Tot_qCarga'
    end
    object QrFrtMnfCabinfAdFisco: TWideMemoField
      FieldName = 'infAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFrtMnfCabinfCpl: TWideMemoField
      FieldName = 'infCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFrtMnfCabSerieDesfe: TIntegerField
      FieldName = 'SerieDesfe'
    end
    object QrFrtMnfCabMDFDesfeita: TIntegerField
      FieldName = 'MDFDesfeita'
    end
    object QrFrtMnfCabCodAgPorto: TWideStringField
      FieldName = 'CodAgPorto'
      Size = 16
    end
    object QrFrtMnfCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFrtMnfCabStatus: TSmallintField
      FieldName = 'Status'
    end
  end
  object DsFrtMnfCab: TDataSource
    DataSet = QrFrtMnfCab
    Left = 80
    Top = 45
  end
  object QrFrtMnfMunCarrega: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fmm.*  '
      'FROM frtmnfmuncarrega fmm '
      'WHERE fmm.Codigo=:P0 '
      'ORDER BY fmm.xMunCarrega ')
    Left = 172
    Top = 25
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtMnfMunCarregaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfMunCarregaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfMunCarregacMunCarrega: TIntegerField
      FieldName = 'cMunCarrega'
    end
    object QrFrtMnfMunCarregaxMunCarrega: TWideStringField
      FieldName = 'xMunCarrega'
      Size = 60
    end
    object QrFrtMnfMunCarregaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfMunCarregaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfMunCarregaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfMunCarregaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfMunCarregaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfMunCarregaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfMunCarregaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtMnfMunCarrega: TDataSource
    DataSet = QrFrtMnfMunCarrega
    Left = 172
    Top = 69
  end
  object PMMunCarrega: TPopupMenu
    OnPopup = PMMunCarregaPopup
    Left = 388
    Top = 656
    object Incluimunicpiodecarregamento1: TMenuItem
      Caption = '&Adiciona munic'#237'pio de carregamento'
      Enabled = False
      OnClick = Incluimunicpiodecarregamento1Click
    end
    object Excluimunicpiodecarregamento1: TMenuItem
      Caption = '&Remove munic'#237'pio de carregamento'
      Enabled = False
      OnClick = Excluimunicpiodecarregamento1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 264
    Top = 648
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrTrnsIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle,'
      'CONCAT(Placa, '#39'  '#39', Nome) DESCRI'
      'FROM trnsits'
      'WHERE tpVeic=0'
      'ORDER BY DESCRI')
    Left = 244
    Top = 52
    object QrTrnsItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTrnsItsDESCRI: TWideStringField
      FieldName = 'DESCRI'
      Size = 255
    end
  end
  object DsTrnsIts: TDataSource
    DataSet = QrTrnsIts
    Left = 244
    Top = 100
  end
  object QrParamsMDFe: TmySQLQuery
    Database = Dmod.MyDB
    Left = 312
    Top = 36
    object QrParamsMDFeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrParamsMDFeNOME: TWideStringField
      FieldName = 'NOME'
    end
  end
  object DsParamsMDFe: TDataSource
    DataSet = QrParamsMDFe
    Left = 312
    Top = 80
  end
  object QrFrtMnfInfPercurso: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fmm.*  '
      'FROM frtmnfinfpercurso fmm '
      'WHERE fmm.Codigo=:P0 '
      'ORDER BY fmm.Controle')
    Left = 388
    Top = 53
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtMnfInfPercursoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfInfPercursoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfInfPercursoUFPer: TWideStringField
      DisplayWidth = 2
      FieldName = 'UFPer'
      Size = 60
    end
    object QrFrtMnfInfPercursoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfInfPercursoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfInfPercursoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfInfPercursoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfInfPercursoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfInfPercursoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfInfPercursoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtMnfInfPercurso: TDataSource
    DataSet = QrFrtMnfInfPercurso
    Left = 388
    Top = 97
  end
  object PMInfPercurso: TPopupMenu
    OnPopup = PMInfPercursoPopup
    Left = 508
    Top = 660
    object IncluiUFdePercurso1: TMenuItem
      Caption = '&Adiciona UF de Percurso'
      Enabled = False
      OnClick = IncluiUFdePercurso1Click
    end
    object ExcluiUFdePercurso1: TMenuItem
      Caption = '&Remove UF de Percurso'
      Enabled = False
      OnClick = ExcluiUFdePercurso1Click
    end
  end
  object QrFrtMnfInfDoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fmd.*, tri.Nome NO_TrnsIts, tri.Placa  '
      'FROM frtmnfinfdoc fmd'
      'LEFT JOIN trnsits tri ON tri.Controle=fmd.TrnsIts'
      'WHERE fmd.Codigo=:P0 '
      'ORDER BY fmd.Controle')
    Left = 456
    Top = 37
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtMnfInfDocPlaca: TWideStringField
      FieldName = 'Placa'
      Size = 11
    end
    object QrFrtMnfInfDocNO_TrnsIts: TWideStringField
      FieldName = 'NO_TrnsIts'
      Size = 100
    end
    object QrFrtMnfInfDocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfInfDocControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfInfDoccMunDescarga: TIntegerField
      FieldName = 'cMunDescarga'
    end
    object QrFrtMnfInfDocxMunDescarga: TWideStringField
      FieldName = 'xMunDescarga'
      Size = 60
    end
    object QrFrtMnfInfDocDocMod: TSmallintField
      FieldName = 'DocMod'
    end
    object QrFrtMnfInfDocDocSerie: TIntegerField
      FieldName = 'DocSerie'
    end
    object QrFrtMnfInfDocDocNumero: TIntegerField
      FieldName = 'DocNumero'
    end
    object QrFrtMnfInfDocDocChave: TWideStringField
      FieldName = 'DocChave'
      Size = 44
    end
    object QrFrtMnfInfDocSegCodBarra: TWideStringField
      FieldName = 'SegCodBarra'
      Size = 36
    end
    object QrFrtMnfInfDocTrnsIts: TIntegerField
      FieldName = 'TrnsIts'
    end
    object QrFrtMnfInfDocLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfInfDocDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfInfDocDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfInfDocUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfInfDocUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfInfDocAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfInfDocAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtMnfInfDoc: TDataSource
    DataSet = QrFrtMnfInfDoc
    Left = 456
    Top = 81
  end
  object PMInfDoc: TPopupMenu
    OnPopup = PMInfDocPopup
    Left = 640
    Top = 660
    object IncluiDocumento1: TMenuItem
      Caption = '&Inclui Documento'
      Enabled = False
      OnClick = IncluiDocumento1Click
    end
    object AlteraDocumento1: TMenuItem
      Caption = '&Altera Documento'
      Enabled = False
      OnClick = AlteraDocumento1Click
    end
    object ExcluiDocumento1: TMenuItem
      Caption = '&Exclui Documento'
      Enabled = False
      OnClick = ExcluiDocumento1Click
    end
  end
  object QrFrtMnfAutXml: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cga.*, '
      
        ' ELT(cga.AddForma + 1,"N'#227'o informado","Contatos","Entidade","Avu' +
        'lso") NO_AddForma,'
      'IF(cga.Tipo=0, "CNPJ", "CPF") TpDOC,'
      'CASE cga.AddForma '
      '  WHEN 0 THEN "(N/I)"  '
      '  WHEN 1 THEN eco.Nome '
      '  WHEN 2 THEN IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) '
      '  WHEN 3 THEN " "  '
      '  ELSE "???" END NO_ENT_ECO, '
      'IF(cga.Tipo=0, cga.CNPJ, cga.CPF) CNPJ_CPF '
      'FROM frtmnfautxml cga'
      'LEFT JOIN entidades ent ON ent.Codigo=cga.AddIDCad'
      'LEFT JOIN enticontat eco ON eco.Controle=cga.AddIDCad'
      'WHERE cga.Codigo=7')
    Left = 592
    Top = 36
    object QrFrtMnfAutXmlCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfAutXmlControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfAutXmlAddForma: TSmallintField
      FieldName = 'AddForma'
    end
    object QrFrtMnfAutXmlAddIDCad: TIntegerField
      FieldName = 'AddIDCad'
    end
    object QrFrtMnfAutXmlTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrFrtMnfAutXmlCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrFrtMnfAutXmlCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrFrtMnfAutXmlLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfAutXmlDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfAutXmlDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfAutXmlUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfAutXmlUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfAutXmlAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfAutXmlAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFrtMnfAutXmlNO_AddForma: TWideStringField
      FieldName = 'NO_AddForma'
      Size = 13
    end
    object QrFrtMnfAutXmlTpDOC: TWideStringField
      FieldName = 'TpDOC'
      Required = True
      Size = 4
    end
    object QrFrtMnfAutXmlNO_ENT_ECO: TWideStringField
      FieldName = 'NO_ENT_ECO'
      Size = 100
    end
    object QrFrtMnfAutXmlCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 14
    end
  end
  object DsFrtMnfAutXml: TDataSource
    DataSet = QrFrtMnfAutXml
    Left = 596
    Top = 81
  end
  object PMLacres: TPopupMenu
    OnPopup = PMLacresPopup
    Left = 752
    Top = 660
    object IncluiLacre1: TMenuItem
      Caption = '&Inclui Lacre'
      Enabled = False
      OnClick = IncluiLacre1Click
    end
    object AlteraLacre1: TMenuItem
      Caption = '&Altera Lacre'
      Enabled = False
      OnClick = AlteraLacre1Click
    end
    object ExcluiLacre1: TMenuItem
      Caption = '&Exclui Lacre'
      Enabled = False
      OnClick = ExcluiLacre1Click
    end
  end
  object QrFrtMnfLacres: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtmnflacres'
      'WHERE Codigo=:P0')
    Left = 524
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtMnfLacresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfLacresControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfLacresnLacre: TWideStringField
      FieldName = 'nLacre'
    end
    object QrFrtMnfLacresLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfLacresDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfLacresDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfLacresUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfLacresUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfLacresAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfLacresAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtMnfLacres: TDataSource
    DataSet = QrFrtMnfLacres
    Left = 524
    Top = 104
  end
  object PMEncerra: TPopupMenu
    OnPopup = PMEncerraPopup
    Left = 884
    Top = 656
    object Encerramanifesto1: TMenuItem
      Caption = '&Encerra manifesto'
      OnClick = Encerramanifesto1Click
    end
    object MenuItem1: TMenuItem
      Caption = '-'
    end
    object Desfazencerramento1: TMenuItem
      Caption = '&Desfaz encerramento'
      OnClick = Desfazencerramento1Click
    end
  end
  object PMCondutor: TPopupMenu
    OnPopup = PMCondutorPopup
    Left = 476
    Top = 704
    object IncluiCondutor1: TMenuItem
      Caption = '&Inclui Condutor'
      Enabled = False
      OnClick = IncluiCondutor1Click
    end
    object AlteraCondutor1: TMenuItem
      Caption = '&Altera Condutor'
      Enabled = False
      OnClick = AlteraCondutor1Click
    end
    object ExcluiCondutor1: TMenuItem
      Caption = '&Exclui Condutor'
      Enabled = False
      OnClick = ExcluiCondutor1Click
    end
  end
  object QrFrtMnfMoRodoCondutor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT rco.*'
      'FROM frtmnfmorodocondutor rco'
      'WHERE rco.Codigo=0')
    Left = 684
    Top = 56
    object QrFrtMnfMoRodoCondutorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfMoRodoCondutorControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfMoRodoCondutorEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrFrtMnfMoRodoCondutorxNome: TWideStringField
      FieldName = 'xNome'
      Size = 60
    end
    object QrFrtMnfMoRodoCondutorCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrFrtMnfMoRodoCondutorLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfMoRodoCondutorDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfMoRodoCondutorDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfMoRodoCondutorUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfMoRodoCondutorUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfMoRodoCondutorAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfMoRodoCondutorAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtMnfMoRodoCondutor: TDataSource
    DataSet = QrFrtMnfMoRodoCondutor
    Left = 684
    Top = 104
  end
  object QrFrtMnfMoRodoVeicRebo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT rcv.*, its.Nome NO_VEIC, its.Placa'
      'FROM frtmnfmorodoveicrebo rcv'
      'LEFT JOIN trnsits its ON its.Controle=rcv.cInt'
      'WHERE rcv.Codigo>0')
    Left = 768
    Top = 40
    object QrFrtMnfMoRodoVeicReboCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfMoRodoVeicReboControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfMoRodoVeicRebocInt: TIntegerField
      FieldName = 'cInt'
    end
    object QrFrtMnfMoRodoVeicReboLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfMoRodoVeicReboDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfMoRodoVeicReboDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfMoRodoVeicReboUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfMoRodoVeicReboUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfMoRodoVeicReboAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfMoRodoVeicReboAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFrtMnfMoRodoVeicReboNO_VEIC: TWideStringField
      FieldName = 'NO_VEIC'
      Size = 100
    end
    object QrFrtMnfMoRodoVeicReboPlaca: TWideStringField
      FieldName = 'Placa'
      Size = 11
    end
  end
  object DsFrtMnfMoRodoVeicRebo: TDataSource
    DataSet = QrFrtMnfMoRodoVeicRebo
    Left = 768
    Top = 88
  end
  object QrFrtMnfMoRodoValePedg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT rcr.*'
      'FROM frtmnfmorodovalepedg rcr'
      'WHERE rcr.Codigo>0')
    Left = 852
    Top = 60
    object QrFrtMnfMoRodoValePedgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfMoRodoValePedgControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfMoRodoValePedgCNPJForn: TWideStringField
      FieldName = 'CNPJForn'
      Size = 14
    end
    object QrFrtMnfMoRodoValePedgCNPJPg: TWideStringField
      FieldName = 'CNPJPg'
      Size = 14
    end
    object QrFrtMnfMoRodoValePedgnCompra: TWideStringField
      FieldName = 'nCompra'
    end
    object QrFrtMnfMoRodoValePedgLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfMoRodoValePedgDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfMoRodoValePedgDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfMoRodoValePedgUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfMoRodoValePedgUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfMoRodoValePedgAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfMoRodoValePedgAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtMnfMoRodoValePedg: TDataSource
    DataSet = QrFrtMnfMoRodoValePedg
    Left = 852
    Top = 108
  end
  object PMReboque: TPopupMenu
    OnPopup = PMReboquePopup
    Left = 596
    Top = 700
    object IncluiVeculoreboque1: TMenuItem
      Caption = '&Inclui Ve'#237'culo reboque'
      Enabled = False
      OnClick = IncluiVeculoreboque1Click
    end
    object AlteraVeculoreboque1: TMenuItem
      Caption = '&Altera Ve'#237'culo reboque'
      Enabled = False
      OnClick = AlteraVeculoreboque1Click
    end
    object ExcluiVeculoreboque1: TMenuItem
      Caption = '&Exclui Ve'#237'culo reboque'
      Enabled = False
      OnClick = ExcluiVeculoreboque1Click
    end
  end
  object PMValePedg: TPopupMenu
    OnPopup = PMValePedgPopup
    Left = 712
    Top = 700
    object IncluiValePedgio1: TMenuItem
      Caption = '&Inclui Vale Ped'#225'gio'
      Enabled = False
      OnClick = IncluiValePedgio2Click
    end
    object AlteraValePedgio1: TMenuItem
      Caption = '&Altera Vale Ped'#225'gio'
      Enabled = False
      OnClick = AlteraValePedgio1Click
    end
    object ExcluiValePedgio1: TMenuItem
      Caption = '&Exclui Vale Ped'#225'gio'
      Enabled = False
      OnClick = ExcluiValePedgio1Click
    end
  end
  object QrFrtMnfMoRodoCIOT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT rcr.*'
      'FROM frtmnfmorodociot rcr'
      'WHERE rcr.Codigo>0')
    Left = 932
    Top = 40
    object QrFrtMnfMoRodoCIOTCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfMoRodoCIOTControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfMoRodoCIOTCIOT: TLargeintField
      FieldName = 'CIOT'
    end
    object QrFrtMnfMoRodoCIOTLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfMoRodoCIOTDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfMoRodoCIOTDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfMoRodoCIOTUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfMoRodoCIOTUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfMoRodoCIOTAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfMoRodoCIOTAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFrtMnfMoRodoCIOT: TDataSource
    DataSet = QrFrtMnfMoRodoCIOT
    Left = 932
    Top = 88
  end
  object PMCIOT: TPopupMenu
    OnPopup = PMCIOTPopup
    Left = 840
    Top = 696
    object IncluiCIOT1: TMenuItem
      Caption = '&Inclui CIOT'
      Enabled = False
      OnClick = IncluiCIOT1Click
    end
    object AlteraCIOT1: TMenuItem
      Caption = '&Altera CIOT'
      Enabled = False
      OnClick = AlteraCIOT1Click
    end
    object ExcluiCIOT1: TMenuItem
      Caption = '&Exclui CIOT'
      Enabled = False
      OnClick = ExcluiCIOT1Click
    end
  end
  object QrMDFeCabA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMDFeCabAAfterOpen
    Left = 228
    Top = 240
    object QrMDFeCabAStatus: TIntegerField
      FieldName = 'Status'
    end
  end
end
