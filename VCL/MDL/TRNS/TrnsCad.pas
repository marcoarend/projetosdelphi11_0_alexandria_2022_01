unit TrnsCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Variants, UnCTeListas;

type
  TFmTrnsCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label9: TLabel;
    EdRNTRC: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrTrnsCad: TmySQLQuery;
    DsTrnsCad: TDataSource;
    QrTrnsIts: TmySQLQuery;
    DsTrnsIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    Label4: TLabel;
    QrTrnsCadCodigo: TIntegerField;
    QrTrnsCadRNTRC: TWideStringField;
    QrTrnsCadLk: TIntegerField;
    QrTrnsCadDataCad: TDateField;
    QrTrnsCadDataAlt: TDateField;
    QrTrnsCadUserCad: TIntegerField;
    QrTrnsCadUserAlt: TIntegerField;
    QrTrnsCadAlterWeb: TSmallintField;
    QrTrnsCadAtivo: TSmallintField;
    QrTrnsItsCodigo: TIntegerField;
    QrTrnsItsControle: TIntegerField;
    QrTrnsItsNome: TWideStringField;
    QrTrnsItsRENAVAM: TWideStringField;
    QrTrnsItsPlaca: TWideStringField;
    QrTrnsItsTaraKg: TIntegerField;
    QrTrnsItsCapKg: TIntegerField;
    QrTrnsItsTpVeic: TSmallintField;
    QrTrnsItsTpRod: TSmallintField;
    QrTrnsItsTpCar: TSmallintField;
    QrTrnsItsLk: TIntegerField;
    QrTrnsItsDataCad: TDateField;
    QrTrnsItsDataAlt: TDateField;
    QrTrnsItsUserCad: TIntegerField;
    QrTrnsItsUserAlt: TIntegerField;
    QrTrnsItsAlterWeb: TSmallintField;
    QrTrnsItsAtivo: TSmallintField;
    QrTrnsCadNO_ENT: TWideStringField;
    QrTrnsCadNO_FantApel: TWideStringField;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    QrTrnsCadCNPJ_CPF: TWideStringField;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    Label7: TLabel;
    DBEdit3: TDBEdit;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    Label10: TLabel;
    DBEdit5: TDBEdit;
    QrTrnsItsNO_TpVeic: TWideStringField;
    QrTrnsItsNO_TpRod: TWideStringField;
    QrTrnsItsNO_TpCar: TWideStringField;
    QrTrnsItsCapM3: TIntegerField;
    QrTrnsItsUF: TWideStringField;
    RGTpProp: TdmkRadioGroup;
    QrTrnsCadTpProp: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTrnsCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTrnsCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTrnsCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrTrnsCadBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormTrnsIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenTrnsIts(Controle: Integer);
    procedure LocCod(Atual, Codigo: Integer);

  end;

var
  FmTrnsCad: TFmTrnsCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, TrnsIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTrnsCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTrnsCad.MostraFormTrnsIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmTrnsIts, FmTrnsIts, afmoNegarComAviso) then
  begin
    FmTrnsIts.ImgTipo.SQLType := SQLType;
    FmTrnsIts.FQrCab := QrTrnsCad;
    FmTrnsIts.FDsCab := DsTrnsCad;
    FmTrnsIts.FQrIts := QrTrnsIts;
    if SQLType = stIns then
      //
    else
    begin
      FmTrnsIts.EdControle.ValueVariant := QrTrnsItsControle.Value;
      //
      FmTrnsIts.EdNome.Text              := QrTrnsItsNome.Value;
      FmTrnsIts.EdRENAVAM.ValueVariant   := QrTrnsItsRENAVAM.Value;
      FmTrnsIts.EdPlaca.ValueVariant     := QrTrnsItsPlaca.Value;
      FmTrnsIts.EdTaraKg.ValueVariant    := QrTrnsItsTaraKg.Value;
      FmTrnsIts.EdCapKg.ValueVariant     := QrTrnsItsCapKg.Value;
      FmTrnsIts.RGTpVeic.ItemIndex       := QrTrnsItsTpVeic.Value;
      FmTrnsIts.RGTpRod.ItemIndex        := QrTrnsItsTpRod.Value;
      FmTrnsIts.RGTpCar.ItemIndex        := QrTrnsItsTpCar.Value;
      FmTrnsIts.Ed_SEL_.ValueVariant     := 0;//QrTrnsIts.Value;
      FmTrnsIts.CB_SEL_.KeyValue         := Null; //QrTrnsIts.Value;
      FmTrnsIts.EdCapM3.ValueVariant     := QrTrnsItsCapM3.Value;
      FmTrnsIts.EdUF.ValueVariant        := QrTrnsItsUF.Value;
    end;
    FmTrnsIts.ShowModal;
    FmTrnsIts.Destroy;
  end;
end;

procedure TFmTrnsCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTrnsCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrTrnsCad, QrTrnsIts);
end;

procedure TFmTrnsCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrTrnsCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrTrnsIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrTrnsIts);
end;

procedure TFmTrnsCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTrnsCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTrnsCad.DefParams;
begin
  VAR_GOTOTABELA := 'trnscad';
  VAR_GOTOMYSQLTABLE := QrTrnsCad;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.Fantasia, ent.Apelido) NO_FantApel, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, ');
  VAR_SQLx.Add('trn.* ');
  VAR_SQLx.Add('FROM trnscad trn ');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=trn.Codigo ');
  //
  VAR_SQL1.Add('WHERE trn.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE :P0');
  //
end;

procedure TFmTrnsCad.ItsAltera1Click(Sender: TObject);
begin
  MostraFormTrnsIts(stUpd);
end;

procedure TFmTrnsCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmTrnsCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTrnsCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTrnsCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  trnsits, 'Controle', QrTrnsItsControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrTrnsIts,
      QrTrnsItsControle, QrTrnsItsControle.Value);
    ReopenTrnsIts(Controle);
  end;
}
end;

procedure TFmTrnsCad.ReopenTrnsIts(Controle: Integer);
var
  ATT_CTeTpVeic, ATT_CTeTpRod, ATT_CTeTpCar: String;
begin
  ATT_CTeTpVeic := dmkPF.ArrayToTexto('tri.TpVeic', 'NO_TpVeic', pvPos, True,
    sCTeTpVeic);
  ATT_CTeTpRod := dmkPF.ArrayToTexto('tri.TpRod', 'NO_TpRod', pvPos, True,
    sCTeTpRod);
  ATT_CTeTpCar := dmkPF.ArrayToTexto('tri.TpCar', 'NO_TpCar', pvPos, True,
    sCTeTpCar);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTrnsIts, Dmod.MyDB, [
  'SELECT ',
  ATT_CTeTpVeic,
  ATT_CTeTpRod,
  ATT_CTeTpCar,
  'tri.* ',
  'FROM trnsits tri',
  'WHERE Codigo=' + Geral.FF0(QrTrnsCadCodigo.Value),
  '']);
  //
  QrTrnsIts.Locate('Controle', Controle, []);
end;


procedure TFmTrnsCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTrnsCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTrnsCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTrnsCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTrnsCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTrnsCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTrnsCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTrnsCadCodigo.Value;
  VAR_CAD_ITEM := QrTrnsItsControle.Value;
  Close;
end;

procedure TFmTrnsCad.ItsInclui1Click(Sender: TObject);
begin
  MostraFormTrnsIts(stIns);
end;

procedure TFmTrnsCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTrnsCad, [PnDados],
  [PnEdita], EdRNTRC, ImgTipo, 'trnscad');
end;

procedure TFmTrnsCad.BtConfirmaClick(Sender: TObject);
var
  Codigo, TpProp: Integer;
  RNTRC: String;
begin
  Codigo := QrTrnsCadCodigo.Value;
  RNTRC  := EdRNTRC.ValueVariant;
  TpProp := RGTpProp.ItemIndex;
  //
  if MyObjects.FIC(Length(RNTRC) < 8, EdRNTRC, 'Informe o RNTRC com 8 casas decimais!') then Exit;
  if MyObjects.FIC(TpProp = 3, RGTpProp, 'Informe o tipo de propriet�rio!') then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'trnscad', False, [
  'RNTRC', 'TpProp'], [
  'Codigo'], [
  RNTRC, TpProp], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmTrnsCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := QrTrnsCadCodigo.Value;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'trnscad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'trnscad', 'Codigo');
end;

procedure TFmTrnsCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmTrnsCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTrnsCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmTrnsCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTrnsCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTrnsCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTrnsCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrTrnsCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTrnsCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTrnsCad.QrTrnsCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTrnsCad.QrTrnsCadAfterScroll(DataSet: TDataSet);
begin
  ReopenTrnsIts(0);
end;

procedure TFmTrnsCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTrnsCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmTrnsCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTrnsCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'trnscad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTrnsCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTrnsCad.CabInclui1Click(Sender: TObject);
(*
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTrnsCad, [PnDados],
  [PnEdita], EdRNTRC, ImgTipo, 'trnscad');
*)
var
  Novas, Transp, Codigo: Integer;
  Qry: TmySQLQUery;
begin
(*
  BtDefaultDirs.Enabled := False;
*)
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ent.Codigo Entidade, trn.Codigo Transp ',
    'FROM entidades ent ',
    'LEFT JOIN trnscad trn ON trn.Codigo=ent.Codigo ',
    'WHERE trn.Codigo IS NULL ',
    'AND ent.Fornece2="V" ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Novas := 0;
      while not Qry.Eof do
      begin
        Transp := Qry.FieldByName('Transp').AsInteger;
        if (Transp = 0) (*and (DModG.QrFiliaisSPEntidade.Value < 0)*)  then
        begin
          Novas := Novas + 1;
          Codigo := Qry.FieldByName('Entidade').AsInteger;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'trnscad', False, [
          ], [
          'Codigo'], [
          ], [
          Codigo], True)
        end;
        //
        Qry.Next;
      end;
      case Novas of
          0: Geral.MB_Aviso('N�o foi localizado nenhuma nova transportador!');
          1: Geral.MB_Aviso('Foi adicionada uma nova transportador!');
        else Geral.MB_Aviso('Foram adicionadas ' + Geral.FF0(Novas) + ' novas transportadors!');
      end;
      if Novas > 0 then
        Va(vpLast);
    end else
      Geral.MB_Aviso('N�o foi localizado nenhuma transportador!');
  finally
    Qry.Free;
  end;
  //
end;

procedure TFmTrnsCad.QrTrnsCadBeforeClose(
  DataSet: TDataSet);
begin
  QrTrnsIts.Close;
end;

procedure TFmTrnsCad.QrTrnsCadBeforeOpen(DataSet: TDataSet);
begin
  QrTrnsCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

