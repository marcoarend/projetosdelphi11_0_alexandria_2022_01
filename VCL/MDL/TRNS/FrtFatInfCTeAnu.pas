unit FrtFatInfCTeAnu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.ComCtrls,
  dmkEditDateTimePicker;

type
  TFmFrtFatInfCTeAnu = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label27: TLabel;
    EdChCTe: TdmkEdit;
    SBChCTe: TSpeedButton;
    Label32: TLabel;
    TPdEmi: TdmkEditDateTimePicker;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopanFrtFatInfCTeAnu();
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtFatInfCTeAnu: TFmFrtFatInfCTeAnu;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnMySQLCuringa, UnUMedi_PF, UnidMed, UnFrt_PF;

{$R *.DFM}

procedure TFmFrtFatInfCTeAnu.BtOKClick(Sender: TObject);
var
  chCte, dEmi: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  chCte          := EdchCte.Text;
  dEmi           := Geral.FDT(TPdEmi.Date, 1);
  //
  if MyObjects.FIC(Trim(chCte) = '', EdchCte, 'Informe a chave do CTe!') then Exit;
  if MyObjects.FIC(TPdEmi.Date < 2, TPdEmi, 'Informe a data da emiss�o!') then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtfatinfcteanu', False, [
  'chCte', 'dEmi'], [
  'Codigo'], [
  chCte, dEmi], [
  Codigo], True) then
  begin
    ReopanFrtFatInfCTeAnu();
    Close;
  end;
end;

procedure TFmFrtFatInfCTeAnu.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtFatInfCTeAnu.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtFatInfCTeAnu.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmFrtFatInfCTeAnu.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtFatInfCTeAnu.ReopanFrtFatInfCTeAnu();
begin
  if FQrIts <> nil then
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
end;

end.
