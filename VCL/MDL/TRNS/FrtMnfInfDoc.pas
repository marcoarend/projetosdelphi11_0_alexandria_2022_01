unit FrtMnfInfDoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmFrtMnfInfDoc = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrDTB_Munici: TmySQLQuery;
    QrDTB_MuniciCodigo: TIntegerField;
    QrDTB_MuniciNome: TWideStringField;
    DsDTB_Munici: TDataSource;
    Label2: TLabel;
    EdMunDescarga: TdmkEditCB;
    CBMunDescarga: TdmkDBLookupComboBox;
    EdDocMod: TdmkEdit;
    Label197: TLabel;
    Label198: TLabel;
    EdDocSerie: TdmkEdit;
    EdDocNumero: TdmkEdit;
    Label199: TLabel;
    Label1: TLabel;
    QrTrnsIts: TmySQLQuery;
    QrTrnsItsControle: TIntegerField;
    QrTrnsItsDESCRI: TWideStringField;
    DsTrnsIts: TDataSource;
    EdTrnsIts: TdmkEditCB;
    Label4: TLabel;
    CBTrnsIts: TdmkDBLookupComboBox;
    EdDocChave: TdmkEdit;
    Label7: TLabel;
    EdSegCodBarra: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdDocChaveRedefinido(Sender: TObject);
    procedure EdSegCodBarraRedefinido(Sender: TObject);
  private
    { Private declarations }
    FAlterando: Boolean;
    procedure ReopenFrtMnfInfDoc(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtMnfInfDoc: TFmFrtMnfInfDoc;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral, UnXXe_PF, ModuleMDFe_0000, UnMDFe_PF;

{$R *.DFM}

procedure TFmFrtMnfInfDoc.BtOKClick(Sender: TObject);
var
  xMunDescarga, DocChave, SegCodBarra: String;
  Codigo, Controle, cMunDescarga, DocMod, DocSerie, DocNumero, TrnsIts: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  cMunDescarga   := EdMunDescarga.ValueVariant;
  xMunDescarga   := CBMunDescarga.Text;
  DocMod         := EdDocMod.ValueVariant;
  DocSerie       := EdDocSerie.ValueVariant;
  DocNumero      := EdDocNumero.ValueVariant;
  DocChave       := EdDocChave.Text;
  SegCodBarra    := EdSegCodBarra.Text;
  TrnsIts        := EdTrnsIts.ValueVariant;

  //
  if MyObjects.FIC(Trim(DocChave) = '', EdDocChave,
  'Informe a chave do documento!') then Exit;
  //
  Controle := UMyMod.BPGS1I32('frtmnfinfdoc', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtmnfinfdoc', False, [
  'Codigo', 'cMunDescarga', 'xMunDescarga',
  'DocMod', 'DocSerie', 'DocNumero',
  'DocChave', 'SegCodBarra', 'TrnsIts'], [
  'Controle'], [
  Codigo, cMunDescarga, xMunDescarga,
  DocMod, DocSerie, DocNumero,
  DocChave, SegCodBarra, TrnsIts], [
  Controle], True) then
  begin
    MDFe_PF.TotaisMDFe(Codigo, FQrCab);
    ReopenFrtMnfInfDoc(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType             := stIns;
      EdControle.ValueVariant     := 0;
      EdDocChave.Text             := '';
      EdSegCodBarra.Text          := '';
      EdMunDescarga.ValueVariant  := 0;
      CBMunDescarga.KeyValue      := Null;
      EdDocMod.ValueVariant       := 0;
      EdDocSerie.ValueVariant     := 0;
      EdDocNumero.ValueVariant    := 0;
      EdTrnsIts.ValueVariant      := 0;
      CBTrnsIts.KeyValue          := Null;
      //
      EdDocChave.SetFocus;
    end else Close;
  end;
end;

procedure TFmFrtMnfInfDoc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtMnfInfDoc.EdDocChaveRedefinido(Sender: TObject);
const
  Versao = 0.00;
  Colunas = 1;
var
  Chave: String;
  UF_IBGE, AAMM, CNPJ, DocMod, DocSer, DocNum, tpEmis, CodSeg: String;
  Qry: TmySQLQuery;
  Indice: Integer;
  ItensX: array of String;
  ItensC: array of Integer;
begin
  if FAlterando then
    Exit;
  if EdDocChave.Lines.Count > 1 then
    EdDocChave.Text := EdDocChave.Lines[0];
  Chave := EdDocChave.Lines[0];
  if Length(Chave) = 44 then
  begin
    if XXe_PF.DesmontaChaveDeAcesso(Chave, Versao, UF_IBGE, AAMM, CNPJ, DocMod,
    DocSer, DocNum, tpEmis, CodSeg) then
    begin
      EdDocMod.Text    := DocMod;
      EdDocSerie.Text  := DocSer;
      EdDocNumero.Text := DocNum;
      //
      if EdDocMod.ValueVariant = 57 then
      begin
        Qry := TmySQLQuery.Create(Dmod);
        try
          //
          UnDmkDAC_PF.AbremySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT cta.ide_cMunFim, crv.cInt, crv.Placa',
          'FROM ctecaba cta',
          'LEFT JOIN ctemorodoveic crv ON ',
          '  crv.FatID=cta.FatID AND',
          '  crv.FatNum=cta.FatNum AND',
          '  crv.Empresa=cta.Empresa',
          'WHERE cta.id="' + Chave + '"',
          '']);
          //
          if Qry.RecordCount > 0 then
          begin
            EdMunDescarga.ValueVariant := Qry.FieldByName('ide_cMunFim').AsInteger;
            CBMunDescarga.KeyValue     := Qry.FieldByName('ide_cMunFim').AsInteger;
            if Qry.RecordCount = 1 then
            begin
              EdTrnsIts.ValueVariant := Qry.FieldByName('cInt').AsInteger;
              CBTrnsIts.KeyValue     := Qry.FieldByName('cInt').AsInteger;
            end;
            SetLength(ItensX, Qry.RecordCount);
            SetLength(ItensC, Qry.RecordCount);
            Qry.First;
            while not Qry.Eof do
            begin
              ItensX[Qry.RecNo - 1] := Qry.FieldByName('placa').AsString;
              ItensC[Qry.RecNo - 1] := Qry.FieldByName('cInt').AsInteger;
              //
              Qry.Next;
            end;
            Indice := MyObjects.SelRadioGroup('Ve�culo onda estar� a carga:',
            'Informe o ve�culo:', ItensX, Colunas);
            if Indice > -1 then
            begin
              EdTrnsIts.ValueVariant := ItensC[Indice];
              CBTrnsIts.KeyValue     := ItensC[Indice];
            end;
          end;
        finally
          Qry.Free;
        end;
      end;
    end;
  end;
end;

procedure TFmFrtMnfInfDoc.EdSegCodBarraRedefinido(Sender: TObject);
begin
  if FAlterando then
    Exit;
  if EdSegCodBarra.Lines.Count > 1 then
    EdSegCodBarra.Text := EdSegCodBarra.Lines[0];
end;

procedure TFmFrtMnfInfDoc.FormActivate(Sender: TObject);
begin
  FAlterando := False;
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtMnfInfDoc.FormCreate(Sender: TObject);
begin
  FAlterando := True;
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrDTB_Munici, DModG.AllID_DB);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTrnsIts, Dmod.MyDB, [
  'SELECT Controle, ',
  'CONCAT(Placa, "  ", Nome) DESCRI ',
  'FROM trnsits ',
  //'WHERE tpVeic=' + Geral.FF0(Integer(ctetpveicReboque)),
  'ORDER BY DESCRI ',
  '']);
end;

procedure TFmFrtMnfInfDoc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtMnfInfDoc.ReopenFrtMnfInfDoc(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
