unit FrtMnfMoRodoValePedg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmFrtMnfMoRodoValePedg = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdnCompra: TdmkEdit;
    Label3: TLabel;
    Label11: TLabel;
    EdCNPJForn: TdmkEdit;
    EdCNPJPg: TdmkEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenFrMnfMoRodotValePedg(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtMnfMoRodoValePedg: TFmFrtMnfMoRodoValePedg;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnMySQLCuringa, UnUMedi_PF, UnidMed, UnFrt_PF;

{$R *.DFM}

procedure TFmFrtMnfMoRodoValePedg.BtOKClick(Sender: TObject);
var
  CNPJForn, CNPJPg, nCompra: String;
  Codigo, Controle: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  CNPJForn       := Geral.SoNumero_TT(EdCNPJForn.Text);
  CNPJPg         := Geral.SoNumero_TT(EdCNPJPg.Text);
  nCompra        := EdnCompra.Text;
  //
  if MyObjects.FIC(Trim(CNPJForn) = '', EdCNPJForn,
  'Informe o CNPJ da Fornecedora!') then
    Exit;
  if MyObjects.FIC(Trim(nCompra) = '', EdnCompra,
  'Informe o n�mero do comprovante da compra!') then
    Exit;
  if MyObjects.FIC(nCompra <> Geral.SoNumero_TT(nCompra), EdnCompra,
  'O n�mero do comprovante da compra s� aceita n�meros!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('frtmnfmorodovalepedg', 'Controle', '', '',
    tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtmnfmorodovalepedg', False, [
  'Codigo', 'CNPJForn', 'CNPJPg',
  'nCompra'], [
  'Controle'], [
  Codigo, CNPJForn, CNPJPg,
  nCompra], [
  Controle], True) then
  begin
    ReopenFrMnfMoRodotValePedg(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      EdControle.ValueVariant  := 0;
      EdCNPJForn.Text          := '';
      EdnCompra.Text           := '';
      EdCNPJPg.Text            := '';
      //
      EdCNPJForn.SetFocus;
    end else Close;
  end;
end;

procedure TFmFrtMnfMoRodoValePedg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtMnfMoRodoValePedg.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtMnfMoRodoValePedg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmFrtMnfMoRodoValePedg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtMnfMoRodoValePedg.ReopenFrMnfMoRodotValePedg(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
