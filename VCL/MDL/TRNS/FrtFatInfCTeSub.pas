unit FrtFatInfCTeSub;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.ComCtrls,
  dmkEditDateTimePicker;

type
  TFmFrtFatInfCTeSub = class(TForm)
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    PCContribICMS: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet0: TTabSheet;
    PCTipoDoc: TPageControl;
    TabSheet10: TTabSheet;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    Panel3: TPanel;
    Label15: TLabel;
    SbrefCTe: TSpeedButton;
    EdrefNFe: TdmkEdit;
    Panel5: TPanel;
    RGTipoDoc: TdmkRadioGroup;
    Panel8: TPanel;
    Label27: TLabel;
    EdChCTe: TdmkEdit;
    SBChCTe: TSpeedButton;
    RGContribICMS: TdmkRadioGroup;
    Panel9: TPanel;
    Label28: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label38: TLabel;
    Edmod_: TdmkEdit;
    Edserie: TdmkEdit;
    Edsubserie: TdmkEdit;
    TPdEmi: TdmkEditDateTimePicker;
    Edvalor: TdmkEdit;
    Label120: TLabel;
    EdCNPJ: TdmkEdit;
    Label136: TLabel;
    EdCPF: TdmkEdit;
    Label1: TLabel;
    Ednro: TdmkEdit;
    TabSheet13: TTabSheet;
    Panel6: TPanel;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    EdrefCTeAnu: TdmkEdit;
    Panel13: TPanel;
    Label3: TLabel;
    SpeedButton2: TSpeedButton;
    EdrefCTe: TdmkEdit;
    EdControle: TdmkEdit;
    LaControle: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGContribICMSClick(Sender: TObject);
    procedure EdrefNFeRedefinido(Sender: TObject);
    procedure EdrefCTeRedefinido(Sender: TObject);
    procedure RGTipoDocClick(Sender: TObject);
    procedure EdChCTeRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenFrtFatInfNFe();
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtFatInfCTeSub: TFmFrtFatInfCTeSub;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnMySQLCuringa, UnUMedi_PF, UnidMed, UnFrt_PF, UnXXe_PF;

{$R *.DFM}

procedure TFmFrtFatInfCTeSub.BtOKClick(Sender: TObject);
var
  ChCTe, refNFe, CNPJ, CPF, mod_, dEmi, refCTe, refCTeAnu: String;
  Codigo, ContribICMS, TipoDoc, serie, subserie, nro: Integer;
  valor: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  //
  ChCTe          := EdChCTe.Text;
  ContribICMS    := RGContribICMS.ItemIndex;;
  TipoDoc        := RGTipoDoc.ItemIndex;
  refNFe         := EdrefNFe.Text;
  CNPJ           := Geral.SoNumero_TT(EdCNPJ.Text);
  CPF            := Geral.SoNumero_TT(EdCPF.Text);
  mod_           := Edmod_.Text;
  serie          := Edserie.ValueVariant;
  subserie       := Edsubserie.ValueVariant;
  nro            := Ednro.ValueVariant;
  valor          := Edvalor.ValueVariant;
  dEmi           := Geral.FDT(TPdEmi.Date, 1);
  refCTe         := EdrefCTe.Text;
  refCTeAnu      := EdrefCTeAnu.Text;
  //
  if MyObjects.FIC(ContribICMS = 0, RGContribICMS, 'Informe o item ICMS!') then
    Exit;
  case ContribICMS of
    1:
    begin
      if MyObjects.FIC(TipoDoc = 0, RGTipoDoc, 'Informe o tipo de documento!') then
        Exit;
      case TipoDoc of
        1:
        begin
          if MyObjects.FIC(Trim(refNFe) = '', EdrefNFe,
          'informe a chave da NF-e!') then
            Exit;

        end;
        2:
        begin
          if MyObjects.FIC(Trim(CNPJ + CPF) = '', nil,
          'Informe o CPF ou CNPJ!') then
            Exit;
          if MyObjects.FIC((Trim(CNPJ) <> '') and (Trim(CPF) <> ''), nil,
          'Informe o apenas CPF ou apenas o CNPJ!') then
            Exit;
          if MyObjects.FIC(Trim(mod_) = '', EdMod_,
          'Informe o modelo do documento!') then
            Exit;
          if MyObjects.FIC(nro = 0, Ednro,
          'Informe o n�mero do documento!') then
            Exit;
          if MyObjects.FIC(TPdEmi.Date < 2, TPdEmi,
          'Informe a data de emiss�o!') then
            Exit;
        end;
        3:
        begin
          if MyObjects.FIC(Trim(refCTe) = '', EdrefCTe,
          'informe a chave CT-e!') then
            Exit;
        end;
      end;
    end;
    2:
    begin
      if MyObjects.FIC(Trim(refCTeAnu) = '', EdrefCTeAnu,
      'informe a chave CT-e de anula��o!') then
        Exit;
    end;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtfatinfctesub', False, [
  'ChCTe', 'ContribICMS', 'TipoDoc',
  'refNFe', 'CNPJ', 'CPF',
  'mod_', 'serie', 'subserie',
  'nro', 'valor', 'dEmi',
  'refCTe', 'refCTeAnu'], [
  'Codigo'], [
  ChCTe, ContribICMS, TipoDoc,
  refNFe, CNPJ, CPF,
  mod_, serie, subserie,
  nro, valor, dEmi,
  refCTe, refCTeAnu], [
  Codigo], True) then
  begin
    ReopenFrtFatInfNFe();
    Close;
  end;
end;

procedure TFmFrtFatInfCTeSub.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtFatInfCTeSub.EdChCTeRedefinido(Sender: TObject);
begin
  if not XXe_PF.VerificaChaveAcessoXXe(EdchCTe.Text, True) then
  try
    EdchCTe.SetFocus;
  except
    //
  end;
end;

procedure TFmFrtFatInfCTeSub.EdrefCTeRedefinido(Sender: TObject);
begin
  if not XXe_PF.VerificaChaveAcessoXXe(EdrefCTE.Text, True) then
    EdrefCTe.SetFocus;
end;

procedure TFmFrtFatInfCTeSub.EdrefNFeRedefinido(Sender: TObject);
begin
  if not XXe_PF.VerificaChaveAcessoXXe(EdrefNFe.Text, True) then
    EdrefNFe.SetFocus;
end;

procedure TFmFrtFatInfCTeSub.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtFatInfCTeSub.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  RGContribICMS.ItemIndex := 1;
  RGContribICMS.ItemIndex := 0;
  //
  RGTipoDoc.ItemIndex := 1;
  RGTipoDoc.ItemIndex := 0;
end;

procedure TFmFrtFatInfCTeSub.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtFatInfCTeSub.ReopenFrtFatInfNFe();
begin
  if FQrIts <> nil then
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
end;

procedure TFmFrtFatInfCTeSub.RGContribICMSClick(Sender: TObject);
begin
  PCContribICMS.Visible := False;
  //
  TabSheet0.TabVisible := True;
  TabSheet1.TabVisible := True;
  TabSheet2.TabVisible := True;
  //
  PCContribICMS.ActivePageIndex := RGContribICMS.ItemIndex;
  case RGContribICMS.ItemIndex of
    0: PCContribICMS.ActivePage := TabSheet0;
    1: PCContribICMS.ActivePage := TabSheet1;
    2: PCContribICMS.ActivePage := TabSheet2;
  end;
  //
  TabSheet0.TabVisible := RGContribICMS.ItemIndex = 0;
  TabSheet1.TabVisible := RGContribICMS.ItemIndex = 1;
  TabSheet2.TabVisible := RGContribICMS.ItemIndex = 2;
  //
  PCContribICMS.Visible := True;
end;

procedure TFmFrtFatInfCTeSub.RGTipoDocClick(Sender: TObject);
begin
  PCTipoDoc.Visible := False;
  //
  TabSheet10.TabVisible := True;
  TabSheet11.TabVisible := True;
  TabSheet12.TabVisible := True;
  TabSheet13.TabVisible := True;
  //
  PCTipoDoc.ActivePageIndex := RGTipoDoc.ItemIndex;
  case RGTipoDoc.ItemIndex of
    0: PCTipoDoc.ActivePage := TabSheet10;
    1: PCTipoDoc.ActivePage := TabSheet11;
    2: PCTipoDoc.ActivePage := TabSheet12;
    3: PCTipoDoc.ActivePage := TabSheet13;
  end;
  //
  TabSheet10.TabVisible := RGTipoDoc.ItemIndex = 0;
  TabSheet11.TabVisible := RGTipoDoc.ItemIndex = 1;
  TabSheet12.TabVisible := RGTipoDoc.ItemIndex = 2;
  TabSheet13.TabVisible := RGTipoDoc.ItemIndex = 3;
  //
  PCTipoDoc.Visible := True;
end;

end.
