unit FrtMnfMoRodoVeicRebo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmFrtMnfMoRodoVeicRebo = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBcInt: TdmkDBLookupComboBox;
    EdcInt: TdmkEditCB;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrTrnsIts: TmySQLQuery;
    QrTrnsItsControle: TIntegerField;
    QrTrnsItsDESCRI: TWideStringField;
    DsTrnsIts: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenFrMnfMoRodotVeicRebo(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtMnfMoRodoVeicRebo: TFmFrtMnfMoRodoVeicRebo;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnTrns_PF;

{$R *.DFM}

procedure TFmFrtMnfMoRodoVeicRebo.BtOKClick(Sender: TObject);
var
  Codigo, Controle, cInt: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  cInt           := EdcInt.ValueVariant;
  //
  if MyObjects.FIC(cInt = 0, EdcInt, 'Informe o ve�culo reboque!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('frtmnfmorodoveicrebo', 'Controle', '', '',
    tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtmnfmorodoveicrebo', False, [
  'Codigo', 'cInt'], [
  'Controle'], [
  Codigo, cInt], [
  Controle], True) then
  begin
    ReopenFrMnfMoRodotVeicRebo(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      //
      EdcInt.ValueVariant      := 0;
      CBcInt.KeyValue          := Null;
      EdcInt.SetFocus;
    end else Close;
  end;
end;

procedure TFmFrtMnfMoRodoVeicRebo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtMnfMoRodoVeicRebo.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtMnfMoRodoVeicRebo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTrnsIts, Dmod.MyDB, [
  'SELECT Controle, ',
  'CONCAT(Placa, "  ", Nome) DESCRI ',
  'FROM trnsits ',
  'WHERE tpVeic=' + Geral.FF0(Integer(ctetpveicReboque)),
  'ORDER BY DESCRI ',
  '']);
end;

procedure TFmFrtMnfMoRodoVeicRebo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtMnfMoRodoVeicRebo.ReopenFrMnfMoRodotVeicRebo(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmFrtMnfMoRodoVeicRebo.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Trns_PF.CadastroESelecaoDeTrnsIts(QrTrnsIts, EdcInt, CBcInt);
end;

end.
