object FmFrtFatInfQ: TFmFrtFatInfQ
  Left = 339
  Top = 185
  Caption = 'FAT-FRETE-002 :: Faturamento de Frete - Quantidades de Carga'
  ClientHeight = 389
  ClientWidth = 675
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 249
    Width = 675
    Height = 26
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 675
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 675
    Height = 137
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label1: TLabel
      Left = 12
      Top = 108
      Width = 117
      Height = 13
      Caption = 'Unidade de Medida [F3]:'
      Visible = False
    end
    object SpeedButton1: TSpeedButton
      Left = 308
      Top = 123
      Width = 21
      Height = 22
      Caption = '...'
      Visible = False
      OnClick = SpeedButton1Click
    end
    object Label2: TLabel
      Left = 96
      Top = 16
      Width = 76
      Height = 13
      Caption = 'Tipo de medida:'
    end
    object SbCaracAd: TSpeedButton
      Left = 508
      Top = 32
      Width = 23
      Height = 22
      OnClick = SbCaracAdClick
    end
    object Label4: TLabel
      Left = 532
      Top = 16
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdUnidMed: TdmkEditCB
      Left = 12
      Top = 124
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      Visible = False
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdUnidMedChange
      OnKeyDown = EdUnidMedKeyDown
      DBLookupComboBox = CBUnidMed
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdSigla: TdmkEdit
      Left = 68
      Top = 124
      Width = 40
      Height = 21
      TabOrder = 2
      Visible = False
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdSiglaChange
      OnExit = EdSiglaExit
      OnKeyDown = EdSiglaKeyDown
    end
    object CBUnidMed: TdmkDBLookupComboBox
      Left = 108
      Top = 124
      Width = 201
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsUnidMed
      TabOrder = 3
      Visible = False
      OnKeyDown = CBUnidMedKeyDown
      dmkEditCB = EdUnidMed
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdMyTpMed: TdmkEditCB
      Left = 96
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MyTpMed'
      UpdCampo = 'MyTpMed'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBMyTpMed
      IgnoraDBLookupComboBox = True
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBMyTpMed: TdmkDBLookupComboBox
      Left = 153
      Top = 32
      Width = 352
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsTrnsMyTpMed
      TabOrder = 5
      dmkEditCB = EdMyTpMed
      QryCampo = 'MyTpMed'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdQCarga: TdmkEdit
      Left = 532
      Top = 32
      Width = 121
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      QryCampo = 'QCarga'
      UpdCampo = 'QCarga'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object RGCUnid: TdmkRadioGroup
      Left = 12
      Top = 56
      Width = 641
      Height = 45
      Caption = ' Unidade de medida: '
      Columns = 7
      ItemIndex = 6
      Items.Strings = (
        '00-M3'
        '01-KG'
        '02-TON'
        '03-UNIDADE'
        '04-LITROS'
        '05-MMBTU'
        '---Indefinido')
      TabOrder = 7
      QryCampo = 'CUnid'
      UpdCampo = 'CUnid'
      UpdType = utYes
      OldValor = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 675
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 627
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 579
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 563
        Height = 32
        Caption = 'Faturamento de Frete - Quantidades de Carga'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 563
        Height = 32
        Caption = 'Faturamento de Frete - Quantidades de Carga'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 563
        Height = 32
        Caption = 'Faturamento de Frete - Quantidades de Carga'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 275
    Width = 675
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 671
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 319
    Width = 675
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 529
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 527
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrUnidMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 172
    Top = 48
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 172
    Top = 96
  end
  object VUUnidMed: TdmkValUsu
    dmkEditCB = EdUnidMed
    QryCampo = 'UnidMed'
    UpdCampo = 'UnidMed'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 172
    Top = 148
  end
  object QrTrnsMyTpMed: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM trnsmytpmed'
      'ORDER BY Nome')
    Left = 448
    Top = 60
    object QrTrnsMyTpMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTrnsMyTpMedNome: TWideStringField
      DisplayWidth = 20
      FieldName = 'Nome'
    end
  end
  object DsTrnsMyTpMed: TDataSource
    DataSet = QrTrnsMyTpMed
    Left = 448
    Top = 108
  end
end
