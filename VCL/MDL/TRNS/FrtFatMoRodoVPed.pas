unit FrtFatMoRodoVPed;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmFrtFatMoRodoVPed = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdvValePed: TdmkEdit;
    Label4: TLabel;
    EdnCompra: TdmkEdit;
    Label3: TLabel;
    Label11: TLabel;
    EdCNPJForn: TdmkEdit;
    EdCNPJPg: TdmkEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopanFrtFatVPed(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtFatMoRodoVPed: TFmFrtFatMoRodoVPed;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnMySQLCuringa, UnUMedi_PF, UnidMed, UnFrt_PF;

{$R *.DFM}

procedure TFmFrtFatMoRodoVPed.BtOKClick(Sender: TObject);
var
  CNPJForn, nCompra, CNPJPg: String;
  Codigo, Controle: Integer;
  vValePed: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  CNPJForn       := Geral.SoNumero_TT(EdCNPJForn.Text);
  nCompra        := EdnCompra.Text;
  CNPJPg         := Geral.SoNumero_TT(EdCNPJPg.Text);
  vValePed       := EdvValePed.ValueVariant;
  //
  if MyObjects.FIC(Trim(CNPJForn) = '', EdCNPJForn,
  'Informe o CNPJ da Fornecedora!') then
    Exit;
  if MyObjects.FIC(Trim(nCompra) = '', EdnCompra,
  'Informe o n�mero do comprovante da compra!') then
    Exit;
  if MyObjects.FIC(nCompra <> Geral.SoNumero_TT(nCompra), EdnCompra,
  'O n�mero do comprovante da compra s� aceita n�meros!') then
    Exit;
  if MyObjects.FIC(vValePed < 0.01, EdvValePed,
  'Informe o valor do vale ped�gio!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('frtfatmorodovped', 'Controle', '', '', tsPos,
    SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtfatmorodovped', False, [
  'Codigo', 'CNPJForn', 'nCompra',
  'CNPJPg', 'vValePed'], [
  'Controle'], [
  Codigo, CNPJForn, nCompra,
  CNPJPg, vValePed], [
  Controle], True) then
  begin
    ReopanFrtFatVPed(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      EdControle.ValueVariant  := 0;
      EdCNPJForn.Text          := '';
      EdnCompra.Text           := '';
      EdCNPJPg.Text            := '';
      EdvValePed.ValueVariant  := 0;
      //
      EdCNPJForn.SetFocus;
    end else Close;
  end;
end;

procedure TFmFrtFatMoRodoVPed.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtFatMoRodoVPed.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtFatMoRodoVPed.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmFrtFatMoRodoVPed.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtFatMoRodoVPed.ReopanFrtFatVPed(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
