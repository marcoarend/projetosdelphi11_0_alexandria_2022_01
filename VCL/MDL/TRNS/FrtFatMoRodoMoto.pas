unit FrtFatMoRodoMoto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmFrtFatMoRodoMoto = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    VUUnidMed: TdmkValUsu;
    QrProdutCad: TmySQLQuery;
    QrProdutCadCodigo: TIntegerField;
    QrProdutCadNome: TWideStringField;
    DsProdutCad: TDataSource;
    EdxNome: TdmkEdit;
    Label3: TLabel;
    QrProdutCadnONU: TWideStringField;
    QrProdutCadGrEmb: TWideStringField;
    Label11: TLabel;
    EdCPF: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopanFrtFatMoRodoMoto(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtFatMoRodoMoto: TFmFrtFatMoRodoMoto;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnMySQLCuringa, UnUMedi_PF, UnidMed, UnFrt_PF;

{$R *.DFM}

procedure TFmFrtFatMoRodoMoto.BtOKClick(Sender: TObject);
var
  xNome, CPF: String;
  Codigo, Controle: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  xNome          := EdxNome.Text;
  CPF            := Geral.SoNumero_TT(EdCPF.Text);
  //
  if MyObjects.FIC(Trim(xNome) = '', EdxNome, 'Informe o nome do motorista!') then
    Exit;
  if MyObjects.FIC(Trim(CPF) = '', EdCPF, 'Informe o CPF do motorista!') then
    Exit;
  Controle := UMyMod.BPGS1I32('frtfatmorodomoto', 'Controle', '', '', tsPos,
    SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtfatmorodomoto', False, [
  'Codigo', 'xNome', 'CPF'], [
  'Controle'], [
  Codigo, xNome, CPF], [
  Controle], True) then
  begin
    ReopanFrtFatMoRodoMoto(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      EdControle.ValueVariant  := 0;
      EdxNome.Text             := '';
      EdCPF.Text               := '';
      //
      EdxNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmFrtFatMoRodoMoto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtFatMoRodoMoto.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtFatMoRodoMoto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmFrtFatMoRodoMoto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtFatMoRodoMoto.ReopanFrtFatMoRodoMoto(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
