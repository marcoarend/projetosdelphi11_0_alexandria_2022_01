unit FrtFatMoRodoLacr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmFrtFatMoRodoLacr = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdnLacre: TdmkEdit;
    Label3: TLabel;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopanFrtFatMoRodoLacr(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtFatMoRodoLacr: TFmFrtFatMoRodoLacr;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnMySQLCuringa, UnUMedi_PF, UnidMed, UnFrt_PF;

{$R *.DFM}

procedure TFmFrtFatMoRodoLacr.BtOKClick(Sender: TObject);
var
  nLacre: String;
  Codigo, Controle: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  nLacre         := EdnLacre.Text;
  //
  if MyObjects.FIC(Trim(nLacre) = '', EdnLacre, 'Informe o numero do lacre!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('frtfatmorodolacr', 'Controle', '', '', tsPos,
    SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtfatmorodolacr', False, [
  'Codigo', 'nLacre'], [
  'Controle'], [
  Codigo, nLacre], [
  Controle], True) then
  begin
    ReopanFrtFatMoRodoLacr(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      EdControle.ValueVariant  := 0;
      EdnLacre.ValueVariant    := '';
      //
      EdnLacre.SetFocus;
    end else Close;
  end;
end;

procedure TFmFrtFatMoRodoLacr.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtFatMoRodoLacr.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtFatMoRodoLacr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmFrtFatMoRodoLacr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtFatMoRodoLacr.ReopanFrtFatMoRodoLacr(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
