unit FrtFatMoRodoVeic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmFrtFatMoRodoVeic = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrTrnsIts: TmySQLQuery;
    DsTrnsIts: TDataSource;
    Label2: TLabel;
    EdcInt: TdmkEditCB;
    CBcInt: TdmkDBLookupComboBox;
    SbCaracAd: TSpeedButton;
    QrTrnsItsControle: TIntegerField;
    QrTrnsItsDESCRI: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbCaracAdClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopanFrtFatVeic(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtFatMoRodoVeic: TFmFrtFatMoRodoVeic;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnMySQLCuringa, UnUMedi_PF, UnidMed, UnFrt_PF, ProdutCad;

{$R *.DFM}

procedure TFmFrtFatMoRodoVeic.BtOKClick(Sender: TObject);
var
  Codigo, Controle, cInt: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  cInt           := EdcInt.ValueVariant;
  //
  if MyObjects.FIC(cInt = 0, EdcInt, 'Informe o ve�culo!') then
    Exit;
 //
  Controle := UMyMod.BPGS1I32('frtfatmorodoveic', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtfatmorodoveic', False, [
  'Codigo', 'cInt'], [
  'Controle'], [
  Codigo, cInt], [
  Controle], True) then
  begin
    ReopanFrtFatVeic(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      //
      EdControle.ValueVariant  := 0;
      EdcInt.ValueVariant      := 0;
      CBcInt.KeyValue          := Null;
      //
      EdcInt.SetFocus;
    end else Close;
  end;
end;

procedure TFmFrtFatMoRodoVeic.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtFatMoRodoVeic.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  //DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtFatMoRodoVeic.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrTrnsIts, Dmod.MyDB);
end;

procedure TFmFrtFatMoRodoVeic.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtFatMoRodoVeic.ReopanFrtFatVeic(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmFrtFatMoRodoVeic.SbCaracAdClick(Sender: TObject);
begin
  //Frt_PF.CadastroESelecaoDeTrnsIts(QrTrnsIts, EdcInt, CBcInt);
end;

end.
