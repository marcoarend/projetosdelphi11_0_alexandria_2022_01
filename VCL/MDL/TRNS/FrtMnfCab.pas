unit FrtMnfCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, Vcl.ComCtrls,
  dmkEditDateTimePicker, dmkMemo;

type
  TFmFrtMnfCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrFrtMnfCab: TmySQLQuery;
    DsFrtMnfCab: TDataSource;
    QrFrtMnfMunCarrega: TmySQLQuery;
    DsFrtMnfMunCarrega: TDataSource;
    PMMunCarrega: TPopupMenu;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtMunCarrega: TBitBtn;
    QrFrtMnfCabCodigo: TIntegerField;
    QrFrtMnfCabNome: TWideStringField;
    QrFrtMnfCabEmpresa: TIntegerField;
    QrFrtMnfCabAbertura: TDateTimeField;
    QrFrtMnfCabEncerrou: TDateTimeField;
    QrFrtMnfCabDataViagIniPrv: TDateTimeField;
    QrFrtMnfCabDataViagIniSai: TDateTimeField;
    QrFrtMnfCabDataViagFimPrv: TDateTimeField;
    QrFrtMnfCabDataViagFimChg: TDateTimeField;
    QrFrtMnfCabFrtRegMnf: TIntegerField;
    QrFrtMnfCabTrnsIts: TIntegerField;
    QrFrtMnfCabTpEmit: TSmallintField;
    QrFrtMnfCabModal: TSmallintField;
    QrFrtMnfCabTpEmis: TSmallintField;
    QrFrtMnfCabFrtRegPth: TIntegerField;
    QrFrtMnfCabUFIni: TWideStringField;
    QrFrtMnfCabUFFim: TWideStringField;
    QrFrtMnfCabParamsMDFe: TIntegerField;
    QrFrtMnfCabTot_qCTe: TIntegerField;
    QrFrtMnfCabTot_qNFe: TIntegerField;
    QrFrtMnfCabTot_qMDFe: TIntegerField;
    QrFrtMnfCabTot_vCarga: TFloatField;
    QrFrtMnfCabTot_cUnid: TSmallintField;
    QrFrtMnfCabTot_qCarga: TFloatField;
    QrFrtMnfCabinfAdFisco: TWideMemoField;
    QrFrtMnfCabinfCpl: TWideMemoField;
    QrFrtMnfCabSerieDesfe: TIntegerField;
    QrFrtMnfCabMDFDesfeita: TIntegerField;
    QrFrtMnfCabCodAgPorto: TWideStringField;
    QrFrtMnfCabLk: TIntegerField;
    QrFrtMnfCabDataCad: TDateField;
    QrFrtMnfCabDataAlt: TDateField;
    QrFrtMnfCabUserCad: TIntegerField;
    QrFrtMnfCabUserAlt: TIntegerField;
    QrFrtMnfCabAlterWeb: TSmallintField;
    QrFrtMnfCabAtivo: TSmallintField;
    QrTrnsIts: TmySQLQuery;
    QrTrnsItsControle: TIntegerField;
    QrTrnsItsDESCRI: TWideStringField;
    DsTrnsIts: TDataSource;
    QrParamsMDFe: TmySQLQuery;
    QrParamsMDFeControle: TIntegerField;
    QrParamsMDFeNOME: TWideStringField;
    DsParamsMDFe: TDataSource;
    Panel7: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    EdEmpresa: TdmkEditCB;
    Label3: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label36: TLabel;
    TpDataViagIniPrv: TdmkEditDateTimePicker;
    EdDataViagIniPrv: TdmkEdit;
    Label4: TLabel;
    TPDataViagFimPrv: TdmkEditDateTimePicker;
    EdDataViagFimPrv: TdmkEdit;
    Label5: TLabel;
    EdTrnsIts: TdmkEditCB;
    CBTrnsIts: TdmkDBLookupComboBox;
    Label32: TLabel;
    EdParamsMDFe: TdmkEditCB;
    CBParamsMDFe: TdmkDBLookupComboBox;
    RGTpEmit: TdmkRadioGroup;
    RGModal: TdmkRadioGroup;
    GroupBox1: TGroupBox;
    Label24: TLabel;
    Label6: TLabel;
    EdUFIni: TdmkEdit;
    EdUFFim: TdmkEdit;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    EdTot_qCTe: TdmkEdit;
    EdTot_qNFe: TdmkEdit;
    EdTot_vCarga: TdmkEdit;
    EdTot_qCarga: TdmkEdit;
    RGTot_cUnid: TdmkRadioGroup;
    Panel6: TPanel;
    Label179: TLabel;
    Label180: TLabel;
    MeInfAdFisco: TdmkMemo;
    MeInfCpl: TdmkMemo;
    EdCodAgPorto: TdmkEdit;
    Label13: TLabel;
    Incluimunicpiodecarregamento1: TMenuItem;
    Excluimunicpiodecarregamento1: TMenuItem;
    QrFrtMnfMunCarregaCodigo: TIntegerField;
    QrFrtMnfMunCarregaControle: TIntegerField;
    QrFrtMnfMunCarregacMunCarrega: TIntegerField;
    QrFrtMnfMunCarregaxMunCarrega: TWideStringField;
    QrFrtMnfMunCarregaLk: TIntegerField;
    QrFrtMnfMunCarregaDataCad: TDateField;
    QrFrtMnfMunCarregaDataAlt: TDateField;
    QrFrtMnfMunCarregaUserCad: TIntegerField;
    QrFrtMnfMunCarregaUserAlt: TIntegerField;
    QrFrtMnfMunCarregaAlterWeb: TSmallintField;
    QrFrtMnfMunCarregaAtivo: TSmallintField;
    QrFrtMnfInfPercurso: TmySQLQuery;
    DsFrtMnfInfPercurso: TDataSource;
    BtInfPercurso: TBitBtn;
    QrFrtMnfInfPercursoCodigo: TIntegerField;
    QrFrtMnfInfPercursoControle: TIntegerField;
    QrFrtMnfInfPercursoUFPer: TWideStringField;
    QrFrtMnfInfPercursoLk: TIntegerField;
    QrFrtMnfInfPercursoDataCad: TDateField;
    QrFrtMnfInfPercursoDataAlt: TDateField;
    QrFrtMnfInfPercursoUserCad: TIntegerField;
    QrFrtMnfInfPercursoUserAlt: TIntegerField;
    QrFrtMnfInfPercursoAlterWeb: TSmallintField;
    QrFrtMnfInfPercursoAtivo: TSmallintField;
    PMInfPercurso: TPopupMenu;
    IncluiUFdePercurso1: TMenuItem;
    ExcluiUFdePercurso1: TMenuItem;
    BtInfDoc: TBitBtn;
    QrFrtMnfInfDoc: TmySQLQuery;
    DsFrtMnfInfDoc: TDataSource;
    QrFrtMnfInfDocCodigo: TIntegerField;
    QrFrtMnfInfDocControle: TIntegerField;
    QrFrtMnfInfDoccMunDescarga: TIntegerField;
    QrFrtMnfInfDocxMunDescarga: TWideStringField;
    QrFrtMnfInfDocDocMod: TSmallintField;
    QrFrtMnfInfDocDocSerie: TIntegerField;
    QrFrtMnfInfDocDocNumero: TIntegerField;
    QrFrtMnfInfDocDocChave: TWideStringField;
    QrFrtMnfInfDocSegCodBarra: TWideStringField;
    QrFrtMnfInfDocTrnsIts: TIntegerField;
    QrFrtMnfInfDocLk: TIntegerField;
    QrFrtMnfInfDocDataCad: TDateField;
    QrFrtMnfInfDocDataAlt: TDateField;
    QrFrtMnfInfDocUserCad: TIntegerField;
    QrFrtMnfInfDocUserAlt: TIntegerField;
    QrFrtMnfInfDocAlterWeb: TSmallintField;
    QrFrtMnfInfDocAtivo: TSmallintField;
    QrFrtMnfInfDocNO_TrnsIts: TWideStringField;
    QrFrtMnfInfDocPlaca: TWideStringField;
    DBGrid3: TDBGrid;
    PMInfDoc: TPopupMenu;
    IncluiDocumento1: TMenuItem;
    AlteraDocumento1: TMenuItem;
    ExcluiDocumento1: TMenuItem;
    BtAutXML: TBitBtn;
    QrFrtMnfAutXml: TmySQLQuery;
    QrFrtMnfAutXmlCodigo: TIntegerField;
    QrFrtMnfAutXmlControle: TIntegerField;
    QrFrtMnfAutXmlAddForma: TSmallintField;
    QrFrtMnfAutXmlAddIDCad: TIntegerField;
    QrFrtMnfAutXmlTipo: TSmallintField;
    QrFrtMnfAutXmlCNPJ: TWideStringField;
    QrFrtMnfAutXmlCPF: TWideStringField;
    QrFrtMnfAutXmlLk: TIntegerField;
    QrFrtMnfAutXmlDataCad: TDateField;
    QrFrtMnfAutXmlDataAlt: TDateField;
    QrFrtMnfAutXmlUserCad: TIntegerField;
    QrFrtMnfAutXmlUserAlt: TIntegerField;
    QrFrtMnfAutXmlAlterWeb: TSmallintField;
    QrFrtMnfAutXmlAtivo: TSmallintField;
    QrFrtMnfAutXmlNO_AddForma: TWideStringField;
    QrFrtMnfAutXmlTpDOC: TWideStringField;
    QrFrtMnfAutXmlNO_ENT_ECO: TWideStringField;
    QrFrtMnfAutXmlCNPJ_CPF: TWideStringField;
    DsFrtMnfAutXml: TDataSource;
    BtLacres: TBitBtn;
    PMLacres: TPopupMenu;
    IncluiLacre1: TMenuItem;
    AlteraLacre1: TMenuItem;
    ExcluiLacre1: TMenuItem;
    QrFrtMnfLacres: TmySQLQuery;
    QrFrtMnfLacresCodigo: TIntegerField;
    QrFrtMnfLacresControle: TIntegerField;
    QrFrtMnfLacresnLacre: TWideStringField;
    QrFrtMnfLacresLk: TIntegerField;
    QrFrtMnfLacresDataCad: TDateField;
    QrFrtMnfLacresDataAlt: TDateField;
    QrFrtMnfLacresUserCad: TIntegerField;
    QrFrtMnfLacresUserAlt: TIntegerField;
    QrFrtMnfLacresAlterWeb: TSmallintField;
    QrFrtMnfLacresAtivo: TSmallintField;
    DsFrtMnfLacres: TDataSource;
    PMEncerra: TPopupMenu;
    MenuItem1: TMenuItem;
    Desfazencerramento1: TMenuItem;
    Encerramanifesto1: TMenuItem;
    QrFrtMnfCabStatus: TSmallintField;
    BtCondutor: TBitBtn;
    BtReboque: TBitBtn;
    BtValePedagio: TBitBtn;
    PMCondutor: TPopupMenu;
    IncluiCondutor1: TMenuItem;
    AlteraCondutor1: TMenuItem;
    ExcluiCondutor1: TMenuItem;
    QrFrtMnfMoRodoCondutor: TmySQLQuery;
    DsFrtMnfMoRodoCondutor: TDataSource;
    QrFrtMnfMoRodoVeicRebo: TmySQLQuery;
    DsFrtMnfMoRodoVeicRebo: TDataSource;
    QrFrtMnfMoRodoValePedg: TmySQLQuery;
    DsFrtMnfMoRodoValePedg: TDataSource;
    QrFrtMnfMoRodoCondutorCodigo: TIntegerField;
    QrFrtMnfMoRodoCondutorControle: TIntegerField;
    QrFrtMnfMoRodoCondutorxNome: TWideStringField;
    QrFrtMnfMoRodoCondutorCPF: TWideStringField;
    QrFrtMnfMoRodoCondutorLk: TIntegerField;
    QrFrtMnfMoRodoCondutorDataCad: TDateField;
    QrFrtMnfMoRodoCondutorDataAlt: TDateField;
    QrFrtMnfMoRodoCondutorUserCad: TIntegerField;
    QrFrtMnfMoRodoCondutorUserAlt: TIntegerField;
    QrFrtMnfMoRodoCondutorAlterWeb: TSmallintField;
    QrFrtMnfMoRodoCondutorAtivo: TSmallintField;
    QrFrtMnfMoRodoCondutorEntidade: TIntegerField;
    Panel8: TPanel;
    DBGrid2: TDBGrid;
    DBGrid1: TDBGrid;
    DBGrid4: TDBGrid;
    Panel9: TPanel;
    DBGrid5: TDBGrid;
    DBGrid6: TDBGrid;
    QrFrtMnfMoRodoVeicReboCodigo: TIntegerField;
    QrFrtMnfMoRodoVeicReboControle: TIntegerField;
    QrFrtMnfMoRodoVeicRebocInt: TIntegerField;
    QrFrtMnfMoRodoVeicReboLk: TIntegerField;
    QrFrtMnfMoRodoVeicReboDataCad: TDateField;
    QrFrtMnfMoRodoVeicReboDataAlt: TDateField;
    QrFrtMnfMoRodoVeicReboUserCad: TIntegerField;
    QrFrtMnfMoRodoVeicReboUserAlt: TIntegerField;
    QrFrtMnfMoRodoVeicReboAlterWeb: TSmallintField;
    QrFrtMnfMoRodoVeicReboAtivo: TSmallintField;
    Panel10: TPanel;
    DBGrid7: TDBGrid;
    QrFrtMnfMoRodoVeicReboNO_VEIC: TWideStringField;
    QrFrtMnfMoRodoVeicReboPlaca: TWideStringField;
    DBGrid8: TDBGrid;
    QrFrtMnfMoRodoValePedgCodigo: TIntegerField;
    QrFrtMnfMoRodoValePedgControle: TIntegerField;
    QrFrtMnfMoRodoValePedgCNPJForn: TWideStringField;
    QrFrtMnfMoRodoValePedgCNPJPg: TWideStringField;
    QrFrtMnfMoRodoValePedgnCompra: TWideStringField;
    QrFrtMnfMoRodoValePedgLk: TIntegerField;
    QrFrtMnfMoRodoValePedgDataCad: TDateField;
    QrFrtMnfMoRodoValePedgDataAlt: TDateField;
    QrFrtMnfMoRodoValePedgUserCad: TIntegerField;
    QrFrtMnfMoRodoValePedgUserAlt: TIntegerField;
    QrFrtMnfMoRodoValePedgAlterWeb: TSmallintField;
    QrFrtMnfMoRodoValePedgAtivo: TSmallintField;
    PMReboque: TPopupMenu;
    IncluiVeculoreboque1: TMenuItem;
    AlteraVeculoreboque1: TMenuItem;
    ExcluiVeculoreboque1: TMenuItem;
    PMValePedg: TPopupMenu;
    AlteraValePedgio1: TMenuItem;
    ExcluiValePedgio1: TMenuItem;
    IncluiValePedgio1: TMenuItem;
    QrFrtMnfMoRodoCIOT: TmySQLQuery;
    DsFrtMnfMoRodoCIOT: TDataSource;
    QrFrtMnfMoRodoCIOTCodigo: TIntegerField;
    QrFrtMnfMoRodoCIOTControle: TIntegerField;
    QrFrtMnfMoRodoCIOTCIOT: TLargeintField;
    QrFrtMnfMoRodoCIOTLk: TIntegerField;
    QrFrtMnfMoRodoCIOTDataCad: TDateField;
    QrFrtMnfMoRodoCIOTDataAlt: TDateField;
    QrFrtMnfMoRodoCIOTUserCad: TIntegerField;
    QrFrtMnfMoRodoCIOTUserAlt: TIntegerField;
    QrFrtMnfMoRodoCIOTAlterWeb: TSmallintField;
    QrFrtMnfMoRodoCIOTAtivo: TSmallintField;
    BtEncerra: TBitBtn;
    BtCIOT: TBitBtn;
    PMCIOT: TPopupMenu;
    IncluiCIOT1: TMenuItem;
    AlteraCIOT1: TMenuItem;
    ExcluiCIOT1: TMenuItem;
    DBGrid9: TDBGrid;
    QrMDFeCabA: TmySQLQuery;
    QrMDFeCabAStatus: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFrtMnfCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrFrtMnfCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtMunCarregaClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMMunCarregaPopup(Sender: TObject);
    procedure RGModalClick(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure Incluimunicpiodecarregamento1Click(Sender: TObject);
    procedure Excluimunicpiodecarregamento1Click(Sender: TObject);
    procedure IncluiUFdePercurso1Click(Sender: TObject);
    procedure ExcluiUFdePercurso1Click(Sender: TObject);
    procedure PMInfPercursoPopup(Sender: TObject);
    procedure BtInfPercursoClick(Sender: TObject);
    procedure BtInfDocClick(Sender: TObject);
    procedure IncluiDocumento1Click(Sender: TObject);
    procedure AlteraDocumento1Click(Sender: TObject);
    procedure ExcluiDocumento1Click(Sender: TObject);
    procedure PMInfDocPopup(Sender: TObject);
    procedure BtAutXMLClick(Sender: TObject);
    procedure PMLacresPopup(Sender: TObject);
    procedure BtLacresClick(Sender: TObject);
    procedure IncluiLacre1Click(Sender: TObject);
    procedure AlteraLacre1Click(Sender: TObject);
    procedure ExcluiLacre1Click(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure Encerramanifesto1Click(Sender: TObject);
    procedure Desfazencerramento1Click(Sender: TObject);
    procedure PMEncerraPopup(Sender: TObject);
    procedure IncluiCondutor1Click(Sender: TObject);
    procedure AlteraCondutor1Click(Sender: TObject);
    procedure ExcluiCondutor1Click(Sender: TObject);
    procedure PMCondutorPopup(Sender: TObject);
    procedure BtCondutorClick(Sender: TObject);
    procedure IncluiVeculoreboque1Click(Sender: TObject);
    procedure AlteraVeculoreboque1Click(Sender: TObject);
    procedure ExcluiVeculoreboque1Click(Sender: TObject);
    procedure PMReboquePopup(Sender: TObject);
    procedure BtReboqueClick(Sender: TObject);
    procedure AlteraValePedgio1Click(Sender: TObject);
    procedure ExcluiValePedgio1Click(Sender: TObject);
    procedure IncluiValePedgio2Click(Sender: TObject);
    procedure PMValePedgPopup(Sender: TObject);
    procedure BtValePedagioClick(Sender: TObject);
    procedure BtCIOTClick(Sender: TObject);
    procedure IncluiCIOT1Click(Sender: TObject);
    procedure AlteraCIOT1Click(Sender: TObject);
    procedure ExcluiCIOT1Click(Sender: TObject);
    procedure PMCIOTPopup(Sender: TObject);
    procedure QrFrtMnfCabBeforeClose(DataSet: TDataSet);
    procedure QrFrtMnfCabBeforeOpen(DataSet: TDataSet);
    procedure QrMDFeCabAAfterOpen(DataSet: TDataSet);
  private
    FBtCab: Boolean;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure HabilitaBotoes(Habilita: Boolean);
    procedure MostraFrtMnfMunCarrega(SQLType: TSQLType);
    procedure MostraFrtMnfInfPercurso(SQLType: TSQLType);
    procedure MostraFrtMnfInfDoc(SQLType: TSQLType);
    procedure MostraFormFrtMnfLacres(SQLType: TSQLType);
    procedure MostraFormFrtMnfAutXml();
    procedure MostraFormFrtMnfMoRodoCondutor(SQLType: TSQLType);
    procedure MostraFormFrtMnfMoRodoVeicRebo(SQLType: TSQLType);
    procedure MostraFormFrtMnfMoRodoValePedg(SQLType: TSQLType);
    procedure MostraFormFrtMnfMoRodoCIOT(SQLType: TSQLType);
    //
    procedure ReopenParamsMDFe();

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenMDFeCabA();
    //
    procedure ReopenFrtMnfMunCarrega(Controle: Integer);
    procedure ReopenFrtMnfInfPercurso(Controle: Integer);
    procedure ReopenFrtMnfInfDoc(Controle: Integer);
    procedure ReopenFrtMnfLacres(Controle: Integer);
    procedure ReopenFrtMnfAutXml(Controle: Integer);
    procedure ReopenFrtMnfMoRodoCondutor(Controle: Integer);
    procedure ReopenFrtMnfMoRodoValePedg(Controle: Integer);
    procedure ReopenFrtMnfMoRodoVeicRebo(Controle: Integer);
    procedure ReopenFrtMnfMoRodoCIOT(Controle: Integer);
  end;

var
  FmFrtMnfCab: TFmFrtMnfCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, FrtMnfLacres,
  UnMDFe_PF, FrtMnfMunCarrega, FrtMnfInfPercurso, FrtMnfInfDoc, FrtMnfAutXml,
  UnFrt_PF, FrtMnfMoRodoCondutor, FrtMnfMoRodoVeicRebo, FrtMnfMoRodoValePedg,
  FrtMnfMoRodoCIOT, ModuleMDFe_0000, UnXXe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFrtMnfCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFrtMnfCab.MostraFormFrtMnfAutXml();
  procedure MostraFormFrtMnfAutXml(Codigo, Terceiro: Integer; InserePadroes: Boolean);
  begin
    if DBCheck.CriaFm(TFmFrtMnfAutXml, FmFrtMnfAutXml, afmoNegarComAviso) then
    begin
      FmFrtMnfAutXml.FCodigo        := Codigo;
      FmFrtMnfAutXml.FTerceiro       := Terceiro;
      FmFrtMnfAutXml.FInserePadroes := InserePadroes;
      //
      FmFrtMnfAutXml.ReopenFrtMnfAutXml(0);
      FmFrtMnfAutXml.ShowModal;
      FmFrtMnfAutXml.Destroy;
    end;
  end;
var
  Codigo, Terceiro: Integer;
begin
  if (QrFrtMnfCab.State <> dsInactive) and (QrFrtMnfCabCodigo.Value <> 0) then
  begin
    Codigo   := QrFrtMnfCabCodigo.Value;
    Terceiro := 0;
    //
    MostraFormFrtMnfAutXml(Codigo, Terceiro, True);
    //
    ReopenFrtMnfAutXml(0);
  end;
end;

procedure TFmFrtMnfCab.MostraFormFrtMnfLacres(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtMnfLacres, FmFrtMnfLacres, afmoNegarComAviso) then
  begin
    FmFrtMnfLacres.ImgTipo.SQLType := SQLType;
    FmFrtMnfLacres.FQrCab := QrFrtMnfCab;
    FmFrtMnfLacres.FDsCab := DsFrtMnfCab;
    FmFrtMnfLacres.FQrIts := QrFrtMnfLacres;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtMnfLacres.EdControle.ValueVariant := QrFrtMnfLacresControle.Value;
      //
      FmFrtMnfLacres.EdnLacre.ValueVariant    := QrFrtMnfLacresnLacre.Value;
    end;
    FmFrtMnfLacres.ShowModal;
    FmFrtMnfLacres.Destroy;
  end;
end;

procedure TFmFrtMnfCab.MostraFormFrtMnfMoRodoCIOT(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtMnfMoRodoCIOT, FmFrtMnfMoRodoCIOT, afmoNegarComAviso) then
  begin
    FmFrtMnfMoRodoCIOT.ImgTipo.SQLType := SQLType;
    FmFrtMnfMoRodoCIOT.FQrCab := QrFrtMnfCab;
    FmFrtMnfMoRodoCIOT.FDsCab := DsFrtMnfCab;
    FmFrtMnfMoRodoCIOT.FQrIts := QrFrtMnfMoRodoCIOT;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtMnfMoRodoCIOT.EdControle.ValueVariant := QrFrtMnfMoRodoCIOTControle.Value;
      //
      FmFrtMnfMoRodoCIOT.EdCIOT.ValueVariant     := QrFrtMnfMoRodoCIOTCIOT.Value;
    end;
    FmFrtMnfMoRodoCIOT.ShowModal;
    FmFrtMnfMoRodoCIOT.Destroy;
  end;
end;

procedure TFmFrtMnfCab.MostraFormFrtMnfMoRodoCondutor(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtMnfMoRodoCondutor, FmFrtMnfMoRodoCondutor, afmoNegarComAviso) then
  begin
    FmFrtMnfMoRodoCondutor.ImgTipo.SQLType := SQLType;
    FmFrtMnfMoRodoCondutor.FQrCab := QrFrtMnfCab;
    FmFrtMnfMoRodoCondutor.FDsCab := DsFrtMnfCab;
    FmFrtMnfMoRodoCondutor.FQrIts := QrFrtMnfMoRodoCondutor;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtMnfMoRodoCondutor.EdControle.ValueVariant := QrFrtMnfMoRodoCondutorControle.Value;
      //
      FmFrtMnfMoRodoCondutor.EdEntidade.ValueVariant := QrFrtMnfMoRodoCondutorEntidade.Value;
      FmFrtMnfMoRodoCondutor.CBEntidade.KeyValue     := QrFrtMnfMoRodoCondutorEntidade.Value;
      FmFrtMnfMoRodoCondutor.EdxNome.ValueVariant    := QrFrtMnfMoRodoCondutorxNome.Value;
      FmFrtMnfMoRodoCondutor.EdCPF.ValueVariant      := QrFrtMnfMoRodoCondutorCPF.Value;
    end;
    FmFrtMnfMoRodoCondutor.ShowModal;
    FmFrtMnfMoRodoCondutor.Destroy;
  end;
end;

procedure TFmFrtMnfCab.MostraFormFrtMnfMoRodoValePedg(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtMnfMoRodoValePedg, FmFrtMnfMoRodoValePedg, afmoNegarComAviso) then
  begin
    FmFrtMnfMoRodoValePedg.ImgTipo.SQLType := SQLType;
    FmFrtMnfMoRodoValePedg.FQrCab := QrFrtMnfCab;
    FmFrtMnfMoRodoValePedg.FDsCab := DsFrtMnfCab;
    FmFrtMnfMoRodoValePedg.FQrIts := QrFrtMnfMoRodoValePedg;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtMnfMoRodoValePedg.EdControle.ValueVariant := QrFrtMnfMoRodoValePedgControle.Value;
      //
      FmFrtMnfMoRodoValePedg.EdCNPJForn.ValueVariant  := QrFrtMnfMoRodoValePedgCNPJForn.Value;
      FmFrtMnfMoRodoValePedg.EdCNPJPg.ValueVariant    := QrFrtMnfMoRodoValePedgCNPJPg.Value;
      FmFrtMnfMoRodoValePedg.EdnCompra.Text           := QrFrtMnfMoRodoValePedgnCompra.Value;
    end;
    FmFrtMnfMoRodoValePedg.ShowModal;
    FmFrtMnfMoRodoValePedg.Destroy;
  end;
end;

procedure TFmFrtMnfCab.MostraFormFrtMnfMoRodoVeicRebo(SQLType: TSQLType);
begin
  if QrFrtMnfMoRodoVeicRebo.RecordCount < 3 then
  begin
    if DBCheck.CriaFm(TFmFrtMnfMoRodoVeicRebo, FmFrtMnfMoRodoVeicRebo, afmoNegarComAviso) then
    begin
      FmFrtMnfMoRodoVeicRebo.ImgTipo.SQLType := SQLType;
      FmFrtMnfMoRodoVeicRebo.FQrCab := QrFrtMnfCab;
      FmFrtMnfMoRodoVeicRebo.FDsCab := DsFrtMnfCab;
      FmFrtMnfMoRodoVeicRebo.FQrIts := QrFrtMnfMoRodoVeicRebo;
      if SQLType = stIns then
        //
      else
      begin
        FmFrtMnfMoRodoVeicRebo.EdControle.ValueVariant := QrFrtMnfMoRodoVeicReboControle.Value;
        //
        FmFrtMnfMoRodoVeicRebo.EdcInt.ValueVariant := QrFrtMnfMoRodoVeicRebocInt.Value;
        FmFrtMnfMoRodoVeicRebo.CBcInt.KeyValue     := QrFrtMnfMoRodoVeicRebocInt.Value;
      end;
      FmFrtMnfMoRodoVeicRebo.ShowModal;
      FmFrtMnfMoRodoVeicRebo.Destroy;
    end;
  end else
    Geral.MB_Aviso('Limite de itens (3) atingido!');
end;

procedure TFmFrtMnfCab.MostraFrtMnfInfDoc(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtMnfInfDoc, FmFrtMnfInfDoc, afmoNegarComAviso) then
  begin
    FmFrtMnfInfDoc.ImgTipo.SQLType := SQLType;
    FmFrtMnfInfDoc.FQrCab := QrFrtMnfCab;
    FmFrtMnfInfDoc.FDsCab := DsFrtMnfCab;
    FmFrtMnfInfDoc.FQrIts := QrFrtMnfInfDoc;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtMnfInfDoc.EdControle.ValueVariant     := QrFrtMnfInfDocControle.Value;
      //
      FmFrtMnfInfDoc.EdDocChave.Text             := QrFrtMnfInfDocDocChave.Value;
      FmFrtMnfInfDoc.EdSegCodBarra.Text          := QrFrtMnfInfDocSegCodBarra.Value;
      FmFrtMnfInfDoc.EdMunDescarga.ValueVariant  := QrFrtMnfInfDoccMunDescarga.Value;
      FmFrtMnfInfDoc.CBMunDescarga.KeyValue      := QrFrtMnfInfDoccMunDescarga.Value;
      FmFrtMnfInfDoc.EdDocMod.ValueVariant       := QrFrtMnfInfDocDocMod.Value;
      FmFrtMnfInfDoc.EdDocSerie.ValueVariant     := QrFrtMnfInfDocDocSerie.Value;
      FmFrtMnfInfDoc.EdDocNumero.ValueVariant    := QrFrtMnfInfDocDocNumero.Value;
      FmFrtMnfInfDoc.EdTrnsIts.ValueVariant      := QrFrtMnfInfDocTrnsIts.Value;
      FmFrtMnfInfDoc.CBTrnsIts.KeyValue          := QrFrtMnfInfDocTrnsIts.Value;
    end;
    FmFrtMnfInfDoc.ShowModal;
    FmFrtMnfInfDoc.Destroy;
  end;
end;

procedure TFmFrtMnfCab.MostraFrtMnfInfPercurso(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtMnfInfPercurso, FmFrtMnfInfPercurso, afmoNegarComAviso) then
  begin
    FmFrtMnfInfPercurso.ImgTipo.SQLType := SQLType;
    FmFrtMnfInfPercurso.FQrCab := QrFrtMnfCab;
    FmFrtMnfInfPercurso.FDsCab := DsFrtMnfCab;
    FmFrtMnfInfPercurso.FQrIts := QrFrtMnfInfPercurso;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtMnfInfPercurso.EdControle.ValueVariant   := QrFrtMnfInfPercursoControle.Value;
      //
      FmFrtMnfInfPercurso.EdUFPer.Text := QrFrtMnfInfPercursoUFPer.Value;
    end;
    FmFrtMnfInfPercurso.ShowModal;
    FmFrtMnfInfPercurso.Destroy;
  end;
end;

procedure TFmFrtMnfCab.MostraFrtMnfMunCarrega(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFrtMnfMunCarrega, FmFrtMnfMunCarrega, afmoNegarComAviso) then
  begin
    FmFrtMnfMunCarrega.ImgTipo.SQLType := SQLType;
    FmFrtMnfMunCarrega.FQrCab := QrFrtMnfCab;
    FmFrtMnfMunCarrega.FDsCab := DsFrtMnfCab;
    FmFrtMnfMunCarrega.FQrIts := QrFrtMnfMunCarrega;
    if SQLType = stIns then
      //
    else
    begin
      FmFrtMnfMunCarrega.EdControle.ValueVariant   := QrFrtMnfMunCarregaControle.Value;
      //
      FmFrtMnfMunCarrega.EdMunCarrega.ValueVariant := QrFrtMnfMunCarregacMunCarrega.Value;
      FmFrtMnfMunCarrega.CBMunCarrega.KeyValue     := QrFrtMnfMunCarregacMunCarrega.Value;
    end;
    FmFrtMnfMunCarrega.ShowModal;
    FmFrtMnfMunCarrega.Destroy;
  end;
end;

procedure TFmFrtMnfCab.PMCabPopup(Sender: TObject);
begin
  if not FBtCab then
  begin
    CabAltera1.Enabled := False;
    CabExclui1.Enabled := False;
  end else
  begin
    MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrFrtMnfCab);
    MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrFrtMnfCab, QrFrtMnfMunCarrega);
  end;
end;

procedure TFmFrtMnfCab.PMCIOTPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiCIOT1, QrFrtMnfCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraCIOT1, QrFrtMnfMoRodoCIOT);
  MyObjects.HabilitaMenuItemItsDel(ExcluiCIOT1, QrFrtMnfMoRodoCIOT);
end;

procedure TFmFrtMnfCab.PMCondutorPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiCondutor1, QrFrtMnfCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraCondutor1, QrFrtMnfMoRodoCondutor);
  MyObjects.HabilitaMenuItemItsDel(ExcluiCondutor1, QrFrtMnfMoRodoCondutor);
end;

procedure TFmFrtMnfCab.PMEncerraPopup(Sender: TObject);
var
  Habilita, Habil: Boolean;
begin
  Habil    := (QrFrtMnfCab.State <> dsInactive) and (QrFrtMnfCab.RecordCount > 0);
  if MyObjects.FIC(not Habil, nil, 'Nenhum manifesto foi selecionado!') then exit;
  Habil    := (QrFrtMnfMunCarrega.State <> dsInactive) and (QrFrtMnfMunCarrega.RecordCount > 0);
  if MyObjects.FIC(not Habil, nil, 'Informe pelo menos um munic�pio de carregamento!') then exit;
  Habil    := (QrFrtMnfInfDoc.State <> dsInactive) and (QrFrtMnfInfDoc.RecordCount > 0);
  if MyObjects.FIC(not Habil, nil, 'Informe pelo menos um item de documento!') then exit;
  //
  Habil    := (QrFrtMnfCab.State <> dsInactive) and (QrFrtMnfCab.RecordCount > 0);
  Habilita := (QrFrtMnfCabEncerrou.Value = 0) and Habil;
  Encerramanifesto1.Enabled := Habilita;
  Desfazencerramento1.Enabled := not Habilita
    and (QrFrtMnfCabStatus.Value < Integer(ctemystatusCTeAddedLote(*50*)));
end;

procedure TFmFrtMnfCab.PMInfDocPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiDocumento1, QrFrtMnfCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraDocumento1, QrFrtMnfInfDoc);
  MyObjects.HabilitaMenuItemItsDel(ExcluiDocumento1, QrFrtMnfInfDoc);
end;

procedure TFmFrtMnfCab.PMInfPercursoPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiUFdePercurso1, QrFrtMnfCab);
  //MyObjects.HabilitaMenuItemItsUpd(AlteraUFdePercurso1, QrFrtMnfInfPercurso);
  MyObjects.HabilitaMenuItemItsDel(ExcluiUFdePercurso1, QrFrtMnfInfPercurso);
end;

procedure TFmFrtMnfCab.PMLacresPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiLacre1, QrFrtMnfCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraLacre1, QrFrtMnfLacres);
  MyObjects.HabilitaMenuItemItsDel(ExcluiLacre1, QrFrtMnfLacres);
end;

procedure TFmFrtMnfCab.PMMunCarregaPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluimunicpiodecarregamento1, QrFrtMnfCab);
  //MyObjects.HabilitaMenuItemItsUpd(AlteraMunicpiodecarregamento1, QrFrtMnfMunCarrega);
  MyObjects.HabilitaMenuItemItsDel(ExcluiMunicpiodecarregamento1, QrFrtMnfMunCarrega);
end;

procedure TFmFrtMnfCab.PMReboquePopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiVeculoreboque1, QrFrtMnfCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraVeculoreboque1, QrFrtMnfMoRodoVeicRebo);
  MyObjects.HabilitaMenuItemItsDel(ExcluiVeculoreboque1, QrFrtMnfMoRodoVeicRebo);
end;

procedure TFmFrtMnfCab.PMValePedgPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiValePedgio1, QrFrtMnfCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraValePedgio1, QrFrtMnfMoRodoValePedg);
  MyObjects.HabilitaMenuItemItsDel(ExcluiValePedgio1, QrFrtMnfMoRodoValePedg);
end;

procedure TFmFrtMnfCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFrtMnfCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFrtMnfCab.DefParams;
begin
  VAR_GOTOTABELA := 'frtmnfcab';
  VAR_GOTOMYSQLTABLE := QrFrtMnfCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM frtmnfcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmFrtMnfCab.Desfazencerramento1Click(Sender: TObject);
var
  FatNum, Empresa: Integer;
begin
  FatNum  := QrFrtMnfCabCodigo.Value;
  Empresa := QrFrtMnfCabEmpresa.Value;
  if Frt_PF.DesfazEncerramentoManifesto(VAR_FATID_6001, FatNum, Empresa) then
    LocCod(FatNum, FatNum);
end;

procedure TFmFrtMnfCab.EdEmpresaRedefinido(Sender: TObject);
begin
  ReopenParamsMDFe();
end;

procedure TFmFrtMnfCab.Encerramanifesto1Click(Sender: TObject);
var
  VersaoMDFe, Empresa, SerieMDFe, NumeroMDFe, ParamsMDFe, SerieDesfe,
  MDFDesfeita, FatID, FatNum: Integer;
  //vRec: Double;
  Tabela: String;
  Qry: TmySQLQuery;
begin
(*
  if (TCTeModal(QrFrtMnfCabModal.Value) = ctemodalRodoviario)
  and (QrFrtMnfCabLota.Value = 1)
  and (QrFrtMnfMoRodoVeic.RecordCount = 0) then
  begin
    Geral.MB_Aviso(
    'Dados dos ve�culos obrigat�rio em CT-e rodovi�rio de lota��o!');
    Exit;
  end;
*)
  VersaoMDFe    := MDFe_PF.VersaoMDFeEmUso();
  // Buscar valores se mudaram sem atualizar
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDb, [
    'SELECT * FROM frtmnfcab ',
    'WHERE Codigo=' + Geral.FF0(QrFrtMnfCabCodigo.Value),
    '']);
    //
    SerieDesfe := Qry.FieldByName('SerieDesfe').AsInteger;
    MDFDesfeita := Qry.FieldByName('MDFDesfeita').AsInteger;
  finally
    Qry.Free;
  end;
  ParamsMDFe := QrFrtMnfCabParamsMDFe.Value;
  //vRec       := QrFrtMnfCabvRec.Value;
  FatID      := VAR_FATID_6001;
  FatNum     := QrFrtMnfCabCodigo.Value;
  Empresa    := QrFrtMnfCabEmpresa.Value;
  Tabela     := 'FrtMnfCab';
  SerieMDFe  := 0;
  NumeroMDFe := 0;
  //
  if not MDFe_PF.Obtem_Serie_e_NumMDF(SerieDesfe, MDFDesfeita, ParamsMDFe,
  FatID, FatNum, Empresa(*, Filial, MaxSeq*), Tabela, SerieMDFe, NumeroMDFe) then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  case VersaoMDFe of
    0100: MDFe_PF.MostraFormMDFeMDFaEdit_0100a(QrFrtMnfCabEmpresa.Value,
          QrFrtMnfCabCodigo.Value,
          QrFrtMnfCab, SerieMDFe, NumeroMDFe);
    else MDFe_PF.AvisoNaoImplemVerMDFe(VersaoMDFe);
  end;
end;

procedure TFmFrtMnfCab.ExcluiCIOT1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtMnfMoRodoCIOT', 'Controle', QrFrtMnfMoRodoCIOTControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtMnfMoRodoCIOT,
      QrFrtMnfMoRodoCIOTControle, QrFrtMnfMoRodoCIOTControle.Value);
    ReopenFrtMnfMoRodoCIOT(Controle);
  end;
end;

procedure TFmFrtMnfCab.ExcluiCondutor1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtMnfMoRodoCondutor', 'Controle', QrFrtMnfMoRodoCondutorControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtMnfMoRodoCondutor,
      QrFrtMnfMoRodoCondutorControle, QrFrtMnfMoRodoCondutorControle.Value);
    ReopenFrtMnfMoRodoCondutor(Controle);
  end;
end;

procedure TFmFrtMnfCab.ExcluiDocumento1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtMnfInfDoc', 'Controle', QrFrtMnfInfDocControle.Value, Dmod.MyDB) = ID_YES then
  begin
    MDFe_PF.TotaisMDFe(QrFrtMnfCabCodigo.Value, QrFrtMnfCab);
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtMnfInfDoc,
      QrFrtMnfInfDocControle, QrFrtMnfInfDocControle.Value);
    ReopenFrtMnfInfDoc(Controle);
  end;
end;

procedure TFmFrtMnfCab.ExcluiLacre1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtMnfLacres', 'Controle', QrFrtMnfLacresControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtMnfLacres,
      QrFrtMnfLacresControle, QrFrtMnfLacresControle.Value);
    ReopenFrtMnfLacres(Controle);
  end;
end;

procedure TFmFrtMnfCab.Excluimunicpiodecarregamento1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtMnfMunCarrega', 'Controle', QrFrtMnfMunCarregaControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtMnfMunCarrega,
      QrFrtMnfMunCarregaControle, QrFrtMnfMunCarregaControle.Value);
    ReopenFrtMnfMunCarrega(Controle);
  end;
end;

procedure TFmFrtMnfCab.ExcluiUFdePercurso1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtMnfInfPercurso', 'Controle', QrFrtMnfInfPercursoControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtMnfInfPercurso,
      QrFrtMnfInfPercursoControle, QrFrtMnfInfPercursoControle.Value);
    ReopenFrtMnfInfPercurso(Controle);
  end;
end;

procedure TFmFrtMnfCab.ExcluiValePedgio1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtMnfMoRodoValePedg', 'Controle', QrFrtMnfMoRodoValePedgControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtMnfMoRodoValePedg,
      QrFrtMnfMoRodoValePedgControle, QrFrtMnfMoRodoValePedgControle.Value);
    ReopenFrtMnfMoRodoValePedg(Controle);
  end;
end;

procedure TFmFrtMnfCab.ExcluiVeculoreboque1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtMnfMoRodoVeicRebo', 'Controle', QrFrtMnfMoRodoVeicReboControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtMnfMoRodoVeicRebo,
      QrFrtMnfMoRodoVeicReboControle, QrFrtMnfMoRodoVeicReboControle.Value);
    ReopenFrtMnfMoRodoVeicRebo(Controle);
  end;
end;

procedure TFmFrtMnfCab.IncluiCIOT1Click(Sender: TObject);
begin
  MostraFormFrtMnfMoRodoCIOT(stIns);
end;

procedure TFmFrtMnfCab.IncluiCondutor1Click(Sender: TObject);
begin
  MostraFormFrtMnfMoRodoCondutor(stIns);
end;

procedure TFmFrtMnfCab.IncluiDocumento1Click(Sender: TObject);
begin
  MostraFrtMnfInfDoc(stIns);
end;

procedure TFmFrtMnfCab.IncluiLacre1Click(Sender: TObject);
begin
  MostraFormFrtMnfLacres(stIns);
end;

procedure TFmFrtMnfCab.Incluimunicpiodecarregamento1Click(Sender: TObject);
begin
  MostraFrtMnfMunCarrega(stIns);
end;

procedure TFmFrtMnfCab.IncluiUFdePercurso1Click(Sender: TObject);
begin
  MostraFrtMnfInfPercurso(stIns);
end;

procedure TFmFrtMnfCab.IncluiValePedgio2Click(Sender: TObject);
begin
  MostraFormFrtMnfMoRodoValePedg(stIns);
end;

procedure TFmFrtMnfCab.IncluiVeculoreboque1Click(Sender: TObject);
begin
  MostraFormFrtMnfMoRodoVeicRebo(stIns);
end;

procedure TFmFrtMnfCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFrtMnfMunCarrega(stUpd);
end;

procedure TFmFrtMnfCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmFrtMnfCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFrtMnfCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFrtMnfCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'FrtMnfMunCarrega', 'Controle', QrFrtMnfMunCarregaControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFrtMnfMunCarrega,
      QrFrtMnfMunCarregaControle, QrFrtMnfMunCarregaControle.Value);
    ReopenFrtMnfMunCarrega(Controle);
  end;
}
end;

procedure TFmFrtMnfCab.ReopenFrtMnfAutXml(Controle: Integer);
var
  ATT_AddForma: String;
begin
  ATT_AddForma := dmkPF.ArrayToTexto('cga.AddForma', 'NO_AddForma', pvPos, True,
    sNFeautXML);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfAutXml, Dmod.MyDB, [
  'SELECT cga.*, ',
  ATT_AddForma,
  'IF(cga.Tipo=0, "CNPJ", "CPF") TpDOC,',
  'CASE cga.AddForma ',
  '  WHEN 0 THEN "(N/I)"  ',
  '  WHEN 1 THEN eco.Nome ',
  '  WHEN 2 THEN IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ',
  '  WHEN 3 THEN " "  ',
  '  ELSE "???" END NO_ENT_ECO, ',
  'IF(cga.Tipo=0, cga.CNPJ, cga.CPF) CNPJ_CPF ',
  'FROM frtmnfautxml cga ',
  'LEFT JOIN entidades ent ON ent.Codigo=cga.AddIDCad',
  'LEFT JOIN enticontat eco ON eco.Controle=cga.AddIDCad',
  'WHERE cga.Codigo=' + Geral.FF0(QrFrtMnfCabCodigo.Value),
  '']);
  //
  if Controle <> 0 then
    QrFrtMnfAutXml.Locate('Controle', Controle, []);
end;

procedure TFmFrtMnfCab.ReopenFrtMnfInfDoc(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfInfDoc, Dmod.MyDB, [
  'SELECT fmd.*, tri.Nome NO_TrnsIts, tri.Placa  ',
  'FROM frtmnfinfdoc fmd',
  'LEFT JOIN trnsits tri ON tri.Controle=fmd.TrnsIts',
  'WHERE fmd.Codigo=' + Geral.FF0(QrFrtMnfCabCodigo.Value),
  'ORDER BY fmd.Controle',
  '']);
  //
  QrFrtMnfInfDoc.Locate('Controle', Controle, []);
end;

procedure TFmFrtMnfCab.ReopenFrtMnfInfPercurso(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfInfPercurso, Dmod.MyDB, [
  'SELECT fmp.*  ',
  'FROM frtmnfinfpercurso fmp ',
  'WHERE fmp.Codigo=' + Geral.FF0(QrFrtMnfCabCodigo.Value),
  'ORDER BY fmp.Controle ',
  '']);
  //
  QrFrtMnfInfPercurso.Locate('Controle', Controle, []);
end;

procedure TFmFrtMnfCab.ReopenFrtMnfLacres(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfLacres, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtmnflacres ',
  'WHERE Codigo=' + Geral.FF0(QrFrtMnfCabCodigo.Value),
  '']);
  //
  QrFrtMnfLacres.Locate('Controle', Controle, []);
end;

procedure TFmFrtMnfCab.ReopenFrtMnfMoRodoCIOT(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfMoRodoCIOT, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtmnfmorodociot ',
  'WHERE Codigo=' + Geral.FF0(QrFrtMnfCabCodigo.Value),
  '']);
  //
  QrFrtMnfMoRodoCondutor.Locate('Controle', Controle, []);
end;

procedure TFmFrtMnfCab.ReopenFrtMnfMoRodoCondutor(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfMoRodoCondutor, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtmnfmorodocondutor ',
  'WHERE Codigo=' + Geral.FF0(QrFrtMnfCabCodigo.Value),
  '']);
  //
  QrFrtMnfMoRodoCondutor.Locate('Controle', Controle, []);
end;

procedure TFmFrtMnfCab.ReopenFrtMnfMoRodoValePedg(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfMoRodoValePedg, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtmnfmorodovalepedg ',
  'WHERE Codigo=' + Geral.FF0(QrFrtMnfCabCodigo.Value),
  '']);
  //
  QrFrtMnfMoRodoValePedg.Locate('Controle', Controle, []);
end;

procedure TFmFrtMnfCab.ReopenFrtMnfMoRodoVeicRebo(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfMoRodoVeicRebo, Dmod.MyDB, [
  'SELECT rcv.*, its.Nome NO_VEIC, its.Placa ',
  'FROM frtmnfmorodoveicrebo rcv ',
  'LEFT JOIN trnsits its ON its.Controle=rcv.cInt ',
  'WHERE rcv.Codigo=' + Geral.FF0(QrFrtMnfCabCodigo.Value),
  '']);
  //
  QrFrtMnfMoRodoVeicRebo.Locate('Controle', Controle, []);
end;

procedure TFmFrtMnfCab.ReopenFrtMnfMunCarrega(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfMunCarrega, Dmod.MyDB, [
  'SELECT fmm.*  ',
  'FROM frtmnfmuncarrega fmm ',
  'WHERE fmm.Codigo=' + Geral.FF0(QrFrtMnfCabCodigo.Value),
  'ORDER BY fmm.Controle ',
  '']);
  //
  QrFrtMnfMunCarrega.Locate('Controle', Controle, []);
end;


procedure TFmFrtMnfCab.ReopenMDFeCabA();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeCabA, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfecaba ',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_6001),
  'AND FatNum=' + Geral.FF0(QrFrtMnfCabCodigo.Value),
  'AND Empresa=' + Geral.FF0(QrFrtMnfCabEmpresa.Value),
  '']);
end;

procedure TFmFrtMnfCab.ReopenParamsMDFe();
var
  Empresa: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  if Empresa <> 0 then
  begin
    Empresa := DModG.QrEmpresasCodigo.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrParamsMDFe, Dmod.MyDB, [
    'SELECT Controle, CONCAT("S�rie ", SerieMDFe) NOME',
    'FROM paramsmdfe ',
    'WHERE Codigo=' + Geral.FF0(Empresa),
    'AND MaxSeqLib > IncSeqAuto',
    'ORDER BY NOME',
    '']);
  end else
    QrParamsMDFe.Close;
end;

procedure TFmFrtMnfCab.RGModalClick(Sender: TObject);
begin
  if RGModal.ItemIndex <> Integer(TMDFeModal.mdfemodalRodoviario(*1*)) then
    RGModal.ItemIndex := Integer(TMDFeModal.mdfemodalRodoviario(*1*));
end;

procedure TFmFrtMnfCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFrtMnfCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFrtMnfCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFrtMnfCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFrtMnfCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFrtMnfCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtMnfCab.BtValePedagioClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMValePedg, BtValePedagio);
end;

procedure TFmFrtMnfCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFrtMnfCabCodigo.Value;
  Close;
end;

procedure TFmFrtMnfCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFrtMnfMunCarrega(stIns);
end;

procedure TFmFrtMnfCab.CabAltera1Click(Sender: TObject);
var
  Filial: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrFrtMnfCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'frtmnfcab');
  //
  Filial := DModG.ObtemFilialDeEntidade(QrFrtMnfCabEmpresa.Value);
  EdEmpresa.ValueVariant := Filial;
  CBEmpresa.KeyValue     := Filial;
  //
end;

procedure TFmFrtMnfCab.BtCondutorClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCondutor, BtCondutor);
end;

procedure TFmFrtMnfCab.BtConfirmaClick(Sender: TObject);
var
  Nome, Abertura, (*Encerrou,*) DataViagIniPrv, (*DataViagIniSai,*) DataViagFimPrv,
  (*DataViagFimChg,*) UFIni, UFFim, infAdFisco, infCpl, CodAgPorto: String;
  Codigo, Empresa, FrtRegMnf, TrnsIts, TpEmit, Modal, (*TpEmis,*) FrtRegPth,
  ParamsMDFe, Tot_qCTe, Tot_qNFe, (*Tot_qMDFe,*) Tot_cUnid, Filial: Integer;
  Tot_vCarga, Tot_qCarga: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Filial         := EdEmpresa.ValueVariant;
  DataViagIniPrv := Geral.FDT(Int(TPDataViagIniPrv.Date) + EdDataViagIniPrv.ValueVariant, 109);
  //DataViagIniSai := ;
  DataViagFimPrv := Geral.FDT(Int(TPDataViagFimPrv.Date) + EdDataViagFimPrv.ValueVariant, 109);
  //DataViagFimChg := ;
  FrtRegMnf      := 0; // ver!!!
  TrnsIts        := EdTrnsIts.ValueVariant;
  TpEmit         := RGTpEmit.ItemIndex;
  Modal          := RGModal.ItemIndex;
  //TpEmis         := RGTpEmis.ItemIndex;
  FrtRegPth      := 0; // ver!!
  UFIni          := EdUFIni.Text;
  UFFim          := EdUFFim.Text;
  ParamsMDFe     := EdParamsMDFe.ValueVariant;
  Tot_qCTe       := EdTot_qCTe.ValueVariant;
  Tot_qNFe       := EdTot_qNFe.ValueVariant;
  //Tot_qMDFe      := ;
  Tot_vCarga     := EdTot_vCarga.ValueVariant;
  Tot_cUnid      := RGTot_cUnid.ItemIndex;
  Tot_qCarga     := EdTot_qCarga.ValueVariant;
  infAdFisco     := MeInfAdFisco.Text;
  infCpl         := MeInfCpl.Text;
  CodAgPorto     := EdCodAgPorto.ValueVariant;
  //
  if SQLType = stIns then
    Abertura := Geral.FDT(DModG.ObtemAgora, 109)
  else
    Abertura := Geral.FDT(QrFrtMnfCabAbertura.Value, 109);
  //
  //if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(TrnsIts = 0, EdTrnsIts, 'Informe o ve�culo de tra��o!') then Exit;
  //
  if MyObjects.FIC(Filial = 0, EdEmpresa, 'Informe a Empresa!') then Exit;
  Empresa := DmodG.ObtemEntidadeDeFilial(Filial);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a Empresa!') then Exit;
  //
  if MyObjects.FIC(ParamsMDFe = 0, EdParamsMDFe, 'Informe a s�rie do MDF-e!') then Exit;
  if MyObjects.FIC(TpEmit = 0, RGTpEmit, 'Informe o tipo de emitente!') then Exit;
  if MyObjects.FIC(Trim(EdUFIni.Text) = '', EdUFIni, 'Informe a UF inicial!') then
    Exit;
  if MyObjects.FIC(Trim(EdUFFim.Text) = '', EdUFFim, 'Informe a UF final!') then
    Exit;
  if MyObjects.FIC(Tot_vCarga < 0.0001, EdTot_vCarga, 'Informe o valor total da carga!') then Exit;
  if MyObjects.FIC(Tot_cUnid = 0, RGTot_cUnid, 'Informe a unidade de medida!') then Exit;
  if MyObjects.FIC(Tot_qCarga < 0.0001, EdTot_qCarga, 'Informe o peso bruto!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('frtmnfcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtmnfcab', False, [
  'Nome', 'Empresa', 'Abertura',
  'DataViagIniPrv', (*'DataViagIniSai',*)
  'DataViagFimPrv', (*'DataViagFimChg',*) 'FrtRegMnf',
  'TrnsIts', 'TpEmit', 'Modal',
  (*'TpEmis',*) 'FrtRegPth', 'UFIni',
  'UFFim', 'ParamsMDFe', 'Tot_qCTe',
  'Tot_qNFe', (*'Tot_qMDFe',*) 'Tot_vCarga',
  'Tot_cUnid', 'Tot_qCarga', 'infAdFisco',
  'infCpl', 'CodAgPorto'], [
  'Codigo'], [
  Nome, Empresa, Abertura,
  DataViagIniPrv, (*DataViagIniSai,*)
  DataViagFimPrv, (*DataViagFimChg,*) FrtRegMnf,
  TrnsIts, TpEmit, Modal,
  (*TpEmis,*) FrtRegPth, UFIni,
  UFFim, ParamsMDFe, Tot_qCTe,
  Tot_qNFe, (*Tot_qMDFe,*) Tot_vCarga,
  Tot_cUnid, Tot_qCarga, infAdFisco,
  infCpl, CodAgPorto], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmFrtMnfCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'frtmnfcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'frtmnfcab', 'Codigo');
end;

procedure TFmFrtMnfCab.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmFrtMnfCab.BtInfDocClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInfDoc, BtInfDoc);
end;

procedure TFmFrtMnfCab.BtInfPercursoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInfPercurso, BtInfPercurso);
end;

procedure TFmFrtMnfCab.BtLacresClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLacres, BtLacres);
end;

procedure TFmFrtMnfCab.BtMunCarregaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMunCarrega, BtMunCarrega);
end;

procedure TFmFrtMnfCab.BtReboqueClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMreboque, BtReboque);
end;

procedure TFmFrtMnfCab.AlteraCIOT1Click(Sender: TObject);
begin
  MostraFormFrtMnfMoRodoCIOT(stUpd);
end;

procedure TFmFrtMnfCab.AlteraCondutor1Click(Sender: TObject);
begin
  MostraFormFrtMnfMoRodoCondutor(stUpd);
end;

procedure TFmFrtMnfCab.AlteraDocumento1Click(Sender: TObject);
begin
  MostraFrtMnfInfDoc(stUpd);
end;

procedure TFmFrtMnfCab.AlteraLacre1Click(Sender: TObject);
begin
  MostraFormFrtMnfLacres(stUpd);
end;

procedure TFmFrtMnfCab.AlteraValePedgio1Click(Sender: TObject);
begin
  MostraFormFrtMnfMoRodoValePedg(stUpd);
end;

procedure TFmFrtMnfCab.AlteraVeculoreboque1Click(Sender: TObject);
begin
  MostraFormFrtMnfMoRodoVeicRebo(stUpd);
end;

procedure TFmFrtMnfCab.BtAutXMLClick(Sender: TObject);
begin
  MostraFormFrtMnfAutXml();
end;

procedure TFmFrtMnfCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmFrtMnfCab.BtCIOTClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCIOT, BtCIOT);
end;

procedure TFmFrtMnfCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FBtCab := True;
  GBEdita.Align := alClient;
  //DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTrnsIts, Dmod.MyDB, [
  'SELECT Controle, ',
  'CONCAT(Placa, "  ", Nome) DESCRI ',
  'FROM trnsits ',
  'WHERE tpVeic=' + Geral.FF0(Integer(ctetpveicTracao)),
  'ORDER BY DESCRI ',
  '']);
end;

procedure TFmFrtMnfCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFrtMnfCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFrtMnfCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFrtMnfCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFrtMnfCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFrtMnfCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFrtMnfCab.QrFrtMnfCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmFrtMnfCab.QrFrtMnfCabAfterScroll(DataSet: TDataSet);
begin
  ReopenMDFeCabA();
  //
  ReopenFrtMnfMunCarrega(0);
  ReopenFrtMnfInfPercurso(0);
  ReopenFrtMnfInfDoc(0);
  ReopenFrtMnfLacres(0);
  ReopenFrtMnfAutXML(0);
  ReopenFrtMnfMoRodoCondutor(0);
  ReopenFrtMnfMoRodoVeicRebo(0);
  ReopenFrtMnfMoRodoValePedg(0);
  ReopenFrtMnfMoRodoCIOT(0);
end;

procedure TFmFrtMnfCab.QrFrtMnfCabBeforeOpen(DataSet: TDataSet);
begin
  QrFrtMnfCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFrtMnfCab.QrMDFeCabAAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := XXe_PF.PerrmiteDesencerrarXXe(QrMDFeCabAStatus.Value);
  //
  HabilitaBotoes(Habilita);
end;

procedure TFmFrtMnfCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrFrtMnfCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmFrtMnfCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFrtMnfCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'frtmnfcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFrtMnfCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtMnfCab.HabilitaBotoes(Habilita: Boolean);
begin
  FBtCab                := Habilita;
  BtMunCarrega.Enabled  := Habilita;
  BtInfPercurso.Enabled := Habilita;
  BtInfDoc.Enabled      := Habilita;
  BtLacres.Enabled      := Habilita;
  BtAutXML.Enabled      := Habilita;
  BtCondutor.Enabled    := Habilita;
  BtReboque.Enabled     := Habilita;
  BtValePedagio.Enabled := Habilita;
  BtCIOT.Enabled        := Habilita;
  BtEncerra.Enabled     := Habilita;
end;

procedure TFmFrtMnfCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrFrtMnfCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'frtmnfcab');
end;

procedure TFmFrtMnfCab.QrFrtMnfCabBeforeClose(DataSet: TDataSet);
begin
  QrFrtMnfMunCarrega.Close;
  QrFrtMnfInfPercurso.Close;
  QrFrtMnfInfDoc.Close;
  QrFrtMnfLacres.Close;
  QrFrtMnfAutXML.Close;
  QrFrtMnfMoRodoCondutor.Close;
  QrFrtMnfMoRodoVeicRebo.Close;
  QrFrtMnfMoRodoValePedg.Close;
  QrFrtMnfMoRodoCIOT.Close;
end;


 { TODO -cMDFe : MDFe - Contingencia }


end.

