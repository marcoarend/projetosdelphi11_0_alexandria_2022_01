unit FrtMnfMoRodoCondutor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmFrtMnfMoRodoCondutor = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBEntidade: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    EdxNome: TdmkEdit;
    LaxNome: TLabel;
    EdCPF: TdmkEdit;
    LaCPF: TLabel;
    QrEntidadesCPF: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdEntidadeRedefinido(Sender: TObject);
  private
    { Private declarations }
    FCriando: Boolean;
    //
    procedure ReopenFrMnfMoRodotCondutor(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFrtMnfMoRodoCondutor: TFmFrtMnfMoRodoCondutor;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleGeral;

{$R *.DFM}

procedure TFmFrtMnfMoRodoCondutor.BtOKClick(Sender: TObject);
var
  xNome, CPF: String;
  Codigo, Controle, Entidade: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  xNome          := EdxNome.Text;
  CPF            := Geral.SoNumero_TT(EdCPF.Text);
  Entidade       := EdEntidade.ValueVariant;
  //
  if MyObjects.FIC(Trim(EdxNome.Text) = '', EdxNome, 'Informe o nome do condutor!') then
    Exit;
  if MyObjects.FIC(Trim(EdCPF.Text) = '', EdCPF, 'Informe o CPF do condutor!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('frtmnfmorodocondutor', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtmnfmorodocondutor', False, [
  'Codigo', 'xNome', 'CPF',
  'Entidade'], [
  'Controle'], [
  Codigo, xNome, CPF,
  Entidade], [
  Controle], True) then
  begin
    ReopenFrMnfMoRodotCondutor(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      //
      EdEntidade.ValueVariant  := 0;
      CBEntidade.KeyValue      := Null;
      EdxNome.ValueVariant     := '';
      EdCPF.ValueVariant       := '';
      //
      EdEntidade.SetFocus;
    end else Close;
  end;
end;

procedure TFmFrtMnfMoRodoCondutor.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFrtMnfMoRodoCondutor.EdEntidadeRedefinido(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := EdEntidade.ValueVariant = 0;
  //
  LaxNome.Enabled := Habilita;
  EdxNome.Enabled := Habilita;
  LaCPF.Enabled := Habilita;
  EdCPF.Enabled := Habilita;
  //
  if (not FCriando) then
  begin
    if not Habilita then
    begin
      EdxNome.Text := QrEntidadesNome.Value;
      EdCPF.ValueVariant := QrEntidadesCPF.Value;
    end;
  end;
end;

procedure TFmFrtMnfMoRodoCondutor.FormActivate(Sender: TObject);
begin
  FCriando := False;
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFrtMnfMoRodoCondutor.FormCreate(Sender: TObject);
begin
  FCriando := True;
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmFrtMnfMoRodoCondutor.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFrtMnfMoRodoCondutor.ReopenFrMnfMoRodotCondutor(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmFrtMnfMoRodoCondutor.SpeedButton1Click(Sender: TObject);
begin
  DModG.CadastroESelecaoDeEntidade(EdEntidade.ValueVariant, QrEntidades,
    EdEntidade, CBEntidade);
end;

end.
