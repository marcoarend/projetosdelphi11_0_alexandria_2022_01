object FmFrtRegFatC: TFmFrtRegFatC
  Left = 368
  Top = 194
  Caption = 'FRT-REGFA-001 :: Regras Comerciais de Frete'
  ClientHeight = 629
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 533
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 129
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsFrtRegFatC
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsFrtRegFatC
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 469
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 533
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 337
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 16
        Top = 240
        Width = 245
        Height = 13
        Caption = 'Caracter'#237'stica adicional do transporte (OPCIONAL) :'
      end
      object Label6: TLabel
        Left = 392
        Top = 240
        Width = 229
        Height = 13
        Caption = 'Caracter'#237'stica adicional do servi'#231'o (OPCIONAL):'
      end
      object Label8: TLabel
        Left = 16
        Top = 280
        Width = 162
        Height = 13
        Caption = 'Observa'#231#245'es Gerais (OPCIONAL):'
      end
      object SbCaracAd: TSpeedButton
        Left = 368
        Top = 256
        Width = 23
        Height = 22
        OnClick = SbCaracAdClick
      end
      object SbCaracSer: TSpeedButton
        Left = 744
        Top = 256
        Width = 23
        Height = 22
        OnClick = SbCaracSerClick
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGForPag: TdmkRadioGroup
        Left = 16
        Top = 60
        Width = 129
        Height = 109
        Caption = 'Forma de pag. do serv.:'
        ItemIndex = 1
        Items.Strings = (
          'Pago'
          'A pagar'
          'Outros')
        TabOrder = 2
        QryCampo = 'ForPag'
        UpdCampo = 'ForPag'
        UpdType = utYes
        OldValor = 0
      end
      object RGModal: TdmkRadioGroup
        Left = 148
        Top = 60
        Width = 241
        Height = 109
        Caption = ' Modal: '
        Columns = 2
        ItemIndex = 1
        Items.Strings = (
          '00-Indefinido'
          '01-Rodovi'#225'rio'
          '02-A'#233'reo'
          '03-Aquavi'#225'rio'
          '04-Ferrovi'#225'rio'
          '05-Dutovi'#225'rio'
          '06-Multimodal')
        TabOrder = 3
        QryCampo = 'Modal'
        UpdCampo = 'Modal'
        UpdType = utYes
        OldValor = 0
      end
      object RGTpServ: TdmkRadioGroup
        Left = 392
        Top = 60
        Width = 213
        Height = 109
        Caption = 'Tipo de Servi'#231'o (OPCIONAL): '
        Items.Strings = (
          '0-Normal'
          '1-Subcontrata'#231#227'o'
          '2-Redespacho'
          '3-Redespacho intermedi'#225'rio'
          '4-Servi'#231'o Vinculado a Multimodal')
        TabOrder = 4
        QryCampo = 'TpServ'
        UpdCampo = 'TpServ'
        UpdType = utYes
        OldValor = 0
      end
      object GroupBox1: TGroupBox
        Left = 268
        Top = 172
        Width = 248
        Height = 65
        Caption = 'Munic'#237'pio de in'#237'cio da presta'#231#227'o (OPCIONAL): '
        TabOrder = 7
        object Label24: TLabel
          Left = 9
          Top = 20
          Width = 17
          Height = 13
          Caption = 'UF:'
        end
        object Label105: TLabel
          Left = 40
          Top = 20
          Width = 36
          Height = 13
          Caption = 'Cidade:'
        end
        object CBCMunIni: TdmkDBLookupComboBox
          Left = 97
          Top = 36
          Width = 144
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsMunIni
          TabOrder = 2
          dmkEditCB = EdCMunIni
          QryCampo = 'CMunIni'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdUFIni: TdmkEdit
          Left = 9
          Top = 36
          Width = 28
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtUF
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'UFIni'
          UpdCampo = 'UFIni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnRedefinido = EdUFIniRedefinido
        end
        object EdCMunIni: TdmkEditCB
          Left = 40
          Top = 36
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CMunIni'
          UpdCampo = 'CMunIni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCMunIni
          IgnoraDBLookupComboBox = True
        end
      end
      object GroupBox2: TGroupBox
        Left = 516
        Top = 172
        Width = 248
        Height = 65
        Caption = 'Munic'#237'pio final da presta'#231#227'o (OPCIONAL): '
        TabOrder = 8
        object Label3: TLabel
          Left = 9
          Top = 20
          Width = 17
          Height = 13
          Caption = 'UF:'
        end
        object Label4: TLabel
          Left = 40
          Top = 20
          Width = 36
          Height = 13
          Caption = 'Cidade:'
        end
        object CBCMunFim: TdmkDBLookupComboBox
          Left = 97
          Top = 36
          Width = 144
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsMunFim
          TabOrder = 2
          dmkEditCB = EdCMunFim
          QryCampo = 'CMunFim'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdUFFim: TdmkEdit
          Left = 9
          Top = 36
          Width = 28
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtUF
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'UFFim'
          UpdCampo = 'UFFim'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnRedefinido = EdUFFimRedefinido
        end
        object EdCMunFim: TdmkEditCB
          Left = 40
          Top = 36
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CMunFim'
          UpdCampo = 'CMunFim'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCMunFim
          IgnoraDBLookupComboBox = True
        end
      end
      object RGToma: TdmkRadioGroup
        Left = 608
        Top = 60
        Width = 157
        Height = 109
        Caption = ' Tomador do servi'#231'o: '
        Items.Strings = (
          '0-Remetente'
          '1-Expedidor'
          '2-Recebedor'
          '3-Destinat'#225'rio'
          '4-Outros')
        TabOrder = 5
        QryCampo = 'Toma'
        UpdCampo = 'Toma'
        UpdType = utYes
        OldValor = 0
      end
      object EdCaracAd: TdmkEditCB
        Left = 16
        Top = 256
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CaracAd'
        UpdCampo = 'CaracAd'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCaracAd
        IgnoraDBLookupComboBox = True
      end
      object CBCaracAd: TdmkDBLookupComboBox
        Left = 73
        Top = 256
        Width = 292
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCaracAd
        TabOrder = 10
        dmkEditCB = EdCaracAd
        QryCampo = 'CaracAd'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCaracSer: TdmkEditCB
        Left = 392
        Top = 256
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CaracSer'
        UpdCampo = 'CaracSer'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCaracSer
        IgnoraDBLookupComboBox = True
      end
      object CBCaracSer: TdmkDBLookupComboBox
        Left = 449
        Top = 256
        Width = 292
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCaracSer
        TabOrder = 12
        dmkEditCB = EdCaracSer
        QryCampo = 'CaracSer'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object MexObs: TdmkMemo
        Left = 16
        Top = 296
        Width = 749
        Height = 89
        MaxLength = 2000
        TabOrder = 13
        OnKeyDown = MexObsKeyDown
        QryCampo = 'xObs'
        UpdCampo = 'xObs'
        UpdType = utYes
      end
      object GroupBox3: TGroupBox
        Left = 16
        Top = 172
        Width = 252
        Height = 65
        Caption = 'Munic'#237'pio de envio do CT-e (transmiss'#227'o do doc.): '
        TabOrder = 6
        object Label10: TLabel
          Left = 9
          Top = 20
          Width = 17
          Height = 13
          Caption = 'UF:'
        end
        object Label11: TLabel
          Left = 40
          Top = 20
          Width = 36
          Height = 13
          Caption = 'Cidade:'
        end
        object CBCMunEnv: TdmkDBLookupComboBox
          Left = 97
          Top = 36
          Width = 148
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsMunEnv
          TabOrder = 2
          dmkEditCB = EdCMunEnv
          QryCampo = 'CMunEnv'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdUFEnv: TdmkEdit
          Left = 9
          Top = 36
          Width = 28
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtUF
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'UFEnv'
          UpdCampo = 'UFEnv'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnRedefinido = EdUFEnvRedefinido
        end
        object EdCMunEnv: TdmkEditCB
          Left = 40
          Top = 36
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CMunEnv'
          UpdCampo = 'CMunEnv'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCMunEnv
          IgnoraDBLookupComboBox = True
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 470
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 0
      Top = 390
      Width = 784
      Height = 80
      Align = alBottom
      TabOrder = 1
      object BtTabelaPrc: TSpeedButton
        Left = 744
        Top = 8
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = BtTabelaPrcClick
      end
      object LaTabelaPrc: TLabel
        Left = 8
        Top = 13
        Width = 81
        Height = 13
        Caption = 'Tabela de pre'#231'o:'
      end
      object BtCondicaoPG: TSpeedButton
        Left = 384
        Top = 52
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = BtCondicaoPGClick
      end
      object Label12: TLabel
        Left = 410
        Top = 37
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object LaCondicaoPG: TLabel
        Left = 8
        Top = 37
        Width = 119
        Height = 13
        Caption = 'Condi'#231#227'o de pagamento:'
      end
      object SpeedButton13: TSpeedButton
        Left = 744
        Top = 52
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton13Click
      end
      object CBTabelaPrc: TdmkDBLookupComboBox
        Left = 188
        Top = 8
        Width = 553
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFrtPrcCad
        TabOrder = 1
        dmkEditCB = EdTabelaPrc
        QryCampo = 'TabelaPrc'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdTabelaPrc: TdmkEditCB
        Left = 128
        Top = 8
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'TabelaPrc'
        UpdCampo = 'TabelaPrc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTabelaPrc
        IgnoraDBLookupComboBox = False
      end
      object CBCartEmiss: TdmkDBLookupComboBox
        Left = 456
        Top = 52
        Width = 285
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCartEmiss
        TabOrder = 5
        dmkEditCB = EdCartEmiss
        QryCampo = 'CartEmiss'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdCartEmiss: TdmkEditCB
        Left = 414
        Top = 52
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CartEmiss'
        UpdCampo = 'CartEmiss'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCartEmiss
        IgnoraDBLookupComboBox = False
      end
      object CBCondicaoPG: TdmkDBLookupComboBox
        Left = 64
        Top = 52
        Width = 317
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsPediPrzCab
        TabOrder = 3
        dmkEditCB = EdCondicaoPG
        QryCampo = 'CondicaoPg'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdCondicaoPG: TdmkEditCB
        Left = 8
        Top = 52
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CondicaoPg'
        UpdCampo = 'CondicaoPg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCondicaoPG
        IgnoraDBLookupComboBox = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 339
        Height = 32
        Caption = 'Regras Comerciais de Frete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 339
        Height = 32
        Caption = 'Regras Comerciais de Frete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 339
        Height = 32
        Caption = 'Regras Comerciais de Frete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrFrtRegFatC: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFrtRegFatCBeforeOpen
    AfterOpen = QrFrtRegFatCAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM frtregfatc')
    Left = 200
    Top = 60
    object QrFrtRegFatCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtRegFatCNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrFrtRegFatCForPag: TSmallintField
      FieldName = 'ForPag'
    end
    object QrFrtRegFatCModal: TSmallintField
      FieldName = 'Modal'
    end
    object QrFrtRegFatCTpServ: TSmallintField
      FieldName = 'TpServ'
    end
    object QrFrtRegFatCCMunIni: TIntegerField
      FieldName = 'CMunIni'
    end
    object QrFrtRegFatCUFIni: TWideStringField
      FieldName = 'UFIni'
      Size = 2
    end
    object QrFrtRegFatCCMunFim: TIntegerField
      FieldName = 'CMunFim'
    end
    object QrFrtRegFatCUFFim: TWideStringField
      FieldName = 'UFFim'
      Size = 2
    end
    object QrFrtRegFatCToma: TSmallintField
      FieldName = 'Toma'
    end
    object QrFrtRegFatCCaracAd: TIntegerField
      FieldName = 'CaracAd'
    end
    object QrFrtRegFatCCaracSer: TIntegerField
      FieldName = 'CaracSer'
    end
    object QrFrtRegFatCxObs: TWideMemoField
      FieldName = 'xObs'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFrtRegFatCLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtRegFatCDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtRegFatCDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtRegFatCUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtRegFatCUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtRegFatCAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtRegFatCAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFrtRegFatCNO_MunFim: TWideStringField
      FieldName = 'NO_MunFim'
      Size = 100
    end
    object QrFrtRegFatCNO_MunIni: TWideStringField
      FieldName = 'NO_MunIni'
      Size = 100
    end
    object QrFrtRegFatCNO_MunEnv: TWideStringField
      FieldName = 'NO_MunEnv'
      Size = 100
    end
    object QrFrtRegFatCCMunEnv: TIntegerField
      FieldName = 'CMunEnv'
    end
    object QrFrtRegFatCUFEnv: TWideStringField
      FieldName = 'UFEnv'
      Size = 2
    end
    object QrFrtRegFatCTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrFrtRegFatCCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrFrtRegFatCCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
  end
  object DsFrtRegFatC: TDataSource
    DataSet = QrFrtRegFatC
    Left = 200
    Top = 108
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrMunIni: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 272
    Top = 60
    object QrMunIniCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMunIniNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMunIni: TDataSource
    DataSet = QrMunIni
    Left = 272
    Top = 104
  end
  object QrMunFim: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 328
    Top = 60
    object QrMunFimCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMunFimNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMunFim: TDataSource
    DataSet = QrMunFim
    Left = 328
    Top = 104
  end
  object QrCaracAd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM frtcaracad'
      'ORDER BY Nome')
    Left = 448
    Top = 60
    object QrCaracAdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCaracAdNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCaracAd: TDataSource
    DataSet = QrCaracAd
    Left = 448
    Top = 104
  end
  object QrCaracSer: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM frtcaracser'
      'ORDER BY Nome')
    Left = 516
    Top = 60
    object QrCaracSerCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCaracSerNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCaracSer: TDataSource
    DataSet = QrCaracSer
    Left = 516
    Top = 104
  end
  object QrMunEnv: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 388
    Top = 60
    object QrMunEnvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMunEnvNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMunEnv: TDataSource
    DataSet = QrMunEnv
    Left = 388
    Top = 104
  end
  object QrPediPrzCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, MaxDesco,'
      'JurosMes, Parcelas, Percent1, Percent2'
      'FROM pediprzcab'
      'WHERE Aplicacao & :P0 <> 0'
      'ORDER BY Nome')
    Left = 580
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 580
    Top = 104
  end
  object QrCartEmiss: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome'
      'FROM carteiras car'
      'WHERE Tipo=2'
      'ORDER BY Nome')
    Left = 656
    Top = 60
    object QrCartEmissCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCartEmissNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsCartEmiss: TDataSource
    DataSet = QrCartEmiss
    Left = 656
    Top = 104
  end
  object QrFrtPrcCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM frtprccad'
      'ORDER BY Nome')
    Left = 732
    Top = 60
    object QrFrtPrcCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtPrcCadNome: TWideStringField
      DisplayWidth = 100
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsFrtPrcCad: TDataSource
    DataSet = QrFrtPrcCad
    Left = 732
    Top = 108
  end
end
