object FmTrnsIts: TFmTrnsIts
  Left = 339
  Top = 185
  Caption = 'LGS-TRNSP-002 :: Cadastro de Ve'#237'culos de Transporte'
  ClientHeight = 554
  ClientWidth = 617
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 413
    Width = 617
    Height = 27
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 617
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNO_ENT
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNO_ENT: TDBEdit
      Left = 72
      Top = 36
      Width = 533
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'NO_ENT'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 617
    Height = 301
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label7: TLabel
      Left = 96
      Top = 16
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object Label1: TLabel
      Left = 12
      Top = 56
      Width = 33
      Height = 13
      Caption = 'Marca:'
      Enabled = False
    end
    object SpeedButton1: TSpeedButton
      Left = 584
      Top = 72
      Width = 21
      Height = 21
      Caption = '...'
      Enabled = False
      OnClick = SpeedButton1Click
    end
    object Label2: TLabel
      Left = 12
      Top = 96
      Width = 56
      Height = 13
      Caption = 'RENAVAM:'
    end
    object Label4: TLabel
      Left = 96
      Top = 96
      Width = 30
      Height = 13
      Caption = 'Placa:'
    end
    object Label8: TLabel
      Left = 180
      Top = 96
      Width = 40
      Height = 13
      Caption = 'Tara kg:'
    end
    object Label9: TLabel
      Left = 248
      Top = 96
      Width = 60
      Height = 13
      Caption = 'Capacid. kg:'
    end
    object Label10: TLabel
      Left = 316
      Top = 96
      Width = 59
      Height = 13
      Caption = 'Capacid. m'#179':'
    end
    object Label11: TLabel
      Left = 384
      Top = 96
      Width = 17
      Height = 13
      Caption = 'UF:'
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 96
      Top = 32
      Width = 509
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CB_Sel_: TdmkDBLookupComboBox
      Left = 68
      Top = 72
      Width = 513
      Height = 21
      Enabled = False
      KeyField = 'CodUsu'
      ListField = 'Nome'
      TabOrder = 3
      dmkEditCB = Ed_Sel_
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object Ed_Sel_: TdmkEditCB
      Left = 12
      Top = 72
      Width = 56
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CB_Sel_
      IgnoraDBLookupComboBox = False
    end
    object EdRENAVAM: TdmkEdit
      Left = 12
      Top = 112
      Width = 80
      Height = 21
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'RENAVAM'
      UpdCampo = 'RENAVAM'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdPlaca: TdmkEdit
      Left = 96
      Top = 112
      Width = 80
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 7
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Placa'
      UpdCampo = 'Placa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdTaraKg: TdmkEdit
      Left = 180
      Top = 112
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'TaraKg'
      UpdCampo = 'TaraKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdCapKg: TdmkEdit
      Left = 248
      Top = 112
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'CapKg'
      UpdCampo = 'CapKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object RGTpVeic: TdmkRadioGroup
      Left = 416
      Top = 96
      Width = 189
      Height = 41
      Caption = 'Tipo de ve'#237'culo:'
      Columns = 2
      Items.Strings = (
        '0=Tracao'
        '1=Reboque')
      TabOrder = 10
      QryCampo = 'TpVeic'
      UpdCampo = 'TpVeic'
      UpdType = utYes
      OldValor = 0
    end
    object RGTpRod: TdmkRadioGroup
      Left = 12
      Top = 140
      Width = 593
      Height = 81
      Caption = 'Tipo de rodado:'
      Columns = 3
      Items.Strings = (
        '00 - N'#227'o aplic'#225'vel'
        '01 - Truck'
        '02 - Toco'
        '03 - Cavalo Mec'#226'nico'
        '04 - VAN'
        '05 - Utilit'#225'rio'
        '06 - Outros')
      TabOrder = 11
      QryCampo = 'TpRod'
      UpdCampo = 'TpRod'
      UpdType = utYes
      OldValor = 0
    end
    object RGTpCar: TdmkRadioGroup
      Left = 12
      Top = 228
      Width = 593
      Height = 61
      Caption = 'Tipo de carroceria:'
      Columns = 3
      Items.Strings = (
        '00 - N'#227'o aplic'#225'vel'
        '01 - Aberta'
        '02 - Fechada/Ba'#250
        '03 - Granelera'
        '04 - Porta Container'
        '05 - Sider')
      TabOrder = 12
      QryCampo = 'TpCar'
      UpdCampo = 'TpCar'
      UpdType = utYes
      OldValor = 0
    end
    object EdCapM3: TdmkEdit
      Left = 316
      Top = 112
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'CapM3'
      UpdCampo = 'CapM3'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdUF: TdmkEdit
      Left = 384
      Top = 112
      Width = 29
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 9
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'UF'
      UpdCampo = 'UF'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 617
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 569
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 521
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 435
        Height = 32
        Caption = 'Cadastro de Ve'#237'culos de Transporte'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 435
        Height = 32
        Caption = 'Cadastro de Ve'#237'culos de Transporte'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 435
        Height = 32
        Caption = 'Cadastro de Ve'#237'culos de Transporte'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 440
    Width = 617
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 613
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 484
    Width = 617
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 471
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 469
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Qr_Sel_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome '
      'FROM enticargos'
      'ORDER BY Nome')
    Left = 216
    Top = 44
    object Qr_Sel_Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr_Sel_CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object Qr_Sel_Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object Ds_Sel_: TDataSource
    DataSet = Qr_Sel_
    Left = 216
    Top = 92
  end
  object VU_Sel_: TdmkValUsu
    dmkEditCB = Ed_Sel_
    QryCampo = '_Sel_'
    UpdCampo = '_Sel_'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 156
    Top = 44
  end
end
