unit ContasSdoImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, frxClass, frxDBSet,
  dmkPermissoes, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, dmkImage,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, mySQLDirectQuery;

type
  TArr2Double = array of array[0..1] of double;
  //
  TFmContasSdoImp = class(TForm)
    DsCliInt: TDataSource;
    QrCliInt: TmySQLQuery;
    QrCliIntNOMEENTIDADE: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    QrLCS: TmySQLQuery;
    QrLCSCODIGOCONTA: TIntegerField;
    QrLCSNOMECONTA: TWideStringField;
    QrLCSCODIGOSUBGRUPO: TIntegerField;
    QrLCSNOMESUBGRUPO: TWideStringField;
    QrLCSCODIGOGRUPO: TIntegerField;
    QrLCSNOMEGRUPO: TWideStringField;
    QrLCSCODIGOCONJUNTO: TIntegerField;
    QrLCSNOMECONJUNTO: TWideStringField;
    QrLCSCODIGOPLANO: TIntegerField;
    QrLCSNOMEPLANO: TWideStringField;
    QrLCSPL_ATU: TFloatField;
    QrLCSPL_FUT: TFloatField;
    QrLCSCJ_ATU: TFloatField;
    QrLCSCJ_FUT: TFloatField;
    QrLCSGR_ATU: TFloatField;
    QrLCSGR_FUT: TFloatField;
    QrLCSSG_ATU: TFloatField;
    QrLCSSG_FUT: TFloatField;
    QrLCSCO_ATU: TFloatField;
    QrLCSCO_FUT: TFloatField;
    QrLCSPL_CS: TSmallintField;
    QrLCSCJ_CS: TSmallintField;
    QrLCSGR_CS: TSmallintField;
    QrLCSSG_CS: TSmallintField;
    QrLCSCO_CS: TSmallintField;
    QrLCSPL_ATU_TXT: TWideStringField;
    QrLCSPL_FUT_TXT: TWideStringField;
    QrLCSCJ_ATU_TXT: TWideStringField;
    QrLCSCJ_FUT_TXT: TWideStringField;
    QrLCSGR_ATU_TXT: TWideStringField;
    QrLCSGR_FUT_TXT: TWideStringField;
    QrLCSSG_ATU_TXT: TWideStringField;
    QrLCSSG_FUT_TXT: TWideStringField;
    QrLCSCO_ATU_TXT: TWideStringField;
    QrLCSCO_FUT_TXT: TWideStringField;
    frxDsLCS: TfrxDBDataset;
    frxLCS: TfrxReport;
    Panel1: TPanel;
    Panel2: TPanel;
    GBSaldos: TGroupBox;
    GroupBox2: TGroupBox;
    CkContasUser: TCheckBox;
    CkControla: TCheckBox;
    CkOculta: TCheckBox;
    PainelDados: TPanel;
    CkMostraSaldos: TCheckBox;
    frxLCL: TfrxReport;
    QrLCL: TmySQLQuery;
    frxDsLCL: TfrxDBDataset;
    QrLCLCODIGOCONTA: TIntegerField;
    QrLCLNOMECONTA: TWideStringField;
    QrLCLCODIGOSUBGRUPO: TIntegerField;
    QrLCLNOMESUBGRUPO: TWideStringField;
    QrLCLCODIGOGRUPO: TIntegerField;
    QrLCLNOMEGRUPO: TWideStringField;
    QrLCLCODIGOCONJUNTO: TIntegerField;
    QrLCLNOMECONJUNTO: TWideStringField;
    QrLCLCODIGOPLANO: TIntegerField;
    QrLCLNOMEPLANO: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    QrCtaCfgCab: TmySQLQuery;
    DsCtaCfgCab: TDataSource;
    QrCtaCfgCabCodigo: TIntegerField;
    QrCtaCfgCabNome: TWideStringField;
    Panel5: TPanel;
    CkAtivos: TCheckBox;
    CkJoin: TCheckBox;
    CkCtasPosi: TCheckBox;
    GroupBox1: TGroupBox;
    EdCtaCfgCab: TdmkEditCB;
    CBCtaCfgCab: TdmkDBLookupComboBox;
    CkInfoNomeCtaCfg: TCheckBox;
    QrLCLDebito: TWideStringField;
    QrLCLCredito: TWideStringField;
    QrLCLAtivo: TSmallintField;
    QrLCLCtrlaSdo: TSmallintField;
    TCSaldos: TTabControl;
    Label1: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    frxFIN_PLCTA_007: TfrxReport;
    frxDsTotais: TfrxDBDataset;
    QrTotais: TMySQLQuery;
    QrTotaisCREDITO: TFloatField;
    QrTotaisDEBITO: TFloatField;
    QrTotaisNO_CTA: TWideStringField;
    QrTotaisNO_SGR: TWideStringField;
    QrTotaisNO_GRU: TWideStringField;
    QrTotaisNO_CJT: TWideStringField;
    QrTotaisNO_PLA: TWideStringField;
    QrTotaisGenero: TIntegerField;
    QrTotaisSubGrupo: TIntegerField;
    QrTotaisGrupo: TIntegerField;
    QrTotaisConjunto: TIntegerField;
    QrTotaisPlano: TIntegerField;
    DqTotais: TMySQLDirectQuery;
    PnCfgPeriodo: TPanel;
    RGCampoData: TRadioGroup;
    Panel7: TPanel;
    LaDataIni: TLabel;
    LaDataFim: TLabel;
    TPDataIni: TDateTimePicker;
    TPDataFim: TDateTimePicker;
    QrLCLORD_CTA: TIntegerField;
    QrLCLORD_SGR: TIntegerField;
    QrLCLORD_GRU: TIntegerField;
    QrLCLORD_CJT: TIntegerField;
    QrLCLFinContab: TSmallintField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrLCSCalcFields(DataSet: TDataSet);
    procedure frxLCSGetValue(const VarName: String; var Value: Variant);
    procedure CkMostraSaldosClick(Sender: TObject);
    procedure TCSaldosChange(Sender: TObject);
    procedure EdCliIntRedefinido(Sender: TObject);
  private
    { Private declarations }
    FDtEncer, FDtMorto: TDateTime;
    FTabLctA, FTabLctB, FTabLctD: String;
    FEntidade, FEmpresa: Integer;
    FEntidTXT, FEmprTXT: String;
    FArrPLA, FArrCJT, FArrGRU, FArrSGR: TArr2Double; //array[0..1] of double;
    //
    procedure ListaContasSaldos();
    procedure ListaContasLista();
    procedure ListaContasTotais();
    procedure ObtemTotaisNivel(const Nivel, Campo: String; var ArrNivel: TArr2Double);
  public
    { Public declarations }
  end;

var
  FmContasSdoImp: TFmContasSdoImp;

implementation

uses UnMyObjects, Module, ModuleGeral, Entidades, DmkDAC_PF, UMySQLDB;

{$R *.DFM}

const
  CO_ID_C = 0;
  CO_ID_D = 1;

procedure TFmContasSdoImp.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasSdoImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCtaCfgCab, Dmod.MyDB);
  TPDataIni.Date    := Geral.PrimeiroDiaDoMes(Date - 30);
  TPDataFim.Date    := Geral.UltimoDiaDoMes(Date - 30);
end;

procedure TFmContasSdoImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasSdoImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasSdoImp.BtConfirmaClick(Sender: TObject);
begin
  if CkMostraSaldos.Checked then
  begin
    case TCSaldos.TabIndex of
      0: ListaContasSaldos();
      1: ListaContasTotais();
    end;
  end else
    ListaContasLista();
end;

procedure TFmContasSdoImp.ListaContasSaldos();
var
  Ativo: String;
  Entidade: Integer;
begin
  Entidade := Geral.IMV(EdCliInt.Text);
  //
  if Entidade = 0 then
  begin
    Geral.MB_Aviso('Defina o cliente interno!');
    Exit;
  end;

  //fazer saldos de contas
  //so mostrar saldos se no cadastro de contas, sbgru, grupos etc. pedir
  if CkAtivos.Checked then Ativo := ' 1 ' else Ativo := '0, 1';
  if CkJoin.Checked then  Ativo := Ativo + ')'
  else Ativo := Ativo + ') OR (co.Ativo IS NULL)';
  QrLCS.Close;
  QrLCS.SQL.Clear;

  QrLCS.SQL.Add('SELECT co.Codigo CODIGOCONTA, co.Nome NOMECONTA,');
  QrLCS.SQL.Add('sg.Codigo CODIGOSUBGRUPO, sg.Nome NOMESUBGRUPO,');
  QrLCS.SQL.Add('gr.Codigo CODIGOGRUPO, gr.Nome NOMEGRUPO,');
  QrLCS.SQL.Add('cj.Codigo CODIGOCONJUNTO, cj.Nome NOMECONJUNTO,');
  QrLCS.SQL.Add('pl.Codigo CODIGOPLANO, pl.Nome NOMEPLANO,');
  QrLCS.SQL.Add('');


  QrLCS.SQL.Add('pls.SdoAtu PL_ATU, pls.SdoFut PL_FUT,');
  QrLCS.SQL.Add('cjs.SdoAtu CJ_ATU, cjs.SdoFut CJ_FUT,');
  QrLCS.SQL.Add('grs.SdoAtu GR_ATU, grs.SdoFut GR_FUT,');
  QrLCS.SQL.Add('sgs.SdoAtu SG_ATU, sgs.SdoFut SG_FUT,');
  QrLCS.SQL.Add('cos.SdoAtu CO_ATU, cos.SdoFut CO_FUT,');

  QrLCS.SQL.Add('pl.CtrlaSdo PL_CS, cj.CtrlaSdo CJ_CS,');
  QrLCS.SQL.Add('gr.CtrlaSdo GR_CS, sg.CtrlaSdo SG_CS,');
  QrLCS.SQL.Add('co.CtrlaSdo CO_CS');

  QrLCS.SQL.Add('FROM plano pl');
  QrLCS.SQL.Add('LEFT JOIN conjuntos cj ON pl.Codigo=cj.Plano');
  QrLCS.SQL.Add('LEFT JOIN grupos    gr ON cj.Codigo=gr.Conjunto');
  QrLCS.SQL.Add('LEFT JOIN subgrupos sg ON gr.Codigo=sg.Grupo');
  QrLCS.SQL.Add('LEFT JOIN contas    co ON sg.Codigo=co.SubGrupo');

  QrLCS.SQL.Add('LEFT JOIN planosdo  pls ON pls.Codigo=pl.Codigo AND pls.Entidade=' + EdCliInt.Text);
  QrLCS.SQL.Add('LEFT JOIN conjunsdo cjs ON cjs.Codigo=cj.Codigo AND cjs.Entidade=' + EdCliInt.Text);
  QrLCS.SQL.Add('LEFT JOIN grupossdo grs ON grs.Codigo=gr.Codigo AND grs.Entidade=' + EdCliInt.Text);
  QrLCS.SQL.Add('LEFT JOIN subgrusdo sgs ON sgs.Codigo=sg.Codigo AND sgs.Entidade=' + EdCliInt.Text);
  QrLCS.SQL.Add('LEFT JOIN contassdo cos ON cos.Codigo=co.Codigo AND cos.Entidade=' + EdCliInt.Text);

  QrLCS.SQL.Add('WHERE (co.Ativo in ('+Ativo + ')');
  //if CBSubgrupos.KeyValue <> NULL then
    //QrLCS.SQL.Add('AND co.Subgrupo='''+IntToStr(CBSubgrupos.KeyValue)+'''');
  if CkContasUser.Checked then //and CkSaldos.Checked then
  begin
    if CkJoin.Checked then
      QrLCS.SQL.Add('AND co.Codigo>0')
    else
      QrLCS.SQL.Add('AND (co.Codigo>0 OR co.Codigo IS NULL)');
  end;
  if CkCtasPosi.Checked then
    QrLCS.SQL.Add('AND co.Codigo>0');

  QrLCS.SQL.Add('ORDER BY cj.Codigo, gr.Codigo, sg.Codigo, co.Codigo, pl.Codigo');
  //
  UnDmkDAC_PF.AbreQuery(QrLCS, Dmod.MyDB);
  //
  MyObjects.frxDefineDataSets(frxLCS, [
    DModG.frxDsDono,
    frxDsLCS
    ], False);
  //
  MyObjects.frxMostra(frxLCS, 'Plano de Contas com Saldos');
end;

procedure TFmContasSdoImp.ListaContasTotais();
var
  Ini_TXT, Fim_TXT, CampoData: String;
  I, ID, Itens: Integer;
begin
  if MyObjects.FIC(FEntidade = 0, EdCliInt, 'Informe a empresa!') then Exit;
  //
  //MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando');
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando lan�amentos do per�odo');
  //
  case RGCampoData.ItemIndex of
    0: CampoData := 'DataDoc';
    1: CampoData := 'Data';
    2: CampoData := 'Vencimento';
    else
    begin
      Geral.MB_Info('Campo de data n�o implementado!');
      CampoData := '??dt??';
    end;
  end;
  //
  Ini_TXT := Geral.FDT(TPDataIni.Date, 1);
  Fim_TXT := Geral.FDT(TPDataFim.Date, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTotais, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _fin_lan_sum_period_mov_',
  ';',
  'CREATE TABLE _fin_lan_sum_period_mov_',
  'SELECT lan.Genero, ',
  'SUM(lan.Debito)DEBITO, SUM(lan.Credito) CREDITO',
  'FROM ' + FTabLctA + ' lan ',
  'WHERE lan.Genero>0 ',
  'AND lan.ID_Pgto = 0',
  'AND lan.' + CampoData + '  BETWEEN "' + Ini_TXT + '" AND "' + Fim_TXT + ' 23:59:59"',
  'AND lan.CliInt=-11',
  'GROUP BY lan.Genero',
  ';',
  '',
  'DROP TABLE IF EXISTS _fin_lan_sum_period_all_',
  ';',
  'CREATE TABLE _fin_lan_sum_period_all_',
  'SELECT mov.CREDITO, mov.DEBITO, cta.Codigo Genero, cta.SubGrupo, ',
  'cta.Nome NO_CTA, sgr.Nome NO_SGR, sgr.Grupo, gru.Nome NO_GRU, ',
  'gru.Conjunto, cjt.Nome NO_CJT, cjt.Plano, pla.Nome NO_PLA,',
  'pla.OrdemLista pla_OrdemLista, cjt.OrdemLista cjt_OrdemLista, ',
  'gru.OrdemLista gru_OrdemLista, sgr.OrdemLista sgr_OrdemLista, ',
  'cta.OrdemLista cta_OrdemLista',
  'FROM ' + TMeuDB + '.contas cta',
  'LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=cta.Subgrupo',
  'LEFT JOIN ' + TMeuDB + '.grupos gru ON gru.Codigo=sgr.grupo',
  'LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto',
  'LEFT JOIN ' + TMeuDB + '.plano pla ON pla.Codigo=cjt.Plano',
  'LEFT JOIN _fin_lan_sum_period_mov_ mov ON mov.Genero=cta.Codigo',
  'WHERE pla.Codigo > 0',
  'AND cjt.Codigo>0 ',
  'AND gru.Codigo>0',
  'AND sgr.Codigo>0',
  'AND (',
  '  cta.Codigo>0',
  '  OR',
  '  cta.Codigo<-10',
  ') AND (',
  '  cta.Ativo<>0',
  '  OR',
  '  mov.CREDITO <> 0',
  '  OR',
  '  mov.DEBITO <> 0',
  ')',
  ';',
  'UPDATE _fin_lan_sum_period_all_',
  'SET CREDITO = 0.000 WHERE CREDITO IS NULL',
  ';',
  'UPDATE _fin_lan_sum_period_all_',
  'SET DEBITO = 0.000 WHERE DEBITO IS NULL',
  ';',
  '',
  'SELECT * ',
  'FROM _fin_lan_sum_period_all_',
  'ORDER BY pla_OrdemLista, No_PLA, cjt_OrdemLista, No_CJT, ',
  'gru_OrdemLista, No_GRU, sgr_OrdemLista, No_SGR, ',
  'cta_OrdemLista, No_CTA',
  ';',
{
  'DROP TABLE IF EXISTS _fin_lan_sum_period_',
  ';',
  '',
  'CREATE TABLE _fin_lan_sum_period_',
  'SELECT lan.Genero, ',
  'SUM(lan.Debito) DEBITO, SUM(lan.Credito) CREDITO',
  'FROM ' + FTabLctA + ' lan ',
  'WHERE lan.Genero>0 ',
  'AND lan.ID_Pgto = 0',
  'AND lan.DataDoc  BETWEEN "' + Ini_TXT + '" AND "' + Fim_TXT + ' 23:59:59"',
  'AND lan.CliInt=-11',
  'GROUP BY lan.Genero',
  ';',
  '',
  'SELECT mov.CREDITO, mov.DEBITO, cta.Codigo Genero, cta.SubGrupo, ',
  'cta.Nome NO_CTA, sgr.Nome NO_SGR, sgr.Grupo, gru.Nome NO_GRU, ',
  'gru.Conjunto, cjt.Nome NO_CJT, cjt.Plano, pla.Nome NO_PLA ',
  'FROM ' + TMeuDB + '.contas cta',
  'LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=cta.Subgrupo',
  'LEFT JOIN ' + TMeuDB + '.grupos gru ON gru.Codigo=sgr.grupo',
  'LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto',
  'LEFT JOIN ' + TMeuDB + '.plano pla ON pla.Codigo=cjt.Plano',
  'LEFT JOIN _fin_lan_sum_period_ mov ON mov.Genero=cta.Codigo',
  'WHERE pla.Codigo > 0',
  'AND cjt.Codigo>0 ',
  'AND gru.Codigo>0',
  'AND sgr.Codigo>0',
  'AND (',
  '  cta.Codigo>0',
  '  OR',
  '  cta.Codigo<-10',
  ') AND (',
  '  cta.Ativo<>0',
  '  OR',
  '  mov.CREDITO <> 0',
  '  OR',
  '  mov.DEBITO <> 0',
  ')',
  'ORDER BY pla.OrdemLista, pla.Nome, cjt.OrdemLista, cjt.Nome, ',
  'gru.OrdemLista, gru.Nome, sgr.OrdemLista, sgr.Nome, ',
  'cta.OrdemLista, cta.Nome',
  ';',
}
  EmptyStr]);

  ObtemTotaisNivel('Subgrupos', 'Subgrupo', FArrSGR);
  ObtemTotaisNivel('Grupos'   , 'Grupo'   , FArrGRU);
  ObtemTotaisNivel('Conjuntos', 'Conjunto', FArrCJT);
  ObtemTotaisNivel('Planos'   , 'Plano'   , FArrPLA);

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando relat�rio');
  MyObjects.frxDefineDataSets(frxFIN_PLCTA_007, [
    DModG.frxDsDono,
    frxDsTotais
    ], False);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Mostrando relat�rio');
  MyObjects.frxMostra(frxFIN_PLCTA_007, 'Plano de Contas com Totais');
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
end;

procedure TFmContasSdoImp.ObtemTotaisNivel(const Nivel, Campo: String;
  var ArrNivel: TArr2Double);
var
  I, ID, Itens: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo saldos de n�veis: ' + Nivel);
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqTotais, DmodG.MyPID_CompressDB, [
  'SELECT ' + Campo + ' ID, SUM(Credito) Credito, SUM(Debito) Debito ',
  'FROM _fin_lan_sum_period_all_ ',
  'GROUP BY ID ',
  'ORDER BY ID DESC ',
  EmptyStr]);
  //
  DqTotais.First;
  Itens := USQLDB.Int(DqTotais, 'ID') + 1;
  SetLength(ArrNivel, Itens);
  for I := 0 to Itens - 1 do
  begin
    ArrNivel[I][CO_ID_C] := 0;
    ArrNivel[I][CO_ID_D] := 0;
  end;
  while not DqTotais.Eof do
  begin
    ID := USQLDB.Int(DqTotais, 'ID');
    ArrNivel[ID][CO_ID_C] := USQLDB.Flu(DqTotais, 'Credito');
    ArrNivel[ID][CO_ID_D] := USQLDB.Flu(DqTotais, 'Debito');
    //
    DqTotais.Next;
  end;
end;

procedure TFmContasSdoImp.QrLCSCalcFields(DataSet: TDataSet);
  function DFS(Status: Integer; ValVerdade: Double): String;
  var
    Verdade: Boolean;
  begin
    Verdade :=  (Status = 0) and (CkOculta.Checked (*or not CkSaldos.Checked*));
    Result := dmkPF.EscolhaDe2Str(Verdade, 'n/i',
      Geral.FFT(ValVerdade, 2, siNegativo));
  end;
begin
  QrLCSPL_ATU_TXT.Value := DFS(QrLCSPL_CS.Value, QrLCSPL_ATU.Value);
  QrLCSPL_FUT_TXT.Value := DFS(QrLCSPL_CS.Value, QrLCSPL_FUT.Value);
  //
  QrLCSCJ_ATU_TXT.Value := DFS(QrLCSCJ_CS.Value, QrLCSCJ_ATU.Value);
  QrLCSCJ_FUT_TXT.Value := DFS(QrLCSCJ_CS.Value, QrLCSCJ_FUT.Value);
  //
  QrLCSGR_ATU_TXT.Value := DFS(QrLCSGR_CS.Value, QrLCSGR_ATU.Value);
  QrLCSGR_FUT_TXT.Value := DFS(QrLCSGR_CS.Value, QrLCSGR_FUT.Value);
  //
  QrLCSSG_ATU_TXT.Value := DFS(QrLCSSG_CS.Value, QrLCSSG_ATU.Value);
  QrLCSSG_FUT_TXT.Value := DFS(QrLCSSG_CS.Value, QrLCSSG_FUT.Value);
  //
  QrLCSCO_ATU_TXT.Value := DFS(QrLCSCO_CS.Value, QrLCSCO_ATU.Value);
  QrLCSCO_FUT_TXT.Value := DFS(QrLCSCO_CS.Value, QrLCSCO_FUT.Value);
  //
end;

procedure TFmContasSdoImp.TCSaldosChange(Sender: TObject);
begin
  PnCfgPeriodo.Enabled := TCSaldos.TabIndex = 1;
(*
  LaDataIni.Enabled := TCSaldos.TabIndex = 1;
  LaDataFim.Enabled := TCSaldos.TabIndex = 1;
  TPDataIni.Enabled := TCSaldos.TabIndex = 1;
  TPDataFim.Enabled := TCSaldos.TabIndex = 1;
*)
end;

procedure TFmContasSdoImp.frxLCSGetValue(const VarName: String;
  var Value: Variant);
var
  IsOK: Boolean;
begin
  if AnsiCompareText(VarName, 'VARF_ENTICONTA') = 0 then
    Value := CBCliInt.Text
  else if AnsiCompareText(VarName, 'VARF_NAO_OCULTA') = 0 then
    Value := not CkControla.Checked (*or not CkSaldos.Checked*)
  else if AnsiCompareText(VarName, 'VARF_NOME_PERFIL') = 0 then
  begin
    if (EdCtaCfgCab.ValueVariant = 0)
    or (CkInfoNomeCtaCfg.Checked = False) then
      Value := ''
    else
      Value := CBCtaCfgCab.Text;
  end
  else if AnsiCompareText(VarName, 'VARF_DATA') = 0 then
    Value := Now()
///
/////////////////////// T O T A I S ////////////////////////////////////////////
///
///  Planos
  else if AnsiCompareText(VarName, 'VARF_PLA_C') = 0 then
    Value := FArrPLA[QrTotaisPlano.Value][CO_ID_C]
  else if AnsiCompareText(VarName, 'VARF_PLA_D') = 0 then
    Value := FArrPLA[QrTotaisPlano.Value][CO_ID_D]
  else if AnsiCompareText(VarName, 'VARF_PLA_R') = 0 then
    Value := FArrPLA[QrTotaisPlano.Value][CO_ID_C] -
             FArrPLA[QrTotaisPlano.Value][CO_ID_D]
///
///  Conjuntos
  else if AnsiCompareText(VarName, 'VARF_CJT_C') = 0 then
    Value := FArrCJT[QrTotaisConjunto.Value][CO_ID_C]
  else if AnsiCompareText(VarName, 'VARF_CJT_D') = 0 then
    Value := FArrCJT[QrTotaisConjunto.Value][CO_ID_D]
  else if AnsiCompareText(VarName, 'VARF_CJT_R') = 0 then
    Value := FArrCJT[QrTotaisConjunto.Value][CO_ID_C] -
             FArrCJT[QrTotaisConjunto.Value][CO_ID_D]
///
///  Grupos
  else if AnsiCompareText(VarName, 'VARF_GRU_C') = 0 then
    Value := FArrGRU[QrTotaisGrupo.Value][CO_ID_C]
  else if AnsiCompareText(VarName, 'VARF_GRU_D') = 0 then
    Value := FArrGRU[QrTotaisGrupo.Value][CO_ID_D]
  else if AnsiCompareText(VarName, 'VARF_GRU_R') = 0 then
    Value := FArrGRU[QrTotaisGrupo.Value][CO_ID_C] -
             FArrGRU[QrTotaisGrupo.Value][CO_ID_D]
///
///  subgrupos
  else if AnsiCompareText(VarName, 'VARF_SGR_C') = 0 then
    Value := FArrSGR[QrTotaisSubgrupo.Value][CO_ID_C]
  else if AnsiCompareText(VarName, 'VARF_SGR_D') = 0 then
    Value := FArrSGR[QrTotaisSubgrupo.Value][CO_ID_D]
  else if AnsiCompareText(VarName, 'VARF_SGR_R') = 0 then
    Value := FArrSGR[QrTotaisSubgrupo.Value][CO_ID_C] -
             FArrSGR[QrTotaisSubgrupo.Value][CO_ID_D]
///
  else if AnsiCompareText(VarName, 'VARF_NO_EMPRESA') = 0 then
    Value := CBCliInt.Text
  else if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then
    Value := 'Per�odo por data do ' + RGCampoData.Items[RGCampoData.ItemIndex] + ': ' +
    dmkPF.PeriodoImp2(TPDataIni.Date, TPDataFim.Date, True, True, '', '', '')
end;

procedure TFmContasSdoImp.CkMostraSaldosClick(Sender: TObject);
begin
  GBSaldos.Visible := CkMostraSaldos.Checked;
end;

procedure TFmContasSdoImp.EdCliIntRedefinido(Sender: TObject);
begin
  FEntidade := DModG.QrEmpresasCodigo.Value;
  FEntidTXT := FormatFloat('0', FEntidade);
  FEmpresa  := DModG.QrEmpresasFilial.Value;
  FEmprTXT  := FormatFloat('0', FEmpresa);
  //
  DModG.Def_EM_ABD(TMeuDB,
    FEntidade, FEmpresa, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
end;

procedure TFmContasSdoImp.ListaContasLista();
var
  Ativo, SQL_Ativo, SQL_QuaisCods, SQL_CtaPosi, SQL_CtaCfgCab, SQL_ORDER: String;
  CtaCfgCab: Integer;
begin
  //fazer saldos de contas
  //so mostrar saldos se no cadastro de contas, sbgru, grupos etc. pedir
  CtaCfgCab := EdCtaCfgCab.ValueVariant;
  //
  if CkAtivos.Checked then
    Ativo := ' 1 '
  else
    Ativo := '0, 1';
  //
  if CkJoin.Checked then
    Ativo := Ativo + ')'
  else
    Ativo := Ativo + ') OR (co.Ativo IS NULL)';
  //
  SQL_Ativo := 'WHERE (co.Ativo in (' + Ativo + ')';
(*
  QrLCL.Close;
  QrLCL.SQL.Clear;

  QrLCL.SQL.Add('SELECT co.Codigo CODIGOCONTA, co.Nome NOMECONTA,');
  QrLCL.SQL.Add('sg.Codigo CODIGOSUBGRUPO, sg.Nome NOMESUBGRUPO,');
  QrLCL.SQL.Add('gr.Codigo CODIGOGRUPO, gr.Nome NOMEGRUPO,');
  QrLCL.SQL.Add('cj.Codigo CODIGOCONJUNTO, cj.Nome NOMECONJUNTO,');
  QrLCL.SQL.Add('pl.Codigo CODIGOPLANO, pl.Nome NOMEPLANO, ');
  QrLCL.SQL.Add('co.Debito, co.Credito, co.CtrlaSdo, co.Ativo ');
  QrLCL.SQL.Add('');

  QrLCL.SQL.Add('FROM plano pl');
  QrLCL.SQL.Add('LEFT JOIN conjuntos cj ON pl.Codigo=cj.Plano');
  QrLCL.SQL.Add('LEFT JOIN grupos    gr ON cj.Codigo=gr.Conjunto');
  QrLCL.SQL.Add('LEFT JOIN subgrupos sg ON gr.Codigo=sg.Grupo');
  QrLCL.SQL.Add('LEFT JOIN contas    co ON sg.Codigo=co.SubGrupo');

  QrLCL.SQL.Add('WHERE (co.Ativo in ('+Ativo + ')');

  if CkContasUser.Checked then //and CkSaldos.Checked then
  begin
    if CkJoin.Checked then
      QrLCL.SQL.Add('AND co.Codigo>0')
    else
      QrLCL.SQL.Add('AND (co.Codigo>0 OR co.Codigo IS NULL)');
  end;
  if CkCtasPosi.Checked then
    QrLCL.SQL.Add('AND co.Codigo>0');

  if CtaCfgCab <> 0 then
  begin
    QrLCL.SQL.Add('AND co.Codigo IN ( ');
    QrLCL.SQL.Add('     SELECT Genero ');
    QrLCL.SQL.Add('     FROM ctacfgits ');
    QrLCL.SQL.Add('     WHERE Codigo=' + Geral.FF0(CtaCfgCab) + ') ');
  end;
  QrLCL.SQL.Add('ORDER BY cj.Codigo, gr.Codigo, sg.Codigo, co.Codigo, pl.Codigo');
  //
  UnDmkDAC_PF.AbreQuery(QrLCL, Dmod.MyDB);
  //Geral.MB_Teste(QrLCL.SQL.Text);
*)
  if CkContasUser.Checked then //and CkSaldos.Checked then
  begin
    if CkJoin.Checked then
      SQL_QuaisCods := 'AND co.Codigo > 0'
    else
      SQL_QuaisCods := 'AND (co.Codigo > 0 OR co.Codigo IS NULL)';
  end else
    SQL_QuaisCods := '';
  //
  if CkCtasPosi.Checked then
    SQL_CtaPosi := 'AND co.Codigo > 0'
  else
    SQL_CtaPosi := '';
  //
  if CtaCfgCab <> 0 then
  begin
    SQL_CtaCfgCab := Geral.ATS([
    'AND co.Codigo IN ( ',
    '     SELECT Genero ',
    '     FROM ctacfgits ',
    '     WHERE Codigo=' + Geral.FF0(CtaCfgCab) + ') '
    ]);
  end else
    SQL_CtaCfgCab := '';
  //
  //SQL_ORDER := 'ORDER BY cj.Codigo, gr.Codigo, sg.Codigo, co.Codigo, pl.Codigo';
  SQL_ORDER := 'ORDER BY pl.FinContab, pl.OrdemLista, pl.Codigo, ' +
  'cj.OrdemLista, cj.Codigo, gr.OrdemLista, gr.Codigo, ' +
  'sg.OrdemLista, sg.Codigo, co.OrdemLista, co.Nome';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLCL, Dmod.MyDB, [
  'SELECT co.Codigo CODIGOCONTA, co.OrdemLista ORD_CTA, co.Nome NOMECONTA, ',
  'sg.Codigo CODIGOSUBGRUPO, sg.OrdemLista ORD_SGR, sg.Nome NOMESUBGRUPO, ',
  'gr.Codigo CODIGOGRUPO, gr.OrdemLista ORD_GRU, gr.Nome NOMEGRUPO, ',
  'cj.Codigo CODIGOCONJUNTO, cj.OrdemLista ORD_CJT, cj.Nome NOMECONJUNTO, ',
  'pl.Codigo CODIGOPLANO, pl.FinContab, pl.Nome NOMEPLANO, ',
  'co.Debito, co.Credito, co.CtrlaSdo, co.Ativo ',
  'FROM plano pl ',
  'LEFT JOIN conjuntos cj ON pl.Codigo=cj.Plano ',
  'LEFT JOIN grupos    gr ON cj.Codigo=gr.Conjunto ',
  'LEFT JOIN subgrupos sg ON gr.Codigo=sg.Grupo ',
  'LEFT JOIN contas    co ON sg.Codigo=co.SubGrupo ',
  SQL_Ativo,
  SQL_QuaisCods,
  SQL_CtaPosi,
  SQL_CtaCfgCab,
  SQL_ORDER,
  '']);
  //
  MyObjects.frxDefineDataSets(frxLCL, [
    DModG.frxDsDono,
    frxDsLCL
    ], False);
  //
  MyObjects.frxMostra(frxLCL, 'Lista de Planos de Contas');
end;

end.

