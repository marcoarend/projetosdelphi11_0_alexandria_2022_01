object FmContasConfPgto: TFmContasConfPgto
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-030 :: Contas Mensais - Confer'#234'ncia de Pagamentos'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 332
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaMes: TLabel
        Left = 496
        Top = 2
        Width = 23
        Height = 13
        Caption = 'M'#234's:'
      end
      object LaAno: TLabel
        Left = 680
        Top = 2
        Width = 22
        Height = 13
        Caption = 'Ano:'
      end
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object CBMes: TComboBox
        Left = 496
        Top = 19
        Width = 182
        Height = 21
        Color = clWhite
        DropDownCount = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Text = 'CBMes'
      end
      object CBAno: TComboBox
        Left = 680
        Top = 19
        Width = 90
        Height = 21
        Color = clWhite
        DropDownCount = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        Text = 'CBAno'
      end
      object EdCliInt: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 425
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECLI'
        ListSource = DsCliInt
        TabOrder = 1
        dmkEditCB = EdCliInt
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GradeDados: TDBGrid
      Left = 0
      Top = 65
      Width = 1008
      Height = 267
      Align = alClient
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = GradeDadosDrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'Conta'
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeCta'
          Title.Caption = 'Descri'#231#227'o da conta (do plano de contas)'
          Width = 274
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdeMin'
          Title.Caption = 'Qtde. m'#237'n.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdeMax'
          Title.Caption = 'Qtde. m'#225'x.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdeExe'
          Title.Caption = 'Qtde. real.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValrMin'
          Title.Caption = 'Valor m'#237'n.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValrMax'
          Title.Caption = 'Valor m'#225'x.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValrExe'
          Title.Caption = 'Valor real.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESTATUS'
          Title.Caption = 'Avalia'#231#227'o'
          Width = 285
          Visible = True
        end>
    end
    object PB1: TProgressBar
      Left = 0
      Top = 48
      Width = 1008
      Height = 17
      Align = alTop
      TabOrder = 2
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 565
        Height = 32
        Caption = 'Contas Mensais - Confer'#234'ncia de Pagamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 565
        Height = 32
        Caption = 'Contas Mensais - Confer'#234'ncia de Pagamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 565
        Height = 32
        Caption = 'Contas Mensais - Confer'#234'ncia de Pagamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 380
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 424
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtIncluiClick
      end
    end
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NOMECLI'
      'FROM entidades'
      'WHERE CliInt>0'
      'ORDER BY NOMECLI')
    Left = 572
    Top = 12
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 600
    Top = 12
  end
  object QrConfPgtos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cp.*, co.Nome NOMECONTA '
      'FROM confpgtos cp'#13
      'LEFT JOIN contas co ON co.Codigo=cp.Codigo'#10
      ''
      'WHERE CliInt=:P0'
      '')
    Left = 632
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConfPgtosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrConfPgtosNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrConfPgtosQtdeMin: TIntegerField
      FieldName = 'QtdeMin'
      Required = True
    end
    object QrConfPgtosQtdeMax: TIntegerField
      FieldName = 'QtdeMax'
      Required = True
    end
    object QrConfPgtosValrMin: TFloatField
      FieldName = 'ValrMin'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrConfPgtosValrMax: TFloatField
      FieldName = 'ValrMax'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrConfPgtosCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrConfPgtosDiaAlert: TSmallintField
      FieldName = 'DiaAlert'
    end
    object QrConfPgtosDiaVence: TSmallintField
      FieldName = 'DiaVence'
    end
  end
  object DataSource1: TDataSource
    DataSet = Query
    Left = 40
    Top = 12
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 664
    Top = 12
    object QrPesqQtde: TLargeintField
      FieldName = 'Qtde'
      Required = True
    end
    object QrPesqDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object Query: TMySQLQuery
    AfterOpen = QueryAfterOpen
    BeforeClose = QueryBeforeClose
    OnCalcFields = QueryCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM _conf_pgtos_'
      'ORDER BY Status')
    Left = 12
    Top = 12
    object QueryControle: TIntegerField
      FieldName = 'Controle'
    end
    object QueryEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QueryEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QueryGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QueryNomeCta: TWideStringField
      FieldName = 'NomeCta'
      Size = 100
    end
    object QueryDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QueryQtdeMin: TIntegerField
      FieldName = 'QtdeMin'
    end
    object QueryQtdeMax: TIntegerField
      FieldName = 'QtdeMax'
    end
    object QueryQtdeExe: TIntegerField
      FieldName = 'QtdeExe'
    end
    object QueryValrMin: TFloatField
      FieldName = 'ValrMin'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QueryValrMax: TFloatField
      FieldName = 'ValrMax'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QueryValrExe: TFloatField
      FieldName = 'ValrExe'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QueryDiaAlert: TSmallintField
      FieldName = 'DiaAlert'
      DisplayFormat = '0;-0; '
    end
    object QueryDiaVence: TSmallintField
      FieldName = 'DiaVence'
      DisplayFormat = '0;-0; '
    end
    object QueryStatus: TIntegerField
      FieldName = 'Status'
    end
    object QueryNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 100
      Calculated = True
    end
  end
end
