unit ContasNivSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, UnDmkProcFunc,
  dmkDBGrid, Db, mySQLDbTables, Variants, dmkImage, UnDMkEnums;

type
  TFmContasNivSel = class(TForm)
    Panel1: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    QrSdoNiveis: TmySQLQuery;
    DsSdoNiveis: TDataSource;
    QrSdoNiveisNivel: TIntegerField;
    QrSdoNiveisGenero: TIntegerField;
    QrSdoNiveisNomeGe: TWideStringField;
    QrSdoNiveisNomeNi: TWideStringField;
    QrSdoNiveisSumMov: TFloatField;
    QrSdoNiveisSdoAnt: TFloatField;
    QrSdoNiveisSumCre: TFloatField;
    QrSdoNiveisSumDeb: TFloatField;
    QrSdoNiveisSdoFim: TFloatField;
    QrSdoNiveisSeleci: TSmallintField;
    QrSdoNiveisCtrla: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    CkControla: TCheckBox;
    CkSeleci: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure CkControlaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenSdoNiveis(Nivel, Genero: Integer);
  public
    { Public declarations }
    FTabela: String;
  end;

  var
  FmContasNivSel: TFmContasNivSel;

implementation

uses UnMyObjects, Module, UnFinanceiro, ContasNiv, dmkGeral, UMySQLModule,
  ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmContasNivSel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasNivSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasNivSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasNivSel.FormShow(Sender: TObject);
begin
  ReopenSdoNiveis(0, 0);
end;

procedure TFmContasNivSel.ReopenSdoNiveis(Nivel, Genero: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSdoNiveis, DModg.MyPID_DB, [
    'SELECT * ',
    'FROM ' + FTabela,
    'WHERE Seleci>' + Geral.FF0(dmkPF.BoolToInt2(CkSeleci.Checked, 0, -1)),
    'AND Ctrla>' + Geral.FF0(dmkPF.BoolToInt2(CkControla.Checked, 0, -1)),
    '']);
  //
  QrSdoNiveis.Locate('Nivel;Genero', VarArrayOf([Nivel,Genero]), [])
end;

procedure TFmContasNivSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmContasNivSel.dmkDBGrid1CellClick(Column: TColumn);
var
  Seleci: Integer;
begin
  if (dmkDBGrid1.SelectedField.Name = 'QrSdoNiveisSeleci') then
  begin
    if QrSdoNiveisSeleci.Value = 0 then
      Seleci := 1
    else
      Seleci := 0;
    //
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE ' + FTabela + ' SET Seleci=:P0');
    DModG.QrUpdPID1.SQL.Add('WHERE Nivel=:P1');
    DModG.QrUpdPID1.SQL.Add('AND Genero=:P2');
    //
    DModG.QrUpdPID1.Params[00].AsInteger := Seleci;
    DModG.QrUpdPID1.Params[01].AsInteger := QrSdoNiveisNivel.Value;
    DModG.QrUpdPID1.Params[02].AsInteger := QrSdoNiveisGenero.Value;
    DModG.QrUpdPID1.ExecSQL;
    //
    ReopenSdoNiveis(QrSdoNiveisNivel.Value, QrSdoNiveisGenero.Value);
  end;
end;

procedure TFmContasNivSel.CkControlaClick(Sender: TObject);
begin
  ReopenSdoNiveis(QrSdoNiveisNivel.Value, QrSdoNiveisGenero.Value);
end;

procedure TFmContasNivSel.BtOKClick(Sender: TObject);
begin
  QrSdoNiveis.First;
  while not QrSdoNiveis.Eof do
  begin
    if QrSdoNiveisSeleci.Value = 1 then
    begin
      UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, stIns, 'contasniv', False, [],
      ['Entidade', 'Nivel', 'Genero'], [], [
        FmContasNiv.QrEmpresasCodigo.Value,
        QrSdoNiveisNivel.Value, QrSdoNiveisGenero.Value], True);
    end;
    QrSdoNiveis.Next;
  end;
  FmContasNiv.ReopenContasNiv(QrSdoNiveisNivel.Value, QrSdoNiveisGenero.Value);
  Close;
end;

end.
