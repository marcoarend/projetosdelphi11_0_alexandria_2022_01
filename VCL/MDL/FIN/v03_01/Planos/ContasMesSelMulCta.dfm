object FmContasMesSelMulCta: TFmContasMesSelMulCta
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-028 :: Previs'#245'es Mensais - M'#250'ltipla Sele'#231#227'o de Contas'
  ClientHeight = 494
  ClientWidth = 702
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 702
    Height = 332
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitWidth = 646
    ExplicitHeight = 398
    object GradeContasMes: TdmkDBGridDAC
      Left = 0
      Top = 0
      Width = 702
      Height = 332
      SQLFieldsToChange.Strings = (
        'Ativo')
      SQLIndexesOnUpdate.Strings = (
        'Codigo')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = '?'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodUsu'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Motivo da sa'#237'da'
          Width = 574
          Visible = True
        end>
      Color = clWindow
      DataSource = DsContasMes
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      SQLTable = 'cad_0'
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = '?'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodUsu'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Motivo da sa'#237'da'
          Width = 574
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 702
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = -138
    ExplicitWidth = 784
    object GB_R: TGroupBox
      Left = 654
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 736
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 606
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 688
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 586
        Height = 32
        Caption = 'Previs'#245'es Mensais - M'#250'ltipla Sele'#231#227'o de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 586
        Height = 32
        Caption = 'Previs'#245'es Mensais - M'#250'ltipla Sele'#231#227'o de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 586
        Height = 32
        Caption = 'Previs'#245'es Mensais - M'#250'ltipla Sele'#231#227'o de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 380
    Width = 702
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitLeft = -82
    ExplicitTop = 378
    ExplicitWidth = 784
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 698
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 780
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 228
        Height = 16
        Caption = 'Somente contas mensais s'#227'o exibidas!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 228
        Height = 16
        Caption = 'Somente contas mensais s'#227'o exibidas!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 424
    Width = 702
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitLeft = -82
    ExplicitTop = 422
    ExplicitWidth = 784
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 698
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 636
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 221
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Todos'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtTodosClick
        NumGlyphs = 2
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 333
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Nenhum'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtNenhumClick
        NumGlyphs = 2
      end
      object Panel3: TPanel
        Left = 587
        Top = 0
        Width = 111
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        ExplicitLeft = 534
        ExplicitTop = 1
        ExplicitHeight = 46
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
      end
    end
  end
  object QrContasMes: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM cad_0'
      'ORDER BY Nome')
    Left = 36
    Top = 120
    object QrContasMesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasMesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrContasMesNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrContasMesAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsContasMes: TDataSource
    DataSet = QrContasMes
    Left = 64
    Top = 120
  end
end
