unit ContasPlanoLista;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker,  UnInternalConsts,
  dmkImage, dmkDBGrid, Clipbrd, Variants, dmkDBGridZTO, UnDmkEnums, Vcl.Mask,
  UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmContasPlanoLista = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    TbContas: TmySQLTable;
    DsContas: TDataSource;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    PnDados: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGContas: TdmkDBGridZTO;
    Panel5: TPanel;
    LbItensMD: TListBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnEdita: TPanel;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel8: TPanel;
    BtDesiste: TBitBtn;
    TbContasCodigo: TIntegerField;
    TbContasNome: TWideStringField;
    TbContasNome2: TWideStringField;
    TbContasNome3: TWideStringField;
    TbContasID: TWideStringField;
    TbContasSubgrupo: TIntegerField;
    TbContasCentroCusto: TIntegerField;
    TbContasEmpresa: TIntegerField;
    TbContasCredito: TWideStringField;
    TbContasDebito: TWideStringField;
    TbContasMensal: TWideStringField;
    TbContasExclusivo: TWideStringField;
    TbContasMensdia: TSmallintField;
    TbContasMensdeb: TFloatField;
    TbContasMensmind: TFloatField;
    TbContasMenscred: TFloatField;
    TbContasMensminc: TFloatField;
    TbContasTerceiro: TIntegerField;
    TbContasExcel: TWideStringField;
    TbContasRateio: TIntegerField;
    TbContasEntidade: TIntegerField;
    TbContasAntigo: TWideStringField;
    TbContasPendenMesSeg: TSmallintField;
    TbContasCalculMesSeg: TSmallintField;
    TbContasOrdemLista: TIntegerField;
    TbContasContasAgr: TIntegerField;
    TbContasContasSum: TIntegerField;
    TbContasCtrlaSdo: TSmallintField;
    TbContasNotPrntBal: TIntegerField;
    TbContasSigla: TWideStringField;
    TbContasProvRat: TSmallintField;
    TbContasNotPrntFin: TIntegerField;
    TbContasTemDocFisi: TSmallintField;
    TbContasLk: TIntegerField;
    TbContasDataCad: TDateField;
    TbContasDataAlt: TDateField;
    TbContasUserCad: TIntegerField;
    TbContasUserAlt: TIntegerField;
    TbContasAlterWeb: TSmallintField;
    TbContasAtivo: TSmallintField;
    TbContasCentroRes: TSmallintField;
    TbContasPagRec: TSmallintField;
    GroupBox7: TGroupBox;
    Panel11: TPanel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Panel7: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    TBTam: TTrackBar;
    DBGrid2: TDBGrid;
    QrConta: TmySQLQuery;
    DsConta: TDataSource;
    QrContaCodigo: TIntegerField;
    QrContaNome: TWideStringField;
    QrContaNome2: TWideStringField;
    QrContaNome3: TWideStringField;
    QrContaID: TWideStringField;
    QrContaSubgrupo: TIntegerField;
    QrContaCentroCusto: TIntegerField;
    QrContaEmpresa: TIntegerField;
    QrContaCredito: TWideStringField;
    QrContaDebito: TWideStringField;
    QrContaMensal: TWideStringField;
    QrContaExclusivo: TWideStringField;
    QrContaMensdia: TSmallintField;
    QrContaMensdeb: TFloatField;
    QrContaMensmind: TFloatField;
    QrContaMenscred: TFloatField;
    QrContaMensminc: TFloatField;
    QrContaTerceiro: TIntegerField;
    QrContaExcel: TWideStringField;
    QrContaRateio: TIntegerField;
    QrContaEntidade: TIntegerField;
    QrContaAntigo: TWideStringField;
    QrContaPendenMesSeg: TSmallintField;
    QrContaCalculMesSeg: TSmallintField;
    QrContaOrdemLista: TIntegerField;
    QrContaContasAgr: TIntegerField;
    QrContaContasSum: TIntegerField;
    QrContaCtrlaSdo: TSmallintField;
    QrContaNotPrntBal: TIntegerField;
    QrContaSigla: TWideStringField;
    QrContaProvRat: TSmallintField;
    QrContaNotPrntFin: TIntegerField;
    QrContaTemDocFisi: TSmallintField;
    QrContaLk: TIntegerField;
    QrContaDataCad: TDateField;
    QrContaDataAlt: TDateField;
    QrContaUserCad: TIntegerField;
    QrContaUserAlt: TIntegerField;
    QrContaAlterWeb: TSmallintField;
    QrContaAtivo: TSmallintField;
    QrContaCentroRes: TSmallintField;
    QrContaPagRec: TSmallintField;
    QrContaCOD_PLA: TIntegerField;
    QrContaNOM_PLA: TWideStringField;
    QrContaCOD_CJT: TIntegerField;
    QrContaNOM_CJT: TWideStringField;
    QrContaCOD_GRU: TIntegerField;
    QrContaNOM_GRU: TWideStringField;
    QrContaCOD_SGR: TIntegerField;
    QrContaNOM_SGR: TWideStringField;
    TbContas_S: TSmallintField;
    TbContas_M: TSmallintField;
    TbContas_C: TIntegerField;
    TbContas_D: TIntegerField;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    TbContas_P: TIntegerField;
    TbContas_R: TIntegerField;
    TbContasLetraP: TWideStringField;
    TbContasLetraR: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TbContasBeforePost(DataSet: TDataSet);
    procedure TbContasDeleting(Sender: TObject; var Allow: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure TBTamChange(Sender: TObject);
    procedure TbContasAfterPost(DataSet: TDataSet);
    procedure LbItensMDDblClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure TbContasAfterInsert(DataSet: TDataSet);
    procedure TbContasNewRecord(DataSet: TDataSet);
    procedure TbContasBeforeOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure TbContasAfterScroll(DataSet: TDataSet);
    procedure TbContasBeforeClose(DataSet: TDataSet);
    procedure TbContasCalcFields(DataSet: TDataSet);
    procedure DBGContasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGContasCellClick(Column: TColumn);
    procedure DBGContasColEnter(Sender: TObject);
    procedure DBGContasColExit(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenConta();
  public
    { Public declarations }
    FPermiteExcluir: Boolean;
    FNovoCodigo: TNovoCodigo;
    FReservados: Variant;
    FSoReserva: Boolean;
    FFldID: String;
    FFldNome: String;
    FDBGInsert: Boolean;
    FModoInclusao: TdmkModoInclusao;
    //
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
    FFldIndices: array of String;
    FValIndices: array of Variant;
*)
    //
    procedure ReabrePesquisa();
  end;

  var
  FmContasPlanoLista: TFmContasPlanoLista;

implementation

uses UnMyObjects, Module, UMySQLModule, CfgCadLista, MyVCLSkin;

{$R *.DFM}

procedure TFmContasPlanoLista.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, TmySQLQuery(TbContas), [PnDados],
  [PnEdita], EdNome, ImgTipo, TbContas.TableName);
end;

procedure TFmContasPlanoLista.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32(TbContas.TableName, FFldID, '', '',
    tsPos, ImgTipo.SQLType, TbContasCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    TbContas.TableName, Codigo, Dmod.QrUpd, [PnEdita], [PnDados],
    ImgTipo, True) then
  begin
(*  Desmarcar se usar "UMyMod.SQLInsUpd(...)" em vez de "UMyMod.ExecSQLInsUpdPanel(...)"
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
*)
    TbContas.Refresh;
    TbContas.Locate(FFldID, Codigo, []);
  end;
end;

procedure TFmContasPlanoLista.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
end;

procedure TFmContasPlanoLista.BtIncluiClick(Sender: TObject);
begin
(*
  UnCfgCadLista.IncluiNovoRegistro(FModoInclusao, Self, PnEdita, [PnDados],
  [PnEdita], EdNome, ImgTipo, FReservados, TbContas, FFldID);
*)
end;

procedure TFmContasPlanoLista.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasPlanoLista.DBGContasCellClick(Column: TColumn);
begin
  if Column.FieldName = '_S' then
  begin
    Screen.Cursor := crHourGlass;
    TbContas.Edit;
    if TbContas_S.Value = 1 then
      TbContasCtrlaSdo.Value := 0
    else
      TbContasCtrlaSdo.Value := 1;
    TbContas.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_M' then
  begin
    Screen.Cursor := crHourGlass;
    TbContas.Edit;
    if TbContas_M.Value = 1 then
      TbContasMensal.Value := 'F'
    else
      TbContasMensal.Value := 'V';
    TbContas.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_C' then
  begin
    Screen.Cursor := crHourGlass;
    TbContas.Edit;
    if TbContas_C.Value = 1 then
      TbContasCredito.Value := 'F'
    else
      TbContasCredito.Value := 'V';
    TbContas.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_D' then
  begin
    Screen.Cursor := crHourGlass;
    TbContas.Edit;
    if TbContas_D.Value = 1 then
      TbContasDebito.Value := 'F'
    else
      TbContasDebito.Value := 'V';
    TbContas.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_R' then
  begin
    Screen.Cursor := crHourGlass;
    TbContas.Edit;
    if TbContasCentroRes.Value = 1 then
      TbContasCentroRes.Value := 2
    else
      TbContasCentroRes.Value := 1;
    TbContas.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_P' then
  begin
    Screen.Cursor := crHourGlass;
    TbContas.Edit;
    if TbContasPagRec.Value = 1 then
      TbContasPagRec.Value := -1
    else
      TbContasPagRec.Value := TbContasPagRec.Value + 1;
    TbContas.Post;
    Screen.Cursor := crDefault;
  end else
(*
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGConta.BeginDrag(True);
*)
end;

procedure TFmContasPlanoLista.DBGContasColEnter(Sender: TObject);
begin
  if (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_S')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_M')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_C')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_D')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_P')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_R') then
    TDBGrid(Sender).Options := TDBGrid(Sender).Options - [dgEditing] else
    TDBGrid(Sender).Options := TDBGrid(Sender).Options + [dgEditing];
end;

procedure TFmContasPlanoLista.DBGContasColExit(Sender: TObject);
begin
  THackDBGrid(Sender).Options := THackDBGrid(Sender).Options - [dgEditing];
end;

procedure TFmContasPlanoLista.DBGContasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
  Inverte = False;
var
  CorFundo, CorTexto: TColor;
begin
  if Column.FieldName = '_M' then
    MeuVCLSkin.DrawGrid(TDbGrid(DBGContas), Rect, 1, TbContas_M.Value);
  if Column.FieldName = '_S' then
    MeuVCLSkin.DrawGrid(TDbGrid(DBGContas), Rect, 1, TbContas_S.Value);
  if Column.FieldName = '_C' then
    MeuVCLSkin.DrawGrid(TDbGrid(DBGContas), Rect, 1, TbContas_C.Value);
  if Column.FieldName = '_D' then
    MeuVCLSkin.DrawGrid(TDbGrid(DBGContas), Rect, 1, TbContas_D.Value);
  //

  if Column.FieldName = '_P' then
  begin
    dmkPF.CorFundoEFonte(
      TdmkTemaCores(TbContas_P.Value), CorFundo, CorTexto, Inverte);
    MyObjects.DesenhaTextoEmDBGrid(TDbGrid(DBGContas), Rect,
      CorTexto, CorFundo, Column.Alignment, TbContasLetraP.Value);
  end;

  if Column.FieldName = '_R' then
  begin
    dmkPF.CorFundoEFonte(
      TdmkTemaCores(TbContas_R.Value), CorFundo, CorTexto, Inverte);
    MyObjects.DesenhaTextoEmDBGrid(TDbGrid(DBGContas), Rect,
      CorTexto, CorFundo, Column.Alignment, TbContasLetraR.Value);
  end;

end;

procedure TFmContasPlanoLista.DBGrid2DblClick(Sender: TObject);
begin
  UnCfgCadLista.LocalizaPesquisadoInt1(TbContas, QrPesq, FFldID, True);
end;

procedure TFmContasPlanoLista.EdPesqChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmContasPlanoLista.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.CorIniComponente();
  BtInclui.Visible := FReservados <> Null;
end;

procedure TFmContasPlanoLista.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Align := alClient;
  FNovoCodigo := ncControle;
  FReservados := Null;
  FSoReserva := False;
  FPermiteExcluir := False;
  FModoInclusao := dmkmiAutomatico;
  FFldID := CO_CODIGO;
  FFldNome := CO_NOME;
  //
  TbContas.TableName := 'contas';
end;

procedure TFmContasPlanoLista.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasPlanoLista.FormShow(Sender: TObject);
begin
  TbContas. Open;
end;

procedure TFmContasPlanoLista.LbItensMDDblClick(Sender: TObject);
begin
  UnCfgCadLista.AddVariavelEmTexto(LbItensMD, TbContas, FFldNome, DBGContas);
end;

procedure TFmContasPlanoLista.ReabrePesquisa();
begin
  UnCfgCadLista.ReabrePesquisa(
    QrPesq, TbContas, EdPesq, TBTam, FFldID, FFldNome);
end;

procedure TFmContasPlanoLista.ReopenConta();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrConta, Dmod.MyDB, [
  'SELECT con.*, ',
  'pla.Codigo COD_PLA, pla.Nome NOM_PLA,  ',
  'cjt.Codigo COD_CJT, cjt.Nome NOM_CJT,  ',
  'gru.Codigo COD_GRU, gru.Nome NOM_GRU,  ',
  'sgr.Codigo COD_SGR, sgr.Nome NOM_SGR  ',
  'FROM contas con ',
  'LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo ',
  'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo ',
  'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto ',
  'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano ',
  'WHERE con.Codigo=' + Geral.FF0(TbContasCodigo.Value),
  '']);
end;

procedure TFmContasPlanoLista.TbContasAfterInsert(DataSet: TDataSet);
begin
 if FDBGInsert = false then
   TbContas.Cancel;
end;

procedure TFmContasPlanoLista.TbContasAfterPost(DataSet: TDataSet);
begin
  VAR_CADASTRO := TbContasCodigo.Value;
end;

procedure TFmContasPlanoLista.TbContasAfterScroll(DataSet: TDataSet);
begin
  ReopenConta();
end;

procedure TFmContasPlanoLista.TbContasBeforeClose(DataSet: TDataSet);
begin
  QrConta.Close;
end;

procedure TFmContasPlanoLista.TbContasBeforeOpen(DataSet: TDataSet);
(*
var
  I: Integer;
  Txt: String;
*)
begin
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
  I := High(FFldIndices);
  TbContas.Filter := '';
  TbContas.Close;  // Ter certeza ??
  TbContas.Filtered := False;
  for I := Low(FFldIndices) to High(FFldIndices) do
  begin
    Txt := 'and ' + FFldIndices[I] + Geral.VariavelToString(FFldIndices[I]);
  end;
  if Txt <> '' then
  begin
    Txt := Copy(Txt, 4);
    TbContas.Filter := Txt;
    TbContas.Filtered := True;
  end;
*)
end;

procedure TFmContasPlanoLista.TbContasBeforePost(DataSet: TDataSet);
begin
  UnCfgCadLista.TbBeforePost(TbContas, FNovoCodigo, FFldID, FReservados);
  //
(* NUDQU - Nunca usado! Desmarcar quando precisar usar
    for I := Low(FFldIndices) to High(FFldIndices) do
      TbContas.FieldByName(FFldIndices[I]).AsVariant := FFldIndices[I];
*)
end;

procedure TFmContasPlanoLista.TbContasCalcFields(DataSet: TDataSet);
begin
  if TbContasMensal.Value = '' then TbContas_M.Value := 0
  else TbContas_M.Value := dmkPF.V_FToInt(TbContasMensal.Value[1]);

  TbContas_S.Value := TbContasCtrlaSdo.Value;

  if TbContasCredito.Value = '' then TbContas_C.Value := 0
  else TbContas_C.Value := dmkPF.V_FToInt(TbContasCredito.Value[1]);

  if TbContasDebito.Value = '' then TbContas_D.Value := 0
  else TbContas_D.Value := dmkPF.V_FToInt(TbContasDebito.Value[1]);
  //

  case TbContasCentroRes.Value of
    1: TbContas_R.Value := Integer(dmkcorXcel2k7Incorreto); //'D';
    2: TbContas_R.Value := Integer(dmkcorXcel2k7Bom); //'A';
    else TbContas_R.Value := Integer(dmkcorXcel2k7Saida); //'';
  end;
  case TbContasPagRec.Value of
    -1: TbContas_P.Value := Integer(dmkcorXcel2k7Incorreto); //'P';
     0: TbContas_P.Value := Integer(dmkcorXcel2k7Neutra); //'A';
     1: TbContas_P.Value := Integer(dmkcorXcel2k7Bom); //'R';
     else TbContas_P.Value := Integer(dmkcorNotaVermelho); //'?';
  end;
  case TbContasCentroRes.Value of
    1: TbContasLetraR.Value := 'D';
    2: TbContasLetraR.Value := 'A';
    else TbContasLetraR.Value := '?';
  end;
  case TbContasPagRec.Value of
    -1: TbContasLetraP.Value := 'P';
     0: TbContasLetraP.Value := 'A';
     1: TbContasLetraP.Value := 'R';
     else TbContasLetraP.Value := '?';
  end;
end;

procedure TFmContasPlanoLista.TbContasDeleting(Sender: TObject; var Allow: Boolean);
begin
  if FPermiteExcluir then
  begin
   Allow := Geral.MB_Pergunta(
   'Confirma a exclus�o do registro selecionado?') = ID_YES;
  end
  else
    Allow := UMyMod.NaoPermiteExcluirDeTable();
end;

procedure TFmContasPlanoLista.TbContasNewRecord(DataSet: TDataSet);
begin
  if FDBGInsert = False then
    TbContas.Cancel;
end;

procedure TFmContasPlanoLista.TBTamChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

end.
