unit PlaAllCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmPlaAllCad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPlaAllCad: TMySQLQuery;
    DsPlaAllCad: TDataSource;
    Panel2: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel3: TPanel;
    QrPlaAllCadCodigo: TIntegerField;
    QrPlaAllCadNome: TWideStringField;
    QrPlaAllCadNivel: TSmallintField;
    QrPlaAllCadAnaliSinte: TIntegerField;
    QrPlaAllCadCredito: TSmallintField;
    QrPlaAllCadDebito: TSmallintField;
    QrPlaAllCadCodNiv1: TIntegerField;
    QrPlaAllCadCodNiv2: TIntegerField;
    QrPlaAllCadCodNiv3: TIntegerField;
    QrPlaAllCadCodNiv4: TIntegerField;
    QrPlaAllCadCodNiv5: TIntegerField;
    QrPlaAllCadCodNiv6: TIntegerField;
    QrPlaAllCadOrdNiv1: TIntegerField;
    QrPlaAllCadOrdNiv2: TIntegerField;
    QrPlaAllCadOrdNiv3: TIntegerField;
    QrPlaAllCadOrdNiv4: TIntegerField;
    QrPlaAllCadOrdNiv5: TIntegerField;
    QrPlaAllCadOrdNiv6: TIntegerField;
    QrPlaAllCadNomNiv1: TWideStringField;
    QrPlaAllCadNomNiv2: TWideStringField;
    QrPlaAllCadNomNiv3: TWideStringField;
    QrPlaAllCadNomNiv4: TWideStringField;
    QrPlaAllCadNomNiv5: TWideStringField;
    QrPlaAllCadNomNiv6: TWideStringField;
    QrPlaAllCadSPED_COD_CTA_REF: TWideStringField;
    QrPlaAllCadOrdens: TWideStringField;
    QrPlaAllCadAtivo: TSmallintField;
    QrPlaAllCadSIGLA_AS: TWideStringField;
    QrPlaAllCadNO_CRED: TWideStringField;
    QrPlaAllCadNO_DEBI: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenStqaLocIts(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmPlaAllCad: TFmPlaAllCad;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmPlaAllCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPlaAllCadCodigo.Value;
  Close;
end;

procedure TFmPlaAllCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPlaAllCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenStqaLocIts(0);
end;

procedure TFmPlaAllCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPlaAllCad.ReopenStqaLocIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPlaAllCad, Dmod.MyDB, [
  'SELECT CASE pac.AnaliSinte',
  '  WHEN 1 THEN "S"',
  '  WHEN 2 THEN "A"',
  '  ELSE "?" END SIGLA_AS,',
  'CASE pac.Credito',
  '  WHEN 0 THEN "N�O"',
  '  WHEN 2 THEN "SIM"',
  '  ELSE "???" END NO_CRED,',
  'CASE pac.Debito',
  '  WHEN 0 THEN "N�O"',
  '  WHEN 2 THEN "SIM"',
  '  ELSE "???" END NO_DEBI,',
  'pac.*',
  'FROM plaallcad pac',
  'ORDER BY pac.Ordens',
  '']);
end;

end.
