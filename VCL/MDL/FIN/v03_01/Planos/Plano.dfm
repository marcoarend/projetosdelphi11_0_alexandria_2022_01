object FmPlano: TFmPlano
  Left = 369
  Top = 181
  Caption = 'FIN-PLCTA-001 :: Cadastro de N'#237'vel 1 do Plano de Contas'
  ClientHeight = 487
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -9
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 95
    Width = 1008
    Height = 392
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 52
    ExplicitTop = 83
    ExplicitWidth = 693
    object DBGrid1: TDBGrid
      Left = 68
      Top = 224
      Width = 529
      Height = 78
      DataSource = DsConjuntos
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -9
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          ReadOnly = True
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 243
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OrdemLista'
          Title.Caption = 'Ordem lista'
          Width = 51
          Visible = True
        end>
    end
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 193
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      ExplicitWidth = 693
      object Label1: TLabel
        Left = 16
        Top = 3
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 63
        Top = 3
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 437
        Top = 3
        Width = 34
        Height = 13
        Caption = 'Ordem:'
      end
      object Label6: TLabel
        Left = 249
        Top = 3
        Width = 99
        Height = 13
        Caption = 'Descri'#231#227'o substituta:'
        FocusControl = DBEdit2
      end
      object Label7: TLabel
        Left = 448
        Top = 140
        Width = 265
        Height = 13
        Caption = 'Conta correl. no Plano de Contas Referenciado da RFB:'
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 20
        Width = 44
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPlano
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 63
        Top = 20
        Width = 183
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsPlano
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 30
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 437
        Top = 20
        Width = 32
        Height = 21
        Color = clWhite
        DataField = 'OrdemLista'
        DataSource = DsPlano
        MaxLength = 50
        TabOrder = 2
      end
      object DBCheckBox1: TDBCheckBox
        Left = 477
        Top = 20
        Width = 95
        Height = 17
        Caption = 'Controla saldo.'
        DataField = 'CtrlaSdo'
        DataSource = DsPlano
        TabOrder = 3
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 16
        Top = 47
        Width = 665
        Height = 90
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = '  Finalidade Cont'#225'bil do Plano: '
        Columns = 4
        DataField = 'FinContab'
        DataSource = DsPlano
        Items.Strings = (
          'Nenhuma'
          '01 - Contas de ativo'
          '02 - Contas de passivo'
          '03 - Patrim'#244'nio l'#237'quido'
          '04 - Contas de resultado'
          '05 - Contas de compensa'#231#227'o'
          ''
          ''
          ''
          '09 - Outras')
        TabOrder = 4
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object DBEdit2: TDBEdit
        Left = 249
        Top = 20
        Width = 182
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome2'
        DataSource = DsPlano
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 30
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 240
        Top = 140
        Width = 201
        Height = 41
        Caption = 'Indicador do tipo de conta:'
        Columns = 3
        DataField = 'AnaliSinte'
        DataSource = DsPlano
        Items.Strings = (
          'Indef.'
          'Sint'#233'tico'
          'Anal'#237'tico')
        TabOrder = 6
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object DBEdit6: TDBEdit
        Left = 448
        Top = 156
        Width = 268
        Height = 21
        DataField = 'SPED_COD_CTA_REF'
        DataSource = DsPlano
        TabOrder = 7
      end
      object DBCheckBox2: TDBCheckBox
        Left = 16
        Top = 150
        Width = 64
        Height = 17
        Caption = 'D'#233'bito'
        DataField = 'PermitDeb'
        DataSource = DsPlano
        TabOrder = 8
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBCheckBox3: TDBCheckBox
        Left = 92
        Top = 150
        Width = 64
        Height = 17
        Caption = 'Cr'#233'dito.'
        DataField = 'PermitCre'
        DataSource = DsPlano
        TabOrder = 9
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBCheckBox4: TDBCheckBox
        Left = 172
        Top = 150
        Width = 64
        Height = 17
        Caption = 'Ativo.'
        DataField = 'Ativo'
        DataSource = DsPlano
        TabOrder = 10
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 320
      Width = 1008
      Height = 72
      Align = alBottom
      TabOrder = 2
      ExplicitWidth = 693
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 169
        Height = 55
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 126
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 85
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 44
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 3
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 171
        Top = 15
        Width = 443
        Height = 55
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
        ExplicitWidth = 128
      end
      object Panel3: TPanel
        Left = 614
        Top = 15
        Width = 392
        Height = 55
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        ExplicitLeft = 299
        object BtExclui: TBitBtn
          Tag = 12
          Left = 185
          Top = 4
          Width = 89
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 94
          Top = 4
          Width = 89
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 89
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 285
          Top = 0
          Width = 107
          Height = 55
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 89
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object StaticText3: TStaticText
      Left = 0
      Top = 193
      Width = 1008
      Height = 17
      Align = alTop
      Alignment = taCenter
      BorderStyle = sbsSunken
      Caption = 'Conjuntos deste plano'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      ExplicitTop = 189
      ExplicitWidth = 693
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 95
    Width = 1008
    Height = 392
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 124
    ExplicitTop = 91
    ExplicitWidth = 693
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 807
      Height = 220
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 63
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 414
        Top = 4
        Width = 55
        Height = 13
        Caption = 'Ordem lista:'
      end
      object Label3: TLabel
        Left = 236
        Top = 4
        Width = 99
        Height = 13
        Caption = 'Descri'#231#227'o substituta:'
      end
      object Label51: TLabel
        Left = 442
        Top = 142
        Width = 265
        Height = 13
        Caption = 'Conta correl. no Plano de Contas Referenciado da RFB:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 20
        Width = 44
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdOrdemLista: TdmkEdit
        Left = 414
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CkCtrlaSdo: TCheckBox
        Left = 477
        Top = 20
        Width = 95
        Height = 17
        Caption = 'Controla saldo.'
        TabOrder = 4
      end
      object RGFinContab: TdmkRadioGroup
        Left = 16
        Top = 47
        Width = 669
        Height = 90
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = '  Finalidade Cont'#225'bil do Plano: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Nenhuma'
          '01 - Contas de ativo'
          '02 - Contas de passivo'
          '03 - Patrim'#244'nio l'#237'quido'
          '04 - Contas de resultado'
          '05 - Contas de compensa'#231#227'o'
          ''
          ''
          ''
          '09 - Outras')
        TabOrder = 5
        UpdType = utYes
        OldValor = 0
      end
      object EdNome: TdmkEdit
        Left = 64
        Top = 20
        Width = 170
        Height = 21
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnRedefinido = EdNomeRedefinido
      end
      object EdNome2: TdmkEdit
        Left = 237
        Top = 20
        Width = 169
        Height = 21
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkPermitDeb: TdmkCheckBox
        Left = 16
        Top = 154
        Width = 68
        Height = 17
        Caption = 'D'#233'bito.'
        TabOrder = 6
        QryCampo = 'PermitDeb'
        UpdCampo = 'PermitDeb'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkPermitCre: TdmkCheckBox
        Left = 92
        Top = 154
        Width = 68
        Height = 17
        Caption = 'Cr'#233'dito.'
        TabOrder = 7
        QryCampo = 'PermitCre'
        UpdCampo = 'PermitCre'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkAtivo: TdmkCheckBox
        Left = 172
        Top = 154
        Width = 57
        Height = 17
        Caption = 'Ativo.'
        TabOrder = 8
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object RGAnaliSinte: TdmkRadioGroup
        Left = 232
        Top = 140
        Width = 201
        Height = 41
        Caption = 'Indicador do tipo de conta:'
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Indef.'
          'Sint'#233'tico'
          'Anal'#237'tico')
        TabOrder = 9
        QryCampo = 'AnaliSinte'
        UpdCampo = 'AnaliSinte'
        UpdType = utYes
        OldValor = 0
      end
      object EdSPED_COD_CTA_REF: TdmkEdit
        Left = 442
        Top = 158
        Width = 267
        Height = 21
        Color = clWhite
        MaxLength = 15
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 320
      Width = 1008
      Height = 72
      Align = alBottom
      TabOrder = 1
      ExplicitWidth = 693
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 89
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 900
        Top = 15
        Width = 106
        Height = 55
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 585
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 88
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 693
    object GB_R: TGroupBox
      Left = 956
      Top = 0
      Width = 52
      Height = 52
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 641
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 213
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 3
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 45
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 213
      Top = 0
      Width = 743
      Height = 52
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 428
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 498
        Height = 32
        Caption = 'Cadastro de N'#237'vel 1 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 498
        Height = 32
        Caption = 'Cadastro de N'#237'vel 1 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 498
        Height = 32
        Caption = 'Cadastro de N'#237'vel 1 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitWidth = 693
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 689
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsPlano: TDataSource
    DataSet = QrPlano
    Left = 576
    Top = 281
  end
  object QrPlano: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPlanoBeforeOpen
    AfterOpen = QrPlanoAfterOpen
    BeforeClose = QrPlanoBeforeClose
    AfterScroll = QrPlanoAfterScroll
    SQL.Strings = (
      'SELECT pl.*'
      'FROM plano pl'
      'WHERE pl.Codigo > 0')
    Left = 548
    Top = 281
    object QrPlanoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPlanoNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrPlanoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPlanoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPlanoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPlanoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPlanoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPlanoCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
      Required = True
    end
    object QrPlanoOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object QrPlanoFinContab: TSmallintField
      FieldName = 'FinContab'
    end
    object QrPlanoNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 60
    end
    object QrPlanoAnaliSinte: TSmallintField
      FieldName = 'AnaliSinte'
    end
    object QrPlanoSPED_COD_CTA_REF: TWideStringField
      FieldName = 'SPED_COD_CTA_REF'
      Size = 60
    end
    object QrPlanoPermitCre: TSmallintField
      FieldName = 'PermitCre'
      Required = True
    end
    object QrPlanoPermitDeb: TSmallintField
      FieldName = 'PermitDeb'
      Required = True
    end
    object QrPlanoAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object TbConjuntos: TMySQLTable
    Filtered = True
    BeforeInsert = TbConjuntosBeforeInsert
    BeforeDelete = TbConjuntosBeforeDelete
    SortFieldNames = 'OrdemLista,Nome'
    TableName = 'conjuntos'
    Left = 548
    Top = 309
    object TbConjuntosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbConjuntosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object TbConjuntosPlano: TIntegerField
      FieldName = 'Plano'
    end
    object TbConjuntosLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbConjuntosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbConjuntosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbConjuntosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbConjuntosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbConjuntosOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object TbConjuntosCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
  end
  object DsConjuntos: TDataSource
    DataSet = TbConjuntos
    Left = 576
    Top = 308
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtSaida
    CanUpd01 = Panel2
    CanDel01 = BtInclui
    Left = 260
    Top = 12
  end
end
