object FmContasNiv: TFmContasNiv
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-025 :: Configura'#231#227'o de Controle de Saldos'
  ClientHeight = 610
  ClientWidth = 975
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 975
    Height = 399
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 406
      Top = 0
      Width = 6
      Height = 399
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
    end
    object dmkDBGrid2: TdmkDBGrid
      Left = 412
      Top = 0
      Width = 563
      Height = 399
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Nivel'
          Title.Caption = 'N'#237'vel'
          Width = 33
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMENIVEL'
          Title.Caption = 'Descri'#231#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'G'#234'nero'
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEGENERO'
          Title.Caption = 'Descri'#231#227'o'
          Width = 299
          Visible = True
        end>
      Color = clWindow
      DataSource = DsContasNiv
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Nivel'
          Title.Caption = 'N'#237'vel'
          Width = 33
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMENIVEL'
          Title.Caption = 'Descri'#231#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'G'#234'nero'
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEGENERO'
          Title.Caption = 'Descri'#231#227'o'
          Width = 299
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 406
      Height = 399
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'Panel3'
      TabOrder = 1
      object dmkDBGrid1: TdmkDBGrid
        Left = 0
        Top = 0
        Width = 406
        Height = 399
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'CliInt'
            Title.Caption = 'Cliente'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Entidade'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLI'
            Title.Caption = 'Nome / Raz'#227'o Social'
            Visible = True
          end>
        Color = clWindow
        DataSource = DsEmpresas
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CliInt'
            Title.Caption = 'Cliente'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Entidade'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLI'
            Title.Caption = 'Nome / Raz'#227'o Social'
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 975
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 916
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 857
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 515
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#227'o de Controle de Saldos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 515
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#227'o de Controle de Saldos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 515
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#227'o de Controle de Saldos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 458
    Width = 975
    Height = 66
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 971
      Height = 46
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 25
        Width = 971
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 524
    Width = 975
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 18
      Width = 971
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 794
        Top = 0
        Width = 177
        Height = 66
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtAcao: TBitBtn
        Tag = 10010
        Left = 26
        Top = 4
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&A'#231#227'o'
        Enabled = False
        TabOrder = 0
        OnClick = BtAcaoClick
      end
    end
  end
  object QrEmpresas: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrEmpresasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, CliInt, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMECLI '
      'FROM entidades'
      'WHERE CliInt <>0'
      '')
    Left = 228
    Top = 96
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmpresasCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmpresasNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 256
    Top = 96
  end
  object QrContasNiv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ELT(niv.Nivel, '#39'Conta'#39', '#39'Sub-grupo'#39', '#39'Grupo'#39', '
      #39'Conjunto'#39', '#39'Plano'#39') NOMENIVEL, ELT(niv.Nivel,'
      'cta.Nome, sgr.Nome, gru.Nome, cjt.Nome, pla.Nome) '
      'NOMEGENERO, niv.Nivel, niv.Genero, niv.Entidade'
      'FROM contasniv niv'
      'LEFT JOIN contas    cta ON cta.Codigo=niv.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=niv.Genero'
      'LEFT JOIN grupos    gru ON gru.Codigo=niv.Genero'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=niv.Genero'
      'LEFT JOIN plano     pla ON pla.Codigo=niv.Genero'
      'WHERE niv.Entidade=:P0'
      '')
    Left = 228
    Top = 124
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasNivNOMENIVEL: TWideStringField
      FieldName = 'NOMENIVEL'
      Size = 9
    end
    object QrContasNivNOMEGENERO: TWideStringField
      FieldName = 'NOMEGENERO'
      Size = 50
    end
    object QrContasNivNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrContasNivGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrContasNivEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
  end
  object DsContasNiv: TDataSource
    DataSet = QrContasNiv
    Left = 256
    Top = 124
  end
  object PMAcao: TPopupMenu
    Left = 68
    Top = 376
    object Incluiitemacontrolar1: TMenuItem
      Caption = '&Inclui item a controlar'
      OnClick = Incluiitemacontrolar1Click
    end
    object Selecionavriositens1: TMenuItem
      Caption = 'Inclui &V'#225'rios itens'
      OnClick = Selecionavriositens1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Excluiitemacontrolar1: TMenuItem
      Caption = '&Exclui item cadastrado'
      OnClick = Excluiitemacontrolar1Click
    end
  end
  object QrNiveis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, CtrlaSdo, 1 Nivel'
      'FROM contas'
      'WHERE Codigo > 0'
      ''
      'UNION'
      ''
      'SELECT Codigo, Nome, CtrlaSdo, 2 Nivel'
      'FROM subgrupos'
      'WHERE Codigo > 0'
      ''
      'UNION'
      ''
      'SELECT Codigo, Nome, CtrlaSdo, 3 Nivel'
      'FROM grupos'
      'WHERE Codigo > 0'
      ''
      'UNION'
      ''
      'SELECT Codigo, Nome, CtrlaSdo, 4 Nivel'
      'FROM conjuntos'
      'WHERE Codigo > 0'
      ''
      'UNION'
      ''
      'SELECT Codigo, Nome, CtrlaSdo, 5 Nivel'
      'FROM plano'
      'WHERE Codigo > 0')
    Left = 205
    Top = 225
    object QrNiveisCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrNiveisNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrNiveisCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
      Required = True
    end
    object QrNiveisNivel: TLargeintField
      FieldName = 'Nivel'
      Required = True
    end
  end
end
