object FmContasPlanoLista: TFmContasPlanoLista
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-047 :: Plano de Contas em Lista'
  ClientHeight = 631
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 311
        Height = 32
        Caption = 'Plano de Contas em Lista'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 311
        Height = 32
        Caption = 'Plano de Contas em Lista'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 311
        Height = 32
        Caption = 'Plano de Contas em Lista'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 92
    Width = 1008
    Height = 373
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 303
      Width = 1008
      Height = 70
      Align = alBottom
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 862
        Top = 15
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 860
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 392
          Top = 4
          Width = 87
          Height = 13
          Caption = '*S: Controla saldo.'
        end
        object Label3: TLabel
          Left = 492
          Top = 4
          Width = 56
          Height = 13
          Caption = '*M: Mensal.'
        end
        object Label4: TLabel
          Left = 564
          Top = 4
          Width = 52
          Height = 13
          Caption = '*D: D'#233'bito.'
        end
        object Label5: TLabel
          Left = 624
          Top = 4
          Width = 53
          Height = 13
          Caption = '*C: Cr'#233'dito.'
        end
        object Label6: TLabel
          Left = 392
          Top = 20
          Width = 256
          Height = 13
          Caption = '*R: Centro de Resultado D=Definitivo, A=Acumulativo.'
        end
        object Label8: TLabel
          Left = 392
          Top = 36
          Width = 292
          Height = 13
          Caption = '*P: Caracteristica Principal. P=Pagar, R=Receber e A=Ambos.'
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Visible = False
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 136
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 260
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtIncluiClick
        end
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 303
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 303
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object DBGContas: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 460
          Height = 303
          Align = alClient
          DataSource = DsContas
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnCellClick = DBGContasCellClick
          OnColEnter = DBGContasColEnter
          OnColExit = DBGContasColExit
          OnDrawColumnCell = DBGContasDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              ReadOnly = True
              Title.Caption = 'C'#243'digo'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 400
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_S'
              Title.Caption = 'S*'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_M'
              Title.Caption = 'M*'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_C'
              Title.Caption = 'C*'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_D'
              Title.Caption = 'D*'
              Width = 17
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = '_R'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Alignment = taCenter
              Title.Caption = '*R'
              Width = 20
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = '_P'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Alignment = taCenter
              Title.Caption = '*P'
              Width = 20
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 684
          Top = 0
          Width = 324
          Height = 303
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object GroupBox7: TGroupBox
            Left = 0
            Top = 0
            Width = 324
            Height = 185
            Align = alTop
            Caption = ' Informa'#231#245'es da conta selecionada: '
            TabOrder = 0
            object Panel11: TPanel
              Left = 2
              Top = 15
              Width = 320
              Height = 168
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label51: TLabel
                Left = 4
                Top = 4
                Width = 52
                Height = 13
                Caption = 'Sub-grupo:'
                FocusControl = DBEdit1
              end
              object Label52: TLabel
                Left = 4
                Top = 44
                Width = 32
                Height = 13
                Caption = 'Grupo:'
                FocusControl = DBEdit2
              end
              object Label53: TLabel
                Left = 4
                Top = 84
                Width = 45
                Height = 13
                Caption = 'Conjunto:'
                FocusControl = DBEdit3
              end
              object Label54: TLabel
                Left = 4
                Top = 124
                Width = 30
                Height = 13
                Caption = 'Plano:'
                FocusControl = DBEdit4
              end
              object DBEdit1: TDBEdit
                Left = 4
                Top = 20
                Width = 312
                Height = 21
                DataField = 'NOM_SGR'
                DataSource = DsConta
                TabOrder = 0
              end
              object DBEdit2: TDBEdit
                Left = 4
                Top = 60
                Width = 312
                Height = 21
                DataField = 'NOM_GRU'
                DataSource = DsConta
                TabOrder = 1
              end
              object DBEdit3: TDBEdit
                Left = 4
                Top = 100
                Width = 312
                Height = 21
                DataField = 'NOM_CJT'
                DataSource = DsConta
                TabOrder = 2
              end
              object DBEdit4: TDBEdit
                Left = 4
                Top = 140
                Width = 312
                Height = 21
                DataField = 'NOM_PLA'
                DataSource = DsConta
                TabOrder = 3
              end
            end
          end
          object Panel7: TPanel
            Left = 0
            Top = 185
            Width = 324
            Height = 118
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 324
              Height = 89
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label1: TLabel
                Left = 4
                Top = 4
                Width = 46
                Height = 13
                Caption = 'Pesquisa:'
              end
              object EdPesq: TEdit
                Left = 4
                Top = 20
                Width = 309
                Height = 21
                TabOrder = 0
                OnChange = EdPesqChange
              end
              object TBTam: TTrackBar
                Left = 0
                Top = 44
                Width = 313
                Height = 41
                Min = 1
                ParentShowHint = False
                Position = 4
                SelStart = 3
                ShowHint = True
                TabOrder = 1
                OnChange = TBTamChange
              end
            end
            object DBGrid2: TDBGrid
              Left = 0
              Top = 89
              Width = 324
              Height = 29
              Align = alClient
              DataSource = DsPesq
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = DBGrid2DblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'C'#243'd.'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 131
                  Visible = True
                end>
            end
          end
        end
        object LbItensMD: TListBox
          Left = 460
          Top = 0
          Width = 224
          Height = 303
          Align = alRight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ItemHeight = 14
          ParentFont = False
          TabOrder = 2
          Visible = False
          OnDblClick = LbItensMDDblClick
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 465
    Width = 1008
    Height = 166
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 501
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 103
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel8: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object TbContas: TmySQLTable
    Database = Dmod.MyDB
    Filter = 'Codigo>0'
    Filtered = True
    BeforeOpen = TbContasBeforeOpen
    BeforeClose = TbContasBeforeClose
    AfterInsert = TbContasAfterInsert
    BeforePost = TbContasBeforePost
    AfterPost = TbContasAfterPost
    AfterScroll = TbContasAfterScroll
    OnCalcFields = TbContasCalcFields
    OnNewRecord = TbContasNewRecord
    OnDeleting = TbContasDeleting
    TableName = 'contas'
    Left = 216
    Top = 144
    object TbContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object TbContasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object TbContasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object TbContasNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object TbContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object TbContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object TbContasCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
      Required = True
    end
    object TbContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object TbContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object TbContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object TbContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object TbContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object TbContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object TbContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object TbContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object TbContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object TbContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object TbContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object TbContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object TbContasRateio: TIntegerField
      FieldName = 'Rateio'
      Required = True
    end
    object TbContasEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object TbContasAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object TbContasPendenMesSeg: TSmallintField
      FieldName = 'PendenMesSeg'
      Required = True
    end
    object TbContasCalculMesSeg: TSmallintField
      FieldName = 'CalculMesSeg'
      Required = True
    end
    object TbContasOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Required = True
    end
    object TbContasContasAgr: TIntegerField
      FieldName = 'ContasAgr'
      Required = True
    end
    object TbContasContasSum: TIntegerField
      FieldName = 'ContasSum'
      Required = True
    end
    object TbContasCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
      Required = True
    end
    object TbContasNotPrntBal: TIntegerField
      FieldName = 'NotPrntBal'
      Required = True
    end
    object TbContasSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
    end
    object TbContasProvRat: TSmallintField
      FieldName = 'ProvRat'
      Required = True
    end
    object TbContasNotPrntFin: TIntegerField
      FieldName = 'NotPrntFin'
      Required = True
    end
    object TbContasTemDocFisi: TSmallintField
      FieldName = 'TemDocFisi'
      Required = True
    end
    object TbContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbContasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbContasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbContasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object TbContasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object TbContasCentroRes: TSmallintField
      FieldName = 'CentroRes'
      Required = True
    end
    object TbContasPagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object TbContas_S: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_S'
      Calculated = True
    end
    object TbContas_M: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_M'
      Calculated = True
    end
    object TbContas_C: TIntegerField
      FieldKind = fkCalculated
      FieldName = '_C'
      Calculated = True
    end
    object TbContas_D: TIntegerField
      FieldKind = fkCalculated
      FieldName = '_D'
      Calculated = True
    end
    object TbContas_P: TIntegerField
      FieldKind = fkCalculated
      FieldName = '_P'
      Calculated = True
    end
    object TbContas_R: TIntegerField
      FieldKind = fkCalculated
      FieldName = '_R'
      Calculated = True
    end
    object TbContasLetraP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LetraP'
      Size = 1
      Calculated = True
    end
    object TbContasLetraR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LetraR'
      Size = 1
      Calculated = True
    end
  end
  object DsContas: TDataSource
    DataSet = TbContas
    Left = 216
    Top = 192
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    Left = 284
    Top = 144
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 284
    Top = 192
  end
  object QrConta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT con.*, '
      'pla.Codigo COD_PLA, pla.Nome NOM_PLA,  '
      'cjt.Codigo COD_CJT, cjt.Nome NOM_CJT,  '
      'gru.Codigo COD_GRU, gru.Nome NOM_GRU,  '
      'sgr.Codigo COD_SGR, sgr.Nome NOM_SGR  '
      'FROM contas con '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo '
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo '
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto '
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano '
      'WHERE con.Codigo>0 '
      ' ')
    Left = 216
    Top = 240
    object QrContaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContaNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrContaNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrContaNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 50
    end
    object QrContaID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContaSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
    end
    object QrContaCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object QrContaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrContaCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContaDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContaMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContaExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContaMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContaMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContaMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContaMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContaMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContaTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContaExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContaRateio: TIntegerField
      FieldName = 'Rateio'
    end
    object QrContaEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrContaAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrContaPendenMesSeg: TSmallintField
      FieldName = 'PendenMesSeg'
    end
    object QrContaCalculMesSeg: TSmallintField
      FieldName = 'CalculMesSeg'
    end
    object QrContaOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object QrContaContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object QrContaContasSum: TIntegerField
      FieldName = 'ContasSum'
    end
    object QrContaCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
    object QrContaNotPrntBal: TIntegerField
      FieldName = 'NotPrntBal'
    end
    object QrContaSigla: TWideStringField
      FieldName = 'Sigla'
    end
    object QrContaProvRat: TSmallintField
      FieldName = 'ProvRat'
    end
    object QrContaNotPrntFin: TIntegerField
      FieldName = 'NotPrntFin'
    end
    object QrContaTemDocFisi: TSmallintField
      FieldName = 'TemDocFisi'
    end
    object QrContaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrContaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrContaCentroRes: TSmallintField
      FieldName = 'CentroRes'
    end
    object QrContaPagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrContaCOD_PLA: TIntegerField
      FieldName = 'COD_PLA'
      Required = True
    end
    object QrContaNOM_PLA: TWideStringField
      FieldName = 'NOM_PLA'
      Size = 50
    end
    object QrContaCOD_CJT: TIntegerField
      FieldName = 'COD_CJT'
      Required = True
    end
    object QrContaNOM_CJT: TWideStringField
      FieldName = 'NOM_CJT'
      Size = 50
    end
    object QrContaCOD_GRU: TIntegerField
      FieldName = 'COD_GRU'
      Required = True
    end
    object QrContaNOM_GRU: TWideStringField
      FieldName = 'NOM_GRU'
      Size = 50
    end
    object QrContaCOD_SGR: TIntegerField
      FieldName = 'COD_SGR'
      Required = True
    end
    object QrContaNOM_SGR: TWideStringField
      FieldName = 'NOM_SGR'
      Size = 50
    end
  end
  object DsConta: TDataSource
    DataSet = QrConta
    Left = 284
    Top = 240
  end
end
