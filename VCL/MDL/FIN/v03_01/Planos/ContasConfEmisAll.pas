unit ContasConfEmisAll;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, frxClass, frxDBSet, DmkDAC_PF, UnDmkEnums;

type
  TFmContasConfEmisAll = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    Query: TmySQLQuery;
    DataSource1: TDataSource;
    QrContasMes: TmySQLQuery;
    QrPesq: TmySQLQuery;
    QrPesqQtde: TLargeintField;
    QrPesqDebito: TFloatField;
    PB1: TProgressBar;
    QrContasMesCodCliInt: TIntegerField;
    QrContasMesCodEnti: TIntegerField;
    QrContasMesNOMECONTA: TWideStringField;
    QrContasMesGenero: TIntegerField;
    QrContasMesControle: TIntegerField;
    QrContasMesDescricao: TWideStringField;
    QrContasMesPeriodoIni: TIntegerField;
    QrContasMesPeriodoFim: TIntegerField;
    QrContasMesValorMin: TFloatField;
    QrContasMesValorMax: TFloatField;
    QrContasMesDiaAlert: TSmallintField;
    QrContasMesDiaVence: TSmallintField;
    QrContasMesQtdeMin: TIntegerField;
    QrContasMesQtdeMax: TIntegerField;
    QueryEmpresa: TIntegerField;
    QueryEntidade: TIntegerField;
    QueryGenero: TIntegerField;
    QueryNomeCta: TWideStringField;
    QueryDescricao: TWideStringField;
    QueryQtdeMin: TIntegerField;
    QueryQtdeMax: TIntegerField;
    QueryQtdeExe: TIntegerField;
    QueryValrMin: TFloatField;
    QueryValrMax: TFloatField;
    QueryValrExe: TFloatField;
    QueryDiaAlert: TSmallintField;
    QueryDiaVence: TSmallintField;
    QueryStatus: TIntegerField;
    QueryNOMESTATUS: TWideStringField;
    QueryControle: TIntegerField;
    Panel5: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    SbImprime: TBitBtn;
    frxFIN_PLCTA_034_0: TfrxReport;
    frxDsConfPgtos: TfrxDBDataset;
    QrContasMesNO_ENT: TWideStringField;
    QueryNomeEnti: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GradeDados: TDBGrid;
    DsContasMes: TDataSource;
    QrContasMesTipMesRef: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure GradeDadosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QueryAfterOpen(DataSet: TDataSet);
    procedure QueryBeforeClose(DataSet: TDataSet);
    procedure QueryCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
  private
    { Private declarations }
    FConfPgtos: String;
    procedure ReopenQuery(Controle: Integer);
  public
    { Public declarations }
    procedure VerificaEmissoes();

  end;

  var
  FmContasConfEmisAll: TFmContasConfEmisAll;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, UCreate, UnFinanceiro;

{$R *.DFM}

procedure TFmContasConfEmisAll.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasConfEmisAll.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stLok;
end;

procedure TFmContasConfEmisAll.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasConfEmisAll.GradeDadosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if (Column.FieldName = 'NOMESTATUS') then
  begin
    case QueryStatus.Value of
      0: Cor := clRed;
      1: Cor := clPurple;
      2: Cor := clNavy;
      3: Cor := clBlue;
      4: Cor := clGreen;
      else Cor := clFuchsia;
    end;
    with GradeDados.Canvas do
    begin
      if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmContasConfEmisAll.QueryAfterOpen(DataSet: TDataSet);
begin
  BtInclui.Enabled := Query.RecordCount > 0;
end;

procedure TFmContasConfEmisAll.QueryBeforeClose(DataSet: TDataSet);
begin
  BtInclui.Enabled := False;
end;

procedure TFmContasConfEmisAll.QueryCalcFields(DataSet: TDataSet);
begin
  QueryNOMESTATUS.Value := UFinanceiro.StatusContasConf_Texto(
    QueryStatus.Value, QueryQtdeMin.Value, QueryQtdeExe.Value);
end;

procedure TFmContasConfEmisAll.ReopenQuery(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FConfPgtos,
  '/* WHERE Status not IN (3,4) */ ',
  'WHERE Alertar=1 ',
  'ORDER BY NomeEnti, Status ',
  '']);
{
  Query.Close;
  UmyMod.AbreQuery(Query, 'TFmContasConfEmisAll.ReopenQuery()');
}
  Query.Locate('Controle', Controle, []);
end;

procedure TFmContasConfEmisAll.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxFIN_PLCTA_034_0, 'Contas Mensais - Alertas de Emiss�es');
end;

procedure TFmContasConfEmisAll.VerificaEmissoes();
  procedure ReabrePesq(CliInt, EntInt, Genero, Mez, TipMesRef: Integer);
  var
    TabLctA: String;
  begin
    TabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
    //
    QrPesq.Close;
    QrPesq.SQL.Clear;
    QrPesq.SQL.Add('SELECT COUNT(Controle) Qtde, SUM(lan.Debito) Debito');
    QrPesq.SQL.Add('FROM ' + TabLctA + ' lan');
    QrPesq.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrPesq.SQL.Add('WHERE car.Tipo <> 1');
    QrPesq.SQL.Add('AND car.ForneceI=:P0');
    QrPesq.SQL.Add('AND lan.Genero=:P1');
    QrPesq.SQL.Add('AND lan.Mez=:P2');
    QrPesq.Params[00].AsInteger := EntInt;
    QrPesq.Params[01].AsInteger := Genero;
    QrPesq.Params[02].AsInteger := Mez + TipMesRef - 2;
    QrPesq.Open;
    //
  end;
  procedure InsereItemQuery(Periodo: Integer);
  var
    Controle, Genero, Status, DiaAlert, DiaVence, Empresa, Entidade, Alertar: Integer;
    NomeCta, Descricao, NomeEnti: String;
    QtdeMin, QtdeMax, QtdeExe, ValrMin, ValrMax, ValrExe: Double;
    DataAlert: TDateTime;
    Dia, Mes, Ano: Word;
    Alerta: Boolean;
  begin
    Status := UFinanceiro.StatusContasConf_Codigo(QrPesqQtde.Value,
      QrContasMesQtdeMin.Value, QrContasMesQtdeMax.Value,
      QrPesqDebito.Value, QrContasMesValorMin.Value, QrContasMesValorMax.Value,
      Periodo, QrContasMesPeriodoIni.Value, QrContasMesPeriodoFim.Value);
    Controle  := QrContasMesControle.Value;
    Empresa   := QrContasMesCodCliInt.Value;
    Entidade  := QrContasMesCodEnti.Value;
    NomeEnti  := QrContasMesNO_ENT.Value;
    Genero    := QrContasMesGenero.Value;
    NomeCta   := QrContasMesNOMECONTA.Value;
    Descricao := QrContasMesDescricao.Value;
    QtdeMin   := QrContasMesQtdeMin.Value;
    QtdeMax   := QrContasMesQtdeMax.Value;
    QtdeExe   := QrPesqQtde.Value;
    ValrMin   := QrContasMesValorMin.Value;
    ValrMax   := QrContasMesValorMax.Value;
    ValrExe   := QrPesqDebito.Value;
    DiaAlert  := QrContasMesDiaAlert.Value;
    DiaVence  := QrContasMesDiaVence.Value;
    //
    if (DiaAlert <> 0) and (DiaVence <> 0) then
    begin
      DecodeDate(Now(), Ano, Mes, Dia);
      DataAlert := EncodeDate(ano, Mes, DiaAlert);
      if DiaAlert > DiaVence then
        DataAlert := IncMonth(DataAlert, -1);
      Alerta := DataAlert <= Int(Date());
    end else Alerta := True;
    if Alerta then
    begin
      if Status in ([2,3,4]) then
        Alertar := 0
      else
        Alertar := 1;
    end else Alertar := 0;

    if Alertar = 1 then
    begin
      //
    end;
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FConfPgtos, False, [
    'Controle', 'Empresa',
    'Entidade', 'NomeEnti',
    'Genero', 'NomeCta', 'QtdeMin',
    'QtdeMax', 'QtdeExe', 'ValrMin',
    'ValrMax', 'ValrExe', 'Descricao',
    'DiaAlert', 'DiaVence',
    'Alertar', 'Status'], [
    ], [
    Controle, Empresa,
    Entidade, NomeEnti,
    Genero, NomeCta, QtdeMin,
    QtdeMax, QtdeExe, ValrMin,
    ValrMax, ValrExe, Descricao,
    DiaAlert, DiaVence,
    Alertar, Status], [
    ], False);
  end;
var
  Mez, Perio, EntInt, CliInt, Genero, TipMesRef: Integer;
  DataI, DataF: String;
begin
  FConfPgtos := UCriar.RecriaTempTableNovo(ntrttConfPgtos, DmodG.QrUpdPID1, False);
  Query.Database := DmodG.MyPID_DB;
  //
  Perio := Geral.Periodo2000(DModG.ObtemAgora()) (*- 1*);
  Mez   := MLAGeral.PeriodoToMez(Perio);
  DataI := MLAGeral.PrimeiroDiaDoPeriodo(Perio, dtSystem);
  DataF := MLAGeral.UltimoDiaDoPeriodo(Perio, dtSystem);
  //
  QrContasMes.Close;
  QrContasMes.Params[00].AsInteger := VAR_FUNCILOGIN;
  QrContasMes.Params[01].AsInteger := Perio;
  //QrContasMes.Params[02].AsInteger := Perio;
  QrContasMes.Open;
  //
  PB1.Position := 0;
  PB1.Max := QrContasMes.RecordCount;
  QrContasMes.First;
  while not QrContasMes.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Pesquisando ' +
      QrContasMesDescricao.Value);
    //
    EntInt := QrContasMesCodEnti.Value;
    CliInt := QrContasMesCodCliInt.Value;
    Genero := QrContasMesGenero.Value;
    TipMesRef := QrContasMesTipMesRef.Value;
    ReabrePesq(CliInt, EntInt, Genero, Mez, TipMesRef);
    InsereItemQuery(Perio);
    //
    QrContasMes.Next;
  end;
  ReopenQuery(0);
  if Query.RecordCount > 0 then
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, False, FormatFloat('0',
      Query.RecordCount) + ' itens precisam de aten��o!')
  else
    Close;
end;

end.
