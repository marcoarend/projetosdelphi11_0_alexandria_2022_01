object FmContasConfCad: TFmContasConfCad
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-029 :: Contas Mensais - Pagamentos Mensais'
  ClientHeight = 591
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 784
    Height = 499
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 9
      Width = 782
      Height = 489
      Align = alBottom
      TabOrder = 0
      object PnEdita: TPanel
        Left = 1
        Top = 308
        Width = 780
        Height = 180
        Align = alBottom
        TabOrder = 0
        Visible = False
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 117
          Height = 13
          Caption = 'Conta (Plano de contas):'
        end
        object PnConfirma: TPanel
          Left = 1
          Top = 131
          Width = 778
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 6
          object BitBtn1: TBitBtn
            Tag = 14
            Left = 20
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Confirma'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BitBtn1Click
          end
          object Panel6: TPanel
            Left = 667
            Top = 0
            Width = 111
            Height = 48
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BitBtn2: TBitBtn
              Tag = 15
              Left = 2
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Desiste'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BitBtn2Click
            end
          end
        end
        object EdConta: TdmkEditCB
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBConta
          IgnoraDBLookupComboBox = False
        end
        object CBConta: TdmkDBLookupComboBox
          Left = 64
          Top = 20
          Width = 600
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsContas
          TabOrder = 1
          dmkEditCB = EdConta
          QryCampo = 'Codigo'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CkContinuar: TCheckBox
          Left = 8
          Top = 112
          Width = 117
          Height = 17
          Caption = 'Continuar inserindo.'
          Checked = True
          State = cbChecked
          TabOrder = 5
        end
        object GroupBox1: TGroupBox
          Left = 8
          Top = 44
          Width = 185
          Height = 65
          Caption = ' Quantidade de documentos: '
          TabOrder = 2
          object Label3: TLabel
            Left = 96
            Top = 20
            Width = 67
            Height = 13
            Caption = 'Qtde. m'#225'xima:'
          end
          object Label2: TLabel
            Left = 12
            Top = 20
            Width = 66
            Height = 13
            Caption = 'Qtde. m'#237'nima:'
          end
          object EdQtdeMax: TdmkEdit
            Left = 96
            Top = 36
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'QtdeMax'
            UpdCampo = 'QtdeMax'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdQtdeMin: TdmkEdit
            Left = 12
            Top = 36
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'QtdeMin'
            UpdCampo = 'QtdeMin'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object GroupBox2: TGroupBox
          Left = 196
          Top = 44
          Width = 221
          Height = 65
          Caption = ' Soma dos valores dos documentos: '
          TabOrder = 3
          object Label5: TLabel
            Left = 12
            Top = 20
            Width = 64
            Height = 13
            Caption = 'Valor m'#237'nimo:'
          end
          object Label4: TLabel
            Left = 116
            Top = 20
            Width = 65
            Height = 13
            Caption = 'Valor m'#225'ximo:'
          end
          object EdValrMin: TdmkEdit
            Left = 12
            Top = 36
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValrMin'
            UpdCampo = 'ValrMin'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdValrMax: TdmkEdit
            Left = 116
            Top = 36
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValrMax'
            UpdCampo = 'ValrMax'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object GroupBox3: TGroupBox
          Left = 420
          Top = 44
          Width = 245
          Height = 65
          Caption = ' Dia do m'#234's: '
          Enabled = False
          TabOrder = 4
          object Label6: TLabel
            Left = 12
            Top = 20
            Width = 74
            Height = 13
            Caption = 'In'#237'cio do alerta:'
            Enabled = False
          end
          object Label7: TLabel
            Left = 96
            Top = 20
            Width = 140
            Height = 13
            Caption = 'Vencimento dos documentos:'
            Enabled = False
          end
          object EdDiaAlert: TdmkEdit
            Left = 12
            Top = 36
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'DiaAlert'
            UpdCampo = 'DiaAlert'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdDiaVence: TdmkEdit
            Left = 96
            Top = 36
            Width = 141
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'DiaVence'
            UpdCampo = 'DiaVence'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
      object PnNomeCond: TPanel
        Left = 1
        Top = 1
        Width = 780
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Nome do Cliente Interno'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 4227327
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object dmkDBGrid1: TdmkDBGrid
        Left = 1
        Top = 42
        Width = 780
        Height = 218
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Conta'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECONTA'
            Title.Caption = 'Descri'#231#227'o da conta (Plano de contas)'
            Width = 358
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtdeMin'
            Title.Caption = 'Qtde. min.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtdeMax'
            Title.Caption = 'Qtde. max.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrMin'
            Title.Caption = 'Valor min.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrMax'
            Title.Caption = 'Valor max.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DiaAlert'
            Title.Caption = 'dd Alerta'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DiaVence'
            Title.Caption = 'dd Venc.'
            Visible = False
          end>
        Color = clWindow
        DataSource = DsConfPgtos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Conta'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECONTA'
            Title.Caption = 'Descri'#231#227'o da conta (Plano de contas)'
            Width = 358
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtdeMin'
            Title.Caption = 'Qtde. min.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtdeMax'
            Title.Caption = 'Qtde. max.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrMin'
            Title.Caption = 'Valor min.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrMax'
            Title.Caption = 'Valor max.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DiaAlert'
            Title.Caption = 'dd Alerta'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'DiaVence'
            Title.Caption = 'dd Venc.'
            Visible = False
          end>
      end
      object PnControle: TPanel
        Left = 1
        Top = 260
        Width = 780
        Height = 48
        Align = alBottom
        TabOrder = 3
        object BtInclui: TBitBtn
          Tag = 10
          Left = 20
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Inclui'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 668
          Top = 1
          Width = 111
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 2
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 112
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Altera'
          NumGlyphs = 2
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 204
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Exclui'
          NumGlyphs = 2
          TabOrder = 3
          OnClick = BtExcluiClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 483
        Height = 32
        Caption = 'Contas Mensais - Pagamentos Mensais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 483
        Height = 32
        Caption = 'Contas Mensais - Pagamentos Mensais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 483
        Height = 32
        Caption = 'Contas Mensais - Pagamentos Mensais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrConfPgtos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cp.*, co.Nome NOMECONTA '
      'FROM confpgtos cp'#13
      'LEFT JOIN contas co ON co.Codigo=cp.Codigo'#10
      'WHERE CliInt=:P0'
      '')
    Left = 36
    Top = 188
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConfPgtosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrConfPgtosNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrConfPgtosQtdeMin: TIntegerField
      FieldName = 'QtdeMin'
      Required = True
    end
    object QrConfPgtosQtdeMax: TIntegerField
      FieldName = 'QtdeMax'
      Required = True
    end
    object QrConfPgtosValrMin: TFloatField
      FieldName = 'ValrMin'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrConfPgtosValrMax: TFloatField
      FieldName = 'ValrMax'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrConfPgtosDiaAlert: TSmallintField
      FieldName = 'DiaAlert'
      DisplayFormat = '0;-0; '
    end
    object QrConfPgtosDiaVence: TSmallintField
      FieldName = 'DiaVence'
      DisplayFormat = '0;-0; '
    end
    object QrConfPgtosCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object DsConfPgtos: TDataSource
    DataSet = QrConfPgtos
    Left = 36
    Top = 236
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE NOT Codigo IN ('
      '  SELECT Codigo FROM confpgtos'
      '  WHERE CliInt=:P0'
      ')'
      'ORDER BY Nome')
    Left = 132
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 132
    Top = 240
  end
end
