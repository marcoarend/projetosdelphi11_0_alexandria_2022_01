object FmGrupos: TFmGrupos
  Left = 369
  Top = 181
  Caption = 'FIN-PLCTA-003 :: Cadastro de N'#237'vel 3 do Plano de Contas'
  ClientHeight = 428
  ClientWidth = 903
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 88
    Width = 903
    Height = 340
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 372
    ExplicitTop = 116
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1009
      Height = 232
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 12
        Top = 4
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 67
        Top = 4
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 12
        Top = 44
        Width = 35
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#237'vel2:'
      end
      object Label4: TLabel
        Left = 475
        Top = 44
        Width = 55
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem: [F4]'
      end
      object SBGrupo: TSpeedButton
        Left = 442
        Top = 60
        Width = 21
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SBGrupoClick
      end
      object Label6: TLabel
        Left = 315
        Top = 4
        Width = 99
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o substituta:'
      end
      object Label51: TLabel
        Left = 10
        Top = 90
        Width = 265
        Height = 13
        Caption = 'Conta correl. no Plano de Contas Referenciado da RFB:'
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 20
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdConjunto: TdmkEditCB
        Left = 12
        Top = 60
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBConjunto
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBConjunto: TdmkDBLookupComboBox
        Left = 67
        Top = 60
        Width = 374
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsConjuntos
        TabOrder = 4
        dmkEditCB = EdConjunto
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdOrdemLista: TdmkEdit
        Left = 475
        Top = 60
        Width = 70
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdOrdemListaKeyDown
      end
      object CkCtrlaSdo: TCheckBox
        Left = 244
        Top = 140
        Width = 119
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Controla saldo.'
        TabOrder = 11
      end
      object EdNome: TdmkEdit
        Left = 68
        Top = 20
        Width = 245
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnRedefinido = EdNomeRedefinido
      end
      object EdNome2: TdmkEdit
        Left = 316
        Top = 20
        Width = 232
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGAnaliSinte: TdmkRadioGroup
        Left = 344
        Top = 91
        Width = 201
        Height = 41
        Caption = 'Indicador do tipo de conta:'
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Indef.'
          'Sint'#233'tico'
          'Anal'#237'tico')
        TabOrder = 7
        QryCampo = 'AnaliSinte'
        UpdCampo = 'AnaliSinte'
        UpdType = utYes
        OldValor = 0
      end
      object CkPermitDeb: TdmkCheckBox
        Left = 12
        Top = 142
        Width = 68
        Height = 17
        Caption = 'D'#233'bito.'
        TabOrder = 8
        QryCampo = 'PermitDeb'
        UpdCampo = 'PermitDeb'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkPermitCre: TdmkCheckBox
        Left = 88
        Top = 142
        Width = 68
        Height = 17
        Caption = 'Cr'#233'dito.'
        TabOrder = 9
        QryCampo = 'PermitCre'
        UpdCampo = 'PermitCre'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkAtivo: TdmkCheckBox
        Left = 168
        Top = 142
        Width = 57
        Height = 17
        Caption = 'Ativo.'
        TabOrder = 10
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object EdSPED_COD_CTA_REF: TdmkEdit
        Left = 10
        Top = 106
        Width = 267
        Height = 21
        Color = clWhite
        MaxLength = 15
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 270
      Width = 903
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 111
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 768
        Top = 15
        Width = 133
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 88
    Width = 903
    Height = 340
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 152
    ExplicitTop = 56
    object DBGrid1: TDBGrid
      Left = 62
      Top = 130
      Width = 660
      Height = 95
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataSource = DsSubGrupos
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          ReadOnly = True
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OrdemLista'
          Title.Caption = 'Ordem lista'
          Visible = True
        end>
    end
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 903
      Height = 89
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      Caption = 'DsConjuntos'
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 20
        Top = 4
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 79
        Top = 4
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object LaConjunto: TLabel
        Left = 659
        Top = 4
        Width = 45
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Conjunto:'
      end
      object Label5: TLabel
        Left = 610
        Top = 4
        Width = 34
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem:'
      end
      object Label13: TLabel
        Left = 340
        Top = 2
        Width = 99
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o substituta:'
      end
      object Label7: TLabel
        Left = 548
        Top = 44
        Width = 265
        Height = 13
        Caption = 'Conta correl. no Plano de Contas Referenciado da RFB:'
      end
      object DBEdCodigo: TDBEdit
        Left = 20
        Top = 20
        Width = 55
        Height = 21
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsGrupos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 79
        Top = 20
        Width = 254
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsGrupos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdConjunto: TDBEdit
        Left = 659
        Top = 20
        Width = 232
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'NOMECONJUNTO'
        DataSource = DsGrupos
        TabOrder = 2
      end
      object DBEdit1: TDBEdit
        Left = 610
        Top = 20
        Width = 40
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'OrdemLista'
        DataSource = DsGrupos
        TabOrder = 3
      end
      object DBCheckBox1: TDBCheckBox
        Left = 244
        Top = 52
        Width = 119
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Controla saldo.'
        DataField = 'CtrlaSdo'
        DataSource = DsGrupos
        TabOrder = 4
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit2: TDBEdit
        Left = 340
        Top = 20
        Width = 265
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        BiDiMode = bdLeftToRight
        DataField = 'Nome2'
        DataSource = DsGrupos
        ParentBiDiMode = False
        TabOrder = 5
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 340
        Top = 44
        Width = 201
        Height = 41
        Caption = 'Indicador do tipo de conta:'
        Columns = 3
        DataField = 'AnaliSinte'
        DataSource = DsGrupos
        Items.Strings = (
          'Indef.'
          'Sint'#233'tico'
          'Anal'#237'tico')
        TabOrder = 6
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object DBEdit6: TDBEdit
        Left = 548
        Top = 60
        Width = 268
        Height = 21
        DataField = 'SPED_COD_CTA_REF'
        DataSource = DsGrupos
        TabOrder = 7
      end
      object DBCheckBox2: TDBCheckBox
        Left = 20
        Top = 52
        Width = 64
        Height = 17
        Caption = 'D'#233'bito'
        DataField = 'PermitDeb'
        DataSource = DsGrupos
        TabOrder = 8
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBCheckBox3: TDBCheckBox
        Left = 96
        Top = 52
        Width = 64
        Height = 17
        Caption = 'Cr'#233'dito.'
        DataField = 'PermitCre'
        DataSource = DsGrupos
        TabOrder = 9
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBCheckBox4: TDBCheckBox
        Left = 176
        Top = 52
        Width = 64
        Height = 17
        Caption = 'Ativo.'
        DataField = 'Ativo'
        DataSource = DsGrupos
        TabOrder = 10
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 270
      Width = 903
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 175
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 130
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 46
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 4
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 177
        Top = 15
        Width = 109
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 286
        Top = 15
        Width = 615
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 231
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 118
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 481
          Top = 0
          Width = 134
          Height = 53
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtNivelAcima: TBitBtn
          Tag = 351
          Left = 345
          Top = 5
          Width = 123
          Height = 40
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Conjuntos'
          NumGlyphs = 2
          TabOrder = 4
          OnClick = BtNivelAcimaClick
        end
      end
    end
    object StaticText3: TStaticText
      Left = 0
      Top = 89
      Width = 903
      Height = 20
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Alignment = taCenter
      BorderStyle = sbsSunken
      Caption = 'Subgrupos deste grupo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      ExplicitLeft = -48
      ExplicitTop = 109
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 903
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 851
      Top = 0
      Width = 52
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 217
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 217
      Top = 0
      Width = 634
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 498
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de N'#237'vel 3 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 498
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de N'#237'vel 3 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 498
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de N'#237'vel 3 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 903
    Height = 36
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 899
      Height = 19
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 12
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 12
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 720
    Top = 153
  end
  object QrGrupos: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrGruposBeforeOpen
    AfterOpen = QrGruposAfterOpen
    AfterScroll = QrGruposAfterScroll
    SQL.Strings = (
      'SELECT gr.*, cj.Nome NOMECONJUNTO'
      'FROM grupos gr'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'WHERE gr.Codigo > 0')
    Left = 692
    Top = 153
    object QrGruposLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'grupos.Lk'
    end
    object QrGruposDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'grupos.DataCad'
    end
    object QrGruposDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'grupos.DataAlt'
    end
    object QrGruposUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'grupos.UserCad'
    end
    object QrGruposUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'grupos.UserAlt'
    end
    object QrGruposCodigo: TSmallintField
      FieldName = 'Codigo'
      Origin = 'grupos.Codigo'
    end
    object QrGruposConjunto: TIntegerField
      FieldName = 'Conjunto'
      Origin = 'grupos.Conjunto'
      Required = True
    end
    object QrGruposNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 50
    end
    object QrGruposOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'grupos.OrdemLista'
      Required = True
    end
    object QrGruposNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'grupos.Nome'
      Required = True
      Size = 50
    end
    object QrGruposCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
      Origin = 'grupos.CtrlaSdo'
      Required = True
    end
    object QrGruposNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 60
    end
    object QrGruposAnaliSinte: TSmallintField
      FieldName = 'AnaliSinte'
    end
    object QrGruposSPED_COD_CTA_REF: TWideStringField
      FieldName = 'SPED_COD_CTA_REF'
      Size = 60
    end
    object QrGruposPermitCre: TSmallintField
      FieldName = 'PermitCre'
      Required = True
    end
    object QrGruposPermitDeb: TSmallintField
      FieldName = 'PermitDeb'
      Required = True
    end
    object QrGruposAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object TbSubGrupos: TMySQLTable
    Database = Dmod.MyDB
    Filtered = True
    BeforeInsert = TbSubGruposBeforeInsert
    BeforeDelete = TbSubGruposBeforeDelete
    SortFieldNames = 'OrdemLista,Nome'
    TableName = 'subgrupos'
    Left = 692
    Top = 182
    object TbSubGruposCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbSubGruposNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object TbSubGruposOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object TbSubGruposGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object TbSubGruposTipoAgrupa: TIntegerField
      FieldName = 'TipoAgrupa'
    end
    object TbSubGruposLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbSubGruposDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbSubGruposDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbSubGruposUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbSubGruposUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsSubGrupos: TDataSource
    DataSet = TbSubGrupos
    Left = 720
    Top = 182
  end
  object QrConjuntos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM conjuntos'
      'AND AnaliSinte=1'
      'ORDER BY Nome'
      '')
    Left = 480
    Top = 64
    object QrConjuntosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.conjuntos.Codigo'
    end
    object QrConjuntosNome: TWideStringField
      DisplayWidth = 50
      FieldName = 'Nome'
      Origin = 'DBMMONEY.conjuntos.Nome'
      Size = 50
    end
  end
  object DsConjuntos: TDataSource
    DataSet = QrConjuntos
    Left = 508
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtSaida
    CanUpd01 = Panel2
    CanDel01 = BtInclui
    Left = 260
    Top = 12
  end
end
