unit ContasConfEmis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, UnDmkProcFunc,
  dmkDBGrid, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables,
  ComCtrls, UnFinanceiro, dmkGeral, MyDBCheck, dmkImage, UnDmkEnums;

type
  TFmContasConfEmis = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    LaMes: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    LaAno: TLabel;
    GradeDados: TDBGrid;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMECLI: TWideStringField;
    QrContasMes: TmySQLQuery;
    PB1: TProgressBar;
    DataSource1: TDataSource;
    QrPesq: TmySQLQuery;
    QrPesqDebito: TFloatField;
    QrPesqQtde: TLargeintField;
    Query: TmySQLQuery;
    QrContasMesCodigo: TIntegerField;
    QrContasMesControle: TIntegerField;
    QrContasMesCliInt: TIntegerField;
    QrContasMesDescricao: TWideStringField;
    QrContasMesPeriodoIni: TIntegerField;
    QrContasMesPeriodoFim: TIntegerField;
    QrContasMesValorMin: TFloatField;
    QrContasMesValorMax: TFloatField;
    QrContasMesQtdeMin: TIntegerField;
    QrContasMesQtdeMax: TIntegerField;
    QrContasMesNOMECONTA: TWideStringField;
    QrContasMesDiaAlert: TSmallintField;
    QrContasMesDiaVence: TSmallintField;
    QueryControle: TIntegerField;
    QueryEmpresa: TIntegerField;
    QueryEntidade: TIntegerField;
    QueryGenero: TIntegerField;
    QueryNomeCta: TWideStringField;
    QueryDescricao: TWideStringField;
    QueryQtdeMin: TIntegerField;
    QueryQtdeMax: TIntegerField;
    QueryQtdeExe: TIntegerField;
    QueryValrMin: TFloatField;
    QueryValrMax: TFloatField;
    QueryValrExe: TFloatField;
    QueryDiaAlert: TSmallintField;
    QueryDiaVence: TSmallintField;
    QueryStatus: TIntegerField;
    QueryNOMESTATUS: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtInclui: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QueryCalcFields(DataSet: TDataSet);
    procedure GradeDadosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtIncluiClick(Sender: TObject);
    procedure QueryBeforeClose(DataSet: TDataSet);
    procedure QueryAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FConfPgtos: String;
    procedure ReopenQuery(Controle: Integer);
  public
    { Public declarations }
    FFinalidade: TLanctoFinalidade;
    FQrLct, FQrCrt: TmySQLQuery;
    FPercJuroM, FPercMulta: Double;
    FSetaVars: TInsAltReopenLct;
    FAlteraAtehFatID, FLockCliInt, FLockForneceI, FLockAccount, FLockVendedor:
    Boolean;
    FCliente, FFornecedor, FForneceI, FAccount, FVendedor, FIDFinalidade: Integer;
    FTabLctA: String;
  end;

  var
  FmContasConfEmis: TFmContasConfEmis;

implementation

uses UnMyObjects, Module, Principal, ModuleGeral, UCreate, UMySQLModule,
  LctEdit, UnInternalConsts, DmkDAC_PF;

{$R *.DFM}

procedure TFmContasConfEmis.BtIncluiClick(Sender: TObject);
const
  Controle  = 0;
  Sub       = 0;
  FatID     = 0;
  FatID_Sub = 0;
  FatNum    = 0;
  Carteira  = 0;
  //
  Credito   = 0.00;
  Debito    = 0.00;
  //
  FisicoSrc = 0;
  FisicoCod = 0;
var
  Genero, CliInt: Integer;
  Data, Vencto, DataDoc: TDateTime;
  Mes: TDateTime;
begin
  Genero  := QueryGenero.Value;
  CliInt  := EdCliInt.ValueVariant;
  Data    := Date();
  Vencto  := Date();
  DataDoc := Date();
  Mes     := EncodeDate(Geral.IMV(CBAno.Text), CBMes.ItemIndex + 1, 1);
  if UFinanceiro.InclusaoLancamento((*TFmLctEdit, FmLctEdit,*) FFinalidade,
  afmoNegarComAviso, FQrLct, FQrCrt, tgrInclui, Controle, Sub, Genero,
  FPercJuroM, FPercMulta, FSetaVars, FatID, FatID_Sub, FatNum, Carteira,
  Credito, Debito, FAlteraAtehFatID, FCliente, FFornecedor, CliInt, FForneceI,
  FAccount, FVendedor, FLockCliInt, FLockForneceI, FLockAccount, FLockVendedor,
  Data, Vencto, DataDoc, FIDFinalidade, Mes, FTabLctA, FisicoSrc, FisicoCod) > 0 then
  begin
    BtOKClick(Self);
  end;
end;

procedure TFmContasConfEmis.BtOKClick(Sender: TObject);
  procedure ReabrePesq(EntInt, Genero, Mez: Integer);
  begin
    QrPesq.Close;
    QrPesq.SQL.Clear;
    QrPesq.SQL.Add('SELECT COUNT(Controle) Qtde, SUM(lan.Debito) Debito');
    QrPesq.SQL.Add('FROM ' + FTabLctA + ' lan');
    QrPesq.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrPesq.SQL.Add('WHERE car.Tipo <> 1');
    QrPesq.SQL.Add('AND car.ForneceI=:P0');
    QrPesq.SQL.Add('AND lan.Genero=:P1');
    QrPesq.SQL.Add('AND lan.Mez=:P2');
    QrPesq.Params[00].AsInteger := EntInt;
    QrPesq.Params[01].AsInteger := Genero;
    QrPesq.Params[02].AsInteger := Mez;
    UnDmkDAC_PF.AbreQuery(QrPesq, Dmod.MyDB);
    //
  end;
  procedure InsereItemQuery(Periodo: Integer);
  var
    Genero, Status, DiaAlert, DiaVence: Integer;
    NomeCta: String;
    QtdeMin, QtdeMax, QtdeExe, ValrMin, ValrMax, ValrExe: Double;
  begin
    Status := UFinanceiro.StatusContasConf_Codigo(QrPesqQtde.Value,
      QrContasMesQtdeMin.Value, QrContasMesQtdeMax.Value,
      QrPesqDebito.Value, QrContasMesValorMin.Value, QrContasMesValorMax.Value,
      Periodo, QrContasMesPeriodoIni.Value, QrContasMesPeriodoFim.Value);
    Genero  := QrContasMesCodigo.Value;
    NomeCta := QrContasMesNOMECONTA.Value;
    QtdeMin := QrContasMesQtdeMin.Value;
    QtdeMax := QrContasMesQtdeMax.Value;
    QtdeExe := QrPesqQtde.Value;
    ValrMin := QrContasMesValorMin.Value;
    ValrMax := QrContasMesValorMax.Value;
    ValrExe := QrPesqDebito.Value;
    DiaAlert := QrContasMesDiaAlert.Value;
    DiaVence := QrContasMesDiaVence.Value;
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FConfPgtos, False, [
    'Genero', 'NomeCta', 'QtdeMin',
    'QtdeMax', 'QtdeExe', 'ValrMin',
    'ValrMax', 'ValrExe',
    'DiaAlert', 'DiaVence', 'Status'], [
    ], [
    Genero, NomeCta, QtdeMin,
    QtdeMax, QtdeExe, ValrMin,
    ValrMax, ValrExe,
    DiaAlert, DiaVence, Status], [
    ], False);
  end;
var
  Mez, Perio, EntInt, Genero: Integer;
  DataI, DataF: String;
begin
  EntInt := EdCliInt.ValueVariant;
  if EntInt = 0 then
  begin
    Geral.MB_Aviso('Informe o cliente interno!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    FConfPgtos := UCriar.RecriaTempTableNovo(ntrttConfPgtos, DmodG.QrUpdPID1, False);
    //FConfPgtos := UCriar.RecriaTempTable('ConfPgtos', DModG.QrUpdPID1, False);
    Query.Database := DmodG.MyPID_DB;
    //
    Mez := ((Geral.IMV(CBAno.Text)-2000) * 100) + CBMes.ItemIndex + 1;
    Perio := ((Geral.IMV(CBAno.Text)-2000) * 12) + CBMes.ItemIndex + 1;
    DataI := dmkPF.PrimeiroDiaDoPeriodo(Perio, dtSystem);
    DataF := dmkPF.UltimoDiaDoPeriodo(Perio, dtSystem);
    //
    QrContasMes.Close;
    QrContasMes.Params[0].AsInteger := EntInt;
    UnDmkDAC_PF.AbreQuery(QrContasMes, Dmod.MyDB);
    //
    PB1.Position := 0;
    PB1.Max := QrContasMes.RecordCount;
    while not QrContasMes.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      Genero := QrContasMesCodigo.Value;
      ReabrePesq(EntInt, Genero, Mez);
      InsereItemQuery(Perio);
      QrContasMes.Next;
    end;
    ReopenQuery(0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmContasConfEmis.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasConfEmis.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasConfEmis.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
end;

procedure TFmContasConfEmis.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasConfEmis.GradeDadosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if (Column.FieldName = 'NOMESTATUS') then
  begin
    case QueryStatus.Value of
      0: Cor := clRed;
      1: Cor := clPurple;
      2: Cor := clNavy;
      3: Cor := clBlue;
      4: Cor := clGreen;
      else Cor := clFuchsia;
    end;
    with GradeDados.Canvas do
    begin
      if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmContasConfEmis.QueryAfterOpen(DataSet: TDataSet);
begin
  BtInclui.Enabled := Query.RecordCount > 0;
end;

procedure TFmContasConfEmis.QueryBeforeClose(DataSet: TDataSet);
begin
  BtInclui.Enabled := False;
end;

procedure TFmContasConfEmis.QueryCalcFields(DataSet: TDataSet);
begin
  QueryNOMESTATUS.Value := UFinanceiro.StatusContasConf_Texto(
    QueryStatus.Value, QueryQtdeMin.Value, QueryQtdeExe.Value);
end;

procedure TFmContasConfEmis.ReopenQuery(Controle: Integer);
begin
  Query.Close;
  UmyMod.AbreQuery(Query, DModG.MyPID_DB, 'TFmContasMesSelMulCta.AtivarTodos()');
  Query.Locate('Controle', Controle, []);
end;

end.

