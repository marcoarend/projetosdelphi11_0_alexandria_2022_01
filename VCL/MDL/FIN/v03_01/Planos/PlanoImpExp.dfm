object FmPlanoImpExp: TFmPlanoImpExp
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-048 :: Exporta / Importa - Plano de Contas'
  ClientHeight = 619
  ClientWidth = 997
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 997
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 950
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 903
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 408
        Height = 31
        Caption = 'Exporta / Importa - Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 408
        Height = 31
        Caption = 'Exporta / Importa - Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 408
        Height = 31
        Caption = 'Exporta / Importa - Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 997
    Height = 449
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 997
      Height = 449
      ActivePage = TabSheet3
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Padr'#227'o Dermatek '
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 989
          Height = 421
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 989
            Height = 421
            Align = alClient
            TabOrder = 0
            object TreeView1: TTreeView
              Left = 2
              Top = 15
              Width = 851
              Height = 404
              Align = alClient
              Indent = 15
              RightClickSelect = True
              TabOrder = 0
            end
            object Panel5: TPanel
              Left = 853
              Top = 15
              Width = 134
              Height = 404
              Align = alRight
              TabOrder = 1
              object RadioGroup1: TRadioGroup
                Left = 1
                Top = 1
                Width = 132
                Height = 154
                Align = alTop
                Caption = ' Exportar dados'
                TabOrder = 0
              end
              object RadioGroup2: TRadioGroup
                Left = 1
                Top = 155
                Width = 132
                Height = 114
                Align = alTop
                Caption = ' Importar dados'
                TabOrder = 1
              end
              object BtImporta: TBitBtn
                Tag = 19
                Left = 7
                Top = 176
                Width = 118
                Height = 40
                Caption = '&Importa XML'
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BtImportaClick
              end
              object BtSalvar: TBitBtn
                Tag = 34
                Left = 7
                Top = 222
                Width = 118
                Height = 39
                Caption = '&Salvar'
                NumGlyphs = 2
                TabOrder = 3
                OnClick = BtSalvarClick
              end
              object BtCarrega: TBitBtn
                Tag = 14
                Left = 7
                Top = 20
                Width = 118
                Height = 39
                Caption = '&Carrega plano'
                NumGlyphs = 2
                TabOrder = 4
                OnClick = BtCarregaClick
              end
              object BtExclui: TBitBtn
                Tag = 12
                Left = 7
                Top = 61
                Width = 118
                Height = 40
                Caption = '&Remove atual'
                NumGlyphs = 2
                TabOrder = 5
                OnClick = BtExcluiClick
              end
              object BtExporta: TBitBtn
                Tag = 269
                Left = 7
                Top = 102
                Width = 118
                Height = 40
                Caption = '&Exporta XML'
                NumGlyphs = 2
                TabOrder = 6
                OnClick = BtExportaClick
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'CSV ATK'
        ImageIndex = 1
        object GradeC: TStringGrid
          Left = 0
          Top = 43
          Width = 989
          Height = 261
          Align = alClient
          DefaultColWidth = 40
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowDefAlign]
          TabOrder = 0
          ColWidths = (
            40
            167
            434
            90
            29)
        end
        object PnCarrega: TPanel
          Left = 0
          Top = 0
          Width = 989
          Height = 43
          Align = alTop
          TabOrder = 1
          object Label110: TLabel
            Left = 8
            Top = 4
            Width = 39
            Height = 13
            Caption = 'Arquivo:'
          end
          object SBArquivoC: TSpeedButton
            Left = 940
            Top = 20
            Width = 21
            Height = 20
            Caption = '...'
            OnClick = SBArquivoCClick
          end
          object SbCarregaC: TSpeedButton
            Left = 960
            Top = 20
            Width = 21
            Height = 20
            Caption = '>'
            OnClick = SbCarregaCClick
          end
          object EdLoadCSVArqC: TEdit
            Left = 8
            Top = 20
            Width = 930
            Height = 21
            TabOrder = 0
            Text = 
              'C:\_Sincro\_MLArend\Clientes\Colosso - Brasinha\Importa'#231#245'es\Plan' +
              'o de Contas Colosso 2022 03 18.csv'
          end
        end
        object MeAvisosC: TMemo
          Left = 0
          Top = 304
          Width = 989
          Height = 69
          Align = alBottom
          Lines.Strings = (
            'MeAvisos')
          TabOrder = 2
        end
        object Panel6: TPanel
          Left = 0
          Top = 373
          Width = 989
          Height = 48
          Align = alBottom
          TabOrder = 3
          object BtInsereC: TBitBtn
            Tag = 14
            Left = 3
            Top = 4
            Width = 118
            Height = 39
            Caption = '&Carrega plano'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtInsereCClick
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'CSV ECC'
        ImageIndex = 2
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 989
          Height = 43
          Align = alTop
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 39
            Height = 13
            Caption = 'Arquivo:'
          end
          object SbArquivoD: TSpeedButton
            Left = 940
            Top = 20
            Width = 21
            Height = 20
            Caption = '...'
            OnClick = SbArquivoDClick
          end
          object SbCarregaD: TSpeedButton
            Left = 960
            Top = 20
            Width = 21
            Height = 20
            Caption = '>'
            OnClick = SbCarregaDClick
          end
          object EdLoadCSVArqD: TEdit
            Left = 8
            Top = 20
            Width = 930
            Height = 21
            TabOrder = 0
            Text = 
              'C:\_Sincro\_MLArend\Clientes\Colosso - Brasinha\Importa'#231#245'es\Plan' +
              'o de Contas Colosso 2022 03 18.csv'
          end
        end
        object GradeD: TStringGrid
          Left = 0
          Top = 43
          Width = 989
          Height = 261
          Align = alClient
          DefaultColWidth = 40
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowDefAlign]
          TabOrder = 1
          ColWidths = (
            80
            32
            432
            80
            132)
        end
        object Panel8: TPanel
          Left = 0
          Top = 373
          Width = 989
          Height = 48
          Align = alBottom
          TabOrder = 2
          object BtInsereD: TBitBtn
            Tag = 14
            Left = 3
            Top = 4
            Width = 118
            Height = 39
            Caption = '&Carrega plano'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtInsereDClick
          end
        end
        object MeAvisosD: TMemo
          Left = 0
          Top = 304
          Width = 989
          Height = 69
          Align = alBottom
          Lines.Strings = (
            'MeAvisos')
          TabOrder = 3
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 496
    Width = 997
    Height = 54
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 993
      Height = 37
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 18
        Width = 993
        Height = 19
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 550
    Width = 997
    Height = 69
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 853
      Top = 15
      Width = 142
      Height = 52
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 851
      Height = 52
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 512
    Top = 251
  end
  object QrPlano: TMySQLQuery
    Database = Dmod.MyDB
    Filtered = True
    SQL.Strings = (
      
        'SELECT Data, Tipo, Carteira, Controle, Genero, Debito, Credito, ' +
        'Documento FROM Lanctos'
      'WHERE Controle=:P0'
      '')
    Left = 44
    Top = 188
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrConjunto: TMySQLQuery
    Database = Dmod.MyDB
    Filtered = True
    SQL.Strings = (
      
        'SELECT Data, Tipo, Carteira, Controle, Genero, Debito, Credito, ' +
        'Documento FROM Lanctos'
      'WHERE Controle=:P0'
      '')
    Left = 40
    Top = 232
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrGrupo: TMySQLQuery
    Database = Dmod.MyDB
    Filtered = True
    SQL.Strings = (
      
        'SELECT Data, Tipo, Carteira, Controle, Genero, Debito, Credito, ' +
        'Documento FROM Lanctos'
      'WHERE Controle=:P0'
      '')
    Left = 40
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrSubgrupo: TMySQLQuery
    Database = Dmod.MyDB
    Filtered = True
    SQL.Strings = (
      
        'SELECT Data, Tipo, Carteira, Controle, Genero, Debito, Credito, ' +
        'Documento FROM Lanctos'
      'WHERE Controle=:P0'
      '')
    Left = 172
    Top = 256
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrConta: TMySQLQuery
    Database = Dmod.MyDB
    Filtered = True
    SQL.Strings = (
      'SELECT *'#11
      'FROM conta')
    Left = 236
    Top = 176
  end
  object PMMenu: TPopupMenu
    OnPopup = PMMenuPopup
    Left = 496
    Top = 400
    object Removeatual1: TMenuItem
      Caption = '&Remove atual'
      OnClick = Removeatual1Click
    end
  end
end
