object FmConjuntos: TFmConjuntos
  Left = 369
  Top = 181
  Caption = 'FIN-PLCTA-002 :: Cadastro de N'#237'vel 2 do Plano de Contas'
  ClientHeight = 559
  ClientWidth = 975
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 84
    Width = 975
    Height = 475
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    ExplicitLeft = 112
    ExplicitTop = 64
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 975
      Height = 149
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 71
        Top = 4
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object LaConjunto: TLabel
        Left = 699
        Top = 4
        Width = 80
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Palno de contas:'
      end
      object Label5: TLabel
        Left = 650
        Top = 4
        Width = 34
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem:'
      end
      object Label7: TLabel
        Left = 359
        Top = 4
        Width = 99
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o substituta:'
        FocusControl = DBEdit1
      end
      object Label8: TLabel
        Left = 548
        Top = 44
        Width = 265
        Height = 13
        Caption = 'Conta correl. no Plano de Contas Referenciado da RFB:'
      end
      object DBEdCodigo: TDBEdit
        Left = 12
        Top = 20
        Width = 55
        Height = 21
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsConjuntos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 71
        Top = 20
        Width = 282
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsConjuntos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdConjunto: TDBEdit
        Left = 699
        Top = 20
        Width = 260
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'NOMEPLANO'
        DataSource = DsConjuntos
        TabOrder = 2
      end
      object DBEdOrdem: TDBEdit
        Left = 650
        Top = 20
        Width = 40
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'OrdemLista'
        DataSource = DsConjuntos
        TabOrder = 3
      end
      object DBCheckBox1: TDBCheckBox
        Left = 452
        Top = 58
        Width = 119
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Controla saldo.'
        DataField = 'CtrlaSdo'
        DataSource = DsConjuntos
        TabOrder = 4
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit1: TDBEdit
        Left = 359
        Top = 20
        Width = 286
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Nome2'
        DataSource = DsConjuntos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 12
        Top = 44
        Width = 201
        Height = 41
        Caption = 'Indicador do tipo de conta:'
        Columns = 3
        DataField = 'AnaliSinte'
        DataSource = DsConjuntos
        Items.Strings = (
          'Indef.'
          'Sint'#233'tico'
          'Anal'#237'tico')
        TabOrder = 6
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object DBEdit6: TDBEdit
        Left = 548
        Top = 60
        Width = 268
        Height = 21
        DataField = 'SPED_COD_CTA_REF'
        DataSource = DsConjuntos
        TabOrder = 7
      end
      object DBCheckBox2: TDBCheckBox
        Left = 220
        Top = 58
        Width = 64
        Height = 17
        Caption = 'D'#233'bito'
        DataField = 'PermitDeb'
        DataSource = DsConjuntos
        TabOrder = 8
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBCheckBox3: TDBCheckBox
        Left = 296
        Top = 58
        Width = 64
        Height = 17
        Caption = 'Cr'#233'dito.'
        DataField = 'PermitCre'
        DataSource = DsConjuntos
        TabOrder = 9
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBCheckBox4: TDBCheckBox
        Left = 376
        Top = 58
        Width = 64
        Height = 17
        Caption = 'Ativo.'
        DataField = 'Ativo'
        DataSource = DsConjuntos
        TabOrder = 10
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
    object PageControl1: TPageControl
      Left = 5
      Top = 177
      Width = 956
      Height = 125
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      TabOrder = 1
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Grupos deste conjunto'
        object DBGGrupos: TDBGrid
          Left = 0
          Top = 0
          Width = 948
          Height = 97
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsGrupos
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              ReadOnly = True
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrdemLista'
              Title.Caption = 'Ordem lista'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Clientes internos com informa'#231#227'o de saldo (em constru'#231#227'o)'
        ImageIndex = 1
        object dmkDBGSdo: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 948
          Height = 97
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Info'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Entidade'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsConjunSdo
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = dmkDBGSdoCellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Info'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Entidade'
              Visible = True
            end>
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 405
      Width = 975
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 212
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 130
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 46
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 4
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 15
        Width = 156
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 370
        Top = 15
        Width = 603
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 231
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 118
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 469
          Top = 0
          Width = 134
          Height = 53
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtNivelAcima: TBitBtn
          Tag = 350
          Left = 345
          Top = 5
          Width = 123
          Height = 40
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Planos'
          NumGlyphs = 2
          TabOrder = 4
          OnClick = BtNivelAcimaClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 84
    Width = 975
    Height = 475
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    ExplicitLeft = 156
    ExplicitTop = 108
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 975
      Height = 209
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 12
        Top = 4
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 67
        Top = 4
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 12
        Top = 44
        Width = 30
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Plano:'
      end
      object Label4: TLabel
        Left = 475
        Top = 44
        Width = 55
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem: [F4]'
      end
      object SBGrupo: TSpeedButton
        Left = 442
        Top = 60
        Width = 21
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SBGrupoClick
      end
      object Label6: TLabel
        Left = 315
        Top = 4
        Width = 99
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o substituta:'
      end
      object Label51: TLabel
        Left = 10
        Top = 90
        Width = 265
        Height = 13
        Caption = 'Conta correl. no Plano de Contas Referenciado da RFB:'
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 20
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CBPlano: TdmkDBLookupComboBox
        Left = 67
        Top = 60
        Width = 374
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPlano
        TabOrder = 4
        dmkEditCB = EdPlano
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdOrdemLista: TdmkEdit
        Left = 475
        Top = 60
        Width = 70
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdOrdemListaKeyDown
      end
      object CkCtrlaSdo: TCheckBox
        Left = 240
        Top = 140
        Width = 119
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Controla saldo.'
        TabOrder = 11
      end
      object EdPlano: TdmkEditCB
        Left = 12
        Top = 60
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPlano
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdNome: TdmkEdit
        Left = 68
        Top = 20
        Width = 245
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnRedefinido = EdNomeRedefinido
      end
      object EdNome2: TdmkEdit
        Left = 316
        Top = 20
        Width = 233
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGAnaliSinte: TdmkRadioGroup
        Left = 288
        Top = 90
        Width = 201
        Height = 41
        Caption = 'Indicador do tipo de conta:'
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Indef.'
          'Sint'#233'tico'
          'Anal'#237'tico')
        TabOrder = 7
        QryCampo = 'AnaliSinte'
        UpdCampo = 'AnaliSinte'
        UpdType = utYes
        OldValor = 0
      end
      object CkPermitDeb: TdmkCheckBox
        Left = 12
        Top = 142
        Width = 68
        Height = 17
        Caption = 'D'#233'bito.'
        TabOrder = 8
        QryCampo = 'PermitDeb'
        UpdCampo = 'PermitDeb'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkPermitCre: TdmkCheckBox
        Left = 88
        Top = 142
        Width = 68
        Height = 17
        Caption = 'Cr'#233'dito.'
        TabOrder = 9
        QryCampo = 'PermitCre'
        UpdCampo = 'PermitCre'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkAtivo: TdmkCheckBox
        Left = 168
        Top = 142
        Width = 57
        Height = 17
        Caption = 'Ativo.'
        TabOrder = 10
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object EdSPED_COD_CTA_REF: TdmkEdit
        Left = 10
        Top = 106
        Width = 267
        Height = 21
        Color = clWhite
        MaxLength = 15
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 405
      Width = 975
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 111
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 840
        Top = 15
        Width = 133
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 975
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 916
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 650
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 498
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de N'#237'vel 2 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 498
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de N'#237'vel 2 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 498
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de N'#237'vel 2 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 975
    Height = 32
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 971
      Height = 15
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = -2
        Width = 12
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = -3
        Width = 12
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsConjuntos: TDataSource
    DataSet = QrConjuntos
    Left = 680
    Top = 233
  end
  object QrConjuntos: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrConjuntosBeforeOpen
    AfterOpen = QrConjuntosAfterOpen
    AfterScroll = QrConjuntosAfterScroll
    SQL.Strings = (
      'SELECT cj.*, pl.Nome NOMEPLANO'
      'FROM conjuntos cj'
      'LEFT JOIN plano pl ON pl.Codigo=cj.Plano'
      'WHERE cj.Codigo>0')
    Left = 652
    Top = 233
    object QrConjuntosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'conjuntos.Codigo'
      Required = True
    end
    object QrConjuntosNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Origin = 'conjuntos.Nome'
      Required = True
      Size = 60
    end
    object QrConjuntosPlano: TIntegerField
      FieldName = 'Plano'
      Origin = 'conjuntos.Plano'
      Required = True
    end
    object QrConjuntosLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'conjuntos.Lk'
    end
    object QrConjuntosDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'conjuntos.DataCad'
    end
    object QrConjuntosDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'conjuntos.DataAlt'
    end
    object QrConjuntosUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'conjuntos.UserCad'
    end
    object QrConjuntosUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'conjuntos.UserAlt'
    end
    object QrConjuntosNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Origin = 'plano.Nome'
      Size = 50
    end
    object QrConjuntosOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'conjuntos.OrdemLista'
    end
    object QrConjuntosCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
      Origin = 'conjuntos.CtrlaSdo'
      Required = True
    end
    object QrConjuntosNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 60
    end
    object QrConjuntosAnaliSinte: TSmallintField
      FieldName = 'AnaliSinte'
    end
    object QrConjuntosSPED_COD_CTA_REF: TWideStringField
      FieldName = 'SPED_COD_CTA_REF'
      Size = 60
    end
    object QrConjuntosPermitCre: TSmallintField
      FieldName = 'PermitCre'
      Required = True
    end
    object QrConjuntosPermitDeb: TSmallintField
      FieldName = 'PermitDeb'
      Required = True
    end
    object QrConjuntosAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object QrPlano: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM plano'
      'WHERE AnaliSinte=1'
      'ORDER BY Nome'
      '')
    Left = 480
    Top = 68
    object QrPlanoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.conjuntos.Codigo'
    end
    object QrPlanoNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Origin = 'DBMMONEY.conjuntos.Nome'
      Size = 60
    end
  end
  object DsPlano: TDataSource
    DataSet = QrPlano
    Left = 508
    Top = 68
  end
  object TbGrupos: TMySQLTable
    Filtered = True
    BeforeInsert = TbGruposBeforeInsert
    BeforeDelete = TbGruposBeforeDelete
    SortFieldNames = 'OrdemLista,Nome'
    TableName = 'grupos'
    Left = 652
    Top = 261
    object TbGruposCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'grupos.Codigo'
    end
    object TbGruposNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'grupos.Nome'
      Size = 50
    end
    object TbGruposConjunto: TIntegerField
      FieldName = 'Conjunto'
      Origin = 'grupos.Conjunto'
    end
    object TbGruposOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'grupos.OrdemLista'
    end
    object TbGruposLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'grupos.Lk'
    end
    object TbGruposDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'grupos.DataCad'
    end
    object TbGruposDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'grupos.DataAlt'
    end
    object TbGruposUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'grupos.UserCad'
    end
    object TbGruposUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'grupos.UserAlt'
    end
  end
  object DsGrupos: TDataSource
    DataSet = TbGrupos
    Left = 680
    Top = 260
  end
  object QrConjunSdo: TMySQLQuery
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) '
      'NOMEENT, scj.* '
      'FROM conjunsdo scj'
      'LEFT JOIN entidades ent ON ent.Codigo=scj.Entidade'
      'WHERE scj.Codigo=:P0'
      'ORDER BY NOMEENT')
    Left = 556
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConjunSdoNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrConjunSdoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrConjunSdoEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrConjunSdoSdoAtu: TFloatField
      FieldName = 'SdoAtu'
      Required = True
    end
    object QrConjunSdoSdoFut: TFloatField
      FieldName = 'SdoFut'
      Required = True
    end
    object QrConjunSdoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrConjunSdoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrConjunSdoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrConjunSdoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrConjunSdoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrConjunSdoPerAnt: TFloatField
      FieldName = 'PerAnt'
      Required = True
    end
    object QrConjunSdoPerAtu: TFloatField
      FieldName = 'PerAtu'
      Required = True
    end
    object QrConjunSdoInfo: TSmallintField
      FieldName = 'Info'
      Required = True
      MaxValue = 1
    end
  end
  object DsConjunSdo: TDataSource
    DataSet = QrConjunSdo
    Left = 584
    Top = 156
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtSaida
    CanUpd01 = Panel2
    CanDel01 = BtInclui
    Left = 260
    Top = 12
  end
end
