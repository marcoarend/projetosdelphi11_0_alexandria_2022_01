unit NivelEGenero;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, DBCtrls, UnDmkProcFunc,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, dmkImage, UnDMkEnums;

type
  TFmNivelEGenero = class(TForm)
    QrCodiNivel: TmySQLQuery;
    QrCodiNivelCodigo: TIntegerField;
    QrCodiNivelNome: TWideStringField;
    DsCodiNivel: TDataSource;
    Panel4: TPanel;
    Label12: TLabel;
    RGNivel: TRadioGroup;
    dmkEditItem: TdmkEditCB;
    dmkDBItem: TdmkDBLookupComboBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGNivelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCodiNivel(Codigo: Integer);
  public
    { Public declarations }
    FNivel, FGenero: Integer;
  end;

  var
  FmNivelEGenero: TFmNivelEGenero;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmNivelEGenero.BtSaidaClick(Sender: TObject);
begin
  FNivel := 0;
  FGenero := 0;
  Close;
end;

procedure TFmNivelEGenero.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNivelEGenero.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNivelEGenero.ReopenCodiNivel(Codigo: Integer);
begin
  QrCodiNivel.Close;
  if RGNivel.ItemIndex > 0 then
  begin
    QrCodiNivel.SQL[1] := 'FROM ' +
      Lowercase(dmkPF.NivelSelecionado(RgNivel.ItemIndex, 2, '', ''));
    UnDmkDAC_PF.AbreQuery(QrCodiNivel, Dmod.MyDB);
    //
    if Codigo <> 0 then
      QrCodiNivel.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmNivelEGenero.RGNivelClick(Sender: TObject);
begin
  ReopenCodiNivel(0);
  Label12.Caption :=
    dmkPF.NivelSelecionado(RGNivel.ItemIndex, 1, 'Item ', ':');
end;

procedure TFmNivelEGenero.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FNivel := 0;
  FGenero := 0;
end;

procedure TFmNivelEGenero.BtOKClick(Sender: TObject);
begin
  if RGNivel.ItemIndex = 0 then
  begin
    Geral.MB_Aviso('Defina o n�vel e o item do n�vel (g�nero)!');
    RGNivel.SetFocus;
    Exit;
  end;
  FNivel  := RGNivel.ItemIndex;
  FGenero := dmkEditItem.ValueVariant;
  if FGenero = 0 then
  begin
    Geral.MB_Aviso('Defina item do n�vel (g�nero)!');
    dmkEditItem.SetFocus;
    Exit;
  end;
  Close;
end;

end.

