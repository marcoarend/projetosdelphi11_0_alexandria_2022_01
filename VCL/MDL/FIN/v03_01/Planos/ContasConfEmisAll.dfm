object FmContasConfEmisAll: TFmContasConfEmisAll
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-034 :: Contas Mensais - Alertas de Emiss'#245'es'
  ClientHeight = 462
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 469
        Height = 32
        Caption = 'Contas Mensais - Alertas de Emiss'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 469
        Height = 32
        Caption = 'Contas Mensais - Alertas de Emiss'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 469
        Height = 32
        Caption = 'Contas Mensais - Alertas de Emiss'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 283
    Align = alClient
    Caption = ' Esclarecimento sobre a pesquisa: '
    TabOrder = 1
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 26
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 13
        Top = 2
        Width = 845
        Height = 16
        Caption = 
          'S'#227'o pesquisados os lan'#231'amentos pelo vencimento (dos codom'#237'nios a' +
          'ssistidos pelo usu'#225'rio logado e pelos condom'#237'nios sem assistente' +
          ' definido).'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label2: TLabel
        Left = 12
        Top = 1
        Width = 845
        Height = 16
        Caption = 
          'S'#227'o pesquisados os lan'#231'amentos pelo vencimento (dos codom'#237'nios a' +
          'ssistidos pelo usu'#225'rio logado e pelos condom'#237'nios sem assistente' +
          ' definido).'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object PageControl1: TPageControl
      Left = 2
      Top = 41
      Width = 1004
      Height = 240
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Alertas'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeDados: TDBGrid
          Left = 0
          Top = 0
          Width = 996
          Height = 212
          Align = alClient
          DataSource = DataSource1
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = GradeDadosDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Empresa'
              Title.Caption = 'Cond.'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DiaAlert'
              Title.Caption = 'Alerta'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DiaVence'
              Title.Caption = 'Venc.'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o da configura'#231#227'o de emiss'#227'o'
              Width = 554
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeMin'
              Title.Caption = 'Q.m'#237'n.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeMax'
              Title.Caption = 'Q.m'#225'x.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeExe'
              Title.Caption = 'Q.real.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValrMin'
              Title.Caption = 'Valor m'#237'n.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValrMax'
              Title.Caption = 'Valor m'#225'x.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValrExe'
              Title.Caption = 'Valor real.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESTATUS'
              Title.Caption = 'Avalia'#231#227'o'
              Width = 300
              Visible = True
            end>
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 392
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Inclui'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 331
    Width = 1008
    Height = 17
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 17
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object PB1: TProgressBar
        Left = 0
        Top = 0
        Width = 1008
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 348
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object Query: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QueryAfterOpen
    BeforeClose = QueryBeforeClose
    OnCalcFields = QueryCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM _conf_pgtos_'
      '/* WHERE Status not IN (3,4) */'
      'WHERE Alertar=1'
      'ORDER BY NomeEnti, Status')
    Left = 216
    Top = 360
    object QueryControle: TIntegerField
      FieldName = 'Controle'
    end
    object QueryEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QueryEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QueryGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QueryNomeCta: TWideStringField
      FieldName = 'NomeCta'
      Size = 100
    end
    object QueryDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QueryQtdeMin: TIntegerField
      FieldName = 'QtdeMin'
    end
    object QueryQtdeMax: TIntegerField
      FieldName = 'QtdeMax'
    end
    object QueryQtdeExe: TIntegerField
      FieldName = 'QtdeExe'
    end
    object QueryValrMin: TFloatField
      FieldName = 'ValrMin'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QueryValrMax: TFloatField
      FieldName = 'ValrMax'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QueryValrExe: TFloatField
      FieldName = 'ValrExe'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QueryDiaAlert: TSmallintField
      FieldName = 'DiaAlert'
      DisplayFormat = '0;-0; '
    end
    object QueryDiaVence: TSmallintField
      FieldName = 'DiaVence'
      DisplayFormat = '0;-0; '
    end
    object QueryStatus: TIntegerField
      FieldName = 'Status'
    end
    object QueryNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 100
      Calculated = True
    end
    object QueryNomeEnti: TWideStringField
      FieldName = 'NomeEnti'
      Size = 100
    end
  end
  object DataSource1: TDataSource
    DataSet = Query
    Left = 244
    Top = 360
  end
  object QrContasMes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'cm.Codigo Genero, cm.Controle, cm.Descricao,  cm.TipMesRef,'
      'cm.PeriodoIni, cm.PeriodoFim, cm.ValorMin, cm.ValorMax, '
      'cm.QtdeMin, cm.QtdeMax, cm.DiaAlert, cm.DiaVence,'
      'eci.CodCliInt, eci.CodEnti, gen.Nome NOMECONTA'
      'FROM contasmes cm'
      'LEFT JOIN contas gen ON gen.Codigo=cm.Codigo'
      'LEFT JOIN cond con ON con.Cliente=cm.CliInt'
      'LEFT JOIN enticliint eci ON eci.CodEnti=cm.CliInt'
      'LEFT JOIN entidades ent ON ent.Codigo=cm.CliInt'
      'WHERE eci.TipoTabLct=1'
      'AND (con.Assistente=0'
      'OR con.Assistente=:P0)'
      'AND (cm.PeriodoIni=0 OR cm.PeriodoIni <= :P1)'
      '/*'
      'AND (cm.PeriodoFim=0 OR PeriodoFim >= :P2)'
      '*/'
      '')
    Left = 272
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrContasMesCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
      Required = True
    end
    object QrContasMesCodEnti: TIntegerField
      FieldName = 'CodEnti'
      Required = True
    end
    object QrContasMesGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrContasMesControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContasMesDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrContasMesPeriodoIni: TIntegerField
      FieldName = 'PeriodoIni'
    end
    object QrContasMesPeriodoFim: TIntegerField
      FieldName = 'PeriodoFim'
    end
    object QrContasMesQtdeMin: TIntegerField
      FieldName = 'QtdeMin'
    end
    object QrContasMesQtdeMax: TIntegerField
      FieldName = 'QtdeMax'
    end
    object QrContasMesValorMin: TFloatField
      FieldName = 'ValorMin'
    end
    object QrContasMesValorMax: TFloatField
      FieldName = 'ValorMax'
    end
    object QrContasMesDiaAlert: TSmallintField
      FieldName = 'DiaAlert'
    end
    object QrContasMesDiaVence: TSmallintField
      FieldName = 'DiaVence'
    end
    object QrContasMesNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrContasMesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrContasMesTipMesRef: TSmallintField
      FieldName = 'TipMesRef'
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    Left = 300
    Top = 360
    object QrPesqQtde: TLargeintField
      FieldName = 'Qtde'
      Required = True
    end
    object QrPesqDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object frxFIN_PLCTA_034_0: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 160
    Top = 360
    Datasets = <
      item
        DataSet = frxDsConfPgtos
        DataSetName = 'frxDsConfPgtos'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object GH1: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 124.724490000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsConfPgtos."Empresa"'
        object Memo5: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsConfPgtos."Empresa"] - [frxDsConfPgtos."NomeEnti"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 62.362204720000000000
        Top = 166.299320000000000000
        Width = 680.315400000000000000
        DataSet = frxDsConfPgtos
        DataSetName = 'frxDsConfPgtos'
        RowCount = 0
        object Memo6: TfrxMemoView
          Width = 680.315400000000000000
          Height = 62.362204724409450000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 3.779530000000000000
          Top = 17.007876460000000000
          Width = 672.756047090000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsConfPgtos
          DataSetName = 'frxDsConfPgtos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o da configura'#231#227'o: [frxDsConfPgtos."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'Status: [frxDsConfPgtos."Status"] - [frxDsConfPgtos."NOMESTATUS"' +
              ']')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 37.795300000000000000
          Top = 34.015770000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'dd Alerta')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 94.488250000000000000
          Top = 34.015770000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'dd Vence')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 170.078850000000000000
          Top = 34.015770000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtd m'#237'n.')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 283.464750000000000000
          Top = 34.015770000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtd real')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 226.771800000000000000
          Top = 34.015770000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtd m'#225'x.')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 359.055350000000000000
          Top = 34.015770000000000000
          Width = 94.488188980000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor m'#237'nimo')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 548.031850000000000000
          Top = 34.015770000000000000
          Width = 94.488188980000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor real')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 453.543600000000000000
          Top = 34.015770000000000000
          Width = 94.488188980000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor m'#225'ximo')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 37.795300000000000000
          Top = 47.244094490000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DiaAlert'
          DataSet = frxDsConfPgtos
          DataSetName = 'frxDsConfPgtos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfPgtos."DiaAlert"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 94.488250000000000000
          Top = 47.244094490000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DiaVence'
          DataSet = frxDsConfPgtos
          DataSetName = 'frxDsConfPgtos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfPgtos."DiaVence"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 170.078850000000000000
          Top = 47.244094490000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'QtdeMin'
          DataSet = frxDsConfPgtos
          DataSetName = 'frxDsConfPgtos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfPgtos."QtdeMin"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 283.464750000000000000
          Top = 47.244094490000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'QtdeExe'
          DataSet = frxDsConfPgtos
          DataSetName = 'frxDsConfPgtos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfPgtos."QtdeExe"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 226.771800000000000000
          Top = 47.244094490000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'QtdeMax'
          DataSet = frxDsConfPgtos
          DataSetName = 'frxDsConfPgtos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfPgtos."QtdeMax"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 359.055350000000000000
          Top = 47.244094490000000000
          Width = 94.488188980000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'ValrMin'
          DataSet = frxDsConfPgtos
          DataSetName = 'frxDsConfPgtos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfPgtos."ValrMin"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 548.031850000000000000
          Top = 47.244094490000000000
          Width = 94.488188980000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'ValrExe'
          DataSet = frxDsConfPgtos
          DataSetName = 'frxDsConfPgtos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfPgtos."ValrExe"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 453.543600000000000000
          Top = 47.244094490000000000
          Width = 94.488188980000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'ValrMax'
          DataSet = frxDsConfPgtos
          DataSetName = 'frxDsConfPgtos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConfPgtos."ValrMax"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 11.338590000000000000
        Top = 253.228510000000000000
        Visible = False
        Width = 680.315400000000000000
      end
      object PageHeader3: TfrxPageHeader
        Height = 43.464586460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo22: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo36: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 124.724426540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date] - [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 124.724426540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 132.283550000000000000
          Top = 18.897650000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Contas Mensais - Alertas de Emiss'#245'es')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118120000000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
        object Memo39: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxDsConfPgtos: TfrxDBDataset
    UserName = 'frxDsConfPgtos'
    CloseDataSource = False
    DataSet = Query
    BCDToCurrency = False
    Left = 188
    Top = 360
  end
  object DsContasMes: TDataSource
    DataSet = QrContasMes
    Left = 272
    Top = 388
  end
end
