unit CtaCfgIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums;

type
  TFmCtaCfgIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    QrLoc: TmySQLQuery;
    CkContinuar: TCheckBox;
    EdGenero: TdmkEditCB;
    CBGenero: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrLocGenero: TIntegerField;
    EdNome: TdmkEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCtaCfgIts: TFmCtaCfgIts;

implementation

uses UnMyObjects, Module, CtaCfgCab, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmCtaCfgIts.BtOKClick(Sender: TObject);
var
  Genero, Codigo, Controle: Integer;
  Nome: String;
begin
  Genero := EdGenero.ValueVariant;
  if MyObjects.FIC(Genero = 0, EdGenero,
  'Informe a conta do plano de contas')  then
    Exit;
  Nome := EdNome.Text;
  Codigo := FmCtaCfgCab.QrCtaCfgCabCodigo.Value;
  //
  if ImgTipo.SQLType = stIns then
  begin
    QrLoc.Close;
    QrLoc.Params[00].AsInteger := Genero;
    QrLoc.Params[01].AsInteger := Codigo;
    UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
    //
    if MyObjects.FIC(QrLoc.RecordCount > 0, EdGenero,
      'Esta conta j� est� cadastrada no grupo selecionado') then
      Exit;
  end;
  //
  Controle := UMyMod.BPGS1I32(
    'ctacfgits', 'Controle', '', '', tsPos, stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ctacfgits', False, [
  'Nome', 'Codigo', 'Genero'], ['Controle'], [
  Nome, Codigo, Genero], [Controle], True) then
  begin
    FmCtaCfgCab.ReopenCtaCfgIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      EdNome.Text := '';
      //
      EdGenero.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmCtaCfgIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtaCfgIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCtaCfgIts.FormCreate(Sender: TObject);
begin
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
end;

procedure TFmCtaCfgIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
