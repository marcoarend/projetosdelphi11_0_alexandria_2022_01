unit CtSub1;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkDBLookupComboBox, dmkEditCB, UnFinanceiro, dmkCheckBox;

type
  TFmCtSub1 = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCtSub1: TMySQLQuery;
    DsCtSub1: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrCtSub1Codigo: TIntegerField;
    QrCtSub1Nome: TWideStringField;
    QrCtSub1Nome2: TWideStringField;
    QrCtSub1Nome3: TWideStringField;
    QrCtSub1ID: TWideStringField;
    QrCtSub1Conta: TIntegerField;
    QrCtSub1OrdemLista: TIntegerField;
    QrCtSub1Sigla: TWideStringField;
    QrCtSub1Antigo: TWideStringField;
    QrCtSub1NotPrntFin: TIntegerField;
    QrCtSub1ImpactEstru: TSmallintField;
    QrCtSub1SPED_COD_CTA_REF: TWideStringField;
    QrCtSub1AnaliSinte: TIntegerField;
    QrCtSub1NO_Conta: TWideStringField;
    Label19: TLabel;
    EdNome2: TdmkEdit;
    Label17: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    EdSigla: TdmkEdit;
    Label11: TLabel;
    Label51: TLabel;
    EdSPED_COD_CTA_REF: TdmkEdit;
    QrContas: TMySQLQuery;
    DsContas: TDataSource;
    Label27: TLabel;
    EdNome3: TdmkEdit;
    Label4: TLabel;
    EdOrdemLista: TdmkEdit;
    EdAntigo: TdmkEdit;
    Label3: TLabel;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    RGAnaliSinte: TdmkRadioGroup;
    CkPermitDeb: TdmkCheckBox;
    CkPermitCre: TdmkCheckBox;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    QrCtSub1PermitCre: TSmallintField;
    QrCtSub1PermitDeb: TSmallintField;
    DBCheckBox3: TDBCheckBox;
    QrCtSub1Ativo: TSmallintField;
    CkAtivo: TdmkCheckBox;
    DBRadioGroup2: TDBRadioGroup;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCtSub1AfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCtSub1BeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure RGAnaliSinteClick(Sender: TObject);
    procedure EdNomeRedefinido(Sender: TObject);
    procedure EdOrdemListaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrCtSub1AfterScroll(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCtSub1: TFmCtSub1;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleFin;

{$R *.DFM}

const
  FNivel = 6; // N�o mexer!!!

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCtSub1.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCtSub1.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCtSub1Codigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCtSub1.DefParams;
begin
  VAR_GOTOTABELA := 'ctsub1';
  VAR_GOTOMYSQLTABLE := QrCtSub1;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cta.Nome NO_Conta, ct1.*  ');
  VAR_SQLx.Add('FROM ctsub1 ct1 ');
  VAR_SQLx.Add('LEFT JOIN contas cta ON cta.Codigo=ct1.Conta ');
  VAR_SQLx.Add('WHERE ct1.Codigo > 0');
  //
  VAR_SQL1.Add('AND ct1.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND ct1.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ct1.Nome Like :P0');
  //
end;

procedure TFmCtSub1.EdNomeRedefinido(Sender: TObject);
begin
  if EdNome2.ValueVariant = '' then
    EdNome2.ValueVariant := EdNome.ValueVariant;
  if EdNome3.ValueVariant = '' then
    EdNome3.ValueVariant := EdNome.ValueVariant;
end;

procedure TFmCtSub1.EdOrdemListaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Conta: Integer;
begin
  if Key = VK_F4 then
  begin
    Conta := QrContasCodigo.Value;
    if MyObjects.FIC(Conta = 0, EdConta, 'Infome o N�vel 5 primeiro!') then Exit;
    EdOrdemLista.ValueVariant :=
      UFinanceiro.ObtemProximaOrdemNoNivelDoPlanoDeContas('ctsub1', 'OrdemLista',
    'Conta', Conta);
  end;
end;

procedure TFmCtSub1.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCtSub1.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCtSub1.RGAnaliSinteClick(Sender: TObject);
begin
  if RGAnaliSinte.ItemIndex <> 2 then
    RGAnaliSinte.ItemIndex := 2;
end;

procedure TFmCtSub1.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCtSub1.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCtSub1.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCtSub1.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCtSub1.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCtSub1.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtSub1.BtAlteraClick(Sender: TObject);
begin
  if (QrCtSub1.State <> dsInactive) and (QrCtSub1.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCtSub1, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'ctsub1');
    RGAnaliSinte.ItemIndex := 2;
  end;
end;

procedure TFmCtSub1.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCtSub1Codigo.Value;
  Close;
end;

procedure TFmCtSub1.BtConfirmaClick(Sender: TObject);
var
  Nome, Nome2, Nome3, (*ID,*) Sigla, Antigo, SPED_COD_CTA_REF: String;
  Codigo, Conta, OrdemLista, (*NotPrntFin,*) ImpactEstru, AnaliSinte,
  PermitDeb, PermitCre, Ativo: Integer;
  SQLType: TSQLType;
begin
  SQLType          := ImgTipo.SQLType;
  Codigo           := EdCodigo.ValueVariant;
  Nome             := EdNome.ValueVariant;
  Nome2            := EdNome2.ValueVariant;
  Nome3            := EdNome3.ValueVariant;
  //ID               := ''; //EdID.ValueVariant;
  Conta            := EdConta.ValueVariant;
  OrdemLista       := EdOrdemLista.ValueVariant;
  Sigla            := EdSigla.ValueVariant;
  Antigo           := EdAntigo.ValueVariant;
  //NotPrntFin       := EdNotPrntFin.ValueVariant;
  //Impacto na Estrutura da empresa
  //0: Patrimonial, 1: Resultado
  ImpactEstru      := -1;
  SPED_COD_CTA_REF := EdSPED_COD_CTA_REF.ValueVariant;
  AnaliSinte       := RGAnaliSinte.ItemIndex;
  PermitDeb        := Geral.BoolToInt(CkPermitDeb.Checked);
  PermitCre        := Geral.BoolToInt(CkPermitCre.Checked);
  Ativo            := Geral.BoolToInt(CkAtivo.Checked);
  //
  if MyObjects.FIC(Nome = EmptyStr, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(Nome2) = 0, EdNome2, 'Defina uma descri��o substituta!') then Exit;
  if MyObjects.FIC(Conta = 0, EdConta, 'Defina o N�vel 5!') then Exit;
  if MyObjects.FIC(Length(Sigla) = 0, EdSigla, 'Defina uma sigla!') then Exit;
  if MyObjects.FIC(AnaliSinte = 0, RGAnaliSinte, 'Informe o idicador de tipo de conta.') then Exit;
  //
  //
  //Codigo := UMyMod.BPGS1I32('ctsub1', 'Codigo', '', '', tsPos, SQLType, Codigo);
  Codigo := UFinanceiro.ObtemProximoPlaFluCad(SQLType, Codigo, FNivel);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctsub1', False, [
  'Nome', 'Nome2', 'Nome3',
  (*'ID',*) 'Conta', 'OrdemLista',
  'Sigla', 'Antigo', (*'NotPrntFin',*)
  'ImpactEstru', 'SPED_COD_CTA_REF', 'AnaliSinte',
  'PermitDeb', 'PermitCre', 'Ativo'], [
  'Codigo'], [
  Nome, Nome2, Nome3,
  (*ID,*) Conta, OrdemLista,
  Sigla, Antigo, (*NotPrntFin,*)
  ImpactEstru, SPED_COD_CTA_REF, AnaliSinte,
  PermitDeb, PermitCre, Ativo], [
  Codigo], True) then
  begin
    DModFin.AtualizaPlaAllCad();
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCtSub1.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ctsub1', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCtSub1.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCtSub1, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ctsub1');
  RGAnaliSinte.ItemIndex := 2;
end;

procedure TFmCtSub1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
end;

procedure TFmCtSub1.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCtSub1Codigo.Value, LaRegistro.Caption);
end;

procedure TFmCtSub1.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCtSub1.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCtSub1Codigo.Value, LaRegistro.Caption);
end;

procedure TFmCtSub1.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCtSub1.QrCtSub1AfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCtSub1.QrCtSub1AfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrCtSub1Codigo.Value > 0;
end;

procedure TFmCtSub1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCtSub1.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCtSub1Codigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ctsub1', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCtSub1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtSub1.QrCtSub1BeforeOpen(DataSet: TDataSet);
begin
  QrCtSub1Codigo.DisplayFormat := FFormatFloat;
end;

end.

