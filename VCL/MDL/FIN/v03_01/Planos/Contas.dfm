object FmContas: TFmContas
  Left = 370
  Top = 194
  Caption = 'FIN-PLCTA-005 :: Cadastro de N'#237'vel 5 do Plano de Contas'
  ClientHeight = 613
  ClientWidth = 1021
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 78
    Width = 1021
    Height = 535
    Align = alClient
    BevelOuter = bvNone
    Color = clHighlight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 248
    ExplicitTop = 74
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 1021
      Height = 401
      Align = alTop
      TabOrder = 0
      object Label14: TLabel
        Left = 16
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label15: TLabel
        Left = 78
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label19: TLabel
        Left = 326
        Top = 4
        Width = 99
        Height = 13
        Caption = 'Descri'#231#227'o substituta:'
      end
      object Label17: TLabel
        Left = 16
        Top = 44
        Width = 38
        Height = 13
        Caption = 'N'#237'vel 4:'
      end
      object Label11: TLabel
        Left = 326
        Top = 44
        Width = 26
        Height = 13
        Caption = 'Sigla:'
      end
      object SBSubgrupo: TSpeedButton
        Left = 298
        Top = 59
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = SBSubgrupoClick
      end
      object Label51: TLabel
        Left = 14
        Top = 106
        Width = 265
        Height = 13
        Caption = 'Conta correl. no Plano de Contas Referenciado da RFB:'
      end
      object Label52: TLabel
        Left = 286
        Top = 107
        Width = 55
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem: [F4]'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 20
        Width = 60
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 78
        Top = 20
        Width = 242
        Height = 21
        Color = clWhite
        MaxLength = 50
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdNomeExit
      end
      object EdNome2: TdmkEdit
        Left = 326
        Top = 20
        Width = 304
        Height = 21
        Color = clWhite
        MaxLength = 50
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CBSubgrupo: TdmkDBLookupComboBox
        Left = 78
        Top = 60
        Width = 218
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSubGrupos
        TabOrder = 4
        dmkEditCB = EdSubgrupo
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdSubgrupo: TdmkEditCB
        Left = 16
        Top = 60
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdSubgrupoChange
        DBLookupComboBox = CBSubgrupo
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object PageControl2: TPageControl
        Left = 1
        Top = 147
        Width = 1019
        Height = 253
        ActivePage = TabSheet5
        Align = alBottom
        TabOrder = 13
        object TabSheet5: TTabSheet
          Caption = 'Mensal'
          object Panel15: TPanel
            Left = 0
            Top = 0
            Width = 1011
            Height = 277
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object CBMensal: TCheckBox
              Left = 12
              Top = 10
              Width = 60
              Height = 16
              Caption = 'Mensal'
              TabOrder = 0
              OnClick = CBMensalClick
            end
          end
        end
        object TabSheet12: TTabSheet
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Caption = 'Outros dados'
          ImageIndex = 4
          object Panel30: TPanel
            Left = 0
            Top = 0
            Width = 1011
            Height = 225
            Margins.Left = 2
            Margins.Top = 2
            Margins.Right = 2
            Margins.Bottom = 2
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label27: TLabel
              Left = 5
              Top = 24
              Width = 153
              Height = 13
              Caption = 'Descri'#231#227'o padr'#227'o para hist'#243'rico:'
            end
            object EdNome3: TdmkEdit
              Left = 5
              Top = 40
              Width = 325
              Height = 21
              Color = clWhite
              MaxLength = 50
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object CBExclusivo: TCheckBox
              Left = 8
              Top = 4
              Width = 76
              Height = 17
              Caption = 'Exclusivo.'
              TabOrder = 0
            end
            object CkTemDocFisi: TCheckBox
              Left = 96
              Top = 4
              Width = 253
              Height = 17
              Caption = 'Tem documento f'#237'sico? (Exige Foto ou XML).'
              TabOrder = 1
            end
            object RGCentroRes: TRadioGroup
              Left = 337
              Top = 4
              Width = 275
              Height = 37
              Caption = ' Centro de Resultado: '
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                'Automatizado'
                'Definitivo'
                'Acumulativo')
              TabOrder = 3
            end
            object GroupBox3: TGroupBox
              Left = 5
              Top = 65
              Width = 607
              Height = 127
              Caption = '    '
              TabOrder = 4
              object Label16: TLabel
                Left = 8
                Top = 20
                Width = 96
                Height = 13
                Caption = 'Dia da mensalidade:'
              end
              object Label21: TLabel
                Left = 114
                Top = 20
                Width = 117
                Height = 13
                Caption = 'D'#233'bito mensal esperado:'
              end
              object Label22: TLabel
                Left = 236
                Top = 20
                Width = 107
                Height = 13
                Caption = 'D'#233'bito mensal m'#237'nimo:'
              end
              object Label23: TLabel
                Left = 358
                Top = 20
                Width = 119
                Height = 13
                Caption = 'Cr'#233'dito mensal esperado:'
              end
              object Label24: TLabel
                Left = 481
                Top = 20
                Width = 109
                Height = 13
                Caption = 'Cr'#233'dito mensal m'#237'nimo:'
              end
              object EdMensDia: TdmkEdit
                Left = 8
                Top = 35
                Width = 103
                Height = 21
                Alignment = taRightJustify
                MaxLength = 2
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnExit = EdMensDiaExit
              end
              object EdMensdeb: TdmkEdit
                Left = 114
                Top = 35
                Width = 120
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object Edmensmind: TdmkEdit
                Left = 236
                Top = 35
                Width = 119
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdMenscred: TdmkEdit
                Left = 358
                Top = 35
                Width = 120
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdMensminc: TdmkEdit
                Left = 481
                Top = 35
                Width = 119
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object CkCalculMesSeg: TCheckBox
                Left = 8
                Top = 102
                Width = 241
                Height = 17
                Caption = 'Pendente somente ap'#243's encerramento do m'#234's.'
                Enabled = False
                TabOrder = 6
              end
              object RGPendenMesSeg: TRadioGroup
                Left = 8
                Top = 59
                Width = 592
                Height = 40
                Caption = ' C'#225'lculo da pend'#234'ncia: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'Meses futuros n'#227'o considerar'
                  'Considerar sempre'
                  'Proporcional na data atual.')
                TabOrder = 5
              end
            end
            object CkUsoNoERP: TCheckBox
              Left = 279
              Top = 166
              Width = 169
              Height = 17
              Caption = 'Dispon'#237'vel em todo ERP.'
              TabOrder = 5
            end
          end
        end
        object TabSheet6: TTabSheet
          Caption = 'Extrato'
          ImageIndex = 1
          object Panel16: TPanel
            Left = 0
            Top = 0
            Width = 1011
            Height = 225
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label41: TLabel
              Left = 8
              Top = 8
              Width = 168
              Height = 13
              Caption = 'Agrupamento de contas em extrato:'
            end
            object Label18: TLabel
              Left = 8
              Top = 90
              Width = 193
              Height = 13
              Caption = 'Identificador autom'#225'tico em importa'#231#245'es:'
              Enabled = False
            end
            object EdContasAgr: TdmkEditCB
              Left = 8
              Top = 24
              Width = 60
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBContasAgr
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBContasAgr: TdmkDBLookupComboBox
              Left = 71
              Top = 24
              Width = 335
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContasAgr
              TabOrder = 1
              dmkEditCB = EdContasAgr
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdID: TdmkEdit
              Left = 8
              Top = 106
              Width = 300
              Height = 21
              Color = clWhite
              Enabled = False
              MaxLength = 50
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object CkContasSum: TCheckBox
              Left = 8
              Top = 63
              Width = 261
              Height = 17
              Caption = 'Soma os itens iguais no extrato.'
              TabOrder = 2
            end
          end
        end
        object TabSheet7: TTabSheet
          Caption = 'Espec'#237'ficos'
          ImageIndex = 2
          object Panel24: TPanel
            Left = 0
            Top = 0
            Width = 1011
            Height = 225
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label20: TLabel
              Left = 12
              Top = 8
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label29: TLabel
              Left = 12
              Top = 47
              Width = 78
              Height = 13
              Caption = 'Centro de custo:'
            end
            object Label30: TLabel
              Left = 319
              Top = 8
              Width = 45
              Height = 13
              Caption = 'Entidade:'
            end
            object Label25: TLabel
              Left = 382
              Top = 47
              Width = 107
              Height = 13
              Caption = 'Espec'#237'fico de terceiro:'
              Enabled = False
            end
            object Label26: TLabel
              Left = 319
              Top = 47
              Width = 60
              Height = 13
              Caption = 'C'#233'lula excel:'
            end
            object EdEmpresa: TdmkEditCB
              Left = 12
              Top = 24
              Width = 60
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEmpresa
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdCentroCusto: TdmkEditCB
              Left = 12
              Top = 63
              Width = 60
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCentroCusto
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCentroCusto: TdmkDBLookupComboBox
              Left = 75
              Top = 63
              Width = 241
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCentroCusto
              TabOrder = 5
              dmkEditCB = EdCentroCusto
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object CBEmpresa: TdmkDBLookupComboBox
              Left = 75
              Top = 24
              Width = 241
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsEpresas
              TabOrder = 1
              dmkEditCB = EdEmpresa
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdEntidade: TdmkEditCB
              Left = 319
              Top = 24
              Width = 60
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEntidade
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBEntidade: TdmkDBLookupComboBox
              Left = 382
              Top = 24
              Width = 241
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsEntidades
              TabOrder = 3
              dmkEditCB = EdEntidade
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdExcel: TdmkEdit
              Left = 319
              Top = 63
              Width = 60
              Height = 21
              CharCase = ecUpperCase
              Color = clWhite
              MaxLength = 6
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnExit = EdNomeExit
            end
            object CBTerceiro: TDBLookupComboBox
              Left = 382
              Top = 63
              Width = 241
              Height = 21
              Color = clWhite
              Enabled = False
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsTerceiros
              TabOrder = 7
            end
            object CGNotPrntBal: TdmkCheckGroup
              Left = 12
              Top = 130
              Width = 611
              Height = 44
              Caption = ' N'#227'o imprimir nos seguintes modelos de balancete: '
              Items.Strings = (
                'Balancete de resultado industrial modelo 01')
              TabOrder = 8
              UpdType = utYes
              Value = 0
              OldValor = 0
            end
            object CGNotPrntFin: TdmkCheckGroup
              Left = 12
              Top = 178
              Width = 611
              Height = 44
              Caption = ' Ocultar nos seguintes relat'#243'rios financeiros: '
              Items.Strings = (
                'Pesquisa por N'#237'vel do Plano de Contas - Modelo 2012')
              TabOrder = 9
              UpdType = utYes
              Value = 0
              OldValor = 0
            end
          end
        end
        object TabSheet11: TTabSheet
          Caption = 'CRO'
          ImageIndex = 3
          object Panel28: TPanel
            Left = 0
            Top = 0
            Width = 1011
            Height = 225
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox5: TGroupBox
              Left = 4
              Top = 4
              Width = 355
              Height = 64
              Caption = ' CRO - Custos e Resultados Operacionais: '
              TabOrder = 0
              object Panel29: TPanel
                Left = 2
                Top = 15
                Width = 351
                Height = 47
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label50: TLabel
                  Left = 4
                  Top = 4
                  Width = 57
                  Height = 13
                  Caption = 'Conta CRO:'
                end
                object EdCROCon: TdmkEditCB
                  Left = 4
                  Top = 20
                  Width = 60
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBCroCon
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBCroCon: TdmkDBLookupComboBox
                  Left = 67
                  Top = 20
                  Width = 273
                  Height = 21
                  Color = clWhite
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsCROCon
                  TabOrder = 1
                  dmkEditCB = EdCROCon
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                  LocF7PreDefProc = f7pNone
                end
              end
            end
          end
        end
      end
      object EdSigla: TdmkEdit
        Left = 326
        Top = 60
        Width = 304
        Height = 21
        Color = clWhite
        MaxLength = 15
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdNomeExit
      end
      object CBDebito: TCheckBox
        Left = 16
        Top = 86
        Width = 68
        Height = 17
        Caption = 'D'#233'bito.'
        TabOrder = 6
      end
      object CBCredito: TCheckBox
        Left = 93
        Top = 86
        Width = 68
        Height = 17
        Caption = 'Cr'#233'dito.'
        TabOrder = 7
      end
      object CkAtivo: TCheckBox
        Left = 170
        Top = 86
        Width = 68
        Height = 17
        Caption = 'Ativo.'
        Checked = True
        State = cbChecked
        TabOrder = 8
      end
      object CkCtrlaSdo: TCheckBox
        Left = 245
        Top = 86
        Width = 100
        Height = 17
        Caption = 'Controla saldo.'
        TabOrder = 9
      end
      object Grade: TStringGrid
        Left = 674
        Top = 8
        Width = 87
        Height = 68
        ColCount = 1
        DefaultColWidth = 51
        DefaultRowHeight = 14
        FixedCols = 0
        RowCount = 3
        FixedRows = 0
        TabOrder = 14
        Visible = False
      end
      object EdSPED_COD_CTA_REF: TdmkEdit
        Left = 14
        Top = 122
        Width = 267
        Height = 21
        Color = clWhite
        MaxLength = 15
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdNomeExit
      end
      object EdOrdemLista: TdmkEdit
        Left = 285
        Top = 122
        Width = 95
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdOrdemListaKeyDown
      end
      object RGAnaliSinte: TdmkRadioGroup
        Left = 384
        Top = 104
        Width = 201
        Height = 41
        Caption = 'Indicador do tipo de conta:'
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Indef.'
          'Sint'#233'tico'
          'Anal'#237'tico')
        TabOrder = 12
        QryCampo = 'AnaliSinte'
        UpdCampo = 'AnaliSinte'
        UpdType = utYes
        OldValor = 0
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 463
      Width = 1021
      Height = 72
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 16
        Top = 16
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel19: TPanel
        Left = 865
        Top = 15
        Width = 154
        Height = 55
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 0
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 78
    Width = 1021
    Height = 535
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 796
      Height = 520
      BevelOuter = bvNone
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 796
        Height = 520
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        OnChange = PageControl1Change
        object TabSheet1: TTabSheet
          Caption = 'Dados'
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 788
            Height = 492
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label1: TLabel
              Left = 59
              Top = 4
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
              FocusControl = DBEdCodigo
            end
            object Label2: TLabel
              Left = 122
              Top = 4
              Width = 51
              Height = 13
              Caption = 'Descri'#231#227'o:'
              FocusControl = DBEdNome
            end
            object Label3: TLabel
              Left = 59
              Top = 64
              Width = 99
              Height = 13
              Caption = 'Descri'#231#227'o substituta:'
              FocusControl = DBEdNome2
            end
            object Label13: TLabel
              Left = 366
              Top = 64
              Width = 153
              Height = 13
              Caption = 'Descri'#231#227'o padr'#227'o para hist'#243'rico:'
            end
            object LaSubGrupo: TLabel
              Left = 59
              Top = 104
              Width = 38
              Height = 13
              Caption = 'N'#237'vel 4:'
            end
            object Label53: TLabel
              Left = 366
              Top = 104
              Width = 23
              Height = 13
              Caption = 'Sigla'
              FocusControl = DBEdit4
            end
            object Label54: TLabel
              Left = 260
              Top = 144
              Width = 265
              Height = 13
              Caption = 'Conta correl. no Plano de Contas Referenciado da RFB:'
            end
            object DBEdCodigo: TDBEdit
              Left = 59
              Top = 20
              Width = 60
              Height = 21
              Hint = 'N'#186' do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Codigo'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 8281908
              Font.Height = -9
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
            end
            object DBEdNome: TDBEdit
              Left = 122
              Top = 20
              Width = 548
              Height = 21
              Hint = 'Nome do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Nome'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -9
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
            end
            object DBCBDebito: TDBCheckBox
              Left = 59
              Top = 44
              Width = 52
              Height = 16
              TabStop = False
              Caption = 'D'#233'bito.'
              DataField = 'Debito'
              DataSource = DsContas
              TabOrder = 2
              ValueChecked = 'V'
              ValueUnchecked = 'F'
            end
            object DBCBCredito: TDBCheckBox
              Left = 114
              Top = 44
              Width = 60
              Height = 16
              TabStop = False
              Caption = 'Cr'#233'dito.'
              DataField = 'Credito'
              DataSource = DsContas
              TabOrder = 3
              ValueChecked = 'V'
              ValueUnchecked = 'F'
            end
            object DBCBExclusivo: TDBCheckBox
              Left = 174
              Top = 44
              Width = 65
              Height = 16
              TabStop = False
              Caption = 'Exclusivo.'
              DataField = 'Exclusivo'
              DataSource = DsContas
              TabOrder = 4
              ValueChecked = 'V'
              ValueUnchecked = 'F'
            end
            object DBCBAtivo: TDBCheckBox
              Left = 244
              Top = 44
              Width = 66
              Height = 16
              TabStop = False
              Caption = 'Ativo.'
              DataField = 'Ativo'
              DataSource = DsContas
              TabOrder = 5
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBEdNome2: TDBEdit
              Left = 59
              Top = 80
              Width = 304
              Height = 21
              Hint = 'Nome do banco'
              TabStop = False
              DataField = 'Nome2'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -9
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 6
            end
            object DBEdNome3: TDBEdit
              Left = 366
              Top = 80
              Width = 300
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'Nome3'
              DataSource = DsContas
              TabOrder = 7
            end
            object DBEdSubgrupo: TDBEdit
              Left = 59
              Top = 120
              Width = 304
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'NOMESUBGRUPO'
              DataSource = DsContas
              TabOrder = 8
            end
            object DBCheckBox1: TDBCheckBox
              Left = 295
              Top = 44
              Width = 96
              Height = 16
              Caption = 'Controla saldo.'
              DataField = 'CtrlaSdo'
              DataSource = DsContas
              TabOrder = 9
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBRadioGroup4: TDBRadioGroup
              Left = 56
              Top = 144
              Width = 201
              Height = 41
              Caption = 'Indicador do tipo de conta:'
              Columns = 3
              DataField = 'AnaliSinte'
              DataSource = DsContas
              Items.Strings = (
                'Indef.'
                'Sint'#233'tico'
                'Anal'#237'tico')
              TabOrder = 10
              Values.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9')
            end
            object DBEdit4: TDBEdit
              Left = 366
              Top = 120
              Width = 299
              Height = 21
              DataField = 'Sigla'
              DataSource = DsContas
              TabOrder = 11
            end
            object DBEdit6: TDBEdit
              Left = 260
              Top = 160
              Width = 268
              Height = 21
              DataField = 'SPED_COD_CTA_REF'
              DataSource = DsContas
              TabOrder = 12
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Localiza'
          ImageIndex = 1
          object Panel13: TPanel
            Left = 0
            Top = 0
            Width = 716
            Height = 45
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label40: TLabel
              Left = 4
              Top = 4
              Width = 51
              Height = 13
              Caption = 'Descri'#231#227'o:'
            end
            object EdLocate: TdmkEdit
              Left = 3
              Top = 20
              Width = 360
              Height = 17
              MaxLength = 30
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdLocateChange
              OnKeyDown = EdLocateKeyDown
            end
          end
          object DBGrid4: TDBGrid
            Left = 0
            Top = 45
            Width = 716
            Height = 447
            Align = alClient
            DataSource = DsLocate
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -9
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGrid4DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 144
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESUBGRUPO'
                Title.Caption = 'Subgrupo'
                Width = 112
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEEMPRESA'
                Title.Caption = 'Empresa'
                Width = 112
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETERCEIRO'
                Title.Caption = 'Terceiro'
                Width = 112
                Visible = True
              end>
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Mensalidades pr'#233'-estipuladas'
          ImageIndex = 2
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 788
            Height = 99
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitWidth = 716
            object DBGrid1: TDBGrid
              Left = 232
              Top = 39
              Width = 484
              Height = 60
              Align = alClient
              DataSource = DsContasItsCtas
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -9
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Contas dependentes'
                  Width = 359
                  Visible = True
                end>
            end
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 716
              Height = 39
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object Label37: TLabel
                Left = 291
                Top = 3
                Width = 36
                Height = 13
                Caption = 'C'#243'digo:'
                FocusControl = EdDBCodigo
              end
              object Label38: TLabel
                Left = 354
                Top = 3
                Width = 51
                Height = 13
                Caption = 'Descri'#231#227'o:'
                FocusControl = EdDBDescricao
              end
              object BtInsere: TBitBtn
                Tag = 10
                Left = 4
                Top = 4
                Width = 89
                Height = 32
                Cursor = crHandPoint
                Hint = 'Inclui novo registro'
                Caption = 'I&nsere'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtInsereClick
              end
              object BtExcluiIts: TBitBtn
                Tag = 12
                Left = 98
                Top = 4
                Width = 89
                Height = 32
                Cursor = crHandPoint
                Hint = 'Exclui registro atual'
                Caption = 'E&xclui'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtExcluiItsClick
              end
              object EdDBCodigo: TDBEdit
                Left = 291
                Top = 16
                Width = 60
                Height = 21
                Hint = 'N'#186' do banco'
                TabStop = False
                Color = clWhite
                DataField = 'Codigo'
                DataSource = DsContas
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 8281908
                Font.Height = -9
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 2
              end
              object EdDBDescricao: TDBEdit
                Left = 354
                Top = 16
                Width = 352
                Height = 21
                Hint = 'Nome do banco'
                TabStop = False
                Color = clWhite
                DataField = 'Nome'
                DataSource = DsContas
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -9
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 3
              end
              object BtAlteraIts: TBitBtn
                Tag = 11
                Left = 193
                Top = 4
                Width = 89
                Height = 32
                Cursor = crHandPoint
                Hint = 'Exclui registro atual'
                Caption = '&Altera'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 4
                OnClick = BtAlteraItsClick
              end
            end
            object DBGrid3: TDBGrid
              Left = 0
              Top = 39
              Width = 232
              Height = 60
              Align = alLeft
              DataSource = DsContasIts
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 2
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -9
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOMEPERIODO'
                  Title.Caption = 'M'#234's'
                  Width = 73
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'ESTIPULADO'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Estipulado'
                  Width = 84
                  Visible = True
                end>
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 99
            Width = 788
            Height = 393
            Align = alClient
            ParentBackground = False
            TabOrder = 1
            Visible = False
            object Panel9: TPanel
              Left = 1
              Top = 350
              Width = 786
              Height = 42
              Align = alBottom
              TabOrder = 0
              ExplicitWidth = 714
              object BtConfirma2: TBitBtn
                Tag = 14
                Left = 8
                Top = 4
                Width = 89
                Height = 32
                Cursor = crHandPoint
                Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
                Caption = '&Confirma'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -9
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtConfirma2Click
              end
              object BtDesiste2: TBitBtn
                Tag = 15
                Left = 614
                Top = 4
                Width = 89
                Height = 32
                Cursor = crHandPoint
                Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
                Caption = '&Desiste'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -9
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtDesiste2Click
              end
            end
            object Panel10: TPanel
              Left = 1
              Top = 1
              Width = 268
              Height = 349
              Align = alLeft
              TabOrder = 1
              object Label36: TLabel
                Left = 103
                Top = 122
                Width = 86
                Height = 13
                Caption = 'Fator (Valor ou %):'
              end
              object GroupBox1: TGroupBox
                Left = 1
                Top = 1
                Width = 260
                Height = 48
                Caption = ' Per'#237'odo Inicial: '
                TabOrder = 0
                object Label32: TLabel
                  Left = 4
                  Top = 11
                  Width = 23
                  Height = 13
                  Caption = 'M'#234's:'
                end
                object Label33: TLabel
                  Left = 178
                  Top = 11
                  Width = 22
                  Height = 13
                  Caption = 'Ano:'
                end
                object CBMesI: TComboBox
                  Left = 5
                  Top = 24
                  Width = 169
                  Height = 21
                  Style = csDropDownList
                  Color = clWhite
                  DropDownCount = 12
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 7622183
                  Font.Height = -9
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 0
                end
                object CBAnoI: TComboBox
                  Left = 176
                  Top = 24
                  Width = 77
                  Height = 21
                  Style = csDropDownList
                  Color = clWhite
                  DropDownCount = 3
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 7622183
                  Font.Height = -9
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 1
                end
              end
              object GroupBox2: TGroupBox
                Left = 1
                Top = 48
                Width = 260
                Height = 46
                Caption = ' Per'#237'odo final: '
                TabOrder = 1
                object Label34: TLabel
                  Left = 4
                  Top = 11
                  Width = 23
                  Height = 13
                  Caption = 'M'#234's:'
                end
                object Label35: TLabel
                  Left = 178
                  Top = 11
                  Width = 22
                  Height = 13
                  Caption = 'Ano:'
                end
                object CBMesF: TComboBox
                  Left = 5
                  Top = 24
                  Width = 169
                  Height = 21
                  Style = csDropDownList
                  Color = clWhite
                  DropDownCount = 12
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 7622183
                  Font.Height = -9
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 0
                end
                object CBAnoF: TComboBox
                  Left = 176
                  Top = 24
                  Width = 77
                  Height = 21
                  Style = csDropDownList
                  Color = clWhite
                  DropDownCount = 3
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 7622183
                  Font.Height = -9
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 1
                end
              end
              object RGTipo: TRadioGroup
                Left = 1
                Top = 95
                Width = 98
                Height = 63
                Caption = ' Tipo de C'#225'culo: '
                ItemIndex = 0
                Items.Strings = (
                  'Valor'
                  'Porcentagem')
                TabOrder = 2
              end
              object EdFator: TdmkEdit
                Left = 103
                Top = 138
                Width = 87
                Height = 19
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
            object Panel11: TPanel
              Left = 269
              Top = 1
              Width = 518
              Height = 349
              Align = alClient
              TabOrder = 2
              ExplicitWidth = 446
              object Panel12: TPanel
                Left = 1
                Top = 1
                Width = 444
                Height = 38
                Align = alTop
                TabOrder = 0
                object BtIncluiIts: TBitBtn
                  Tag = 10
                  Left = 4
                  Top = 4
                  Width = 89
                  Height = 32
                  Cursor = crHandPoint
                  Hint = 'Inclui novo registro'
                  Caption = '&1. Insere'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtIncluiItsClick
                end
                object BtExclui2: TBitBtn
                  Tag = 12
                  Left = 185
                  Top = 4
                  Width = 89
                  Height = 32
                  Cursor = crHandPoint
                  Hint = 'Exclui registro atual'
                  Caption = '&2. Exclui'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BtExclui2Click
                end
              end
              object DBGrid2: TDBGrid
                Left = 1
                Top = 39
                Width = 444
                Height = 309
                Align = alClient
                DataSource = DsCtas
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -9
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Conta'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Width = 280
                    Visible = True
                  end>
              end
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Lan'#231'amentos'
          ImageIndex = 3
          object Panel14: TPanel
            Left = 0
            Top = 0
            Width = 788
            Height = 39
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            Visible = False
            object Label39: TLabel
              Left = 4
              Top = 3
              Width = 250
              Height = 13
              Caption = 'Conta para a qual ser'#227'o transferidos os lan'#231'amentos.'
            end
            object BtReciclo: TBitBtn
              Left = 614
              Top = 4
              Width = 89
              Height = 32
              Hint = 'Sub-grupos|Cadastro de sub-grupos de contas'
              Caption = '&Tranferir'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtRecicloClick
            end
            object EdNova: TdmkEditCB
              Left = 4
              Top = 16
              Width = 64
              Height = 17
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdNovaChange
              DBLookupComboBox = CBNova
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBNova: TdmkDBLookupComboBox
              Left = 71
              Top = 16
              Width = 532
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsNova
              TabOrder = 2
              dmkEditCB = EdNova
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
          object DBGLct: TDBGrid
            Left = 0
            Top = 39
            Width = 788
            Height = 453
            Align = alClient
            DataSource = DsLct
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -9
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
            Columns = <
              item
                Expanded = False
                FieldName = 'Data'
                Width = 41
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Documento'
                Title.Caption = 'Cheque'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Descri'#231#227'o'
                Width = 131
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debito'
                Title.Caption = 'D'#233'bito'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Credito'
                Title.Caption = 'Cr'#233'dito'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Title.Caption = 'Vencim.'
                Width = 41
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'COMPENSADO_TXT'
                Title.Caption = 'Compen.'
                Width = 41
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MENSAL'
                Title.Caption = 'M'#234's'
                Width = 30
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESIT'
                Title.Caption = 'Situa'#231#227'o'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'Lan'#231'to'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Sub'
                Width = 19
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SALDO'
                Title.Caption = 'Saldo'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECLIENTE'
                Title.Caption = 'Membro'
                Width = 219
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEFORNECEDOR'
                Title.Caption = 'Fornecedor'
                Width = 243
                Visible = True
              end>
          end
        end
        object TabSheet8: TTabSheet
          Caption = 'Dados espec'#237'ficos de entidades'
          ImageIndex = 4
          object Panel18: TPanel
            Left = 0
            Top = 278
            Width = 788
            Height = 167
            Align = alBottom
            ParentBackground = False
            TabOrder = 0
            Visible = False
            object Label43: TLabel
              Left = 4
              Top = 4
              Width = 279
              Height = 13
              Caption = 'Entidade (que possui pelo menos uma carteira cadastrada):'
            end
            object Label42: TLabel
              Left = 4
              Top = 44
              Width = 137
              Height = 13
              Caption = 'Contrato de d'#233'bito em conta:'
            end
            object Label12: TLabel
              Left = 4
              Top = 84
              Width = 247
              Height = 13
              Caption = 'C'#243'digo da conta do terceiro (Exporta'#231#227'o de dados) :'
            end
            object EdEnti2: TdmkEditCB
              Left = 4
              Top = 20
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEnti2
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBEnti2: TdmkDBLookupComboBox
              Left = 51
              Top = 20
              Width = 332
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENT'
              ListSource = DsEnti2
              TabOrder = 1
              dmkEditCB = EdEnti2
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdCntrDebCta: TdmkEdit
              Left = 4
              Top = 60
              Width = 379
              Height = 21
              Color = clWhite
              MaxLength = 60
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCodExporta: TdmkEdit
              Left = 4
              Top = 100
              Width = 379
              Height = 21
              Color = clWhite
              MaxLength = 60
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'CodExporta'
              UpdCampo = 'CodExporta'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object Panel17: TPanel
            Left = 0
            Top = 0
            Width = 788
            Height = 39
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Label44: TLabel
              Left = 291
              Top = 3
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
              FocusControl = EdDBCodigo2
            end
            object Label45: TLabel
              Left = 354
              Top = 3
              Width = 51
              Height = 13
              Caption = 'Descri'#231#227'o:'
              FocusControl = EdDBDescricao2
            end
            object EdDBCodigo2: TDBEdit
              Left = 291
              Top = 16
              Width = 60
              Height = 21
              Hint = 'N'#186' do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Codigo'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 8281908
              Font.Height = -9
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
            end
            object EdDBDescricao2: TDBEdit
              Left = 354
              Top = 16
              Width = 352
              Height = 21
              Hint = 'Nome do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Nome'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -9
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
            end
            object Panel21: TPanel
              Left = 0
              Top = 0
              Width = 287
              Height = 39
              Align = alLeft
              BevelOuter = bvNone
              Caption = 'Panel21'
              TabOrder = 2
              object BitBtn3: TBitBtn
                Tag = 12
                Left = 98
                Top = 4
                Width = 89
                Height = 32
                Cursor = crHandPoint
                Hint = 'Exclui registro atual'
                Caption = 'E&xclui'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
              object BitBtn4: TBitBtn
                Tag = 11
                Left = 193
                Top = 4
                Width = 89
                Height = 32
                Cursor = crHandPoint
                Hint = 'Exclui registro atual'
                Caption = '&Altera'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BitBtn4Click
              end
              object BitBtn2: TBitBtn
                Tag = 10
                Left = 4
                Top = 4
                Width = 89
                Height = 32
                Cursor = crHandPoint
                Hint = 'Inclui novo registro'
                Caption = 'I&nsere'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BitBtn2Click
              end
            end
          end
          object Panel20: TPanel
            Left = 0
            Top = 445
            Width = 788
            Height = 47
            Align = alBottom
            ParentBackground = False
            TabOrder = 2
            Visible = False
            object BitBtn1: TBitBtn
              Tag = 14
              Left = 8
              Top = 4
              Width = 89
              Height = 39
              Cursor = crHandPoint
              Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
              Caption = '&Confirma'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BitBtn1Click
            end
            object BitBtn5: TBitBtn
              Tag = 15
              Left = 614
              Top = 4
              Width = 89
              Height = 39
              Cursor = crHandPoint
              Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
              Caption = '&Desiste'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BitBtn5Click
            end
          end
          object DBGrid5: TDBGrid
            Left = 0
            Top = 39
            Width = 788
            Height = 239
            Align = alClient
            DataSource = DsContasEnt
            TabOrder = 3
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -9
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Entidade'
                Width = 37
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Nome'
                Width = 185
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CntrDebCta'
                Title.Caption = 'Contrato d'#233'b. conta'
                Width = 160
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodExporta'
                Title.Caption = 'C'#243'd. exporta'#231#227'o'
                Width = 160
                Visible = True
              end>
          end
        end
        object TabSheet9: TTabSheet
          Caption = 'Mensalidades previstas'
          ImageIndex = 5
          object Panel22: TPanel
            Left = 0
            Top = 0
            Width = 788
            Height = 49
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label46: TLabel
              Left = 291
              Top = 4
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
              FocusControl = EdDBCodigo3
            end
            object Label47: TLabel
              Left = 354
              Top = 4
              Width = 51
              Height = 13
              Caption = 'Descri'#231#227'o:'
              FocusControl = EdDBDescricao3
            end
            object EdDBCodigo3: TDBEdit
              Left = 291
              Top = 20
              Width = 60
              Height = 21
              Hint = 'N'#186' do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Codigo'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 8281908
              Font.Height = -9
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
            end
            object EdDBDescricao3: TDBEdit
              Left = 354
              Top = 20
              Width = 352
              Height = 21
              Hint = 'Nome do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Nome'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -9
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
            end
            object Panel23: TPanel
              Left = 0
              Top = 0
              Width = 287
              Height = 49
              Align = alLeft
              BevelOuter = bvNone
              Caption = 'Panel21'
              TabOrder = 2
              object BitBtn6: TBitBtn
                Tag = 12
                Left = 98
                Top = 3
                Width = 89
                Height = 32
                Cursor = crHandPoint
                Hint = 'Exclui registro atual'
                Caption = 'E&xclui'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BitBtn6Click
              end
              object BitBtn7: TBitBtn
                Tag = 11
                Left = 193
                Top = 3
                Width = 89
                Height = 32
                Cursor = crHandPoint
                Hint = 'Exclui registro atual'
                Caption = '&Altera'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BitBtn7Click
              end
              object BitBtn8: TBitBtn
                Tag = 10
                Left = 4
                Top = 3
                Width = 89
                Height = 32
                Cursor = crHandPoint
                Hint = 'Inclui novo registro'
                Caption = 'I&nsere'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BitBtn8Click
              end
            end
          end
          object GradeContasMes: TdmkDBGrid
            Left = 0
            Top = 49
            Width = 788
            Height = 443
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 26
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PERIODOINI_TXT'
                Title.Caption = 'In'#237'cio'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PERIODOFIM_TXT'
                Title.Caption = 'Final'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMin'
                Title.Caption = 'Valor m'#237'n.'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMax'
                Title.Caption = 'Valor m'#225'x.'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CliInt'
                Title.Caption = 'Cliente interno'
                Width = 483
                Visible = True
              end>
            Color = clWindow
            DataSource = DsContasMes
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -9
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 26
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PERIODOINI_TXT'
                Title.Caption = 'In'#237'cio'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PERIODOFIM_TXT'
                Title.Caption = 'Final'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMin'
                Title.Caption = 'Valor m'#237'n.'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMax'
                Title.Caption = 'Valor m'#225'x.'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CliInt'
                Title.Caption = 'Cliente interno'
                Width = 483
                Visible = True
              end>
          end
        end
        object TabSheet10: TTabSheet
          Caption = 'Dados2'
          ImageIndex = 6
          object Panel27: TPanel
            Left = 0
            Top = 0
            Width = 788
            Height = 492
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label48: TLabel
              Left = 59
              Top = 4
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
              FocusControl = DBEdit2
            end
            object Label49: TLabel
              Left = 122
              Top = 4
              Width = 51
              Height = 13
              Caption = 'Descri'#231#227'o:'
              FocusControl = DBEdit3
            end
            object Label28: TLabel
              Left = 59
              Top = 168
              Width = 78
              Height = 13
              Caption = 'Centro de custo:'
            end
            object Label31: TLabel
              Left = 366
              Top = 168
              Width = 45
              Height = 13
              Caption = 'Entidade:'
            end
            object LaIDFat: TLabel
              Left = 59
              Top = 208
              Width = 193
              Height = 13
              Caption = 'Identificador autom'#225'tico em importa'#231#245'es:'
            end
            object Label10: TLabel
              Left = 362
              Top = 208
              Width = 60
              Height = 13
              Caption = 'C'#233'lula excel:'
              FocusControl = DBEdExcel
            end
            object Label9: TLabel
              Left = 426
              Top = 208
              Width = 107
              Height = 13
              Caption = 'Espec'#237'fico de terceiro:'
              Enabled = False
            end
            object Label4: TLabel
              Left = 482
              Top = 4
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object DBRadioGroup2: TDBRadioGroup
              Left = 59
              Top = 46
              Width = 359
              Height = 37
              Caption = ' Caracter'#237'stica: '
              Columns = 3
              DataField = 'PagRec'
              DataSource = DsContas
              Items.Strings = (
                'Pagar'
                'Ambos'
                'Receber')
              TabOrder = 0
              Values.Strings = (
                '-1'
                '0'
                '1')
            end
            object DBEdit2: TDBEdit
              Left = 59
              Top = 20
              Width = 60
              Height = 21
              Hint = 'N'#186' do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Codigo'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 8281908
              Font.Height = -9
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 1
            end
            object DBEdit3: TDBEdit
              Left = 122
              Top = 20
              Width = 359
              Height = 21
              Hint = 'Nome do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Nome'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -9
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
            end
            object DBRadioGroup3: TDBRadioGroup
              Left = 423
              Top = 46
              Width = 359
              Height = 37
              Caption = ' Centro de Resultado: '
              Columns = 3
              DataField = 'CentroRes'
              DataSource = DsContas
              Items.Strings = (
                'Automatizado'
                'Definitivo'
                'Acumulativo')
              TabOrder = 3
              Values.Strings = (
                '-1'
                '0'
                '1')
            end
            object DBCheckBox2: TDBCheckBox
              Left = 59
              Top = 128
              Width = 238
              Height = 17
              Caption = 'Tem documento f'#237'sico? (Exige Foto ou XML).'
              DataField = 'TemDocFisi'
              DataSource = DsContas
              TabOrder = 4
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBCheckBox3: TDBCheckBox
              Left = 59
              Top = 148
              Width = 211
              Height = 16
              Caption = 'Dispon'#237'vel em todo ERP.'
              DataField = 'UsoNoERP'
              DataSource = DsContas
              TabOrder = 5
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBEdit1: TDBEdit
              Left = 59
              Top = 184
              Width = 304
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'NOMECENTROCUSTO'
              DataSource = DsContas
              TabOrder = 6
            end
            object EdDBEntidade: TDBEdit
              Left = 366
              Top = 184
              Width = 300
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'NOMEENTIDADE'
              DataSource = DsContas
              TabOrder = 7
            end
            object DBEdID: TDBEdit
              Left = 59
              Top = 224
              Width = 300
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'ID'
              DataSource = DsContas
              TabOrder = 8
            end
            object DBEdExcel: TDBEdit
              Left = 362
              Top = 224
              Width = 60
              Height = 21
              Hint = 'Nome do banco'
              TabStop = False
              CharCase = ecUpperCase
              Color = clWhite
              DataField = 'Excel'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -9
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 9
            end
            object DbEdTerceiro: TDBEdit
              Left = 426
              Top = 224
              Width = 240
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'NOMETERCEIRO'
              DataSource = DsContas
              Enabled = False
              TabOrder = 10
            end
            object DBRGRateio: TDBRadioGroup
              Left = 59
              Top = 249
              Width = 398
              Height = 41
              Caption = ' Rateio: '
              DataField = 'Rateio'
              DataSource = DsContas
              TabOrder = 11
            end
            object DBCkCalculMesSeg: TDBCheckBox
              Left = 58
              Top = 294
              Width = 241
              Height = 17
              TabStop = False
              Caption = 'Pendente somente ap'#243's encerramento do m'#234's.'
              DataField = 'CalculMesSeg'
              DataSource = DsContas
              TabOrder = 12
              ValueChecked = '1'
              ValueUnchecked = '0'
              OnClick = DBCBMensalClick
            end
            object DBCBMensal: TDBCheckBox
              Left = 67
              Top = 317
              Width = 60
              Height = 17
              TabStop = False
              Caption = 'Mensal: '
              DataField = 'Mensal'
              DataSource = DsContas
              TabOrder = 13
              ValueChecked = 'V'
              ValueUnchecked = 'F'
              OnClick = DBCBMensalClick
            end
            object GroupBox4: TGroupBox
              Left = 59
              Top = 317
              Width = 607
              Height = 113
              Caption = '    '
              TabOrder = 14
              object LaSaldo: TLabel
                Left = 8
                Top = 20
                Width = 96
                Height = 13
                Caption = 'Dia da mensalidade:'
                FocusControl = DBEdMensDia
              end
              object Label5: TLabel
                Left = 110
                Top = 20
                Width = 117
                Height = 13
                Caption = 'D'#233'bito mensal esperado:'
                FocusControl = DBEdMensdeb
              end
              object Label6: TLabel
                Left = 232
                Top = 20
                Width = 107
                Height = 13
                Caption = 'D'#233'bito mensal m'#237'nimo:'
                FocusControl = DBEdmensmind
              end
              object Label7: TLabel
                Left = 354
                Top = 20
                Width = 119
                Height = 13
                Caption = 'Cr'#233'dito mensal esperado:'
              end
              object Label8: TLabel
                Left = 477
                Top = 20
                Width = 109
                Height = 13
                Caption = 'Cr'#233'dito mensal m'#237'nimo:'
                FocusControl = DBEdMensminc
              end
              object DBEdMensDia: TDBEdit
                Left = 8
                Top = 36
                Width = 99
                Height = 21
                TabStop = False
                Color = clWhite
                DataField = 'Mensdia'
                DataSource = DsContas
                TabOrder = 0
              end
              object DBEdMensdeb: TDBEdit
                Left = 110
                Top = 36
                Width = 120
                Height = 21
                TabStop = False
                Color = clWhite
                DataField = 'Mensdeb'
                DataSource = DsContas
                TabOrder = 1
              end
              object DBEdmensmind: TDBEdit
                Left = 232
                Top = 36
                Width = 119
                Height = 21
                TabStop = False
                Color = clWhite
                DataField = 'Mensmind'
                DataSource = DsContas
                TabOrder = 2
              end
              object DBEdMenscred: TDBEdit
                Left = 354
                Top = 36
                Width = 120
                Height = 21
                TabStop = False
                Color = clWhite
                DataField = 'Menscred'
                DataSource = DsContas
                TabOrder = 3
              end
              object DBEdMensminc: TDBEdit
                Left = 477
                Top = 36
                Width = 119
                Height = 21
                TabStop = False
                Color = clWhite
                DataField = 'Mensminc'
                DataSource = DsContas
                TabOrder = 4
              end
              object DBRadioGroup1: TDBRadioGroup
                Left = 8
                Top = 61
                Width = 588
                Height = 40
                Caption = ' C'#225'lculo da pend'#234'ncia: '
                Columns = 3
                DataField = 'PendenMesSeg'
                DataSource = DsContas
                Items.Strings = (
                  'Meses futuros n'#227'o considerar'
                  'Considerar sempre'
                  'Proporcional na data atual.')
                TabOrder = 5
                Values.Strings = (
                  '0'
                  '1'
                  '2')
              end
            end
            object DBEdEmpresa: TDBEdit
              Left = 482
              Top = 20
              Width = 300
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'NOMEEMPRESA'
              DataSource = DsContas
              TabOrder = 15
            end
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 463
      Width = 1021
      Height = 72
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 175
        Height = 55
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 86
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 45
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 3
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 177
        Top = 15
        Width = 348
        Height = 55
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 525
        Top = 15
        Width = 494
        Height = 55
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel26: TPanel
          Left = 388
          Top = 0
          Width = 106
          Height = 55
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 8
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo registro'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 98
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera registro atual'
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 193
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui registro atual'
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtExcluiClick
        end
        object BtNivelAcima: TBitBtn
          Tag = 353
          Left = 288
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Subgrupos'
          NumGlyphs = 2
          TabOrder = 4
          OnClick = BtNivelAcimaClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1021
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 974
      Top = 0
      Width = 47
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 8
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 217
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 3
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 45
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 217
      Top = 0
      Width = 757
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 498
        Height = 32
        Caption = 'Cadastro de N'#237'vel 5 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 498
        Height = 32
        Caption = 'Cadastro de N'#237'vel 5 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 498
        Height = 32
        Caption = 'Cadastro de N'#237'vel 5 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1021
    Height = 26
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel25: TPanel
      Left = 2
      Top = 15
      Width = 1017
      Height = 9
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = -5
        Width = 9
        Height = 13
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -10
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = -6
        Width = 9
        Height = 13
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -10
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrContasBeforeClose
    AfterScroll = QrContasAfterScroll
    SQL.Strings = (
      'SELECT co.*, sg.Nome NOMESUBGRUPO, cc.Nome NOMECENTROCUSTO,'
      
        'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END NOMEEMP' +
        'RESA,'
      
        'CASE WHEN te.Tipo=0 THEN te.RazaoSocial ELSE te.Nome END NOMETER' +
        'CEIRO,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEENT' +
        'IDADE'
      'FROM contas co'
      'LEFT JOIN subgrupos   sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN centrocusto cc ON cc.Codigo=co.CentroCusto'
      'LEFT JOIN entidades   cl ON cl.Codigo=co.Empresa'
      'LEFT JOIN entidades   te ON te.Codigo=co.Terceiro'
      'LEFT JOIN entidades   en ON en.Codigo=co.Entidade')
    Left = 840
    Top = 252
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Required = True
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Origin = 'contas.Nome2'
      Required = True
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Origin = 'contas.Nome3'
      Required = True
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Origin = 'contas.ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Origin = 'contas.Subgrupo'
      Required = True
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'contas.Empresa'
      Required = True
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Origin = 'contas.Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Origin = 'contas.Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Origin = 'contas.Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Origin = 'contas.Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
      Origin = 'contas.Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
      Origin = 'contas.Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
      Origin = 'contas.Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
      Origin = 'contas.Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
      Origin = 'contas.Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'contas.Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Origin = 'contas.Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Origin = 'contas.Excel'
      Size = 6
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'contas.DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'contas.DataAlt'
    end
    object QrContasUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'contas.UserCad'
    end
    object QrContasUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'contas.UserAlt'
    end
    object QrContasCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
      Origin = 'contas.CentroCusto'
      Required = True
    end
    object QrContasNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 50
    end
    object QrContasNOMECENTROCUSTO: TWideStringField
      FieldName = 'NOMECENTROCUSTO'
      Origin = 'centrocusto.Nome'
      Size = 50
    end
    object QrContasNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrContasNOMETERCEIRO: TWideStringField
      FieldName = 'NOMETERCEIRO'
      Size = 100
    end
    object QrContasRateio: TIntegerField
      FieldName = 'Rateio'
      Origin = 'contas.Rateio'
      Required = True
    end
    object QrContasEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'contas.Entidade'
      Required = True
    end
    object QrContasNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrContasAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'contas.Antigo'
    end
    object QrContasPendenMesSeg: TSmallintField
      FieldName = 'PendenMesSeg'
      Origin = 'contas.PendenMesSeg'
      Required = True
    end
    object QrContasCalculMesSeg: TSmallintField
      FieldName = 'CalculMesSeg'
      Origin = 'contas.CalculMesSeg'
      Required = True
    end
    object QrContasOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'contas.OrdemLista'
      Required = True
    end
    object QrContasContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object QrContasContasSum: TIntegerField
      FieldName = 'ContasSum'
      Required = True
    end
    object QrContasCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
      Required = True
    end
    object QrContasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrContasNotPrntBal: TIntegerField
      FieldName = 'NotPrntBal'
      Required = True
    end
    object QrContasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrContasSigla: TWideStringField
      DisplayWidth = 15
      FieldName = 'Sigla'
      Required = True
    end
    object QrContasNotPrntFin: TIntegerField
      FieldName = 'NotPrntFin'
    end
    object QrContasProvRat: TSmallintField
      FieldName = 'ProvRat'
    end
    object QrContasTemDocFisi: TSmallintField
      FieldName = 'TemDocFisi'
    end
    object QrContasCentroRes: TIntegerField
      FieldName = 'CentroRes'
    end
    object QrContasPagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrContasUsoNoERP: TSmallintField
      FieldName = 'UsoNoERP'
    end
    object QrContasSPED_COD_CTA_REF: TWideStringField
      FieldName = 'SPED_COD_CTA_REF'
      Size = 60
    end
    object QrContasAnaliSinte: TSmallintField
      FieldName = 'AnaliSinte'
    end
    object QrContasImpactEstru: TSmallintField
      FieldName = 'ImpactEstru'
      Required = True
    end
    object QrContasPermitCre: TSmallintField
      FieldName = 'PermitCre'
      Required = True
    end
    object QrContasPermitDeb: TSmallintField
      FieldName = 'PermitDeb'
      Required = True
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 868
    Top = 252
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Genero'
      'FROM :p0'
      'WHERE Genero=:P1')
    Left = 432
    Top = 353
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object QrSubGrupos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, TipoAgrupa'
      'FROM subgrupos'
      'WHERE AnaliSinte=1'
      'ORDER BY Nome')
    Left = 840
    Top = 280
    object QrSubGruposCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.subgrupos.Codigo'
    end
    object QrSubGruposNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.subgrupos.Nome'
      Size = 50
    end
    object QrSubGruposTipoAgrupa: TIntegerField
      FieldName = 'TipoAgrupa'
    end
  end
  object DsSubGrupos: TDataSource
    DataSet = QrSubGrupos
    Left = 868
    Top = 280
  end
  object QrEmpresas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades')
    Left = 840
    Top = 484
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmpresasNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 45
    end
  end
  object DsEpresas: TDataSource
    DataSet = QrEmpresas
    Left = 868
    Top = 484
  end
  object QrTerceiros: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades')
    Left = 840
    Top = 456
    object QrTerceirosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTerceirosNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 45
    end
  end
  object DsTerceiros: TDataSource
    DataSet = QrTerceiros
    Left = 868
    Top = 456
  end
  object QrCentroCusto: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM centrocusto'
      'ORDER BY Nome')
    Left = 840
    Top = 396
    object QrCentroCustoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCentroCustoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCentroCusto: TDataSource
    DataSet = QrCentroCusto
    Left = 868
    Top = 396
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE '
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 840
    Top = 424
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 868
    Top = 424
  end
  object QrContasIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrContasItsBeforeClose
    AfterScroll = QrContasItsAfterScroll
    OnCalcFields = QrContasItsCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM contasits'
      'WHERE Codigo=:P0'
      'ORDER BY Periodo DESC')
    Left = 840
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasItsPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrContasItsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrContasItsFator: TFloatField
      FieldName = 'Fator'
    end
    object QrContasItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContasItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContasItsNOMEPERIODO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEPERIODO'
      Size = 30
      Calculated = True
    end
    object QrContasItsESTIPULADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ESTIPULADO'
      Calculated = True
    end
  end
  object DsContasIts: TDataSource
    DataSet = QrContasIts
    Left = 868
    Top = 308
  end
  object QrCtas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ctasitsctas')
    Left = 840
    Top = 512
    object QrCtasConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCtasNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCtas: TDataSource
    DataSet = QrCtas
    Left = 868
    Top = 512
  end
  object QrLoc2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo=:P0'
      '')
    Left = 460
    Top = 353
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLoc2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLoc2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrContasItsCtas: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrContasItsCalcFields
    SQL.Strings = (
      'SELECT cc.*, co.Nome NOMECONTA '
      'FROM contasitsctas cc'
      'LEFT JOIN contas co ON co.Codigo=cc.Conta'
      'WHERE cc.Codigo=:P0'
      'AND cc.Periodo=:P1'
      'ORDER BY NOMECONTA')
    Left = 840
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrContasItsCtasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasItsCtasPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrContasItsCtasConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrContasItsCtasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasItsCtasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasItsCtasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasItsCtasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContasItsCtasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContasItsCtasNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
  end
  object DsContasItsCtas: TDataSource
    DataSet = QrContasItsCtas
    Left = 868
    Top = 336
  end
  object QrLocate: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.*, sg.Nome NOMESUBGRUPO, cc.Nome NOMECENTROCUSTO,'
      
        'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END NOMEEMP' +
        'RESA,'
      
        'CASE WHEN te.Tipo=0 THEN te.RazaoSocial ELSE te.Nome END NOMETER' +
        'CEIRO,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEENT' +
        'IDADE'
      'FROM contas co'
      'LEFT JOIN subgrupos   sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN centrocusto cc ON cc.Codigo=co.CentroCusto'
      'LEFT JOIN entidades   cl ON cl.Codigo=co.Empresa'
      'LEFT JOIN entidades   te ON te.Codigo=co.Terceiro'
      'LEFT JOIN entidades   en ON en.Codigo=co.Entidade'
      'ORDER BY Nome')
    Left = 840
    Top = 608
    object QrLocateCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocateNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrLocateNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrLocateNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrLocateID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrLocateSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrLocateEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrLocateCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrLocateDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrLocateMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrLocateExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrLocateMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrLocateMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrLocateMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrLocateMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrLocateMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrLocateLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocateTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrLocateExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrLocateDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocateDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocateUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocateUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLocateCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
      Required = True
    end
    object QrLocateRateio: TIntegerField
      FieldName = 'Rateio'
      Required = True
    end
    object QrLocateEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrLocateNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrLocateNOMECENTROCUSTO: TWideStringField
      FieldName = 'NOMECENTROCUSTO'
      Size = 50
    end
    object QrLocateNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrLocateNOMETERCEIRO: TWideStringField
      FieldName = 'NOMETERCEIRO'
      Size = 100
    end
    object QrLocateNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsLocate: TDataSource
    DataSet = QrLocate
    Left = 868
    Top = 608
  end
  object QrNova: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'AND Codigo<>:P0'
      'ORDER BY Nome')
    Left = 840
    Top = 582
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsNova: TDataSource
    DataSet = QrNova
    Left = 868
    Top = 582
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 840
    Top = 200
  end
  object QrLct: TMySQLQuery
    Database = Dmod.MyDB
    Left = 868
    Top = 200
    object QrLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 25
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctCONTA: TIntegerField
      FieldName = 'CONTA'
    end
    object QrLctNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      FixedChar = True
      Size = 128
    end
    object QrLctNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 128
    end
    object QrLctNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 4
    end
    object QrLctNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 4
    end
    object QrLctNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 4
    end
    object QrLctNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctcliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLctUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLctUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLctCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLctPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrContasAgr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contasagr'
      'ORDER BY Nome')
    Left = 629
    Top = 345
    object QrContasAgrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasAgrNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContasAgr: TDataSource
    DataSet = QrContasAgr
    Left = 657
    Top = 345
  end
  object DsEnti2: TDataSource
    DataSet = QrEnti2
    Left = 868
    Top = 556
  end
  object QrEnti2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 840
    Top = 556
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object QrContasEnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEENT, ce.*'
      'FROM contasent ce'
      'LEFT JOIN entidades en ON en.Codigo=ce.Entidade'
      'WHERE ce.Codigo=:P0'
      'ORDER BY NOMEENT'
      '')
    Left = 630
    Top = 374
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasEntNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrContasEntCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contasent.Codigo'
      Required = True
    end
    object QrContasEntEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'contasent.Entidade'
      Required = True
    end
    object QrContasEntCntrDebCta: TWideStringField
      DisplayWidth = 60
      FieldName = 'CntrDebCta'
      Origin = 'contasent.CntrDebCta'
      Size = 50
    end
    object QrContasEntLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'contasent.Lk'
    end
    object QrContasEntDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'contasent.DataCad'
    end
    object QrContasEntDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'contasent.DataAlt'
    end
    object QrContasEntUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'contasent.UserCad'
    end
    object QrContasEntUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'contasent.UserAlt'
    end
    object QrContasEntAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'contasent.AlterWeb'
      Required = True
    end
    object QrContasEntAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'contasent.Ativo'
      Required = True
    end
    object QrContasEntCodExporta: TWideStringField
      FieldName = 'CodExporta'
      Origin = 'contasent.CodExporta'
      Size = 60
    end
  end
  object DsContasEnt: TDataSource
    DataSet = QrContasEnt
    Left = 658
    Top = 374
  end
  object QrContasMes: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrContasMesCalcFields
    SQL.Strings = (
      'SELECT IF(en.Tipo=0,RazaoSocial,en.Nome) NO_CliInt, cm.* '
      'FROM contasmes cm'
      'LEFT JOIN entidades en ON en.Codigo=cm.CliInt'
      'WHERE cm.Codigo=:P0')
    Left = 840
    Top = 364
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasMesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasMesControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContasMesCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrContasMesDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrContasMesPeriodoIni: TIntegerField
      FieldName = 'PeriodoIni'
    end
    object QrContasMesPeriodoFim: TIntegerField
      FieldName = 'PeriodoFim'
    end
    object QrContasMesValorMin: TFloatField
      FieldName = 'ValorMin'
    end
    object QrContasMesValorMax: TFloatField
      FieldName = 'ValorMax'
    end
    object QrContasMesPERIODOINI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODOINI_TXT'
      Size = 10
      Calculated = True
    end
    object QrContasMesPERIODOFIM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODOFIM_TXT'
      Size = 10
      Calculated = True
    end
    object QrContasMesNO_CliInt: TWideStringField
      FieldName = 'NO_CliInt'
      Size = 100
    end
    object QrContasMesQtdeMin: TIntegerField
      FieldName = 'QtdeMin'
    end
    object QrContasMesQtdeMax: TIntegerField
      FieldName = 'QtdeMax'
    end
    object QrContasMesDiaAlert: TSmallintField
      FieldName = 'DiaAlert'
    end
    object QrContasMesDiaVence: TSmallintField
      FieldName = 'DiaVence'
    end
  end
  object DsContasMes: TDataSource
    DataSet = QrContasMes
    Left = 868
    Top = 364
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 260
    Top = 12
  end
  object QrCROCon: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM crocon'
      'ORDER BY Nome')
    Left = 112
    Top = 568
    object QrCROConCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCROConNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCROCon: TDataSource
    DataSet = QrCROCon
    Left = 220
    Top = 616
  end
end
