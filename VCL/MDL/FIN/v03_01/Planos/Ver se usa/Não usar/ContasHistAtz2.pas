unit ContasHistAtz2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, Db, mySQLDbTables, DBCtrls,
  dmkEdit, Menus, dmkPermissoes, UnInternalConsts, dmkDBLookupComboBox,
  dmkEditCB;

type
  TFmContasHistAtz2 = class(TForm)
    PainelConfirma: TPanel;
    BtNao: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    STAvisoContinuar: TStaticText;
    QrSdoIni: TmySQLQuery;
    QrSdoIniSdoIni: TFloatField;
    QrSdoIniCodigo: TIntegerField;
    QrIns: TmySQLQuery;
    QrDel: TmySQLQuery;
    BtSim: TBitBtn;
    QrMax: TmySQLQuery;
    QrMaxData: TDateField;
    QrCliInt: TmySQLQuery;
    QrCliIntNOMEENTIDADE: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    DsCliInt: TDataSource;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrCtLan2: TmySQLQuery;
    QrCtLan2Genero: TIntegerField;
    QrDel2: TmySQLQuery;
    QrVal: TmySQLQuery;
    QrValCredito: TFloatField;
    QrValDebito: TFloatField;
    BtExclui: TBitBtn;
    QrMinT: TmySQLQuery;
    QrMinTData: TDateField;
    QrMinC: TmySQLQuery;
    QrMinCData: TDateField;
    Panel3: TPanel;
    PB: TProgressBar;
    StaticText2: TStaticText;
    STCliInt_: TStaticText;
    STConta_: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    STPeriodo_: TStaticText;
    Label1: TLabel;
    EdCliInt: TdmkEditCB;
    Label2: TLabel;
    EdConta: TdmkEditCB;
    Label5: TLabel;
    dmkEdPeriodo: TdmkEdit;
    Edit1: TEdit;
    Label3: TLabel;
    Label16: TLabel;
    CBConta: TdmkDBLookupComboBox;
    CBCliInt: TdmkDBLookupComboBox;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtNaoClick(Sender: TObject);
    procedure BtSimClick(Sender: TObject);
    procedure EdCliIntChange(Sender: TObject);
    procedure dmkEdPeriodoChange(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtExcluiClick(Sender: TObject);
    procedure dmkEdPeriodoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure LimpaSelecionados(EntiCod, Genero, Periodo: Integer);
    procedure MudaPeriodo(Key: Word; CancelaNavi: Boolean);
    procedure ReopenMinT(Entidade: Integer);
    procedure ReopenMinC(Entidade, Genero: Integer);
    procedure ReopenVal(Entidade, Genero: Integer; DataIni, DataFim: String);
  public
    { Public declarations }
    FAtivou, FContinua: Boolean;
    FEntiCod, FGenero, FPeriodo: Integer;
  end;

  var
  FmContasHistAtz2: TFmContasHistAtz2;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmContasHistAtz2.BtSaidaClick(Sender: TObject);
begin
  FContinua := False;
  Close;
end;

procedure TFmContasHistAtz2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if not FAtivou then
  begin
    STCliInt_.Caption  := FormatFloat('  000', FEntiCod);
    EdCliInt.Text := IntToStr(FEntiCod);
    CBCliInt.KeyValue := FEntiCod;
    //
    STConta_.Caption   := FormatFloat('  000', FGenero);
    EdConta.Text := IntToStr(FGenero);
    CBConta.KeyValue := FGenero;
    //
    STPeriodo_.Caption := FormatFloat('  000', FPeriodo);
    dmkEdPeriodo.ValueVariant := Fperiodo;
    FAtivou := True;
  end;
end;

procedure TFmContasHistAtz2.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasHistAtz2.FormCreate(Sender: TObject);
begin
  QrCliInt.Close;
  QrCliInt.Open;
  QrContas.Close;
  QrContas.Open;
  //
  FContinua := False;
  FGenero   := -999999999;
  FPeriodo  := -23999; // janeiro do ano zero depois de cristo
end;

procedure TFmContasHistAtz2.BtNaoClick(Sender: TObject);
begin
  FContinua := True;
  //
  Close;
end;

procedure TFmContasHistAtz2.BtSimClick(Sender: TObject);
var
  Genero, Periodo, PeriodoFim: Integer;
  MyCursor: TCursor;
  Cre, Deb, Ant, Fim: Double;
  DataIni, DataFim: String;
  TodosGeneros, TodosPeriodos: Boolean;
begin
  FEntiCod := Geral.IMV(EdCliInt.Text);
  if FEntiCod = 0 then
  begin
    Application.MessageBox('Defina o cliente interno!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  FPeriodo := dmkEdPeriodo.ValueVariant;
  FContinua := True;
  STCliInt_.Caption := FormatFloat('  000', FEntiCod);
  //
  // Define se � um genero ou todos
  FGenero  := Geral.IMV(EdConta.Text);
  TodosGeneros :=(FGenero = 0) or (FGenero = -999999999);
  //
  //
  // Define per�odo inicial
  if TodosGeneros then
  begin
    ReopenMinT(FEntiCod);
    Periodo := Geral.Periodo2000(QrMinTData.Value);
  end else begin
    ReopenMinC(FEntiCod, FGenero);
    Periodo := Geral.Periodo2000(QrMinCData.Value);
  end;
  TodosPeriodos := Periodo >= FPeriodo;
  // para n�o criar registros desnecess�rios
  if TodosPeriodos then
  begin
    FPeriodo := Periodo;
    LimpaSelecionados(FEntiCod, FGenero, FPeriodo-1);
  end else LimpaSelecionados(FEntiCod, FGenero, FPeriodo);
  //
  // Abre saldos iniciais
  QrSdoIni.Close;
  QrSdoIni.Params[00].AsInteger := FEntiCod;
  QrSdoIni.Open;

  // Buscar a data mais atual nos lan�amentos
  QrMax.Close;
  QrMax.SQL.Clear;
  QrMax.SQL.Add('SELECT Max(data) Data');
  QrMax.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrMax.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrMax.SQL.Add('WHERE lan.Tipo < 2');
  QrMax.SQL.Add('AND car.ForneceI=:P0');
  QrMax.Params[0].AsInteger := FEntiCod;
  QrMax.Open;
  if QrMaxData.Value > Date then
    PeriodoFim := Geral.Periodo2000(QrMaxData.Value)
  else
    PeriodoFim := Geral.Periodo2000(Date);
  //
  // lista das contas do cliente interno que tiveram movimento no periodo selecionado
  QrCtLan2.Close;
  QrCtLan2.SQL.Clear;
  QrCtLan2.SQL.Add('SELECT DISTINCT lan.Genero ');
  QrCtLan2.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrCtLan2.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrCtLan2.SQL.Add('WHERE lan.Tipo < 2');
  QrCtLan2.SQL.Add('AND lan.Genero <> 0');
  QrCtLan2.SQL.Add('AND car.ForneceI=' + IntToStr(FEntiCod));
  if not TodosGeneros then
    QrCtLan2.SQL.Add('AND lan.Genero=' + IntToStr(FGenero));
  //Erro
  //if not TodosPeriodos then
    //QrCtLan2.SQL.Add('AND lan.Data < "' +
    //MLAGeral.PrimeiroDiaDoPeriodo(FPeriodo+1, dtSystem) + '"');
  QrCtLan2.SQL.Add('ORDER BY Genero');
  QrCtLan2.Open;
  //
  // Insere movimentos de contas
  // por per�odo na tabela ContasHis (Hist�rico de contas)
  QrIns.Params[00].AsInteger := FEntiCod;
  PB.Position := 0;
  PB.Max := QrCtLan2.RecordCount;
  QrCtLan2.First;
  while not QrCtLan2.Eof do
  begin
    Genero := QrCtLan2Genero.Value;
    STConta_.Caption := FormatFloat('  000', Genero);
    QrIns.Params[01].AsInteger := Genero;
    PB.Position := PB.Position + 1;
    PB.Update;
    Application.ProcessMessages;

    // Obtem a data do lancto mais antigo da conta selecionada do cliente interno

    ReopenMinC(FEntiCod, Genero);
    Periodo := Geral.Periodo2000(QrMinCData.Value);
    //
    // inclui saldo final no periodo anterior ao inicial caso
    // tiver atualizando todos per�odos ou o movimento da conta come�a
    // durante ou ap�s o periodo inicial solicitado
    if TodosPeriodos or (FPeriodo <= Periodo) then
    begin
      Inc(Periodo, - 1);
      STPeriodo_.Caption := FormatFloat('  000', Periodo);
      //
      Deb := 0;
      Cre := 0;
      Ant := 0;
      Fim := 0;
      // verifica se a conta do cliente interno tem saldo inicial!!!
      if QrSdoIni.Locate('Codigo', Genero, []) then
      begin
        Fim := QrSdoIniSdoIni.Value;
        // S� inclui se for Todos per�odos, caso contr�rio gerar� um erro, pois
        // o registro j� existe
        if TodosPeriodos then
        begin
          if QrSdoIniSdoIni.Value < 0 then
            Deb := - QrSdoIniSdoIni.Value
          else
            Cre := QrSdoIniSdoIni.Value;
          QrIns.Params[02].AsInteger := Periodo;
          QrIns.Params[03].AsFloat   := Ant;
          QrIns.Params[04].AsFloat   := Cre;
          QrIns.Params[05].AsFloat   := Deb;
          QrIns.Params[06].AsFloat   := Fim;
          QrIns.ExecSQL;
        end;
      end;
      // volta ao per�odo inicial
      Inc(Periodo, 1);
    end else begin
      Periodo := FPeriodo;
      // caso contr�rio � necess�rio calcular o saldo anterior final
      if QrSdoIni.Locate('Codigo', Genero, []) then
        Ant := QrSdoIniSdoIni.Value
      else Ant := 0;
      //
      DataIni := Geral.FDT(1, 1);
      DataFim := MLAGeral.UltimoDiaDoPeriodo(Periodo-1, dtSystem);
      //
      ReopenVal(FEntiCod, Genero, DataIni, DataFim);
      Fim := Ant + QrValCredito.Value - QrValDebito.Value;
      //
    end;

    while Periodo <= PeriodoFim do
    begin
      Ant := Fim;
      DataIni := MLAGeral.PrimeiroDiaDoPeriodo(Periodo, dtSystem);
      DataFim := MLAGeral.UltimoDiaDoPeriodo(  Periodo, dtSystem);
      //
      ReopenVal(FEntiCod, Genero, DataIni, DataFim);
      Fim := Ant + QrValCredito.Value - QrValDebito.Value;
      //
      QrIns.Params[02].AsInteger := Periodo;
      QrIns.Params[03].AsFloat   := Ant;
      QrIns.Params[04].AsFloat   := QrValCredito.Value;
      QrIns.Params[05].AsFloat   := QrValDebito.Value;
      QrIns.Params[06].AsFloat   := Fim;
      QrIns.ExecSQL;
      Inc(Periodo, 1);
      STPeriodo_.Caption := FormatFloat('  000', Periodo);
    end;
    //
    QrCtLan2.Next;
  end;

  //
  Screen.Cursor := MyCursor;
  //
  Close;
end;

procedure TFmContasHistAtz2.EdCliIntChange(Sender: TObject);
var
  CliInt: Integer;
begin
  CliInt := Geral.IMV(EdCliInt.Text);
  BtExclui.Enabled := CliInt <> 0;
  //
  ReopenMinT(CliInt);
  dmkEdPeriodo.ValueVariant := Geral.Periodo2000(QrMinTData.Value)
  //
end;

procedure TFmContasHistAtz2.MudaPeriodo(Key: Word; CancelaNavi: Boolean);
begin
  case key of
    VK_DOWN:  if not CancelaNavi then dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant - 1;
    VK_UP:    if not CancelaNavi then dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant + 1;
    VK_PRIOR: if not CancelaNavi then dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant - 12;
    VK_NEXT:  if not CancelaNavi then dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant + 12;
    VK_F1:    dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date) - 1;
    VK_F2:    dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date) - 2;
    VK_F3:    dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date) - 3;
    VK_F4:    dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date) - 4;
    VK_F5:    dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date) - 5;
    VK_F6:    dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date) - 6;
    VK_F7:    dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date) - 7;
    VK_F8:    dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date) - 8;
    VK_F9:    dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date) - 9;
    VK_F10:   dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date) - 10;
    VK_F11:   dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date) - 11;
    VK_F12:   dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date) - 12;
  end;
end;

procedure TFmContasHistAtz2.ReopenMinC(Entidade, Genero: Integer);
begin
  QrMinC.Close;
  QrMinC.SQL.Clear;
  QrMinC.SQL.Add('SELECT MIN(data) Data');
  QrMinC.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrMinC.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrMinC.SQL.Add('WHERE lan.Tipo < 2');
  QrMinC.SQL.Add('AND car.ForneceI=:P0');
  QrMinC.SQL.Add('AND lan.Genero=:P1');
  QrMinC.Params[0].AsInteger := Entidade;
  QrMinC.Params[1].AsInteger := Genero;
  QrMinC.Open;
end;

procedure TFmContasHistAtz2.ReopenMinT(Entidade: Integer);
begin
  QrMinT.Close;
  QrMinT.SQL.Clear;
  QrMinT.SQL.Add('SELECT MIN(data) Data');
  QrMinT.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrMinT.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrMinT.SQL.Add('WHERE lan.Tipo < 2');
  QrMinT.SQL.Add('AND car.ForneceI=:P0');
  QrMinT.Params[00].AsInteger := Entidade;
  QrMinT.Open;
end;

procedure TFmContasHistAtz2.ReopenVal(Entidade, Genero: Integer; DataIni,
  DataFim: String);
begin
  QrVal.Close;
  QrVal.SQL.Clear;
  QrVal.SQL.Add('SELECT SUM(Credito) Credito, SUM(Debito) Debito');
  QrVal.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrVal.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrVal.SQL.Add('WHERE car.Tipo < 2');
  QrVal.SQL.Add('AND car.ForneceI=:P0');
  QrVal.SQL.Add('AND lan.Genero=:P1');
  QrVal.SQL.Add('AND lan.Data BETWEEN :P2 AND :P3');
  QrVal.Params[00].AsInteger := Entidade;
  QrVal.Params[01].AsInteger := Genero;
  QrVal.Params[02].AsString  := DataIni;
  QrVal.Params[03].AsString  := DataFim;
  QrVal.Open;
end;

procedure TFmContasHistAtz2.dmkEdPeriodoChange(Sender: TObject);
begin
  Edit1.Text := MLAGeral.MesEAnoDoPeriodoLongo(dmkEdPeriodo.ValueVariant);
end;

procedure TFmContasHistAtz2.dmkEdPeriodoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MudaPeriodo(Key, True);
end;

procedure TFmContasHistAtz2.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MudaPeriodo(Key, False);
end;

procedure TFmContasHistAtz2.LimpaSelecionados(EntiCod, Genero, Periodo: Integer);
begin
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('DELETE FROM contashis');
  Dmod.QrUpdM.SQL.Add('WHERE Entidade=' + FormatFloat('0', EntiCod));
  Dmod.QrUpdM.SQL.Add('AND Periodo>=' + FormatFloat('0', Periodo));
  if (Genero <> -999999999) and (Genero <> 0) then
    Dmod.QrUpdM.SQL.Add('AND Codigo=' + FormatFloat('0', Genero));
  Dmod.QrUpdM.ExecSQL;
end;

procedure TFmContasHistAtz2.BtExcluiClick(Sender: TObject);
var
  EntiCod: Integer;
begin
  EntiCod := Geral.IMV(EdCliInt.Text);
  if Application.MessageBox(PChar('Confirma a exclus�o de todos saldos de ' +
  'todas contas do cliente interno "' + CBCliInt.Text + '"?' + sLineBreak +
  'Observa��o: Os saldos iniciais n�o ser�o zerados!'), 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM contashis');
    Dmod.QrUpdM.SQL.Add('WHERE Entidade=' + FormatFloat('0', EntiCod));
    Dmod.QrUpdM.ExecSQL;
    //
  end;
end;

end.

