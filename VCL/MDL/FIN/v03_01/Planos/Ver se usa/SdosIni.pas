unit SdosIni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEditCB, ComCtrls, dmkEditDateTimePicker, dmkEdit, DB,
  mySQLDbTables, Variants;

type
  TFmSdosIni = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    EdValor: TdmkEdit;
    Label1: TLabel;
    TPData: TdmkEditDateTimePicker;
    Label2: TLabel;
    EdGenero: TdmkEditCB;
    CBGenero: TdmkDBLookupComboBox;
    Label3: TLabel;
    Label4: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    EdDescri: TdmkEdit;
    Label5: TLabel;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrCarteiras: TmySQLQuery;
    DsContas: TDataSource;
    DsCarteiras: TDataSource;
    QrLcto: TmySQLQuery;
    QrLctoData: TDateField;
    QrCarteirasForneceI: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmSdosIni: TFmSdosIni;

implementation

uses ContasHistSdo, ModuleGeral, UMySQLModule, UnInternalConsts;

{$R *.DFM}

procedure TFmSdosIni.BtOKClick(Sender: TObject);
var
  Genero, Carteira: Integer;
  Credito, Debito: Double;
  Data: String;
begin
  Genero   := Geral.IMV(EdGenero.Text);
  Carteira := Geral.IMV(EdCarteira.Text);
  if EdValor.ValueVariant < 0 then
  begin
    Credito := 0;
    Debito  := -EdValor.ValueVariant;
  end else begin
    Debito  := 0;
    Credito := EdValor.ValueVariant;
  end;
  Data := Geral.FDT(TPData.Date, 1);
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('DELETE FROM trfoldnew WHERE ');
  DmodG.QrUpdPID1.SQL.Add('Genero=:P0 AND Carteira=:P1');
  DmodG.QrUpdPID1.Params[00].AsInteger := Genero;
  DmodG.QrUpdPID1.Params[01].AsInteger := Carteira;
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  UMyMod.SQLInsUpd(DmodG.QrUpdPID1, LaTipo.SQLType, 'trfoldnew', False, [
  'Data', 'Genero', 'Carteira', 'Credito', 'Debito', 'Descri'], [], [
  Data, Genero, Carteira, Credito, Debito, EdDescri.Text], [], False);
  //
  FmContasHistSdo.ReopenSdosInis();
  if Application.MessageBox('Deseja continuar inserindo?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    EdGenero.Text := '';
    CBGenero.KeyValue := Null;
    //
    EdCarteira.Text := '';
    CBCarteira.KeyValue := Null;
    //
    EdValor.ValueVariant := 0;
    TPData.Date := Date -1;
    EdDescri.Text := 'S A L D O   I N I C I A L';
    //
    EdGenero.SetFocus;
  end else Close;
end;

procedure TFmSdosIni.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSdosIni.EdCarteiraChange(Sender: TObject);
begin
  QrLcto.Close;
  QrLcto.SQL.Clear;
  QrLcto.SQL.Add('SELECT MIN(Data) Data');
  QrLcto.SQL.Add('FROM ' + VAR_LCT);
  QrLcto.SQL.Add('WHERE Carteira=:P0');
  QrLcto.Params[0].AsInteger := EdCarteira.ValueVariant;
  QrLcto.Open;
  //
  if QrLctoData.Value > 0 then
    TPData.Date := QrLctoData.Value -1
  else
    TPData.Date := Date -1;
end;

procedure TFmSdosIni.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSdosIni.FormCreate(Sender: TObject);
begin
  QrContas.Open;
  QrCarteiras.Close;
  QrCarteiras.Params[0].AsInteger := FmContasHistSdo.QrCliIntEntidade.Value;
  QrCarteiras.Open;
end;

procedure TFmSdosIni.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.

