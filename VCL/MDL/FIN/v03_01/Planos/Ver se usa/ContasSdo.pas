unit ContasSdo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls,Db, mySQLDbTables, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmContasSdo = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Label3: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdCodigo: TdmkEditCB;
    CBCodigo: TdmkDBLookupComboBox;
    EdSdoIni: TdmkEdit;
    Label4: TLabel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrPesq: TmySQLQuery;
    QrPesqSdoIni: TFloatField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure EdSdoIniExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPesq();
  public
    { Public declarations }
  end;

  var
  FmContasSdo: TFmContasSdo;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts;

{$R *.DFM}

procedure TFmContasSdo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasSdo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ReopenPesq();
end;

procedure TFmContasSdo.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasSdo.EdEntidadeChange(Sender: TObject);
begin
  ReopenPesq();
end;

procedure TFmContasSdo.EdSdoIniExit(Sender: TObject);
begin
  EdSdoIni.Text := Geral.TFT(EdSdoIni.Text, 2, siNegativo);
end;

procedure TFmContasSdo.FormCreate(Sender: TObject);
begin
  QrEntidades.Open;
  QrContas.Open;
end;

procedure TFmContasSdo.BtOKClick(Sender: TObject);
var
  Codigo, Entidade: Integer;
  Status: String;
begin
  if QrPesq.State <> dsBrowse then ReopenPesq();
  Codigo := Geral.IMV(EdCodigo.Text);
  Entidade := Geral.IMV(EdEntidade.Text);
  if (Codigo = 0) or (Entidade = 0) then
  begin
    Application.MessageBox('A entidade e a conta s�o obrigat�rias!',
    'Aviso', MB_OK + MB_ICONWARNING);
    if Codigo = 0 then
      EdCodigo.SetFocus
    else
      EdEntidade.SetFocus;
    Exit;
  end;
  if QrPesq.RecordCount = 0 then
    Status := CO_INCLUSAO
  else
    Status := CO_ALTERACAO;
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, Status, 'ContasSdo', False,
  [
    'SdoIni'
  ], ['Codigo', 'Entidade'],
  [
    Geral.DMV(EdSdoIni.Text)
  ], [Codigo, Entidade]) then
  begin
   if CkContinuar.Checked then
   begin
     ReopenPesq();
     Application.MessageBox(PChar(Status + ' realizada com sucesso!'), 'Mensagem',
     MB_OK+MB_ICONINFORMATION);
   end else
     Close;
  end;   
end;

procedure TFmContasSdo.ReopenPesq();
begin
  QrPesq.Close;
  QrPesq.Params[00].AsInteger := Geral.IMV(EdCodigo.Text);
  QrPesq.Params[01].AsInteger := Geral.IMV(EdEntidade.Text);
  QrPesq.Open;
end;

end.
