unit ContasHisVerif;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,Db, Grids, DBGrids;

type
  TFmContasHisVerif = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DBGrid1: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmContasHisVerif: TFmContasHisVerif;

implementation

uses UnMyObjects, ModuleFin;

{$R *.DFM}

procedure TFmContasHisVerif.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasHisVerif.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasHisVerif.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasHisVerif.FormCreate(Sender: TObject);
begin
  DBGrid1.DataSource := DmodFin.DsVerifHis;
end;

end.
