unit CtaCfgCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, UnMsgInt,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnInternalConsts2,
  UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, ComCtrls, DmkDAC_PF, UnDmkEnums;

type
  TFmCtaCfgCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrCtaCfgCab: TmySQLQuery;
    DsCtaCfgCab: TDataSource;
    QrCtaCfgIts: TmySQLQuery;
    QrCtaCfgItsCodigo: TIntegerField;
    DsCtaCfgIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrCtaCfgItsControle: TIntegerField;
    QrCtaCfgItsGenero: TIntegerField;
    QrCtaCfgItsNome: TWideStringField;
    N1: TMenuItem;
    Sacramantar1: TMenuItem;
    odos1: TMenuItem;
    SelecionadoCdigonegativo1: TMenuItem;
    QrGeneros: TmySQLQuery;
    QrGenerosGenero: TIntegerField;
    QrIts: TmySQLQuery;
    QrItsITENS: TLargeintField;
    PB1: TProgressBar;
    Duplicaratual1: TMenuItem;
    QrCCI: TmySQLQuery;
    QrCCICodigo: TIntegerField;
    QrCCIControle: TIntegerField;
    QrCCIGenero: TIntegerField;
    QrCCINome: TWideStringField;
    QrCtaCfgItsNO_CTA: TWideStringField;
    QrCtaCfgCabCodigo: TIntegerField;
    QrCtaCfgCabNome: TWideStringField;
    QrCtaCfgCabLk: TIntegerField;
    QrCtaCfgCabDataCad: TDateField;
    QrCtaCfgCabDataAlt: TDateField;
    QrCtaCfgCabUserCad: TIntegerField;
    QrCtaCfgCabUserAlt: TIntegerField;
    QrCtaCfgCabAlterWeb: TSmallintField;
    QrCtaCfgCabAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCtaCfgCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCtaCfgCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrCtaCfgCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure odos1Click(Sender: TObject);
    procedure SelecionadoCdigonegativo1Click(Sender: TObject);
    procedure Duplicaratual1Click(Sender: TObject);
  private
    (*FEmpresa,*) FCliInt, FEntidade: Integer;
    FTabLctA, FTabLctB, FTabLctD(*, FEntidade_TXT*): String;
    FDtEncer, FDtMorto: TDateTime;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraCtaCfgIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenCtaCfgIts(Controle: Integer);

  end;

var
  FmCtaCfgCab: TFmCtaCfgCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, CtaCfgIts, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCtaCfgCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCtaCfgCab.MostraCtaCfgIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmCtaCfgIts, FmCtaCfgIts, afmoNegarComAviso) then
  begin
    FmCtaCfgIts.ImgTipo.SQLType := SQLType;
    if SQLType = stUpd then
    begin
      FmCtaCfgIts.EdGenero.ValueVariant := QrCtaCfgItsGenero.Value;
      FmCtaCfgIts.CBGenero.KeyValue     := QrCtaCfgItsGenero.Value;
      FmCtaCfgIts.EdNome.Text := QrCtaCfgItsNome.Value;
    end;
    FmCtaCfgIts.ShowModal;
    FmCtaCfgIts.Destroy;
  end;
end;

procedure TFmCtaCfgCab.odos1Click(Sender: TObject);
var
  Nome: String;
  Codigo, Controle, Genero: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT CodCliInt, CodEnti ',
    'FROM enticliint ',
    'WHERE CodCliInt > 0 ',
    'ORDER BY CodCliInt ',
    '']);
    //
    PB1.Position := 0;
    PB1.Max := Dmod.QrAux.recordCount;
    Dmod.QrAux.First;
    while not Dmod.QrAux.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      //
      FEntidade := Dmod.QrAux.FieldByName('CodEnti').AsInteger;
      FCliInt   := Dmod.QrAux.FieldByName('CodCliInt').AsInteger;
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Verificando cliente interno ' + Geral.FFN(FCliInt, 6));
      //
      DModG.Def_EM_ABD(TMeuDB, FEntidade, FCliInt,
        FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
      //
      Nome := '[SANCIONADO] ' + Geral.FDT(Now(), 0);
      UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
      'INSERT INTO ctacfgcab ',
      'SET Codigo=' + Geral.FF0(-FCliInt),
      ', Nome="' + Nome + '" ',
      'ON DUPLICATE KEY UPDATE NOME = "' + Nome + '" ',
      '']);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrGeneros, Dmod.MyDB, [
      'SELECT DISTINCT Genero ',
      'FROM ' + FTabLctA,
      'WHERE Genero > 0',
      '',
      'UNION',
      '',
      'SELECT DISTINCT Genero ',
      'FROM ' + FTabLctB,
      'WHERE Genero > 0',
      '',
      'UNION ',
      '',
      'SELECT DISTINCT Genero ',
      'FROM ' + FTabLctD,
      'WHERE Genero > 0',
      '',
      'ORDER BY Genero',
      '']);
      //
      QrGeneros.First;
      while not QrGeneros.Eof do
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrIts, Dmod.MyDB, [
        'SELECT Count(*) ITENS ',
        'FROM ctacfgits ',
        'WHERE Codigo=' + Geral.FF0(-FCliInt),
        'AND Genero=' + Geral.FF0(QrGenerosGenero.Value),
        '']);
        if QrItsITENS.Value = 0 then
        begin
          Codigo := -FCliInt;
          Genero := QrGenerosGenero.Value;
          Nome := '';
          MyObjects.Informa2(LaAviso1, LaAviso2, False,
            'Sancionando cliente interno ' + Geral.FFN(FCliInt, 6) + ' conta ' +
            Geral.FFN(Genero, 6));
          //
          Controle := UMyMod.BPGS1I32(
            'ctacfgits', 'Controle', '', '', tsPos, stIns, 0);
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ctacfgits', False, [
          'Codigo', 'Genero', 'Nome'], ['Controle'], [
          Codigo, Genero, Nome], [Controle], True);
        end;
        //
        QrGeneros.Next;
      end;
      //
      Dmod.QrAux.Next;
    end;
    // FIM
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'Sancionamento finalizado com sucesso!');
    Va(vpFirst);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCtaCfgCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrCtaCfgCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrCtaCfgCab, QrCtaCfgIts);
  //
  SelecionadoCdigonegativo1.Enabled := QrCtaCfgCabCodigo.Value < 0;
end;

procedure TFmCtaCfgCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrCtaCfgCab);
  //MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrCtaCfgIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrCtaCfgIts);
end;

procedure TFmCtaCfgCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCtaCfgCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCtaCfgCab.DefParams;
begin
  VAR_GOTOTABELA := 'ctacfgcab';
  VAR_GOTOMYSQLTABLE := QrCtaCfgCab;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM ctacfgcab');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome LIKE :P0');
  //
end;

procedure TFmCtaCfgCab.Duplicaratual1Click(Sender: TObject);
var
  Codigo, Controle, Genero: Integer;
  Nome: String;
begin
  Codigo := 0;
  if Geral.MB_Pergunta('Confirma a duplica��o do Perfil atual?') = ID_YES then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Copiando perfil ' + QrCtaCfgCabNome.Value);
    //
    QrCCI.Close;
    QrCCI.Params[0].AsInteger := QrCtaCfgCabCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrCCI, Dmod.MyDB);
    PB1.Position := 0;
    PB1.Max := QrCCI.RecordCount;
    //
    Codigo := UMyMod.BPGS1I32(
            'ctacfgcab', 'Codigo', '', '', tsPos, stIns, 0);
    Nome := QrCtaCfgCabNome.Value + ' [C�pia] ';
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ctacfgcab', False, [
    'Nome'], ['Codigo'], [
    Nome], [Codigo], True) then
    begin
      QrCCI.First;
      while not QrCCI.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 
          'Copiando conta ' + Geral.FF0(QrCCIGenero.Value));
        Controle := UMyMod.BPGS1I32(
              'ctacfgits', 'Controle', '', '', tsPos, stIns, 0);
        Genero := QrCCIGenero.Value;
        Nome   := QrCCINome.Value;
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ctacfgits', False, [
        'Codigo', 'Genero', 'Nome'], ['Controle'], [
        Codigo, Genero, Nome], [Controle], True);
        //
        QrCCI.Next;
      end;
    end;
  end;
  QrCCI.Close;
  LocCod(Codigo, codigo);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
end;

procedure TFmCtaCfgCab.ItsAltera1Click(Sender: TObject);
begin
  MostraCtaCfgIts(stUpd);
end;

procedure TFmCtaCfgCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmCtaCfgCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCtaCfgCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCtaCfgCab.ItsExclui1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Confirma a retirada da conta selecionada do grupo atual?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM ctacfgits ');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[0].AsInteger  := QrCtaCfgItsControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenCtaCfgIts(0);
  end;
end;

procedure TFmCtaCfgCab.ReopenCtaCfgIts(Controle: Integer);
begin
  QrCtaCfgIts.Close;
  QrCtaCfgIts.Params[0].AsInteger := QrCtaCfgCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrCtaCfgIts, Dmod.MyDB);
  //
  QrCtaCfgIts.Locate('Controle', Controle, []);
end;


procedure TFmCtaCfgCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCtaCfgCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCtaCfgCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCtaCfgCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCtaCfgCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCtaCfgCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtaCfgCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCtaCfgCabCodigo.Value;
  Close;
end;

procedure TFmCtaCfgCab.ItsInclui1Click(Sender: TObject);
begin
  MostraCtaCfgIts(stIns);
end;

procedure TFmCtaCfgCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCtaCfgCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ctacfgcab');
end;

procedure TFmCtaCfgCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('ctacfgcab', 'Codigo', ImgTipo.SQLType,
    QrCtaCfgCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Fmctacfgcab, PnEdita,
    'ctacfgcab', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmCtaCfgCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ctacfgcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ctacfgcab', 'Codigo');
end;

procedure TFmCtaCfgCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmCtaCfgCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmCtaCfgCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmCtaCfgCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCtaCfgCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCtaCfgCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCtaCfgCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCtaCfgCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCtaCfgCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCtaCfgCab.QrCtaCfgCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCtaCfgCab.QrCtaCfgCabAfterScroll(DataSet: TDataSet);
begin
  ReopenCtaCfgIts(0);
end;

procedure TFmCtaCfgCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrCtaCfgCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmCtaCfgCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCtaCfgCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ctacfgcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCtaCfgCab.SelecionadoCdigonegativo1Click(Sender: TObject);
begin
  Geral.MB_Info('Procedimento n�o implementado! Solicite � DERMATEK!');
end;

procedure TFmCtaCfgCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtaCfgCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCtaCfgCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ctacfgcab');
end;

procedure TFmCtaCfgCab.QrCtaCfgCabBeforeOpen(DataSet: TDataSet);
begin
  QrCtaCfgCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

