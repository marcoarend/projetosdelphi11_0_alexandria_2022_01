unit Grupos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, Variants, dmkPermissoes, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, UnDmkProcFunc, dmkImage, UnDmkEnums, dmkRadioGroup, UnFinanceiro,
  dmkCheckBox;

type
  TFmGrupos = class(TForm)
    PainelDados: TPanel;
    DsGrupos: TDataSource;
    QrGrupos: TmySQLQuery;
    QrGruposLk: TIntegerField;
    QrGruposDataCad: TDateField;
    QrGruposDataAlt: TDateField;
    QrGruposUserCad: TIntegerField;
    QrGruposUserAlt: TIntegerField;
    QrGruposCodigo: TSmallintField;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    LaConjunto: TLabel;
    DBEdConjunto: TDBEdit;
    DBGrid1: TDBGrid;
    TbSubGrupos: TmySQLTable;
    DsSubGrupos: TDataSource;
    TbSubGruposCodigo: TIntegerField;
    TbSubGruposOrdemLista: TIntegerField;
    TbSubGruposNome: TWideStringField;
    Label3: TLabel;
    EdConjunto: TdmkEditCB;
    CBConjunto: TdmkDBLookupComboBox;
    QrConjuntos: TmySQLQuery;
    QrConjuntosCodigo: TIntegerField;
    QrConjuntosNome: TWideStringField;
    DsConjuntos: TDataSource;
    QrGruposConjunto: TIntegerField;
    QrGruposNOMECONJUNTO: TWideStringField;
    TbSubGruposGrupo: TIntegerField;
    TbSubGruposTipoAgrupa: TIntegerField;
    TbSubGruposLk: TIntegerField;
    TbSubGruposDataCad: TDateField;
    TbSubGruposDataAlt: TDateField;
    TbSubGruposUserCad: TIntegerField;
    TbSubGruposUserAlt: TIntegerField;
    Label4: TLabel;
    EdOrdemLista: TdmkEdit;
    QrGruposOrdemLista: TIntegerField;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    QrGruposNome: TWideStringField;
    DBCheckBox1: TDBCheckBox;
    CkCtrlaSdo: TCheckBox;
    QrGruposCtrlaSdo: TSmallintField;
    dmkPermissoes1: TdmkPermissoes;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    StaticText3: TStaticText;
    BtNivelAcima: TBitBtn;
    SBGrupo: TSpeedButton;
    EdNome: TdmkEdit;
    EdNome2: TdmkEdit;
    Label6: TLabel;
    QrGruposNome2: TWideStringField;
    Label13: TLabel;
    DBEdit2: TDBEdit;
    QrGruposAnaliSinte: TSmallintField;
    DBRadioGroup2: TDBRadioGroup;
    RGAnaliSinte: TdmkRadioGroup;
    CkPermitDeb: TdmkCheckBox;
    CkPermitCre: TdmkCheckBox;
    CkAtivo: TdmkCheckBox;
    QrGruposSPED_COD_CTA_REF: TWideStringField;
    QrGruposPermitCre: TSmallintField;
    QrGruposPermitDeb: TSmallintField;
    QrGruposAtivo: TSmallintField;
    Label51: TLabel;
    EdSPED_COD_CTA_REF: TdmkEdit;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGruposAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrGruposAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGruposBeforeOpen(DataSet: TDataSet);
    procedure TbSubGruposDeleting(Sender: TObject; var Allow: Boolean);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure TbSubGruposBeforeInsert(DataSet: TDataSet);
    procedure TbSubGruposBeforeDelete(DataSet: TDataSet);
    procedure BtNivelAcimaClick(Sender: TObject);
    procedure SBGrupoClick(Sender: TObject);
    procedure EdNomeRedefinido(Sender: TObject);
    procedure EdOrdemListaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenConjuntos();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmGrupos: TFmGrupos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, Conjuntos, DmkDAC_PF, UnFinanceiroJan,
  ModuleFin;

{$R *.DFM}

const
  FNivel = 3; // N�o mexer!!!

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGrupos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGrupos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGruposCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGrupos.DefParams;
begin
  VAR_GOTOTABELA := 'Grupos';
  VAR_GOTOMYSQLTABLE := QrGrupos;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT gr.*, cj.Nome NOMECONJUNTO');
  VAR_SQLx.Add('FROM grupos gr');
  VAR_SQLx.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  //
  VAR_SQL1.Add('WHERE gr.Codigo=:P0');
  //
  VAR_SQLa.Add('WHERE gr.Nome Like :P0');
  //
end;

procedure TFmGrupos.EdNomeRedefinido(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    if EdNome2.ValueVariant = EmptyStr then
      EdNome2.ValueVariant := EdNome.ValueVariant;
end;

procedure TFmGrupos.EdOrdemListaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Conjunto: Integer;
begin
  if Key = VK_F4 then
  begin
    Conjunto := QrConjuntosCodigo.Value;
    if MyObjects.FIC(Conjunto = 0, EdConjunto, 'Infome o N�vel 2 primeiro!') then Exit;
    EdOrdemLista.ValueVariant :=
      UFinanceiro.ObtemProximaOrdemNoNivelDoPlanoDeContas('grupos', 'OrdemLista',
    'conjunto', Conjunto);
  end;
end;

procedure TFmGrupos.MostraEdicao(Mostra: Boolean; SQLType: TSQLType);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    if SQLType = stIns then
    begin
      EdNome.ValueVariant       := EmptyStr;
      EdNome2.ValueVariant      := EmptyStr;
      EdCodigo.ValueVariant     := 0;
      EdConjunto.ValueVariant   := 0;
      CBConjunto.KeyValue       := Null;
      EdOrdemLista.ValueVariant := 0;
      CkCtrlaSdo.Checked        := True;
    end else begin
      EdCodigo.ValueVariant     := QrGruposCodigo.Value;
      EdNome.ValueVariant       := QrGruposNome.Value;
      EdNome2.ValueVariant      := QrGruposNome2.Value;
      EdConjunto.ValueVariant   := QrGruposConjunto.Value;
      CBConjunto.KeyValue       := QrGruposConjunto.Value;
      EdOrdemLista.ValueVariant := IntToStr(QrGruposOrdemLista.Value);
      CkCtrlaSdo.Checked        := Geral.IntToBool_0(QrGruposCtrlaSdo.Value);
      //
      RGAnaliSinte.ItemIndex    := QrGruposAnaliSinte.Value;
      CkPermitDeb.Checked       := Geral.IntToBool(QrGruposPermitDeb.Value);
      CkPermitCre.Checked       := Geral.IntToBool(QrGruposPermitCre.Value);
      CkAtivo.Checked           := Geral.IntToBool(QrGruposAtivo.Value);
(*
    EdNome.Text := CO_VAZIO;
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
      EdConjunto.Text := '';
      CBConjunto.KeyValue := Null;
      EdOrdemLista.Text := '';
      CkCtrlaSdo.Checked := True;
    end else begin
      EdCodigo.Text := DBEdCodigo.Text;
      EdNome.Text := DBEdNome.Text;
      EdConjunto.Text := IntToStr(QrGruposConjunto.Value);
      CBConjunto.KeyValue := QrGruposConjunto.Value;
      EdOrdemLista.Text := IntToStr(QrGruposOrdemLista.Value);;
      CkCtrlaSdo.Checked := Geral.IntToBool_0(QrGruposCtrlaSdo.Value);
*)
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGrupos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGrupos.AlteraRegistro;
var
  Grupos : Integer;
begin
  Grupos := QrGruposCodigo.Value;
  if (QrGrupos.State = dsInactive) or (QrGrupos.RecordCount = 0) or
    (QrGruposCodigo.Value <= 0) then
    Exit;
  //
  if not UMyMod.SelLockY(Grupos, Dmod.MyDB, 'Grupos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Grupos, Dmod.MyDB, 'Grupos', 'Codigo');
      MostraEdicao(True, stUpd);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmGrupos.IncluiRegistro;
var
  Cursor : TCursor;
  //Grupos : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
(*
    Grupos := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Grupos', 'Grupo', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Grupos))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro(
      'Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
*)
    MostraEdicao(True, stIns);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmGrupos.QueryPrincipalAfterOpen;
begin
end;

    // ini 2022-03-03
procedure TFmGrupos.ReopenConjuntos();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrConjuntos, Dmod.MyDB, [
  'SELECT cjt.Codigo, CONCAT(cjt.Nome, " (", pla.Nome, ")") Nome  ',
  'FROM conjuntos cjt ',
  'LEFT JOIN plano pla ON pla.Codigo=cjt.Plano ',
  'ORDER BY cjt.Nome, pla.Nome ',
  ' ']);
end;
    // fim 2022-03-03

procedure TFmGrupos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGrupos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGrupos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGrupos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGrupos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGrupos.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmGrupos.BtNivelAcimaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmConjuntos, FmConjuntos, afmoNegarComAviso) then
  begin
    FmConjuntos.ShowModal;
    FmConjuntos.Destroy;
    // ini 2022-03-03

(*
    QrConjuntos.Close;
    UnDmkDAC_PF.AbreQuery(QrConjuntos, Dmod.MyDB);
*)
    ReopenConjuntos();
    //fim 2022-03-03
    DefParams;
  end;
end;

procedure TFmGrupos.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmGrupos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGruposCodigo.Value;
  Close;
end;

procedure TFmGrupos.BtConfirmaClick(Sender: TObject);
var
  Nome, Nome2, SPED_COD_CTA_REF: String;
  Codigo, Conjunto, OrdemLista, CtrlaSdo(*, NotPrntFin*), AnaliSinte,
  PermitDeb, PermitCre, Ativo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  //
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Nome2          := EdNome2.ValueVariant;
  Conjunto       := EdConjunto.ValueVariant;
  OrdemLista     := EdOrdemLista.ValueVariant;
  CtrlaSdo       := Geral.BoolToInt(CkCtrlaSdo.Checked);
  //NotPrntFin     := EdNotPrntFin.ValueVariant;
  SPED_COD_CTA_REF := EdSPED_COD_CTA_REF.ValueVariant;
  AnaliSinte       := RGAnaliSinte.ItemIndex;
  PermitDeb        := Geral.BoolToInt(CkPermitDeb.Checked);
  PermitCre        := Geral.BoolToInt(CkPermitCre.Checked);
  Ativo            := Geral.BoolToInt(CkAtivo.Checked);
  //
  if MyObjects.FIC(Trim(Nome) = EmptyStr, EdNome, 'Informe a descri��o!') then Exit;
  if MyObjects.FIC(AnaliSinte = 0, RGAnaliSinte, 'Informe o idicador de tipo de conta.') then Exit;
  //
  (*if SQLType = stIns then
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Grupos', 'Grupo', 'Codigo');*)
  Codigo := UFinanceiro.ObtemProximoPlaFluCad(SQLType, Codigo, FNivel);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'grupos', False, [
  'Nome', 'Conjunto', 'OrdemLista',
  'CtrlaSdo', (*'NotPrntFin',*) 'Nome2', 'SPED_COD_CTA_REF',
  'AnaliSinte',
  'PermitDeb', 'PermitCre', 'Ativo'], [
  'Codigo'], [
  Nome, Conjunto, OrdemLista,
  CtrlaSdo, (*NotPrntFin,*) Nome2, SPED_COD_CTA_REF,
  AnaliSinte,
  PermitDeb, PermitCre, Ativo], [
  Codigo], True) then
  begin
(*
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO grupos SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE grupos SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Conjunto=:P1, OrdemLista=:P2, CtrlaSdo=:P3, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Geral.IMV(EdConjunto.Text);
  Dmod.QrUpdU.Params[02].AsInteger := Geral.IMV(EdOrdemLista.Text);
  Dmod.QrUpdU.Params[03].AsInteger := Geral.BoolToInt(CkCtrlaSdo.Checked);
  //
  Dmod.QrUpdU.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[05].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[06].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
*)
    DModFin.AtualizaPlaAllCad();
    //
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Grupos', 'Codigo');
    MostraEdicao(False, stLok);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmGrupos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Grupos', 'Codigo');
  MostraEdicao(False, stLok);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Grupos', 'Codigo');
end;

procedure TFmGrupos.BtExcluiClick(Sender: TObject);
begin
//
end;

procedure TFmGrupos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  DBGrid1.Align     := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreTable(TbSubGrupos, Dmod.MyDB);
    // ini 2022-03-03
  // UnDmkDAC_PF.AbreQuery(QrConjuntos, Dmod.MyDB);
  ReopenConjuntos();
    // fim 2022-03-03
end;

procedure TFmGrupos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGruposCodigo.Value,LaRegistro.Caption);
end;

procedure TFmGrupos.SBGrupoClick(Sender: TObject);
var
  Conjunto: Integer;
begin
  Conjunto     := EdConjunto.ValueVariant;
  VAR_CADASTRO := 0;
  //
  FinanceiroJan.CadastroDeConjutos(Conjunto);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdConjunto, CBConjunto, QrConjuntos, VAR_CADASTRO);
    //
    EdConjunto.SetFocus;
  end;
end;

procedure TFmGrupos.SbImprimeClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

procedure TFmGrupos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGrupos.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmGrupos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGrupos.QrGruposAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGrupos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Grupos', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmGrupos.QrGruposAfterScroll(DataSet: TDataSet);
begin
  TbSubGrupos.Filter := 'Grupo='+IntToStr(QrGruposCodigo.Value);
  BtAltera.Enabled := QrGruposCodigo.Value > 0;
end;

procedure TFmGrupos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGruposCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Grupos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGrupos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGrupos.QrGruposBeforeOpen(DataSet: TDataSet);
begin
  QrGruposCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmGrupos.TbSubGruposBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmGrupos.TbSubGruposBeforeInsert(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmGrupos.TbSubGruposDeleting(Sender: TObject;
  var Allow: Boolean);
begin
  Allow := False;
  Geral.MB_Aviso(
  'Para excluir sub-grupo de conta acesse o cadastro de sub-grupos!');
end;

end.

