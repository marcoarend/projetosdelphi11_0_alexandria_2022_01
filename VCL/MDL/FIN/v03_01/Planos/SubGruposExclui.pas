unit SubGruposExclui;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Grids, DBGrids,
  Variants, dmkPermissoes, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral,
  dmkImage, UnDmkEnums;

type
  TFmSubGruposExclui = class(TForm)
    PainelDados: TPanel;
    DsSubgrupos: TDataSource;
    QrSubGrupos: TmySQLQuery;
    Panel1: TPanel;
    Label1: TLabel;
    EdSubgrupo: TdmkEditCB;
    CBSubGrupo: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdNova: TdmkEditCB;
    CBNova: TdmkDBLookupComboBox;
    Label3: TLabel;
    QrSubGruposCodigo: TIntegerField;
    QrSubGruposNome: TWideStringField;
    DsNova: TDataSource;
    QrNova: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsContas: TDataSource;
    QrContas: TmySQLQuery;
    DBGLct: TDBGrid;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdSubgrupoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrSubGruposAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmSubGruposExclui: TFmSubGruposExclui;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmSubGruposExclui.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSubGruposExclui.BtConfirmaClick(Sender: TObject);
var
  Nova, Subgrupo: Integer;
begin
  if CBSubgrupo.KeyValue = Null then
  begin
    Geral.MB_Aviso('Informe um sub-grupo a excluir!');
    EdSubgrupo.SetFocus;
    Exit;
  end;
  Subgrupo := Geral.IMV(EdSubgrupo.Text);
  if (CBNova.KeyValue = Null) and (QrContas.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Informe um sub-grupo para transferência!');
    EdNova.SetFocus;
    Exit;
  end;
  Nova := Geral.IMV(EdNova.Text);
  if Geral.MB_Pergunta('Confirma a exclusão do sub-grupo "'+
  CBSubgrupo.Text+'"?') <> ID_YES then Exit;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE contas SET Subgrupo=:P0 WHERE Subgrupo=:P1');
  Dmod.QrUpd.Params[0].AsInteger := Nova;
  Dmod.QrUpd.Params[1].AsInteger := Subgrupo;
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM subgrupos WHERE Codigo=:P0');
  Dmod.QrUpd.Params[0].AsInteger := Subgrupo;
  Dmod.QrUpd.ExecSQL;
  //
  UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Subgrupos', Subgrupo);
  Close;
end;

procedure TFmSubGruposExclui.EdSubgrupoChange(Sender: TObject);
begin
  if Length(Trim(EdSubgrupo.Text)) = 0 then QrContas.Close;
end;

procedure TFmSubGruposExclui.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrSubgrupos, Dmod.MyDB);
end;

procedure TFmSubGruposExclui.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSubGruposExclui.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSubGruposExclui.QrSubGruposAfterScroll(DataSet: TDataSet);
begin
  if CBSubgrupo.KeyValue <> Null then
  begin
    EdNova.Text := '';
    CBNova.KeyValue := Null;
    QrNova.Close;
    QrNova.Params[0].AsInteger := QrSubgruposCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrNova, Dmod.MyDB);
    //
    QrContas.Close;
    QrContas.Params[0].AsInteger := QrSubgruposCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  end;
end;

end.

