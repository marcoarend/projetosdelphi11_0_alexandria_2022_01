object FmCtaCfgCab: TFmCtaCfgCab
  Left = 368
  Top = 194
  Caption = 'FIN-PLCTA-039 :: Perfil Restritivo de Contas'
  ClientHeight = 555
  ClientWidth = 965
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnEdita: TPanel
    Left = 0
    Top = 139
    Width = 965
    Height = 416
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 965
      Height = 154
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 20
        Top = 20
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 94
        Top = 20
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 20
        Top = 39
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 94
        Top = 39
        Width = 779
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 338
      Width = 965
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 830
        Top = 18
        Width = 132
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 139
    Width = 965
    Height = 416
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 965
      Height = 80
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 20
        Top = 20
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 94
        Top = 20
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 20
        Top = 39
        Width = 69
        Height = 21
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCtaCfgCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 94
        Top = 39
        Width = 848
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsCtaCfgCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 337
      Width = 965
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 35
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 321
        Top = 18
        Width = 641
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 478
          Top = 0
          Width = 163
          Height = 58
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Left = 5
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Perfil'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Left = 158
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Conta'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 80
      Width = 965
      Height = 182
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      DataSource = DsCtaCfgIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -15
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'Conta'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CTA'
          Title.Caption = 'Descri'#231#227'o'
          Width = 683
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 906
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 640
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 371
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Perfil Restritivo de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 371
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Perfil Restritivo de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 371
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Perfil Restritivo de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 64
    Width = 965
    Height = 75
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 960
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 33
        Width = 960
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrCtaCfgCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCtaCfgCabBeforeOpen
    AfterOpen = QrCtaCfgCabAfterOpen
    AfterScroll = QrCtaCfgCabAfterScroll
    SQL.Strings = (
      'SELECT * FROM ctacfgcab'
      'WHERE Codigo > 0')
    Left = 64
    Top = 64
    object QrCtaCfgCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtaCfgCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrCtaCfgCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCtaCfgCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCtaCfgCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCtaCfgCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCtaCfgCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCtaCfgCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCtaCfgCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsCtaCfgCab: TDataSource
    DataSet = QrCtaCfgCab
    Left = 92
    Top = 64
  end
  object QrCtaCfgIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.Nome NO_CTA, cci.* '
      'FROM ctacfgits cci'
      'LEFT JOIN contas cta ON cta.Codigo=cci.Genero'
      'WHERE cci.Codigo=:P0'
      'ORDER BY cta.Nome')
    Left = 148
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCtaCfgItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtaCfgItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCtaCfgItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCtaCfgItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCtaCfgItsNO_CTA: TWideStringField
      FieldName = 'NO_CTA'
      Size = 50
    end
  end
  object DsCtaCfgIts: TDataSource
    DataSet = QrCtaCfgIts
    Left = 176
    Top = 64
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 372
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Duplicaratual1: TMenuItem
      Caption = '&Duplicar atual'
      OnClick = Duplicaratual1Click
    end
    object Sacramantar1: TMenuItem
      Caption = 'Sancionar'
      object odos1: TMenuItem
        Caption = 'Todos (criar'#225' c'#243'd neg. p/ cada cli. int.)'
        OnClick = odos1Click
      end
      object SelecionadoCdigonegativo1: TMenuItem
        Caption = 'Perfil selecionado (c'#243'd neg.)'
        Enabled = False
        OnClick = SelecionadoCdigonegativo1Click
      end
    end
  end
  object QrGeneros: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Genero '
      'FROM lct0001a'
      ''
      'UNION'
      ''
      'SELECT DISTINCT Genero '
      'FROM lct0001b'
      ''
      'UNION'
      ''
      'SELECT DISTINCT Genero '
      'FROM lct0001d'
      ''
      'ORDER BY Genero')
    Left = 236
    Top = 64
    object QrGenerosGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object QrIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Count(*) ITENS'
      'FROM ctacfgits'
      'WHERE Codigo=:P0'
      'AND Genero=:P1')
    Left = 264
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrItsITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object QrCCI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ctacfgits'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 292
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCCICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCCIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCCIGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCCINome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
end
