unit ContasMesGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, mySQLDbTables,
  dmkGeral, dmkDBGrid, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel,
  dmkImage, UnDmkEnums;

type
  TFmContasMesGer = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    PnNomeCond: TPanel;
    QrContasMes: TmySQLQuery;
    QrContasMesCodigo: TIntegerField;
    QrContasMesControle: TIntegerField;
    QrContasMesCliInt: TIntegerField;
    QrContasMesDescricao: TWideStringField;
    QrContasMesPeriodoIni: TIntegerField;
    QrContasMesPeriodoFim: TIntegerField;
    QrContasMesValorMin: TFloatField;
    QrContasMesValorMax: TFloatField;
    QrContasMesPERIODOINI_TXT: TWideStringField;
    QrContasMesPERIODOFIM_TXT: TWideStringField;
    QrContasMesQtdeMin: TIntegerField;
    QrContasMesQtdeMax: TIntegerField;
    QrContasMesDiaAlert: TSmallintField;
    QrContasMesDiaVence: TSmallintField;
    DsContasMes: TDataSource;
    GradeContasMes: TdmkDBGrid;
    QrContasMesNO_Conta: TWideStringField;
    QrContasMesTipMesRef: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtSaida: TBitBtn;
    QrContasMesCO_SHOW: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    //procedure ReopenContas(EntInt: Integer);
    procedure MostraContasMes(SQLType: TSQLType);
    //procedure TravaForm();
  public
    { Public declarations }
    FEntInt: Integer;
    procedure ReopenContasMes(EntInt: Integer);
  end;

  var
  FmContasMesGer: TFmContasMesGer;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule, ContasMes, MyDBCheck,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmContasMesGer.BtAlteraClick(Sender: TObject);
begin
  MostraContasMes(stUpd);
end;

procedure TFmContasMesGer.BtExcluiClick(Sender: TObject);
begin
  DBCheck.ExcluiRegistro(Dmod.QrUpd, QrContasMes, 'contasmes', ['Controle'],
    ['Controle'], True, 'Confirma a exclus�o do item selecionado?');
end;

procedure TFmContasMesGer.BtIncluiClick(Sender: TObject);
begin
  MostraContasMes(stIns);
end;

procedure TFmContasMesGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasMesGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasMesGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmContasMesGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasMesGer.FormShow(Sender: TObject);
begin
  ReopenContasMes(FEntInt);
end;

procedure TFmContasMesGer.MostraContasMes(SQLType: TSQLType);
begin
  if UMyMod.FormInsUpd_Cria(TFmContasMes, FmContasMes, afmoNegarComAviso,
    QrContasMes, SQLType) then
  begin
    if SQLType = stIns then
    begin
      FmContasMes.EdCliInt.ValueVariant := QrContasMesCO_SHOW.Value;
      FmContasMes.CBCliInt.KeyValue     := QrContasMesCO_SHOW.Value;
    end else
      FmContasMes.FControle := QrContasMesControle.Value;
    FmContasMes.FQuery := QrContasMes;
    FmContasMes.ShowModal;
    FmContasMes.Destroy;
  end;
end;

procedure TFmContasMesGer.ReopenContasMes(EntInt: Integer);
var
  Qry: TmySQLQuery;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrContasMes, Dmod.MyDB, [
  'SELECT IF(en.Codigo<-10,en.Filial,en.CliInt) + 0.000 CO_SHOW, ',
  'co.Nome NO_Conta, cm.* ',
  'FROM contasmes cm',
  'LEFT JOIN contas co ON co.Codigo=cm.Codigo ',
  'LEFT JOIN entidades en ON en.Codigo=cm.CliInt ',
  'WHERE cm.CliInt=' + Geral.FF0(EntInt),
  '']);
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT IF(Tipo=0,RazaoSocial,Nome) NOMECLI ',
    'FROM entidades ',
    'WHERE Codigo=' + Geral.FF0(EntInt),
    '']);
    PnNomeCond.Caption := Geral.FF0(EntInt) + ' - ' + Qry.FieldByName('NOMECLI').AsString;
  finally
    Qry.Free;
  end;;
end;

{
procedure TFmContasMesGer.ReopenContas(EntInt: Integer);
begin
  QrContas.Close;
  QrContas.SQL.Clear;
  QrContas.SQL.Add('SELECT Codigo, Nome');
  QrContas.SQL.Add('FROM contas');
  QrContas.SQL.Add('WHERE Debito="V"');
  if EntInt <> 0 then
  begin
    QrContas.SQL.Add('AND NOT (Codigo IN (');
    QrContas.SQL.Add('  SELECT Codigo FROM confpgtos');
    QrContas.SQL.Add('  WHERE CliInt=' + FormatFloat('0', EntInt));
    QrContas.SQL.Add('))');
  end;
  QrContas.SQL.Add('ORDER BY Nome');
  QrContas.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
end;
}

end.

