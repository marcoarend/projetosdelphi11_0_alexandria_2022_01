object FmPlaAllCad: TFmPlaAllCad
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-051:: Lista do Plano de Contas'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 305
        Height = 32
        Caption = 'Lista do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 305
        Height = 32
        Caption = 'Lista do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 305
        Height = 32
        Caption = 'Lista do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object dmkDBGridZTO1: TdmkDBGridZTO
      Left = 0
      Top = 13
      Width = 812
      Height = 454
      Align = alClient
      DataSource = DsPlaAllCad
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Ordens'
          Title.Caption = 'Refer'#234'ncia'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 400
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SIGLA_AS'
          Title.Caption = 'A/S'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CRED'
          Title.Caption = 'Cr'#233'dito'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_DEBI'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SPED_COD_CTA_REF'
          Title.Caption = 'RFB'
          Width = 100
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 13
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrPlaAllCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE pac.AnaliSinte'
      '  WHEN 1 THEN "S"'
      '  WHEN 2 THEN "A"'
      '  ELSE "?" END SIGLA_AS,'
      'CASE pac.Credito'
      '  WHEN 0 THEN "N'#195'O"'
      '  WHEN 2 THEN "SIM"'
      '  ELSE "???" END NO_CRED,'
      'CASE pac.Debito'
      '  WHEN 0 THEN "N'#195'O"'
      '  WHEN 2 THEN "SIM"'
      '  ELSE "???" END NO_DEBI,'
      'pac.*'
      'FROM plaallcad pac'
      'ORDER BY pac.Ordens')
    Left = 384
    Top = 284
    object QrPlaAllCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPlaAllCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrPlaAllCadNivel: TSmallintField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPlaAllCadAnaliSinte: TIntegerField
      FieldName = 'AnaliSinte'
      Required = True
    end
    object QrPlaAllCadCredito: TSmallintField
      FieldName = 'Credito'
      Required = True
    end
    object QrPlaAllCadDebito: TSmallintField
      FieldName = 'Debito'
      Required = True
    end
    object QrPlaAllCadCodNiv1: TIntegerField
      FieldName = 'CodNiv1'
      Required = True
    end
    object QrPlaAllCadCodNiv2: TIntegerField
      FieldName = 'CodNiv2'
      Required = True
    end
    object QrPlaAllCadCodNiv3: TIntegerField
      FieldName = 'CodNiv3'
      Required = True
    end
    object QrPlaAllCadCodNiv4: TIntegerField
      FieldName = 'CodNiv4'
      Required = True
    end
    object QrPlaAllCadCodNiv5: TIntegerField
      FieldName = 'CodNiv5'
      Required = True
    end
    object QrPlaAllCadCodNiv6: TIntegerField
      FieldName = 'CodNiv6'
      Required = True
    end
    object QrPlaAllCadOrdNiv1: TIntegerField
      FieldName = 'OrdNiv1'
      Required = True
    end
    object QrPlaAllCadOrdNiv2: TIntegerField
      FieldName = 'OrdNiv2'
      Required = True
    end
    object QrPlaAllCadOrdNiv3: TIntegerField
      FieldName = 'OrdNiv3'
      Required = True
    end
    object QrPlaAllCadOrdNiv4: TIntegerField
      FieldName = 'OrdNiv4'
      Required = True
    end
    object QrPlaAllCadOrdNiv5: TIntegerField
      FieldName = 'OrdNiv5'
      Required = True
    end
    object QrPlaAllCadOrdNiv6: TIntegerField
      FieldName = 'OrdNiv6'
      Required = True
    end
    object QrPlaAllCadNomNiv1: TWideStringField
      FieldName = 'NomNiv1'
      Size = 60
    end
    object QrPlaAllCadNomNiv2: TWideStringField
      FieldName = 'NomNiv2'
      Size = 60
    end
    object QrPlaAllCadNomNiv3: TWideStringField
      FieldName = 'NomNiv3'
      Size = 60
    end
    object QrPlaAllCadNomNiv4: TWideStringField
      FieldName = 'NomNiv4'
      Size = 60
    end
    object QrPlaAllCadNomNiv5: TWideStringField
      FieldName = 'NomNiv5'
      Size = 60
    end
    object QrPlaAllCadNomNiv6: TWideStringField
      FieldName = 'NomNiv6'
      Size = 60
    end
    object QrPlaAllCadSPED_COD_CTA_REF: TWideStringField
      FieldName = 'SPED_COD_CTA_REF'
      Size = 60
    end
    object QrPlaAllCadOrdens: TWideStringField
      FieldName = 'Ordens'
      Size = 255
    end
    object QrPlaAllCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPlaAllCadSIGLA_AS: TWideStringField
      FieldName = 'SIGLA_AS'
      Required = True
      Size = 1
    end
    object QrPlaAllCadNO_CRED: TWideStringField
      FieldName = 'NO_CRED'
      Required = True
      Size = 3
    end
    object QrPlaAllCadNO_DEBI: TWideStringField
      FieldName = 'NO_DEBI'
      Required = True
      Size = 3
    end
  end
  object DsPlaAllCad: TDataSource
    DataSet = QrPlaAllCad
    Left = 384
    Top = 340
  end
end
