unit ContasConfCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, mySQLDbTables,
  dmkGeral, dmkDBGrid, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel,
  dmkImage, UnDmkEnums;

type
  TFmContasConfCad = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    PnEdita: TPanel;
    PnNomeCond: TPanel;
    QrConfPgtos: TmySQLQuery;
    DsConfPgtos: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    QrConfPgtosCodigo: TIntegerField;
    QrConfPgtosQtdeMin: TIntegerField;
    QrConfPgtosQtdeMax: TIntegerField;
    QrConfPgtosValrMin: TFloatField;
    QrConfPgtosValrMax: TFloatField;
    QrConfPgtosNOMECONTA: TWideStringField;
    PnControle: TPanel;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    PnConfirma: TPanel;
    BitBtn1: TBitBtn;
    Panel6: TPanel;
    BitBtn2: TBitBtn;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    EdQtdeMax: TdmkEdit;
    Label3: TLabel;
    EdQtdeMin: TdmkEdit;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    EdValrMin: TdmkEdit;
    Label5: TLabel;
    Label4: TLabel;
    EdValrMax: TdmkEdit;
    GroupBox3: TGroupBox;
    Label6: TLabel;
    EdDiaAlert: TdmkEdit;
    EdDiaVence: TdmkEdit;
    Label7: TLabel;
    QrConfPgtosDiaAlert: TSmallintField;
    QrConfPgtosDiaVence: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrConfPgtosCliInt: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenContas(EntInt: Integer);
    //procedure TravaForm();
  public
    { Public declarations }
    FEntInt: Integer;
    procedure ReopenConfPgtos(EntInt: Integer);
  end;

  var
  FmContasConfCad: TFmContasConfCad;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmContasConfCad.BitBtn1Click(Sender: TObject);
var
  Codigo, CliInt, QtdeMin, QtdeMax, DiaAlert, DiaVence: Integer;
  ValrMin, ValrMax: Double;
begin
  Codigo  := EdConta.ValueVariant;
  if Codigo < 1 then
  begin
    Geral.MB_Aviso('Informe a conta do plano de contas!');
    EdConta.SetFocus;
    Exit;
  end;
  CliInt  := FEntInt;
  if CliInt = 0 then
  begin
    Geral.MB_Aviso('Nenhum cliente interno foi informado!');
    Exit;
  end;
  QtdeMin := EdQtdeMin.ValueVariant;
  QtdeMax := EdQtdeMax.ValueVariant;
  ValrMin := EdValrMin.ValueVariant;
  ValrMax := EdValrMax.ValueVariant;
  //
  DiaAlert := EdDiaAlert.ValueVariant;
  DiaVence := EdDiaVence.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'confpgtos', False, [
    'QtdeMin', 'QtdeMax', 'ValrMin', 'ValrMax', 'DiaAlert', 'DiaVence'
  ], ['Codigo', 'CliInt'], [
    QtdeMin, QtdeMax, ValrMin, ValrMax, DiaAlert, DiaVence
  ], [Codigo, CliInt], True) then
  begin
    EdConta.ValueVariant   := 0;
    CBConta.KeyValue       := 0;
    EdQtdeMin.ValueVariant := 0;
    EdQtdeMax.ValueVariant := 0;
    EdValrMin.ValueVariant := 0;
    EdValrMax.ValueVariant := 0;
    EdDiaAlert.ValueVariant := 0;
    EdDiaVence.ValueVariant := 0;
    UMyMod.TravaFmEmPanelInsUpd([PnEdita],[PnControle], ImgTipo);
    ReopenConfPgtos(FEntInt);
    if CkContinuar.Checked then
      BtIncluiClick(Self);
  end;
end;

procedure TFmContasConfCad.BitBtn2Click(Sender: TObject);
begin
  UMyMod.TravaFmEmPanelInsUpd([PnEdita],[PnControle], ImgTipo);
end;

procedure TFmContasConfCad.BtAlteraClick(Sender: TObject);
begin
  if (QrConfPgtos.State = dsInactive) or (QrConfPgtos.RecordCount = 0) then
    Exit;  
  //
  EdConta.Enabled := False;
  CBConta.Enabled := False;
  ReopenContas(0);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrConfPgtos, [PnControle],
    [PnEdita], EdQtdeMin, ImgTipo, 'confpgtos');
end;

procedure TFmContasConfCad.BtExcluiClick(Sender: TObject);
begin
  DBCheck.ExcluiRegistro(Dmod.QrUpd, QrConfPgtos, 'confpgtos', ['Codigo', 'CliInt'],
    ['Codigo', 'CliInt'], True, 'Confirma a exclus�o do item selecionado?');
end;

procedure TFmContasConfCad.BtIncluiClick(Sender: TObject);
begin
  // Deve ser antes para evitar erro
  EdConta.Enabled := True;
  CBConta.Enabled := True;
  ReopenContas(FEntInt);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrConfPgtos, [PnControle],
    [PnEdita], EdConta, ImgTipo, 'confpgtos');
end;

procedure TFmContasConfCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasConfCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasConfCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmContasConfCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasConfCad.FormShow(Sender: TObject);
begin
  ReopenConfPgtos(FEntInt);
end;

procedure TFmContasConfCad.ReopenConfPgtos(EntInt: Integer);
begin
  QrConfPgtos.Close;
  QrConfPgtos.Params[0].AsInteger := EntInt;
  UnDmkDAC_PF.AbreQuery(QrConfPgtos, Dmod.myDB);
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT IF(Tipo=0,RazaoSocial,Nome) NOMECLI');
  Dmod.QrAux.SQL.Add('FROM entidades WHERE Codigo=' + FormatFloat('0', EntInt));
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.myDB);
  PnNomeCond.Caption := Dmod.QrAux.FieldByName('NOMECLI').AsString;
  //
end;

procedure TFmContasConfCad.ReopenContas(EntInt: Integer);
begin
  QrContas.Close;
  QrContas.SQL.Clear;
  QrContas.SQL.Add('SELECT Codigo, Nome');
  QrContas.SQL.Add('FROM contas');
  QrContas.SQL.Add('WHERE Debito="V"');
  if EntInt <> 0 then
  begin
    QrContas.SQL.Add('AND NOT (Codigo IN (');
    QrContas.SQL.Add('  SELECT Codigo FROM confpgtos');
    QrContas.SQL.Add('  WHERE CliInt=' + FormatFloat('0', EntInt));
    QrContas.SQL.Add('))');
  end;
  QrContas.SQL.Add('ORDER BY Nome');
  QrContas.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.myDB);
end;

{
procedure TFmContasConfCad.TravaForm;
begin
  PnControle.Visible := True;
  PnEdita.Visible    := False;
  ImgTipo.SQLType     := stLok;
end;
}

end.

