object FmCtSub1: TFmCtSub1
  Left = 368
  Top = 194
  Caption = 'FIN-PLCTA-049 :: Cadastro de N'#237'vel 6 do Plano de Contas'
  ClientHeight = 364
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 268
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 32
    ExplicitTop = 104
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 189
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 12
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 72
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label19: TLabel
        Left = 426
        Top = 16
        Width = 99
        Height = 13
        Caption = 'Descri'#231#227'o substituta:'
      end
      object Label17: TLabel
        Left = 365
        Top = 56
        Width = 38
        Height = 13
        Caption = 'N'#237'vel 5:'
      end
      object Label11: TLabel
        Left = 286
        Top = 100
        Width = 26
        Height = 13
        Caption = 'Sigla:'
      end
      object Label51: TLabel
        Left = 12
        Top = 100
        Width = 265
        Height = 13
        Caption = 'Conta correl. no Plano de Contas Referenciado da RFB:'
      end
      object Label27: TLabel
        Left = 12
        Top = 56
        Width = 153
        Height = 13
        Caption = 'Descri'#231#227'o padr'#227'o para hist'#243'rico:'
      end
      object Label4: TLabel
        Left = 504
        Top = 101
        Width = 55
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem [F4]:'
      end
      object Label3: TLabel
        Left = 418
        Top = 100
        Width = 79
        Height = 13
        Caption = 'C'#243'digo terceiros:'
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 72
        Top = 32
        Width = 348
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnRedefinido = EdNomeRedefinido
      end
      object EdNome2: TdmkEdit
        Left = 426
        Top = 32
        Width = 348
        Height = 21
        Color = clWhite
        MaxLength = 50
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome2'
        UpdCampo = 'Nome2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdConta: TdmkEditCB
        Left = 365
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Conta'
        UpdCampo = 'Conta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBConta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBConta: TdmkDBLookupComboBox
        Left = 424
        Top = 72
        Width = 349
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 5
        dmkEditCB = EdConta
        QryCampo = 'Conta'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdSigla: TdmkEdit
        Left = 286
        Top = 116
        Width = 127
        Height = 21
        Color = clWhite
        MaxLength = 15
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Sigla'
        UpdCampo = 'Sigla'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSPED_COD_CTA_REF: TdmkEdit
        Left = 12
        Top = 116
        Width = 268
        Height = 21
        Color = clWhite
        MaxLength = 15
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'SPED_COD_CTA_REF'
        UpdCampo = 'SPED_COD_CTA_REF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNome3: TdmkEdit
        Left = 12
        Top = 72
        Width = 348
        Height = 21
        Color = clWhite
        MaxLength = 50
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome3'
        UpdCampo = 'Nome3'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdOrdemLista: TdmkEdit
        Left = 503
        Top = 116
        Width = 62
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'OrdemLista'
        UpdCampo = 'OrdemLista'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdOrdemListaKeyDown
      end
      object EdAntigo: TdmkEdit
        Left = 418
        Top = 116
        Width = 81
        Height = 21
        Color = clWhite
        MaxLength = 15
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Antigo'
        UpdCampo = 'Antigo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGAnaliSinte: TdmkRadioGroup
        Left = 568
        Top = 100
        Width = 201
        Height = 41
        Caption = 'Indicador do tipo de conta:'
        Columns = 3
        ItemIndex = 2
        Items.Strings = (
          'Indef.'
          'Sint'#233'tico'
          'Anal'#237'tico')
        TabOrder = 10
        QryCampo = 'AnaliSinte'
        UpdCampo = 'AnaliSinte'
        UpdType = utYes
        OldValor = 0
      end
      object CkPermitDeb: TdmkCheckBox
        Left = 12
        Top = 142
        Width = 68
        Height = 17
        Caption = 'D'#233'bito.'
        TabOrder = 11
        QryCampo = 'PermitDeb'
        UpdCampo = 'PermitDeb'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkPermitCre: TdmkCheckBox
        Left = 88
        Top = 142
        Width = 68
        Height = 17
        Caption = 'Cr'#233'dito.'
        TabOrder = 12
        QryCampo = 'PermitCre'
        UpdCampo = 'PermitCre'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkAtivo: TdmkCheckBox
        Left = 168
        Top = 142
        Width = 57
        Height = 17
        Caption = 'Ativo.'
        TabOrder = 13
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 205
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 645
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 268
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 181
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 12
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 72
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 426
        Top = 16
        Width = 99
        Height = 13
        Caption = 'Descri'#231#227'o substituta:'
      end
      object Label6: TLabel
        Left = 12
        Top = 56
        Width = 153
        Height = 13
        Caption = 'Descri'#231#227'o padr'#227'o para hist'#243'rico:'
      end
      object Label8: TLabel
        Left = 365
        Top = 56
        Width = 38
        Height = 13
        Caption = 'N'#237'vel 5:'
      end
      object Label10: TLabel
        Left = 12
        Top = 100
        Width = 265
        Height = 13
        Caption = 'Conta correl. no Plano de Contas Referenciado da RFB:'
      end
      object Label12: TLabel
        Left = 418
        Top = 100
        Width = 79
        Height = 13
        Caption = 'C'#243'digo terceiros:'
      end
      object Label13: TLabel
        Left = 506
        Top = 101
        Width = 34
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem:'
      end
      object Label14: TLabel
        Left = 286
        Top = 100
        Width = 26
        Height = 13
        Caption = 'Sigla:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCtSub1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 72
        Top = 32
        Width = 348
        Height = 21
        DataField = 'Nome'
        DataSource = DsCtSub1
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 426
        Top = 32
        Width = 348
        Height = 21
        DataField = 'Nome2'
        DataSource = DsCtSub1
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 12
        Top = 72
        Width = 349
        Height = 21
        DataField = 'Nome3'
        DataSource = DsCtSub1
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 364
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Conta'
        DataSource = DsCtSub1
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 424
        Top = 72
        Width = 349
        Height = 21
        DataField = 'NO_Conta'
        DataSource = DsCtSub1
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 12
        Top = 116
        Width = 268
        Height = 21
        DataField = 'SPED_COD_CTA_REF'
        DataSource = DsCtSub1
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 286
        Top = 116
        Width = 127
        Height = 21
        DataField = 'Sigla'
        DataSource = DsCtSub1
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 419
        Top = 116
        Width = 81
        Height = 21
        DataField = 'Antigo'
        DataSource = DsCtSub1
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 505
        Top = 116
        Width = 62
        Height = 21
        DataField = 'OrdemLista'
        DataSource = DsCtSub1
        TabOrder = 9
      end
      object DBCheckBox1: TDBCheckBox
        Left = 12
        Top = 142
        Width = 72
        Height = 17
        Caption = 'D'#233'bito'
        DataField = 'PermitDeb'
        DataSource = DsCtSub1
        TabOrder = 10
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBCheckBox2: TDBCheckBox
        Left = 88
        Top = 142
        Width = 72
        Height = 17
        Caption = 'Cr'#233'dito.'
        DataField = 'PermitCre'
        DataSource = DsCtSub1
        TabOrder = 11
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBCheckBox3: TDBCheckBox
        Left = 168
        Top = 142
        Width = 72
        Height = 17
        Caption = 'Ativo.'
        DataField = 'Ativo'
        DataSource = DsCtSub1
        TabOrder = 12
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 572
        Top = 100
        Width = 201
        Height = 41
        Caption = 'Indicador do tipo de conta:'
        Columns = 3
        DataField = 'AnaliSinte'
        DataSource = DsCtSub1
        Items.Strings = (
          'Indef.'
          'Sint'#233'tico'
          'Anal'#237'tico')
        TabOrder = 13
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 204
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 88
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 262
        Top = 15
        Width = 520
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 387
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 486
        Height = 32
        Caption = 'Cadastro de N'#237'vel 6 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 486
        Height = 32
        Caption = 'Cadastro de N'#237'vel 6 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 486
        Height = 32
        Caption = 'Cadastro de N'#237'vel 6 do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrCtSub1: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCtSub1BeforeOpen
    AfterOpen = QrCtSub1AfterOpen
    AfterScroll = QrCtSub1AfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM ctsub1')
    Left = 248
    Top = 84
    object QrCtSub1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCtSub1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrCtSub1Nome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 60
    end
    object QrCtSub1Nome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 60
    end
    object QrCtSub1ID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrCtSub1Conta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrCtSub1OrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Required = True
    end
    object QrCtSub1Sigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
    end
    object QrCtSub1Antigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrCtSub1NotPrntFin: TIntegerField
      FieldName = 'NotPrntFin'
      Required = True
    end
    object QrCtSub1ImpactEstru: TSmallintField
      FieldName = 'ImpactEstru'
      Required = True
    end
    object QrCtSub1SPED_COD_CTA_REF: TWideStringField
      FieldName = 'SPED_COD_CTA_REF'
      Size = 60
    end
    object QrCtSub1AnaliSinte: TIntegerField
      FieldName = 'AnaliSinte'
      Required = True
    end
    object QrCtSub1NO_Conta: TWideStringField
      FieldName = 'NO_Conta'
      Size = 60
    end
    object QrCtSub1PermitCre: TSmallintField
      FieldName = 'PermitCre'
      Required = True
    end
    object QrCtSub1PermitDeb: TSmallintField
      FieldName = 'PermitDeb'
      Required = True
    end
    object QrCtSub1Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsCtSub1: TDataSource
    DataSet = QrCtSub1
    Left = 248
    Top = 132
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 248
    Top = 180
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo> 0'
      'AND AnaliSinte=1'
      'ORDER BY Nome')
    Left = 524
    Top = 280
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 524
    Top = 332
  end
end
