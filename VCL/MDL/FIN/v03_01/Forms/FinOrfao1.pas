unit FinOrfao1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, Menus, dmkDBGrid,
  dmkGeral, dmkImage, UnDmkEnums;

type
  TFmFinOrfao1 = class(TForm)
    Panel1: TPanel;
    DsELSCI: TDataSource;
    PMAcao: TPopupMenu;
    Atribuirlanamentoaodonodacarteira1: TMenuItem;
    Excluirlanamentoincondicionalmente1: TMenuItem;
    DBGLct: TdmkDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtAcao: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Atribuirlanamentoaodonodacarteira1Click(Sender: TObject);
    procedure Excluirlanamentoincondicionalmente1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
  private
    { Private declarations }
    procedure ExecuteAcao(Acao: Integer);
  public
    { Public declarations }
    FTabLct: String;
  end;

  var
  FmFinOrfao1: TFmFinOrfao1;

implementation

uses UnMyObjects, ModuleFin, Module, UnInternalConsts, UnFinanceiro, DmkDAC_PF,
  UnDmkProcFunc;

{$R *.DFM}

procedure TFmFinOrfao1.Atribuirlanamentoaodonodacarteira1Click(Sender: TObject);
begin
  ExecuteAcao(01);
end;

procedure TFmFinOrfao1.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmFinOrfao1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFinOrfao1.Excluirlanamentoincondicionalmente1Click(
  Sender: TObject);
begin
  ExecuteAcao(02);
end;

procedure TFmFinOrfao1.ExecuteAcao(Acao: Integer);
  procedure DefineAcao(Acao, Linha: Integer);
    procedure DefineClienteInternoPelaCarteira(Controle, Sub: Integer);
    begin
      Dmod.QrUpd.SQL.Clear;
      {  Demora muito!
      Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLct + ' lan, carteiras car');
      Dmod.QrUpd.SQL.Add('SET lan.CliInt=car.ForneceI');
      Dmod.QrUpd.SQL.Add('WHERE lan.Carteira=car.Codigo');
      Dmod.QrUpd.SQL.Add('AND lan.Controle=:P0 AND lan.Sub=:P1');
      Dmod.QrUpd.SQL.Add('AND lan.CliInt=0');
      }
      Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLct + ' lan SET lan.CliInt=:P0');
      Dmod.QrUpd.SQL.Add('WHERE lan.Controle=:P0 AND lan.Sub=:P1');
      Dmod.QrUpd.SQL.Add('AND lan.CliInt=0');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := DmodFin.QrELSCICART_DONO.Value;
      Dmod.QrUpd.Params[01].AsInteger := Controle;
      Dmod.QrUpd.Params[02].AsInteger := Sub;
      Dmod.QrUpd.ExecSQL;
    end;
  begin
    case Acao of
      01: DefineClienteInternoPelaCarteira(DModFin.QrELSCIControle.Value,
            DModFin.QrELSCISub.Value);
      02: UFinanceiro.ExcluiLct_Unico(FTabLct,  DModFin.QrELSCI.Database,
            DModFin.QrELSCIData.Value, DModFin.QrELSCITipo.Value,
            DModFin.QrELSCICarteira.Value, DModFin.QrELSCIControle.Value,
            DModFin.QrELSCISub.Value, dmkPF.MotivDel_ValidaCodigo(305),
            False);
    end;
  end;
var
  i: Integer;
begin
  Screen.Cursor := crHourGlass;
  if Acao = 02 then
  begin
    if DBGLct.SelectedRows.Count < 2 then
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o dos '+
      IntToStr(DBGLct.SelectedRows.Count) + ' itens selecionados?') <> ID_YES then
        Exit;
    end else begin
      if Geral.MB_Pergunta(
      'Confirma a exclus�o do item selecionado?') <> ID_YES then
        Exit;
    end;
  end;
  if DBGLct.SelectedRows.Count > 0 then
  begin
    with DBGLct.DataSource.DataSet do
    for i:= 0 to DBGLct.SelectedRows.Count-1 do
    begin
      //GotoBookmark(DBGLct.SelectedRows.Items[i]);
      GotoBookmark(DBGLct.SelectedRows.Items[i]);
      DefineAcao(Acao, DmodFin.QrELSCIControle.Value);
    end;
  end else DefineAcao(Acao, DmodFin.QrELSCIControle.Value);
  //
  DmodFin.QrELSCI.Close;
  UnDmkDAC_PF.AbreQueryApenas(DmodFin.QrELSCI);
  Screen.Cursor := crDefault;
end;

procedure TFmFinOrfao1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if UFinanceiro.TabLctNaoDefinida(FTabLct, 'TFmLctEdit.FormActivate()') then
    Close;
end;

procedure TFmFinOrfao1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DsELSCI.Dataset := DmodFin.QrELSCI;
end;

procedure TFmFinOrfao1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
