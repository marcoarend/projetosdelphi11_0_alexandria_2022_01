unit Carteiras;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Variants, dmkPermissoes, Grids,
  DBGrids, dmkDBGrid, ComCtrls, MyDBCheck, dmkGeral, dmkCheckBox, UnDmkProcFunc,
  dmkImage, DmkDAC_PF, UnDmkEnums, UnProjGroup_Consts;

type
  TFmCarteiras = class(TForm)
    DsCarteiras: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasPagRec: TIntegerField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TIntegerField;
    QrCarteirasUserAlt: TIntegerField;
    QrCarteirasNome2: TWideStringField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasContato1: TWideStringField;
    QrCarteirasNOMETIPO: TWideStringField;
    QrCarteirasNOMEDOBANCO: TWideStringField;
    QrCarteirasNOMEFORNECEI: TWideStringField;
    PainelEdita: TPanel;
    PMCarteira: TPopupMenu;
    Caixa1: TMenuItem;
    Banco1: TMenuItem;
    Emisso1: TMenuItem;
    QrBancos: TmySQLQuery;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    DsBancos: TDataSource;
    QrForneceI: TmySQLQuery;
    QrForneceICodigo: TIntegerField;
    QrForneceINOMEENTIDADE: TWideStringField;
    DsForneceI: TDataSource;
    QrCarteirasAntigo: TWideStringField;
    QrCarteirasContab: TWideStringField;
    QrCarteirasOrdem: TIntegerField;
    QrCarteirasForneceN: TSmallintField;
    QrCarteirasFuturoC: TFloatField;
    QrCarteirasFuturoD: TFloatField;
    QrCarteirasFuturoS: TFloatField;
    QrCarteirasExclusivo: TSmallintField;
    QrCarteirasAlterWeb: TSmallintField;
    QrCarteirasAtivo: TSmallintField;
    QrCarteirasRecebeBloq: TSmallintField;
    QrEntiDent: TMySQLQuery;
    DsEntiDent: TDataSource;
    QrCarteirasEntiDent: TIntegerField;
    QrEntiDentCodigo: TIntegerField;
    QrEntiDentNOMEENT: TWideStringField;
    QrCarteirasNOMEENTIDENT: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    QrCliInt: TmySQLQuery;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMECLI: TWideStringField;
    QrCliIntCliInt: TIntegerField;
    DsCliInt: TDataSource;
    QrCliCart: TmySQLQuery;
    DsCliCart: TDataSource;
    QrCliCartCodigo: TIntegerField;
    QrCliCartTipo: TIntegerField;
    QrCliCartNome: TWideStringField;
    QrCliCartNOME_TIPO: TWideStringField;
    QrCliCartNOMEBCO: TWideStringField;
    QrCliCartBanco1: TIntegerField;
    QrCliCartAgencia1: TIntegerField;
    QrCliCartConta1: TWideStringField;
    PMUsuario: TPopupMenu;
    Adiciona1: TMenuItem;
    Retira1: TMenuItem;
    QrCarteirasU: TmySQLQuery;
    QrCarteirasUControle: TIntegerField;
    QrCarteirasUUsuario: TIntegerField;
    QrCarteirasULogin: TWideStringField;
    QrCarteirasUFuncionario: TIntegerField;
    QrCarteirasUNOMEUSU: TWideStringField;
    DsCarteirasU: TDataSource;
    PainelDados: TPanel;
    QrCarteirasCodCedente: TWideStringField;
    QrCliCartSaldo: TFloatField;
    QrCliCartInicial: TFloatField;
    QrLan: TmySQLQuery;
    QrLanCliInt: TIntegerField;
    QrCarteirasIgnorSerie: TSmallintField;
    QrJaIns: TmySQLQuery;
    QrJaInsItens: TFloatField;
    QrCarteirasValMorto: TFloatField;
    QrCarteirasValIniOld: TFloatField;
    QrCarteirasSdoFimB: TFloatField;
    QrCarteirasCliInt: TIntegerField;
    PMTaloes: TPopupMenu;
    Incluinovotalo1: TMenuItem;
    Alterataloatual1: TMenuItem;
    Excluitalo1: TMenuItem;
    QrCartTalCH: TmySQLQuery;
    DsCartTalCH: TDataSource;
    QrCartTalCHCodigo: TIntegerField;
    QrCartTalCHControle: TIntegerField;
    QrCartTalCHSerie: TWideStringField;
    QrCartTalCHNumIni: TIntegerField;
    QrCartTalCHNumFim: TIntegerField;
    QrCartTalCHNumAtu: TIntegerField;
    QrUsaTalao: TmySQLQuery;
    QrUsaTalaoTaloes: TLargeintField;
    QrCarteirasNotConcBco: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel12: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel11: TPanel;
    BtDesiste: TBitBtn;
    GBTipoShowEdit: TGroupBox;
    PnBancEdit: TPanel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label36: TLabel;
    EdInicial_B: TdmkEdit;
    EdBanco1: TdmkEdit;
    EdAgencia1: TdmkEdit;
    EdConta1: TdmkEdit;
    EdCodCedente: TdmkEdit;
    PnEmisEdit: TPanel;
    Label21: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label37: TLabel;
    GBFatura2: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    Label20: TLabel;
    EdIDFat: TdmkEdit;
    EdDdFechamento: TdmkEdit;
    EdDiaMesVence: TdmkEdit;
    CkFatura: TCheckBox;
    CkExigeNumCheque: TCheckBox;
    RGTipoDoc: TRadioGroup;
    EdBanco1_1: TdmkEdit;
    EdAgencia1_1: TdmkEdit;
    EdConta1_1: TdmkEdit;
    CheckBox1: TCheckBox;
    CkIgnorSerie: TdmkCheckBox;
    CkNotConcBco: TdmkCheckBox;
    PnCaixEdit: TPanel;
    Label23: TLabel;
    CkPrazo: TCheckBox;
    EdInicial_C: TdmkEdit;
    CkRecebeBloq: TCheckBox;
    GBDefault: TGroupBox;
    PnBasicas: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label14: TLabel;
    Label33: TLabel;
    EdCodigo: TdmkEdit;
    RGPagRec: TRadioGroup;
    EdNome: TdmkEdit;
    EdNome2: TdmkEdit;
    CkAtivo: TCheckBox;
    CkForneceN: TCheckBox;
    CkExclusivo: TCheckBox;
    EdEntiDent: TdmkEditCB;
    CBEntiDent: TdmkDBLookupComboBox;
    RGTipo: TRadioGroup;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtTaloes: TBitBtn;
    BtUsuario: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel10: TPanel;
    GroupBox2: TGroupBox;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label19: TLabel;
    Label3: TLabel;
    Label12: TLabel;
    Label38: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    EdDBNome: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit1: TDBEdit;
    DBRGTipo: TDBRadioGroup;
    DBEdit9: TDBEdit;
    DBRGPagRec: TDBRadioGroup;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    DBEdit15: TDBEdit;
    GBTipoShowData: TGroupBox;
    PnBancShow: TPanel;
    Label4: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label39: TLabel;
    DBEdit2: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit16: TDBEdit;
    PnCaixShow: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    PnEmisShow: TPanel;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    GroupBox1: TGroupBox;
    LaIDFat: TLabel;
    Label6: TLabel;
    Label11: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBRGTipoDoc: TDBRadioGroup;
    DBCheckBox7: TDBCheckBox;
    DBCheckBox8: TDBCheckBox;
    TabSheet4: TTabSheet;
    Splitter2: TSplitter;
    PnFast: TPanel;
    Panel7: TPanel;
    EdCliInt: TEdit;
    dmkDBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    Panel6: TPanel;
    CkSohAtivos: TCheckBox;
    EdCliCart: TEdit;
    TabSheet3: TTabSheet;
    DBGCartTalCH: TDBGrid;
    TabSheet2: TTabSheet;
    DBGFunci: TdmkDBGrid;
    dmkDBGrid2: TdmkDBGrid;
    CkMudaBco: TdmkCheckBox;
    QrCarteirasMudaBco: TIntegerField;
    DBCheckBox9: TDBCheckBox;
    Label40: TLabel;
    EdBanco: TdmkEditCB;
    CBBanco: TdmkDBLookupComboBox;
    LaForneceI: TLabel;
    EdForneceI: TdmkEditCB;
    CBForneceI: TdmkDBLookupComboBox;
    QrCarteirasTipCart: TIntegerField;
    EdFornecePadrao: TdmkEditCB;
    Label41: TLabel;
    CBFornecePadrao: TdmkDBLookupComboBox;
    EdClientePadrao: TdmkEditCB;
    CBClientePadrao: TdmkDBLookupComboBox;
    Label42: TLabel;
    QrFornecePadrao: TMySQLQuery;
    QrFornecePadraoCodigo: TIntegerField;
    QrFornecePadraoNOMEENT: TWideStringField;
    DsFornecePadrao: TDataSource;
    QrClientePadrao: TMySQLQuery;
    QrClientePadraoCodigo: TIntegerField;
    QrClientePadraoNOMEENT: TWideStringField;
    DsClientePadrao: TDataSource;
    QrCarteirasNO_FornecePadrao: TWideStringField;
    QrCarteirasNO_ClientePadrao: TWideStringField;
    Label43: TLabel;
    Label44: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    QrCarteirasFornecePAdrao: TIntegerField;
    QrCarteirasClientePadrao: TIntegerField;
    QrCarteirasFechaDia: TSmallintField;
    QrCarteirasGenCtb: TIntegerField;
    QrCarteirasNO_GenCtb: TWideStringField;
    QrContas: TMySQLQuery;
    IntegerField1: TIntegerField;
    DsContas: TDataSource;
    QrContasNome: TWideStringField;
    Panel2: TPanel;
    Label32: TLabel;
    LaID2: TLabel;
    Label28: TLabel;
    Label46: TLabel;
    Label45: TLabel;
    EdOrdem: TdmkEdit;
    EdID: TdmkEdit;
    EdContato1: TdmkEdit;
    EdContab: TdmkEdit;
    CkFechaDia: TdmkCheckBox;
    EdGenCtb: TdmkEditCB;
    CBGenCtb: TdmkDBLookupComboBox;
    RGTipoCart: TRadioGroup;
    Panel8: TPanel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    DBEdit13: TDBEdit;
    EdDBID: TDBEdit;
    EdDBContato: TDBEdit;
    EdDBContab: TDBEdit;
    DBCheckBox11: TDBCheckBox;
    DBRadioGroup1: TDBRadioGroup;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCarteirasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrCarteirasAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCarteirasBeforeOpen(DataSet: TDataSet);
    procedure QrCarteirasCalcFields(DataSet: TDataSet);
    procedure EdCliIntChange(Sender: TObject);
    procedure EdCliCartChange(Sender: TObject);
    procedure QrCliIntAfterScroll(DataSet: TDataSet);
    procedure QrCliCartAfterScroll(DataSet: TDataSet);
    procedure CkSohAtivosClick(Sender: TObject);
    procedure BtUsuarioClick(Sender: TObject);
    procedure Adiciona1Click(Sender: TObject);
    procedure Retira1Click(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure Caixa1Click(Sender: TObject);
    procedure Banco1Click(Sender: TObject);
    procedure Emisso1Click(Sender: TObject);
    procedure DBRGTipoChange(Sender: TObject);
    procedure DBRGTipoClick(Sender: TObject);
    procedure EdNomeExit(Sender: TObject);
    procedure Incluinovotalo1Click(Sender: TObject);
    procedure Alterataloatual1Click(Sender: TObject);
    procedure Excluitalo1Click(Sender: TObject);
    procedure PMTaloesPopup(Sender: TObject);
    procedure BtTaloesClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure dmkDBGrid2DblClick(Sender: TObject);
    procedure EdForneceIChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure QrCliIntBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo, Tipo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenCliInt();
    procedure ReopenCliCart(Carteira: Integer);
    procedure DefinePanelEdit();
    procedure DefinePanelShow();
    function  RegistrosDaCarteira(CliInt, Carteira: Integer): Integer;
    procedure DefineUsaTalao(Carteira: Integer);
    procedure ReopenBancos(ForneceI: Integer);
  public
    { Public declarations }
    FSeq: Integer;
    procedure ReopenCarteirasU(Controle: Integer);
    procedure ReopenCartTalCH(Controle: Integer);
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCarteiras: TFmCarteiras;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleFin, UnFinanceiro, CarteirasU, ModuleGeral,
  CartTalCH, MyListas;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCarteiras.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCarteiras.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCarteirasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCarteiras.DefParams;
begin
  VAR_GOTOTABELA := 'Carteiras';
  VAR_GOTOMYSQLTABLE := QrCarteiras;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT DISTINCT ca.*, ba.Nome NOMEDOBANCO, en.CliInt, ');
  VAR_SQLx.Add('IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEFORNECEI, ');
  VAR_SQLx.Add('IF(eg.Tipo=0, eg.RazaoSocial, eg.Nome) NOMEENTIDENT, ');
  VAR_SQLx.Add('IF(fp.Tipo=0, fp.RazaoSocial, fp.Nome) NO_FornecePadrao, ');
  VAR_SQLx.Add('IF(cp.Tipo=0, cp.RazaoSocial, cp.Nome) NO_CLientePadrao, ');
  VAR_SQLx.Add('cta.Nome NO_GenCtb ');
  VAR_SQLx.Add('FROM carteiras ca');
  VAR_SQLx.Add('LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco');
  VAR_SQLx.Add('LEFT JOIN entidades en ON en.Codigo=ca.ForneceI');
  VAR_SQLx.Add('LEFT JOIN entidades eg ON eg.Codigo=ca.EntiDent');
  VAR_SQLx.Add('LEFT JOIN entidades fp ON fp.Codigo=ca.FornecePadrao');
  VAR_SQLx.Add('LEFT JOIN entidades cp ON cp.Codigo=ca.ClientePadrao');
  VAR_SQLx.Add('LEFT JOIN contas    cta ON cta.Codigo=ca.GenCtb');
  VAR_SQLx.Add('WHERE ca.Codigo>-1000000');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND ca.Codigo=:P0');
  //
  VAR_SQLa.Add('AND ca.Nome LIKE :P0');
end;

procedure TFmCarteiras.dmkDBGrid2DblClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmCarteiras.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo,
  Tipo: Integer);
  procedure ReopenForneceI_Normal();
  begin
    QrForneceI.Close;
    QrForneceI.SQL.Clear;
    QrForneceI.SQL.Add('SELECT Codigo,');
    QrForneceI.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrForneceI.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrForneceI.SQL.Add('FROM entidades');
    QrForneceI.SQL.Add('WHERE CliInt <> 0'); // adicionado 2010-09-11
    QrForneceI.SQL.Add('ORDER BY NomeENTIDADE');
    UMyMod.AbreQuery(QrForneceI, Dmod.MyDB, ' > TFmCarteiras.MostraEdicao()');
  end;
var
  Habilita: Boolean;
  //EntiTxt,
  CartTxt: String;
begin
  VAR_CARTEIRA   := QrCarteirasTipo.Value;
  CartTxt := FormatFloat('0', QrCarteirasCodigo.Value);
  // Evitar erro!!!
  EdNome.Enabled := True;
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PnCaixEdit.Visible  := False;
      PnBancEdit.Visible  := False;
      PnEmisEdit.Visible  := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      //
      if SQLType = stIns then
      begin
        ReopenForneceI_Normal();
        //
        EdCodigo.ValueVariant       := Codigo;
        EdNome.ValueVariant         := CO_VAZIO;
        EdNome2.ValueVariant        := CO_VAZIO;
        EdID.ValueVariant           := CO_VAZIO;
        EdIDFat.ValueVariant        := CO_VAZIO;
        EdInicial_C.ValueVariant    := CO_VAZIO;
        EdInicial_B.ValueVariant    := CO_VAZIO;
        EdDdFechamento.ValueVariant := CO_VAZIO;
        CkFatura.Checked            := False;
        EdBanco.ValueVariant        := 0;
        CBBanco.KeyValue            := Null;
        //EdForneceI.ValueVariant   := '';
        //CBForneceI.KeyValue       := Null;
        //
        DModG.SelecionaEmpresaSeUnica(EdForneceI, CBForneceI, DModG.QrEmpresas, 'Codigo');
        //
        RGTipoDoc.ItemIndex     := 0;
        EdBanco1.ValueVariant   := '';
        EdAgencia1.ValueVariant := '';
        EdConta1.ValueVariant   := '';
        EdContato1.ValueVariant := '';
        EdContab.ValueVariant   := '';
        CkAtivo.Checked         := True;
        EdOrdem.ValueVariant    := '';
        CkForneceN.Checked      := False;
        CkExclusivo.Checked     := False;
        CkRecebeBloq.Checked    := False;
        CkIgnorSerie.Checked    := True;
        CkNotConcBco.Checked    := False;
        CkFechaDia.Checked      := True;
        //EdEntiDent.ValueVariant := 0;
        //CBEntiDent.KeyValue     := Null;
        //
        DModG.SelecionaEmpresaSeUnica(EdEntiDent, CBEntiDent, DModG.QrEmpresas, 'Codigo');
        //
        EdCodCedente.ValueVariant := '';
        RGTipo.ItemIndex          := Tipo;
        RGTipoCart.ItemIndex      := -1;
        EdGenCtb.ValueVariant     := 0;
        CBGenCtb.KeyValue         := Null;
        //
        RGTipo.Enabled     := True;
        EdForneceI.Enabled := True;
        CBForneceI.Enabled := True;
        //
        CkMudaBco.Checked := False;
      end else
      begin
        ReopenForneceI_Normal();
        //
        RGTipo.ItemIndex      := DBRGTipo.ItemIndex;
        RGPagRec.ItemIndex    := DBRGPagRec.ItemIndex;
        EdCodigo.ValueVariant := DBEdCodigo.Text;
        EdNome.ValueVariant   := DBEdNome.Text;
        EdNome2.ValueVariant  := QrCarteirasNome2.Value;
        if QrCarteirasFatura.Value = 'V' then
          CkFatura.Checked := True
        else
          CkFatura.Checked   := False;
        //
        CkPrazo.Checked             := Geral.IntToBool_0(QrCarteirasPrazo.Value);
        EdIDFat.ValueVariant        := QrCarteirasID_Fat.Value;
        EdDdFechamento.ValueVariant := QrCarteirasFechamento.Value;
        EdDiaMesVence.ValueVariant  := QrCarteirasDiaMesVence.Value;
        CkExigeNumCheque.Checked    := Geral.IntToBool_0(QrCarteirasExigeNumCheque.Value);
        EdForneceI.ValueVariant     := QrCarteirasForneceI.Value;
        CBForneceI.KeyValue         := QrCarteirasForneceI.Value;
        RGTipoDoc.ItemIndex         := QrCarteirasTipoDoc.Value;
        //
        EdInicial_B.ValueVariant := Geral.FFT(QrCarteirasInicial.Value, 2, siPositivo);
        EdInicial_C.ValueVariant := Geral.FFT(QrCarteirasInicial.Value, 2, siPositivo);
        EdID.ValueVariant        := QrCarteirasID.Value;
        EdBanco.ValueVariant     := QrCarteirasBanco.Value;
        CBBanco.KeyValue         := QrCarteirasBanco.Value;
        //
        EdBanco1.ValueVariant   := dmkPF.FFD(QrCarteirasBanco1.Value, 3, siPositivo);
        EdAgencia1.ValueVariant := dmkPF.FFD(QrCarteirasAgencia1.Value, 4, siPositivo);
        EdConta1.ValueVariant   := QrCarteirasConta1.Value;
        //
        EdBanco1_1.ValueVariant   := dmkPF.FFD(QrCarteirasBanco1.Value, 3, siPositivo);
        EdAgencia1_1.ValueVariant := dmkPF.FFD(QrCarteirasAgencia1.Value, 4, siPositivo);
        EdConta1_1.ValueVariant   := QrCarteirasConta1.Value;
        //
        EdContato1.ValueVariant := QrCarteirasContato1.Value;
        EdContab.ValueVariant   := QrCarteirasContab.Value;
        CkAtivo.Checked         := Geral.IntToBool_0(QrCarteirasAtivo.Value);
        //
        EdOrdem.ValueVariant := QrCarteirasOrdem.Value;
        //
        CkForneceN.Checked   := Geral.IntToBool_0(QrCarteirasForneceN.Value);
        CkExclusivo.Checked  := Geral.IntToBool_0(QrCarteirasExclusivo.Value);
        CkRecebeBloq.Checked := Geral.IntToBool_0(QrCarteirasRecebeBloq.Value);
        CkIgnorSerie.Checked := Geral.IntToBool_0(QrCarteirasIgnorSerie.Value);
        CkNotConcBco.Checked := Geral.IntToBool_0(QrCarteirasNotConcBco.Value);
        CkFechaDia.Checked   := Geral.IntToBool_0(QrCarteirasFechaDia.Value);
        //
        EdEntiDent.ValueVariant        := QrCarteirasEntiDent.Value;
        CBEntiDent.KeyValue            := QrCarteirasEntiDent.Value;
        EdFornecePadrao.ValueVariant   := QrCarteirasFornecePadrao.Value;
        CBFornecePadrao.KeyValue       := QrCarteirasFornecePadrao.Value;
        EdClientePadrao.ValueVariant   := QrCarteirasClientePadrao.Value;
        CBClientePadrao.KeyValue       := QrCarteirasClientePadrao.Value;
        EdCodCedente.ValueVariant      := QrCarteirasCodCedente.Value;
        //
        Habilita           := RegistrosDaCarteira(QrCarteirasCliInt.Value, QrCarteirasCodigo.Value) = 0;
        RGTipo.Enabled     := Habilita;
        if QrCarteirasCodigo.Value < 0 then
        begin
          RGTipo.Enabled     := False;
          EdNome.Enabled     := False;
        end else
          EdNome.Enabled     := True;

        EdForneceI.Enabled := Habilita and EdForneceI.ValueVariant <> 0;
        CBForneceI.Enabled := EdForneceI.Enabled;
        //
        CkMudaBco.Checked    := Geral.IntToBool_0(QrCarteirasMudaBco.Value);
        RGTipoCart.ItemIndex := QrCarteirasTipCart.Value;
        //
        EdGenCtb.ValueVariant         := QrCarteirasGenCtb.Value;
        CBGenCtb.KeyValue             := QrCarteirasGenCtb.Value;
        //
      end;
      try
        if EdNome.Enabled then
          EdNome.SetFocus;
      except
        //
      end;
      DefinePanelEdit();
    end;
  end;
  ImgTipo.SQLType := SQLType;
  //
  GOTOy.BotoesSb(ImgTipo.SQLType);
  //
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmCarteiras.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 1 then //Pesquisa
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      ReopenCliInt;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmCarteiras.PMTaloesPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrCarteiras.State = dsBrowse) and (QrCarteiras.RecordCount > 0);
  Incluinovotalo1.Enabled := Habilita;
  Habilita := (QrCartTalCH.State = dsBrowse) and (QrCartTalCH.RecordCount > 0);
  Alterataloatual1.Enabled := Habilita;
  Excluitalo1.Enabled      := Habilita;
end;

procedure TFmCarteiras.Retira1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if QrCarteirasU.RecordCount > 0 then
  begin
    Controle := QrCarteirasUControle.Value;
    //
    if Geral.MB_Pergunta('Confirma a exclus�o do usu�rio '+
    QrCarteirasUNOMEUSU.Value +'?') = ID_YES
    then begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM carteirasu WHERE Controle=:P0');
      Dmod.QrUpd.Params[00].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenCarteirasU(0);
    end;
  end else
  begin
    Geral.MB_Aviso('N�o foi localizado nenhum usu�rio.');
  end;
end;

procedure TFmCarteiras.RGTipoClick(Sender: TObject);
begin
  DefinePanelEdit();
end;

procedure TFmCarteiras.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCarteiras.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCarteiras.ReopenCliCart(Carteira: Integer);
begin
  QrCliCart.Close;
  QrCliCart.SQL.Clear;
  QrCliCart.SQL.Add('SELECT car.Codigo, car.Tipo, car.Nome,');
  QrCliCart.SQL.Add('ELT(car.Tipo+1,"Caixa","C/C Banco",');
  QrCliCart.SQL.Add('"Emiss�o") NOME_TIPO,');
  QrCliCart.SQL.Add('IF(car.Banco=0,"",bco.Nome) NOMEBCO,');
  QrCliCart.SQL.Add('car.Banco1, car.Agencia1, car.Conta1, ');
  QrCliCart.SQL.Add('car.Inicial, car.Saldo, car.ForneceI');
  QrCliCart.SQL.Add('FROM carteiras car');
  QrCliCart.SQL.Add('LEFT JOIN carteiras bco ON bco.Codigo=car.Banco');
  QrCliCart.SQL.Add('WHERE car.ForneceI=' +
    dmkPF.FFP(QrCliIntCodigo.Value, 0));
  if Trim(EdCliCart.Text) <> '' then
    QrCliCart.SQL.Add('AND car.Nome LIKE "%' + EdCliCart.Text + '%"');
  if CkSohAtivos.Checked then
    QrCliCart.SQL.Add('AND car.Ativo=1');
  UMyMod.AbreQuery(QrCliCart, Dmod.MyDB, 'TFmCarteiras.ReopenCliCart()');
  //
  QrCliCart.Locate('Codigo', Carteira, []);
end;

procedure TFmCarteiras.ReopenCliInt();
var
  SQL: String;
begin
  (*
  QrCliInt.Close;
  QrCliInt.SQL.Clear;
  QrCliInt.SQL.Add('SELECT DISTINCT ent.Codigo, ent.CliInt,');
  QrCliInt.SQL.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMECLI');
  QrCliInt.SQL.Add('FROM entidades ent');
  QrCliInt.SQL.Add('LEFT JOIN carteiras car ON car.ForneceI=ent.Codigo');
  QrCliInt.SQL.Add('WHERE (ent.CliInt<>0');
  QrCliInt.SQL.Add('OR car.ForneceI <> 0)');
  QrCliInt.SQL.Add('');
  QrCliInt.SQL.Add('');
  if Trim(EdCliInt.Text) <> '' then
    QrCliInt.SQL.Add('AND (IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome)) LIKE "%' +
    EdCliInt.Text + '%"');
   UMyMod.AbreQuery(QrCliInt, Dmod.MyDB, 'TFmCarteiras.ReopenCliInt()');

  Atualizado em 06/07/2013 => Motivo: Melhorias no desempenho
  *)
  if Trim(EdCliInt.Text) <> '' then
    SQL := 'AND (IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome)) LIKE "%' + EdCliInt.Text + '%"'
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCliInt, Dmod.MyDB, [
  'SELECT DISTINCT ent.Codigo, ent.CliInt, ',
  'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMECLI ',
  'FROM carteiras car ',
  'LEFT JOIN entidades ent ON car.ForneceI=ent.Codigo ',
  'WHERE car.ForneceI <> 0 ',
  'AND ent.Codigo IS NOT NULL ',
  SQL,
  '']);
end;

procedure TFmCarteiras.DBRGTipoChange(Sender: TObject);
begin
  DefinePanelShow();
end;

procedure TFmCarteiras.DBRGTipoClick(Sender: TObject);
begin
  DefinePanelShow();
end;

procedure TFmCarteiras.DefineONomeDoForm;
begin
end;

procedure TFmCarteiras.DefinePanelEdit();
begin
  CkExigeNumCheque.Checked := False;
  //
  case RGTipo.ItemIndex of
    -1:
    begin
      PnCaixEdit.Visible := False;
      PnBancEdit.Visible := False;
      PnEmisEdit.Visible := False;
    end;
    0:
    begin
      PnCaixEdit.Visible := True;
      PnBancEdit.Visible := False;
      PnEmisEdit.Visible := False;
    end;
    1:
    begin
      PnBancEdit.Visible := True;
      PnCaixEdit.Visible := False;
      PnEmisEdit.Visible := False;
    end;
    2:
    begin
      PnEmisEdit.Visible := True;
      PnCaixEdit.Visible := False;
      PnBancEdit.Visible := False;
    end;
  end;
end;

procedure TFmCarteiras.DefinePanelShow;
begin
  case DBRGTipo.ItemIndex of
    -1:
    begin
      PnCaixShow.Visible := False;
      PnBancShow.Visible := False;
      PnEmisShow.Visible := False;
    end;
    0:
    begin
      PnCaixShow.Visible := True;
      PnBancShow.Visible := False;
      PnEmisShow.Visible := False;
    end;
    1:
    begin
      PnBancShow.Visible := True;
      PnCaixShow.Visible := False;
      PnEmisShow.Visible := False;
    end;
    2:
    begin
      PnEmisShow.Visible := True;
      PnCaixShow.Visible := False;
      PnBancShow.Visible := False;
    end;
  end;
end;

procedure TFmCarteiras.DefineUsaTalao(Carteira: Integer);
var
  UsaTalao: Integer;
begin
  QrUsaTalao.Close;
  QrUsaTalao.Params[00].AsInteger := Carteira;
  UnDmkDAC_PF.AbreQuery(QrUsaTalao, Dmod.MyDB);
  //
  if QrUsaTalaoTaloes.Value > 0 then
    UsaTalao := 1
  else
    UsaTalao := 0;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'carteiras', False, [
  'UsaTalao'], ['Codigo'], [UsaTalao], [Carteira], False);
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCarteiras.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCarteiras.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCarteiras.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCarteiras.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCarteiras.BtIncluiClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMCarteira, BtInclui);
end;

procedure TFmCarteiras.Alterataloatual1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  if UmyMod.FormInsUpd_Cria(TFmCartTalCH, FmCartTalCH, afmoNegarComAviso,
    QrCartTalCH, stUpd) then
  begin
    {
    if not DBCheck.LiberaPelaSenhaBoss then
    begin
      FmCartTalCH.LaNumAtu.Enabled := False;
      FmCartTalCH.EdNumAtu.Enabled := False;
    end;
    }
    FmCartTalCH.ShowModal;
    FmCartTalCH.Destroy;
    //
    DefineUsaTalao(QrCarteirasCodigo.Value);
  end;
end;

procedure TFmCarteiras.Banco1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0, 1);
end;

procedure TFmCarteiras.BtAlteraClick(Sender: TObject);
var
  Carteiras : Integer;
begin
  if (QrCarteiras.State = dsInactive) or (QrCarteiras.RecordCount = 0) or
    ((QrCarteirasCodigo.Value = 0) or (QrCarteirasCodigo.Value < -1)) then
    Exit;
  //
  PageControl1.ActivePageIndex := 0;
  Carteiras := QrCarteirasCodigo.Value;
  if not UMyMod.SelLockY(Carteiras, Dmod.MyDB, 'Carteiras', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Carteiras, Dmod.MyDB, 'Carteiras', 'Codigo');
      MostraEdicao(1, stUpd, 0, -1);
      DefinePanelEdit();
      if QrCarteirasCodigo.Value = 0 then EdNome.Enabled := False
      else EdNome.Enabled := True;
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCarteiras.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCarteirasCodigo.Value;
  Close;
end;

procedure TFmCarteiras.BtTaloesClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMTaloes, BtTaloes);
end;

procedure TFmCarteiras.BtUsuarioClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 3;
  MyObjects.MostraPopUpDeBotao(PMUsuario, BtUsuario);
end;

procedure TFmCarteiras.BtConfirmaClick(Sender: TObject);
var
  DiaMesVence, ExigeNumCheque, Codigo, Banco, Fechamento, Prazo, ForneceI,
  Banco1, Agencia1, CliInt, MudaBco, TipoDoc, TipCart, FornecePadrao,
  ClientePadrao, FechaDia, GenCtb: Integer;
  TabLctA, Nome, Nome2, Fatura, Conta1: String;
  Inicial: Double;
begin
  DiaMesVence    := Geral.IMV(EdDiaMesVence.Text);
  ExigeNumCheque := Geral.BoolToInt(CkExigeNumCheque.Checked);
  Prazo          := Geral.BoolToInt(CkPrazo.Checked);
  Nome           := EdNome.Text;
  Nome2          := EdNome2.Text;
  MudaBco        := Geral.BoolToInt(CkMudaBco.Checked);
  TipoDoc        := RGTipoDoc.ItemIndex;
  TipCart        := RGTipoCart.ItemIndex;
  GenCtb         := EdGenCtb.ValueVariant;
  //
  if (TipoDoc = 5) and (MudaBco = 1) then
  begin
    Geral.MB_Aviso('Para carteiras do tipo boletos n�o � permitido mudar a carteira banc�ria no momento da quita��o');
    Exit;
  end;
  //
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  //
  if MyObjects.FIC(TipCart < 0, RGTipoCart, 'O campo Finalidade deve ser informado!') then Exit;
  //
  Fechamento := Geral.IMV(EdDdFechamento.Text);
  if (EdBanco.ValueVariant <> 0) then
  begin
    if RGTipo.ItemIndex = 2 then
      Banco := EdBanco.ValueVariant
    else
      Banco := 0;
  end else
  begin
    if RGTipo.ItemIndex = 2 then
    begin
      Geral.MB_Aviso('Defina um banco!');
      CBBanco.SetFocus;
      Exit;
    end else
      Banco := 0;
  end;
  ForneceI := Geral.IMV(EdForneceI.Text);
  //
  if ForneceI = 0 then ForneceI := Dmod.QrMasterDono.Value;
  if CkFatura.Checked = True then Fatura := 'V' else Fatura := 'F';
  //
  Banco1   := 0;
  Agencia1 := 0;
  Conta1   := '';
  Inicial  := Geral.DMV(EdInicial_B.Text);

  if PnCaixEdit.Visible then
  begin
    Inicial := Geral.DMV(EdInicial_C.Text)
  end else if PnBancEdit.Visible then
  begin
    Banco1   := Geral.IMV(EdBanco1.Text);
    Agencia1 := Geral.IMV(EdAgencia1.Text);
    Conta1   := EdConta1.Text;
  end else if PnEmisEdit.Visible then
  begin
    Banco1   := Geral.IMV(EdBanco1_1.Text);
    Agencia1 := Geral.IMV(EdAgencia1_1.Text);
    Conta1   := EdConta1_1.Text;
  end;
  //
  FornecePadrao := EdFornecePadrao.ValueVariant;
  ClientePadrao := EdClientePadrao.ValueVariant;
  FechaDia      := Geral.BoolToInt(CkFechaDia.Checked);
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('Carteiras', 'Codigo', ImgTipo.SQLType,
    QrCarteirasCodigo.Value);
  //
  if (Codigo = 0) or
  (
    (ImgTipo.SQLType = stUpd) and
    (EdCodigo.ValueVariant <> QrCarteirasCodigo.Value)
  ) then
  begin
    Geral.MB_Erro('C�digo inv�lido: ' + Geral.FF0(Codigo));
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'Carteiras', False,
  [
    'Nome', 'ID', 'Inicial', 'Banco', 'Fatura', 'ID_Fat',
    'Fechamento', 'Prazo',
    'PagRec', 'DiaMesVence', 'ExigeNumCheque',
    'ForneceI', 'Nome2', 'TipoDoc', 'Banco1', 'Agencia1', 'Conta1',
    'Ativo', 'Contato1', 'Contab', 'Ordem', 'ForneceN',
    'Exclusivo', 'RecebeBloq', 'EntiDent',
    'CodCedente', 'Tipo', 'IgnorSerie', 'NotConcBco', 'MudaBco', 'TipCart',
    'FornecePadrao', 'ClientePadrao', 'FechaDia',
    'GenCtb'
  ], ['Codigo'],
  [
    Nome, EdID.Text, Inicial, Banco, Fatura, EdIDFat.Text,
    Fechamento, Prazo,
    RGPagRec.ItemIndex - 1, DiaMesVence, ExigeNumCheque,
    ForneceI, Nome2, TipoDoc, Banco1, Agencia1, Conta1,
    Geral.BoolToInt(CkAtivo.Checked), EdContato1.Text, EdContab.Text,
    Geral.IMV(EdOrdem.Text), Geral.BoolToInt(CkForneceN.Checked),
    Geral.BoolToInt(CkExclusivo.Checked),
    Geral.BoolToInt(CkRecebeBloq.Checked), EdEntiDent.ValueVariant,
    Geral.SoNumero_TT(EdCodCedente.Text), RGTipo.ItemIndex,
    Geral.BoolToInt(CkIgnorSerie.Checked),
    Geral.BoolToInt(CkNotConcBco.Checked), MudaBco, TipCart,
    FornecePadrao, ClientePadrao, FechaDia,
    GenCtb
  ], [Codigo], True) then
  begin
    // Atualizar lan�amentos j� emitidos para a carteira
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT CliInt');
    Dmod.QrAux.SQL.Add('FROM entidades');
    Dmod.QrAux.SQL.Add('WHERE Codigo=' + FormatFloat('0', ForneceI));
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    CliInt := Dmod.QrAux.FieldByName('CliInt').AsInteger;
    if CliInt <> 0 then
    begin
      TabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False,
        ['Tipo'], ['Carteira'], [RGTipo.ItemIndex], [Codigo], True, '', TabLctA);
    end;
    //
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Carteiras', 'Codigo');
    MostraEdicao(0, stLok, 0, -1);
{###
    UFinanceiro.RecalcSaldoCarteira(Codigo, QrCarteiras, nil, True, True);
}
{### Deprecado
    Dmod.QrCarteiras.Close;
    UMyMod.AbreQuery(Dmod.QrCarteiras, 'TFmCarteiras.BtConfirmaClick()');
}
    ReopenBancos(ForneceI);
    //
    ReopenCliCart(Codigo);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCarteiras.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Carteiras', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Carteiras', 'Codigo');
  MostraEdicao(0, stLok, 0, -1);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Carteiras', 'Codigo');
end;

procedure TFmCarteiras.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DmodFin.CarregaItensTipoDoc(DBRGTipoDoc);
  DmodFin.CarregaItensTipoDoc(RGTipoDoc);
  //
  FSeq := 0;
  //
  PageControl1.ActivePageIndex := 0;
  //
  if CO_DMKID_APP <> 7 then
  begin
    TabSheet2.TabVisible := False;
    BtUsuario.Visible    := False;
  end else
  begin
    TabSheet2.TabVisible := True;
    BtUsuario.Visible    := True;
  end;
  GBTipoShowData.Align := alClient;
  GBTipoShowEdit.Align := alClient;
  //PainelDados.Align := alClient;
  //PnFast.Align      := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrEntiDent, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornecePadrao, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientePadrao, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  //
  QrBancos.Close;
  QrJaIns.Close;
  QrJaIns.Database := DModG.MyPID_DB;
  //
  PageControl1.Align := alClient;
end;

procedure TFmCarteiras.SbNumeroClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  LaRegistro.Caption := GOTOy.Codigo(QrCarteirasCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCarteiras.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmCarteiras.SbNomeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCarteiras.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmCarteiras.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrCarteirasCodigo.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCarteiras.QrCarteirasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCarteiras.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Carteiras', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, stIns, 0, -1);
end;

procedure TFmCarteiras.QrCarteirasAfterScroll(DataSet: TDataSet);
begin
  //BtAltera.Enabled := GOTOy.BtEnabled(QrCarteirasCodigo.Value, False);
  ReopenCarteirasU(0);
  ReopenCartTalCH(0);
  BtExclui.Enabled := GOTOy.BtEnabled(QrCarteirasCodigo.Value, False);
end;

procedure TFmCarteiras.SbQueryClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  LocCod(QrCarteirasCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Carteiras', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCarteiras.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCarteiras.Incluinovotalo1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  UmyMod.FormInsUpd_Show(TFmCartTalCH, FmCartTalCH, afmoNegarComAviso,
    QrCartTalCH, stIns);
  DefineUsaTalao(QrCarteirasCodigo.Value);
end;

procedure TFmCarteiras.QrCarteirasBeforeOpen(DataSet: TDataSet);
begin
  QrCarteirasCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCarteiras.QrCarteirasCalcFields(DataSet: TDataSet);
begin
  QrCarteirasNOMETIPO.Value :=
    dmkPF.TipoDeCarteiraCashier(QrCarteirasTipo.Value, False);
end;

procedure TFmCarteiras.QrCliCartAfterScroll(DataSet: TDataSet);
begin
  LocCod(QrCliCartCodigo.Value, QrCliCartCodigo.Value);
end;

procedure TFmCarteiras.QrCliIntAfterScroll(DataSet: TDataSet);
begin
  ReopenCliCart(0);
end;

procedure TFmCarteiras.QrCliIntBeforeClose(DataSet: TDataSet);
begin
  QrCliCart.Close;
end;

procedure TFmCarteiras.EdNomeExit(Sender: TObject);
begin
  if EdNome2.Text = '' then
    EdNome2.Text := EdNome.Text;
end;

procedure TFmCarteiras.EdCliCartChange(Sender: TObject);
begin
  ReopenCliCart(0);
end;

procedure TFmCarteiras.EdCliIntChange(Sender: TObject);
begin
  ReopenCliInt();
end;

procedure TFmCarteiras.EdForneceIChange(Sender: TObject);
begin
  if EdForneceI.ValueVariant <> 0 then
    ReopenBancos(EdForneceI.ValueVariant);
end;

procedure TFmCarteiras.Adiciona1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCarteirasU, FmCarteirasU, afmoNegarComAviso) then
  begin
    FmCarteirasU.ShowModal;
    FmCarteirasU.Destroy;
  end;
end;

procedure TFmCarteiras.CkSohAtivosClick(Sender: TObject);
begin
  ReopenCliCart(0);
end;

procedure TFmCarteiras.Caixa1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0, 0);
end;

procedure TFmCarteiras.Emisso1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0, 2);
end;

procedure TFmCarteiras.Excluitalo1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrCartTalCH, DBGCartTalCH,
    'carttalch', ['Controle'], ['Controle'], istAtual, '');
  DefineUsaTalao(QrCarteirasCodigo.Value);
end;

function TFmCarteiras.RegistrosDaCarteira(CliInt, Carteira: Integer): Integer;
var
  CartTxt, TabLctA, TabLctB, TabLctD: String;
begin
  //06/07/2013 Para evitar erro no Syndi2 porque n�o tem tabela de lan�amentos para a empresa -11
  if CliInt > 0 then
  begin
    CartTxt := FormatFloat('0', Carteira);
    //
    if VAR_MULTIPLAS_TAB_LCT then
    begin
      //
      TabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
      TabLctB := DModG.NomeTab(TMeuDB, ntLct, False, ttB, CliInt);
      TabLctD := DModG.NomeTab(TMeuDB, ntLct, False, ttD, CliInt);
      //
      QrJaIns.Close;
      QrJaIns.SQL.Clear;
      QrJaIns.SQL.Add('DROP TABLE IF EXISTS _qtd_reg_lct;');
      QrJaIns.SQL.Add('');
      QrJaIns.SQL.Add('CREATE TABLE _qtd_reg_lct');
      QrJaIns.SQL.Add('SELECT COUNT(Controle) Itens');
      QrJaIns.SQL.Add('FROM ' + TabLctA);
      QrJaIns.SQL.Add('WHERE Carteira = ' + CartTxt);
      QrJaIns.SQL.Add('UNION');
      QrJaIns.SQL.Add('SELECT COUNT(Controle) Itens');
      QrJaIns.SQL.Add('FROM ' + TabLctB);
      QrJaIns.SQL.Add('WHERE Carteira = ' + CartTxt);
      QrJaIns.SQL.Add('UNION');
      QrJaIns.SQL.Add('SELECT COUNT(Controle) Itens');
      QrJaIns.SQL.Add('FROM ' + TabLctD);
      QrJaIns.SQL.Add('WHERE Carteira = ' + CartTxt + ';');
      QrJaIns.SQL.Add('');
      QrJaIns.SQL.Add('SELECT SUM(Itens) Itens');
      QrJaIns.SQL.Add('FROM _qtd_reg_lct;');
      QrJaIns.SQL.Add('DROP TABLE IF EXISTS _qtd_reg_lct;');
      //
    end else begin
      QrJaIns.Close;
      QrJaIns.SQL.Clear;
      QrJaIns.SQL.Add('DROP TABLE IF EXISTS _qtd_reg_lct;');
      QrJaIns.SQL.Add('');
      QrJaIns.SQL.Add('CREATE TABLE _qtd_reg_lct');
      QrJaIns.SQL.Add('SELECT COUNT(Controle) Itens');
      QrJaIns.SQL.Add('FROM ' + TMeuDB + '.' + LAN_CTOS);
      QrJaIns.SQL.Add('WHERE Carteira = ' + CartTxt);
      QrJaIns.SQL.Add(';');
      QrJaIns.SQL.Add('SELECT SUM(Itens) Itens');
      QrJaIns.SQL.Add('FROM _qtd_reg_lct;');
      QrJaIns.SQL.Add('DROP TABLE IF EXISTS _qtd_reg_lct;');
      //
    end;
    UnDmkDAC_PF.AbreQuery(QrJaIns, DModG.MyPID_DB);
    //
    Result := Trunc(QrJaInsItens.Value);
  end else
    Result := 0;
end;

procedure TFmCarteiras.ReopenBancos(ForneceI: Integer);
begin
  QrBancos.Close;
  QrBancos.Params[0].AsInteger := ForneceI;
  UMyMod.AbreQuery(QrBancos, Dmod.MyDB, 'TFmCarteiras.ReopenBancos()');
end;

procedure TFmCarteiras.ReopenCarteirasU(Controle: Integer);
begin
  QrCarteirasU.Close;
  QrCarteirasU.Params[0].AsInteger := QrCarteirasCodigo.Value;
  UMyMod.AbreQuery(QrCarteirasU, Dmod.MyDB, 'TFmCarteiras.ReopenCarteirasU()');
  //
  QrCarteirasU.Locate('Controle', Controle, []);
end;

procedure TFmCarteiras.ReopenCartTalCH(Controle: Integer);
begin
  QrCartTalCH.Close;
  QrCartTalCH.Params[0].AsInteger := QrCarteirasCodigo.Value;
  UMyMod.AbreQuery(QrCartTalCH, Dmod.MyDB, 'TFmCarteiras.ReopenCartTalCH()');
  //
  QrCartTalCH.Locate('Controle', Controle, []);
end;

end.

