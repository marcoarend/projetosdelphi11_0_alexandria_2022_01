unit LctDelLanctoz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, UnDmkProcFunc, UnDmkEnums;

type
  TFmLctDelLanctoz = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    DBGLct: TdmkDBGrid;
    Panel5: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdControle: TdmkEdit;
    CkDoc: TCheckBox;
    EdDoc: TdmkEdit;
    CkControle: TCheckBox;
    EdIdPgto: TdmkEdit;
    CkIdPgto: TCheckBox;
    BtOK: TBitBtn;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctLk: TIntegerField;
    QrLctFatID: TIntegerField;
    QrLctFatParcela: TIntegerField;
    QrLctCONTA: TIntegerField;
    QrLctNOMECONTA: TWideStringField;
    QrLctNOMEEMPRESA: TWideStringField;
    QrLctNOMESUBGRUPO: TWideStringField;
    QrLctNOMEGRUPO: TWideStringField;
    QrLctNOMECONJUNTO: TWideStringField;
    QrLctNOMESIT: TWideStringField;
    QrLctAno: TFloatField;
    QrLctMENSAL: TWideStringField;
    QrLctMENSAL2: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctLocal: TIntegerField;
    QrLctFatura: TWideStringField;
    QrLctSub: TSmallintField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctPago: TFloatField;
    QrLctSALDO: TFloatField;
    QrLctID_Sub: TSmallintField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctcliente: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctNOMECLIENTE: TWideStringField;
    QrLctNOMEFORNECEDOR: TWideStringField;
    QrLctTIPOEM: TWideStringField;
    QrLctNOMERELACIONADO: TWideStringField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctMulta: TFloatField;
    QrLctATRASO: TFloatField;
    QrLctJUROS: TFloatField;
    QrLctDataDoc: TDateField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctMes2: TLargeintField;
    QrLctProtesto: TDateField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TSmallintField;
    QrLctUserAlt: TSmallintField;
    QrLctControle: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctCtrlIni: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctCOMPENSADO_TXT: TWideStringField;
    QrLctCliInt: TIntegerField;
    QrLctDepto: TIntegerField;
    QrLctDescoPor: TIntegerField;
    QrLctPrazo: TSmallintField;
    QrLctForneceI: TIntegerField;
    QrLctQtde: TFloatField;
    QrLctFatNum: TFloatField;
    QrLctEmitente: TWideStringField;
    QrLctAgencia: TIntegerField;
    QrLctContaCorrente: TWideStringField;
    QrLctCNPJCPF: TWideStringField;
    QrLctDescoVal: TFloatField;
    QrLctDescoControle: TIntegerField;
    QrLctNFVal: TFloatField;
    QrLctAntigo: TWideStringField;
    QrLctUnidade: TIntegerField;
    QrLctExcelGru: TIntegerField;
    QrLctSerieCH: TWideStringField;
    QrLctMoraVal: TFloatField;
    QrLctMultaVal: TFloatField;
    QrLctDoc2: TWideStringField;
    QrLctCNAB_Sit: TSmallintField;
    QrLctTipoCH: TSmallintField;
    QrLctID_Quit: TIntegerField;
    QrLctReparcel: TIntegerField;
    QrLctAtrelado: TIntegerField;
    QrLctPagMul: TFloatField;
    QrLctPagJur: TFloatField;
    QrLctSubPgto1: TIntegerField;
    QrLctMultiPgto: TIntegerField;
    QrLctProtocolo: TIntegerField;
    QrLctCtrlQuitPg: TIntegerField;
    QrLctAlterWeb: TSmallintField;
    QrLctAtivo: TSmallintField;
    QrLctSerieNF: TWideStringField;
    QrLctEndossas: TSmallintField;
    QrLctEndossan: TFloatField;
    QrLctEndossad: TFloatField;
    QrLctCancelado: TSmallintField;
    QrLctEventosCad: TIntegerField;
    QrLctUH: TWideStringField;
    QrLctEncerrado: TIntegerField;
    QrLctErrCtrl: TIntegerField;
    QrLctNO_Carteira: TWideStringField;
    QrLctBanco1: TIntegerField;
    QrLctAgencia1: TIntegerField;
    QrLctConta1: TWideStringField;
    QrLctNOMEFORNECEI: TWideStringField;
    QrLctNO_ENDOSSADO: TWideStringField;
    QrLctSERIE_CHEQUE: TWideStringField;
    QrLctIndiPag: TIntegerField;
    QrLctFisicoSrc: TSmallintField;
    QrLctFisicoCod: TIntegerField;
    DsLct: TDataSource;
    QrLctDataDel: TDateTimeField;
    QrLctUserDel: TIntegerField;
    QrLctUSERDEL_TXT: TWideStringField;
    Bevel1: TBevel;
    Bevel2: TBevel;
    EdCredMax: TdmkEdit;
    EdCredMin: TdmkEdit;
    CkCredito: TCheckBox;
    CkDebito: TCheckBox;
    EdDebMin: TdmkEdit;
    EdDebMax: TdmkEdit;
    QrLctNO_MOTVDEL: TWideStringField;
    CkMotivo: TCheckBox;
    CBMotivo: TComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdDebMinExit(Sender: TObject);
    procedure EdCredMinExit(Sender: TObject);
  private
    { Private declarations }
    FLstMotivDel: TStringList;
    procedure ReabreLcts();
  public
    { Public declarations }
  end;

  var
  FmLctDelLanctoz: TFmLctDelLanctoz;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral, UnFinanceiro;

{$R *.DFM}

procedure TFmLctDelLanctoz.BtOKClick(Sender: TObject);
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Cliente interno n�o definido!') then Exit;
  //
  try
    Screen.Cursor := crHourGlass;
    //
    ReabreLcts();
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctDelLanctoz.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctDelLanctoz.EdCredMinExit(Sender: TObject);
begin
  if Geral.DMV(EdCredMax.Text) < Geral.DMV(EdCredMin.Text) then
    EdCredMax.Text := EdCredMin.Text;
end;

procedure TFmLctDelLanctoz.EdDebMinExit(Sender: TObject);
begin
  if Geral.DMV(EdDebMax.Text) < Geral.DMV(EdDebMin.Text) then
    EdDebMax.Text := EdDebMin.Text;
end;

procedure TFmLctDelLanctoz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctDelLanctoz.FormCreate(Sender: TObject);
var
  MotivoStr: String;
  i, MotivoCod: Integer;
begin
  ImgTipo.SQLType := stLok;
  FLstMotivDel    := TStringList.Create;
  //
  for i := 0 to CO_MOTVDEL_MAX do
  begin
    MotivoCod := ListaMotivosExclusao_Int[i];
    //
    //C�digos de exclus�o do financeiro
    if (MotivoCod = 0) or ((MotivoCod >= 100) and (MotivoCod <= 320)) then
    begin
      MotivoStr := dmkPF.MotivDel_ObtemDescricao(MotivoCod);
      //
      if MotivoStr <> '' then
      begin
        FLstMotivDel.Add(Geral.FF0(MotivoCod));
        CBMotivo.AddItem(MotivoStr, nil);
      end;
    end;
  end;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmLctDelLanctoz.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctDelLanctoz.ReabreLcts;
var
  Controle, ID_Pgto, CliInt, MotvDel: Integer;
  Documento: Double;
  MotivoExclusaoLct: String;
begin
  Controle          := EdControle.ValueVariant;
  ID_Pgto           := EdIdPgto.ValueVariant;
  CliInt            := DModG.QrEmpresasCodigo.Value;
  Documento         := EdDoc.ValueVariant;
  MotivoExclusaoLct := UFinanceiro.ObtemSequenciaDeCodigo_MotivExclusaoLct('la.MotvDel', 'NO_MOTVDEL') + ' ';
  //
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
  QrLct.SQL.Add('  ((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
  if VAR_KIND_DEPTO = kdUH then
    QrLct.SQL.Add('ci.Unidade UH, ')
  else
    QrLct.SQL.Add('"" UH, ');
  QrLct.SQL.Add('la.*, ct.Codigo CONTA, ca.Prazo, ca.Nome NO_Carteira, ');
  QrLct.SQL.Add('ca.Banco1, ca.Agencia1, ca.Conta1,');
  QrLct.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
  QrLct.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
  QrLct.SQL.Add('IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA,');
  QrLct.SQL.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,');
  QrLct.SQL.Add('IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,');
  QrLct.SQL.Add('IF(la.ForneceI=0, "",');
  QrLct.SQL.Add('  IF(fi.Tipo=0, fi.RazaoSocial, fi.Nome)) NOMEFORNECEI,');
  QrLct.SQL.Add('IF(la.Sit<2, la.Credito-la.Debito-(la.Pago*la.Sit), 0) SALDO,');
  QrLct.SQL.Add('IF(la.Cliente>0,');
  QrLct.SQL.Add('  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),');
  QrLct.SQL.Add('  IF (la.Fornecedor>0,');
  QrLct.SQL.Add('    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO, ');
  QrLct.SQL.Add('ELT(la.Endossas, "Endossado", "Endossante", "Ambos", "? ? ? ?") NO_ENDOSSADO, ');
  QrLct.SQL.Add('CASE la.UserDel ');
  QrLct.SQL.Add('WHEN -2 THEN "BOSS [Gerencial]" ');
  QrLct.SQL.Add('WHEN -1 THEN "MASTER [Admin. DERMATEK]" ');
  QrLct.SQL.Add('WHEN 0 THEN "N�o definido" ');
  QrLct.SQL.Add('ELSE se.Login END USERDEL_TXT, ');
  QrLct.SQL.Add(MotivoExclusaoLct + ' ');
  QrLct.SQL.Add('FROM lanctoz la');
  QrLct.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  QrLct.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  QrLct.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  QrLct.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  QrLct.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  QrLct.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  QrLct.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrLct.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  QrLct.SQL.Add('LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI');
  QrLct.SQL.Add('LEFT JOIN senhas se ON se.Numero = la.UserDel');
  if VAR_KIND_DEPTO = kdUH then
    QrLct.SQL.Add('LEFT JOIN condimov  ci ON ci.Conta=la.Depto');
  QrLct.SQL.Add('WHERE la.CliInt=' + Geral.FF0(CliInt));
  //Motivo
  if (CkMotivo.Checked) and (CBMotivo.ItemIndex >= 0) then
    QrLct.SQL.Add('AND la.MotvDel=' + FLstMotivDel[CBMotivo.ItemIndex]);
  //Controle
  if CkControle.Checked then
    QrLct.SQL.Add('AND la.Controle=' + Geral.FF0(Controle));
  //Documento
  if CkDoc.Checked then
    QrLct.SQL.Add('AND la.Documento=' + FormatFloat('0', Documento));
  //ID Pagto
  if CkIdPgto.Checked then
    QrLct.SQL.Add('AND la.ID_Pgto=' + Geral.FF0(ID_Pgto));
  //Debito
  if CkDebito.Checked then
    QrLct.SQL.Add('AND la.Debito BETWEEN ' +
      Geral.FFT_Dot(EdDebMin.ValueVariant, 2, siNegativo) + ' AND ' +
      Geral.FFT_Dot(EdDebMax.ValueVariant, 2, siNegativo));
  //Credito
  if CkCredito.Checked then
    QrLct.SQL.Add('AND la.Credito BETWEEN ' +
      Geral.FFT_Dot(EdCredMin.ValueVariant, 2, siNegativo) + ' AND ' +
      Geral.FFT_Dot(EdCredMax.ValueVariant, 2, siNegativo));
  QrLct.SQL.Add('ORDER BY la.Data, la.Controle');
  UMyMod.AbreQuery(QrLct, Dmod.MyDB);
end;

end.
