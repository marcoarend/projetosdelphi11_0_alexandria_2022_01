unit CtaFat0;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  dmkDBLookupComboBox, dmkEditCB, dmkCheckBox, UnDmkProcFunc, DmkDAC_PF,
  UnDmkEnums;

type
  TFmCtaFat0 = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCtaFat0: TmySQLQuery;
    DsCtaFat0: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdFatID: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdFatID: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    Label4: TLabel;
    EdPlaGen: TdmkEditCB;
    CBPlaGen: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    QrCtaFat0NO_CTA: TWideStringField;
    QrCtaFat0PlaGen: TIntegerField;
    QrCtaFat0FatID: TIntegerField;
    QrCtaFat0Nome: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    QrCtaFat0OldNeg: TIntegerField;
    QrCtaFat0AutoUpd: TSmallintField;
    BtContas: TBitBtn;
    CkAutoUpd: TdmkCheckBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCtaFat0AfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCtaFat0BeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Desejado: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmCtaFat0: TFmCtaFat0;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, MyListas, UnFinanceiroJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCtaFat0.LocCod(Atual, Desejado: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Desejado);
end;

procedure TFmCtaFat0.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCtaFat0FatID.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCtaFat0.DefParams;
begin
  VAR_GOTOTABELA := 'ctafat0';
  VAR_GOTOMYSQLTABLE := QrCtaFat0;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := 'FatID';
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cta.Nome NO_CTA, cof.*');
  VAR_SQLx.Add('FROM ctafat0 cof');
  VAR_SQLx.Add('LEFT JOIN contas cta ON cta.Codigo=cof.PlaGen');
  //
  VAR_SQL1.Add('WHERE cof.FatID=:P0');
  //
  //VAR_SQL2.Add('WHERE cof.CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE cof.Nome Like :P0');
  //
end;

procedure TFmCtaFat0.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCtaFat0.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCtaFat0.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCtaFat0.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCtaFat0.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCtaFat0.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCtaFat0.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCtaFat0.SpeedButton5Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo       := EdPlaGen.ValueVariant;
  VAR_CADASTRO := 0;
  //
  FinanceiroJan.CadastroDeContas(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrContas, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdPlaGen, CBPlaGen, QrContas, VAR_CADASTRO);
  end;
end;

procedure TFmCtaFat0.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtaFat0.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCtaFat0, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ctafat0');
end;

procedure TFmCtaFat0.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCtaFat0FatID.Value;
  Close;
end;

procedure TFmCtaFat0.BtConfirmaClick(Sender: TObject);
var
  FatID, PlaGen: Integer;
  Nome: String;
  SQLType: TSQLType;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  PlaGen := EdPlaGen.ValueVariant;
  if MyObjects.FIC(PlaGen = 0, EdPlaGen, 'Defina uma conta (do plano de contas)!') then Exit;
  //
  SQLType := ImgTipo.SQLType;
  FatID := UMyMod.BuscaEmLivreY_Def('ctafat0', 'FatID', ImgTipo.SQLType,
    QrCtaFat0FatID.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmCtaFat0, PnEdita,
    'ctafat0', FatID, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    if (SQLType = stUpd) and ((
    CkAutoUpd.Checked) or (QrCtaFat0OldNeg.Value < -99)) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
      'Genero'], ['FatID'], [PlaGen], [FatID], True);
    end;
    LocCod(FatID, FatID);

  end;
end;

procedure TFmCtaFat0.BtContasClick(Sender: TObject);
var
  Codigo, PlaGen, SubGrupo, FatID: Integer;
  Nome, Nome2, Credito, Debito: String;
begin
  if Geral.MensagemBox(
  'A a��o a seguir ir� criar contas novas no plano de contas, ' + sLineBreak +
  'sendo criada uma conta para cada conta de faturamento sem conta definida!' + sLineBreak +
  '' + sLineBreak +
  'Antes de executar esta a��o tenha certeza que n�o existe cadastro ' + sLineBreak +
  'de contas que poderia ser usada para alguma conta de faturamento!' + sLineBreak +
  '' + sLineBreak +
  'Deseja realmente criar as contas e atrel�-las �s contas de faturamento sem contas?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT FatID, Nome, ValFator ',
    'FROM ctafat0 ',
    'WHERE PlaGen=0 ',
    'AND FatID > 0 ',
    '']);
    //
    if Dmod.QrAux.RecordCount > 0 then
    begin
      if Geral.MensagemBox(
      'Foram localizadas ' + Geral.FF0(Dmod.QrAux.RecordCount) +
      ' contas de faturamento sem conta atrelada!' + sLineBreak +
      '' + sLineBreak +
      'Deseja realmente criar as contas e atrel�-las a estas contas de faturamento?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Dmod.QrAux.First;
        while not Dmod.QrAux.Eof do
        begin
          Nome := Dmod.QrAux.FieldByName('Nome').AsString;
          Nome2 := Nome;
          if Dmod.QrAux.FieldByName('ValFator').AsInteger = 1 then
          begin
            Credito := 'V';
            Debito := 'F';
          end else begin
            Credito := 'F';
            Debito := 'V';
          end;
          Subgrupo := 0;
          //
          Codigo := UMyMod.BuscaEmLivreY_Def('contas', 'Codigo', stIns, 0);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'contas', False, [
          'Nome', 'Nome2',
          'Subgrupo', 'Credito', 'Debito'], [
          'Codigo'], [
          Nome, Nome2,
          Subgrupo, Credito, Debito], [
          Codigo], False) then
          begin
            PlaGen := Codigo;
            FatID := Dmod.QrAux.FieldByName('FatID').AsInteger;
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctafat0', False, [
            'PlaGen'], ['FatID'], [PlaGen], [FatID], False);
          end;
          //
          Dmod.QrAux.Next;
        end;
        //
        LocCod(QrCtaFat0FatID.Value, QrCtaFat0FatID.Value);
      end;
    end else Geral.MensagemBox(
      'N�o foram localizadas contas de faturamento sem conta atrelada!', 'Mensagem',
      MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TFmCtaFat0.BtDesisteClick(Sender: TObject);
var
  FatID: Integer;
begin
  FatID := EdFatID.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ctafat0', FatID);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(FatID, Dmod.MyDB, 'ctafat0', 'FatID');
end;

procedure TFmCtaFat0.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCtaFat0, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ctafat0');
end;

procedure TFmCtaFat0.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  UMyMod.AbreQuery(QrContas, Dmod.MyDB);
end;

procedure TFmCtaFat0.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCtaFat0FatID.Value, LaRegistro.Caption);
end;

procedure TFmCtaFat0.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCtaFat0.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCtaFat0FatID.Value, LaRegistro.Caption);
end;

procedure TFmCtaFat0.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCtaFat0.QrCtaFat0AfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCtaFat0.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCtaFat0.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCtaFat0FatID.Value,
  CuringaLoc.CriaForm('FatID', CO_NOME, 'ctafat0', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCtaFat0.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtaFat0.QrCtaFat0BeforeOpen(DataSet: TDataSet);
begin
  QrCtaFat0FatID.DisplayFormat := FFormatFloat;
end;

end.

