unit ContasFat0;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  dmkDBLookupComboBox, dmkEditCB;

type
  TFmContasFat0 = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrContasFat0: TmySQLQuery;
    DsContasFat0: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdFatID: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdFatID: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    Label4: TLabel;
    EdPlaGen: TdmkEditCB;
    CBPlaGen: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    QrContasFat0NO_CTA: TWideStringField;
    QrContasFat0PlaGen: TIntegerField;
    QrContasFat0FatID: TIntegerField;
    QrContasFat0Nome: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrContasFat0AfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrContasFat0BeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Desejado: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmContasFat0: TFmContasFat0;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Contas, MyDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmContasFat0.LocCod(Atual, Desejado: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Desejado);
end;

procedure TFmContasFat0.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrContasFat0FatID.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmContasFat0.DefParams;
begin
  VAR_GOTOTABELA := 'ContasFat0';
  VAR_GOTOMYSQLTABLE := QrContasFat0;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := 'FatID';
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cta.Nome NO_CTA, cof.*');
  VAR_SQLx.Add('FROM ContasFat0 cof');
  VAR_SQLx.Add('LEFT JOIN contas cta ON cta.Codigo=cof.PlaGen');
  //
  VAR_SQL1.Add('WHERE cof.FatID=:P0');
  //
  //VAR_SQL2.Add('WHERE cof.CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE cof.Nome Like :P0');
  //
end;

procedure TFmContasFat0.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmContasFat0.QueryPrincipalAfterOpen;
begin
end;

procedure TFmContasFat0.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmContasFat0.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmContasFat0.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmContasFat0.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmContasFat0.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmContasFat0.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.ShowModal;
    FmContas.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodigoPesquisado(EdPlaGen, CBPlaGen, QrContas, VAR_CADASTRO);
  end;
end;

procedure TFmContasFat0.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasFat0.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrContasFat0, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ContasFat0');
end;

procedure TFmContasFat0.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrContasFat0FatID.Value;
  Close;
end;

procedure TFmContasFat0.BtConfirmaClick(Sender: TObject);
var
  FatID: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  FatID := UMyMod.BuscaEmLivreY_Def('ContasFat0', 'FatID', ImgTipo.SQLType,
    QrContasFat0FatID.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmContasFat0, PnEdita,
    'ContasFat0', FatID, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(FatID, FatID);
  end;
end;

procedure TFmContasFat0.BtDesisteClick(Sender: TObject);
var
  FatID: Integer;
begin
  FatID := EdFatID.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ContasFat0', FatID);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(FatID, Dmod.MyDB, 'ContasFat0', 'FatID');
end;

procedure TFmContasFat0.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrContasFat0, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ContasFat0');
end;

procedure TFmContasFat0.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  UMyMod.AbreQuery(QrContas);
end;

procedure TFmContasFat0.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrContasFat0FatID.Value, LaRegistro.Caption);
end;

procedure TFmContasFat0.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmContasFat0.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrContasFat0FatID.Value, LaRegistro.Caption);
end;

procedure TFmContasFat0.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmContasFat0.QrContasFat0AfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmContasFat0.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasFat0.SbQueryClick(Sender: TObject);
begin
  LocCod(QrContasFat0FatID.Value,
  CuringaLoc.CriaForm('FatID', CO_NOME, 'ContasFat0', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmContasFat0.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasFat0.QrContasFat0BeforeOpen(DataSet: TDataSet);
begin
  QrContasFat0FatID.DisplayFormat := FFormatFloat;
end;

end.

