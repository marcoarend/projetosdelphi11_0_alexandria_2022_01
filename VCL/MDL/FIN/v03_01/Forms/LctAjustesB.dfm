object FmLctAjustesB: TFmLctAjustesB
  Left = 339
  Top = 185
  Caption = 'Emiss'#245'es Quitadas sem Lan'#231'amento de Quita'#231#227'o'
  ClientHeight = 469
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 105
    Width = 784
    Height = 294
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 61
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaResu: TLabel
        Left = 16
        Top = 44
        Width = 13
        Height = 13
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 12
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 705
        Height = 21
        KeyField = 'FILIAL'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 61
      Width = 784
      Height = 233
      Align = alClient
      DataSource = DsSemQuita
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Carteira'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Width = 443
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Width = 72
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 606
        Height = 32
        Caption = 'Emiss'#245'es Quitadas sem Lan'#231'amento de Quita'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 606
        Height = 32
        Caption = 'Emiss'#245'es Quitadas sem Lan'#231'amento de Quita'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 606
        Height = 32
        Caption = 'Emiss'#245'es Quitadas sem Lan'#231'amento de Quita'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 784
    Height = 57
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 23
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 399
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 18
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object RGQuais: TRadioGroup
        Left = 144
        Top = 0
        Width = 257
        Height = 45
        Caption = ' Quais: '
        Columns = 3
        Items.Strings = (
          'Ambos'
          'Com Ctrl'
          'Sem Ctrl')
        TabOrder = 1
      end
      object BtImprimir: TBitBtn
        Tag = 5
        Left = 408
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprimir'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtImprimirClick
      end
    end
  end
  object QrEmiss: TmySQLQuery
    Database = Dmod.MyDB
    Left = 8
    Top = 8
    object QrEmissData: TDateField
      FieldName = 'Data'
    end
    object QrEmissCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrEmissDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrEmissControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEmissCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrEmissCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrEmissCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrEmissDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrQuit: TmySQLQuery
    Database = Dmod.MyDB
    Left = 36
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrSemQuita: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _sem_quita_'
      'ORDER BY Carteira, Data, Controle')
    Left = 64
    Top = 8
    object QrSemQuitaCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrSemQuitaData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSemQuitaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSemQuitaDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrSemQuitaCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSemQuitaDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsSemQuita: TDataSource
    DataSet = QrSemQuita
    Left = 92
    Top = 8
  end
  object QrPgto: TmySQLQuery
    Database = Dmod.MyDB
    Left = 36
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object frxDsSemQuita: TfrxDBDataset
    UserName = 'frxDsSemQuita'
    CloseDataSource = False
    DataSet = QrSemQuita
    BCDToCurrency = False
    Left = 92
    Top = 36
  end
  object frxSemQuita: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40568.502692662030000000
    ReportOptions.LastChange = 40568.507147650460000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 120
    Top = 36
    Datasets = <
      item
        DataSet = frxDsSemQuita
        DataSetName = 'frxDsSemQuita'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 22.677180000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Relat'#243'rio de Emiss'#245'es Quitadas sem sua Quita'#231#227'o ser Localizada')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 18.897650000000000000
        Top = 64.252010000000000000
        Width = 680.315400000000000000
        object Memo3: TfrxMemoView
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsSemQuita
          DataSetName = 'frxDsSemQuita'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 56.692950000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsSemQuita
          DataSetName = 'frxDsSemQuita'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 113.385900000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsSemQuita
          DataSetName = 'frxDsSemQuita'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 170.078850000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsSemQuita
          DataSetName = 'frxDsSemQuita'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 245.669450000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsSemQuita
          DataSetName = 'frxDsSemQuita'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 321.260050000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsSemQuita
          DataSetName = 'frxDsSemQuita'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 143.622140000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSemQuita
        DataSetName = 'frxDsSemQuita'
        RowCount = 0
        object Memo2: TfrxMemoView
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Carteira'
          DataSet = frxDsSemQuita
          DataSetName = 'frxDsSemQuita'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSemQuita."Carteira"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 56.692950000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Data'
          DataSet = frxDsSemQuita
          DataSetName = 'frxDsSemQuita'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSemQuita."Data"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 113.385900000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Controle'
          DataSet = frxDsSemQuita
          DataSetName = 'frxDsSemQuita'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSemQuita."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 170.078850000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsSemQuita
          DataSetName = 'frxDsSemQuita'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',<frxDsSemQui' +
              'ta."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 245.669450000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsSemQuita
          DataSetName = 'frxDsSemQuita'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',<frxDsSemQui' +
              'ta."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 321.260050000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Descricao'
          DataSet = frxDsSemQuita
          DataSetName = 'frxDsSemQuita'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSemQuita."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        Height = 18.897650000000000000
        Top = 185.196970000000000000
        Width = 680.315400000000000000
        object Memo14: TfrxMemoView
          Left = 495.118430000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
end
