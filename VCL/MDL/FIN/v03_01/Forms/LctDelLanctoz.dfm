object FmLctDelLanctoz: TFmLctDelLanctoz
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Lan'#231'amentos Exclu'#237'dos'
  ClientHeight = 692
  ClientWidth = 965
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 906
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 847
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 341
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lan'#231'amentos Exclu'#237'dos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 341
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lan'#231'amentos Exclu'#237'dos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 341
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lan'#231'amentos Exclu'#237'dos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 965
    Height = 492
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 965
      Height = 492
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 965
        Height = 492
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object DBGLct: TdmkDBGrid
          Left = 2
          Top = 215
          Width = 961
          Height = 275
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataCad'
              Title.Caption = 'Cadastro'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SerieCH'
              Title.Caption = 'S'#233'rie'
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Title.Caption = 'Cheque'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duplicata'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Alignment = taRightJustify
              Title.Caption = 'Quantidade'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 164
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'to'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Title.Caption = 'Saldo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Carteira'
              Title.Caption = 'Carteira'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Pgto'
              Title.Caption = 'ID Pagto.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataDel'
              Title.Caption = 'Data / Hora Del.'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USERDEL_TXT'
              Title.Caption = 'Usu'#225'rio Del.'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MOTVDEL'
              Title.Caption = 'Motivo da exclus'#227'o'
              Width = 250
              Visible = True
            end>
          Color = clWindow
          DataSource = DsLct
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataCad'
              Title.Caption = 'Cadastro'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SerieCH'
              Title.Caption = 'S'#233'rie'
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Title.Caption = 'Cheque'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duplicata'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Alignment = taRightJustify
              Title.Caption = 'Quantidade'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 164
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'to'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Title.Caption = 'Saldo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Carteira'
              Title.Caption = 'Carteira'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Pgto'
              Title.Caption = 'ID Pagto.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataDel'
              Title.Caption = 'Data / Hora Del.'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USERDEL_TXT'
              Title.Caption = 'Usu'#225'rio Del.'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MOTVDEL'
              Title.Caption = 'Motivo da exclus'#227'o'
              Width = 250
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 2
          Top = 18
          Width = 961
          Height = 197
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitWidth = 960
          object Label1: TLabel
            Left = 5
            Top = 5
            Width = 87
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente interno:'
          end
          object Bevel1: TBevel
            Left = 5
            Top = 58
            Width = 228
            Height = 70
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
          end
          object Bevel2: TBevel
            Left = 242
            Top = 58
            Width = 228
            Height = 70
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
          end
          object EdEmpresa: TdmkEditCB
            Left = 5
            Top = 25
            Width = 54
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInt64
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 59
            Top = 25
            Width = 756
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdControle: TdmkEdit
            Left = 591
            Top = 91
            Width = 108
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object CkDoc: TCheckBox
            Left = 480
            Top = 66
            Width = 103
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Documento'
            TabOrder = 8
          end
          object EdDoc: TdmkEdit
            Left = 480
            Top = 91
            Width = 103
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object CkControle: TCheckBox
            Left = 591
            Top = 66
            Width = 108
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Controle'
            TabOrder = 10
          end
          object EdIdPgto: TdmkEdit
            Left = 706
            Top = 91
            Width = 109
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 13
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object CkIdPgto: TCheckBox
            Left = 706
            Top = 66
            Width = 109
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ID pagto.'
            TabOrder = 12
          end
          object BtOK: TBitBtn
            Tag = 14
            Left = 480
            Top = 137
            Width = 148
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Ok'
            NumGlyphs = 2
            TabOrder = 16
            OnClick = BtOKClick
          end
          object EdCredMax: TdmkEdit
            Left = 350
            Top = 92
            Width = 93
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCredMin: TdmkEdit
            Left = 251
            Top = 92
            Width = 94
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdCredMinExit
          end
          object CkCredito: TCheckBox
            Left = 251
            Top = 63
            Width = 190
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cr'#233'dito (m'#237'nimo e m'#225'ximo)'
            TabOrder = 5
          end
          object CkDebito: TCheckBox
            Left = 16
            Top = 63
            Width = 188
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'D'#233'bito (m'#237'nimo e m'#225'ximo)'
            TabOrder = 2
          end
          object EdDebMin: TdmkEdit
            Left = 16
            Top = 91
            Width = 94
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdDebMinExit
          end
          object EdDebMax: TdmkEdit
            Left = 117
            Top = 91
            Width = 93
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object CkMotivo: TCheckBox
            Left = 5
            Top = 135
            Width = 148
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Motivo da exclus'#227'o'
            TabOrder = 14
          end
          object CBMotivo: TComboBox
            Left = 5
            Top = 160
            Width = 465
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Style = csDropDownList
            TabOrder = 15
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 551
    Width = 965
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 960
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 606
    Width = 965
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 785
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 783
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object QrLct: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, la.DataDel,'
      'CASE la.UserDel'
      'WHEN -2 THEN "BOSS [Administrador]"'
      'WHEN -1 THEN "MASTER [Admin. DERMATEK]"'
      'WHEN 0 THEN "N'#227'o definido"'
      'ELSE se.Login END USERDEL_TXT,'
      '  ((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,'
      'ci.Unidade UH, '
      'la.*, ct.Codigo CONTA, ca.Prazo, ca.Nome NO_Carteira, '
      'ca.Banco1, ca.Agencia1, ca.Conta1,'
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,'
      'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,'
      'IF(la.ForneceI=0, "",'
      '  IF(fi.Tipo=0, fi.RazaoSocial, fi.Nome)) NOMEFORNECEI,'
      'IF(la.Sit<2, la.Credito-la.Debito-(la.Pago*la.Sit), 0) SALDO,'
      'IF(la.Cliente>0,'
      '  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),'
      '  IF (la.Fornecedor>0,'
      
        '    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO' +
        ', '
      
        'ELT(la.Endossas, "Endossado", "Endossante", "Ambos", "? ? ? ?") ' +
        'NO_ENDOSSADO '
      'FROM lanctoz la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI'
      'LEFT JOIN condimov  ci ON ci.Conta=la.Depto'
      'LEFT JOIN senhas se ON se.Numero = la.UserDel'
      'WHERE la.Data BETWEEN "2007/05/11" AND "2010/10/04"'
      'AND la.Carteira=3'
      'ORDER BY la.Data, la.Controle')
    Left = 196
    Top = 244
    object QrLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 25
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrLctNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 128
    end
    object QrLctNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'contas.Nome'
      Size = 128
    end
    object QrLctNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 128
    end
    object QrLctNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 128
    end
    object QrLctNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 128
    end
    object QrLctNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctcliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctUserCad: TSmallintField
      FieldName = 'UserCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLctCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLctPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrLctForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLctQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLctEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrLctAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrLctContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrLctCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrLctDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrLctDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrLctNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrLctAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrLctUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrLctExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrLctSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLctMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrLctMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrLctDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrLctCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrLctTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrLctID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrLctReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrLctAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrLctPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrLctPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrLctSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrLctMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrLctProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrLctCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
      Required = True
    end
    object QrLctAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLctAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrLctSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 5
    end
    object QrLctEndossas: TSmallintField
      FieldName = 'Endossas'
    end
    object QrLctEndossan: TFloatField
      FieldName = 'Endossan'
    end
    object QrLctEndossad: TFloatField
      FieldName = 'Endossad'
    end
    object QrLctCancelado: TSmallintField
      FieldName = 'Cancelado'
    end
    object QrLctEventosCad: TIntegerField
      FieldName = 'EventosCad'
    end
    object QrLctUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrLctEncerrado: TIntegerField
      FieldName = 'Encerrado'
    end
    object QrLctErrCtrl: TIntegerField
      FieldName = 'ErrCtrl'
    end
    object QrLctNO_Carteira: TWideStringField
      FieldName = 'NO_Carteira'
      Size = 100
    end
    object QrLctBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrLctAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrLctConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrLctNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrLctNO_ENDOSSADO: TWideStringField
      FieldName = 'NO_ENDOSSADO'
      Size = 10
    end
    object QrLctSERIE_CHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_CHEQUE'
      Calculated = True
    end
    object QrLctIndiPag: TIntegerField
      FieldName = 'IndiPag'
      DisplayFormat = '0;-0; '
    end
    object QrLctFisicoSrc: TSmallintField
      FieldName = 'FisicoSrc'
    end
    object QrLctFisicoCod: TIntegerField
      FieldName = 'FisicoCod'
    end
    object QrLctDataDel: TDateTimeField
      FieldName = 'DataDel'
    end
    object QrLctUserDel: TIntegerField
      FieldName = 'UserDel'
    end
    object QrLctUSERDEL_TXT: TWideStringField
      FieldName = 'USERDEL_TXT'
      Size = 30
    end
    object QrLctNO_MOTVDEL: TWideStringField
      FieldName = 'NO_MOTVDEL'
      Size = 100
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 224
    Top = 244
  end
end
