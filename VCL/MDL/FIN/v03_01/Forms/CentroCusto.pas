unit CentroCusto;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, Menus, MyDBCheck, dmkPermissoes, dmkEdit, dmkGeral, UnDmkProcFunc,
  dmkImage, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB;

type
  TFmCentroCusto = class(TForm)
    PainelDados: TPanel;
    DsCentroCusto: TDataSource;
    QrCentroCusto: TmySQLQuery;
    PainelEdita: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    QrCentroCustoCodigo: TIntegerField;
    QrCentroCustoNome: TWideStringField;
    QrCentroCustoLk: TIntegerField;
    QrCentroCustoDataCad: TDateField;
    QrCentroCustoDataAlt: TDateField;
    QrCentroCustoUserCad: TIntegerField;
    QrCentroCustoUserAlt: TIntegerField;
    Panel1: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    DBGrid1: TDBGrid;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TIntegerField;
    QrContasUserAlt: TIntegerField;
    QrContasCentroCusto: TIntegerField;
    QrContasRateio: TIntegerField;
    QrContasEntidade: TIntegerField;
    PMCentroCusto: TPopupMenu;
    Incluicentrodecusto1: TMenuItem;
    Alteracentrodecusto1: TMenuItem;
    Excluicentrodecusto1: TMenuItem;
    PMContas: TPopupMenu;
    IncluiContas1: TMenuItem;
    ExcluiContas1: TMenuItem;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtConfirma: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel6: TPanel;
    BitBtn1: TBitBtn;
    BtCentroCusto: TBitBtn;
    BtContas: TBitBtn;
    BtNivelAcima: TBitBtn;
    QrCentroCustoAlterWeb: TSmallintField;
    QrCentroCustoAtivo: TSmallintField;
    QrCentroCustoPagRec: TSmallintField;
    QrCentroCustoCentroCust2: TIntegerField;
    QrCentroCustoNO_CC2: TWideStringField;
    Label3: TLabel;
    EdCentroCust2: TdmkEditCB;
    CBCentroCust2: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    DBRadioGroup2: TDBRadioGroup;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    RGPagRec: TRadioGroup;
    QrCentroCust2: TmySQLQuery;
    QrCentroCust2Codigo: TIntegerField;
    QrCentroCust2Nome: TWideStringField;
    QrCentroCust2PagRec: TSmallintField;
    QrCentroCust2Lk: TIntegerField;
    QrCentroCust2DataCad: TDateField;
    QrCentroCust2DataAlt: TDateField;
    QrCentroCust2UserCad: TIntegerField;
    QrCentroCust2UserAlt: TIntegerField;
    QrCentroCust2AlterWeb: TSmallintField;
    QrCentroCust2Ativo: TSmallintField;
    DsCentroCust2: TDataSource;
    Label5: TLabel;
    EdOrdem: TdmkEdit;
    QrCentroCustoOrdem: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtCentroCustoClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCentroCustoAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrCentroCustoAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCentroCustoBeforeOpen(DataSet: TDataSet);
    procedure Incluicentrodecusto1Click(Sender: TObject);
    procedure Alteracentrodecusto1Click(Sender: TObject);
    procedure IncluiContas1Click(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure ExcluiContas1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtNivelAcimaClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure PMCentroCustoPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    function RemoveConta(): Boolean;
  public
    { Public declarations }
    procedure ReopenContas(Conta: Integer);
  end;

var
  FmCentroCusto: TFmCentroCusto;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, CentroCustoCtas, QuaisItens, DmkDAC_PF, CentroCust2;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCentroCusto.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCentroCusto.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCentroCustoCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCentroCusto.DefParams;
begin
  VAR_GOTOTABELA := 'CentroCusto';
  VAR_GOTOMYSQLTABLE := QrCentroCusto;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT cc1.*, cc2.Nome NO_CC2  ');
  VAR_SQLx.Add('FROM centrocusto cc1 ');
  VAR_SQLx.Add('LEFT JOIN centrocust2 cc2 ON cc2.Codigo=cc1.CentroCust2 ');
  VAR_SQLx.Add('WHERE cc1.Codigo > -1000000');
  //
  VAR_SQL1.Add('AND cc1.Codigo=:P0');
  //
  VAR_SQLa.Add('AND cc1.Nome Like :P0');
  //
end;

procedure TFmCentroCusto.ExcluiContas1Click(Sender: TObject);
begin
  RemoveConta;
end;

procedure TFmCentroCusto.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    EdNome.Text := CO_VAZIO;
    EdNome.Visible := True;
    if SQLType = stIns then
    begin
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
    end else begin
      EdCodigo.Text  := DBEdCodigo.Text;
      EdNome.Text    := DBEdNome.Text;
      RGPagRec.ItemIndex         := QrCentroCustoPagRec.Value;
      EdCentroCust2.ValueVariant := QrCentroCustoCentroCust2.Value;
      CBCentroCust2.KeyValue     := QrCentroCustoCentroCust2.Value;
      EdOrdem.ValueVariant       := QrCentroCustoOrdem.Value;
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCentroCusto.PMCentroCustoPopup(Sender: TObject);
begin
  //MyObjects.HabilitaMenuItemCabIns(Incluicentrodecusto1, QrCentroCusto);
  MyObjects.HabilitaMenuItemCabUpd(Alteracentrodecusto1, QrCentroCusto);
  MyObjects.HabilitaMenuItemCabDel(Excluicentrodecusto1, QrCentroCusto, QrContas);
end;

procedure TFmCentroCusto.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCentroCusto.Alteracentrodecusto1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmCentroCusto.AlteraRegistro;
var
  CentroCusto : Integer;
begin
  CentroCusto := QrCentroCustoCodigo.Value;
  if not UMyMod.SelLockY(CentroCusto, Dmod.MyDB, 'CentroCusto', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(CentroCusto, Dmod.MyDB, 'CentroCusto', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCentroCusto.Incluicentrodecusto1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmCentroCusto.IncluiContas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCentroCustoCtas, FmCentroCustoCtas, afmoNegarComAviso) then
  begin
    FmCentroCustoCtas.ShowModal;
    FmCentroCustoCtas.Destroy;
  end;
end;

procedure TFmCentroCusto.IncluiRegistro;
var
  Cursor : TCursor;
  CentroCusto : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    CentroCusto := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'CentroCusto', 'CentroCusto', 'Codigo');
    if Length(FormatFloat(FFormatFloat, CentroCusto))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro(
      'Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, stIns, CentroCusto);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmCentroCusto.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCentroCusto.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCentroCusto.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCentroCusto.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCentroCusto.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCentroCusto.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCentroCusto.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCentroCust2, FmCentroCust2, afmoNegarComAviso) then
  begin
    FmCentroCust2.ShowModal;
    FmCentroCust2.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdCentroCust2, CBCentroCust2, QrCentroCust2,
    VAR_CADASTRO);
end;

procedure TFmCentroCusto.BitBtn1Click(Sender: TObject);
begin
  VAR_CADASTRO := QrCentroCustoCodigo.Value;
  Close;
end;

procedure TFmCentroCusto.BtCentroCustoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCentroCusto, BtCentroCusto);
end;

procedure TFmCentroCusto.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, PagRec, CentroCust2, Ordem: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  PagRec         := RGPagRec.ItemIndex - 1;
  CentroCust2    := EdCentroCust2.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then
    Exit;
  if MyObjects.FIC(PagRec = 0, RGPagRec, 'Defina uma caracteristica v�lida!') then
    Exit;
  if MyObjects.FIC(CentroCust2 = 0, EdCentroCust2, 'Defina uma caracteristica v�lida!') then
    Exit;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'centrocusto', False, [
  'Nome', 'PagRec', 'CentroCust2',
  'Ordem'], [
  'Codigo'], [
  Nome, PagRec, CentroCust2,
  Ordem], [
  Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CentroCusto', 'Codigo');
    MostraEdicao(False, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmCentroCusto.BtContasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMContas, Btcontas);
end;

procedure TFmCentroCusto.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'CentroCusto', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CentroCusto', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CentroCusto', 'Codigo');
end;

procedure TFmCentroCusto.BtNivelAcimaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCentroCust2, FmCentroCust2, afmoNegarComAviso) then
  begin
    FmCentroCust2.ShowModal;
    FmCentroCust2.Destroy;
  end;
end;

procedure TFmCentroCusto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  DBGrid1.Align := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrCentroCust2, Dmod.MyDB);
end;

procedure TFmCentroCusto.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCentroCustoCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCentroCusto.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmCentroCusto.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCentroCusto.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmCentroCusto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCentroCusto.QrCentroCustoAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCentroCusto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'CentroCusto', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmCentroCusto.QrCentroCustoAfterScroll(DataSet: TDataSet);
begin
  ReopenContas(0);
end;

procedure TFmCentroCusto.ReopenContas(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrContas, Dmod.MyDB, [
  'SELECT * ',
  'FROM contas ',
  'WHERE CentroCusto=' + Geral.FF0(QrCentroCustoCodigo.Value),
  'ORDER BY Nome ',
  '']);
  //
  if Conta <> 0 then
    QrContas.Locate('Codigo', Conta, []);
end;

procedure TFmCentroCusto.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCentroCustoCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'CentroCusto', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCentroCusto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCentroCusto.QrCentroCustoBeforeOpen(DataSet: TDataSet);
begin
  QrCentroCustoCodigo.DisplayFormat := FFormatFloat;
end;

function TFmCentroCusto.RemoveConta(): Boolean;
  function ExecutaSQL: Boolean;
  begin
    //Result := False;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE contas SET CentroCusto=0 WHERE Codigo=' +
      FormatFloat('0', QrContasCodigo.Value));
    Dmod.QrUpd.ExecSQL;
    Result := True;
  end;
var
  n, m: integer;
  q: TSelType;
begin
  q := istNenhum;
  Result := False;
  if (QrContas.State <> dsBrowse) or (QrContas.RecordCount = 0) then
  begin
    Geral.MB_Aviso('N�o h� item a ser removido!');
    Exit;
  end;
  if MyObjects.CriaForm_AcessoTotal(TFmQuaisItens, FmQuaisItens) then
  begin
    with FmQuaisItens do
    begin
      ShowModal;
      if not FSelecionou then
      begin
        Geral.MB_Aviso('Remo��o cancelada pelo usu�rio!');
        q := istDesiste;
      end else q := FEscolha;
      Destroy;
    end;
  end;
  if q = istDesiste then Exit;
  //
  Screen.Cursor := crHourGlass;
  m := 0;
  if (q = istSelecionados) and (DBGrid1.SelectedRows.Count < 2) then
    q := istAtual;
  case q of
    istAtual:
    begin
      if Geral.MB_Pergunta('Confirma a remo��o do item selecionado?') = ID_YES then
        if ExecutaSQL() then m := 1;
    end;
    istSelecionados:
    begin
      if Geral.MB_Pergunta('Confirma a remo��o dos ' +
      IntToStr(DBGrid1.SelectedRows.Count) + ' itens selecionados?') = ID_YES then
      begin
        with DBGrid1.DataSource.DataSet do
        for n := 0 to DBGrid1.SelectedRows.Count-1 do
        begin
          //GotoBookmark(DBGrid1.SelectedRows.Items[n]);
          GotoBookmark(DBGrid1.SelectedRows.Items[n]);
          if ExecutaSQL() then inc(m, 1);
        end;
      end;
    end;
    istTodos:
    begin
      if Geral.MB_Pergunta('Confirma a remo��o de todos os ' +
      IntToStr(QrContas.RecordCount) + ' itens selecionados?') = ID_YES then
      begin
        QrContas.First;
        while not QrContas.Eof do
        begin
          if ExecutaSQL() then inc(m, 1);
          QrContas.Next;
        end;
      end;
    end;
  end;
  if m > 0 then
  begin
    Result := True;
    FmCentroCusto.ReopenContas(QrContasCodigo.Value);
    UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
    if m = 1 then Geral.MB_Aviso('Um item foi removido!') else
    Geral.MB_Aviso(IntToStr(m) + ' itens foram removidos!');
  end;
  Screen.Cursor := crDefault;
end;

end.

