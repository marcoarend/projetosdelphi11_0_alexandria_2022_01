object FmCcrCad: TFmCcrCad
  Left = 357
  Top = 194
  Caption = 'CCR-CADAS-001 :: Cadastro de Centros de Custos - N'#237'veis'
  ClientHeight = 411
  ClientWidth = 779
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 94
    Width = 779
    Height = 317
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 200
    ExplicitTop = 126
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 779
      Height = 125
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object LaNivSel: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 118
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object LaNivSup: TLabel
        Left = 16
        Top = 48
        Width = 36
        Height = 13
        Caption = 'Nivel 2:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 700
        Top = 48
        Width = 34
        Height = 13
        Caption = 'Ordem:'
        Color = clBtnFace
        ParentColor = False
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 98
        Height = 21
        BiDiMode = bdLeftToRight
        DataField = 'Codigo'
        DataSource = DsCentroCusto
        ParentBiDiMode = False
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 118
        Top = 24
        Width = 423
        Height = 21
        BiDiMode = bdLeftToRight
        DataField = 'Nome'
        DataSource = DsCentroCusto
        ParentBiDiMode = False
        TabOrder = 1
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 543
        Top = 8
        Width = 214
        Height = 38
        Caption = ' Estrutura: '
        Columns = 3
        DataField = 'PagRec'
        DataSource = DsCentroCusto
        Items.Strings = (
          'Custo'
          'N/D'
          'Resultado')
        TabOrder = 2
        Values.Strings = (
          '-1'
          '0'
          '1')
      end
      object DBEdit1: TDBEdit
        Left = 15
        Top = 64
        Width = 55
        Height = 21
        DataField = 'CentroCust2'
        DataSource = DsCentroCusto
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 74
        Top = 64
        Width = 623
        Height = 21
        DataField = 'NO_CC2'
        DataSource = DsCentroCusto
        TabOrder = 4
      end
      object DBEdit3: TDBEdit
        Left = 700
        Top = 64
        Width = 55
        Height = 21
        DataField = 'Ordem'
        DataSource = DsCentroCusto
        TabOrder = 5
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 254
      Width = 779
      Height = 63
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 169
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 126
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 86
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 47
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 171
        Top = 15
        Width = 93
        Height = 46
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 264
        Top = 15
        Width = 513
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel6: TPanel
          Left = 382
          Top = 0
          Width = 131
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BitBtn1: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 118
            Height = 39
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn1Click
          end
        end
        object BtCentroCusto: TBitBtn
          Tag = 10132
          Left = 6
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&C.Custo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCentroCustoClick
        end
        object BtContas: TBitBtn
          Tag = 10133
          Left = 246
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = 'C&ontas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtContasClick
        end
        object BtNivelAcima: TBitBtn
          Tag = 353
          Left = 126
          Top = 4
          Width = 118
          Height = 39
          Caption = '&Nivel 2'
          NumGlyphs = 2
          TabOrder = 3
          OnClick = BtNivelAcimaClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 94
    Width = 779
    Height = 317
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object Label9: TLabel
      Left = 16
      Top = 8
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label10: TLabel
      Left = 82
      Top = 8
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label3: TLabel
      Left = 16
      Top = 48
      Width = 38
      Height = 13
      Caption = 'N'#237'vel 2:'
    end
    object SpeedButton5: TSpeedButton
      Left = 672
      Top = 61
      Width = 25
      Height = 25
      Caption = '...'
      OnClick = SpeedButton5Click
    end
    object Label5: TLabel
      Left = 701
      Top = 48
      Width = 34
      Height = 13
      Caption = 'Ordem:'
      Color = clBtnFace
      ParentColor = False
    end
    object EdCodigo: TdmkEdit
      Left = 16
      Top = 24
      Width = 62
      Height = 20
      Alignment = taRightJustify
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 2
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 82
      Top = 24
      Width = 459
      Height = 20
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 248
      Width = 779
      Height = 69
      Align = alBottom
      TabOrder = 2
      object PnSaiDesis: TPanel
        Left = 635
        Top = 15
        Width = 142
        Height = 52
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 13
          Left = 12
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 633
        Height = 52
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 12
          Top = 4
          Width = 118
          Height = 39
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
    object EdCentroCust2: TdmkEditCB
      Left = 16
      Top = 64
      Width = 55
      Height = 20
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Entidade'
      UpdCampo = 'Entidade'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCentroCust2
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCentroCust2: TdmkDBLookupComboBox
      Left = 71
      Top = 64
      Width = 598
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCentroCust2
      TabOrder = 4
      dmkEditCB = EdCentroCust2
      QryCampo = 'Entidade'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object RGPagRec: TRadioGroup
      Left = 543
      Top = 8
      Width = 214
      Height = 36
      Caption = ' Estrutura: '
      Columns = 3
      ItemIndex = 1
      Items.Strings = (
        'Custo'
        'N/D'
        'Resultado')
      TabOrder = 5
    end
    object EdOrdem: TdmkEdit
      Left = 701
      Top = 64
      Width = 55
      Height = 20
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Ordem'
      UpdCampo = 'Ordem'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 779
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 731
      Top = 0
      Width = 48
      Height = 51
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 213
      Height = 51
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 8
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 213
      Top = 0
      Width = 518
      Height = 51
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 456
        Height = 31
        Caption = 'Cadastro de Centros de Custos - N'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 456
        Height = 31
        Caption = 'Cadastro de Centros de Custos - N'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 456
        Height = 31
        Caption = 'Cadastro de Centros de Custos - N'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 51
    Width = 779
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 775
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsCentroCusto: TDataSource
    DataSet = QrCentroCusto
    Left = 460
    Top = 209
  end
  object QrCentroCusto: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCentroCustoBeforeOpen
    AfterOpen = QrCentroCustoAfterOpen
    AfterScroll = QrCentroCustoAfterScroll
    SQL.Strings = (
      'SELECT cc1.*, cc2.Nome NO_CC2 '
      'FROM centrocusto cc1'
      'LEFT JOIN centrocust2 cc2 ON cc2.Codigo=cc1.CentroCust2'
      'WHERE cc1.Codigo > 0')
    Left = 460
    Top = 161
    object QrCentroCustoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCentroCustoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCentroCustoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCentroCustoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCentroCustoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCentroCustoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCentroCustoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCentroCustoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCentroCustoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCentroCustoPagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrCentroCustoCentroCust2: TIntegerField
      FieldName = 'CentroCust2'
    end
    object QrCentroCustoNO_CC2: TWideStringField
      FieldName = 'NO_CC2'
      Size = 50
    end
    object QrCentroCustoOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas '
      'WHERE CentroCusto=:P0'
      'ORDER BY Nome')
    Left = 180
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContasCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object QrContasRateio: TIntegerField
      FieldName = 'Rateio'
    end
    object QrContasEntidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 208
    Top = 172
  end
  object PMCentroCusto: TPopupMenu
    OnPopup = PMCentroCustoPopup
    Left = 340
    Top = 248
    object Incluicentrodecusto1: TMenuItem
      Caption = '&Inclui centro de custo'
      OnClick = Incluicentrodecusto1Click
    end
    object Alteracentrodecusto1: TMenuItem
      Caption = '&Altera centro de custo'
      OnClick = Alteracentrodecusto1Click
    end
    object Excluicentrodecusto1: TMenuItem
      Caption = '&Exclui centro de custo'
      Enabled = False
    end
  end
  object PMContas: TPopupMenu
    Left = 424
    Top = 252
    object IncluiContas1: TMenuItem
      Caption = '&Inclui Conta(s)'
      OnClick = IncluiContas1Click
    end
    object ExcluiContas1: TMenuItem
      Caption = '&Remove Conta(s)'
      OnClick = ExcluiContas1Click
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Incluicentrodecusto1
    CanIns02 = IncluiContas1
    CanUpd01 = Alteracentrodecusto1
    CanDel01 = Excluicentrodecusto1
    CanDel02 = ExcluiContas1
    Left = 260
    Top = 12
  end
  object QrCentroCust2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM centrocust2')
    Left = 620
    Top = 204
    object QrCentroCust2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCentroCust2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCentroCust2PagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrCentroCust2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCentroCust2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCentroCust2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCentroCust2UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCentroCust2UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCentroCust2AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCentroCust2Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsCentroCust2: TDataSource
    DataSet = QrCentroCust2
    Left = 680
    Top = 256
  end
end
