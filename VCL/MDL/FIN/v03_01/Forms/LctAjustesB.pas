unit LctAjustesB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, frxClass, frxDBSet, UnDmkEnums, dmkImage,
  UnInternalConsts;

type
  TFmLctAjustesB = class(TForm)
    Panel1: TPanel;
    QrEmiss: TmySQLQuery;
    Panel3: TPanel;
    QrQuit: TmySQLQuery;
    QrEmissData: TDateField;
    QrEmissCtrlQuitPg: TIntegerField;
    QrSemQuita: TmySQLQuery;
    QrSemQuitaControle: TIntegerField;
    QrSemQuitaData: TDateField;
    QrSemQuitaDescricao: TWideStringField;
    DsSemQuita: TDataSource;
    DBGrid1: TDBGrid;
    QrEmissDescricao: TWideStringField;
    QrEmissControle: TIntegerField;
    QrEmissCompensado: TDateField;
    QrEmissCarteira: TIntegerField;
    QrPgto: TmySQLQuery;
    LaResu: TLabel;
    QrSemQuitaCarteira: TIntegerField;
    QrSemQuitaCredito: TFloatField;
    QrSemQuitaDebito: TFloatField;
    QrEmissCredito: TFloatField;
    QrEmissDebito: TFloatField;
    frxDsSemQuita: TfrxDBDataset;
    frxSemQuita: TfrxReport;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    RGQuais: TRadioGroup;
    BtImprimir: TBitBtn;
    PB1: TProgressBar;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimirClick(Sender: TObject);
  private
    { Private declarations }
    F_sem_quita_: String;
  public
    { Public declarations }
  end;

  var
  FmLctAjustesB: TFmLctAjustesB;

implementation

{$R *.DFM}

uses UnMyObjects, Module, ModuleGeral, UCreate, UMySQLModule, DmkDAC_PF;


procedure TFmLctAjustesB.BtImprimirClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxSemQuita, 'Lan�amentos sem quita��o localizada');
end;

procedure TFmLctAjustesB.BtOKClick(Sender: TObject);
const
  Ativo = 1;
var
  Controle, Carteira, N: Integer;
  Data, Descricao, Total: String;
  Credito, Debito: Double;
  //
  Filial: Integer;
  TabLctA: String;
begin
  Filial  := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Filial = 0, EdEMpresa, 'Informe a empresa!') then
    Exit;
  TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  //
  N := 0;
  if MyObjects.FIC(RGQuais.ItemIndex = -1, RGQuais,
    'Informe "quais" ser�o pesquisados!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    F_sem_quita_ := UCriar.RecriaTempTableNovo(ntrttSemQuita, DmodG.QrUpdPID1, False);
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmiss, Dmod.MyDB, [
    'SELECT Data, Controle, CtrlQuitPg, ',
    'Descricao, Compensado, Carteira, ',
    'Credito, Debito ',
    'FROM ' + TabLctA,
    'WHERE Tipo=2 ',
    'AND ((Compensado>2) or (Sit>1)) ',
    Geral.ATS_If(RGQuais.ItemIndex = 1, ['AND CtrlQuitPg > 0']),
    Geral.ATS_If(RGQuais.ItemIndex = 2, ['AND CtrlQuitPg = 0']),
    '']);
    //
    PB1.Position := 0;
    PB1.Max := QrEmiss.RecordCount;
    Total := IntToStr(QrEmiss.RecordCount);
    MyObjects.Informa(LaResu, False, 'Registros n�o localizados: 0');
    QrEmiss.First;
    while not QrEmiss.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando registo ' +
      IntToStr(QrEmiss.RecNo) + ' de ' + Total);
      //
      if QrEmissCtrlQuitPg.Value > 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrQuit, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + TabLctA,
        'WHERE Controle=' + Geral.FF0(QrEmissCtrlQuitPg.Value),
        '']);
        //
        if QrQuit.RecordCount = 0 then
        begin
          Carteira  :=  QrEmissCarteira.Value;
          Controle  :=  QrEmissControle.Value;
          Data      :=  Geral.FDT(QrEmissData.Value, 1);
          Descricao :=  QrEmissDescricao.Value;
          Credito   :=  QrEmissCredito.Value;
          Debito    :=  QrEmissDebito.Value;
          //
          UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, F_sem_quita_, False, [
          'Carteira', 'Data', 'Descricao', 'Credito', 'Debito', 'Ativo'
          ], ['Controle'], [
          Carteira, Data, Descricao, Credito, Debito, Ativo
          ], [Controle], False);
          //
          N := N + 1;
          MyObjects.Informa(LaResu, False, 'Registros n�o localizados: ' +
          IntToStr(N));
        end;
      end else
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrPgto, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + TabLctA,
        'WHERE ID_Pgto=' + Geral.FF0(QrEmissControle.Value),
        '']);
        //
        if QrPgto.RecordCount = 0 then
        begin
          Carteira  :=  QrEmissCarteira.Value;
          Controle  :=  QrEmissControle.Value;
          Data      :=  Geral.FDT(QrEmissData.Value, 1);
          Descricao :=  QrEmissDescricao.Value;
          Credito   :=  QrEmissCredito.Value;
          Debito    :=  QrEmissDebito.Value;
          //
          UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, F_sem_quita_, False, [
          'Carteira', 'Data', 'Descricao', 'Credito', 'Debito', 'Ativo'
          ], ['Controle'], [
          Carteira, Data, Descricao, Credito, Debito, Ativo
          ], [Controle], False);
          //
          N := N + 1;
          MyObjects.Informa(LaResu, False, 'Registros n�o localizados: ' +
          IntToStr(N));
        end;
      end;
      //
      QrEmiss.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'Pesquisa finalizada! Total de lan�amentos pesquisados: ' + Total);
    QrSemQuita.Close;
    UnDmkDAC_PF.AbreQuery(QrSemQuita, DModG.MyPID_DB);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctAjustesB.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctAjustesB.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctAjustesB.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  CBEmpresa.ListSource := DModG.DsEmpresas;
 //
  QrSemQuita.Close;
  QrSemQuita.DataBase := DModG.MyPID_DB;
  try
    UnDmkDAC_PF.AbreQuery(QrSemQuita, DModG.MyPID_DB);
  except
    ; // Nada
  end;
end;

procedure TFmLctAjustesB.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
