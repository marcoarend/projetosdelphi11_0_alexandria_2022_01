unit CarteirasU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Variants, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkGeral,
  dmkImage, UnDmkEnums;

type
  TFmCarteirasU = class(TForm)
    Panel1: TPanel;
    EdUsuario: TdmkEditCB;
    CBUsuario: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrSenhas: TmySQLQuery;
    DsSenhas: TDataSource;
    QrSenhasLogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenSenhas();
  public
    { Public declarations }
  end;

  var
  FmCarteirasU: TFmCarteirasU;

implementation

uses UnMyObjects, Module, Carteiras, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmCarteirasU.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Usuario: Integer;
begin
  Usuario := Geral.IMV(EdUsuario.Text);
  if Usuario = 0 then
  begin
    Geral.MB_Aviso('Informe o usu�rio!');
    EdUsuario.SetFocus;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT *');
  Dmod.QrAux.SQL.Add('FROM carteirasu');
  Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
  Dmod.QrAux.SQL.Add('AND Usuario=:P1');
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Geral.MB_Aviso('O usu�rio "' + CBUsuario.Text + '" j� est� ' +
    'cadastrado para a carteira "' + FmCarteiras.QrCarteirasNome.Value + '".');
    Screen.Cursor := crDefault;
    Exit;
  end;
  Codigo   := FmCarteiras.QrCarteirasCodigo.Value;
  Controle := UMyMod.BuscaEmLivreY_Def('carteirasu', 'Controle', stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'carteirasu', False, [
    'Codigo', 'Usuario'], ['Controle'], [
    Codigo, Usuario], [Controle], True) then
  begin
    FmCarteiras.ReopenCarteirasU(Controle);
    if CkContinuar.Checked then
    begin
      EdUsuario.ValueVariant := 0;
      CBUsuario.KeyValue     := Null;
      //
      EdUsuario.SetFocus;
      ReopenSenhas();
    end else Close;
  end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCarteirasU.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCarteirasU.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCarteirasU.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenSenhas();
end;

procedure TFmCarteirasU.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCarteirasU.ReopenSenhas();
begin
  QrSenhas.Close;
  QrSenhas.Params[0].AsInteger := FmCarteiras.QrCarteirasCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSenhas, Dmod.MyDB);
end;

end.

