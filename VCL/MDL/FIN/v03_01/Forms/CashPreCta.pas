unit CashPreCta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, frxClass, frxDBSet,
  DB, mySQLDbTables, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkCheckBox, ComCtrls, dmkDBGridDAC, Mask, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmCashPreCta = class(TForm)
    Panel1: TPanel;
    frxDsEmp: TfrxDBDataset;
    Panel6: TPanel;
    CkNaoAgruparNada: TCheckBox;
    GroupBox4: TGroupBox;
    CkAcordos: TCheckBox;
    CkPeriodos: TCheckBox;
    CkTextos: TCheckBox;
    Panel3: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    LaAno: TLabel;
    LaMes: TLabel;
    CBMesIni: TComboBox;
    CBAnoIni: TComboBox;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    CBMesFim: TComboBox;
    CBAnoFim: TComboBox;
    QrExclusivos: TmySQLQuery;
    QrExclusivosMovim: TFloatField;
    QrExclusivosCREDITO: TFloatField;
    QrExclusivosDEBITO: TFloatField;
    QrExclusivosANTERIOR: TFloatField;
    frxDsExclusivos: TfrxDBDataset;
    QrOrdinarios: TmySQLQuery;
    QrOrdinariosMovim: TFloatField;
    QrOrdinariosCREDITO: TFloatField;
    QrOrdinariosDEBITO: TFloatField;
    QrOrdinariosANTERIOR: TFloatField;
    frxDsOrdinarios: TfrxDBDataset;
    QrOrdiMov: TmySQLQuery;
    QrOrdiMovCredito: TFloatField;
    QrOrdiMovDebito: TFloatField;
    QrExclMov: TmySQLQuery;
    QrExclMovCredito: TFloatField;
    QrExclMovDebito: TFloatField;
    frxPreCta: TfrxReport;
    QrCtasNiv: TmySQLQuery;
    DsCtasNiv: TDataSource;
    QrCtasNivNOMENIVEL: TWideStringField;
    QrCtasNivNOMEGENERO: TWideStringField;
    QrCtasNivNivel: TIntegerField;
    QrCtasNivGenero: TIntegerField;
    QrAnt: TmySQLQuery;
    QrMov: TmySQLQuery;
    QrAntSDOANT: TFloatField;
    QrMovCredito: TFloatField;
    QrMovDebito: TFloatField;
    QrSdoNivCtas: TmySQLQuery;
    frxDsSdoNivCtas: TfrxDBDataset;
    QrSdoNivCtasNomeNivel: TWideStringField;
    QrSdoNivCtasNivel: TIntegerField;
    QrSdoNivCtasNomeGenero: TWideStringField;
    QrSdoNivCtasGenero: TIntegerField;
    QrSdoNivCtasSdoIni: TFloatField;
    QrSdoNivCtasMovCre: TFloatField;
    QrSdoNivCtasMovDeb: TFloatField;
    QrSdoNivCtasSdoFim: TFloatField;
    QrSdoNivCtasAtivo: TIntegerField;
    QrSTCPa: TmySQLQuery;
    frxDsSTCPa: TfrxDBDataset;
    QrSTCPd: TmySQLQuery;
    frxDsSTCPd: TfrxDBDataset;
    QrSTCPf: TmySQLQuery;
    frxDsSTCPf: TfrxDBDataset;
    QrSTCPdSUMCRE: TFloatField;
    QrSTCPdSUMDEB: TFloatField;
    QrSTCPfSDOFIM: TFloatField;
    QrSTCPaSDOANT: TFloatField;
    QrSTCPdSUMMOV: TFloatField;
    QrInad: TmySQLQuery;
    QrInadCREDITO: TFloatField;
    frxDsInad: TfrxDBDataset;
    CkPaginar: TdmkCheckBox;
    Label4: TLabel;
    Label5: TLabel;
    EdReceVencer: TdmkEdit;
    QrCtasNivAtivo: TSmallintField;
    QrFutDeb: TmySQLQuery;
    QrCNS: TmySQLQuery;
    QrCNSNivel: TIntegerField;
    QrCNSGenero: TIntegerField;
    QrCNA: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    DsFutDeb: TDataSource;
    QrFutDebData: TDateField;
    QrFutDebControle: TIntegerField;
    QrFutDebGenero: TIntegerField;
    QrFutDebDescricao: TWideStringField;
    QrFutDebNotaFiscal: TIntegerField;
    QrFutDebDebito: TFloatField;
    QrFutDebVencimento: TDateField;
    QrFutDebCarteira: TIntegerField;
    QrFutDebDocumento: TFloatField;
    QrFutDebFornecedor: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label6: TLabel;
    DBGContasNiv: TdmkDBGridDAC;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    Panel4: TPanel;
    QrSumDeb: TmySQLQuery;
    DsSumDeb: TDataSource;
    QrSumDebDEBITO: TFloatField;
    QrArreBaI: TmySQLQuery;
    QrArreBaIValor: TFloatField;
    QrArreBaIPercent: TFloatField;
    QrArreBaIFator: TSmallintField;
    TabSheet3: TTabSheet;
    DBGrid2: TDBGrid;
    DsArreBaI: TDataSource;
    QrArreBaIParcelasRestantes: TLargeintField;
    QrArreBaIDoQue: TSmallintField;
    QrArreBaIValFracAsk: TSmallintField;
    QrArreBaITexto: TWideStringField;
    QrArreBaIValFracDef: TFloatField;
    QrArreBaINome: TWideStringField;
    QrArreBaIVAL_RESTANTE: TFloatField;
    EdDebiVencer: TdmkEdit;
    QrPagantes: TmySQLQuery;
    QrPagantesITENS: TLargeintField;
    QrDebitos: TmySQLQuery;
    QrDebitosCOMPENSADO_TXT: TWideStringField;
    QrDebitosDATA: TWideStringField;
    QrDebitosDescricao: TWideStringField;
    QrDebitosDEBITO: TFloatField;
    QrDebitosNOTAFISCAL: TLargeintField;
    QrDebitosSERIECH: TWideStringField;
    QrDebitosDOCUMENTO: TFloatField;
    QrDebitosMEZ: TLargeintField;
    QrDebitosCompensado: TDateField;
    QrDebitosNOMECON: TWideStringField;
    QrDebitosNOMESGR: TWideStringField;
    QrDebitosNOMEGRU: TWideStringField;
    QrDebitosITENS: TLargeintField;
    QrDebitosMES: TWideStringField;
    QrDebitosSERIE_DOC: TWideStringField;
    QrDebitosNF_TXT: TWideStringField;
    QrDebitosMES2: TWideStringField;
    QrDebitosControle: TIntegerField;
    QrDebitosSub: TSmallintField;
    QrDebitosCarteira: TIntegerField;
    QrDebitosCartao: TIntegerField;
    QrDebitosVencimento: TDateField;
    QrDebitosSit: TIntegerField;
    QrDebitosGenero: TIntegerField;
    QrDebitosTipo: TSmallintField;
    DsDebitos: TDataSource;
    frxDsDebitos: TfrxDBDataset;
    frxDsCreditos: TfrxDBDataset;
    DsCreditos: TDataSource;
    QrCreditos: TmySQLQuery;
    QrCreditosMez: TIntegerField;
    QrCreditosCredito: TFloatField;
    QrCreditosNOMECON: TWideStringField;
    QrCreditosNOMESGR: TWideStringField;
    QrCreditosNOMEGRU: TWideStringField;
    QrCreditosMES: TWideStringField;
    QrCreditosNOMECON_2: TWideStringField;
    QrCreditosSubPgto1: TIntegerField;
    QrCreditosControle: TIntegerField;
    QrCreditosSub: TSmallintField;
    QrCreditosCarteira: TIntegerField;
    QrCreditosCartao: TIntegerField;
    QrCreditosVencimento: TDateField;
    QrCreditosCompensado: TDateField;
    QrCreditosSit: TIntegerField;
    QrCreditosGenero: TIntegerField;
    QrCreditosTipo: TSmallintField;
    QrSaldoA: TmySQLQuery;
    QrSaldoAInicial: TFloatField;
    QrSaldoASALDO: TFloatField;
    QrSaldoATOTAL: TFloatField;
    frxDsSaldoA: TfrxDBDataset;
    QrResumo: TmySQLQuery;
    QrResumoCredito: TFloatField;
    QrResumoDebito: TFloatField;
    QrResumoSALDO: TFloatField;
    QrResumoFINAL: TFloatField;
    frxDsResumo: TfrxDBDataset;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtImprime: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrExclusivosCalcFields(DataSet: TDataSet);
    procedure QrOrdinariosCalcFields(DataSet: TDataSet);
    procedure frxPreCtaGetValue(const VarName: string; var Value: Variant);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdEmpresaExit(Sender: TObject);
    procedure QrArreBaICalcFields(DataSet: TDataSet);
    procedure CBMesIniChange(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrResumoCalcFields(DataSet: TDataSet);
    procedure QrSaldoACalcFields(DataSet: TDataSet);
    procedure QrDebitosCalcFields(DataSet: TDataSet);
    procedure QrCreditosCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FDtIni, FDtEncer, FDtMorto: TDateTime;
    FEntidade, FPeriodoIni, FPeriodoFim, FCliInt(*, FGenero, FUH*): Integer;
    FCliInt_TXT, FCtasNiv, FSdoNivCtas, FDataI1, FDataF1, FDataD1,
    FTabAriA, FTabCnsA, FTabPriA, FTabPrvA,
    FTabLctA, FTabLctB, FTabLctD(*, FTabLctX*): String;
    procedure DemostrativoDeReceitasEDespesas();
    procedure ReopenNiveisCtas();
    procedure SQLDebitosFuturos(Query: TmySQLQuery; SQL_Zero: String);
    function ValorArrecadacaoFutura(): Double;
    procedure FechaPesquisa();
    procedure ReopenCreditosEDebitos(Entidade: Integer; DataI, DataF: String;
              Acordos, Periodos, Textos, NaoAgruparNada: Boolean);
  public
    { Public declarations }
  end;

  var
  FmCashPreCta: TFmCashPreCta;

implementation

uses UnMyObjects, ModuleGeral, Module, ModuleFin, UnInternalConsts, dmkGeral,
UCreate, UMySQLModule, GetValor, UnFinanceiro, DmkDAC_PF;

{$R *.DFM}

procedure TFmCashPreCta.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxPreCta, 'Presta��o de contas');
end;

procedure TFmCashPreCta.BtOKClick(Sender: TObject);
var
  MezIni, AnoIni, MesIni, MezFim, AnoFim, MesFim: Integer;
  Tab: String;
  //
  NomeNivel, NomeGenero, PeriF_TXT: String;
  Nivel, Genero: Integer;
  //DebiVencer,
  SdoIni, MovCre, MovDeb, SdoFim, ReceVencer: Double;
begin
  if EdEmpresa.ValueVariant = 0 then
  begin
    Geral.MB_Aviso('Informe o cliente interno!');
    EdEmpresa.SetFocus;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    FPeriodoIni := ((Geral.IMV(CBAnoIni.Text)-2000) * 12) + CBMesIni.ItemIndex + 1;
    FPeriodoFim := ((Geral.IMV(CBAnoFim.Text)-2000) * 12) + CBMesFim.ItemIndex + 1;
    PeriF_TXT   := FormatFloat('0', FPeriodoFim);
    //
    FCliInt   := EdEmpresa.ValueVariant;
    FEntidade := DModG.QrEmpresasCodigo.Value;
    FCliInt_TXT := FormatFloat('0', FCliInt);
    FDtIni  := dmkPF.PrimeiroDiaDoPeriodo_Date(FPeriodoIni);
    FDataI1 := Geral.FDT(dmkPF.PrimeiroDiaDoPeriodo_Date(FPeriodoIni), 1);
    FDataF1 := Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoFim), 1);
    FDataD1 := Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoFim)+1, 1);
    AnoIni := Geral.IMV(CBAnoIni.Text);
    MesIni := CBMesIni.ItemIndex + 1;
    MezIni := ((AnoIni - 2000) * 100) + MesIni;
    //
    AnoFim := Geral.IMV(CBAnoFim.Text);
    MesFim := CBMesFim.ItemIndex + 1;
    MezFim := ((AnoFim - 2000) * 100) + MesFim;
    //
    DModG.Def_EM_ABD(TMeuDB,
      FEntidade, FCliInt, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
    //
    FTabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, FCliInt);
    FTabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, FCliInt);
    FTabPriA := DModG.NomeTab(TMeuDB, ntPri, False, ttA, FCliInt);
    FTabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, FCliInt);
    //
    DmodFin.AtualizaContasHistSdo3(FEntidade, LaAviso1, LaAviso2,
      DModG.MezLastEncerr(), FTabLctA);
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;
    //
    //
    // Colocar no DModFin ???

    FSdoNivCtas := UCriar.RecriaTempTableNovo(ntrttSdoNivCtas, DmodG.QrUpdPID1, False, 0, 'SdoNivCtas');
    {
    QrContasNiv.Close;
    QrContasNiv.Params[0].AsInteger := FCliInt;
    UnDmkDAC_PF.AbreQuery(QrContasNiv, Dmod.MyDB);
    }
    QrCtasNiv.First;
    while not QrCtasNiv.Eof do
    begin
      case QrCtasNivNivel.Value of
        1: Tab := 'Genero';
        2: Tab := 'SubGrupo';
        3: Tab := 'Grupo';
        4: Tab := 'Conjunto';
        5: Tab := 'Plano';
        else Tab := '???';
      end;
      QrAnt.Close;
      QrAnt.SQL.Clear;
      QrAnt.SQL.Add('SELECT SUM(Movim) SDOANT');
      QrAnt.SQL.Add('FROM contasmov');
      QrAnt.SQL.Add('WHERE CliInt=:P0');
      QrAnt.SQL.Add('AND Mez<:P1');
      QrAnt.SQL.Add('AND '+Tab+'=:P2');
      QrAnt.SQL.Add('GROUP BY '+Tab);
  {
  SELECT Conjunto, SUM(Movim) SDOANT
  FROM contasmov
  WHERE CliInt=1751
  AND Mez<1004
  AND Conjunto=1
  GROUP BY Conjunto
  }
      QrAnt.Params[00].AsInteger := FCliInt;
      QrAnt.Params[01].AsInteger := MezIni;
      QrAnt.Params[02].AsInteger := QrCtasNivGenero.Value;
      UnDmkDAC_PF.AbreQuery(QrAnt, Dmod.MyDB);
      //
      QrMov.Close;
      QrMov.SQL.Clear;
      QrMov.SQL.Add('SELECT SUM(Credito) Credito, SUM(Debito) Debito ');
      QrMov.SQL.Add('FROM contasmov');
      QrMov.SQL.Add('WHERE CliInt=:P0');
      QrMov.SQL.Add('AND Mez BETWEEN :P1 AND :P2');
      QrMov.SQL.Add('AND '+Tab+'=:P3');
      QrMov.SQL.Add('GROUP BY '+Tab);
      QrMov.Params[00].AsInteger := FCliInt;
      QrMov.Params[01].AsInteger := MezIni;
      QrMov.Params[02].AsInteger := MezFim;
      QrMov.Params[03].AsInteger := QrCtasNivGenero.Value;
      UnDmkDAC_PF.AbreQuery(QrMov, Dmod.MyDB);
      //
      NomeNivel   := QrCtasNivNOMENIVEL.Value;
      Nivel       := QrCtasNivNivel.Value;
      NomeGenero  := QrCtasNivNOMEGENERO.Value;
      Genero      := QrCtasNivGenero.Value;
      SdoIni      := QrAntSDOANT.Value;
      MovCre      := QrMovCredito.Value;
      MovDeb      := QrMovDebito.Value;
      SdoFim      := QrAntSDOANT.Value + QrMovCredito.Value - QrMovDebito.Value;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSdoNivCtas, False, [
      'NomeNivel', 'Nivel', 'NomeGenero',
      'Genero', 'SdoIni', 'MovCre',
      'MovDeb', 'SdoFim'], [
      ], [
      NomeNivel, Nivel, NomeGenero,
      Genero, SdoIni, MovCre,
      MovDeb, SdoFim], [
      ], False);
      //
      QrCtasNiv.Next;
    end;
    //
    QrSdoNivCtas.Close;
    QrSdoNivCtas.Database := DModG.MyPID_DB;
    UnDmkDAC_PF.AbreQuery(QrSdoNivCtas, DModG.MyPID_DB);
    // informar o saldo total mesmo que n�o informe os saldos dos n�veis
    DModFin.QrSTCP.Close;
    DModFin.QrSTCP.Params[0].AsInteger := FCliInt;
    DModFin.QrSTCP.Params[1].AsInteger := FPeriodoIni;
    UnDmkDAC_PF.AbreQuery(DModFin.QrSTCP, Dmod.MyDB);
    //
    // SOMAS DE VALORES DE TODAS CONTAS
    // saldo anterior
    QrSTCPa.Close;
    QrSTCPa.Params[00].AsInteger := FEntidade;
    QrSTCPa.Params[01].AsInteger := MezIni;
    UnDmkDAC_PF.AbreQuery(QrSTCPa, Dmod.MyDB);
    // Movimento durante o per�odo selecionado
    QrSTCPd.Close;
    QrSTCPd.Params[00].AsInteger := FEntidade;
    QrSTCPd.Params[01].AsInteger := MezIni;
    QrSTCPd.Params[02].AsInteger := MezFim;
    UnDmkDAC_PF.AbreQuery(QrSTCPd, Dmod.MyDB);
    // saldo final
    QrSTCPf.Close;
    QrSTCPf.Params[00].AsInteger := FEntidade;
    QrSTCPf.Params[01].AsInteger := MezFim;
    UnDmkDAC_PF.AbreQuery(QrSTCPf, Dmod.MyDB);
    // FIM SOMAS DE VALORES DE TODAS CONTAS
    //
    // Inadimpl�ncia
    QrInad.Close;
    QrInad.SQL.Clear;
{
SELECT SUM(lan.Credito) CREDITO
FROM lan ctos lan
WHERE lan.FatID in (600,601,610)
AND lan.Reparcel=0
AND lan.Tipo=2
AND lan.CliInt=:P0
AND lan.Vencimento  < :P1
AND
          (lan.Compensado = 0
          OR
          lan.Compensado>= :P2)
}
    QrInad.SQL.Add('SELECT SUM(lan.Credito) CREDITO');
    QrInad.SQL.Add('FROM ' + FTabLctA + ' lan');
    QrInad.SQL.Add('WHERE lan.FatID in (600,601,610)');
    QrInad.SQL.Add('AND lan.Reparcel=0');
    QrInad.SQL.Add('AND lan.Tipo=2');
    QrInad.SQL.Add('AND lan.CliInt=:P0');
    QrInad.SQL.Add('AND lan.Vencimento  < :P1');
    QrInad.SQL.Add('AND');
    QrInad.SQL.Add('          (lan.Compensado = 0');
    QrInad.SQL.Add('          OR');
    QrInad.SQL.Add('          lan.Compensado>= :P2)');
    QrInad.Params[00].AsInteger := FCliInt;
    QrInad.Params[01].AsString  := FDataD1;
    QrInad.Params[02].AsString  := FDataD1;
    UnDmkDAC_PF.AbreQuery(QrInad, Dmod.MyDB);
    // Fim inadimpl�ncia
    //
    QrFutDeb.Close;
    QrFutDeb.SQL.Clear;
    QrFutDeb.SQL.Add('SELECT lan.Data, lan.Controle, lan.Genero,');
    QrFutDeb.SQL.Add('lan.Descricao, lan.NotaFiscal, lan.Debito,');
    QrFutDeb.SQL.Add('lan.Vencimento, lan.Carteira, lan.Documento,');
    QrFutDeb.SQL.Add('lan.Fornecedor');
    QrFutDeb.SQL.Add('FROM ' + FTabLctA + ' lan');
    QrFutDeb.SQL.Add('LEFT JOIN contas cta ON cta.Codigo=lan.Genero');
    QrFutDeb.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
    QrFutDeb.SQL.Add('LEFT JOIN grupos gru ON gru.Codigo=sgr.Grupo');
    QrFutDeb.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrFutDeb.SQL.Add('LEFT JOIN plano pla ON pla.Codigo=cjt.Plano');
    QrFutDeb.SQL.Add('WHERE NOT (lan.FatID in (600,601,610))');
    QrFutDeb.SQL.Add('AND lan.Tipo=2');
    QrFutDeb.SQL.Add('AND lan.CliInt=' + FCliInt_TXT);
    QrFutDeb.SQL.Add('AND (lan.Compensado<2');
    QrFutDeb.SQL.Add('  OR lan.Compensado>="'+FDataF1+'")');
    //
    // n�veis variados a serem pesquisados:
    SQLDebitosFuturos(QrFutDeb, 'AND lan.Controle=0');
    QrFutDeb.SQL.Add('ORDER BY lan.Vencimento, lan.Data');
    UnDmkDAC_PF.AbreQuery(QrFutDeb, Dmod.MyDB);
    //
    QrSumDeb.Close;
    QrSumDeb.SQL.Clear;
    QrSumDeb.SQL.Add('SELECT SUM(lan.Debito) DEBITO');
    QrSumDeb.SQL.Add('FROM ' + FTabLctA + ' lan');
    QrSumDeb.SQL.Add('LEFT JOIN contas cta ON cta.Codigo=lan.Genero');
    QrSumDeb.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
    QrSumDeb.SQL.Add('LEFT JOIN grupos gru ON gru.Codigo=sgr.Grupo');
    QrSumDeb.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrSumDeb.SQL.Add('LEFT JOIN plano pla ON pla.Codigo=cjt.Plano');
    QrSumDeb.SQL.Add('WHERE NOT (lan.FatID in (600,601,610))');
    QrSumDeb.SQL.Add('AND lan.Tipo=2');
    QrSumDeb.SQL.Add('AND lan.CliInt=' + FCliInt_TXT);
    QrSumDeb.SQL.Add('AND (lan.Compensado<2');
    QrSumDeb.SQL.Add('  OR lan.Compensado>="'+FDataF1+'")');
    SQLDebitosFuturos(QrSumDeb, 'AND lan.Controle=0');
    UnDmkDAC_PF.AbreQuery(QrSumDeb, Dmod.MyDB);
    EdDebiVencer.ValueVariant := QrSumDebDEBITO.Value;
    // Fim n�veis variados a serem pesquisados.
    //
    if VAR_KIND_DEPTO = kdUH then
    begin
      // Arrecada��es Futuras!
      QrArreBaI.Close;
      QrArreBaI.SQL.Clear;
      QrArreBaI.SQL.Add('SELECT ((bai.ParcPerF - '+PeriF_TXT+') + 1) ParcelasRestantes,');
      QrArreBaI.SQL.Add('bai.Valor, bai.Percent, bai.Fator, bai.DoQue, ');
      QrArreBaI.SQL.Add('bai.ValFracAsk, bai.ValFracDef, bai.Texto, bac.Nome');
      QrArreBaI.SQL.Add('FROM arrebai bai');
      QrArreBaI.SQL.Add('LEFT JOIN arrebac bac ON bac.Codigo=bai.Codigo');
      QrArreBaI.SQL.Add('LEFT JOIN contas cta ON cta.Codigo=bac.Conta');
      QrArreBaI.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
      QrArreBaI.SQL.Add('LEFT JOIN grupos gru ON gru.Codigo=sgr.Grupo');
      QrArreBaI.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
      QrArreBaI.SQL.Add('LEFT JOIN plano pla ON pla.Codigo=cjt.Plano');
      QrArreBaI.SQL.Add('WHERE bai.Cond=' + FormatFloat('0', EdEmpresa.ValueVariant));
      QrArreBaI.SQL.Add('AND (/*(SitCobr=1) OR*/ (SitCobr=2 AND '+PeriF_TXT);
      QrArreBaI.SQL.Add(' BETWEEN bai.ParcPerI AND bai.ParcPerF))');
      // n�o usar porque � mais complexa a situa��o
      //QrArreBaI.SQL.Add('AND bai.DoQue > 2'); // DoQue in (0,1,2) se desconhece o valor!
      SQLDebitosFuturos(QrArreBaI, 'AND bai.Controle=0');
      UnDmkDAC_PF.AbreQuery(QrArreBaI, Dmod.MyDB);
      ReceVencer := 0;
      while not QrArreBaI.Eof do
      begin
        ReceVencer := ReceVencer + QrArreBaIVAL_RESTANTE.Value;
        //
        QrArreBaI.Next;
      end;
      EdReceVencer.ValueVariant := ReceVencer;
    end;
    //
    // Fim do c�lculo (colocar  no DMod.Fin).
    //
    //
    QrExclMov.Close;
    QrExclMov.Params[00].AsInteger := FEntidade;
    QrExclMov.Params[01].AsInteger := MezIni;
    QrExclMov.Params[02].AsInteger := MezFim;
    UnDmkDAC_PF.AbreQuery(QrExclMov, Dmod.MyDB);
    //
    QrExclusivos.Close;
    QrExclusivos.Params[00].AsInteger := FEntidade;
    QrExclusivos.Params[01].AsInteger := MezFim;
    UnDmkDAC_PF.AbreQuery(QrExclusivos, Dmod.MyDB);
    //
    QrOrdiMov.Close;
    QrOrdiMov.Params[00].AsInteger := FEntidade;
    QrOrdiMov.Params[01].AsInteger := MezIni;
    QrOrdiMov.Params[02].AsInteger := MezFim;
    UnDmkDAC_PF.AbreQuery(QrOrdiMov, Dmod.MyDB);
    //
    QrOrdinarios.Close;
    QrOrdinarios.Params[00].AsInteger := FEntidade;
    QrOrdinarios.Params[01].AsInteger := MezFim;
    UnDmkDAC_PF.AbreQuery(QrOrdinarios, Dmod.MyDB);
    //
    DemostrativoDeReceitasEDespesas();
    //
    BtImprime.Enabled := True;
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCashPreCta.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCashPreCta.CBMesIniChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmCashPreCta.DemostrativoDeReceitasEDespesas();
  procedure GeraParteSQL_Resumo(TabLct: String);
  begin
    QrResumo.SQL.Add('SELECT SUM(lct.Credito) Credito, -SUM(lct.Debito) Debito');
    QrResumo.SQL.Add('FROM ' + TabLct + ' lct');
    QrResumo.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrResumo.SQL.Add('WHERE lct.Tipo <> 2');
    QrResumo.SQL.Add('AND lct.Genero > 0');
    QrResumo.SQL.Add('AND car.ForneceI=' + FormatFloat('0', FEntidade));
    QrResumo.SQL.Add('AND lct.Data BETWEEN "' + FDataI1 + '" AND "' + FDataF1 + '"');
    QrResumo.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
  end;
var
  FldIni, TabIni: String;
begin
  ReopenCreditosEDebitos(FEntidade, FDataI1, FDataF1, CkAcordos.Checked,
  CkPeriodos.Checked, CkTextos.Checked, CkNaoAgruparNada.Checked);
  Screen.Cursor := crHourGlass;
  //
  FldIni := UFinanceiro.DefLctFldSdoIni(FDtIni, FDtEncer, FDtMorto);
  TabIni := UFinanceiro.DefLctTab(FDtIni, FDtEncer, FDtMorto,
            FTabLcta, FTabLctB, FTabLctD);
  //
  //
  QrSaldoA.SQL.Clear;
  QrSaldoA.SQL.Add('SELECT SUM(' + FldIni + ') Inicial,');
  QrSaldoA.SQL.Add('(');
  QrSaldoA.SQL.Add('  SELECT SUM(lct.Credito-lct.Debito)');
  QrSaldoA.SQL.Add('  FROM ' + TabIni + ' lct');
  QrSaldoA.SQL.Add('  LEFT JOIN carteiras crt ON crt.Codigo=lct.Carteira');
  QrSaldoA.SQL.Add('  WHERE crt.Tipo <> 2');
  QrSaldoA.SQL.Add('  AND crt.ForneceI=' + FormatFloat('0', FEntidade));
  QrSaldoA.SQL.Add('  AND lct.Data < "' + FDataI1 + '"');
  QrSaldoA.SQL.Add(') SALDO');
  QrSaldoA.SQL.Add('FROM carteiras car');
  QrSaldoA.SQL.Add('WHERE car.ForneceI=' + FormatFloat('0', FEntidade));
  QrSaldoA.SQL.Add('AND car.Tipo <> 2');
  UnDmkDAC_PF.AbreQuery(QrSaldoA, Dmod.MyDB);

  // Deve ser ap�s SaldoA
  QrResumo.Close;
  QrResumo.SQL.Clear;
  QrResumo.SQL.Add('DROP TABLE IF EXISTS _FIN_BALAN_003_01_RESU_;');
  QrResumo.SQL.Add('CREATE TABLE _FIN_BALAN_003_01_RESU_ IGNORE ');
  QrResumo.SQL.Add('');
  GeraParteSQL_Resumo(FTabLctA);
  QrResumo.SQL.Add('UNION');
  GeraParteSQL_Resumo(FTabLctB);
  QrResumo.SQL.Add('UNION');
  GeraParteSQL_Resumo(FTabLctD);
  QrResumo.SQL.Add(';');
  QrResumo.SQL.Add('');
  QrResumo.SQL.Add('SELECT SUM(Credito) Credito, -SUM(Debito) Debito ');
  QrResumo.SQL.Add('FROM _FIN_BALAN_003_01_RESU_;');
  QrResumo.SQL.Add('');
  // N�o pode !!
  //QrResumo.SQL.Add('DROP TABLE IF EXISTS _FIN_BALAN_003_01_RESU_;');
  QrResumo.SQL.Add('');
  //
  UMyMod.AbreQuery(QrResumo, DModG.MyPID_DB, 'TFmCashPreCta.DemostrativoDeReceitasEDespesas()');
  //
end;

procedure TFmCashPreCta.EdEmpresaChange(Sender: TObject);
begin
  if not EdEmpresa.Focused then
    ReopenNiveisCtas();
end;

procedure TFmCashPreCta.EdEmpresaExit(Sender: TObject);
begin
  ReopenNiveisCtas();
end;

procedure TFmCashPreCta.FechaPesquisa;
begin
  BtImprime.Enabled := False;
  QrFutDeb.Close;
  QrArreBai.Close;
end;

procedure TFmCashPreCta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCashPreCta.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  //
  QrSdoNivCtas.Close;
  QrSdoNivCtas.Database := DModG.MyPID_DB;
  //
  QrCtasNiv.Close;
  QrCtasNiv.Database := DModG.MyPID_DB;
  //
  QrCNS.Close;
  QrCNS.Database := DModG.MyPID_DB;
  //
  QrCNA.Close;
  QrCNA.Database := DModG.MyPID_DB;
  //
  frxDsEmp.DataSet := DModG.QrEmpresas;
  //
  MyObjects.PreencheCBAnoECBMes(CBAnoIni, CBMesIni, -7);
  MyObjects.PreencheCBAnoECBMes(CBAnoFim, CBMesFim, -1);
  //
  QrCreditos.Close;
  QrCreditos.Database := DModG.MyPID_DB;
  //
  QrDebitos.Close;
  QrDebitos.Database := DModG.MyPID_DB;
  //
  QrResumo.Close;
  QrResumo.Database := DModG.MyPID_DB;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmCashPreCta.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCashPreCta.frxPreCtaGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6,
    dmkPF.PrimeiroDiaDoPeriodo_Date(FPeriodoIni)) + ' at� ' +
    FormatDateTime(VAR_FORMATDATE6,
    dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoFim))
  else if VarName = 'VARF_PAGINAR' then
    Value := CkPaginar.Checked
  else if AnsiCompareText(VarName, 'VAR_DATA_PER_ANT') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2,
    dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoIni-1))
  else if AnsiCompareText(VarName, 'VAR_DATA_PER_FIM') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2,
    dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoFim))
  else if AnsiCompareText(VarName, 'VARF_DESP_VENCER') = 0 then Value :=
    - EdDebiVencer.ValueVariant
  else if AnsiCompareText(VarName, 'VARF_RECE_VENCER') = 0 then Value :=
    EdReceVencer.ValueVariant
  {
  else if VarName = 'VARF_PERIODO_TXT' then
    Value := 'FAZER PERIODO_TXT!'//Geral.Maiusculas(MyObjects.CBAnoECBMesToPeriodoTxt(CBAno, CBMes), False)
  else if AnsiCompareText(VarName, 'VARF_ULTIMODIA') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE3,
    dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoFim))
  else if AnsiCompareText(VarName, 'MES_ANO ') = 0 then Value :=
    dmkPF.MesEAnoDoPeriodoLongo(FPeriodoAtu)
  else if AnsiCompareText(VarName, 'CIDADE  ') = 0 then Value :=
    QrEmpCIDADE.Value
  else if AnsiCompareText(VarName, 'SIGLA_UF') = 0 then Value :=
    QrEmpNO_UF.Value
  else if AnsiCompareText(VarName, 'DATA_ATU') = 0 then Value :=
    Geral.FDT(Date, 6)
  else if AnsiCompareText(VarName, 'DATA_FIM') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6,
    dmkPF.UltimoDiaDoPeriodo_Date(FPeriodoFim))
  else if Copy(VarName, 1, 5) = 'VMES_' then
    Value := Copy(VarName, 6) + ' / ' + CBAno.Text
  else if VarName = 'VARF_ANO1' then
    Value := 'Total ' + CBAno.Text
  else if VarName = 'VARF_ANO0' then
    Value := 'Total ' + FormatFloat('0', Geral.IMV(CBAno.Text) -1)
  else if VarName = 'VARF_MID1' then
    Value := 'M�dia ' + CBAno.Text
  else if VarName = 'VARF_MID0' then
    Value := 'M�dia ' + FormatFloat('0', Geral.IMV(CBAno.Text) -1)
  else if VarName = 'LogoBalanceteExiste' then
    Value := FileExists(QrEntiCfgRel_01LogoPath.Value)
  else if VarName = 'LogoBalancetePath' then
    Value := QrEntiCfgRel_01LogoPath.Value
  else if VarName = 'VARF_EMPRESA' then
    Value := CBEmpresa.Text
  else if VarName = 'VARF_RESUMIDO_01' then Value := FResumido_01
  else if VarName = 'VARF_RESUMIDO_02' then Value := FResumido_02
  else if VarName = 'VARF_RESUMIDO_03' then Value := FResumido_03
  else if VarName = 'VARF_RESUMIDO_04' then Value := FResumido_04
  else if VarName = 'VARF_RESUMIDO_05' then Value := FResumido_05
  else if VarName = 'VARF_RESUMIDO_06' then Value := FResumido_06
  }
end;

procedure TFmCashPreCta.QrArreBaICalcFields(DataSet: TDataSet);
begin
  QrArreBaIVAL_RESTANTE.Value := ValorArrecadacaoFutura();
end;

procedure TFmCashPreCta.QrCreditosCalcFields(DataSet: TDataSet);
var
  SubPgto1: String;
begin
  if QrCreditosSubPgto1.Value = 0 then SubPgto1 := ''
    else SubPgto1 := ' RECEB. ACORDO EXTRAJUDICIAL';
  //
  QrCreditosNOMECON_2.Value := QrCreditosNOMECON.Value + SubPgto1;
  //
  QrCreditosMES.Value := dmkPF.MezToMesEAno(QrCreditosMez.Value);
end;

procedure TFmCashPreCta.QrDebitosCalcFields(DataSet: TDataSet);
begin
  QrDebitosMES.Value := dmkPF.MezToMesEAno(QrDebitosMez.Value);
  QrDebitosMES2.Value := dmkPF.MezToMesEAnoCurto(QrDebitosMez.Value);
  //
  QrDebitosSERIE_DOC.Value := Trim(QrDebitosSerieCH.Value);
  if QrDebitosDocumento.Value <> 0 then QrDebitosSERIE_DOC.Value :=
    QrDebitosSERIE_DOC.Value + FormatFloat('000000', QrDebitosDocumento.Value);
  //
  QrDebitosNF_TXT.Value := FormatFloat('000000;-000000; ', QrDebitosNotaFiscal.Value);
  //
end;

procedure TFmCashPreCta.QrExclusivosCalcFields(DataSet: TDataSet);
begin
  QrExclusivosCREDITO.Value := QrExclMovCredito.Value;
  QrExclusivosDEBITO.Value := QrExclMovDebito.Value;
  QrExclusivosANTERIOR.Value := QrExclusivosMovim.Value -
    QrExclMovDebito.Value + QrExclMovCredito.Value;
end;

procedure TFmCashPreCta.QrOrdinariosCalcFields(DataSet: TDataSet);
begin
  QrOrdinariosCREDITO.Value := QrOrdiMovCredito.Value;
  QrOrdinariosDEBITO.Value := QrOrdiMovDebito.Value;
  QrOrdinariosANTERIOR.Value := QrOrdinariosMovim.Value +
    QrOrdiMovDebito.Value - QrOrdiMovCredito.Value;
end;

procedure TFmCashPreCta.QrResumoCalcFields(DataSet: TDataSet);
begin
  QrResumoSALDO.Value := QrResumoCredito.Value - QrResumoDebito.Value;
  QrResumoFINAL.Value := QrResumoSALDO.Value + QrSaldoATOTAL.Value;
end;

procedure TFmCashPreCta.QrSaldoACalcFields(DataSet: TDataSet);
begin
  QrSaldoATOTAL.Value := QrSaldoAInicial.Value + QrSaldoASALDO.Value;
end;

procedure TFmCashPreCta.ReopenCreditosEDebitos(Entidade: Integer; DataI,
  DataF: String; Acordos, Periodos, Textos, NaoAgruparNada: Boolean);
  procedure GeraParteSQL_Creditos(TabLct: String);
  begin
    if NaoAgruparNada = True then
      QrCreditos.SQL.Add('SELECT lct.Mez, lct.Credito, ')
    else
      QrCreditos.SQL.Add('SELECT lct.Mez, SUM(lct.Credito) Credito, ');
    QrCreditos.SQL.Add('lct.Controle, lct.Sub, lct.Carteira, lct.Cartao, lct.Tipo,');
    QrCreditos.SQL.Add('lct.Vencimento, lct.Compensado, lct.Sit, lct.Genero, ');
    QrCreditos.SQL.Add('lct.SubPgto1, lct.Data, ');
    QrCreditos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
    QrCreditos.SQL.Add('gru.OrdemLista OL_GRU, sgr.OrdemLista OL_SGR, ');
    QrCreditos.SQL.Add('con.OrdemLista OL_CTA ');
    QrCreditos.SQL.Add('FROM ' + TabLct + ' lct');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lct.Genero');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
    QrCreditos.SQL.Add('WHERE lct.Tipo <> 2');
    QrCreditos.SQL.Add('AND lct.Credito > 0');
    QrCreditos.SQL.Add('AND lct.Genero>0');
    QrCreditos.SQL.Add('AND car.ForneceI = ' + FormatFloat('0', Entidade));
    QrCreditos.SQL.Add('AND lct.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
    QrCreditos.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
    if NaoAgruparNada = False then
    begin
      QrCreditos.SQL.Add('GROUP BY lct.Genero ');
      if Periodos then
        QrCreditos.SQL.Add(', lct.Mez');
      if Acordos then
        QrCreditos.SQL.Add(', lct.SubPgto1');
    end;
  end;
  //
  procedure GeraParteSQL_Debitos(TabLct: String);
  begin
    if Textos and (NaoAgruparNada = False) then
    begin
      QrDebitos.SQL.Add('SELECT IF(COUNT(lct.Compensado) > 1, "V�rias", IF(lct.Compensado<2,"",');
      QrDebitos.SQL.Add('DATE_FORMAT(lct.Compensado, "%d/%m/%y"))) COMPENSADO_TXT,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, "V�rias", DATE_FORMAT(lct.Data, "%d/%m/%y")) DATA,');
      QrDebitos.SQL.Add('lct.Descricao, SUM(lct.Debito) DEBITO,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.NotaFiscal) NOTAFISCAL,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, "", lct.SerieCH) SERIECH,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.Documento) DOCUMENTO,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.Mez) MEZ, lct.Compensado,');
      QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
      QrDebitos.SQL.Add('COUNT(lct.Data) ITENS, ');
    end else begin
      QrDebitos.SQL.Add('SELECT IF(lct.Compensado<2,"", DATE_FORMAT(');
      QrDebitos.SQL.Add('lct.Compensado, "%d/%m/%y")) COMPENSADO_TXT,');
      QrDebitos.SQL.Add('DATE_FORMAT(lct.Data, "%d/%m/%y") DATA, ');
      QrDebitos.SQL.Add('lct.Descricao, lct.Debito, ');
      QrDebitos.SQL.Add('IF(lct.NotaFiscal = 0, 0, lct.NotaFiscal) NOTAFISCAL,');
      QrDebitos.SQL.Add('IF(lct.SerieCH = "", "", lct.SerieCH) SERIECH,');
      QrDebitos.SQL.Add('IF(lct.Documento = 0, 0, lct.Documento) DOCUMENTO,');
      QrDebitos.SQL.Add('IF(lct.Mez = 0, 0, lct.Mez) MEZ, lct.Compensado,');
      QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
      QrDebitos.SQL.Add('0 ITENS, ');
    end;
    QrDebitos.SQL.Add('lct.Vencimento, lct.Sit, lct.Genero, lct.Tipo, ');
    QrDebitos.SQL.Add('lct.Controle, lct.Sub, lct.Carteira, lct.Cartao, ');
    QrDebitos.SQL.Add('gru.OrdemLista OL_GRU, sgr.OrdemLista OL_SGR, ');
    QrDebitos.SQL.Add('con.OrdemLista OL_CTA ');
    QrDebitos.SQL.Add('FROM ' + TabLct + ' lct');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lct.Genero');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
    QrDebitos.SQL.Add('WHERE lct.Tipo <> 2');
    QrDebitos.SQL.Add('AND lct.Debito > 0');
    QrDebitos.SQL.Add('AND lct.Genero>0');
    QrDebitos.SQL.Add('AND car.ForneceI = ' + FormatFloat('0', Entidade));
    QrDebitos.SQL.Add('AND lct.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
    QrDebitos.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
    //
    if NaoAgruparNada = False then
    begin
      if Textos then
        QrDebitos.SQL.Add('GROUP BY lct.Descricao');
    end;
  end;
begin
  //// C R � D I T O S
  QrCreditos.Close;
  QrCreditos.SQL.Clear;
  QrCreditos.SQL.Add('DROP TABLE IF EXISTS _FIN_BALAN_003_01_CRED_;');
  QrCreditos.SQL.Add('CREATE TABLE _FIN_BALAN_003_01_CRED_ IGNORE ');
  QrCreditos.SQL.Add('');
  GeraParteSQL_Creditos(FTabLctA);
  QrCreditos.SQL.Add('UNION');
  GeraParteSQL_Creditos(FTabLctB);
  QrCreditos.SQL.Add('UNION');
  GeraParteSQL_Creditos(FTabLctD);
  QrCreditos.SQL.Add(';');
  QrCreditos.SQL.Add('');
  QrCreditos.SQL.Add('SELECT * FROM _FIN_BALAN_003_01_CRED_');
  QrCreditos.SQL.Add('ORDER BY OL_GRU, NOMEGRU, OL_SGR,');
  QrCreditos.SQL.Add('NOMESGR, OL_CTA, NOMECON, Mez, Data;');
  QrCreditos.SQL.Add('');
  QrCreditos.SQL.Add('DROP TABLE IF EXISTS _FIN_BALAN_003_01_CRED_;');
  QrCreditos.SQL.Add('');
  UMyMod.AbreQuery(QrCreditos, DModG.MyPID_DB, 'TFmCashPreCta.ReopenCreditosEDebitos()');

  //

  //// D � B I T O S
  QrDebitos.Close;
  QrDebitos.SQL.Clear;
  QrDebitos.SQL.Add('DROP TABLE IF EXISTS _FIN_BALAN_003_01_DEBI_;');
  QrDebitos.SQL.Add('CREATE TABLE _FIN_BALAN_003_01_DEBI_ IGNORE ');
  QrDebitos.SQL.Add('');
  GeraParteSQL_Debitos(FTabLctA);
  QrDebitos.SQL.Add('UNION');
  GeraParteSQL_Debitos(FTabLctB);
  QrDebitos.SQL.Add('UNION');
  GeraParteSQL_Debitos(FTabLctD);
  QrDebitos.SQL.Add(';');
  QrDebitos.SQL.Add('');
  QrDebitos.SQL.Add('SELECT * FROM _FIN_BALAN_003_01_DEBI_');
  QrDebitos.SQL.Add('ORDER BY OL_GRU, NOMEGRU, OL_SGR,');
  QrDebitos.SQL.Add('NOMESGR, OL_CTA, NOMECON, Compensado, Descricao;');
  QrDebitos.SQL.Add('');
  QrDebitos.SQL.Add('DROP TABLE IF EXISTS _FIN_BALAN_003_01_DEBI_;');
  QrDebitos.SQL.Add('');
  UMyMod.AbreQuery(QrDebitos, DModG.MyPID_DB, 'TFmCashPreCta.ReopenCreditosEDebitos()');
end;

procedure TFmCashPreCta.ReopenNiveisCtas();
begin
  // Ini 2020-01-27
  //FCtasNiv := UCriar.RecriaTempTableNovo(ntrttSdoNivCtas, DModG.QrUpdPID1, False, 0, 'CtasNiv');
  FCtasNiv := UCriar.RecriaTempTableNovo(ntrttCtasNiv, DModG.QrUpdPID1, False, 0, 'CtasNiv');
  // Fim 2020-01-27
  //
  if (FEntidade = 0) and (EdEmpresa.ValueVariant <> 0) then
    FEntidade := DModG.QrEmpresasCodigo.Value;
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT IGNORE INTO ' + FCtasNiv);
  DmodG.QrUpdPID1.SQL.Add('SELECT ELT(niv.Nivel, "Conta", "Sub-grupo", "Grupo",');
  DmodG.QrUpdPID1.SQL.Add('"Conjunto", "Plano") NOMENIVEL, ELT(niv.Nivel,');
  DmodG.QrUpdPID1.SQL.Add('cta.Nome, sgr.Nome, gru.Nome, cjt.Nome, pla.Nome)');
  DmodG.QrUpdPID1.SQL.Add('NOMEGENERO, niv.Nivel, niv.Genero, 0 Ativo');
  DmodG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.contasniv niv');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    cta ON cta.Codigo=niv.Genero');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=niv.Genero');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=niv.Genero');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=niv.Genero');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.plano     pla ON pla.Codigo=niv.Genero');
  DmodG.QrUpdPID1.SQL.Add('WHERE niv.Entidade=' + Geral.FF0(FEntidade) + ';');
  DmodG.QrUpdPID1.ExecSQL;
  //
  QrCtasNiv.Close;
  UnDmkDAC_PF.AbreQuery(QrCtasNiv, DModG.MyPID_DB);
  //
  FechaPesquisa();
end;

procedure TFmCashPreCta.SQLDebitosFuturos(Query: TmySQLQuery;
SQL_Zero: String);
var
  Tab: String;
begin
  QrCNS.Close;
  UnDmkDAC_PF.AbreQuery(QrCNS, DModG.MyPID_DB);
  case QrCNS.RecordCount of
    0: Query.SQL.Add(SQL_Zero); // = nada
    1:
    begin
      case QrCNSNivel.Value of
        1: Tab := 'cta';
        2: Tab := 'sgr';
        3: Tab := 'gru';
        4: Tab := 'cjt';
        5: Tab := 'pla';
        else Tab := '???';
      end;
      Query.SQL.Add('AND '+Tab+'.Codigo='+FormatFloat('0', QrCNSGenero.Value));
    end;
    else
    begin
      QrCNS.First;
      Query.SQL.Add('AND (');
      while not QrCNS.Eof do
      begin
        case QrCNSNivel.Value of
          1: Tab := 'cta';
          2: Tab := 'sgr';
          3: Tab := 'gru';
          4: Tab := 'cjt';
          5: Tab := 'pla';
          else Tab := '???';
        end;
        if QrCNS.RecNo = 1 then
          Query.SQL.Add('    ('+Tab+'.Codigo='+FormatFloat('0', QrCNSGenero.Value)+ ')')
        else
          Query.SQL.Add(' OR ('+Tab+'.Codigo='+FormatFloat('0', QrCNSGenero.Value)+ ')');
        QrCNS.Next;
      end;
      Query.SQL.Add(')');
    end;
  end;
end;

function TFmCashPreCta.ValorArrecadacaoFutura(): Double;
var
  //ValCalc,
  ValB: Double;
  Titulo: String;
  ResVar: Variant;
begin
  ValB := 0;
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    // UnAuxCondGer.IncluiintensbasedearrecadaoNovo(SoListaProvisoes: Boolean);
    // C�digo copiado de
    // ArreBaIFator para ValB
    case QrArreBaIFator.Value of
      // Fator = Valor
      0: ValB := QrArreBaIValor.Value;
      // 1: Fator = Percentual
      1:
      begin
        (*if DCond.QrAptos.RecordCount = 0 then ValB := 0 else*)
        begin
          //ValCalc := 0;
          case QrArreBaIDoQue.Value of

  // DoQue = (0,1,2) se desconhece o valor, ent�o ser� zero!
  (*
            //  Sobre Provis�es
            0: ValCalc := FmCondGer.QrPrevGastos.Value;
            //  Sobre Consumos
            1: ValCalc := 0;//QrSumCTVALOR.Value;
            //  Sobre ambos
            // N�o se sabe o valor do FmCondGer.QrPrevGastos.Value ainda!
            2: ValCalc := FmCondGer.QrPrevGastos.Value;// + QrSumCTVALOR.Value;
            //  Valor a definir / definido
  *)
            3:
            begin
              if QrArreBaIValFracAsk.Value = 1 then
              begin
                // Parei aqui
                if QrArreBaITexto.Value <> '' then
                  Titulo := '(' + QrArreBaITexto.Value + ')'
                else
                  Titulo := '';
                if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
                QrArreBaIValFracDef.Value, 2, 0, '', '', True,
                QrArreBaINome.Value, 'Informe o valor total: ' + Titulo, 0, ResVar) then
                  ValB := ResVar
                else
                  ValB := 0;
              end else ValB := QrArreBaIValFracDef.Value;
            end;
            4:
            begin
              // Parei aqui!!!
              // Precisa Fazer ArreBaI
            end;
            //else ValCalc := 0;
          end;
  // N�o se sabe ainda, ent�o ser� zero!
  (*
          if QrArreBaIDoQue.Value <> 3 then
          begin
            if QrArreBaIComoCobra.Value = 0 then
            //  Calcular o percentual sobre o valor cobrado.
            ValB := QrArreBaIPercent.Value * ValCalc / 100
            else
            begin
            //  Incluir o pr�prio valor gerado no c�lculo.
              if QrArreBaIPercent.Value = 0 then ValB := 0 else
              begin
                PercXtra := QrArreBaIPercent.Value;
                PercXtra := PercXtra + (PercXtra * PercXtra / 100);
                ValB := PercXtra * ValCalc / 100;
              end;
            end;
          end;
  *)
        end;
      end;
      else begin
        ValB := 0;
        Geral.MB_Erro(
          'Fator n�o definido no cadastro de arrecada��es base!' + sLineBreak +
          'Avise a DERMATEK!');
      end;
    end;
    // FIM ArreBaIFator para ValB
    QrPagantes.Close;
    QrPagantes.SQL.Clear;
    QrPagantes.SQL.Add('SELECT COUNT(cdi.Conta) ITENS');
    QrPagantes.SQL.Add('FROM condimov cdi');
    QrPagantes.SQL.Add('WHERE cdi.SitImv=1');
    QrPagantes.SQL.Add('AND cdi.Codigo=' + FormatFloat('0', EdEmpresa.ValueVariant));
    UnDmkDAC_PF.AbreQuery(QrPagantes, Dmod.MyDB);
    Result := ValB * QrArreBaIParcelasRestantes.Value * QrPagantesITENS.Value;
  end else Result := 0;  
end;

end.
