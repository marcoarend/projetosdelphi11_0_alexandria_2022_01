object FmCarteiras: TFmCarteiras
  Left = 346
  Top = 171
  Caption = 'FIN-CARTE-001 :: Carteiras'
  ClientHeight = 737
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 95
    Width = 1008
    Height = 642
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    ExplicitLeft = 200
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 1008
      Height = 473
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Carteiras'
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 445
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitWidth = 634
          ExplicitHeight = 402
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 265
            Align = alTop
            Caption = ' Informa'#231#245'es b'#225'sicas: '
            TabOrder = 0
            object PainelData: TPanel
              Left = 2
              Top = 15
              Width = 640
              Height = 248
              Align = alClient
              BevelOuter = bvNone
              Enabled = False
              TabOrder = 0
              ExplicitWidth = 691
              ExplicitHeight = 284
              object Label1: TLabel
                Left = 8
                Top = 0
                Width = 36
                Height = 13
                Caption = 'C'#243'digo:'
                FocusControl = DBEdCodigo
              end
              object Label2: TLabel
                Left = 67
                Top = 0
                Width = 51
                Height = 13
                Caption = 'Descri'#231#227'o:'
                FocusControl = DBEdNome
              end
              object Label19: TLabel
                Left = 300
                Top = 0
                Width = 125
                Height = 13
                Caption = 'Descri'#231#227'o para impress'#227'o:'
                FocusControl = EdDBNome
              end
              object Label3: TLabel
                Left = 528
                Top = 0
                Width = 24
                Height = 13
                Caption = 'Tipo:'
                FocusControl = DBEdit1
              end
              object Label12: TLabel
                Left = 8
                Top = 100
                Width = 317
                Height = 13
                Caption = 
                  'Nome do terceiro ao qual a carteira est'#225' vinculada (Cliente inte' +
                  'rno):'
                FocusControl = DBEdit9
              end
              object Label38: TLabel
                Left = 8
                Top = 160
                Width = 146
                Height = 13
                Caption = 'Entidade detentora da carteira:'
              end
              object Label43: TLabel
                Left = 194
                Top = 160
                Width = 146
                Height = 13
                Caption = 'Fornecedor padr'#227'o da carteira:'
              end
              object Label44: TLabel
                Left = 379
                Top = 160
                Width = 124
                Height = 13
                Caption = 'Cliente padr'#227'o da carteira:'
              end
              object DBEdCodigo: TDBEdit
                Left = 8
                Top = 16
                Width = 56
                Height = 21
                Hint = 'N'#186' do banco'
                TabStop = False
                DataField = 'Codigo'
                DataSource = DsCarteiras
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 8281908
                Font.Height = -9
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                MaxLength = 1
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 0
              end
              object DBEdNome: TDBEdit
                Left = 67
                Top = 16
                Width = 230
                Height = 21
                Hint = 'Nome do banco'
                Color = clWhite
                DataField = 'Nome'
                DataSource = DsCarteiras
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -9
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                MaxLength = 13
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
              end
              object EdDBNome: TDBEdit
                Left = 300
                Top = 16
                Width = 225
                Height = 21
                Hint = 'Nome do banco'
                Color = clWhite
                DataField = 'Nome2'
                DataSource = DsCarteiras
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -9
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
              end
              object DBEdit14: TDBEdit
                Left = 528
                Top = 16
                Width = 20
                Height = 21
                DataField = 'Tipo'
                DataSource = DsCarteiras
                TabOrder = 3
              end
              object DBEdit1: TDBEdit
                Left = 551
                Top = 16
                Width = 86
                Height = 21
                DataField = 'NOMETIPO'
                DataSource = DsCarteiras
                TabOrder = 4
              end
              object DBRGTipo: TDBRadioGroup
                Left = 8
                Top = 41
                Width = 625
                Height = 60
                Caption = ' Tipo de carteira: '
                DataField = 'Tipo'
                DataSource = DsCarteiras
                Items.Strings = (
                  'Caixa: dinheiro em esp'#233'cie, cheques recebidos a vista, etc.'
                  
                    'Banco: conta corrente banc'#225'ria fiel ao extrato com d'#233'bitos diret' +
                    'os em conta, compensa'#231#227'o de cheques, etc.'
                  
                    'Emiss'#227'o: emiss'#227'o de cheques, recebimento de cheques a prazo, dup' +
                    'licatas, bloquetos, d'#233'bitos pr'#233'-agendados, e outros itens a praz' +
                    'o.')
                TabOrder = 5
                Values.Strings = (
                  '0'
                  '1'
                  '2')
                OnChange = DBRGTipoChange
                OnClick = DBRGTipoClick
              end
              object DBEdit9: TDBEdit
                Left = 8
                Top = 116
                Width = 413
                Height = 21
                DataField = 'NOMEFORNECEI'
                DataSource = DsCarteiras
                TabOrder = 6
              end
              object DBRGPagRec: TDBRadioGroup
                Left = 424
                Top = 103
                Width = 211
                Height = 36
                Caption = ' Caracter'#237'stica: '
                Columns = 3
                DataField = 'PagRec'
                DataSource = DsCarteiras
                Items.Strings = (
                  'Pagar'
                  'Ambos'
                  'Receber')
                TabOrder = 7
                Values.Strings = (
                  '-1'
                  '0'
                  '1')
              end
              object DBCheckBox1: TDBCheckBox
                Left = 510
                Top = 140
                Width = 48
                Height = 25
                Caption = 'Ativa.'
                DataField = 'Ativo'
                DataSource = DsCarteiras
                TabOrder = 8
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBCheckBox5: TDBCheckBox
                Left = 8
                Top = 140
                Width = 273
                Height = 17
                Caption = 'Omitir saldo de relat'#243'rio de saldos. (carteira exclusiva)'
                DataField = 'Exclusivo'
                DataSource = DsCarteiras
                TabOrder = 9
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBEdit15: TDBEdit
                Left = 8
                Top = 176
                Width = 182
                Height = 21
                DataField = 'NOMEENTIDENT'
                DataSource = DsCarteiras
                TabOrder = 10
              end
              object DBEdit17: TDBEdit
                Left = 194
                Top = 176
                Width = 182
                Height = 21
                DataField = 'NO_FornecePadrao'
                DataSource = DsCarteiras
                TabOrder = 11
              end
              object DBEdit18: TDBEdit
                Left = 379
                Top = 176
                Width = 183
                Height = 21
                DataField = 'NO_CLientePadrao'
                DataSource = DsCarteiras
                TabOrder = 12
              end
            end
            object Panel8: TPanel
              Left = 642
              Top = 15
              Width = 356
              Height = 248
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              ExplicitLeft = 648
              ExplicitTop = 0
              ExplicitHeight = 332
              object Label48: TLabel
                Left = 0
                Top = 0
                Width = 34
                Height = 13
                Caption = 'Ordem:'
              end
              object Label49: TLabel
                Left = 55
                Top = 0
                Width = 176
                Height = 13
                Caption = 'Identificador extrato banco (Importar):'
              end
              object Label50: TLabel
                Left = 0
                Top = 40
                Width = 40
                Height = 13
                Caption = 'Contato:'
                Visible = False
              end
              object Label51: TLabel
                Left = 1
                Top = 80
                Width = 204
                Height = 13
                Caption = 'Identificador conta contabilidade (exportar):'
              end
              object Label52: TLabel
                Left = 0
                Top = 122
                Width = 168
                Height = 13
                Caption = 'Conta Cont'#225'bil do Plano de Contas:'
              end
              object DBEdit13: TDBEdit
                Left = 3
                Top = 14
                Width = 41
                Height = 21
                DataField = 'Ordem'
                DataSource = DsCarteiras
                TabOrder = 0
              end
              object EdDBID: TDBEdit
                Left = 53
                Top = 14
                Width = 159
                Height = 21
                Hint = 'Nome do banco'
                Color = clWhite
                DataField = 'ID'
                DataSource = DsCarteiras
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -9
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
              end
              object EdDBContato: TDBEdit
                Left = 0
                Top = 56
                Width = 132
                Height = 21
                Hint = 'Nome do banco'
                Color = clWhite
                DataField = 'Contato1'
                DataSource = DsCarteiras
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -9
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
              end
              object EdDBContab: TDBEdit
                Left = 0
                Top = 96
                Width = 212
                Height = 21
                Hint = 'Nome do banco'
                Color = clWhite
                DataField = 'Contab'
                DataSource = DsCarteiras
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -9
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 3
              end
              object DBCheckBox11: TDBCheckBox
                Left = 1
                Top = 159
                Width = 168
                Height = 17
                Caption = 'Fechamento diario obrigat'#243'rio.'
                DataField = 'FechaDia'
                DataSource = DsCarteiras
                TabOrder = 4
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBRadioGroup1: TDBRadioGroup
                Left = 0
                Top = 176
                Width = 356
                Height = 72
                Align = alBottom
                Caption = ' Finalidade: '
                Columns = 3
                DataField = 'TipCart'
                DataSource = DsCarteiras
                Items.Strings = (
                  'Caixa'
                  'Banco'
                  'Cart'#227'o de cr'#233'dito'
                  'Duplicata'
                  'Cheque'
                  'Boleto'
                  'Contas '#224' pagar'
                  'Contas '#224' receber'
                  'Outras emiss'#245'es')
                TabOrder = 5
                ExplicitTop = 212
              end
              object DBEdit19: TDBEdit
                Left = 0
                Top = 136
                Width = 44
                Height = 21
                DataField = 'GenCtb'
                DataSource = DsCarteiras
                TabOrder = 6
              end
              object DBEdit20: TDBEdit
                Left = 44
                Top = 136
                Width = 195
                Height = 21
                DataField = 'NO_GenCtb'
                DataSource = DsCarteiras
                TabOrder = 7
              end
            end
          end
          object GBTipoShowData: TGroupBox
            Left = 0
            Top = 265
            Width = 1000
            Height = 176
            Align = alTop
            Caption = 'Informa'#231#245'es extras: '
            TabOrder = 1
            object PnBancShow: TPanel
              Left = 2
              Top = 15
              Width = 996
              Height = 159
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              Visible = False
              ExplicitWidth = 630
              ExplicitHeight = 133
              object Label4: TLabel
                Left = 8
                Top = 4
                Width = 59
                Height = 13
                Caption = 'Saldo inicial:'
                FocusControl = DBEdit2
              end
              object Label13: TLabel
                Left = 90
                Top = 4
                Width = 34
                Height = 13
                Caption = 'Banco:'
                FocusControl = DBEdit10
              end
              object Label17: TLabel
                Left = 158
                Top = 4
                Width = 42
                Height = 13
                Caption = 'Ag'#234'ncia:'
                FocusControl = DBEdit11
              end
              object Label18: TLabel
                Left = 225
                Top = 4
                Width = 73
                Height = 13
                Caption = 'Conta corrente:'
                FocusControl = DBEdit12
              end
              object Label39: TLabel
                Left = 319
                Top = 4
                Width = 104
                Height = 13
                Caption = 'C'#243'digo retorno CNAB:'
                FocusControl = DBEdit16
              end
              object DBEdit2: TDBEdit
                Left = 8
                Top = 20
                Width = 78
                Height = 21
                DataField = 'Inicial'
                DataSource = DsCarteiras
                TabOrder = 0
              end
              object DBEdit10: TDBEdit
                Left = 90
                Top = 20
                Width = 64
                Height = 21
                DataField = 'Banco1'
                DataSource = DsCarteiras
                TabOrder = 1
              end
              object DBEdit11: TDBEdit
                Left = 158
                Top = 20
                Width = 63
                Height = 21
                DataField = 'Agencia1'
                DataSource = DsCarteiras
                TabOrder = 2
              end
              object DBEdit12: TDBEdit
                Left = 225
                Top = 20
                Width = 92
                Height = 21
                DataField = 'Conta1'
                DataSource = DsCarteiras
                TabOrder = 3
              end
              object DBEdit16: TDBEdit
                Left = 319
                Top = 20
                Width = 158
                Height = 21
                DataField = 'CodCedente'
                DataSource = DsCarteiras
                TabOrder = 4
              end
            end
            object PnCaixShow: TPanel
              Left = 2
              Top = 15
              Width = 996
              Height = 159
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 2
              Visible = False
              ExplicitWidth = 630
              ExplicitHeight = 133
              object Label7: TLabel
                Left = 8
                Top = 4
                Width = 59
                Height = 13
                Caption = 'Saldo inicial:'
                FocusControl = DBEdit6
              end
              object Label8: TLabel
                Left = 90
                Top = 4
                Width = 75
                Height = 13
                Caption = 'Saldo em caixa:'
                FocusControl = DBEdit7
              end
              object DBEdit6: TDBEdit
                Left = 8
                Top = 20
                Width = 78
                Height = 21
                DataField = 'Inicial'
                DataSource = DsCarteiras
                TabOrder = 0
              end
              object DBEdit7: TDBEdit
                Left = 90
                Top = 20
                Width = 80
                Height = 21
                DataField = 'EmCaixa'
                DataSource = DsCarteiras
                TabOrder = 1
              end
              object DBCheckBox3: TDBCheckBox
                Left = 178
                Top = 24
                Width = 95
                Height = 16
                Caption = 'Permite prazo.'
                DataField = 'Prazo'
                DataSource = DsCarteiras
                TabOrder = 2
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBCheckBox6: TDBCheckBox
                Left = 280
                Top = 24
                Width = 190
                Height = 16
                Caption = 'Recebe pagamentos de bloquetos.'
                DataField = 'RecebeBloq'
                DataSource = DsCarteiras
                TabOrder = 3
                ValueChecked = '1'
                ValueUnchecked = '0'
                Visible = False
              end
            end
            object PnEmisShow: TPanel
              Left = 2
              Top = 15
              Width = 996
              Height = 159
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              Visible = False
              ExplicitWidth = 630
              ExplicitHeight = 133
              object Label5: TLabel
                Left = 8
                Top = 0
                Width = 239
                Height = 13
                Caption = 'Nome do banco ao qual a emiss'#227'o est'#225' vinculada:'
                FocusControl = DBEdit3
              end
              object DBEdit3: TDBEdit
                Left = 8
                Top = 16
                Width = 316
                Height = 21
                DataField = 'NOMEDOBANCO'
                DataSource = DsCarteiras
                TabOrder = 0
              end
              object GroupBox1: TGroupBox
                Left = 8
                Top = 42
                Width = 633
                Height = 59
                Caption = '     '
                TabOrder = 1
                object LaIDFat: TLabel
                  Left = 7
                  Top = 16
                  Width = 238
                  Height = 13
                  Caption = 'Identificador autom'#225'tico de fatura em importa'#231#245'es:'
                end
                object Label6: TLabel
                  Left = 274
                  Top = 16
                  Width = 157
                  Height = 13
                  Caption = 'Fechamento da fatura (dd antes):'
                end
                object Label11: TLabel
                  Left = 442
                  Top = 16
                  Width = 92
                  Height = 13
                  Caption = 'Dia do vencimento:'
                end
                object DBEdit4: TDBEdit
                  Left = 8
                  Top = 32
                  Width = 262
                  Height = 21
                  DataField = 'ID_Fat'
                  DataSource = DsCarteiras
                  TabOrder = 0
                end
                object DBEdit5: TDBEdit
                  Left = 274
                  Top = 32
                  Width = 163
                  Height = 21
                  DataField = 'Fechamento'
                  DataSource = DsCarteiras
                  TabOrder = 1
                end
                object DBEdit8: TDBEdit
                  Left = 442
                  Top = 32
                  Width = 95
                  Height = 21
                  DataField = 'DiaMesVence'
                  DataSource = DsCarteiras
                  TabOrder = 2
                end
              end
              object DBCheckBox2: TDBCheckBox
                Left = 20
                Top = 42
                Width = 95
                Height = 16
                Caption = 'Emite fatura: '
                DataField = 'Fatura'
                DataSource = DsCarteiras
                TabOrder = 2
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBCheckBox4: TDBCheckBox
                Left = 8
                Top = 104
                Width = 237
                Height = 17
                Caption = 'Exige n'#250'mero de cheque em lan'#231'amentos.'
                DataField = 'ExigeNumCheque'
                DataSource = DsCarteiras
                TabOrder = 3
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBRGTipoDoc: TDBRadioGroup
                Left = 5
                Top = 121
                Width = 984
                Height = 34
                Caption = ' Tipo de Documento: '
                Columns = 9
                DataField = 'TipoDoc'
                DataSource = DsCarteiras
                Items.Strings = (
                  'Outros'
                  'Cheque'
                  'DOC'
                  'TED'
                  'Esp'#233'cie'
                  'Bloqueto'
                  'Duplicata'
                  'TEF'
                  'D'#233'bito c/c')
                TabOrder = 4
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9'
                  '10'
                  '11'
                  '12'
                  '13'
                  '14'
                  '15'
                  '16')
              end
              object DBCheckBox7: TDBCheckBox
                Left = 233
                Top = 104
                Width = 201
                Height = 17
                Caption = 'Ignorar s'#233'rie do cheque na concilia'#231#227'o banc'#225'ria.'
                DataField = 'IgnorSerie'
                DataSource = DsCarteiras
                TabOrder = 5
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBCheckBox8: TDBCheckBox
                Left = 442
                Top = 104
                Width = 215
                Height = 17
                Caption = 'Ignorar carteira na concilia'#231#227'o banc'#225'ria'
                DataField = 'NotConcBco'
                DataSource = DsCarteiras
                TabOrder = 6
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBCheckBox9: TDBCheckBox
                Left = 327
                Top = 16
                Width = 302
                Height = 16
                Caption = 'Permite mudar a carteira banc'#225'ria no momento da quita'#231#227'o'
                DataField = 'MudaBco'
                DataSource = DsCarteiras
                TabOrder = 7
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
            end
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Pesquisa'
        ImageIndex = 3
        object Splitter2: TSplitter
          Left = 303
          Top = 0
          Width = 10
          Height = 445
          ExplicitHeight = 375
        end
        object PnFast: TPanel
          Left = 0
          Top = 0
          Width = 303
          Height = 445
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 402
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 303
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object EdCliInt: TEdit
              Left = 4
              Top = 4
              Width = 238
              Height = 21
              TabOrder = 0
              OnChange = EdCliIntChange
            end
          end
          object dmkDBGrid1: TdmkDBGrid
            Left = 0
            Top = 27
            Width = 303
            Height = 418
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'CliInt'
                Title.Caption = 'Cli.int.'
                Width = 35
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'Entid.'
                Width = 35
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECLI'
                Title.Caption = 'Raz'#227'o Social / Nome'
                Width = 179
                Visible = True
              end>
            Color = clWindow
            DataSource = DsCliInt
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -9
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'CliInt'
                Title.Caption = 'Cli.int.'
                Width = 35
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'Entid.'
                Width = 35
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECLI'
                Title.Caption = 'Raz'#227'o Social / Nome'
                Width = 179
                Visible = True
              end>
          end
        end
        object Panel4: TPanel
          Left = 313
          Top = 0
          Width = 687
          Height = 445
          Align = alClient
          TabOrder = 1
          ExplicitWidth = 321
          ExplicitHeight = 402
          object Panel6: TPanel
            Left = 1
            Top = 1
            Width = 685
            Height = 28
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 319
            object CkSohAtivos: TCheckBox
              Left = 205
              Top = 8
              Width = 95
              Height = 17
              Caption = 'Somente ativos.'
              Checked = True
              State = cbChecked
              TabOrder = 0
              OnClick = CkSohAtivosClick
            end
            object EdCliCart: TEdit
              Left = 4
              Top = 4
              Width = 194
              Height = 21
              TabOrder = 1
              OnChange = EdCliCartChange
            end
          end
          object dmkDBGrid2: TdmkDBGrid
            Left = 1
            Top = 29
            Width = 685
            Height = 415
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'NOME_TIPO'
                Title.Caption = 'Tipo'
                Width = 46
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco1'
                Title.Caption = 'Bco'
                Width = 26
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Agencia1'
                Title.Caption = 'Ag'#234'nc.'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Conta1'
                Title.Caption = 'Conta corrente'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o carteira'
                Width = 248
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEBCO'
                Title.Caption = 'Banco atrelado'
                Width = 201
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Inicial'
                Title.Caption = 'Saldo inicial'
                Width = 58
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Saldo'
                Title.Caption = 'Saldo atual'
                Width = 58
                Visible = True
              end>
            Color = clWindow
            DataSource = DsCliCart
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -9
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = dmkDBGrid2DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'NOME_TIPO'
                Title.Caption = 'Tipo'
                Width = 46
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco1'
                Title.Caption = 'Bco'
                Width = 26
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Agencia1'
                Title.Caption = 'Ag'#234'nc.'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Conta1'
                Title.Caption = 'Conta corrente'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o carteira'
                Width = 248
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEBCO'
                Title.Caption = 'Banco atrelado'
                Width = 201
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Inicial'
                Title.Caption = 'Saldo inicial'
                Width = 58
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Saldo'
                Title.Caption = 'Saldo atual'
                Width = 58
                Visible = True
              end>
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Tal'#245'es de cheque'
        ImageIndex = 2
        object DBGCartTalCH: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 445
          Align = alClient
          DataSource = DsCartTalCH
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -9
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Usu'#225'rios'
        ImageIndex = 2
        object DBGFunci: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 445
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Usuario'
              Title.Caption = 'Usu'#225'rio'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Login'
              Width = 84
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Funcionario'
              Title.Caption = 'Funcion'#225'rio'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEUSU'
              Title.Caption = 'Nome do Funcion'#225'rio'
              Width = 411
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCarteirasU
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -9
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Usuario'
              Title.Caption = 'Usu'#225'rio'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Login'
              Width = 84
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Funcionario'
              Title.Caption = 'Funcion'#225'rio'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEUSU'
              Title.Caption = 'Nome do Funcion'#225'rio'
              Width = 411
              Visible = True
            end>
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 572
      Width = 1008
      Height = 70
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 171
        Height = 53
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 3
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 86
          Top = 3
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 45
          Top = 3
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 3
          Top = 3
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 173
        Top = 15
        Width = 195
        Height = 53
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
        ExplicitLeft = 142
        ExplicitWidth = 187
      end
      object Panel3: TPanel
        Left = 368
        Top = 15
        Width = 638
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel1: TPanel
          Left = 528
          Top = 0
          Width = 110
          Height = 53
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 7
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 94
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 184
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui'
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
        object BtTaloes: TBitBtn
          Left = 274
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Tal'#245'es'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtTaloesClick
        end
        object BtUsuario: TBitBtn
          Left = 364
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Usu'#225'rios'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtUsuarioClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 95
    Width = 1008
    Height = 642
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    Visible = False
    ExplicitTop = 99
    object GBConfirma: TGroupBox
      Left = 0
      Top = 572
      Width = 1008
      Height = 70
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 18
        Width = 96
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel11: TPanel
        Left = 888
        Top = 15
        Width = 118
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 522
        ExplicitHeight = 39
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 96
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBTipoShowEdit: TGroupBox
      Left = 0
      Top = 382
      Width = 1008
      Height = 190
      Align = alBottom
      Caption = ' Informa'#231#245'es extras: '
      TabOrder = 1
      ExplicitTop = 396
      object PnCaixEdit: TPanel
        Left = 853
        Top = 15
        Width = 153
        Height = 173
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        Visible = False
        ExplicitLeft = 487
        ExplicitHeight = 137
        object Label23: TLabel
          Left = 3
          Top = 0
          Width = 59
          Height = 13
          Caption = 'Saldo inicial:'
          Visible = False
        end
        object CkPrazo: TCheckBox
          Left = 3
          Top = 40
          Width = 75
          Height = 16
          Caption = 'Permite prazo?'
          TabOrder = 1
        end
        object EdInicial_C: TdmkEdit
          Left = 3
          Top = 16
          Width = 83
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          Visible = False
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object CkRecebeBloq: TCheckBox
          Left = 3
          Top = 56
          Width = 187
          Height = 16
          Caption = 'Recebe pagamentos de bloquetos.'
          TabOrder = 2
          Visible = False
        end
      end
      object PnBancEdit: TPanel
        Left = 2
        Top = 15
        Width = 851
        Height = 173
        Align = alClient
        TabOrder = 0
        Visible = False
        ExplicitWidth = 485
        ExplicitHeight = 137
        object Label24: TLabel
          Left = 8
          Top = 4
          Width = 59
          Height = 13
          Caption = 'Saldo inicial:'
          Visible = False
        end
        object Label25: TLabel
          Left = 90
          Top = 4
          Width = 34
          Height = 13
          Caption = 'Banco:'
        end
        object Label26: TLabel
          Left = 158
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Ag'#234'ncia:'
        end
        object Label27: TLabel
          Left = 225
          Top = 4
          Width = 73
          Height = 13
          Caption = 'Conta corrente:'
        end
        object Label36: TLabel
          Left = 8
          Top = 43
          Width = 174
          Height = 13
          Caption = 'C'#243'digo retorno CNAB: (C'#243'd cedente)'
        end
        object Label40: TLabel
          Left = 191
          Top = 62
          Width = 304
          Height = 13
          Caption = 'Apenas para os bancos: 001, 399, 409 que utilizam o CNAB 400'
        end
        object EdInicial_B: TdmkEdit
          Left = 8
          Top = 20
          Width = 78
          Height = 20
          Alignment = taRightJustify
          TabOrder = 0
          Visible = False
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdBanco1: TdmkEdit
          Left = 90
          Top = 20
          Width = 64
          Height = 20
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAgencia1: TdmkEdit
          Left = 158
          Top = 20
          Width = 63
          Height = 20
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdConta1: TdmkEdit
          Left = 225
          Top = 20
          Width = 92
          Height = 20
          Alignment = taCenter
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCodCedente: TdmkEdit
          Left = 8
          Top = 59
          Width = 177
          Height = 21
          Alignment = taCenter
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object PnEmisEdit: TPanel
        Left = 2
        Top = 15
        Width = 851
        Height = 173
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        ExplicitLeft = 162
        ExplicitTop = 51
        object Label21: TLabel
          Left = 8
          Top = 0
          Width = 243
          Height = 13
          Caption = 'Carteira banc'#225'ria ao qual a emiss'#227'o est'#225' vinculada:'
        end
        object Label34: TLabel
          Left = 372
          Top = 0
          Width = 34
          Height = 13
          Caption = 'Banco:'
        end
        object Label35: TLabel
          Left = 416
          Top = 0
          Width = 42
          Height = 13
          Caption = 'Ag'#234'ncia:'
        end
        object Label37: TLabel
          Left = 469
          Top = 0
          Width = 31
          Height = 13
          Caption = 'Conta:'
        end
        object GBFatura2: TGroupBox
          Left = 8
          Top = 38
          Width = 639
          Height = 59
          Caption = '      '
          TabOrder = 7
          Visible = False
          object Label15: TLabel
            Left = 7
            Top = 16
            Width = 238
            Height = 13
            Caption = 'Identificador autom'#225'tico de fatura em importa'#231#245'es:'
          end
          object Label16: TLabel
            Left = 366
            Top = 16
            Width = 157
            Height = 13
            Caption = 'Fechamento da fatura (dd antes):'
          end
          object Label20: TLabel
            Left = 531
            Top = 16
            Width = 92
            Height = 13
            Caption = 'Dia do vencimento:'
          end
          object EdIDFat: TdmkEdit
            Left = 8
            Top = 32
            Width = 349
            Height = 21
            Color = clWhite
            MaxLength = 50
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdDdFechamento: TdmkEdit
            Left = 366
            Top = 32
            Width = 159
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdDiaMesVence: TdmkEdit
            Left = 531
            Top = 32
            Width = 98
            Height = 21
            Alignment = taRightJustify
            Color = clWhite
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object CkFatura: TCheckBox
          Left = 20
          Top = 38
          Width = 126
          Height = 17
          Caption = 'Pagamento por fatura.'
          TabOrder = 6
        end
        object CkExigeNumCheque: TCheckBox
          Left = 8
          Top = 100
          Width = 111
          Height = 17
          Caption = 'Exige n'#250'mero do cheque.'
          TabOrder = 8
        end
        object RGTipoDoc: TRadioGroup
          Left = 4
          Top = 117
          Width = 639
          Height = 34
          Caption = ' Tipo de Documento: '
          Columns = 9
          ItemIndex = 0
          Items.Strings = (
            'N/I'
            'Cheque'
            'DOC'
            'TED'
            'Esp'#233'cie'
            'Bloqueto'
            'Duplicata'
            'TEF'
            'D'#233'bito c/c')
          TabOrder = 12
        end
        object EdBanco1_1: TdmkEdit
          Left = 372
          Top = 16
          Width = 37
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAgencia1_1: TdmkEdit
          Left = 416
          Top = 16
          Width = 49
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdConta1_1: TdmkEdit
          Left = 469
          Top = 16
          Width = 124
          Height = 21
          Alignment = taCenter
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CheckBox1: TCheckBox
          Left = 128
          Top = 100
          Width = 116
          Height = 17
          Caption = 'Ocultar saldo no bloqueto.'
          TabOrder = 9
        end
        object CkIgnorSerie: TdmkCheckBox
          Left = 253
          Top = 100
          Width = 209
          Height = 17
          Caption = 'Ignorar s'#233'rie do cheque na concilia'#231#227'o banc'#225'ria.'
          TabOrder = 10
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object CkNotConcBco: TdmkCheckBox
          Left = 462
          Top = 100
          Width = 176
          Height = 17
          Caption = 'Ignorar carteira na concilia'#231#227'o banc'#225'ria'
          TabOrder = 11
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object CkMudaBco: TdmkCheckBox
          Left = 598
          Top = 16
          Width = 248
          Height = 17
          Caption = 'Permite mudar a carteira banc'#225'ria ao quitar.'
          TabOrder = 5
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object EdBanco: TdmkEditCB
          Left = 8
          Top = 16
          Width = 42
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBBanco
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBBanco: TdmkDBLookupComboBox
          Left = 51
          Top = 16
          Width = 314
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsBancos
          TabOrder = 1
          dmkEditCB = EdBanco
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
    object GBDefault: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 265
      Align = alTop
      Caption = ' Informa'#231#245'es b'#225'sicas: '
      TabOrder = 0
      object PnBasicas: TPanel
        Left = 2
        Top = 15
        Width = 648
        Height = 248
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 659
        ExplicitHeight = 332
        object Label9: TLabel
          Left = 8
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label10: TLabel
          Left = 62
          Top = 0
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label14: TLabel
          Left = 383
          Top = 0
          Width = 105
          Height = 13
          Caption = 'Texto para Impress'#227'o:'
        end
        object Label33: TLabel
          Left = 8
          Top = 158
          Width = 146
          Height = 13
          Caption = 'Entidade detentora da carteira:'
        end
        object LaForneceI: TLabel
          Left = 8
          Top = 100
          Width = 167
          Height = 13
          Caption = 'Cliente interno que utiliza a carteira:'
        end
        object Label41: TLabel
          Left = 10
          Top = 198
          Width = 146
          Height = 13
          Caption = 'Fornecedor padr'#227'o da carteira:'
        end
        object Label42: TLabel
          Left = 327
          Top = 198
          Width = 124
          Height = 13
          Caption = 'Cliente padr'#227'o da carteira:'
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 16
          Width = 52
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object RGPagRec: TRadioGroup
          Left = 432
          Top = 104
          Width = 209
          Height = 34
          Caption = ' Caracter'#237'stica: '
          Columns = 3
          ItemIndex = 1
          Items.Strings = (
            'Pagar'
            'Ambos'
            'Receber')
          TabOrder = 6
        end
        object EdNome: TdmkEdit
          Left = 62
          Top = 16
          Width = 319
          Height = 21
          Color = clWhite
          MaxLength = 50
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdNomeExit
        end
        object EdNome2: TdmkEdit
          Left = 383
          Top = 16
          Width = 258
          Height = 21
          Color = clWhite
          MaxLength = 50
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkAtivo: TCheckBox
          Left = 594
          Top = 140
          Width = 51
          Height = 17
          Caption = 'Ativa.'
          Checked = True
          State = cbChecked
          TabOrder = 9
        end
        object CkForneceN: TCheckBox
          Left = 8
          Top = 140
          Width = 285
          Height = 17
          Caption = 'A carteira n'#227'o pertence ao cliente interno indicado.'
          TabOrder = 7
        end
        object CkExclusivo: TCheckBox
          Left = 306
          Top = 140
          Width = 283
          Height = 17
          Caption = 'Omitir saldo de relat'#243'rio de saldos. (carteira exclusiva)'
          TabOrder = 8
        end
        object EdEntiDent: TdmkEditCB
          Left = 8
          Top = 174
          Width = 42
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEntiDent
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEntiDent: TdmkDBLookupComboBox
          Left = 51
          Top = 174
          Width = 586
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          ListSource = DsEntiDent
          TabOrder = 11
          dmkEditCB = EdEntiDent
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object RGTipo: TRadioGroup
          Left = 8
          Top = 41
          Width = 633
          Height = 60
          Caption = ' Tipo de carteira: '
          Items.Strings = (
            'Caixa: dinheiro em esp'#233'cie, cheques recebidos a vista, etc.'
            
              'Banco: conta corrente banc'#225'ria fiel ao extrato com d'#233'bitos diret' +
              'os em conta, compensa'#231#227'o de cheques, etc.'
            
              'Emiss'#227'o: emiss'#227'o de cheques, recebimento de cheques a prazo, dup' +
              'licatas, bloquetos, d'#233'bitos pr'#233'-agendados, e outros itens a praz' +
              'o.')
          TabOrder = 3
          OnClick = RGTipoClick
        end
        object EdForneceI: TdmkEditCB
          Left = 8
          Top = 116
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdForneceIChange
          DBLookupComboBox = CBForneceI
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBForneceI: TdmkDBLookupComboBox
          Left = 63
          Top = 116
          Width = 362
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsForneceI
          TabOrder = 5
          dmkEditCB = EdForneceI
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdFornecePadrao: TdmkEditCB
          Left = 10
          Top = 214
          Width = 42
          Height = 21
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBFornecePadrao
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBFornecePadrao: TdmkDBLookupComboBox
          Left = 54
          Top = 214
          Width = 268
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          ListSource = DsFornecePadrao
          TabOrder = 13
          dmkEditCB = EdFornecePadrao
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdClientePadrao: TdmkEditCB
          Left = 327
          Top = 214
          Width = 41
          Height = 21
          Alignment = taRightJustify
          TabOrder = 14
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBClientePadrao
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBClientePadrao: TdmkDBLookupComboBox
          Left = 370
          Top = 214
          Width = 268
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          ListSource = DsClientePadrao
          TabOrder = 15
          dmkEditCB = EdClientePadrao
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object Panel2: TPanel
        Left = 650
        Top = 15
        Width = 356
        Height = 248
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 648
        ExplicitTop = 0
        ExplicitHeight = 332
        object Label32: TLabel
          Left = 0
          Top = 0
          Width = 34
          Height = 13
          Caption = 'Ordem:'
        end
        object LaID2: TLabel
          Left = 55
          Top = 0
          Width = 176
          Height = 13
          Caption = 'Identificador extrato banco (Importar):'
        end
        object Label28: TLabel
          Left = 0
          Top = 40
          Width = 40
          Height = 13
          Caption = 'Contato:'
          Visible = False
        end
        object Label46: TLabel
          Left = 1
          Top = 80
          Width = 204
          Height = 13
          Caption = 'Identificador conta contabilidade (exportar):'
        end
        object Label45: TLabel
          Left = 0
          Top = 122
          Width = 168
          Height = 13
          Caption = 'Conta Cont'#225'bil do Plano de Contas:'
        end
        object EdOrdem: TdmkEdit
          Left = 0
          Top = 16
          Width = 42
          Height = 21
          Alignment = taRightJustify
          Color = clWhite
          MaxLength = 50
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdID: TdmkEdit
          Left = 43
          Top = 16
          Width = 306
          Height = 21
          Color = clWhite
          MaxLength = 50
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdContato1: TdmkEdit
          Left = 0
          Top = 56
          Width = 349
          Height = 21
          Color = clWhite
          MaxLength = 50
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdContab: TdmkEdit
          Left = 1
          Top = 96
          Width = 348
          Height = 21
          Color = clWhite
          MaxLength = 50
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkFechaDia: TdmkCheckBox
          Left = 0
          Top = 159
          Width = 189
          Height = 17
          Caption = 'Fechamento diario obrigat'#243'rio.'
          TabOrder = 6
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object EdGenCtb: TdmkEditCB
          Left = 0
          Top = 136
          Width = 42
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBGenCtb
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGenCtb: TdmkDBLookupComboBox
          Left = 44
          Top = 136
          Width = 305
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsContas
          TabOrder = 5
          dmkEditCB = EdGenCtb
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object RGTipoCart: TRadioGroup
          Left = 0
          Top = 176
          Width = 356
          Height = 72
          Align = alBottom
          Caption = ' Finalidade: '
          Columns = 3
          Items.Strings = (
            'Caixa'
            'Banco'
            'Cart'#227'o de cr'#233'dito'
            'Duplicata'
            'Cheque'
            'Boleto'
            'Contas '#224' pagar'
            'Contas '#224' receber'
            'Outras emiss'#245'es')
          TabOrder = 7
          ExplicitTop = 224
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 642
    object GB_R: TGroupBox
      Left = 956
      Top = 0
      Width = 52
      Height = 52
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 967
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 217
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 3
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 45
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 217
      Top = 0
      Width = 739
      Height = 52
      Align = alClient
      TabOrder = 2
      ExplicitLeft = 308
      ExplicitWidth = 659
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 111
        Height = 32
        Caption = 'Carteiras'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 111
        Height = 32
        Caption = 'Carteiras'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 111
        Height = 32
        Caption = 'Carteiras'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitTop = 42
    ExplicitWidth = 642
    object Panel12: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 638
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 566
        Height = 16
        Caption = 
          'Na aba Pesquisa d'#234' um duplo clique na grade onde mostram as cart' +
          'eiras para localiz'#225'-la.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 566
        Height = 16
        Caption = 
          'Na aba Pesquisa d'#234' um duplo clique na grade onde mostram as cart' +
          'eiras para localiz'#225'-la.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 408
    Top = 5
  end
  object QrCarteiras: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCarteirasBeforeOpen
    AfterOpen = QrCarteirasAfterOpen
    AfterScroll = QrCarteirasAfterScroll
    OnCalcFields = QrCarteirasCalcFields
    SQL.Strings = (
      'SELECT DISTINCT ca.*, ba.Nome NOMEDOBANCO, en.CliInt,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEFORNECEI,'
      'IF(eg.Tipo=0, eg.RazaoSocial, eg.Nome) NOMEENTIDENT,'
      'IF(fp.Tipo=0, fp.RazaoSocial, fp.Nome) NO_FornecePadrao,'
      'IF(cp.Tipo=0, cp.RazaoSocial, cp.Nome) NO_CLientePadrao'
      'FROM carteiras ca'
      'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco'
      'LEFT JOIN entidades en ON en.Codigo=ca.ForneceI'
      'LEFT JOIN entidades eg ON eg.Codigo=ca.EntiDent'
      'LEFT JOIN entidades fp ON fp.Codigo=ca.FornecePadrao'
      'LEFT JOIN entidades cp ON cp.Codigo=ca.ClientePadrao'
      'WHERE ca.Codigo>-1000000')
    Left = 380
    Top = 5
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'carteiras.Codigo'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'carteiras.Tipo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrCarteirasInicial: TFloatField
      FieldName = 'Inicial'
      Origin = 'carteiras.Inicial'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'carteiras.Banco'
    end
    object QrCarteirasID: TWideStringField
      FieldName = 'ID'
      Origin = 'carteiras.ID'
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'carteiras.Fatura'
      Size = 1
    end
    object QrCarteirasID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Origin = 'carteiras.ID_Fat'
      Size = 50
    end
    object QrCarteirasSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'carteiras.Saldo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      Origin = 'carteiras.EmCaixa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
      Origin = 'carteiras.Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
      Origin = 'carteiras.Prazo'
    end
    object QrCarteirasPagRec: TIntegerField
      FieldName = 'PagRec'
      Origin = 'carteiras.PagRec'
    end
    object QrCarteirasDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
      Origin = 'carteiras.DiaMesVence'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
      Origin = 'carteiras.ExigeNumCheque'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
      Origin = 'carteiras.ForneceI'
    end
    object QrCarteirasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'carteiras.Lk'
    end
    object QrCarteirasDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'carteiras.DataCad'
    end
    object QrCarteirasDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'carteiras.DataAlt'
    end
    object QrCarteirasUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'carteiras.UserCad'
    end
    object QrCarteirasUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'carteiras.UserAlt'
    end
    object QrCarteirasNome2: TWideStringField
      FieldName = 'Nome2'
      Origin = 'carteiras.Nome2'
      Size = 100
    end
    object QrCarteirasTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
      Origin = 'carteiras.TipoDoc'
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
      Origin = 'carteiras.Banco1'
    end
    object QrCarteirasAgencia1: TIntegerField
      FieldName = 'Agencia1'
      Origin = 'carteiras.Agencia1'
    end
    object QrCarteirasConta1: TWideStringField
      FieldName = 'Conta1'
      Origin = 'carteiras.Conta1'
      Size = 15
    end
    object QrCarteirasCheque1: TIntegerField
      FieldName = 'Cheque1'
      Origin = 'carteiras.Cheque1'
    end
    object QrCarteirasContato1: TWideStringField
      FieldName = 'Contato1'
      Origin = 'carteiras.Contato1'
      Size = 100
    end
    object QrCarteirasNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 15
      Calculated = True
    end
    object QrCarteirasNOMEDOBANCO: TWideStringField
      FieldName = 'NOMEDOBANCO'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrCarteirasNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Origin = 'NOMEFORNECEI'
      Size = 100
    end
    object QrCarteirasAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'carteiras.Antigo'
    end
    object QrCarteirasContab: TWideStringField
      FieldName = 'Contab'
      Origin = 'carteiras.Contab'
    end
    object QrCarteirasOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'carteiras.Ordem'
      Required = True
    end
    object QrCarteirasForneceN: TSmallintField
      FieldName = 'ForneceN'
      Origin = 'carteiras.ForneceN'
      Required = True
    end
    object QrCarteirasFuturoC: TFloatField
      FieldName = 'FuturoC'
      Origin = 'carteiras.FuturoC'
    end
    object QrCarteirasFuturoD: TFloatField
      FieldName = 'FuturoD'
      Origin = 'carteiras.FuturoD'
    end
    object QrCarteirasFuturoS: TFloatField
      FieldName = 'FuturoS'
      Origin = 'carteiras.FuturoS'
    end
    object QrCarteirasExclusivo: TSmallintField
      FieldName = 'Exclusivo'
      Origin = 'carteiras.Exclusivo'
      Required = True
    end
    object QrCarteirasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'carteiras.AlterWeb'
      Required = True
    end
    object QrCarteirasAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'carteiras.Ativo'
      Required = True
    end
    object QrCarteirasRecebeBloq: TSmallintField
      FieldName = 'RecebeBloq'
      Origin = 'carteiras.RecebeBloq'
    end
    object QrCarteirasEntiDent: TIntegerField
      FieldName = 'EntiDent'
      Origin = 'carteiras.EntiDent'
    end
    object QrCarteirasNOMEENTIDENT: TWideStringField
      FieldName = 'NOMEENTIDENT'
      Size = 100
    end
    object QrCarteirasCodCedente: TWideStringField
      FieldName = 'CodCedente'
    end
    object QrCarteirasIgnorSerie: TSmallintField
      FieldName = 'IgnorSerie'
    end
    object QrCarteirasValMorto: TFloatField
      FieldName = 'ValMorto'
    end
    object QrCarteirasValIniOld: TFloatField
      FieldName = 'ValIniOld'
    end
    object QrCarteirasSdoFimB: TFloatField
      FieldName = 'SdoFimB'
    end
    object QrCarteirasCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrCarteirasNotConcBco: TSmallintField
      FieldName = 'NotConcBco'
    end
    object QrCarteirasMudaBco: TIntegerField
      FieldName = 'MudaBco'
    end
    object QrCarteirasTipCart: TIntegerField
      FieldName = 'TipCart'
    end
    object QrCarteirasNO_FornecePadrao: TWideStringField
      FieldName = 'NO_FornecePadrao'
      Size = 100
    end
    object QrCarteirasNO_ClientePadrao: TWideStringField
      FieldName = 'NO_ClientePadrao'
      Size = 100
    end
    object QrCarteirasFornecePAdrao: TIntegerField
      FieldName = 'FornecePAdrao'
    end
    object QrCarteirasClientePadrao: TIntegerField
      FieldName = 'ClientePadrao'
    end
    object QrCarteirasFechaDia: TSmallintField
      FieldName = 'FechaDia'
    end
    object QrCarteirasGenCtb: TIntegerField
      FieldName = 'GenCtb'
    end
    object QrCarteirasNO_GenCtb: TWideStringField
      FieldName = 'NO_GenCtb'
      Size = 60
    end
  end
  object PMCarteira: TPopupMenu
    Left = 116
    Top = 220
    object Caixa1: TMenuItem
      Caption = '&Caixa (Dinheiro em esp'#233'cie)'
      OnClick = Caixa1Click
    end
    object Banco1: TMenuItem
      Caption = '&Banco (Conta banc'#225'ria)'
      OnClick = Banco1Click
    end
    object Emisso1: TMenuItem
      Caption = '&Emiss'#227'o (Cheque, duplicata, etc.)'
      OnClick = Emisso1Click
    end
  end
  object QrBancos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE Tipo=1'
      'AND ForneceI=:P0'
      'ORDER BY Nome')
    Left = 488
    Top = 108
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 516
    Top = 108
  end
  object QrForneceI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NomeENTIDADE')
    Left = 88
    Top = 180
    object QrForneceICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceINOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsForneceI: TDataSource
    DataSet = QrForneceI
    Left = 116
    Top = 180
  end
  object QrEntiDent: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 24
    Top = 400
    object QrEntiDentCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiDentNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsEntiDent: TDataSource
    DataSet = QrEntiDent
    Left = 24
    Top = 448
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = EdCliInt
    CanUpd01 = Panel7
    CanDel01 = PnFast
    Left = 284
    Top = 12
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCliIntBeforeClose
    AfterScroll = QrCliIntAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo, ent.CliInt,'
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMECLI'
      'FROM entidades ent'
      'LEFT JOIN carteiras car ON car.ForneceI=ent.Codigo'
      'WHERE ent.CliInt<>0'
      'OR car.ForneceI <> 0')
    Left = 88
    Top = 144
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
    object QrCliIntCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 116
    Top = 144
  end
  object QrCliCart: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCliCartAfterScroll
    SQL.Strings = (
      'SELECT car.Codigo, car.Tipo, car.Nome,'
      'ELT(car.Tipo+1,"Caixa","C/C Banco",'
      '"Emiss'#227'o") NOME_TIPO, '
      'IF(car.Banco=0,"",bco.Nome) NOMEBCO,'
      'car.Banco1, car.Agencia1, car.Conta1,'
      'car.Inicial, car.Saldo'
      'FROM carteiras car'
      'LEFT JOIN carteiras bco ON bco.Codigo=car.Banco'
      'WHERE car.ForneceI=1')
    Left = 496
    Top = 4
    object QrCliCartCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliCartTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCliCartNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCliCartNOME_TIPO: TWideStringField
      FieldName = 'NOME_TIPO'
      Size = 9
    end
    object QrCliCartNOMEBCO: TWideStringField
      FieldName = 'NOMEBCO'
      Size = 100
    end
    object QrCliCartBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCliCartAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrCliCartConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCliCartInicial: TFloatField
      FieldName = 'Inicial'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCliCartSaldo: TFloatField
      FieldName = 'Saldo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsCliCart: TDataSource
    DataSet = QrCliCart
    Left = 524
    Top = 4
  end
  object PMUsuario: TPopupMenu
    Left = 790
    Top = 614
    object Adiciona1: TMenuItem
      Caption = '&Adiciona'
      OnClick = Adiciona1Click
    end
    object Retira1: TMenuItem
      Caption = '&Retira'
      OnClick = Retira1Click
    end
  end
  object QrCarteirasU: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cpu.Controle, cpu.Usuario, '
      'sen.Login, sen.Funcionario, '
      'IF(ent.Tipo=0, RazaoSocial, Nome) NOMEUSU'
      'FROM carteirasu cpu'
      'LEFT JOIN senhas sen ON cpu.Usuario=sen.Numero'
      'LEFT JOIN entidades ent ON ent.Codigo=sen.Funcionario'
      'WHERE cpu.Codigo=:P0')
    Left = 436
    Top = 5
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasUControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCarteirasUUsuario: TIntegerField
      FieldName = 'Usuario'
      Required = True
    end
    object QrCarteirasULogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
    object QrCarteirasUFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
    object QrCarteirasUNOMEUSU: TWideStringField
      FieldName = 'NOMEUSU'
      Size = 100
    end
  end
  object DsCarteirasU: TDataSource
    DataSet = QrCarteirasU
    Left = 464
    Top = 5
  end
  object QrLan: TMySQLQuery
    Database = Dmod.MyDB
    Left = 88
    Top = 220
    object QrLanCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object QrJaIns: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _qtd_reg_lct;'
      ''
      'CREATE TABLE _qtd_reg_lct'
      'SELECT COUNT(Controle) Itens'
      'FROM exul2.lct0001a'
      'WHERE Carteira > 0'
      'UNION'
      'SELECT COUNT(Controle) Itens'
      'FROM exul2.lct0001b'
      'WHERE Carteira > 0'
      'UNION'
      'SELECT COUNT(Controle) Itens'
      'FROM exul2.lct0001d'
      'WHERE Carteira > 0;'
      ''
      'SELECT SUM(Itens) Itens '
      'FROM _qtd_reg_lct;'
      '')
    Left = 132
    Top = 340
    object QrJaInsItens: TFloatField
      FieldName = 'Itens'
    end
  end
  object PMTaloes: TPopupMenu
    OnPopup = PMTaloesPopup
    Left = 700
    Top = 620
    object Incluinovotalo1: TMenuItem
      Caption = 'Inclui novo tal'#227'o'
      OnClick = Incluinovotalo1Click
    end
    object Alterataloatual1: TMenuItem
      Caption = '&Altera tal'#227'o atual'
      OnClick = Alterataloatual1Click
    end
    object Excluitalo1: TMenuItem
      Caption = '&Exclui tal'#227'o'
      OnClick = Excluitalo1Click
    end
  end
  object QrCartTalCH: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM carttalch'
      'WHERE Codigo=:P0')
    Left = 88
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCartTalCHCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartTalCHControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCartTalCHSerie: TWideStringField
      FieldName = 'Serie'
      Size = 10
    end
    object QrCartTalCHNumIni: TIntegerField
      FieldName = 'NumIni'
    end
    object QrCartTalCHNumFim: TIntegerField
      FieldName = 'NumFim'
    end
    object QrCartTalCHNumAtu: TIntegerField
      FieldName = 'NumAtu'
    end
  end
  object DsCartTalCH: TDataSource
    DataSet = QrCartTalCH
    Left = 116
    Top = 300
  end
  object QrUsaTalao: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(Controle) Taloes'
      'FROM carttalch'
      'WHERE Codigo=:P0')
    Left = 100
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUsaTalaoTaloes: TLargeintField
      FieldName = 'Taloes'
      Required = True
    end
  end
  object QrFornecePadrao: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 108
    Top = 400
    object QrFornecePadraoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecePadraoNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsFornecePadrao: TDataSource
    DataSet = QrFornecePadrao
    Left = 108
    Top = 448
  end
  object QrClientePadrao: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 228
    Top = 400
    object QrClientePadraoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientePadraoNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsClientePadrao: TDataSource
    DataSet = QrClientePadrao
    Left = 228
    Top = 448
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome')
    Left = 492
    Top = 396
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 492
    Top = 444
  end
end
