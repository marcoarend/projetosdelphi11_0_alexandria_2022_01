unit GetDataECartBco;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, dmkEdit, dmkGeral,
  dmkEditDateTimePicker, dmkImage, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEditCB, UnDmkEnums;

type
  TFmGetDataECartBco = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    TPData: TdmkEditDateTimePicker;
    LaHora: TLabel;
    EdHora: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    Label2: TLabel;
    QrCarteiras: TMySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FSelecionou: Boolean;
    FMinData: TDateTime;
  end;

  var
  FmGetDataECartBco: TFmGetDataECartBco;

implementation

uses UnMyObjects, UnInternalConsts, DmkDAC_PF, Module;

{$R *.DFM}

procedure TFmGetDataECartBco.BtSaidaClick(Sender: TObject);
begin
  FSelecionou := False;
  VAR_GETDATA := 0;
  VAR_GETCARTBCO := 0;
  //
  Close;
end;

procedure TFmGetDataECartBco.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  MyObjects.CorIniComponente();
end;

procedure TFmGetDataECartBco.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGetDataECartBco.FormCreate(Sender: TObject);
begin
  FSelecionou := False;
  VAR_GETDATA := 0;
  VAR_GETCARTBCO := 0;
  TPData.MinDate := VAR_DATA_MINIMA;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCarteiras, Dmod.MyDB, [
  'SELECT Codigo, Nome',
  'FROM carteiras',
  'WHERE Tipo=1',
  'AND Codigo > 0',
  'ORDER BY Nome',
  '']);
end;

procedure TFmGetDataECartBco.BtOKClick(Sender: TObject);
begin
  FSelecionou := True;
  VAR_GETDATA := Int(TPData.Date) + EdHora.ValueVariant;
  VAR_GETCARTBCO := EdCarteira.ValueVariant;
  //
  Close;
end;

end.
