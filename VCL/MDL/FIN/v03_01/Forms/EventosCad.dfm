object FmEventosCad: TFmEventosCad
  Left = 368
  Top = 194
  Caption = 'EVE-GEREN-001 :: Gerenciamento de Eventos'
  ClientHeight = 564
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 516
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 152
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 64
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 4
        Top = 136
        Width = 110
        Height = 13
        Caption = 'Hist'#243'rico de atividades:'
      end
      object Label6: TLabel
        Left = 4
        Top = 96
        Width = 29
        Height = 13
        Caption = 'Local:'
        FocusControl = DBEdNome
      end
      object Label11: TLabel
        Left = 404
        Top = 56
        Width = 121
        Height = 13
        Caption = 'Cliente Interno (entidade):'
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 148
        Top = 20
        Width = 585
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object CkAtivo: TdmkCheckBox
        Left = 736
        Top = 24
        Width = 45
        Height = 17
        Caption = 'Ativo.'
        TabOrder = 3
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object EdLocal: TdmkEdit
        Left = 4
        Top = 112
        Width = 777
        Height = 21
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Local'
        UpdCampo = 'Local'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object GroupBox1: TGroupBox
        Left = 4
        Top = 44
        Width = 197
        Height = 49
        Caption = ' Data e hora de in'#237'cio: '
        TabOrder = 4
        object TPDadtaIni: TdmkEditDateTimePicker
          Left = 8
          Top = 20
          Width = 116
          Height = 21
          Date = 40137.793551006940000000
          Time = 40137.793551006940000000
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataIni'
          UpdCampo = 'DataIni'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdHoraIni: TdmkEdit
          Left = 128
          Top = 20
          Width = 60
          Height = 21
          TabOrder = 1
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfLong
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'HoraIni'
          UpdCampo = 'HoraIni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox2: TGroupBox
        Left = 204
        Top = 44
        Width = 197
        Height = 49
        Caption = ' Data e hora de t'#233'rmino: '
        TabOrder = 5
        object TPDataFim: TdmkEditDateTimePicker
          Left = 8
          Top = 20
          Width = 116
          Height = 21
          Date = 40137.793551006940000000
          Time = 40137.793551006940000000
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataFim'
          UpdCampo = 'DataFim'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdHoraFim: TdmkEdit
          Left = 128
          Top = 20
          Width = 60
          Height = 21
          TabOrder = 1
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfLong
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'HoraFim'
          UpdCampo = 'HoraFim'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object EdCliInt: TdmkEditCB
        Left = 404
        Top = 71
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Entidade'
        UpdCampo = 'Entidade'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 460
        Top = 71
        Width = 320
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsCliInt
        TabOrder = 7
        dmkEditCB = EdCliInt
        QryName = 'Entidade'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object MeHistorico: TdmkMemo
      Left = 1
      Top = 153
      Width = 790
      Height = 89
      Align = alTop
      ScrollBars = ssBoth
      TabOrder = 1
      QryCampo = 'Historico'
      UpdCampo = 'Historico'
      UpdType = utYes
    end
    object GBConfirma: TGroupBox
      Left = 1
      Top = 452
      Width = 790
      Height = 63
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 650
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 52
    Width = 792
    Height = 512
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 1
      Top = 341
      Width = 790
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      ExplicitLeft = 9
      ExplicitTop = 216
    end
    object PainelData: TPanel
      Left = 1
      Top = 45
      Width = 790
      Height = 152
      Align = alTop
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 64
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 4
        Top = 136
        Width = 110
        Height = 13
        Caption = 'Hist'#243'rico de atividades:'
      end
      object Label10: TLabel
        Left = 4
        Top = 96
        Width = 29
        Height = 13
        Caption = 'Local:'
        FocusControl = DBEdNome
      end
      object Label13: TLabel
        Left = 404
        Top = 48
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
        FocusControl = DBEdit8
      end
      object DBEdCodigo: TDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsEventosCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 148
        Top = 20
        Width = 585
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsEventosCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsEventosCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object GroupBox3: TGroupBox
        Left = 4
        Top = 44
        Width = 197
        Height = 49
        Caption = ' Data e hora de in'#237'cio: '
        TabOrder = 3
        object DBEdit3: TDBEdit
          Left = 8
          Top = 20
          Width = 180
          Height = 21
          DataField = 'DH_INI'
          DataSource = DsEventosCad
          TabOrder = 0
        end
      end
      object DBCheckBox1: TDBCheckBox
        Left = 736
        Top = 24
        Width = 45
        Height = 17
        Caption = 'Ativo.'
        DataField = 'Ativo'
        DataSource = DsEventosCad
        TabOrder = 4
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit2: TDBEdit
        Left = 4
        Top = 112
        Width = 777
        Height = 21
        DataField = 'Local'
        DataSource = DsEventosCad
        TabOrder = 5
      end
      object GroupBox4: TGroupBox
        Left = 204
        Top = 44
        Width = 197
        Height = 49
        Caption = ' Data e hora de t'#233'rmino: '
        TabOrder = 6
        object DBEdit6: TDBEdit
          Left = 8
          Top = 20
          Width = 180
          Height = 21
          DataField = 'DH_FIM'
          DataSource = DsEventosCad
          TabOrder = 0
        end
      end
      object DBEdit7: TDBEdit
        Left = 460
        Top = 64
        Width = 321
        Height = 21
        DataField = 'NOMECLIINT'
        DataSource = DsEventosCad
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 404
        Top = 64
        Width = 56
        Height = 21
        DataField = 'Entidade'
        DataSource = DsEventosCad
        TabOrder = 8
      end
    end
    object MeDBHistorico: TDBMemo
      Left = 1
      Top = 197
      Width = 790
      Height = 52
      Align = alTop
      DataField = 'Historico'
      DataSource = DsEventosCad
      TabOrder = 1
    end
    object DBGLct: TdmkDBGridZTO
      Left = 1
      Top = 344
      Width = 790
      Height = 103
      Align = alBottom
      DataSource = DsLct1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Documento'
          Title.Caption = 'Docum.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Title.Caption = 'Nota fiscal'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CO_TERCEIRO'
          Title.Caption = 'Terceiro'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_TERCEIRO'
          Title.Caption = 'Nome terceiros'
          Width = 300
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataDoc'
          Title.Caption = 'Data doc.'
          Width = 56
          Visible = True
        end>
    end
    object GBCntrl: TGroupBox
      Left = 1
      Top = 447
      Width = 790
      Height = 64
      Align = alBottom
      TabOrder = 3
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 267
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtEvento: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Evento'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtEventoClick
        end
        object BtLct: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lancto'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtLctClick
        end
      end
    end
    object GBAvisos1: TGroupBox
      Left = 1
      Top = 1
      Width = 790
      Height = 44
      Align = alTop
      Caption = ' Avisos: '
      TabOrder = 4
      object Panel25: TPanel
        Left = 2
        Top = 15
        Width = 786
        Height = 27
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 329
        Height = 32
        Caption = 'Gerenciamento de Eventos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 329
        Height = 32
        Caption = 'Gerenciamento de Eventos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 329
        Height = 32
        Caption = 'Gerenciamento de Eventos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DsEventosCad: TDataSource
    DataSet = QrEventosCad
    Left = 28
    Top = 320
  end
  object QrEventosCad: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrEventosCadBeforeOpen
    AfterOpen = QrEventosCadAfterOpen
    BeforeClose = QrEventosCadBeforeClose
    AfterScroll = QrEventosCadAfterScroll
    SQL.Strings = (
      'SELECT '
      'CONCAT(DATE_FORMAT(eve.DataIni, "%d/%m/%y"), " ",'
      'DATE_FORMAT(eve.HoraIni, "%H:%i:%s")) DH_INI,'
      ''
      'CONCAT(DATE_FORMAT(eve.DataFim, "%d/%m/%y"), " ",'
      'DATE_FORMAT(eve.HoraFim, "%H:%i:%s")) DH_FIM,'
      ''
      'eve.*, IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMECLIINT'
      'FROM eventoscad eve'
      'LEFT JOIN entidades ent ON ent.Codigo= eve.Entidade'
      'WHERE eve.Codigo > 0')
    Left = 32
    Top = 276
    object QrEventosCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'eventoscad.Codigo'
    end
    object QrEventosCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'eventoscad.CodUsu'
    end
    object QrEventosCadNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'eventoscad.Nome'
      Size = 255
    end
    object QrEventosCadDataIni: TDateField
      FieldName = 'DataIni'
      Origin = 'eventoscad.DataIni'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrEventosCadHoraIni: TTimeField
      FieldName = 'HoraIni'
      Origin = 'eventoscad.HoraIni'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrEventosCadDataFim: TDateField
      FieldName = 'DataFim'
      Origin = 'eventoscad.DataFim'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrEventosCadHoraFim: TTimeField
      FieldName = 'HoraFim'
      Origin = 'eventoscad.HoraFim'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrEventosCadLocal: TWideStringField
      FieldName = 'Local'
      Origin = 'eventoscad.Local'
      Size = 255
    end
    object QrEventosCadHistorico: TWideMemoField
      FieldName = 'Historico'
      Origin = 'eventoscad.Historico'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrEventosCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'eventoscad.Ativo'
    end
    object QrEventosCadEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrEventosCadNOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Size = 100
    end
    object QrEventosCadDH_INI: TWideStringField
      FieldName = 'DH_INI'
      Size = 22
    end
    object QrEventosCadDH_FIM: TWideStringField
      FieldName = 'DH_FIM'
      Size = 22
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtEvento
    CanUpd01 = BtSaida
    Left = 48
    Top = 48
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Codigo < -10'
      'OR CliInt > 0'
      'ORDER BY NomeENTIDADE')
    Left = 588
    Top = 12
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliIntNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 588
    Top = 60
  end
  object QrLct1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan.Data, lan.Vencimento, lan.Controle, '
      'lan.Descricao, lan.NotaFiscal, lan.Debito, '
      'lan.Credito, lan.Documento, IF(lan.Credito>0,'
      '  lan.Cliente, lan.Fornecedor) + 0.000 CO_TERCEIRO,'
      'IF(Credito>0,'
      '  IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome),'
      '  IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)'
      ') NO_TERCEIRO,'
      'lan.DataDoc, lan.Sub'
      'FROM lanctos lan'
      'LEFT JOIN entidades frn ON frn.Codigo=lan.Fornecedor'
      'LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente'
      'WHERE eventoscad=:P0'
      'ORDER BY Data, Controle')
    Left = 324
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLct1Data: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLct1Vencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLct1Controle: TIntegerField
      FieldName = 'Controle'
      DisplayFormat = '000000;-000000; '
    end
    object QrLct1Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLct1NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrLct1Debito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLct1Credito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLct1Documento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000;-000000; '
    end
    object QrLct1CO_TERCEIRO: TFloatField
      FieldName = 'CO_TERCEIRO'
    end
    object QrLct1NO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrLct1DataDoc: TDateField
      FieldName = 'DataDoc'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLct1Sub: TSmallintField
      FieldName = 'Sub'
    end
  end
  object DsLct1: TDataSource
    DataSet = QrLct1
    Left = 352
    Top = 168
  end
  object frxEvento1: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.407328206020000000
    ReportOptions.LastChange = 39949.407328206020000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxEvento1GetValue
    Left = 352
    Top = 252
    Datasets = <
      item
      end
      item
        DataSet = frxDsEventosCad
        DataSetName = 'frxDsEventosCad'
      end
      item
        DataSet = frxDsLct1
        DataSetName = 'frxDsLct1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader5: TfrxPageHeader
        FillType = ftBrush
        Height = 117.165420240000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Shape4: TfrxShapeView
          Width = 1046.929810000000000000
          Height = 45.354360000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo104: TfrxMemoView
          Left = 7.559060000000000000
          Width = 1031.811690000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo105: TfrxMemoView
          Left = 170.078850000000000000
          Top = 18.897650000000000000
          Width = 725.669760000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEventosCad."CodUsu"] - [frxDsEventosCad."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 162.519790000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          Left = 895.748610000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          Left = 7.559060000000000000
          Top = 45.354360000000000000
          Width = 1009.134510000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEventosCad."NOMECLIINT"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Top = 102.047310000000000000
          Width = 45.354330708661420000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 45.354360000000000000
          Top = 102.047310000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencim.')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 90.708720000000000000
          Top = 102.047310000000000000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 147.401670000000000000
          Top = 102.047310000000000000
          Width = 427.086890000000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 574.488560000000000000
          Top = 102.047310000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Nota fiscal')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 634.961040000000000000
          Top = 102.047310000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 702.992580000000000000
          Top = 102.047310000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 771.024120000000000000
          Top = 102.047310000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 831.496600000000000000
          Top = 102.047310000000000000
          Width = 215.433210000000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo - Entidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 7.559060000000000000
          Top = 64.252010000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Local:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 64.252010000000000000
          Top = 64.252010000000000000
          Width = 952.441560000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEventosCad."Local"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 7.559060000000000000
          Top = 83.149660000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 64.252010000000000000
          Top = 83.149660000000000000
          Width = 952.441520940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 196.535560000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsLct1
        DataSetName = 'frxDsLct1'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 45.354330708661420000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLct1."Data"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 45.354360000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Vencimento'
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLct1."Vencimento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 90.708720000000000000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct1."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 147.401670000000000000
          Width = 427.086890000000000000
          Height = 15.118110240000000000
          DataField = 'Descricao'
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLct1."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 574.488560000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'NotaFiscal'
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct1."NotaFiscal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 634.961040000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataField = 'Credito'
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct1."Credito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 702.992580000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataField = 'Debito'
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct1."Debito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 771.024120000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Documento'
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct1."Documento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 831.496600000000000000
          Width = 215.433210000000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLct1."CO_TERCEIRO"] - [frxDsLct1."NO_TERCEIRO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 317.480520000000000000
        Width = 1046.929810000000000000
        object Memo120: TfrxMemoView
          Width = 1046.929810000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 272.126160000000000000
        Width = 1046.929810000000000000
        object Memo9: TfrxMemoView
          Left = 634.961040000000000000
          Top = 3.779530000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct1."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 702.992580000000000000
          Top = 3.779530000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct1."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 771.024120000000000000
          Top = 3.779530000000000000
          Width = 207.874110940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'RESULTADO: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 978.898270000000000000
          Top = 3.779530000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct1."Credito">) - SUM(<frxDsLct1."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Top = 3.779530000000000000
          Width = 634.961000940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAIS: ')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsEventosCad: TfrxDBDataset
    UserName = 'frxDsEventosCad'
    CloseDataSource = False
    DataSet = QrEventosCad
    BCDToCurrency = False
    Left = 380
    Top = 252
  end
  object frxDsLct1: TfrxDBDataset
    UserName = 'frxDsLct1'
    CloseDataSource = False
    DataSet = QrLct1
    BCDToCurrency = False
    Left = 380
    Top = 168
  end
  object frxEvento2: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.407328206020000000
    ReportOptions.LastChange = 39949.407328206020000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxEvento1GetValue
    Left = 352
    Top = 224
    Datasets = <
      item
      end
      item
        DataSet = frxDsEventosCad
        DataSetName = 'frxDsEventosCad'
      end
      item
        DataSet = frxDsLct2
        DataSetName = 'frxDsLct2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader5: TfrxPageHeader
        FillType = ftBrush
        Height = 117.165420240000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Shape4: TfrxShapeView
          Width = 1046.929810000000000000
          Height = 45.354360000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo104: TfrxMemoView
          Left = 7.559060000000000000
          Width = 1031.811690000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo105: TfrxMemoView
          Left = 170.078850000000000000
          Top = 18.897650000000000000
          Width = 725.669760000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEventosCad."CodUsu"] - [frxDsEventosCad."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 162.519790000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          Left = 895.748610000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          Left = 7.559060000000000000
          Top = 45.354360000000000000
          Width = 1009.134510000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEventosCad."NOMECLIINT"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Top = 102.047310000000000000
          Width = 45.354330708661420000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 45.354360000000000000
          Top = 102.047310000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencim.')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 90.708720000000000000
          Top = 102.047310000000000000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 147.401670000000000000
          Top = 102.047310000000000000
          Width = 427.086890000000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 574.488560000000000000
          Top = 102.047310000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Nota fiscal')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 634.961040000000000000
          Top = 102.047310000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 702.992580000000000000
          Top = 102.047310000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 771.024120000000000000
          Top = 102.047310000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 831.496600000000000000
          Top = 102.047310000000000000
          Width = 215.433210000000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo - Entidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 7.559060000000000000
          Top = 64.252010000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Local:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 64.252010000000000000
          Top = 64.252010000000000000
          Width = 952.441560000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEventosCad."Local"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 7.559060000000000000
          Top = 83.149660000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 64.252010000000000000
          Top = 83.149660000000000000
          Width = 952.441520940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 291.023810000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsLct2
        DataSetName = 'frxDsLct2'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 45.354330708661420000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = frxDsLct2
          DataSetName = 'frxDsLct2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLct2."Data"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 45.354360000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Vencimento'
          DataSet = frxDsLct2
          DataSetName = 'frxDsLct2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLct2."Vencimento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 90.708720000000000000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsLct2
          DataSetName = 'frxDsLct2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct2."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 147.401670000000000000
          Width = 427.086890000000000000
          Height = 15.118110240000000000
          DataField = 'Descricao'
          DataSet = frxDsLct2
          DataSetName = 'frxDsLct2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLct2."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 574.488560000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'NotaFiscal'
          DataSet = frxDsLct2
          DataSetName = 'frxDsLct2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct2."NotaFiscal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 634.961040000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataField = 'Credito'
          DataSet = frxDsLct2
          DataSetName = 'frxDsLct2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct2."Credito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 702.992580000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataField = 'Debito'
          DataSet = frxDsLct2
          DataSetName = 'frxDsLct2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct2."Debito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 771.024120000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Documento'
          DataSet = frxDsLct2
          DataSetName = 'frxDsLct2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct2."Documento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 831.496600000000000000
          Width = 215.433210000000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLct2."CO_TERCEIRO"] - [frxDsLct2."NO_TERCEIRO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 514.016080000000000000
        Width = 1046.929810000000000000
        object Memo120: TfrxMemoView
          Width = 1046.929810000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 468.661720000000000000
        Width = 1046.929810000000000000
        object Memo9: TfrxMemoView
          Left = 634.961040000000000000
          Top = 3.779530000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct2."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 702.992580000000000000
          Top = 3.779530000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct2."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 771.024120000000000000
          Top = 3.779530000000000000
          Width = 207.874110940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'RESULTADO: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 978.898270000000000000
          Top = 3.779530000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct2."Credito">) - SUM(<frxDsLct2."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Top = 3.779530000000000000
          Width = 634.961000940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAIS: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 196.535560000000000000
        Width = 1046.929810000000000000
        Condition = 'frxDsLct2."NO_SUBGRUPO"'
        object Memo28: TfrxMemoView
          Top = 3.779530000000000000
          Width = 1046.929810000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsLct2."SubGrupo"] - [frxDsLct2."NO_SUBGRUPO"]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 245.669450000000000000
        Width = 1046.929810000000000000
        Condition = 'frxDsLct2."NO_CONTA"'
        object Memo34: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779530000000000000
          Width = 1009.134510000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsLct2."Genero"] - [frxDsLct2."NO_CONTA"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 328.819110000000000000
        Width = 1046.929810000000000000
        object Memo36: TfrxMemoView
          Left = 634.961040000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct2."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 702.992580000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct2."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          Left = 771.024120000000000000
          Width = 207.874110940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'RESULTADO: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          Left = 978.898270000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct2."Credito">) - SUM(<frxDsLct2."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 374.173470000000000000
        Width = 1046.929810000000000000
        object Memo30: TfrxMemoView
          Left = 634.961040000000000000
          Top = 3.779530000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct2."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 702.992580000000000000
          Top = 3.779530000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct2."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 771.024120000000000000
          Top = 3.779530000000000000
          Width = 207.874110940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'RESULTADO: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 978.898270000000000000
          Top = 3.779530000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct2."Credito">) - SUM(<frxDsLct2."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object QrLct2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan.Data, lan.Vencimento, lan.Controle, '
      'lan.Descricao, lan.NotaFiscal, lan.Debito, '
      'lan.Credito, lan.Documento, IF(lan.Credito>0,'
      '  lan.Cliente, lan.Fornecedor) CO_TERCEIRO,'
      'IF(lan.Credito>0,'
      '  IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome),'
      '  IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)'
      ') NO_TERCEIRO, lan.Genero, cta.Nome NO_CONTA,'
      'cta.SubGrupo, sgr.Nome NO_SUBGRUPO'
      'FROM lanctos lan'
      'LEFT JOIN entidades frn ON frn.Codigo=lan.Fornecedor'
      'LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente'
      'LEFT JOIN contas cta ON cta.Codigo=lan.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo'
      'WHERE eventoscad=:P0'
      'ORDER BY sgr.OrdemLista, sgr.Nome, cta.OrdemLista, '
      'cta.Nome, lan.Data, lan.Controle'
      '')
    Left = 324
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLct2Data: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLct2Vencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLct2Controle: TIntegerField
      FieldName = 'Controle'
      DisplayFormat = '000000;-000000; '
    end
    object QrLct2Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLct2NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrLct2Debito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLct2Credito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLct2Documento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000;-000000; '
    end
    object QrLct2CO_TERCEIRO: TLargeintField
      FieldName = 'CO_TERCEIRO'
    end
    object QrLct2NO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrLct2Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLct2NO_CONTA: TWideStringField
      FieldName = 'NO_CONTA'
      Size = 50
    end
    object QrLct2SubGrupo: TIntegerField
      FieldName = 'SubGrupo'
    end
    object QrLct2NO_SUBGRUPO: TWideStringField
      FieldName = 'NO_SUBGRUPO'
      Size = 50
    end
  end
  object frxDsLct2: TfrxDBDataset
    UserName = 'frxDsLct2'
    CloseDataSource = False
    DataSet = QrLct2
    BCDToCurrency = False
    Left = 380
    Top = 224
  end
  object PMImprime: TPopupMenu
    Left = 12
    Top = 65532
    object Resultadoporlistagememordemdedata1: TMenuItem
      Caption = 'Resultado com &Listagem por ordem de data'
      OnClick = Resultadoporlistagememordemdedata1Click
    end
    object ResultadoporSubgruposecontas1: TMenuItem
      Caption = 'Resultado por &Subgrupos e Contas'
      OnClick = ResultadoporSubgruposecontas1Click
    end
    object ResultadoporGruposeSuggrupos1: TMenuItem
      Caption = 'Resultado por Grupos e Sug-grupos'
      OnClick = ResultadoporGruposeSuggrupos1Click
    end
  end
  object QrLct3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan.Data, lan.Vencimento, lan.Controle, '
      'lan.Descricao, lan.NotaFiscal, lan.Debito, '
      'lan.Credito, lan.Documento, IF(lan.Credito>0,'
      '  lan.Cliente, lan.Fornecedor) CO_TERCEIRO,'
      'IF(lan.Credito>0,'
      '  IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome),'
      '  IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)'
      ') NO_TERCEIRO, lan.Genero, cta.Nome NO_CONTA,'
      'cta.SubGrupo, sgr.Nome NO_SUBGRUPO,'
      'sgr.Grupo, gru.Nome NO_GRUPO'
      'FROM lanctos lan'
      'LEFT JOIN entidades frn ON frn.Codigo=lan.Fornecedor'
      'LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente'
      'LEFT JOIN contas cta ON cta.Codigo=lan.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo'
      'LEFT JOIN grupos gru ON gru.Codigo=sgr.Grupo'
      'WHERE eventoscad=:P0'
      'ORDER BY gru.OrdemLista, gru.Nome, '
      'sgr.OrdemLista, sgr.Nome, cta.OrdemLista, '
      'cta.Nome, lan.Data, lan.Controle'
      '')
    Left = 324
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object DateField1: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object DateField2: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object IntegerField1: TIntegerField
      FieldName = 'Controle'
      DisplayFormat = '000000;-000000; '
    end
    object StringField1: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object IntegerField2: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object FloatField1: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object FloatField2: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object FloatField3: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000;-000000; '
    end
    object LargeintField1: TLargeintField
      FieldName = 'CO_TERCEIRO'
    end
    object StringField2: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object IntegerField3: TIntegerField
      FieldName = 'Genero'
    end
    object StringField3: TWideStringField
      FieldName = 'NO_CONTA'
      Size = 50
    end
    object IntegerField4: TIntegerField
      FieldName = 'SubGrupo'
    end
    object StringField4: TWideStringField
      FieldName = 'NO_SUBGRUPO'
      Size = 50
    end
    object QrLct3Grupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrLct3NO_GRUPO: TWideStringField
      FieldName = 'NO_GRUPO'
      Size = 50
    end
  end
  object frxEvento3: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.407328206020000000
    ReportOptions.LastChange = 39949.407328206020000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxEvento1GetValue
    Left = 352
    Top = 196
    Datasets = <
      item
      end
      item
        DataSet = frxDsEventosCad
        DataSetName = 'frxDsEventosCad'
      end
      item
        DataSet = frxDsLct3
        DataSetName = 'frxDsLct3'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader5: TfrxPageHeader
        FillType = ftBrush
        Height = 117.165420240000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Shape4: TfrxShapeView
          Width = 1046.929810000000000000
          Height = 45.354360000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo104: TfrxMemoView
          Left = 7.559060000000000000
          Width = 1031.811690000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo105: TfrxMemoView
          Left = 170.078850000000000000
          Top = 18.897650000000000000
          Width = 725.669760000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEventosCad."CodUsu"] - [frxDsEventosCad."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 162.519790000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          Left = 895.748610000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          Left = 7.559060000000000000
          Top = 45.354360000000000000
          Width = 1009.134510000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEventosCad."NOMECLIINT"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Top = 102.047310000000000000
          Width = 45.354330708661420000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 45.354360000000000000
          Top = 102.047310000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencim.')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 90.708720000000000000
          Top = 102.047310000000000000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 147.401670000000000000
          Top = 102.047310000000000000
          Width = 427.086890000000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 574.488560000000000000
          Top = 102.047310000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Nota fiscal')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 634.961040000000000000
          Top = 102.047310000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 702.992580000000000000
          Top = 102.047310000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 771.024120000000000000
          Top = 102.047310000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 831.496600000000000000
          Top = 102.047310000000000000
          Width = 215.433210000000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo - Entidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 7.559060000000000000
          Top = 64.252010000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Local:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 64.252010000000000000
          Top = 64.252010000000000000
          Width = 952.441560000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEventosCad."Local"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 7.559060000000000000
          Top = 83.149660000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 64.252010000000000000
          Top = 83.149660000000000000
          Width = 952.441520940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 291.023810000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsLct3
        DataSetName = 'frxDsLct3'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 45.354330708661420000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = frxDsLct3
          DataSetName = 'frxDsLct3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLct3."Data"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 45.354360000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Vencimento'
          DataSet = frxDsLct3
          DataSetName = 'frxDsLct3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLct3."Vencimento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 90.708720000000000000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsLct3
          DataSetName = 'frxDsLct3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct3."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 147.401670000000000000
          Width = 427.086890000000000000
          Height = 15.118110240000000000
          DataField = 'Descricao'
          DataSet = frxDsLct3
          DataSetName = 'frxDsLct3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLct3."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 574.488560000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'NotaFiscal'
          DataSet = frxDsLct3
          DataSetName = 'frxDsLct3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct3."NotaFiscal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 634.961040000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataField = 'Credito'
          DataSet = frxDsLct3
          DataSetName = 'frxDsLct3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct3."Credito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 702.992580000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataField = 'Debito'
          DataSet = frxDsLct3
          DataSetName = 'frxDsLct3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct3."Debito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 771.024120000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Documento'
          DataSet = frxDsLct3
          DataSetName = 'frxDsLct3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct3."Documento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 831.496600000000000000
          Width = 215.433210000000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLct3."CO_TERCEIRO"] - [frxDsLct3."NO_TERCEIRO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 540.472790000000000000
        Width = 1046.929810000000000000
        object Memo120: TfrxMemoView
          Width = 1046.929810000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 495.118430000000000000
        Width = 1046.929810000000000000
        object Memo9: TfrxMemoView
          Left = 634.961040000000000000
          Top = 3.779530000000022000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct3."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 702.992580000000000000
          Top = 3.779530000000022000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct3."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 771.024120000000000000
          Top = 3.779530000000022000
          Width = 207.874110940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'RESULTADO: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 978.898270000000000000
          Top = 3.779530000000022000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct3."Credito">) - SUM(<frxDsLct3."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Top = 3.779530000000022000
          Width = 634.961000940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAIS: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 196.535560000000000000
        Width = 1046.929810000000000000
        Condition = 'frxDsLct3."NO_GRUPO"'
        object Memo28: TfrxMemoView
          Top = 3.779529999999994000
          Width = 1046.929810000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsLct3."Grupo"] - [frxDsLct3."NO_GRUPO"]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 245.669450000000000000
        Width = 1046.929810000000000000
        Condition = 'frxDsLct3."NO_SUBGRUPO"'
        object Memo34: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779529999999994000
          Width = 1009.134510000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsLct3."SubGrupo"] - [frxDsLct3."NO_SUBGRUPO"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 328.819110000000000000
        Width = 1046.929810000000000000
        object Memo36: TfrxMemoView
          Left = 634.961040000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct3."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 702.992580000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct3."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          Left = 771.024120000000000000
          Width = 207.874110940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'RESULTADO: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          Left = 978.898270000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct3."Credito">) - SUM(<frxDsLct3."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 49.133890000000000000
        Top = 385.512060000000000000
        Width = 1046.929810000000000000
        object Memo30: TfrxMemoView
          Left = 634.961040000000000000
          Top = 3.779530000000022000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct3."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 702.992580000000000000
          Top = 3.779530000000022000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct3."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 771.024120000000000000
          Top = 3.779530000000022000
          Width = 207.874110940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'RESULTADO: ')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 978.898270000000000000
          Top = 3.779530000000022000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLct1
          DataSetName = 'frxDsLct1'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct3."Credito">) - SUM(<frxDsLct3."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsLct3: TfrxDBDataset
    UserName = 'frxDsLct3'
    CloseDataSource = False
    DataSet = QrLct3
    BCDToCurrency = False
    Left = 380
    Top = 196
  end
  object PMEvento: TPopupMenu
    OnPopup = PMEventoPopup
    Left = 344
    Top = 480
    object Incluinovoevento1: TMenuItem
      Caption = '&Inclui novo evento'
      OnClick = Incluinovoevento1Click
    end
    object Alteraeventoatual1: TMenuItem
      Caption = '&Altera evento atual'
      OnClick = Alteraeventoatual1Click
    end
    object Excluieventoatual1: TMenuItem
      Caption = '&Exclui evento atual'
      Enabled = False
    end
  end
  object PMLct: TPopupMenu
    OnPopup = PMLctPopup
    Left = 440
    Top = 484
    object Atrelalanamentos1: TMenuItem
      Caption = '&Atrela lan'#231'amentos'
      OnClick = Atrelalanamentos1Click
    end
    object Retiralanamentos1: TMenuItem
      Caption = '&Retira lan'#231'amentos'
      OnClick = Retiralanamentos1Click
    end
  end
  object QrSel: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2,'
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,'
      'ca.Nome NOMECARTEIRA, la.*, co.Nome NOMEGENERO, '
      'ca.ForneceI CLIENTE_INTERNO '
      'FROM lanctos la, carteiras ca, contas co'
      'WHERE ca.Codigo=la.Carteira'
      'AND co.Codigo=la.Genero'
      'AND la.Data BETWEEN '#39'2010/01/01'#39' AND '#39'2010/05/27'#39
      'AND la.Genero = '#39'33'#39
      'ORDER BY la.Data, la.Controle'
      '')
    Left = 636
    Top = 12
    object QrSelControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSelSub: TSmallintField
      FieldName = 'Sub'
    end
  end
end
