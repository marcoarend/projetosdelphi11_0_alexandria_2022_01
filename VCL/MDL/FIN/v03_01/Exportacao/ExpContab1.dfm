object FmExpContab1: TFmExpContab1
  Left = 379
  Top = 217
  Caption = 'EDC-CONTB-001 :: Layout de Exporta'#231#227'o para Contabilidade'
  ClientHeight = 413
  ClientWidth = 637
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 637
    Height = 238
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 233
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 23
      Height = 13
      Caption = 'Item:'
    end
    object Label2: TLabel
      Left = 200
      Top = 8
      Width = 30
      Height = 13
      Caption = 'In'#237'cio:'
    end
    object Label3: TLabel
      Left = 284
      Top = 8
      Width = 48
      Height = 13
      Caption = 'Tamanho:'
    end
    object Label4: TLabel
      Left = 368
      Top = 8
      Width = 74
      Height = 13
      Caption = 'Preenchimento:'
    end
    object Label5: TLabel
      Left = 16
      Top = 192
      Width = 270
      Height = 13
      Caption = 'Carteira padr'#227'o de dep'#243'sitos cuja carteira est'#225' indefinida:'
    end
    object StaticText3: TStaticText
      Left = 16
      Top = 26
      Width = 180
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      Caption = 'Dia'
      ParentBiDiMode = False
      TabOrder = 0
    end
    object EdDiaI: TdmkEdit
      Left = 200
      Top = 24
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdDiaIExit
    end
    object EdDiaF: TdmkEdit
      Left = 284
      Top = 24
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdDiaFExit
    end
    object EdDiaP: TdmkEdit
      Left = 368
      Top = 24
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 1
      ParentFont = False
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object StaticText1: TStaticText
      Left = 16
      Top = 50
      Width = 180
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      Caption = 'M'#234's'
      ParentBiDiMode = False
      TabOrder = 4
    end
    object EdMesI: TdmkEdit
      Left = 200
      Top = 48
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdMesIExit
    end
    object EdMesF: TdmkEdit
      Left = 284
      Top = 48
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdMesFExit
    end
    object EdMesP: TdmkEdit
      Left = 368
      Top = 48
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 1
      ParentFont = False
      TabOrder = 7
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object StaticText2: TStaticText
      Left = 16
      Top = 74
      Width = 180
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      Caption = 'Devedor'
      ParentBiDiMode = False
      TabOrder = 8
    end
    object EdDevI: TdmkEdit
      Left = 200
      Top = 72
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 9
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdDevIExit
    end
    object EdDevF: TdmkEdit
      Left = 284
      Top = 72
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 10
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdDevFExit
    end
    object EdDevP: TdmkEdit
      Left = 368
      Top = 72
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 1
      ParentFont = False
      TabOrder = 11
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object StaticText4: TStaticText
      Left = 16
      Top = 98
      Width = 180
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      Caption = 'Credor'
      ParentBiDiMode = False
      TabOrder = 12
    end
    object EdCreI: TdmkEdit
      Left = 200
      Top = 96
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 13
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdCreIExit
    end
    object EdCreF: TdmkEdit
      Left = 284
      Top = 96
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 14
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdCreFExit
    end
    object EdCreP: TdmkEdit
      Left = 368
      Top = 96
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 1
      ParentFont = False
      TabOrder = 15
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object StaticText5: TStaticText
      Left = 16
      Top = 122
      Width = 180
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      Caption = 'Complemento'
      ParentBiDiMode = False
      TabOrder = 16
    end
    object EdComI: TdmkEdit
      Left = 200
      Top = 120
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 17
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdComIExit
    end
    object EdComF: TdmkEdit
      Left = 284
      Top = 120
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 18
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdComFExit
    end
    object EdComP: TdmkEdit
      Left = 368
      Top = 120
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 1
      ParentFont = False
      TabOrder = 19
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object StaticText6: TStaticText
      Left = 16
      Top = 146
      Width = 180
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      Caption = 'Valor'
      ParentBiDiMode = False
      TabOrder = 20
    end
    object EdValI: TdmkEdit
      Left = 200
      Top = 144
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 21
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdValIExit
    end
    object EdValF: TdmkEdit
      Left = 284
      Top = 144
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 22
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdValFExit
    end
    object EdValP: TdmkEdit
      Left = 368
      Top = 144
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 1
      ParentFont = False
      TabOrder = 23
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object StaticText7: TStaticText
      Left = 16
      Top = 170
      Width = 180
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      Caption = 'Documento'
      ParentBiDiMode = False
      TabOrder = 24
    end
    object EdDocI: TdmkEdit
      Left = 200
      Top = 168
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 25
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdDocIExit
    end
    object EdDocF: TdmkEdit
      Left = 284
      Top = 168
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 3
      ParentFont = False
      TabOrder = 26
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdDocFExit
    end
    object EdDocP: TdmkEdit
      Left = 368
      Top = 168
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 1
      ParentFont = False
      TabOrder = 27
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdCartPadr: TdmkEditCB
      Left = 20
      Top = 208
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 28
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCartPadr
    end
    object CBCartPadr: TdmkDBLookupComboBox
      Left = 84
      Top = 208
      Width = 365
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCarteiras
      TabOrder = 29
      dmkEditCB = EdCartPadr
      UpdType = utYes
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 637
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = -69
    ExplicitWidth = 630
    object GB_R: TGroupBox
      Left = 589
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 582
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 541
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 534
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 500
        Height = 32
        Caption = 'Layout de Exporta'#231#227'o para Contabilidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 500
        Height = 32
        Caption = 'Layout de Exporta'#231#227'o para Contabilidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 500
        Height = 32
        Caption = 'Layout de Exporta'#231#227'o para Contabilidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 286
    Width = 637
    Height = 57
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 280
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 633
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 626
      ExplicitHeight = 27
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 603
        Height = 16
        Caption = 
          ' Branco = N'#227'o selecionado     Azul = Selecionado     Vermelho = ' +
          'ERRO (selecionado mais de uma vez).'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 603
        Height = 16
        Caption = 
          ' Branco = N'#227'o selecionado     Azul = Selecionado     Vermelho = ' +
          'ERRO (selecionado mais de uma vez).'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PnCores: TPanel
        Left = 0
        Top = 18
        Width = 633
        Height = 22
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitTop = 24
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 343
    Width = 637
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitLeft = -14
    ExplicitTop = 333
    ExplicitWidth = 630
    object PnSaiDesis: TPanel
      Left = 491
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 484
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object PnOK: TPanel
      Left = 2
      Top = 15
      Width = 489
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 482
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object QrExpContab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM expcontab')
    Left = 480
    Top = 84
    object QrExpContabDiaI: TIntegerField
      FieldName = 'DiaI'
    end
    object QrExpContabDiaF: TIntegerField
      FieldName = 'DiaF'
    end
    object QrExpContabDiaP: TWideStringField
      FieldName = 'DiaP'
      Size = 1
    end
    object QrExpContabMesI: TIntegerField
      FieldName = 'MesI'
    end
    object QrExpContabMesF: TIntegerField
      FieldName = 'MesF'
    end
    object QrExpContabMesP: TWideStringField
      FieldName = 'MesP'
      Size = 1
    end
    object QrExpContabDevI: TIntegerField
      FieldName = 'DevI'
    end
    object QrExpContabDevF: TIntegerField
      FieldName = 'DevF'
    end
    object QrExpContabDevP: TWideStringField
      FieldName = 'DevP'
      Size = 1
    end
    object QrExpContabCreI: TIntegerField
      FieldName = 'CreI'
    end
    object QrExpContabCreF: TIntegerField
      FieldName = 'CreF'
    end
    object QrExpContabCreP: TWideStringField
      FieldName = 'CreP'
      Size = 1
    end
    object QrExpContabComI: TIntegerField
      FieldName = 'ComI'
    end
    object QrExpContabComF: TIntegerField
      FieldName = 'ComF'
    end
    object QrExpContabComP: TWideStringField
      FieldName = 'ComP'
      Size = 1
    end
    object QrExpContabValI: TIntegerField
      FieldName = 'ValI'
    end
    object QrExpContabValF: TIntegerField
      FieldName = 'ValF'
    end
    object QrExpContabValP: TWideStringField
      FieldName = 'ValP'
      Size = 1
    end
    object QrExpContabDocI: TIntegerField
      FieldName = 'DocI'
    end
    object QrExpContabDocF: TIntegerField
      FieldName = 'DocF'
    end
    object QrExpContabDocP: TWideStringField
      FieldName = 'DocP'
      Size = 1
    end
    object QrExpContabCartPadr: TIntegerField
      FieldName = 'CartPadr'
    end
  end
  object DsExpContab: TDataSource
    DataSet = QrExpContab
    Left = 508
    Top = 84
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM carteiras'
      'WHERE Codigo <> 0'
      'ORDER BY Nome')
    Left = 176
    Top = 265
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 204
    Top = 265
  end
end
