unit LctMudaCart;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, DBCtrls, ComCtrls,
  Grids, DBGrids, dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkGeral, dmkImage,
  UnDmkEnums, DmkDAC_PF;

type
  TFmLctMudaCart = class(TForm)
    PainelDados: TPanel;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrCarteirasTipo: TIntegerField;
    Label15: TLabel;
    QrCarteirasForneceI: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtCancela: TBitBtn;
    procedure BtOKClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCarteiras();
  public
    { Public declarations }
    SohPermiteTipo1: Boolean;
    FEntCliInt, FCarteiraTip, FCarteiraSel: Integer;
  end;

  var
  FmLctMudaCart: TFmLctMudaCart;

implementation

{$R *.DFM}

uses UnMyObjects, Module;

procedure TFmLctMudaCart.BtOKClick(Sender: TObject);
begin
  FCarteiraSel := Geral.IMV(EdCarteira.Text);
  FCarteiraTip := QrCarteirasTipo.Value;
  if SohPermiteTipo1 and  (FCarteiraTip <> 1) then
  begin
    Geral.MB_Aviso('Tipo de carteira n�o permitida para quita��o!');
    Exit;
  end;
  Close;
end;

procedure TFmLctMudaCart.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctMudaCart.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctMudaCart.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctMudaCart.FormShow(Sender: TObject);
begin
  ReopenCarteiras();
end;

procedure TFmLctMudaCart.ReopenCarteiras;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCarteiras, Dmod.MyDB, [
    'SELECT Codigo, Nome, Tipo, ForneceI ',
    'FROM carteiras ',
    'WHERE Ativo = 1 ',
    'AND ForneceI=' + Geral.FF0(FEntCliInt),
    'ORDER BY Nome ',
    '']);
end;

procedure TFmLctMudaCart.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FCarteiraSel := -1000;
end;

end.
