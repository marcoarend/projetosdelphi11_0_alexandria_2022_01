unit LctDuplic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkImage, dmkGeral,
  UnDmkEnums;

type
  TFmLctDuplic = class(TForm)
    Panel1: TPanel;
    STNotaF: TStaticText;
    STCheque: TStaticText;
    GradeCH: TDBGrid;
    GradeNF: TDBGrid;
    STValorContaMes: TStaticText;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtEstahEmLoop: TBitBtn;
    BtOK: TBitBtn;
    STValEntContaData: TStaticText;
    GradeDU: TDBGrid;
    GradeEV: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtEstahEmLoopClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConfirma: Integer;
  end;

  var
  FmLctDuplic: TFmLctDuplic;

implementation

uses UnMyObjects, ModuleFin;

{$R *.DFM}

procedure TFmLctDuplic.BtSaidaClick(Sender: TObject);
begin
  FConfirma := 0; // N�o inclui o item atual
  Close;
end;

procedure TFmLctDuplic.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctDuplic.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctDuplic.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FConfirma := 0; // N�o inclui o item atual
  //
  GradeCH.DataSource := DModFin.DsDuplCH;
  GradeNF.DataSource := DModFin.DsDuplNF;
  GradeDU.DataSource := DModFin.DsDuplVal;
  GradeEV.DataSource := DmodFin.DsDuplEntValData;
end;

procedure TFmLctDuplic.BtEstahEmLoopClick(Sender: TObject);
begin
  FConfirma := -1; // Informa para deixar um poss�vel loop de inclus�es!
  Close;
end;

procedure TFmLctDuplic.BtOKClick(Sender: TObject);
begin
  FConfirma := 1; // Confirma a inclus�o
  Close;
end;

end.
