unit CartPgto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*)
  ZCF2, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  UnMyLinguas, mySQLDbTables, ComCtrls, dmkGeral, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkEditDateTimePicker, dmkImage, UnDmkEnums,
  UnDmkProcFunc;

type
  TFmCartPgto = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    DBGPagtos: TDBGrid;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    DBEdit5: TDBEdit;
    Label1: TLabel;
    EdControle: TDBEdit;
    DBEdit7: TDBEdit;
    Label5: TLabel;
    QrPagtos: TMySQLQuery;
    QrPagtosData: TDateField;
    QrPagtosTipo: TSmallintField;
    QrPagtosCarteira: TIntegerField;
    QrPagtosAutorizacao: TIntegerField;
    QrPagtosGenero: TIntegerField;
    QrPagtosDescricao: TWideStringField;
    QrPagtosNotaFiscal: TIntegerField;
    QrPagtosDebito: TFloatField;
    QrPagtosCredito: TFloatField;
    QrPagtosCompensado: TDateField;
    QrPagtosDocumento: TFloatField;
    QrPagtosSit: TIntegerField;
    QrPagtosVencimento: TDateField;
    QrPagtosLk: TIntegerField;
    QrPagtosFatID: TIntegerField;
    QrPagtosFatParcela: TIntegerField;
    LaControle: TLabel;
    Label6: TLabel;
    DsPagtos: TDataSource;
    QrSoma: TMySQLQuery;
    LaCred: TLabel;
    EdDeb: TdmkEdit;
    LaDeb: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCred: TdmkEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    QrSomaCredito: TFloatField;
    QrSomaDebito: TFloatField;
    QrPagtosNOMESIT: TWideStringField;
    QrPagtosAno: TFloatField;
    QrPagtosMENSAL: TWideStringField;
    QrPagtosMENSAL2: TWideStringField;
    Label12: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit12: TDBEdit;
    QrPagtosMoraDia: TFloatField;
    QrPagtosMulta: TFloatField;
    QrPagtosSub: TSmallintField;
    QrPagtosCartao: TIntegerField;
    QrPagtosControle: TIntegerField;
    QrPagtosID_Pgto: TIntegerField;
    QrPagtosMes2: TLargeintField;
    DataSource1: TDataSource;
    QrDescoPor: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    IntegerField4: TIntegerField;
    DsDescoPor: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    DsCarteiras: TDataSource;
    QrCarteirasTipo: TIntegerField;
    QrPagtosFatNum: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    PnGerencia: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    PnDesco: TPanel;
    BtPagtosAltera: TBitBtn;
    TPData: TdmkEditDateTimePicker;
    Label20: TLabel;
    CBCarteiraDesco: TdmkDBLookupComboBox;
    EdCarteiraDesco: TdmkEditCB;
    Label19: TLabel;
    EdDescoVal: TdmkEdit;
    LaDescoVal: TLabel;
    CBDescoPor: TdmkDBLookupComboBox;
    EdDescoPor: TdmkEditCB;
    LaDescoPor: TLabel;
    EdSaldoCred: TdmkEdit;
    Label10: TLabel;
    Label11: TLabel;
    EdSaldoDeb: TdmkEdit;
    Label21: TLabel;
    Label22: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    QrSomaDesconto: TFloatField;
    QrSomaMulta: TFloatField;
    QrSomaJuros: TFloatField;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    QrPagtosMoraVal: TFloatField;
    QrPagtosMultaVal: TFloatField;
    QrPagtosCliInt: TIntegerField;
    BtLocaliza1: TBitBtn;
    QrPagtosDescoVal: TFloatField;
    QrSomaValLiq: TFloatField;
    QrSomaQtDtPg: TLargeintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrPagtosAfterOpen(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrPagtosCalcFields(DataSet: TDataSet);
    procedure QrPagtosAfterScroll(DataSet: TDataSet);
    procedure DBGPagtosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormResize(Sender: TObject);
    procedure BtPagtosAlteraClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtLocaliza1Click(Sender: TObject);
  private
    { Private declarations }
    procedure EditaPagtos(Estado: Integer);
    procedure AtualizaEmissaoMaster;
    procedure ConfiguraDBGCarteira;
  public
    { Public declarations }
    FCodLctLocaliz: Integer;
    FTabLctA: String;
    FQrLct, FQrCarteira: TmySQLQuery;
    procedure PagtosReopen(Lancto: Double);
  end;

var
  FmCartPgto: TFmCartPgto;

implementation

uses UnMyObjects, Module, CartPgtoEdit, Principal, UnFinanceiro, ModuleFin,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmCartPgto.ConfiguraDBGCarteira;
var
  i: Integer;
begin
  for i := 0 to DBGPagtos.Columns.Count -1 do
  begin
    if DBGPagtos.Columns[i].FieldName = 'Documento' then
    begin
      if QrPagtosTipo.Value = 0 then DBGPagtos.Columns[i].Visible := False
      else DBGPagtos.Columns[i].Visible := True;
    end;
    if DBGPagtos.Columns[i].FieldName = 'Vencimento' then
    begin
      if QrPagtosTipo.Value = 2 then DBGPagtos.Columns[i].Visible := True
      else DBGPagtos.Columns[i].Visible := False;
    end;
    if DBGPagtos.Columns[i].FieldName = 'Compensado' then
    begin
      if QrPagtosTipo.Value = 2 then DBGPagtos.Columns[i].Visible := True
      else DBGPagtos.Columns[i].Visible := False;
    end;
    if DBGPagtos.Columns[i].FieldName = 'NOMESIT' then
    begin
      if QrPagtosTipo.Value = 2 then DBGPagtos.Columns[i].Visible := True
      else DBGPagtos.Columns[i].Visible := False;
    end;
  end;
end;

procedure TFmCartPgto.AtualizaEmissaoMaster;
var
  SaldoCred, SaldoDeb, Pago: Double;
  Multa, Juros, Desconto: Double;
  Sit, QtDtPg: Integer;
begin
  QrSoma.Close;
  QrSoma.SQL.Clear;
  QrSoma.SQL.Add('SELECT SUM(Credito) Credito, SUM(Debito) Debito, ');
  QrSoma.SQL.Add('SUM(MultaVal) Multa, SUM(MoraVal) Juros, SUM(DescoVal) Desconto, ');
  QrSoma.SQL.Add('(IF(SUM(Credito) - SUM(Debito) >= 0, SUM(Credito) - SUM(Debito), ');
  QrSoma.SQL.Add('(SUM(Credito) - SUM(Debito)) * -1) - SUM(MultaVal) - SUM(MoraVal) + SUM(DescoVal)) ValLiq, ');
  QrSoma.SQL.Add('COUNT(DISTINCT(Data)) QtDtPg ');
  QrSoma.SQL.Add('FROM ' + FTabLctA + ' WHERE ID_Pgto=:P0');
  QrSoma.Params[0].AsFloat := StrToFloat(LaControle.Caption);
  UnDmkDAC_PF.AbreQuery(QrSoma, Dmod.MyDB);
  //
  Multa    := QrSomaMulta.Value;
  Juros    := QrSomaJuros.Value;
  Desconto := QrSomaDesconto.Value;
  QtDtPg   := QrSomaQtDtPg.Value;
  //
  if FQrLct.FieldByName('Debito').AsFloat = 0 then
    SaldoDeb := 0
  else
    //SaldoDeb := (QrSoma.FieldByName('Debito').AsFloat - FQrLct.FieldByName('Debito').AsFloat);

    (* Mudado em 16/02/2016 erro ao somar valores com v�rgula
    SaldoDeb := ((QrSoma.FieldByName('Debito').AsFloat -
      QrSoma.FieldByName('Multa').AsFloat - QrSoma.FieldByName('Juros').AsFloat) +
      QrSoma.FieldByName('Desconto').AsFloat) - FQrLct.FieldByName('Debito').AsFloat;
    *)
    SaldoDeb := QrSomaValLiq.Value - FQrLct.FieldByName('Debito').AsFloat;
  //
  if FQrLct.FieldByName('Credito').AsFloat = 0 then
    SaldoCred := 0
  else
    //SaldoCred := (QrSoma.FieldByName('Credito').AsFloat - FQrLct.FieldByName('Credito').AsFloat);

    (* Mudado em 16/02/2016 erro ao somar valores com v�rgula
    SaldoCred := ((QrSoma.FieldByName('Credito').AsFloat -
      QrSoma.FieldByName('Multa').AsFloat - QrSoma.FieldByName('Juros').AsFloat) +
      QrSoma.FieldByName('Desconto').AsFloat) - FQrLct.FieldByName('Credito').AsFloat;
    *)
    SaldoDeb := QrSomaValLiq.Value - FQrLct.FieldByName('Credito').AsFloat;
  //
  Pago        := QrSoma.FieldByName('Credito').AsFloat - QrSoma.FieldByName('Debito').AsFloat;
  EdDeb.Text  := Geral.TFT(FloatToStr(QrSoma.FieldByName('Debito').AsFloat), 2, siNegativo);
  EdCred.Text := Geral.TFT(FloatToStr(QrSoma.FieldByName('Credito').AsFloat), 2, siNegativo);
  //
  EdSaldoDeb.Text  := Geral.TFT(FloatToStr(SaldoDeb), 2, siNegativo);
  EdSaldoCred.Text := Geral.TFT(FloatToStr(QrSoma.FieldByName('Credito').AsFloat -
    FQrLct.FieldByName('Credito').AsFloat), 2, siNegativo);
  //
  if (SaldoCred >= 0) and (SaldoDeb >= 0) then
    Sit := 2
  else if (QrSoma.FieldByName('Debito').AsFloat <> 0) or (QrSoma.FieldByName('Credito').AsFloat <> 0) then
    Sit := 1
  else
    Sit := 0;
  //
  QrSoma.Close;
  {
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET Sit=:P0, Pago=:P1');
  Dmod.QrUpdM.SQL.Add('WHERE Controle=:P2');
  Dmod.QrUpdM.Params[0].AsInteger := Sit;
  Dmod.QrUpdM.Params[1].AsFloat := Pago;
  Dmod.QrUpdM.Params[2].AsFloat := StrToFloat(LaControle.Caption);
  Dmod.QrUpdM.ExecSQL;
  }
  UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False,
    ['Sit', 'Pago', 'PagJur', 'PagMul', 'RecDes', 'QtDtPg'], ['Controle'],
    [Sit, Pago, Juros, Multa, Desconto, QtDtPg], [LaControle.Caption], True, '', FTabLctA);
  //
  UFinanceiro.RecalcSaldoCarteira(FQrLct.FieldByName('Carteira').AsInteger,
    FQrCarteira, FQrLct, True, True);
end;

procedure TFmCartPgto.EditaPagtos(Estado: Integer);
var
  Lancto: Int64;
  Sub, Genero, Cartao, Sit, Tipo, ID_Pgto, Carteira: Integer;
  Data: TDateTime;
  Valor, ValAtualiz, ValMulta, ValJuro, PerMulta, PerJuro: Double;
begin
  //if UMyMod.AcessoNegadoAoForm(ivTabPerfis, 'QuitaDocs', 0) then Exit;
  Data     := QrPagtosData.Value;
  Lancto   := QrPagtosControle.Value;
  Sub      := QrPagtosSub.Value;
  Genero   := QrPagtosGenero.Value;
  Cartao   := QrPagtosCartao.Value;
  Sit      := QrPagtosSit.Value;
  Tipo     := QrPagtosTipo.Value;
  ID_Pgto  := QrPagtosID_Pgto.Value;
  Carteira := QrPagtosCarteira.Value;
  //
  if UMyMod.SelLockInt64Y(Lancto, Dmod.MyDB, FTabLctA, 'Controle') then
    Exit;
  UMyMod.UpdLockInt64Y(Lancto, Dmod.MyDB, FTabLctA, 'Controle');
  Refresh;
  //
  if Estado < 3 then
  begin
    Application.CreateForm(TFmCartPgtoEdit, FmCartPgtoEdit);
    FmCartPgtoEdit.FTabLctA := FTabLctA;
    with FmCartPgtoEdit do
    begin
      if Estado = 1 then
      begin
        Valor := Geral.DMV(EdSaldoDeb.ValueVariant);
        if Valor < 0 then
          Valor := Valor * -1
        else begin
          Valor := Geral.DMV(EdSaldoCred.ValueVariant);
          if Valor < 0 then
            Valor := Valor * -1;
        end;
        //
        PerMulta := FQrLct.FieldByName('Multa').AsFloat;
        PerJuro  := FQrLct.FieldByName('MoraDia').AsFloat;
        //
        UFinanceiro.CalculaValMultaEJuros(Valor, Date,
          FQrLct.FieldByName('Vencimento').AsDateTime, PerMulta, PerJuro,
          ValAtualiz, ValMulta, ValJuro, False);
        //
        EdOldControle.ValueVariant  := FQrLct.FieldByName('Controle').AsInteger;
        TPOldData.Date              := FQrLct.FieldByName('Vencimento').AsDateTime;
        EdOldSaldo.ValueVariant     := Valor;
        EdOldDescricao.ValueVariant := FQrLct.FieldByName('Descricao').AsString;
        //
        ImgTipo.SQLType          := stIns;
        TPData.Date              := dmkPF.ObtemDataInserir();
        TPVencimento.Date        := Date;
        EdValor.ValueVariant     := ValAtualiz;
        EdMoraVal.ValueVariant   := ValJuro;
        EdMultaVal.ValueVariant  := ValMulta;
        EdDescricao.ValueVariant := FQrLct.FieldByName('Descricao').AsString;
        EdMoraDia.ValueVariant   := PerJuro;
        EdMulta.ValueVariant     := PerMulta;
        //
        dmkEdNF.ValueVariant     := FQrLct.FieldByName('NotaFiscal').AsInteger;
      end else begin
        {
        TPOldData.Date             := FQrLct.FieldByName('Data').AsDateTime;
        EdOldTipo.ValueVariant     := IntToStr(FQrLct.FieldByName('Tipo').AsInteger);
        EdOldCarteira.ValueVariant := IntToStr(FQrLct.FieldByName('Carteira').AsInteger);
        EdOldControle.ValueVariant := IntToStr(FQrLct.FieldByName('Controle').AsInteger);
        EdOldSub.ValueVariant      := IntToStr(FQrLct.FieldByName('Sub').AsInteger);
        }//
        TPOldData.Date             := QrPagtosData.Value;
        TPOldData.Time             := 0;
        FOldTipo                   := QrPagtosTipo.Value;
        FOldCarteira               := QrPagtosCarteira.Value;
        EdOldControle.ValueVariant := QrPagtosControle.Value;
        FOldSub                    := QrPagtosSub.Value;
        //
        ImgTipo.SQLType      := stUpd;
        EdCodigo.Text        := Geral.TFT(FloatToStr(QrPagtosControle.Value), 0, siPositivo);
        TPData.Date          := QrPagtosData.Value;
        EdValor.Text         := Geral.TFT(FloatToStr(QrPagtosDebito.Value + QrPagtosCredito.Value), 2, siPositivo);
        EdMoraVal.Text       := Geral.TFT(FloatToStr(QrPagtosMoraDia.Value), 2, siPositivo);
        EdMultaVal.Text      := Geral.TFT(FloatToStr(QrPagtosMulta.Value), 2, siPositivo);
        EdDoc.Text           := Geral.TFT(FloatToStr(QrPagtosDocumento.Value), 0, siPositivo);
        TPVencimento.Date    := QrPagtosVencimento.Value;
        EdDescricao.Text     := QrPagtosDescricao.Value;
        RGCarteira.ItemIndex := QrPagtosTipo.Value;
        EDCarteira.Text      := IntToStr(QrPagtosCarteira.Value);
        CBCarteira.KeyValue  := QrPagtosCarteira.Value;
        //
        dmkEdNF.ValueVariant := QrPagtosNotaFiscal.Value;
      end;
    end;
    FmCartPgtoEdit.ShowModal;
    FmCartPgtoEdit.Destroy;
  end else begin
    if Geral.MB_Pergunta('Confirma exclus�o deste registro?') <> ID_YES
    then
      Exit;
    //
    UMyMod.UpdUnLockInt64Y(Lancto, Dmod.MyDB, FTabLctA, 'Controle');
    //FmPrincipal.ExcluiItemCarteira(Lancto, Sub, Genero, Cartao, Sit, Tipo, 1,
    //FQrLct, FQrCarteira, False);
    UFinanceiro.ExcluiItemCarteira(Lancto, Data, Carteira, Sub, Genero, Cartao, Sit, Tipo,
     (*Chamada*) 1, ID_Pgto, FQrLct, FQrCarteira, False, Carteira,
     dmkPF.MotivDel_ValidaCodigo(105), FTabLctA, True);
  end;
  PagtosReopen(Lancto);
  AtualizaEmissaoMaster;
  UMyMod.UpdUnLockInt64Y(Lancto, Dmod.MyDB, FTabLctA, 'Controle');
end;

procedure TFmCartPgto.PagtosReopen(Lancto: Double);
var
  ID_Pgto: Double;
begin
  ID_Pgto := Geral.DMV(Geral.TFT(LaControle.Caption, 0, siPositivo));
  QrPagtos.Close;
  QrPagtos.SQL.Clear;
  QrPagtos.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
  QrPagtos.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
  QrPagtos.SQL.Add(' la.*');
  QrPagtos.SQL.Add('FROM ' + FTabLctA + ' la');
  QrPagtos.SQL.Add('WHERE la.ID_Pgto=:P0');
  QrPagtos.SQL.Add('ORDER BY la.Data, la.Controle');
  QrPagtos.Params[0].AsFloat := ID_Pgto;
  UnDmkDAC_PF.AbreQuery(QrPagtos, Dmod.MyDB);
  //
  if Lancto <> 0 then
    QrPagtos.Locate('Controle', Lancto, []);
end;

procedure TFmCartPgto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCartPgto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if FQrLct = nil then
  begin
    Geral.MB_Erro('"FQrLct" n�o definido. AVISE A DERMATEK');
    Close;
  end;
  // deve ser depois
  AtualizaEmissaoMaster;
  //
end;

procedure TFmCartPgto.BtIncluiClick(Sender: TObject);
begin
  EditaPagtos(1);
end;

procedure TFmCartPgto.BtLocaliza1Click(Sender: TObject);
begin
  if (QrPagtos.State <> dsInactive) and (QrPagtos.RecordCount > 0) then
  begin
    FCodLctLocaliz := QrPagtosControle.Value;
    Close;
  end;
end;

procedure TFmCartPgto.BtAlteraClick(Sender: TObject);
begin
    // 2013-07-25 N�o altera mais nesta janela!
//  EditaPagtos(2);
    Geral.MB_Aviso('Altera��o desativada. Solicite implementa��o � Dermatek!');
end;

procedure TFmCartPgto.BtExcluiClick(Sender: TObject);
begin
  EditaPagtos(3);
end;

procedure TFmCartPgto.QrPagtosAfterOpen(DataSet: TDataSet);
begin
  if QrPagtos.RecordCount > 0 then
  begin
    //BtAltera.Enabled := True;
    BtAltera.Visible := False;
    BtExclui.Enabled := True;
  end else begin
    //BtAltera.Enabled := False;
    BtAltera.Visible := False;
    BtExclui.Enabled := False;
  end;
end;

procedure TFmCartPgto.QrPagtosCalcFields(DataSet: TDataSet);
begin
  if QrPagtosMes2.Value > 0 then
    QrPagtosMENSAL.Value := FormatFloat('00', QrPagtosMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrPagtosAno.Value), 3, 2)
   else QrPagtosMENSAL.Value := CO_VAZIO;
  if QrPagtosMes2.Value > 0 then
    QrPagtosMENSAL2.Value := FormatFloat('0000', QrPagtosAno.Value)+'/'+
    FormatFloat('00', QrPagtosMes2.Value)+'/01'
   else QrPagtosMENSAL2.Value := CO_VAZIO;

  QrPagtosNOMESIT.Value := UFinanceiro.NomeSitLancto(QrPagtosSit.Value,
    QrPagtosTipo.Value, 0(*QrPagtosPrazo.Value*), QrPagtosVencimento.Value,
    0(*QrPagtosReparcel.Value*));
end;

procedure TFmCartPgto.QrPagtosAfterScroll(DataSet: TDataSet);
begin
  ConfiguraDBGCarteira;
end;

procedure TFmCartPgto.DBGPagtosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  MyObjects.DefineCorTextoSitLancto(TDBGrid(Sender), Rect,
    Column.FieldName, QrPagtosNOMESIT.Value);
end;

procedure TFmCartPgto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCartPgto.BtPagtosAlteraClick(Sender: TObject);
var
  CarteiraDesco, Sit, Controle, DescoPor: Integer;
  Valor, DescoVal: Double;
begin
  if UFinanceiro.TabLctNaoDefinida(FTabLctA, 'TFmCartPgto.BtPagtosAlteraClick()') then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    {
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET DescoPor=:P0, DescoVal=:P1');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
    Dmod.QrUpd.Params[00].AsInteger := Geral.IMV(EdDescoPor.Text);
    Dmod.QrUpd.Params[01].AsFloat   := Geral.DMV(EdDescoVal.Text);
    Dmod.QrUpd.Params[02].AsInteger := Geral.IMV(EdControle.Text);
    //Dmod.QrUpd.Params[03].AsInteger := ;
    Dmod.QrUpd.ExecSQL;
    }
    DescoPor := Geral.IMV(EdDescoPor.Text);
    DescoVal := Geral.DMV(EdDescoVal.Text);
    Controle := Geral.IMV(EdControle.Text);
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['DescoPor', 'DescoVal'], [
    'Controle'], [DescoPor, DescoVal], [Controle], True, '', FTabLctA);
    //
    CarteiraDesco := Geral.IMV(EdCarteiraDesco.Text);
    if CarteiraDesco > 0 then
    begin
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', FTabLctA, LAN_CTOS, 'Controle');
      if QrCarteirasTipo.Value = 2 then Sit := 0 else Sit := 2;
      //
      DmodFin.QrLcts.Close;
      DmodFin.QrLcts.SQL.Clear;
      DmodFin.QrLcts.SQL.Add('SELECT Data, Tipo, Carteira, Controle, Sub, FatID');
      DmodFin.QrLcts.SQL.Add('FROM ' + FTabLctA);
      DmodFin.QrLcts.SQL.Add('WHERE DescoControle=:P0');
      DmodFin.QrLcts.Params[0].AsFloat := Controle;
      UnDmkDAC_PF.AbreQuery(DmodFin.QrLcts, Dmod.MyDB);
      while not DmodFin.QrLcts.Eof do
      begin
        UFinanceiro.ExcluiLct_Unico(FTabLctA, Dmod.MyDB, DmodFin.QrLctsData.Value,
          DmodFin.QrLctsTipo.Value, DmodFin.QrLctsCarteira.Value,
          DmodFin.QrLctsControle.Value, DmodFin.QrLctsSub.Value,
          dmkPF.MotivDel_ValidaCodigo(105), False);
        //
        DmodFin.QrLcts.Next;
      end;
      //
      Valor := Geral.DMV(EdDescoVal.Text);
      if Valor <> 0 then
      begin
        Dmod.QrUpdU.SQL.Clear;
        //Dmod.QrUpd.SQL.Add('INSERT INTO ' + VAR LCT + ' SET DescoControle=:P0');
        Dmod.QrUpdU.SQL.Add('INSERT INTO ' + FTabLctA + ' SET ');
        Dmod.QrUpdU.SQL.Add('Data=:P0');
        Dmod.QrUpdU.SQL.Add(', Tipo=:P1');
        Dmod.QrUpdU.SQL.Add(', Carteira=:P2');
        Dmod.QrUpdU.SQL.Add(', Documento=:P3');
        Dmod.QrUpdU.SQL.Add(', Genero=:P4');
        Dmod.QrUpdU.SQL.Add(', Descricao=:P5');
        Dmod.QrUpdU.SQL.Add(', NotaFiscal=:P6');
        Dmod.QrUpdU.SQL.Add(', Debito=:P7');
        Dmod.QrUpdU.SQL.Add(', Credito=:P8');
        Dmod.QrUpdU.SQL.Add(', Compensado=:P9');
        Dmod.QrUpdU.SQL.Add(', Vencimento=:P10');
        Dmod.QrUpdU.SQL.Add(', Sit=:P11');
        Dmod.QrUpdU.SQL.Add(', Fornecedor=:P12');
        Dmod.QrUpdU.SQL.Add(', Cliente=:P13');
        Dmod.QrUpdU.SQL.Add(', MoraDia=:P14');
        Dmod.QrUpdU.SQL.Add(', Multa=:P15');
        Dmod.QrUpdU.SQL.Add(', Vendedor=:P16');
        Dmod.QrUpdU.SQL.Add(', Account=:P17');
        Dmod.QrUpdU.SQL.Add(', FatID_Sub=:P18');
        Dmod.QrUpdU.SQL.Add(', CliInt=:P19');
        //
        Dmod.QrUpdU.SQL.Add(', DescoControle=:P20');
        Dmod.QrUpdU.SQL.Add(', UserCad=:Pa');
        Dmod.QrUpdU.SQL.Add(', Controle=:Pb');
        //
        Dmod.QrUpdU.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPData.Date);
        Dmod.QrUpdU.Params[01].AsInteger := QrCarteirasTipo.Value;
        Dmod.QrUpdU.Params[02].AsInteger := QrCarteirasCodigo.Value;
        Dmod.QrUpdU.Params[03].AsFloat   := 0;
        Dmod.QrUpdU.Params[04].AsInteger := -9; // Desconto duplicata
        Dmod.QrUpdU.Params[05].AsString  := 'Desconto de duplicata';
        Dmod.QrUpdU.Params[06].AsInteger := 0;
        Dmod.QrUpdU.Params[07].AsFloat   := Valor;
        Dmod.QrUpdU.Params[08].AsFloat   := 0;
        Dmod.QrUpdU.Params[09].AsString  := CO_VAZIO;
        Dmod.QrUpdU.Params[10].AsString  := FormatDateTime(VAR_FORMATDATE, TPData.Date);
        Dmod.QrUpdU.Params[11].AsInteger := Sit;
        Dmod.QrUpdU.Params[12].AsInteger := Geral.IMV(EdDescoPor.Text);
        Dmod.QrUpdU.Params[13].AsInteger := 0;
        Dmod.QrUpdU.Params[14].AsFloat   := 0;
        Dmod.QrUpdU.Params[15].AsFloat   := 0;
        Dmod.QrUpdU.Params[16].AsInteger := 0;
        Dmod.QrUpdU.Params[17].AsInteger := 0;
        Dmod.QrUpdU.Params[18].AsInteger := 0;
        Dmod.QrUpdU.Params[19].AsInteger := 0;
        //
        Dmod.QrUpdU.Params[20].AsInteger := Geral.IMV(EdControle.Text);
        //
        Dmod.QrUpdU.Params[21].AsInteger := VAR_USUARIO;
        Dmod.QrUpdU.Params[22].AsFloat   := Controle;
        Dmod.QrUpdU.ExecSQL;
      end;
    end;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
  //
end;

procedure TFmCartPgto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  FCodLctLocaliz := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrDescoPor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  TPData.Date := Date;
  (*if Dmod.QrControle.FieldByName('ContaDesco').AsInteger <> 0 then
  begin
    Label19.Enabled := True;
    Label20.Enabled := True;
    Label21.Visible := False;
    //
    EdCarteiraDesco.Enabled := True;
    CBCarteiraDesco.Enabled := True;
    TPData.Enabled := True;
  end; *)
end;

end.

