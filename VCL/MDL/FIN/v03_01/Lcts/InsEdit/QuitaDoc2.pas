unit QuitaDoc2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, Db, (*DBTables,*) UnInternalConsts,
  UnGOTOy, UMySQLModule, ExtCtrls, Mask, DBCtrls, mySQLDbTables, dmkGeral,
  dmkEdit, dmkEditDateTimePicker, dmkImage, dmkEditCB, dmkDBLookupComboBox,
  DmkDAC_PF, UnDmkEnums;

type
  TFmQuitaDoc2 = class(TForm)
    PainelDados: TPanel;
    Label2: TLabel;
    EdDocumento: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdDescricao: TdmkEdit;
    Label8: TLabel;
    EdSerieCH: TDmkEdit;
    EdValor: TDmkEdit;
    Label10: TLabel;
    EdMulta: TDmkEdit;
    Label14: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    EdLancto: TdmkEdit;
    EdSub: TdmkEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    Label16: TLabel;
    EdDescoVal: TdmkEdit;
    TPData1: TdmkEditDateTimePicker;
    Label1: TLabel;
    Label11: TLabel;
    EdMultaVal: TdmkEdit;
    EdMoraVal: TdmkEdit;
    Label12: TLabel;
    Label15: TLabel;
    EdMoraDia: TdmkEdit;
    DBEdit5: TDBEdit;
    Label9: TLabel;
    GBCarteira: TGroupBox;
    Label13: TLabel;
    CBCarteira: TdmkDBLookupComboBox;
    EdCarteira: TdmkEditCB;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasTipo: TIntegerField;
    DsCarteiras: TDataSource;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdMoraDiaExit(Sender: TObject);
    procedure EdMultaExit(Sender: TObject);
    procedure TPData1Change(Sender: TObject);
    procedure TPData1Click(Sender: TObject);
    procedure EdMultaValExit(Sender: TObject);
    procedure EdMoraValExit(Sender: TObject);
    procedure EdDescoValExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaMultaJuroVal;
    procedure CalculaMultaJuroPer;
 public
    { Public declarations }
    FTabLctA: String;
    FCtrlIni, FControle: Double;
    FQrCrt, FQrLct: TmySQLQuery;
  end;

var
  FmQuitaDoc2: TFmQuitaDoc2;

implementation

uses UnMyObjects, Module, Principal, UnFinanceiro, ModuleFin, ModuleGeral;

{$R *.DFM}

procedure TFmQuitaDoc2.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmQuitaDoc2.FormActivate(Sender: TObject);
var
  Controle, Valor, PerMulta, PerJuro, ValAtualiz, ValMulta, ValJuro: Double;
  Sub: Integer;
begin
  MyObjects.CorIniComponente();
  TPData1.SetFocus;
  if UFinanceiro.TabLctNaoDefinida(FTabLctA, 'TFmQuitaDoc2.FormActivate()') then
    Close;
  //
  Controle :=   EdLancto.ValueVariant;
  Sub :=        EdSub.ValueVariant;
  // in�cio abertura no DModFin
  if DModFin.AbreLanctosAtrelados(Trunc(Controle), Sub, FTabLctA) then
  begin
    BtConfirma.Enabled       := True;
    TPData1.Date             := Date;
    EdDescricao.Text         := DModFin.QrLancto1Descricao.Value;
    EdDocumento.ValueVariant := DModFin.QrLancto1Documento.Value;
    EdSerieCH.Text           := DModFin.QrLancto1SerieCH.Value;
    //
    if DModFin.QrLancto1Debito.Value > 0 then
    begin
      Label10.Caption := Dmod.QrControle.FieldByName('Moeda').AsString + ' D�bito:';
      Valor           := DModFin.QrLancto1Debito.Value;
    end else
    begin
      Label10.Caption := Dmod.QrControle.FieldByName('Moeda').AsString + ' Cr�dito:';
      Valor           := DModFin.QrLancto1Credito.Value;
    end;
    PerMulta := DModFin.QrLancto1Multa.Value;
    PerJuro  := DModFin.QrLancto1MoraDia.Value;
    //
    UFinanceiro.CalculaValMultaEJuros(Valor, Date,
      DModFin.QrLancto1Vencimento.Value, PerMulta, PerJuro,
      ValAtualiz, ValMulta, ValJuro, False);
    //
    EdMulta.ValueVariant    := PerMulta;
    EdMoraDia.ValueVariant  := PerJuro;
    EdMultaVal.ValueVariant := ValMulta;
    EdMoraVal.ValueVariant  := ValJuro;
    EdValor.ValueVariant    := ValAtualiz;
  end
  else
    BtConfirma.Enabled := False;
  // fim abertura no DModFin
end;

procedure TFmQuitaDoc2.BtConfirmaClick(Sender: TObject);
const
  TaxasVal = 0.00;
  Duplicata = '';
var
  Carteira: Integer;
  Msg: String;
begin
  if UFinanceiro.FatIDProibidoEmLct(FQrLct.FieldByName('FatID').AsInteger, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    Exit;
  end;
  if MyObjects.FIC(((GBCarteira.Visible = True) and
    (EdCarteira.ValueVariant = 0)), EdCarteira, 'Carteira n�o definida!')
  then
    Exit;
  //
  if GBCarteira.Visible = True then
    Carteira := EdCarteira.ValueVariant
  else
    Carteira := 0;
  //
  if DModFin.QuitacaoTotalDeDocumento(EdValor.ValueVariant, Trunc(FCtrlIni),
    Trunc(FControle), EdDocumento.ValueVariant, Duplicata, TPData1.Date,
    EdDescricao.Text, TaxasVal, EdMultaVal.ValueVariant, EdMoraVal.ValueVariant,
    EdDescoVal.ValueVariant, FQrCrt, FQrLct, True, FTabLctA, False, 0, 0, 0, 0,
    0, '', False, Carteira)
  then
    Close;
end;

procedure TFmQuitaDoc2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmQuitaDoc2.FormShow(Sender: TObject);
begin
  UFinanceiro.ConfiguraGBCarteiraPgto(GBCarteira, EdCarteira, CBCarteira,
    FQrLct.FieldByName('Carteira').AsInteger);
end;

procedure TFmQuitaDoc2.TPData1Change(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmQuitaDoc2.TPData1Click(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmQuitaDoc2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  FCtrlIni := -1000;
  //
  DmodG.ReopenControle;
  //
  UFinanceiro.ReopenCarteirasPgto(QrCarteiras);
end;

procedure TFmQuitaDoc2.CalculaMultaJuroPer;
var
  ValOrig, ValorNew, MultaPerc, JuroPerc: Double;
begin
  ValOrig := FQrLct.FieldByName('Credito').AsFloat - FQrLct.FieldByName('Debito').AsFloat;
  //
  if ValOrig < 0 then
    ValOrig := ValOrig * -1;
  //
  UFinanceiro.CalculaPercMultaEJuros(ValOrig, EdMultaVal.ValueVariant,
    EdMoraVal.ValueVariant, TPData1.Date,
    FQrLct.FieldByName('Vencimento').AsDateTime, ValorNew, MultaPerc, JuroPerc);
  //
  EdMoraDia.ValueVariant := JuroPerc;
  EdMulta.ValueVariant   := MultaPerc;
  EdValor.ValueVariant   := ValorNew;
end;

procedure TFmQuitaDoc2.CalculaMultaJuroVal;
var
  ValOrig, MultaPer, JuroPer, ValAtualiz, ValMulta, ValJuro: Double;
begin
  MultaPer := EdMulta.ValueVariant;
  JuroPer  := EdMoraDia.ValueVariant;
  ValOrig  := FQrLct.FieldByName('Credito').AsFloat - FQrLct.FieldByName('Debito').AsFloat;
  //
  if ValOrig < 0 then
    ValOrig := ValOrig * -1;
  //
  UFinanceiro.CalculaValMultaEJuros(ValOrig, TPData1.Date,
    FQrLct.FieldByName('Vencimento').AsDateTime, MultaPer, JuroPer, ValAtualiz,
    ValMulta, ValJuro, True);
  //
  EdMoraVal.ValueVariant  := ValJuro;
  EdMultaVal.ValueVariant := ValMulta;
  EdValor.ValueVariant    := ValAtualiz;
end;

procedure TFmQuitaDoc2.EdDescoValExit(Sender: TObject);
var
  Valor, MultaVal, JuroVal, DescoVal: Double;
begin
  MultaVal := EdMultaVal.ValueVariant;
  JuroVal  := EdMoraVal.ValueVariant;
  DescoVal := EdDescoVal.ValueVariant;
  Valor    := FQrLct.FieldByName('Credito').AsFloat - FQrLct.FieldByName('Debito').AsFloat;
  //
  if Valor < 0 then
    Valor := Valor * -1;
  //
  EdValor.ValueVariant := Valor + JuroVal + MultaVal - DescoVal;
end;

procedure TFmQuitaDoc2.EdMoraDiaExit(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmQuitaDoc2.EdMoraValExit(Sender: TObject);
begin
  CalculaMultaJuroPer;
end;

procedure TFmQuitaDoc2.EdMultaExit(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmQuitaDoc2.EdMultaValExit(Sender: TObject);
begin
  CalculaMultaJuroPer;
end;

end.
