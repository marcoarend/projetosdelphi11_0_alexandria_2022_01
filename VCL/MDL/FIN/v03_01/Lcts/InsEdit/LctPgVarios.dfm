object FmLctPgVarios: TFmLctPgVarios
  Left = 404
  Top = 197
  Caption = 'FIN-PGTOS-007 :: Compensa'#231#227'o em Conta Corrente'
  ClientHeight = 616
  ClientWidth = 913
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 47
    Width = 913
    Height = 119
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label3: TLabel
      Left = 12
      Top = 8
      Width = 85
      Height = 13
      Caption = 'Data da quita'#231#227'o:'
    end
    object Label2: TLabel
      Left = 146
      Top = 8
      Width = 40
      Height = 13
      Caption = 'Multa %:'
    end
    object Label4: TLabel
      Left = 240
      Top = 8
      Width = 63
      Height = 13
      Caption = 'Juros %/m'#234's:'
    end
    object Label1: TLabel
      Left = 338
      Top = 8
      Width = 54
      Height = 13
      Caption = 'Total pago:'
    end
    object BitBtn1: TBitBtn
      Left = 434
      Top = 24
      Width = 20
      Height = 20
      Caption = '...'
      TabOrder = 4
      OnClick = BitBtn1Click
    end
    object TPData: TdmkEditDateTimePicker
      Left = 12
      Top = 24
      Width = 130
      Height = 21
      Date = 39411.000000000000000000
      Time = 0.393054282401863000
      TabOrder = 0
      OnExit = TPDataExit
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdMulta: TdmkEdit
      Left = 146
      Top = 24
      Width = 92
      Height = 20
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdMultaExit
    end
    object EdTaxaM: TdmkEdit
      Left = 240
      Top = 24
      Width = 92
      Height = 20
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdTaxaMExit
    end
    object EdTotalPago: TdmkEdit
      Left = 338
      Top = 24
      Width = 92
      Height = 20
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object GroupBox1: TGroupBox
      Left = 490
      Top = 0
      Width = 423
      Height = 56
      Align = alRight
      Enabled = False
      TabOrder = 5
      object Label5: TLabel
        Left = 8
        Top = 12
        Width = 63
        Height = 13
        Caption = 'Valor original:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 110
        Top = 12
        Width = 54
        Height = 13
        Caption = 'Valor pago:'
        FocusControl = DBEdit2
      end
      object Label7: TLabel
        Left = 213
        Top = 12
        Width = 55
        Height = 13
        Caption = 'Valor multa:'
        FocusControl = DBEdit3
      end
      object Label8: TLabel
        Left = 315
        Top = 12
        Width = 52
        Height = 13
        Caption = 'Valor juros:'
        FocusControl = DBEdit4
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 27
        Width = 98
        Height = 21
        DataField = 'ValorOri'
        DataSource = DsSum
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 110
        Top = 27
        Width = 99
        Height = 21
        DataField = 'ValorPgt'
        DataSource = DsSum
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 213
        Top = 27
        Width = 98
        Height = 21
        DataField = 'MultaVal'
        DataSource = DsSum
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 315
        Top = 27
        Width = 99
        Height = 21
        DataField = 'JurosVal'
        DataSource = DsSum
        TabOrder = 3
      end
    end
    object GBCarteira: TGroupBox
      Left = 0
      Top = 56
      Width = 913
      Height = 63
      Align = alBottom
      Caption = 'Carteira onde o lan'#231'amento ser'#225' quitado'
      TabOrder = 6
      object Label9: TLabel
        Left = 12
        Top = 16
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object CBCarteira: TdmkDBLookupComboBox
        Left = 67
        Top = 31
        Width = 244
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 1
        dmkEditCB = EdCarteira
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCarteira: TdmkEditCB
        Left = 12
        Top = 31
        Width = 55
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCarteira
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 166
    Width = 913
    Height = 338
    Align = alClient
    DataSource = DsLctoEdit
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Data'
        Title.Alignment = taCenter
        Width = 59
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ValorOri'
        Title.Caption = 'Valor original'
        Width = 57
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ValorPgt'
        Title.Caption = 'Valor pagto'
        Width = 57
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'JurosVal'
        Title.Caption = '$ Juros'
        Width = 57
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MultaVal'
        Title.Caption = '$ Multa'
        Width = 57
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Vencimento'
        Title.Alignment = taCenter
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descricao'
        Title.Caption = 'Descri'#231#227'o'
        Width = 271
        Visible = True
      end>
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 913
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 866
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 819
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 391
        Height = 31
        Caption = 'Compensa'#231#227'o em Conta Corrente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 391
        Height = 31
        Caption = 'Compensa'#231#227'o em Conta Corrente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 391
        Height = 31
        Caption = 'Compensa'#231#227'o em Conta Corrente'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 504
    Width = 913
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 909
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 547
    Width = 913
    Height = 69
    Align = alBottom
    TabOrder = 4
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 909
      Height = 52
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 767
        Top = 0
        Width = 142
        Height = 52
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtCancela: TBitBtn
          Tag = 15
          Left = 6
          Top = 3
          Width = 118
          Height = 39
          Caption = '&Cancela'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtCancelaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 16
        Top = 3
        Width = 118
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object PB1: TProgressBar
        Left = 142
        Top = 20
        Width = 623
        Height = 17
        TabOrder = 2
      end
    end
  end
  object QrCarteiras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo'
      'FROM carteiras'
      'WHERE Tipo IN(0,1)'
      'AND ForneceI=:P0'
      'AND Codigo > 0'
      'AND Ativo = 1'
      'ORDER BY Nome')
    Left = 96
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 124
    Top = 240
  end
  object TbLctoEdit: TMySQLTable
    Database = Dmod.MyDB
    BeforePost = TbLctoEditBeforePost
    AfterPost = TbLctoEditAfterPost
    TableName = 'LctoEdit'
    Left = 200
    Top = 196
    object TbLctoEditControle: TIntegerField
      FieldName = 'Controle'
      ReadOnly = True
    end
    object TbLctoEditSub: TIntegerField
      FieldName = 'Sub'
      ReadOnly = True
    end
    object TbLctoEditDescricao: TWideStringField
      FieldName = 'Descricao'
      ReadOnly = True
      Size = 255
    end
    object TbLctoEditData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbLctoEditVencimento: TDateField
      FieldName = 'Vencimento'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yy'
    end
    object TbLctoEditMultaVal: TFloatField
      FieldName = 'MultaVal'
      DisplayFormat = '#,###,##0.00'
    end
    object TbLctoEditJurosVal: TFloatField
      FieldName = 'JurosVal'
      DisplayFormat = '#,###,##0.00'
    end
    object TbLctoEditValorOri: TFloatField
      FieldName = 'ValorOri'
      ReadOnly = True
      DisplayFormat = '#,###,##0.00'
    end
    object TbLctoEditValorPgt: TFloatField
      FieldName = 'ValorPgt'
      DisplayFormat = '#,###,##0.00'
    end
    object TbLctoEditCarteira: TIntegerField
      FieldName = 'Carteira'
    end
  end
  object DsLctoEdit: TDataSource
    DataSet = TbLctoEdit
    Left = 228
    Top = 196
  end
  object QrLancto1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 656
    Top = 240
    object QrLancto1Sub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLancto1Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 128
    end
    object QrLancto1Credito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00  '
    end
    object QrLancto1Debito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00  '
    end
    object QrLancto1NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLancto1Documento: TFloatField
      FieldName = 'Documento'
    end
    object QrLancto1Vencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLancto1Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLancto1Data: TDateField
      FieldName = 'Data'
    end
    object QrLancto1NOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 128
    end
    object QrLancto1TIPOCARTEIRA: TIntegerField
      FieldName = 'TIPOCARTEIRA'
    end
    object QrLancto1CARTEIRABANCO: TIntegerField
      FieldName = 'CARTEIRABANCO'
    end
    object QrLancto1Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLancto1Fornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLancto1DataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLancto1Nivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLancto1Vendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLancto1Account: TIntegerField
      FieldName = 'Account'
    end
    object QrLancto1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLancto1CtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLancto1Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLancto1Mez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrLancto1Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrLancto1Doc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrLancto1SerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLancto1MoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLancto1Multa: TFloatField
      FieldName = 'Multa'
    end
    object QrLancto1ICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrLancto1ICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrLancto1CliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLancto1Depto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLancto1DescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLancto1ForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLancto1Unidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrLancto1Qtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrLancto1Qtd2: TFloatField
      FieldName = 'Qtd2'
    end
    object QrLancto1FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLancto1FatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLancto1DescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrLancto1NFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrLancto1FatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLancto1FatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLancto1CentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object QrLancto1VctoOriginal: TDateField
      FieldName = 'VctoOriginal'
    end
    object QrLancto1ModeloNF: TWideStringField
      FieldName = 'ModeloNF'
    end
    object QrLancto1GenCtb: TIntegerField
      FieldName = 'GenCtb'
    end
    object QrLancto1GenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrLancto1GenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
  end
  object QrLancto2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 684
    Top = 240
    object QrLancto2Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLancto2Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrLancto2Debito: TFloatField
      FieldName = 'Debito'
    end
    object QrLancto2Sub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLancto2Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLancto2Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSum: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSumAfterOpen
    SQL.Strings = (
      'SELECT SUM(ValorOri) ValorOri, SUM(ValorPgt) ValorPgt,'
      'SUM(MultaVal) MultaVal, SUM(JurosVal) JurosVal'
      'FROM lctoedit')
    Left = 200
    Top = 224
    object QrSumValorPgt: TFloatField
      FieldName = 'ValorPgt'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumValorOri: TFloatField
      FieldName = 'ValorOri'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumMultaVal: TFloatField
      FieldName = 'MultaVal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumJurosVal: TFloatField
      FieldName = 'JurosVal'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSum: TDataSource
    DataSet = QrSum
    Left = 228
    Top = 224
  end
  object QrTot: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ValorOri) ValorOri, SUM(ValorPgt) ValorPgt,'
      'SUM(MultaVal) MultaVal, SUM(JurosVal) JurosVal'
      'FROM lctoedit')
    Left = 200
    Top = 252
    object QrTotValorOri: TFloatField
      FieldName = 'ValorOri'
    end
    object QrTotValorPgt: TFloatField
      FieldName = 'ValorPgt'
    end
    object QrTotMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrTotJurosVal: TFloatField
      FieldName = 'JurosVal'
    end
  end
  object DsTot: TDataSource
    DataSet = QrTot
    Left = 228
    Top = 252
  end
  object QrContas1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 448
    Top = 260
    object QrContas1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContas1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas1: TDataSource
    DataSet = QrContas1
    Left = 476
    Top = 260
  end
  object DsContas2: TDataSource
    DataSet = QrContas2
    Left = 476
    Top = 289
  end
  object QrContas2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 448
    Top = 289
    object QrContas2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContas2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 284
    Top = 392
  end
end
