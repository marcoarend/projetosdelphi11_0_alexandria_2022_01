unit LctEditC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, UnMLAGeral, UnGOTOy, ComCtrls, UnMyLinguas, Db,
  mySQLDbTables, ExtCtrls, Buttons, UnInternalConsts, UMySQLModule, Grids,
  DBGrids, frxChBox, frxClass, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkEditDateTimePicker, dmkRadioGroup, Variants, dmkGeral, dmkCheckBox,
  dmkValUsu, dmkImage, DmkDAC_PF, dmkCheckGroup, UnDmkProcFunc;

type
  TTipoValor = (tvNil, tvCred, tvDeb);
  TFmLctEditC = class(TForm)
    DsContas: TDataSource;
    DsCarteiras: TDataSource;
    DsFornecedores: TDataSource;
    DsClientes: TDataSource;
    QrContas: TmySQLQuery;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasFechamento: TIntegerField;
    QrFatura: TmySQLQuery;
    QrFaturaData: TDateField;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    QrFornecedores: TmySQLQuery;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOMEENTIDADE: TWideStringField;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVendedores: TDataSource;
    DsAccounts: TDataSource;
    QrAccounts: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    QrFunci: TmySQLQuery;
    DsFunci: TDataSource;
    QrFunciCodigo: TIntegerField;
    QrFunciNOMEENTIDADE: TWideStringField;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PainelDados: TPanel;
    LaFunci: TLabel;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctLk: TIntegerField;
    QrLctFatID: TIntegerField;
    QrLctFatParcela: TIntegerField;
    QrLctCONTA: TIntegerField;
    QrLctNOMECONTA: TWideStringField;
    QrLctNOMEEMPRESA: TWideStringField;
    QrLctNOMESUBGRUPO: TWideStringField;
    QrLctNOMEGRUPO: TWideStringField;
    QrLctNOMECONJUNTO: TWideStringField;
    QrLctNOMESIT: TWideStringField;
    QrLctAno: TFloatField;
    QrLctMENSAL: TWideStringField;
    QrLctMENSAL2: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctLocal: TIntegerField;
    QrLctFatura: TWideStringField;
    QrLctSub: TSmallintField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctPago: TFloatField;
    QrLctSALDO: TFloatField;
    QrLctID_Sub: TSmallintField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctcliente: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctNOMECLIENTE: TWideStringField;
    QrLctNOMEFORNECEDOR: TWideStringField;
    QrLctTIPOEM: TWideStringField;
    QrLctNOMERELACIONADO: TWideStringField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctMulta: TFloatField;
    QrLctATRASO: TFloatField;
    QrLctJUROS: TFloatField;
    QrLctDataDoc: TDateField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctMes2: TLargeintField;
    QrLctProtesto: TDateField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TSmallintField;
    QrLctUserAlt: TSmallintField;
    QrLctControle: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctCtrlIni: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctCOMPENSADO_TXT: TWideStringField;
    QrLctCliInt: TIntegerField;
    QrLctNOMECARTEIRA: TWideStringField;
    QrLctSALDOCARTEIRA: TFloatField;
    DsLct: TDataSource;
    TabSheet2: TTabSheet;
    DsDeptos: TDataSource;
    QrDeptos: TmySQLQuery;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTipo: TIntegerField;
    QrForneceI: TmySQLQuery;
    DsForneceI: TDataSource;
    QrForneceICodigo: TIntegerField;
    QrForneceINOMEENTIDADE: TWideStringField;
    TabSheet3: TTabSheet;
    QrCarteirasExigeNumCheque: TSmallintField;
    Panel2: TPanel;
    TabSheet4: TTabSheet;
    TabControl1: TTabControl;
    DBGrid1: TDBGrid;
    Panel7: TPanel;
    GBParcelamento: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    RGArredondar: TRadioGroup;
    RGPeriodo: TRadioGroup;
    EdDias: TdmkEdit;
    RGIncremCH: TRadioGroup;
    EdParcela1: TdmkEdit;
    EdParcelaX: TdmkEdit;
    CkArredondar: TCheckBox;
    EdSoma: TdmkEdit;
    CkParcelamento: TCheckBox;
    DBGParcelas: TDBGrid;
    TbParcpagtos: TmySQLTable;
    TbParcpagtosParcela: TIntegerField;
    TbParcpagtosData: TDateField;
    TbParcpagtosCredito: TFloatField;
    TbParcpagtosDebito: TFloatField;
    TbParcpagtosDoc: TLargeintField;
    TbParcpagtosMora: TFloatField;
    TbParcpagtosMulta: TFloatField;
    TbParcpagtosICMS_V: TFloatField;
    TbParcpagtosDuplicata: TWideStringField;
    TbParcpagtosDescricao: TWideStringField;
    DsParcPagtos: TDataSource;
    QrSoma: TmySQLQuery;
    QrSomaVALOR: TFloatField;
    CkIncremDU: TCheckBox;
    RGIncremDupl: TGroupBox;
    Label27: TLabel;
    EdDuplSep: TEdit;
    RGDuplSeq: TRadioGroup;
    GBIncremTxt: TGroupBox;
    Label30: TLabel;
    EdSepTxt: TEdit;
    RGSepTxt: TRadioGroup;
    CkIncremTxt: TCheckBox;
    QrDeptosCODI_1: TIntegerField;
    QrDeptosCODI_2: TLargeintField;
    QrDeptosNOME_1: TWideStringField;
    TabSheet5: TTabSheet;
    Panel8: TPanel;
    MeConfig: TMemo;
    dmkEdParcelas: TdmkEdit;
    Panel10: TPanel;
    Label5: TLabel;
    dmkEdCNAB_Sit: TdmkEdit;
    Label6: TLabel;
    dmkEdID_pgto: TdmkEdit;
    dmkEdFatID: TdmkEdit;
    Label12: TLabel;
    Label16: TLabel;
    dmkEdTipoCH: TdmkEdit;
    dmkEdFatNum: TdmkEdit;
    Label17: TLabel;
    Label31: TLabel;
    dmkEdFatID_Sub: TdmkEdit;
    dmkEdNivel: TdmkEdit;
    Label32: TLabel;
    Label33: TLabel;
    dmkEdFatParcela: TdmkEdit;
    dmkEdCtrlIni: TdmkEdit;
    Label34: TLabel;
    Label35: TLabel;
    dmkEdDescoPor: TdmkEdit;
    dmkEdDescoVal: TdmkEdit;
    Label36: TLabel;
    Label37: TLabel;
    dmkEdUnidade: TdmkEdit;
    dmkEdDoc2: TdmkEdit;
    Label38: TLabel;
    Label39: TLabel;
    dmkEdNFVal: TdmkEdit;
    Label40: TLabel;
    Label41: TLabel;
    dmkEdTipoAnt: TdmkEdit;
    dmkEdCartAnt: TdmkEdit;
    Label42: TLabel;
    dmkEdPercJuroM: TdmkEdit;
    dmkEdPercMulta: TdmkEdit;
    Label43: TLabel;
    Label44: TLabel;
    dmkEd_: TdmkEdit;
    Label45: TLabel;
    dmkEdExecs: TdmkEdit;
    Label46: TLabel;
    dmkEdOneCliInt: TdmkEdit;
    QrLctFatNum: TFloatField;
    PnLancto1: TPanel;
    BtContas: TSpeedButton;
    CBGenero: TdmkDBLookupComboBox;
    EdGenero: TdmkEditCB;
    Label2: TLabel;
    Label3: TLabel;
    dmkEdOneForneceI: TdmkEdit;
    Label47: TLabel;
    dmkEdOneVendedor: TdmkEdit;
    Label48: TLabel;
    dmkEdOneAccount: TdmkEdit;
    Label49: TLabel;
    dmkEdOneCliente: TdmkEdit;
    dmkEdOneFornecedor: TdmkEdit;
    Label50: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesAccount: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrContasNOMEPLANO: TWideStringField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasBanco1: TIntegerField;
    CkDuplicando: TCheckBox;
    QrEventosCad: TmySQLQuery;
    QrEventosCadCodigo: TIntegerField;
    QrEventosCadCodUsu: TIntegerField;
    QrEventosCadNome: TWideStringField;
    DsEventosCad: TDataSource;
    dmkValUsu1: TdmkValUsu;
    QrCarteirasUsaTalao: TSmallintField;
    Label9: TLabel;
    EdTabLctA: TdmkEdit;
    QrLocMes: TmySQLQuery;
    QrLocMesMEZ: TIntegerField;
    Label4: TLabel;
    LaFinalidade: TLabel;
    Panel13: TPanel;
    GroupBox5: TGroupBox;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    EdOldControle: TdmkEdit;
    TPOldData: TdmkEditDateTimePicker;
    EdOldSub: TdmkEdit;
    EdOldTipo: TdmkEdit;
    EdOldCarteira: TdmkEdit;
    QrIndiPag: TmySQLQuery;
    DsIndiPag: TDataSource;
    QrIndiPagCodigo: TIntegerField;
    QrIndiPagNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel111: TPanel;
    GBVendas: TGroupBox;
    PnVendas: TPanel;
    Label10: TLabel;
    LaVendedor: TLabel;
    LaICMS_P: TLabel;
    LaICMS_V: TLabel;
    dmkCBFunci: TdmkDBLookupComboBox;
    dmkEdCBFunci: TdmkEditCB;
    dmkEdCBVendedor: TdmkEditCB;
    dmkCBVendedor: TdmkDBLookupComboBox;
    dmkEdICMS_P: TdmkEdit;
    dmkEdICMS_V: TdmkEdit;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    LaDataDoc: TLabel;
    Label22: TLabel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label8: TLabel;
    Label7: TLabel;
    dmkEdMoraDia: TdmkEdit;
    dmkEdMulta: TdmkEdit;
    GroupBox4: TGroupBox;
    Label19: TLabel;
    Label21: TLabel;
    dmkEdMultaVal: TdmkEdit;
    dmkEdMoraVal: TdmkEdit;
    GroupBox9: TGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    BitBtn2: TBitBtn;
    dmkEdValNovo: TdmkEdit;
    dmkEdPerMult: TdmkEdit;
    dmkEdTPDataDoc: TdmkEditDateTimePicker;
    dmkEdTPCompensado: TdmkEditDateTimePicker;
    dmkRGTipoCH: TdmkRadioGroup;
    Panel3: TPanel;
    PnMaskPesq: TPanel;
    GBLancto3: TGroupBox;
    CkPesqNF: TCheckBox;
    CkPesqCH: TCheckBox;
    CkPesqVal: TCheckBox;
    TabSheet6: TTabSheet;
    Panel9: TPanel;
    Panel12: TPanel;
    LaCliInt: TLabel;
    dmkEdCBCliInt: TdmkEditCB;
    dmkCBCliInt: TdmkDBLookupComboBox;
    PnDepto: TPanel;
    LaDepto: TLabel;
    dmkCBDepto: TdmkDBLookupComboBox;
    dmkEdCBDepto: TdmkEditCB;
    PnForneceI: TPanel;
    LaForneceI: TLabel;
    dmkCBForneceI: TdmkDBLookupComboBox;
    dmkEdCBForneceI: TdmkEditCB;
    PnAccount: TPanel;
    LaAccount: TLabel;
    dmkEdCBAccount: TdmkEditCB;
    dmkCBAccount: TdmkDBLookupComboBox;
    PnLancto2: TPanel;
    LaEventosCad: TLabel;
    SbEventosCad: TSpeedButton;
    Label60: TLabel;
    SbIndiPag: TSpeedButton;
    EdEventosCad: TdmkEditCB;
    CBEventosCad: TdmkDBLookupComboBox;
    EdIndiPag: TdmkEditCB;
    CBIndiPag: TdmkDBLookupComboBox;
    Panel14: TPanel;
    EdCredito: TdmkEdit;
    LaCred: TLabel;
    EdDebito: TdmkEdit;
    LaDeb: TLabel;
    EdPsqCta: TEdit;
    QrPsqCta: TmySQLQuery;
    DsPsqCta: TDataSource;
    QrPsqCtaCodigo: TIntegerField;
    QrPsqCtaNome: TWideStringField;
    QrPsqCtaNome2: TWideStringField;
    QrPsqCtaNome3: TWideStringField;
    QrPsqCtaID: TWideStringField;
    QrPsqCtaSubgrupo: TIntegerField;
    QrPsqCtaCentroCusto: TIntegerField;
    QrPsqCtaEmpresa: TIntegerField;
    QrPsqCtaCredito: TWideStringField;
    QrPsqCtaDebito: TWideStringField;
    QrPsqCtaMensal: TWideStringField;
    QrPsqCtaExclusivo: TWideStringField;
    QrPsqCtaMensdia: TSmallintField;
    QrPsqCtaMensdeb: TFloatField;
    QrPsqCtaMensmind: TFloatField;
    QrPsqCtaMenscred: TFloatField;
    QrPsqCtaMensminc: TFloatField;
    QrPsqCtaTerceiro: TIntegerField;
    QrPsqCtaExcel: TWideStringField;
    QrPsqCtaAtivo: TSmallintField;
    QrPsqCtaRateio: TIntegerField;
    QrPsqCtaEntidade: TIntegerField;
    QrPsqCtaAntigo: TWideStringField;
    QrPsqCtaPendenMesSeg: TSmallintField;
    QrPsqCtaCalculMesSeg: TSmallintField;
    QrPsqCtaLk: TIntegerField;
    QrPsqCtaDataCad: TDateField;
    QrPsqCtaDataAlt: TDateField;
    QrPsqCtaUserCad: TIntegerField;
    QrPsqCtaUserAlt: TIntegerField;
    QrPsqCtaOrdemLista: TIntegerField;
    QrPsqCtaContasAgr: TIntegerField;
    QrPsqCtaContasSum: TIntegerField;
    QrPsqCtaCtrlaSdo: TSmallintField;
    QrPsqCtaAlterWeb: TSmallintField;
    QrPsqCtaNotPrntBal: TIntegerField;
    QrPsqCtaSigla: TWideStringField;
    QrPsqCtaProvRat: TSmallintField;
    QrPsqCtaNotPrntFin: TIntegerField;
    QrPsqCtaNOMEPLANO: TWideStringField;
    QrPsqCtaNOMESUBGRUPO: TWideStringField;
    QrPsqCtaNOMEGRUPO: TWideStringField;
    QrPsqCtaNOMECONJUNTO: TWideStringField;
    QrPsqCtaNOMEEMPRESA: TWideStringField;
    Panel15: TPanel;
    DBGrid2: TDBGrid;
    Panel11: TPanel;
    GroupBox7: TGroupBox;
    Panel16: TPanel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    CkNaoPesquisar: TCheckBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel6: TPanel;
    BtConfirma: TBitBtn;
    CkContinuar: TCheckBox;
    CkCopiaCH: TCheckBox;
    BitBtn1: TBitBtn;
    Panel4: TPanel;
    PageControl2: TPageControl;
    TabSheet7: TTabSheet;
    Panel18: TPanel;
    Panel21: TPanel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    CkDataIni: TCheckBox;
    CkDataFim: TCheckBox;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    CkConta: TCheckBox;
    CBConta: TdmkDBLookupComboBox;
    CkDescricao: TCheckBox;
    EdPesqDescri: TEdit;
    CkDebito: TCheckBox;
    CkCredito: TCheckBox;
    CkNF: TCheckBox;
    CkDoc: TCheckBox;
    CkControle: TCheckBox;
    EdDebMin: TdmkEdit;
    EdCredMin: TdmkEdit;
    EdNF: TdmkEdit;
    EdDoc: TdmkEdit;
    EdControle: TdmkEdit;
    RgOrdem: TRadioGroup;
    EdDuplicata: TdmkEdit;
    CkDuplicata: TCheckBox;
    EdDebMax: TdmkEdit;
    EdCredMax: TdmkEdit;
    CkExcelGru: TCheckBox;
    EdConta: TdmkEditCB;
    CGCarteiras: TdmkCheckGroup;
    CkCliInt: TCheckBox;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    CkVctoIni: TCheckBox;
    TPVctoIni: TdmkEditDateTimePicker;
    CkVctoFim: TCheckBox;
    TPVctoFim: TdmkEditDateTimePicker;
    CBFornece: TdmkDBLookupComboBox;
    EdFornece: TdmkEditCB;
    CkFornece: TCheckBox;
    CkCliente: TCheckBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    RGTabela: TRadioGroup;
    GBAvisos1: TGroupBox;
    Panel19: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrPContas: TmySQLQuery;
    DsPContas: TDataSource;
    QrPCliInt: TmySQLQuery;
    IntegerField3: TIntegerField;
    QrCliIntNOMEENT: TWideStringField;
    DsPCliInt: TDataSource;
    QrPFornece: TmySQLQuery;
    QrPForneceCodigo: TIntegerField;
    QrPForneceNOMEENT: TWideStringField;
    DsPFornece: TDataSource;
    QrPCliente: TmySQLQuery;
    QrPClienteCodigo: TIntegerField;
    QrPClienteNOMEENT: TWideStringField;
    DsPCliente: TDataSource;
    BtPsqLct: TBitBtn;
    BtReabreLct: TBitBtn;
    Panel22: TPanel;
    Panel23: TPanel;
    Label14: TLabel;
    dmkEdControle: TdmkEdit;
    Label15: TLabel;
    dmkEdCarteira: TdmkEditCB;
    dmkCBCarteira: TdmkDBLookupComboBox;
    SbCarteira: TSpeedButton;
    Label1: TLabel;
    dmkEdTPData: TdmkEditDateTimePicker;
    LaCliente: TLabel;
    dmkEdCBCliente: TdmkEditCB;
    dmkCBCliente: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    LaFornecedor: TLabel;
    dmkEdCBFornece: TdmkEditCB;
    dmkCBFornece: TdmkDBLookupComboBox;
    LaForneceRN: TLabel;
    LaForneceFA: TLabel;
    SpeedButton2: TSpeedButton;
    Label11: TLabel;
    dmkEdDuplicata: TdmkEdit;
    GroupBox6: TGroupBox;
    Panel5: TPanel;
    Label18: TLabel;
    LaContas: TLabel;
    Panel17: TPanel;
    Label61: TLabel;
    LaCtaPsq: TLabel;
    Label13: TLabel;
    EdDescricao: TdmkEdit;
    Label20: TLabel;
    dmkEdQtde: TdmkEdit;
    LaNF: TLabel;
    dmkEdNF: TdmkEdit;
    CkCancelado: TdmkCheckBox;
    QrCliIntCtaCfgCab: TIntegerField;
    QrCliIntCodigo: TIntegerField;
    QrCliIntAccount: TIntegerField;
    QrCliIntNOMEENTIDADE: TWideStringField;
    BitBtn3: TBitBtn;
    QrMulInsLct: TmySQLQuery;
    DsMulInsLct: TDataSource;
    QrMulInsLctGeneroCod: TIntegerField;
    QrMulInsLctGeneroNom: TWideStringField;
    QrMulInsLctValor: TFloatField;
    QrMulInsLctDescricao: TWideStringField;
    QrMulInsLctAtivo: TSmallintField;
    QrMulInsLctOrdIns: TIntegerField;
    Panel25: TPanel;
    LaDoc: TLabel;
    LaMes: TLabel;
    LaVencimento: TLabel;
    dmkEdSerieCH: TdmkEdit;
    dmkEdDoc: TdmkEdit;
    EdMes: TdmkEdit;
    dmkEdTPVencto: TdmkEditDateTimePicker;
    DBGrid3: TDBGrid;
    QrMulInsLctMes: TWideStringField;
    TbParcpagtosGenero: TIntegerField;
    TbParcpagtosOrdIns: TIntegerField;
    RGFisicoSrc: TdmkRadioGroup;
    Label62: TLabel;
    EdFisicoCod: TdmkEdit;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbCarteiraClick(Sender: TObject);
    procedure QrContasAfterScroll(DataSet: TDataSet);
    procedure EdDuplicataKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabSheet2Resize(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure TPDataChange(Sender: TObject);
    procedure QrCarteirasAfterScroll(DataSet: TDataSet);
    procedure QrCliIntAfterScroll(DataSet: TDataSet);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CkParcelamentoClick(Sender: TObject);
    procedure RGPeriodoClick(Sender: TObject);
    procedure DBGParcelasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkArredondarClick(Sender: TObject);
    procedure RGArredondarClick(Sender: TObject);
    procedure EdDiasExit(Sender: TObject);
    procedure RGIncremCHClick(Sender: TObject);
    procedure CkIncremDUClick(Sender: TObject);
    procedure EdDuplSepExit(Sender: TObject);
    procedure RGDuplSeqClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure CkIncremTxtClick(Sender: TObject);
    procedure QrDeptosAfterScroll(DataSet: TDataSet);
    procedure EdGeneroChange(Sender: TObject);
    procedure EdMesExit(Sender: TObject);
    procedure EdDebitoExit(Sender: TObject);
    procedure CBGeneroClick(Sender: TObject);
    procedure EdCreditoExit(Sender: TObject);
    procedure dmkEdDocChange(Sender: TObject);
    procedure dmkEdTPVenctoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkCBForneceKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdCBDeptoChange(Sender: TObject);
    procedure dmkCBDeptoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkCBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdICMS_PExit(Sender: TObject);
    procedure dmkEdICMS_PKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdICMS_VExit(Sender: TObject);
    procedure dmkEdICMS_VKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdParcelasExit(Sender: TObject);
    procedure dmkEdCBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkDBLookupComboBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdTPDataClick(Sender: TObject);
    procedure dmkEdTPVenctoChange(Sender: TObject);
    procedure dmkEdTPVenctoClick(Sender: TObject);
    procedure dmkEdDuplicataExit(Sender: TObject);
    procedure dmkEdDocExit(Sender: TObject);
    procedure dmkEdSerieCHExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SbEventosCadClick(Sender: TObject);
    procedure dmkEdCBCliIntChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dmkEdCarteiraChange(Sender: TObject);
    procedure BtReabreLctClick(Sender: TObject);
    procedure EdMesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbIndiPagClick(Sender: TObject);
    procedure QrContasBeforeClose(DataSet: TDataSet);
    procedure QrContasAfterOpen(DataSet: TDataSet);
    procedure EdPsqCtaEnter(Sender: TObject);
    procedure EdPsqCtaChange(Sender: TObject);
    procedure QrPsqCtaBeforeClose(DataSet: TDataSet);
    procedure QrPsqCtaAfterOpen(DataSet: TDataSet);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure CkDataFimClick(Sender: TObject);
    procedure CkVctoFimClick(Sender: TObject);
    procedure EdDebMinExit(Sender: TObject);
    procedure EdCredMinExit(Sender: TObject);
    procedure BtPsqLctClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure DBGrid3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGeneroExit(Sender: TObject);
    procedure QrMulInsLctAfterOpen(DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
    procedure dmkEdDocKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdSerieCHKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FCriandoForm: Boolean;
    FValorAParcelar,
    FICMS: Double;
    FIDFinalidade: Integer;
    FParcPagtos, FMulInsLct: String;
    FPsqCta: String;
    FOrdIns: Integer;
    EdMesEnabled: Boolean;
    FParcela1, FParcelaX: Double;
    //
    procedure VerificaEdits;
    procedure CarteirasReopen;
    procedure SetaAccount(var Key: Word; Shift: TShiftState);
    procedure ExigeFuncionario;
    procedure CalculaICMS;
    function ReopenLct: Boolean;

    function VerificaVencimento(): Boolean;
    procedure ConfiguraVencimento;
    procedure ConfiguraComponentesCarteira;
    procedure ReopenFornecedores(Tipo: Integer);
    procedure ReopenIndiPag();
    // Parcelamento
    procedure CalculaParcelas();
    function GetTipoValor(): TTipoValor;
    procedure ConfiguracoesIniciais();
    procedure IncrementaExecucoes();
    procedure EventosCadReopen(Entidade: Integer);
    procedure ReopenContas(Entidade, Codigo: Integer);
    //
    //Pesquisa Lanctos
    procedure ReopenPCliInt();
    procedure ReopenMulInsLct(OrdIns: Integer);
    procedure ReopenSoma();

    procedure PulaParaItensDoMul();
    procedure ArmazenaChequeAtual(Carteira, Cheque: Integer);
    function BuscaChequeArmazenado(Carteira: Integer): Integer;

  public
    { Public declarations }
    //FQrCarteiras: TmySQLQuery; Usar UFinanceiro.VLAN_QrCarteiras
    // ??? FCarteira,
    {
    FCNAB_Sit, FFatID, FFatNum, FFAtPArcela, FFatID_Sub, FID_Pgto,
    FNivel, FDescoPor, FCtrlIni, FUnidade: Integer;
    FDescoVal, FNFVal, FPercJur, FPercMulta: Double;
    FDoc2: String;
    FCondPercJuros, FCondPercMulta: Double;
    FTPDataIni, FTPDataFim: TDateTimePicker;
    FDepto: Variant;
    FCondominio: Integer;
    }
    //
    FFisicoSrc: Integer;
    FTabLctA: String;
    //
    FQuemChamou: Integer;
    FQrCrt, FQrLct: TmySQLQuery;
    FQrLoc: TmySQLQuery;
    {FDTPDataIni: TDateTimePicker;
    FComponentClass: TComponentClass;
    FReference: TComponent;
    F_CliInt, FShowForm: Integer;
    FLocSohCliInt: Boolean;}
  end;

const
  FLargMaior = 800;
  FLargMenor = 604;

var
  FmLctEditC: TFmLctEditC;
  Pagto_Doc: Double;

implementation

uses UnMyObjects, Module, Principal, Entidades, UnFinanceiro, MyDBCheck, ModuleFin,
  ModuleGeral, UnMyPrinters, Contas, {###FinForm,} EventosCad, IndiPag,
  LocLancto, ModuleLct0, UCreateFin, LctFisico, CashTabs;

{$R *.DFM}

{
procedure TFmLctEditC.CalculaParcelas();
var
  i, Parce, DiasP, Casas: Integer;
  Valor, Valor1, ValorX, ValorA, ValorC, ValorD, Total, Mora, Multa, Fator: Double;
  Data: TDate;
  TipoValor: TTipoValor;
  Duplicata, Descricao, DuplSeq, DuplSep, Fmt, TxtSeq: String;
begin
  Screen.Cursor := crHourGlass;
  try
    TbParcpagtos.Close;
    FParcPagtos := UCriar.RecriaTempTable('ParcPagtos', DModG.QrUpdPID1, False);
    TbParcpagtos.Database := DModG.MyPID_DB;
    if GBParcelamento.Visible then
    begin
      TipoValor := GetTipoValor;
      Pagto_Doc := Geral.DMV(dmkEdDoc.Text);
      DiasP := Geral.IMV(EdDias.Text);
      Parce := Geral.IMV(dmkEdParcelas.Text);
      Mora  := Geral.DMV(dmkEdMoraDia.Text);
      Multa := Geral.DMV(dmkEdMulta.Text);
      Total := FValorAParcelar;
      if Total > 0 then
        Fator := Geral.DMV(dmkEdICMS_V.Text) / Total else Fator := 0;
      if Total <= 0 then Valor := 0
      else
      begin
        Valor := (Total / Parce)*100;
        Valor := (Trunc(Valor))/100;
      end;
      if CkArredondar.Checked then Valor := int(Valor);
      Valor1 := Valor;
      ValorX := Valor;
      if RGArredondar.ItemIndex = 0 then
      begin
        EdParcela1.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
        ValorX := Total - ((Parce - 1) * Valor);
        EdParcelaX.Text := Geral.TFT(FloatToStr(ValorX), 2, siPositivo);
      end else begin
        EdParcelaX.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
        Valor1 := Total - ((Parce - 1) * Valor);
        EdParcela1.Text := Geral.TFT(FloatToStr(Valor1), 2, siPositivo);
      end;
      //Duplicata := MLAGeral.IncrementaDuplicata(dmkEdDuplicata.Text, -1);
      Duplicata := dmkEdDuplicata.Text;
      Casas := Length(IntTostr(Parce));
      fmt := '';
      for i := 1 to Casas do fmt := fmt + '0';
      DuplSep := Trim(EdDuplSep.Text);
      if DuplSep = '' then DuplSep := ' ';
      for i := 1 to Parce do
      begin
        if i= 1 then ValorA := Valor1
        else if i = Parce then ValorA := ValorX
        else ValorA := Valor;
        if TipoValor = tvCred then ValorC := ValorA else ValorC := 0;
        if TipoValor = tvDeb  then ValorD := ValorA else ValorD := 0;
        //
        if RGPeriodo.ItemIndex = 0 then
          Data := MLAGeral.IncrementaMeses(dmkEdTPVencto.Date, i-1, True)
        else
          Data := dmkEdTPVencto.Date + (DiasP * (i-1));
        //
        if EdMesEnabled then
        begin
          if CkIncremTxt.Checked then
          begin
            case RGSepTxt.ItemIndex of
              0: TxtSeq := FormatFloat(fmt, i);
              1: TxtSeq := MLAGeral.IntToColTxt(i);
              else TxtSeq := '?';
            end;
            case RGSepTxt.ItemIndex of
              0: Descricao := FormatFloat(fmt, Parce);
              1: Descricao := MLAGeral.IntToColTxt(Parce);
              else TxtSeq := '?';
            end;
            TxtSeq := TxtSeq + EdSepTxt.Text;
            Descricao := TxtSeq + Descricao + ' ' + EdDescricao.Text;
          end else
            Descricao := EdDescricao.Text
        end else
          Descricao := IntToStr(i) + '�/'+ IntToStr(Parce) + ' ' +EdDescricao.Text;
        //Duplicata := MLAGeral.IncrementaDuplicata(Duplicata, 1);
        //
        if CkIncremDU.Checked then
        begin
          case RGDuplSeq.ItemIndex of
            0: DuplSeq := FormatFloat(fmt, i);
            1: DuplSeq := MLAGeral.IntToColTxt(i);
            else DuplSeq := '?';
          end;
          DuplSeq := DuplSep + DuplSeq;
        end else DuplSeq := '';
        DmodG.QrUpdPID1.SQL.Clear;
        DmodG.QrUpdPID1.SQL.Add('INSERT INTO parcpagtos SET Parcela=:P0, ');
        DmodG.QrUpdPID1.SQL.Add('Data=:P1, Credito=:P2, Debito=:P3, Doc=:P4, ');
        DmodG.QrUpdPID1.SQL.Add('Mora=:P5, Multa=:P6, ICMS_V=:P7, Duplicata=:P8, ');
        DmodG.QrUpdPID1.SQL.Add('Descricao=:P9 ');
        DmodG.QrUpdPID1.Params[0].AsInteger := i;
        DmodG.QrUpdPID1.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Data);
        DmodG.QrUpdPID1.Params[2].AsFloat := ValorC;
        DmodG.QrUpdPID1.Params[3].AsFloat := ValorD;
        DmodG.QrUpdPID1.Params[4].AsFloat := Pagto_Doc;
        DmodG.QrUpdPID1.Params[5].AsFloat := Mora;
        DmodG.QrUpdPID1.Params[6].AsFloat := Multa;
        DmodG.QrUpdPID1.Params[7].AsFloat := Fator * (ValorC+ValorD);
        DmodG.QrUpdPID1.Params[8].AsString := Duplicata + DuplSeq;
        DmodG.QrUpdPID1.Params[9].AsString := Descricao;
        DmodG.QrUpdPID1.ExecSQL;
        if not RGIncremCH.Enabled then Pagto_Doc := Pagto_Doc
        else Pagto_Doc := Pagto_Doc + RGIncremCH.ItemIndex;
        //
      end;
      TbParcpagtos.Open;
      TbParcpagtos.EnableControls;
    end;
    QrSoma.Close;
    QrSoma.Database := DModG.MyPID_DB;
    QrSoma.Open;
    EdSoma.Text := FormatFloat('#,###,##0.00', QrSomaVALOR.Value);
  finally
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TFmLctEditC.CalculaParcelas();
var
  i, Parce, DiasP, Casas: Integer;
  Valor, Valor1, ValorX, ValorA, ValorC, ValorD, Total, Mora, Multa, Fator: Double;
  Data: TDate;
  TipoValor: TTipoValor;
  Duplicata, Descricao, DuplSeq, DuplSep, Fmt, TxtSeq: String;
  //
  Doc, Credito, Debito, ICMS_V: Double;
  Genero, Parcela, OrdIns: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    FParcela1 := 0;
    FParcelaX := 0;
    TbParcpagtos.Close;
    FParcPagtos :=
      UCriarFin.RecriaTempTableNovo(ntrtt_ParcPagtos, DModG.QrUpdPID1, False);
    TbParcpagtos.Database := DModG.MyPID_DB;
    //
    if GBParcelamento.Visible then
    begin
      QrMulInsLct.DisableControls;
      try
        QrMulInsLct.First;
        while not QrMulInsLct.Eof do
        begin
          TipoValor := GetTipoValor;
          Pagto_Doc := Geral.DMV(dmkEdDoc.Text);
          DiasP := Geral.IMV(EdDias.Text);
          Parce := Geral.IMV(dmkEdParcelas.Text);
          Mora  := Geral.DMV(dmkEdMoraDia.Text);
          Multa := Geral.DMV(dmkEdMulta.Text);
          Total := FValorAParcelar;
          if Total > 0 then
            Fator := Geral.DMV(dmkEdICMS_V.Text) / Total else Fator := 0;
          if Total <= 0 then Valor := 0
          else
          begin
            Valor := (Total / Parce)*100;
            Valor := (Trunc(Valor))/100;
          end;
          if CkArredondar.Checked then Valor := int(Valor);
          Valor1 := Valor;
          ValorX := Valor;
          if RGArredondar.ItemIndex = 0 then
          begin
            EdParcela1.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
            ValorX := Total - ((Parce - 1) * Valor);
            EdParcelaX.Text := Geral.TFT(FloatToStr(ValorX), 2, siPositivo);
          end else begin
            EdParcelaX.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
            Valor1 := Total - ((Parce - 1) * Valor);
            EdParcela1.Text := Geral.TFT(FloatToStr(Valor1), 2, siPositivo);
          end;
          FParcela1 := FParcela1 + EdParcela1.ValueVariant;
          FParcelaX := FParcelaX + EdParcelaX.ValueVariant;
          //
          //Duplicata := MLAGeral.IncrementaDuplicata(dmkEdDuplicata.Text, -1);
          Duplicata := dmkEdDuplicata.Text;
          Casas := Length(IntTostr(Parce));
          fmt := '';
          for i := 1 to Casas do fmt := fmt + '0';
          DuplSep := Trim(EdDuplSep.Text);
          if DuplSep = '' then DuplSep := ' ';
          for i := 1 to Parce do
          begin
            if i= 1 then ValorA := Valor1
            else if i = Parce then ValorA := ValorX
            else ValorA := Valor;
            if TipoValor = tvCred then ValorC := ValorA else ValorC := 0;
            if TipoValor = tvDeb  then ValorD := ValorA else ValorD := 0;
            //
            if RGPeriodo.ItemIndex = 0 then
              Data := MLAGeral.IncrementaMeses(dmkEdTPVencto.Date, i-1, True)
            else
              Data := dmkEdTPVencto.Date + (DiasP * (i-1));
            //
            if EdMesEnabled then
            begin
              if CkIncremTxt.Checked then
              begin
                case RGSepTxt.ItemIndex of
                  0: TxtSeq := FormatFloat(fmt, i);
                  1: TxtSeq := DmkPF.IntToColTxt(i);
                  else TxtSeq := '?';
                end;
                case RGSepTxt.ItemIndex of
                  0: Descricao := FormatFloat(fmt, Parce);
                  1: Descricao := DmkPF.IntToColTxt(Parce);
                  else TxtSeq := '?';
                end;
                TxtSeq := TxtSeq + EdSepTxt.Text;
                Descricao := TxtSeq + Descricao + ' ' + QrMulInsLctDescricao.Value;
              end else
                Descricao := QrMulInsLctDescricao.Value
            end else
              Descricao := IntToStr(i) + '�/' + IntToStr(Parce) + ' ' +
                QrMulInsLctDescricao.Value;
            //Duplicata := MLAGeral.IncrementaDuplicata(Duplicata, 1);
            //
            if CkIncremDU.Checked then
            begin
              case RGDuplSeq.ItemIndex of
                0: DuplSeq := FormatFloat(fmt, i);
                1: DuplSeq := DmkPF.IntToColTxt(i);
                else DuplSeq := '?';
              end;
              DuplSeq := DuplSep + DuplSeq;
            end else DuplSeq := '';
{
            DmodG.QrUpdPID1.SQL.Clear;
            DmodG.QrUpdPID1.SQL.Add('INSERT INTO parcpagtos SET Parcela=:P0, ');
            DmodG.QrUpdPID1.SQL.Add('Data=:P1, Credito=:P2, Debito=:P3, Doc=:P4, ');
            DmodG.QrUpdPID1.SQL.Add('Mora=:P5, Multa=:P6, ICMS_V=:P7, Duplicata=:P8, ');
            DmodG.QrUpdPID1.SQL.Add('Descricao=:P9 ');
            DmodG.QrUpdPID1.Params[0].AsInteger := i;
            DmodG.QrUpdPID1.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Data);
            DmodG.QrUpdPID1.Params[2].AsFloat := ValorC;
            DmodG.QrUpdPID1.Params[3].AsFloat := ValorD;
            DmodG.QrUpdPID1.Params[4].AsFloat := Pagto_Doc;
            DmodG.QrUpdPID1.Params[5].AsFloat := Mora;
            DmodG.QrUpdPID1.Params[6].AsFloat := Multa;
            DmodG.QrUpdPID1.Params[7].AsFloat := Fator * (ValorC+ValorD);
            DmodG.QrUpdPID1.Params[8].AsString := Duplicata + DuplSeq;
            DmodG.QrUpdPID1.Params[9].AsString := Descricao;
            DmodG.QrUpdPID1.ExecSQL;
}
            Doc := Pagto_Doc;
            Credito := ValorC;
            Debito := ValorD;
            ICMS_V := Fator * (ValorC+ValorD);
            Genero := QrMulInsLctGeneroCod.Value;
            Parcela := I;
            OrdIns := QrMulInsLctOrdIns.Value;
            //
            UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FParcPagtos, False, [
            (*'SerieCH',*) 'Doc', 'Data',
            'Credito', 'Debito', 'Mora',
            'Multa', 'ICMS_V', 'Duplicata',
            'Descricao', 'Genero'], [
            'Parcela', 'OrdIns'], [
            (*SerieCH,*) Doc, Geral.FDT(Data, 1),
            Credito, Debito, Mora,
            Multa, ICMS_V, Duplicata + DuplSeq,
            Descricao, Genero], [
            Parcela, OrdIns], False);
            //
            if not RGIncremCH.Enabled then Pagto_Doc := Pagto_Doc
            else Pagto_Doc := Pagto_Doc + RGIncremCH.ItemIndex;
            //
          end;
          QrMulInsLct.Next;
        end;
        TbParcpagtos.Open;
        TbParcpagtos.EnableControls;
        EdParcela1.ValueVariant := FParcela1;
        EdParcelaX.ValueVariant := FParcelaX;
      finally
        QrMulInsLct.EnableControls;
      end;
    end;
    ReopenSoma();
    EdSoma.ValueVariant := QrSomaVALOR.Value;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctEditC.CarteirasReopen;
begin
  QrCarteiras.Close;
  QrCarteiras.SQL.Clear;
  QrCarteiras.SQL.Add('SELECT car.*');
  QrCarteiras.SQL.Add('FROM carteiras car');
  QrCarteiras.SQL.Add('WHERE car.ForneceI IN (' + VAR_LIB_EMPRESAS + ')');
  QrCarteiras.SQL.Add('ORDER BY Nome');
  QrCarteiras.Open;
end;

procedure TFmLctEditC.VerificaEdits;
begin
  if (QrContasMensal.Value = 'V') or (Dmod.QrControleMensalSempre.Value=1) then
  begin
    EdMesEnabled := True;
    LaMes.Enabled := True;
  end else begin
    EdMesEnabled := False;
    LaMes.Enabled := False;
  end;
  //
  if (VAR_BAIXADO <> -2) and not VAR_FATURANDO then Exit;
  if QrContasCredito.Value = 'V' then
  begin
    EdCredito.Enabled := True;
    LaCred.Enabled := True;
    dmkEdCBCliente.Enabled := True;
    LaCliente.Enabled := True;
    dmkCBCliente.Enabled := True;
  end else begin
    EdCredito.Enabled := False;
    LaCred.Enabled := False;
    dmkEdCBCliente.Enabled := False;
    LaCliente.Enabled := False;
    dmkCBCliente.Enabled := False;
  end;

  if QrContasDebito.Value = 'V' then
  begin
    EdDebito.Enabled := True;
    LaDeb.Enabled := True;
    dmkEdCBFornece.Enabled := True;
    LaFornecedor.Enabled := True;
    dmkCBFornece.Enabled := True;
  end else begin
    EdDebito.Enabled := False;
    LaDeb.Enabled := False;
    dmkEdCBFornece.Enabled := False;
    LaFornecedor.Enabled := False;
    dmkCBFornece.Enabled := False;
  end;

end;

procedure TFmLctEditC.BtDesisteClick(Sender: TObject);
begin
  Close;
  VAR_VALOREMITIRIMP := 0;
end;

procedure TFmLctEditC.BtPsqLctClick(Sender: TObject);
var
  Cursor : TCursor;
  DataIni, DataFim, VctoIni, VctoFim: String;
  i: Integer;
  TabLct: String;
begin
  FQrCrt := DmLct0.QrCrt;
  FQrLct := DmLct0.QrLct;
  //
  if UFinanceiro.TabLctNaoDefinida(FTabLctA, 'TFmLctEditC.FormActivate()') then
    Exit;
  ///
  //////////////////////////////////////////////////////////////////////////////
  ///
  TabLct := Copy(FTabLctA, 1, Length(FTabLctA) -1) +
    Lowercase(RGTabela.Items[RGTabela.ItemIndex][1]);
  //if FShowForm = 0 then
  //begin
    MyObjects.CriaForm_AcessoTotal(TFmLocLancto, FmLocLancto);
    FmLocLancto.FModuleLctX := DmLct0;
    FmLocLancto.PnTabLctA.Visible := RGTabela.ItemIndex = 0;
    FmLocLancto.FConfirmou    := False;
    FmLocLancto.FQrCrt        := FQrCrt;
    FmLocLancto.FQrLct        := FQrLct;
    FmLocLancto.FLocSohCliInt := CkCliInt.Checked;
    FmLocLancto.FCliLoc       := Geral.IMV(EdCliInt.Text);
    FQrLoc := FmLocLancto.QrLoc;
    case FQuemChamou of
      0: ; // Nada
      1:
      begin
        FmLocLancto.GBConfirma2.Visible := True;
        FmLocLancto.GBConfirma1.Visible := False;
      end;
      2: ;// EventosCad
    end;
  //end;
  //
  i := 0;
  DataIni := FormatDateTime(VAR_FORMATDATE, TPDataIni.Date);
  DataFim := FormatDateTime(VAR_FORMATDATE, TPDataFim.Date);
  VctoIni := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date);
  VctoFim := FormatDateTime(VAR_FORMATDATE, TPVctoFim.Date);
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  Refresh;
  try
    FQrLoc.SQL.Clear;
    //FQrLoc.SQL.Add('SELECT MONTH(la.Mes) Mes2, YEAR(la.Mes) Ano, ');
    FQrLoc.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
    FQrLoc.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
    FQrLoc.SQL.Add('ca.Nome NOMECARTEIRA, la.*, co.Nome NOMEGENERO, ');
    FQrLoc.SQL.Add('ca.ForneceI CLIENTE_INTERNO ');
    FQrLoc.SQL.Add('FROM ' + TabLct + ' la, carteiras ca, contas co');
    FQrLoc.SQL.Add('WHERE ca.Codigo=la.Carteira');
    FQrLoc.SQL.Add('AND co.Codigo=la.Genero');
    // configura data de emiss�o
    if CkDataFim.Checked then
      FQrLoc.SQL.Add('AND la.Data BETWEEN '''+DataIni+''' AND '''+DataFim+'''')
      else if CkDataIni.Checked then
        FQrLoc.SQL.Add('AND la.Data='''+DataIni+'''');
    // configura data de vencimento
    if CkVctoFim.Checked then
      FQrLoc.SQL.Add('AND la.Vencimento BETWEEN '''+VctoIni+''' AND '''+VctoFim+'''')
      else if CkVctoIni.Checked then
        FQrLoc.SQL.Add('AND la.Vencimento='''+VctoIni+'''');
    // configara Conta
    if CkConta.Checked = True then
    begin
      if CBConta.KeyValue <> Null then
      FQrLoc.SQL.Add('AND la.Genero = '''+IntToStr(CBConta.KeyValue)+'''');
    end;
    // configara Fornecedor
    if CkFornece.Checked = True then
    begin
      if CBFornece.KeyValue <> Null then
      FQrLoc.SQL.Add('AND la.Fornecedor = '''+IntToStr(CBFornece.KeyValue)+'''');
    end;
    // configara Cliente
    if CkCliente.Checked = True then
    begin
      if CBCliente.KeyValue <> Null then
      FQrLoc.SQL.Add('AND la.Cliente = '''+IntToStr(CBCliente.KeyValue)+'''');
    end;
    // configara descri��o
    if CkDescricao.Checked = True then
      FQrLoc.SQL.Add('AND la.Descricao LIKE '''+EdPesqDescri.Text+'''');
    // configara valores
    if CkDebito.Checked then i := i + 1;
    if CkCredito.Checked then i := i + 2;
    case i of
      1: FQrLoc.SQL.Add('AND la.Debito BETWEEN :P0 AND :P1');
      2: FQrLoc.SQL.Add('AND la.Credito BETWEEN :P0 AND :P1');
      3:
      begin
        FQrLoc.SQL.Add('AND la.Debito BETWEEN :P0 AND :P1');
        FQrLoc.SQL.Add('AND la.Credito BETWEEN :P2 AND :P3');
      end;
    end;
    case i of
      1:
      begin
        FQrLoc.Params[0].AsFloat := Geral.DMV(EdDebMin.Text);
        FQrLoc.Params[1].AsFloat := Geral.DMV(EdDebMax.Text);
      end;
      2:
      begin
        FQrLoc.Params[0].AsFloat := Geral.DMV(EdCredMin.Text);
        FQrLoc.Params[1].AsFloat := Geral.DMV(EdCredMax.Text);
      end;
      3:
      begin
        FQrLoc.Params[0].AsFloat := Geral.DMV(EdDebMin.Text);
        FQrLoc.Params[1].AsFloat := Geral.DMV(EdDebMax.Text);
        FQrLoc.Params[2].AsFloat := Geral.DMV(EdCredMin.Text);
        FQrLoc.Params[3].AsFloat := Geral.DMV(EdCredMax.Text);
      end;
    end;
    if CkNF.Checked = True then
      FQrLoc.SQL.Add('AND la.NotaFiscal='+
      IntToStr(Geral.IMV(EdNF.Text)));
    // configara Duplicata
    if CkDuplicata.Checked = True then
      FQrLoc.SQL.Add('AND la.Duplicata="'+
      EdDuplicata.Text + '"');
    // configara Controle
    if CkControle.Checked = True then
      FQrLoc.SQL.Add('AND la.Controle='+IntToStr(Geral.IMV(EdControle.Text)));
    // configara Documento
    if CkDoc.Checked = True then
      FQrLoc.SQL.Add('AND la.Documento='+IntToStr(Geral.IMV(EdDoc.Text)));
    // Configura ExcelGru
    if CkExcelGru.Checked = True then
      FQrLoc.SQL.Add('AND (la.ExcelGru IS Null OR la.ExcelGru = 0)');
    // Tipo de carteiras
    case CGcarteiras.Value of
      1: FQrLoc.SQL.Add('AND ca.Tipo = 0');
      2: FQrLoc.SQL.Add('AND ca.Tipo = 1');
      3: FQrLoc.SQL.Add('AND ca.Tipo in (0,1)');
      4: FQrLoc.SQL.Add('AND ca.Tipo = 2');
      5: FQrLoc.SQL.Add('AND ca.Tipo in (0,2)');
      6: FQrLoc.SQL.Add('AND ca.Tipo in (1,2)');
      7: ; // Qualquer coisa
      else Geral.MensagemBox('Nenhum tipo de carteira foi definido!',
        'Aviso', MB_OK+MB_ICONWARNING);
    end;
    if CkCliInt.Checked then FQrLoc.SQL.Add('AND ca.ForneceI = ' +
      FormatFloat('0', Geral.IMV(EdCliInt.Text)));

    // Ordem
    case RGOrdem.ItemIndex of
      0: FQrLoc.SQL.Add('ORDER BY la.Data, la.Controle');
      1: FQrLoc.SQL.Add('ORDER BY la.Controle');
      2: FQrLoc.SQL.Add('ORDER BY la.NotaFiscal');
      3: FQrLoc.SQL.Add('ORDER BY la.Documento');
      4: FQrLoc.SQL.Add('ORDER BY la.Descricao');
      5: FQrLoc.SQL.Add('ORDER BY la.Debito, la.Credito');
      6: FQrLoc.SQL.Add('ORDER BY la.Credito, la.Debito');
    end;
    try
      UMyMod.AbreQuery(FQrLoc, Dmod.MyDB);
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(FQrLoc, '', nil, True, True);
    end;
  finally
    Screen.Cursor := Cursor;
  end;

  //

  {case FShowForm of
    0:
    begin}
      FmLocLancto.ShowModal;
      //
      VAR_LOC_LCTO_TRUE := FmLocLancto.FConfirmou;
      VAR_LOC_LCTO_CTRL := FmLocLancto.QrLocControle.Value;
      VAR_LOC_LCTO_SUB  := FmLocLancto.QrLocSub.Value;
      VAR_LOC_LCTO_DEBI := FmLocLancto.QrLocDebito.Value;
      VAR_LOC_LCTO_CRED := FmLocLancto.QrLocCredito.Value;
      //
      FmLocLancto.Destroy;
      {if FQuemChamou = 1 then
        Close;
    end;
    1: Close;
    2: Close; // EventosCad
  end;}
  //
  if VAR_LOC_LCTO_TRUE then
  begin
    CkDuplicando.Checked := False;
    //
    // Parei aqui
{ TODO :           CONTINUAR AQUI!!!!! }
    {
    Fazer aqui a duplica��o
    Duplicar o que?
    Na verdade Abrir uma query copiando os valores para os campos!
    }
    //
  end;
end;

procedure TFmLctEditC.BtReabreLctClick(Sender: TObject);
begin
  if VLAN_QRLCT <> nil then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      VLAN_QRLCT.Close;
      VLAN_QRLCT.Open;
      VLAN_QRLCT.Locate('Controle', FLAN_Controle, []);
      //
      BtReabreLct.Enabled := False;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

function TFmLctEditC.BuscaChequeArmazenado(Carteira: Integer): Integer;
var
  I, K: Integer;
begin
  Result := 0;
  K := High(FmLctFisico.FArrCartCod);
  for I := 0 to K do
  begin
    if FmLctFisico.FArrCartCod[I] = Carteira then
    begin
      Result := FmLctFisico.FArrCartDoc[I] + 1;
      Break;
    end;
  end;
end;

procedure TFmLctEditC.BtConfirmaClick(Sender: TObject);
var
  Cartao, Carteira, Genero, TipoAnt, CartAnt, NF, Sit, Linha, Depto, Fornecedor,
  Cliente, CliInt, ForneceI, Vendedor, Account, Funci, Parcelas, DifMes,
  Me1, Me2, EventosCad: Integer;
  Controle: Int64;
  Credito, Debito, MoraDia, Multa, ICMS_V, Difer, MultaVal, MoraVal: Double;
  Mes: Integer;
  MesTxt, Vencimento, Compensado: String;
  Inseriu: Boolean;
  MesX, AnoX: Word;
  DataX: TDateTime;
  SerieCH: String;
  ValParc, Documen, QtdFolhasCh: Double;
  EhMulInsLct: Boolean;

begin
  try
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando inclus�o');
  //
  Me1 := 0;
  //
  //  Veriificar integridade do parcelamento
  if MyObjects.FIC(CkParcelamento.Checked and (ImgTipo.SQLType = stUpd), nil,
  'N�o pode haver parcelamento em altera��o!') then
    Exit;
  if MyObjects.FIC(CkParcelamento.Checked and
  (QrSomaVALOR.Value <> EdSoma.ValueVariant), nil,
  'Valor do parcelamento n�o confere com d�bito/cr�dito!') then
    Exit;
  // fim integridade parcelamento

  Screen.Cursor := crHourGlass;
  if dmkEdCBFornece.Enabled = False then
    Fornecedor := 0
  else
    Fornecedor := dmkEdCBFornece.ValueVariant;
  if dmkEdCBCliente.Enabled = False then
    Cliente := 0
  else
    Cliente := dmkEdCBCliente.ValueVariant;
  Vendedor := Geral.IMV(dmkEdCBVendedor.Text);
  Account  := Geral.IMV(dmkEdCBAccount.Text);
  Depto    := Geral.IMV(dmkEdCBDepto.Text);
  ForneceI := Geral.IMV(dmkEdCBForneceI.Text);
  CliInt   := Geral.IMV(dmkEdCBCliInt.Text);
  if (CliInt = 0) or not DmodG.EmpresaLogada(CliInt)then
  begin
    Geral.MensagemBox('ERRO! Cliente interno inv�lido!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    LaCliInt.Enabled := True;
    dmkEdCBCliInt.Enabled := True;
    dmkCBCliInt.Enabled := True;
    if dmkEdCBCliInt.Enabled then dmkEdCBCliInt.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (VAR_AVISOSCXAEDIT in ([1,3,5,7]))
  and (Vendedor < 1) and (dmkCBVendedor.Visible) and ((*Panel3*)GBVendas.Visible) then
  begin
    if Geral.MensagemBox('Deseja continuar mesmo sem definir o vendedor?',
    'Aviso de escolha de vendedor', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      if dmkCBVendedor.Enabled then dmkCBVendedor.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  if (VAR_AVISOSCXAEDIT in ([2,3,6,7]))
  and (Account < 1) and (dmkCBAccount.Visible) then
  begin
    if Geral.MensagemBox('Deseja continuar mesmo sem definir o Representante?',
    'Aviso de escolha de Representante', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      if dmkCBAccount.Enabled then dmkCBAccount.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  VAR_CANCELA := False;
  if not VerificaVencimento() then
  begin
   Screen.Cursor := crDefault;
   Exit;
  end;
  Cartao := 0;
  Compensado := CO_VAZIO;
  if VAR_FATURANDO then
  begin
    (*
    desabilitei at� ser necess�rio 2009-12-21
    Cartao := FmPrincipal.CartaoDeFatura;
    Compensado := FmPrincipal.CompensacaoDeFatura;
    *)
  end else if dmkEdTPCompensado.Date > 1 then
    Compensado := Geral.FDT(dmkEdTPCompensado.Date, 1);
  if (dmkEdNF.Enabled = False) then NF := 0
  else NF := Geral.IMV(dmkEdNF.Text);
  Credito := Geral.DMV(EdCredito.Text);
  if VAR_CANCELA then
  begin
    Geral.MensagemBox('Inclus�o cancelada pelo usu�rio!',
    'Cancelaqmento de Inclus�o', MB_OK + MB_ICONEXCLAMATION);
    if dmkEdNF.Enabled then dmkEdNF.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  Debito  := Geral.DMV(EdDebito.Text);
  MoraDia := Geral.DMV(dmkEdMoraDia.Text);
  Multa   := Geral.DMV(dmkEdMulta.Text);
  ICMS_V  := Geral.DMV(dmkEdICMS_V.Text);
  Difer   := Credito-Debito;
  if ((Difer<0) and (ICMS_V>0)) or ((Difer>0) and (ICMS_V<0)) then
  begin
    Geral.MensagemBox('ICMS incorreto! Para d�bitos o ICMS deve ser negativo!',
    'Cancelaqmento de Inclus�o', MB_OK + MB_ICONEXCLAMATION);
    if dmkEdICMS_P.Enabled then dmkEdICMS_P.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (EdCredito.Enabled = False) and (Credito <> 0) and (VAR_BAIXADO = -2) then
  begin
    EdCredito.Text := '0';
    Credito := 0;
  end;
  if (EdDebito.Enabled = False) and (Debito <> 0) and (VAR_BAIXADO = -2) then
  begin
    EdDebito.Text := '0';
    Debito := 0;
  end;
  Carteira := dmkEdCarteira.ValueVariant;
  if Carteira = 0 then
  begin
    Geral.MensagemBox('ERRO. Defina uma carteira!', 'Erro', MB_OK+MB_ICONERROR);
    if dmkEdCarteira.Enabled then dmkEdCarteira.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (dmkCBFunci.Visible = True) and
     ((*Panel3*)GBVendas.Visible = True) and (dmkCBFunci.KeyValue = Null)then
  begin
    Geral.MensagemBox('ERRO. Defina um funcion�rio!', 'Erro', MB_OK+MB_ICONERROR);
    if dmkEdCBFunci.Enabled
    and dmkEdCBFunci.Visible then
    begin
      PageControl1.ActivePageIndex := 1;
      dmkEdCBFunci.SetFocus;
    end;
    Screen.Cursor := crDefault;
    Exit;
  end else Funci := Geral.IMV(dmkEdCBFunci.Text);
  if Funci = 0 then Funci := VAR_USUARIO;
  Genero := EdGenero.ValueVariant;

  EhMulInsLct := (QrMulInsLct.State <> dsInactive) and (QrMulInsLct.recordCount > 0);
  // Lan�amento direto (sem pr�-lan�amento)
  if EhMulInsLct = False then
  begin
    if Genero = 0 then
    begin
      Geral.MensagemBox('ERRO. Defina uma conta!', 'Erro', MB_OK+MB_ICONERROR);
      if EdGenero.Enabled then EdGenero.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
    if (Credito > 0) and (QrContasCredito.Value = 'F') then
    begin
      Geral.MensagemBox('ERRO. Valor n�o pode ser cr�dito', 'Erro', MB_OK+MB_ICONERROR);
      EdCredito.ValueVariant;
      Screen.Cursor := crDefault;
      Exit;
    end;
    if (Debito > 0) and (QrContasDebito.Value = 'F') then
    begin
      Geral.MensagemBox('ERRO. Valor n�o pode ser d�bito', 'Erro', MB_OK+MB_ICONERROR);
      EdDebito.ValueVariant;
      Screen.Cursor := crDefault;
      Exit;
    end;
    if (EdCredito.Enabled = False) and (EdDebito.Enabled = False) and
    (VAR_BAIXADO = -2) then
    begin
      Geral.MensagemBox('ERRO. Opera��o imposs�vel. Conta sem permiss�o de d�bito ou cr�dito.', 'Erro', MB_OK+MB_ICONERROR);
      Screen.Cursor := crDefault;
      Exit;
    end;
    if (Credito = 0) and (Debito = 0) and (CkCancelado.Checked = False) then
    begin
      Geral.MensagemBox('ERRO. Defina um d�bito ou um cr�dito.', 'Erro', MB_OK+MB_ICONERROR);
      if EdDebito.Enabled = True then EdDebito.SetFocus
      else if EdCredito.Enabled = True then EdCredito.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
    if EdMes.Text = CO_VAZIO then Mes := 0 else
    if (EdMes.Enabled = False) then Mes := 0 else
    begin
      MesTxt := (*EdMes.Text[4]+EdMes.Text[5]+*)EdMes.Text[6] + EdMes.Text[7]+(*'/'+*)
      EdMes.Text[1] + EdMes.Text[2](*+'/01'*);
      Mes := Geral.IMV(MesTxt);
      Me1 := Geral.Periodo2000(dmkEdTPVencto.Date);
      AnoX := Geral.IMV(Copy(MesTxt, 1, 2)) + 2000;
      MesX := Geral.IMV(Copy(MesTxt, 3, 2));
      DataX := EncodeDate(AnoX, MesX, 1);
      Me2 := Geral.Periodo2000(DataX);
      DifMes := Me2 - Me1;
      if Mes = 0 then
      begin
        Geral.MensagemBox('ERRO. Defina o m�s do vencimento.', 'Erro', MB_OK+MB_ICONERROR);
        if EdMesEnabled then EdMes.SetFocus;
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    if (Mes = 0) and (EdMesEnabled = True) then
    begin
      Geral.MensagemBox('ERRO. Defina o m�s do vencimento.', 'Erro', MB_OK+MB_ICONERROR);
      if EdMesEnabled then EdMes.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  // Fim parcial > Lan�amento direto (sem pr�-lan�amento)



  // n�o mexer no vencimento aqui!
  (*if (dmkEdTPVencto.Enabled = False) then Vencimento :=
    FormatDateTime(VAR_FORMATDATE, dmkEdTPdata.Date)
  else*)
  Vencimento := FormatDateTime(VAR_FORMATDATE, dmkEdTPVencto.Date);
  if VAR_IMPORTANDO then Linha := VAR_IMPLINHA else Linha := 0;
  if VAR_FATURANDO then Sit := 3
  else if VAR_IMPORTANDO then Sit := -1
    else if VAR_BAIXADO <> -2 then Sit := VAR_BAIXADO
      else if QrCarteirasTipo.Value = 2 then Sit := 0
        else Sit := 3;
  TipoAnt := StrToInt(dmkEdTipoAnt.Text);
  CartAnt := StrToInt(dmkEdCartAnt.Text);
  MultaVal := Geral.DMV(dmkEdMultaVal.Text);
  MoraVal := Geral.DMV(dmkEdMoraVal.Text);
  if ImgTipo.SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', FTabLctA, LAN_CTOS, 'Controle');
    VAR_DATAINSERIR := dmkEdTPdata.Date;

  end else begin
    Controle := Geral.IMV(dmkEdControle.Text);
(*
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add(EXCLUI_DE + FTabLctA + ' WHERE Controle=:P0');
    Dmod.QrUpdM.Params[0].AsFloat := Controle;
    Dmod.QrUpdM.ExecSQL;
*)
    UFinanceiro.ExcluiLct_Unico(FTabLcta, Dmod.MyDB, TPOldData.Date,
      EdOldTipo.ValueVariant, EdOldCarteira.ValueVariant,
      EdOldControle.ValueVariant, EdOldSub.ValueVariant,
      CO_MOTVDEL_104_ALTERALCTFISICO, False);
    UFinanceiro.RecalcSaldoCarteira(CartAnt, nil, nil, False, False);
    VAR_DATAINSERIR := dmkEdTPdata.Date;
  end;

  if CkCancelado.Checked then
  begin
    Credito    := 0;
    Debito     := 0;
    Compensado := FormatDateTime(VAR_FORMATDATE, dmkEdTPData.Date);
    Sit        := 4;
  end;

  (*
  Colocar em baixo de onde obtem o n�mero do cheque para n�o dar erro (Continuar Inserindo!
  if not CkNaoPesquisar.Checked then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando duplica��es');
    if UFinanceiro.NaoDuplicarLancto(ImgTipo.SQLType, dmkEdDoc.ValueVariant,
    CkPesqCH.Checked, dmkEdSerieCH.Text, dmkEdNF.ValueVariant, CkPesqNF.Checked,
    Credito, Debito, Genero, Mes, CkPesqVal.Checked,
    Geral.IMV(dmkEdCBCliInt.Text), False, dmkEdCarteira.ValueVariant,
    dmkEdCBCliente.ValueVariant, dmkEdCBFornece.ValueVariant,
    LaAviso, FtabLctA, '', '') <> 1 then
    begin
      Geral.MensagemBox('Inclus�o cancelada pelo usu�rio!', 'Aviso', MB_OK+MB_ICONWARNING);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...';
      Exit;
    end;
  end;
  *)

  //

  if (ImgTipo.SQLType = stUpd) then
  begin
    Documen := dmkEdDoc.ValueVariant;
    SerieCH := dmkEdSerieCH.Text;
  end else
  {
  if (QrCarteirasUsaTalao.Value = 1) and (ImgTipo.SQLType = stIns)  and
  (dmkEdDoc.Enabled = False) and CkDuplicando.Checked then
  begin
    Documen := dmkEdDoc.ValueVariant;
    SerieCH := dmkEdSerieCH.Text;
    //
  end else
  (*  ERRO?
  //Ap�s obter o novo n�mero ele seta o campo e d� erro no Continuar Inserindo.
  //if (QrCarteirasUsaTalao.Value = 1) and (ImgTipo.SQLType = stIns) and
  //(dmkEdDoc.ValueVariant = 0) then
  *)
  if (QrCarteirasUsaTalao.Value = 1) and (ImgTipo.SQLType = stIns) and
  (dmkEdDoc.Enabled = False) then
  begin
    if CkParcelamento.Checked then
    begin
      QtdFolhasCh := TbParcpagtos.RecordCount;
      if EhMulInsLct then
        QtdFolhasCh := QtdFolhasCh / QrMulInsLct.RecordCount;
      if UMyMod.ObtemQtdeCHTaloes(QrCarteirasCodigo.Value) < QtdFolhasCh then
        if Geral.MensagemBox(
        'N�o existem folhas de cheque suficientes para todos lan�amentos.' +
        sLineBreak + 'Deseja continuar assim mesmo?', 'Pergunta',
        MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
          Exit;
    end;
    //  Parcelamento ser� feito adiante
    if CkParcelamento.Checked = False then
    begin
      if not UMyMod.ObtemProximoCHTalao(QrCarteirasCodigo.Value,
      dmkEdSerieCH, dmkEdDoc) then
        Exit
      else begin
        Documen := dmkEdDoc.ValueVariant;
        SerieCH := dmkEdSerieCH.Text;
      end;
    end;
  end else
  }
  if (dmkEdDoc.Enabled = False) and (VAR_BAIXADO = -2) then
    Documen := 0
  else begin
    Documen := Geral.DMV(dmkEdDoc.Text);
    SerieCH := dmkEdSerieCH.Text;
    if (QrCarteirasExigeNumCheque.Value = 1) and (Documen=0) then
    begin
      Geral.MensagemBox('Esta carteira exige o n�mero do cheque (documento)!',
      'Aviso', MB_OK+MB_ICONWARNING);
      dmkEdDoc.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;

  if not CkNaoPesquisar.Checked then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando duplica��es');
    if UFinanceiro.NaoDuplicarLancto(ImgTipo.SQLType, dmkEdDoc.ValueVariant,
    CkPesqCH.Checked, dmkEdSerieCH.Text, dmkEdNF.ValueVariant, CkPesqNF.Checked,
    Credito, Debito, Genero, Mes, CkPesqVal.Checked,
    Geral.IMV(dmkEdCBCliInt.Text), False, dmkEdCarteira.ValueVariant,
    dmkEdCBCliente.ValueVariant, dmkEdCBFornece.ValueVariant,
    LaAviso1, LaAviso2, FtabLctA, '', '') <> 1 then
    begin
      Geral.MensagemBox('Inclus�o cancelada pelo usu�rio!', 'Aviso', MB_OK+MB_ICONWARNING);
      MyObjects.Informa2(LaAviso1, LaAviso2, True, '...');
      Exit;
    end;
  end;

  if (Cliente = 0) and (Fornecedor = 0) then
  begin
    Cliente := dmkEdCBCliente.ValueVariant;
    Fornecedor := dmkEdCBFornece.ValueVariant;
  end;

  VAR_LANCTO2 := Controle;

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados');
  UFinanceiro.LancamentoDefaultVARS;
  FLAN_EventosCad := EdEventosCad.ValueVariant;
  if FLAN_EventosCad <> 0 then
    FLAN_EventosCad := QrEventosCadCodigo.Value;
  FLAN_Documento  := Trunc(Documen);
  FLAN_Data       := FormatDateTime(VAR_FORMATDATE, dmkEdTPdata.Date);
  FLAN_Tipo       := QrCarteirasTipo.Value;
  FLAN_Carteira   := Carteira;
  FLAN_Credito    := Credito;
  FLAN_Debito     := Debito;
  FLAN_Genero     := Genero;
  FLAN_NotaFiscal := NF;
  FLAN_Vencimento := Vencimento;
  FLAN_Mez        := Mes;
  FLAN_Descricao  := EdDescricao.Text;
  FLAN_Sit        := Sit;
  FLAN_Controle   := Controle;
  FLAN_Cartao     := Cartao;
  FLAN_Compensado := Compensado;
  FLAN_Linha      := Linha;
  FLAN_Fornecedor := Fornecedor;
  FLAN_Cliente    := Cliente;
  FLAN_MoraDia    := MoraDia;
  FLAN_Multa      := Multa;
  FLAN_DataCad    := FormatDateTime(VAR_FORMATDATE, Date);
  FLAN_UserCad    := Funci;
  FLAN_DataDoc    := FormatDateTime(VAR_FORMATDATE, dmkEdTPDataDoc.Date);
  FLAN_Vendedor   := Vendedor;
  FLAN_Account    := Account;
  FLAN_FatID      := dmkEdFatID.ValueVariant;
  FLAN_FatID_Sub  := dmkEdFatID_Sub.ValueVariant;
  FLAN_ICMS_P     := Geral.DMV(dmkEdICMS_P.Text);
  FLAN_ICMS_V     := Geral.DMV(dmkEdICMS_V.Text);
  FLAN_Duplicata  := dmkEdDuplicata.Text;
  FLAN_CliInt     := CliInt;
  FLAN_Depto      := Depto;
  FLAN_DescoPor   := dmkEdDescoPor.ValueVariant;
  FLAN_ForneceI   := ForneceI;
  FLAN_DescoVal   := dmkEdDescoVal.ValueVariant;
  FLAN_NFVal      := dmkEdNFVAl.ValueVariant;
  FLAN_Unidade    := dmkEdUnidade.ValueVariant;
  //FLAN_Qtde       := Qtde;  ABAIXO
  FLAN_FatNum     := MLAGeral.I64MV(dmkEdFatNum.ValueVariant);
  FLAN_FatParcela := dmkedFatParcela.ValueVariant;
  FLAN_Doc2       := dmkEdDoc2.ValueVariant;
  FLAN_SerieCH    := dmkEdSerieCh.Text;
  FLAN_MultaVal   := MultaVal;
  FLAN_MoraVal    := MoraVal;
  FLAN_TipoCH     := dmkRGTipoCH.ItemIndex;
  FLAN_IndiPag    := EdIndiPag.ValueVariant;
  FLAN_FisicoSrc  := RGFisicoSrc.ItemIndex;
  FLAN_FisicoCod  := EdFisicoCod.ValueVariant;
  // Evitar erro na duplica��o!
  if ImgTipo.SQLType = stUpd then
  begin
    FLAN_ID_Pgto    := dmkEdID_Pgto.ValueVariant;
    FLAN_CNAB_Sit   := dmkEdCNAB_Sit.ValueVariant;
    FLAN_Nivel      := dmkEdNivel.ValueVariant;
    FLAN_CtrlIni    := dmkEdCtrlIni.ValueVariant;
  end;
  if dmkEdQtde.Text <> '' then
    FLAN_Qtde := Geral.DMV(dmkEdQtde.Text);

  Parcelas := 0;
  if CkParcelamento.Checked then
  begin
    TbParcpagtos.First;
    while not TbParcpagtos.Eof do
    begin
      if (Me1 > 0) then
      begin
        Me2            := Geral.Periodo2000(TbParcpagtosData.Value) + DifMes;
        FLAN_Mez       := dmkPF.PeriodoToAnoMes(Me2);
        FLAN_Descricao := TbParcPagtosDescricao.Value;
        //
        if (CkIncremTxt.Checked = False) then
          FLAN_Descricao  := FLAN_Descricao + ' - ' + MLAGeral.MesEAnoDoPeriodo(Me2)
      end else
      begin
        FLAN_Mez       := 0;
        FLAN_Descricao := TbParcPagtosDescricao.Value;
      end;
      if Parcelas > 0 then
        FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
          'Controle', FTabLctA, LAN_CTOS, 'Controle');
      FLAN_Duplicata  := TbParcPagtosDuplicata.Value;
      FLAN_Credito    := TbParcpagtosCredito.Value;
      FLAN_Debito     := TbParcpagtosDebito.Value;
      FLAN_MoraDia    := dmkEdPercJuroM.ValueVariant;//FmCondGer.QrCondPercJuros.Value;
      FLAN_Multa      := dmkEdPercMulta.ValueVariant;//FmCondGer.QrCondPercMulta.Value;
      FLAN_ICMS_V     := TbParcpagtosICMS_V.Value;
      FLAN_Vencimento := Geral.FDT(TbParcpagtosData.Value, 1);
      FLAN_Sit        := Sit;
      {if (QrCarteirasUsaTalao.Value = 1) and (ImgTipo.SQLType = stIns)  and
      (dmkEdDoc.Enabled = False) and CkDuplicando.Checked then
      begin
        FLAN_Documento := dmkEdDoc.ValueVariant;
        FLAN_SerieCH   := dmkEdSerieCH.Text;
      end else
      if (QrCarteirasUsaTalao.Value = 1) and (ImgTipo.SQLType = stIns)  and
      (CkDuplicando.Checked = False) then
      begin
        if not UMyMod.ObtemProximoCHTalao(QrCarteirasCodigo.Value,
        dmkEdSerieCH, dmkEdDoc) then
          Exit
        else
        begin
          FLAN_Documento := dmkEdDoc.ValueVariant;
          FLAN_SerieCH   := dmkEdSerieCH.Text;
        end;
      end else
        FLAN_Documento := TbParcpagtosDoc.Value;
      }
      FLAN_SerieCH   := dmkEdSerieCH.Text;
      FLAN_Documento := TbParcpagtosDoc.Value;
      //
      if UFinanceiro.InsereLancamento(FTabLctA) then
      begin
        Inc(Parcelas, 1);
        IncrementaExecucoes;
        ArmazenaChequeAtual(FLAN_Carteira, Trunc(FLAN_Documento));
      end;
      TbParcPagtos.Next;
    end;
    Inseriu := TbParcPagtos.RecordCount = Parcelas;
    if not Inseriu then
      Geral.MensagemBox('ATEN��O! Foram inseridas ' +
      IntToStr(Parcelas) + ' de ' + IntToStr(TbParcPagtos.RecordCount) + '!',
      'Aviso de diverg�ncias', MB_OK+MB_ICONWARNING);
  end else
  if EhMulInsLct then
  begin
    QrMulInsLct.DisableControls;
    try
      QrMulInsLct.First;
      while not QrMulInsLct.Eof do
      begin
        FLAN_Descricao := QrMulInsLctDescricao.Value;
        FLAN_Mez       := Geral.IMV(Geral.MesBarraAnoToMez(QrMulInsLctMes.Value));
        if (FLAN_Mez <> 0) then
          FLAN_Descricao  := FLAN_Descricao + ' - ' + QrMulInsLctMes.Value;
        if Parcelas > 0 then
          FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
            'Controle', FTabLctA, LAN_CTOS, 'Controle');
        //FLAN_Duplicata  := QrMulInsLctDuplicata.Value;
        if QrMulInsLctValor.Value > 0 then
        begin
          FLAN_Credito    := QrMulInsLctValor.Value;
          FLAN_Debito     := 0;
        end else
        begin
          FLAN_Credito    := 0;
          FLAN_Debito     := - QrMulInsLctValor.Value;
        end;
        FLAN_MoraDia    := dmkEdPercJuroM.ValueVariant;//FmCondGer.QrCondPercJuros.Value;
        FLAN_Multa      := dmkEdPercMulta.ValueVariant;//FmCondGer.QrCondPercMulta.Value;
        //FLAN_ICMS_V     := QrMulInsLctICMS_V.Value;
        //FLAN_Vencimento := Geral.FDT(QrMulInsLctData.Value, 1);
        FLAN_Sit        := Sit;
        {
        if (QrCarteirasUsaTalao.Value = 1) and (ImgTipo.SQLType = stIns)  and
        (dmkEdDoc.Enabled = False) and CkDuplicando.Checked then
        begin
          FLAN_Documento := dmkEdDoc.ValueVariant;
          FLAN_SerieCH   := dmkEdSerieCH.Text;
        end else
        if (QrCarteirasUsaTalao.Value = 1) and (ImgTipo.SQLType = stIns)  and
        (CkDuplicando.Checked = False) then
        begin
          if not UMyMod.ObtemProximoCHTalao(QrCarteirasCodigo.Value,
          dmkEdSerieCH, dmkEdDoc) then
            Exit
          else
          begin
            FLAN_Documento := dmkEdDoc.ValueVariant;
            FLAN_SerieCH   := dmkEdSerieCH.Text;
          end;
        end else
        begin
        }
          FLAN_Documento := dmkEdDoc.ValueVariant;
          FLAN_SerieCH   := dmkEdSerieCH.Text;
        {
        end;
        }
        //
        if UFinanceiro.InsereLancamento(FTabLctA) then
        begin
          // N�o tem parcelas
          Inc(Parcelas, 1);
          IncrementaExecucoes;
          ArmazenaChequeAtual(FLAN_Carteira, Trunc(FLAN_Documento));
        end;
        QrMulInsLct.Next;
      end;
      Inseriu := QrMulInsLct.RecordCount = Parcelas;
      if not Inseriu then
        Geral.MensagemBox('ATEN��O! Foram inseridas ' +
        IntToStr(Parcelas) + ' de ' + IntToStr(QrMulInsLct.RecordCount) + '!',
        'Aviso de diverg�ncias', MB_OK+MB_ICONWARNING);
    finally
      QrMulInsLct.EnableControls;
    end;
  end else
  begin
    Inseriu := UFinanceiro.InsereLancamento(FTabLctA);
    ArmazenaChequeAtual(FLAN_Carteira, Trunc(FLAN_Documento));
  end;

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Incrementando execu��es');
  if Inseriu then
  begin
    IncrementaExecucoes;
    //
    if ImgTipo.SQLType = stIns then
      UFinanceiro.RecalcSaldoCarteira(dmkCBCarteira.KeyValue, nil, nil, False, False)
    else// begin if CBNovo.Checked = False then
    begin
      UFinanceiro.RecalcSaldoCarteira(dmkCBCarteira.KeyValue, nil, nil, False, False);
      if CartAnt <> dmkCBCarteira.KeyValue then
        UFinanceiro.RecalcSaldoCarteira(CartAnt, nil, nil, False, False);
    end;
    //
    VAR_VALOREMITIRIMP := VAR_VALOREMITIRIMP - Credito - Debito;
    (*###
    if CkCopiaCH.Checked then
      FmFinForm.ImprimeCopiaDeCh(FLAN_Controle, Geral.IMV(dmkEdCBCliInt.Text));
    *)
    if (CkContinuar.Checked) and (ImgTipo.SQLType = stIns) then
    begin
      CkParcelamento.Checked       := False;
      PageControl1.ActivePageIndex := 0;
      Geral.MensagemBox('Inclus�o concluida.', 'Aviso', MB_OK+MB_ICONINFORMATION);
      if Pagecontrol1.ActivePageIndex = 0 then
      if dmkEdTPData.Enabled then
        dmkEdTPData.SetFocus
      else
        EdGenero.SetFocus;
      BtReabreLct.Enabled := True;
    end else Close;
  end;
  finally
    Screen.Cursor := crDefault;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //CkDuplicando.Checked := False; ??
  end;
end;

procedure TFmLctEditC.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Contin: Integer;
begin
  if CkContinuar.Checked then
    Contin := 1
  else
    Contin := 0;
  Geral.WriteAppKeyCU('CkContinuar', Application.Title + '\LctEdit', Contin, ktInteger);
end;

procedure TFmLctEditC.FormCreate(Sender: TObject);
var
  CliInt: Integer;
begin
  ImgTipo.SQLType := stIns;
  //
  MyObjects.ConfiguraRadioGroup(RGFisicoSrc, CO_TAB_INDX_CB4_DESCREVE,
    Length(CO_TAB_INDX_CB4_DESCREVE), 0);
  //
  EdMesEnabled := False;
  FOrdIns := 0;
  FPsqCta := EdPsqCta.Text;

  //
  dmkEdTPData.MinDate := VAR_DATA_MINIMA;
  PnLancto1.BevelOuter  := bvNone;
  PnDepto.BevelOuter    := bvNone;
  PnForneceI.BevelOuter := bvNone;
  PnAccount.BevelOuter  := bvNone;
  //PnEdit05.BevelOuter := bvNone;
  //PnEdit06.BevelOuter := bvNone;
  PnLancto2.BevelOuter  := bvNone;
  //PnLancto3.BevelOuter  := bvNone;
  //Width := FLargMenor;
  FCriandoForm := True;
  // 2012-05-15
  //QrContas.Open;
  ReopenContas(0, 0);
  // FIM 2012-05-15
  if VAR_CXEDIT_ICM then
  begin
    LaICMS_P.Visible := True;
    LaICMS_V.Visible := True;
    dmkEdICMS_P.Visible := True;
    dmkEdICMS_V.Visible := True;
  end;
  //if Trim(VAR_CLIENTEC) <> '' then
  //begin
    QrClientes.SQL.Clear;
    QrClientes.SQL.Add('SELECT Codigo, Account, ');
    QrClientes.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrClientes.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrClientes.SQL.Add('FROM entidades');
    QrClientes.SQL.Add('WHERE Cliente1="V"');
    QrClientes.SQL.Add('OR    Cliente2="V"');
    QrClientes.SQL.Add('OR    Cliente3="V"');
    QrClientes.SQL.Add('OR    Cliente4="V"');
    QrClientes.SQL.Add('OR    Terceiro="V"');
    QrClientes.SQL.Add('ORDER BY NOMEENTIDADE');
    QrClientes.Open;
    //////
    LaCliente.Visible := True;
    dmkEdCBCliente.Visible := True;
    dmkCBCliente.Visible := True;
    //////
    //dmkEdCBCliInt.Enabled := False;
    //dmkCBCliInt.Enabled := False;
    //if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
    CliInt := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
    dmkEdCBCliInt.Text := IntToStr(CliInt);
    dmkCBCliInt.KeyValue := CliInt;
  //end;
  ReopenFornecedores(0);
  if Trim(VAR_FORNECEF) <> '' then
  begin
    //
    LaFornecedor.Visible := True;
    dmkEdCBFornece.Visible := True;
    dmkCBFornece.Visible := True;
  end;
  if Trim(VAR_FORNECEV) <> '' then
  begin
    QrVendedores.SQL.Clear;
    QrVendedores.SQL.Add('SELECT Codigo, ');
    QrVendedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrVendedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrVendedores.SQL.Add('FROM entidades');
    QrVendedores.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrVendedores.SQL.Add('ORDER BY NOMEENTIDADE');
    QrVendedores.Open;
    //////
    QrAccounts.SQL.Clear;
    QrAccounts.SQL.Add('SELECT Codigo, ');
    QrAccounts.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrAccounts.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrAccounts.SQL.Add('FROM entidades');
    QrAccounts.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrAccounts.SQL.Add('ORDER BY NOMEENTIDADE');
    QrAccounts.Open;
    //////
    LaVendedor.Visible := True;
    dmkEdCBVendedor.Visible := True;
    dmkCBVendedor.Visible   := True;
    PnAccount.Visible := True;
  end;
  dmkEdTPDataDoc.Date := Date;
  if VAR_LANCTOCONDICIONAL1 then
  begin
    VAR_DATAINSERIR := Date;
    dmkEdTPDataDoc.Enabled := False;
    LaDataDoc.Enabled := False;
    //
    dmkEdCarteira.Enabled := False;
    dmkCBCarteira.Enabled := False;
    dmkEdTPdata.Enabled := False;
  end;
  if VAR_TIPOVARCLI <> '' then ExigeFuncionario;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  TabControl1.TabIndex := 0;
  //
  CkContinuar.Checked := Geral.ReadAppKeyCU('CkContinuar', Application.Title +
  '\LctEdit', ktInteger, 0);
  ReopenIndiPag();
//procedure T F m L o c D e f s.FormCreate(Sender: TObject);
//begin
  TPDataIni.Date := 0;
  TPDataFim.Date := 0;
  TPVctoIni.Date := 0;
  TPVctoFim.Date := 0;
  //FShowForm      := 0;
  UMyMod.AbreQuery(QrPContas, Dmod.MyDB);
  UMyMod.AbreQuery(QrPCliente, Dmod.MyDB);
  UMyMod.AbreQuery(QrPFornece, Dmod.MyDB);
  CGCarteiras.SetMaxValue();
  //ImgTipo.SQLType := stPsq;
  /////////////////////
  ReopenPCliInt(); ////
  /////////////////////
//end;
  FMulInsLct := UCriarFin.RecriaTempTableNovo(ntrtt_MulInsLct, DModG.QrUpdPID1, False);
  //TbMulInsLct.Database := DModG.MyPID_DB;
  // Fazer aqui tamb�m para n�o dar erro!
  FParcPagtos := UCriarFin.RecriaTempTableNovo(ntrtt_ParcPagtos, DModG.QrUpdPID1, False);
end;

procedure TFmLctEditC.FormDestroy(Sender: TObject);
begin
  case RGFisicoSrc.ItemIndex of
    1: FmLctFisico.Show;
    2: FmLctFisico.Show;
  end;
end;

procedure TFmLctEditC.FormActivate(Sender: TObject);
var
  Carteira: Integer;
begin
  FTabLctA := EdTabLctA.Text;
  if UFinanceiro.TabLctNaoDefinida(FTabLctA, 'TFmLctEdit.FormActivate()') then
    Close;
  CarteirasReopen;
  if ImgTipo.SQLType = stIns then
  begin
    CkContinuar.Visible := True;
    CkParcelamento.Visible := True;
    //
    if VLAN_QrCarteiras <> nil then
    begin
      Carteira := VLAN_QrCarteiras.FieldByName('Codigo').AsInteger;
      if Carteira <> 0 then
      begin
        dmkEdCarteira.ValueVariant := Carteira;
        dmkCBCarteira.KeyValue     := Carteira;
      end;
    end;
  end;
  //if dmkEdCarteira.Enabled then dmkEdCarteira.SetFocus;
  if EdGenero.Enabled then
    EdGenero.SetFocus;
  VerificaEdits;
  if FCriandoForm then
  begin
    ConfiguracoesIniciais;
  end;
  FCriandoForm := False;
  MyObjects.CorIniComponente();
  //
end;

procedure TFmLctEditC.ConfiguraComponentesCarteira;
begin
  case QrCarteirasTipo.Value of
    0:
    begin
      if QrCarteirasPrazo.Value = 1 then
      begin
        LaDoc.Enabled := True;
        dmkEdSerieCH.Enabled := True;
        dmkEdDoc.Enabled := True;
        dmkEdNF.Enabled := True;
        LaNF.Enabled := True;
      end else begin
        // N�mero bloqueto no cheque
        LaDoc.Enabled := True;
        dmkEdSerieCH.Enabled := True;
        dmkEdDoc.Enabled := True;
        LaDoc.Enabled := True;
        //
        dmkEdNF.Enabled := True;
        LaNF.Enabled := True;
      end;
    end;
    1:
    begin
      LaDoc.Enabled := True;
      dmkEdSerieCH.Enabled := True;
      dmkEdDoc.Enabled := True;
    end;
    2:
    begin
      LaDoc.Enabled := True;
      dmkEdSerieCH.Enabled := True;
      dmkEdDoc.Enabled := True;
      //
      dmkEdNF.Enabled := True;
      LaNF.Enabled := True;
    end;
  end;
  ConfiguraVencimento;
end;

procedure TFmLctEditC.BtContasClick(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := dmkEdCBCliInt.ValueVariant;
  //
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.LocCod(QrContasCodigo.Value, QrContasCodigo.Value);
    FmContas.ShowModal;
    FmContas.Destroy;
    ReopenContas(Entidade, 0);
    EdGenero.Text := IntToStr(VAR_CONTA);
    CBGenero.KeyValue := VAR_CONTA;
    //
    if (ImgTipo.SQLType = stIns) and (CkDuplicando.Checked = False) then
      EdDescricao.Text := CBGenero.Text;
  end;
end;

function TFmLctEditC.VerificaVencimento(): Boolean;
var
  Carteira: Integer;
  Data: TDateTime;
begin
  Result := True;
  if ImgTipo.SQLType = stIns then
  begin
    if QrCarteirasFatura.Value = 'V' then
    begin
      if dmkCBCarteira.KeyValue <> Null then Carteira := dmkCBCarteira.KeyValue
      else Carteira := 0;
      QrFatura.Close;
      QrFatura.Params[0].AsInteger := Carteira;
      QrFatura.Open;
      if (QrFatura.RecordCount > 0) and (dmkCBCarteira.KeyValue <> Null) then
      begin
        Data := QrFaturaData.Value;
        while (Data - QrCarteirasFechamento.Value) < dmkEdTPdata.Date do
          Data := IncMonth(Data, 1);
        if int(dmkEdTPVencto.Date) <> Int(Data) then
        begin
          case Geral.MensagemBox(
          'A configura��o do sistema sugere a data de vencimento '+
          FormatDateTime(VAR_FORMATDATE3, Data)+' ao inv�s de '+
          FormatDateTime(VAR_FORMATDATE3, dmkEdTPVencto.Date)+
          '. Confirma a altera��o?', VAR_APPNAME,
          MB_ICONQUESTION+MB_YESNOCANCEL+MB_DEFBUTTON1+MB_APPLMODAL) of
            ID_YES    : dmkEdTPVencto.Date := Data;
            ID_CANCEL : Result := False;
            ID_NO     : ;
          end;
        end;
      end;
      QrFatura.Close;
    end;
  end;
end;

procedure TFmLctEditC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctEditC.SbCarteiraClick(Sender: TObject);
begin
  VAR_CARTNUM := Geral.IMV(dmkEdCarteira.Text);
  FmPrincipal.CadastroDeCarteiras(QrCarteirasTipo.Value,
    Geral.IMV(dmkEdCarteira.Text), VAR_CARTNUM);
  QrCarteiras.Close;
  QrCarteiras.Open;
end;

procedure TFmLctEditC.SbEventosCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEventosCad, FmEventosCad, afmoNegarComAviso) then
  begin
    FmEventosCad.ShowModal;
    FmEventosCad.Destroy;
    //
    UMyMod.SetaCodUsuDeCodigo(EdEventosCad, CBEventosCad, QrEventosCad,
        VAR_CADASTRO);
  end;
end;

procedure TFmLctEditC.SbIndiPagClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmIndiPag, FmIndiPag, afmoNegarComAviso) then
  begin
    FmIndiPag.ShowModal;
    FmIndiPag.Destroy;
    //
    UMyMod.SetaCodigoPesquisado(EdIndiPag, CBIndiPag, QrIndiPag,
      VAR_CADASTRO);
  end;
end;

procedure TFmLctEditC.QrContasAfterOpen(DataSet: TDataSet);
begin
  LaContas.Caption := Geral.FFN(QrContas.RecordCount, 3);
end;

procedure TFmLctEditC.QrContasAfterScroll(DataSet: TDataSet);
begin
  VerificaEdits;
end;

procedure TFmLctEditC.QrContasBeforeClose(DataSet: TDataSet);
begin
  LaContas.Caption := '000';
end;

procedure TFmLctEditC.SetaAccount(var Key: Word; Shift: TShiftState);
begin
  if key=VK_F4 then
  begin
    dmkEdCBAccount.Text   := IntToStr(QrClientesAccount.Value);
    dmkCBAccount.KeyValue := QrClientesAccount.Value;
  end;
end;

procedure TFmLctEditC.ExigeFuncionario;
begin
  QrFunci.Close;
  QrFunci.SQL.Clear;
  QrFunci.SQL.Add('SELECT Codigo,');
  QrFunci.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
  QrFunci.SQL.Add('ELSE Nome END NOMEENTIDADE');
  QrFunci.SQL.Add('FROM entidades');
  QrFunci.SQL.Add('WHERE '+VAR_TIPOVARCLI+'="V"');
  QrFunci.SQL.Add('ORDER BY NomeENTIDADE');
  QrFunci.Open;
  Label10.Visible      := True;
  dmkEdCBFunci.Visible := True;
  dmkCBFunci.Visible   := True;
end;

procedure TFmLctEditC.CalculaICMS();
{
var
  C, D, B, V, P: Double;
}
begin
{
  V := Geral.DMV(dmkEdICMS_V.Text);
  P := Geral.DMV(dmkEdICMS_P.Text);
  C := Geral.DMV(Credito);
  D := Geral.DMV(Debito);
  //if C > D then B := C - D else B := D - C;
  B := C-D;
  if FICMS = 0 then V := B * P / 100
  else if B <> 0 then P := V / B * 100 else P := 0;
  dmkEdICMS_V.Text := Geral.FFT(V, 2, siPositivo);
  dmkEdICMS_P.Text := Geral.FFT(P, 2, siPositivo);
}
end;

procedure TFmLctEditC.EdDuplicataKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key in ([VK_DOWN, VK_UP]) then
  begin
    if (key=VK_DOWN) then
      dmkEdDuplicata.Text := MLAGeral.DuplicataIncrementa(dmkEdDuplicata.Text, -1)
    else
      dmkEdDuplicata.Text := MLAGeral.DuplicataIncrementa(dmkEdDuplicata.Text,  1);
  end;
end;

function TFmLctEditC.ReopenLct: Boolean;
var
  Ini, Fim: String;
begin
  Ini := FormatDateTime(VAR_FORMATDATE, 1);
  Fim := FormatDateTime(VAR_FORMATDATE, Date);
  QrLct.Close;
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
  QrLct.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
  QrLct.SQL.Add('la.*, ct.Codigo CONTA,');
  QrLct.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
  QrLct.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
  QrLct.SQL.Add('CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial');
  QrLct.SQL.Add('ELSE em.Nome END NOMEEMPRESA,');
  QrLct.SQL.Add('CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  QrLct.SQL.Add('ELSE cl.Nome END NOMECLIENTE,');
  QrLct.SQL.Add('CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial');
  QrLct.SQL.Add('ELSE fo.Nome END NOMEFORNECEDOR,');
  QrLct.SQL.Add('ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA');
  {#### colocar B e D?}
  QrLct.SQL.Add('FROM ' + FTabLctA + ' la');
  QrLct.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  QrLct.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  QrLct.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  QrLct.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  QrLct.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  QrLct.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrLct.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  QrLct.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  //
  QrLct.SQL.Add('WHERE la.Data BETWEEN "'+Ini+'" AND "'+Fim+'"');
  //
  case TabControl1.TabIndex of
    0: QrLct.SQL.Add('AND la.Carteira='  +IntToStr(Geral.IMV(dmkEdCarteira.Text)));
    1: QrLct.SQL.Add('AND ca.ForneceI='  +IntToStr(Geral.IMV(dmkEdCBCliInt.Text)));
    2: QrLct.SQL.Add('AND la.Cliente='   +IntToStr(Geral.IMV(dmkEdCBCliente.Text)));
    3: QrLct.SQL.Add('AND la.Fornecedor='+IntToStr(Geral.IMV(dmkEdCBFornece.Text)));
    4: QrLct.SQL.Add('AND la.Genero='    +IntToStr(Geral.IMV(EdGenero.Text)));
  end;
  QrLct.SQL.Add('ORDER BY la.Data DESC, la.Controle DESC');
  QrLct.Open;
  Result := True;
end;

procedure TFmLctEditC.ReopenMulInsLct(OrdIns: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMulInsLct, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FMulInsLct,
  'ORDER BY OrdIns ',
  '']);
end;

procedure TFmLctEditC.ReopenPCliInt();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPCliInt, Dmod.MyDB, [
  'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT ',
  'FROM entidades ',
  'WHERE Codigo IN (' + VAR_LIB_EMPRESAS + ') ',
  'ORDER BY NOMEENT ',
  '']);
end;

procedure TFmLctEditC.ReopenSoma();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoma, DModG.MyPID_DB, [
  'SELECT SUM(Credito - Debito) VALOR ',
  'FROM ' + FParcPagtos,
  '']);
end;

procedure TFmLctEditC.TabSheet2Resize(Sender: TObject);
begin
  (*DBGrid1.Align := alClient;
  DBGrid1.Align := alNone;
  DBGrid1.Top := 24;
  DBGrid1.Height := DBGrid1.Height - 24;*)
end;

procedure TFmLctEditC.PageControl1Change(Sender: TObject);
begin
  ReopenLct;
{ Desabilitado em 2012-07-15 - Habilitar de novo?
  if PageControl1.ActivePageIndex in ([2,3]) then
    WindowState := wsMaximized
  else
    WindowState := wsNormal;
}
end;

procedure TFmLctEditC.PulaParaItensDoMul();
begin
  if (QrMulInsLct.State <> dsInactive)
  and (QrMulInsLct.RecordCount  > 0) then
  begin
    if EdDebito.Visible and EdDebito.Enabled then
      EdDebito.SetFocus
    else
    if EdCredito.Visible and EdCredito.Enabled then
      EdCredito.SetFocus
    else
    if EdDescricao.Visible and EdDescricao.Enabled then
      EdDescricao.SetFocus;
  end
  else
    dmkEdSerieCH.SetFocus;
end;

procedure TFmLctEditC.TabControl1Change(Sender: TObject);
begin
  ReopenLct;
end;

procedure TFmLctEditC.TPDataChange(Sender: TObject);
begin
  ConfiguraVencimento;
end;

procedure TFmLctEditC.ConfiguraVencimento;
begin
  if ImgTipo.SQLType = stIns then
    dmkEdTPVencto.Date := dmkEdTPdata.Date;
end;

procedure TFmLctEditC.QrCarteirasAfterScroll(DataSet: TDataSet);
begin
  ConfiguraComponentesCarteira;
end;

procedure TFmLctEditC.ReopenContas(Entidade, Codigo: Integer);
var
  CtaCfgCab: Integer;
  SQL1: String;
begin
  QrContas.Close;
  if QrCliInt.State <> dsInactive then
  begin
    QrCliInt.Locate('Codigo', Entidade, []);
    //
    CtaCfgCab := QrCliIntCtaCfgCab.Value;
    if CtaCfgCab <> 0 then
    begin
      SQL1 :=
      'AND co.Codigo IN ( ' + sLineBreak +
      '     SELECT Genero ' + sLineBreak +
      '     FROM ctacfgits ' + sLineBreak +
      '     WHERE Codigo=' + Geral.FF0(CtaCfgCab) + ') ';
    end else
      SQL1 := '';
    UnDmkDAC_PF.AbreMySQLQuery0(QrContas, Dmod.MyDB, [
    'SELECT co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO, ',
    'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO, ',
    'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA ',
    'FROM contas co ',
    'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo ',
    'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo ',
    'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto ',
    'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano ',
    'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa ',
    'WHERE co.Terceiro=0 ',
    'AND co.Codigo>0 ',
    SQL1,
    'ORDER BY co.Nome ',
    '']);
    //
    if Codigo <> 0 then
      QrContas.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmLctEditC.ReopenFornecedores(Tipo: Integer);
begin
  QrFornecedores.Close;
  QrFornecedores.SQL.Clear;
  QrFornecedores.SQL.Add('SELECT Codigo, ');
  if Tipo = 0 then
  begin
    QrFornecedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrFornecedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    //
    LaFornecedor.Font.Color := LaForneceRN.Font.Color;
  end else begin
    QrFornecedores.SQL.Add('CASE WHEN Tipo=0 THEN Fantasia');
    QrFornecedores.SQL.Add('ELSE Apelido END NOMEENTIDADE');
    //
    LaFornecedor.Font.Color := LaForneceFA.Font.Color;
  end;
  QrFornecedores.SQL.Add('FROM entidades');
  QrFornecedores.SQL.Add('WHERE Fornece1="V"');
  QrFornecedores.SQL.Add('OR    Fornece2="V"');
  QrFornecedores.SQL.Add('OR    Fornece3="V"');
  QrFornecedores.SQL.Add('OR    Fornece4="V"');
  QrFornecedores.SQL.Add('OR    Fornece5="V"');
  QrFornecedores.SQL.Add('OR    Fornece6="V"');
  QrFornecedores.SQL.Add('OR    Terceiro="V"');
  QrFornecedores.SQL.Add('ORDER BY NOMEENTIDADE');
  QrFornecedores.Open;
  //
end;

procedure TFmLctEditC.ReopenIndiPag();
begin
  QrIndiPag.Close;
  QrIndiPag.Open;
end;

procedure TFmLctEditC.QrCliIntAfterScroll(DataSet: TDataSet);
begin
  if FIDFinalidade = 0 then
    FIDFinalidade := Geral.IMV(Copy(LaFinalidade.Caption, 1, 3));
  case FIDFinalidade of
    1: // Pr�prios
    begin
      //Nada
    end;
    2: // Condom�nios
    begin
      QrForneceI.Close;
      QrForneceI.SQL.Clear;
{
      QrForneceI.SQL.Add('SELECT Codigo,');
      QrForneceI.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
      QrForneceI.SQL.Add('ELSE Nome END NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM entidades');
      QrForneceI.SQL.Add('WHERE Cliente2="V"');
      QrForneceI.SQL.Add('AND Codigo in (');
      QrForneceI.SQL.Add('  SELECT ci2.Propriet');
      QrForneceI.SQL.Add('  FROM condimov ci2');
      QrForneceI.SQL.Add('  LEFT JOIN cond co2 ON co2.Codigo=ci2.Codigo');
      QrForneceI.SQL.Add('  WHERE co2.Cliente IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add(') ORDER BY NOMEENTIDADE');
}
      QrForneceI.SQL.Add('SELECT DISTINCT ent.Codigo,');
      QrForneceI.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
      QrForneceI.SQL.Add('ELSE ent.Nome END NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM condimov ci2');
      QrForneceI.SQL.Add('LEFT JOIN cond con ON con.Codigo = ci2.Codigo');
      QrForneceI.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ci2.Propriet');
      //QrForneceI.SQL.Add('WHERE ci2.Codigo IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add('WHERE con.Cliente IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add('ORDER BY NOMEENTIDADE');
      QrForneceI.SQL.Add('');
      QrForneceI.Open;
    end;
    3: // Cursos c�clicos
    begin
      //Nada ?
    end;
  end;
end;

procedure TFmLctEditC.SpeedButton1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
  begin
    FmEntidades.LocCod(QrClientesCodigo.Value, QrClientesCodigo.Value);
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
  end;
  QrClientes.Close;
  QrClientes.Open;
  if dmkEdCBCliente.Enabled then
  begin
    dmkEdCBCliente.Text := IntToStr(VAR_ENTIDADE);
    dmkCBCliente.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmLctEditC.SpeedButton2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
  begin
    FmEntidades.LocCod(QrFornecedoresCodigo.Value, QrFornecedoresCodigo.Value);
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
  end;
  QrFornecedores.Close;
  QrFornecedores.Open;
  if dmkEdCBFornece.Enabled then
  begin
    dmkEdCBFornece.Text := IntToStr(VAR_ENTIDADE);
    dmkCBFornece.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmLctEditC.ArmazenaChequeAtual(Carteira, Cheque: Integer);
var
  I, N, K: Integer;
begin
  N := -1;
  K := High(FmLctFisico.FArrCartCod);
  for I := 0 to K do
  begin
    if FmLctFisico.FArrCartCod[I] = Carteira then
    begin
      N := I;
      FmLctFisico.FArrCartDoc[I] := Cheque;
      Break;
    end;
  end;
  if N = -1 then
  begin
    if K = -1 then
      K := 0;
    SetLength(FmLctFisico.FArrCartCod, K + 1);
    SetLength(FmLctFisico.FArrCartDoc, K + 1);
    //
    FmLctFisico.FArrCartCod[K] := Carteira;
    FmLctFisico.FArrCartDoc[K] := Cheque;
  end;
end;

procedure TFmLctEditC.BitBtn1Click(Sender: TObject);
begin
{
  MyPrinters.EmiteCheque(0, 0, QrCarteirasBanco1.Value,
    dmkEdDeb.ValueVariant, dmkCBFornece.Text,
    dmkEdCred.ValueVariant, dmkCBCliente.Text,
    ''(*Serie*), 0(*Cheque*), 0(*Data*), 0(*Carteira*),
    ''(*CampoNomeFornecedor*), nil (*Grade*), FTabLctA);
}
end;

procedure TFmLctEditC.EventosCadReopen(Entidade: Integer);
begin
  QrEventosCad.Close;
  QrEventosCad.Params[0].AsInteger := Entidade;
  QrEventosCad.Open;
  //
  LaEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  EdEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  CBEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  SbEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  if not QrEventosCad.Locate('CodUsu', QrEventosCadCodUsu.Value, []) then
  begin
    EdEventosCad.ValueVariant := 0;
    CBEventosCad.KeyValue     := 0;
  end;
end;

procedure TFmLctEditC.CkParcelamentoClick(Sender: TObject);
begin
  if CkParcelamento.Checked then
  begin
    GBParcelamento.Visible := True;
    DBGParcelas.Visible := True;
    CalculaParcelas();
  end else begin
    GBParcelamento.Visible := False;
    DBGParcelas.Visible := True;
  end;
end;

procedure TFmLctEditC.CkVctoFimClick(Sender: TObject);
begin
  if CkDataFim.Checked = True then CkDataIni.Checked := True;
end;

procedure TFmLctEditC.RGPeriodoClick(Sender: TObject);
begin
  if RGPeriodo.ItemIndex = 1 then
  begin
    EdDias.Enabled := True;
    EdDias.SetFocus;
  end else begin
    EdDias.Enabled := False;
  end;
  CalculaParcelas();
end;

procedure TFmLctEditC.EdCredMinExit(Sender: TObject);
begin
  if Geral.DMV(EdCredMax.Text) < Geral.DMV(EdCredMin.Text) then
    EdCredMax.Text := EdCredMin.Text;
end;

procedure TFmLctEditC.EdDebMinExit(Sender: TObject);
begin
  if Geral.DMV(EdDebMax.Text) < Geral.DMV(EdDebMin.Text) then
   EdDebMax.Text := EdDebMin.Text;
end;

procedure TFmLctEditC.EdDiasExit(Sender: TObject);
begin
  CalculaParcelas();
end;

procedure TFmLctEditC.RGIncremCHClick(Sender: TObject);
begin
  CalculaParcelas();
end;

procedure TFmLctEditC.CkArredondarClick(Sender: TObject);
begin
  CalculaParcelas();
end;

procedure TFmLctEditC.CkDataFimClick(Sender: TObject);
begin
  if CkDataFim.Checked = True then CkDataIni.Checked := True;
end;

procedure TFmLctEditC.RGArredondarClick(Sender: TObject);
begin
  CalculaParcelas();
end;

procedure TFmLctEditC.DBGParcelasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then if TbParcPagtos.State in ([dsEdit, dsInsert])
  then TbParcpagtos.Post;
end;

procedure TFmLctEditC.DBGrid2DblClick(Sender: TObject);
begin
  if (QrPsqCta.State <> dsInactive) and (QrPsqCta.RecordCount > 0) then
  begin
    EdGenero.ValueVariant := QrPsqCtaCodigo.Value;
    CBGenero.KeyValue := QrPsqCtaCodigo.Value;
    //
    PulaParaItensDoMul();
  end;
end;

procedure TFmLctEditC.DBGrid3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key=VK_DELETE) and (Shift=[ssCtrl]) then
  begin
    if Geral.MensagemBox('Confirma a exclus�o da pr�-inclus�o seleciionada?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
      'DELETE FROM ' + FMulInsLct,
      'WHERE OrdIns=' + Geral.FF0(QrMulInsLctOrdIns.Value),
      '']);
      //
      QrMulInsLct.Next;
      ReopenMulInsLct(QrMulInsLctOrdIns.Value);
    end;
  end;
end;

function TFmLctEditC.GetTipoValor(): TTipoValor;
{
var
  D,C: Double;
}
begin
{
  if dmkEdDeb.Enabled  then D := Geral.DMV(dmkEdDeb.Text)  else D := 0;
  if dmkEdCred.Enabled then C := Geral.DMV(dmkEdCred.Text) else C := 0;
  FValorAParcelar := D + C;
  if (D>0) and (C>0) then
  begin
    Geral.MensagemBox('Valor n�o pode ser cr�dito e d�bito ao mesmo tempo!',
    'Erro', MB_OK+MB_ICONERROR);
    Result := tvNil;
    Exit;
  end else if D>0 then Result := tvDeb
  else if C>0 then Result := tvCred
  else Result := tvNil;
}
  FValorAParcelar := QrMulInsLctValor.Value;
  if QrMulInsLctValor.Value < 0 then Result := tvDeb
  else
  if QrMulInsLctValor.Value > 0 then Result := tvCred
  else Result := tvNil;
  if FValorAParcelar < 0 then
    FValorAParcelar := - QrMulInsLctValor.Value;
end;

procedure TFmLctEditC.CkIncremDUClick(Sender: TObject);
begin
  RGIncremDupl.Visible := CkIncremDU.Checked;
  CalculaParcelas();
end;

procedure TFmLctEditC.EdDuplSepExit(Sender: TObject);
begin
  CalculaParcelas();
end;

procedure TFmLctEditC.EdPsqCtaChange(Sender: TObject);
var
  CtaCfgCab, Entidade: Integer;
  SQL1: String;
begin
  QrPsqCta.Close;
  if Length(EdPsqCta.Text) > 2 then
  begin
    Entidade := dmkEdCBCliInt.ValueVariant;
    if QrCliInt.State <> dsInactive then
    begin
      QrCliInt.Locate('Codigo', Entidade, []);
      //
      CtaCfgCab := QrCliIntCtaCfgCab.Value;
      if CtaCfgCab <> 0 then
      begin
        SQL1 :=
        'AND co.Codigo IN ( ' + sLineBreak +
        '     SELECT Genero ' + sLineBreak +
        '     FROM ctacfgits ' + sLineBreak +
        '     WHERE Codigo=' + Geral.FF0(CtaCfgCab) + ') ';
      end else
        SQL1 := '';
      UnDmkDAC_PF.AbreMySQLQuery0(QrPsqCta, Dmod.MyDB, [
      'SELECT co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO, ',
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO, ',
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA ',
      'FROM contas co ',
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo ',
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo ',
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto ',
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano ',
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa ',
      'WHERE co.Terceiro=0 ',
      'AND co.Codigo>0 ',
      'AND co.Nome LIKE "%' + EdPsqCta.Text + '%" ',
      SQL1,
      'ORDER BY co.Nome ',
      '']);
    end;
  end;
end;

procedure TFmLctEditC.EdPsqCtaEnter(Sender: TObject);
begin
  if FPsqCta = EdPsqCta.Text then
  begin
    EdPsqCta.Text := '';
    EdPsqCta.Font.Color := EdDescricao.Font.Color;
  end;
end;

procedure TFmLctEditC.RGDuplSeqClick(Sender: TObject);
begin
  CalculaParcelas();
end;

procedure TFmLctEditC.BitBtn2Click(Sender: TObject);
{
var
  ValOrig, ValMult, ValJuro, ValNovo, PerMult, ValDife: Double;
}
begin
{
  ValOrig := Geral.DMV(EdCredito.Text) - Geral.DMV(EdDebito.Text);
  if ValOrig < 0 then ValOrig := ValOrig * -1;
  ValNovo := Geral.DMV(dmkEdValNovo.Text);
  PerMult := Geral.DMV(dmkEdPerMult.Text);
  //
  ValDife := ValOrig - ValNovo;
  ValMult := Int(ValOrig * PerMult ) / 100;
  if ValMult > ValDife then ValMult := ValDife;
  ValJuro := ValDife - ValMult;
  //
  dmkEdMultaVal.Text := Geral.FFT(ValMult, 2, siNegativo);
  dmkEdMoraVal.Text  := Geral.FFT(ValJuro, 2, siNegativo);
}
end;

procedure TFmLctEditC.BitBtn3Click(Sender: TObject);
  function CredMenosDeb(): Double;
  begin
    Result := 0;
    if EdCredito.Enabled then
      Result := Result + EdCredito.ValueVariant;
    if EdDebito.Enabled then
      Result := Result - EdDebito.ValueVariant;
  end;
var
  GeneroNom, Descricao, Mes: String;
  GeneroCod, OrdIns: Integer;
  Valor: Double;
begin
  FOrdIns := FOrdIns + 1;
  //
  GeneroCod      := EdGenero.ValueVariant;
  GeneroNom      := CBGenero.Text;
  Valor          := CredMenosDeb();
  Descricao      := EdDescricao.Text;
  OrdIns         := FOrdIns;
  if EdMesEnabled then
    Mes          := EdMes.Text
  else
    Mes          := '';
  //
  if MyObjects.FIC(GeneroCod = 0, EdGenero,
    'Informe a conta do plano de contas') then
    Exit;
  if MyObjects.FIC((Valor = 0) and (CkCancelado.Checked = False), nil,
    'Informe o valor ou marque o cancelamento do lan�amento!') then
    Exit;
  try
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FMulInsLct, False, [
    'GeneroNom', 'Valor', 'Descricao', 'Mes', 'OrdIns'], ['GeneroCod'], [
    GeneroNom, Valor, Descricao, Mes, OrdIns], [GeneroCod], False);
    //
    ReopenMulInsLct(OrdIns);
    //
    EdGenero.SetFocus;
  except
    Geral.MensagemBox('N�o foi poss�vel fazer a pr�-inclus�o.' + sLineBreak +
    'Verifique se a conta selecionada j� foi pr�-inclu�da!' + sLineBreak +
    'A mesma conta n�o pode ser pr�-inclu�da duas vezes!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmLctEditC.CkIncremTxtClick(Sender: TObject);
begin
  GBIncremTxt.Visible := CkIncremTxt.Checked;
  CalculaParcelas();
end;

procedure TFmLctEditC.QrDeptosAfterScroll(DataSet: TDataSet);
begin
  case FIDFinalidade of
    1: // Pr�prio
    begin
      QrForneceI.Close;
      QrForneceI.SQL.Clear;
      QrForneceI.SQL.Add('SELECT ent.Codigo,');
      QrForneceI.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM intentosoc soc');
      QrForneceI.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=soc.Socio');
      QrForneceI.SQL.Add('WHERE soc.Codigo=:P0');
      QrForneceI.SQL.Add('ORDER BY NOMEENTIDADE');
      QrForneceI.SQL.Add('');
      QrForneceI.Params[0].AsInteger := QrDeptosCODI_1.Value;
      QrForneceI.Open;
      if not QrForneceI.Locate('Codigo', Geral.IMV(dmkEdCBForneceI.Text), []) then
      begin
        dmkEdCBForneceI.Text := '';
        dmkCBForneceI.KeyValue := Null;
      end;
    end;
    2: // Condom�nios
    begin
      //Nada
    end;
    3: // Cursos c�clicos
    begin
      //Nada
    end;
  end;
end;

procedure TFmLctEditC.QrMulInsLctAfterOpen(DataSet: TDataSet);
begin
  ReopenSoma();
end;

procedure TFmLctEditC.QrPsqCtaAfterOpen(DataSet: TDataSet);
begin
  LaCtaPsq.Caption := Geral.FFN(QrPsqCta.RecordCount, 3);
end;

procedure TFmLctEditC.QrPsqCtaBeforeClose(DataSet: TDataSet);
begin
  LaCtaPsq.Caption := '000';
end;

procedure TFmLctEditC.EdGeneroChange(Sender: TObject);
begin
  VerificaEdits;
  if (ImgTipo.SQLType = stIns) and (CkDuplicando.Checked = False) then
    if EdGenero.ValueVariant <> 0 then
      EdDescricao.Text := QrContasNome.Value;
end;

procedure TFmLctEditC.dmkDBLookupComboBox1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLctEditC.EdMesExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (CkDuplicando.Checked = False)  then
    EdDescricao.Text := QrContasNome2.Value + ' ' + EdMes.Text;
end;

procedure TFmLctEditC.EdMesKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Genero, Cliente, Fornece: Integer;
  GenTxt, EntTxt, Qual: String;
begin
  if Key = VK_F4 then
  begin
    if ImgTipo.SQLType = stIns then
    begin
      Genero  := EdGenero.ValueVariant;
      if dmkEdCBCliente.Enabled then
        Cliente := dmkEdCBCliente.ValueVariant
      else Cliente := 0;
      if dmkEdCBFornece.Enabled then
        Fornece := dmkEdCBFornece.ValueVariant
      else Fornece := 0;  
      //
      GenTxt := FormatFloat('0', Genero);
      EntTxt := 'qualquer cliente ou fornecedor';
      Qual   := 'De ';
      if MyObjects.FIC(Genero = 0, nil (*EdGenero*),
        'Informe a conta do plano de contas!') then Exit;
      if MyObjects.FIC((Cliente = 0) and (Fornece = 0), nil (*dmkEdCBCliente*),
        'Informe um cliente ou um fornecedor!') then Exit;
       //
       QrLocMes.Close;
       QrLocMes.SQL.Clear;
       QrLocMes.SQL.Add('SELECT MAX(Mez) MEZ');
       QrLocMes.SQL.Add('FROM ' + FTabLctA);
       QrLocMes.SQL.Add('WHERE Mez > 0');
       QrLocMes.SQL.Add('AND Genero=' + GenTxt);
       if Cliente <> 0 then
       begin
         Qual := 'Cliente: ';
         EntTxt := FormatFloat('0', Cliente);
         QrLocMes.SQL.Add('AND Cliente=' + EntTxt);
       end else if Fornece <> 0 then
       begin
         Qual := 'Fornecedor: ';
         EntTxt := FormatFloat('0', Fornece);
         QrLocMes.SQL.Add('AND Fornecedor=' + EntTxt);
       end;
       QrLocMes.Open;
       if QrLocMes.RecordCount > 0 then
       begin
         EdMes.Texto := MLAGeral.MezToFDT(QrLocMesMEZ.Value, 32, 14);
       end else Geral.MensagemBox('N�o foi localizado nenhum m�s para:' + sLineBreak+
       'Conta do plano de contas: ' + sLineBreak + Qual + EntTxt,
       'Aviso', MB_OK+MB_ICONWARNING);
    end else Geral.MensagemBox(
    'A sugest�o do pr�ximo m�s de compet�ncia � v�lida' + sLineBreak +
    'apenas quando o status da janela for de inclus�o!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmLctEditC.EdDebitoExit(Sender: TObject);
begin
  CalculaICMS;
  if CkParcelamento.Checked then
    CalculaParcelas();
end;

procedure TFmLctEditC.CBGeneroClick(Sender: TObject);
begin
  VerificaEdits;
end;

procedure TFmLctEditC.CBGeneroExit(Sender: TObject);
begin
  if EdGenero.ValueVariant <> 0 then
    PulaParaItensDoMul();
end;

procedure TFmLctEditC.EdCreditoExit(Sender: TObject);
begin
  CalculaICMS;
  if CkParcelamento.Checked then
    CalculaParcelas();
end;

procedure TFmLctEditC.dmkEdDocChange(Sender: TObject);
begin
  if Geral.DMV(dmkEdDoc.Text) = 0 then
  begin
    RGIncremCH.Enabled := False;
    //GBCheque.Visible := False;
  end else begin
    RGIncremCH.Enabled := True;
    //GBCheque.Visible := True;
  end;
end;

procedure TFmLctEditC.dmkEdDocExit(Sender: TObject);
begin
  if CkParcelamento.Checked then
    CalculaParcelas();
end;

procedure TFmLctEditC.dmkEdDocKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    dmkEdDoc.ValueVariant := BuscaChequeArmazenado(dmkEdCarteira.ValueVariant);
end;

procedure TFmLctEditC.dmkEdDuplicataExit(Sender: TObject);
begin
  if CkParcelamento.Checked then
    CalculaParcelas();
end;

procedure TFmLctEditC.dmkEdTPDataClick(Sender: TObject);
begin
  dmkEdTPVencto.Date := dmkEdTPData.Date;
end;

procedure TFmLctEditC.dmkEdTPVenctoChange(Sender: TObject);
begin
  if CkParcelamento.Checked then
    CalculaParcelas();
end;

procedure TFmLctEditC.dmkEdTPVenctoClick(Sender: TObject);
begin
  if CkParcelamento.Checked then
    CalculaParcelas();
end;

procedure TFmLctEditC.dmkEdTPVenctoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F6 then
    dmkEdTPVencto.Date := dmkEdTPdata.Date;
end;

procedure TFmLctEditC.ConfiguracoesIniciais();
begin
  QrCliInt.Close;
  QrCliInt.Open;
  // 2012-05-15
  // Precisa do cliInt aberto para abrir as contas restritivas corretamente
  ReopenContas(dmkEdCBCliInt.ValueVariant, 0);
  // FIM 2012-05-15
  //
  MeConfig.Lines.Clear;
  QrDeptos.Close;
  QrDeptos.SQL.Clear;
  FIDFinalidade := Geral.IMV(Copy(LaFinalidade.Caption, 1, 3));
  case FIDFinalidade of
    1: // Pr�prios
    begin
      LaCliInt.Caption := 'Empresa (Filial):';
      if Uppercase(Application.Title) = 'SYNKER' then
      begin
        //
        QrDeptos.SQL.Add('SELECT Codigo CODI_1, Nome NOME_1, FLOOR(0) CODI_2');
        QrDeptos.SQL.Add('FROM intentocad');
        QrDeptos.SQL.Add('WHERE Ativo=1');
        QrDeptos.SQL.Add('ORDER BY NOME_1');
        //
        {
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Add('SELECT Codigo, ');
        QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
        QrCliInt.SQL.Add('FROM entidades');
        QrCliInt.SQL.Add('WHERE Codigo=' + IntToStr(Dmod.QrControleDono.Value));
        QrCliInt.Open;
        }
        //////
        //PnDepto.Visible   := True;
        //PnForneceI.Visible := True;
      end;
    end;
    2: // Condom�nios
    begin
      LaCliInt.Caption := 'Condom�nio (Cliente interno):';
      //
      {
      QrDeptos.SQL.Add('SELECT Conta CODI_1, Unidade NOME_1, FLOOR(Propriet) CODI_2');
      QrDeptos.SQL.Add('FROM condimov');
      QrDeptos.SQL.Add('WHERE Codigo=:P0');
      QrDeptos.SQL.Add('ORDER BY NOME_1');
      }
      QrDeptos.SQL.Add('SELECT imv.Conta CODI_1, imv.Unidade NOME_1,');
      QrDeptos.SQL.Add('FLOOR(imv.Propriet) CODI_2');
      QrDeptos.SQL.Add('FROM condimov imv');
      QrDeptos.SQL.Add('LEFT JOIN cond con ON con.Codigo = imv.Codigo');
      QrDeptos.SQL.Add('WHERE con.Cliente=:P0');
      QrDeptos.SQL.Add('ORDER BY NOME_1');
      QrDeptos.Params[0].AsInteger := dmkEdOneCliInt.ValueVariant;//FmCondGer.QrCondCodigo.Value;
      //
      if Trim(VAR_CLIENTEI) <> '' then
      begin
        {
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Add('SELECT Codigo, ');
        QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
        QrCliInt.SQL.Add('FROM entidades');
        QrCliInt.SQL.Add('WHERE Cliente'+VAR_CLIENTEI+'="V"');
        QrCliInt.SQL.Add('ORDER BY NOMEENTIDADE');
        QrCliInt.Open;
        }
        //////
        LaCliInt.Visible := True;
        dmkEdCBCliInt.Visible := True;
        dmkCBCliInt.Visible := True;
      end;
      PnDepto.Visible   := True;
      PnForneceI.Visible := True;
    end;
    3: // Cursos c�clicos
    begin
      {
      QrCliInt.SQL.Clear;
      QrCliInt.SQL.Add('SELECT Codigo, ');
      QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
      QrCliInt.SQL.Add('FROM entidades');
      QrCliInt.SQL.Add('WHERE CliInt>0 OR Codigo=-1');
      QrCliInt.SQL.Add('ORDER BY NOMEENTIDADE');
      QrCliInt.Open;
      }
      LaCliInt.Caption        := 'Empresa (Filial):';
      LaDepto.Caption         := '';
      LaAccount.Caption       := 'Respons�vel interno pelo movimento:';
      dmkEdCBForneceI.Visible := False;
      dmkCBForneceI.Visible   := False;
      //
      QrAccounts.SQL.Clear;
      QrAccounts.SQL.Add('SELECT Codigo, ');
      QrAccounts.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
      QrAccounts.SQL.Add('FROM entidades');
      QrAccounts.SQL.Add('WHERE Fornece2="V" OR Fornece3="V"');
      QrAccounts.Open;
      //
      PnAccount.Visible           := True;
      if ImgTipo.SQLType = stIns then
      begin
        dmkEdCBAccount.ValueVariant := dmkEdOneAccount.ValueVariant;
        dmkCBAccount.KeyValue       := dmkEdOneAccount.ValueVariant;
      end;
    end;
    else Geral.MensagemBox('Nenhuma finalidade foi definida para a ' +
    'janela de inclus�o / altera��o de lan�amentos!', 'Aviso',
    MB_OK+MB_ICONWARNING);
  end;
  if QrDeptos.SQL.Text <> '' then QrDeptos.Open;
end;

procedure TFmLctEditC.dmkCBForneceKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F5 then ReopenFornecedores(0) else
  if Key = VK_F6 then ReopenFornecedores(1);
end;

procedure TFmLctEditC.dmkEdCBDeptoChange(Sender: TObject);
begin
  if not FCriandoForm then
  begin
    if FIDFinalidade = 2 then
    begin
      if dmkEdCBDepto.ValueVariant = 0 then
      begin
        dmkEdCBForneceI.Text := '0';
        dmkCBForneceI.KeyValue := Null;
      end else begin
        dmkEdCBForneceI.Text := FormatFloat('0', QrDeptosCODI_2.Value);
        dmkCBForneceI.KeyValue := Int(QrDeptosCODI_2.Value / 1);
      end;
    end;
  end;
end;

procedure TFmLctEditC.dmkCBDeptoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_DELETE then
  begin
    dmkEdCBDepto.Text := '';
    dmkCBDepto.KeyValue := Null;
  end;
end;

procedure TFmLctEditC.EdDescricaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if EdMesEnabled then Mes := EdMes.Text else Mes := '';
    if key=VK_F4 then EdDescricao.Text := QrContasNome.Value+' '+Mes;
    if key=VK_F5 then EdDescricao.Text := QrContasNome2.Value+' '+Mes;
    if key=VK_F6 then EdDescricao.Text := QrContasNome3.Value+' '+Mes;
    if EdDescricao.Enabled then EdDescricao.SetFocus;
    EdDescricao.SelStart := Length(EdDescricao.Text);
    EdDescricao.SelLength := 0;
  end else if key = VK_F8 then
  begin
    if dmkCBFornece.Enabled then
      EdDescricao.Text := EdDescricao.Text + ' - ' + dmkCBFornece.Text
    else if dmkCBCliente.Enabled then
      EdDescricao.Text := EdDescricao.Text + ' - ' + dmkCBCliente.Text;
  end;
end;

procedure TFmLctEditC.dmkCBAccountKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLctEditC.dmkEdICMS_PExit(Sender: TObject);
begin
  CalculaICMS;
end;

procedure TFmLctEditC.dmkEdICMS_PKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if MLAGeral.EhNumeroOuSinalOuSeparador(Key) then FICMS := 0;
end;

procedure TFmLctEditC.dmkEdICMS_VExit(Sender: TObject);
begin
  CalculaICMS;
end;

procedure TFmLctEditC.dmkEdICMS_VKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if MLAGeral.EhNumeroOuSinalOuSeparador(Key) then FICMS := 1;
end;

procedure TFmLctEditC.dmkEdCarteiraChange(Sender: TObject);
{
var
  UsaTalao: Boolean;
}
begin
{
  UsaTalao := QrCarteirasUsaTalao.Value = 0;
  dmkEdSerieCH.Enabled := UsaTalao;
  dmkEdDoc.Enabled     := UsaTalao;
  LaDoc.Enabled        := UsaTalao;
}
end;

procedure TFmLctEditC.dmkEdCBAccountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLctEditC.dmkEdCBCliIntChange(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := dmkEdCBCliInt.ValueVariant;
  EventosCadReopen(Entidade);
  ReopenContas(Entidade, 0);
end;

procedure TFmLctEditC.dmkEdParcelasExit(Sender: TObject);
begin
  CalculaParcelas();
end;

procedure TFmLctEditC.dmkEdSerieCHExit(Sender: TObject);
begin
  if CkParcelamento.Checked then
    CalculaParcelas();
end;

procedure TFmLctEditC.dmkEdSerieCHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    dmkEdDoc.ValueVariant := BuscaChequeArmazenado(dmkEdCarteira.ValueVariant);
end;

procedure TFmLctEditC.IncrementaExecucoes();
begin
  dmkEdExecs.ValueVariant := dmkEdExecs.ValueVariant + 1;
end;

end.



