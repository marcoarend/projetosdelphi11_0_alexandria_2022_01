unit LctPgVarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, DBCtrls, Grids,
  DBGrids, ComCtrls, UnFinanceiro, Variants, dmkEdit, dmkEditDateTimePicker,
  Mask, dmkGeral, dmkEditCB, dmkDBLookupComboBox, dmkImage, UnDmkEnums,
  DmkDAC_PF;

type
  THackDBGrid = class(TDBGrid);
  TFmLctPgVarios = class(TForm)
    PainelDados: TPanel;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasTipo: TIntegerField;
    Label3: TLabel;
    Label2: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdMulta: TdmkEdit;
    EdTaxaM: TdmkEdit;
    Label4: TLabel;
    DBGrid1: TDBGrid;
    TbLctoEdit: TmySQLTable;
    DsLctoEdit: TDataSource;
    TbLctoEditControle: TIntegerField;
    TbLctoEditSub: TIntegerField;
    TbLctoEditDescricao: TWideStringField;
    TbLctoEditData: TDateField;
    TbLctoEditVencimento: TDateField;
    TbLctoEditMultaVal: TFloatField;
    TbLctoEditJurosVal: TFloatField;
    TbLctoEditValorOri: TFloatField;
    TbLctoEditValorPgt: TFloatField;
    QrLancto1: TmySQLQuery;
    QrLancto1Sub: TSmallintField;
    QrLancto1Descricao: TWideStringField;
    QrLancto1Credito: TFloatField;
    QrLancto1Debito: TFloatField;
    QrLancto1NotaFiscal: TIntegerField;
    QrLancto1Documento: TFloatField;
    QrLancto1Vencimento: TDateField;
    QrLancto1Carteira: TIntegerField;
    QrLancto1Data: TDateField;
    QrLancto1NOMECARTEIRA: TWideStringField;
    QrLancto1TIPOCARTEIRA: TIntegerField;
    QrLancto1CARTEIRABANCO: TIntegerField;
    QrLancto1Cliente: TIntegerField;
    QrLancto1Fornecedor: TIntegerField;
    QrLancto1DataDoc: TDateField;
    QrLancto1Nivel: TIntegerField;
    QrLancto1Vendedor: TIntegerField;
    QrLancto1Account: TIntegerField;
    QrLancto1Controle: TIntegerField;
    QrLancto1CtrlIni: TIntegerField;
    QrLancto1Genero: TIntegerField;
    QrLancto1Mez: TIntegerField;
    QrLancto1Duplicata: TWideStringField;
    QrLancto1Doc2: TWideStringField;
    QrLancto1SerieCH: TWideStringField;
    QrLancto1MoraDia: TFloatField;
    QrLancto1Multa: TFloatField;
    QrLancto1ICMS_P: TFloatField;
    QrLancto1ICMS_V: TFloatField;
    QrLancto1CliInt: TIntegerField;
    QrLancto1Depto: TIntegerField;
    QrLancto1DescoPor: TIntegerField;
    QrLancto1ForneceI: TIntegerField;
    QrLancto1Unidade: TIntegerField;
    QrLancto1Qtde: TFloatField;
    QrLancto1FatID: TIntegerField;
    QrLancto1FatID_Sub: TIntegerField;
    QrLancto1DescoVal: TFloatField;
    QrLancto1NFVal: TFloatField;
    QrLancto1FatParcela: TIntegerField;
    QrLancto2: TmySQLQuery;
    QrLancto2Tipo: TSmallintField;
    QrLancto2Credito: TFloatField;
    QrLancto2Debito: TFloatField;
    QrLancto2Sub: TSmallintField;
    QrLancto2Carteira: TIntegerField;
    QrLancto2Controle: TIntegerField;
    QrLancto1FatNum: TFloatField;
    EdTotalPago: TdmkEdit;
    Label1: TLabel;
    QrSum: TmySQLQuery;
    DsSum: TDataSource;
    QrSumValorPgt: TFloatField;
    BitBtn1: TBitBtn;
    QrSumValorOri: TFloatField;
    QrTot: TmySQLQuery;
    DsTot: TDataSource;
    QrTotValorOri: TFloatField;
    QrTotValorPgt: TFloatField;
    QrTotMultaVal: TFloatField;
    QrTotJurosVal: TFloatField;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    QrSumMultaVal: TFloatField;
    QrSumJurosVal: TFloatField;
    Label7: TLabel;
    DBEdit3: TDBEdit;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    QrContas1: TmySQLQuery;
    QrContas1Codigo: TIntegerField;
    QrContas1Nome: TWideStringField;
    DsContas1: TDataSource;
    DsContas2: TDataSource;
    QrContas2: TmySQLQuery;
    QrContas2Codigo: TIntegerField;
    QrContas2Nome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtCancela: TBitBtn;
    BtOK: TBitBtn;
    PB1: TProgressBar;
    GBCarteira: TGroupBox;
    Label9: TLabel;
    CBCarteira: TdmkDBLookupComboBox;
    EdCarteira: TdmkEditCB;
    QrLoc: TmySQLQuery;
    TbLctoEditCarteira: TIntegerField;
    QrLancto1CentroCusto: TIntegerField;
    QrLancto1Qtd2: TFloatField;
    QrLancto1VctoOriginal: TDateField;
    QrLancto1ModeloNF: TWideStringField;
    QrLancto1GenCtb: TIntegerField;
    QrLancto1GenCtbD: TIntegerField;
    QrLancto1GenCtbC: TIntegerField;
    procedure BtOKClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdMultaExit(Sender: TObject);
    procedure EdTaxaMExit(Sender: TObject);
    procedure TPDataExit(Sender: TObject);
    procedure TbLctoEditBeforePost(DataSet: TDataSet);
    procedure TbLctoEditAfterPost(DataSet: TDataSet);
    procedure QrSumAfterOpen(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaMultaEJuros;
    procedure ConfiguraGBCarteira;
  public
    { Public declarations }
    FTabLctA: String;
    FQrCrt, FQrLct: TmySQLQuery;
    FCalcular: Boolean;

  end;

  var
  FmLctPgVarios: TFmLctPgVarios;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, ModuleGeral,
UnDmkProcFunc, ModuleFin;

procedure TFmLctPgVarios.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctPgVarios.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FCalcular := True;
  CalculaMultaEJuros;
  if UFinanceiro.TabLctNaoDefinida(FTabLctA, 'TFmLctPgVarios.FormActivate()') then
    Close;
end;

procedure TFmLctPgVarios.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctPgVarios.FormShow(Sender: TObject);
begin
  ConfiguraGBCarteira();
end;

procedure TFmLctPgVarios.QrSumAfterOpen(DataSet: TDataSet);
begin
  EdTotalPago.ValueVariant := QrSumValorPgt.Value;
end;

procedure TFmLctPgVarios.BitBtn1Click(Sender: TObject);
var
  Dias, Multa, Juros, Fator, Total, Need_, Dif: Double;
begin
  Screen.Cursor := crHourGlass;
  Multa := EdMulta.ValueVariant / 100;
  Need_ := EdTotalPago.ValueVariant;
  TbLctoEdit.First;
  while not TbLctoEdit.Eof do
  begin
    TbLctoEdit.Edit;
    TbLctoEditMultaVal.Value := TbLctoEditValorOri.Value * Multa;
    TbLctoEdit.Post;
    //
    TbLctoEdit.Next;
  end;
  QrTot.Close;
  UnDmkDAC_PF.AbreQuery(QrTot, DModG.MyPID_DB);
  Juros := EdTotalPago.ValueVariant - QrTotValorOri.Value - QrTotMultaVal.Value;
  Fator := Juros / QrTotValorOri.Value;
  //
  Dias := Trunc(TPData.Date) - TbLctoEditVencimento.Value;
  //
  if Dias > 0 then
    EdTaxaM.ValueVariant := Fator * 100 / Dias * 30
  else
    EdTaxaM.ValueVariant := 0;
  //
  TbLctoEdit.First;
  while not TbLctoEdit.Eof do
  begin
    Juros := TbLctoEditValorOri.Value * Fator;
    Total := TbLctoEditMultaVal.Value + TbLctoEditValorOri.Value + Juros;
    TbLctoEdit.Edit;
    TbLctoEditJurosVal.Value := Juros;
    TbLctoEditValorPgt.Value := Total;
    TbLctoEdit.Post;
    //
    TbLctoEdit.Next;
  end;
  //
  QrSum.Close;
  UnDmkDAC_PF.AbreQuery(QrSum, DModG.MyPID_DB);
  //
  Dif := Need_ - QrSumValorPgt.Value;
  if Dif <> 0 then
  begin
    TbLctoEdit.Edit;
    TbLctoEditJurosVal.Value := TbLctoEditJurosVal.Value + Dif;
    TbLctoEditValorPgt.Value := TbLctoEditValorPgt.Value + Dif;
    TbLctoEdit.Post;
    //
    QrSum.Close;
    UnDmkDAC_PF.AbreQuery(QrSum, DModG.MyPID_DB);
    //
  end;
  EdMulta.Enabled := False;
  EdTaxaM.Enabled := False;
  Screen.Cursor := crDefault;
end;

procedure TFmLctPgVarios.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.ReopenControle;
  //
  TPData.Date := Date;
  FCalcular := False;
  //
  UFinanceiro.ReopenCarteirasPgto(QrCarteiras);
  //
  QrSum.Close;
  QrSum.DataBase := DModG.MyPID_DB;
  QrTot.Close;
  QrTot.DataBase := DModG.MyPID_DB;
  //
  TbLctoEdit.Close;
  TbLctoEdit.DataBase := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreTable(TbLctoEdit, DModG.MyPID_DB);
end;

procedure TFmLctPgVarios.EdMultaExit(Sender: TObject);
begin
  CalculaMultaEJuros;
end;

procedure TFmLctPgVarios.EdTaxaMExit(Sender: TObject);
begin
  CalculaMultaEJuros;
end;

procedure TFmLctPgVarios.TPDataExit(Sender: TObject);
begin
  CalculaMultaEJuros;
end;

procedure TFmLctPgVarios.CalculaMultaEJuros;
var
  Venct, Pagto: TDateTime;
  MultaV, MultaT, TaxaM, JurosV, JurosT: Double;
  Dias, Controle: Integer;
begin
  if FCalcular then
  begin
    MultaT := Geral.DMV(EdMulta.Text);
    TaxaM  := Geral.DMV(EdTaxaM.Text);
    Pagto  := Trunc(TPData.Date);
    Venct  := UMyMod.CalculaDataDeposito(TbLctoEditVencimento.Value);
    TbLctoEdit.DisableControls;
    Controle := TbLctoEditControle.Value;
    TbLctoEdit.First;
    while not TbLctoEdit.Eof do
    begin
      JurosV := 0;
      MultaV := 0;
      if Venct < Pagto then
        Dias := UMyMod.DiasUteis(Venct + 1, Pagto) else
        Dias := 0;
      if Dias > 0 then
      begin
        JurosT := dmkPF.CalculaJuroComposto(TaxaM, Pagto - Venct);
        JurosV := Round(JurosT * TbLctoEditValorOri.Value) / 100;
        MultaV := Round(MultaT * TbLctoEditValorOri.Value) / 100;
      end;
      //
      TbLctoEdit.Edit;
      TbLctoEditData.Value     := Pagto;
      TbLctoEditJurosVal.Value := JurosV;
      TbLctoEditMultaVal.Value := MultaV;
      TbLctoEditValorPgt.Value := TbLctoEditValorOri.Value + JurosV + MultaV;
      TbLctoEdit.Post;
      //
      TbLctoEdit.Next;
    end;
    TbLctoEdit.Locate('Controle', Controle, []);
  end;
  TbLctoEdit.EnableControls;
end;

procedure TFmLctPgVarios.ConfiguraGBCarteira;
var
  Carteira: Integer;
begin
  GBCarteira.Visible := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, DModG.MyPID_DB, [
    'SELECT Carteira ',
    'FROM lctoedit ',
    'GROUP BY Carteira ',
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    Carteira := QrLoc.FieldByName('Carteira').AsInteger;
    //
    UFinanceiro.ConfiguraGBCarteiraPgto(GBCarteira, EdCarteira, CBCarteira, Carteira);
  end;
end;

procedure TFmLctPgVarios.TbLctoEditBeforePost(DataSet: TDataSet);
  function GetNomeCampo: String;
  begin
    Result := DBGrid1.Columns[THackDBGrid(DBGrid1).Col-1].FieldName;
  end;
var
  MultaV, JurosV, TotalV, TotalI: Double;
  Campo: String;
begin
  MultaV := TbLctoEditMultaVal.Value;
  JurosV := TbLctoEditJurosVal.Value;
  TotalV := TbLctoEditValorPgt.Value;
  TotalI := TbLctoEditValorOri.Value;
  if TotalV <> MultaV + JurosV then
  begin
    Campo := GetNomeCampo;
    if Campo = 'ValorPgt' then
    begin
      if TotalV < TotalI then
      begin
        MultaV := 0;
        JurosV := 0;
      end else begin
        JurosV := TotalV - MultaV - TotalI;
        if JurosV < 0 then
        begin
          MultaV := MultaV + JurosV;
          JurosV := 0;
        end;
        if MultaV < 0 then
        begin
          TotalV := TotalV + MultaV;
          MultaV := 0;
        end;
      end;
      if TotalV < 0 then TotalV := 0;
      TbLctoEditJurosVal.Value := JurosV;
      TbLctoEditMultaVal.Value := MultaV;
      TbLctoEditValorPgt.Value := TotalV;
    end;
    if Campo = 'JurosVal' then
      TbLctoEditValorPgt.Value := TotalI + JurosV + MultaV;
    if Campo = 'MultaVal' then
      TbLctoEditValorPgt.Value := TotalI + JurosV + MultaV;
  end;
end;

procedure TFmLctPgVarios.TbLctoEditAfterPost(DataSet: TDataSet);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM lctoedit WHERE Controle < 1');
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.ExecSQL;
  TbLctoEdit.Refresh;
end;

procedure TFmLctPgVarios.BtOKClick(Sender: TObject);

  procedure QuitaRegistroAtual;
  var
    Texto: PChar;
    Doc, Controle, Controle2, Credito, Debito, MultaVal, MoraVal: Double;
    Carteira, Sub: Integer;
    Data, Compensado: String;
  begin
    QrLancto1.Close;
    QrLancto1.SQL.Clear;
    QrLancto1.SQL.Add('SELECT la.Controle, la.Sub, la.Descricao, la.Credito, la.Debito,');
    QrLancto1.SQL.Add('la.NotaFiscal, la.Documento, la.Vencimento, la.Carteira, Data,');
    QrLancto1.SQL.Add('ca.Nome NOMECARTEIRA, ca.Tipo TIPOCARTEIRA ,');
    QrLancto1.SQL.Add('ca.Banco CARTEIRABANCO, la.Cliente, la.Fornecedor,');
    QrLancto1.SQL.Add('la.DataDoc, la.Nivel, la.CtrlIni, la.Vendedor, la.Account,');
    QrLancto1.SQL.Add('la.Genero, la.Mez, la.Duplicata, la.Doc2, la.SerieCH, la.MoraDia,');
    QrLancto1.SQL.Add('la.Multa, la.ICMS_P, la.ICMS_V, la.CliInt, la.Depto, la.DescoPor,');
    QrLancto1.SQL.Add('la.ForneceI, la.Unidade, la.Qtde, la.Qtd2, la.FatID, la.FatID_Sub,');
    QrLancto1.SQL.Add('la.FatNum, la.DescoVal, la.NFVal, la.FatParcela,');
    QrLancto1.SQL.Add('la.GenCtb, la.GenCtbD, la.GenCtbC ');
    {### colocar B e D?}
    QrLancto1.SQL.Add('FROM ' + FTabLctA + ' la, Carteiras ca');
    QrLancto1.SQL.Add('WHERE la.Controle=:P0');
    QrLancto1.SQL.Add('AND la.Sub=:P1');
    QrLancto1.SQL.Add('AND ca.Codigo=la.Carteira');
    QrLancto1.Params[00].AsInteger := TbLctoEditControle.Value;
    QrLancto1.Params[01].AsInteger := TbLctoEditSub.Value;
    UnDmkDAC_PF.AbreQuery(QrLancto1, Dmod.MyDB);
    //
    QrLancto2.Close;
    QrLancto2.SQL.Clear;
    QrLancto2.SQL.Add('SELECT Tipo, Credito, Debito, Controle, Sub, Carteira');
    {### colocar B e D?}
    QrLancto2.SQL.Add('FROM ' + FTabLctA);
    QrLancto2.SQL.Add('WHERE ID_Pgto=:P0');
    QrLancto2.SQL.Add('AND ID_Sub=:P1');
    QrLancto2.Params[00].AsFloat   := TbLctoEditControle.Value;
    QrLancto2.Params[01].AsInteger := QrLancto1Sub.Value;
    UnDmkDAC_PF.AbreQuery(QrLancto2, Dmod.MyDB);
    //
    if QrLancto1Debito.Value <> 0 then
    begin
      Debito  := -TbLctoEditValorPgt.Value;
      Credito := 0;
    end else
    begin
      Debito  := 0;
      Credito := TbLctoEditValorPgt.Value;
    end;
    FLAN_Nivel := QrLancto1Nivel.Value + 1;
    //
    if QrLancto1CtrlIni.Value > 0 then
      FLAN_CtrlIni := Trunc(QrLancto1CtrlIni.Value)
    else
      FLAN_CtrlIni := Trunc(QrLancto1Controle.Value);
    //
    Controle := QrLancto1Controle.Value;
    Sub      := QrLancto1Sub.Value;
    Doc      := QrLancto1Documento.Value;
    //
    if QrLancto2.RecordCount <> 0 then
    begin
      if ((QrLancto2Credito.Value + QrLancto2Debito.Value) =
        (QrLancto1Credito.Value + QrLancto1Debito.Value))
      and (QrLancto2.RecordCount = 1) and (QrLancto2Tipo.Value = 1) then
      begin
        Texto := 'J� existe uma quita��o, e ela possui o mesmo valor,' +
                   ' deseja difini-la como quita��o desta emiss�o ?';
        //
        if Geral.MB_Pergunta(Texto) = ID_YES then
        begin
          {
          Dmod.QrUpdM.SQL.Clear;
          Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Sit=3, Compensado=:P0,');
          Dmod.QrUpdM.SQL.Add('DataAlt=:P1, UserAlt=:P2');
          Dmod.QrUpdM.SQL.Add('WHERE Controle=:P3 AND Tipo=2');
          Dmod.QrUpdM.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, TbLctoEditData.Value);
          Dmod.QrUpdM.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Date);
          Dmod.QrUpdM.Params[2].AsInteger := VAR_USUARIO;
          Dmod.QrUpdM.Params[3].AsFloat := Controle;
  //        Dmod.QrUpdM.Params[2].AsInteger := Sub; precisa??
          Dmod.QrUpdM.ExecSQL;
          }
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Sit', 'Compensado'], ['Controle', 'Tipo'], [
          3, FormatDateTime(VAR_FORMATDATE, TbLctoEditData.Value)], [
          Controle, 2], True, '', FTabLctA);
          //
          UFinanceiro.RecalcSaldoCarteira(QrLancto1Carteira.Value,
            nil, nil, False, True);

          {
          Dmod.QrUpdM.SQL.Clear;
          Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Data=:P0,');
          Dmod.QrUpdM.SQL.Add('DataAlt=:P1, UserAlt=:P2');
          Dmod.QrUpdM.SQL.Add('WHERE ID_Pgto=:P3 AND Tipo=1 AND ID_Pgto>0');
          Dmod.QrUpdM.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, TbLctoEditData.Value);
          Dmod.QrUpdM.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Date);
          Dmod.QrUpdM.Params[2].AsInteger := VAR_USUARIO;
          Dmod.QrUpdM.Params[3].AsFloat := Controle;
          Dmod.QrUpdM.ExecSQL;
          }
          if Controle > 0 then
          begin
            UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
            'Data'], ['ID_Pgto', 'Tipo'], [
            FormatDateTime(VAR_FORMATDATE, TbLctoEditData.Value)],
            [Controle, 1], True, '', FTabLctA);
          end;
          UFinanceiro.RecalcSaldoCarteira(QrLancto1Carteira.Value,
          nil, nil, False, True);
        end;
{ T O D O : Fazer relat�rio para aqui e TDModFin.QuitaDocumento }
      end else
      //ShowMessage('J� existe pagamentos/rolagem para esta emiss�o!');
      Geral.MB_Aviso('Quita��o cancelada! ' + sLineBreak +
        'J� existe pagamento(s) parcial(is)/rolagem(ns) para esta emiss�o!');
    end else
    begin
      if (GBCarteira.Visible = True) and (EdCarteira.ValueVariant <> 0) then
        Carteira := EdCarteira.ValueVariant
      else
        Carteira := QrLancto1CARTEIRABANCO.Value;
      //
      if Carteira = 0 then
      begin
        Geral.MB_Erro('Carteira n�o definida para a fun��o "TFmLctPgVarios.BtOKClick.QuitaRegistroAtual"!');
        Exit;
      end;
      //
      Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        FTabLctA, LAN_CTOS, 'Controle');
      //
      {
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Sit=3, Compensado=:P0,');
      Dmod.QrUpdM.SQL.Add('Documento=:P1, Descricao=:P2, ID_Pgto=:P3, ');
      Dmod.QrUpdM.SQL.Add('DataAlt=:Pa, UserAlt=:Pb');
      Dmod.QrUpdM.SQL.Add('WHERE Controle=:Pc AND Sub=:Pd AND Tipo=2');
      Dmod.QrUpdM.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TbLctoEditData.Value);
      Dmod.QrUpdM.Params[01].AsFloat   := Doc;
      Dmod.QrUpdM.Params[02].AsString  := TbLctoEditDescricao.Value;
      Dmod.QrUpdM.Params[03].AsFloat   := Controle2;
      //
      Dmod.QrUpdM.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpdM.Params[05].AsInteger := VAR_USUARIO;
      Dmod.QrUpdM.Params[06].AsFloat   := Controle;
      Dmod.QrUpdM.Params[07].AsInteger := Sub;
      Dmod.QrUpdM.ExecSQL;
      }
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['Sit', 'Compensado',
      'Documento', 'Descricao', 'ID_Quit'], ['Controle', 'Sub', 'Tipo'], [
      3, FormatDateTime(VAR_FORMATDATE, TbLctoEditData.Value),
      Doc, TbLctoEditDescricao.Value, Controle2], [Controle, Sub, 2], True, '',
      FTabLctA);
      //
      UFinanceiro.RecalcSaldoCarteira(QrLancto1Carteira.Value,
          nil, nil, False, True);
      //
      MultaVal   := TbLctoEditMultaVal.Value;
      MoraVal    := TbLctoEditJurosVal.Value;
      Data       := Geral.FDT(TbLctoEditData.Value, 1);
      //
      UFinanceiro.LancamentoDefaultVARS;
      FLAN_Data       := Data;
      FLAN_Vencimento := Geral.FDT(QrLancto1Vencimento.Value, 1);
      FLAN_DataCad    := Geral.FDT(Date, 1);
      //FLAN_Mez        := dmkPF.ITS_Null(QrLancto1Mez.Value);
      FLAN_Mez        := QrLancto1Mez.Value;
      //
      FLAN_Descricao  := TbLctoEditDescricao.Value;
      //
      Compensado := Geral.FDT(TbLctoEditData.Value, 1);
      if Compensado = '' then
        FLAN_Compensado := '0000-00-00'
      else
        FLAN_Compensado := Compensado; 
      //
      FLAN_Duplicata  := QrLancto1Duplicata.Value;
      FLAN_Doc2       := QrLancto1Doc2.Value;
      FLAN_SerieCH    := QrLancto1SerieCH.Value;

      FLAN_Documento  := Trunc(QrLancto1Documento.Value);
      FLAN_Tipo       := 1;
      FLAN_Carteira   := Carteira;
      FLAN_Credito    := Credito;
      FLAN_Debito     := Debito;
      FLAN_Genero     := QrLancto1Genero.Value;  // FAZER CONTABIL !!!!!!
      FLAN_GenCtb      := QrLancto1GenCtb.Value;
      //FLAN_GenCtbD     :=  // Definido abaixo!!!
      //FLAN_GenCtbC     :=  // Definido abaixo!!!
      FLAN_NotaFiscal := QrLancto1NotaFiscal.Value;
      FLAN_Sit        := 3;
      FLAN_Cartao     := 0;
      FLAN_Linha      := 0;
      FLAN_Fornecedor := QrLancto1Fornecedor.Value;
      FLAN_Cliente    := QrLancto1Cliente.Value;
      FLAN_MoraDia    := QrLancto1MoraDia.Value;
      FLAN_Multa      := QrLancto1Multa.Value;
      //FLAN_UserCad    := VAR_USUARIO;
      //FLAN_DataDoc    := Geral.FDT(Date, 1);
      FLAN_Vendedor   := QrLancto1Vendedor.Value;
      FLAN_Account    := QrLancto1Account.Value;
      FLAN_CentroCusto := QrLancto1CentroCusto.Value;
      FLAN_ICMS_P     := QrLancto1ICMS_P.Value;
      FLAN_ICMS_V     := QrLancto1ICMS_V.Value;
      FLAN_CliInt     := QrLancto1CliInt.Value;
      FLAN_Depto      := QrLancto1Depto.Value;
      FLAN_DescoPor   := QrLancto1DescoPor.Value;
      FLAN_ForneceI   := QrLancto1ForneceI.Value;
      FLAN_DescoVal   := QrLancto1DescoVal.Value;
      FLAN_NFVal      := QrLancto1NFVal.Value;
      FLAN_Unidade    := QrLancto1Unidade.Value;
      FLAN_Qtde       := QrLancto1Qtde.Value;
      FLAN_Qtd2       := QrLancto1Qtd2.Value;
      FLAN_FatID      := QrLancto1FatID.Value;
      FLAN_FatID_Sub  := QrLancto1FatID_Sub.Value;
      FLAN_FatNum     := Trunc(QrLancto1FatNum.Value);
      FLAN_FatParcela := QrLancto1FatParcela.Value;
      FLAN_MultaVal   := MultaVal;
      FLAN_MoraVal    := MoraVal;
      FLAN_ID_Pgto    := Round(Controle);
      FLAN_Controle   := Round(Controle2);
      //
      FLAN_VctoOriginal := Geral.FDT(QrLancto1VctoOriginal.Value, 1);
      FLAN_ModeloNF     := QrLancto1ModeloNF.Value;
      //
      //
      // ini 2022-03-24
      if not UFinanceiro.PagamentoEmBanco_DefineGenerosECarteira(
        QrLancto1Controle.Value, QrLancto1Sub.Value, FLAN_Credito,
        FLAN_Debito, (*OutraCartCompensa*)0, Carteira, QrLancto1GenCtbD.Value,
        QrLancto1GenCtbC.Value, FLAN_Carteira, FLAN_GenCtbD, FLAN_GenCtbC) then Exit;
      // fim 2022-03-24
      //
      UFinanceiro.InsereLancamento(FTabLctA);
    end;
  end;

begin
  Screen.Cursor := crHourGlass;
  PB1.Position  := 0;
  PB1.Max       := TbLctoEdit.RecordCount;
  //
  if MyObjects.FIC(((GBCarteira.Visible = True) and
    (EdCarteira.ValueVariant = 0)), EdCarteira, 'Carteira n�o definida!')
  then
    Exit;
  //
  TbLctoEdit.First;
  while not TbLctoEdit.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
    //
    if TbLctoEditControle.Value <> 0 then
      QuitaRegistroAtual();
    TbLctoEdit.Next;
  end;
  //
  UFinanceiro.RecalcSaldoCarteira(
    FQrCrt.FieldByName('Banco').AsInteger, nil, nil, False, false);
  UFinanceiro.RecalcSaldoCarteira(
    FQrCrt.FieldByName('Codigo').AsInteger, FQrCrt, FQrLct, True, True);
  //
  Screen.Cursor := crDefault;
  Close;
end;

end.


