object FmCartPgto: TFmCartPgto
  Left = 340
  Top = 178
  Caption = 'FIN-PGTOS-005 :: Pagamento / Rolagem de Emiss'#227'o'
  ClientHeight = 666
  ClientWidth = 1026
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 52
    Width = 1026
    Height = 125
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 292
    ExplicitTop = 120
    object Label2: TLabel
      Left = 10
      Top = 44
      Width = 31
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Conta:'
    end
    object Label3: TLabel
      Left = 188
      Top = 44
      Width = 52
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Sub-grupo:'
    end
    object Label1: TLabel
      Left = 10
      Top = 4
      Width = 62
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Lan'#231'amento:'
    end
    object Label5: TLabel
      Left = 94
      Top = 4
      Width = 51
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Descri'#231#227'o:'
    end
    object LaControle: TLabel
      Left = 177
      Top = 4
      Width = 6
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
      Visible = False
    end
    object Label6: TLabel
      Left = 364
      Top = 4
      Width = 44
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Empresa:'
    end
    object LaCred: TLabel
      Left = 842
      Top = 4
      Width = 92
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Cr'#233'dito documento:'
    end
    object LaDeb: TLabel
      Left = 714
      Top = 4
      Width = 90
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'D'#233'bito documento:'
    end
    object Label8: TLabel
      Left = 714
      Top = 44
      Width = 95
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'D'#233'bito pagamentos:'
    end
    object Label9: TLabel
      Left = 842
      Top = 44
      Width = 97
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Cr'#233'dito pagamentos:'
    end
    object Label12: TLabel
      Left = 10
      Top = 84
      Width = 42
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Entidade'
    end
    object Label13: TLabel
      Left = 866
      Top = 84
      Width = 59
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Vencimento:'
    end
    object Label14: TLabel
      Left = 763
      Top = 84
      Width = 42
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Emiss'#227'o:'
    end
    object Label4: TLabel
      Left = 364
      Top = 44
      Width = 32
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Grupo:'
    end
    object Label7: TLabel
      Left = 538
      Top = 44
      Width = 45
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Conjunto:'
    end
    object DBEdit1: TDBEdit
      Left = 10
      Top = 60
      Width = 170
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'NOMECONTA'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 188
      Top = 60
      Width = 170
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'NOMESUBGRUPO'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 1
    end
    object DBEdit5: TDBEdit
      Left = 364
      Top = 20
      Width = 344
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'NOMEEMPRESA'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 2
    end
    object EdControle: TDBEdit
      Left = 10
      Top = 20
      Width = 80
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Controle'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 3
    end
    object DBEdit7: TDBEdit
      Left = 94
      Top = 20
      Width = 264
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Descricao'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 4
    end
    object EdDeb: TdmkEdit
      Left = 714
      Top = 60
      Width = 124
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Color = clInactiveCaption
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdCred: TdmkEdit
      Left = 842
      Top = 60
      Width = 124
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Color = clInactiveCaption
      ReadOnly = True
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object DBEdit8: TDBEdit
      Left = 714
      Top = 20
      Width = 124
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Debito'
      DataSource = DataSource1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 7
    end
    object DBEdit9: TDBEdit
      Left = 842
      Top = 20
      Width = 124
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Credito'
      DataSource = DataSource1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 8
    end
    object DBEdit10: TDBEdit
      Left = 10
      Top = 100
      Width = 748
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'NOMERELACIONADO'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 9
    end
    object DBEdit11: TDBEdit
      Left = 866
      Top = 100
      Width = 100
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Vencimento'
      DataSource = DataSource1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 10
    end
    object DBEdit12: TDBEdit
      Left = 763
      Top = 100
      Width = 100
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Data'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 11
    end
    object DBEdit3: TDBEdit
      Left = 364
      Top = 60
      Width = 170
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'NOMEGRUPO'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 12
    end
    object DBEdit4: TDBEdit
      Left = 538
      Top = 60
      Width = 170
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clInactiveCaption
      DataField = 'NOMECONJUNTO'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 13
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 177
    Width = 1026
    Height = 383
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 207
    ExplicitHeight = 319
    object DBGPagtos: TDBGrid
      Left = 0
      Top = 85
      Width = 1026
      Height = 298
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      DataSource = DsPagtos
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = DBGPagtosDrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Documento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 178
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Title.Caption = 'Nota Fiscal'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Compensado'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Mes'
          Title.Caption = 'M'#234's'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESIT'
          Title.Caption = 'Situa'#231#227'o'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Lan'#231'amento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MoraVal'
          Title.Caption = 'Juros $'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MultaVal'
          Title.Caption = 'Multa $'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DescoVal'
          Title.Caption = 'Desconto $'
          Visible = True
        end>
    end
    object PnDesco: TPanel
      Left = 0
      Top = 0
      Width = 1026
      Height = 85
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 1
      Visible = False
      ExplicitLeft = 4
      object Label20: TLabel
        Left = 325
        Top = 44
        Width = 26
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data:'
      end
      object Label19: TLabel
        Left = 10
        Top = 44
        Width = 124
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Criar desconto na carteira:'
      end
      object LaDescoVal: TLabel
        Left = 325
        Top = 4
        Width = 52
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Valor juros:'
      end
      object LaDescoPor: TLabel
        Left = 10
        Top = 4
        Width = 110
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Fatura descontada por:'
      end
      object Label21: TLabel
        Left = 670
        Top = 31
        Width = 210
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#227'o mostrar este panel! Deprecado!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label22: TLabel
        Left = 670
        Top = 42
        Width = 210
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#227'o mostrar este panel! Deprecado!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label25: TLabel
        Left = 670
        Top = 19
        Width = 210
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#227'o mostrar este panel! Deprecado!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label26: TLabel
        Left = 670
        Top = 6
        Width = 210
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#227'o mostrar este panel! Deprecado!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label27: TLabel
        Left = 670
        Top = 66
        Width = 210
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#227'o mostrar este panel! Deprecado!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label28: TLabel
        Left = 670
        Top = 53
        Width = 210
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#227'o mostrar este panel! Deprecado!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BtPagtosAltera: TBitBtn
        Tag = 240
        Left = 485
        Top = 37
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desconta'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtPagtosAlteraClick
      end
      object TPData: TdmkEditDateTimePicker
        Left = 325
        Top = 60
        Width = 138
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 40425.846935405090000000
        Time = 40425.846935405090000000
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object CBCarteiraDesco: TdmkDBLookupComboBox
        Left = 69
        Top = 60
        Width = 251
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 2
        dmkEditCB = EdCarteiraDesco
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCarteiraDesco: TdmkEditCB
        Left = 10
        Top = 60
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCarteiraDesco
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdDescoVal: TdmkEdit
        Left = 325
        Top = 20
        Width = 139
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CBDescoPor: TdmkDBLookupComboBox
        Left = 69
        Top = 20
        Width = 251
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsDescoPor
        TabOrder = 5
        dmkEditCB = EdDescoPor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdDescoPor: TdmkEditCB
        Left = 10
        Top = 20
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBDescoPor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1026
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 974
      Top = 0
      Width = 52
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 967
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 52
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 52
      Top = 0
      Width = 922
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      ExplicitLeft = 59
      ExplicitWidth = 908
      ExplicitHeight = 59
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 433
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pagamento / Rolagem de Emiss'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 433
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pagamento / Rolagem de Emiss'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 433
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pagamento / Rolagem de Emiss'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 560
    Width = 1026
    Height = 36
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1022
      Height = 19
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 26
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 120
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 120
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 596
    Width = 1026
    Height = 70
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 591
      Top = 15
      Width = 433
      Height = 53
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitTop = 18
      ExplicitHeight = 66
      object Label10: TLabel
        Left = 5
        Top = 7
        Width = 65
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Saldo cr'#233'dito:'
      end
      object Label11: TLabel
        Left = 143
        Top = 7
        Width = 62
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Saldo d'#233'bito:'
      end
      object BtSaida: TBitBtn
        Tag = 13
        Left = 276
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
      object EdSaldoCred: TdmkEdit
        Left = 5
        Top = 26
        Width = 124
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdSaldoDeb: TdmkEdit
        Left = 143
        Top = 26
        Width = 124
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object PnGerencia: TPanel
      Left = 2
      Top = 15
      Width = 589
      Height = 53
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitTop = 18
      ExplicitHeight = 66
      object BtInclui: TBitBtn
        Tag = 10
        Left = 10
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 386
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera banco atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 135
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Exclui banco atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Excl&ui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtExcluiClick
      end
      object BtLocaliza1: TBitBtn
        Tag = 22
        Left = 261
        Top = 4
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Localizar'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtLocaliza1Click
      end
    end
  end
  object QrPagtos: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPagtosAfterOpen
    AfterScroll = QrPagtosAfterScroll
    OnCalcFields = QrPagtosCalcFields
    Left = 216
    Top = 269
    object QrPagtosData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPagtosCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrPagtosAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrPagtosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPagtosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 128
    end
    object QrPagtosNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrPagtosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '###,###,##0.00;-###,###,##0.00; '
    end
    object QrPagtosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '###,###,##0.00;-###,###,##0.00; '
    end
    object QrPagtosCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000; ; '
    end
    object QrPagtosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrPagtosVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPagtosFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPagtosFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPagtosNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrPagtosAno: TFloatField
      FieldName = 'Ano'
    end
    object QrPagtosMENSAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 15
      Calculated = True
    end
    object QrPagtosMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Size = 15
      Calculated = True
    end
    object QrPagtosMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrPagtosMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrPagtosSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrPagtosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrPagtosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagtosID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrPagtosMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrPagtosFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPagtosMoraVal: TFloatField
      FieldName = 'MoraVal'
      DisplayFormat = '###,###,##0.00;-###,###,##0.00; '
    end
    object QrPagtosMultaVal: TFloatField
      FieldName = 'MultaVal'
      DisplayFormat = '###,###,##0.00;-###,###,##0.00; '
    end
    object QrPagtosCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPagtosDescoVal: TFloatField
      FieldName = 'DescoVal'
      DisplayFormat = '###,###,##0.00;-###,###,##0.00; '
    end
  end
  object DsPagtos: TDataSource
    DataSet = QrPagtos
    Left = 244
    Top = 269
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    Left = 288
    Top = 269
    object QrSomaCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrSomaDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrSomaDesconto: TFloatField
      FieldName = 'Desconto'
    end
    object QrSomaMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrSomaJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSomaValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrSomaQtDtPg: TLargeintField
      FieldName = 'QtDtPg'
    end
  end
  object DataSource1: TDataSource
    Left = 12
    Top = 5
  end
  object QrDescoPor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece2="V"'
      'ORDER BY NomeENTIDADE')
    Left = 168
    Top = 148
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField3: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object IntegerField4: TIntegerField
      FieldName = 'Account'
      Required = True
    end
  end
  object DsDescoPor: TDataSource
    DataSet = QrDescoPor
    Left = 196
    Top = 148
  end
  object QrCarteiras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo'
      'FROM carteiras ')
    Left = 168
    Top = 212
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.carteiras.Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.carteiras.Nome'
      FixedChar = True
      Size = 18
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 196
    Top = 212
  end
end
