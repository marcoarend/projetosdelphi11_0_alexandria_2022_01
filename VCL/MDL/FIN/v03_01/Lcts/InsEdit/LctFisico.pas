{$I dmk.inc}
unit LctFisico;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmLctFisico = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasUsaTalao: TSmallintField;
    DsCarteiras: TDataSource;
    Label15: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    SbCarteira: TSpeedButton;
    Label1: TLabel;
    TPData: TdmkEditDateTimePicker;
    QrFornClie: TmySQLQuery;
    QrFornClieCodigo: TIntegerField;
    QrFornClieAccount: TIntegerField;
    QrFornClieNOMEENTIDADE: TWideStringField;
    DsFornClie: TDataSource;
    LaNF: TLabel;
    EdNotaFiscal: TdmkEdit;
    RGFisicoSrc: TRadioGroup;
    EdFornClie: TdmkEditCB;
    CBFornClie: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    RGFornClie: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGFornClieClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FEmpresa, FEntidade, FCarteira, FFornecedor, FCliente, FNotaFiscal: Integer;
    FData: TDateTime;
  public
    { Public declarations }
    FArrCartCod, FArrCartDoc: array of Integer;
    procedure InicializaForm(Entidade, Empresa: Integer);
    procedure ReopenFornClie();
    procedure MostraLctEditC(FisicoSrc, FisicoCod: Integer);

  end;

  var
  FmLctFisico: TFmLctFisico;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleLct0, UnFinanceiro,
{$IFDEF DELPHI12_UP}
   // XE2 Ver o que fazer!
{$ELSE}
  //ObtemFoto2, Migrar para ObtemFoto4???
{$ENDIF}
  LctEditC, ModuleCond;

{$R *.DFM}

procedure TFmLctFisico.BtOKClick(Sender: TObject);
var
  FornClie, FisicoSrc, FisicoCod: Integer;
begin
  FCarteira := EdCarteira.ValueVariant;
  if MyObjects.FIC(FCarteira = 0, EdCarteira, 'Informe a Carteira!') then
    Exit;
  FornClie := EdFornClie.ValueVariant;
  if MyObjects.FIC(FornClie = 0, EdFornClie, 'Informe o Fornecedor / Cliente!') then
    Exit;
  FFornecedor := 0;
  FCliente := 0;
  case RGFornClie.ItemIndex of
    0: FFornecedor := FornClie;
    1: FCliente := FornClie;
    else begin
      Geral.MensagemBox('Op��o n�o implementada para tipo de transa��o!',
      'ERRO', MB_OK+MB_ICONERROR);
      Halt(0);
    end;
  end;
  //
  FNotaFiscal := EdNotaFiscal.ValueVariant;
  FData := TPData.Date;
  //
  FisicoCod := 0;
  FisicoSrc := RGFisicoSrc.ItemIndex;
  //
  case FisicoSrc of
    0:
    begin
      Geral.MensagemBox('Defina a fonte f�sica do documento!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    1:
    begin
      MostraLctEditC(FisicoSrc, FisicoCod);
      // Parei aqui!
      //Close; // Continua inserindo...
    end;
    2:
    begin
{$IFDEF DELPHI12_UP}
      // #XE2 Ver o que fazer!
      ShowMessage('Em implementa��o! Solicite � DERMATEK');
{$ELSE}
      FmObtemFoto2.FFisicoCod := FisicoCod;
      FmObtemFoto2.FTerceiro := EdFornClie.ValueVariant;
      FmObtemFoto2.FNotaFiscal := EdNotaFiscal.ValueVariant;
      FmLctFisico.Hide;
      if FmObtemFoto2.Visible = False then
        FmObtemFoto2.Show;
      FmObtemFoto2.BringToFront;
      FmObtemFoto2.Enabled := True;
(*
      FisicoCod := FmObtemFoto2.FFisicoCod;
      //
      if FisicoCod = 0 then
        Exit
      else
        MostraLctEditC(FisicoSrc, FisicoCod);
*)
{$ENDIF}
    end
    else
    begin
      Geral.MensagemBox(
      'A a��o para a "fonte f�sica do documento" selecionada n�o foi implementada!',
      'Mensagem', MB_OK+MB_ICONINFORMATION);
      Exit;
    end;
  end;
end;

procedure TFmLctFisico.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctFisico.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctFisico.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Screen.Cursor := crHourGlass;
  try
    case RGFisicoSrc.ItemIndex of
      2: // Foto
      begin
{$IFDEF DELPHI12_UP}
        // #XE2 Ver o que fazer!
{$ELSE}
        if RGFisicoSrc.Enabled = False then
          FmObtemFoto2.Close;
{$ENDIF}
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctFisico.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
end;

procedure TFmLctFisico.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctFisico.FormShow(Sender: TObject);
begin
  FmLctFisico.BringToFront;
end;

procedure TFmLctFisico.InicializaForm(Entidade, Empresa: Integer);
begin
  FEmpresa := Empresa;
  FEntidade := Entidade;
  //
  TPData.Date := Date;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCarteiras, Dmod.MyDB, [
  'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, ',
  'Tipo, ExigeNumCheque, ForneceI, Banco1, UsaTalao ',
  'FROM carteiras ',
  'WHERE ForneceI=' + Geral.FF0(Entidade),
  'ORDER BY Nome ',
  '']);
  //
  ReopenFornClie();
end;

procedure TFmLctFisico.MostraLctEditC(FisicoSrc, FisicoCod: Integer);
const
  AlteraAtehFatID = True;
  Acao = tgrInclui;
begin
  if UFinanceiro.InclusaoLancamento(TFmLctEditC, FmLctEditC, lfCondominio,
  afmoNegarComAviso, DmLct0.QrLct, DmLct0.QrCrt,
  Acao, DmLct0.QrLctControle.Value, DmLct0.QrLctSub.Value,
  0(*Genero.Value*), DmCond.QrCondPercJuros.Value, DmCond.QrCondPercMulta.Value,
  nil(*SetaVars*), 0(*FatID*), 0(*FatID_Sub*), 0(*FatNum*), FCarteira(*Carteira*),
  0(*Credito*), 0(*Debito*), AlteraAtehFatID,
  FCliente(*Cliente*), FFornecedor(*Fornecedor*), FEntidade(*cliInt*),
  0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
  False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
  FData(*Data*), FData(*Vencto*), 0(*DataDoc*), 2(*IDFinalidade*), 0(*Mes: TDateTime*),
  DmLct0.FtabLctA, FisicoSrc, FisicoCod) > 0 then
  begin
    {
    DmLct0.LocCod(DmLct0.QrCrtCodigo.Value, DmLct0.QrCrtCodigo.Value,
    DmLct0.QrCrt, '');
    DmLct0.QrLct.Locate('Controle', FLAN_CONTROLE, []);
    }
    //
    if EdCarteira.Visible and EdCarteira.Enabled then
    try
      EdFornClie.ValueVariant := 0;
      CBFornClie.KeyValue := Null;
      EdNotaFiscal.ValueVariant := 0;
      //
      EdCarteira.SetFocus;
    except

    end;
  end;
end;

procedure TFmLctFisico.ReopenFornClie();
begin
  QrFornClie.Close;
  case RGFornClie.ItemIndex of
    0:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrFornClie, Dmod.MyDB, [
      'SELECT Codigo, Account, ',
      'CASE WHEN Tipo=0 THEN RazaoSocial ',
      'ELSE Nome END NOMEENTIDADE ',
      'FROM entidades ',
      'WHERE Fornece1="V" ',
      'OR Fornece2="V" ',
      'OR Fornece3="V" ',
      'OR Fornece4="V" ',
      'OR Fornece5="V" ',
      'OR Fornece6="V" ',
      'OR Fornece7="V" ',
      'OR Fornece8="V" ',
      'OR Terceiro="V" ',
      'ORDER BY NomeENTIDADE ',
      '']);
    end;
    1:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrFornClie, Dmod.MyDB, [
      'SELECT Codigo, Account, ',
      'CASE WHEN Tipo=0 THEN RazaoSocial ',
      'ELSE Nome END NOMEENTIDADE ',
      'FROM entidades ',
      'WHERE Cliente1="V" ',
      'OR Cliente2="V" ',
      'OR Cliente3="V" ',
      'OR Cliente4="V" ',
      'OR Terceiro="V" ',
      'ORDER BY NomeENTIDADE ',
      '']);
    end;
  end;
end;

procedure TFmLctFisico.RGFornClieClick(Sender: TObject);
begin
  ReopenFornClie();
  EdFornClie.ValueVariant := 0;
  CBFornClie.KeyValue := 0;
end;

end.
