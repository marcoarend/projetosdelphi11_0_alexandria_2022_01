unit EmpresaSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, Db, mySQLDbTables, Grids,
  DBGrids, dmkDBGrid, dmkEdit, dmkDBLookupComboBox, dmkEditCB, dmkGeral,
  dmkImage, UnDmkEnums;

type
  TFmEmpresaSel = class(TForm)
    dmkDBGrid1: TdmkDBGrid;
    Panel5: TPanel;
    Panel1: TPanel;
    Panel3: TPanel;
    RGLetraLct: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox1: TGroupBox;
    EdNome: TdmkEdit;
    GroupBox2: TGroupBox;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdNomeChange(Sender: TObject);
    procedure dmkDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //FEmp resa, FEnt idade: Integer;
  end;

  var
  FmEmpresaSel: TFmEmpresaSel;

implementation

uses UnMyObjects, ModuleGeral, Principal, UnInternalConsts;

{$R *.DFM}

procedure TFmEmpresaSel.BtSaidaClick(Sender: TObject);
begin
  DModG.EmpresaAtual_SetaCodigos(0, tecEntidade, False);
  //
  Close;
end;

procedure TFmEmpresaSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmpresaSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmpresaSel.EdNomeChange(Sender: TObject);
var
  Filtro: String;
begin
  Filtro := Trim(EdNome.Text);
  if Filtro <> '' then
    Filtro := 'AND (IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE "' + EdNome.Text + '")';
  DModG.ReopenEmpresas(VAR_USUARIO, 0, nil, nil, Filtro);
end;

procedure TFmEmpresaSel.dmkDBGrid1DblClick(Sender: TObject);
begin
  if DModG.QrEmpresas.RecordCount > 0 then
  begin
    EdEmpresa.ValueVariant := DModG.QrEmpresasFilial.Value;
    CBEmpresa.KeyValue     := DModG.QrEmpresasFilial.Value;
    BtOKClick(Self);
  end;
end;

procedure TFmEmpresaSel.BtOKClick(Sender: TObject);
begin
  if EdEmpresa.ValueVariant = 0 then
  begin
    Geral.MB_Aviso('Defina a empresa!');
  end else begin
    DModG.EmpresaAtual_SetaCodigos(DModG.QrEmpresasCodigo.Value, tecEntidade, True);
    VAR_LETRA_LCT := RGLetraLct.Items[RGLetraLct.ItemIndex][1];
    // N�o pode!
    //FmPrincipal.DefineVarsCliInt(FEmpresa);
    DModG.NomeTab(TMeuDB, ntLct, True);
    Close;
  end;
end;

procedure TFmEmpresaSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stNil;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

end.
