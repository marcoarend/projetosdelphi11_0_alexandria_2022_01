unit SelfGer2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkDBGrid, ComCtrls, DBCtrls, Grids,
  DBGrids, Menus, Db, mySQLDbTables, dmkPermissoes, dmkGeral, dmkEdit,
  dmkEditCB, Variants, dmkDBLookupComboBox, Mask, UMySQLModule,
  dmkEditDateTimePicker, UnDmkProcFunc, dmkCompoStore, dmkDBGridZTO,
  UnDmkEnums, UnGrl_Geral;

type
  TFmSelfGer2 = class(TForm)
    Timer2: TTimer;
    PMAtzCarts: TPopupMenu;
    Acarteiraselecionada1: TMenuItem;
    Todascarteiras1: TMenuItem;
    PMMenu: TPopupMenu;
    Transferir1: TMenuItem;
    Nova1: TMenuItem;
    Editar1: TMenuItem;
    Excluir1: TMenuItem;
    Localizar1: TMenuItem;
    Quitar1: TMenuItem;
    Compensar1: TMenuItem;
    Reverter1: TMenuItem;
    N10: TMenuItem;
    Pagar1: TMenuItem;
    N11: TMenuItem;
    PagarAVista1: TMenuItem;
    N2: TMenuItem;
    Localizarpagamentosdestaemisso1: TMenuItem;
    Lanamento1: TMenuItem;
    Localizar2: TMenuItem;
    Copiar1: TMenuItem;
    Mudacarteiradelanamentosselecionados1: TMenuItem;
    Carteiras1: TMenuItem;
    Data1: TMenuItem;
    Compensao1: TMenuItem;
    Msdecompetncia1: TMenuItem;
    TextoParcial1: TMenuItem;
    Recibo1: TMenuItem;
    RetornoCNAB1: TMenuItem;
    Colocarmsdecompetnciaondenotem1: TMenuItem;
    Datalancto1: TMenuItem;
    Vencimento1: TMenuItem;
    PMQuita: TPopupMenu;
    Compensar2: TMenuItem;
    Reverter2: TMenuItem;
    MenuItem3: TMenuItem;
    Pagar2: TMenuItem;
    MenuItem5: TMenuItem;
    PagarAVista2: TMenuItem;
    PMRefresh: TPopupMenu;
    Carteira1: TMenuItem;
    Atual5: TMenuItem;
    Todas1: TMenuItem;
    ContaPlanodecontas1: TMenuItem;
    PMSaldoAqui: TPopupMenu;
    Calcula1: TMenuItem;
    Limpa1: TMenuItem;
    Diferena1: TMenuItem;
    PMEspecificos: TPopupMenu;
    N1: TMenuItem;
    Exclusoincondicional1: TMenuItem;
    ransfernciaentrecontas1: TMenuItem;
    Inclui1: TMenuItem;
    PMTrfCta: TPopupMenu;
    Novatransferncia1: TMenuItem;
    Alteratransferncia1: TMenuItem;
    Excluitransferncia1: TMenuItem;
    dmkPermissoes1: TdmkPermissoes;
    QrDonosCart: TmySQLQuery;
    DsDonosCart: TDataSource;
    QrDonosCartForneceI: TIntegerField;
    QrDonosCartNO_ENT: TWideStringField;
    DsDonosLcto: TDataSource;
    QrDonosLcto: TmySQLQuery;
    QrDonosLctoFilial: TIntegerField;
    QrDonosLctoNO_ENT: TWideStringField;
    QrDonosLctoCodigo: TIntegerField;
    QrDonosCartFilial: TIntegerField;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_ENT: TWideStringField;
    QrFornecedores: TmySQLQuery;
    DsFornecedores: TDataSource;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNO_ENT: TWideStringField;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    DsCarteiras: TDataSource;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrCarteira: TmySQLQuery;
    QrCarteiraCodigo: TIntegerField;
    QrCarteiraNome: TWideStringField;
    DsCarteira: TDataSource;
    PMRelat: TPopupMenu;
    Pesquisas1: TMenuItem;
    Result1: TMenuItem;
    porPerodo1: TMenuItem;
    Mensais1: TMenuItem;
    Saldos1: TMenuItem;
    SaldosEm1: TMenuItem;
    Descontodeduplicatas1: TMenuItem;
    Extrato1: TMenuItem;
    PagarReceber1: TMenuItem;
    Movimento1: TMenuItem;
    QrCarteirasForneceI: TIntegerField;
    Emitidos1: TMenuItem;
    Quitados1: TMenuItem;
    PainelLct: TPanel;
    PainelDados2: TPanel;
    Panel5: TPanel;
    Panel7: TPanel;
    Label20: TLabel;
    Label5: TLabel;
    Label19: TLabel;
    Label21: TLabel;
    BtSaldoAqui: TBitBtn;
    EdSdoAqui: TdmkEdit;
    BtRefresh: TBitBtn;
    EdSoma2: TdmkEdit;
    DBEdCredito: TDBEdit;
    DBEdDebito: TDBEdit;
    DBEdSaldo: TDBEdit;
    Panel6: TPanel;
    Panel3: TPanel;
    BtMenu: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtEspecificos: TBitBtn;
    BtConcilia: TBitBtn;
    BtQuita: TBitBtn;
    BtCNAB: TBitBtn;
    BtCopiaCH: TBitBtn;
    BtContarDinheiro: TBitBtn;
    BtFluxoCxa: TBitBtn;
    BtAutom: TBitBtn;
    BtTrfCta: TBitBtn;
    BtLctoEndoss: TBitBtn;
    BtRelat: TBitBtn;
    Panel4: TPanel;
    Label14: TLabel;
    Label8: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label16: TLabel;
    Label18: TLabel;
    Label1: TLabel;
    CkDataI: TCheckBox;
    TPDataI: TdmkEditDateTimePicker;
    CkDataF: TCheckBox;
    TPDataF: TdmkEditDateTimePicker;
    CkVctoI: TCheckBox;
    TPVctoI: TdmkEditDateTimePicker;
    CkVctoF: TCheckBox;
    TPVctoF: TdmkEditDateTimePicker;
    EdCliIntCart: TdmkEditCB;
    CBCliIntCart: TdmkDBLookupComboBox;
    EdCliIntLancto: TdmkEditCB;
    CBCliIntLancto: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    EdMezIni: TdmkEdit;
    CkMezIni: TCheckBox;
    EdMezFim: TdmkEdit;
    CkMezFim: TCheckBox;
    BitBtn1: TBitBtn;
    RGValores: TRadioGroup;
    CBUH: TDBLookupComboBox;
    Panel8: TPanel;
    Label6: TLabel;
    EdCodigo: TDBEdit;
    EdNome: TDBEdit;
    StatusBar: TStatusBar;
    DBGLct: TdmkDBGridZTO;
    ProgressBar1: TProgressBar;
    Panel2: TPanel;
    Label2: TLabel;
    DBText1: TDBText;
    Label3: TLabel;
    DBText2: TDBText;
    Label4: TLabel;
    DBText3: TDBText;
    Label7: TLabel;
    DBText4: TDBText;
    CSTabSheetChamou: TdmkCompoStore;
    Panel1: TPanel;
    BtSair: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGCarts1DblClick(Sender: TObject);
    procedure DBGLctCellClick(Column: TColumn);
    procedure DBGLctDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGLctKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PageControl1Change(Sender: TObject);
    procedure PMQuitaPopup(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Acarteiraselecionada1Click(Sender: TObject);
    procedure Todascarteiras1Click(Sender: TObject);
    procedure Nova1Click(Sender: TObject);
    procedure Editar1Click(Sender: TObject);
    procedure Excluir1Click(Sender: TObject);
    procedure Localizar1Click(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure Compensar1Click(Sender: TObject);
    procedure Reverter1Click(Sender: TObject);
    procedure Pagar1Click(Sender: TObject);
    procedure PagarAVista1Click(Sender: TObject);
    procedure Localizarpagamentosdestaemisso1Click(Sender: TObject);
    procedure Localizar2Click(Sender: TObject);
    procedure Copiar1Click(Sender: TObject);
    procedure Carteiras1Click(Sender: TObject);
    procedure Data1Click(Sender: TObject);
    procedure Compensao1Click(Sender: TObject);
    procedure Msdecompetncia1Click(Sender: TObject);
    procedure TextoParcial1Click(Sender: TObject);
    procedure Recibo1Click(Sender: TObject);
    procedure RetornoCNAB1Click(Sender: TObject);
    procedure Datalancto1Click(Sender: TObject);
    procedure Vencimento1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtConciliaClick(Sender: TObject);
    procedure BtQuitaClick(Sender: TObject);
    procedure Compensar2Click(Sender: TObject);
    procedure Reverter2Click(Sender: TObject);
    procedure Pagar2Click(Sender: TObject);
    procedure PagarAVista2Click(Sender: TObject);
    procedure BtCNABClick(Sender: TObject);
    procedure BtCopiaCHClick(Sender: TObject);
    procedure TPDataIChange(Sender: TObject);
    procedure CBUHClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtEspecificosClick(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure Atual5Click(Sender: TObject);
    procedure Todas1Click(Sender: TObject);
    procedure BtSaldoAquiClick(Sender: TObject);
    procedure Calcula1Click(Sender: TObject);
    procedure Limpa1Click(Sender: TObject);
    procedure Diferena1Click(Sender: TObject);
    procedure BtContarDinheiroClick(Sender: TObject);
    procedure BtFluxoCxaClick(Sender: TObject);
    procedure BtAutomClick(Sender: TObject);
    procedure TPDataFChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Exclusoincondicional1Click(Sender: TObject);
    procedure BtTrfCtaClick(Sender: TObject);
    procedure Novatransferncia1Click(Sender: TObject);
    procedure Alteratransferncia1Click(Sender: TObject);
    procedure Excluitransferncia1Click(Sender: TObject);
    procedure PMTrfCtaPopup(Sender: TObject);
    procedure BtLctoEndossClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CkDataIClick(Sender: TObject);
    procedure CkDataFClick(Sender: TObject);
    procedure CkVctoIClick(Sender: TObject);
    procedure CkVctoFClick(Sender: TObject);
    procedure EdCliIntCartChange(Sender: TObject);
    procedure EdCliIntLanctoChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdFornecedorChange(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
    procedure EdContaChange(Sender: TObject);
    procedure RGValoresClick(Sender: TObject);
    procedure BtRelatClick(Sender: TObject);
    procedure Pesquisas1Click(Sender: TObject);
    procedure Mensais1Click(Sender: TObject);
    procedure Saldos1Click(Sender: TObject);
    procedure SaldosEm1Click(Sender: TObject);
    procedure Descontodeduplicatas1Click(Sender: TObject);
    procedure PagarReceber1Click(Sender: TObject);
    procedure Movimento1Click(Sender: TObject);
    procedure Emitidos1Click(Sender: TObject);
    procedure Quitados1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtSairClick(Sender: TObject);
  private
    { Private declarations }
    procedure AvisoJanelaDeprecada(Codigo: Integer);
    procedure ShowHint(Sender: TObject);
    procedure SomaLinhas_2;
    procedure VerificaBotoes();
    procedure VerificaCarteirasCliente();
    procedure MudaDataLctSel(CampoData: String);
    procedure ReverterCompensacao;
    procedure CompensarContaCorrenteBanco;
    procedure PagarAVistaEmCaixa;
    procedure Colocarmsdecompetnciaondenotem(Tipo: Integer);
    procedure ReopenSomaLinhas();
    procedure FechaLcto();
    procedure CompletaSQL(Query: TmySQLQuery);
    procedure PagarRolarEmissao();
    function  ReopenLct(Controle, Sub: Integer): Boolean;
  public
    { Public declarations }
    FModuleLctX: TDataModule;
    FDtEncer, FDtMorto: TDateTime;
    FTabLctA, FTabLctB, FTabLctD: String;
    //
    FFinalidade: TIDFinalidadeLct;
    FGeradorTxt: String;
    procedure AtualizaCarteiraAtual();
    procedure AtualizaTodasCarteiras();
    //procedure CompensarContaCorrenteBanco;
    procedure DefineDataModule(DM: TDataModule);
    procedure MyFormCreate(Sender: TObject);
  end;

  var
  FmSelfGer2: TFmSelfGer2;

implementation

uses
{$IfNDef NAO_CMEB} Concilia, {$EndIf}
  UnMyObjects, Principal, UnInternalConsts, Module, LctMudaCart, GetData,
  MudaTexto, UnFinanceiro, LctPgEmCxa, UnGOTOy, MyDBCheck,
  ModuleFin, ModuleGeral, (*FluxoCxa,*) LctEdit, Pesquisas, Resmes,
  PrincipalImp,
  (*
       Ver se j� n�o tem no financeiro novo!:
  FinForm,
       Ver se realmente precisa!:
  LctoEndoss, DescDupli,
       Ver se j� n�o tem no Balancete!:
  Resultados1, , Resultados2
  *)
  Extratos2, Saldos,
  //
  ModuleLct2, MyGlyfs, DmkDAC_PF;

{$R *.DFM}

procedure TFmSelfGer2.FechaLcto;
begin
  //D m o d F i n . Q r L c t o s .Close;
  TDmLct2(FModuleLctX).QrLctos.Close;
  DBGLct.Visible := False;
end;

procedure TFmSelfGer2.FormActivate(Sender: TObject);
begin
  if TFmSelfGer2(Self).Owner is TApplication then
  begin
    MyObjects.DefineTituloDBGrid(TDBGrid(DBGLct), 'FatNum', VAR_TITULO_FATNUM);
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmSelfGer2.FormResize(Sender: TObject);
begin
(*
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
*)
end;

procedure TFmSelfGer2.FormShow(Sender: TObject);
begin
  MyObjects.DefineTituloDBGrid(TDBGrid(DBGLct), 'FatNum', VAR_TITULO_FATNUM);
  MyObjects.CorIniComponente();
  //
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmSelfGer2.DBGCarts1DblClick(Sender: TObject);
begin
(*
  PageControl1.ActivePageIndex := 2;
  PageControl1Change(PageControl1);
*)
end;

procedure TFmSelfGer2.DefineDataModule(DM: TDataModule);
begin
  FModuleLctX := DM;
  //
{
  EdCodigo.DataSource     := TDmLct2(FModuleLctX).DsCrt;
  EdNome.DataSource       := TDmLct2(FModuleLctX).DsCrt;
  EdSaldo.DataSource      := TDmLct2(FModuleLctX).DsCrt;
  EdDiferenca.DataSource  := TDmLct2(FModuleLctX).DsCrt;
  EdCaixa.DataSource      := TDmLct2(FModuleLctX).DsCrt;
  DBGCrt.DataSource       := TDmLct2(FModuleLctX).DsCrt;
  //
  DBGLct.DataSource       := TDmLct2(FModuleLctX).DsLct;
  DBEdit1.DataSource      := TDmLct2(FModuleLctX).DsLct;
}
end;

procedure TFmSelfGer2.Descontodeduplicatas1Click(Sender: TObject);
begin

  AvisoJanelaDeprecada(1);

(* Ver se realmente precisa
  if DBCheck.CriaFm(TFmDescDupli, FmDescDupli, afmoNegarComAviso) then
  begin
    FmDescDupli.ShowModal;
    FmDescDupli.Destroy;
  end;
*)
end;

procedure TFmSelfGer2.DBGLctCellClick(Column: TColumn);
begin
  Somalinhas_2;
end;

procedure TFmSelfGer2.DBGLctDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
(*  Ver o que fazer ERRO DE DESENHO DO TEXTO !!!
  MyObjects.DefineCorTextoSitLancto(TDBGrid(Sender), Rect, 'NOMESIT',
    TDmLct2(FModuleLctX).QrLctosNOMESIT.Value);
*)
end;

procedure TFmSelfGer2.DBGLctKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
const
  ExcluiMesmoSeNegativo = True;
begin
  if (key=VK_UP) and (Shift=[ssShift]) then Somalinhas_2
  else if (key=VK_DOWN) and (Shift=[ssShift]) then Somalinhas_2
  //
  else if key=13 then

{
  UFinanceiro.InsAltLancamento(TFmLctEdit, FmLctEdit,
    lfProprio, afmoNegarComAviso, TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts,
    tgrAltera, TDmLct2(FModuleLctX).QrLctosControle.Value, TDmLct2(FModuleLctX).QrLctosSub.Value,
    TDmLct2(FModuleLctX).QrLctosGenero.Value, Dmod.QrControleMyPerJuros.Value,
    Dmod.QrControleMyPerMulta.Value, nil, 0, 0, 0, 0, 0, 0, False,
    0(*Cliente*), 0(*Fornecedor*), TDmLct2(FModuleLctX).QrCartsForneceI.Value(*cliInt*),
    0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
    False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
    0, 0, TDmLct2(FModuleLctX).QrLctosCompensado.Value, 0, 1, 0)
}
  UFinanceiro.AlteracaoLancamento(TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrLctos, lfProprio,
    0(*OneAccount*), TDmLct2(FModuleLctX).QrCartsForneceI.Value(*OneCliInt*), FTabLctA (*, True LockCliInt*),
    False)

  else if (key=VK_DELETE) and (Shift=[ssCtrl]) then
  UFinanceiro.ExcluiItemCarteira(TDmLct2(FModuleLctX).QrLctosControle.Value,
    TDmLct2(FModuleLctX).QrLctosData.Value, TDmLct2(FModuleLctX).QrLctosCarteira.Value,
    TDmLct2(FModuleLctX).QrLctosSub.Value, TDmLct2(FModuleLctX).QrLctosGenero.Value,
    TDmLct2(FModuleLctX).QrLctosCartao.Value, TDmLct2(FModuleLctX).QrLctosSit.Value,
    TDmLct2(FModuleLctX).QrLctosTipo.Value, 0, TDmLct2(FModuleLctX).QrLctosID_Pgto.Value,
    TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts, True,
    TDmLct2(FModuleLctX).QrLctosCarteira.Value, (*TDmLct2(FModuleLctX).QrLctosCarteira.Value*)
    dmkPF.MotivDel_ValidaCodigo(313),
    FTabLcta, ExcluiMesmoSeNegativo)
  else if (key=VK_F4) and (Shift=[ssCtrl]) then
    MyObjects.MostraPopUpDeBotaoObject(PMQuita, DBGLct, 0, 0)
  //else if (key=VK_F5) and (Shift=[ssCtrl]) then ExibirLista1Click(Self)
  //else if (key=VK_F6) and (Shift=[ssCtrl]) then Promissria1Click(Self)
end;

procedure TFmSelfGer2.PageControl1Change(Sender: TObject);
begin
(*
  //!BtSaida.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    1: VerificaCarteirasCliente();
    2: VerificaBotoes;
  end;
  //!PainelConfirma.Visible := PageControl1.ActivePageIndex in ([0,1,2]);
*)
end;

procedure TFmSelfGer2.Pesquisas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPesquisas, FmPesquisas, afmoNegarComAviso) then
  begin
    FmPesquisas.ShowModal;
    FmPesquisas.Destroy;
  end;
end;

procedure TFmSelfGer2.SomaLinhas_2;
begin
  Timer2.Enabled := False;
  Timer2.Enabled := True;
end;

procedure TFmSelfGer2.PMQuitaPopup(Sender: TObject);
const
  k = 3;
var
  i, n: Integer;
  c: array [0..k] of Integer;
begin
  for i := 0 to k do c[i] := 0;
  if DBGLct.SelectedRows.Count > 1 then
  begin
    with DBGLct.DataSource.DataSet do
    for i:= 0 to DBGLct.SelectedRows.Count-1 do
    begin
      //GotoBookmark(DBGLct.SelectedRows.Items[i]);
      GotoBookmark(DBGLct.SelectedRows.Items[i]);
      Inc(c[TDmLct2(FModuleLctX).QrLctosSit.Value], 1);
    end;
    n := DBGLct.SelectedRows.Count;
  end else
  begin
    c[TDmLct2(FModuleLctX).QrLctosSit.Value] := 1;
    n := 1;
  end;
  Compensar2.Enabled   := False;
  Pagar2.Enabled       := False;
  Reverter2.Enabled    := False;
  PagarAVista2.Enabled := False;
  if TDmLct2(FModuleLctX).QrCartsTipo.Value = 2 then
  begin
    //if TDmLct2(FModuleLctX).QrLctosSit.Value in [0] then   Compensar2.Enabled := True;
    Compensar2.Enabled := c[0] = n;
    //if TDmLct2(FModuleLctX).QrLctosSit.Value in [0,1,2] then   Pagar2.Enabled := True;
    Pagar2.Enabled := (n=1) and (c[3] = 0);
    //if TDmLct2(FModuleLctX).QrLctosSit.Value in [3] then    Reverter2.Enabled := True;
    Reverter2.Enabled := c[3] = n;
    //if TDmLct2(FModuleLctX).QrLctosSit.Value in [0] then PagarAvista2.Enabled := True;
    PagarAvista2.Enabled := c[0] = n;
  end;
end;

procedure TFmSelfGer2.PMTrfCtaPopup(Sender: TObject);
begin
  Alteratransferncia1.Enabled := TDmLct2(FModuleLctX).QrLctosFatID.Value = -1;
  Excluitransferncia1.Enabled := TDmLct2(FModuleLctX).QrLctosFatID.Value = -1;
end;

procedure TFmSelfGer2.Quitados1Click(Sender: TObject);
begin

  AvisoJanelaDeprecada(2);

{
  if DBCheck.CriaFm(TFmResultados2, FmResultados2, afmoNegarComAviso) then
  begin
    FmResultados2.ShowModal;
    FmResultados2.Destroy;
  end;
}
end;

procedure TFmSelfGer2.VerificaCarteirasCliente();
var
  Carteira: Integer;
begin
  if TDmLct2(FModuleLctX).QrCarts.State <> dsBrowse then Carteira := 0
  else Carteira := TDmLct2(FModuleLctX).QrCartsCodigo.Value;
  //
  if (TDmLct2(FModuleLctX).QrCarts.State = dsInactive)
  or not DmodG.EmpresaLogada(TDmLct2(FModuleLctX).QrCartsForneceI.Value) then
    //DmodFin.ReabreCarteiras(Carteira, TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrCartSum,
    TDmLct2(FModuleLctX).ReabreCarteiras(Carteira, TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrCartSum,
    'TFmSelfGer2.VerificaCarteirasCliente()');
end;

procedure TFmSelfGer2.VerificaBotoes();
var
  Ativo: Boolean;
begin
  if TDmLct2(FModuleLctX).QrCarts.State = dsBrowse then
  begin
    if TDmLct2(FModuleLctX).QrCarts.RecordCount > 0 then Ativo := True
    else Ativo := False;
  end else Ativo := False;
  //BtMenu.Enabled := Ativo;
  //BtInclui.Enabled := Ativo;
  BtSaldoAqui.Enabled := Ativo;
  BtContarDinheiro.Enabled := Ativo;
  BtRefresh.Enabled := Ativo;
  //
  Ativo := (TDmLct2(FModuleLctX).QrLctos.State = dsBrowse) and (TDmLct2(FModuleLctX).QrLctos.RecordCount > 0);
  BtAltera.Enabled := Ativo;
  BtExclui.Enabled := Ativo;
  //
  (*if TDmLct2(FModuleLctX).QrLctos.State = dsBrowse then
  begin
    if TDmLct2(FModuleLctX).QrLctos.RecordCount > 0 then Ativo := True
    else Ativo := False;
  end else Ativo := False;*)
end;

procedure TFmSelfGer2.Timer2Timer(Sender: TObject);
var
  Debito, Credito: Double;
  i: Integer;
begin
  Timer2.Enabled := False;
  Debito := 0;
  Credito := 0;
  with DBGLct.DataSource.DataSet do
  for i:= 0 to DBGLct.SelectedRows.Count-1 do
  begin
    //GotoBookmark(DBGLct.SelectedRows.Items[i]);
    GotoBookmark(DBGLct.SelectedRows.Items[i]);
    Debito := Debito + TDmLct2(FModuleLctX).QrLctosDebito.Value;
    Credito := Credito + TDmLct2(FModuleLctX).QrLctosCredito.Value;
  end;
  EdSoma2.Text := Geral.FFT(Credito-Debito, 2, siNegativo);
end;

procedure TFmSelfGer2.Acarteiraselecionada1Click(Sender: TObject);
begin
  AtualizaCarteiraAtual();
end;

procedure TFmSelfGer2.Todascarteiras1Click(Sender: TObject);
begin
  AtualizaTodasCarteiras();
end;

procedure TFmSelfGer2.AtualizaCarteiraAtual();
begin
  UFinanceiro.AtualizaVencimentos(FTabLctA);
  UFinanceiro.RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrCartsCodigo.Value,
    TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrLctos, True, True);
end;

procedure TFmSelfGer2.AtualizaTodasCarteiras();
var
  Carteira: Integer;
begin
  // parei aqui. ver
  Screen.Cursor := crHourGlass;
  Carteira := TDmLct2(FModuleLctX).QrCartsCodigo.Value;
  UFinanceiro.AtualizaVencimentos(FTabLctA);
  TDmLct2(FModuleLctX).QrCarts.DisableControls;
  TDmLct2(FModuleLctX).QrCarts.First;
  while not TDmLct2(FModuleLctX).QrCarts.Eof do
  begin
    UFinanceiro.RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrCartsCodigo.Value,
      TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrLctos, True, True);
    //
    TDmLct2(FModuleLctX).QrCarts.Next;
  end;
{ Ver o que fazer!
  DmodFin.LocCod(Carteira, Carteira, TDmLct2(FModuleLctX).QrCarts,
    'TFmSelfGer2.AtualizaTodasCarteiras()');
}
  TDmLct2(FModuleLctX).QrCarts.EnableControls;
  Screen.Cursor := crDefault;
end;

procedure TFmSelfGer2.AvisoJanelaDeprecada(Codigo: Integer);
begin
  Geral.MB_Aviso(
  'Janela deprecada. Caso haja interesse de reativ�-la solicite � Dermatek' +
  sLineBreak + 'C�digo: ' + Geral.FF0(Codigo));
end;

procedure TFmSelfGer2.Nova1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCart(0, TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts,
    True, CliInt, 0, 0, 0, FTabLctA);
  UFinanceiro.RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrCartsCodigo.Value,
   TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrLctos, True, True);
  //if QrCarts.State = dsBrowse then ReopenResumo;
end;

procedure TFmSelfGer2.Novatransferncia1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCtas(0, TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts,
    True, CliInt, 0, 0, 0, FTabLctA);
  TDmLct2(FModuleLctX).RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrCartsTipo.Value,
  TDmLct2(FModuleLctX).QrCartsCodigo.Value, 1, FTabLctA);
end;

procedure TFmSelfGer2.EdCarteiraChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.EdClienteChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.EdCliIntCartChange(Sender: TObject);
begin
  FTabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, EdCliIntCart.ValueVariant);
  FechaLcto();
end;

procedure TFmSelfGer2.EdCliIntLanctoChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.EdContaChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.EdFornecedorChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.Editar1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCart(1, TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts,
    True, CliInt, 0, 0, 0, FTabLctA);
  UFinanceiro.RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrCartsCodigo.Value,
    TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrLctos, True, True);
end;

procedure TFmSelfGer2.Emitidos1Click(Sender: TObject);
begin

  AvisoJanelaDeprecada(3)

{
  if DBCheck.CriaFm(TFmResultados1, FmResultados1, afmoNegarComAviso) then
  begin
    FmResultados1.FTabLctA := FTabLctA;
    FmResultados1.ShowModal;
    FmResultados1.Destroy;
  end;
}
end;

procedure TFmSelfGer2.Excluir1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCart(2, TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts,
    True, CliInt, 0, 0, 0, FTabLctA);
  UFinanceiro.RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrCartsCodigo.Value,
    TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrLctos, True, True);
end;

procedure TFmSelfGer2.Excluitransferncia1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCtas(2, TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts,
    True, CliInt, 0, 0, 0, FTabLctA);
  TDmLct2(FModuleLctX).RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrCartsTipo.Value,
  TDmLct2(FModuleLctX).QrCartsCodigo.Value, 1, FTabLctA);
end;

procedure TFmSelfGer2.Exclusoincondicional1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if Geral.MB_Pergunta('Confirma a exclus�o INCONDICIONAL deste lan�amento?') = ID_YES then
  begin
    UFinanceiro.ExcluiLct_Unico(FTabLctA, TDmLct2(FModuleLctX).QrLctos.Database,
      TDmLct2(FModuleLctX).QrLctosData.Value, TDmLct2(FModuleLctX).QrLctosTipo.Value,
      TDmLct2(FModuleLctX).QrLctosCarteira.Value, TDmLct2(FModuleLctX).QrLctosControle.Value,
      TDmLct2(FModuleLctX).QrLctosSub.Value, dmkPF.MotivDel_ValidaCodigo(313), True);
    //
    TDmLct2(FModuleLctX).QrLctos.Next;
    Controle := TDmLct2(FModuleLctX).QrLctosControle.Value;
    {
    Parei Aqui
    n�o esta recalculando!
    TDmLct2(FModuleLctX).RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrLctosTipo.Value,
      TDmLct2(FModuleLctX).QrLctosCarteira.Value, 1);
    }
    TDmLct2(FModuleLctX).QrLctos.Close;
    UnDmkDAC_PF.AbreQuery(TDmLct2(FModuleLctX).QrLctos, Dmod.MyDB);
    TDmLct2(FModuleLctX).QrLctos.Locate('Controle', Controle, []);
  end;
end;

procedure TFmSelfGer2.Localizar1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCart(3, TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts,
    True, CliInt, 0, 0, 0, FTabLctA);
  UFinanceiro.RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrCartsCodigo.Value,
    TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrLctos, True, True);
end;

procedure TFmSelfGer2.BtMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMenu, BtMenu);
end;

procedure TFmSelfGer2.Compensar1Click(Sender: TObject);
begin
  CompensarContaCorrenteBanco;
end;

procedure TFmSelfGer2.CompensarContaCorrenteBanco();
var
  Codigo, Controle: Integer;
begin

  //AvisoJanelaDeprecada(4);

(*
  Codigo   := TDmLct2(FModuleLctX).QrLctosCarteira.Value;
  Controle := TDmLct2(FModuleLctX).QrLctosControle.Value;
  UFinanceiro.CriaFmQuitaDoc2(TDmLct2(FModuleLctX).QrLctosData.Value,
    TDmLct2(FModuleLctX).QrLctosSub.Value, TDmLct2(FModuleLctX).QrLctosControle.Value,
    TDmLct2(FModuleLctX).QrLctosCtrlIni.Value, TDmLct2(FModuleLctX).QrLctosSub.Value,
    TDmLct2(FModuleLctX).QrCarts);
  DmodFin.LocCod(Codigo, Codigo, TDmLct2(FModuleLctX).QrCarts,
    'TFmSelfGer2.CompensarContaCorrenteBanco()');
  TDmLct2(FModuleLctX).QrLctos.Locate('Controle', Controle, []);
*)
  UFinanceiro.Quitacao_CompensaNaContaCorrente(TDmLct2(FModuleLctX).QrCarts,
    TDmLct2(FModuleLctX).QrLctos, FTabLctA);
end;

procedure TFmSelfGer2.Reverter1Click(Sender: TObject);
begin
  ReverterCompensacao;
end;

procedure TFmSelfGer2.ReverterCompensacao;
var
  i, k: Integer;
begin
  if DBGLct.SelectedRows.Count > 1 then
  begin
    if Geral.MB_Pergunta('Confirma a revers�o da compensa��o ' +
      'dos itens selecionados?') = ID_YES then
    begin
      with DBGLct.DataSource.DataSet do
      for i:= 0 to DBGLct.SelectedRows.Count-1 do
      begin
        //GotoBookmark(DBGLct.SelectedRows.Items[i]);
        GotoBookmark(DBGLct.SelectedRows.Items[i]);
        if TDmLct2(FModuleLctX).QrLctosSit.Value = 3 then
          UFinanceiro.ReverterPagtoEmissao(TDmLct2(FModuleLctX).QrLctos,
            TDmLct2(FModuleLctX).QrCarts, False, False, False, FTabLctA);
      end;
      k := TDmLct2(FModuleLctX).QrLctosControle.Value;
      UFinanceiro.RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrCartsCodigo.Value,
        TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrLctos, True, True);
      TDmLct2(FModuleLctX).QrLctos.Locate('Controle', k, []);
    end;
  end else UFinanceiro.ReverterPagtoEmissao(TDmLct2(FModuleLctX).QrLctos,
    TDmLct2(FModuleLctX).QrCarts, True, True, True, FTabLctA);
end;

procedure TFmSelfGer2.RGValoresClick(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.Pagar1Click(Sender: TObject);
begin
  PagarRolarEmissao;
end;

procedure TFmSelfGer2.PagarAVista1Click(Sender: TObject);
begin
  PagarAVistaEmCaixa;
end;

procedure TFmSelfGer2.Localizarpagamentosdestaemisso1Click(Sender: TObject);
begin

  AvisoJanelaDeprecada(5)

(*
  UFinanceiro.LocalizaPagamentosDeEmissao(TDmLct2(FModuleLctX).QrLctosControle.Value,
    TPDataI, TPDataF, TDmLct2(FModuleLctX).QrLctos);
*)
end;

procedure TFmSelfGer2.Localizar2Click(Sender: TObject);
begin
  UFinanceiro.LocalizarLancamento(TPDataI, TPDataF, 0, TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrLct, True, 0, FTabLctA, FModuleLctX, 0, False);
end;

procedure TFmSelfGer2.Copiar1Click(Sender: TObject);
begin

  AvisoJanelaDeprecada(6)

{
  DmodFin.AlteraLanctoEsp(TDmLct2(FModuleLctX).QrLctos, 3,
    Dmod.QrControleMyPerJuros.Value, Dmod.QrControleMyPerMulta.Value,
    TPDataI, TPDataF, CBUH.KeyValue, FFinalidade);
}
end;

procedure TFmSelfGer2.Carteiras1Click(Sender: TObject);
var
  i, Carteira, Tipo, Sit: Integer;
  Compensado: String;
begin
  if DBCheck.CriaFm(TFmLctMudaCart, FmLctMudaCart, afmoNegarComAviso) then
  begin
    FmLctMudaCart.ShowModal;
    Carteira := FmLctMudaCart.FCarteiraSel;
    Tipo     := FmLctMudaCart.FCarteiraTip;
    FmLctMudaCart.Destroy;
    if Carteira = TDmLct2(FModuleLctX).QrLctosCarteira.Value then Exit;
    if Carteira <> -1000 then
    begin
      Screen.Cursor := crHourGlass;
      with DBGLct.DataSource.DataSet do
      for i:= 0 to DBGLct.SelectedRows.Count-1 do
      begin
        if Tipo <> 2 then
        begin
          Sit := 3;
          Compensado := Geral.FDT(TDmLct2(FModuleLctX).QrLctosVencimento.Value, 1)
        end else begin
          Sit := TDmLct2(FModuleLctX).QrLctosControle.Value;
          Compensado := Geral.FDT(TDmLct2(FModuleLctX).QrLctosCompensado.Value, 1)
        end;
        if TDmLct2(FModuleLctX).QrLctosGenero.Value < 1 then
        begin
          Geral.MB_Aviso('O lan�amento ' + Geral.FF0(
            TDmLct2(FModuleLctX).QrLctosControle.Value) + ' � protegido. Para editar ' +
            'este item selecione seu caminho correto!');
          Exit;
        end;
        if TDmLct2(FModuleLctX).QrLctosCartao.Value > 0 then
        begin
          Geral.MB_Aviso('O lan�amento ' + Geral.FF0(
            TDmLct2(FModuleLctX).QrLctosControle.Value) + ' n�o pode ser editado pois ' +
            'pertence a uma fatura!');
          Exit;
        end;
        //GotoBookmark(DBGLct.SelectedRows.Items[i]);
        GotoBookmark(DBGLct.SelectedRows.Items[i]);
        {
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Carteira=:P0, Tipo=:P1, ');
        Dmod.QrUpd.SQL.Add('Sit=:P2, Compensado=:P3 ');
        Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
        Dmod.QrUpd.SQL.Add('');
        Dmod.QrUpd.Params[00].AsInteger := Carteira;
        Dmod.QrUpd.Params[01].AsInteger := Tipo;
        Dmod.QrUpd.Params[02].AsInteger := Sit;
        Dmod.QrUpd.Params[03].AsString  := Compensado;
        //
        Dmod.QrUpd.Params[04].AsInteger := TDmLct2(FModuleLctX).QrLctosControle.Value;
        Dmod.QrUpd.Params[05].AsInteger := TDmLct2(FModuleLctX).QrLctosSub.Value;
        Dmod.QrUpd.ExecSQL;
        }
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Carteira', 'Tipo', 'Sit', 'Compensado'], ['Controle', 'Sub'], [
        Carteira, Tipo, Sit, Compensado], [
        TDmLct2(FModuleLctX).QrLctosControle.Value, TDmLct2(FModuleLctX).QrLctosSub.Value], True, '', FTabLctA);
      end;
      UFinanceiro.RecalcSaldoCarteira(Carteira, nil, nil, False, False);
      UFinanceiro.RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrLctosCarteira.Value,
        TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrLctos,  True, True);
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelfGer2.MudaDataLctSel(CampoData: String);
var
  //, Tipo
  i, Carteira, Controle, Sub: Integer;
  Data: String;
begin
  //Tipo     := -1000;
  Carteira := -1000;
  MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
  FmGetData.TPData.Date := Date;
  FmGetData.ShowModal;
  FmGetData.Destroy;
  if VAR_GETDATA = 0 then
    Exit
  else
  begin
    with DBGLct.DataSource.DataSet do
    for i:= 0 to DBGLct.SelectedRows.Count-1 do
    begin
      //GotoBookmark(DBGLct.SelectedRows.Items[i]);
      GotoBookmark(DBGLct.SelectedRows.Items[i]);
      //
      if (TDmLct2(FModuleLctX).QrLctosSit.Value < 2) and
      (Uppercase(CampoData) = Uppercase('Compensado')) then
      begin
        Geral.MB_Aviso('O lan�amento ' + Geral.FF0(
          TDmLct2(FModuleLctX).QrLctosControle.Value) + ' n�o foi compensado ainda!');
        Exit;
      end;
      if (Uppercase(CampoData) = Uppercase('Mez')) then
      begin
        if VAR_GETDATA = 0 then
          Data := ''
        else
          Data := Geral.FDT(VAR_GETDATA, 13);
      end else Data := Geral.FDT(VAR_GETDATA, 1);
      {
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET '+CampoData+'=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P1 AND Sub=:P2');
      Dmod.QrUpd.SQL.Add('');
      //
      Dmod.QrUpd.Params[00].AsString  := Data;
      Dmod.QrUpd.Params[01].AsInteger := TDmLct2(FModuleLctX).QrLctosControle.Value;
      Dmod.QrUpd.Params[02].AsInteger := TDmLct2(FModuleLctX).QrLctosSub.Value;
      Dmod.QrUpd.ExecSQL;
      }
      Controle := TDmLct2(FModuleLctX).QrLctosControle.Value;
      Sub      := TDmLct2(FModuleLctX).QrLctosSub.Value;
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      CampoData], ['Controle', 'Sub'], [Data], [Controle, Sub], True, '', FTabLctA);
      if (Uppercase(CampoData) = Uppercase('Compensado')) then
      begin
        {
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET Data=:P0');
        Dmod.QrUpdM.SQL.Add('WHERE ID_Pgto=:P1 AND ID_Pgto>0');
        Dmod.QrUpdM.SQL.Add('');
        Dmod.QrUpdM.Params[00].AsString  := Data;
        Dmod.QrUpdM.Params[01].AsInteger := TDmLct2(FModuleLctX).QrLctosControle.Value;
        Dmod.QrUpdM.ExecSQL;
        // Igualar  Vencimento e compensado dos pagamentos � vista
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET Compensado=:P0, Vencimento=:P1');
        Dmod.QrUpdM.SQL.Add('WHERE ID_Pgto=:P2 AND Tipo<>2 AND ID_Pgto > 0');
        Dmod.QrUpdM.Params[00].AsString  := Data;
        Dmod.QrUpdM.Params[01].AsString  := Data;
        Dmod.QrUpdM.Params[02].AsInteger := TDmLct2(FModuleLctX).QrLctosControle.Value;
        Dmod.QrUpdM.ExecSQL;
        }
        if Controle > 0 then
        begin
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Data'], ['ID_Pgto'], [Data], [Controle], True, '', FTabLctA);
          // Igualar  Vencimento e compensado dos pagamentos � vista
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Compensado', 'Vencimento'], ['ID_Pgto', 'Tipo'], [
          Data, Data], [Controle], True, '', FTabLctA);
        end;
      end;
    end;
    UFinanceiro.RecalcSaldoCarteira(Carteira, nil, nil, False, False);
    UFinanceiro.RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrLctosCarteira.Value,
      TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrLctos, True, True);
  end;
end;

procedure TFmSelfGer2.MyFormCreate(Sender: TObject);
begin
  UnDmkDAC_PF.AbreQuery(QrDonosCart, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDonosLcto, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornecedores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  //
  TDmLct2(FModuleLctX).FTPDataIni := TPDataI;
  TDmLct2(FModuleLctX).FTPDataFim := TPDataF;
  FFinalidade                     := idflIndefinido;
  Application.OnHint              := ShowHint;
  //PageControl1.ActivePageIndex    := 0;
  //DBGCarteiras.DataSource      := TDmLct2(FModuleLctX).DsCarts;
  //DBGCarts1.DataSource         := TDmLct2(FModuleLctX).DsCarts;
  //DBGCarts2.DataSource         := TDmLct2(FModuleLctX).DsCarts;
  EdCodigo.DataSource            := TDmLct2(FModuleLctX).DsLctos;
  EdNome.DataSource              := TDmLct2(FModuleLctX).DsLctos;
  //EdSaldo.DataSource           := TDmLct2(FModuleLctX).DsCarts;
  //EdDiferenca.DataSource       := TDmLct2(FModuleLctX).DsCarts;
  //EdCaixa.DataSource           := TDmLct2(FModuleLctX).DsCarts;
  //
  DBGLct.DataSource            := TDmLct2(FModuleLctX).DsLctos;
  DBText1.DataSource           := TDmLct2(FModuleLctX).DsLctos;
  DBText2.DataSource           := TDmLct2(FModuleLctX).DsLctos;
  DBText3.DataSource           := TDmLct2(FModuleLctX).DsLctos;
  DBText4.DataSource           := TDmLct2(FModuleLctX).DsLctos;
  //
  DBEdCredito.DataSource       := TDmLct2(FModuleLctX).DsSomaLinhas;
  DBEdDebito.DataSource        := TDmLct2(FModuleLctX).DsSomaLinhas;
  DBEdSaldo.DataSource         := TDmLct2(FModuleLctX).DsSomaLinhas;
  //////////////////////////////////////////////////////////////////////////////
  TPDataI.Date := Date - Geral.ReadAppKey('Dias', Application.Title,
    ktInteger, 60, HKEY_LOCAL_MACHINE);
  TPDataF.Date := Date;
  //////////////////////////////////////////////////////////////////////////////
  //
  {
  Imagem := Geral.ReadAppKey(
    'ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg', HKEY_LOCAL_MACHINE);
  if FileExists(Imagem) then
    ImgSelfGer.Bitmap.LoadFromFile(Imagem);
  }
  if DmodG.SoUmaEmpresaLogada(False) then
  begin
    EdCliIntCart.ValueVariant := DmodG.QrFiliLogFilial.Value;
    CBCliIntCart.KeyValue     := DmodG.QrFiliLogFilial.Value;
    //
    EdCliIntLancto.ValueVariant := DmodG.QrFiliLogFilial.Value;
    CBCliIntLancto.KeyValue     := DmodG.QrFiliLogFilial.Value;
  end;
  //
end;

procedure TFmSelfGer2.Data1Click(Sender: TObject);
begin
  MudaDataLctSel('Data');
end;

procedure TFmSelfGer2.Compensao1Click(Sender: TObject);
begin
  MudaDataLctSel('Compensado');
end;

procedure TFmSelfGer2.Mensais1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmResMes, FmResMes, afmoNegarComAviso) then
  begin
    FmResMes.ShowModal;
    FmResMes.Destroy;
  end;
end;

procedure TFmSelfGer2.Movimento1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPrincipalImp, FmPrincipalImp, afmoNegarComAviso) then
  begin
    FmPrincipalImp.ShowModal;
    FmPrincipalImp.Destroy;
  end;
end;

procedure TFmSelfGer2.Msdecompetncia1Click(Sender: TObject);
begin
  MudaDataLctSel('Mez');
end;

procedure TFmSelfGer2.TextoParcial1Click(Sender: TObject);
var
  i: Integer;
  Atual, Novo, TodoTexto: String;
  Muda: Boolean;
begin
  if DBCheck.CriaFm(TFmMudaTexto, FmMudaTexto, afmoNegarComAviso) then
  begin
    FmMudaTexto.ShowModal;
    Muda  := FmMudaTexto.FMuda;
    Atual := FmMudaTexto.FAtual;
    Novo  := FmMudaTexto.FNovo;
    FmMudaTexto.Destroy;
    if Muda then
    begin
      Screen.Cursor := crHourGlass;
      with DBGLct.DataSource.DataSet do
      for i:= 0 to DBGLct.SelectedRows.Count-1 do
      begin
        //GotoBookmark(DBGLct.SelectedRows.Items[i]);
        GotoBookmark(DBGLct.SelectedRows.Items[i]);
        TodoTexto := TDmLct2(FModuleLctX).QrLctosDescricao.Value;
        if pos(Atual, TodoTexto) > 0 then
        begin
          TodoTexto := Geral.Substitui(TodoTexto, Atual, Novo);
          {
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Descricao=:P0 ');
          Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
          Dmod.QrUpd.SQL.Add('');
          Dmod.QrUpd.Params[00].AsString  := TodoTexto;
          //
          Dmod.QrUpd.Params[01].AsInteger := TDmLct2(FModuleLctX).QrLctosControle.Value;
          Dmod.QrUpd.Params[02].AsInteger := TDmLct2(FModuleLctX).QrLctosSub.Value;
          Dmod.QrUpd.ExecSQL;
          }
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Descricao'], ['Controle', 'Sub'], [TodoTexto], [
          TDmLct2(FModuleLctX).QrLctosControle.Value, TDmLct2(FModuleLctX).QrLctosSub.Value], True, '', FTabLctA);
        end;
      end;
    end;
    ReopenLct(TDmLct2(FModuleLctX).QrLctosControle.Value, TDmLct2(FModuleLctX).QrLctosSub.Value);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelfGer2.PagarAVistaEmCaixa;
begin
  UFinanceiro.PagarAVistaEmCaixa(TDmLct2(FModuleLctX).QrCarts,
    TDmLct2(FModuleLctX).QrLctos, TDBGrid(DBGLct), FTabLctA);
end;

procedure TFmSelfGer2.PagarReceber1Click(Sender: TObject);
begin
  if UFinanceiro.AtualizaPagamentosAVista(TDmLct2(FModuleLctX).QrLctos, FTabLctA) then
  begin
    if DBCheck.CriaFm(TFmExtratos2, FmExtratos2, afmoNegarComAviso) then
    begin
      FmExtratos2.ShowModal;
      FmExtratos2.Destroy;
    end;
  end;
end;

procedure TFmSelfGer2.PagarRolarEmissao;
var
  Lancto, ControleToLoc: Integer;
begin
  UFinanceiro.PagarRolarEmissao(TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrLct, FTabLctA, False, Lancto);
  //
  if Lancto <> 0 then
    UFinanceiro.LocalizarlanctoCliInt(Lancto, 0, EdCliIntCart.ValueVariant,
      TPDataI, TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt,
      TDmLct2(FModuleLctX).QrCrtSum, True, FTabLctA, FTabLctB, FTabLctD,
      ControleToLoc);

end;

procedure TFmSelfGer2.Recibo1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{$IFNDEF NAO_USA_RECIBO}
  Codigo := 0; // Fazer. Parei Aqui!!!
  if TDmLct2(FModuleLctX).QrLctosCredito.Value > 0 then
    GOTOy.EmiteRecibo(Codigo,
    TDmLct2(FModuleLctX).QrLctosCliente.Value,
    TDmLct2(FModuleLctX).QrLctosCliInt.Value,
    TDmLct2(FModuleLctX).QrLctosCredito.Value, 0, 0,
    IntToStr(TDmLct2(FModuleLctX).QrLctosControle.Value) + '-' +
    IntToStr(TDmLct2(FModuleLctX).QrLctosSub.Value),
    TDmLct2(FModuleLctX).QrLctosDescricao.Value, '', '',
    TDmLct2(FModuleLctX).QrLctosData.Value, TDmLct2(FModuleLctX).QrLctosSit.Value)
  else
    GOTOy.EmiteRecibo(Codigo,
    TDmLct2(FModuleLctX).QrLctosCliInt.Value,
    TDmLct2(FModuleLctX).QrLctosFornecedor.Value,
    TDmLct2(FModuleLctX).QrLctosDebito.Value, 0, 0,
    IntToStr(TDmLct2(FModuleLctX).QrLctosControle.Value) + '-' +
    IntToStr(TDmLct2(FModuleLctX).QrLctosSub.Value),
    TDmLct2(FModuleLctX).QrLctosDescricao.Value, '', '',
    TDmLct2(FModuleLctX).QrLctosData.Value, TDmLct2(FModuleLctX).QrLctosSit.Value);
{$Else}
  Geral.MB_Aviso('M�dulo de Recibo desabilitado!');
{$EndIf}
end;

procedure TFmSelfGer2.RetornoCNAB1Click(Sender: TObject);
begin
  FmPrincipal.RetornoCNAB;
end;

procedure TFmSelfGer2.Datalancto1Click(Sender: TObject);
begin
  Colocarmsdecompetnciaondenotem(0);
end;

procedure TFmSelfGer2.Vencimento1Click(Sender: TObject);
begin
  Colocarmsdecompetnciaondenotem(1);
end;

procedure TFmSelfGer2.Colocarmsdecompetnciaondenotem(Tipo: Integer);
var
  Data: TDateTime;
  Ano, Mes, Dia: Word;
begin
  if Geral.MB_Pergunta('Somente os lan�amentos presentes na grade ' +
    ' (conforme sele��a de carteira e per�odo) ser�o analisados! Deseja ' +
    'continuar assim mesmo?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    TDmLct2(FModuleLctX).QrLctos.First;
    while not TDmLct2(FModuleLctX).QrLctos.Eof do
    begin
      Application.ProcessMessages;
      DmodFin.QrLocCta.Close;
      DmodFin.QrLocCta.Params[0].AsInteger := TDmLct2(FModuleLctX).QrLctosGenero.Value;
      UnDmkDAC_PF.AbreQuery(DmodFin.QrLocCta, Dmod.MyDB);
      if (Uppercase(DmodFin.QrLocCtaMensal.Value) = 'V')
      and (TDmLct2(FModuleLctX).QrLctosMez.Value = 0) then
      begin
        case Tipo of
          1: {Vencimento} Data := TDmLct2(FModuleLctX).QrLctosVencimento.Value;
          else {Data}     Data := TDmLct2(FModuleLctX).QrLctosData.Value;
        end;
        DecodeDate(Data, Ano, Mes, Dia);
        while ano > 100 do Ano := Ano - 100;
        {
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Mez=:P0 ');
        Dmod.QrUpd.SQL.Add('WHERE Controle=:P1 ');
        Dmod.QrUpd.SQL.Add('');
        Dmod.QrUpd.Params[00].AsInteger := Ano * 100 + Mes;
        Dmod.QrUpd.Params[01].AsInteger := TDmLct2(FModuleLctX).QrLctosControle.Value;
        Dmod.QrUpd.ExecSQL;
        }
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['Mez'], ['Controle'], [
        Ano * 100 + Mes], [TDmLct2(FModuleLctX).QrLctosControle.Value], True, '', FTabLctA);
      end;
      TDmLct2(FModuleLctX).QrLctos.Next;
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelfGer2.BtIncluiClick(Sender: TObject);
var
  IDFinalidade: Integer;
begin
  VerificaCarteirasCliente();
  VerificaBotoes();
  // Ver juros e multa da construtora
  IDFinalidade := Integer(FFinalidade);
  //
  if UFinanceiro.InclusaoLancamento((*TFmLctEdit, FmLctEdit,*)
  lfProprio, afmoNegarComAviso, TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts,
  tgrInclui, 0, 0, TDmLct2(FModuleLctX).QrLctosGenero.Value,
  Dmod.QrControle.FieldByName('MyPerJuros').AsFloat, Dmod.QrControle.FieldByName('MyPerMulta').AsFloat, nil,
  0, 0, 0, 0, 0, 0, False,
  EdCliente.ValueVariant, EdFornecedor.ValueVariant, TDmLct2(FModuleLctX).QrCartsForneceI.Value(*cliInt*),
  0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
  False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
  0(*Data*), 0(*Vencto*), TDmLct2(FModuleLctX).QrLctosCompensado.Value(*DataDoc*),
  IDFinalidade, 0(*Mes*), FTabLctA, 0(*FisicoSrc*), 0(*FisicoCod*)) > 0 then
    ReopenLct(TDmLct2(FModuleLctX).QrLctosControle.Value, TDmLct2(FModuleLctX).QrLctosSub.Value);
end;

procedure TFmSelfGer2.BtLctoEndossClick(Sender: TObject);
begin

  AvisoJanelaDeprecada(7);

(*
  if DBCheck.CriaFm(TFmLctoEndoss, FmLctoEndoss, afmoNegarComAviso) then
  begin
    FmLctoEndoss.FControle  := TDmLct2(FModuleLctX).QrLctosControle.Value;
    FmLctoEndoss.FSub       := TDmLct2(FModuleLctX).QrLctosSub.Value;
    FmLctoEndoss.FTPDataIni := TPDataI;
    FmLctoEndoss.FCarteiras := TDmLct2(FModuleLctX).QrCarts;
    FmLctoEndoss.FLct       := TDmLct2(FModuleLctX).QrLctos;
    //
    FmLctoEndoss.ReopenLancto(0, 0, True);
    //
    FmLctoEndoss.ShowModal;
    FmLctoEndoss.Destroy;
  end;
*)
end;

procedure TFmSelfGer2.BitBtn1Click(Sender: TObject);
begin
  ReopenLct(TDmLct2(FModuleLctX).QrLctosControle.Value, TDmLct2(FModuleLctX).QrLctosSub.Value);
end;

procedure TFmSelfGer2.BtAlteraClick(Sender: TObject);
begin
{
  if UFinanceiro.InsAltLancamento(TFmLctEdit, FmLctEdit,
  lfProprio, afmoNegarComAviso, TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts,
  tgrAltera, TDmLct2(FModuleLctX).QrLctosControle.Value, TDmLct2(FModuleLctX).QrLctosSub.Value,
  TDmLct2(FModuleLctX).QrLctosGenero.Value, Dmod.QrControleMyPerJuros.Value,
  Dmod.QrControleMyPerMulta.Value, nil,
  0, 0, 0, 0, 0, 0, False,
  0(*Cliente*), 0(*Fornecedor*), TDmLct2(FModuleLctX).QrCartsForneceI.Value(*cliInt*),
  0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
  False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
  0, 0, TDmLct2(FModuleLctX).QrLctosCompensado.Value, 0, 1, 0) > 0 then
    ReopenLct(TDmLct2(FModuleLctX).QrLctosControle.Value, TDmLct2(FModuleLctX).QrLctosSub.Value);
}
  UFinanceiro.AlteracaoLancamento(TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrLctos, lfProprio,
    0(*OneAccount*), TDmLct2(FModuleLctX).QrCartsForneceI.Value(*OneCliInt*), FTabLctA (*, True LockCliInt*),
    False)
end;

procedure TFmSelfGer2.BtExcluiClick(Sender: TObject);
begin

AvisoJanelaDeprecada(8)


(*
  UFinanceiro.ExcluiLanctoGrade(TDBGrid(DBGLct),
    TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts, False);
*)
end;

procedure TFmSelfGer2.BtConciliaClick(Sender: TObject);
begin
{$IfNDef NAO_CMEB}
  // Criar carteira de concilia��o
  if DBCheck.CriaFm(TFmConcilia, FmConcilia, afmoNegarComAviso) then
  begin
    FmConcilia.FQrCrt        := TDmLct2(FModuleLctX).QrCarts;
    FmConcilia.FQrLct        := TDmLct2(FModuleLctX).QrLctos;
    FmConcilia.ReopenConcilia(0);
    //FmConcilia.FCartConcil := QrCondCartConcil.Value;
    FmConcilia.FCartConcilia := 0;
    FmConcilia.FMostra       := FmConcilia.QrConcilia0.RecordCount > 0;
    FmConcilia.F_CliInt      := TDmLct2(FModuleLctX).QrCartsForneceI.Value;
    FmConcilia.FDTPicker     := TPDataI;
    FmConcilia.ShowModal;
    FmConcilia.Destroy;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(mdlappConciBco);
{$EndIf}
end;

procedure TFmSelfGer2.BtQuitaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMQuita, BtQuita);
end;

procedure TFmSelfGer2.Compensar2Click(Sender: TObject);
begin
  CompensarContaCorrenteBanco;
end;

procedure TFmSelfGer2.Reverter2Click(Sender: TObject);
begin
  ReverterCompensacao;
end;

procedure TFmSelfGer2.Pagar2Click(Sender: TObject);
begin
  PagarRolarEmissao;
end;

procedure TFmSelfGer2.PagarAVista2Click(Sender: TObject);
begin
  PagarAVistaEmCaixa;
end;

procedure TFmSelfGer2.BtCNABClick(Sender: TObject);
begin
  FmPrincipal.RetornoCNAB;
end;

procedure TFmSelfGer2.BtCopiaCHClick(Sender: TObject);
begin
(*  Ver se j� n�o tem no financeiro novo!

  FmFinForm.ImprimeCopiaDeCh(TDmLct2(FModuleLctX).QrLctosControle.Value,
    TDmLct2(FModuleLctX).QrLctosCliInt.Value);
*)
end;

procedure TFmSelfGer2.TPDataIChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.CBUHClick(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.CkDataFClick(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.CkDataIClick(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.CkVctoFClick(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.CkVctoIClick(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Geral.WriteAppKey('Dias', Application.Title, Date - TPDataI.Date, ktInteger, HKEY_LOCAL_MACHINE);
  //
  TDmLct2(FModuleLctX).FTPDataIni := nil;
  TDmLct2(FModuleLctX).FTPDataFim := nil;
end;

procedure TFmSelfGer2.Saldos1Click(Sender: TObject);
begin
  DmodFin.ImprimeSaldos(TDmLct2(FModuleLctX).QrCartsForneceI.Value);
end;

procedure TFmSelfGer2.SaldosEm1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSaldos, FmSaldos, afmoNegarComAviso) then
  begin
    FmSaldos.ShowModal;
    FmSaldos.Destroy;
  end;
end;

procedure TFmSelfGer2.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmSelfGer2.BtEspecificosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEspecificos, BtEspecificos);
end;

procedure TFmSelfGer2.PMMenuPopup(Sender: TObject);
begin
  if TDmLct2(FModuleLctX).QrLctosGenero.Value = -1 then begin
    Editar1.Enabled := True;
    Excluir1.Enabled := True;
  end else begin
    Editar1.Enabled  := False;
    Excluir1.Enabled := False;
  end;
  Compensar1.Enabled := False;
  Pagar1.Enabled     := False;
  Reverter1.Enabled  := False;
  if TDmLct2(FModuleLctX).QrLctosTipo.Value = 2 then
  begin
    if TDmLct2(FModuleLctX).QrLctosSit.Value in [0] then
      Compensar1.Enabled := True;
    if TDmLct2(FModuleLctX).QrLctosSit.Value in [0,1,2] then
      Pagar1.Enabled := True;
    if TDmLct2(FModuleLctX).QrLctosSit.Value in [3] then
      Reverter1.Enabled := True;
  end;
  if DBGLct.SelectedRows.Count > 0 then
  begin
    Mudacarteiradelanamentosselecionados1.Enabled := True;
    PagarAVista1.Enabled := True;
  end else begin
    Mudacarteiradelanamentosselecionados1.Enabled := False;
    PagarAVista1.Enabled := False;
  end;
end;

procedure TFmSelfGer2.BtRefreshClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRefresh, BtRefresh);
end;

procedure TFmSelfGer2.BtRelatClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRelat, BtRelat);
end;

procedure TFmSelfGer2.Alteratransferncia1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCtas(1, TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts,
    True, CliInt, 0, 0, 0, FTabLctA);
  TDmLct2(FModuleLctX).RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrCartsTipo.Value,
  TDmLct2(FModuleLctX).QrCartsCodigo.Value, 1, FtabLctA);
end;

procedure TFmSelfGer2.Atual5Click(Sender: TObject);
begin
  UFinanceiro.RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrCartsCodigo.Value,
    TDmLct2(FModuleLctX).QrCarts, TDmLct2(FModuleLctX).QrLctos, True, True);
end;

procedure TFmSelfGer2.Todas1Click(Sender: TObject);
begin
  AtualizaTodasCarteiras();
end;

procedure TFmSelfGer2.BtSairClick(Sender: TObject);
begin
  if TFmSelfGer2(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmSelfGer2(Self), TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmSelfGer2.BtSaldoAquiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSaldoAqui, BtSaldoAqui);
end;

procedure TFmSelfGer2.BtTrfCtaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTrfCta, BtTrfCta);
end;

procedure TFmSelfGer2.Calcula1Click(Sender: TObject);
begin
  EdSdoAqui.Text := UFinanceiro.SaldoAqui(1, EdSdoAqui.Text,
    TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts);
end;

procedure TFmSelfGer2.Limpa1Click(Sender: TObject);
begin
  EdSdoAqui.Text := UFinanceiro.SaldoAqui(0, EdSdoAqui.Text,
    TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts);
end;

procedure TFmSelfGer2.Diferena1Click(Sender: TObject);
begin
  EdSdoAqui.Text := UFinanceiro.SaldoAqui(2, EdSdoAqui.Text,
    TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts);
end;

procedure TFmSelfGer2.BtContarDinheiroClick(Sender: TObject);
begin
  UFinanceiro.MudaValorEmCaixa(
    TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts);
end;

procedure TFmSelfGer2.BtFluxoCxaClick(Sender: TObject);
begin

AvisoJanelaDeprecada(9)

{
  if DBCheck.CriaFm(TFmFluxoCxa, FmFluxoCxa, afmoNegarComAviso) then
  begin
    FmFluxoCxa.FEntCod := TDmLct2(FModuleLctX).QrCartsForneceI.Value;
    FmFluxoCxa.FConCod := 0;
    FmFluxoCxa.ShowModal;
    FmFluxoCxa.Destroy;
  end;
}
end;

procedure TFmSelfGer2.BtAutomClick(Sender: TObject);
begin
  UFinanceiro.QuitacaoAutomaticaDmk(TdmkDBGrid(DBGLct),
    TDmLct2(FModuleLctX).QrLctos, TDmLct2(FModuleLctX).QrCarts, FTabLctA);
end;

procedure TFmSelfGer2.TPDataFChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.FormDestroy(Sender: TObject);
begin
  //Application.OnHint := FmPrincipal.ShowHint;
  Application.OnHint := FmPrincipal.MyOnHint;

end;

function TFmSelfGer2.ReopenLct(Controle, Sub: Integer): Boolean;
var
  LIni, LFim: String;
begin
  Result := False;
  if Geral.IMV(EdCliIntCart.Text) = 0 then
  begin
    Geral.MB_Aviso('� necess�rio informar o dono da carteira!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    {
    if not FALiberar then
    begin
      Result := False;
      Exit;
    end;
    //
    }
    LIni := FormatDateTime(VAR_FORMATDATE, TPDataI.Date);
    LFim := FormatDateTime(VAR_FORMATDATE, TPDataF.Date);
    TDmLct2(FModuleLctX).QrLctos.Close;
    TDmLct2(FModuleLctX).QrLctos.SQL.Clear;
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('SELECT MOD(la.Mez, 100) Mes2, ');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, ');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('la.*, ct.Codigo CONTA, ca.Prazo, ca.Nome NO_Carteira, ');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('ca.Banco1, ca.Agencia1, ca.Conta1, ca.Banco BancoCar, ');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial ');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('ELSE em.Nome END NOMEEMPRESA, ');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial ');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('ELSE cl.Nome END NOMECLIENTE,');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial ');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('ELSE fo.Nome END NOMEFORNECEDOR,');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('CASE 1 WHEN fi.Tipo=0 THEN fi.RazaoSocial ');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('ELSE fi.Nome END NOMEFORNECEI,');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('ELT(la.Endossas, "Endossado", "Endossante", ');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('"Ambos", "? ? ? ?") NO_ENDOSSADO, ');
    if VAR_KIND_DEPTO = kdUH then
      TDmLct2(FModuleLctX).QrLctos.SQL.Add('cim.Unidade UH')
    else
      TDmLct2(FModuleLctX).QrLctos.SQL.Add('"" UH');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('FROM ' + FTabLctA + ' la');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
    TDmLct2(FModuleLctX).QrLctos.SQL.Add('LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI');
    if VAR_KIND_DEPTO = kdUH then
      TDmLct2(FModuleLctX).QrLctos.SQL.Add('LEFT JOIN condimov cim ON cim.Conta=la.Depto');
    //
    CompletaSQL(TDmLct2(FModuleLctX).QrLctos);
    //
    UnDmkDAC_PF.AbreQuery(TDmLct2(FModuleLctX).QrLctos, Dmod.MyDB);
    //
    if Controle > -1000 then Result := TDmLct2(FModuleLctX).QrLctos.Locate('Controle;Sub', VarArrayOf(
    [Controle, Sub]), []) else Result := False;
    if (TDmLct2(FModuleLctX).QrLctosControle.Value <> Controle) and
       (TDmLct2(FModuleLctX).QrLctosSub.Value <> Sub) then TDmLct2(FModuleLctX).QrLctos.Last;
    //////////////////////////////////////////////////////////////////////////////
    DBGLct.Visible := True;
    //
    ReopenSomaLinhas;
    //
    VerificaBotoes();
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelfGer2.ReopenSomaLinhas();
var
  LIni, LFim: String;
begin
  //if not FALiberar then Exit;
  //
  LIni := FormatDateTime(VAR_FORMATDATE, TPDataI.Date);
  LFim := FormatDateTime(VAR_FORMATDATE, TPDataF.Date);
  TDmLct2(FModuleLctX).QrSomaLinhas.Close;
  TDmLct2(FModuleLctX).QrSomaLinhas.SQL.Clear;
  TDmLct2(FModuleLctX).QrSomaLinhas.SQL.Add('SELECT SUM(la.Credito) CREDITO, SUM(la.Debito) DEBITO');
  TDmLct2(FModuleLctX).QrSomaLinhas.SQL.Add('FROM ' + FTabLctA + ' la');
  TDmLct2(FModuleLctX).QrSomaLinhas.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  TDmLct2(FModuleLctX).QrSomaLinhas.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  TDmLct2(FModuleLctX).QrSomaLinhas.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  TDmLct2(FModuleLctX).QrSomaLinhas.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  TDmLct2(FModuleLctX).QrSomaLinhas.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  TDmLct2(FModuleLctX).QrSomaLinhas.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  TDmLct2(FModuleLctX).QrSomaLinhas.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  TDmLct2(FModuleLctX).QrSomaLinhas.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  TDmLct2(FModuleLctX).QrSomaLinhas.SQL.Add('LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI');
  //
  CompletaSQL(TDmLct2(FModuleLctX).QrSomaLinhas);
  //
  UnDmkDAC_PF.AbreQuery(TDmLct2(FModuleLctX).QrSomaLinhas, Dmod.MyDB);
end;

procedure TFmSelfGer2.CompletaSQL(Query: TmySQLQuery);
begin
  Query.SQL.Add('WHERE ');
  case RGValores.ItemIndex of
    0: Query.SQL.Add('la.Credito>0');
    1: Query.SQL.Add('la.Debito>0');
    2: Query.SQL.Add('la.Controle>-1000000');
  end;
  //
  //if EdCliIntCart.ValueVariant <> 0 then
    Query.SQL.Add('AND ca.ForneceI=' +
    dmkPF.FFP(QrDonosCartForneceI.Value, 0));
  //
  if EdCliIntLancto.ValueVariant <> 0 then
    Query.SQL.Add('AND la.CliInt=' +
    dmkPF.FFP(QrDonosLctoCodigo.Value, 0));
  //
  Query.SQL.Add(dmkPF.SQL_Periodo(' AND la.Data ',
    TPDataI.Date, TPDataF.Date, CkDataI.Checked, CkDataF.Checked));
  //
  Query.SQL.Add(dmkPF.SQL_Periodo(' AND la.Vencimento ',
    TPVctoI.Date, TPVctoF.Date, CkVctoI.Checked, CkVctoF.Checked));
  //
  Query.SQL.Add(dmkPF.SQL_Competencia_Mensal(' AND la.Mez ', EdMezIni.ValueVariant,
    EdMezFim.ValueVariant, CkMezIni.Checked, CkMezFim.Checked));
  //
  if EdCarteira.ValueVariant <> 0 then
    Query.SQL.Add('AND la.Carteira=' +
    dmkPF.FFP(EdCarteira.ValueVariant, 0));
  //
  if EdCliente.ValueVariant <> 0 then
    Query.SQL.Add('AND la.Cliente=' +
    dmkPF.FFP(EdCliente.ValueVariant, 0));
  //
  if EdFornecedor.ValueVariant <> 0 then
    Query.SQL.Add('AND la.Fornecedor=' +
    dmkPF.FFP(EdFornecedor.ValueVariant, 0));
  //
  if EdConta.ValueVariant <> 0 then
    Query.SQL.Add('AND la.Genero=' +
    dmkPF.FFP(EdConta.ValueVariant, 0));
  //
  {
  case RGOrdem.ItemIndex of
    1: Query.SQL.Add('ORDER BY la.Controle, la.Sub');
    2: Query.SQL.Add('ORDER BY la.Vencimento, la.Data, la.Controle, la.Sub');
    3: Query.SQL.Add('ORDER BY la.Duplicata, la.Vencimento, la.Data, la.Controle, la.Sub');
    else Query.SQL.Add('ORDER BY la.Data, la.Controle');
  end;
  }
end;

end.

