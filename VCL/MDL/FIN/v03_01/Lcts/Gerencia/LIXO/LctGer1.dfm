object FmLctGer1: TFmLctGer1
  Left = 339
  Top = 185
  Caption = 'FIN-SELFG-001 :: Finan'#231'as'
  ClientHeight = 614
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 496
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnLct: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 496
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Shape1: TShape
        Left = 492
        Top = 16
        Width = 65
        Height = 57
        Brush.Color = clBlue
        Visible = False
      end
      object Label14: TLabel
        Left = 492
        Top = 76
        Width = 65
        Height = 13
        Caption = '{                   }'
        Visible = False
      end
      object DBGLct: TdmkDBGridZTO
        Left = 0
        Top = 297
        Width = 1008
        Height = 159
        Align = alClient
        DataSource = DmLct0.DsLct
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        OnDrawColumnCell = DBGLctDrawColumnCell
        OnDblClick = DBGLctDblClick
        OnKeyDown = DBGLctKeyDown
        FieldsCalcToOrder.Strings = (
          'NOMESIT=Sit,Vencimento'
          'SERIE_CHEQUE=SerieCH,Documento'
          'COMPENSADO_TXT=Compensado'
          'MENSAL=Mez')
        Columns = <
          item
            Expanded = False
            FieldName = 'Data'
            Width = 51
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SerieCH'
            Title.Caption = 'S'#233'rie'
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Documento'
            Title.Caption = 'CH / Docum.'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Duplicata'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Alignment = taRightJustify
            Title.Caption = 'Quantidade'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descri'#231#227'o'
            Width = 164
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Debito'
            Title.Caption = 'D'#233'bito'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Credito'
            Title.Caption = 'Cr'#233'dito'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Vencimento'
            Title.Caption = 'Vencim.'
            Width = 51
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COMPENSADO_TXT'
            Title.Caption = 'Compen.'
            Width = 51
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MENSAL'
            Title.Caption = 'M'#234's'
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMESIT'
            Title.Caption = 'Situa'#231#227'o'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'Lan'#231'to'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Sub'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SALDO'
            Title.Caption = 'Saldo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NotaFiscal'
            Title.Caption = 'Nota Fiscal'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMERELACIONADO'
            Title.Caption = 'Terceiro'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UH'
            Width = 132
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MultaVal'
            Title.Caption = 'Multa paga'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MoraVal'
            Title.Caption = 'Juros pagos'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DescoVal'
            Title.Caption = 'Desc. Rece.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'USERCAD_TXT'
            Title.Caption = 'Cadastrado por'
            Width = 130
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATACAD_TXT'
            Title.Caption = 'Data Cadastro'
            Width = 51
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'USERALT_TXT'
            Title.Caption = 'Alterado por'
            Width = 130
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATAALT_TXT'
            Title.Caption = 'Data Altera'#231#227'o'
            Width = 51
            Visible = True
          end>
      end
      object PainelDados2: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 297
        Align = alTop
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 797
          Height = 297
          Align = alLeft
          BevelOuter = bvNone
          Caption = 'Panel3'
          TabOrder = 0
          object Panel4: TPanel
            Left = 0
            Top = 49
            Width = 797
            Height = 248
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 797
              Height = 57
              Align = alTop
              Caption = ' Plano de contas '
              TabOrder = 0
              object Panel9: TPanel
                Left = 2
                Top = 15
                Width = 793
                Height = 40
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label2: TLabel
                  Left = 4
                  Top = -1
                  Width = 31
                  Height = 13
                  Caption = 'Conta:'
                end
                object Label3: TLabel
                  Left = 252
                  Top = -1
                  Width = 52
                  Height = 13
                  Caption = 'Sub-grupo:'
                end
                object Label4: TLabel
                  Left = 432
                  Top = -1
                  Width = 32
                  Height = 13
                  Caption = 'Grupo:'
                end
                object Label7: TLabel
                  Left = 612
                  Top = -1
                  Width = 45
                  Height = 13
                  Caption = 'Conjunto:'
                end
                object DBEdit1: TDBEdit
                  Left = 4
                  Top = 13
                  Width = 244
                  Height = 21
                  DataField = 'NOMECONTA'
                  DataSource = DmLct0.DsLct
                  TabOrder = 0
                end
                object DBEdit2: TDBEdit
                  Left = 252
                  Top = 13
                  Width = 176
                  Height = 21
                  DataField = 'NOMESUBGRUPO'
                  DataSource = DmLct0.DsLct
                  TabOrder = 1
                end
                object DBEdit3: TDBEdit
                  Left = 432
                  Top = 13
                  Width = 176
                  Height = 21
                  DataField = 'NOMEGRUPO'
                  DataSource = DmLct0.DsLct
                  TabOrder = 2
                end
                object DBEdit4: TDBEdit
                  Left = 612
                  Top = 13
                  Width = 176
                  Height = 21
                  DataField = 'NOMECONJUNTO'
                  DataSource = DmLct0.DsLct
                  TabOrder = 3
                end
              end
            end
            object Panel10: TPanel
              Left = 660
              Top = 121
              Width = 137
              Height = 127
              Align = alRight
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object GroupBox5: TGroupBox
                Left = 0
                Top = 0
                Width = 137
                Height = 127
                Align = alClient
                Caption = ' Per'#237'odo de visualiza'#231#227'o:'
                TabOrder = 0
                object Label8: TLabel
                  Left = 4
                  Top = 80
                  Width = 13
                  Height = 13
                  Caption = 'ini:'
                end
                object Label5: TLabel
                  Left = 4
                  Top = 104
                  Width = 16
                  Height = 13
                  Caption = 'fim:'
                end
                object TPDataIni: TdmkEditDateTimePicker
                  Left = 24
                  Top = 78
                  Width = 106
                  Height = 21
                  Date = 37617.122587314800000000
                  Time = 37617.122587314800000000
                  TabOrder = 0
                  OnClick = TPDataIniClick
                  OnChange = TPDataIniChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                end
                object TPDataFim: TdmkEditDateTimePicker
                  Left = 24
                  Top = 102
                  Width = 106
                  Height = 21
                  Date = 37617.122587314800000000
                  Time = 37617.122587314800000000
                  TabOrder = 1
                  OnClick = TPDataFimClick
                  OnChange = TPDataFimChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                end
                object RGTipoData: TRadioGroup
                  Left = 2
                  Top = 15
                  Width = 133
                  Height = 62
                  Align = alTop
                  Caption = ' Per'#237'odo: '
                  ItemIndex = 1
                  Items.Strings = (
                    'Emiss'#227'o'
                    'Vencimento'
                    'Compensado')
                  TabOrder = 2
                  OnClick = RGTipoDataClick
                end
              end
            end
            object GBAcoes: TGroupBox
              Left = 0
              Top = 57
              Width = 797
              Height = 64
              Align = alTop
              Caption = 
                ' A'#231#245'es em lan'#231'amentos: (h'#225' restri'#231#245'es para carteiras negativas!)' +
                ' '
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 2
              object PnAcoesTravaveis: TPanel
                Left = 2
                Top = 15
                Width = 785
                Height = 47
                Align = alLeft
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object BtMenu: TBitBtn
                  Tag = 237
                  Left = 4
                  Top = 4
                  Width = 92
                  Height = 40
                  Caption = '&Menu'
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtMenuClick
                end
                object BtInclui: TBitBtn
                  Tag = 10
                  Left = 96
                  Top = 4
                  Width = 92
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Inclui novo banco'
                  Caption = '&Inclui'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BtIncluiClick
                end
                object BtAltera: TBitBtn
                  Tag = 11
                  Left = 188
                  Top = 4
                  Width = 92
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Altera banco atual'
                  Caption = '&Altera'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                  OnClick = BtAlteraClick
                end
                object BtExclui: TBitBtn
                  Tag = 12
                  Left = 280
                  Top = 4
                  Width = 92
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Exclui banco atual'
                  Caption = 'Excl&ui'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 3
                  OnClick = BtExcluiClick
                end
                object BtQuita: TBitBtn
                  Tag = 10024
                  Left = 464
                  Top = 4
                  Width = 92
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Quita'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 4
                  OnClick = BtQuitaClick
                end
                object BtDuplica: TBitBtn
                  Tag = 56
                  Left = 372
                  Top = 4
                  Width = 92
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Exclui banco atual'
                  Caption = '&Duplica'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 5
                  OnClick = BtDuplicaClick
                end
                object BtConcilia: TBitBtn
                  Tag = 10011
                  Left = 596
                  Top = 4
                  Width = 92
                  Height = 40
                  Cursor = crHandPoint
                  Caption = 'Concilia'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 6
                  OnClick = BtConciliaClick
                end
                object BtTrfCta: TBitBtn
                  Tag = 330
                  Left = 688
                  Top = 4
                  Width = 92
                  Height = 40
                  Cursor = crHandPoint
                  Caption = 'Transf.'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 7
                  OnClick = BtTrfCtaClick
                end
                object BtAutom: TBitBtn
                  Tag = 174
                  Left = 556
                  Top = 4
                  Width = 40
                  Height = 40
                  Cursor = crHandPoint
                  NumGlyphs = 2
                  TabOrder = 8
                  OnClick = BtAutomClick
                end
              end
            end
            object Panel15: TPanel
              Left = 0
              Top = 121
              Width = 660
              Height = 127
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 3
              object GroupBox7: TGroupBox
                Left = 0
                Top = 0
                Width = 660
                Height = 64
                Align = alTop
                Caption = ' Outras a'#231#245'es / impress'#245'es: '
                TabOrder = 0
                object Panel12: TPanel
                  Left = 2
                  Top = 15
                  Width = 656
                  Height = 47
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object BtPlaCtas: TBitBtn
                    Tag = 323
                    Left = 4
                    Top = 3
                    Width = 40
                    Height = 40
                    NumGlyphs = 2
                    TabOrder = 0
                    OnClick = BtPlaCtasClick
                  end
                  object BtRecibo: TBitBtn
                    Tag = 199
                    Left = 45
                    Top = 3
                    Width = 95
                    Height = 40
                    Caption = '&Recibo'
                    TabOrder = 1
                    OnClick = BtReciboClick
                  end
                  object BtCheque: TBitBtn
                    Tag = 10042
                    Left = 141
                    Top = 3
                    Width = 95
                    Height = 40
                    Caption = 'Cheque(s)'
                    TabOrder = 2
                    OnClick = BtChequeClick
                  end
                  object BtSaldoTotal: TBitBtn
                    Tag = 238
                    Left = 237
                    Top = 3
                    Width = 95
                    Height = 40
                    Caption = 'Sdo Total'
                    TabOrder = 3
                    OnClick = BtSaldoTotalClick
                  end
                  object BtConfPagtosCad: TBitBtn
                    Tag = 316
                    Left = 413
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 6
                    OnClick = BtConfPagtosCadClick
                  end
                  object BtConfPgtosExe: TBitBtn
                    Tag = 318
                    Left = 453
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 7
                    OnClick = BtConfPgtosExeClick
                  end
                  object BtRefresh: TBitBtn
                    Tag = 18
                    Left = 493
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 8
                    OnClick = BtRefreshClick
                  end
                  object BtDesfazOrdenacao: TBitBtn
                    Tag = 329
                    Left = 533
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 9
                    OnClick = BtDesfazOrdenacaoClick
                  end
                  object BitBtn1: TBitBtn
                    Tag = 22
                    Left = 373
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 5
                    OnClick = BitBtn1Click
                  end
                  object BtBloqueto: TBitBtn
                    Left = 573
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 10
                    OnClick = BtBloquetoClick
                  end
                  object BtProtocolo: TBitBtn
                    Tag = 10051
                    Left = 333
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 4
                    OnClick = BtProtocoloClick
                  end
                  object BtSalvaPosicaoGrade: TBitBtn
                    Tag = 10007
                    Left = 613
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 11
                    OnClick = BtSalvaPosicaoGradeClick
                  end
                end
              end
              object GroupBox8: TGroupBox
                Left = 0
                Top = 64
                Width = 660
                Height = 64
                Align = alTop
                Caption = ' Relat'#243'rios e pesquisas: '
                TabOrder = 1
                object Panel13: TPanel
                  Left = 2
                  Top = 15
                  Width = 656
                  Height = 47
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object BtBalancete: TBitBtn
                    Tag = 360
                    Left = 4
                    Top = 3
                    Width = 92
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'Balancete'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtBalanceteClick
                  end
                  object BtPesquisas: TBitBtn
                    Tag = 22
                    Left = 190
                    Top = 3
                    Width = 92
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'Pesquisas'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 1
                    OnClick = BtPesquisasClick
                  end
                  object BtSaldos: TBitBtn
                    Tag = 326
                    Left = 283
                    Top = 3
                    Width = 92
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'Saldos'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 2
                    OnClick = BtSaldosClick
                  end
                  object BtListas: TBitBtn
                    Tag = 363
                    Left = 376
                    Top = 3
                    Width = 92
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'Listas'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 3
                    OnClick = BtListasClick
                  end
                  object BtDiversos: TBitBtn
                    Tag = 10096
                    Left = 469
                    Top = 3
                    Width = 92
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'Diversos'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 4
                    OnClick = BtDiversosClick
                  end
                  object BitBtn6: TBitBtn
                    Tag = 10097
                    Left = 561
                    Top = 3
                    Width = 92
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'Eventos'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 5
                    OnClick = BitBtn6Click
                  end
                  object BtExtratos: TBitBtn
                    Tag = 366
                    Left = 97
                    Top = 3
                    Width = 92
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'Extratos'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 6
                    OnClick = BtExtratosClick
                  end
                end
              end
            end
          end
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 797
            Height = 49
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Label6: TLabel
              Left = 4
              Top = 5
              Width = 39
              Height = 13
              Caption = 'Carteira:'
            end
            object Label1: TLabel
              Left = 492
              Top = 4
              Width = 42
              Height = 13
              Caption = 'Terceiro:'
            end
            object EdCodigo: TDBEdit
              Left = 4
              Top = 20
              Width = 44
              Height = 24
              TabStop = False
              DataField = 'Codigo'
              DataSource = DmLct0.DsCrt
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlue
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
            end
            object EdNome: TDBEdit
              Left = 48
              Top = 20
              Width = 440
              Height = 24
              TabStop = False
              DataField = 'Nome'
              DataSource = DmLct0.DsCrt
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlue
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              ReadOnly = True
              TabOrder = 1
            end
            object DBEdit5: TDBEdit
              Left = 492
              Top = 20
              Width = 296
              Height = 22
              DataField = 'NOMERELACIONADO'
              DataSource = DmLct0.DsLct
              Font.Charset = ANSI_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
            end
          end
        end
        object Panel7: TPanel
          Left = 797
          Top = 0
          Width = 211
          Height = 297
          Align = alClient
          ParentBackground = False
          TabOrder = 1
          object PageControl1: TPageControl
            Left = 1
            Top = 1
            Width = 209
            Height = 295
            ActivePage = TabSheet1
            Align = alClient
            TabOrder = 0
            object TabSheet1: TTabSheet
              Caption = ' Carteiras '
              object DBGCrt: TdmkDBGridZTO
                Left = 0
                Top = 0
                Width = 201
                Height = 267
                Align = alClient
                DataSource = DmLct0.DsCrt
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'C'#243'digo'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 265
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EmCaixa'
                    Title.Alignment = taRightJustify
                    Title.Caption = 'Em caixa'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Saldo'
                    Title.Alignment = taRightJustify
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DIFERENCA'
                    Title.Caption = 'Diferen'#231'a'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEDOBANCO'
                    Title.Caption = 'Banco '
                    Width = 198
                    Visible = True
                  end>
              end
            end
            object TabSheet2: TTabSheet
              Caption = ' Soma linhas '
              ImageIndex = 1
              object Panel8: TPanel
                Left = 0
                Top = 0
                Width = 201
                Height = 267
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label20: TLabel
                  Left = 49
                  Top = 6
                  Width = 80
                  Height = 13
                  Caption = 'Soma das linhas:'
                end
                object Label15: TLabel
                  Left = 49
                  Top = 46
                  Width = 90
                  Height = 13
                  Caption = 'Soma dos cr'#233'ditos:'
                end
                object Label16: TLabel
                  Left = 49
                  Top = 86
                  Width = 84
                  Height = 13
                  Caption = 'Soma dos d'#233'bitos'
                end
                object EdSoma: TdmkEdit
                  Left = 48
                  Top = 20
                  Width = 100
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                end
                object BtSomaLinhas: TBitBtn
                  Left = 1
                  Top = 4
                  Width = 40
                  Height = 40
                  Cursor = crHandPoint
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BtSomaLinhasClick
                end
                object EdCred: TdmkEdit
                  Left = 48
                  Top = 60
                  Width = 100
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                end
                object EdDebi: TdmkEdit
                  Left = 48
                  Top = 100
                  Width = 100
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                end
              end
            end
            object TabSheet3: TTabSheet
              Caption = ' Saldo '
              ImageIndex = 2
              object Panel14: TPanel
                Left = 0
                Top = 0
                Width = 201
                Height = 267
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object GroupBox1: TGroupBox
                  Left = 0
                  Top = 203
                  Width = 201
                  Height = 64
                  Align = alBottom
                  Caption = ' Saldo aqui: '
                  TabOrder = 0
                  object BtSaldoAqui: TBitBtn
                    Tag = 238
                    Left = 7
                    Top = 20
                    Width = 40
                    Height = 40
                    TabOrder = 0
                    OnClick = BtSaldoAquiClick
                  end
                  object EdSdoAqui: TdmkEdit
                    Left = 50
                    Top = 28
                    Width = 108
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                  end
                end
                object GroupBox2: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 201
                  Height = 203
                  Align = alClient
                  Caption = ' Saldo em caixa: '
                  TabOrder = 1
                  object LaDiferenca: TLabel
                    Left = 88
                    Top = 65
                    Width = 49
                    Height = 13
                    Caption = 'Diferen'#231'a:'
                  end
                  object LaSaldo: TLabel
                    Left = 12
                    Top = 25
                    Width = 30
                    Height = 13
                    Caption = 'Saldo:'
                  end
                  object LaCaixa: TLabel
                    Left = 12
                    Top = 65
                    Width = 46
                    Height = 13
                    Caption = 'Em caixa:'
                  end
                  object EdSaldo: TDBEdit
                    Left = 12
                    Top = 40
                    Width = 105
                    Height = 21
                    TabStop = False
                    Color = clBtnFace
                    DataField = 'Saldo'
                    DataSource = DmLct0.DsCrt
                    Enabled = False
                    TabOrder = 0
                  end
                  object EdDiferenca: TDBEdit
                    Left = 88
                    Top = 80
                    Width = 72
                    Height = 21
                    TabStop = False
                    Color = clBtnFace
                    DataField = 'DIFERENCA'
                    DataSource = DmLct0.DsCrt
                    Enabled = False
                    TabOrder = 1
                  end
                  object EdCaixa: TDBEdit
                    Left = 12
                    Top = 80
                    Width = 72
                    Height = 21
                    Cursor = crHandPoint
                    Color = clWhite
                    DataField = 'EmCaixa'
                    DataSource = DmLct0.DsCrt
                    TabOrder = 2
                  end
                  object BtContarDinheiro: TBitBtn
                    Tag = 239
                    Left = 121
                    Top = 20
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 3
                    OnClick = BtContarDinheiroClick
                  end
                end
              end
            end
          end
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 456
        Width = 1008
        Height = 40
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        Visible = False
        object Label9: TLabel
          Left = 4
          Top = 2
          Width = 62
          Height = 13
          Caption = 'Lan'#231'amento:'
          FocusControl = DBEdit6
        end
        object Label10: TLabel
          Left = 80
          Top = 2
          Width = 17
          Height = 13
          Caption = 'NF:'
          FocusControl = DBEdit7
        end
        object Label11: TLabel
          Left = 148
          Top = 2
          Width = 57
          Height = 13
          Caption = 'Fornecedor:'
          FocusControl = DBEdit8
        end
        object Label13: TLabel
          Left = 492
          Top = 2
          Width = 69
          Height = 13
          Caption = 'Transportador:'
          FocusControl = DBEdit10
        end
        object Label12: TLabel
          Left = 792
          Top = 2
          Width = 39
          Height = 13
          Caption = 'Parcela:'
          FocusControl = DBEdit12
        end
        object DBEdit6: TDBEdit
          Left = 4
          Top = 16
          Width = 73
          Height = 21
          TabStop = False
          DataField = 'CODIGO'
          ReadOnly = True
          TabOrder = 0
        end
        object DBEdit7: TDBEdit
          Left = 80
          Top = 15
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'NF'
          ReadOnly = True
          TabOrder = 1
        end
        object DBEdit8: TDBEdit
          Left = 148
          Top = 15
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'FORNECEDOR'
          ReadOnly = True
          TabOrder = 2
        end
        object DBEdit9: TDBEdit
          Left = 216
          Top = 15
          Width = 273
          Height = 21
          TabStop = False
          DataField = 'NOMEFORNECEDOR'
          ReadOnly = True
          TabOrder = 3
        end
        object DBEdit10: TDBEdit
          Left = 492
          Top = 15
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'FORNECEDOR'
          ReadOnly = True
          TabOrder = 4
        end
        object DBEdit11: TDBEdit
          Left = 560
          Top = 15
          Width = 225
          Height = 21
          TabStop = False
          DataField = 'NOMETRANSPORTADOR'
          ReadOnly = True
          TabOrder = 5
        end
        object DBEdit12: TDBEdit
          Left = 792
          Top = 15
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'FatParcela'
          ReadOnly = True
          TabOrder = 6
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 111
        Height = 32
        Caption = 'Finan'#231'as'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 111
        Height = 32
        Caption = 'Finan'#231'as'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 111
        Height = 32
        Caption = 'Finan'#231'as'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 544
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object GBAvisos1: TGroupBox
      Left = 133
      Top = 15
      Width = 729
      Height = 53
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 1
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 725
        Height = 36
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 2
      Top = 15
      Width = 131
      Height = 53
      Align = alLeft
      Caption = 'Localiza lan'#231'amento...'
      TabOrder = 2
      object Label17: TLabel
        Left = 8
        Top = 12
        Width = 76
        Height = 13
        Caption = '... pelo controle:'
      end
      object BtLocCtrl: TBitBtn
        Left = 104
        Top = 28
        Width = 21
        Height = 21
        Caption = '>'
        TabOrder = 0
        OnClick = BtLocCtrlClick
      end
      object EdLocCtrl: TdmkEditCB
        Left = 8
        Top = 28
        Width = 93
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        IgnoraDBLookupComboBox = False
      end
    end
  end
  object PMTrfCta: TPopupMenu
    Left = 716
    Top = 172
    object Entrecarteitas1: TMenuItem
      Caption = 'Entre c&arteitas'
      object Novatransferncia1: TMenuItem
        Caption = '&Nova transfer'#234'ncia entre carteiras'
        OnClick = Novatransferncia1Click
      end
      object Alteratransferncia1: TMenuItem
        Caption = '&Altera transfer'#234'ncia  entre carteiras'
        OnClick = Alteratransferncia1Click
      end
      object Excluitransferncia1: TMenuItem
        Caption = '&Exclui transfer'#234'ncia entre carteiras'
        OnClick = Excluitransferncia1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Localizatransferncia1: TMenuItem
        Caption = '&Localiza transfer'#234'ncia entre carteiras'
        OnClick = Localizatransferncia1Click
      end
    end
    object Entrecontas1: TMenuItem
      Caption = 'Entre c&ontas'
      object Novatransfernciaentrecontas1: TMenuItem
        Caption = '&Nova transfer'#234'ncia entre contas'
        OnClick = Novatransfernciaentrecontas1Click
      end
      object Alteratransfernciaentrecontas1: TMenuItem
        Caption = '&Altera transfer'#234'ncia entre contas'
        OnClick = Alteratransfernciaentrecontas1Click
      end
      object Excluitransfernciaentrecontas1: TMenuItem
        Caption = '&Exclui transfer'#234'ncia entre contas'
        OnClick = Excluitransfernciaentrecontas1Click
      end
    end
  end
  object PMSaldoAqui: TPopupMenu
    Left = 820
    Top = 232
    object Calcula1: TMenuItem
      Caption = '&Calcula'
      OnClick = Calcula1Click
    end
    object Limpa1: TMenuItem
      Caption = '&Limpa'
      OnClick = Limpa1Click
    end
    object Diferena1: TMenuItem
      Caption = '&Diferen'#231'a'
      OnClick = Diferena1Click
    end
  end
  object PMMenu: TPopupMenu
    OnPopup = PMMenuPopup
    Left = 72
    Top = 160
    object Transferir1: TMenuItem
      Caption = '&Transfer'#234'ncia'
      object Entrecarteiras1: TMenuItem
        Caption = 'Entre c&arteiras'
        object Incluinovatransfernciaentrecarteiras1: TMenuItem
          Caption = '&Inclui nova transfer'#234'ncia entre carteiras'
          OnClick = Incluinovatransfernciaentrecarteiras1Click
        end
        object Alteratransfernciaentrecarteirasatual1: TMenuItem
          Caption = '&Altera transfer'#234'ncia entre carteiras atual'
          OnClick = Alteratransfernciaentrecarteirasatual1Click
        end
        object Excluitransfernciaentrecarteirasatual1: TMenuItem
          Caption = '&Exclui transfer'#234'ncia entre carteiras atual'
          OnClick = Excluitransfernciaentrecarteirasatual1Click
        end
        object N2: TMenuItem
          Caption = '-'
        end
        object Localizatransfernciaentrecarteiras1: TMenuItem
          Caption = 'Localiza transfer'#234'ncia entre carteiras'
          OnClick = Localizatransfernciaentrecarteiras1Click
        end
      end
      object Entrecontas2: TMenuItem
        Caption = 'Entre c&ontas'
        object Incluinovatransfernciaentrecontas1: TMenuItem
          Caption = '&Inclui nova transfer'#234'ncia entre contas'
          OnClick = Incluinovatransfernciaentrecontas1Click
        end
        object Alteratransfernciaentrecontasatual1: TMenuItem
          Caption = '&Altera transfer'#234'ncia entre contas atual'
          OnClick = Alteratransfernciaentrecontasatual1Click
        end
        object Excluitransfernciaentrecontasatual1: TMenuItem
          Caption = '&Exclui transfer'#234'ncia entre contas atual'
          OnClick = Excluitransfernciaentrecontasatual1Click
        end
      end
    end
    object Quitar1: TMenuItem
      Caption = '&Quita'#231#227'o'
      object Compensar1: TMenuItem
        Caption = '&Compensar na conta corrente (Banco)'
        Enabled = False
        OnClick = Compensar1Click
      end
      object Reverter1: TMenuItem
        Caption = 'D&esfazer compensa'#231#227'o'
        Enabled = False
        OnClick = Reverter1Click
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object Pagar1: TMenuItem
        Caption = '&Pagar / Rolar com emiss'#227'o  (cheque, etc)'
        Enabled = False
        OnClick = Pagar1Click
      end
      object N11: TMenuItem
        Caption = '-'
      end
      object PagarAVista1: TMenuItem
        Caption = 'Pagar '#224' vista para um caixa'
        OnClick = PagarAVista1Click
      end
      object N19: TMenuItem
        Caption = '-'
      end
      object Localizarlanamentoorigem1: TMenuItem
        Caption = 'Localizar lan'#231'amento origem'
        OnClick = Localizarlanamentoorigem1Click
      end
    end
    object Lanamento1: TMenuItem
      Caption = '&Lan'#231'amento'
      object Localizar2: TMenuItem
        Caption = '&Localizar'
        OnClick = Localizar2Click
      end
      object Copiar1: TMenuItem
        Caption = '&Copiar'
        OnClick = Copiar1Click
      end
    end
    object Recibo1: TMenuItem
      Caption = '&Recibo'
      OnClick = Recibo1Click
    end
    object Mudacarteiradelanamentosselecionados1: TMenuItem
      Caption = '&Muda lan'#231'amentos selecionados'
      Enabled = False
      object Carteiras1: TMenuItem
        Caption = '&Carteira'
        OnClick = Carteiras1Click
      end
      object Data1: TMenuItem
        Caption = '&Data'
        OnClick = Data1Click
      end
      object Compensao1: TMenuItem
        Caption = '&Compensa'#231#227'o'
        OnClick = Compensao1Click
      end
      object Transformaremitemdebloqueto1: TMenuItem
        Caption = '&Transformar em item de bloqueto'
        OnClick = Transformaremitemdebloqueto1Click
      end
      object N15: TMenuItem
        Caption = '-'
      end
      object Acertarcreditopelovalorpago1: TMenuItem
        Caption = '&Acertar credito pelo valor pago (juro / multa negativos)'
        OnClick = Acertarcreditopelovalorpago1Click
      end
    end
    object Colocarmsdecompetnciaondenotem1: TMenuItem
      Caption = 'Colocar m'#234's de compet'#234'ncia onde n'#227'o tem'
      object Datalancto1: TMenuItem
        Caption = '&Data lancto'
        OnClick = Datalancto1Click
      end
      object Vencimento1: TMenuItem
        Caption = '&Vencimento'
        OnClick = Vencimento1Click
      end
      object MsanterioraoVencimento1: TMenuItem
        Caption = '&M'#234's anterior ao Vencimento '
        OnClick = MsanterioraoVencimento1Click
      end
    end
    object N18: TMenuItem
      Caption = '-'
    end
    object Exclusoincondicional1: TMenuItem
      Caption = 'Exclus'#227'o incondicional'
      OnClick = Exclusoincondicional1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Ferramentas1: TMenuItem
      Caption = '&Ferramentas'
      object Atualizalanamentos1: TMenuItem
        Caption = '&Atualiza lan'#231'amentos'
        OnClick = Atualizalanamentos1Click
      end
    end
  end
  object PMQuita: TPopupMenu
    OnPopup = PMQuitaPopup
    Left = 489
    Top = 174
    object Compensar2: TMenuItem
      Caption = '&Compensar na conta corrente (Banco)'
      OnClick = Compensar2Click
    end
    object Reverter2: TMenuItem
      Caption = 'D&esfazer compensa'#231#227'o'
      OnClick = Reverter2Click
    end
    object N12: TMenuItem
      Caption = '-'
    end
    object Pagar2: TMenuItem
      Caption = '&Pagar / Rolar com emiss'#227'o  (cheque, etc)'
      OnClick = Pagar2Click
    end
    object N13: TMenuItem
      Caption = '-'
    end
    object PagarAVista2: TMenuItem
      Caption = 'Pagar em carteira para um caixa'
      OnClick = PagarAVista2Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Localizarlanamentoorigem2: TMenuItem
      Caption = 'Localizar lan'#231'amento origem / quita'#231#227'o'
      OnClick = Localizarlanamentoorigem2Click
    end
  end
  object PMExtratos: TPopupMenu
    Left = 156
    Top = 300
    object PagarReceber1: TMenuItem
      Caption = '&Pagar/Receber'
      OnClick = PagarReceber1Click
    end
    object Movimento1: TMenuItem
      Caption = '&Movimento'
      OnClick = Movimento1Click
    end
    object ResultadosMensais1: TMenuItem
      Caption = '&Resultados Mensais'
      OnClick = ResultadosMensais1Click
    end
    object Cheques1: TMenuItem
      Caption = '&Cheques'
      OnClick = Cheques1Click
    end
  end
  object PMPesquisas: TPopupMenu
    Left = 228
    Top = 280
    object PorNveldoPLanodeContas1: TMenuItem
      Caption = '&Por N'#237'vel do PLano de Contas'
      OnClick = PorNveldoPLanodeContas1Click
    end
    object ContasControladas1: TMenuItem
      Caption = '&Contas Controladas'
      OnClick = ContasControladas1Click
    end
    object Contassazonais1: TMenuItem
      Caption = 'Contas sa&zonais'
      OnClick = Contassazonais1Click
    end
  end
  object PMSaldos: TPopupMenu
    Left = 316
    Top = 296
    object Futuros1: TMenuItem
      Caption = '&Futuros'
      OnClick = Futuros1Click
    end
    object Em1: TMenuItem
      Caption = '&Em....'
      OnClick = Em1Click
    end
  end
  object PMListas: TPopupMenu
    Left = 408
    Top = 284
    object Contas1: TMenuItem
      Caption = '&Contas'
      OnClick = Contas1Click
    end
    object Entidades1: TMenuItem
      Caption = '&Entidades'
    end
  end
  object PMDiversos: TPopupMenu
    Left = 504
    Top = 288
    object Etiquetas1: TMenuItem
      Caption = '&Etiquetas'
      OnClick = Etiquetas1Click
    end
    object Formulrios1: TMenuItem
      Caption = '&Formul'#225'rios'
      OnClick = Formulrios1Click
    end
    object Mensalidades1: TMenuItem
      Caption = '&Mensalidades'
      OnClick = Mensalidades1Click
    end
  end
  object PMConfPagtosCad: TPopupMenu
    Left = 423
    Top = 223
    object ContasMensaisCadastrodePagamentos1: TMenuItem
      Caption = 'Contas Mensais - Cadastro de &Pagamentos'
      OnClick = ContasMensaisCadastrodePagamentos1Click
    end
    object ContasMensaisCadastrodeEmisses1: TMenuItem
      Caption = 'Contas Mensais - Cadastro de &Emiss'#245'es'
      OnClick = ContasMensaisCadastrodeEmisses1Click
    end
  end
  object PMConfPgtosExe: TPopupMenu
    Left = 465
    Top = 222
    object Pagamentosexecutados1: TMenuItem
      Caption = 'Contas Mensais - Confer'#234'ncia de &Pagamentos'
      OnClick = Pagamentosexecutados1Click
    end
    object Contasmensais1: TMenuItem
      Caption = 'Contas Mensais - &Confer'#234'ncia de Emiss'#245'es'
      OnClick = Contasmensais1Click
    end
  end
  object PMBloqueto: TPopupMenu
    OnPopup = PMBloquetoPopup
    Left = 580
    Top = 256
    object Gerabloqueto1: TMenuItem
      Caption = '&Gera bloqueto'
      OnClick = Gerabloqueto1Click
    end
    object ImprimeGerenciabloqueto1: TMenuItem
      Caption = '&Imprime / Gerencia bloqueto'
      OnClick = ImprimeGerenciabloqueto1Click
    end
  end
  object PMProtocolo: TPopupMenu
    Left = 340
    Top = 224
    object Adicionalanamentos1: TMenuItem
      Caption = '&Adiciona lan'#231'amentos'
      object Atual4: TMenuItem
        Caption = '&Atual'
        OnClick = Atual4Click
      end
      object Selecionados5: TMenuItem
        Caption = '&Selecionados'
        object UmporDocumento1: TMenuItem
          Caption = 'Um por &Documento '
          OnClick = UmporDocumento1Click
        end
        object UmparacadaLanamento1: TMenuItem
          Caption = 'Um para cada &Lan'#231'amento'
          OnClick = UmparacadaLanamento1Click
        end
      end
    end
    object Localizalote1: TMenuItem
      Caption = '&Localiza lote'
      OnClick = Localizalote1Click
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 12
    Top = 12
  end
  object PMSalvaPosicaoGrade: TPopupMenu
    Left = 624
    Top = 256
    object SalvarOrdenaodascolunasdagrade2: TMenuItem
      Caption = '&Salvar ordena'#231#227'o das colunas da grade'
      OnClick = SalvarOrdenaodascolunasdagrade2Click
    end
    object CarregarOrdenaopadrodascolunasdagrade1: TMenuItem
      Caption = '&Carregar ordena'#231#227'o padr'#227'o das colunas da grade'
      OnClick = CarregarOrdenaopadrodascolunasdagrade1Click
    end
  end
end
