unit LctGer1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, dmkEdit,
  ComCtrls, Mask, DBCtrls, dmkDBGrid, dmkEditDateTimePicker, dmkGeral,
  UnFinanceiro, DB, mySQLDbTables, Menus, dmkEditCB, dmkImage, dmkPermissoes,
  dmkDBGridZTO, UnDmkEnums;

type
  TFmLctGer1 = class(TForm)
    Panel1: TPanel;
    PnLct: TPanel;
    Shape1: TShape;
    Label14: TLabel;
    DBGLct: TdmkDBGridZTO;
    PainelDados2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Label6: TLabel;
    EdCodigo: TDBEdit;
    EdNome: TDBEdit;
    Panel6: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    PMTrfCta: TPopupMenu;
    Novatransferncia1: TMenuItem;
    Alteratransferncia1: TMenuItem;
    Excluitransferncia1: TMenuItem;
    Localizatransferncia1: TMenuItem;
    N1: TMenuItem;
    Entrecarteitas1: TMenuItem;
    Entrecontas1: TMenuItem;
    Novatransfernciaentrecontas1: TMenuItem;
    Alteratransfernciaentrecontas1: TMenuItem;
    Excluitransfernciaentrecontas1: TMenuItem;
    PMSaldoAqui: TPopupMenu;
    Calcula1: TMenuItem;
    Limpa1: TMenuItem;
    Diferena1: TMenuItem;
    PMMenu: TPopupMenu;
    Transferir1: TMenuItem;
    Quitar1: TMenuItem;
    Compensar1: TMenuItem;
    Reverter1: TMenuItem;
    N10: TMenuItem;
    Pagar1: TMenuItem;
    N11: TMenuItem;
    PagarAVista1: TMenuItem;
    N19: TMenuItem;
    Localizarlanamentoorigem1: TMenuItem;
    Lanamento1: TMenuItem;
    Localizar2: TMenuItem;
    Copiar1: TMenuItem;
    Recibo1: TMenuItem;
    Mudacarteiradelanamentosselecionados1: TMenuItem;
    Carteiras1: TMenuItem;
    Data1: TMenuItem;
    Compensao1: TMenuItem;
    Transformaremitemdebloqueto1: TMenuItem;
    N15: TMenuItem;
    Acertarcreditopelovalorpago1: TMenuItem;
    Colocarmsdecompetnciaondenotem1: TMenuItem;
    Datalancto1: TMenuItem;
    Vencimento1: TMenuItem;
    MsanterioraoVencimento1: TMenuItem;
    N18: TMenuItem;
    Exclusoincondicional1: TMenuItem;
    Entrecarteiras1: TMenuItem;
    Entrecontas2: TMenuItem;
    Incluinovatransfernciaentrecarteiras1: TMenuItem;
    Alteratransfernciaentrecarteirasatual1: TMenuItem;
    Excluitransfernciaentrecarteirasatual1: TMenuItem;
    N2: TMenuItem;
    Localizatransfernciaentrecarteiras1: TMenuItem;
    Incluinovatransfernciaentrecontas1: TMenuItem;
    Alteratransfernciaentrecontasatual1: TMenuItem;
    Excluitransfernciaentrecontasatual1: TMenuItem;
    DBEdit5: TDBEdit;
    Label1: TLabel;
    PMQuita: TPopupMenu;
    Compensar2: TMenuItem;
    Reverter2: TMenuItem;
    N12: TMenuItem;
    Pagar2: TMenuItem;
    N13: TMenuItem;
    PagarAVista2: TMenuItem;
    Localizarlanamentoorigem2: TMenuItem;
    N3: TMenuItem;
    PMExtratos: TPopupMenu;
    PagarReceber1: TMenuItem;
    Movimento1: TMenuItem;
    ResultadosMensais1: TMenuItem;
    PMPesquisas: TPopupMenu;
    PorNveldoPLanodeContas1: TMenuItem;
    ContasControladas1: TMenuItem;
    Contassazonais1: TMenuItem;
    PMSaldos: TPopupMenu;
    Futuros1: TMenuItem;
    Em1: TMenuItem;
    PMListas: TPopupMenu;
    Contas1: TMenuItem;
    Entidades1: TMenuItem;
    PMDiversos: TPopupMenu;
    Etiquetas1: TMenuItem;
    Formulrios1: TMenuItem;
    Mensalidades1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    Panel10: TPanel;
    GBAcoes: TGroupBox;
    PnAcoesTravaveis: TPanel;
    BtMenu: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtQuita: TBitBtn;
    BtDuplica: TBitBtn;
    BtConcilia: TBitBtn;
    Panel7: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGCrt: TdmkDBGridZTO;
    TabSheet2: TTabSheet;
    Panel8: TPanel;
    Label20: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    EdSoma: TdmkEdit;
    BtSomaLinhas: TBitBtn;
    EdCred: TdmkEdit;
    EdDebi: TdmkEdit;
    TabSheet3: TTabSheet;
    Panel14: TPanel;
    GroupBox1: TGroupBox;
    BtSaldoAqui: TBitBtn;
    EdSdoAqui: TdmkEdit;
    GroupBox2: TGroupBox;
    LaDiferenca: TLabel;
    LaSaldo: TLabel;
    LaCaixa: TLabel;
    EdSaldo: TDBEdit;
    EdDiferenca: TDBEdit;
    EdCaixa: TDBEdit;
    BtContarDinheiro: TBitBtn;
    Panel15: TPanel;
    GroupBox7: TGroupBox;
    Panel12: TPanel;
    BtPlaCtas: TBitBtn;
    BtRecibo: TBitBtn;
    BtCheque: TBitBtn;
    BtSaldoTotal: TBitBtn;
    BtConfPagtosCad: TBitBtn;
    BtConfPgtosExe: TBitBtn;
    BtRefresh: TBitBtn;
    BtDesfazOrdenacao: TBitBtn;
    GroupBox8: TGroupBox;
    Panel13: TPanel;
    BtBalancete: TBitBtn;
    BtPesquisas: TBitBtn;
    BtSaldos: TBitBtn;
    BtListas: TBitBtn;
    BtDiversos: TBitBtn;
    BitBtn6: TBitBtn;
    GroupBox5: TGroupBox;
    Label8: TLabel;
    Label5: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    RGTipoData: TRadioGroup;
    GroupBox4: TGroupBox;
    Label17: TLabel;
    BtLocCtrl: TBitBtn;
    EdLocCtrl: TdmkEditCB;
    BitBtn1: TBitBtn;
    PMConfPagtosCad: TPopupMenu;
    ContasMensaisCadastrodePagamentos1: TMenuItem;
    ContasMensaisCadastrodeEmisses1: TMenuItem;
    PMConfPgtosExe: TPopupMenu;
    Pagamentosexecutados1: TMenuItem;
    Contasmensais1: TMenuItem;
    BtBloqueto: TBitBtn;
    PMBloqueto: TPopupMenu;
    Gerabloqueto1: TMenuItem;
    ImprimeGerenciabloqueto1: TMenuItem;
    BtProtocolo: TBitBtn;
    PMProtocolo: TPopupMenu;
    Adicionalanamentos1: TMenuItem;
    Atual4: TMenuItem;
    Selecionados5: TMenuItem;
    UmporDocumento1: TMenuItem;
    UmparacadaLanamento1: TMenuItem;
    Localizalote1: TMenuItem;
    dmkPermissoes1: TdmkPermissoes;
    PMSalvaPosicaoGrade: TPopupMenu;
    SalvarOrdenaodascolunasdagrade2: TMenuItem;
    CarregarOrdenaopadrodascolunasdagrade1: TMenuItem;
    BtSalvaPosicaoGrade: TBitBtn;
    BtTrfCta: TBitBtn;
    BtAutom: TBitBtn;
    BtExtratos: TBitBtn;
    N4: TMenuItem;
    Ferramentas1: TMenuItem;
    Atualizalanamentos1: TMenuItem;
    Cheques1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TPDataIniChange(Sender: TObject);
    procedure TPDataFimChange(Sender: TObject);
    procedure RGTipoDataClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtDesfazOrdenacaoClick(Sender: TObject);
    procedure BtDuplicaClick(Sender: TObject);
    procedure BtTrfCtaClick(Sender: TObject);
    procedure Novatransferncia1Click(Sender: TObject);
    procedure Alteratransferncia1Click(Sender: TObject);
    procedure Excluitransferncia1Click(Sender: TObject);
    procedure Localizatransferncia1Click(Sender: TObject);
    procedure Alteratransfernciaentrecontas1Click(Sender: TObject);
    procedure Excluitransfernciaentrecontas1Click(Sender: TObject);
    procedure Novatransfernciaentrecontas1Click(Sender: TObject);
    procedure BtSaldoAquiClick(Sender: TObject);
    procedure Calcula1Click(Sender: TObject);
    procedure Limpa1Click(Sender: TObject);
    procedure Diferena1Click(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure Incluinovatransfernciaentrecarteiras1Click(Sender: TObject);
    procedure Alteratransfernciaentrecarteirasatual1Click(Sender: TObject);
    procedure Excluitransfernciaentrecarteirasatual1Click(Sender: TObject);
    procedure Localizatransfernciaentrecarteiras1Click(Sender: TObject);
    procedure Incluinovatransfernciaentrecontas1Click(Sender: TObject);
    procedure Alteratransfernciaentrecontasatual1Click(Sender: TObject);
    procedure Excluitransfernciaentrecontasatual1Click(Sender: TObject);
    procedure Compensar1Click(Sender: TObject);
    procedure Reverter1Click(Sender: TObject);
    procedure Pagar1Click(Sender: TObject);
    procedure PagarAVista1Click(Sender: TObject);
    procedure Localizarlanamentoorigem1Click(Sender: TObject);
    procedure DBGLctDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGLctKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBGLctDblClick(Sender: TObject);
    procedure BtSomaLinhasClick(Sender: TObject);
    procedure BtContarDinheiroClick(Sender: TObject);
    procedure BtQuitaClick(Sender: TObject);
    procedure Compensar2Click(Sender: TObject);
    procedure Reverter2Click(Sender: TObject);
    procedure Pagar2Click(Sender: TObject);
    procedure PMQuitaPopup(Sender: TObject);
    procedure Localizarlanamentoorigem2Click(Sender: TObject);
    procedure PagarAVista2Click(Sender: TObject);
    procedure Copiar1Click(Sender: TObject);
    procedure Localizar2Click(Sender: TObject);
    procedure Recibo1Click(Sender: TObject);
    procedure Carteiras1Click(Sender: TObject);
    procedure Data1Click(Sender: TObject);
    procedure Compensao1Click(Sender: TObject);
    procedure Transformaremitemdebloqueto1Click(Sender: TObject);
    procedure Acertarcreditopelovalorpago1Click(Sender: TObject);
    procedure Datalancto1Click(Sender: TObject);
    procedure Vencimento1Click(Sender: TObject);
    procedure MsanterioraoVencimento1Click(Sender: TObject);
    procedure Exclusoincondicional1Click(Sender: TObject);
    procedure BtConciliaClick(Sender: TObject);
    procedure BtSaldoTotalClick(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure BtBalanceteClick(Sender: TObject);
    procedure PagarReceber1Click(Sender: TObject);
    procedure Movimento1Click(Sender: TObject);
    procedure ResultadosMensais1Click(Sender: TObject);
    procedure BtExtratosClick(Sender: TObject);
    procedure PorNveldoPLanodeContas1Click(Sender: TObject);
    procedure ContasControladas1Click(Sender: TObject);
    procedure BtPesquisasClick(Sender: TObject);
    procedure Contassazonais1Click(Sender: TObject);
    procedure Futuros1Click(Sender: TObject);
    procedure Em1Click(Sender: TObject);
    procedure BtSaldosClick(Sender: TObject);
    procedure Contas1Click(Sender: TObject);
    procedure Etiquetas1Click(Sender: TObject);
    procedure Formulrios1Click(Sender: TObject);
    procedure Mensalidades1Click(Sender: TObject);
    procedure BtListasClick(Sender: TObject);
    procedure BtDiversosClick(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BtReciboClick(Sender: TObject);
    procedure BtLocCtrlClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtPlaCtasClick(Sender: TObject);
    procedure BtChequeClick(Sender: TObject);
    procedure BtConfPagtosCadClick(Sender: TObject);
    procedure BtConfPgtosExeClick(Sender: TObject);
    procedure ContasMensaisCadastrodePagamentos1Click(Sender: TObject);
    procedure ContasMensaisCadastrodeEmisses1Click(Sender: TObject);
    procedure Pagamentosexecutados1Click(Sender: TObject);
    procedure Contasmensais1Click(Sender: TObject);
    procedure TPDataIniClick(Sender: TObject);
    procedure TPDataFimClick(Sender: TObject);
    procedure BtBloquetoClick(Sender: TObject);
    procedure Gerabloqueto1Click(Sender: TObject);
    procedure ImprimeGerenciabloqueto1Click(Sender: TObject);
    procedure PMBloquetoPopup(Sender: TObject);
    procedure BtProtocoloClick(Sender: TObject);
    procedure Atual4Click(Sender: TObject);
    procedure Localizalote1Click(Sender: TObject);
    procedure UmporDocumento1Click(Sender: TObject);
    procedure UmparacadaLanamento1Click(Sender: TObject);
    procedure BtSalvaPosicaoGradeClick(Sender: TObject);
    procedure SalvarOrdenaodascolunasdagrade2Click(Sender: TObject);
    procedure CarregarOrdenaopadrodascolunasdagrade1Click(Sender: TObject);
    procedure BtAutomClick(Sender: TObject);
    procedure Atualizalanamentos1Click(Sender: TObject);
    procedure Cheques1Click(Sender: TObject);
  private
    { Private declarations }
    FThisEntidade, FThisCliInt, FThisFilial: Integer;
    FFatIDHabilitaByCrt, FFatIDHabilitaByLct: Boolean;
    //
    FMaxTrava: Integer;
    FArrTrava: array of TWinControl;
    //
    procedure VeSeReabreLct();
    procedure InsAlt(Acao: TGerencimantoDeRegistro);
    procedure RecalcSaldoCarteira();
    procedure TranferenciaEntreCarteiras(Tipo: Integer);
    procedure TranferenciaEntreContas(Tipo: Integer);
    procedure CriaImpressaoDiversos(Indice: Integer);
    procedure PagarRolarEmissao();
  public
    { Public declarations }
    FDtEncer, FDtMorto: TDateTime;
    FTabLctA, FTabLctB, FTabLctD: String;
    //
    procedure DmLct0_QrCrtAfterScroll();
    procedure DmLct0_QrLctAfterScroll();
  end;

  var
  FmLctGer1: TFmLctGer1;

implementation

uses UnMyObjects, ModuleLct0, UnInternalConsts, Module, ModuleFin, ModuleGeral,
MyDBCheck, DmkDAC_PF, LctEdit, CashBal, MyListas, Principal, UMySQLModule,
UnGOTOy, Extratos, PrincipalImp, Resmes, PesqContaCtrl, Pesquisas, CtaGruImp,
Saldos, Formularios, Maladireta, EventosCad, ContasConfEmis, ContasMesGer,
ContasConfCad, UnMyPrinters, ContasConfPgto, CashTabs, 
{$IfNDef SemProtocolo} Protocolo, {$EndIf}
{$IFDEF IMP_BLOQLCT}UnBloquetos, {$ENDIF} RelChAbertos;

{$R *.DFM}

procedure TFmLctGer1.Acertarcreditopelovalorpago1Click(Sender: TObject);
begin
  UFinanceiro.AcertarCreditoPeloValorPago(DmLct0.QrCrt, DmLct0.QrLct,
  TDBGrid(DBGLct), FTabLctA);
end;

procedure TFmLctGer1.Alteratransferncia1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(1);
end;

procedure TFmLctGer1.Alteratransfernciaentrecarteirasatual1Click(
  Sender: TObject);
begin
  TranferenciaEntreCarteiras(1);
end;

procedure TFmLctGer1.Alteratransfernciaentrecontas1Click(Sender: TObject);
begin
  TranferenciaEntreContas(1);
end;

procedure TFmLctGer1.Alteratransfernciaentrecontasatual1Click(Sender: TObject);
begin
  TranferenciaEntreContas(1);
end;

procedure TFmLctGer1.Atual4Click(Sender: TObject);
begin
{$IfNDef SemProtocolo}
  UnProtocolo.ProtocolosCD_Lct(istAtual, TDBGrid(DBGLct), DmLct0, FThisEntidade);
{$EndIf}
end;

procedure TFmLctGer1.Atualizalanamentos1Click(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass; 
    //
    UFinanceiro.LancamentosComProblemas(FTabLctA);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctGer1.BtDuplicaClick(Sender: TObject);
begin
  InsAlt(tgrDuplica);
end;

procedure TFmLctGer1.BtSaldosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSaldos, BtSaldos);
end;

procedure TFmLctGer1.BtDiversosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDiversos, BtDiversos);
end;

procedure TFmLctGer1.BitBtn1Click(Sender: TObject);
begin
  UFinanceiro.LocalizarLancamento(TPDataIni, RGTipoData.ItemIndex, DmLct0.QrCrt,
    DmLct0.QrLct, True, 0, FTabLctA, DmLct0);
end;

procedure TFmLctGer1.BitBtn6Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEventosCad, FmEventosCad, afmoNegarComAviso) then
  begin
    FmEventosCad.ShowModal;
    FmEventosCad.Destroy;
  end;
end;

procedure TFmLctGer1.BtAlteraClick(Sender: TObject);
begin
  InsAlt(tgrAltera);
end;

procedure TFmLctGer1.BtAutomClick(Sender: TObject);
begin
  UFinanceiro.QuitacaoAutomaticaDmk(TDmkDBGrid(DBGLct), DmLct0.QrLct, DmLct0.QrCrt, FTabLctA);
end;

procedure TFmLctGer1.BtBalanceteClick(Sender: TObject);
begin
  FmPrincipal.BalanceteConfiguravel(FTabLctA); // Ver no credito2
end;

procedure TFmLctGer1.BtBloquetoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBloqueto, BtBloqueto);
end;

procedure TFmLctGer1.BtChequeClick(Sender: TObject);
begin
  MyPrinters.EmiteCheque(
    DmLct0.QrLctControle.Value,
    DmLct0.QrLctSub.Value, DmLct0.QrCrtBanco1.Value,
    0, '', 0, '', DmLct0.QrLctSerieCH.Value, DmLct0.QrLctDocumento.Value,
    DmLct0.QrLctData.Value, DmLct0.QrLctCarteira.Value,
    'NOMEFORNECEDOR', TDBGrid(DBGLct), DmLct0.FTabLctA, RGTipoData.ItemIndex,
    TPDataIni.Date, TPDataFim.Date);
end;

procedure TFmLctGer1.BtConciliaClick(Sender: TObject);
const
  Condominio = 0;
var
  CartConcilia, Entidade(*, CliInt*): Integer;
  NomeCI: String;
begin
  CartConcilia := DmLct0.QrCrt.FieldByName('Codigo').AsInteger;
  Entidade     := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
  //CliInt       := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
  NomeCI       := DModG.EmpresaAtual_ObtemNome();
  //
  UFinanceiro.ConciliacaoBancaria(DmLct0.QrCrt, DmLct0.QrLct, TPDataIni,
    NomeCI, Entidade, Condominio, CartConcilia, FTabLctA, DmLct0);
end;

procedure TFmLctGer1.BtConfPagtosCadClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConfPagtosCad, BtConfPagtosCad);
end;

procedure TFmLctGer1.BtConfPgtosExeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConfPgtosExe, BtConfPgtosExe);
end;

procedure TFmLctGer1.BtContarDinheiroClick(Sender: TObject);
begin
  UFinanceiro.MudaValorEmCaixa(DmLct0.QrLct, DmLct0.QrCrt);
end;

procedure TFmLctGer1.BtDesfazOrdenacaoClick(Sender: TObject);
{
var
  Query: TmySQLQuery;
  Controle: Integer;
}
begin
  UnDmkDAC_PF.DesfazOrdenacaoDmkDBGrid(TDmkDBGrid(DBGLct), 'Controle');
{
  if DBGLct.DataSource.DataSet is TmySQLQuery then
    Query := TmySQLQuery(DBGLct.DataSource.DataSet)
  else
    Query := nil;
  if Query <> nil then
  begin
    Query.SortFieldNames := '';
    if Query.State <> dsInactive then
    begin
      Controle := Query.FieldByName('Controle').AsInteger;
      Query.Close;
      Query.Open;
      Query.Locate('Controle', Controle, []);
    end;
  end;
}
end;

procedure TFmLctGer1.BtExcluiClick(Sender: TObject);
var
  i, k: Integer;
begin
  if DBGLct.SelectedRows.Count > 1 then
  begin
    if Geral.MensagemBox('Confirma a exclus�o dos itens selecionados?',
    'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
    ID_YES then
    begin
      with DBGLct.DataSource.DataSet do
      for i:= 0 to DBGLct.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGLct.SelectedRows.Items[i]));
        //
        if DmLct0.QrLctFatID.Value <> 0 then
        begin
          Geral.MensagemBox('Lan�amento espec�fico. Para excluir '+
            'este item selecione sua janela correta!', 'Erro', MB_OK+MB_ICONERROR);
          Exit;
        end;
        UFinanceiro.ExcluiItemCarteira(DmLct0.QrLctControle.Value,
          DmLct0.QrLctData.Value, DmLct0.QrLctCarteira.Value,
          DmLct0.QrLctSub.Value, DmLct0.QrLctGenero.Value,
          DmLct0.QrLctCartao.Value, DmLct0.QrLctSit.Value,
          DmLct0.QrLctTipo.Value, 0, DmLct0.QrLctID_Pgto.Value,
          DmLct0.QrLct, DmLct0.QrCrt, False, DmLct0.QrLctCarteira.Value,
          CO_MOTVDEL_300_EXCLUILCT, FTabLctA);
      end;
      k := UMyMod.ProximoRegistro(DmLct0.QrLct, 'Controle',  0);
      UFinanceiro.RecalcSaldoCarteira(
        DmLct0.QrCrtCodigo.Value, DmLct0.QrCrt, DmLct0.QrLct, False, True);
      DmLct0.QrLct.Locate('Controle', k, []);
    end;
  end else
    if DmLct0.QrLctFatID.Value <> 0 then
    begin
      Geral.MensagemBox('Lan�amento espec�fico. Para excluir '+
        'este item selecione sua janela correta!', 'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;
    UFinanceiro.ExcluiItemCarteira(DmLct0.QrLctControle.Value,
      DmLct0.QrLctData.Value, DmLct0.QrLctCarteira.Value,
      DmLct0.QrLctSub.Value, DmLct0.QrLctGenero.Value,
      DmLct0.QrLctCartao.Value, DmLct0.QrLctSit.Value,
      DmLct0.QrLctTipo.Value, 0, DmLct0.QrLctID_Pgto.Value,
      DmLct0.QrLct, DmLct0.QrCrt, True, DmLct0.QrLctCarteira.Value,
      CO_MOTVDEL_300_EXCLUILCT, FTabLctA);
end;

procedure TFmLctGer1.BtExtratosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExtratos, BtExtratos);
end;

procedure TFmLctGer1.BtIncluiClick(Sender: TObject);
begin
  InsAlt(tgrInclui);
end;

procedure TFmLctGer1.BtListasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMListas, BtListas);
end;

procedure TFmLctGer1.BtMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMenu, BtMenu);
end;

procedure TFmLctGer1.BtOKClick(Sender: TObject);
begin
//
end;

procedure TFmLctGer1.BtPesquisasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesquisas, BtPesquisas);
end;

procedure TFmLctGer1.BtPlaCtasClick(Sender: TObject);
begin
  FmPrincipal.CadastroDeNiveisPlano();
end;

procedure TFmLctGer1.BtProtocoloClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProtocolo, BtProtocolo);
end;

procedure TFmLctGer1.BtQuitaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMQuita, BtQuita);
end;

procedure TFmLctGer1.BtReciboClick(Sender: TObject);
begin
  if DmLct0.QrLctCredito.Value > 0 then
    GOTOy.EmiteRecibo(DmLct0.QrLctCliente.Value,
    FThisEntidade,
    DmLct0.QrLctCredito.Value, 0, 0,
    IntToStr(DmLct0.QrLctControle.Value) + '-' +
    IntToStr(DmLct0.QrLctSub.Value),
    DmLct0.QrLctDescricao.Value, '', '',
    DmLct0.QrLctData.Value,
    DmLct0.QrLctSit.Value)
  else
    GOTOy.EmiteRecibo(FThisEntidade,
    DmLct0.QrLctFornecedor.Value,
    DmLct0.QrLctDebito.Value, 0, 0,
    IntToStr(DmLct0.QrLctControle.Value) + '-' +
    IntToStr(DmLct0.QrLctSub.Value),
    DmLct0.QrLctDescricao.Value, '', '',
    DmLct0.QrLctData.Value,
    DmLct0.QrLctSit.Value);
end;

procedure TFmLctGer1.BtRefreshClick(Sender: TObject);
var
  Controle: Integer;
begin
  if DmLct0.QrLct.State = dsBrowse then
    Controle := DmLct0.QrLctControle.Value
  else
    Controle := 0;
  //
  { n�o ajuda! J� est� na procedure!
  DmLct0.QrCrt.DisableControls;
  try
  }
    UFinanceiro.AtualizaTodasCarteiras(DmLct0.QrCrt, DmLct0.QrLct, FTabLctA);
  {
  finally
    DmLct0.QrCrt.EnableControls;
  end;
  }
  //
  if Controle <> 0 then
    DmLct0.QrLct.Locate('Controle', Controle, []);
end;

procedure TFmLctGer1.BtSaldoAquiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSaldoAqui, BtSaldoAqui);
end;

procedure TFmLctGer1.BtSaldoTotalClick(Sender: TObject);
begin
  BtSaldoTotal.Caption := FormatFloat('#,###,###,##0.00',
  UFinanceiro.ObtemSaldoCarteira(DmLct0.QrCrtCodigo.Value));
end;

procedure TFmLctGer1.BtSalvaPosicaoGradeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSalvaPosicaoGrade, BtSalvaPosicaoGrade);
end;

procedure TFmLctGer1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctGer1.BtSomaLinhasClick(Sender: TObject);
begin
  UFinanceiro.SomaLinhas_2(TDBGrid(DBGLct), DmLct0.QrCrt, DmLct0.QrLct,
    EdCred, EdDebi, EdSoma);
end;

procedure TFmLctGer1.BtTrfCtaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTrfCta, BtTrfCta);
end;

procedure TFmLctGer1.Calcula1Click(Sender: TObject);
begin
  EdSdoAqui.Text :=
    UFinanceiro.SaldoAqui(1, EdSdoAqui.Text, DmLct0.QrLct, DmLct0.QrCrt);
end;

procedure TFmLctGer1.CarregarOrdenaopadrodascolunasdagrade1Click(
  Sender: TObject);
begin
  if Geral.MensagemBox('Deseja carregar a ordena��o padr�o das colunas da grade?'
    + sLineBreak + 'Ao fazer isso a configura��o atual ser� alterada.' + sLineBreak +
    'Deseja continuar?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    VAR_SAVE_CFG_DBGRID_GRID := DBGLct;
    DModG.SalvaPosicaoColunasDmkDBGrid(VAR_USUARIO, True);
    Close;
  end;
end;

procedure TFmLctGer1.Carteiras1Click(Sender: TObject);
begin
  UFinanceiro.MudaCarteiraLancamentosSelecionados(
    DmLct0.QrCrt, DmLct0.QrLct, TDBGrid(DBGLct), FTabLctA);
end;

procedure TFmLctGer1.Cheques1Click(Sender: TObject);
begin
  Application.CreateForm(TFmRelChAbertos, FmRelChAbertos);
  if FThisCliInt <> 0 then
  begin
    FmRelChAbertos.EdEmpresa.ValueVariant := FThisCliInt;
    FmRelChAbertos.CBEmpresa.KeyValue     := FThisCliInt;
  end;
  FmRelChAbertos.ShowModal;
  FmRelChAbertos.Destroy;
end;

procedure TFmLctGer1.Compensao1Click(Sender: TObject);
begin
  UFinanceiro.MudaDataLancamentosSelecionados(
    cdCompensado, DMLct0.QrCrt, DMLct0.QrLct, TDBGrid(DBGLct), FTabLctA);
end;

procedure TFmLctGer1.Compensar1Click(Sender: TObject);
begin
  UFinanceiro.QuitacaoDeDocumentos(TDBGrid(DBGLct), DmLct0.QrCrt, DmLct0.QrLct,
  FTabLctA);
end;

procedure TFmLctGer1.Compensar2Click(Sender: TObject);
begin
  UFinanceiro.QuitacaoDeDocumentos(TDBGrid(DBGLct), DmLct0.QrCrt, DmLct0.QrLct,
    FTabLctA);
end;

procedure TFmLctGer1.Contas1Click(Sender: TObject);
begin
  CriaImpressaoDiversos(2);
end;

procedure TFmLctGer1.ContasControladas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPesqContaCtrl, FmPesqContaCtrl, afmoNegarComAviso) then
  begin
    FmPesqContaCtrl.ShowModal;
    FmPesqContaCtrl.Destroy;
  end;
end;

procedure TFmLctGer1.Contasmensais1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasConfEmis, FmContasConfEmis, afmoNegarComAviso) then
  begin
    with FmContasConfEmis do
    begin
      FFinalidade      := lfCondominio;
      FQrLct           := DmLct0.QrLct;
      FQrCrt           := DmLct0.QrCrt;
      FPercJuroM       := Dmod.QrControleMoraDD.Value;
      FPercMulta       := Dmod.QrControleMulta.Value;
      FSetaVars        := nil;
      FAlteraAtehFatID := True;
      FLockCliInt      := True;
      FLockForneceI    := False;
      FLockAccount     := False;
      FLockVendedor    := False;
      FCliente         := 0;
      FFornecedor      := 0;
      FForneceI        := 0;
      FAccount         := 0;
      FVendedor        := 0;
      FIDFinalidade    := 2;
      FTabLctA         := FmLctGer1.FTabLctA;
      //
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmLctGer1.ContasMensaisCadastrodeEmisses1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasMesGer, FmContasMesGer, afmoNegarComAviso) then
  begin
    FmContasMesGer.ShowModal;
    FmContasMesGer.Destroy;
  end;
end;

procedure TFmLctGer1.ContasMensaisCadastrodePagamentos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasConfCad, FmContasConfCad, afmoNegarComAviso) then
  begin
    FmContasConfCad.ShowModal;
    FmContasConfCad.Destroy;
  end;
end;

procedure TFmLctGer1.Contassazonais1Click(Sender: TObject);
begin
  Application.CreateForm(TFmCtaGruImp, FmCtaGruImp);
  FmCtaGruImp.ShowModal;
  FmCtaGruImp.Destroy;
end;

procedure TFmLctGer1.Copiar1Click(Sender: TObject);
begin
  InsAlt(tgrDuplica);
end;

procedure TFmLctGer1.CriaImpressaoDiversos(Indice: Integer);
begin
  if DBCheck.CriaFm(TFmFormularios, FmFormularios, afmoNegarComAviso) then
  begin
    FmFormularios.RGRelatorio.ItemIndex := Indice;
    FmFormularios.ShowModal;
    FmFormularios.Destroy;
  end;
end;

procedure TFmLctGer1.Data1Click(Sender: TObject);
begin
  UFinanceiro.MudaDataLancamentosSelecionados(
    cdData, DMLct0.QrCrt, DMLct0.QrLct, TDBGrid(DBGLct), FTabLctA);
end;

procedure TFmLctGer1.Datalancto1Click(Sender: TObject);
begin
  UFinanceiro.ColocarMesDeCompetenciaOndeNaoTem(dcData,
    DmLct0.QrCrt, DmLct0.QrLct, FTabLctA);
end;

procedure TFmLctGer1.DBGLctDblClick(Sender: TObject);
begin
  if FFatIDHabilitaByLct then
    InsAlt(tgrAltera);
end;

procedure TFmLctGer1.DBGLctDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  MyObjects.DefineCorTextoSitLancto(TDBGrid(Sender), Rect,
    Column.FieldName, DmLct0.QrLctNOMESIT.Value);
end;

procedure TFmLctGer1.DBGLctKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if FFatIDHabilitaByLct then
  begin
    if key=13 then InsAlt(tgrAltera)
    else if (key=VK_DELETE) and (Shift=[ssCtrl]) then
      UFinanceiro.ExcluiItemCarteira(DmLct0.QrLctControle.Value,
        DmLct0.QrLctData.Value, DmLct0.QrLctCarteira.Value,
        DmLct0.QrLctSub.Value, DmLct0.QrLctGenero.Value,
        DmLct0.QrLctCartao.Value, DmLct0.QrLctSit.Value,
        DmLct0.QrLctTipo.Value, 0, DmLct0.QrLctID_Pgto.Value,
        DmLct0.QrLct, DmLct0.QrCrt, True, DmLct0.QrLctCarteira.Value,
        CO_MOTVDEL_300_EXCLUILCT, FTabLctA)
    else if (key=VK_F4) and (Shift=[ssCtrl]) then
      MyObjects.MostraPopUpDeBotaoObject(PMQuita, DBGLct, 0, 0)
  end;
end;

procedure TFmLctGer1.Diferena1Click(Sender: TObject);
begin
  EdSdoAqui.Text :=
    UFinanceiro.SaldoAqui(2, EdSdoAqui.Text, DmLct0.QrLct, DmLct0.QrCrt);
end;

procedure TFmLctGer1.DmLct0_QrCrtAfterScroll();
var
 I: Integer;
 CompoName: String;
begin
  FFatIDHabilitaByCrt := DmLct0.QrCrtCodigo.Value > -1;
  //
{
  for I := 0 to FmLctGer1.ComponentCount - 1 do
  begin
    if TComponent(FmLctGer1.Components[I]).GetParentComponent = PnAcoesTravaveis then
      TWinControl(FmLctGer1.Components[I]).Enabled := FFatIDHabilitaByCrt;
  end;
}
  for I := 0 to FMaxTrava - 1 do
  begin
    CompoName := FArrTrava[I].Name;
    //
    if (CO_DMKID_APP = 22) and ((CompoName = 'BtInclui') or
      (CompoName = 'BtAltera') or (CompoName = 'BtExclui')) and
      (FFatIDHabilitaByCrt = False)
    then
      FArrTrava[I].Enabled := True
    else
      FArrTrava[I].Enabled := FFatIDHabilitaByCrt;
  end;
end;

procedure TFmLctGer1.DmLct0_QrLctAfterScroll();
var
  I: Integer;
begin
  if FFatIDHabilitaByCrt then
  begin
    FFatIDHabilitaByLct := (DmLct0.QrLctFatID.Value < 300) or
                (DmLct0.QrLctFatID.Value > 399);
    //if not FFatIDHabilitaByLct then
    for I := 0 to FMaxTrava - 1 do
      FArrTrava[I].Enabled := FFatIDHabilitaByLct;
  end;
end;

procedure TFmLctGer1.Em1Click(Sender: TObject);
begin
  Application.CreateForm(TFmSaldos, FmSaldos);
  FmSaldos.ShowModal;
  FmSaldos.Destroy;
end;

procedure TFmLctGer1.Etiquetas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmLctGer1.Excluitransferncia1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(2);
end;

procedure TFmLctGer1.Excluitransfernciaentrecarteirasatual1Click(
  Sender: TObject);
begin
  TranferenciaEntreCarteiras(2);
end;

procedure TFmLctGer1.Excluitransfernciaentrecontas1Click(Sender: TObject);
begin
  TranferenciaEntreContas(2);
end;

procedure TFmLctGer1.Excluitransfernciaentrecontasatual1Click(Sender: TObject);
begin
  TranferenciaEntreContas(2);
end;

procedure TFmLctGer1.Exclusoincondicional1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  UFinanceiro.ExclusaoIncondicionalDeLancamento(
    DmLct0.QrCrt, DmLct0.QrLct, FTabLctA);
end;

procedure TFmLctGer1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctGer1.FormClose(Sender: TObject; var Action: TCloseAction);
var
  M: Integer;
begin
  Geral.WriteAppKeyCU('Dias', Application.Title, Int(Date - TPDataIni.Date), ktInteger);
  if WindowState = wsMaximized then
    M := 1
  else
    M := 0;
  Geral.WriteAppKeyCU('MaximizeLctGer1', Application.Title, M, ktInteger);
end;

procedure TFmLctGer1.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  VAR_SAVE_CFG_DBGRID_GRID := DBGLct;
  DModG.CarregaPosicaoColunasDmkDBGrid(VAR_USUARIO);
  //
  ImgTipo.SQLType := stPsq;
  PageControl1.ActivePageIndex := 0;
  FThisEntidade           := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
  FThisCliInt             := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
  FThisFilial             := DModG.EmpresaAtual_ObtemCodigo(tecFilial);
  //
  EdCodigo.DataSource     := DmLct0.DsCrt;
  EdNome.DataSource       := DmLct0.DsCrt;
  EdSaldo.DataSource      := DmLct0.DsCrt;
  EdDiferenca.DataSource  := DmLct0.DsCrt;
  EdCaixa.DataSource      := DmLct0.DsCrt;
  DBGCrt.DataSource       := DmLct0.DsCrt;
  //
  DBGLct.DataSource       := DmLct0.DsLct;
  DBEdit1.DataSource      := DmLct0.DsLct;
  //
  //////////////////////////////////////////////////////////////////////////////
  TPDataIni.Date := Date - Geral.ReadAppKeyCU('Dias', Application.Title, ktInteger, 60);
  // ser�  feito ap�s definir o TabLctA
  //TPDataFim.Date := Date + 180;
  //////////////////////////////////////////////////////////////////////////////
  //
  Caption := Caption + ' :: ' + FormatFloat('000',
    DModG.EmpresaAtual_ObtemCodigo(tecCliInt)) + ' - ' +
    DModG.EmpresaAtual_ObtemNome();
  FormResize(Self);
  //
  if Geral.ReadAppKeyCU(
  'MaximizeLctGer1', Application.Title, ktInteger, 0) = 1 then
    WindowState := wsMaximized;
  //
  FMaxTrava := 0;
  for I := 0 to FmLctGer1.ComponentCount - 1 do
  begin
    if TComponent(FmLctGer1.Components[I]).GetParentComponent = PnAcoesTravaveis then
    begin
      FMaxTrava := FMaxTrava + 1;
      SetLength(FArrTrava, FMaxTrava);
      FArrTrava[FMaxTrava -1] := TWinControl(FmLctGer1.Components[I]);
    end;
  end;
  //
  {$IFDEF IMP_BLOQLCT}
    BtBloqueto.Visible := True;
  {$ELSE}
    BtBloqueto.Visible := False;
  {$ENDIF}
  {$IfDef SemProtocolo}
    BtProtocolo.Visible := False;
  {$EndIf}
  for i := 0 to DBGLct.Columns.Count - 1 do
  begin
    if DBGLct.Columns[I].FieldName = 'UH' then
    begin
      DBGLct.Columns[I].Visible := VAR_KIND_DEPTO <> kdNenhum;
      case VAR_KIND_DEPTO of
        kdUH: DBGLct.Columns[I].Title.Caption := 'UH';
        kdObra: DBGLct.Columns[I].Title.Caption := 'Obra';
      end;
    end;
  end;
end;

procedure TFmLctGer1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctGer1.Formulrios1Click(Sender: TObject);
begin
  CriaImpressaoDiversos(0);
end;

procedure TFmLctGer1.Futuros1Click(Sender: TObject);
begin
  DModFin.ImprimeSaldos(FmPrincipal.FEntInt);
end;

procedure TFmLctGer1.Gerabloqueto1Click(Sender: TObject);
{$IFDEF IMP_BLOQLCT}
var
  LoteCR: Integer;
{$ENDIF}
begin
  {$IFDEF IMP_BLOQLCT}
  Gerabloqueto1.Enabled := False;
  //
  if UBloquetos.EmiteBoletoAvulso(FTabLctA, TdmkDBGrid(DBGLct), DmLct0.QrLct, True,
    DmLct0.QrLctFatNum.Value, LoteCR) then
  begin
    if Geral.MensagemBox('Deseja imprimir o bloqueto?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      if UBloquetos.DefineVarsBloGeren(FmPrincipal.FModBloq_EntCliInt,
        FmPrincipal.FModBloq_CliInt, FmPrincipal.FModBloq_TabLctA) then
      begin
        UBloquetos.CadastroBloGeren(3, FmPrincipal.PageControl1,
          FmPrincipal.AdvToolBarPager1);
      end;
    end;
  end;
  Gerabloqueto1.Enabled := True;
  {$ENDIF}
end;

procedure TFmLctGer1.ImprimeGerenciabloqueto1Click(Sender: TObject);
begin
{$IFDEF IMP_BLOQLCT}
  if UBloquetos.DefineVarsBloGeren(FmPrincipal.FModBloq_EntCliInt,
    FmPrincipal.FModBloq_CliInt, FmPrincipal.FModBloq_TabLctA) then
  begin
    UBloquetos.CadastroBloGeren(3, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPager1);
  end;
{$ENDIF}
end;

procedure TFmLctGer1.Incluinovatransfernciaentrecarteiras1Click(
  Sender: TObject);
begin
  TranferenciaEntreCarteiras(0);
end;

procedure TFmLctGer1.Incluinovatransfernciaentrecontas1Click(Sender: TObject);
begin
  TranferenciaEntreContas(0);
end;

procedure TFmLctGer1.InsAlt(Acao: TGerencimantoDeRegistro);
const
  AlteraAtehFatID = False;
var
  Continua: Boolean;
begin
  {
  if Acao = tgrInclui then
    Continua :=
    UFinanceiro.InclusaoLancamento(TFmLctEdit, FmLctEdit, lfProprio,
    afmoNegarComAviso, DmLct0.QrLct, DmLct0.QrCrt, Acao, DmLct0.QrLctControle.Value,
    DmLct0.QrLctSub.Value, (*DmLct0.QrLctGenero.Value*) 0 (*Genero*),
    Dmod.QrControleMyPerJuros.Value, Dmod.QrControleMyPerMulta.Value,
    nil(*SetaVars*), 0(*FatID*), 0(*FatID_Sub*), 0(*FatNum*), 0(*Carteira*),
    0(*Credito*), 0(*Debito*), AlteraAtehFatID,
    0(*Cliente*), 0(*Fornecedor*), FThisEntidade (*cliInt*),
    0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
    False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
    0(*Data*), 0(*Vencto*), 0(*DataDoc*), 1(*IDFinalidade*), 0(*Mes: TDateTime*),
    FtabLctA, 0(*FisicoSrc*), 0(*FisicoCod*)) > 0
  else if Acao = tgrAltera then
    Continua :=
    UFinanceiro.AlteracaoLancamento(DmLct0.QrCrt, DmLct0.QrLct, lfProprio,
    0(*OneAccount*), DmLct0.QrCrtForneceI.Value(*OneCliInt*), FTabLctA)
  else
    Continua := False;
  }
  Continua := UFinanceiro.InclusaoLancamento(TFmLctEdit, FmLctEdit, lfProprio,
    afmoNegarComAviso, DmLct0.QrLct, DmLct0.QrCrt, Acao, DmLct0.QrLctControle.Value,
    DmLct0.QrLctSub.Value, (*DmLct0.QrLctGenero.Value*) 0 (*Genero*),
    Dmod.QrControleMyPerJuros.Value, Dmod.QrControleMyPerMulta.Value,
    nil(*SetaVars*), 0(*FatID*), 0(*FatID_Sub*), 0(*FatNum*), 0(*Carteira*),
    0(*Credito*), 0(*Debito*), AlteraAtehFatID,
    0(*Cliente*), 0(*Fornecedor*), FThisEntidade (*cliInt*),
    0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
    False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
    0(*Data*), 0(*Vencto*), 0(*DataDoc*), 1(*IDFinalidade*), 0(*Mes: TDateTime*),
    FtabLctA, 0(*FisicoSrc*), 0(*FisicoCod*)) > 0;
  //
  if Continua then
  begin
    DmLct0.LocCod(DmLct0.QrCrtCodigo.Value, DmLct0.QrCrtCodigo.Value,
    DmLct0.QrCrt, '');
    DmLct0.QrLct.Locate('Controle', FLAN_CONTROLE, []);
    //
    if TPDataFim.Date < VAR_DATAINSERIR then
    begin
      TPDataFim.Date := VAR_DATAINSERIR;
      TPDataFimChange(Self);
    end;
  end;
end;

procedure TFmLctGer1.Limpa1Click(Sender: TObject);
begin
  EdSdoAqui.Text :=
    UFinanceiro.SaldoAqui(0, EdSdoAqui.Text, DmLct0.QrLct, DmLct0.QrCrt);
end;

procedure TFmLctGer1.Localizalote1Click(Sender: TObject);
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT Controle');
  Dmod.QrAux.SQL.Add('FROM protpakits');
  Dmod.QrAux.SQL.Add('WHERE Conta=:P0');
  Dmod.QrAux.Params[0].AsInteger := DmLct0.QrLctProtocolo.Value;
  Dmod.QrAux.Open;
  //
  FmPrincipal.CadastroDeProtocolos(Dmod.QrAux.FieldByName('Controle').AsInteger);
end;

procedure TFmLctGer1.Localizar2Click(Sender: TObject);
begin
  UFinanceiro.LocalizarLancamento(TPDataIni, RGTipoData.ItemIndex, DmLct0.QrCrt,
    DmLct0.QrLct, True, 0, FTabLctA, DmLct0);
end;

procedure TFmLctGer1.Localizarlanamentoorigem1Click(Sender: TObject);
begin
  UFinanceiro.LocalizarlanctoCliInt(DmLct0.QrLctID_Pgto.Value, 0,
    FThisEntidade, TPDataIni, DmLct0.QrLct,
    DmLct0.QrCrt, DmLct0.QrCrtSum, True, FTabLctA);
end;

procedure TFmLctGer1.Localizarlanamentoorigem2Click(Sender: TObject);
var
  Lancto: Integer;
begin
  Lancto := DmLct0.QrLctID_Pgto.Value;
  //
  if Lancto = 0 then
    Lancto := DmLct0.QrLctID_Quit.Value;
  //
  UFinanceiro.LocalizarlanctoCliInt(Lancto, 0, FThisEntidade, TPDataIni,
    DmLct0.QrLct, DmLct0.QrCrt, DmLct0.QrCrtSum, True, FTabLctA);
end;

procedure TFmLctGer1.Localizatransferncia1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(3);
end;

procedure TFmLctGer1.Localizatransfernciaentrecarteiras1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(3);
end;

procedure TFmLctGer1.Mensalidades1Click(Sender: TObject);
begin
  CriaImpressaoDiversos(1);
end;

procedure TFmLctGer1.Movimento1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPrincipalImp, FmPrincipalImp, afmoNegarComAviso) then
  begin
    FmPrincipalImp.ShowModal;
    FmPrincipalImp.Destroy;
  end;
end;

procedure TFmLctGer1.MsanterioraoVencimento1Click(Sender: TObject);
begin
  UFinanceiro.ColocarMesDeCompetenciaOndeNaoTem(dcMesAnteriorAoVencto,
    DmLct0.QrCrt, DmLct0.QrLct, FTabLctA);
end;

procedure TFmLctGer1.Novatransferncia1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(0);
end;

procedure TFmLctGer1.Novatransfernciaentrecontas1Click(Sender: TObject);
begin
  TranferenciaEntreContas(0);
end;

procedure TFmLctGer1.Pagamentosexecutados1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasConfPgto, FmContasConfPgto, afmoNegarComAviso) then
  begin
    with FmContasConfPgto do
    begin
      FFinalidade      := lfCondominio;
      FQrLct           := DmLct0.QrLct;
      FQrCrt           := DmLct0.QrCrt;
      FPercJuroM       := Dmod.QrControleMoraDD.Value;
      FPercMulta       := Dmod.QrControleMulta.Value;
      FSetaVars        := nil;
      FAlteraAtehFatID := True;
      FLockCliInt      := True;
      FLockForneceI    := False;
      FLockAccount     := False;
      FLockVendedor    := False;
      FCliente         := 0;
      FFornecedor      := 0;
      FForneceI        := 0;
      FAccount         := 0;
      FVendedor        := 0;
      FIDFinalidade    := 2;
      FTabLctA         := FmLctGer1.FTabLctA;
      //
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmLctGer1.Pagar1Click(Sender: TObject);
begin
  PagarRolarEmissao;
end;

procedure TFmLctGer1.Pagar2Click(Sender: TObject);
begin
  PagarRolarEmissao;
end;

procedure TFmLctGer1.PagarAVista1Click(Sender: TObject);
begin
  UFinanceiro.PagarAVistaEmCaixa(DmLct0.QrCrt, DmLct0.QrLct, TDBGrid(DBGLct),
  FTabLctA);
end;

procedure TFmLctGer1.PagarAVista2Click(Sender: TObject);
begin
  UFinanceiro.PagarAVistaEmCaixa(DmLct0.QrCrt, DmLct0.QrLct, TDBGrid(DBGLct),
  FTabLctA);
end;

procedure TFmLctGer1.PagarReceber1Click(Sender: TObject);
begin
  FmPrincipal.ExtratosFinanceirosEmpresaUnica(FTabLctA);
end;

procedure TFmLctGer1.PagarRolarEmissao;
var
  Lancto: Integer;
begin
  UFinanceiro.PagarRolarEmissao(DmLct0.QrCrt, DmLct0.QrLct, FTabLctA, Lancto);
  //
  if Lancto <> 0 then
    UFinanceiro.LocalizarlanctoCliInt(Lancto, 0, FThisEntidade, TPDataIni,
      DmLct0.QrLct, DmLct0.QrCrt, DmLct0.QrCrtSum, True, FTabLctA);
end;

procedure TFmLctGer1.PMBloquetoPopup(Sender: TObject);
{$IFDEF IMP_BLOQLCT}
var
  Enab: Boolean;
{$ENDIF}
begin
{$IFDEF IMP_BLOQLCT}
  Enab := UBloquetos.VerificaSeBoletoExiste(DmLct0.QrLctControle.Value);
  //
  Gerabloqueto1.Enabled := not Enab;
  ImprimeGerenciabloqueto1.Enabled := Enab;
{$ENDIF}
end;

procedure TFmLctGer1.PMMenuPopup(Sender: TObject);
begin
  if DmLct0.QrLctGenero.Value = -1 then
  begin
    Alteratransfernciaentrecarteirasatual1.Enabled := True;
    Excluitransfernciaentrecarteirasatual1.Enabled := True;
  end else begin
    Alteratransfernciaentrecarteirasatual1.Enabled := False;
    Excluitransfernciaentrecarteirasatual1.Enabled := False;
  end;
  Compensar1.Enabled := False;
  Pagar1.Enabled     := False;
  Reverter1.Enabled  := False;
  if DmLct0.QrCrtTipo.Value = 2 then
  begin
    if DmLct0.QrLctSit.Value in [0] then Compensar1.Enabled := True;
    if DmLct0.QrLctSit.Value in [0,1,2] then Pagar1.Enabled := True;
    if DmLct0.QrLctSit.Value in [3] then  Reverter1.Enabled := True;
  end;
  if DBGLct.SelectedRows.Count > 1 then
  begin
    Mudacarteiradelanamentosselecionados1.Enabled := True;
    PagarAVista1.Enabled := True;
    Localizarlanamentoorigem1.Enabled := False;
  end else begin
    Mudacarteiradelanamentosselecionados1.Enabled := False;
    PagarAVista1.Enabled := False;
    Localizarlanamentoorigem1.Enabled := DmLct0.QrLctID_Pgto.Value > 0;
  end;
end;

procedure TFmLctGer1.PMQuitaPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Compensar2.Enabled := False;
  Pagar2.Enabled     := False;
  Reverter2.Enabled  := False;
  if DmLct0.QrCrtTipo.Value = 2 then
  begin
    if DmLct0.QrLctSit.Value in [0] then Compensar2.Enabled := True;
    if DmLct0.QrLctSit.Value in [0,1,2] then Pagar2.Enabled := True;
    if DmLct0.QrLctSit.Value in [3] then  Reverter2.Enabled := True;
  end;
  if DBGLct.SelectedRows.Count > 1 then
  begin
    PagarAVista2.Enabled := True;
    Localizarlanamentoorigem2.Enabled := False;
  end else begin
    Enab := (DmLct0.QrLctID_Pgto.Value > 0) or ((DmLct0.QrLctID_Quit.Value > 0)
              and (DmLct0.QrLctSit.Value = 3));
    //
    PagarAVista2.Enabled := False;
    Localizarlanamentoorigem2.Enabled := Enab;
  end;
end;

procedure TFmLctGer1.PorNveldoPLanodeContas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPesquisas, FmPesquisas, afmoNegarComAviso) then
  begin
    FmPesquisas.ShowModal;
    FmPesquisas.FCliente_Txt := VAR_FIN_SELFG_000_CLI + ':';
    FmPesquisas.FFornece_Txt := VAR_FIN_SELFG_000_FRN + ':';
    FmPesquisas.Destroy;
  end;
end;

procedure TFmLctGer1.RecalcSaldoCarteira();
begin
  UFinanceiro.RecalcSaldoCarteira(DmLct0.QrCrtCodigo.Value,
  DmLct0.QrCrt, DmLct0.QrLct, True, True);
end;

procedure TFmLctGer1.Recibo1Click(Sender: TObject);
begin
  BtReciboClick(Self);
end;

procedure TFmLctGer1.ResultadosMensais1Click(Sender: TObject);
var
  Entidade, CliInt: Integer;
begin
  if DModG.SelecionaEmpresa(sllNenhuma) then
  begin
    Entidade := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
    CliInt   := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
    if DBCheck.CriaFm(TFmResMes, FmResMes, afmoSoAdmin) then
    begin
      DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, FmResMes.FDtEncer, FmResMes.FDtMorto,
        FmResMes.FTabLctA, FmResMes.FTabLctB, FmResMes.FTabLctD);
      FmResMes.ShowModal;
      FmResMes.Destroy;
    end;
  end;
end;

procedure TFmLctGer1.Reverter1Click(Sender: TObject);
begin
  UFinanceiro.ReverterPagtoEmissao(DmLct0.QrLct, DmLct0.QrCrt, True, True, True,
  FTabLctA);
end;

procedure TFmLctGer1.Reverter2Click(Sender: TObject);
begin
  UFinanceiro.ReverterPagtoEmissao(DmLct0.QrLct, DmLct0.QrCrt, True, True, True,
  FTabLctA);
end;

procedure TFmLctGer1.RGTipoDataClick(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmLctGer1.SalvarOrdenaodascolunasdagrade2Click(Sender: TObject);
begin
  VAR_SAVE_CFG_DBGRID_GRID := DBGLct;
  DModG.SalvaPosicaoColunasDmkDBGrid(VAR_USUARIO, False);
end;

procedure TFmLctGer1.BtLocCtrlClick(Sender: TObject);
begin
  if not DmLct0.QrLct.Locate('Controle', EdLocCtrl.ValueVariant, []) then
    Geral.MensagemBox('O lan�amento ' + IntToStr(EdLocCtrl.ValueVariant) +
    ' n�o foi localizado nesta carteira!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmLctGer1.TPDataFimChange(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmLctGer1.TPDataFimClick(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmLctGer1.TPDataIniChange(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmLctGer1.TPDataIniClick(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmLctGer1.TranferenciaEntreCarteiras(Tipo: Integer);
begin
  UFinanceiro.CriarTransferCart(Tipo, DmLct0.QrLct, DmLct0.QrCrt,
    True, FThisEntidade, 0, 0, 0, FTabLctA);
  RecalcSaldoCarteira();
end;

procedure TFmLctGer1.TranferenciaEntreContas(Tipo: Integer);
begin
  UFinanceiro.CriarTransferCtas(Tipo, DmLct0.QrLct, DmLct0.QrCrt,
    True, FThisEntidade, 0, 0, 0, FTabLctA);
  RecalcSaldoCarteira();
end;

procedure TFmLctGer1.Transformaremitemdebloqueto1Click(Sender: TObject);
const
  FatID = 0; // N�o bloquetos ainda! Vai gerar mensagem de aviso!
begin
  UFinanceiro.TransformarLancamentoEmItemDeBloqueto(
    FatID, DmLct0.QrCrt, DmLct0.QrLct, TDBGrid(DBGLct), FTabLctA);
end;

procedure TFmLctGer1.UmparacadaLanamento1Click(Sender: TObject);
begin
{$IfNDef SemProtocolo}
  UnProtocolo.ProtocolosCD_Lct(istSelecionados, TDBGrid(DBGLct), DmLct0, FThisEntidade);
{$EndIf}
end;

procedure TFmLctGer1.UmporDocumento1Click(Sender: TObject);
begin
{$IfNDef SemProtocolo}
  UnProtocolo.ProtocolosCD_Doc(istSelecionados, TDBGrid(DBGLct), DmLct0, FThisEntidade);
{$EndIf}
end;

procedure TFmLctGer1.Vencimento1Click(Sender: TObject);
begin
  UFinanceiro.ColocarMesDeCompetenciaOndeNaoTem(dcVencimento,
    DmLct0.QrCrt, DmLct0.QrLct, FTabLctA);
end;

procedure TFmLctGer1.VeSeReabreLct();
begin
  DmLct0.FTipoData := RGTipoData.ItemIndex;
  //
  DmLct0.VeSeReabreLct(TPDataIni, TPDataFim, 0,
  DmLct0.QrLctControle.Value, DmLct0.QrLctSub.Value,
  DmLct0.QrCrt, DmLct0.QrLct);
end;


end.
