unit ReIniLctsSoComOpnEmiss;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkEditDateTimePicker, mySQLDbTables;

type
  TFmReIniLctsSoComOpnEmiss = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtCopia: TBitBtn;
    LaTitulo1C: TLabel;
    BtExec: TBitBtn;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    LaEmissIni: TLabel;
    TPEmissIni: TdmkEditDateTimePicker;
    QrLcts: TmySQLQuery;
    QrLctsData: TDateField;
    QrLctsTipo: TIntegerField;
    QrLctsCarteira: TIntegerField;
    QrLctsControle: TIntegerField;
    QrLctsSub: TSmallintField;
    QrLctsGenero: TIntegerField;
    QrLctsDescricao: TWideStringField;
    QrLctsSerieNF: TWideStringField;
    QrLctsNotaFiscal: TIntegerField;
    QrLctsDebito: TFloatField;
    QrLctsCredito: TFloatField;
    QrLctsCompensado: TDateField;
    QrLctsSerieCH: TWideStringField;
    QrLctsDocumento: TFloatField;
    QrLctsSit: TSmallintField;
    QrLctsVencimento: TDateField;
    QrLctsPago: TFloatField;
    QrLctsMez: TIntegerField;
    QrLctsFornecedor: TIntegerField;
    QrLctsCliente: TIntegerField;
    QrLctsCliInt: TIntegerField;
    QrLctsForneceI: TIntegerField;
    QrLctsDataDoc: TDateField;
    QrLctsDuplicata: TWideStringField;
    QrLctsDepto: TIntegerField;
    QrLctsCtrlQuitPg: TIntegerField;
    QrLctsID_Pgto: TIntegerField;
    QrLctsValAPag: TFloatField;
    QrLctsValARec: TFloatField;
    QrLctsVTransf: TFloatField;
    QrLctsAtivo: TSmallintField;
    QrLctsPagRec: TSmallintField;
    QrLctsSeqPag: TIntegerField;
    PB1: TProgressBar;
    QrPago: TmySQLQuery;
    QrPagoValor: TFloatField;
    QrTots: TmySQLQuery;
    QrNew: TmySQLQuery;
    QrNewData: TDateField;
    QrNewTipo: TIntegerField;
    QrNewPagRec: TSmallintField;
    QrNewCarteira: TIntegerField;
    QrNewControle: TIntegerField;
    QrNewSub: TSmallintField;
    QrNewSeqPag: TIntegerField;
    QrNewGenero: TIntegerField;
    QrNewDescricao: TWideStringField;
    QrNewSerieNF: TWideStringField;
    QrNewNotaFiscal: TIntegerField;
    QrNewDebito: TFloatField;
    QrNewCredito: TFloatField;
    QrNewCompensado: TDateField;
    QrNewSerieCH: TWideStringField;
    QrNewDocumento: TFloatField;
    QrNewSit: TSmallintField;
    QrNewVencimento: TDateField;
    QrNewPago: TFloatField;
    QrNewMez: TIntegerField;
    QrNewFornecedor: TIntegerField;
    QrNewCliente: TIntegerField;
    QrNewCliInt: TIntegerField;
    QrNewForneceI: TIntegerField;
    QrNewDataDoc: TDateField;
    QrNewDuplicata: TWideStringField;
    QrNewDepto: TIntegerField;
    QrNewCtrlQuitPg: TIntegerField;
    QrNewID_Pgto: TIntegerField;
    QrNewValAPag: TFloatField;
    QrNewValARec: TFloatField;
    QrNewVTransf: TFloatField;
    QrNewValPgCre: TFloatField;
    QrNewValPgDeb: TFloatField;
    QrNewValPgTrf: TFloatField;
    QrNewAtivo: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCopiaClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtExecClick(Sender: TObject);
  private
    { Private declarations }
    FTabLctA,
    FTabTemp,
    FFluxCxaOpn,
    FEmissIni, FEmissFim: String;
    //
    procedure InsereAtual();
  public
    { Public declarations }
  end;

  var
  FmReIniLctsSoComOpnEmiss: TFmReIniLctsSoComOpnEmiss;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, UCreateFin, DmkDAC_PF,
UnFinanceiro;

{$R *.DFM}

procedure TFmReIniLctsSoComOpnEmiss.BtCopiaClick(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Esta a��o ir� fazer uma c�pia temporaria de todos lan�amentos da empresa selecionada!' +
  sLineBreak + 'Deseja continuar?') = ID_YES then
  begin
    DModG.MyPID_DB.Execute(Geral.ATS([
    'DROP TABLE IF EXISTS ' + FTabTemp + ';',
    'CREATE TABLE ' + FTabTemp,
    'SELECT * FROM ' + FTabLctA + ';',
    '']));
    //
    Geral.MB_Info('Tabela "' + FTabTemp + '" criada com sucesso!');
  end;
end;

procedure TFmReIniLctsSoComOpnEmiss.BtExecClick(Sender: TObject);
var
  DataI(*, DataF*): TDateTime;
  Data, SQL: String;
  ValAPag, ValARec, VTransf: Double;
  Entidade, Erros: Integer;
  Tipo, PagRec, Carteira, Sub: Integer;
  ValAPagAnt, ValARecAnt, VTransfAnt, ValAPagMov, ValARecMov, VTransfMov,
  EfetSdoAnt, EfetMovCre, EfetMovDeb, EfetSdoAcu, ValPgCre, ValPgDeb, ValPgTrf,
  Controle: Double;
  //
  Descricao, SerieNF, Compensado, SerieCH, Vencimento, DataDoc, Duplicata: String;
  Genero, NotaFiscal, Sit, Mez, Fornecedor, Cliente, CliInt, ForneceI, Depto,
  CtrlQuitPg, ID_Pgto, SeqPag: Integer;
  Debito, Credito, Documento, Pago, ValEmiPagC, ValEmiPagD: Double;
begin
  if not UMyMod.ObtemCodigoDeCodUsu(EdEmpresa, Entidade, 'Informe a empresa',
  'Codigo', 'FILIAL') then
    Exit;
  Erros := 0;
  //
  DataI := Trunc(TPEmissIni.Date);
  if MyObjects.FIC(DataI < 2, TPEmissIni, 'Informe a data do novo inicio!') then
    Exit;
  //DataF := Trunc(TPEmissFim.Date);
  FEmissIni := Geral.FDT(DataI, 1);
  //FEmissFim := Geral.FDT(TPEmissFim.Date, 1);
  FEmissFim := FEmissIni;
  //
  FFluxCxaOpn :=
    UCriarFin.RecriaTempTableNovo(ntrtt_FluxCxaOpn, DModG.QrUpdPID1, False);
  // Lancamentos em emissoes lancados antes da data inicial da pesquisa e que
  // continuam abertas ou foram quitadas a partir do dia inicial da pesquisa.
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DELETE FROM _fluxcxaopn_; ',
  'INSERT INTO _fluxcxaopn_ ',
  'SELECT lct.Data, car.Tipo, car.PagRec, lct.Carteira, lct.Controle, lct.Sub, 0 SeqPag, ',
  'lct.Genero, lct.Descricao, lct.SerieNF, lct.NotaFiscal, ',
  'lct.Debito, lct.Credito, lct.Compensado, lct.SerieCH, ',
  'lct.Documento, lct.Sit, lct.Vencimento, lct.Pago, lct.Mez, ',
  'lct.Fornecedor, lct.Cliente, lct.CliInt, lct.ForneceI, ',
  'lct.DataDoc, lct.Duplicata, lct.Depto, lct.CtrlQuitPg, ',
  'lct.ID_Pgto, 0 ValAPag, 0 ValARec, 0 VTransf, ',
  '0 ValPgCre, 0 ValPgDeb, 0 ValPgTrf, ',
  '1 Ativo ',
  'FROM ' + FTabTemp + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira ',
  'WHERE Data < "' + FEmissIni + '" ',
  'AND car.Tipo=2 ',
  'AND (lct.Sit<2 ',
  'OR lct.Compensado >= "' + FEmissIni + '") ',
  '']));
  //
  // Lancamentos em emissoes durante o periodo da pesquisa.
  SQL := Geral.ATS([
  'INSERT INTO _fluxcxaopn_ ',
  'SELECT lct.Data, car.Tipo, car.PagRec, lct.Carteira, lct.Controle, lct.Sub, 0 SeqPag, ',
  'lct.Genero, lct.Descricao, lct.SerieNF, lct.NotaFiscal, ',
  'lct.Debito, lct.Credito, lct.Compensado, lct.SerieCH, ',
  'lct.Documento, lct.Sit, lct.Vencimento, lct.Pago, lct.Mez, ',
  'lct.Fornecedor, lct.Cliente, lct.CliInt, lct.ForneceI, ',
  'lct.DataDoc, lct.Duplicata, lct.Depto, lct.CtrlQuitPg, ',
  'lct.ID_Pgto, 0 ValAPag, 0 ValARec, 0 VTransf, ',
  '0 ValPgCre, 0 ValPgDeb, 0 ValPgTrf, ',
  '1 Ativo ',
  'FROM ' + FTabTemp + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira ',
  'WHERE Data BETWEEN "' + FEmissIni + '" AND "' + FEmissFim + '" ',
  'AND car.Tipo=2 ',
  '']);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  //
  // Excluir o controle zero pois veio junto e nao interessa.
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DELETE FROM _fluxcxaopn_ WHERE Controle=0',
  '']));
  //
  // Abrir os lancamentos de emissoes e trata-los conforme sua quitacao ou nao.
  UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM _fluxcxaopn_ ',
  'ORDER BY Data, Controle ',
  '']);
  PB1.Position := 0;
  PB1.Max := QrLcts.RecordCount;
  QrLcts.First;
  while not QrLcts.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    if (QrLctsTipo.Value = 2) then
    begin
      if ((QrLctsSit.Value < 2) and (QrLctsCompensado.Value > 2))
      or ((QrLctsSit.Value > 1) and (QrLctsCompensado.Value < 2)) then
      begin
        // Erro que pode estragar os saldos e somas.
        Geral.MB_ERRO('O lan�amento controle n�mero ' +
        Geral.FFI(QrLctsControle.Value) + ' est� quitado incorretamente!' +
        sLineBreak + '"Sit" n�o combina com "Compensado"' + sLineBreak +
        'Sit: ' + Geral.FF0(QrLctsSit.Value) + sLineBreak +
        'Compensado: ' + Geral.FDT(QrLctsCompensado.Value, 2) + sLineBreak +
        'Valor: ' +
        Geral.FFT(QrLctsCredito.Value - QrLctsDebito.Value, 2, siNegativo) +
        sLineBreak + 'Pago: ' + Geral.FFT(QrLctsPago.Value, 2, siNegativo));
        //
        Erros := Erros + 1;
      end;
      if (QrLctsSit.Value < 2)
      or ((QrLctsSit.Value > 1) and (QrLctsCompensado.Value > QrLctsData.Value)) then
      begin
        // Emissoes com pagamento parcial.
        // Somar apenas valores pagos antes do dia inicial da pesquisa.
        if ((QrLctsSit.Value = 1) and (QrLctsCompensado.Value < 2)) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrPago, DModG.MyPID_DB, [
          'SELECT SUM(Credito - Debito) Valor ',
          'FROM ' + FTabTemp,
          'WHERE ID_Pgto=' + Geral.FFI(QrLctsControle.Value),
          'AND Data<"' + FEmissIni + '"',
          '']);
          ValAPag  := 0;
          ValARec  := 0;
          // Nao pode!! Erro quando QrPagoValor.Value = 0
          //if QrPagoValor.Value > 0 then
          if (QrLctsCredito.Value - QrLctsDebito.Value > 0) then
            ValARec := QrLctsCredito.Value - QrLctsDebito.Value - QrPagoValor.Value
          else
            ValAPag := QrLctsCredito.Value + QrLctsDebito.Value + QrPagoValor.Value
        end else
        begin
          ValAPag  := QrLctsDebito.Value;
          ValARec  := QrLctsCredito.Value;
          //
        end;
        //VTransf ???
        Data     := Geral.FDT(QrLctsData.Value, 1);
        Tipo     := QrLctsTipo.Value;
        Pagrec   := QrLctsPagRec.Value;
        Carteira := QrLctsCarteira.Value;
        Controle := QrLctsControle.Value;
        Sub      := QrLctsSub.Value;
        SeqPag   := QrLctsSeqPag.Value;
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_fluxcxaopn_', False, [
        'ValAPag', 'ValARec', 'VTransf'], [
        'Data', 'Tipo', 'Pagrec',
        'Carteira', 'Controle', 'Sub', 'SeqPag'], [
        ValAPag, ValARec, VTransf], [
        Data, Tipo, PagRec,
        Carteira, Controle, Sub, SeqPag], False);
      end;
    end;
    //
    QrLcts.Next;
  end;
  if Erros > 0 then
    Exit;
  //
{ SQL que agrupa e explica quitacoes
SELECT Tipo, IF(ID_Pgto=0, 0, 1) _ID_Pgto,
IF(CtrlQuitPg=0, 0, 1) _CtrlQuitPg,
COUNT(Controle) ITENS
FROM lct0001a
GROUP BY Tipo, _ID_Pgto, _CtrlQuitPg
}

  //  Lancamentos Direto em caixa!!!
{
  SQL := Geral.ATS([
  'INSERT INTO _fluxcxaopn_ ',
  'SELECT lct.Data, car.Tipo, car.PagRec, lct.Carteira, lct.Controle, lct.Sub, 0 SeqPag, ',
  'lct.Genero, lct.Descricao, lct.SerieNF, lct.NotaFiscal, ',
  'lct.Debito, lct.Credito, lct.Compensado, lct.SerieCH, ',
  'lct.Documento, lct.Sit, lct.Vencimento, lct.Pago, lct.Mez, ',
  'lct.Fornecedor, lct.Cliente, lct.CliInt, lct.ForneceI, ',
  'lct.DataDoc, lct.Duplicata, lct.Depto, lct.CtrlQuitPg, ',
  'lct.ID_Pgto, 0 ValAPag, 0 ValARec, 0 VTransf, ',
  '0 ValPgCre, 0 ValPgDeb, 0 ValPgTrf, ',
  '1 Ativo ',
  'FROM ' + FTabLctA + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira ',
  'WHERE Data BETWEEN "' + FEmissIni + '" AND "' + FEmissFim + '" ',
  'AND car.Tipo=0 ',
  '']);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);

  // Lancamentos diretos em Banco ou quitacoes / pagamentos de emissoes!!!
  SQL := Geral.ATS([
  'INSERT INTO _fluxcxaopn_ ',
  'SELECT lct.Data, car.Tipo, car.PagRec, lct.Carteira, lct.Controle, lct.Sub, 0 SeqPag, ',
  'lct.Genero, lct.Descricao, lct.SerieNF, lct.NotaFiscal, ',
  'lct.Debito, lct.Credito, lct.Compensado, lct.SerieCH, ',
  'lct.Documento, lct.Sit, lct.Vencimento, lct.Pago, lct.Mez, ',
  'lct.Fornecedor, lct.Cliente, lct.CliInt, lct.ForneceI, ',
  'lct.DataDoc, lct.Duplicata, lct.Depto, lct.CtrlQuitPg, ',
  'lct.ID_Pgto, 0 ValAPag, 0 ValARec, 0 VTransf, ',
  '0 ValPgCre, 0 ValPgDeb, 0 ValPgTrf, ',
  '1 Ativo ',
  'FROM ' + FTabLctA + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira ',
  'WHERE Data BETWEEN "' + FEmissIni + '" AND "' + FEmissFim + '" ',
  'AND car.Tipo=1 ',
  '']);
  //Geral.MB_Aviso(SQL);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);

  // Abrir os lancamentos de caixas e bancos que quitaram emissoes para gerar
  // lancamentos que neutralizem estas emissoes (quando houver) que estao
  // atreladas a estes lancamentos em caixa e banco.
  // Usar campos especificos para isto! >>
  UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM _fluxcxaopn_ ',
  'WHERE Tipo < 2',
  'AND ID_Pgto <> 0 ',
  'ORDER BY Data, Controle ',
  '']);
  PB1.Position := 0;
  PB1.Max := QrLcts.RecordCount;
  QrLcts.First;
  while not QrLcts.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    if (QrLctsTipo.Value < 2) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLct2, DModG.MyPID_DB, [
      'SELECT * ',
      'FROM _fluxcxaopn_ ',
      'WHERE Controle =' + Geral.FF0(QrLctsID_Pgto.Value),
      '']);
      if QrLct2.RecordCount > 0 then
      begin
        Pagrec     := QrLct2PagRec.Value;
        Genero     := QrLct2Genero.Value;
        Descricao  := QrLct2Descricao.Value;
        SerieNF    := QrLct2SerieNF.Value;
        NotaFiscal := QrLct2NotaFiscal.Value;
        // 2014-04-11
        Debito     := -QrLctsCredito.Value;
        Credito    := -QrLctsDebito.Value;
        Compensado := Geral.FDT(QrLctsCompensado.Value, 1);
        SerieCH    := QrLct2SerieCH.Value;
        Documento  := QrLct2Documento.Value;
        Sit        := 2;
        Vencimento := Geral.FDT(QrLct2Vencimento.Value, 1);
        Pago       := QrLctsCredito.Value - QrLctsDebito.Value;
        Mez        := QrLct2Mez.Value;
        Fornecedor := QrLct2Fornecedor.Value;
        Cliente    := QrLct2Cliente.Value;
        CliInt     := QrLct2CliInt.Value;
        ForneceI   := QrLct2ForneceI.Value;
        DataDoc    := Geral.FDT(QrLct2DataDoc.Value, 1);
        Duplicata  := QrLct2Duplicata.Value;
        Depto      := QrLct2Depto.Value;
        CtrlQuitPg := QrLct2CtrlQuitPg.Value;
        ID_Pgto    := QrLctsControle.Value;
        ValAPag    := 0;
        ValARec    := 0;
        VTransf    := 0;
        //
        Data       := Geral.FDT(QrLctsCompensado.Value, 1);
        Tipo       := QrLct2Tipo.Value;
        Carteira   := QrLct2Carteira.Value;
        Controle   := QrLct2Controle.Value;
        Sub        := QrLct2Sub.Value;
        //Criado para evitar erro de primary key!!!
        SeqPag     := SeqPag + 1;
        //
        ValPgCre   := QrLctsCredito.Value;
        ValPgDeb   := QrLctsDebito.Value;
        ValPgTrf   := 0;
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_fluxcxaopn_', False, [
        'PagRec', 'Genero', 'Descricao',
        'SerieNF', 'NotaFiscal', 'Debito',
        'Credito', 'Compensado', 'SerieCH',
        'Documento', 'Sit', 'Vencimento',
        'Pago', 'Mez', 'Fornecedor',
        'Cliente', 'CliInt', 'ForneceI',
        'DataDoc', 'Duplicata', 'Depto',
        'CtrlQuitPg', 'ID_Pgto', 'ValAPag',
        'ValARec', 'VTransf',
        'ValPgCre', 'ValPgDeb', 'ValPgTrf',
        'SeqPag'
        ], [
        'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
        PagRec, Genero, Descricao,
        SerieNF, NotaFiscal, Debito,
        Credito, Compensado, SerieCH,
        Documento, Sit, Vencimento,
        Pago, Mez, Fornecedor,
        Cliente, CliInt, ForneceI,
        DataDoc, Duplicata, Depto,
        CtrlQuitPg, ID_Pgto, ValAPag,
        ValARec, VTransf,
        ValPgCre, ValPgDeb, ValPgTrf,
        SeqPag
        ], [
        Data, Tipo, Carteira, Controle, Sub], False);
      end else
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLct0, DMod.MyDB, [
        'SELECT * ',
        'FROM ' + FTabLctA,
        'WHERE Controle =' + Geral.FF0(QrLctsID_Pgto.Value),
        'AND (Data < "' + FEmissIni + '" ',
        'OR Data > "' + FEmissFim + '") ',
        '']);
        if QrLct0.RecordCount = 0 then
        begin
          Erros := Erros + 1;
          //
          Geral.MB_Aviso(
          'Origem de quita��o de lan�amento n�o localizada:'
          + sLineBreak + 'Controle Origem: ' + Geral.FF0(QrLctsID_Pgto.Value) +
          sLineBreak + 'Controle do pagemento: ' + Geral.FF0(QrLctsControle.Value));
        end;
      end;
    end;
    //
    QrLcts.Next;
  end;
  if Erros > 0 then
    Exit;
  //
   //  Dados base prontos.
  //  Preparar aorupamentos e suas somas.
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDias, DModG.MyPID_DB, [
  'SELECT DISTINCT Data FROM _fluxcxaopn_ ',
  'WHERE Data >="' + FEmissIni + '" ',
  'ORDER BY Data ',
  '']);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DELETE FROM _fluxcxadid_; ',
  '']));
  //
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DELETE FROM _fluxcxadis_; ',
  '']));
  //
  QrDias.First;
  while not QrDias.Eof do
  begin
    Data := Geral.FDT(QrDiasData.Value, 1);
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_fluxcxadid_', False, [
    'Data'], [], [
    Data], [], False);
    //
    // "Saldos" anteriores das carteiras tipo=2 emissoes.
    // Eh feito separado porque pode haver pagamento parcial
    // e porque na verdade sao os lancamentos em aberto
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumAnt, DModG.MyPID_DB, [
    'SELECT SUM(Credito) Credito, SUM(Debito) Debito, ',
    'SUM(VTransf) VTransf, ',
    'car.Tipo, car.PagRec, fcd.Carteira, ',
    'SUM(IF(Compensado<"1900-01-01" or Compensado>="' + Data + '", ',
    'ValAPag, 0)) ValAPag, ',
    'SUM(IF(Compensado<"1900-01-01" or Compensado>="' + Data + '", ',
    'ValARec, 0)) ValARec ',
    'FROM _fluxcxaopn_  fcd ',
    'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=fcd.Carteira ',
    'WHERE Data<"' + Data + '" ',
    'GROUP BY car.Tipo, car.Codigo ',
    //'ORDER BY car.Ordem, car.Nome',
    '']);
    //dmkPF.LeMeuTexto(QrSumAnt.SQL.Text);
    QrSumAnt.First;
    while not QrSumAnt.Eof do
    begin
      Tipo       := QrSumAntTipo.Value;
      PagRec     := QrSumAntPagRec.Value;
      Carteira   := QrSumAntCarteira.Value;
      ValAPagAnt := QrSumAntValAPag.Value;
      ValARecAnt := QrSumAntValARec.Value;
      VTransfAnt := QrSumAntVTransf.Value;
      ValAPagMov := 0;
      ValARecMov := 0;
      VTransfMov := 0;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_fluxcxadis_', False, [
      'Tipo', 'PagRec', 'Carteira', 'ValAPagAnt',
      'ValARecAnt', 'VTransfAnt', 'ValAPagMov',
      'ValARecMov', 'VTransfMov'], [
      'Data'], [
      Tipo, PagRec, Carteira, ValAPagAnt,
      ValARecAnt, VTransfAnt, ValAPagMov,
      ValARecMov, VTransfMov], [
      Data], False);
      //
      QrSumAnt.Next;
    end;
    //
    // Saldos anteriores das carteiras tipo 0=caixa e 1=banco
    UnDmkDAC_PF.AbreMySQLQuery0(QrSaldos, Dmod.MyDB, [
    'SELECT car.Tipo, car.PagRec, lct.Carteira, SUM(lct.Credito) Credito, ',
    'SUM(lct.Debito) Debito ',
    'FROM ' + FTabLctA + ' lct ',
    'LEFT JOIN carteiras car ON car.Codigo=lct.Carteira ',
    'WHERE car.Tipo <> 2 ',
    'AND lct.Data < "' + Data + '" ',
    'GROUP BY car.Tipo, car.Codigo ',
    '']);
    QrSaldos.First;
    while not QrSaldos.Eof do
    begin
      Tipo       := QrSaldosTipo.Value;
      PagRec     := QrSaldosPagRec.Value;
      Carteira   := QrSaldosCarteira.Value;
      ValAPagAnt := 0; //QrSaldosDebito.Value;
      ValARecAnt := 0; //QrSaldosCredito.Value;
      VTransfAnt := 0;//QrSaldosVTransf.Value;
      ValAPagMov := 0;
      ValARecMov := 0;
      VTransfMov := 0;
      EfetSdoAnt := QrSaldosCredito.Value - QrSaldosDebito.Value;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_fluxcxadis_', False, [
      'Tipo', 'PagRec', 'Carteira', 'ValAPagAnt',
      'ValARecAnt', 'VTransfAnt', 'ValAPagMov',
      'ValARecMov', 'VTransfMov',
      'EfetSdoAnt'], [
      'Data'], [
      Tipo, PagRec, Carteira, ValAPagAnt,
      ValARecAnt, VTransfAnt, ValAPagMov,
      ValARecMov, VTransfMov,
      EfetSdoAnt], [
      Data], False);
      //
      QrSaldos.Next;
    end;
    //
    // Movimento de todos tipos de carteira!!
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumMov, DModG.MyPID_DB, [
(*
    'SELECT SUM(Credito) Credito, SUM(Debito) Debito, SUM(ValAPag) ValAPag, ',
    'SUM(ValARec) ValARec, SUM(VTransf) VTransf, ',
    'car.Tipo, car.PagRec, fcd.Carteira ',
    'FROM _fluxcxaopn_  fcd ',
    'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=fcd.Carteira ',
    'WHERE Data="' + Data + '" ',
    'GROUP BY car.Tipo, car.Codigo ',
*)
    'SELECT SUM(Credito) Credito, ',
    'SUM(Debito) Debito, SUM(ValAPag) ValAPag, ',
    'SUM(ValARec) ValARec, SUM(VTransf) VTransf, ',
    'SUM(ValPgCre) ValPgCre, SUM(ValPgDeb) ValPgDeb, ',
    'car.Tipo, car.PagRec, fcd.Carteira,',
    'IF(fcd.SeqPag=0, 0, 1) TIPOMOV ',
    'FROM _fluxcxaopn_  fcd ',
    'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=fcd.Carteira ',
    'WHERE Data="' + Data + '" ',
    'GROUP BY car.Tipo, car.Codigo, TIPOMOV ',
    '']);
    QrSumMov.First;
    while not QrSumMov.Eof do
    begin
      Tipo       := QrSumMovTipo.Value;
      PagRec     := QrSumMovPagRec.Value;
      Carteira   := QrSumMovCarteira.Value;
      ValAPagAnt := 0;
      ValARecAnt := 0;
      VTransfAnt := 0;
      VTransfMov := 0;
      ValEmiPagC := 0;
      ValEmiPagD := 0;
      ValAPagMov := 0;
      ValARecMov := 0;
      EfetMovCre := 0;
      EfetMovDeb := 0;
      if QrSumMovTipo.Value = 2 then
      begin
        if QrSumMovTIPOMOV.Value = 0 then
        begin
          ValAPagMov := QrSumMovDebito.Value;
          ValARecMov := QrSumMovCredito.Value;
        end
        else
        begin
          ValEmiPagC := QrSumMovValPgCre.Value;
          ValEmiPagD := QrSumMovValPgDeb.Value;
        end;
      end else
      begin
        EfetMovCre := QrSumMovCredito.Value;
        EfetMovDeb := QrSumMovDebito.Value;
      end;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_fluxcxadis_', False, [
      'Tipo', 'PagRec', 'Carteira', 'ValAPagAnt',
      'ValARecAnt', 'VTransfAnt', 'ValAPagMov',
      'ValARecMov', 'VTransfMov',
      'EfetMovCre', 'EfetMovDeb',
      'ValEmiPagC', 'ValEmiPagD'], [
      'Data'], [
      Tipo, PagRec, Carteira, ValAPagAnt,
      ValARecAnt, VTransfAnt, ValAPagMov,
      ValARecMov, VTransfMov,
      EfetMovCre, EfetMovDeb,
      ValEmiPagC, ValEmiPagD], [
      Data], False);
      //
      QrSumMov.Next;
    end;
    //
    QrDias.Next;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFCD, DModG.MyPID_DB, [
  'SELECT DISTINCT Data FROM _fluxcxaopn_ ',
  'WHERE Data >="' + FEmissIni + '" ',
  'ORDER BY Data ',
  '']);
  //
  FDtaImp := Now(); //Geral.FDT(Now(), 107);
  FNO_Empresa := CBEmpresa.Text;
  if DataI <> DataF then
    FPeriodo := dmkPF.PeriodoImp2(DataI, DataF, True, True, 'Per�odo: ', '', ' ')
  else
    FPeriodo := dmkPF.PeriodoImp2(DataI, DataF, True, False, 'Data: ', '', ' ');
  BtAbertos.Enabled := True;
  //
}

  UnDmkDAC_PF.AbreMySQLQuery0(QrTots, DModG.MyPID_DB, [
  'SELECT COUNT(Controle) ITENS, ',
  'SUM(ValAPag) ValAPag, ',
  'SUM(ValARec) ValARec ',
  'FROM _fluxcxaopn_ ',
  'WHERE Data < "' + FEmissIni + '" ',
  '']);
  //
  if QrTots.FieldByName('ITENS').AsInteger = 0 then
  begin
     Geral.MB_Aviso('Nenhum lan�amento combina com o necess�rio!');
     Exit;
  end;
  if Geral.MB_Pergunta('Os seguintes totais ser�o relan�ados ap�s a exclus�o '+
  'de TODOS lan�amentos financeiros desta empresa.' + sLineBreak +
  'Quantidade de lan�amentos: ' +
  Geral.FF0(QrTots.FieldByName('ITENS').AsInteger) + sLineBreak +
  'Contas a receber: ' +
  Geral.FFT(QrTots.FieldByName('ValARec').AsFloat, 2, siNegativo) + sLineBreak +
  'Contas a pagar: ' +
  Geral.FFT(QrTots.FieldByName('ValAPag').AsFloat, 2, siNegativo) + sLineBreak +
  'Deseja continuar?') <> ID_YES then
    Exit;
  //
  if Geral.MB_Pergunta(
  'CONFIRMA a exclus�o de TODOS lan�amentos financeiros desta empresa?') <>
  ID_YES then
    Exit;
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
  'DELETE FROM ' + FTabLctA,
  '']));
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNew, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM _fluxcxaopn_ ',
  'WHERE Data < "' + FEmissIni + '" ',
  '']);
  PB1.Position := 0;
  PB1.Max := QrNew.RecordCount;
  QrNew.First;
  while not QrNew.Eof do
  begin
    InsereAtual;
    //
    QrNew.Next;
  end;
end;

procedure TFmReIniLctsSoComOpnEmiss.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReIniLctsSoComOpnEmiss.EdEmpresaChange(Sender: TObject);
var
  OK: Boolean;
begin
  OK := EdEmpresa.ValueVariant <> 0;
  BtCopia.Enabled := OK;
  BtExec.Enabled  := OK;
  if OK then
  begin
    FTabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, EdEmpresa.ValueVariant);
    FTabTemp := '_MIGRA_2014_Lct' + Geral.FFN(EdEmpresa.ValueVariant, 4) + '_A';
  end else
  begin
    FTabLctA := '';
    FTabTemp := '';
  end;
end;

procedure TFmReIniLctsSoComOpnEmiss.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmReIniLctsSoComOpnEmiss.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmReIniLctsSoComOpnEmiss.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmReIniLctsSoComOpnEmiss.InsereAtual;
var
  Controle: Integer;
begin
  MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
  //
  UFinanceiro.LancamentoDefaultVARS;
  //
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', FTabLctA, LAN_CTOS, 'Controle');
  VAR_LANCTO2 := Controle;
  //
  FLAN_EventosCad := 0;
  FLAN_Documento  := QrNewDocumento.Value;// Trunc(Documen);
  FLAN_Data       := FormatDateTime(VAR_FORMATDATE, QrNewData.Value);//dmkEdTPdata.Date);
  FLAN_Tipo       := QrNewTipo.Value;
  FLAN_Carteira   := QrNewCarteira.Value;
  FLAN_Credito    := QrNewValARec.Value;
  FLAN_Debito     := QrNewValAPag.Value;
  FLAN_Genero     := QrNewGenero.Value;

  FLAN_GenCtb     := GenCtb;
  FLAN_GenCtbD    := GenCtbD;
  FLAN_GenCtbC    := GenCtbC;

  FLAN_NotaFiscal := QrNewNotaFiscal.Value;
  FLAN_Vencimento := FormatDateTime(VAR_FORMATDATE, QrNewVencimento.Value);//Vencimento;
  FLAN_Mez        := QrNewMez.Value;
  FLAN_Descricao  := QrNewDescricao.Value;
  FLAN_Sit        := 0;
  FLAN_Controle   := Controle;
  FLAN_Cartao     := 0; //Cartao;
  //
  FLAN_Compensado := '0000-00-00';
  //
  FLAN_Linha      := 0; //Linha;
  FLAN_Fornecedor := QrNewFornecedor.Value;
  FLAN_Cliente    := QrNewCliente.Value;
  FLAN_MoraDia    := 0; //MoraDia;
  FLAN_Multa      := 0; //Multa;
  FLAN_DataCad    := FormatDateTime(VAR_FORMATDATE, Date); //QrNewDataCad.Value);
  FLAN_UserCad    := VAR_USUARIO; //QrNewUserCad.Value;
  FLAN_DataDoc    := FormatDateTime(VAR_FORMATDATE, QrNewDataDoc.Value); //dmkEdTPDataDoc.Date);
  FLAN_Vendedor   := 0;//Vendedor;
  FLAN_Account    := 0;//Account;
  FLAN_FatID      := 0; //QrNewFatID.Value;
  FLAN_FatID_Sub  := 0; //QrNewFatID_Sub.ValueVariant;
  FLAN_ICMS_P     := 0; //Geral.DMV(dmkEdICMS_P.Text);
  FLAN_ICMS_V     := 0; //Geral.DMV(dmkEdICMS_V.Text);
  FLAN_Duplicata  := QrNewDuplicata.Value;
  FLAN_CliInt     := QrNewCliInt.Value;
  FLAN_Depto      := QrNewDepto.Value;
  FLAN_DescoPor   := 0; //dmkEdDescoPor.ValueVariant;
  FLAN_ForneceI   := QrNewForneceI.Value;
  FLAN_DescoVal   := 0; //dmkEdDescoVal.ValueVariant;
  FLAN_NFVal      := 0; //dmkEdNFVAl.ValueVariant;
  FLAN_Unidade    := 0;//QrNewUnidade.Value;
  //FLAN_Qtde       := Qtde;  ABAIXO
  //FLAN_Qtd2       := Qtd2;  ABAIXO
  FLAN_FatNum     := 0; //QrNewFatNum.Value);
  FLAN_FatParcela := 0; //QrNewFatParcela.Value;
  FLAN_Doc2       := ''; //QrNewDoc2.Value;
  FLAN_SerieCH    := QrNewSerieCh.Value;
  FLAN_MultaVal   := 0; //MultaVal;
  FLAN_MoraVal    := 0; //MoraVal;
  FLAN_TipoCH     := 0; //QrNewTipoCH.Value;
  FLAN_IndiPag    := 0; //QrNewIndiPag.Value;
  FLAN_FisicoSrc  := 0; //RGFisicoSrc.ItemIndex;
  FLAN_FisicoCod  := 0; //EdFisicoCod.ValueVariant;
  // Evitar erro na duplica��o!
  //if ImgTipo.SQLType = stUpd then
  begin
    FLAN_ID_Pgto    := 0; //dmkEdID_Pgto.ValueVariant;
    FLAN_CNAB_Sit   := 0; //dmkEdCNAB_Sit.ValueVariant;
    FLAN_Nivel      := 0; //dmkEdNivel.ValueVariant;
    FLAN_CtrlIni    := 0; //dmkEdCtrlIni.ValueVariant;
  end;
  FLAN_Qtde       := 0; //QrNewQtde.Value;
  FLAN_Qtd2       := 0; //QrNewQtd2.Value;
  UFinanceiro.InsereLancamento(FTabLctA);
end;

end.
