unit PreLctExe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditDateTimePicker,
  mySQLDbTables, Vcl.Menus, dmkDBGridZTO;

type
  TFmPreLctExe = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtLanca: TBitBtn;
    LaTitulo1C: TLabel;
    QrPreLctIts: TmySQLQuery;
    QrPreLctItsCodigo: TIntegerField;
    QrPreLctItsControle: TIntegerField;
    QrPreLctItsCarteira: TIntegerField;
    QrPreLctItsGenero: TIntegerField;
    QrPreLctItsDiaVencto: TIntegerField;
    QrPreLctItsCliInt: TIntegerField;
    QrPreLctItsCliente: TIntegerField;
    QrPreLctItsFornecedor: TIntegerField;
    QrPreLctItsMez: TIntegerField;
    QrPreLctItsDebito: TFloatField;
    QrPreLctItsCredito: TFloatField;
    QrPreLctItsDescricao: TWideStringField;
    QrPreLctItsLk: TIntegerField;
    QrPreLctItsDataCad: TDateField;
    QrPreLctItsDataAlt: TDateField;
    QrPreLctItsUserCad: TIntegerField;
    QrPreLctItsUserAlt: TIntegerField;
    QrPreLctItsAlterWeb: TSmallintField;
    QrPreLctItsAtivo: TSmallintField;
    QrPreLctItsNO_Carteira: TWideStringField;
    QrPreLctItsNOMECONTA: TWideStringField;
    QrPreLctItsNOMEEMPRESA: TWideStringField;
    QrPreLctItsNOMECLIENTE: TWideStringField;
    QrPreLctItsNOMEFORNECEDOR: TWideStringField;
    QrPreLctItsNOMETERCEIRO: TWideStringField;
    DsPreLctIts: TDataSource;
    Panel5: TPanel;
    LaEmissIni: TLabel;
    TPData: TdmkEditDateTimePicker;
    PMLanca: TPopupMenu;
    Selecionados1: TMenuItem;
    odos1: TMenuItem;
    PB1: TProgressBar;
    QrCarteira: TmySQLQuery;
    QrConta: TmySQLQuery;
    QrCarteiraCodigo: TIntegerField;
    QrCarteiraNome: TWideStringField;
    QrCarteiraFatura: TWideStringField;
    QrCarteiraFechamento: TIntegerField;
    QrCarteiraPrazo: TSmallintField;
    QrCarteiraTipo: TIntegerField;
    QrCarteiraExigeNumCheque: TSmallintField;
    QrCarteiraForneceI: TIntegerField;
    QrCarteiraBanco1: TIntegerField;
    QrCarteiraUsaTalao: TSmallintField;
    QrContaCodigo: TIntegerField;
    QrContaNome: TWideStringField;
    QrContaNome2: TWideStringField;
    QrContaNome3: TWideStringField;
    QrContaID: TWideStringField;
    QrContaSubgrupo: TIntegerField;
    QrContaCentroCusto: TIntegerField;
    QrContaEmpresa: TIntegerField;
    QrContaCredito: TWideStringField;
    QrContaDebito: TWideStringField;
    QrContaMensal: TWideStringField;
    QrContaExclusivo: TWideStringField;
    QrContaMensdia: TSmallintField;
    QrContaMensdeb: TFloatField;
    QrContaMensmind: TFloatField;
    QrContaMenscred: TFloatField;
    QrContaMensminc: TFloatField;
    QrContaTerceiro: TIntegerField;
    QrContaExcel: TWideStringField;
    QrContaRateio: TIntegerField;
    QrContaEntidade: TIntegerField;
    QrContaAntigo: TWideStringField;
    QrContaPendenMesSeg: TSmallintField;
    QrContaCalculMesSeg: TSmallintField;
    QrContaOrdemLista: TIntegerField;
    QrContaContasAgr: TIntegerField;
    QrContaContasSum: TIntegerField;
    QrContaCtrlaSdo: TSmallintField;
    QrContaNotPrntBal: TIntegerField;
    QrContaSigla: TWideStringField;
    QrContaProvRat: TSmallintField;
    QrContaNotPrntFin: TIntegerField;
    QrContaTemDocFisi: TSmallintField;
    QrContaLk: TIntegerField;
    QrContaDataCad: TDateField;
    QrContaDataAlt: TDateField;
    QrContaUserCad: TIntegerField;
    QrContaUserAlt: TIntegerField;
    QrContaAlterWeb: TSmallintField;
    QrContaAtivo: TSmallintField;
    DGDados: TdmkDBGridZTO;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtLancaClick(Sender: TObject);
    procedure odos1Click(Sender: TObject);
    procedure Selecionados1Click(Sender: TObject);
  private
    { Private declarations }
    FAno, FMes, FDia: Word;
    FDataDta: TDateTime;
    FDataTxt: String;
    //
    function  TudoOK(): Boolean;
    procedure InsereAtual();
  public
    { Public declarations }
    FCodigo, FCliInt: Integer;
    FTabLctA: String;
  end;

  var
  FmPreLctExe: TFmPreLctExe;

implementation

uses UnMyObjects, Module, UnFinanceiro, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmPreLctExe.BtLancaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLanca, BtLanca);
end;

procedure TFmPreLctExe.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPreLctExe.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPreLctExe.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPreLctExe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPreLctExe.InsereAtual();
var
  Controle, Mez, Sit, Ano, Mes: Integer;
  Vencimento, MyDta: TDateTime;
begin
  MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCarteira, Dmod.MyDB, [
  'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, ',
  'Tipo, ExigeNumCheque, ForneceI, Banco1, UsaTalao ',
  'FROM carteiras ',
  'WHERE Codigo=' + Geral.FF0(QrPreLctItsCarteira.Value),
  '']);
  case QrCarteiraTipo.Value of
    0, 1: Sit := 2;
    2: Sit := 0;
    else Sit := 0;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrConta, Dmod.MyDB, [
  'SELECT co.* ',
  'FROM contas co ',
  'WHERE co.Codigo=' + Geral.FF0(QrPreLctItsGenero.Value),
  '']);
  //
  if QrContaMensal.Value = 'V' then
  begin
    //MyDta := IncMonth(FdataDta, QrPreLctItsMez.Value -1);
    //MyDta := IncMonth(FdataDta, QrPreLctItsMez.Value -1);
    Mez := Geral.IMV(Geral.DataToMez(FdataDta));
  end else
    Mez := 0;
  //
  Ano := FAno;
  Mes := FMes + QrPreLctItsMez.Value -1;
{
  if Mes > 12 then
  begin
    Mes := Mes - 12;
    Ano := Ano + 1;
  end;
  Vencimento := EncodeDate(Ano, Mes, QrPreLctItsDiaVencto.Value);
}
  Vencimento := Geral.EncodeDateSafe(Ano, Mes, QrPreLctItsDiaVencto.Value, detLastDaySameMonth);
  //
  UFinanceiro.LancamentoDefaultVARS;
  //
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', FTabLctA, LAN_CTOS, 'Controle');
  VAR_LANCTO2 := Controle;
  //
  FLAN_EventosCad := 0;
  FLAN_Documento  := 0;// Trunc(Documen);
  FLAN_Data       := FDataTxt;//dmkEdTPdata.Date);
  FLAN_Tipo       := QrCarteiraTipo.Value;
  FLAN_Carteira   := QrCarteiraCodigo.Value;
  FLAN_Credito    := QrPreLctItsCredito.Value;
  FLAN_Debito     := QrPreLctItsDebito.Value;
  FLAN_Genero     := QrPreLctItsGenero.Value;
  FLAN_GenCtb     := ?;
  FLAN_GenCtbD    := ?;
  FLAN_GenCtbC    := ?;
  FLAN_NotaFiscal := 0;
  FLAN_Vencimento := FormatDateTime(VAR_FORMATDATE, Vencimento);//Vencimento;
  FLAN_Mez        := Mez;
  FLAN_Descricao  := QrPreLctItsDescricao.Value;
  FLAN_Sit        := Sit;
  FLAN_Controle   := Controle;
  FLAN_Cartao     := 0; //Cartao;
  //
  FLAN_Compensado := '0000-00-00';
  //
  FLAN_Linha      := 0; //Linha;
  FLAN_Fornecedor := QrPreLctItsFornecedor.Value;
  FLAN_Cliente    := QrPreLctItsCliente.Value;
  FLAN_MoraDia    := 0; //MoraDia;
  FLAN_Multa      := 0; //Multa;
  FLAN_DataCad    := FormatDateTime(VAR_FORMATDATE, Date); //QrNewDataCad.Value);
  FLAN_UserCad    := VAR_USUARIO; //QrNewUserCad.Value;
  FLAN_DataDoc    := FLAN_Data; //FormatDateTime(VAR_FORMATDATE, QrNewDataDoc.Value); //dmkEdTPDataDoc.Date);
  FLAN_Vendedor   := 0;//Vendedor;
  FLAN_Account    := 0;//Account;
  FLAN_FatID      := 0; //QrNewFatID.Value;
  FLAN_FatID_Sub  := 0; //QrNewFatID_Sub.ValueVariant;
  FLAN_ICMS_P     := 0; //Geral.DMV(dmkEdICMS_P.Text);
  FLAN_ICMS_V     := 0; //Geral.DMV(dmkEdICMS_V.Text);
  FLAN_Duplicata  := ''; //QrNewDuplicata.Value;
  FLAN_CliInt     := FCliInt; //QrNewCliInt.Value;
  FLAN_Depto      := 0; //QrNewDepto.Value;
  FLAN_DescoPor   := 0; //dmkEdDescoPor.ValueVariant;
  FLAN_ForneceI   := 0; //QrNewForneceI.Value;
  FLAN_DescoVal   := 0; //dmkEdDescoVal.ValueVariant;
  FLAN_NFVal      := 0; //dmkEdNFVAl.ValueVariant;
  FLAN_Unidade    := 0;//QrNewUnidade.Value;
  //FLAN_Qtde       := Qtde;  ABAIXO
  //FLAN_Qtd2       := Qtd2;  ABAIXO
  FLAN_FatNum     := 0; //QrNewFatNum.Value);
  FLAN_FatParcela := 0; //QrNewFatParcela.Value;
  FLAN_Doc2       := ''; //QrNewDoc2.Value;
  FLAN_SerieCH    := ''; //QrNewSerieCh.Value;
  FLAN_MultaVal   := 0; //MultaVal;
  FLAN_MoraVal    := 0; //MoraVal;
  FLAN_TipoCH     := 0; //QrNewTipoCH.Value;
  FLAN_IndiPag    := 0; //QrNewIndiPag.Value;
  FLAN_FisicoSrc  := 0; //RGFisicoSrc.ItemIndex;
  FLAN_FisicoCod  := 0; //EdFisicoCod.ValueVariant;
  // Evitar erro na duplicação!
  //if ImgTipo.SQLType = stUpd then
  begin
    FLAN_ID_Pgto    := 0; //dmkEdID_Pgto.ValueVariant;
    FLAN_CNAB_Sit   := 0; //dmkEdCNAB_Sit.ValueVariant;
    FLAN_Nivel      := 0; //dmkEdNivel.ValueVariant;
    FLAN_CtrlIni    := 0; //dmkEdCtrlIni.ValueVariant;
  end;
  FLAN_Qtde       := 0; //QrNewQtde.Value;
  FLAN_Qtd2       := 0; //
  UFinanceiro.InsereLancamento(FTabLctA);
  UFinanceiro.AtualizaEmissaoMasterRapida(Controle, Sit, 0, '', FTabLctA);
end;

procedure TFmPreLctExe.odos1Click(Sender: TObject);
var
  N: Integer;
begin
  N := 0;
  if not TudoOK() then
    Exit;
  if Geral.MB_Pergunta('Confirma o lançamento de todos itens?') = ID_YES then
  begin
    PB1.Position := 0;
    PB1.Max := QrPreLctIts.RecordCount;
    QrPreLctIts.First;
    while not QrPreLctIts.Eof do
    begin
      InsereAtual();
      N := N + 1;
      QrPreLctIts.Next;
    end;
    Geral.MB_Info(Geral.FF0(N) + ' lançamentos foram realizados!');
  end;
end;

procedure TFmPreLctExe.Selecionados1Click(Sender: TObject);
var
  I, N: Integer;
begin
  if DGDados.SelectedRows.Count >= 1 then
  begin
    N := 0;
    if not TudoOK() then
      Exit;
    if Geral.MB_Pergunta('Confirma o lançamento dos itens selecionados?') = ID_YES then
    begin
      PB1.Position := 0;
      PB1.Max := DGDados.SelectedRows.Count;
      with DGDados.DataSource.DataSet do
      for I := 0 to DGDados.SelectedRows.Count-1 do
      begin
        //GotoBookmark(Pointer(DGDados.SelectedRows.Items[I]));
        GotoBookmark(DGDados.SelectedRows.Items[I]);
        InsereAtual();
        N := N + 1;
      end;
      Geral.MB_Pergunta(Geral.FF0(N) + ' lançamentos foram realizados!');
    end;
  end;
end;

function TFmPreLctExe.TudoOK(): Boolean;
begin
  Result := False;
  FDataDta := 0;
  FAno := 0;
  FMes := 0;
  FDia := 0;
  FDataTxt := '?????';
  //
  if TPData.Date < 2 then
  begin
    Geral.MB_Aviso('Informe a data de lançamento!');
    Exit;
  end;
  FDataDta := TPData.Date;
  FDataTxt := FormatDateTime(VAR_FORMATDATE, FDataDta);
  DecodeDate(FDataDta, FAno, FMes, FDia);
  //
  Result := True;
end;

end.
