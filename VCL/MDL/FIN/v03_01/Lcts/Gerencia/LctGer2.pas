unit LctGer2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkEdit,
  ComCtrls, Mask, DBCtrls, dmkDBGrid, dmkEditDateTimePicker, dmkGeral,
  UnFinanceiro, DB, mySQLDbTables, Menus, dmkEditCB, dmkImage, dmkPermissoes,
  dmkCompoStore, dmkDBGridZTO, UnDmkEnums, UnDmkProcFunc, UnAppPF,
  dmkDBLookupComboBox, dmkDBGridTRB, UnGrl_Vars;

type
  THackDBGrid = class(TDBGrid);
  TFmLctGer2 = class(TForm)
    Panel1: TPanel;
    PnLct: TPanel;
    Shape1: TShape;
    Label14: TLabel;
    DBGLct: TdmkDBGridZTO;
    PainelDados2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    PMTrfCta: TPopupMenu;
    Novatransferncia1: TMenuItem;
    Alteratransferncia1: TMenuItem;
    Excluitransferncia1: TMenuItem;
    Localizatransferncia1: TMenuItem;
    N1: TMenuItem;
    Entrecarteitas1: TMenuItem;
    Entrecontas1: TMenuItem;
    Novatransfernciaentrecontas1: TMenuItem;
    Alteratransfernciaentrecontas1: TMenuItem;
    Excluitransfernciaentrecontas1: TMenuItem;
    PMSaldoAqui: TPopupMenu;
    Calcula1: TMenuItem;
    Limpa1: TMenuItem;
    Diferena1: TMenuItem;
    PMMenu: TPopupMenu;
    Transferir1: TMenuItem;
    Quitar1: TMenuItem;
    Compensar1: TMenuItem;
    Reverter1: TMenuItem;
    N10: TMenuItem;
    Pagar1: TMenuItem;
    N11: TMenuItem;
    PagarAVista1: TMenuItem;
    N19: TMenuItem;
    Localizarlanamentoorigem1: TMenuItem;
    Lanamento1: TMenuItem;
    Localizar2: TMenuItem;
    Copiar1: TMenuItem;
    Recibo1: TMenuItem;
    Mudacarteiradelanamentosselecionados1: TMenuItem;
    Carteiras1: TMenuItem;
    Data1: TMenuItem;
    Compensao1: TMenuItem;
    Transformaremitemdebloqueto1: TMenuItem;
    Acertarcreditopelovalorpago1: TMenuItem;
    Colocarmsdecompetnciaondenotem1: TMenuItem;
    Datalancto1: TMenuItem;
    Vencimento1: TMenuItem;
    MsanterioraoVencimento1: TMenuItem;
    N18: TMenuItem;
    Exclusoincondicional1: TMenuItem;
    Entrecarteiras1: TMenuItem;
    Entrecontas2: TMenuItem;
    Incluinovatransfernciaentrecarteiras1: TMenuItem;
    Alteratransfernciaentrecarteirasatual1: TMenuItem;
    Excluitransfernciaentrecarteirasatual1: TMenuItem;
    N2: TMenuItem;
    Localizatransfernciaentrecarteiras1: TMenuItem;
    Incluinovatransfernciaentrecontas1: TMenuItem;
    Alteratransfernciaentrecontasatual1: TMenuItem;
    Excluitransfernciaentrecontasatual1: TMenuItem;
    Label1: TLabel;
    PMQuita: TPopupMenu;
    Compensar2: TMenuItem;
    Reverter2: TMenuItem;
    N12: TMenuItem;
    Pagar2: TMenuItem;
    PagarAVista2: TMenuItem;
    Localizarlanamentoorigem2: TMenuItem;
    N3: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel10: TPanel;
    GBAcoes: TGroupBox;
    PnAcoesTravaveis: TPanel;
    BtMenu: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtQuita: TBitBtn;
    BtDuplica: TBitBtn;
    BtConcilia: TBitBtn;
    Panel7: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGCrt: TdmkDBGrid;
    TabSheet2: TTabSheet;
    Panel8: TPanel;
    Label20: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    EdSoma: TdmkEdit;
    BtSomaLinhas: TBitBtn;
    EdCred: TdmkEdit;
    EdDebi: TdmkEdit;
    TabSheet3: TTabSheet;
    Panel14: TPanel;
    GroupBox1: TGroupBox;
    BtSaldoAqui: TBitBtn;
    EdSdoAqui: TdmkEdit;
    GroupBox2: TGroupBox;
    LaDiferenca: TLabel;
    LaSaldo: TLabel;
    LaCaixa: TLabel;
    EdSaldo: TDBEdit;
    EdDiferenca: TDBEdit;
    EdCaixa: TDBEdit;
    BtContarDinheiro: TBitBtn;
    Panel15: TPanel;
    GroupBox7: TGroupBox;
    Panel12: TPanel;
    BtPlaCtas: TBitBtn;
    BtCheque: TBitBtn;
    BtSaldoTotal: TBitBtn;
    BtConfPagtosCad: TBitBtn;
    BtConfPgtosExe: TBitBtn;
    BtRefresh: TBitBtn;
    BtDesfazOrdenacao: TBitBtn;
    GroupBox8: TGroupBox;
    Panel13: TPanel;
    BtRelatorios: TBitBtn;
    GroupBox5: TGroupBox;
    Label8: TLabel;
    Label5: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    RGTipoData: TRadioGroup;
    GroupBox4: TGroupBox;
    Label17: TLabel;
    BtLocCtrl: TBitBtn;
    EdLocCtrl: TdmkEditCB;
    PnAcoesNoLock: TPanel;
    BtTrfCta: TBitBtn;
    BtLocaliza: TBitBtn;
    PMConfPagtosCad: TPopupMenu;
    ContasMensaisCadastrodePagamentos1: TMenuItem;
    ContasMensaisCadastrodeEmisses1: TMenuItem;
    PMConfPgtosExe: TPopupMenu;
    Pagamentosexecutados1: TMenuItem;
    Contasmensais1: TMenuItem;
    BtBloqueto: TBitBtn;
    PMBloqueto: TPopupMenu;
    Gerabloqueto1: TMenuItem;
    ImprimeGerenciabloqueto1: TMenuItem;
    BtProtocolo: TBitBtn;
    PMProtocolo: TPopupMenu;
    Adicionalanamentos1: TMenuItem;
    Atual4: TMenuItem;
    Selecionados5: TMenuItem;
    UmporDocumento1: TMenuItem;
    UmparacadaLanamento1: TMenuItem;
    Localizalote1: TMenuItem;
    BtSalvaPosicaoGrade: TBitBtn;
    PMSalvaPosicaoGrade: TPopupMenu;
    SalvarOrdenaodascolunasdagrade2: TMenuItem;
    CarregarOrdenaopadrodascolunasdagrade1: TMenuItem;
    BtAutom: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    PMRelatorios: TPopupMenu;
    Balancete1: TMenuItem;
    Extratos1: TMenuItem;
    PagarReceber2: TMenuItem;
    Movimento2: TMenuItem;
    ResultadosMensais2: TMenuItem;
    Pesquisas1: TMenuItem;
    PorNveldoPLanodeContas2: TMenuItem;
    ContasControladas2: TMenuItem;
    Contassazonais2: TMenuItem;
    Saldos1: TMenuItem;
    Futuros2: TMenuItem;
    Em2: TMenuItem;
    Listas1: TMenuItem;
    Contas2: TMenuItem;
    Diversos1: TMenuItem;
    Etiquetas2: TMenuItem;
    Formulrios2: TMenuItem;
    Mensalidades2: TMenuItem;
    Eventos1: TMenuItem;
    BtFluxCxaDia: TBitBtn;
    ReceitaseDespesas21: TMenuItem;
    PrestaodeContas1: TMenuItem;
    Relatriodecheques1: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    Vencimento2: TMenuItem;
    Descrio1: TMenuItem;
    N9: TMenuItem;
    BtCalculadora: TBitBtn;
    Lanamentos1: TMenuItem;
    BtCopiaCH: TBitBtn;
    PMCopiaCH: TPopupMenu;
    Cpiadecheque1: TMenuItem;
    CpiadechequeNovo1: TMenuItem;
    Cpiadedbitoemconta1: TMenuItem;
    N23: TMenuItem;
    Cpiaautomtica1: TMenuItem;
    BtFluxoCxa: TBitBtn;
    Correes1: TMenuItem;
    Importaes1: TMenuItem;
    N14: TMenuItem;
    Salrios1: TMenuItem;
    Modelo01RelExactus1: TMenuItem;
    Modelo02txtPholha1: TMenuItem;
    BtRecibo: TBitBtn;
    BtArrecada: TBitBtn;
    BtProvisoes: TBitBtn;
    PMProvisoes: TPopupMenu;
    Agendamentodeproviso1: TMenuItem;
    Pesquisaagendamentodeprovisoefetivado1: TMenuItem;
    Lanamentoscomproblemas1: TMenuItem;
    Pagamentorpido2: TMenuItem;
    N13: TMenuItem;
    Pagamentorpido1: TMenuItem;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    PMRecibo: TPopupMenu;
    partirdolanamentoselecionado1: TMenuItem;
    Avulso1: TMenuItem;
    Receitasedespesas1: TMenuItem;
    ModeloA1: TMenuItem;
    ModeloB1: TMenuItem;
    MovimentoPlanodecontas1: TMenuItem;
    ModeloA2: TMenuItem;
    ModeloB2: TMenuItem;
    Saldos2: TMenuItem;
    N15: TMenuItem;
    Conta1: TMenuItem;
    PagarReceber31: TMenuItem;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    Label18: TLabel;
    QrEntidades: TMySQLQuery;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesCodigo: TIntegerField;
    DsEntidades: TDataSource;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    CkCredito: TCheckBox;
    CkDebito: TCheckBox;
    CkGenese: TCheckBox;
    CkSuplente: TCheckBox;
    CkAVencer: TCheckBox;
    CkQuitado: TCheckBox;
    CkVencido: TCheckBox;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    BtReabre: TBitBtn;
    Panel11: TPanel;
    EdCarteiraPsq: TdmkEdit;
    Label6: TLabel;
    EdCarteira: TdmkEdit;
    Multiplo1: TMenuItem;
    Recebepagamentoeimprime1: TMenuItem;
    JaneladeRecibos1: TMenuItem;
    ImprimirReciboCriado1: TMenuItem;
    QrRecibos: TMySQLQuery;
    QrRecibosCodigo: TIntegerField;
    QrRecibosControle: TIntegerField;
    DsRecibos: TDataSource;
    MudarvencimentoLocao1: TMenuItem;
    Pagtorpidoselbanco1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TPDataIniChange(Sender: TObject);
    procedure TPDataFimChange(Sender: TObject);
    procedure RGTipoDataClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtDesfazOrdenacaoClick(Sender: TObject);
    procedure BtDuplicaClick(Sender: TObject);
    procedure BtTrfCtaClick(Sender: TObject);
    procedure Novatransferncia1Click(Sender: TObject);
    procedure Alteratransferncia1Click(Sender: TObject);
    procedure Excluitransferncia1Click(Sender: TObject);
    procedure Localizatransferncia1Click(Sender: TObject);
    procedure Alteratransfernciaentrecontas1Click(Sender: TObject);
    procedure Excluitransfernciaentrecontas1Click(Sender: TObject);
    procedure Novatransfernciaentrecontas1Click(Sender: TObject);
    procedure BtSaldoAquiClick(Sender: TObject);
    procedure Calcula1Click(Sender: TObject);
    procedure Limpa1Click(Sender: TObject);
    procedure Diferena1Click(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure Incluinovatransfernciaentrecarteiras1Click(Sender: TObject);
    procedure Alteratransfernciaentrecarteirasatual1Click(Sender: TObject);
    procedure Excluitransfernciaentrecarteirasatual1Click(Sender: TObject);
    procedure Localizatransfernciaentrecarteiras1Click(Sender: TObject);
    procedure Incluinovatransfernciaentrecontas1Click(Sender: TObject);
    procedure Alteratransfernciaentrecontasatual1Click(Sender: TObject);
    procedure Excluitransfernciaentrecontasatual1Click(Sender: TObject);
    procedure Compensar1Click(Sender: TObject);
    procedure Reverter1Click(Sender: TObject);
    procedure Pagar1Click(Sender: TObject);
    procedure PagarAVista1Click(Sender: TObject);
    procedure Localizarlanamentoorigem1Click(Sender: TObject);
    procedure DBGLctDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGLctKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBGLctDblClick(Sender: TObject);
    procedure BtSomaLinhasClick(Sender: TObject);
    procedure BtContarDinheiroClick(Sender: TObject);
    procedure BtQuitaClick(Sender: TObject);
    procedure Compensar2Click(Sender: TObject);
    procedure Reverter2Click(Sender: TObject);
    procedure Pagar2Click(Sender: TObject);
    procedure PMQuitaPopup(Sender: TObject);
    procedure Localizarlanamentoorigem2Click(Sender: TObject);
    procedure PagarAVista2Click(Sender: TObject);
    procedure Copiar1Click(Sender: TObject);
    procedure Localizar2Click(Sender: TObject);
    procedure Recibo1Click(Sender: TObject);
    procedure Carteiras1Click(Sender: TObject);
    procedure Data1Click(Sender: TObject);
    procedure Compensao1Click(Sender: TObject);
    procedure Transformaremitemdebloqueto1Click(Sender: TObject);
    procedure Acertarcreditopelovalorpago1Click(Sender: TObject);
    procedure Datalancto1Click(Sender: TObject);
    procedure Vencimento1Click(Sender: TObject);
    procedure MsanterioraoVencimento1Click(Sender: TObject);
    procedure Exclusoincondicional1Click(Sender: TObject);
    procedure BtConciliaClick(Sender: TObject);
    procedure BtSaldoTotalClick(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure BtRelatoriosClick(Sender: TObject);
    procedure BtReciboClick(Sender: TObject);
    procedure BtLocCtrlClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtLocalizaClick(Sender: TObject);
    procedure BtPlaCtasClick(Sender: TObject);
    procedure BtChequeClick(Sender: TObject);
    procedure BtConfPagtosCadClick(Sender: TObject);
    procedure BtConfPgtosExeClick(Sender: TObject);
    procedure ContasMensaisCadastrodePagamentos1Click(Sender: TObject);
    procedure ContasMensaisCadastrodeEmisses1Click(Sender: TObject);
    procedure Pagamentosexecutados1Click(Sender: TObject);
    procedure Contasmensais1Click(Sender: TObject);
    procedure TPDataIniClick(Sender: TObject);
    procedure TPDataFimClick(Sender: TObject);
    procedure BtBloquetoClick(Sender: TObject);
    procedure Gerabloqueto1Click(Sender: TObject);
    procedure ImprimeGerenciabloqueto1Click(Sender: TObject);
    procedure PMBloquetoPopup(Sender: TObject);
    procedure BtProtocoloClick(Sender: TObject);
    procedure Atual4Click(Sender: TObject);
    procedure Localizalote1Click(Sender: TObject);
    procedure UmporDocumento1Click(Sender: TObject);
    procedure UmparacadaLanamento1Click(Sender: TObject);
    procedure BtSalvaPosicaoGradeClick(Sender: TObject);
    procedure SalvarOrdenaodascolunasdagrade2Click(Sender: TObject);
    procedure CarregarOrdenaopadrodascolunasdagrade1Click(Sender: TObject);
    procedure BtAutomClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Balancete1Click(Sender: TObject);
    procedure PagarReceber2Click(Sender: TObject);
    procedure Movimento2Click(Sender: TObject);
    procedure ResultadosMensais2Click(Sender: TObject);
    procedure ContasControladas2Click(Sender: TObject);
    procedure Contassazonais2Click(Sender: TObject);
    procedure Futuros2Click(Sender: TObject);
    procedure Em2Click(Sender: TObject);
    procedure Contas2Click(Sender: TObject);
    procedure Etiquetas2Click(Sender: TObject);
    procedure Formulrios2Click(Sender: TObject);
    procedure Mensalidades2Click(Sender: TObject);
    procedure Eventos1Click(Sender: TObject);
    procedure BtFluxCxaDiaClick(Sender: TObject);
    procedure PrestaodeContas1Click(Sender: TObject);
    procedure Relatriodecheques1Click(Sender: TObject);
    procedure Vencimento2Click(Sender: TObject);
    procedure Descrio1Click(Sender: TObject);
    procedure BtCalculadoraClick(Sender: TObject);
    procedure Lanamentos1Click(Sender: TObject);
    procedure Cpiaautomtica1Click(Sender: TObject);
    procedure BtCopiaCHClick(Sender: TObject);
    procedure Cpiadedbitoemconta1Click(Sender: TObject);
    procedure CpiadechequeNovo1Click(Sender: TObject);
    procedure Cpiadecheque1Click(Sender: TObject);
    procedure BtFluxoCxaClick(Sender: TObject);
    procedure Modelo01RelExactus1Click(Sender: TObject);
    procedure Modelo02txtPholha1Click(Sender: TObject);
    procedure Agendamentodeproviso1Click(Sender: TObject);
    procedure Pesquisaagendamentodeprovisoefetivado1Click(Sender: TObject);
    procedure BtProvisoesClick(Sender: TObject);
    procedure BtArrecadaClick(Sender: TObject);
    procedure Lanamentoscomproblemas1Click(Sender: TObject);
    procedure Pagamentorpido2Click(Sender: TObject);
    procedure Pagamentorpido1Click(Sender: TObject);
    procedure partirdolanamentoselecionado1Click(Sender: TObject);
    procedure PMReciboPopup(Sender: TObject);
    procedure Avulso1Click(Sender: TObject);
    procedure Receitasedespesas1Click(Sender: TObject);
    procedure ModeloA1Click(Sender: TObject);
    procedure ModeloB1Click(Sender: TObject);
    procedure MovimentoPlanodecontas1Click(Sender: TObject);
    procedure ModeloA2Click(Sender: TObject);
    procedure ModeloB2Click(Sender: TObject);
    procedure Saldos2Click(Sender: TObject);
    procedure Conta1Click(Sender: TObject);
    procedure PagarReceber31Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEntidadeRedefinido(Sender: TObject);
    procedure CkCreditoClick(Sender: TObject);
    procedure CkDebitoClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure EdCarteiraPsqRedefinido(Sender: TObject);
    procedure DBGCrtDblClick(Sender: TObject);
    procedure EdCarteiraRedefinido(Sender: TObject);
    procedure JaneladeRecibos1Click(Sender: TObject);
    procedure Recebepagamentoeimprime1Click(Sender: TObject);
    procedure ImprimirReciboCriado1Click(Sender: TObject);
    procedure MudarvencimentoLocao1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Pagtorpidoselbanco1Click(Sender: TObject);
  private
    { Private declarations }
    FThisEntidade, FThisCliInt, FThisFilial: Integer;
    FFatIDHabilitaByCrt, FFatIDHabilitaByLct: Boolean;
    FLctFinalidade: TLanctoFinalidade;
    //
    FMaxTrava: Integer;
    FArrTrava: array of TWinControl;
    //
    procedure VeSeReabreLct(Forca: Boolean = False);
    procedure InsAlt(Acao: TGerencimantoDeRegistro);
    procedure RecalcSaldoCarteira();
    procedure TranferenciaEntreCarteiras(Tipo: Integer);
    procedure TranferenciaEntreContas(Tipo: Integer);
    procedure PagarRolarEmissao();
    procedure LocalizarOLan�amentoDeOrigem();
    procedure ConfiguraPopoupQuitacoes();
    procedure PagamentoRapido();
    procedure ReabreCfgAtual();
    function  LocalizaCarteiraSelecionada(): Boolean;
    procedure LocalizaLancto(Controle: Integer);
  public
    { Public declarations }
    //
    FModuleLctX: TDataModule;
    FDtEncer, FDtMorto: TDateTime;
    FTabLctA, FTabLctB, FTabLctD: String;
    //
    procedure DmLct2_QrCrtAfterScroll();
    procedure DmLct2_QrLctAfterScroll();
    procedure DefineDataModule(DM: TDataModule);
    procedure MyFormCreate(Sender: TObject);
  end;

  var
  FmLctGer2: TFmLctGer2;

implementation

uses
{$IfNDef NAO_USA_IMP_CHEQUE} UnMyPrinters, {$EndIf}
{$IfNDef SemProtocolo} UnProtocoUnit, Protocolo, {$EndIf}
{$IfNDef sBLQ}UnBloquetos, {$EndIf}
{$IfDef TEM_UH}UnBloquetos_Jan, {$EndIf}
UnMyObjects, ModuleLct2, UnInternalConsts, Module, ModuleFin, ModuleGeral,
MyDBCheck, DmkDAC_PF, MyListas, UnFinanceiroJan, LctEdit, Principal,
UMySQLModule, UnGOTOy, Maladireta, MyGlyfs, ContasConfEmis, ContasMesGer,
ContasConfCad, ContasConfPgto, FluxCxaDia2, UnGrl_Geral, GerlShowGrid;

{$R *.DFM}

procedure TFmLctGer2.Acertarcreditopelovalorpago1Click(Sender: TObject);
begin
  UFinanceiro.AcertarCreditoPeloValorPago(TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrLct, TDBGrid(DBGLct), FTabLctA);
end;

procedure TFmLctGer2.Agendamentodeproviso1Click(Sender: TObject);
begin
  {$IfDef TEM_UH}
  FinanceiroJan.MostraPrevBaCLctos('LctGer2');
  {$EndIf}
end;

procedure TFmLctGer2.Alteratransferncia1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(1);
end;

procedure TFmLctGer2.Alteratransfernciaentrecarteirasatual1Click(
  Sender: TObject);
begin
  TranferenciaEntreCarteiras(1);
end;

procedure TFmLctGer2.Alteratransfernciaentrecontas1Click(Sender: TObject);
begin
  TranferenciaEntreContas(1);
end;

procedure TFmLctGer2.Alteratransfernciaentrecontasatual1Click(Sender: TObject);
begin
  TranferenciaEntreContas(1);
end;

procedure TFmLctGer2.Atual4Click(Sender: TObject);
begin
  {$IfNDef SemProtocolo}
    UnProtocolo.ProtocolosCD_Lct(istAtual, TDBGrid(DBGLct), FModuleLctX,
      FThisEntidade, Name, FTabLctA);
  {$Else}
    Grl_Geral.InfoSemModulo(mdlappProtocolos);
  {$EndIf}
end;

procedure TFmLctGer2.Avulso1Click(Sender: TObject);
begin
{$IFNDEF NAO_USA_RECIBO}
  GOTOy.EmiteRecibo(0, FThisEntidade, 0, 0, 0, 0, '', '', '', '', DModG.ObtemAgora, 0);
{$Else}
  Geral.MB_Aviso('M�dulo de Recibo desabilitado!');
{$EndIf}
end;

procedure TFmLctGer2.BtDuplicaClick(Sender: TObject);
begin
  InsAlt(tgrDuplica);
end;

procedure TFmLctGer2.Balancete1Click(Sender: TObject);
begin
  FinanceiroJan.MostraCashBal(FThisCliInt);
end;

procedure TFmLctGer2.BtLocalizaClick(Sender: TObject);
begin
  LocalizaLancto(0);
end;

procedure TFmLctGer2.BtProvisoesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProvisoes, BtProvisoes);
end;

procedure TFmLctGer2.BtArrecadaClick(Sender: TObject);
begin
  {$IfNDef sBLQ}
  if not AppPF.AcaoEspecificaDeApp('LctGer2', FThisCliInt, FThisEntidade,
    TDmLct2(FModuleLctX).QrLct, nil)
  then
    Geral.MB_Aviso('O agendamento est� indispon�vel ' +
      'para este aplicativo! Para ativ�-lo, contate a DERMATEK!');
  {$EndIf}
end;

procedure TFmLctGer2.BtAlteraClick(Sender: TObject);
begin
  InsAlt(tgrAltera);
end;

procedure TFmLctGer2.BtAutomClick(Sender: TObject);
begin
  PagamentoRapido();
end;

procedure TFmLctGer2.BtRelatoriosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRelatorios, BtRelatorios);
end;

procedure TFmLctGer2.BtBloquetoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBloqueto, BtBloqueto);
end;

procedure TFmLctGer2.BtCalculadoraClick(Sender: TObject);
begin
  dmkPF.MostraCalculadoraDoWindows(Sender);
end;

procedure TFmLctGer2.BtChequeClick(Sender: TObject);
begin
{$IfNDef NAO_USA_IMP_CHEQUE}
  MyPrinters.EmiteCheque(
    TDmLct2(FModuleLctX).QrLctControle.Value,
    TDmLct2(FModuleLctX).QrLctSub.Value, TDmLct2(FModuleLctX).QrCrtBanco1.Value,
    0, '', 0, '', TDmLct2(FModuleLctX).QrLctSerieCH.Value, TDmLct2(FModuleLctX).QrLctDocumento.Value,
    TDmLct2(FModuleLctX).QrLctData.Value, TDmLct2(FModuleLctX).QrLctCarteira.Value,
    'NOMEFORNECEDOR', TDBGrid(DBGLct), TDmLct2(FModuleLctX).FTabLctA, RGTipoData.ItemIndex,
    TPDataIni.Date, TPDataFim.Date);
{$EndIf}
end;

procedure TFmLctGer2.BtConciliaClick(Sender: TObject);
const
  Condominio = 0;
var
  CartConcilia, Entidade(*, CliInt*): Integer;
  NomeCI: String;
begin
  if LocalizaCarteiraSelecionada() then
  begin
    CartConcilia := TDmLct2(FModuleLctX).QrCrt.FieldByName('Codigo').AsInteger;
    Entidade     := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
    //CliInt       := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
    NomeCI       := DModG.EmpresaAtual_ObtemNome();
    //
    UFinanceiro.ConciliacaoBancaria(TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, TPDataIni,
      NomeCI, Entidade, Condominio, CartConcilia, FTabLctA, FModuleLctX);
  end;
end;

procedure TFmLctGer2.BtConfPagtosCadClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConfPagtosCad, BtConfPagtosCad);
end;

procedure TFmLctGer2.BtConfPgtosExeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConfPgtosExe, BtConfPgtosExe);
end;

procedure TFmLctGer2.BtContarDinheiroClick(Sender: TObject);
begin
  UFinanceiro.MudaValorEmCaixa(TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt);
end;

procedure TFmLctGer2.BtCopiaCHClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCopiaCH, BtCopiaCH);
end;

procedure TFmLctGer2.BtDesfazOrdenacaoClick(Sender: TObject);
begin
  UnDmkDAC_PF.DesfazOrdenacaoDmkDBGrid(TDmkDBGrid(DBGLct), 'Controle');
end;

procedure TFmLctGer2.BtExcluiClick(Sender: TObject);
var
  i, k: Integer;
begin
  if DBGLct.SelectedRows.Count > 1 then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o dos itens selecionados?') =
    ID_YES then
    begin
      with DBGLct.DataSource.DataSet do
      for i:= 0 to DBGLct.SelectedRows.Count-1 do
      begin
        //GotoBookmark(DBGLct.SelectedRows.Items[i]);
        GotoBookmark(DBGLct.SelectedRows.Items[i]);
        //
        UFinanceiro.ExcluiItemCarteira(TDmLct2(FModuleLctX).QrLctControle.Value,
          TDmLct2(FModuleLctX).QrLctData.Value, TDmLct2(FModuleLctX).QrLctCarteira.Value,
          TDmLct2(FModuleLctX).QrLctSub.Value, TDmLct2(FModuleLctX).QrLctGenero.Value,
          TDmLct2(FModuleLctX).QrLctCartao.Value, TDmLct2(FModuleLctX).QrLctSit.Value,
          TDmLct2(FModuleLctX).QrLctTipo.Value, 0, TDmLct2(FModuleLctX).QrLctID_Pgto.Value,
          TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt, False,
          TDmLct2(FModuleLctX).QrLctCarteira.Value, dmkPF.MotivDel_ValidaCodigo(300),
          FTabLctA, False);
      end;
      k := UMyMod.ProximoRegistro(TDmLct2(FModuleLctX).QrLct, 'Controle',  0);
      UFinanceiro.RecalcSaldoCarteira(
        TDmLct2(FModuleLctX).QrCrtCodigo.Value, TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, False, True);
      TDmLct2(FModuleLctX).QrLct.Locate('Controle', k, []);
    end;
  end else
    UFinanceiro.ExcluiItemCarteira(TDmLct2(FModuleLctX).QrLctControle.Value,
      TDmLct2(FModuleLctX).QrLctData.Value, TDmLct2(FModuleLctX).QrLctCarteira.Value,
      TDmLct2(FModuleLctX).QrLctSub.Value, TDmLct2(FModuleLctX).QrLctGenero.Value,
      TDmLct2(FModuleLctX).QrLctCartao.Value, TDmLct2(FModuleLctX).QrLctSit.Value,
      TDmLct2(FModuleLctX).QrLctTipo.Value, 0, TDmLct2(FModuleLctX).QrLctID_Pgto.Value,
      TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt, True, TDmLct2(FModuleLctX).QrLctCarteira.Value,
      dmkPF.MotivDel_ValidaCodigo(300), FTabLctA, False);
end;

procedure TFmLctGer2.BtFluxCxaDiaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFluxCxaDia2, FmFluxCxaDia2, afmoNegarComAviso) then
  begin
    FmFluxCxaDia2.EdEmpresa.ValueVariant := FThisCliInt;
    FmFluxCxaDia2.CBEmpresa.KeyValue     := FThisCliInt;
    FmFluxCxaDia2.ShowModal;
    FmFluxCxaDia2.Destroy;
  end;
end;

procedure TFmLctGer2.BtFluxoCxaClick(Sender: TObject);
begin
  FinanceiroJan.MostraFluxoCxa(FThisEntidade, FThisCliInt);
end;

procedure TFmLctGer2.BtIncluiClick(Sender: TObject);
begin
  InsAlt(tgrInclui);
end;

procedure TFmLctGer2.BtMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMenu, BtMenu);
end;

procedure TFmLctGer2.BtOKClick(Sender: TObject);
begin
//
end;

procedure TFmLctGer2.BtPlaCtasClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeNiveisPlano(0, 0);
end;

procedure TFmLctGer2.BtProtocoloClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProtocolo, BtProtocolo);
end;

procedure TFmLctGer2.BtQuitaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMQuita, BtQuita);
end;

procedure TFmLctGer2.BtReabreClick(Sender: TObject);
begin
  ReabreCfgAtual();
end;

procedure TFmLctGer2.BtReciboClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRecibo, BtRecibo);
end;

procedure TFmLctGer2.BtRefreshClick(Sender: TObject);
var
  Controle: Integer;
begin
  if TDmLct2(FModuleLctX).QrLct.State = dsBrowse then
    Controle := TDmLct2(FModuleLctX).QrLctControle.Value
  else
    Controle := 0;
  //
  { n�o ajuda! J� est� na procedure!
  TDmLct2(FModuleLctX).QrCrt.DisableControls;
  try
  }
    UFinanceiro.AtualizaTodasCarteiras(TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, FTabLctA);
  {
  finally
    TDmLct2(FModuleLctX).QrCrt.EnableControls;
  end;
  }
  //
  if Controle <> 0 then
    TDmLct2(FModuleLctX).QrLct.Locate('Controle', Controle, []);
end;

procedure TFmLctGer2.BtSaldoAquiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSaldoAqui, BtSaldoAqui);
end;

procedure TFmLctGer2.BtSaldoTotalClick(Sender: TObject);
begin
  BtSaldoTotal.Caption := FormatFloat('#,###,###,##0.00',
  UFinanceiro.ObtemSaldoCarteira(TDmLct2(FModuleLctX).QrCrtCodigo.Value));
end;

procedure TFmLctGer2.BtSalvaPosicaoGradeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSalvaPosicaoGrade, BtSalvaPosicaoGrade);
end;

procedure TFmLctGer2.BtSaidaClick(Sender: TObject);
begin
  if TFmLctGer2(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmLctGer2(Self), TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmLctGer2.BtSomaLinhasClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    UFinanceiro.SomaLinhas_3(TDBGrid(DBGLct), TDmLct2(FModuleLctX).QrCrt,
      TDmLct2(FModuleLctX).QrLct, EdCred, EdDebi, EdSoma);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctGer2.BtTrfCtaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTrfCta, BtTrfCta);
end;

procedure TFmLctGer2.Calcula1Click(Sender: TObject);
begin
  EdSdoAqui.Text :=
    UFinanceiro.SaldoAqui(1, EdSdoAqui.Text, TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt);
end;

procedure TFmLctGer2.CarregarOrdenaopadrodascolunasdagrade1Click(
  Sender: TObject);
begin
  if Geral.MB_Pergunta('Deseja carregar a ordena��o padr�o das colunas da grade?'
    + sLineBreak + 'Ao fazer isso a configura��o atual ser� alterada.' + sLineBreak +
    'Deseja continuar?') = ID_YES then
  begin
    VAR_SAVE_CFG_DBGRID_GRID := DBGLct;
    DModG.SalvaPosicaoColunasDmkDBGrid(VAR_USUARIO, True);
    Close;
  end;
end;

procedure TFmLctGer2.Carteiras1Click(Sender: TObject);
begin
  UFinanceiro.MudaCarteiraLancamentosSelecionados(TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrLct, TDBGrid(DBGLct), FTabLctA, FThisEntidade);
end;

procedure TFmLctGer2.CkCreditoClick(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmLctGer2.CkDebitoClick(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmLctGer2.Compensao1Click(Sender: TObject);
begin
  UFinanceiro.MudaDataLancamentosSelecionados(cdCompensado,
    TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, TDBGrid(DBGLct),
    FTabLctA);
end;

procedure TFmLctGer2.Compensar1Click(Sender: TObject);
begin
  UFinanceiro.QuitacaoDeDocumentos(TDBGrid(DBGLct), TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, FTabLctA);
end;

procedure TFmLctGer2.Compensar2Click(Sender: TObject);
begin
  UFinanceiro.QuitacaoDeDocumentos(TDBGrid(DBGLct), TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrLct, FTabLctA, True);
end;

procedure TFmLctGer2.ConfiguraPopoupQuitacoes;
const
  k = 3;
var
  i, n, Carteira: Integer;
  c: array [0..k] of Integer;
  Enab: Boolean;
begin
  for i := 0 to k do c[i] := 0;

  if DBGLct.SelectedRows.Count > 1 then
  begin
    with DBGLct.DataSource.DataSet do
    for i:= 0 to DBGLct.SelectedRows.Count-1 do
    begin
      //GotoBookmark(DBGLct.SelectedRows.Items[i]);
      GotoBookmark(DBGLct.SelectedRows.Items[i]);
      Inc(c[TDmLct2(FModuleLctX).QrLctSit.Value], 1);
    end;
    n := DBGLct.SelectedRows.Count;
  end else
  begin
    c[TDmLct2(FModuleLctX).QrLctSit.Value] := 1;
    n := 1;
  end;
  //PMQuita
  Compensar2.Enabled                := False;
  Pagamentorpido2.Enabled           := False;
  Pagtorpidoselbanco1.Enabled       := False;
  Pagar2.Enabled                    := False;
  Reverter2.Enabled                 := False;
  PagarAVista2.Enabled              := False;
  Localizarlanamentoorigem2.Enabled := False;
  //PMMenu
  Compensar1.Enabled                := False;
  Pagamentorpido1.Enabled           := False;
  Pagar1.Enabled                    := False;
  Reverter1.Enabled                 := False;
  PagarAVista1.Enabled              := False;
  Localizarlanamentoorigem1.Enabled := False;
  //
  // ini 2021-04-20
  if LocalizaCarteiraSelecionada() then
  begin
  // fim 2021-04-20
    if TDmLct2(FModuleLctX).QrCrtTipo.Value = 2 then
    begin
      //PMQuita
      Compensar2.Enabled                := c[0] = n;
      Pagamentorpido2.Enabled           := c[0] = n;
      Pagtorpidoselbanco1.Enabled       := c[0] = n;
      Pagar2.Enabled                    := (n=1) and (c[3] = 0);
      Reverter2.Enabled                 := c[3] = n;
      PagarAvista2.Enabled              := c[0] = n;
      Localizarlanamentoorigem2.Enabled := c[3] = n;
      //PMMenu
      Compensar1.Enabled                := c[0] = n;
      Pagamentorpido1.Enabled           := c[0] = n;
      Pagar1.Enabled                    := (n=1) and (c[3] = 0);
      Reverter1.Enabled                 := c[3] = n;
      PagarAVista1.Enabled              := c[0] = n;
      Localizarlanamentoorigem1.Enabled := c[3] = n;
    end else
    begin
      Enab := TDmLct2(FModuleLctX).QrLctID_Pgto.Value <> 0;
      //
      Localizarlanamentoorigem1.Enabled := Enab;
      Localizarlanamentoorigem2.Enabled := Enab;
    end;
  end;
end;

procedure TFmLctGer2.Conta1Click(Sender: TObject);
begin
  UFinanceiro.MudaContaLancamentosSelecionados(TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrLct, TDBGrid(DBGLct), FTabLctA);
end;

procedure TFmLctGer2.Contas2Click(Sender: TObject);
begin
  FinanceiroJan.CriaImpressaoDiversos(FThisCliInt, 2);
end;

procedure TFmLctGer2.ContasControladas2Click(Sender: TObject);
begin
  FinanceiroJan.MostraPesquisaContasControladas(FThisCliInt);
end;

procedure TFmLctGer2.Contasmensais1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasConfEmis, FmContasConfEmis, afmoNegarComAviso) then
  begin
    with FmContasConfEmis do
    begin
      EdCliInt.ValueVariant := FThisEntidade;
      CBCliInt.KeyValue     := FThisEntidade;
      //
      FFinalidade      := FLctFinalidade;
      FQrLct           := TDmLct2(FModuleLctX).QrLct;
      FQrCrt           := TDmLct2(FModuleLctX).QrCrt;
      FPercJuroM       := Dmod.QrControle.FieldByName('MoraDD').AsFloat;
      FPercMulta       := Dmod.QrControle.FieldByName('Multa').AsFloat;
      FSetaVars        := nil;
      FAlteraAtehFatID := True;
      FLockCliInt      := True;
      FLockForneceI    := False;
      FLockAccount     := False;
      FLockVendedor    := False;
      FCliente         := 0;
      FFornecedor      := 0;
      FForneceI        := 0;
      FAccount         := 0;
      FVendedor        := 0;
      FIDFinalidade    := 2;
      FTabLctA         := TFmLctGer2(Self).FTabLctA;
      //
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmLctGer2.ContasMensaisCadastrodeEmisses1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasMesGer, FmContasMesGer, afmoNegarComAviso) then
  begin
    FmContasMesGer.FEntInt := FThisEntidade;
    FmContasMesGer.ShowModal;
    FmContasMesGer.Destroy;
  end;
end;

procedure TFmLctGer2.ContasMensaisCadastrodePagamentos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasConfCad, FmContasConfCad, afmoNegarComAviso) then
  begin
    FmContasConfCad.FEntInt := FThisEntidade;
    FmContasConfCad.ShowModal;
    FmContasConfCad.Destroy;
  end;
end;

procedure TFmLctGer2.Contassazonais2Click(Sender: TObject);
begin
  FinanceiroJan.MostraContasSazonais;
end;

procedure TFmLctGer2.Copiar1Click(Sender: TObject);
begin
  InsAlt(tgrDuplica);
end;

procedure TFmLctGer2.Cpiaautomtica1Click(Sender: TObject);
begin
  FinanceiroJan.MostraCopiaDoc(TDmLct2(FModuleLctX).QrLct, TDBGrid(DBGLct),
    FtabLctA);
end;

procedure TFmLctGer2.Cpiadecheque1Click(Sender: TObject);
var
  I: Integer;
begin
  if DBGLct.SelectedRows.Count > 1 then
  begin
    if Geral.MB_Pergunta('Confirma a impress�o da c�pia de cheque dos itens selecionados?') = ID_YES then
    begin
      with DBGLct.DataSource.DataSet do
      begin
        for i := 0 to DBGLct.SelectedRows.Count-1 do
        begin
          //GotoBookmark(DBGLct.SelectedRows.Items[i]);
          GotoBookmark(DBGLct.SelectedRows.Items[i]);
          FinanceiroJan.ImprimeCopiaCH(TDmLct2(FModuleLctX).QrLctControle.Value,
            TDmLct2(FModuleLctX).QrLctGenero.Value, FThisEntidade, FtabLctA);
        end;
      end;
    end;
  end else
  FinanceiroJan.ImprimeCopiaCH(TDmLct2(FModuleLctX).QrLctControle.Value,
    TDmLct2(FModuleLctX).QrLctGenero.Value, FThisEntidade, FtabLctA);
end;

procedure TFmLctGer2.CpiadechequeNovo1Click(Sender: TObject);
begin
  FinanceiroJan.ImprimeCopiaVC(TDmLct2(FModuleLctX).QrLct, TDBGrid(DBGLct),
    FThisEntidade, FtabLctA);
end;

procedure TFmLctGer2.Cpiadedbitoemconta1Click(Sender: TObject);
begin
  FinanceiroJan.ImprimeCopiaDC(TDmLct2(FModuleLctX).QrLctControle.Value,
    TDmLct2(FModuleLctX).QrLctGenero.Value, FThisEntidade, FtabLctA);
end;

procedure TFmLctGer2.MyFormCreate(Sender: TObject);
var
  I: Integer;
begin
  VAR_SAVE_CFG_DBGRID_GRID := DBGLct;
  DModG.CarregaPosicaoColunasDmkDBGrid(VAR_USUARIO);
  //
  ImgTipo.SQLType              := stPsq;
  PageControl1.ActivePageIndex := 0;
  //
  FThisEntidade := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
  FThisCliInt   := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
  FThisFilial   := DModG.EmpresaAtual_ObtemCodigo(tecFilial);
  //
  if VAR_KIND_DEPTO = kdUH then
    FLctFinalidade := lfCondominio
  else
    FLctFinalidade := lfProprio;
  //
  //////////////////////////////////////////////////////////////////////////////
  TPDataIni.Date := Date - Geral.ReadAppKeyCU('Dias', Application.Title, ktInteger, 60);
  // ser�  feito ap�s definir o TabLctA
  //TPDataFim.Date := Date + 180;
  //////////////////////////////////////////////////////////////////////////////
  //
  Caption := 'FIN-SELFG-001 :: Finan�as :: ' + FormatFloat('000',
    DModG.EmpresaAtual_ObtemCodigo(tecCliInt)) + ' - ' +
    DModG.EmpresaAtual_ObtemNome();
  FormResize(Self);
  //
  if Geral.ReadAppKeyCU(
  'MaximizeLctGer1', Application.Title, ktInteger, 0) = 1 then
    WindowState := wsMaximized;
  //
  FMaxTrava := 0;
  for I := 0 to TFmLctGer2(Self).ComponentCount - 1 do
  begin
    if TComponent(TFmLctGer2(Self).Components[I]).GetParentComponent = PnAcoesTravaveis then
    begin
      FMaxTrava := FMaxTrava + 1;
      SetLength(FArrTrava, FMaxTrava);
      FArrTrava[FMaxTrava -1] := TWinControl(TFmLctGer2(Self).Components[I]);
    end;
  end;
  //
  {$IfDef TEM_UH}
    Importaes1.Visible  := True;
    N18.Visible         := True;
    BtProvisoes.Visible := True;
  {$Else}
    Importaes1.Visible  := False;
    N18.Visible         := False;
    BtProvisoes.Visible := False;
  {$EndIf}
  {$IfNDef SemProtocolo}
    BtProtocolo.Visible := True;
  {$Else}
    BtProtocolo.Visible := False;
  {$EndIf}
  {$IfNDef sBLQ}
    if VAR_KIND_DEPTO <> kdUH then
      BtBloqueto.Visible := True
    else
      BtBloqueto.Visible := False;
    //
    BtArrecada.Visible  := True;
  {$Else}
    BtBloqueto.Visible := False;
    BtArrecada.Visible := False;
  {$EndIf}
  for i := 0 to DBGLct.Columns.Count - 1 do
  begin
    if DBGLct.Columns[I].FieldName = 'UH' then
    begin
      DBGLct.Columns[I].Visible := VAR_KIND_DEPTO <> kdNenhum;
      case VAR_KIND_DEPTO of
        kdUH:
        begin
          DBGLct.Columns[I].Visible       := True;
          DBGLct.Columns[I].Title.Caption := 'UH';
        end;
        kdObra:
        begin
          DBGLct.Columns[I].Visible       := True;
          DBGLct.Columns[I].Title.Caption := 'Obra';
        end
        else
          DBGLct.Columns[I].Visible := False;
      end;
    end;
  end;
end;

procedure TFmLctGer2.Data1Click(Sender: TObject);
begin
  UFinanceiro.MudaDataLancamentosSelecionados(cdData,
    TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, TDBGrid(DBGLct),
    FTabLctA);
end;

procedure TFmLctGer2.Datalancto1Click(Sender: TObject);
begin
  UFinanceiro.ColocarMesDeCompetenciaOndeNaoTem(dcData,
    TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, FTabLctA);
end;

procedure TFmLctGer2.DBGCrtDblClick(Sender: TObject);
begin
  if (TDmLct2(FModuleLctX).QrCrt.State <> dsInactive)
  and (TDmLct2(FModuleLctX).QrCrt.RecordCount > 0) then
  begin
    EdCarteira.ValueVariant := TDmLct2(FModuleLctX).QrCrtCodigo.Value;
  end;
end;

procedure TFmLctGer2.DBGLctDblClick(Sender: TObject);
var
  FatIdTxt: MyArrayLista;
  TitCols: array[0..1] of String;
  Campo: String;
  FatParcRef, Contrato: Integer;
  FatID, FatParcela: Integer;
  FatNum: Double;
begin
  if FFatIDHabilitaByLct then
    InsAlt(tgrAltera);
  //
  if(TDmLct2(FModuleLctX).QrLct.State <> dsInactive) and
    (TDmLct2(FModuleLctX).QrLct.RecordCount > 0) then
  begin
{
    Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    //
    if UpperCase(Campo) = UpperCase('FatID') then
    begin
      FatIdTxt := UFinanceiro.ObtemListaNomeFatID(CO_DMKID_APP);
      //
      TitCols[0] := 'ID';
      TitCols[1] := 'Descri��o';
      //
      Geral.SelecionaItem(FatIdTxt, 0,
        'SEL-LISTA-000 :: Sele��o da origem do faturamento', TitCols, Screen.Width);
    end else
    if UpperCase(Campo) = UpperCase('FatPartcRef') then
}
    begin
      FatParcRef := TDmLct2(FModuleLctX).QrLctFatParcRef.Value;
      if FatParcRef <> 0 then
      begin
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
        'SELECT Codigo ',
        'FROM lctfatref ',
        'WHERE Conta=' + Geral.FF0(FatParcRef),
        '']);
        Contrato := Dmod.QrAux.Fields[0].AsInteger;
        //
        AppPF.MostraOrigemFat(Contrato);
      end else
      begin
        FatID       := TDmLct2(FModuleLctX).QrLctFatID.Value;
        FatNum      := TDmLct2(FModuleLctX).QrLctFatNum.Value;
        FatParcela  := TDmLct2(FModuleLctX).QrLctFatParcela.Value;
        AppPF.MostraOrigemFatGenerico(FatID, FatNum, FatParcela, FatParcRef);
      end;
    end
  end;
end;

procedure TFmLctGer2.DBGLctDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  //if gdSelected in State then Exit;
  if FModuleLctX <> nil then
  begin
    MyObjects.DefineCorTextoSitLancto(TDBGrid(Sender), Rect,
      Column.FieldName, TDmLct2(FModuleLctX).QrLctNOMESIT.Value);
  end;
end;

procedure TFmLctGer2.DBGLctKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if FFatIDHabilitaByLct then
  begin
    if key=13 then InsAlt(tgrAltera)
    else if (key=VK_DELETE) and (Shift=[ssCtrl]) then
      UFinanceiro.ExcluiItemCarteira(TDmLct2(FModuleLctX).QrLctControle.Value,
        TDmLct2(FModuleLctX).QrLctData.Value, TDmLct2(FModuleLctX).QrLctCarteira.Value,
        TDmLct2(FModuleLctX).QrLctSub.Value, TDmLct2(FModuleLctX).QrLctGenero.Value,
        TDmLct2(FModuleLctX).QrLctCartao.Value, TDmLct2(FModuleLctX).QrLctSit.Value,
        TDmLct2(FModuleLctX).QrLctTipo.Value, 0, TDmLct2(FModuleLctX).QrLctID_Pgto.Value,
        TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt, True, TDmLct2(FModuleLctX).QrLctCarteira.Value,
        dmkPF.MotivDel_ValidaCodigo(300), FTabLctA, False)
    else if (key=VK_F4) and (Shift=[ssCtrl]) then
      MyObjects.MostraPopUpDeBotaoObject(PMQuita, DBGLct, 0, 0)
  end;
end;

procedure TFmLctGer2.DefineDataModule(DM: TDataModule);
begin
  FModuleLctX := DM;
  //
  //EdCodigo.DataSource     := TDmLct2(FModuleLctX).DsCrt;
  //EdNome.DataSource       := TDmLct2(FModuleLctX).DsCrt;
  EdSaldo.DataSource      := TDmLct2(FModuleLctX).DsCrt;
  EdDiferenca.DataSource  := TDmLct2(FModuleLctX).DsCrt;
  EdCaixa.DataSource      := TDmLct2(FModuleLctX).DsCrt;
  DBGCrt.DataSource       := TDmLct2(FModuleLctX).DsCrt;
  //
  //DBEdit5.DataSource      := TDmLct2(FModuleLctX).DsLct;
  DBGLct.DataSource       := TDmLct2(FModuleLctX).DsLct;
  DBEdit1.DataSource      := TDmLct2(FModuleLctX).DsLct;
  DBEdit2.DataSource      := TDmLct2(FModuleLctX).DsLct;
  DBEdit3.DataSource      := TDmLct2(FModuleLctX).DsLct;
  DBEdit4.DataSource      := TDmLct2(FModuleLctX).DsLct;
  //
  // Nao pode ser aqui! Nao esta definido ainda!
  //TDmLct2(FModuleLctX).FTabLctA := FTabLctA;
end;

procedure TFmLctGer2.Descrio1Click(Sender: TObject);
begin
  UFinanceiro.MudaTextLancamentosSelecionados(TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrLct, TDBGrid(DBGLct), FTabLctA);
end;

procedure TFmLctGer2.Diferena1Click(Sender: TObject);
begin
  EdSdoAqui.Text :=
    UFinanceiro.SaldoAqui(2, EdSdoAqui.Text, TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt);
end;

procedure TFmLctGer2.DmLct2_QrCrtAfterScroll();
var
 I: Integer;
begin
  FFatIDHabilitaByCrt := TDmLct2(FModuleLctX).QrCrtCodigo.Value > -1;
  //
{
  for I := 0 to TFmLctGer2(Self).ComponentCount - 1 do
  begin
    if TComponent(TFmLctGer2(Self).Components[I]).GetParentComponent = PnAcoesTravaveis then
      TWinControl(TFmLctGer2(Self).Components[I]).Enabled := FFatIDHabilitaByCrt;
  end;
}
  for I := 0 to FMaxTrava - 1 do
    FArrTrava[I].Enabled := FFatIDHabilitaByCrt;
end;

procedure TFmLctGer2.DmLct2_QrLctAfterScroll();
var
  I: Integer;
begin
  if FFatIDHabilitaByCrt then
  begin
    FFatIDHabilitaByLct := (TDmLct2(FModuleLctX).QrLctFatID.Value < 300) or
                (TDmLct2(FModuleLctX).QrLctFatID.Value > 399);
    //if not FFatIDHabilitaByLct then
    for I := 0 to FMaxTrava - 1 do
      FArrTrava[I].Enabled := FFatIDHabilitaByLct;
  end;
end;

procedure TFmLctGer2.EdCarteiraPsqRedefinido(Sender: TObject);
const
  ProcName = 'TFmLctGer2.EdCarteiraRedefinido()';
var
  Carteira: Integer;
begin
  Carteira := 0;
  if TDmLct2(FModuleLctX).QrCrt.State <> dsInactive then
    Carteira := TDmLct2(FModuleLctX).QrCrtCodigo.Value;
  TDmLct2(FModuleLctX).ReabreCarteiras(Carteira, TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrCrtSum, ProcName, EdCarteiraPsq.Text);
end;

procedure TFmLctGer2.EdCarteiraRedefinido(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmLctGer2.EdEntidadeRedefinido(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmLctGer2.Em2Click(Sender: TObject);
begin
  FinanceiroJan.MostraSaldos(FThisCliInt);
end;

procedure TFmLctGer2.Etiquetas2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmLctGer2.Eventos1Click(Sender: TObject);
begin
  FinanceiroJan.MostraEventosCad;
end;

procedure TFmLctGer2.Excluitransferncia1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(2);
end;

procedure TFmLctGer2.Excluitransfernciaentrecarteirasatual1Click(
  Sender: TObject);
begin
  TranferenciaEntreCarteiras(2);
end;

procedure TFmLctGer2.Excluitransfernciaentrecontas1Click(Sender: TObject);
begin
  TranferenciaEntreContas(2);
end;

procedure TFmLctGer2.Excluitransfernciaentrecontasatual1Click(Sender: TObject);
begin
  TranferenciaEntreContas(2);
end;

procedure TFmLctGer2.Exclusoincondicional1Click(Sender: TObject);
begin
  UFinanceiro.ExclusaoIncondicionalDeLancamento(
    TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, FTabLctA);
end;

procedure TFmLctGer2.FormActivate(Sender: TObject);
begin
  if TFmLctGer2(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end else
  begin
    // Pode ter mudado para DefParams de outo form em Aba!
    //DefParams();  n�o tem no LctGer2!!!
  end;
  //
end;

procedure TFmLctGer2.FormClose(Sender: TObject; var Action: TCloseAction);
var
  M: Integer;
begin
  Geral.WriteAppKeyCU('Dias', Application.Title, Int(Date - TPDataIni.Date), ktInteger);
  if WindowState = wsMaximized then
    M := 1
  else
    M := 0;
  Geral.WriteAppKeyCU('MaximizeLctGer1', Application.Title, M, ktInteger);
end;

procedure TFmLctGer2.FormCreate(Sender: TObject);
begin
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmLctGer2.FormDestroy(Sender: TObject);
begin
  VAR_EvitaErroExterno := True;
end;

procedure TFmLctGer2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctGer2.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmLctGer2.Formulrios2Click(Sender: TObject);
begin
  FinanceiroJan.CriaImpressaoDiversos(FThisCliInt, 0);
end;

procedure TFmLctGer2.Futuros2Click(Sender: TObject);
begin
  DModFin.ImprimeSaldos(FThisEntidade);
end;

procedure TFmLctGer2.Gerabloqueto1Click(Sender: TObject);
{$IfNDef sBLQ}
var
  LoteCR: Integer;
{$EndIf}
begin
  {$IfNDef sBLQ}
    if VAR_KIND_DEPTO <> kdUH then
    begin
      Gerabloqueto1.Enabled := False;
      LoteCR                := 0;
      //
      if UBloquetos.EmiteBoletoAvulso(FTabLctA, TdmkDBGrid(DBGLct),
        TDmLct2(FModuleLctX).QrLct, True, TDmLct2(FModuleLctX).QrLctFatNum.Value,
        0, 0, LoteCR) then
      begin
        if Geral.MB_Pergunta('Deseja imprimir o bloqueto?') = ID_YES then
        begin
          UBloquetos.MostraBloGeren(1, 0, TDmLct2(FModuleLctX).QrLctControle.Value,
            0, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPagerNovo);
        end;
      end;
      Gerabloqueto1.Enabled := True;
    end;
  {$EndIf}
end;

procedure TFmLctGer2.ImprimeGerenciabloqueto1Click(Sender: TObject);
begin
{$IfNDef sBLQ}
  if VAR_KIND_DEPTO <> kdUH then
  begin
    UBloquetos.MostraBloGeren(1, 0, TDmLct2(FModuleLctX).QrLctControle.Value,
      0, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPagerNovo);
  end;
{$EndIf}
end;

procedure TFmLctGer2.ImprimirReciboCriado1Click(Sender: TObject);
var
  sControle: String;
  Codigo: Integer;
begin
  Codigo := 0;
  sControle := Geral.FF0(TDmLct2(FModuleLctX).QrLctControle.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRecibos, Dmod.MyDB, [
  'SELECT Codigo, Controle  ',
  'FROM reciboimpori ',
  'WHERE LctCtrl=' + sControle,
  ' ',
  'UNION ',
  ' ',
  'SELECT Codigo, Controle  ',
  'FROM reciboimpdst ',
  'WHERE LctCtrl=' + sControle,
  '']);
  if QrRecibos.RecordCount > 0 then
  begin
    if QrRecibos.RecordCount > 1 then
    begin
      Application.CreateForm(TFmGerlShowGrid, FmGerlShowGrid);
      FmGerlShowGrid.MeAvisos.Lines.Add('Selecione o item desejado!');
      FmGerlShowGrid.DBGSel.DataSource := DsRecibos;
      FmGerlShowGrid.ShowModal;
      if FmGerlShowGrid.FSelecionou then
        Codigo := QrRecibosCodigo.Value;
      FmGerlShowGrid.Destroy;
    end else
      Codigo := QrRecibosCodigo.Value;
  end;
  //
  if Codigo <> 0 then
    FinanceiroJan.MostraFormReciboImpCab(Codigo);
end;

procedure TFmLctGer2.Incluinovatransfernciaentrecarteiras1Click(
  Sender: TObject);
begin
  TranferenciaEntreCarteiras(0);
end;

procedure TFmLctGer2.Incluinovatransfernciaentrecontas1Click(Sender: TObject);
begin
  TranferenciaEntreContas(0);
end;

procedure TFmLctGer2.InsAlt(Acao: TGerencimantoDeRegistro);
const
  AlteraAtehFatID = False;
  Genero = 0;
  GenCtb = 0;
  SetaVars = nil;
  FatID = 0;
  FatID_Sub = 0;
  FatNum = 0;
  Carteira = 0;
  Credito = 0;
  Debito = 0;
  Cliente = 0;
  Fornecedor = 0;
  LockCliInt = True;
  ForneceI = 0;
  Account = 0;
  Vendedor = 0;
  LockForneceI = False;
  LockAccount = False;
  LockVendedor = False;
  Data = 0;
  Vencto = 0;
  DataDoc = 0;
  IDFinalidade = 1;
  Mes = 0;
  FisicoSrc = 0;
  FisicoCod = 0;
var
  Continua: Boolean;
  PercJuroM, PercMulta: Double;
  CliInt: Integer;
begin
  if Acao in [tgrAltera, tgrDuplica] then
  begin
    if (TDmLct2(FModuleLctX).QrLct.State <> dsInactive) and
      (TDmLct2(FModuleLctX).QrLct.RecordCount > 0)
    then
      Continua := True
    else
      Continua := False;
  end else
    Continua := True;
  //
  if Continua then
  begin
    if (Acao = tgrAltera) and
      (TDmLct2(FModuleLctX).QrLct.FieldByName('Genero').AsInteger = -1) then
    begin
      //Transfer�ncias entre carteiras
      TranferenciaEntreCarteiras(1);
    end else
    if (Acao = tgrAltera) and
      (TDmLct2(FModuleLctX).QrLct.FieldByName('FatID').AsInteger = -1) then
    begin
      //Transfer�ncias entre contas
      TranferenciaEntreContas(1);
    end else
    begin
      CliInt    := FThisEntidade;
      PercJuroM := Dmod.QrControle.FieldByName('MyPerJuros').AsFloat;
      PercMulta := Dmod.QrControle.FieldByName('MyPerMulta').AsFloat;
      //
      Continua := UFinanceiro.InclusaoLancamento((*TFmLctEdit, FmLctEdit,*) FLctFinalidade,
        afmoNegarComAviso, TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt,
        Acao, TDmLct2(FModuleLctX).QrLctControle.Value,
        TDmLct2(FModuleLctX).QrLctSub.Value, Genero,
        PercJuroM, PercMulta,
        SetaVars, FatID, FatID_Sub, FatNum, Carteira,
        Credito, Debito, AlteraAtehFatID,
        Cliente, Fornecedor, CliInt,
        ForneceI, Account, Vendedor, LockCliInt,
        LockForneceI, LockAccount, LockVendedor,
        Data, Vencto, DataDoc, IDFinalidade, Mes,
        FtabLctA, FisicoSrc, FisicoCod) > 0;
      //
      if Continua then
      begin
        // ini 2021-04-20 - N�o trabalha mais com carteira!
(*
        TDmLct2(FModuleLctX).LocCod(TDmLct2(FModuleLctX).QrCrtCodigo.Value, TDmLct2(FModuleLctX).QrCrtCodigo.Value,
        TDmLct2(FModuleLctX).QrCrt, '');
*)
        ReabreCfgAtual();
        // fim 2021-04-20 - N�o trabalha mais com carteira!
        TDmLct2(FModuleLctX).QrLct.Locate('Controle', FLAN_CONTROLE, []);
        //
        if TPDataFim.Date < VAR_DATAINSERIR then
        begin
          TPDataFim.Date := VAR_DATAINSERIR;
          TPDataFimChange(Self);
        end;
      end;
    end;
  end;
end;

procedure TFmLctGer2.JaneladeRecibos1Click(Sender: TObject);
const
  Recibo = 0;
begin
  TDmLct2(FModuleLctX).MostraFormReciboImpCab_M(Recibo, FTabLctA, TDBGrid(DBGLct),
    TDmLct2(FModuleLctX));
  VeSeReabreLct(True);
end;

procedure TFmLctGer2.Lanamentos1Click(Sender: TObject);
begin
  FinanceiroJan.MostraLctDelLanctoz;
end;

procedure TFmLctGer2.Lanamentoscomproblemas1Click(Sender: TObject);
begin
  FinanceiroJan.LancamentosComProblemas(FTabLctA);
end;

procedure TFmLctGer2.Limpa1Click(Sender: TObject);
begin
  EdSdoAqui.Text :=
    UFinanceiro.SaldoAqui(0, EdSdoAqui.Text, TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt);
end;

function TFmLctGer2.LocalizaCarteiraSelecionada(): Boolean;
var
  Carteira: Integer;
begin
  Result := False;
  Carteira := EdCarteira.ValueVariant;
  if Carteira <> 0 then
    Result := TDmLct2(FModuleLctX).QrCrt.Locate('Codigo', Carteira, []);
end;

procedure TFmLctGer2.LocalizaLancto(Controle: Integer);
begin
  if TDmLct2(FModuleLctX).QrLct.State = dsInactive then
  begin
    if Geral.MB_Pergunta(
    'Para localizar lan�amento(s) � necess�rio fazer a primeira abertura de lan�amentos nesta janela!' +
    sLineBreak + 'Deseja fazer agora?') = ID_YES then
        ReabreCfgAtual();
  end;
  if TDmLct2(FModuleLctX).QrLct.State <> dsInactive then
  begin
    UFinanceiro.LocalizarLancamento(TPDataIni, TPDataFim, RGTipoData.ItemIndex,
      TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, True, 0,
      FTabLctA, FModuleLctX, Controle, Controle <> 0);
  end;
end;

procedure TFmLctGer2.Localizalote1Click(Sender: TObject);
begin
  {$IfNDef SemProtocolo}
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT Controle',
      'FROM protpakits',
      'WHERE Conta=' + Geral.FF0(TDmLct2(FModuleLctX).QrLctProtocolo.Value),
      '']);
    //
    ProtocoUnit.MostraFormProtocolos(Dmod.QrAux.FieldByName('Controle').AsInteger, 0);
  {$Else}
    Grl_Geral.InfoSemModulo(mdlappProtocolos);
  {$EndIf}
end;

procedure TFmLctGer2.Localizar2Click(Sender: TObject);
var
  ControleToLoc: Integer;
begin
  UFinanceiro.LocalizarLancamento(TPDataIni, TPDataFim, RGTipoData.ItemIndex,
    TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, True, 0,
    FTabLctA, FModuleLctX, 0, False);
end;

procedure TFmLctGer2.Localizarlanamentoorigem1Click(Sender: TObject);
begin
  LocalizarOLan�amentoDeOrigem();
end;

procedure TFmLctGer2.Localizarlanamentoorigem2Click(Sender: TObject);
begin
  LocalizarOLan�amentoDeOrigem();
end;

procedure TFmLctGer2.LocalizarOLan�amentoDeOrigem;
var
  Sub, Lancto, CtrlToLoc: Integer;
  Achou: Boolean;
begin
  Sub    := TDmLct2(FModuleLctX).QrLctSub.Value;
  Lancto := TDmLct2(FModuleLctX).QrLctCtrlQuitPg.Value;
  //
  if Lancto = 0 then
    Lancto := TDmLct2(FModuleLctX).QrLctID_Pgto.Value;
  if Lancto = 0 then
    Lancto := TDmLct2(FModuleLctX).QrLctID_Quit.Value;
  //
  UFinanceiro.LocalizarlanctoCliInt(Lancto, Sub, FThisEntidade, TPDataIni,
    TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt,
//    TDmLct2(FModuleLctX).QrCrtSum, True, FTabLctA, FTabLctB, FTabLctD);
    TDmLct2(FModuleLctX).QrCrtSum, False, FTabLctA, FTabLctB, FTabLctD,
    CtrlToLoc);
  if Achou = False then
  begin
    LocalizaLancto(CtrlToLoc);
  end;
end;

procedure TFmLctGer2.Localizatransferncia1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(3);
end;

procedure TFmLctGer2.Localizatransfernciaentrecarteiras1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(3);
end;

procedure TFmLctGer2.Mensalidades2Click(Sender: TObject);
begin
  FinanceiroJan.CriaImpressaoDiversos(FThisCliInt, 1);
end;

procedure TFmLctGer2.Modelo01RelExactus1Click(Sender: TObject);
begin
  {$IfDef TEM_UH}
  FinanceiroJan.MostraImportSal1(FTabLctA, FThisCliInt,
    TDmLct2(FModuleLctX).QrCrtCodigo.Value);
  {$EndIf}
end;

procedure TFmLctGer2.Modelo02txtPholha1Click(Sender: TObject);
begin
  {$IfDef TEM_UH}
  FinanceiroJan.MostraImportSal2(FTabLctA, FThisCliInt,
    TDmLct2(FModuleLctX).QrCrtCodigo.Value);
  {$EndIf}
end;

procedure TFmLctGer2.ModeloA1Click(Sender: TObject);
begin
  FinanceiroJan.MostraPesquisaPorNveldoPLanodeContas(FThisCliInt);
end;

procedure TFmLctGer2.ModeloA2Click(Sender: TObject);
begin
  FinanceiroJan.MostraReceDesp2(FThisCliInt);
end;

procedure TFmLctGer2.ModeloB1Click(Sender: TObject);
begin
  FinanceiroJan.MostraPesquisaPorNveldoPLanodeContas2(FThisCliInt);
end;

procedure TFmLctGer2.ModeloB2Click(Sender: TObject);
begin
  FinanceiroJan.DemonstrativoDeReceitasEDespesasFree(FThisCliInt);
end;

procedure TFmLctGer2.Movimento2Click(Sender: TObject);
begin
  FinanceiroJan.MostraMovimento(FThisCliInt);
end;

procedure TFmLctGer2.MovimentoPlanodecontas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraMovimentoPlanodeContas(FThisCliInt);
end;

procedure TFmLctGer2.MsanterioraoVencimento1Click(Sender: TObject);
begin
  UFinanceiro.ColocarMesDeCompetenciaOndeNaoTem(dcMesAnteriorAoVencto,
    TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, FTabLctA);
end;

procedure TFmLctGer2.MudarvencimentoLocao1Click(Sender: TObject);
begin
  UFinanceiro.MudaDataVenctoLLctEFatID(TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrLct, TDBGrid(DBGLct), FTabLctA)
end;

procedure TFmLctGer2.Novatransferncia1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(0);
end;

procedure TFmLctGer2.Novatransfernciaentrecontas1Click(Sender: TObject);
begin
  TranferenciaEntreContas(0);
end;

procedure TFmLctGer2.PagamentoRapido();
begin
  UFinanceiro.QuitacaoAutomaticaDmk(TDmkDBGrid(DBGLct), TDmLct2(FModuleLctX).QrLct,
    TDmLct2(FModuleLctX).QrCrt, FTabLctA, True);
  ReabreCfgAtual();
end;

procedure TFmLctGer2.Pagamentorpido2Click(Sender: TObject);
begin
  PagamentoRapido();
end;

procedure TFmLctGer2.Pagtorpidoselbanco1Click(Sender: TObject);
begin
  UFinanceiro.QuitacaoAutomaticaDmk2(TDmkDBGrid(DBGLct), TDmLct2(FModuleLctX).QrLct,
    TDmLct2(FModuleLctX).QrCrt, FTabLctA, True);
  ReabreCfgAtual();
end;

procedure TFmLctGer2.Pagamentorpido1Click(Sender: TObject);
begin
  PagamentoRapido();
end;

procedure TFmLctGer2.Pagamentosexecutados1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasConfPgto, FmContasConfPgto, afmoNegarComAviso) then
  begin
    with FmContasConfPgto do
    begin
      EdCliInt.ValueVariant := FThisEntidade;
      CBCliInt.KeyValue     := FThisEntidade;
      //
      FFinalidade      := FLctFinalidade;
      FQrLct           := TDmLct2(FModuleLctX).QrLct;
      FQrCrt           := TDmLct2(FModuleLctX).QrCrt;
      FPercJuroM       := Dmod.QrControle.FieldByName('MoraDD').AsFloat;
      FPercMulta       := Dmod.QrControle.FieldByName('Multa').AsFloat;
      FSetaVars        := nil;
      FAlteraAtehFatID := True;
      FLockCliInt      := True;
      FLockForneceI    := False;
      FLockAccount     := False;
      FLockVendedor    := False;
      FCliente         := 0;
      FFornecedor      := 0;
      FForneceI        := 0;
      FAccount         := 0;
      FVendedor        := 0;
      FIDFinalidade    := 2;
      FTabLctA         := TFmLctGer2(Self).FTabLctA;
      //
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmLctGer2.Pagar1Click(Sender: TObject);
begin
  PagarRolarEmissao;
end;

procedure TFmLctGer2.Pagar2Click(Sender: TObject);
begin
  PagarRolarEmissao;
end;

procedure TFmLctGer2.PagarAVista1Click(Sender: TObject);
begin
  UFinanceiro.PagarAVistaEmCaixa(TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrLct, TDBGrid(DBGLct), FTabLctA);
end;

procedure TFmLctGer2.PagarAVista2Click(Sender: TObject);
begin
  UFinanceiro.PagarAVistaEmCaixa(TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrLct, TDBGrid(DBGLct), FTabLctA, True);
end;

procedure TFmLctGer2.PagarReceber2Click(Sender: TObject);
begin
  FinanceiroJan.ExtratosFinanceirosEmpresaUnica2();
end;

procedure TFmLctGer2.PagarReceber31Click(Sender: TObject);
begin
  FinanceiroJan.ExtratosFinanceirosEmpresaUnica3();
end;

procedure TFmLctGer2.PagarRolarEmissao;
var
  Lancto, CtrlToLoc: Integer;
begin
  UFinanceiro.PagarRolarEmissao(TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrLct, FTabLctA, True, Lancto);
  //
  if Lancto <> 0 then
    UFinanceiro.LocalizarlanctoCliInt(Lancto, 0, FThisEntidade, TPDataIni,
      TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt,
      TDmLct2(FModuleLctX).QrCrtSum, True, FTabLctA, FTabLctB, FTabLctD,
      CtrlToLoc);
end;

procedure TFmLctGer2.partirdolanamentoselecionado1Click(Sender: TObject);
const
  Codigo = 0;
begin
{$IFNDEF NAO_USA_RECIBO}
  if TDmLct2(FModuleLctX).QrLctCredito.Value > 0 then
    GOTOy.EmiteRecibo(Codigo,
    TDmLct2(FModuleLctX).QrLctCliente.Value,
    FThisEntidade,
    TDmLct2(FModuleLctX).QrLctCredito.Value, 0, 0,
    IntToStr(TDmLct2(FModuleLctX).QrLctControle.Value) + '-' +
    IntToStr(TDmLct2(FModuleLctX).QrLctSub.Value),
    TDmLct2(FModuleLctX).QrLctDescricao.Value, '', '',
    TDmLct2(FModuleLctX).QrLctData.Value,
    TDmLct2(FModuleLctX).QrLctSit.Value)
  else
    GOTOy.EmiteRecibo(Codigo,
    FThisEntidade,
    TDmLct2(FModuleLctX).QrLctFornecedor.Value,
    TDmLct2(FModuleLctX).QrLctDebito.Value, 0, 0,
    IntToStr(TDmLct2(FModuleLctX).QrLctControle.Value) + '-' +
    IntToStr(TDmLct2(FModuleLctX).QrLctSub.Value),
    TDmLct2(FModuleLctX).QrLctDescricao.Value, '', '',
    TDmLct2(FModuleLctX).QrLctData.Value,
    TDmLct2(FModuleLctX).QrLctSit.Value);
{$Else}
  Geral.MB_Aviso('M�dulo de Recibo desabilitado!');
{$EndIf}
end;

procedure TFmLctGer2.Pesquisaagendamentodeprovisoefetivado1Click(
  Sender: TObject);
begin
  {$IfDef TEM_UH}
  UBloquetos_Jan.AgendamentoDeProvisaoEfetivado(TDmLct2(FModuleLctX).QrLctControle.Value,
    DModG.NomeTab(TMeuDB, ntPrv, False, ttA, FThisCliInt), True);
  {$EndIf}
end;

procedure TFmLctGer2.PMBloquetoPopup(Sender: TObject);
{$IfNDef sBLQ}
var
  Enab: Boolean;
{$EndIf}
begin
{$IfNDef sBLQ}
  if VAR_KIND_DEPTO <> kdUH then
  begin
    if DBGLct.SelectedRows.Count > 1 then
      Enab := False
    else
      Enab := UBloquetos.VerificaSeBoletoExiste(TDmLct2(FModuleLctX).QrLctControle.Value);
    //
    Gerabloqueto1.Enabled := not Enab;
    ImprimeGerenciabloqueto1.Enabled := Enab;
  end;
{$EndIf}
end;

procedure TFmLctGer2.PMMenuPopup(Sender: TObject);
begin
  if TDmLct2(FModuleLctX).QrLctGenero.Value = -1 then
  begin
    Alteratransfernciaentrecarteirasatual1.Enabled := True;
    Excluitransfernciaentrecarteirasatual1.Enabled := True;
  end else begin
    Alteratransfernciaentrecarteirasatual1.Enabled := False;
    Excluitransfernciaentrecarteirasatual1.Enabled := False;
  end;
  //
  Mudacarteiradelanamentosselecionados1.Enabled :=
    (TDmLct2(FModuleLctX).QrLct.State <> dsInactive) and
    (TDmLct2(FModuleLctX).QrLct.RecordCount > 0);
  //
  if DBGLct.SelectedRows.Count > 1 then
  begin
    Localizarlanamentoorigem1.Enabled := False;
  end else begin
    Localizarlanamentoorigem1.Enabled := TDmLct2(FModuleLctX).QrLctID_Pgto.Value > 0;
  end;
  ConfiguraPopoupQuitacoes();
end;

procedure TFmLctGer2.PMQuitaPopup(Sender: TObject);
begin
  ConfiguraPopoupQuitacoes();
end;

procedure TFmLctGer2.PMReciboPopup(Sender: TObject);
var
  Visi: Boolean;
begin
{$IFNDEF NAO_USA_RECIBO}
  Visi := True;
{$Else}
  Visi := False;
{$EndIf}
  if Visi = True then
    Visi := (TDmLct2(FModuleLctX).QrLct.State <> dsInactive) and
            (TDmLct2(FModuleLctX).QrLct.RecordCount > 0);
  //
  partirdolanamentoselecionado1.Visible := Visi;
end;

procedure TFmLctGer2.PrestaodeContas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraCashPreCta(FThisCliInt);
end;

procedure TFmLctGer2.ReabreCfgAtual();
begin
  TDmLct2(FModuleLctX).FCriandoLctGer2 := False;
  VeSeReabreLct(True);
  DBGLct.Visible := TDmLct2(FModuleLctX).QrLct.State <> dsInactive;
end;

procedure TFmLctGer2.RecalcSaldoCarteira();
begin
  UFinanceiro.RecalcSaldoCarteira(TDmLct2(FModuleLctX).QrCrtCodigo.Value,
    TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, True, True);
end;

procedure TFmLctGer2.Recebepagamentoeimprime1Click(Sender: TObject);
begin
  TDmLct2(FModuleLctX).GeraReciboImpCabOriDst(FTabLctA, TDBGrid(DBGLct),
    TDmLct2(FModuleLctX));
  VeSeReabreLct(True);
end;

procedure TFmLctGer2.Receitasedespesas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraReceDesp(FThisCliInt);
end;

procedure TFmLctGer2.Recibo1Click(Sender: TObject);
begin
  BtReciboClick(Self);
end;

procedure TFmLctGer2.Relatriodecheques1Click(Sender: TObject);
begin
  FinanceiroJan.MostraRelChAbertos(FThisCliInt);
end;

procedure TFmLctGer2.ResultadosMensais2Click(Sender: TObject);
begin
  FinanceiroJan.MostraResultadosMensais;
end;

procedure TFmLctGer2.Reverter1Click(Sender: TObject);
begin
  UFinanceiro.ReverterPagtoEmissao(TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt, True, True, True,
  FTabLctA);
end;

procedure TFmLctGer2.Reverter2Click(Sender: TObject);
begin
  UFinanceiro.ReverterPagtoEmissao(TDmLct2(FModuleLctX).QrLct,
    TDmLct2(FModuleLctX).QrCrt, True, True, True, FTabLctA);
end;

procedure TFmLctGer2.RGTipoDataClick(Sender: TObject);
begin
  TPDataIni.Visible := RGTipoData.ItemIndex <> 3;
  TPDataFim.Visible := RGTipoData.ItemIndex <> 3;
  VeSeReabreLct();
end;

procedure TFmLctGer2.Saldos2Click(Sender: TObject);
begin
  FinanceiroJan.SaldoDeContas(FThisCliInt);
end;

procedure TFmLctGer2.SalvarOrdenaodascolunasdagrade2Click(Sender: TObject);
begin
  VAR_SAVE_CFG_DBGRID_GRID := DBGLct;
  DModG.SalvaPosicaoColunasDmkDBGrid(VAR_USUARIO, False);
end;

procedure TFmLctGer2.BtLocCtrlClick(Sender: TObject);
begin
  if not TDmLct2(FModuleLctX).QrLct.Locate('Controle', EdLocCtrl.ValueVariant, []) then
    Geral.MB_Aviso('O lan�amento ' + IntToStr(EdLocCtrl.ValueVariant) +
    ' n�o foi localizado nesta carteira!');
end;

procedure TFmLctGer2.TPDataFimChange(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmLctGer2.TPDataFimClick(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmLctGer2.TPDataIniChange(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmLctGer2.TPDataIniClick(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmLctGer2.TranferenciaEntreCarteiras(Tipo: Integer);
begin
  UFinanceiro.CriarTransferCart(Tipo, TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt,
    True, FThisEntidade, 0, 0, 0, FTabLctA);
  //
  RecalcSaldoCarteira();
end;

procedure TFmLctGer2.TranferenciaEntreContas(Tipo: Integer);
begin
  UFinanceiro.CriarTransferCtas(Tipo, TDmLct2(FModuleLctX).QrLct, TDmLct2(FModuleLctX).QrCrt,
    True, FThisEntidade, 0, 0, 0, FTabLctA);
  //
  RecalcSaldoCarteira();
end;

procedure TFmLctGer2.Transformaremitemdebloqueto1Click(Sender: TObject);
const
  FatID = 0; // N�o bloquetos ainda! Vai gerar mensagem de aviso!
begin
  UFinanceiro.TransformarLancamentoEmItemDeBloqueto(FatID,
    TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, TDBGrid(DBGLct),
    FTabLctA);
end;

procedure TFmLctGer2.UmparacadaLanamento1Click(Sender: TObject);
begin
  {$IfNDef SemProtocolo}
    UnProtocolo.ProtocolosCD_Lct(istSelecionados, TDBGrid(DBGLct), FModuleLctX,
      FThisEntidade, Name, FTabLctA);
  {$Else}
    Grl_Geral.InfoSemModulo(mdlappProtocolos);
  {$EndIf}
end;

procedure TFmLctGer2.UmporDocumento1Click(Sender: TObject);
begin
  {$IfNDef SemProtocolo}
    UnProtocolo.ProtocolosCD_Doc(istSelecionados, TDBGrid(DBGLct), FModuleLctX,
      FThisEntidade, Name, FTabLctA);
  {$Else}
    Grl_Geral.InfoSemModulo(mdlappProtocolos);
  {$EndIf}
end;

procedure TFmLctGer2.Vencimento1Click(Sender: TObject);
begin
  UFinanceiro.ColocarMesDeCompetenciaOndeNaoTem(dcVencimento,
    TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, FTabLctA);
end;

procedure TFmLctGer2.Vencimento2Click(Sender: TObject);
begin
  UFinanceiro.MudaDataLancamentosSelecionados(cdVencimento,
    TDmLct2(FModuleLctX).QrCrt, TDmLct2(FModuleLctX).QrLct, TDBGrid(DBGLct),
    FTabLctA);
end;

procedure TFmLctGer2.VeSeReabreLct(Forca: Boolean);
(*
begin
  TDmLct2(FModuleLctX).FTipoData := RGTipoData.ItemIndex;
  //
  TDmLct2(FModuleLctX).VeSeReabreLct(TPDataIni, TPDataFim, 0,
    TDmLct2(FModuleLctX).QrLctCarteira.Value,
    TDmLct2(FModuleLctX).QrLctControle.Value,
    TDmLct2(FModuleLctX).QrLctSub.Value,
    TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrLct);
*)


(*
var
  Cliente, Fornecedor: Integer;
  CliFrnCondition: TCliFrnCondition;
  PsqCredito, PsqDebito, ForcaAbertura: Boolean;
*)
begin
  //
  TDmLct2(FModuleLctX).FTipoData := RGTipoData.ItemIndex;
  //
  TDmLct2(FModuleLctX).F2021 := True;
  //
  TDmLct2(FModuleLctX).F2021_Carteira         := EdCarteira.ValueVariant;
  TDmLct2(FModuleLctX).F2021_Cliente          := EdEntidade.ValueVariant;
  TDmLct2(FModuleLctX).F2021_Fornecedor       := EdEntidade.ValueVariant;
  TDmLct2(FModuleLctX).F2021_CliFrnCondition  := TCliFrnCondition.cofcBothOr;
  TDmLct2(FModuleLctX).F2021_PsqCredito       := CkCredito.Checked;
  TDmLct2(FModuleLctX).F2021_PsqDebito        := CkDebito.Checked;
  TDmLct2(FModuleLctX).F2021_Genese           := CkGenese.Checked;
  TDmLct2(FModuleLctX).F2021_Suplente         := CkSuplente.Checked;
  TDmLct2(FModuleLctX).F2021_AVencer          := CkAVencer.Checked;
  TDmLct2(FModuleLctX).F2021_Vencido          := CkVencido.Checked;
  TDmLct2(FModuleLctX).F2021_Quitado          := CkQuitado.Checked;
  //TDmLct2(FModuleLctX).F2021_ForcaAbertura    := True;
  //
  TDmLct2(FModuleLctX).VeSeReabreLct2021(Self,
    TPDataIni, TPDataFim, 0,
    TDmLct2(FModuleLctX).QrLctCarteira.Value,
    TDmLct2(FModuleLctX).QrLctControle.Value,
    TDmLct2(FModuleLctX).QrLctSub.Value,
    TDmLct2(FModuleLctX).QrCrt,
    TDmLct2(FModuleLctX).QrLct, Forca);
end;

(*   DBEdits de carteira e Terceiro
object EdCodigo: TDBEdit
  Left = 4
  Top = 20
  Width = 44
  Height = 26
  TabStop = False
  DataField = 'Codigo'
  DataSource = DmLct2.DsCrt
  Font.Charset = ANSI_CHARSET
  Font.Color = clBlue
  Font.Height = -15
  Font.Name = 'Arial'
  Font.Style = [fsBold]
  ParentFont = False
  ReadOnly = True
  TabOrder = 0
end
object EdNome: TDBEdit
  Left = 48
  Top = 20
  Width = 440
  Height = 26
  TabStop = False
  DataField = 'Nome'
  DataSource = DmLct2.DsCrt
  Font.Charset = ANSI_CHARSET
  Font.Color = clBlue
  Font.Height = -15
  Font.Name = 'Arial'
  Font.Style = [fsBold]
  ParentFont = False
  ReadOnly = True
  TabOrder = 1
end
object DBEdit5: TDBEdit
  Left = 492
  Top = 20
  Width = 296
  Height = 23
  DataField = 'NOMERELACIONADO'
  DataSource = DmLct2.DsLct
  Font.Charset = ANSI_CHARSET
  Font.Color = clNavy
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = [fsBold]
  ParentFont = False
  TabOrder = 2
end
*)
end.
