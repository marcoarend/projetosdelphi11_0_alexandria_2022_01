unit PreLctLoa;

interface

uses         
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, Grids, ComObj, ComCtrls,
  Variants, dmkGeral, DB, mySQLDbTables, DBGrids, dmkDBGrid, DBCtrls, dmkImage,
  UnDmkEnums;

type
  TFmPreLctLoa = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Grade1: TStringGrid;
    Panel3: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    LaAviso: TLabel;
    EdCaminho: TdmkEdit;
    PB1: TProgressBar;
    dmkDBGrid1: TdmkDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtAbre: TBitBtn;
    BtCarrega: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
  private
    { Private declarations }
    procedure CarregaXLS();
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmPreLctLoa: TFmPreLctLoa;

implementation

uses UnMyObjects, Module, UMySQLModule, MyVCLSkin;

const
  FCaminho = 'C:\_MLArend\Clientes\DDFog\Planilhas';
  FTits = 7;
  FTitulos: array[0..FTits-1] of String = (
{00}                            'Carteira',
{01}                            'Conta',
'Cliente',
'Fornecedor',
'Nome entidade',
'Credito',
'Debito');

{$R *.DFM}

procedure TFmPreLctLoa.BtAbreClick(Sender: TObject);
begin
  if pos('http', LowerCase(EdCaminho.ValueVariant)) > 0 then
  begin
    Geral.MB_Info('Voc� selecionar um arquivo um arquivo para abr�-lo!');
    Exit;
  end;
  //
  MyObjects.Xls_To_StringGrid(Grade1, EdCaminho.Text, PB1, LaAviso1, LaAviso2);
  BtCarrega.Enabled := True;
end;

procedure TFmPreLctLoa.BtCarregaClick(Sender: TObject);
begin
  CarregaXLS();
end;

procedure TFmPreLctLoa.CarregaXLS();
var
  R: Integer;
  DataStr, IndiceStr, Mes, Ano: String;
  Indice: Double;
  Data: TDateTime;
var
  Descricao: String;
  Codigo, Controle, Carteira, Genero, Cliente, Fornecedor, Mez, DiaVencto: Integer;
  Debito, Credito: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    PB1.Position := 0;
    PB1.Max := Grade1.RowCount;
    //  Le dados da Grade1 e grava no BD
    //
    for R := 1 to Grade1.RowCount -1 do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //
      Codigo         := FCodigo;
      Controle       := 0;
      Carteira       := Geral.IMV(Grade1.Cells[01, R]);
      Genero         := Geral.IMV(Grade1.Cells[02, R]);
      Cliente        := Geral.IMV(Grade1.Cells[03, R]);
      Fornecedor     := Geral.IMV(Grade1.Cells[04, R]);
      Mez            := Geral.IMV(Grade1.Cells[08, R]);
      Debito         := Geral.DMV(Grade1.Cells[07, R]);
      Credito        := Geral.DMV(Grade1.Cells[06, R]);
      Descricao      := Grade1.Cells[10, R];
      DiaVencto      := Geral.IMV(Grade1.Cells[09, R]);
      //
      if (Cliente <> 0) or (Fornecedor <> 0) or (Credito <> 0) or (Debito <> 0) then
      begin
        Controle := UMyMod.BPGS1I32('prelctits', 'Controle', '', '', tsPos, stIns, Controle);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prelctits', False, [
        'Codigo', 'Carteira',
        'Genero', 'Cliente',
        'Fornecedor', 'Mez', 'Debito',
        'Credito', 'Descricao',
        'DiaVencto'], [
        'Controle'], [
        Codigo, Carteira,
        Genero, Cliente,
        Fornecedor, Mez, Debito,
        Credito, Descricao,
        DiaVencto], [
        Controle], True) then
        begin
          //
        end;
      end;
    end;
    PB1.Position := PB1.Max;
    Geral.MB_Info('Importa��o finalizada!');
  finally
    LaAviso.Caption := '';
    Screen.Cursor   := crDefault;
  end;
end;

procedure TFmPreLctLoa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPreLctLoa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPreLctLoa.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  ImgTipo.SQLType        := stLok;
  EdCaminho.ValueVariant := FCaminho;
  //
  Grade1.ColCount := FTits;
  //
  for I := 0 to FTits - 1 do
    Grade1.Cells[I, 0] := FTitulos[I];
  //
end;

procedure TFmPreLctLoa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPreLctLoa.SpeedButton8Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := '';
  Arquivo := '';
  //
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo, 'Selecione o arquivo',
    '', [], Arquivo)
  then
    EdCaminho.Text := Arquivo;
end;

end.
