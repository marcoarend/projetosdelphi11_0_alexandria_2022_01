unit LctEncerraMes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DBCtrls, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, Mask, Menus,
  ComCtrls, dmkImage, UnDmkEnums;

type
  TSentidoMigracao = (smEncerra, smAtiva);
  TFmLctEncerraMes = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrLast: TmySQLQuery;
    QrLastData: TDateField;
    QrPeriodos: TmySQLQuery;
    QrFeitos: TmySQLQuery;
    QrFeitosAM: TWideStringField;
    QrPeriodosAM: TWideStringField;
    QrFim: TmySQLQuery;
    QrMov: TmySQLQuery;
    QrFimCarteira: TIntegerField;
    QrFimSaldo: TFloatField;
    QrFimCredito: TFloatField;
    QrFimDebito: TFloatField;
    QrFimLctos: TLargeintField;
    QrLocAnt: TmySQLQuery;
    QrLocAntSaldoFim: TFloatField;
    QrLctoEncer: TmySQLQuery;
    DsLctoEncer: TDataSource;
    QrLctoEncerData: TDateField;
    QrLctoEncerCarteira: TIntegerField;
    QrLctoEncerLctos: TIntegerField;
    QrLctoEncerSaldoIni: TFloatField;
    QrLctoEncerCreditos: TFloatField;
    QrLctoEncerDebitos: TFloatField;
    QrLctoEncerSaldoFim: TFloatField;
    QrLctoEncerDataCad: TDateField;
    QrLctoEncerDataAlt: TDateField;
    QrLctoEncerUserCad: TIntegerField;
    QrLctoEncerUserAlt: TIntegerField;
    QrMeses: TmySQLQuery;
    DsMeses: TDataSource;
    QrLctoEncerNO_CARTEIRA: TWideStringField;
    QrMesesSaldoIni: TFloatField;
    QrMesesCreditos: TFloatField;
    QrMesesDebitos: TFloatField;
    QrMesesSaldoFim: TFloatField;
    QrMesesAM: TWideStringField;
    QrMesesData: TDateField;
    QrIniCart: TmySQLQuery;
    Panel5: TPanel;
    Panel6: TPanel;
    GradeMeses: TDBGrid;
    Pn_: TPanel;
    GroupBox1: TGroupBox;
    LaAno: TLabel;
    LaMes: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    QrMovCarteira: TIntegerField;
    QrMovLctos: TLargeintField;
    QrMovCredito: TFloatField;
    QrMovDebito: TFloatField;
    PMEncerra: TPopupMenu;
    Msatual1: TMenuItem;
    odosmeses1: TMenuItem;
    Encerramsinformado1: TMenuItem;
    Desfazencerramento1: TMenuItem;
    QrMesesTabLct: TWideStringField;
    QrBAnt: TmySQLQuery;
    QrBAntCarteira: TIntegerField;
    QrBAntGenero: TIntegerField;
    QrBAntSaldo: TFloatField;
    QrBAntTipo: TSmallintField;
    QrSumCrt: TmySQLQuery;
    PnAvisos: TPanel;
    LaAviso1: TLabel;
    PB1: TProgressBar;
    LaAviso2: TLabel;
    QrSumCrtCarteira: TIntegerField;
    QrSumCrtSaldo: TFloatField;
    QrSumCrtNome: TWideStringField;
    QrCrts: TmySQLQuery;
    QrCrtsCodigo: TIntegerField;
    QrSum1: TmySQLQuery;
    QrSum1Saldo: TFloatField;
    QrSum2: TmySQLQuery;
    QrSum2Saldo: TFloatField;
    QrErrCrt: TmySQLQuery;
    QrErrCrtValor: TFloatField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet2: TTabSheet;
    DsFim: TDataSource;
    DBGrid2: TDBGrid;
    TabSheet3: TTabSheet;
    DsMov: TDataSource;
    DBGrid3: TDBGrid;
    CkComparaSaldos: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtEncerra: TBitBtn;
    BtArquiva: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QrPeriodosAno: TFloatField;
    QrPeriodosMes: TFloatField;
    QrPeriodosPeriodo: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure QrMesesBeforeClose(DataSet: TDataSet);
    procedure QrMesesAfterScroll(DataSet: TDataSet);
    procedure BtArquivaClick(Sender: TObject);
    procedure QrMesesAfterOpen(DataSet: TDataSet);
    procedure Msatual1Click(Sender: TObject);
    procedure odosmeses1Click(Sender: TObject);
    procedure Encerramsinformado1Click(Sender: TObject);
    procedure PMEncerraPopup(Sender: TObject);
    procedure GradeMesesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
    FEmpresa, FEntidade: Integer;
    FTabLctA, FTabLctB, FTabLctD: String;
    FDtEncer, FDtMorto: TDateTime;
    //
    function  EncerraPeriodoSelecionado(): Boolean;
    procedure ReopenMeses(Data: TDateTime);
    procedure ReopenLctoEncer(Carteira: Integer);
    function  MigraLancamentos(Origem, Destino, Data: String;
              Sentido: TSentidoMigracao): Boolean;
    procedure EncerraMesSelecionado();
    function  DesfazMesAtual(): Boolean;
    procedure AtualizaSdoFimB(Entidade, Empresa: Integer);
    function  ComparacaoDeSaldos(): Boolean;
  public
    { Public declarations }
  end;

  var
  FmLctEncerraMes: TFmLctEncerraMes;

implementation

uses UnMyObjects, ModuleGeral, Module, UMySQLModule, UnInternalConsts,
  UnFinanceiro, ModuleFin, DmkDAC_PF, UnDmkProcFunc, UnFinanceiroJan;

{$R *.DFM}

procedure TFmLctEncerraMes.AtualizaSdoFimB(Entidade, Empresa: Integer);
begin
  MyObjects.Informa(LaAviso2, False, '...');
  MyObjects.Informa(LaAviso1, True, 'Atualizando saldos iniciais das carteiras');
  //
  PB1.Position := 0;
  QrSumCrt.Close;
  QrSumCrt.SQL.Clear;
{
SELECT car.Nome, lanb.Carteira,
SUM(lanb.Credito - lanb.Debito) Saldo
FROM lct 0001b lanb
LEFT JOIN carteiras car ON car.Codigo=lanb.Carteira
WHERE Carteira IN
(
  SELECT Codigo
  FROM carteiras
  WHERE ForneceI=:P0
  AND Tipo <> 2
)
GROUP BY Carteira
}
  QrSumCrt.SQL.Add('SELECT car.Nome, car.Codigo Carteira,');
  QrSumCrt.SQL.Add('SUM(lanb.Credito - lanb.Debito) Saldo');
  QrSumCrt.SQL.Add('FROM carteiras car ');
  QrSumCrt.SQL.Add('LEFT JOIN ' + DModG.NomeTab(TMeuDB, ntLct, False, ttB, Empresa) + ' lanb ON car.Codigo=lanb.Carteira');
  QrSumCrt.SQL.Add('WHERE car.ForneceI=' + FormatFloat('0', Entidade));
  QrSumCrt.SQL.Add('GROUP BY car.Codigo');
  QrSumCrt.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrSumCrt, Dmod.MyDB);
  //
  PB1.Max := QrSumCrt.RecordCount;
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE carteiras SET ');
  DMod.QrUpd.SQL.Add('SdoFimB=:P0 WHERE Codigo=:P1');
  while not QrSumCrt.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    MyObjects.Informa(LaAviso2, True, QrSumCrtNome.Value);
    //
    DMod.QrUpd.Params[00].AsFloat   := QrSumCrtSaldo.Value;
    DMod.QrUpd.Params[01].AsInteger := QrSumCrtCarteira.Value;
    DMod.QrUpd.ExecSQL;
    //
    QrSumCrt.Next;
  end;
  MyObjects.Informa(LaAviso2, False, '...');
  MyObjects.Informa(LaAviso1, False, '...');
end;

procedure TFmLctEncerraMes.BitBtn1Click(Sender: TObject);
begin
  ComparacaoDeSaldos()
end;

procedure TFmLctEncerraMes.BitBtn2Click(Sender: TObject);
begin
  AtualizaSdoFimB(FEntidade, FEmpresa);
end;

procedure TFmLctEncerraMes.BtArquivaClick(Sender: TObject);
const
  Descricao = 'S A L D O   A N T E R I O R';
  Sit = 3;
  FatNum = 0;
  FatParcela = 0;
  FatID_Sub = 0;
  Sub = 0;
var
  UltDia: TDateTime;
  Data, Compensado, Vencimento: String;
  Continua, Controle, Genero, Carteira, CliInt, Tipo: Integer;
  Debito, Credito: Double;
begin
{
  if not DModG.EmpresaAtual_SetaCodigos(DModG.QrEmpresasCodigo.Value,
    tecEntidade, True) then
    Exit;
}
  //
  // Verifica se saldos coicidem
  if ComparacaoDeSaldos() = False then
{
  begin
    if Geral.MB_Pergunta('Deseja continuar mesmo com os erros de saldos?') <> ID_YES then
      Exit;
  end;
}
    Exit;
  //
  UltDia := QrMesesData.Value;
  Data := Geral.FDT(UltDia, 3);
  Continua := Geral.MB_Pergunta('Confirma o envio do movimento at� o dia ' + Data +
  ' para o arquivo morto?');
  if Continua = ID_YES then
  begin
    Continua := Geral.MB_Pergunta('Os lan�amentos enviados para o arquivo ' +
    'morto n�o poder�o ser revertidos para edi��o / exclus�o!' + sLineBreak +
    'Deseja continuar assim mesmo?');
  end;
  if Continua = ID_YES then
  begin
    Data := Geral.FDT(UltDia, 1);
    QrBAnt.Close;
    QrBAnt.SQL.Clear;
    QrBAnt.SQL.Add('SELECT Carteira, Genero, Tipo,');
    QrBAnt.SQL.Add('SUM(Credito) - SUM(Debito) Saldo');
    QrBAnt.SQL.Add('FROM ' + FTabLctB);
    QrBAnt.SQL.Add('WHERE Data <= :P0');
    QrBAnt.SQL.Add('GROUP BY Carteira, Genero');
    QrBAnt.Params[0].AsString := Data;
    UnDmkDAC_PF.AbreQuery(QrBAnt, Dmod.MyDB);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctB + ' SET ');
    Dmod.QrUpd.SQL.Add('Antigo="E X C L U I R" ');  // para poder excluir depois
    Dmod.QrUpd.SQL.Add('WHERE FatID = ' + LAN_SDO_INI_FATID);
    Dmod.QrUpd.ExecSQL;
    //
    if MigraLancamentos(FTabLctB, FTabLctD, Data, smEncerra) then
    begin
      while not QrBAnt.Eof do
      begin
        if QrBAntSaldo.Value <> 0 then
        begin
          CliInt     := FEmpresa;
          Compensado := Data;
          Vencimento := Data;
          Genero     := QrBAntGenero.Value;
          Carteira   := QrBAntCarteira.Value;
          Tipo       := QrBAntTipo.Value;
          if QrBAntSaldo.Value > 0 then
          begin
            Credito  := QrBAntSaldo.Value;
            Debito   := 0;
          end else begin
            Credito  := 0;
            Debito   := - QrBAntSaldo.Value;
          end;
          Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, FTabLctB,
            'Controle', [], [], stIns, 0, siPositivo, nil);
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, FTabLctB, False, [
            'Genero', 'Descricao', 'Debito', 'Credito', 'Compensado',
            'Sit', 'Vencimento', 'FatID', 'FatID_Sub',
            'FatNum', 'FatParcela', 'CliInt'
          ], [
            'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
            Genero, Descricao, Debito, Credito, Compensado,
            Sit, Vencimento, LAN_SDO_INI_FATID, FatID_Sub,
            FatNum, FatParcela, CliInt
          ], [
            Data, Tipo, Carteira, Controle, Sub], True);
        end;
        //
        QrBAnt.Next;
      end;
{
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add(EXCLUI_DE + FTabLctD + ' WHERE ');
      Dmod.QrUpd.SQL.Add('Antigo="E X C L U I R" ');
      Dmod.QrUpd.SQL.Add('AND FatID = ' + LAN_SDO_INI_FATID);
      Dmod.QrUpd.ExecSQL;
}
      DmodFin.QrLcts.Close;
      DmodFin.QrLcts.SQL.Clear;
      DmodFin.QrLcts.SQL.Add('SELECT Data, Tipo, Carteira, Controle, Sub, FatID');
      DmodFin.QrLcts.SQL.Add('FROM ' + FTabLctD);
      DmodFin.QrLcts.SQL.Add('WHERE Antigo="E X C L U I R" ');
      DmodFin.QrLcts.SQL.Add('AND FatID = ' + LAN_SDO_INI_FATID);
      UnDmkDAC_PF.AbreQuery(DmodFin.QrLcts, Dmod.MyDB);
      while not DmodFin.QrLcts.Eof do
      begin
        UFinanceiro.ExcluiLct_Unico(FTabLctA, Dmod.MyDB, DmodFin.QrLctsData.Value,
          DmodFin.QrLctsTipo.Value, DmodFin.QrLctsCarteira.Value,
          DmodFin.QrLctsControle.Value, DmodFin.QrLctsSub.Value,
          dmkPF.MotivDel_ValidaCodigo(300), False);
        //
        DmodFin.QrLcts.Next;
      end;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lctoencer lce, carteiras car');
      Dmod.QrUpd.SQL.Add('SET lce.TabLct="D"');
      Dmod.QrUpd.SQL.Add('WHERE lce.Carteira=car.Codigo');
      Dmod.QrUpd.SQL.Add('AND lce.Data <=:P0');
      Dmod.QrUpd.Params[0].AsString := Data;
      Dmod.QrUpd.ExecSQL;
      //
      AtualizaSdoFimB(FEntidade, FEmpresa);
      //
      ReopenMeses(UltDia);
      //
      // Verifica se saldos coicidem
      ComparacaoDeSaldos();
      //
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctEncerraMes.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmLctEncerraMes.EncerraMesSelecionado();
var
  DtIni, DtFim: TDateTime;
  DataI, DataF: String;
  Continua: Integer;
begin
  if MyObjects.FIC(FEmpresa = 0, EdEmpresa, 'Informe o cliente interno') then Exit;
  //
{
  if not DModG.EmpresaAtual_SetaCodigos(DModG.QrEmpresasCodigo.Value,
    tecEntidade, True) then
    Exit;
}  //
  if Geral.MB_Pergunta('Confirma o encerramento do m�s selecionado') <> ID_YES then
    Exit;
  // Verifica se saldos coicidem
  if ComparacaoDeSaldos() = False then
    Exit;
  //
  PB1.Position := 0;
  MyObjects.Informa(LaAviso1, True, 'Encerramento mensal');
  MyObjects.Informa(LaAviso2, True, 'Preparando encerramento');
  //
  Screen.Cursor := crHourGlass;
  try
    QrLast.Close;
    QrLast.Params[0].AsInteger := FEntidade;
    UnDmkDAC_PF.AbreQuery(QrLast, Dmod.MyDB);
    //
    QrFeitos.Close;
    QrFeitos.Params[0].AsInteger := FEntidade;
    UnDmkDAC_PF.AbreQuery(QrFeitos, Dmod.MyDB);
    //
    DtIni := QrLastData.Value + 1;
    DtFim := (MyObjects.CBAnoECBMesToDate(CBAno, CBMes, 1, 1) -1);
    if DtIni >= DtFim then
    begin
      Geral.MB_Aviso('M�s j� encerrado!');
      Screen.Cursor := crDefault;
      Exit;
    end;
    DataI := Geral.FDT(DtIni, 1);
    DataF := Geral.FDT(DtFim, 1);
    //
    QrPeriodos.Close;
    QrPeriodos.SQL.Clear;
    QrPeriodos.SQL.Add('SELECT DISTINCT YEAR(lct.Data)+0.000 Ano, MONTH(lct.Data)+0.000 Mes,');
    QrPeriodos.SQL.Add('((YEAR(lct.Data) - 2000) * 12) + MONTH(lct.Data) + 0.000 Periodo,');
    QrPeriodos.SQL.Add('CONCAT(LPAD(MONTH(lct.Data), 2, "0"), YEAR(lct.Data)) AM');
    QrPeriodos.SQL.Add('FROM carteiras car');
    QrPeriodos.SQL.Add('LEFT JOIN ' + FTabLctA + ' lct ON car.Codigo=lct.Carteira');
    QrPeriodos.SQL.Add('WHERE car.ForneceI=:P0');
    QrPeriodos.SQL.Add('AND lct.Data BETWEEN :P1 AND :P2');
    QrPeriodos.Params[00].AsInteger := FEntidade;
    QrPeriodos.Params[01].AsString  := DataI;
    QrPeriodos.Params[02].AsString  := DataF;
    UnDmkDAC_PF.AbreQuery(QrPeriodos, Dmod.MyDB);
    PB1.Max := QrPeriodos.RecordCount;

    //
    Continua := ID_NO;
    case QrPeriodos.RecordCount of
      0: Geral.MB_Aviso('N�o h� dados para encerramento de m�s solicitado!');
      1: Continua := ID_YES;
      else Continua := Geral.MB_Pergunta('Para encerrar o m�s solicitado' +
      ' � necess�rio encerrar ' + FormatFloat('0', QrPeriodos.RecordCount-1) +
      ' per�odos anteriores!'+sLineBreak+'Deseja encerrar assim mesmo?');
    end;
    if Continua = ID_YES then
    begin
      QrPeriodos.First;
      while not QrPeriodos.Eof do
      begin
        PB1.Position := PB1.Position + 1;
        Update;
        Application.ProcessMessages;
        //
        if not EncerraPeriodoSelecionado() then
        begin
          Screen.Cursor := crDefault;
          Break;
        end;
        //
        QrPeriodos.Next;
      end;
    end;
    Exit;
  finally
    AtualizaSdoFimB(FEntidade, FEmpresa);
    ReopenMeses(0);
    // Verifica se saldos coicidem
    ComparacaoDeSaldos();
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctEncerraMes.Encerramsinformado1Click(Sender: TObject);
begin
  {
  Parei Aqui
  Encerrar gerenciamento de per�odo?:
  prev
  TAB_PRI
  TAB_ARI
  TAB_CNS
  }

  if DModFin.EntidadeHabilitadadaParaFinanceiroNovo(FEntidade,
      True, True) <> sfnNovoFin then Exit;
  if FinanceiroJan.LancamentosComProblemas(FTabLctA) then Exit;

  EncerraMesSelecionado();
end;

procedure TFmLctEncerraMes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmLctEncerraMes.ComparacaoDeSaldos(): Boolean;
  function ValorSum2(FatID: Boolean): Int64;
  var
    Entidade: String;
  begin
    Entidade := FormatFloat('0', DModG.QrEmpresasCodigo.Value);
    //
    QrSum2.Close;
    QrSum2.SQL.Clear;
    QrSum2.SQL.Add('DROP TABLE IF EXISTS _soma_cart_;');
    QrSum2.SQL.Add('CREATE TABLE _soma_cart_');
    QrSum2.SQL.Add('SELECT "A" Tipo, SUM(Credito-Debito) Saldo');
    QrSum2.SQL.Add('FROM ' + TMeuDB + '.carteiras car ');
    QrSum2.SQL.Add('LEFT JOIN ' + FTabLctA + ' lct ON car.Codigo=lct.Carteira');
    QrSum2.SQL.Add('WHERE (car.Tipo <> 2) OR (Carteira=0)');
    QrSum2.SQL.Add('');
    QrSum2.SQL.Add('UNION');
    QrSum2.SQL.Add('');
    QrSum2.SQL.Add('SELECT "B" Tipo, SUM(Credito-Debito) Saldo');
    QrSum2.SQL.Add('FROM ' + TMeuDB + '.carteiras car ');
    QrSum2.SQL.Add('LEFT JOIN ' + FTabLctB + ' lct ON car.Codigo=lct.Carteira');
    QrSum2.SQL.Add('WHERE (car.Tipo <> 2) OR (Carteira=0)');
    if FatID then
      QrSum2.SQL.Add('AND (FatID <> ' + LAN_SDO_INI_FATID + ' AND Controle>0)');
    QrSum2.SQL.Add('');
    QrSum2.SQL.Add('UNION');
    QrSum2.SQL.Add('');
    QrSum2.SQL.Add('SELECT "D" Tipo, SUM(Credito-Debito) Saldo');
    QrSum2.SQL.Add('FROM ' + TMeuDB + '.carteiras car ');
    QrSum2.SQL.Add('LEFT JOIN ' + FTabLctD + ' lct ON car.Codigo=lct.Carteira');
    QrSum2.SQL.Add('WHERE (car.Tipo <> 2) OR (Carteira=0);');
    QrSum2.SQL.Add('');
    QrSum2.SQL.Add('SELECT SUM(Saldo) Saldo');
    QrSum2.SQL.Add('FROM _soma_cart_;');
    QrSum2.SQL.Add('');
    QrSum2.SQL.Add('DROP TABLE IF EXISTS _soma_cart_;');
    UnDmkDAC_PF.AbreQuery(QrSum2, DModG.MyPID_DB);
    //
    Result := Trunc(QrSum2Saldo.Value * 100);
    //
    QrSum2.CLose;
    QrSum2.SQL.Clear;
    QrSum2.SQL.Add('DROP TABLE IF EXISTS _soma_cart_;');
    QrSum2.SQL.Add('CREATE TABLE _soma_cart_');
    QrSum2.SQL.Add('');
    QrSum2.SQL.Add('SELECT SUM(Credito-Debito) Saldo');
    QrSum2.SQL.Add('FROM ' + FTabLctA);
    QrSum2.SQL.Add('WHERE Carteira NOT IN (SELECT Codigo FROM ' + TMeuDB + '.carteiras');
    QrSum2.SQL.Add('WHERE ForneceI=' + Entidade + ')');
    QrSum2.SQL.Add('');
    QrSum2.SQL.Add('UNION');
    QrSum2.SQL.Add('');
    QrSum2.SQL.Add('SELECT SUM(Credito-Debito) Saldo');
    QrSum2.SQL.Add('FROM ' + FTabLctB);
    QrSum2.SQL.Add('WHERE Carteira NOT IN (SELECT Codigo FROM ' + TMeuDB + '.carteiras');
    QrSum2.SQL.Add('WHERE ForneceI=' + Entidade + ')');
    QrSum2.SQL.Add('');
    QrSum2.SQL.Add('UNION');
    QrSum2.SQL.Add('');
    QrSum2.SQL.Add('SELECT SUM(Credito-Debito) Saldo');
    QrSum2.SQL.Add('FROM ' + FTabLctB);
    QrSum2.SQL.Add('WHERE Carteira NOT IN (SELECT Codigo FROM ' + TMeuDB + '.carteiras');
    QrSum2.SQL.Add('WHERE ForneceI=' + Entidade + ');');
    QrSum2.SQL.Add('');
    QrSum2.SQL.Add('SELECT SUM(Saldo) Saldo');
    QrSum2.SQL.Add('FROM _soma_cart_;');
    QrSum2.SQL.Add('');
    QrSum2.SQL.Add('DROP TABLE IF EXISTS _soma_cart_;');
    QrSum2.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(QrSum2, DModG.MyPID_DB);
    //
    Result := Result - Trunc(QrSum2Saldo.Value * 100);
    //
  end;
var
  ValorSum1: Int64;
  //Igual: Boolean;
  MyCursor: TCursor;
begin
  if CkComparaSaldos.Checked = False then
  begin
    Result := True;
    Exit;
  end;
  //Result := False;
  MyCursor := Screen.Cursor;
  Screen.cursor := crHourGlass;
  Application.ProcessMessages;
  QrCrts.Close;
  QrCrts.Params[00].AsInteger := FEntidade;
  UnDmkDAC_PF.AbreQuery(QrCrts, Dmod.MyDB);
  while not QrCrts.Eof do
  begin
    UFinanceiro.RecalcSaldoCarteira(QrCrtsCodigo.Value, nil, nil, False, False);
    QrCrts.Next;
  end;
  QrSum1.Close;
  QrSum1.Params[00].AsInteger := FEntidade;
  UnDmkDAC_PF.AbreQuery(QrSum1, Dmod.MyDB);
  //
  ValorSum1 := Trunc(QrSum1Saldo.Value * 100);
  Result := ValorSum1 = ValorSum2(False);
  if not Result then
    Result := ValorSum1 = ValorSum2(True);
  if not Result then
    Geral.MB_Erro(
  'Saldos atuais das carteiras n�o conferem com as somas dos arquivos "a", "b" e "d"!'
  + sLineBreak + 'Saldo total atual: ' + FormatFloat('#,###,###,##0.00', QrSum1Saldo.Value)
  + sLineBreak + 'Saldo total a+b+d: ' + FormatFloat('#,###,###,##0.00', QrSum2Saldo.Value)
  + sLineBreak + 'AVISE A DERMATEK!');
  Screen.Cursor := MyCursor;
end;

function TFmLctEncerraMes.DesfazMesAtual: Boolean;
var
  Data: String;
  //Dia, Mes, Ano: Word;
begin
  Result := False;
  if MyObjects.FIC(FEntidade = 0, EdEmpresa, 'Informe o cliente interno') then Exit;
  //
{
  if not DModG.EmpresaAtual_SetaCodigos(DModG.QrEmpresasCodigo.Value,
    tecEntidade, True) then
    Exit;
  //
}
  // Verifica se j� n�o est� no arquivo morto!
  if MyObjects.FIC(Lowercase(QrMesesTabLct.Value) <> 'b', nil,
  'Desfazimento cancelado! M�s j� se encontra no arquivo morto!') then Exit;
  //
  // Verifica se saldos coicidem
  if ComparacaoDeSaldos() = False then
    Exit;
  //
  if Geral.MB_Pergunta('Confirma o cancelamento do encerramento do m�s: ' +
  QrMesesAM.Value + '?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Data := Geral.FDT(QrMesesData.Value, 1);
      //
      DMod.QrUpd.SQL.Clear;
      DMod.QrUpd.SQL.Add('DELETE lae');
      DMod.QrUpd.SQL.Add('FROM lctoencer lae, carteiras car');
      DMod.QrUpd.SQL.Add('WHERE lae.Carteira=car.Codigo');
      DMod.QrUpd.SQL.Add('AND car.ForneceI=:P0');
      DMod.QrUpd.SQL.Add('AND lae.Data=:P1');
      DMod.QrUpd.Params[00].AsInteger := FEntidade;
      DMod.QrUpd.Params[01].AsString  := Data;
      DMod.QrUpd.ExecSQL;
      //
      Data := Geral.FDT(Geral.PrimeiroDiaDoMes(QrMesesData.Value), 1);
      MigraLancamentos(FTabLctB, FTabLctA, Data, smAtiva);
      //
      ReopenMeses(0);
      //
      AtualizaSdoFimB(FEntidade, FEmpresa);
      //
      // Verifica se saldos coicidem
      Result := ComparacaoDeSaldos();
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmLctEncerraMes.EdEmpresaChange(Sender: TObject);
begin
  if EdEmpresa.ValueVariant = 0 then
    FEntidade := 0
  else begin
    FEntidade := DModG.QrEmpresasCodigo.Value;
    FEmpresa  := DModG.QrEmpresasFilial.Value;  // CliInt
    if (FEntidade <> 0) and (FEmpresa <> 0) then
      DModG.Def_EM_ABD(TMeuDB,
        FEntidade, FEmpresa, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  end;
  ReopenMeses(0);
end;

function TFmLctEncerraMes.EncerraPeriodoSelecionado(): Boolean;
var
  DataI, DataF, DataA: String;
  DataD: TDateTime;
  //
  Lctos, Carteira, Certos, Periodo: Integer;
  SaldoIni, Creditos, Debitos, SaldoFim: Double;
  AnoMes, Data: String;
  A, B: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;

  //
  try
    Periodo := Trunc(QrPeriodosPeriodo.Value);
    DataD := Geral.PeriodoToDate(Periodo, 1, False);
    DataI := Geral.FDT(DataD, 1);
    AnoMes := Geral.FDT(DataD, 20);
    DataD := Geral.PeriodoToDate(Periodo + 1, 1, False) - 1;
    DataF := Geral.FDT(DataD, 1);
    DataD := Geral.PeriodoToDate(Periodo, 1, False) - 1;
    DataA := Geral.FDT(DataD, 1);
    MyObjects.Informa(LaAviso2, True,
      'Encerrando per�odo de ' + DataI + ' at� ' + DataF);
    //
    QrMov.Close;
    QrMov.SQL.Clear;
    QrMov.SQL.Add('SELECT car.Codigo Carteira, COUNT(Controle) Lctos,');
    QrMov.SQL.Add('IF(car.Tipo=2, 0, SUM(lan.Credito)) Credito,');
    QrMov.SQL.Add('IF(car.Tipo=2, 0, SUM(lan.Debito)) Debito');
    QrMov.SQL.Add('FROM carteiras car');
    QrMov.SQL.Add('LEFT JOIN ' + FTabLctA + ' lan ON car.Codigo=lan.Carteira');
    QrMov.SQL.Add('WHERE car.ForneceI=:P0');
    QrMov.SQL.Add('AND Data BETWEEN :P1 AND :P2');
    QrMov.SQL.Add('GROUP BY car.Codigo');
    QrMov.SQL.Add('');
    QrMov.Params[00].AsInteger := FEntidade;
    QrMov.Params[01].AsString := DataI;
    QrMov.Params[02].AsString := DataF;
    UnDmkDAC_PF.AbreQuery(QrMov, Dmod.MyDB);
    //  Deve ser depois pois tem lookup
    QrFim.Close;
    QrFim.SQL.Clear;
    QrFim.SQL.Add('DROP TABLE IF EXISTS _sdofim_;');
    QrFim.SQL.Add('CREATE TABLE _sdofim_');
    QrFim.SQL.Add('SELECT "A" Tabela, car.Codigo Carteira, IF(car.Tipo=2, 0,');
    QrFim.SQL.Add('  SUM(lan.Credito-lan.Debito)) Saldo');
    QrFim.SQL.Add('FROM ' + TMeuDB + '.carteiras car');
    QrFim.SQL.Add('LEFT JOIN ' + FTabLctA + ' lan ON car.Codigo=lan.Carteira');
    QrFim.SQL.Add('WHERE car.ForneceI=:P0');
    QrFim.SQL.Add('AND Data <= :P1');
    QrFim.SQL.Add('GROUP BY car.Codigo');
    QrFim.SQL.Add('');
    QrFim.SQL.Add('UNION');
    QrFim.SQL.Add('');
    QrFim.SQL.Add('SELECT "B" Tabela, car.Codigo Carteira, IF(car.Tipo=2, 0,');
    QrFim.SQL.Add('  SUM(lan.Credito-lan.Debito)) Saldo');
    QrFim.SQL.Add('FROM ' + TMeuDB + '.carteiras car');
    QrFim.SQL.Add('LEFT JOIN ' + FTabLctB + ' lan ON car.Codigo=lan.Carteira');
    QrFim.SQL.Add('WHERE car.ForneceI=:P2');
    QrFim.SQL.Add('GROUP BY car.Codigo;');
    QrFim.SQL.Add('');
    QrFim.SQL.Add('SELECT Carteira, SUM(Saldo) Saldo');
    QrFim.SQL.Add('FROM _sdofim_');
    QrFim.SQL.Add('GROUP BY Carteira;');
    //
    QrFim.Params[00].AsInteger := FEntidade;
    QrFim.Params[01].AsString  := DataF;
    QrFim.Params[02].AsInteger := FEntidade;
    UnDmkDAC_PF.AbreQuery(QrFim, DModG.MyPID_DB);
    //
    Certos := 0;
    QrFim.First;
    while not QrFim.Eof do
    begin
      Carteira := QrFimCarteira.Value;
      //Lctos    := QrFimLctos.Value;
      Creditos := QrFimCredito.Value;
      Debitos  := QrFimDebito.Value;
      SaldoFim := QrFimSaldo.Value;
      SaldoIni := SaldoFim - Creditos + Debitos;
      Data     := DataF;
      //
      QrLocAnt.Close;
      QrLocAnt.Params[00].AsInteger := Carteira;
      QrLocAnt.Params[01].AsString  := DataA;
      UnDmkDAC_PF.AbreQuery(QrLocAnt, Dmod.MyDB);
      //
      A := Round(QrLocAntSaldoFim.Value * 100);
      B := Round(SaldoIni * 100);
      if A = B then
        Certos := Certos + 1
      else begin
        Geral.MB_Erro('Erro de saldo inicial na carteira: ' +
        FormatFloat('0', Carteira) +
        sLineBreak + 'A: ' + FormatFloat('#,###,###,##0.000000', A / 100) +
        sLineBreak + 'B: ' + FormatFloat('#,###,###,##0.000000', B / 100));
         PageControl1.ActivePageIndex := 1;
        Screen.Cursor := crDefault;
        Exit;
      end;
      //
      QrFim.Next;
    end;
    if Certos <> QrFim.RecordCount then
    begin
      Geral.MB_Erro('ERRO de confer�ncia de saldos iniciais!'+ sLineBreak+
      'Carteiras com saldos incorretos: ' +
      FormatFloat('0', QrFim.RecordCount - Certos) + sLineBreak +
      'AVISE A DERMATEK!');
      Screen.Cursor := crDefault;
      Exit;
    end;
    QrFim.First;
    while not QrFim.Eof do
    begin
      Carteira := QrFimCarteira.Value;
      Lctos    := QrFimLctos.Value;
      Creditos := QrFimCredito.Value;
      Debitos  := QrFimDebito.Value;
      SaldoFim := QrFimSaldo.Value;
      SaldoIni := SaldoFim - Creditos + Debitos;
      Data     := DataF;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'lctoencer', False, [
      'Lctos', 'SaldoIni', 'Creditos', 'Debitos', 'SaldoFim'], [
      'Data', 'Carteira'], [
      Lctos, SaldoIni, Creditos, Debitos, SaldoFim], [
      Data, Carteira], True);
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctA + ' SET Encerrado=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Carteira=:P1');
      Dmod.QrUpd.SQL.Add('AND Data BETWEEN :P2 AND :P3');
      Dmod.QrUpd.Params[00].AsString  := AnoMes;
      Dmod.QrUpd.Params[01].AsInteger := Carteira;
      Dmod.QrUpd.Params[02].AsString  := DataI;
      Dmod.QrUpd.Params[03].AsString  := DataF;
      Dmod.QrUpd.ExecSQL;
      //
      QrFim.Next;
    end;
    //
    Result := MigraLancamentos(FTabLctA, FTabLctB, DataF, smEncerra);
    //
    //if Result then Result := ComparacaoDeSaldos(); � feito depois!
    //
  finally
      Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctEncerraMes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctEncerraMes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  QrFim.Database  := DModG.MyPID_DB;
  QrSum2.Database := DModG.MyPID_DB;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmLctEncerraMes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctEncerraMes.GradeMesesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'TabLct') then
  begin
    if Lowercase(QrMesesTabLct.Value) = 'b' then
    begin
      Txt := clWindow;
      Bak := clBlue;
    end else
    if Lowercase(QrMesesTabLct.Value) = 'd' then
    begin
      Txt := clWindow;
      Bak := clRed;
    end else begin
      // em caso de erro
      Txt := clRed;
      Bak := clBlack;
    end;
    MyObjects.DesenhaTextoEmDBGrid(GradeMeses, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
end;

function TFmLctEncerraMes.MigraLancamentos(Origem, Destino, Data: String;
Sentido: TSentidoMigracao): Boolean;
var
  A1, A2, A3, B1, B2, B3, QtdReg1: Integer;
  Campos: String;
begin
  Result := False;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT COUNT(Controle) Itens FROM ' + Origem);
  Dmod.QrAux.SQL.Add('WHERE Controle <> 0');
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  A1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
  //
  if A1 = 0 then
  begin
    DModG.ReopenEndereco(FEntidade);
    //
    Geral.MB_Aviso('N�o h� lan�amentos a serem migrados para o cliente "' +
    DModG.QrEnderecoNOME_ENT.Value + '"!');
    Exit;
  end else begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('LOCK TABLES ' + Origem + ' WRITE, ' + Destino + ' WRITE, master WRITE');
    Dmod.QrUpd.ExecSQL;
    //
    try
      //  parei aqui! fazer s� no "a"!
      UFinanceiro.AtualizaPagamentosAVista(nil, Origem);
      //
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT COUNT(Controle) Itens FROM ' + Destino);
      Dmod.QrAux.SQL.Add('WHERE Controle <> 0');
      UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
      B1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
      //
      Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, Destino, '', QtdReg1);
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO ' + Destino);
      Dmod.QrUpd.SQL.Add('SELECT ' + Campos);
      Dmod.QrUpd.SQL.Add('FROM ' + Origem);
      Dmod.QrUpd.SQL.Add('WHERE Controle <> 0');
      case Sentido of
        smEncerra:
        begin
          Dmod.QrUpd.SQL.Add('AND Data <= :P0');
          Dmod.QrUpd.SQL.Add('AND Sit > 1');
        end;
        smAtiva:
        begin
          Dmod.QrUpd.SQL.Add('AND Data >= :P0');
          //Dmod.QrUpd.SQL.Add('AND Sit > 1');
        end;
        else begin
          Dmod.QrUpd.SQL.Add('AND Controle = 0');
          Geral.MB_Erro('ERRO. Sentido n�o definido para a migra��o de lan�amentos!');
          Screen.Cursor := crDefault;
          Exit;
        end;
      end;
      Dmod.QrUpd.Params[00].AsString := Data;  //2009/11/30
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT COUNT(Controle) Itens FROM ' + Destino);
      Dmod.QrAux.SQL.Add('WHERE Controle <> 0');
      UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
      B2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
      //
      B3 := B2 - B1;
      if B3 > 0 then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add(DELETE_FROM + Origem);
        Dmod.QrUpd.SQL.Add('WHERE Controle <> 0');
        case Sentido of
          smEncerra:
          begin
            Dmod.QrUpd.SQL.Add('AND Data <= :P0');
            Dmod.QrUpd.SQL.Add('AND Sit > 1');
          end;
          smAtiva:
          begin
            Dmod.QrUpd.SQL.Add('AND Data >= :P0');
            //Dmod.QrUpd.SQL.Add('AND Sit > 1');
          end;
          else begin
            Dmod.QrUpd.SQL.Add('AND Controle = 0');
            Geral.MB_Erro('ERRO. Sentido n�o definido para a migra��o de lan�amentos!');
            Screen.Cursor := crDefault;
            Exit;
          end;
        end;
        Dmod.QrUpd.Params[00].AsString := Data;
        Dmod.QrUpd.ExecSQL;
      end;
      //
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT COUNT(Controle) Itens FROM ' + Origem);
      Dmod.QrAux.SQL.Add('WHERE Controle <> 0');
      UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
      A2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
      //
      Result := True;
    finally
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UNLOCK TABLES');
      Dmod.QrUpd.ExecSQL;
    end;
    A3 := A1 - A2;
    if A3 <> B3 then
    begin
      Geral.MB_Erro('Ocorreu um erro na migra��o:' + sLineBreak +
      'Lan�amentos na tabela "' + Origem + '" antes da migra��o = ' + IntToStr(A1) + sLineBreak +
      'Lan�amentos na tabela "' + Destino + '" antes da migra��o = ' + IntToStr(B1) + sLineBreak +
      'Lan�amentos na tabela "' + Origem + '" depois da migra��o = ' + IntToStr(A2) + sLineBreak +
      'Lan�amentos na tabela "' + Destino + '" Depois da migra��o = ' + IntToStr(B2) + sLineBreak +
      'Diferen�a na migra��o: ' + IntToStr(B3 - A3) + ' lan�amentos!');
    end else
    begin
      Result := True;
      MyObjects.Informa(LaAviso2, False,
        'Foram migrados ' + IntToStr(B2) + ' lan�amentos!');
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctEncerraMes.Msatual1Click(Sender: TObject);
begin
  DesfazMesAtual();
end;

procedure TFmLctEncerraMes.odosmeses1Click(Sender: TObject);
var
  Continua: Boolean;
begin
  Continua := True;
  while Continua and (QrMeses.RecordCount > 0) do
  begin
    Continua := DesfazMesAtual();
    QrMeses.First;
  end;
end;

procedure TFmLctEncerraMes.PMEncerraPopup(Sender: TObject);
begin
  Desfazencerramento1.Enabled := QrMeses.RecNo = 1;
end;

procedure TFmLctEncerraMes.QrMesesAfterOpen(DataSet: TDataSet);
var
  //Ano, Mes, Dia: Word;
  Meses: Integer;
begin
  if QrMeses.RecordCount > 0 then
  begin
    Meses :=
      Geral.Periodo2000(Date) -
      Geral.Periodo2000(QrMesesData.Value) - 1;
    if Meses > 1 then
    begin
      CBMes.Items.Clear;
      CBAno.Items.Clear;
      MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -Meses);
    end;
  end;
end;

procedure TFmLctEncerraMes.QrMesesAfterScroll(DataSet: TDataSet);
begin
  ReopenLctoEncer(0);
  BtArquiva.Enabled := (QrMeses.RecNo > 12) and (Lowercase(QrMesesTabLct.Value) = 'b');
end;

procedure TFmLctEncerraMes.QrMesesBeforeClose(DataSet: TDataSet);
begin
  BtArquiva.Enabled := False;
  QrLctoEncer.Close;
end;

procedure TFmLctEncerraMes.ReopenLctoEncer(Carteira: Integer);
begin
  QrLctoEncer.Close;
  if EdEmpresa.ValueVariant > 0 then
  begin
    QrLctoEncer.Params[0].AsInteger := FEntidade;
    QrLctoEncer.Params[1].AsString  := Geral.FDT(QrMesesData.Value, 1);
    UnDmkDAC_PF.AbreQuery(QrLctoEncer, Dmod.MyDB);
    if Carteira <> 0 then
      QrLctoEncer.Locate('Carteira', Carteira, []);
  end;
end;

procedure TFmLctEncerraMes.ReopenMeses(Data: TDateTime);
begin
  QrMeses.Close;
  if EdEmpresa.ValueVariant > 0 then
  begin
    QrMeses.Params[0].AsInteger := FEntidade;
    UnDmkDAC_PF.AbreQuery(QrMeses, Dmod.MyDB);
    if Data > 2 then
      QrMeses.Locate('Data', Data, []);
  end;
end;

{ Parei aqui
  TODO:
  Verificar se todas carteiras est�o com saldo inicial zerado.
}
end.
