object FmLctEncerraMes: TFmLctEncerraMes
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-032 :: Encerramento Mensal Financeiro'
  ClientHeight = 705
  ClientWidth = 1184
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 1184
    Height = 506
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1184
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 15
        Top = 5
        Width = 87
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente interno:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 15
        Top = 25
        Width = 54
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInt64
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 69
        Top = 25
        Width = 1084
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object Panel5: TPanel
      Left = 479
      Top = 55
      Width = 705
      Height = 451
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object PnAvisos: TPanel
        Left = 0
        Top = 0
        Width = 705
        Height = 75
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 15
          Top = 10
          Width = 13
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LaAviso2: TLabel
          Left = 15
          Top = 30
          Width = 13
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object PB1: TProgressBar
          Left = 15
          Top = 49
          Width = 680
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 0
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 75
        Width = 705
        Height = 376
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 1
        object TabSheet1: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Saldos finais'
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 697
            Height = 345
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsLctoEncer
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Carteira'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CARTEIRA'
                Title.Caption = 'Descri'#231#227'o da carteira'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Lctos'
                Title.Caption = 'Lan'#231'tos'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SaldoIni'
                Title.Caption = 'Saldo inicial'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Creditos'
                Title.Caption = 'Cr'#233'ditos'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debitos'
                Title.Caption = 'D'#233'bitos'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SaldoFim'
                Title.Caption = 'Saldo final'
                Visible = True
              end>
          end
        end
        object TabSheet2: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Saldos finais anteriores (do encerramenrto recem realizado)'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid2: TDBGrid
            Left = 0
            Top = 0
            Width = 695
            Height = 422
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsFim
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet3: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Movimento (do encerramenrto recem realizado)'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid3: TDBGrid
            Left = 0
            Top = 0
            Width = 695
            Height = 422
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsMov
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 55
      Width = 479
      Height = 451
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
      object GradeMeses: TDBGrid
        Left = 0
        Top = 0
        Width = 479
        Height = 363
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsMeses
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = GradeMesesDrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'AM'
            Title.Caption = 'Ano/M'#234's'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SaldoIni'
            Title.Caption = 'Saldo inicial'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Creditos'
            Title.Caption = 'Cr'#233'ditos'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Debitos'
            Title.Caption = 'D'#233'bitos'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SaldoFim'
            Title.Caption = 'Saldo Final'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TabLct'
            Title.Caption = 'Tab'
            Visible = True
          end>
      end
      object Pn_: TPanel
        Left = 0
        Top = 363
        Width = 479
        Height = 88
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 1
        object GroupBox1: TGroupBox
          Left = 15
          Top = 5
          Width = 287
          Height = 75
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Per'#237'odo a ser encerrado: '
          TabOrder = 0
          object LaAno: TLabel
            Left = 192
            Top = 17
            Width = 27
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Ano:'
          end
          object LaMes: TLabel
            Left = 10
            Top = 17
            Width = 29
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'M'#234's:'
          end
          object CBMes: TComboBox
            Left = 10
            Top = 38
            Width = 178
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Color = clWhite
            DropDownCount = 12
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            Text = 'CBMes'
          end
          object CBAno: TComboBox
            Left = 192
            Top = 38
            Width = 85
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Color = clWhite
            DropDownCount = 3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Text = 'CBAno'
          end
        end
        object CkComparaSaldos: TCheckBox
          Left = 315
          Top = 44
          Width = 159
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Comparar saldos antes.'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1184
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1125
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1066
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 471
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Encerramento Mensal Financeiro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 471
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Encerramento Mensal Financeiro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 471
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Encerramento Mensal Financeiro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 565
    Width = 1184
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1180
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label3: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 619
    Width = 1184
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 18
      Width = 1180
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 1003
        Top = 0
        Width = 177
        Height = 66
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtEncerra: TBitBtn
        Tag = 10
        Left = 10
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Encerra'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtEncerraClick
      end
      object BtArquiva: TBitBtn
        Tag = 12
        Left = 162
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Arquiva'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtArquivaClick
      end
      object BitBtn1: TBitBtn
        Left = 354
        Top = 5
        Width = 159
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Compara saldos'
        TabOrder = 3
        OnClick = BitBtn1Click
      end
      object BitBtn2: TBitBtn
        Left = 522
        Top = 5
        Width = 257
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Atualiza saldo final B'
        TabOrder = 4
        OnClick = BitBtn2Click
      end
    end
  end
  object QrLast: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(lae.Data) Data '
      'FROM lctoencer lae'
      'LEFT JOIN carteiras car ON car.Codigo=lae.Carteira'
      'WHERE car.ForneceI=:P0'
      '')
    Left = 40
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastData: TDateField
      FieldName = 'Data'
      Required = True
    end
  end
  object QrPeriodos: TMySQLQuery
    Database = Dmod.MyDB
    Left = 68
    Top = 196
    object QrPeriodosAM: TWideStringField
      FieldName = 'AM'
      Size = 6
    end
    object QrPeriodosAno: TFloatField
      FieldName = 'Ano'
    end
    object QrPeriodosMes: TFloatField
      FieldName = 'Mes'
    end
    object QrPeriodosPeriodo: TFloatField
      FieldName = 'Periodo'
    end
  end
  object QrFeitos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT'
      'CONCAT(LPAD(MONTH(lae.Data), 2, "0"), YEAR(lae.Data)) AM'
      'FROM lctoencer lae'
      'LEFT JOIN carteiras car ON car.Codigo=lae.Carteira'
      'WHERE car.ForneceI=:P0'
      'ORDER BY AM DESC'
      '')
    Left = 96
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFeitosAM: TWideStringField
      FieldName = 'AM'
      Size = 6
    end
  end
  object QrFim: TMySQLQuery
    Database = Dmod.MyDB
    Left = 40
    Top = 232
    object QrFimCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrFimSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrFimCredito: TFloatField
      FieldKind = fkLookup
      FieldName = 'Credito'
      LookupDataSet = QrMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'Credito'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrFimDebito: TFloatField
      FieldKind = fkLookup
      FieldName = 'Debito'
      LookupDataSet = QrMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'Debito'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrFimLctos: TLargeintField
      FieldKind = fkLookup
      FieldName = 'Lctos'
      LookupDataSet = QrMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'lctos'
      KeyFields = 'Carteira'
      Lookup = True
    end
  end
  object QrMov: TMySQLQuery
    Database = Dmod.MyDB
    Left = 68
    Top = 232
    object QrMovCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrMovLctos: TLargeintField
      FieldName = 'Lctos'
      Required = True
    end
    object QrMovCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrMovDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrLocAnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SaldoFim'
      'FROM lctoencer'
      'WHERE Carteira=:P0'
      'AND Data=:P1'
      ' ')
    Left = 96
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocAntSaldoFim: TFloatField
      FieldName = 'SaldoFim'
    end
  end
  object QrLctoEncer: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Nome NO_CARTEIRA, lae.*'
      'FROM lctoencer lae'
      'LEFT JOIN carteiras car ON car.Codigo=lae.Carteira'
      'WHERE car.ForneceI=:P0'
      'AND lae.Data=:P1'
      'ORDER BY NO_CARTEIRA')
    Left = 248
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLctoEncerData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctoEncerCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctoEncerLctos: TIntegerField
      FieldName = 'Lctos'
      DisplayFormat = '0;-0; '
    end
    object QrLctoEncerSaldoIni: TFloatField
      FieldName = 'SaldoIni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctoEncerCreditos: TFloatField
      FieldName = 'Creditos'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctoEncerDebitos: TFloatField
      FieldName = 'Debitos'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctoEncerSaldoFim: TFloatField
      FieldName = 'SaldoFim'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctoEncerDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctoEncerDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctoEncerUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLctoEncerUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLctoEncerNO_CARTEIRA: TWideStringField
      FieldName = 'NO_CARTEIRA'
      Size = 100
    end
  end
  object DsLctoEncer: TDataSource
    DataSet = QrLctoEncer
    Left = 276
    Top = 232
  end
  object QrMeses: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMesesAfterOpen
    BeforeClose = QrMesesBeforeClose
    AfterScroll = QrMesesAfterScroll
    SQL.Strings = (
      'SELECT lae.Data, SUM(lae.SaldoIni) SaldoIni,'
      'SUM(lae.Creditos) Creditos, SUM(lae.Debitos) Debitos,'
      'SUM(lae.SaldoFim) SaldoFim, TabLct,'
      'CONCAT(YEAR(lae.Data), "/", LPAD(MONTH(lae.Data), 2, "0")) AM'
      'FROM lctoencer lae'
      'LEFT JOIN carteiras car ON car.Codigo=lae.Carteira'
      'WHERE car.ForneceI=:P0'
      'GROUP BY Data'
      'ORDER BY AM DESC')
    Left = 248
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMesesSaldoIni: TFloatField
      FieldName = 'SaldoIni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrMesesCreditos: TFloatField
      FieldName = 'Creditos'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrMesesDebitos: TFloatField
      FieldName = 'Debitos'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrMesesSaldoFim: TFloatField
      FieldName = 'SaldoFim'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrMesesAM: TWideStringField
      FieldName = 'AM'
      Size = 7
    end
    object QrMesesData: TDateField
      FieldName = 'Data'
    end
    object QrMesesTabLct: TWideStringField
      FieldName = 'TabLct'
      Size = 1
    end
  end
  object DsMeses: TDataSource
    DataSet = QrMeses
    Left = 276
    Top = 204
  end
  object QrIniCart: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Inicial) Inicial'
      'FROM carteiras'
      'WHERE ForneceI=:P0')
    Left = 168
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object PMEncerra: TPopupMenu
    OnPopup = PMEncerraPopup
    Left = 44
    Top = 512
    object Encerramsinformado1: TMenuItem
      Caption = '&Encerra m'#234's informado'
      OnClick = Encerramsinformado1Click
    end
    object Desfazencerramento1: TMenuItem
      Caption = '&Desfaz encerramento'
      object Msatual1: TMenuItem
        Caption = '&M'#234's atual'
        OnClick = Msatual1Click
      end
      object odosmeses1: TMenuItem
        Caption = '&Todos meses'
        OnClick = odosmeses1Click
      end
    end
  end
  object QrBAnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Carteira, Genero, Tipo,'
      'SUM(Credito) - SUM(Debito) Saldo'
      'FROM lct0001b'
      'WHERE Data <= :P0'
      'GROUP BY Carteira, Genero')
    Left = 244
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBAntCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrBAntGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrBAntSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrBAntTipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object QrSumCrt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Nome, lanb.Carteira, '
      'SUM(lanb.Credito - lanb.Debito) Saldo'
      'FROM lct0001b lanb'
      'LEFT JOIN carteiras car ON car.Codigo=lanb.Carteira'
      'WHERE Carteira IN'
      '('
      '  SELECT Codigo'
      '  FROM carteiras'
      '  WHERE ForneceI=:P0'
      '  AND Tipo <> 2'
      ')'
      'GROUP BY Carteira')
    Left = 12
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumCrtCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrSumCrtSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrSumCrtNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrCrts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM carteiras'
      'WHERE ForneceI=:P0')
    Left = 40
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCrtsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSum1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Saldo) Saldo'
      'FROM carteiras'
      'WHERE ForneceI=:P0'
      '')
    Left = 68
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSum1Saldo: TFloatField
      FieldName = 'Saldo'
    end
  end
  object QrSum2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _soma_cart_;'
      'CREATE TABLE _soma_cart_'
      'SELECT SUM(Credito-Debito) Saldo'
      'FROM exul2.lct0001a lct'
      'LEFT JOIN exul2.carteiras car ON car.Codigo=lct.Carteira'
      'WHERE car.Tipo <> 2'
      ''
      'UNION'
      ''
      'SELECT SUM(Credito-Debito) Saldo'
      'FROM exul2.lct0001b lct'
      'LEFT JOIN exul2.carteiras car ON car.Codigo=lct.Carteira'
      'WHERE car.Tipo <> 2'
      ''
      'UNION'
      ''
      'SELECT SUM(Credito-Debito) Saldo'
      'FROM exul2.lct0001d lct'
      'LEFT JOIN exul2.carteiras car ON car.Codigo=lct.Carteira'
      'WHERE car.Tipo <> 2;'
      ''
      'SELECT SUM(Saldo) Saldo '
      'FROM _soma_cart_;'
      ''
      'DROP TABLE IF EXISTS _soma_cart_;'
      '')
    Left = 96
    Top = 344
    object QrSum2Saldo: TFloatField
      FieldName = 'Saldo'
    end
  end
  object QrErrCrt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito-Debito) Valor'
      'FROM lct0028a lcta'
      'WHERE Carteira NOT IN (SELECT Codigo FROM Carteiras'
      'WHERE ForneceI=2625)'
      '')
    Left = 556
    Top = 276
    object QrErrCrtValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object DsFim: TDataSource
    DataSet = QrFim
    Left = 40
    Top = 260
  end
  object DsMov: TDataSource
    DataSet = QrMov
    Left = 68
    Top = 260
  end
end
