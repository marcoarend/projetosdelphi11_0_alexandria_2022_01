object FmCopiaDoc: TFmCopiaDoc
  Left = 339
  Top = 185
  Caption = 'LAN-COPIA-001 :: C'#243'pia de Lan'#231'amento'
  ClientHeight = 496
  ClientWidth = 841
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 841
    Height = 334
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGrid2: TDBGrid
      Left = 0
      Top = 45
      Width = 841
      Height = 289
      Align = alClient
      DataSource = DsCopiaCH2
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SerieCH'
          Title.Caption = 'S'#233'rie documento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Documento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Banco1'
          Title.Caption = 'Banco'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Agencia1'
          Title.Caption = 'Ag'#234'ncia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta1'
          Title.Caption = 'Conta corrente'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Tipo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TipoDoc'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_Copia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_Pgto'
          Visible = True
        end>
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 841
      Height = 45
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object CkDono: TCheckBox
        Left = 289
        Top = 15
        Width = 146
        Height = 17
        Caption = 'Imprime nome da empresa.'
        TabOrder = 0
      end
      object RGDataTipo: TRadioGroup
        Left = 114
        Top = 1
        Width = 170
        Height = 40
        Caption = 'Imprimir data:'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Emiss'#227'o'
          'Vencimento')
        TabOrder = 1
      end
      object RGItensPagina: TRadioGroup
        Left = 4
        Top = 1
        Width = 104
        Height = 40
        Caption = ' Itens por p'#225'gina: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '1'
          '2'
          '3')
        TabOrder = 2
      end
      object CkNaoAgrupar: TCheckBox
        Left = 441
        Top = 15
        Width = 146
        Height = 17
        Caption = 'N'#227'o agrupar lan'#231'amentos'
        TabOrder = 3
        OnClick = CkNaoAgruparClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 841
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 793
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 745
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 266
        Height = 32
        Caption = 'C'#243'pia de Lan'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 266
        Height = 32
        Caption = 'C'#243'pia de Lan'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 266
        Height = 32
        Caption = 'C'#243'pia de Lan'#231'amento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 382
    Width = 841
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 837
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 426
    Width = 841
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 695
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 693
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object QrCorrige: TMySQLQuery
    Database = Dmod.MyDB
    Left = 264
    Top = 164
    object QrCorrigeTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrCorrigeID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
  end
  object QrCopiaCH1: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCopiaCH1CalcFields
    SQL.Strings = (
      'SELECT COUNT(cc1.Debito) Itens, Banco1, Conta1, '
      'Agencia1, SerieCH, Data, Documento, NOMEBANCO, '
      'TipoDoc, Tipo, ID_Copia, SUM(Debito) DEBITO,'
      'NOMEEMPRESA, NotaFiscal, Duplicata, Genero,'
      'NOMEFORNECE, Controle, Descricao, lan.Protocolo, '
      'IF(lan.Protocolo <> 0, 1, 0) EANTem '
      'FROM copiach1 cc1'
      'GROUP BY Banco1, Conta1, '
      'Agencia1, SerieCH, Data, Documento, NOMEBANCO, '
      'TipoDoc, Tipo, ID_Copia')
    Left = 292
    Top = 164
    object QrCopiaCH1Itens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
    object QrCopiaCH1Banco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCopiaCH1Conta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCopiaCH1Agencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrCopiaCH1SerieCH: TWideStringField
      FieldName = 'SerieCH'
    end
    object QrCopiaCH1Data: TDateField
      FieldName = 'Data'
    end
    object QrCopiaCH1Documento: TFloatField
      FieldName = 'Documento'
    end
    object QrCopiaCH1NOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrCopiaCH1TipoDoc: TIntegerField
      FieldName = 'TipoDoc'
    end
    object QrCopiaCH1Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCopiaCH1ID_Copia: TIntegerField
      FieldName = 'ID_Copia'
    end
    object QrCopiaCH1DEBITO: TFloatField
      FieldName = 'DEBITO'
    end
    object QrCopiaCH1NOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrCopiaCH1NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrCopiaCH1Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 50
    end
    object QrCopiaCH1Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCopiaCH1NOMEFORNECE: TWideStringField
      FieldName = 'NOMEFORNECE'
      Size = 100
    end
    object QrCopiaCH1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCopiaCH1Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrCopiaCH1Protocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrCopiaCH1EAN128I: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EAN128I'
      Calculated = True
    end
    object QrCopiaCH1EANTem: TIntegerField
      FieldName = 'EANTem'
    end
  end
  object QrItens: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT NOMEFORNECE, Descricao, Controle '
      'FROM copiach1 cc1'
      'WHERE Banco1=:P0'
      'AND Conta1=:P1'
      'AND Agencia1=:P2'
      'AND SerieCH=:P3'
      'AND Data=:P4'
      'AND Documento=:P5'
      'AND NOMEBANCO=:P6'
      'AND TipoDoc=:P7'
      'AND Tipo=:P8'
      'AND ID_Copia=:P9')
    Left = 320
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P8'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P9'
        ParamType = ptUnknown
      end>
    object QrItensNOMEFORNECE: TWideStringField
      FieldName = 'NOMEFORNECE'
      Size = 100
    end
    object QrItensDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrItensControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrCopiaCH2: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCopiaCH2AfterOpen
    BeforeClose = QrCopiaCH2BeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM copiach2'
      'ORDER BY SerieCH, Documento')
    Left = 348
    Top = 164
    object QrCopiaCH2Banco1: TIntegerField
      FieldName = 'Banco1'
      Origin = 'copiach2.Banco1'
    end
    object QrCopiaCH2Conta1: TWideStringField
      FieldName = 'Conta1'
      Origin = 'copiach2.Conta1'
      Size = 15
    end
    object QrCopiaCH2Agencia1: TIntegerField
      FieldName = 'Agencia1'
      Origin = 'copiach2.Agencia1'
    end
    object QrCopiaCH2SerieCH: TWideStringField
      FieldName = 'SerieCH'
      Origin = 'copiach2.SerieCH'
    end
    object QrCopiaCH2Data: TDateField
      FieldName = 'Data'
      Origin = 'copiach2.Data'
    end
    object QrCopiaCH2Documento: TFloatField
      FieldName = 'Documento'
      Origin = 'copiach2.Documento'
    end
    object QrCopiaCH2Debito: TFloatField
      FieldName = 'Debito'
      Origin = 'copiach2.Debito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrCopiaCH2NOMEFORNECE: TWideMemoField
      FieldName = 'NOMEFORNECE'
      Origin = 'copiach2.NOMEFORNECE'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCopiaCH2Descricao: TWideMemoField
      FieldName = 'Descricao'
      Origin = 'copiach2.Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCopiaCH2Controles: TWideMemoField
      FieldName = 'Controles'
      Origin = 'copiach2.Controles'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCopiaCH2ID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Origin = 'copiach2.ID_Pgto'
    end
    object QrCopiaCH2NOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Origin = 'copiach2.NOMEBANCO'
      Size = 100
    end
    object QrCopiaCH2TipoDoc: TIntegerField
      FieldName = 'TipoDoc'
      Origin = 'copiach2.TipoDoc'
    end
    object QrCopiaCH2Tipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'copiach2.Tipo'
    end
    object QrCopiaCH2ID_Copia: TIntegerField
      FieldName = 'ID_Copia'
      Origin = 'copiach2.ID_Copia'
    end
    object QrCopiaCH2QtdDocs: TIntegerField
      FieldName = 'QtdDocs'
      Origin = 'copiach2.QtdDocs'
    end
    object QrCopiaCH2NOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrCopiaCH2NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrCopiaCH2Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 50
    end
    object QrCopiaCH2Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCopiaCH2Protocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrCopiaCH2EANTem: TIntegerField
      FieldName = 'EANTem'
    end
    object QrCopiaCH2EAN128I: TWideStringField
      DisplayWidth = 30
      FieldName = 'EAN128I'
      Size = 30
    end
  end
  object DsCopiaCH2: TDataSource
    DataSet = QrCopiaCH2
    Left = 376
    Top = 164
  end
  object frxCopiaDOC: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39094.004039166700000000
    ReportOptions.LastChange = 39094.004039166700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MeContratoOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  MeContrato.Visible := <VARF_EHDEBITO>;'
      '  //        '
      '  if <VARF_TEM_CODBARRA> then'
      '  begin'
      
        '    BarCodeI.Visible := True;                                   ' +
        '             '
      '  end else begin'
      
        '    BarCodeI.Visible := False;                                  ' +
        '              '
      '  end;            '
      'end;'
      ''
      'begin'
      '  Memo20.Visible     := <VAR_IMPDONO>;    '
      
        '  MasterData1.Height := (13.80 / 2.539 * 96) * (2 / <VARF_ITENS>' +
        ');                                                              ' +
        '              '
      'end.')
    OnGetValue = frxCopiaDOCGetValue
    Left = 404
    Top = 164
    Datasets = <
      item
        DataSet = frxDsCopiaDOC
        DataSetName = 'frxDsCopiaDOC'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 521.574803150000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        DataSet = frxDsCopiaDOC
        DataSetName = 'frxDsCopiaDOC'
        RowCount = 0
        object Memo24: TfrxMemoView
          Left = 41.574830000000000000
          Top = 445.984540000000000000
          Width = 264.567100000000000000
          Height = 75.590600000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Left = 196.535560000000000000
          Top = 64.252010000000000000
          Width = 521.575140000000000000
          Height = 279.685220000000000000
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Left = 215.433210000000000000
          Top = 68.031540000000010000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Banco')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 260.787570000000000000
          Top = 68.031540000000010000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag'#234'ncia')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 332.598640000000000000
          Top = 68.031540000000010000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta corrente')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 457.323130000000000000
          Top = 68.031540000000010000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 514.016080000000000000
          Top = 68.031540000000010000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[TIT_DOC]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 200.315090000000000000
          Top = 90.708720000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Banco1"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 245.669450000000000000
          Top = 90.708720000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Agencia1"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 321.260050000000000000
          Top = 90.708720000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Conta1"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 449.764070000000000000
          Top = 90.708720000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."SerieCH"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 514.016080000000000000
          Top = 90.708720000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'000000;-000000;------'#39', <frxDsCopiaDOC."Documento"' +
              '>)]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 597.165740000000000000
          Top = 68.031540000000010000
          Height = 41.574830000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Memo11: TfrxMemoView
          Left = 600.945270000000000000
          Top = 68.031540000000010000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'R$')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 600.945270000000000000
          Top = 86.929190000000000000
          Width = 113.385900000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '$[frxDsCopiaDOC."Debito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 200.315090000000000000
          Top = 113.385900000000000000
          Width = 514.016080000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[TXT_EXTENSO][VARF_EXTENSO]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 200.315090000000000000
          Top = 151.181200000000000000
          Width = 514.016080000000000000
          Height = 60.472455590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[TXT_PARA][frxDsCopiaDOC."NOMEFORNECE"][TXT_OUPARA]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 200.315090000000000000
          Top = 245.669450000000000000
          Width = 514.016080000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."CIDADE"], [frxDsCopiaDOC."Data"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo16: TfrxMemoView
          Left = 200.315090000000000000
          Top = 211.653680000000000000
          Width = 514.016080000000000000
          Height = 30.236240000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCopiaDOC."NOMEBANCO"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 200.315090000000000000
          Top = 306.141930000000000000
          Width = 514.016080000000000000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."NOMEEMPRESA"]')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          Left = 41.574830000000000000
          Top = 351.496290000000000000
          Width = 676.535870000000000000
          Height = 170.078850000000000000
          Frame.Width = 0.500000000000000000
        end
        object Shape3: TfrxShapeView
          Left = 487.559370000000000000
          Top = 351.496290000000000000
          Width = 230.551330000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape4: TfrxShapeView
          Left = 253.228510000000000000
          Top = 351.496290000000000000
          Width = 234.330860000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape5: TfrxShapeView
          Left = 41.574830000000000000
          Top = 351.496290000000000000
          Width = 211.653680000000000000
          Height = 64.252010000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo18: TfrxMemoView
          Left = 257.008040000000000000
          Top = 381.732530000000000000
          Width = 457.323130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Do banco: [frxDsCopiaDOC."NOMEBANCO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 257.008040000000000000
          Top = 400.630180000000000000
          Width = 457.323130000000000000
          Height = 41.574805590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Utilizado para: [frxDsCopiaDOC."Descricao"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 41.574830000000000000
          Top = 445.984540000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCopiaDOC
          DataSetName = 'frxDsCopiaDOC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 306.141930000000000000
          Top = 445.984540000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  CAIXA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 306.141930000000000000
          Top = 464.882190000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  C/C')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 306.141930000000000000
          Top = 483.779840000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  TAL'#195'O')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 306.141930000000000000
          Top = 502.677490000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 396.850650000000000000
          Top = 445.984540000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 396.850650000000000000
          Top = 464.882190000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 396.850650000000000000
          Top = 502.677490000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 487.559370000000000000
          Top = 445.984540000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TIT_RESPONSABILIDADE]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 487.559370000000000000
          Top = 464.882190000000000000
          Width = 230.551330000000000000
          Height = 56.692950000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 45.354360000000000000
          Top = 355.275820000000000000
          Width = 204.094620000000000000
          Height = 56.692950000000010000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'LAN'#199'AMENTOS: [frxDsCopiaDOC."Controles"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 257.008040000000000000
          Top = 355.275820000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VISADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox1: TfrxCheckBoxView
          Left = 336.378170000000000000
          Top = 355.275820000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_SIM'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo38: TfrxMemoView
          Left = 359.055350000000000000
          Top = 355.275820000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox2: TfrxCheckBoxView
          Left = 411.968770000000000000
          Top = 355.275820000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_NAO'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 434.645950000000000000
          Top = 355.275820000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 491.338900000000000000
          Top = 355.275820000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CRUZADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox3: TfrxCheckBoxView
          Left = 570.709030000000000000
          Top = 355.275820000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_SIM'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo41: TfrxMemoView
          Left = 593.386210000000000000
          Top = 355.275820000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox4: TfrxCheckBoxView
          Left = 646.299630000000000000
          Top = 355.275820000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_NAO'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo42: TfrxMemoView
          Left = 668.976810000000000000
          Top = 355.275820000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Top = 18.897650000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TIT_COPIA]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 200.315090000000000000
          Top = 37.795300000000000000
          Width = 517.795610000000000000
          Height = 26.456685590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Utilizado para: [frxDsCopiaDOC."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Shape8: TfrxShapeView
          Left = 41.574830000000000000
          Top = 415.748300000000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape9: TfrxShapeView
          Left = 41.574830000000000000
          Top = 430.866420000000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo22: TfrxMemoView
          Left = 49.133890000000000000
          Top = 415.748300000000000000
          Width = 200.315090000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N.F. N'#186' [frxDsCopiaDOC."NotaFiscal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 49.133890000000000000
          Top = 430.866420000000000000
          Width = 200.315090000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DUPLICATA N'#186' [frxDsCopiaDOC."Duplicata"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeContrato: TfrxMemoView
          Left = 253.228510000000000000
          Top = 351.496290000000000000
          Width = 464.882190000000000000
          Height = 26.456692910000000000
          OnBeforePrint = 'MeContratoOnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWindow
          Memo.UTF8W = (
            'CONTRATO: [VARF_CONTRATO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape7: TfrxShapeView
          Top = 22.677180000000000000
          Width = 192.755920160000000000
          Height = 321.259854720000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo21: TfrxMemoView
          Left = 41.574830000000000000
          Top = 30.236240000000000000
          Width = 143.622140000000000000
          Height = 287.244280000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'C A N H O T O')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object BarCodeI: TfrxBarCodeView
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 23.039270000000000000
          Height = 178.000000000000000000
          Visible = False
          BarType = bcCodeEAN128B
          DataField = 'EAN128I'
          DataSet = frxDsCopiaDOC
          DataSetName = 'frxDsCopiaDOC'
          Rotation = 90
          ShowText = False
          TestLine = False
          Text = 'L00012345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object ShNoCoBarra: TfrxShapeView
          Top = 351.496290000000000000
          Width = 41.574830000000000000
          Height = 170.078850000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo25: TfrxMemoView
          Left = 15.118120000000000000
          Top = 355.275820000000000000
          Width = 22.677180000000000000
          Height = 162.519790000000000000
          DataField = 'Protocolo'
          DataSet = frxDsCopiaDOC
          DataSetName = 'frxDsCopiaDOC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Protocolo"]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Top = 355.275820000000000000
          Width = 15.118120000000000000
          Height = 162.519790000000000000
          DataSet = FmFinanceiroImp.frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo:')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
    end
  end
  object frxCheckBoxObject1: TfrxCheckBoxObject
    Left = 376
    Top = 192
  end
  object frxRichObject1: TfrxRichObject
    Left = 404
    Top = 192
  end
  object frxDsCopiaDOC: TfrxDBDataset
    UserName = 'frxDsCopiaDOC'
    CloseDataSource = False
    DataSet = QrCopiaCH2
    BCDToCurrency = False
    Left = 348
    Top = 192
  end
  object QrContasEnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM contasent'
      'WHERE Codigo=:P0'
      'AND Entidade=:P1')
    Left = 264
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrContasEntCntrDebCta: TWideStringField
      FieldName = 'CntrDebCta'
      Size = 50
    end
  end
  object frxCopiaDOC2: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39094.004039166700000000
    ReportOptions.LastChange = 41048.714796828700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MeContratoOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  MeContrato.Visible := <VARF_EHDEBITO>;'
      '  //        '
      '  if <VARF_TEM_CODBARRA> then'
      '  begin'
      
        '    BarCodeI.Visible := True;                                   ' +
        '             '
      '  end else begin'
      
        '    BarCodeI.Visible := False;                                  ' +
        '              '
      '  end;            '
      'end;'
      ''
      'begin'
      '  Memo20.Visible := <VAR_IMPDONO>;    '
      
        '  //MasterData1.Height := (13.80 / 2.539 * 96) * (2 / <VARF_ITEN' +
        'S>);                                                            ' +
        '                '
      'end.')
    OnGetValue = frxCopiaDOCGetValue
    Left = 432
    Top = 164
    Datasets = <
      item
        DataSet = frxDsCopiaDOC
        DataSetName = 'frxDsCopiaDOC'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 374.173228350000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        DataSet = frxDsCopiaDOC
        DataSetName = 'frxDsCopiaDOC'
        RowCount = 0
        object Line2: TfrxLineView
          Top = 374.173228350000000000
          Width = 793.700787400000000000
          Color = clBlack
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftTop]
        end
        object Shape1: TfrxShapeView
          Left = 79.370105590000000000
          Top = 72.015770000000000000
          Width = 680.315400000000000000
          Height = 132.283550000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Left = 90.913420000000000000
          Top = 75.795300000000000000
          Width = 41.574830000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Banco')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 147.606370000000000000
          Top = 75.795300000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag'#234'ncia')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 238.315090000000000000
          Top = 75.795300000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta corrente')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 378.157700000000000000
          Top = 75.795300000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 453.748300000000000000
          Top = 75.795300000000000000
          Width = 79.370130000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TIT_DOC]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 90.913420000000000000
          Top = 90.913420000000000000
          Width = 41.574830000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Banco1"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 147.606370000000000000
          Top = 90.913420000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Agencia1"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 238.315090000000000000
          Top = 90.913420000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Conta1"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 378.157700000000000000
          Top = 90.913420000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."SerieCH"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 453.748300000000000000
          Top = 90.913420000000000000
          Width = 79.370130000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              '[FormatFloat('#39'000000;-000000;------'#39', <frxDsCopiaDOC."Documento"' +
              '>)]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 620.047620000000000000
          Top = 75.795300000000000000
          Height = 30.236240000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Memo11: TfrxMemoView
          Left = 623.827150000000000000
          Top = 75.795300000000000000
          Width = 120.944960000000000000
          Height = 11.338580240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'R$')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 623.827150000000000000
          Top = 87.354360000000000000
          Width = 120.944960000000000000
          Height = 18.897640240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '$[frxDsCopiaDOC."Debito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 90.708683390000000000
          Top = 105.811070000000000000
          Width = 653.858250630000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[TXT_EXTENSO][VARF_EXTENSO]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 90.708683390000000000
          Top = 121.149660000000000000
          Width = 653.858250630000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[TXT_PARA][frxDsCopiaDOC."NOMEFORNECE"][TXT_OUPARA]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 90.708683390000000000
          Top = 150.629921260000000000
          Width = 461.102220630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."CIDADE"], [frxDsCopiaDOC."Data"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo16: TfrxMemoView
          Left = 90.708683390000000000
          Top = 136.645669290000000000
          Width = 653.858250630000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCopiaDOC."NOMEBANCO"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 154.960644570000000000
          Top = 189.181200000000000000
          Width = 529.133858270000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."NOMEEMPRESA"]')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          Left = 120.944908740000000000
          Top = 211.653543310000000000
          Width = 638.740133070000000000
          Height = 120.944881890000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape3: TfrxShapeView
          Left = 525.354330708661400000
          Top = 211.653543310000000000
          Width = 230.551330000000000000
          Height = 15.118110240000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape4: TfrxShapeView
          Left = 294.803152050000000000
          Top = 211.653543310000000000
          Width = 230.551178660000000000
          Height = 15.118110240000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape5: TfrxShapeView
          Left = 120.944908740000000000
          Top = 211.653543310000000000
          Width = 173.858380000000000000
          Height = 30.236230240000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo26: TfrxMemoView
          Left = 347.716537870000000000
          Top = 272.125984250000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  CAIXA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 347.716537870000000000
          Top = 287.244094490000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  C/C')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 347.716537870000000000
          Top = 302.362204720000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  TAL'#195'O')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 347.716537870000000000
          Top = 317.480314960000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 438.425199290000000000
          Top = 272.125984250000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 438.425199290000000000
          Top = 287.244094490000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 438.425199290000000000
          Top = 302.362204720000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 529.133860710000000000
          Top = 272.125984250000000000
          Width = 230.551181100000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TIT_RESPONSABILIDADE]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 124.929190000000000000
          Top = 211.653543310000000000
          Width = 166.299320000000000000
          Height = 30.236220470000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'LAN'#199'AMENTOS: [frxDsCopiaDOC."Controles"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 294.803149610000000000
          Top = 211.653543307086600000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VISADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox1: TfrxCheckBoxView
          Left = 374.378170000000000000
          Top = 211.653543307086600000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_SIM'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo38: TfrxMemoView
          Left = 397.055350000000000000
          Top = 211.653543307086600000
          Width = 26.456710000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox2: TfrxCheckBoxView
          Left = 449.968770000000000000
          Top = 211.653543307086600000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_NAO'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 472.645950000000000000
          Top = 211.653543307086600000
          Width = 26.456710000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 533.118430000000000000
          Top = 211.653543307086600000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CRUZADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox3: TfrxCheckBoxView
          Left = 601.149970000000000000
          Top = 211.653543307086600000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_SIM'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo41: TfrxMemoView
          Left = 631.386210000000000000
          Top = 211.653543307086600000
          Width = 26.456710000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox4: TfrxCheckBoxView
          Left = 684.299630000000000000
          Top = 211.653543307086600000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_NAO'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo42: TfrxMemoView
          Left = 706.976810000000000000
          Top = 211.653543307086600000
          Width = 26.456710000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 38.000000000000000000
          Top = 37.795275590551180000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TIT_COPIA]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 38.000000000000000000
          Top = 56.692913390000000000
          Width = 718.110700000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Utilizado para: [frxDsCopiaDOC."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Shape8: TfrxShapeView
          Left = 120.944908740000000000
          Top = 241.889763780000000000
          Width = 173.858380000000000000
          Height = 15.118110240000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape9: TfrxShapeView
          Left = 120.944908740000000000
          Top = 257.007874020000000000
          Width = 173.858380000000000000
          Height = 15.118110240000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo22: TfrxMemoView
          Left = 124.929190000000000000
          Top = 241.889763780000000000
          Width = 166.299320000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N.F. N'#186' [frxDsCopiaDOC."NotaFiscal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 124.929190000000000000
          Top = 257.007874020000000000
          Width = 166.299320000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DUPLICATA N'#186' [frxDsCopiaDOC."Duplicata"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeContrato: TfrxMemoView
          Left = 294.803152050000000000
          Top = 211.653543310000000000
          Width = 464.882190000000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'MeContratoOnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWindow
          Memo.UTF8W = (
            'CONTRATO: [VARF_CONTRATO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 438.425480000000000000
          Top = 317.480314960000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 294.803340000000000000
          Top = 226.771800000000000000
          Width = 464.882190000000000000
          Height = 45.354350240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 298.582870000000000000
          Top = 226.771800000000000000
          Width = 457.323130000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Do banco: [frxDsCopiaDOC."NOMEBANCO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 298.582679610000000000
          Top = 241.889910240000000000
          Width = 457.323130000000000000
          Height = 30.236220470000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Utilizado para: [frxDsCopiaDOC."Descricao"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 120.944960000000000000
          Top = 272.126160000000000000
          Width = 226.771800000000000000
          Height = 60.472480000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Shape7: TfrxShapeView
          Left = 37.795300000000000000
          Top = 71.811070000000000000
          Width = 37.795190160000000000
          Height = 260.787374720000000000
          Frame.Width = 0.100000000000000000
        end
        object BarCodeI: TfrxBarCodeView
          Left = 46.110236220000000000
          Top = 86.929190000000000000
          Width = 23.039270000000000000
          Height = 178.000000000000000000
          Visible = False
          BarType = bcCodeEAN128B
          DataField = 'EAN128I'
          DataSet = frxDsCopiaDOC
          DataSetName = 'frxDsCopiaDOC'
          Rotation = 90
          ShowText = False
          TestLine = False
          Text = 'L00012345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object ShNoCoBarra: TfrxShapeView
          Left = 79.370130000000000000
          Top = 211.653680000000000000
          Width = 41.574830000000000000
          Height = 120.944960000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo35: TfrxMemoView
          Left = 94.488250000000000000
          Top = 215.433210000000000000
          Width = 22.677180000000000000
          Height = 109.606370000000000000
          DataField = 'Protocolo'
          DataSet = frxDsCopiaDOC
          DataSetName = 'frxDsCopiaDOC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Protocolo"]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 79.370130000000000000
          Top = 215.433210000000000000
          Width = 15.118120000000000000
          Height = 109.606370000000000000
          DataSet = FmFinanceiroImp.frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo:')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 120.944960000000000000
          Top = 272.126160000000000000
          Width = 226.771800000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
        end
      end
    end
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 432
    Top = 192
  end
end
