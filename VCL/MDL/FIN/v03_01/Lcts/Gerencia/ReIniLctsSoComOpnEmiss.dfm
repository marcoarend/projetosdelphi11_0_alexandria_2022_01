object FmReIniLctsSoComOpnEmiss: TFmReIniLctsSoComOpnEmiss
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ????????????? ??????????? ????????????'
  ClientHeight = 427
  ClientWidth = 715
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 715
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 667
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 619
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 603
        Height = 32
        Caption = 'Reinicio do Financeiro S'#243' Com Emiss'#245'es Abertas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 603
        Height = 32
        Caption = 'Reinicio do Financeiro S'#243' Com Emiss'#245'es Abertas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 603
        Height = 32
        Caption = 'Reinicio do Financeiro S'#243' Com Emiss'#245'es Abertas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 715
    Height = 256
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 715
      Height = 256
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 715
        Height = 256
        Align = alClient
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 2
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object LaEmissIni: TLabel
          Left = 524
          Top = 22
          Width = 58
          Height = 13
          Caption = 'Novo in'#237'cio:'
        end
        object EdEmpresa: TdmkEditCB
          Left = 8
          Top = 18
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 64
          Top = 18
          Width = 453
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object TPEmissIni: TdmkEditDateTimePicker
          Left = 588
          Top = 18
          Width = 112
          Height = 21
          Time = 0.516375590297684500
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 304
    Width = 715
    Height = 53
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 711
      Height = 19
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object PB1: TProgressBar
      Left = 2
      Top = 34
      Width = 711
      Height = 17
      Align = alBottom
      TabOrder = 1
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 357
    Width = 715
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 569
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 567
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtCopia: TBitBtn
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&1. Copia Lcts'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtCopiaClick
      end
      object BtExec: TBitBtn
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&2. Recria'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtExecClick
      end
    end
  end
  object QrLcts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _fluxcxadia_ '
      'ORDER BY Data, Controle ')
    Left = 80
    Top = 152
    object QrLctsData: TDateField
      FieldName = 'Data'
    end
    object QrLctsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrLctsCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctsSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLctsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctsSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrLctsNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLctsDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLctsCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctsCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLctsSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLctsDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLctsSit: TSmallintField
      FieldName = 'Sit'
    end
    object QrLctsVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLctsPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctsMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctsFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLctsCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctsForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLctsDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrLctsDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctsCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrLctsID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrLctsValAPag: TFloatField
      FieldName = 'ValAPag'
    end
    object QrLctsValARec: TFloatField
      FieldName = 'ValARec'
    end
    object QrLctsVTransf: TFloatField
      FieldName = 'VTransf'
    end
    object QrLctsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLctsPagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrLctsSeqPag: TIntegerField
      FieldName = 'SeqPag'
    end
  end
  object QrPago: TMySQLQuery
    Database = Dmod.MyDB
    Left = 196
    Top = 152
    object QrPagoValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrTots: TMySQLQuery
    Database = Dmod.MyDB
    Left = 292
    Top = 164
  end
  object QrNew: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _fluxcxaopn_ '
      'WHERE Data < "2014-01-01"')
    Left = 428
    Top = 184
    object QrNewData: TDateField
      FieldName = 'Data'
    end
    object QrNewTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrNewPagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrNewCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrNewControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNewSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrNewSeqPag: TIntegerField
      FieldName = 'SeqPag'
    end
    object QrNewGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrNewDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrNewSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrNewNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrNewDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrNewCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrNewCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrNewSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrNewDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrNewSit: TSmallintField
      FieldName = 'Sit'
    end
    object QrNewVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrNewPago: TFloatField
      FieldName = 'Pago'
    end
    object QrNewMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrNewFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrNewCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrNewCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrNewForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrNewDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrNewDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrNewDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrNewCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrNewID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrNewValAPag: TFloatField
      FieldName = 'ValAPag'
    end
    object QrNewValARec: TFloatField
      FieldName = 'ValARec'
    end
    object QrNewVTransf: TFloatField
      FieldName = 'VTransf'
    end
    object QrNewValPgCre: TFloatField
      FieldName = 'ValPgCre'
    end
    object QrNewValPgDeb: TFloatField
      FieldName = 'ValPgDeb'
    end
    object QrNewValPgTrf: TFloatField
      FieldName = 'ValPgTrf'
    end
    object QrNewAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
end
