unit SomaDinheiroCH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, UnGOTOy, StdCtrls, Buttons, UnInternalConsts,
  ComCtrls, dmkEdit, dmkEditDateTimePicker, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmSomaDinheiroCH = class(TForm)
    PainelDados: TPanel;
    EdDescricao: TdmkEdit;
    Label4: TLabel;
    Label3: TLabel;
    EdValor: TdmkEdit;
    Label1: TLabel;
    EdControle: TdmkEdit;
    TPVencimento: TdmkEditDateTimePicker;
    Label2: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    BtConfirma: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmSomaDinheiroCH: TFmSomaDinheiroCH;

implementation

uses UnMyObjects, Module, SomaDinheiro, UMySQLModule;

{$R *.DFM}

procedure TFmSomaDinheiroCH.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdValor.SetFocus;
end;

procedure TFmSomaDinheiroCH.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSomaDinheiroCH.BtConfirmaClick(Sender: TObject);
var
  Controle: Integer;
begin
  Dmod.QrUpdM.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'SomaIts', 'SomaIts', 'Controle');
    Dmod.QrUpdM.SQL.Add('INSERT INTO somaits SET ');
  end else begin
    Controle := EdControle.ValueVariant;
    Dmod.QrUpdM.SQL.Add('UPDATE somaits SET ');
  end;
  Dmod.QrUpdM.SQL.Add('Valor=:P0, Descricao=:P1, Vencimento=:P2 ');
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdM.SQL.Add(', Caixa=:Pa, Controle=:Pb')
  else
    Dmod.QrUpdM.SQL.Add('WHERE Caixa=:Pa AND Controle=:Pb');
  //
  Dmod.QrUpdM.Params[00].AsFloat  := EdValor.ValueVariant;
  Dmod.QrUpdM.Params[01].AsString := EdDescricao.Text;
  Dmod.QrUpdM.Params[02].AsString := Geral.FDT(TPVencimento.Date, 1);
  //
  Dmod.QrUpdM.Params[03].AsInteger := VAR_QRCARTEIRAS.FieldByName('Codigo').AsInteger;
  Dmod.QrUpdM.Params[04].AsInteger := Controle;
  Dmod.QrUpdM.ExecSQL;
  Close;
  FmSomaDinheiro.ReopenSomaIts(Controle);
end;

procedure TFmSomaDinheiroCH.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSomaDinheiroCH.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPVencimento.Date := Date;
end;

end.
