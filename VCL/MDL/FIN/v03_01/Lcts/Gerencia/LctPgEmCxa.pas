unit LctPgEmCxa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, DBCtrls, Grids,
  DBGrids, ComCtrls, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkEditDateTimePicker, dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmLctPgEmCxa = class(TForm)
    PainelDados: TPanel;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrCarteirasTipo: TIntegerField;
    Label3: TLabel;
    Label2: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdMulta: TdmkEdit;
    EdTaxaM: TdmkEdit;
    Label4: TLabel;
    DBGrid1: TDBGrid;
    Label5: TLabel;
    TbLctoEdit: TmySQLTable;
    DsLctoEdit: TDataSource;
    TbLctoEditControle: TIntegerField;
    TbLctoEditSub: TIntegerField;
    TbLctoEditDescricao: TWideStringField;
    TbLctoEditData: TDateField;
    TbLctoEditVencimento: TDateField;
    TbLctoEditMultaVal: TFloatField;
    TbLctoEditJurosVal: TFloatField;
    TbLctoEditValorOri: TFloatField;
    TbLctoEditValorPgt: TFloatField;
    QrCarteirasForneceI: TIntegerField;
    QrContas1: TmySQLQuery;
    QrContas1Codigo: TIntegerField;
    QrContas1Nome: TWideStringField;
    DsContas1: TDataSource;
    QrContas2: TmySQLQuery;
    QrContas2Codigo: TIntegerField;
    QrContas2Nome: TWideStringField;
    DsContas2: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtCancela: TBitBtn;
    BtOK: TBitBtn;
    procedure BtOKClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdMultaExit(Sender: TObject);
    procedure EdTaxaMExit(Sender: TObject);
    procedure TPDataExit(Sender: TObject);
    procedure TbLctoEditBeforePost(DataSet: TDataSet);
    procedure TbLctoEditAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    FCriando: Boolean;
    procedure CalculaMultaEJuros;
  public
    { Public declarations }
    FQrCrt, FQrLct: TmySQLQuery;
    FCalcular: Boolean;
    FTabLctA, FTmpLctoEdit: String
  end;

  var
  FmLctPgEmCxa: TFmLctPgEmCxa;

implementation

{$R *.DFM}

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, ModuleFin, UnInternalConsts,
UnFinanceiro, DmkDAC_PF;

procedure TFmLctPgEmCxa.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctPgEmCxa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if UFinanceiro.TabLctNaoDefinida(FTabLctA, 'TFmLctPgEmCxa.FormActivate()') then
    Close;
  if FCriando then
  begin
    FCriando := False;
    QrCarteiras.Close;
    QrCarteiras.SQL.Clear;
    QrCarteiras.SQL.Add('SELECT car.*');
    QrCarteiras.SQL.Add('FROM carteiras car');
    QrCarteiras.SQL.Add('WHERE car.Tipo=0');
    QrCarteiras.SQL.Add('AND car.Codigo>0');
    QrCarteiras.SQL.Add('AND car.ForneceI IN (' + VAR_LIB_EMPRESAS + ')');
    QrCarteiras.SQL.Add('ORDER BY Nome');
    UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
    UnDmkDAC_PF.AbreTable(TbLctoEdit, DModG.MyPID_DB);
  end;
  FCalcular := True;
  CalculaMultaEJuros;
end;

procedure TFmLctPgEmCxa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctPgEmCxa.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TbLctoEdit.DataBase := DModG.MyPID_DB;
  FCriando    := True;
  TPData.Date := Date;
  FCalcular   := False;
end;

procedure TFmLctPgEmCxa.EdMultaExit(Sender: TObject);
begin
  CalculaMultaEJuros;
end;

procedure TFmLctPgEmCxa.EdTaxaMExit(Sender: TObject);
begin
  CalculaMultaEJuros;
end;

procedure TFmLctPgEmCxa.TPDataExit(Sender: TObject);
begin
  CalculaMultaEJuros;
end;

procedure TFmLctPgEmCxa.CalculaMultaEJuros;
var
  Venct, Pagto: TDateTime;
  MultaV, MultaT, TaxaM, JurosV, JurosT: Double;
  Dias, Controle: Integer;
begin
  if FCalcular and (TbLctoEdit.State <> dsInactive) then
  begin
    MultaT := Geral.DMV(EdMulta.Text);
    TaxaM  := Geral.DMV(EdTaxaM.Text);
    Pagto  := Trunc(TPData.Date);
    //Venct  := TbLctoEditVencimento.Value;
    Venct  := UMyMod.CalculaDataDeposito(TbLctoEditVencimento.Value);
    TbLctoEdit.DisableControls;
    Controle := TbLctoEditControle.Value;
    TbLctoEdit.First;
    while not TbLctoEdit.Eof do
    begin
      JurosV := 0;
      MultaV := 0;
      if Venct < Pagto then
        Dias := UMyMod.DiasUteis(Venct + 1, Pagto) else
        Dias := 0;
      if Dias > 0 then
      begin
        JurosT := dmkPF.CalculaJuroComposto(TaxaM, Pagto - Venct);
        JurosV := Round(JurosT * TbLctoEditValorOri.Value) / 100;
        MultaV := Round(MultaT * TbLctoEditValorOri.Value) / 100;
      end;
      //
      TbLctoEdit.Edit;
      TbLctoEditData.Value     := Pagto;
      TbLctoEditJurosVal.Value := JurosV;
      TbLctoEditMultaVal.Value := MultaV;
      TbLctoEditValorPgt.Value := TbLctoEditValorOri.Value + JurosV + MultaV;
      TbLctoEdit.Post;
      //
      TbLctoEdit.Next;
    end;
    TbLctoEdit.Locate('Controle', Controle, []);
  end;
  TbLctoEdit.EnableControls;
end;

procedure TFmLctPgEmCxa.TbLctoEditBeforePost(DataSet: TDataSet);
  function GetNomeCampo: String;
  begin
    Result := DBGrid1.Columns[THackDBGrid(DBGrid1).Col-1].FieldName;
  end;
var
  MultaV, JurosV, TotalV, TotalI: Double;
  Campo: String;
begin
  MultaV := TbLctoEditMultaVal.Value;
  JurosV := TbLctoEditJurosVal.Value;
  TotalV := TbLctoEditValorPgt.Value;
  TotalI := TbLctoEditValorOri.Value;
  if TotalV <> MultaV + JurosV then
  begin
    Campo := GetNomeCampo;
    if Campo = 'ValorPgt' then
    begin
      if TotalV < TotalI then
      begin
        MultaV := 0;
        JurosV := 0;
      end else begin
        JurosV := TotalV - MultaV - TotalI;
        if JurosV < 0 then
        begin
          MultaV := MultaV + JurosV;
          JurosV := 0;
        end;
        if MultaV < 0 then
        begin
          TotalV := TotalV + MultaV;
          MultaV := 0;
        end;
      end;
      if TotalV < 0 then TotalV := 0;
      TbLctoEditJurosVal.Value := JurosV;
      TbLctoEditMultaVal.Value := MultaV;
      TbLctoEditValorPgt.Value := TotalV;
    end;
    if Campo = 'JurosVal' then
      TbLctoEditValorPgt.Value := TotalI + JurosV + MultaV;
    if Campo = 'MultaVal' then
      TbLctoEditValorPgt.Value := TotalI + JurosV + MultaV;
  end;
end;

procedure TFmLctPgEmCxa.TbLctoEditAfterPost(DataSet: TDataSet);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM ' + FTmpLctoEdit + ' WHERE Controle < 1');
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.ExecSQL;
  TbLctoEdit.Refresh;
end;

procedure TFmLctPgEmCxa.BtOKClick(Sender: TObject);
  procedure AlteraRegistroAtual(Carteira: Integer);
  var
    MultaV, JurosV, TotalV, Credito, Debito: Double;
  begin
    MultaV := TbLctoEditMultaVal.Value;
    JurosV := TbLctoEditJurosVal.Value;
    TotalV := TbLctoEditValorPgt.Value;
    //TotalI := TbLctoEditValorOri.Value;

    if TotalV < 0 then
    begin
      Debito := - TotalV;
      Credito := 0;
    end else begin
      Credito := TotalV;
      Debito := 0;
    end;
    if MultaV < 0 then MultaV := - MultaV;
    if JurosV < 0 then JurosV := - JurosV;
    //
    {
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET AlterWeb=1, ');
    Dmod.QrUpd.SQL.Add('Tipo=0, Sit=2, ');
    Dmod.QrUpd.SQL.Add('Descricao=:P0, Data=:P1, Vencimento=:P2, ');
    Dmod.QrUpd.SQL.Add('Compensado=:P3, Credito=:P4, Debito=:P5, ');
    Dmod.QrUpd.SQL.Add('MultaVal=:P6, MoraVal=:P7, Carteira=:P8 ');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
    Dmod.QrUpd.SQL.Add('');
    //
    Dmod.QrUpd.Params[00].AsString  := TbLctoEditDescricao.Value;
    Dmod.QrUpd.Params[01].AsString  := Geral.FDT(TbLctoEditData.Value, 1);
    Dmod.QrUpd.Params[02].AsString  := Geral.FDT(TbLctoEditVencimento.Value, 1);
    Dmod.QrUpd.Params[03].AsString  := Geral.FDT(TbLctoEditData.Value, 1);
    Dmod.QrUpd.Params[04].AsFloat   := Credito;
    Dmod.QrUpd.Params[05].AsFloat   := Debito;
    Dmod.QrUpd.Params[06].AsFloat   := MultaV;
    Dmod.QrUpd.Params[07].AsFloat   := JurosV;
    Dmod.QrUpd.Params[08].AsInteger := Carteira;
    //
    Dmod.QrUpd.Params[09].AsInteger := TbLctoEditControle.Value;
    Dmod.QrUpd.Params[10].AsInteger := TbLctoEditSub.Value;
    //
    Dmod.QrUpd.ExecSQL;
    }
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Tipo', 'Sit', 'Descricao', 'Data', 'Vencimento', 'Compensado', 'Credito',
      'Debito', 'MultaVal', 'MoraVal', 'Carteira'], ['Controle', 'Sub'], [
      0, 2, TbLctoEditDescricao.Value, Geral.FDT(TbLctoEditData.Value, 1),
      Geral.FDT(TbLctoEditVencimento.Value, 1), Geral.FDT(TbLctoEditData.Value, 1),
      Credito, Debito, MultaV, JurosV, Carteira], [TbLctoEditControle.Value,
      TbLctoEditSub.Value], True, '', FTabLctA);
  end;
var
  Compl1, Compl2, Msg: String;
  Carteira: Integer;
begin
  if UFinanceiro.FatIDProibidoEmLct(FQrLct.FieldByName('FatID').AsInteger, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    Exit;
  end;
  if TbLctoEdit.RecordCount > 1 then
  begin
    Compl1 := '�o';
    Compl2 := 's';
  end else begin
    Compl1 := '�';
    Compl2 := '';
  end;
  Carteira := EdCarteira.ValueVariant;
  //
  if Carteira = 0 then
  begin
    Geral.MB_Aviso('Escolha a carteira (caixa) na qual ser' +
      Compl1 + ' efetuado' + Compl2 + ' o' + Compl2 + ' pagamento' + Compl2 + '!');
    EdCarteira.SetFocus;
    Exit;
  end;
  //
  TbLctoEdit.First;
  while not TbLctoEdit.Eof do
  begin
    if TbLctoEditControle.Value <> 0 then
      AlteraRegistroAtual(Carteira);
    TbLctoEdit.Next;
  end;
  UFinanceiro.AtualizaTodasCarteiras(FQrCrt, FQrLct, FTabLctA, Carteira);
  Close;
end;

end.

