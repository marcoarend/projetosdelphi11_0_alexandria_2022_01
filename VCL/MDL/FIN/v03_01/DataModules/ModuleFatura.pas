unit ModuleFatura;

interface

uses
  Windows, Forms, Messages, Controls, SysUtils, Classes, DB,
  mySQLDbTables, dmkGeral, UnInternalConsts, UnDmkEnums, UnGrl_Geral,
  UnAppPF;

type
  TTipoFinanceiro = (tfinNone=0, tfinCred=1, tfinDeb=2);
  TDataFatura = (dfUserDefine=0, dfAbertura=1, dfEncerramento=2, dfPreDefinido=4);
  TTipoFiscal = (tfatNone=0, tfatVenda=1, tfatServico=2, tfaMultiSV=3);
  //
  TDmFatura = class(TDataModule)
    QrNF_X: TmySQLQuery;
    QrNF_XSerieNFTxt: TWideStringField;
    QrNF_XNumeroNF: TIntegerField;
    QrPrzT: TmySQLQuery;
    QrPrzTDias: TIntegerField;
    QrPrzTPercent1: TFloatField;
    QrPrzTPercent2: TFloatField;
    QrPrzTControle: TIntegerField;
    QrPrzX: TmySQLQuery;
    QrPrzXDias: TIntegerField;
    QrPrzXPercent: TFloatField;
    QrPrzXControle: TIntegerField;
    QrSumT: TmySQLQuery;
    QrSumTPercent1: TFloatField;
    QrSumTPercent2: TFloatField;
    QrSumTJurosMes: TFloatField;
    QrSumX: TmySQLQuery;
    QrSumXTotal: TFloatField;
    QrParamsEmp: TmySQLQuery;
    QrParamsEmpCtaProdVen: TIntegerField;
    QrParamsEmpFaturaSeq: TSmallintField;
    QrParamsEmpFaturaSep: TWideStringField;
    QrParamsEmpFaturaDta: TSmallintField;
    QrParamsEmpFaturaNum: TSmallintField;
    QrParamsEmpTxtProdVen: TWideStringField;
    QrParamsEmpDupProdVen: TWideStringField;
    QrParamsEmpCtaServico: TIntegerField;
    QrParamsEmpTxtServico: TWideStringField;
    QrParamsEmpDupServico: TWideStringField;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    DsPediPrzCab: TDataSource;
    QrCartEmis: TmySQLQuery;
    QrCartEmisCodigo: TIntegerField;
    QrCartEmisNome: TWideStringField;
    DsCartEmis: TDataSource;
    QrFisRegCad: TmySQLQuery;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadModeloNF: TIntegerField;
    QrFisRegCadNO_MODELO_NF: TWideStringField;
    QrFisRegCadFinanceiro: TSmallintField;
    DsFisRegCad: TDataSource;
    QrCartEmisTipo: TIntegerField;
    QrContas: TmySQLQuery;
    IntegerField1: TIntegerField;
    DsContas: TDataSource;
    QrContasNome: TWideStringField;
    QrEntiCliInt: TmySQLQuery;
    QrEntiCliIntCodEnti: TIntegerField;
    QrEntiCliIntCodFilial: TIntegerField;
    QrParamsEmpDupMultiSV: TWideStringField;
    QrParamsEmpCtaMultiSV: TIntegerField;
    QrParamsEmpTxtMultiSV: TWideStringField;
    QrProd: TMySQLQuery;
    QrProdUsaSubsTrib: TSmallintField;
    QrPediPrzCabMedDDSimpl: TFloatField;
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo
    //
  public
    { Public declarations }
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
    //
{  ini 2022-03-24
    function  EmiteFaturas(FatGru: Integer;
              TipoFID, Entidade, FatID: Integer; FatNum: Double;
              Terceiro: Integer; DataAbriu, DataEncer: TDateTime;
              EMP_IDDuplicata, NumeroNF: Integer; SerieNF: String;
              CartEmis, TipoCart, Conta, CondicaoPG, Represen: Integer;

              TipoFiscal: TTipoFiscal;
              EMP_FaturaDta: TDataFatura; Financeiro: TTipoFinanceiro;

              ValorServico: Double;

              Associada: Integer = 0; AFP_Sit: Integer = 0;
              AFP_Per: Double = 0; ASS_CtaFaturas: Integer = 0; ASS_IDDuplicata:
              Integer = 0; DataFatPreDef: TDateTime = 0; AskIns: Boolean = True;
              ExcluiLct: Boolean = True; CodHist: Integer = 0): Boolean;
fim 2022-03-24}
    function  DefineParamFaturaFilial(const StrMid: String; const Entidade: Integer;
              const TipoFiscal: TTipoFiscal;
              var FaturaNum, CtaFaturas, FaturaSeq: Integer;
              var TpDuplicata, FaturaSep, TxtFaturas: String;
              var Filial: Integer; const Genero, CodHist: Integer): Boolean;
    function  ObtemFilialDeEntidade(Entidade: Integer): Integer;
    function  ObtemEntidadeDeFilial(Filial: Integer): Integer;
    //
    function  InsereRef_3001(FatGru, FatID, FatNum, Lancto: Integer; Valor:
              Double; Vcto: TDateTime): Integer;
    //
    procedure ReopenPediPrzCab();
    procedure ReopenCartEmis(EmprEnti: Integer; Todas: Boolean = False);
    procedure ReopenContas(TipoFinanceiro: TTipoFinanceiro);
    function  InsereItemStqMov(NFe_FatID, OriCodi, OriCnta, Empresa, Cliente,
              Associada, RegrFiscal, GraGruX: Integer; NO_tabelaPrc,
              NO_TbFatPedFis: String; StqCenCad, FatSemEstq, AFP_Sit: Integer;
              AFP_Per, Qtde: Double; Cli_Tipo: Integer; Cli_IE: String; Cli_UF,
              EMP_UF, EMP_FILIAL, ASS_CO_UF, ASS_FILIAL, Item_MadeBy: Integer;
              Item_IPI_ALq: Double; Preco_PrecoF, Preco_PercCustom,
              Preco_MedidaC, Preco_MedidaL, Preco_MedidaA, Preco_MedidaE: Double;
              Preco_MedOrdem: Integer; SQLType: TSQLType; PediVda, OriPart,
              InfAdCuztm, TipoNF, modNF, Serie, nNF, SitDevolu, Servico: Integer;
              refNFe: String; TotalPreCalc: Double; var
              OriCtrl: Integer; Pecas, AreaM2, AreaP2, Peso: Double; TipoCalc,
              prod_indTot: Integer;
              // 2020-11-??
              prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
              // Fim 2020-11-??
              var IDCtrl_PreDef: Integer; const Ativa: Boolean = False): Boolean;
    function  ObtemNumeroCFOP(Tabela_FatPedFis: String; OriCodi, OriCnta,
              GraGruX, Empresa, Cliente, Item_MadeBy, RegrFiscal: Integer;
              EhServico, EhSubsTrib: Boolean; var CFOP: String; var ICMS_Aliq:
              Double; var CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio,
              CFOP_SubsTrib: Integer): Boolean;
    procedure ReopenProd(GraGruX: Integer);
  end;

var
  DmFatura: TDmFatura;

implementation

uses
  UnFinanceiro,
//{$IFNDef semPediVda} PediVda1, PediVda2, {$EndIf}
{$IfNDef SemNFe_0000} ModuleNFe_0000, {$EndIf}
  Module, ModuleGeral, GetData, UnMyObjects, UMySQLModule,
  DmkDAC_PF, UnDmkProcFunc, ModProd(*, CashTabs*),
{$IfNDef SemPediVda}   ModPediVda, {$EndIf}
  GraGruX,
  StqCenCad;

{$R *.dfm}

{ TDmFatura }

function TDmFatura.DefineParamFaturaFilial(const StrMid: String; const Entidade: Integer;
const TipoFiscal: TTipoFiscal;
var FaturaNum, CtaFaturas, FaturaSeq: Integer;
var TpDuplicata, FaturaSep, TxtFaturas: String;
var Filial: Integer;
const Genero, CodHist: Integer): Boolean;
var
  TxtHist: String;
begin
  Result := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrParamsEmp, Dmod.MyDB, [
  'SELECT par.CtaProdVen, par.CtaServico, ',
  'par.FaturaSeq, par.FaturaSep, ',
  'par.FaturaDta, par.FaturaNum, ',
  'par.TxtProdVen, par.DupProdVen, ',
  'par.TxtServico, par.DupServico, ',
  'CtaMultiSV, TxtMultiSV, DupMultiSV ',
  'FROM paramsemp par ',
  'WHERE par.Codigo=' + Geral.FF0(Entidade),
  '']);
  Filial := ObtemFilialDeEntidade(Entidade);
  //
  FaturaSep := QrParamsEmpFaturaSep.Value;
  FaturaSeq := QrParamsEmpFaturaSeq.Value;
  FaturaNum := QrParamsEmpFaturaNum.Value;
  case TipoFiscal of
    tfatVenda:
    begin
      CtaFaturas  := QrParamsEmpCtaProdVen.Value;
      TpDuplicata := QrParamsEmpDupProdVen.Value;
      TxtFaturas  := QrParamsEmpTxtProdVen.Value;
      Result := True;
    end;
    tfatServico:
    begin
      CtaFaturas  := QrParamsEmpCtaServico.Value;
      TpDuplicata := QrParamsEmpDupServico.Value;
      TxtFaturas  := QrParamsEmpTxtServico.Value;
      Result := True;
    end;
    tfaMultiSV:
    begin
      CtaFaturas  := QrParamsEmpCtaMultiSV.Value;
      TpDuplicata := QrParamsEmpDupMultiSV.Value;
      TxtFaturas  := QrParamsEmpTxtMultiSV.Value;
      Result := True;
      //
    end;
    else
    begin
      CtaFaturas  := 0;
      TpDuplicata := '';
      TxtFaturas  := '';
      //
      Geral.MB_Erro('Tipo de ' + StrMid + ' indefinido!' + sLineBreak +
      'Entidade empresarial: ' + Geral.FF0(Entidade));
    end;
  end;
  if CodHist <> 0 then
  begin
    TxtHist := Trim(AppPF.TextoDeCodHist_Sigla(CodHist));
    if TxtHist <> EmptyStr then
    begin
      TxtFaturas := TxtHist;
    end;
  end;
  if (CtaFaturas = 0) and (Genero = 0) then
  begin
    Result := False;
    //
    Geral.MB_Erro('Conta (do plano de contas) indefinido!' + sLineBreak +
    'Defina as contas para venda, servi�o, frete, etc nos par�metros das filiais!'
    + sLineBreak + 'Entidade empresarial: ' + Geral.FF0(Entidade));
  end;
end;

procedure TDmFatura.DefineProperties(Filer: TFiler);
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

(*  ini 2022-03-24
function TDmFatura.EmiteFaturas(FatGru: Integer;
  TipoFID, Entidade, FatID: Integer; FatNum: Double;
  Terceiro: Integer; DataAbriu, DataEncer: TDateTime;
  EMP_IDDuplicata, NumeroNF: Integer;  SerieNF: String;
  CartEmis, TipoCart, Conta, CondicaoPG, Represen: Integer;
  TipoFiscal: TTipoFiscal;
  EMP_FaturaDta: TDataFatura; Financeiro: TTipoFinanceiro;

  ValorServico: Double;

  Associada: Integer = 0; AFP_Sit: Integer = 0;
  AFP_Per: Double = 0; ASS_CtaFaturas: Integer = 0; ASS_IDDuplicata:
  Integer = 0; DataFatPreDef: TDateTime = 0; AskIns: Boolean = True;
  ExcluiLct: Boolean = True; CodHist: Integer = 0): Boolean;
  //
const
  sProcName = 'TDmFatura.EmiteFaturas()';
  //
  procedure IncluiLancto(Valor: Double; Data, Vencto: TDateTime; Duplicata,
    Descricao: String; TipoCart, Carteira, Genero, CliInt, Parcela, FatParcRef,
    NotaFiscal, Account: Integer; SerieNF: String; VerificaCliInt: Boolean;
    TabLctA: String; Lancto: Integer);
  var
    Sit: Integer;
  begin
    UFinanceiro.LancamentoDefaultVARS;
    //
    if Financeiro = tfinCred then
    begin
      FLAN_Credito    := Valor;
      FLAN_Cliente    := Terceiro;
    end else
    if Financeiro = tfinDeb then
    begin
      FLAN_Debito     := Valor;
      FLAN_Fornecedor := Terceiro;
    end;
    //
   if TipoCart = 2 then
     Sit := 0
   else
     Sit := 3;
    //
    FLAN_VERIFICA_CLIINT := VerificaCliInt;
    FLAN_Sit        := Sit;
    FLAN_Data       := Geral.FDT(Data, 1);
    FLAN_Tipo       := TipoCart;
    FLAN_Documento  := 0;
    FLAN_MoraDia    := QrSumTJurosMes.Value;
    FLAN_Multa      := 0;//
    FLAN_Carteira   := Carteira;
    F L A N _ G e n e r o     := Genero;
  FLAN_GenCtb     := GenCtb;
  FLAN_GenCtbD    := GenCtbD;
  FLAN_GenCtbC    := GenCtbC;
    FLAN_CliInt     := CliInt;
    //FLAN_Depto      := QrBolacarAApto.Value;
    //FLAN_ForneceI   := QrBolacarAPropriet.Value;
    FLAN_Vencimento   := Geral.FDT(Vencto, 1);
    FLAN_VctoOriginal := Geral.FDT(Vencto, 1);
    //FLAN_Mez        := IntToStr(dmkPF.PeriodoToAnoMes(QrPrevPeriodo.Value));
    FLAN_FatID      := FatID;
    FLAN_FatNum     := FatNum;
    FLAN_FatParcela := Parcela;
    {$IFDEF DEFINE_VARLCT}
    FLAN_FatParcRef := FatParcRef;
    {$ENDIF}
    FLAN_Descricao  := Descricao;
    FLAN_Duplicata  := Duplicata;
    FLAN_SerieNF    := SerieNF;
    FLAN_NotaFiscal := NotaFiscal;
    FLAN_Account    := Account;
    if Lancto <> 0 then
      FLAN_CONTROLE := Lancto
    else
      FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
      // 2015-07-21
      //'Controle', VAR_LCT, VAR_LCT, 'Controle');
      'Controle', LAN_CTOS, LAN_CTOS, 'Controle');
      // FIM 2015-07-21
    {$IFDEF DEFINE_VARLCT}
    if UFinanceiro.InsereLancamento(TabLctA) then
    begin
      // nada
    end;
    {$ELSE}
    if UFinanceiro.InsereLancamento() then
    begin
      // nada
    end;
    {$ENDIF}
  end;
const
  Pergunta = False;
var
  DataFat: TDateTime;
  FaturaNum, DataEnc_TXT, Duplicata, NovaDup, NF_Emp_Serie, NF_Ass_Serie: String;
  Parcela, FatParcRef, NF_Emp_Numer, NF_Ass_Numer: Integer;
  T1, T2, F1, F2, V1, V2, P1, P2: Double;
  TemAssociada: Boolean;
  FreteVal, Seguro, Outros: Double;
  //
  EMP_CtaFaturas, EMP_FaturaSeq, EMP_FaturaNum: Integer;
  EMP_TpDuplicata, EMP_FaturaSep, EMP_TxtFaturas: String;
  //
  Ass_FaturaSeq, Ass_FaturaNum: Integer;
  Ass_TpDuplicata, Ass_FaturaSep, Ass_TxtFaturas: String;
  //
  TabLctA: String;
  Emp_Filial, Ass_Filial, Lancto: Integer;
  Vencto: TDateTime;
  StrIni, StrMid: String;
  Insere: Boolean;
begin
  F1 := 0;
  T1 := 0;
  //ShowMessage(IntToStr(TipoFat));
  Result := False;
  if  (TipoFID <> VAR_FATID_0001)
  and (TipoFID <> VAR_FATID_0102)
  and (TipoFID <> VAR_FATID_0103)
  and (TipoFID <> VAR_FATID_3001) // TREN
  and (TipoFID <> VAR_FATID_3002) // TREN  2015-07-20
  and (TipoFID <> VAR_FATID_3011) // TREN  2015-07-30
  and (TipoFID <> VAR_FATID_4001) // BUGS
  and (TipoFID <> VAR_FATID_7001) // YROd  2021-01-01
  then
  begin
    Geral.MB_Erro('TipoFID ' + Geral.FF0(TipoFID) +
    ' n�o definido para a vari�vel "StrMid"! ' + sLineBreak +
    sProcName + sLineBreak + 'AVISE A DERMATEK!');
    Exit;
  end;
  // Encerra Faturamento
  if Financeiro = tfinDeb then
  begin
    StrIni := 'Lan�amento(s) de obriga��o';
    StrMid := 'lan�amento(s) de obriga��o';
  end else
  begin
    StrIni := 'Lan�amento(s) de faturamento';
    StrMid := 'Lan�amento(s) de faturamento';
  end;
  //
  if AskIns then
    Insere := Geral.MB_Pergunta('Confirma o ' + StrMid + '?') = ID_YES
  else
    Insere := True;
  if Insere then
  begin
    try
      //
      if not DefineParamFaturaFilial(StrMid, Entidade, TipoFiscal,
        Emp_FaturaNum, Emp_CtaFaturas, Emp_FaturaSeq,
        Emp_TpDuplicata, Emp_FaturaSep, Emp_TxtFaturas, Emp_Filial, {Genero}Conta, CodHist) then
      Exit;
      TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, EMP_Filial);

      if Conta <> 0 then
        Emp_CtaFaturas := Conta;

      // Exclui lan�amentos para evitar duplica��o
      // **************Ver como fazer!***************
      if (FatID <> 0) and (FatNum <> 0) and (ExcluiLct = True) then
      begin
        {$IFDEF DEFINE_VARLCT}
        UFinanceiro.ExcluiLct_FatNum(nil, FatID, FatNum, Entidade, CartEmis,
          dmkPF.MotivDel_ValidaCodigo(311), Pergunta, TabLctA);
        {$ELSE}
        UFinanceiro.ExcluiLct_FatNum(nil, FatID, FatNum,
          Entidade, CartEmis, Pergunta);
        {$ENDIF}
      end;
      //
      if (TipoFID in ([VAR_FATID_0001, VAR_FATID_0102, VAR_FATID_0103])) then
      begin
        QrNF_X.Close;
        QrNF_X.Params[00].AsInteger := TipoFID;
        QrNF_X.Params[01].AsInteger := Trunc(FatNum);
        QrNF_X.Params[02].AsInteger := Entidade;
        UnDmkDAC_PF.AbreQuery(QrNF_X, Dmod.MyDB);
        NF_Emp_Serie := QrNF_XSerieNFTxt.Value;
        NF_Emp_Numer := QrNF_XNumeroNF.Value;
        if (Trim(NF_Emp_Serie) = '') or (NF_Emp_Numer = 0) then
        begin
          Geral.MB_Aviso(StrIni + ' n�o realizado!'+ sLineBreak +
            'O n�mero de nota fiscal n�o foi definida para a empresa ' +
            FormatFloat('000', EMP_FILIAL) + ' !'+ sLineBreak +
            'Para definir o n�mero de nota fiscal clique no bot�o "Fatura" > "'+
            'Nota Fiscal"');
          Exit;
        end;
        //
        TemAssociada := (AFP_Sit = 1) and (AFP_Per > 0);
        if TemAssociada then
        begin
          if not DefineParamFaturaFilial(StrMid, Associada, TipoFiscal,
          Ass_FaturaNum, Ass_CtaFaturas, Ass_FaturaSeq,
          Ass_TpDuplicata, Ass_FaturaSep, Ass_TxtFaturas, Ass_Filial, Conta{Genero}, CodHist) then
            Exit;
          //
          QrNF_X.Close;
          QrNF_X.Params[00].AsInteger := TipoFID;
          QrNF_X.Params[01].AsInteger := Trunc(FatNum);
          QrNF_X.Params[02].AsInteger := Associada;
          UnDmkDAC_PF.AbreQuery(QrNF_X, Dmod.MyDB);
          NF_Ass_Serie := QrNF_XSerieNFTxt.Value;
          NF_Ass_Numer := QrNF_XNumeroNF.Value;
          if (Trim(NF_Ass_Serie) = '') or (NF_Ass_Numer = 0) then
          begin
            Geral.MB_Aviso(StrIni + ' n�o realizado!'+ sLineBreak +
            'O n�mero de nota fiscal n�o foi definida para a empresa ' +
            FormatFloat('000', ASS_FILIAL) + ' !'+ sLineBreak +
            'Para definir o n�mero de nota fiscal clique no bot�o "Fatura" > "'+
            'Nota Fiscal"');
            Exit;
          end;
          //
          P2 := AFP_Per;
          P1 := 100 - P2;
        end else begin
          P1 := 100;
          P2 := 0;
          NF_Ass_Serie := '';
          NF_Ass_Numer := 0;
        end;
        //
        if EMP_CtaFaturas = 0 then
        begin
          Geral.MB_Aviso(StrIni + ' n�o realizado!'+ sLineBreak +
          'A conta de venda de produtos n�o foi definida para a empresa ID = ' +
          IntToStr(EMP_FILIAL) + '!');
          Exit;
        end;
        if (Associada <> 0) and
        (ASS_CtaFaturas = 0) and TemAssociada then
        begin
          Geral.MB_Aviso(StrIni + ' n�o realizado!'+ sLineBreak +
          'A conta de venda de produtos n�o foi definida para a empresa ID = ' +
          IntToStr(ASS_FILIAL) + '!');
          Exit;
        end;
      end else
      if (TipoFID = VAR_FATID_3001) // TREN
      or (TipoFID = VAR_FATID_3002) // TREN // 2015-07-20
      or (TipoFID = VAR_FATID_4001) // BUGS
      or (TipoFID = VAR_FATID_7001) // YROd
      then
      begin
        NF_Emp_Serie := SerieNF;
        NF_Emp_Numer := NumeroNF;
        //
        TemAssociada := False;
        NF_Ass_Serie := '';
        NF_Ass_Numer := 0;
        //
        P1 := 100;
        P2 := 0;
      end else
      begin
        if (TipoFID <> VAR_FATID_3011) then // TREN // 2015-07-30
        begin
          Geral.MB_Erro('Defina��o de s�rie e n�mero de NF n�o implementado!');
        end;
        //
        NF_Emp_Serie := '';
        NF_Emp_Numer := 0;
        //
        TemAssociada := False;
        NF_Ass_Serie := '';
        NF_Ass_Numer := 0;
        //
        P1 := 100;
        P2 := 0;
      end;
      if Financeiro <> tfinNone then
      begin
        if CartEmis = 0 then
        begin
          Geral.MB_Aviso(StrIni + ' n�o realizado!'+ sLineBreak +
          'A carteira n�o foi definida no pedido selecionado! (0)');
          Exit;
        end;
        QrPrzT.Close;
        QrPrzT.Params[0].AsInteger := CondicaoPG;
        UnDmkDAC_PF.AbreQuery(QrPrzT, Dmod.MyDB);
        if QrPrzT.RecordCount = 0 then
        begin
          Geral.MB_Aviso(StrIni + ' n�o realizado!'+ sLineBreak +
          'N�o h� parcela(s) definida(s) para a empresa ' +
          IntToStr(EMP_FILIAL) + ' na condi��o de pagamento ' +
          'cadastrada no pedido selecionado!');
          Exit;
        end;
        if (AFP_Sit = 1) and
        (AFP_Per > 0) then
        begin
          QrSumT.Close;
          QrSumT.Params[0].AsInteger := CondicaoPG;
          UnDmkDAC_PF.AbreQuery(QrSumT, Dmod.MyDB);
          if (QrSumTPercent2.Value <> AFP_Per) then
          begin
            Geral.MB_Aviso(StrIni + ' n�o realizado!'+ sLineBreak +
            'Percentual da fatura parcial no pedido n�o confere com percentual ' +
            'nos prazos da condi��o de pagamento!');
            Exit;
          end;
          if QrSumT.RecordCount = 0 then
          begin
            Geral.MB_Aviso(StrIni + ' n�o realizado!'+ sLineBreak +
            'Percentual da fatura parcial no pedido n�o confere com percentual ' +
            'nos prazos da condi��o de pagamento!');
            Exit;
          end;
        end;
      end else Geral.MB_Info('N�o ser� gerado lan�amentos financeiros.' +
      sLineBreak + 'Na Regra Fiscal est� definido para n�o gerar lan�amentos!');
      Screen.Cursor := crHourGlass;
      //DataEnc := DModG.ObtemAgora();
      DataEnc_TXT := Geral.FDT(DataEncer, 105);
      case EMP_FaturaDta of
        dfUserDefine:
        begin
          MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
          FmGetData.TPData.Date := Date;
          FmGetData.OcultaJurosEMulta();
          FmGetData.ShowModal;
          FmGetData.Destroy;
          DataFat := VAR_GETDATA;
        end;
        dfAbertura: DataFat     := DataAbriu;
        dfEncerramento: DataFat := DataEncer;
        dfPreDefinido: DataFat  := DataFatPreDef;
        else DataFat := 0;
      end;
      if DataFat = 0 then
      begin
        Geral.MB_Aviso(StrIni + ' n�o realizado!'+ sLineBreak +
        'Data de faturamento n�o definida!');
        Exit;
      end else
      begin
        if (TipoFID in ([VAR_FATID_0001, VAR_FATID_0102, VAR_FATID_0103])) then
        begin
          // Alterar data de emiss�o da NF
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False, [
            'DtEmissNF'], ['Tipo', 'OriCodi'
          ], [Geral.FDT(DataFat, 1)], [TipoFID, FatNum], True);
        end;
      end;
      //
      // Faturamento empresa principal
      if (TipoFID in ([VAR_FATID_0001, VAR_FATID_0102, VAR_FATID_0103])) then
      begin
        QrSumX.Close;
        QrSumX.SQL.Clear;
        QrSumX.SQL.Add('SELECT SUM(Total) Total');
        QrSumX.SQL.Add('FROM stqmovvala');
        QrSumX.SQL.Add('WHERE Tipo='  + FormatFloat('0', TipoFID));
        QrSumX.SQL.Add('AND OriCodi=' + FormatFloat('0', FatNum));
        QrSumX.SQL.Add('AND Empresa=' + FormatFloat('0', Entidade));
        UnDmkDAC_PF.AbreQuery(QrSumX, Dmod.MyDB);
        T1 := QrSumXTotal.Value + FreteVal + Seguro + Outros;
        F1 := T1;
      end else
      if (TipoFID = VAR_FATID_3001) // TREN
      or (TipoFID = VAR_FATID_3002) // TREN // 2015-07-20
      or (TipoFID = VAR_FATID_3011) // TREN // 2015-07-30
      or (TipoFID = VAR_FATID_4001) // BUGS
      or (TipoFID = VAR_FATID_7001) // YROd // 2020-01-01
      then
      begin
        T1 := ValorServico;
        F1 := T1;
      end;
      if Financeiro <> tfinNone then
      begin
        QrPrzX.Close;
        QrPrzX.SQL.Clear;
        QrPrzX.SQL.Add('SELECT Controle, Dias, Percent1 Percent ');
        QrPrzX.SQL.Add('FROM pediprzits');
        QrPrzX.SQL.Add('WHERE Percent1 > 0');
        QrPrzX.SQL.Add('AND Codigo=:P0');
        QrPrzX.SQL.Add('ORDER BY Dias');
        QrPrzX.Params[00].AsInteger := CondicaoPG;
        UnDmkDAC_PF.AbreQuery(QrPrzX, Dmod.MyDB);
        QrPrzX.First;
        while not QrPrzX.Eof do
        begin
          if QrPrzX.RecordCount = QrPrzX.RecNo then
            V1 := F1
          else begin
            if P1 = 0 then V1 := 0 else
              V1 := (Round(T1 * (QrPrzXPercent.Value / P1 * 100))) / 100;

            F1 := F1 - V1;
          end;

          QrPrzT.Locate('Controle', QrPrzXControle.Value, []);
          Parcela := QrPrzT.RecNo;
          case EMP_FaturaNum of
            CO_FATNUM_POR_COD_FAT: FaturaNum := FormatFloat('000000', EMP_IDDuplicata);
            CO_FATNUM_POR_COD_NNF: FaturaNum := FormatFloat('000000', NumeroNF);
            CO_FATNUM_POR_COD_ORI:
            begin
              if FatGru <> 0 then
                FaturaNum := FormatFloat('000000', FatGru)
              else
                FaturaNum := FormatFloat('000000', EMP_IDDuplicata);
            end
            else begin
              Geral.MB_Erro('"ComoGeraFatNum" n�o definido!');
              FaturaNum := '0';
            end;
          end;
          Duplicata := EMP_TpDuplicata + FaturaNum + EMP_FaturaSep +
            dmkPF.ParcelaFatura(QrPrzX.RecNo, EMP_FaturaSeq);
          // 2011-08-21
          // Teste para substituir no futuro (uso no form FmFatDivCms)
          NovaDup := DmProd.MontaDuplicata(EMP_IDDuplicata, NumeroNF,
          Parcela, EMP_FaturaNum, EMP_FaturaSeq,
          EMP_TpDuplicata, EMP_FaturaSep);
          //if NovaDup <> Duplicata then
            //Geral.MB_Info('Defini��o da duplicata:' + sLineBreak +
            //Duplicata + ' <> ' + NovaDup);
          // Fim 2011-08-21
          Lancto := 0;
          Vencto := DataFat + QrPrzXDias.Value;
          if (TipoFID = VAR_FATID_3001) // TREN
          or (TipoFID = VAR_FATID_3002) // TREN // 2015-07-20
          or (TipoFID = VAR_FATID_3011) // TREN // 2015-07-30
          or (TipoFID = VAR_FATID_4001) // BUGS
          or (TipoFID = VAR_FATID_7001) // YROd // 2020-01-01
          then
          begin
            Lancto := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
                      // 2015-07-21
                      //'Controle', VAR_LCT, VAR_LCT, 'Controle');
                      'Controle', LAN_CTOS, LAN_CTOS, 'Controle');
                      // FIM 2015-07-21
            FatParcRef := InsereRef_3001(FatGru, TipoFID, Trunc(FatNum), Lancto, V1, Vencto);
          end
          else FatParcRef := 0;
          IncluiLancto(V1, DataFat, Vencto, Duplicata, EMP_TxtFaturas,
            TIPOCART, CartEmis, EMP_CtaFaturas, Entidade, Parcela, FatParcRef,
            NF_Emp_Numer, Represen, NF_Emp_Serie, True, TabLctA, Lancto);
          //
          QrPrzX.Next;
        end;


        // Faturamento associada
        if TemAssociada then
        begin
          if not DefineParamFaturaFilial(StrMid, Associada, TipoFiscal,
          Emp_FaturaNum, Ass_CtaFaturas, Ass_FaturaSeq,
          Ass_TpDuplicata, Ass_FaturaSep, Ass_TxtFaturas, Ass_Filial, Conta{Genero}, CodHist) then
            Exit;
          //
          QrSumX.Close;
          QrSumX.SQL.Clear;
          QrSumX.SQL.Add('SELECT SUM(Total) Total');
          QrSumX.SQL.Add('FROM stqmovvala');
          QrSumX.SQL.Add('WHERE Tipo='  + FormatFloat('0', TipoFID));
          QrSumX.SQL.Add('AND OriCodi=' + FormatFloat('0', FatNum));
          QrSumX.SQL.Add('AND Empresa=' + FormatFloat('0', Associada));
          UnDmkDAC_PF.AbreQuery(QrSumX, Dmod.MyDB);
          T2 := QrSumXTotal.Value;
          F2 := T2;
          //
          QrPrzX.Close;
          QrPrzX.SQL.Clear;
          QrPrzX.SQL.Add('SELECT Controle, Dias, Percent2 Percent ');
          QrPrzX.SQL.Add('FROM pediprzits');
          QrPrzX.SQL.Add('WHERE Percent2 > 0');
          QrPrzX.SQL.Add('AND Codigo=:P0');
          QrPrzX.SQL.Add('ORDER BY Dias');
          QrPrzX.Params[00].AsInteger := CondicaoPG;
          UnDmkDAC_PF.AbreQuery(QrPrzX, Dmod.MyDB);
          QrPrzX.First;
          while not QrPrzX.Eof do
          begin
            if QrPrzX.RecordCount = QrPrzX.RecNo then
              V2 := F2
            else begin
              if P2 = 0 then V2 := 0 else
                V2 := (Round(T2 * (QrPrzXPercent.Value / P2 * 100))) / 100;
              F2 := F2 - V2;
            end;
            QrPrzT.Locate('Controle', QrPrzXControle.Value, []);
            Parcela := QrPrzT.RecNo;

            if EMP_FaturaNum = 0 then
              FaturaNum := FormatFloat('000000', ASS_IDDuplicata)
            else
              FaturaNum := FormatFloat('000000', NumeroNF);
            Duplicata := ASS_TpDuplicata + FormatFloat('000000', ASS_IDDuplicata) +
              ASS_FaturaSep + dmkPF.ParcelaFatura(
              QrPrzX.RecNo, ASS_FaturaSeq);
            IncluiLancto(V2, DataFat, DataFat + QrPrzXDias.Value, Duplicata,
              ASS_TxtFaturas, TIPOCART, CartEmis, ASS_CtaFaturas, Associada,
              Parcela, 0, NF_Ass_Numer, Represen, NF_Ass_Serie, False, TabLctA, 0);
            //
            QrPrzX.Next;
          end;
        end;
      end;
      //
      if (TipoFID in ([VAR_FATID_0001, VAR_FATID_0102, VAR_FATID_0103])) then
      begin
        if TipoFiscal = tfatVenda then
        begin
          // Ativa itens no movimento
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Ativo=1');
          Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1 ');
          Dmod.QrUpd.Params[00].AsInteger := TipoFID;
          Dmod.QrUpd.Params[01].AsInteger := Trunc(FatNum);
          Dmod.QrUpd.ExecSQL;
          //
          // Encerra definivamente faturamento
          case TipoFID of
            1: // FatPedCab
            begin
              Dmod.QrUpd.SQL.Clear;
              Dmod.QrUpd.SQL.Add('UPDATE fatpedcab SET ');
              Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
              Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
              Dmod.QrUpd.Params[00].AsString  := DataEnc_TXT;
              Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
              Dmod.QrUpd.Params[02].AsInteger := Trunc(FatNum);
              Dmod.QrUpd.ExecSQL;
            end;
            102: //CMPTOut
            begin
              Dmod.QrUpd.SQL.Clear;
              Dmod.QrUpd.SQL.Add('UPDATE cmptout SET ');
              Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
              Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
              Dmod.QrUpd.Params[00].AsString  := DataEnc_TXT;
              Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
              Dmod.QrUpd.Params[02].AsInteger := Trunc(FatNum);
              Dmod.QrUpd.ExecSQL;
            end;
            103: //NFeMPInn
            begin
              Dmod.QrUpd.SQL.Clear;
              Dmod.QrUpd.SQL.Add('UPDATE nfempinn SET ');
              Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
              Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
              Dmod.QrUpd.Params[00].AsString  := DataEnc_TXT;
              Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
              Dmod.QrUpd.Params[02].AsInteger := Trunc(FatNum);
              Dmod.QrUpd.ExecSQL;
            end;
            else Geral.MB_Erro(
            'Faturamento sem finaliza��o! AVISE A DERMATEK!');
          end;
        end;
      end else
      if (TipoFID = VAR_FATID_3001) // TREN
      or (TipoFID = VAR_FATID_3002) // TREN // 2015-07-20
      or (TipoFID = VAR_FATID_3011) // TREN // 2015-07-30
      or (TipoFID = VAR_FATID_4001) // BUGS
      or (TipoFID = VAR_FATID_7001) // BUGS // 2020-01-01
      then
      begin
        // Nada
      end;
      //

      Result := True;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;
*)

function TDmFatura.InsereItemStqMov(NFe_FatID, OriCodi, OriCnta, Empresa,
  Cliente, Associada, RegrFiscal, GraGruX: Integer; NO_tabelaPrc,
  NO_TbFatPedFis: String; StqCenCad, FatSemEstq, AFP_Sit: Integer; AFP_Per,
  Qtde: Double; Cli_Tipo: Integer; Cli_IE: String; Cli_UF, EMP_UF, EMP_FILIAL,
  ASS_CO_UF, ASS_FILIAL, Item_MadeBy: Integer; Item_IPI_ALq, Preco_PrecoF,
  Preco_PercCustom, Preco_MedidaC, Preco_MedidaL, Preco_MedidaA, Preco_MedidaE:
  Double; Preco_MedOrdem: Integer; SQLType: TSQLType; PediVda, OriPart,
  InfAdCuztm, TipoNF, modNF, Serie, nNF, SitDevolu, Servico: Integer;
  refNFe: String; TotalPreCalc: Double; var OriCtrl: Integer; Pecas, AreaM2,
  AreaP2, Peso: Double; TipoCalc, prod_indTot: Integer;
  prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
  var IDCtrl_PreDef: Integer; const Ativa: Boolean): Boolean;

  procedure AlteraCliente(Cliente: Integer; QrCli: TmySQLQuery);
  begin
    DModG.CadastroDeEntidade(Cliente, fmcadEntidade2, fmcadEntidade2);
    if QrCli <> nil then
    begin
      QrCli.Close;
      UnDmkDAC_PF.AbreQueryApenas(QrCli);
    end;
  end;

var
  DataHora, Msg: String;
  ID, IDCtrl, AlterWeb, Ativo,
  Continua: Integer;
  Sdo, Falta, Preco, Total, PercCustom: Double;
  //
  CFOP_Contrib,
  CFOP_MesmaUF,
  CFOP_Proprio, CFOP_SubsTrib: Integer;
  CFOP_Emp, CFOP_Ass: String;
  TemAssociada: Boolean;
  Campo: String;
  MedidaC, MedidaL, MedidaA, MedidaE: Double;
  MedOrdem: Integer;
  //
  EhServico, EhSubsTrib: Boolean;
  ICMS_Aliq, Qtd: Double;
  Baixa, Fator: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    OriCtrl := 0;
    Result  := False;
    //MedidaC := 0;
    //
    TemAssociada := (AFP_Sit = 1) and (AFP_Per > 0);
    //
    if not (SQLType in [stIns, stUpd]) then
    begin
      Geral.MB_Erro('Status da janela sem a��o definida!' + sLineBreak +
        'INFORME A DERMATEK');
      Exit;
    end;
    if (GraGruX = 0) and (Servico = 0) then
    begin
      Geral.MB_Aviso('Reduzido n�o definido!');
      Exit;
    end;

    // CFOP Empresa principal
    EhServico  := Servico  <> 0;
    ReopenProd(GraGruX);
    EhSubsTrib := QrProdUsaSubsTrib.Value <> 0;
    //
    if not ObtemNumeroCFOP(NO_TbFatPedFis, OriCodi, OriCnta, GraGruX, Empresa,
      Cliente, Item_MadeBy, RegrFiscal, EhServico, EhSubsTrib, CFOP_Emp, ICMS_Aliq,
      CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib)
    then
      Exit;
    // CFOP Empresa associada
    if TemAssociada then
    begin
      if ASS_CO_UF = 0 then
      begin
         Geral.MB_Aviso('UF da empresa ' + Geral.FF0(ASS_FILIAL) + ' n�o definida!');
         Exit;
      end;
      if not ObtemNumeroCFOP(NO_TbFatPedFis, OriCodi, OriCnta, GraGruX, Empresa,
        Cliente, Item_MadeBy, RegrFiscal, EhServico, EhSubsTrib, CFOP_Ass,
        ICMS_Aliq, CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib)
      then
        Exit;
    end;
    //
    //  dados gerais
    DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    AlterWeb  := 1;
    // ini 2020-10-12
    if Ativa then
      Ativo     := 1
    else
    // fim 2020-10-12
      Ativo     := 0;
    //
    case TipoCalc of
        1: Fator :=  1;
        2: Fator := -1;
      else Fator :=  0;
    end;
    Qtd := Qtde * Fator;
    //
    Continua := DmProd.VerificaEstoqueProduto(Qtd, Fator, GraGruX, StqCenCad,
                  RegrFiscal, OriCodi, OriCnta, Empresa, FatSemEstq, False, Msg);
    //
    if Continua = ID_YES then
    begin
      MedidaC    := Preco_MedidaC;
      MedidaL    := Preco_MedidaL;
      MedidaA    := Preco_MedidaA;
      MedidaE    := Preco_MedidaE;
      MedOrdem   := Preco_MedOrdem;
      //
      Campo     := 'stqmov' + FormatFloat('000', NFe_FatID);
      OriCtrl   := DModG.BuscaProximoCodigoInt('controle', Campo(*'stqmov001'*), '', 0);
      //
      case TipoCalc of
        0: Baixa :=   0; //Nada
        1: Baixa :=   1; //Adiciona
        2: Baixa :=  -1; //Subtrai
        else begin
          Geral.MB_Erro('Tipo de c�lculo de estoque n�o definido!');
          Exit;
        end;
      end;
      if IDCtrl_PreDef = 0 then
        IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0)
      else
        IDCtrl := IDCtrl_PreDef;

      //  N�o perder tempo com pesquisa de ID ?
      if (SQLType = stUpd) and (IDCtrl_PreDef <> 0) then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM stqmovvala ');
        Dmod.QrUpd.SQL.Add('WHERE IDCtrl=:P0');
        Dmod.QrUpd.Params[0].AsInteger := IDCtrl_PreDef;
        Dmod.QrUpd.ExecSQL;
      end;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqmovitsa', False, [
        'DataHora', 'OriCnta', 'Empresa',
        'StqCenCad', 'GraGruX', 'Qtde',
        'OriPart',
        'Pecas', 'AreaM2', 'AreaP2', 'Peso',
        'Baixa', 'AlterWeb', 'Ativo',
        'Tipo', 'OriCodi', 'OriCtrl',
        'vFrete', 'vSeg', 'vDesc', 'vOutro'
        ], ['IDCtrl'], [
        DataHora, OriCnta, Empresa,
        StqCenCad, GraGruX, Qtde,
        OriPart,
        Pecas, AreaM2, AreaP2, Peso,
        Baixa, AlterWeb, Ativo,
        NFe_FatID, OriCodi, OriCtrl,
        prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro
        ], [IDCtrl], False) then
      begin
        if TemAssociada then
        begin
          Preco := dmkPF.FFF(Preco_PrecoF * AFP_Per / 100,
                     Dmod.QrControleCasasProd.Value, siNegativo);
          //
          PercCustom := Preco_PercCustom;
          Associada  := Associada;
          if TotalPreCalc >= 0.01 then
            Total := TotalPreCalc
          else
            Total := Qtde * Preco;
          //
          ID := UMyMod.BuscaEmLivreY_Def('stqmovvala', 'ID', (*SQLType*)stIns, 0);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, (*SQLType*)stIns, 'stqmovvala', False, [
            'Tipo', 'OriCodi', 'OriCtrl', 'OriCnta', 'Empresa',
            'StqCenCad', 'GraGruX', 'Qtde', 'Preco', 'Total',
            'CFOP', 'IDCtrl', 'SeqInReduz', 'InfAdCuztm', 'PercCustom', 'MedidaC',
            'MedidaL', 'MedidaA', 'MedidaE', 'MedOrdem',
            'TipoNF', 'refNFe', 'modNF', 'Serie', 'nNF', 'SitDevolu', 'Servico',
            'CFOP_Contrib', 'CFOP_MesmaUF', 'CFOP_Proprio', 'prod_indTot',
            'prod_vFrete', 'prod_vSeg', 'prod_vDesc', 'prod_vOutro'
            ], ['ID'], [
            NFe_FatID, OriCodi, OriCtrl, OriCnta, ASSOCIADA, // <- CUIDADO!!! N�o trocar por empresa!
            StqCenCad, GraGruX, Qtde, Preco, Total,
            CFOP_Ass, IDCtrl, 0, InfAdCuztm, PercCustom, MedidaC,
            MedidaL, MedidaA, MedidaE, MedOrdem,
            TipoNF, refNFe, modNF, Serie, nNF, SitDevolu, Servico,
            CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, prod_indTot,
            prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro
            ], [ID], False);
        end else
          Preco := 0;
        //
        PercCustom := Preco_PercCustom;
        Preco      := Preco_PrecoF - Preco;
        Total      := Qtde * Preco;
        //
        ID := UMyMod.BuscaEmLivreY_Def('stqmovvala', 'ID', (*SQLType*)stIns, 0);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, (*SQLType*)stIns, 'stqmovvala', False, [
          'Tipo', 'OriCodi', 'OriCtrl', 'OriCnta', 'Empresa',
          'StqCenCad', 'GraGruX', 'Qtde', 'Preco', 'Total',
          'CFOP', 'IDCtrl', 'SeqInReduz', 'InfAdCuztm', 'PercCustom', 'MedidaC',
          'MedidaL', 'MedidaA', 'MedidaE', 'MedOrdem',
          'TipoNF', 'refNFe', 'modNF', 'Serie', 'nNF', 'SitDevolu', 'Servico',
          'CFOP_Contrib', 'CFOP_MesmaUF', 'CFOP_Proprio', 'prod_indTot',
          'prod_vFrete', 'prod_vSeg', 'prod_vDesc', 'prod_vOutro'
          ], ['ID'], [
          NFe_FatID, OriCodi, OriCtrl, OriCnta, Empresa,
          StqCenCad, GraGruX, Qtde, Preco, Total,
          CFOP_Emp, IDCtrl, 1, InfAdCuztm, PercCustom, MedidaC,
          MedidaL, MedidaA, MedidaE, MedOrdem,
          TipoNF, refNFe, modNF, Serie, nNF, SitDevolu, Servico,
          CFOP_Contrib, CFOP_MesmaUF, CFOP_Proprio, prod_indTot,
          prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro
          ], [ID], False) then
{$IFNDef semPediVda}
       DmPediVda.AtualizaUmItemPediVda(OriPart)
{$EndIf}
       ;
      end;
    end;
    IDCtrl_PreDef := IDCtrl;
    Result        := OriCtrl <> 0;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDmFatura.InsereRef_3001(FatGru, FatID, FatNum, Lancto: Integer; Valor:
  Double; Vcto: TDateTime): Integer;
var
  Vencto: String;
  Codigo, Controle, Conta: Integer;
begin
  Result := 0;
  Codigo         := FatGru;
  Controle       := FatNum;
  Vencto         := Geral.FDT(Vcto, 109);
  //
  Conta := UMyMod.BPGS1I32('lctfatref', 'Conta', '', '', tsPos, stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'lctfatref', False, [
  'Codigo', 'Controle', 'Lancto', 'FatID',
  'Valor', 'Vencto'], [
  'Conta'], [
  Codigo, Controle, Lancto, FatID,
  Valor, Vencto], [
  Conta], True) then
  begin
    Result := Conta;
  end;
end;

function TDmFatura.ObtemEntidadeDeFilial(Filial: Integer): Integer;
begin
  Result := DModG.ObtemEntidadeDeFilial(Filial);
end;

function TDmFatura.ObtemFilialDeEntidade(Entidade: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiCliInt, Dmod.MyDB, [
  'SELECT CodEnti, CodFilial ',
  'FROM enticliint ',
  'WHERE CodEnti=' + Geral.FF0(Entidade),
  '']);
  Result := QrEntiCliIntCodFilial.Value;
end;

function TDmFatura.ObtemNumeroCFOP(Tabela_FatPedFis: String; OriCodi, OriCnta,
  GraGruX, Empresa, Cliente, Item_MadeBy, RegrFiscal: Integer;
  EhServico, EhSubsTrib: Boolean; var CFOP: String; var ICMS_Aliq: Double; var CFOP_Contrib,
  CFOP_MesmaUF, CFOP_Proprio, CFOP_SubsTrib: Integer): Boolean;
begin
{$IfNDef SemNFe_0000}
  Result := DmNFe_0000.ObtemNumeroCFOP_Emissao(Tabela_FatPedFis, [OriCodi, OriCnta,
              GraGruX], Empresa, Cliente, Item_MadeBy, RegrFiscal, EhServico,
              EhSubsTrib, CFOP, ICMS_Aliq, CFOP_Contrib, CFOP_MesmaUF,
              CFOP_Proprio, CFOP_SubsTrib);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappNFe);
  //
  CFOP         := '';
  ICMS_Aliq    := 0;
  CFOP_Contrib := 0;
  CFOP_MesmaUF := 0;
  CFOP_Proprio := 0;
{$EndIf}
end;

procedure TDmFatura.ReopenContas(TipoFinanceiro: TTipoFinanceiro);
var
  SQLCompl: String;
begin
  case TipoFinanceiro of
    tfinNone:
      SQLCompl := '';
    tfinCred:
      SQLCompl := 'AND Credito="V"';
    tfinDeb:
      SQLCompl := 'AND Debito="V"';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrContas, Dmod.MyDB, [
    'SELECT Codigo, Nome',
    'FROM contas',
    'WHERE Codigo > 0',
    'AND Ativo = 1',
    SQLCompl,
    'ORDER BY Nome',
  '']);
end;

procedure TDmFatura.ReadPixelsPerInch(Reader: TReader);
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

procedure TDmFatura.ReopenCartEmis(EmprEnti: Integer; Todas: Boolean = False);
var
  SQLCompl: String;
begin
  if Todas = False then
    SQLCompl := 'AND  Tipo=2 '
  else
    SQLCompl := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartEmis, Dmod.MyDB, [
  'SELECT car.Codigo, car.Nome, car.Tipo ',
  'FROM carteiras car ',
  'WHERE Codigo<>0 ',
  Geral.ATS_IF(EmprEnti <> 0, ['AND ForneceI=' + Geral.FF0(EmprEnti)]),
  SQLCompl,
  'ORDER BY Nome ',
  '']);
end;

procedure TDmFatura.ReopenPediPrzCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzCab, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome, MaxDesco, ',
  'JurosMes, Parcelas, Percent1, Percent2, ',
  'MedDDSimpl ',
  'FROM pediprzcab ',
  //'WHERE Aplicacao & :P0 <> 0 ',
  'WHERE Aplicacao >= 0 ',
  'ORDER BY Nome ',
  '']);
end;

procedure TDmFatura.ReopenProd(GraGruX: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrProd, Dmod.MyDB, [
  'SELECT gg1.UsaSubsTrib',
  'FROM gragrux ggx',
  'LEFT JOIN gragru1 gg1 ON ggx.GraGru1=gg1.Nivel1',
  'WHERE ggx.Controle=' + Geral.FF0(GraGruX),
  '']);
end;

procedure TDmFatura.WritePixelsPerInch(Writer: TWriter);
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

end.
