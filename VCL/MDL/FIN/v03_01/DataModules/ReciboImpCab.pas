unit ReciboImpCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmReciboImpCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrReciboImpCab: TMySQLQuery;
    DsReciboImpCab: TDataSource;
    QrReciboImpOri: TMySQLQuery;
    DsReciboImpOri: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DBGOri: TDBGrid;
    QrReciboImpDst: TMySQLQuery;
    DsReciboImpDst: TDataSource;
    QrReciboImpCabCodigo: TIntegerField;
    QrReciboImpCabDtRecibo: TDateTimeField;
    QrReciboImpCabDtPrnted: TDateTimeField;
    QrReciboImpCabEmitente: TIntegerField;
    QrReciboImpCabBeneficiario: TIntegerField;
    QrReciboImpCabCNPJCPF: TWideStringField;
    QrReciboImpCabValor: TFloatField;
    QrReciboImpCabDtConfrmad: TDateTimeField;
    QrReciboImpOriCodigo: TIntegerField;
    QrReciboImpOriControle: TIntegerField;
    QrReciboImpOriLctCtrl: TIntegerField;
    QrReciboImpOriLctSub: TIntegerField;
    QrReciboImpDstCodigo: TIntegerField;
    QrReciboImpDstControle: TIntegerField;
    QrReciboImpDstLctCtrl: TIntegerField;
    QrReciboImpDstLctSub: TIntegerField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label10: TLabel;
    DBEdit6: TDBEdit;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    DBGDst: TDBGrid;
    Splitter1: TSplitter;
    QrReciboImpCabNO_EMITENTE: TWideStringField;
    QrReciboImpCabNO_BEFICIARIO: TWideStringField;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    QrReciboImpCabNome: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrReciboImpCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrReciboImpCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrReciboImpCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrReciboImpCabBeforeClose(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraReciboImpOri(SQLType: TSQLType; NewValor: Double);

  public
    { Public declarations }
    FTabLctA: String;
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    FNewCod: Integer;
    FValPag: Double;
    FModuleLctX: TDataModule;
    FCodigo: Integer;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure ReopenReciboImpOri(Controle: Integer);
    procedure ReopenReciboImpdst(Controle: Integer);
    procedure InserePagamento(ValorPago: Double);

  end;

var
  FmReciboImpCab: TFmReciboImpCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ReciboImpPag, ModuleLct2;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmReciboImpCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmReciboImpCab.MostraReciboImpOri(SQLType: TSQLType; NewValor: Double);
var
  Codigo: Integer;
begin
  Codigo := QrReciboImpCabCodigo.Value;
  //
  if DBCheck.CriaFm(TFmReciboImpPag, FmReciboImpPag, afmoNegarComAviso) then
  begin
    FmReciboImpPag.FTabLctA := FTabLctA;
    FmReciboImpPag.ImgTipo.SQLType := SQLType;
    FmReciboImpPag.FQrCab := QrReciboImpCab;
    FmReciboImpPag.FDsCab := DsReciboImpCab;
    FmReciboImpPag.FQrIts := QrReciboImpDst;
    FmReciboImpPag.FCliInt := -11;
    FmReciboImpPag.FModuleLctX := TDmLct2(FModuleLctX);
    FmReciboImpPag.EdCodigo.ValueVariant := Codigo;
    FmReciboImpPag.FCodigo               := Codigo;
    //
    if SQLType = stIns then
    begin
      FmReciboImpPag.EdValor.ValueVariant := NewValor;
      FmReciboImpPag.ShowModal;
      //
    end else
    begin
      Geral.MB_Info('Altera��o de pagamento n�o � poss�vel');
(*
      FmReciboImpPag.EdControle.ValueVariant := QrReciboImpDstControle.Value;
      //
      FmReciboImpPag.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrReciboImpPagCNPJ_CPF.Value);
      FmReciboImpPag.EdNomeEmiSac.Text := QrReciboImpPagNome.Value;
      FmReciboImpPag.EdCPF1.ReadOnly := True;
*)
    end;
    FmReciboImpPag.ShowModal;
    FmReciboImpPag.Destroy;
    //
    LocCod(FCodigo, Codigo);
  end;
end;

procedure TFmReciboImpCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrReciboImpCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrReciboImpCab, QrReciboImpOri);
end;

procedure TFmReciboImpCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrReciboImpCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrReciboImpOri);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrReciboImpOri);
end;

procedure TFmReciboImpCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrReciboImpCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmReciboImpCab.DefParams;
begin
  VAR_GOTOTABELA := 'reciboimpcab';
  VAR_GOTOMYSQLTABLE := QrReciboImpCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cab.*,');
  VAR_SQLx.Add('IF(emi.Tipo=0, ben.RazaoSocial, emi.Nome) NO_EMITENTE,');
  VAR_SQLx.Add('IF(ben.Tipo=0, ben.RazaoSocial, ben.Nome) NO_BEFICIARIO ');
  VAR_SQLx.Add('FROM reciboimpcab cab');
  VAR_SQLx.Add('LEFT JOIN entidades emi ON emi.Codigo=cab.Emitente');
  VAR_SQLx.Add('LEFT JOIN entidades ben ON ben.Codigo=cab.Beneficiario');
  VAR_SQLx.Add('WHERE cab.Codigo > 0');
  //
  VAR_SQL1.Add('AND cab.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cab.Nome Like :P0');
  //
end;

procedure TFmReciboImpCab.InserePagamento(ValorPago: Double);
begin
  FmReciboImpCab.MostraReciboImpOri(stIns, FValPag);
end;

procedure TFmReciboImpCab.ItsAltera1Click(Sender: TObject);
begin
  //MostraReciboImpOri(stUpd);
end;

procedure TFmReciboImpCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmReciboImpCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmReciboImpCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmReciboImpCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'ReciboImpOri', 'Controle', QrReciboImpOriControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrReciboImpOri,
      QrReciboImpOriControle, QrReciboImpOriControle.Value);
    ReopenReciboImpOri(Controle);
  end;
}
end;

procedure TFmReciboImpCab.ReopenReciboImpDst(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrReciboImpDst, Dmod.MyDB, [
  'SELECT * ',
  'FROM reciboimpdst ',
  'WHERE Codigo=' + Geral.FF0(QrReciboImpCabCodigo.Value),
  '']);
  //
  QrReciboImpDst.Locate('Controle', Controle, []);
end;

procedure TFmReciboImpCab.ReopenReciboImpOri(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrReciboImpOri, Dmod.MyDB, [
  'SELECT * ',
  'FROM reciboimpori ',
  'WHERE Codigo=' + Geral.FF0(QrReciboImpCabCodigo.Value),
  '']);
  //
  QrReciboImpOri.Locate('Controle', Controle, []);
end;


procedure TFmReciboImpCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmReciboImpCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmReciboImpCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmReciboImpCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmReciboImpCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmReciboImpCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReciboImpCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrReciboImpCabCodigo.Value;
  Close;
end;

procedure TFmReciboImpCab.ItsInclui1Click(Sender: TObject);
begin
  MostraReciboImpOri(stIns, FValPag);
end;

procedure TFmReciboImpCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrReciboImpCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'reciboimpcab');
end;

procedure TFmReciboImpCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
{
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  ?Codigo := UMyMod.BuscaEmLivreY_Def('reciboimpcab', 'Codigo', ImgTipo.SQLType,
    QrReciboImpCabCodigo.Value);
  ou > ?Codigo := UMyMod.BPGS1I32('reciboimpcab', 'Codigo', '', '',
    tsPosNeg?, ImgTipo.SQLType, QrReciboImpCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'reciboimpcab',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
(*  Desmarcar se usar "UMyMod.SQLInsUpd(...)" em vez de "UMyMod.ExecSQLInsUpdPanel(...)"
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
*)
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
}
end;

procedure TFmReciboImpCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'reciboimpcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'reciboimpcab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmReciboImpCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmReciboImpCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmReciboImpCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DBGDst.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  FTabLctA    := '';
  FCabIni     := 0;
  //FLocIni     := False; ?????
  FNewCod     := 0;
  FValPag     := 0.00;
  FModuleLctX := nil; //TDataModule;
  FCodigo     := 0;
end;

procedure TFmReciboImpCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrReciboImpCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmReciboImpCab.SbImprimeClick(Sender: TObject);
const
  ValorP = 0;
  ValorE = 0;
  ReferenteP = '';
  ReferenteE = '';
begin
  GOTOy.EmiteRecibo(QrReciboImpCabCodigo.Value, QrReciboImpCabEmitente.Value,
  QrReciboImpCabBeneficiario.Value,
  QrReciboImpCabValor.Value, ValorP, ValorE, 'RCI#' +  // Recibo com controle de impress�es
  Geral.FF0(QrReciboImpCabCodigo.Value),
  QrReciboImpCabNome.Value, ReferenteP, ReferenteE,
  QrReciboImpCabDtRecibo.Value,
  (*Sit*)4);
end;

procedure TFmReciboImpCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmReciboImpCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrReciboImpCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmReciboImpCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmReciboImpCab.QrReciboImpCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmReciboImpCab.QrReciboImpCabAfterScroll(DataSet: TDataSet);
begin
  ReopenReciboImpOri(0);
  ReopenReciboImpDst(0);
end;

procedure TFmReciboImpCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrReciboImpCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmReciboImpCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrReciboImpCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'reciboimpcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmReciboImpCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmReciboImpCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrReciboImpCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'reciboimpcab');
end;

procedure TFmReciboImpCab.QrReciboImpCabBeforeClose(
  DataSet: TDataSet);
begin
  QrReciboImpOri.Close;
end;

procedure TFmReciboImpCab.QrReciboImpCabBeforeOpen(DataSet: TDataSet);
begin
  QrReciboImpCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

