object FmReciboImpCab: TFmReciboImpCab
  Left = 368
  Top = 194
  Caption = 'FIN-RECIB-001 :: Recibo'
  ClientHeight = 580
  ClientWidth = 624
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 624
    Height = 484
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 624
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 421
      Width = 624
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 484
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 624
    Height = 484
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 105
      Top = 181
      Height = 239
      ExplicitLeft = 0
      ExplicitTop = 340
      ExplicitHeight = 1126
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 624
      Height = 181
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label3: TLabel
        Left = 76
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Data recibo:'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 192
        Top = 16
        Width = 76
        Height = 13
        Caption = 'Data impress'#227'o:'
        FocusControl = DBEdit2
      end
      object Label5: TLabel
        Left = 16
        Top = 96
        Width = 44
        Height = 13
        Caption = 'Emitente:'
        FocusControl = DBEdit3
      end
      object Label6: TLabel
        Left = 16
        Top = 56
        Width = 55
        Height = 13
        Caption = 'Beneficiario'
        FocusControl = DBEdit4
      end
      object Label8: TLabel
        Left = 16
        Top = 136
        Width = 61
        Height = 13
        Caption = 'CNPJ / CPF:'
        FocusControl = DBEdit5
      end
      object Label10: TLabel
        Left = 132
        Top = 136
        Width = 27
        Height = 13
        Caption = 'Valor:'
        FocusControl = DBEdit6
      end
      object Label11: TLabel
        Left = 236
        Top = 136
        Width = 75
        Height = 13
        Caption = 'Dt confirma'#231#227'o:'
        FocusControl = DBEdit7
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsReciboImpCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtRecibo'
        DataSource = DsReciboImpCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 192
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DtPrnted'
        DataSource = DsReciboImpCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 16
        Top = 112
        Width = 52
        Height = 21
        DataField = 'Emitente'
        DataSource = DsReciboImpCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 16
        Top = 72
        Width = 52
        Height = 21
        DataField = 'Beneficiario'
        DataSource = DsReciboImpCab
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 16
        Top = 152
        Width = 112
        Height = 21
        DataField = 'CNPJCPF'
        DataSource = DsReciboImpCab
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 132
        Top = 152
        Width = 100
        Height = 21
        DataField = 'Valor'
        DataSource = DsReciboImpCab
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 236
        Top = 152
        Width = 112
        Height = 21
        DataField = 'DtConfrmad'
        DataSource = DsReciboImpCab
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 68
        Top = 112
        Width = 449
        Height = 21
        DataField = 'NO_EMITENTE'
        DataSource = DsReciboImpCab
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 72
        Top = 72
        Width = 400
        Height = 21
        DataField = 'NO_BEFICIARIO'
        DataSource = DsReciboImpCab
        TabOrder = 9
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 420
      Width = 624
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 242
        Top = 15
        Width = 380
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 247
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&???'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&???'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtItsClick
        end
      end
    end
    object DBGOri: TDBGrid
      Left = 0
      Top = 181
      Width = 105
      Height = 239
      Align = alLeft
      DataSource = DsReciboImpOri
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'LctCtrl'
          Title.Caption = 'Origem'
          Visible = True
        end>
    end
    object DBGDst: TDBGrid
      Left = 108
      Top = 181
      Width = 516
      Height = 239
      Align = alClient
      DataSource = DsReciboImpDst
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'LctCtrl'
          Title.Caption = 'Lan'#231'amento'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 360
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 187
        Height = 32
        Caption = 'Recibo M'#250'ltiplo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 187
        Height = 32
        Caption = 'Recibo M'#250'ltiplo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 187
        Height = 32
        Caption = 'Recibo M'#250'ltiplo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 624
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrReciboImpCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrReciboImpCabBeforeOpen
    AfterOpen = QrReciboImpCabAfterOpen
    BeforeClose = QrReciboImpCabBeforeClose
    AfterScroll = QrReciboImpCabAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM reciboimpcab')
    Left = 96
    Top = 229
    object QrReciboImpCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrReciboImpCabDtRecibo: TDateTimeField
      FieldName = 'DtRecibo'
      Required = True
    end
    object QrReciboImpCabDtPrnted: TDateTimeField
      FieldName = 'DtPrnted'
      Required = True
    end
    object QrReciboImpCabEmitente: TIntegerField
      FieldName = 'Emitente'
      Required = True
    end
    object QrReciboImpCabBeneficiario: TIntegerField
      FieldName = 'Beneficiario'
      Required = True
    end
    object QrReciboImpCabCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
    end
    object QrReciboImpCabValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrReciboImpCabDtConfrmad: TDateTimeField
      FieldName = 'DtConfrmad'
      Required = True
    end
    object QrReciboImpCabNO_EMITENTE: TWideStringField
      FieldName = 'NO_EMITENTE'
      Size = 100
    end
    object QrReciboImpCabNO_BEFICIARIO: TWideStringField
      FieldName = 'NO_BEFICIARIO'
      Size = 100
    end
    object QrReciboImpCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 511
    end
  end
  object DsReciboImpCab: TDataSource
    DataSet = QrReciboImpCab
    Left = 92
    Top = 277
  end
  object QrReciboImpOri: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM reciboimpori')
    Left = 188
    Top = 237
    object QrReciboImpOriCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrReciboImpOriControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrReciboImpOriLctCtrl: TIntegerField
      FieldName = 'LctCtrl'
      Required = True
    end
    object QrReciboImpOriLctSub: TIntegerField
      FieldName = 'LctSub'
      Required = True
    end
  end
  object DsReciboImpOri: TDataSource
    DataSet = QrReciboImpOri
    Left = 188
    Top = 281
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrReciboImpDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.*,'
      'IF(emi.Tipo=0, ben.RazaoSocial, emi.Nome) NO_EMITENTE,'
      'IF(ben.Tipo=0, ben.RazaoSocial, ben.Nome) NO_BEFICIARIO '
      'FROM reciboimpcab cab'
      'LEFT JOIN entidades emi ON emi.Codigo=cab.Emitente'
      'LEFT JOIN entidades ben ON ben.Codigo=cab.Beneficiario')
    Left = 304
    Top = 241
    object QrReciboImpDstCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrReciboImpDstControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrReciboImpDstLctCtrl: TIntegerField
      FieldName = 'LctCtrl'
      Required = True
    end
    object QrReciboImpDstLctSub: TIntegerField
      FieldName = 'LctSub'
      Required = True
    end
  end
  object DsReciboImpDst: TDataSource
    DataSet = QrReciboImpDst
    Left = 304
    Top = 285
  end
end
