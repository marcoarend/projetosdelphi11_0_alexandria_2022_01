unit ModuleLct0;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  mySQLDbTables, dmkGeral, frxClass, frxDBSet, dmkEditDateTimePicker,
  Variants, UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TDmLct0 = class(TDataModule)
    DsCrt: TDataSource;
    DsLct: TDataSource;
    DsCrtSum: TDataSource;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctLk: TIntegerField;
    QrLctFatID: TIntegerField;
    QrLctFatParcela: TIntegerField;
    QrLctCONTA: TIntegerField;
    QrLctNOMECONTA: TWideStringField;
    QrLctNOMEEMPRESA: TWideStringField;
    QrLctNOMESUBGRUPO: TWideStringField;
    QrLctNOMEGRUPO: TWideStringField;
    QrLctNOMECONJUNTO: TWideStringField;
    QrLctNOMESIT: TWideStringField;
    QrLctAno: TFloatField;
    QrLctMENSAL: TWideStringField;
    QrLctMENSAL2: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctLocal: TIntegerField;
    QrLctFatura: TWideStringField;
    QrLctSub: TSmallintField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctPago: TFloatField;
    QrLctSALDO: TFloatField;
    QrLctID_Sub: TSmallintField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctcliente: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctNOMECLIENTE: TWideStringField;
    QrLctNOMEFORNECEDOR: TWideStringField;
    QrLctTIPOEM: TWideStringField;
    QrLctNOMERELACIONADO: TWideStringField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctMulta: TFloatField;
    QrLctATRASO: TFloatField;
    QrLctJUROS: TFloatField;
    QrLctDataDoc: TDateField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctMes2: TLargeintField;
    QrLctProtesto: TDateField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TSmallintField;
    QrLctUserAlt: TSmallintField;
    QrLctControle: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctCtrlIni: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctCOMPENSADO_TXT: TWideStringField;
    QrLctCliInt: TIntegerField;
    QrLctDepto: TIntegerField;
    QrLctDescoPor: TIntegerField;
    QrLctPrazo: TSmallintField;
    QrLctForneceI: TIntegerField;
    QrLctQtde: TFloatField;
    QrLctFatNum: TFloatField;
    QrLctEmitente: TWideStringField;
    QrLctContaCorrente: TWideStringField;
    QrLctCNPJCPF: TWideStringField;
    QrLctDescoVal: TFloatField;
    QrLctDescoControle: TIntegerField;
    QrLctNFVal: TFloatField;
    QrLctAntigo: TWideStringField;
    QrLctUnidade: TIntegerField;
    QrLctExcelGru: TIntegerField;
    QrLctSerieCH: TWideStringField;
    QrLctMoraVal: TFloatField;
    QrLctMultaVal: TFloatField;
    QrLctDoc2: TWideStringField;
    QrLctCNAB_Sit: TSmallintField;
    QrLctTipoCH: TSmallintField;
    QrLctID_Quit: TIntegerField;
    QrLctReparcel: TIntegerField;
    QrLctAtrelado: TIntegerField;
    QrLctPagMul: TFloatField;
    QrLctPagJur: TFloatField;
    QrLctSubPgto1: TIntegerField;
    QrLctMultiPgto: TIntegerField;
    QrLctProtocolo: TIntegerField;
    QrLctCtrlQuitPg: TIntegerField;
    QrLctAlterWeb: TSmallintField;
    QrLctAtivo: TSmallintField;
    QrLctSerieNF: TWideStringField;
    QrLctEndossas: TSmallintField;
    QrLctEndossan: TFloatField;
    QrLctEndossad: TFloatField;
    QrLctCancelado: TSmallintField;
    QrLctEventosCad: TIntegerField;
    QrCrt: TmySQLQuery;
    QrCrtDIFERENCA: TFloatField;
    QrCrtTIPOPRAZO: TWideStringField;
    QrCrtNOMEPAGREC: TWideStringField;
    QrCrtCodigo: TIntegerField;
    QrCrtNome: TWideStringField;
    QrCrtTipo: TIntegerField;
    QrCrtInicial: TFloatField;
    QrCrtBanco: TIntegerField;
    QrCrtID: TWideStringField;
    QrCrtFatura: TWideStringField;
    QrCrtID_Fat: TWideStringField;
    QrCrtSaldo: TFloatField;
    QrCrtLk: TIntegerField;
    QrCrtEmCaixa: TFloatField;
    QrCrtFechamento: TIntegerField;
    QrCrtPrazo: TSmallintField;
    QrCrtDataCad: TDateField;
    QrCrtDataAlt: TDateField;
    QrCrtUserCad: TIntegerField;
    QrCrtUserAlt: TIntegerField;
    QrCrtPagRec: TIntegerField;
    QrCrtDiaMesVence: TSmallintField;
    QrCrtExigeNumCheque: TSmallintField;
    QrCrtForneceI: TIntegerField;
    QrCrtNome2: TWideStringField;
    QrCrtTipoDoc: TSmallintField;
    QrCrtBanco1: TIntegerField;
    QrCrtAgencia1: TIntegerField;
    QrCrtConta1: TWideStringField;
    QrCrtCheque1: TIntegerField;
    QrCrtContato1: TWideStringField;
    QrCrtAntigo: TWideStringField;
    QrCrtContab: TWideStringField;
    QrCrtOrdem: TIntegerField;
    QrCrtFuturoC: TFloatField;
    QrCrtFuturoD: TFloatField;
    QrCrtFuturoS: TFloatField;
    QrCrtForneceN: TSmallintField;
    QrCrtExclusivo: TSmallintField;
    QrCrtRecebeBloq: TSmallintField;
    QrCrtEntiDent: TIntegerField;
    QrCrtCodCedente: TWideStringField;
    QrCrtAlterWeb: TSmallintField;
    QrCrtAtivo: TSmallintField;
    QrCrtValMorto: TFloatField;
    QrCrtNOMEDOBANCO: TWideStringField;
    QrCrtNOMEFORNECEI: TWideStringField;
    QrCrtSum: TmySQLQuery;
    QrCrtSumSALDO: TFloatField;
    QrCrtSumFuturoC: TFloatField;
    QrCrtSumFuturoD: TFloatField;
    QrCrtSumFuturoS: TFloatField;
    QrCrtSumEmCaixa: TFloatField;
    QrCrtSumDifere: TFloatField;
    QrCrtSumSDO_FUT: TFloatField;
    QrCrtCliInt: TIntegerField;
    QrCrtIgnorSerie: TSmallintField;
    QrCrtValIniOld: TFloatField;
    QrCrtSdoFimB: TFloatField;
    QrLctUH: TWideStringField;
    QrLctEncerrado: TIntegerField;
    QrLctErrCtrl: TIntegerField;
    QrLctNO_Carteira: TWideStringField;
    QrLctBanco1: TIntegerField;
    QrLctAgencia1: TIntegerField;
    QrLctConta1: TWideStringField;
    QrLctNOMEFORNECEI: TWideStringField;
    QrLctNO_ENDOSSADO: TWideStringField;
    QrLctSERIE_CHEQUE: TWideStringField;
    QrLctIndiPag: TIntegerField;
    QrLctAgencia: TIntegerField;
    QrLcP: TmySQLQuery;
    DsLcP: TDataSource;
    QrLcI: TmySQLQuery;
    DsQrLcI: TDataSource;
    QrLcPData: TDateField;
    QrLcPTipo: TSmallintField;
    QrLcPCarteira: TIntegerField;
    QrLcPControle: TLargeintField;
    QrLcPSub: TSmallintField;
    QrLcPCliInt: TIntegerField;
    QrLcPCliente: TIntegerField;
    QrLcPFornecedor: TIntegerField;
    QrLcPDocumento: TFloatField;
    QrLcPCredito: TFloatField;
    QrLcPDebito: TFloatField;
    QrLcPSerieCH: TWideStringField;
    QrLcPVencimento: TDateField;
    QrLcPITENS: TLargeintField;
    QrLcILancto: TLargeintField;
    QrLctFisicoSrc: TSmallintField;
    QrLctFisicoCod: TIntegerField;
    QrLctUSERCAD_TXT: TWideStringField;
    QrLctUSERALT_TXT: TWideStringField;
    QrLctDATACAD_TXT: TWideStringField;
    QrLctDATAALT_TXT: TWideStringField;
    procedure QrLctBeforeClose(DataSet: TDataSet);
    procedure QrLctCalcFields(DataSet: TDataSet);
    procedure QrCrtAfterClose(DataSet: TDataSet);
    procedure QrCrtAfterScroll(DataSet: TDataSet);
    procedure QrCrtBeforeClose(DataSet: TDataSet);
    procedure QrCrtCalcFields(DataSet: TDataSet);
    procedure QrLctAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FTabLctA: String;
    FTipoData: Integer;
    FBtConcilia1: TBitBtn;
    FTPDataIni, FTPDataFim: TdmkEditDateTimePicker;
    //
    //procedure DefineVarsEmpresa(Empresa: Integer);
    //procedure EncerrraMes();
    procedure FechaQueries();
    procedure GerenciaEmpresa();
    procedure MigraLctsParaTabLct();
    procedure MostraFmLctGer1();
    {
    function  SelecionaEmpresa(SelectLetraLct: TSelectLetraLct;
              FinanceiroNovo: Boolean = True): Boolean;
    }
    // Crt e Lct
    //function  AtualizaPagamentosAVista(TabLct: String; QrLct: TmySQLQuery): Boolean;  J� tem no UnFinanceiro!
    function  DefParams(QrCrt, QrCrtSum: TmySQLQuery;
              AvisaErro: Boolean; Aviso: String): Boolean;
    function  LocCod(Atual, Codigo: Integer; QrCrt: TmySQLQuery;
              (*CliInt: Integer; *)Aviso: String): Boolean;
    function  LocalizaLancamento(Carteira, Lancamento: Integer;
              QrCrt, QrLct: TmySQLQuery; FormDefs, FormLocLct: TForm): Boolean;
    procedure LocalizaUltimoLanctoDia(Data: TDateTime; Carteira: Integer);
    procedure ReabreCarteiras(LocCart: Integer;
              QrCrt, QrCrtSum: TmySQLQuery; Aviso: String);
    procedure ReabreSoLct(QrLct: TmySQLQuery; ItemCarteira, Controle, Sub:
              Integer; TPDataIni, TPDataFim: TdmkEditDateTimePicker; Apto:
              Variant);
    procedure VeSeReabreLct(TPDataIni, TPDataFim: TdmkEditDateTimePicker;
              Apto: Variant; Controle, Sub: Integer; QrCrt, QrLct: TmySQLQuery;
              ForcaAbertura: Boolean = False);
    //function  DataUltimoLct(TabLctA: String): TDateTime;
    // Fim CXtr e Lct

  end;

var
  DmLct0: TDmLct0;

implementation

uses UnInternalConsts, ModuleGeral, MyDBCheck, EmpresaSel, Principal, LctGer1,
UMySQLModule, Module, UnMLAGeral, UnFinanceiro, ModuleFin, MyListas;


{$R *.dfm}

{
procedure TDmLct0.DefineVarsEmpresa(Empresa: Integer);
begin
  DmodG.QrCliIntLog.Close;
  DmodG.QrCliIntLog.Params[0].AsInteger := Empresa;
  DmodG.QrCliIntLog.Open;
  //
  DModG.FEntidade := DmodG.QrCliIntUniCodigo.Value;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntLogCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  //
end;
}

{ J� tem no UnFinanceiro
function TDmLct0.AtualizaPagamentosAVista(TabLct: String; QrLct: TmySQLQuery): Boolean;
var
  Controle: Integer;
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + TabLct + ' SET Compensado = Data ');
  Dmod.QrUpd.SQL.Add('WHERE Tipo < 2 AND Compensado < 2');
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + TabLct + ' SET Sit = 3 ');
  Dmod.QrUpd.SQL.Add('WHERE Tipo < 2 AND Sit < 2');
  Dmod.QrUpd.ExecSQL;
  Result := True;
  //
  if (QrLct <> nil) and (QrLct.State <> dsInactive) then
  begin
    Controle := QrLct.FieldByName('Controle').AsInteger;
    QrLct.Close;
    QrLct.Open;
    if Controle > 0 then
      QrLct.Locate('Controle', Controle, []);
  end;
  Screen.Cursor := MyCursor;
end;
}

function TDmLct0.DefParams(QrCrt, QrCrtSum: TmySQLQuery; AvisaErro: Boolean;
  Aviso: String): Boolean;
var
  Carteira: Integer;
begin
  if QrCrt <> nil then
  begin
    if QrCrt.State <> dsBrowse then
      Carteira := 0
    else begin
      if QrCrt.RecordCount = 0 then Carteira := 0
      else
        Carteira := QrCrt.FieldByName('Codigo').AsInteger;
    end;
    ReabreCarteiras(Carteira, QrCrt, QrCrtSum, Aviso +
      ' > TDmLct0.DefParams()');
    Result := True;
  end else begin
    Result := False;
    if AvisaErro then
      Geral.MensagemBox(
      '"QrCrt" n�o definido na procedure "DefParams"', 'ERRO',
      MB_OK+MB_ICONERROR);
  end;
end;

procedure TDmLct0.FechaQueries();
var
  I: Integer;
  Compo: TComponent;
  Query: TmySQLQuery;
begin
  FTabLctA := '';
  for I := 0 to DmLct0.ComponentCount - 1 do
  begin
    Compo := DmLct0.Components[I];
    if Compo is TmySQLQuery then
    begin
      Query := TmySQLQuery(Compo);
      Query.Close;
      Query.SortFieldNames := '';
    end;
  end;
end;

procedure TDmLct0.GerenciaEmpresa();
begin
  FechaQueries();
  if DModG.SelecionaEmpresa(sllLivre) then
    MostraFmLctGer1();
end;

function TDmLct0.LocalizaLancamento(Carteira, Lancamento: Integer;
QrCrt, QrLct: TmySQLQuery; FormDefs, FormLocLct: TForm): Boolean;
  function Localiza(Lancamento: Double; QrLct: TmySQLQuery;
  FormDefs, FormLocLct: TForm): Boolean;
  begin
    if QrLct.State = dsInactive then
      QrLct.Open;
    if QrLct.Locate('Controle', Lancamento, []) then
    begin
      if FormDefs <> nil then
        FormDefs.Close;
      Screen.Cursor := crDefault;
      if FormLocLct <> nil then
        FormLocLct.Close;
      //
      Result := True
    end else Result := False;
  end;
begin
  Result := False;
  if DefParams(QrCrt, nil, True, 'FmLocLancto.BtLocaliza1Click()') then
  begin
    if not LocCod(Carteira, Carteira, QrCrt, 'TDmLct0.LocalizaLancamento()') then
      Geral.MensagemBox('N�o foi poss�vel localizar a carteira ' +
      IntToStr(Carteira) + '!' + sLineBreak +
      'Verifique se esta carteira pertence realmente a empresa ' +
      VAR_LIB_EMPRESAS + '!', 'Aviso', MB_OK+MB_ICONWARNING)
    else begin
      if not Localiza(Lancamento, QrLct, FormDefs, FormLocLct) then
      begin
        if not Localiza(Lancamento, QrLct, FormDefs, FormLocLct) then
          Geral.MensagemBox('N�o foi poss�vel localizar o ' +
          'lan�amento ' + FormatFloat('0', Lancamento) + '!', 'Aviso',
        MB_OK+MB_ICONWARNING)
      end else Result := True;
    end;
  end;
end;

procedure TDmLct0.LocalizaUltimoLanctoDia(Data: TDateTime; Carteira: Integer);
var
  Controle: Integer;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT Max(Controle) Controle');
  Dmod.QrAux.SQL.Add('FROM ' + FTabLctA + '');
  Dmod.QrAux.SQL.Add('WHERE Carteira=:P0');
  Dmod.QrAux.SQL.Add('AND Data=:P1');
  Dmod.QrAux.Params[00].AsInteger := Carteira;
  Dmod.QrAux.Params[01].AsString := Geral.FDT(Data, 1);
  Dmod.QrAux.Open;
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Controle := Dmod.QrAux.FieldByName('Controle').AsInteger;
    if not QrLct.Locate('Controle', Controle, []) then
      Geral.MensagemBox('O lan�amento n�mero ' + IntToStr(Controle) +
      ' n�o pode ser localizado sem pesquisa!', 'Aviso', MB_OK+MB_ICONWARNING);
  end else begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Max(Controle) Controle');
    Dmod.QrAux.SQL.Add('FROM ' + FTabLcta + '');
    Dmod.QrAux.SQL.Add('WHERE Carteira=:P0');
    Dmod.QrAux.SQL.Add('AND Data<:P1');
    Dmod.QrAux.Params[00].AsInteger := Carteira;
    Dmod.QrAux.Params[01].AsString := Geral.FDT(Data, 1);
    Dmod.QrAux.Open;
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Controle := Dmod.QrAux.FieldByName('Controle').AsInteger;
      if not QrLct.Locate('Controle', Controle, []) then
        Geral.MensagemBox('O lan�amento n�mero ' + IntToStr(Controle) +
        ' n�o pode ser localizado sem pesquisa!', 'Aviso', MB_OK+MB_ICONWARNING);
    end else
      Geral.MensagemBox('A pesquisa n�o localizou nenhum lan�amento!',
      'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

function TDmLct0.LocCod(Atual, Codigo: Integer; QrCrt: TmySQLQuery;
  Aviso: String): Boolean;
begin
  Result := False;
  DefParams(QrCrt, nil, True, Aviso + ' > TDmodFin.LocCod(');
  if not QrCrt.Locate('Codigo', Codigo, []) then
    QrCrt.Locate('Codigo', Atual, [])
  else Result := True;
end;

procedure TDmLct0.MigraLctsParaTabLct();
const
  Antiga = LAN_CTOS;
var
  A1, A2, B1, B2, A3, B3, Entidade, CliInt: Integer;
  Campos, TabLctA: String;
  Continua: Boolean;
begin
  if DModG.SelecionaEmpresa(sllForcaA, False) then
  begin
    Entidade := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
    CliInt   := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
    TabLctA  := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
    //
    ShowMessage('Entidade: ' + IntToStr(Entidade));
    ShowMessage('CliInt: ' + IntToStr(CliInt));
    //verificar saldos de contas e carteiras!
    if DmodFin.EntidadeHabilitadadaParaFinanceiroNovo(Entidade, True, False) <> sfnFaltaMigrar then Exit;
    //
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT COUNT(Controle) Itens FROM ' + Antiga);
    Dmod.QrAux.SQL.Add('WHERE Controle <> 0 AND CliInt=:P0 ');
    Dmod.QrAux.Params[0].AsInteger := Entidade;
    Dmod.QrAux.Open;
    A1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
    //
    DModG.ReopenEndereco(Entidade);
    if A1 = 0 then
    begin
      Geral.MensagemBox('N�o h� lan�amentos a serem migrados para o cliente "' +
      DModG.QrEnderecoNOME_ENT.Value + '"!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if Geral.MensagemBox('Confirma a migra��o dos ' + IntToStr(A1) +
    ' lan�amentos da tabela "' + Antiga + '" para a tabela "' + TabLctA + '"?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      //
      try
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('LOCK TABLES ' + TabLctA + ' WRITE, ' + Antiga + ' WRITE, master WRITE');
        Dmod.QrUpd.ExecSQL;
        //
        try
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Controle) Itens FROM ' + TabLctA);
          Dmod.QrAux.SQL.Add('WHERE Controle <> 0 AND CliInt=:P0 ');
          Dmod.QrAux.Params[0].AsInteger := Entidade;
          Dmod.QrAux.Open;
          B1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabLctA, '');
          //
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO ' + TabLctA);
          Dmod.QrUpd.SQL.Add('SELECT ' + Campos);
          Dmod.QrUpd.SQL.Add('FROM ' + Antiga);
          Dmod.QrUpd.SQL.Add('WHERE Controle <> 0 AND CliInt = ' + FormatFloat('0', Entidade));
          Dmod.QrUpd.ExecSQL;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Controle) Itens FROM ' + TabLctA);
          Dmod.QrAux.SQL.Add('WHERE Controle <> 0 AND CliInt=:P0 ');
          Dmod.QrAux.Params[0].AsInteger := Entidade;
          Dmod.QrAux.Open;
          B2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          B3 := B2 - B1;
          if B3 > 0 then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add(DELETE_FROM + Antiga);
            Dmod.QrUpd.SQL.Add('WHERE Controle <> 0 AND CliInt = ' + FormatFloat('0', Entidade));
            Dmod.QrUpd.ExecSQL;
            //
            // Parei aqui!
            //fazer update na tabela carteira (entidade?) que esta foi migrada
          end;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Controle) Itens FROM ' + Antiga);
          Dmod.QrAux.SQL.Add('WHERE Controle <> 0 AND CliInt=:P0 ');
          Dmod.QrAux.Params[0].AsInteger := Entidade;
          Dmod.QrAux.Open;
          A2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
        finally
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UNLOCK TABLES');
          Dmod.QrUpd.ExecSQL;
        end;
        A3 := A1 - A2;
        if A3 <> B3 then
        begin
          Geral.MensagemBox('Ocorreu um erro na migra��o:' + sLineBreak +
          'Lan�amentos na tabela "' + Antiga + '" antes da migra��o = ' + IntToStr(A1) + sLineBreak +
          'Lan�amentos na tabela "' + TabLctA + '" antes da migra��o = ' + IntToStr(B1) + sLineBreak +
          'Lan�amentos na tabela "' + Antiga + '" depois da migra��o = ' + IntToStr(A2) + sLineBreak +
          'Lan�amentos na tabela "' + TabLctA + '" Depois da migra��o = ' + IntToStr(B2) + sLineBreak +
          'Diferen�a na migra��o: ' + IntToStr(B3 - A3) + ' lan�amentos!', 'ERRO', MB_OK+MB_ICONERROR);
        end else
        begin
          {$IFDEF MIGRAOUTROS}
          Continua := FmPrincipal.MigraEspecificos(CliInt);
          {$ELSE}
          Continua := True;
          {$ENDIF}
          if Continua then
          begin
            // Cadastra que a empresa usar� o novo financeiro e n�o mais o antigo
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'enticliint', False, [
            'TipoTabLct'], ['CodEnti'], [1], [Entidade], True);
            //
            Geral.MensagemBox('Migra��o realizada com sucesso.' + sLineBreak +
            'Foram migrados ' + IntToStr(B2) + ' lan�amentos!', 'Aviso',
            MB_OK+MB_ICONINFORMATION);
          end else
            Geral.MensagemBox('ERRO durante a migra��o!' + sLineBreak +
            'Foram migrados ' + IntToStr(B2) + ' lan�amentos!', 'ERRO',
            MB_OK+MB_ICONERROR);
        end;
        Screen.Cursor := crDefault;
     finally
        Screen.Cursor := crDefault;
        //Geral.MensagemBox('ERRO ao migrar lan�amentos!', 'Erro', MB_OK+MB_ICONERROR);
      end;
    end;
  end;
end;

procedure TDmLct0.MostraFmLctGer1();
var
  Entidade, CliInt: Integer;
const
  LocCart = 0;
{
var
  I: Integer;
}
begin
  Screen.Cursor := crHourGlass;
  {
  if not CondAnterior or not FJaAbriuFmLctGer1 then
  begin
    for I := 0 to FmLctGer1.ComponentCount - 1 do
    begin
      if FmLctGer1.Components[I] is TmySQLQuery then
        TmySQLQuery(FmLctGer1.Components[I]).Close;
    end;
    for I := 0 to DCond.ComponentCount - 1 do
    begin
      if DCond.Components[I] is TmySQLQuery then
        TmySQLQuery(DCond.Components[I]).Close;
    end;
    DCond.ReopenQrCond(FCliInt_);
    if Periodo > 0 then
    begin
      FmLctGer1.LocPeriodo(Periodo, Periodo);
      FmLctGer1.PageControl1.ActivePageIndex := 3;
      //F_EntInt := DCond.QrCondCliente.Value;
    end;
    QrCarteiras.Close;
    QrLct.Close;
  end;
  FmLctGer1.QrPropriet.Close;
  FmLctGer1.QrPropriet.Open;
  FmLctGer1.QrBancos.Close;
  FmLctGer1.QrBancos.Open;
  }
  Screen.Cursor := crDefault;
  Entidade := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
  CliInt   := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
  DModG.DefineDataMinima(Entidade);
  if DBCheck.CriaFm(TFmLctGer1, FmLctGer1, afmoNegarComAviso) then
  begin
    DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, FmLctGer1.FDtEncer, FmLctGer1.FDtMorto,
        FmLctGer1.FTabLctA, FmLctGer1.FTabLctB, FmLctGer1.FTabLctD);
    FmLctGer1.TPDataFim.Date := DModFin.DataUltimoLct(FmLctGer1.FTabLctA);
    FTPDataIni := FmLctGer1.TPDataIni;
    FTPDataFim := FmLctGer1.TPDataFim;
    ReabreCarteiras(LocCart, QrCrt, QrCrtSum, 'TDmLct0.MostraFmLctGer1()');
    FmLctGer1.ShowModal;
    //
    FmLctGer1.Destroy;
  end;
{
  Application.OnHint := ShowHint;
}
end;

procedure TDmLct0.QrCrtAfterClose(DataSet: TDataSet);
begin
  QrLct.Close;
end;

procedure TDmLct0.QrCrtAfterScroll(DataSet: TDataSet);
var
  Controle, Sub: Integer;
begin
  Controle := 0;
  Sub := 1;
  if QrLct.State = dsBrowse then
  begin
    Controle := QrLctControle.Value;
    Sub := QrLctSub.Value;
  end;
  ReabreSoLct(QrLct, QrCrtCodigo.Value, Controle, Sub, FTPDataIni, FTPDataFim, Null);

  if (FindWindow('TFmLctGer1', nil) > 0) and (CO_DMKID_APP <> 4) then  //Syndi2
    FmLctGer1.DmLct0_QrCrtAfterScroll();

{###
var
  Mostra: Boolean;
begin
  ReabreSoLct(QrCarteirasTipo.Value, QrCarteirasCodigo.Value, -1, 0);

  Mostra := QrCarteirasTipo.Value in ([0,1]);
  if Mostra then EdNome.Width := 181 else EdNome.Width := 593;
  EdSaldo.Visible := Mostra;
  LaSaldo.Visible := Mostra;
  EdDiferenca.Visible := Mostra;
  LaDiferenca.Visible := Mostra;
  EdCaixa.Visible := Mostra;
  LaCaixa.Visible := Mostra;
  BtPagtoDuvida.Visible := Mostra;
  BtContarDinheiro.Visible := Mostra;
  EdSdoAqui.Visible := Mostra;
  //
  BtConcilia.Enabled := QrCarteirasTipo.Value = 1;
}
end;

procedure TDmLct0.QrCrtBeforeClose(DataSet: TDataSet);
begin
  if FBtConcilia1 <> nil then
    FBtConcilia1.Enabled := False;
end;

procedure TDmLct0.QrCrtCalcFields(DataSet: TDataSet);
begin
  QrCrtDIFERENCA.Value :=
    QrCrtEmCaixa.Value - QrCrtSaldo.Value;
  case QrCrtPrazo.Value of
    0: QrCrtTIPOPRAZO.Value := 'V';
    1: QrCrtTIPOPRAZO.Value := 'F';
  end;
  case QrCrtPagRec.Value of
   -1: QrCrtNOMEPAGREC.Value := 'CONTAS A PAGAR';
    0: QrCrtNOMEPAGREC.Value := 'CONTAS A PAGAR E RECEBER';
    1: QrCrtNOMEPAGREC.Value := 'CONTAS A RECEBER';
  end;
end;

procedure TDmLct0.QrLctAfterScroll(DataSet: TDataSet);
begin
  if (FindWindow('TFmLctGer1', nil) > 0) and (CO_DMKID_APP <> 4) then //Syndi2
    FmLctGer1.DmLct0_QrLctAfterScroll();
end;

procedure TDmLct0.QrLctBeforeClose(DataSet: TDataSet);
begin
  VAR_LANCTO := QrLctControle.Value;
end;

procedure TDmLct0.QrLctCalcFields(DataSet: TDataSet);
begin
  if QrLctMes2.Value > 0 then
    QrLctMENSAL.Value := FormatFloat('00', QrLctMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrLctAno.Value), 3, 2)
   else QrLctMENSAL.Value := CO_VAZIO;
  if QrLctMes2.Value > 0 then
    QrLctMENSAL2.Value := FormatFloat('0000', QrLctAno.Value)+'/'+
    FormatFloat('00', QrLctMes2.Value)+'/01'
   else QrLctMENSAL2.Value := CO_VAZIO;

  //

  QrLctNOMESIT.Value := UFinanceiro.NomeSitLancto(QrLctSit.Value,
    QrLctTipo.Value, QrLctPrazo.Value, QrLctVencimento.Value,
    QrLctReparcel.Value);

  //

  case QrLctSit.Value of
    0: QrLctSALDO.Value := QrLctCredito.Value - QrLctDebito.Value;
    1: QrLctSALDO.Value := (QrLctCredito.Value - QrLctDebito.Value) - QrLctPago.Value;
    else QrLctSALDO.Value := 0;
  end;
  QrLctNOMERELACIONADO.Value := CO_VAZIO;
  if QrLctcliente.Value <> 0 then
  QrLctNOMERELACIONADO.Value := QrLctNOMECLIENTE.Value;
  if QrLctFornecedor.Value <> 0 then
  QrLctNOMERELACIONADO.Value := QrLctNOMERELACIONADO.Value +
  QrLctNOMEFORNECEDOR.Value;
  //
  if QrLctVencimento.Value > Date then QrLctATRASO.Value := 0
    else QrLctATRASO.Value := Date - QrLctVencimento.Value;
  //
  QrLctJUROS.Value :=
    Trunc(QrLctATRASO.Value * QrLctMoraDia.Value * 100)/100;
  //
  if QrLctCompensado.Value = 0 then
     QrLctCOMPENSADO_TXT.Value := '' else
     QrLctCOMPENSADO_TXT.Value :=
     FormatDateTime(VAR_FORMATDATE3, QrLctCompensado.Value);
  //
  QrLctSERIE_CHEQUE.Value := QrLctSerieCH.Value +
    FormatFloat('000000;-0; ', QrLctDocumento.Value);
end;

procedure TDmLct0.ReabreCarteiras(LocCart: Integer;
QrCrt, QrCrtSum: TmySQLQuery; Aviso: String);
var
  Empresa: Integer;
  SQL: String;
  //Empresas: String;
begin
  Empresa := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
  //
  if CO_DMKID_APP = 22 then //Credito2
    SQL := 'AND ca.Codigo <> 0'
  else
    SQL := 'AND ca.Codigo > 0';
  //
  if Empresa = 0 then
    Empresa := -100000000; 
  //
  {
  if VAR_LIB_EMPRESAS = '' then
    Empresas := '-100000000'
  else
    Empresas := VAR_LIB_EMPRESAS;
  }
  QrCrt.Close;
  QrCrt.SQL.Clear;
  QrCrt.SQL.Text :=
    'SELECT DISTINCT ca.*, ba.Nome NOMEDOBANCO, '      + sLineBreak +
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial '         + sLineBreak +
    'ELSE en.Nome END NOMEFORNECEI, en.CliInt '        + sLineBreak +
    'FROM carteiras ca '                               + sLineBreak +
    'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco '    + sLineBreak +
    'LEFT JOIN entidades en ON en.Codigo=ca.ForneceI ' + sLineBreak +
    //'WHERE ca.ForneceI in (' + Empresas + ')'         + sLineBreak +
    'WHERE ca.ForneceI in (' + Geral.FF0(Empresa) + ')'+ sLineBreak +
    //'OR ca.Codigo=0'                                   + sLineBreak +
    SQL                                                + sLineBreak +
    'AND ca.Ativo = 1'                                 + sLineBreak +
    'ORDER BY ca.Nome';
  UMyMod.AbreQuery(QrCrt, Dmod.MyDB, Aviso + ' > TDmLct0.ReabreCarteiras() > QrCarteira');
  //
  QrCrt.Locate('Codigo', LocCart, []);
  //
  if QrCrtSum <> nil  then
  begin
    QrCrtSum.Close;
    QrCrtSum.SQL.Text :=
      'SELECT SUM(Saldo) SALDO, SUM(FuturoC) FuturoC,'    + sLineBreak +
      'SUM(FuturoD) FuturoD, SUM(FuturoS) FuturoS,'       + sLineBreak +
      'SUM(EmCaixa) EmCaixa, SUM(EmCaixa-Saldo) Difere,'  + sLineBreak +
      'SUM(Saldo+FuturoS) SDO_FUT'                        + sLineBreak +
      'FROM carteiras ca'                                 + sLineBreak +
      //'WHERE ca.ForneceI in (' + Empresas + ')';
      'WHERE ca.ForneceI in (' + Geral.FF0(Empresa) + ')' + sLineBreak +
      'AND ca.Codigo > 0'                                 + sLineBreak +
      'AND ca.Ativo = 1';
    UMyMod.AbreQuery(QrCrtSum, Dmod.MyDB, Aviso + 'TDmLct0.ReabreCarteiras() > QrCartSum');
  end;
  //
end;

procedure TDmLct0.ReabreSoLct(QrLct: TmySQLQuery; ItemCarteira, Controle,
  Sub: Integer; TPDataIni, TPDataFim: TdmkEditDateTimePicker; Apto: Variant);
var
  Ini, Fim, Campo, Tab_Lct: String;
  MeuC, MeuS: Integer;
begin
  Tab_Lct := DModG.NomeTab(TMeuDB, ntLct, True);
  QrLct.Close;

  // Fazer mais r�pido inclus�es multiplas!
  if VAR_NaoReabrirLct then
    Exit;

  if TPDataIni <> nil then
    VAR_FL_DataIni := TPDataIni.Date;
  if TPDataFim <> nil then
    VAR_FL_DataFim := TPDataFim.Date;
  //
  Ini := FormatDateTime(VAR_FORMATDATE, VAR_FL_DataIni);
  Fim := FormatDateTime(VAR_FORMATDATE, VAR_FL_DataFim);
  //
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
  QrLct.SQL.Add('  ((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
  case VAR_KIND_DEPTO of
    kdUH: QrLct.SQL.Add('ci.Unidade UH, ');
    kdObra: QrLct.SQL.Add('ci.Sigla UH, ');
    else QrLct.SQL.Add('"" UH, ');
  end;
  QrLct.SQL.Add('la.*, ct.Codigo CONTA, ca.Prazo, ca.Nome NO_Carteira, ');
  QrLct.SQL.Add('ca.Banco1, ca.Agencia1, ca.Conta1,');
  QrLct.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
  QrLct.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
  QrLct.SQL.Add('IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA,');
  QrLct.SQL.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,');
  QrLct.SQL.Add('IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,');
  QrLct.SQL.Add('IF(la.ForneceI=0, "",');
  QrLct.SQL.Add('  IF(fi.Tipo=0, fi.RazaoSocial, fi.Nome)) NOMEFORNECEI,');
  QrLct.SQL.Add('IF(la.Sit<2, la.Credito-la.Debito-(la.Pago*la.Sit), 0) SALDO,');
  QrLct.SQL.Add('IF(la.Cliente>0,');
  QrLct.SQL.Add('  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),');
  QrLct.SQL.Add('  IF (la.Fornecedor>0,');
  QrLct.SQL.Add('    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO, ');
  QrLct.SQL.Add('ELT(la.Endossas, "Endossado", "Endossante", "Ambos", "? ? ? ?") NO_ENDOSSADO, ');
  QrLct.SQL.Add('uc.Login USERCAD_TXT, ua.Login USERALT_TXT, ');
  QrLct.SQL.Add('IF(la.DataCad < 2, "",DATE_FORMAT(la.DataCad,"%d/%m/%y")) DATACAD_TXT, ');
  QrLct.SQL.Add('IF(la.DataAlt < 2, "", DATE_FORMAT(la.DataAlt, "%d/%m/%y")) DATAALT_TXT ');
  QrLct.SQL.Add('FROM ' + Tab_Lct + ' la');
  QrLct.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  QrLct.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  QrLct.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  QrLct.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  QrLct.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  QrLct.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  QrLct.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrLct.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  QrLct.SQL.Add('LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI');
  QrLct.SQL.Add('LEFT JOIN senhas uc ON uc.Numero=la.UserCad');
  QrLct.SQL.Add('LEFT JOIN senhas ua ON ua.Numero=la.UserAlt');
  case VAR_KIND_DEPTO of
    kdUH: QrLct.SQL.Add('LEFT JOIN condimov  ci ON ci.Conta=la.Depto');
    kdObra: QrLct.SQL.Add('LEFT JOIN obrascab  ci ON ci.Codigo=la.Depto');
  end;

  case FTipoData of
    //0: Campo := 'la.Data';
    1: Campo := 'la.Vencimento';
    2: Campo := 'la.Compensado';
    else Campo := 'la.Data';
  end;
  QrLct.SQL.Add('WHERE ' + Campo + ' BETWEEN "'+Ini+'" AND "'+Fim+'"');
  // est� gerando problemas
  //QrLct.SQL.Add('AND la.Tipo='+IntToStr(TipoCarteira));
  QrLct.SQL.Add('AND la.Carteira='+IntToStr(ItemCarteira));
  //
  if Apto <> Null then
  begin
    if Apto <> 0 then
      QrLct.SQL.Add('AND la.Depto='+IntToStr(Apto));
  end;
  //
  QrLct.SQL.Add('AND ca.Ativo=1');
  QrLct.SQL.Add('ORDER BY la.Data, la.Controle');
  QrLct.Open;
  //
  if Controle = -1 then QrLct.Last else
  begin
    if Controle > 0 then
    begin
      MeuC := Controle;
      MeuS := Sub;
    end else begin
      MeuC := VAR_CONTROLE;
      MeuS := 0;
    end;
    if not QrLct.Locate('Controle;Sub', VarArrayOf([MeuC, MeuS]),[]) then
      QrLct.Last;
  end;
end;

procedure TDmLct0.VeSeReabreLct(TPDataIni, TPDataFim: TdmkEditDateTimePicker;
  Apto: Variant; Controle, Sub: Integer; QrCrt, QrLct: TmySQLQuery;
  ForcaAbertura: Boolean);
begin
  if (QrCrt.State = dsBrowse) and ((QrLct.State = dsBrowse) or ForcaAbertura) then
  ReabreSoLct(QrLct, (*QrCrt.FieldByName('Tipo').AsInteger,*)
    QrCrt.FieldByName('Codigo').AsInteger,
    Controle, Sub, TPDataIni, TPDataFim, Apto);
end;

end.
