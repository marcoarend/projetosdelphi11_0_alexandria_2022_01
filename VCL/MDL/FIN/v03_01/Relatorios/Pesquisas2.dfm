object FmPesquisas2: TFmPesquisas2
  Left = 379
  Top = 164
  Caption = 'FIN-RELAT-001 :: Pesquisas Financeiras'
  ClientHeight = 839
  ClientWidth = 965
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 965
    Height = 640
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 178
      Width = 965
      Height = 148
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox2: TGroupBox
        Left = 18
        Top = 10
        Width = 533
        Height = 75
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 1
        object Label4: TLabel
          Left = 10
          Top = 20
          Width = 66
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'M'#234's inicial:'
          Enabled = False
        end
        object Label5: TLabel
          Left = 167
          Top = 20
          Width = 64
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ano inicial:'
          Enabled = False
        end
        object Label6: TLabel
          Left = 266
          Top = 20
          Width = 56
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'M'#234's final:'
          Enabled = False
        end
        object Label7: TLabel
          Left = 428
          Top = 20
          Width = 54
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ano final:'
          Enabled = False
        end
        object CBMesIni: TComboBox
          Left = 11
          Top = 39
          Width = 148
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          DropDownCount = 12
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          Text = 'CBMesIni'
        end
        object CBAnoIni: TComboBox
          Left = 166
          Top = 39
          Width = 91
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          DropDownCount = 3
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Text = 'CBAnoIni'
        end
        object CBMesFim: TComboBox
          Left = 267
          Top = 39
          Width = 153
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          DropDownCount = 12
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          Text = 'CBMesFim'
        end
        object CBAnoFim: TComboBox
          Left = 427
          Top = 39
          Width = 91
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          DropDownCount = 3
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 7622183
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          Text = 'CBAnoFim'
        end
      end
      object CkCompetencia: TCheckBox
        Left = 33
        Top = 7
        Width = 228
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Agrupar por mes de compet'#234'ncia:'
        TabOrder = 0
        OnClick = CkCompetenciaClick
      end
      object RGTipo: TRadioGroup
        Left = 554
        Top = 10
        Width = 397
        Height = 47
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Tipo de formul'#225'rio: '
        ItemIndex = 0
        Items.Strings = (
          'Contas')
        TabOrder = 2
      end
      object RGNivel: TRadioGroup
        Left = 20
        Top = 89
        Width = 749
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' N'#237'vel do plano de contas: '
        Columns = 6
        ItemIndex = 0
        Items.Strings = (
          'Nenhum'
          'Conta'
          'Sub-grupo'
          'Grupo'
          'Conjunto'
          'Plano')
        TabOrder = 3
        OnClick = RGNivelClick
      end
      object RGAgrupamentos: TRadioGroup
        Left = 773
        Top = 89
        Width = 178
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Agrupamentos: '
        Columns = 4
        ItemIndex = 1
        Items.Strings = (
          '0'
          '1'
          '2'
          '3')
        TabOrder = 4
      end
      object CkSuprimirTamanho: TCheckBox
        Left = 556
        Top = 64
        Width = 405
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Aplicar ocultamento de lan'#231'amentos conforme plano de contas.'
        Checked = True
        State = cbChecked
        TabOrder = 5
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 548
      Width = 965
      Height = 92
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Align = alClient
      DataSource = DsLct1
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'MENSAL'
          Title.Caption = 'M'#234's'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETIPO'
          Title.Caption = 'Tipo de carteira'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECARTEIRA'
          Title.Caption = 'Nome da carteira'
          Width = 151
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 192
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end>
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 965
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label10: TLabel
        Left = 10
        Top = 0
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 10
        Top = 20
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 79
        Top = 20
        Width = 872
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GroupBox6: TGroupBox
      Left = 0
      Top = 49
      Width = 965
      Height = 129
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Datas dos documentos: '
      TabOrder = 1
      object PainelC: TPanel
        Left = 2
        Top = 18
        Width = 961
        Height = 104
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object PnDatas2: TPanel
          Left = 246
          Top = 0
          Width = 715
          Height = 104
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object GBVencto: TGroupBox
            Left = 2
            Top = 5
            Width = 232
            Height = 89
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '                                           '
            TabOrder = 0
            object LaVctoIni: TLabel
              Left = 10
              Top = 30
              Width = 34
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'In'#237'cio:'
            end
            object LaVctoFim: TLabel
              Left = 10
              Top = 59
              Width = 25
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Fim:'
            end
            object TPVctoIni: TdmkEditDateTimePicker
              Left = 59
              Top = 25
              Width = 138
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 0.516375590297684500
              Time = 0.516375590297684500
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object TPVctoFim: TdmkEditDateTimePicker
              Left = 59
              Top = 54
              Width = 138
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 41046.516439456000000000
              Time = 41046.516439456000000000
              TabOrder = 2
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object CkVencto: TCheckBox
              Left = 17
              Top = 0
              Width = 149
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data do vencimento: '
              TabOrder = 0
              OnClick = CkVenctoClick
            end
          end
          object GroupBox3: TGroupBox
            Left = 236
            Top = 5
            Width = 232
            Height = 89
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '                                          '
            TabOrder = 1
            object LaDocIni: TLabel
              Left = 15
              Top = 30
              Width = 34
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'In'#237'cio:'
            end
            object LaDocFim: TLabel
              Left = 15
              Top = 59
              Width = 25
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Fim:'
            end
            object TPDocIni: TdmkEditDateTimePicker
              Left = 59
              Top = 25
              Width = 138
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 0.516375590297684500
              Time = 0.516375590297684500
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object TPDocFim: TdmkEditDateTimePicker
              Left = 59
              Top = 54
              Width = 138
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 41046.516439456000000000
              Time = 41046.516439456000000000
              TabOrder = 2
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object CkDataDoc: TCheckBox
              Left = 20
              Top = 0
              Width = 144
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data do documento: '
              TabOrder = 0
              OnClick = CkDataDocClick
            end
          end
          object GroupBox4: TGroupBox
            Left = 470
            Top = 5
            Width = 232
            Height = 89
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '                               '
            TabOrder = 2
            object LaCompIni: TLabel
              Left = 15
              Top = 30
              Width = 34
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'In'#237'cio:'
            end
            object LaCompFim: TLabel
              Left = 15
              Top = 59
              Width = 25
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Fim:'
            end
            object TPCompIni: TdmkEditDateTimePicker
              Left = 59
              Top = 25
              Width = 138
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 0.516375590297684500
              Time = 0.516375590297684500
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object TPCompFim: TdmkEditDateTimePicker
              Left = 59
              Top = 54
              Width = 138
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 41046.516439456000000000
              Time = 41046.516439456000000000
              TabOrder = 2
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object CkDataComp: TCheckBox
              Left = 20
              Top = 0
              Width = 109
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Compensado: '
              TabOrder = 0
              OnClick = CkDataCompClick
            end
          end
        end
        object PnDatas1: TPanel
          Left = 0
          Top = 0
          Width = 246
          Height = 104
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox5: TGroupBox
            Left = 15
            Top = 5
            Width = 231
            Height = 89
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '                                     '
            TabOrder = 0
            object LaEmissIni: TLabel
              Left = 15
              Top = 30
              Width = 34
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'In'#237'cio:'
            end
            object LaEmissFim: TLabel
              Left = 15
              Top = 59
              Width = 25
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Fim:'
            end
            object TPEmissIni: TdmkEditDateTimePicker
              Left = 59
              Top = 25
              Width = 138
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 0.516375590297684500
              Time = 0.516375590297684500
              TabOrder = 0
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object TPEmissFim: TdmkEditDateTimePicker
              Left = 59
              Top = 54
              Width = 138
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 41046.516439456000000000
              Time = 41046.516439456000000000
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object CkEmissao: TCheckBox
              Left = 20
              Top = 0
              Width = 129
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data da emiss'#227'o: '
              TabOrder = 2
              OnClick = CkEmissaoClick
            end
          end
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 326
      Width = 965
      Height = 222
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
      object GroupBox1: TGroupBox
        Left = 410
        Top = 0
        Width = 555
        Height = 222
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = ' Ordenar por: '
        TabOrder = 0
        object RGOrdem1: TRadioGroup
          Left = 2
          Top = 18
          Width = 137
          Height = 202
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Ordem 1: '
          ItemIndex = 8
          Items.Strings = (
            'Compet'#234'ncia'
            'Data de emiss'#227'o'
            'Vencimento'
            'Data documento'
            'Compensa'#231#227'o'
            'Nome Terceiro'
            'Cr'#233'ditos/D'#233'bitos'
            'D'#233'bitos/Cr'#233'ditos'
            'Plano de contas')
          TabOrder = 0
        end
        object RGOrdem2: TRadioGroup
          Left = 139
          Top = 18
          Width = 137
          Height = 202
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Ordem 2: '
          ItemIndex = 1
          Items.Strings = (
            'Compet'#234'ncia'
            'Data de emiss'#227'o'
            'Vencimento'
            'Data documento'
            'Compensa'#231#227'o'
            'Nome Terceiro'
            'Cr'#233'ditos/D'#233'bitos'
            'D'#233'bitos/Cr'#233'ditos'
            'Plano de contas')
          TabOrder = 1
        end
        object RGOrdem3: TRadioGroup
          Left = 276
          Top = 18
          Width = 136
          Height = 202
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Ordem 3: '
          ItemIndex = 2
          Items.Strings = (
            'Compet'#234'ncia'
            'Data de emiss'#227'o'
            'Vencimento'
            'Data documento'
            'Compensa'#231#227'o'
            'Nome Terceiro'
            'Cr'#233'ditos/D'#233'bitos'
            'D'#233'bitos/Cr'#233'ditos'
            'Plano de contas')
          TabOrder = 2
        end
        object RGOrdem4: TRadioGroup
          Left = 412
          Top = 18
          Width = 137
          Height = 202
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Ordem 4: '
          ItemIndex = 3
          Items.Strings = (
            'Compet'#234'ncia'
            'Data de emiss'#227'o'
            'Vencimento'
            'Data documento'
            'Compensa'#231#227'o'
            'Nome Terceiro'
            'Cr'#233'ditos/D'#233'bitos'
            'D'#233'bitos/Cr'#233'ditos'
            'Plano de contas')
          TabOrder = 3
        end
      end
      object GroupBox7: TGroupBox
        Left = 0
        Top = 0
        Width = 410
        Height = 222
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Op'#231#245'es:'
        TabOrder = 1
        object Panel6: TPanel
          Left = 2
          Top = 18
          Width = 406
          Height = 202
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 10
            Top = 1
            Width = 176
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'G'#234'nero do n'#237'vel selecionado:'
          end
          object Label8: TLabel
            Left = 10
            Top = 49
            Width = 50
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Carteira:'
          end
          object LaCliente: TLabel
            Left = 10
            Top = 98
            Width = 44
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente:'
          end
          object LaFornece: TLabel
            Left = 10
            Top = 148
            Width = 73
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fornecedor:'
          end
          object EdNivelSel: TdmkEditCB
            Left = 10
            Top = 21
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBNivelSel
            IgnoraDBLookupComboBox = False
          end
          object CBNivelSel: TdmkDBLookupComboBox
            Left = 79
            Top = 21
            Width = 320
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsNivelSel
            TabOrder = 1
            OnClick = CBNivelSelClick
            dmkEditCB = EdNivelSel
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCarteira: TdmkEditCB
            Left = 10
            Top = 69
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCarteira
            IgnoraDBLookupComboBox = False
          end
          object CBCarteira: TdmkDBLookupComboBox
            Left = 79
            Top = 69
            Width = 320
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCarteiras
            TabOrder = 3
            dmkEditCB = EdCarteira
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCliente: TdmkEditCB
            Left = 10
            Top = 118
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 79
            Top = 118
            Width = 320
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsClientes
            TabOrder = 5
            dmkEditCB = EdCliente
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CBFornece: TdmkDBLookupComboBox
            Left = 79
            Top = 167
            Width = 320
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsFornecedores
            TabOrder = 6
            dmkEditCB = EdFornece
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdFornece: TdmkEditCB
            Left = 10
            Top = 167
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBFornece
            IgnoraDBLookupComboBox = False
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 906
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 847
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 325
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pesquisas Financeiras'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 325
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pesquisas Financeiras'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 325
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pesquisas Financeiras'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 699
    Width = 965
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 961
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 753
    Width = 965
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 786
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 18
      Width = 784
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 556
        Top = 5
        Width = 49
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Arquivo:'
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 399
        Top = 5
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Exporta excel'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BitBtn1Click
      end
      object EdArq: TEdit
        Left = 556
        Top = 25
        Width = 208
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 2
        Text = 'Pla1.xls'
      end
    end
  end
  object QrNivelSel: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas'
      '/* '
      'WHERE Codigo > 0 '
      '*/'
      'ORDER BY Nome')
    Left = 356
    Top = 48
    object QrNivelSelCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.contas.Codigo'
    end
    object QrNivelSelNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.contas.Nome'
      Size = 25
    end
  end
  object DsNivelSel: TDataSource
    DataSet = QrNivelSel
    Left = 384
    Top = 48
  end
  object QrLct1: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLct1CalcFields
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2,'
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,'
      '(la.Credito-la.Debito) CREDEB, la.*, co.Nome NOMEGENERO,'
      'ca.Nome NOMECARTEIRA, sg.Nome NOME_SG, gr.Nome NOME_GR,'
      'IF(la.Fornecedor<>0,'
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ELSE fo.Nome END,'
      
        'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END) NOMEEN' +
        'TIDADE,'
      'IF(la.Credito > 0, 1.000, 0.000) EhCred, '
      'IF(la.Debito > 0, 1.000, 0.000) EhDebt, '
      
        'pl.OrdemLista plOL, pl.Nome plNome, pl.Codigo plCodigo, pl.NotPr' +
        'ntFin plNPF, '
      
        'cj.OrdemLista cjOL, cj.Nome cjNome, cj.Codigo cjCodigo, cj.NotPr' +
        'ntFin cjNPF, '
      
        'gr.OrdemLista grOL, gr.Nome grNome, gr.Codigo grCodigo, gr.NotPr' +
        'ntFin grNPF, '
      
        'sg.OrdemLista sgOL, sg.Nome sgNome, sg.Codigo sgCodigo, sg.NotPr' +
        'ntFin sgNPF, '
      
        'co.OrdemLista coOL, co.Nome coNome, co.Codigo coCodigo, co.NotPr' +
        'ntFin coNPF'
      ''
      'FROM lct0001a la'
      'LEFT JOIN contas co    ON co.Codigo=la.Genero'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'WHERE Plano=0')
    Left = 384
    Top = 12
    object QrLct1Data: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLct1Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLct1Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLct1Sub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLct1Autorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrLct1Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLct1Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 25
    end
    object QrLct1NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLct1Debito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLct1Credito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLct1Compensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLct1Documento: TFloatField
      FieldName = 'Documento'
    end
    object QrLct1Sit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLct1Vencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLct1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLct1FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLct1FatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLct1Fatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 1
    end
    object QrLct1Banco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLct1Local: TIntegerField
      FieldName = 'Local'
    end
    object QrLct1Cartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLct1Linha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLct1NOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Calculated = True
    end
    object QrLct1NOMEGENERO: TWideStringField
      FieldName = 'NOMEGENERO'
      Size = 25
    end
    object QrLct1NOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 10
    end
    object QrLct1Ano: TFloatField
      FieldName = 'Ano'
    end
    object QrLct1MENSAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Calculated = True
    end
    object QrLct1MENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLct1ID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLct1Pago: TFloatField
      FieldName = 'Pago'
    end
    object QrLct1Mez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLct1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLct1ID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLct1Mes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLct1OperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLct1Lancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLct1Fornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLct1Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLct1MoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLct1Multa: TFloatField
      FieldName = 'Multa'
    end
    object QrLct1Protesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLct1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLct1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLct1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLct1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLct1DataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLct1CtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrLct1Nivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLct1Vendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLct1Account: TIntegerField
      FieldName = 'Account'
    end
    object QrLct1FatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLct1ICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrLct1ICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrLct1Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrLct1CliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLct1Depto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLct1DescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLct1ForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLct1Qtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrLct1Emitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrLct1Agencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrLct1ContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrLct1CNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrLct1DescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrLct1DescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrLct1NFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrLct1Antigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrLct1NOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrLct1CREDEB: TFloatField
      FieldName = 'CREDEB'
    end
    object QrLct1FatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLct1NOME_SG: TWideStringField
      FieldName = 'NOME_SG'
      Size = 50
    end
    object QrLct1NOME_GR: TWideStringField
      FieldName = 'NOME_GR'
      Size = 50
    end
    object QrLct1EhCred: TFloatField
      FieldName = 'EhCred'
      Required = True
    end
    object QrLct1EhDebt: TFloatField
      FieldName = 'EhDebt'
      Required = True
    end
    object QrLct1plOL: TIntegerField
      FieldName = 'plOL'
    end
    object QrLct1plNome: TWideStringField
      FieldName = 'plNome'
      Size = 50
    end
    object QrLct1plCodigo: TIntegerField
      FieldName = 'plCodigo'
      Required = True
    end
    object QrLct1cjOL: TIntegerField
      FieldName = 'cjOL'
    end
    object QrLct1cjNome: TWideStringField
      FieldName = 'cjNome'
      Size = 50
    end
    object QrLct1cjCodigo: TIntegerField
      FieldName = 'cjCodigo'
      Required = True
    end
    object QrLct1grOL: TIntegerField
      FieldName = 'grOL'
    end
    object QrLct1grNome: TWideStringField
      FieldName = 'grNome'
      Size = 50
    end
    object QrLct1grCodigo: TIntegerField
      FieldName = 'grCodigo'
      Required = True
    end
    object QrLct1sgOL: TIntegerField
      FieldName = 'sgOL'
    end
    object QrLct1sgNome: TWideStringField
      FieldName = 'sgNome'
      Size = 50
    end
    object QrLct1sgCodigo: TIntegerField
      FieldName = 'sgCodigo'
      Required = True
    end
    object QrLct1coOL: TIntegerField
      FieldName = 'coOL'
    end
    object QrLct1coNome: TWideStringField
      FieldName = 'coNome'
      Size = 50
    end
    object QrLct1coCodigo: TIntegerField
      FieldName = 'coCodigo'
      Required = True
    end
    object QrLct1plNPF: TIntegerField
      FieldName = 'plNPF'
    end
    object QrLct1cjNPF: TIntegerField
      FieldName = 'cjNPF'
    end
    object QrLct1grNPF: TIntegerField
      FieldName = 'grNPF'
    end
    object QrLct1sgNPF: TIntegerField
      FieldName = 'sgNPF'
    end
    object QrLct1coNPF: TIntegerField
      FieldName = 'coNPF'
    end
  end
  object DsLct1: TDataSource
    DataSet = QrLct1
    Left = 356
    Top = 12
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, ForneceI '
      'FROM carteiras'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 416
    Top = 48
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 444
    Top = 48
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE'
      '')
    Left = 476
    Top = 48
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrEntidades
    Left = 504
    Top = 48
  end
  object DsFornecedores: TDataSource
    DataSet = QrEntidades
    Left = 532
    Top = 48
  end
  object frxDsLct1: TfrxDBDataset
    UserName = 'frxDsLct1'
    CloseDataSource = False
    DataSet = QrLct1
    BCDToCurrency = False
    Left = 412
    Top = 12
  end
  object frxContaN: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.502501446800000000
    ReportOptions.LastChange = 41047.561779699080000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GH1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  if <VFR_ORD1_SEE> = True then GH1.Visible := True else GH1.Vis' +
        'ible := False;'
      
        '  if <VFR_ORD1_SEE> = True then GF1.Visible := True else GF1.Vis' +
        'ible := False;'
      '  GH1.Condition := <VFR_ORD1_CODITION>;'
      '  GH1.Height := <VFR_ORD1_TAM_GH>;'
      
        '  MeGH1.Visible := <VFR_ORD1_VISIBLE_GH>;                       ' +
        '           '
      '  MeGH1.Height := <VFR_ORD1_TAM_GH>;    '
      '  MD1.Height := <VFR_ORD1_TAM_MD>;'
      '  Me001.Height := <VFR_ORD1_TAM_MD>;'
      
        '  Me002.Height := <VFR_ORD1_TAM_MD>;                            ' +
        '            '
      
        '  Me003.Height := <VFR_ORD1_TAM_MD>;                            ' +
        '            '
      
        '  Me004.Height := <VFR_ORD1_TAM_MD>;                            ' +
        '            '
      
        '  Me005.Height := <VFR_ORD1_TAM_MD>;                            ' +
        '            '
      
        '  Me006.Height := <VFR_ORD1_TAM_MD>;                            ' +
        '            '
      
        '  Me007.Height := <VFR_ORD1_TAM_MD>;                            ' +
        '            '
      
        '  Me008.Height := <VFR_ORD1_TAM_MD>;                            ' +
        '            '
      
        '  Me009.Height := <VFR_ORD1_TAM_MD>;                            ' +
        '            '
      '  Me001.Visible := <VFR_ORD1_VISIBLE_MD>;'
      
        '  Me002.Visible := <VFR_ORD1_VISIBLE_MD>;                       ' +
        '                 '
      
        '  Me003.Visible := <VFR_ORD1_VISIBLE_MD>;                       ' +
        '                 '
      
        '  Me004.Visible := <VFR_ORD1_VISIBLE_MD>;                       ' +
        '                 '
      
        '  Me005.Visible := <VFR_ORD1_VISIBLE_MD>;                       ' +
        '                 '
      
        '  Me006.Visible := <VFR_ORD1_VISIBLE_MD>;                       ' +
        '                 '
      
        '  Me007.Visible := <VFR_ORD1_VISIBLE_MD>;                       ' +
        '                 '
      
        '  Me008.Visible := <VFR_ORD1_VISIBLE_MD>;                       ' +
        '                 '
      
        '  Me009.Visible := <VFR_ORD1_VISIBLE_MD>;                       ' +
        '                 '
      'end;'
      ''
      'procedure GH2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  if <VFR_ORD2_SEE> = True then GH2.Visible := True else GH2.Vis' +
        'ible := False;'
      
        '  if <VFR_ORD2_SEE> = True then GF2.Visible := True else GF2.Vis' +
        'ible := False;'
      '  GH2.Condition := <VFR_ORD2_CODITION>;'
      '  GH2.Height := <VFR_ORD2_TAM_GH>;'
      
        '  MeGH2.Visible := <VFR_ORD2_VISIBLE_GH>;                       ' +
        '           '
      '  MeGH2.Height := <VFR_ORD2_TAM_GH>;    '
      'end;'
      ''
      'procedure GH3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  if <VFR_ORD3_SEE> = True then GH3.Visible := True else GH3.Vis' +
        'ible := False;'
      
        '  if <VFR_ORD3_SEE> = True then GF3.Visible := True else GF3.Vis' +
        'ible := False;'
      '  GH3.Condition := <VFR_ORD3_CODITION>;'
      '  GH3.Height := <VFR_ORD3_TAM_GH>;'
      
        '  MeGH3.Visible := <VFR_ORD3_VISIBLE_GH>;                       ' +
        '           '
      '  MeGH3.Height := <VFR_ORD3_TAM_GH>;    '
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxConta1aGetValue
    Left = 440
    Top = 12
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsLct1
        DataSetName = 'frxDsLct1'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          Left = 22.677165354330700000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 90.708661420000000000
          Width = 83.149606300000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 377.952753460000000000
          Width = 212.000000000000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 589.606294330000000000
          Width = 45.354330708661400000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 634.960634800000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Width = 22.677165350000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Comp.')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 52.913385826771700000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 340.157477870000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 173.858270160000000000
          Width = 166.299212600000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Entidade')
          ParentFont = False
        end
      end
      object MD1: TfrxMasterData
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 355.275820000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLct1
        DataSetName = 'frxDsLct1'
        RowCount = 0
        object Me002: TfrxMemoView
          Left = 22.677162910000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLct1."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me003: TfrxMemoView
          Left = 52.913385830000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct1."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me004: TfrxMemoView
          Left = 90.708661420000000000
          Width = 83.149606300000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLct1."NOMECARTEIRA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me008: TfrxMemoView
          Left = 589.606294330000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct1."Debito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me009: TfrxMemoView
          Left = 634.960634800000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct1."Credito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me007: TfrxMemoView
          Left = 377.952753460000000000
          Width = 212.000000000000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLct1."Descricao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me001: TfrxMemoView
          Width = 22.677162910000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct1."MENSAL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me006: TfrxMemoView
          Left = 340.157477870000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLct1."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me005: TfrxMemoView
          Left = 173.858265280000000000
          Width = 166.299212600000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLct1."NOMEENTIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 12.000000000000000000
        Top = 551.811380000000000000
        Width = 680.315400000000000000
        object Memo14: TfrxMemoView
          Left = 589.606299210000000000
          Width = 45.354330710000000000
          Height = 12.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct1."Debito">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 634.960629920000000000
          Width = 45.354330710000000000
          Height = 12.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct1."Credito">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 536.692913385827000000
          Width = 52.913385826771700000
          Height = 12.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total no per'#237'odo:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 118.747990000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo23: TfrxMemoView
          Top = 87.747990000000000000
          Width = 98.267716540000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_CLIENTE_TIT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 98.267716540000000000
          Top = 87.747990000000000000
          Width = 581.858380000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_CLIENTENOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Top = 103.747990000000000000
          Width = 98.267716540000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_FORNECE_TIT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 98.267716540000000000
          Top = 103.747990000000000000
          Width = 581.858380000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_FORNECENOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo41: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo45: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Top = 71.811070000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Compet'#234'ncia: [COMPETENCIA]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 585.827150000000000000
        Width = 680.315400000000000000
        object Memo17: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228348900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsLct1."Data"'
        object MeGH1: TfrxMemoView
          Left = 18.897637800000000000
          Width = 692.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_ORD1_NOME]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.440940000000000000
        Top = 472.441250000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          Left = 589.606299210000000000
          Width = 45.354330710000000000
          Height = 12.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct1."Debito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 634.960629920000000000
          Width = 45.354330710000000000
          Height = 12.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct1."Credito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Width = 589.606313860000000000
          Height = 12.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SUB-TOTAL: [VFR_ORD1_NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 272.126160000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsLct1."Data"'
        object MeGH2: TfrxMemoView
          Left = 18.897637800000000000
          Width = 692.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_ORD2_NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.440940000000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          Left = 589.606299210000000000
          Width = 45.354330710000000000
          Height = 12.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct1."Debito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 634.960629920000000000
          Width = 45.354330710000000000
          Height = 12.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct1."Credito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Width = 589.606313860000000000
          Height = 12.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SUB-TOTAL: [VFR_ORD2_NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH3: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 313.700990000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GH3OnBeforePrint'
        Condition = 'frxDsLct1."Data"'
        object MeGH3: TfrxMemoView
          Left = 18.897637800000000000
          Width = 692.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_ORD3_NOME]')
          ParentFont = False
        end
      end
      object GF3: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.440940000000000000
        Top = 389.291590000000000000
        Width = 680.315400000000000000
        object Memo5: TfrxMemoView
          Left = 589.606299210000000000
          Width = 45.354330710000000000
          Height = 12.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct1."Debito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 634.960629920000000000
          Width = 45.354330710000000000
          Height = 12.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLct1."Credito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Width = 589.606313860000000000
          Height = 12.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SUB-TOTAL: [VFR_ORD3_NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
end
