unit Extratos2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnInternalConsts, ExtCtrls, StdCtrls, Buttons, ComCtrls, Db,
  (*DBTables,*) DBCtrls, UnGOTOy, Variants, UnDmkProcFunc, mySQLDbTables, DockForm,
  frxClass, frxDBSet, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkEditDateTimePicker, dmkPermissoes, dmkImage, dmkCheckGroup, UnDmkEnums,
  UnGrl_Vars;

type
  TFmExtratos2 = class(TForm)
    QrExtrato: TmySQLQuery;
    QrAnterior: TmySQLQuery;
    QrTerceiros: TmySQLQuery;
    DsTerceiros: TDataSource;
    QrTerceirosCodigo: TIntegerField;
    QrTerceirosNOMECONTATO: TWideStringField;
    QrPagRecIts: TmySQLQuery;
    StringField5: TWideStringField;
    QrPagRecItsNOMEVENCIDO: TWideStringField;
    QrPagRecItsVALOR: TFloatField;
    QrPagRecItsPAGO_REAL: TFloatField;
    QrPagRecItsATRAZODD: TFloatField;
    QrPagRecItsATUALIZADO: TFloatField;
    QrPagRecItsMULTA_REAL: TFloatField;
    QrPagRecItsVENCER: TDateField;
    QrPagRecItsVENCIDO: TDateField;
    QrPagRecX: TmySQLQuery;
    StringField7: TWideStringField;
    StringField8: TWideStringField;
    QrPagRecXNOMEVENCIDO: TWideStringField;
    QrPagRecXVALOR: TFloatField;
    QrPagRecXPAGO_REAL: TFloatField;
    QrPagRecXATRAZODD: TFloatField;
    QrPagRecXATUALIZADO: TFloatField;
    QrPagRecXMULTA_REAL: TFloatField;
    QrPagRecXVENCER: TDateField;
    QrPagRecXVENCIDO: TDateField;
    QrPagRec1: TmySQLQuery;
    QrPagRec1PAGO_ROLADO: TWideStringField;
    QrPagRecXPENDENTE: TFloatField;
    QrPagRec1DEVIDO: TFloatField;
    QrPagRec1PEND_TOTAL: TFloatField;
    QrPagRec: TmySQLQuery;
    QrPagRecXData: TDateField;
    QrPagRecXTipo: TSmallintField;
    QrPagRecXCarteira: TIntegerField;
    QrPagRecXSub: TSmallintField;
    QrPagRecXAutorizacao: TIntegerField;
    QrPagRecXGenero: TIntegerField;
    QrPagRecXDescricao: TWideStringField;
    QrPagRecXNotaFiscal: TIntegerField;
    QrPagRecXDebito: TFloatField;
    QrPagRecXCredito: TFloatField;
    QrPagRecXCompensado: TDateField;
    QrPagRecXDocumento: TFloatField;
    QrPagRecXSit: TIntegerField;
    QrPagRecXVencimento: TDateField;
    QrPagRecXLk: TIntegerField;
    QrPagRecXFatID: TIntegerField;
    QrPagRecXFatParcela: TIntegerField;
    QrPagRecXID_Sub: TSmallintField;
    QrPagRecXFatura: TWideStringField;
    QrPagRecXBanco: TIntegerField;
    QrPagRecXLocal: TIntegerField;
    QrPagRecXCartao: TIntegerField;
    QrPagRecXLinha: TIntegerField;
    QrPagRecXOperCount: TIntegerField;
    QrPagRecXLancto: TIntegerField;
    QrPagRecXPago: TFloatField;
    QrPagRecXFornecedor: TIntegerField;
    QrPagRecXCliente: TIntegerField;
    QrPagRecXMoraDia: TFloatField;
    QrPagRecXMulta: TFloatField;
    QrPagRecXProtesto: TDateField;
    QrPagRecXDataCad: TDateField;
    QrPagRecXDataAlt: TDateField;
    QrPagRecXUserCad: TSmallintField;
    QrPagRecXUserAlt: TSmallintField;
    QrPagRecXDataDoc: TDateField;
    QrPagRecXNivel: TIntegerField;
    QrPagRecXVendedor: TIntegerField;
    QrPagRecXAccount: TIntegerField;
    QrPagRecXNOMECONTA: TWideStringField;
    QrPagRecXNOMECARTEIRA: TWideStringField;
    QrPagRecXSALDO: TFloatField;
    QrPagRecXEhCRED: TWideStringField;
    QrPagRecXEhDEB: TWideStringField;
    QrPagRecXNOME_TERCEIRO: TWideStringField;
    QrPagRec1Data: TDateField;
    QrPagRec1DataDoc: TDateField;
    QrPagRec1Vencimento: TDateField;
    QrPagRec1TERCEIRO: TFloatField;
    QrPagRec1NOME_TERCEIRO: TWideStringField;
    QrPagRec1NOMEVENCIDO: TWideStringField;
    QrPagRec1NOMECONTA: TWideStringField;
    QrPagRec1Descricao: TWideStringField;
    QrPagRec1CtrlPai: TFloatField;
    QrPagRec1Tipo: TFloatField;
    QrPagRec1Documento: TFloatField;
    QrPagRec1NotaFiscal: TFloatField;
    QrPagRec1Valor: TFloatField;
    QrPagRec1MoraDia: TFloatField;
    QrPagRec1PAGO_REAL: TFloatField;
    QrPagRec1PENDENTE: TFloatField;
    QrPagRec1ATUALIZADO: TFloatField;
    QrPagRec1MULTA_REAL: TFloatField;
    QrPagRec1ATRAZODD: TFloatField;
    QrPagRecItsData: TDateField;
    QrPagRecItsTipo: TSmallintField;
    QrPagRecItsCarteira: TIntegerField;
    QrPagRecItsSub: TSmallintField;
    QrPagRecItsAutorizacao: TIntegerField;
    QrPagRecItsGenero: TIntegerField;
    QrPagRecItsDescricao: TWideStringField;
    QrPagRecItsNotaFiscal: TIntegerField;
    QrPagRecItsDebito: TFloatField;
    QrPagRecItsCredito: TFloatField;
    QrPagRecItsCompensado: TDateField;
    QrPagRecItsDocumento: TFloatField;
    QrPagRecItsSit: TIntegerField;
    QrPagRecItsVencimento: TDateField;
    QrPagRecItsLk: TIntegerField;
    QrPagRecItsFatID: TIntegerField;
    QrPagRecItsFatParcela: TIntegerField;
    QrPagRecItsID_Sub: TSmallintField;
    QrPagRecItsFatura: TWideStringField;
    QrPagRecItsBanco: TIntegerField;
    QrPagRecItsLocal: TIntegerField;
    QrPagRecItsCartao: TIntegerField;
    QrPagRecItsLinha: TIntegerField;
    QrPagRecItsOperCount: TIntegerField;
    QrPagRecItsLancto: TIntegerField;
    QrPagRecItsPago: TFloatField;
    QrPagRecItsFornecedor: TIntegerField;
    QrPagRecItsCliente: TIntegerField;
    QrPagRecItsMoraDia: TFloatField;
    QrPagRecItsMulta: TFloatField;
    QrPagRecItsProtesto: TDateField;
    QrPagRecItsDataCad: TDateField;
    QrPagRecItsDataAlt: TDateField;
    QrPagRecItsUserCad: TSmallintField;
    QrPagRecItsUserAlt: TSmallintField;
    QrPagRecItsDataDoc: TDateField;
    QrPagRecItsNivel: TIntegerField;
    QrPagRecItsVendedor: TIntegerField;
    QrPagRecItsAccount: TIntegerField;
    QrPagRecItsNOMECONTA: TWideStringField;
    QrPagRecItsNOMECARTEIRA: TWideStringField;
    QrPagRecItsSALDO: TFloatField;
    QrPagRecItsEhCRED: TWideStringField;
    QrPagRecItsEhDEB: TWideStringField;
    QrPagRecItsNOME_TERCEIRO: TWideStringField;
    QrPagRecItsTERCEIRO: TLargeintField;
    QrExtratoNOMECONTA: TWideStringField;
    QrExtratoNOMECARTEIRA: TWideStringField;
    DsAccounts: TDataSource;
    QrAccounts: TmySQLQuery;
    QrAccountsCodigo: TIntegerField;
    QrAccountsNOMECONTATO: TWideStringField;
    QrVendedores: TmySQLQuery;
    DsVendedores: TDataSource;
    QrVendedoresCodigo: TIntegerField;
    QrVendedoresNOMECONTATO: TWideStringField;
    QrPagRecXControle: TIntegerField;
    QrPagRecXID_Pgto: TIntegerField;
    QrPagRecXMez: TIntegerField;
    QrPagRecXCtrlIni: TIntegerField;
    QrPagRec1ID_Pgto: TFloatField;
    QrPagRec1CtrlIni: TFloatField;
    QrPagRec1Controle: TFloatField;
    QrPagRecItsControle: TIntegerField;
    QrPagRecItsID_Pgto: TIntegerField;
    QrPagRecItsMez: TIntegerField;
    QrPagRecItsCtrlIni: TIntegerField;
    QrPagRecData: TDateField;
    QrPagRecTipo: TSmallintField;
    QrPagRecCarteira: TIntegerField;
    QrPagRecControle: TIntegerField;
    QrPagRecSub: TSmallintField;
    QrPagRecAutorizacao: TIntegerField;
    QrPagRecGenero: TIntegerField;
    QrPagRecDescricao: TWideStringField;
    QrPagRecNotaFiscal: TIntegerField;
    QrPagRecDebito: TFloatField;
    QrPagRecCredito: TFloatField;
    QrPagRecCompensado: TDateField;
    QrPagRecDocumento: TFloatField;
    QrPagRecSit: TIntegerField;
    QrPagRecVencimento: TDateField;
    QrPagRecLk: TIntegerField;
    QrPagRecFatID: TIntegerField;
    QrPagRecFatParcela: TIntegerField;
    QrPagRecID_Pgto: TIntegerField;
    QrPagRecID_Sub: TSmallintField;
    QrPagRecFatura: TWideStringField;
    QrPagRecBanco: TIntegerField;
    QrPagRecLocal: TIntegerField;
    QrPagRecCartao: TIntegerField;
    QrPagRecLinha: TIntegerField;
    QrPagRecOperCount: TIntegerField;
    QrPagRecLancto: TIntegerField;
    QrPagRecPago: TFloatField;
    QrPagRecMez: TIntegerField;
    QrPagRecFornecedor: TIntegerField;
    QrPagRecCliente: TIntegerField;
    QrPagRecMoraDia: TFloatField;
    QrPagRecMulta: TFloatField;
    QrPagRecProtesto: TDateField;
    QrPagRecDataCad: TDateField;
    QrPagRecDataAlt: TDateField;
    QrPagRecUserCad: TSmallintField;
    QrPagRecUserAlt: TSmallintField;
    QrPagRecDataDoc: TDateField;
    QrPagRecCtrlIni: TIntegerField;
    QrPagRecNivel: TIntegerField;
    QrPagRecVendedor: TIntegerField;
    QrPagRecAccount: TIntegerField;
    QrPagRecNOMECONTA: TWideStringField;
    QrPagRecNOMECARTEIRA: TWideStringField;
    QrPagRecSALDO: TFloatField;
    QrPagRecEhCRED: TWideStringField;
    QrPagRecEhDEB: TWideStringField;
    QrPagRecNOME_TERCEIRO: TWideStringField;
    QrPagRecTERCEIRO: TLargeintField;
    QrPagRecVENCER: TDateField;
    QrPagRecVALOR: TFloatField;
    QrPagRecVENCIDO: TDateField;
    QrPagRecATRAZODD: TFloatField;
    QrPagRecMULTA_REAL: TFloatField;
    QrPagRecATUALIZADO: TFloatField;
    QrPagRecPAGO_REAL: TFloatField;
    QrPagRecNOMEVENCIDO: TWideStringField;
    QrExtratoData: TDateField;
    QrExtratoTipo: TSmallintField;
    QrExtratoCarteira: TIntegerField;
    QrExtratoControle: TIntegerField;
    QrExtratoSub: TSmallintField;
    QrExtratoAutorizacao: TIntegerField;
    QrExtratoGenero: TIntegerField;
    QrExtratoDescricao: TWideStringField;
    QrExtratoNotaFiscal: TIntegerField;
    QrExtratoDebito: TFloatField;
    QrExtratoCredito: TFloatField;
    QrExtratoCompensado: TDateField;
    QrExtratoDocumento: TFloatField;
    QrExtratoSit: TIntegerField;
    QrExtratoVencimento: TDateField;
    QrExtratoLk: TIntegerField;
    QrExtratoFatID: TIntegerField;
    QrExtratoFatParcela: TIntegerField;
    QrExtratoID_Pgto: TIntegerField;
    QrExtratoID_Sub: TSmallintField;
    QrExtratoFatura: TWideStringField;
    QrExtratoBanco: TIntegerField;
    QrExtratoLocal: TIntegerField;
    QrExtratoCartao: TIntegerField;
    QrExtratoLinha: TIntegerField;
    QrExtratoOperCount: TIntegerField;
    QrExtratoLancto: TIntegerField;
    QrExtratoPago: TFloatField;
    QrExtratoMez: TIntegerField;
    QrExtratoFornecedor: TIntegerField;
    QrExtratoCliente: TIntegerField;
    QrExtratoMoraDia: TFloatField;
    QrExtratoMulta: TFloatField;
    QrExtratoProtesto: TDateField;
    QrExtratoDataCad: TDateField;
    QrExtratoDataAlt: TDateField;
    QrExtratoUserCad: TSmallintField;
    QrExtratoUserAlt: TSmallintField;
    QrExtratoDataDoc: TDateField;
    QrExtratoCtrlIni: TIntegerField;
    QrExtratoNivel: TIntegerField;
    QrExtratoVendedor: TIntegerField;
    QrExtratoAccount: TIntegerField;
    QrTerceirosTel1: TWideStringField;
    QrTerceirosTel2: TWideStringField;
    QrTerceirosTel3: TWideStringField;
    QrTerceirosTEL1_TXT: TWideStringField;
    QrTerceirosTEL2_TXT: TWideStringField;
    QrTerceirosTEL3_TXT: TWideStringField;
    QrTerceirosTELEFONES: TWideStringField;
    QrPagRecXDuplicata: TWideStringField;
    QrPagRecFatID_Sub: TIntegerField;
    QrPagRecICMS_P: TFloatField;
    QrPagRecICMS_V: TFloatField;
    QrPagRecDuplicata: TWideStringField;
    QrPagRec1Duplicata: TWideStringField;
    QrPagRecXFatID_Sub: TIntegerField;
    QrPagRecXICMS_P: TFloatField;
    QrPagRecXICMS_V: TFloatField;
    QrContas: TMySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    frxFin_Relat_005_02_B1: TfrxReport;
    frxDsPagRec1: TfrxDBDataset;
    frxFin_Relat_005_02_B2: TfrxReport;
    frxDsPagRec: TfrxDBDataset;
    frxFin_Relat_005_00: TfrxReport;
    frxDsExtrato: TfrxDBDataset;
    frxFin_Relat_005_01_A08: TfrxReport;
    frxDsFluxo: TfrxDBDataset;
    QrExtratoQtde: TFloatField;
    QrPagRecQtde: TFloatField;
    frxFin_Relat_005_02_A08: TfrxReport;
    QrPagRecXQtde: TFloatField;
    QrPagRec1Qtde: TFloatField;
    frxFin_Relat_005_02_A06: TfrxReport;
    QrPagRecXNO_UH: TWideStringField;
    QrPagRecNO_UH: TWideStringField;
    QrPagRecItsNO_UH: TWideStringField;
    frxFin_Relat_005_02_A07: TfrxReport;
    QrPagRec1NO_UH: TWideStringField;
    DsFlxo: TDataSource;
    QrFlxo: TmySQLQuery;
    QrExtr: TmySQLQuery;
    QrExtratoDataE: TDateField;
    QrExtratoDataV: TDateField;
    QrExtratoDataQ: TDateField;
    QrExtratoDataX: TDateField;
    QrExtratoTexto: TWideStringField;
    QrExtratoDocum: TWideStringField;
    QrExtratoNotaF: TWideStringField;
    QrExtratoSaldo: TFloatField;
    QrExtratoCartC: TIntegerField;
    QrExtratoCartN: TWideStringField;
    QrExtratoCodig: TIntegerField;
    QrExtratoCtrle: TIntegerField;
    QrExtratoCtSub: TIntegerField;
    QrExtratoID_Pg: TIntegerField;
    QrExtratoTipoI: TIntegerField;
    DsExtr: TDataSource;
    QrFlxoData: TDateField;
    QrFlxoCarteira: TIntegerField;
    QrFlxoControle: TIntegerField;
    QrFlxoSub: TSmallintField;
    QrFlxoQtde: TFloatField;
    QrFlxoDescricao: TWideStringField;
    QrFlxoNotaFiscal: TIntegerField;
    QrFlxoDebito: TFloatField;
    QrFlxoCredito: TFloatField;
    QrFlxoCompensado: TDateField;
    QrFlxoSerieCH: TWideStringField;
    QrFlxoDocumento: TFloatField;
    QrFlxoVencimento: TDateField;
    QrFlxoNOMECART: TWideStringField;
    frxDsExtr: TfrxDBDataset;
    QrExtrVALOR: TFloatField;
    QrExtrCredi: TFloatField;
    QrExtrDebit: TFloatField;
    QrFlxoNOMERELACIONADO: TWideStringField;
    frxFin_Relat_005_01_A07: TfrxReport;
    frxFin_Relat_005_01_A06: TfrxReport;
    QrPagRecSERIEDOC: TWideStringField;
    QrPagRecSerieCH: TWideStringField;
    QrFlxoID_Pgto: TIntegerField;
    QrInicial: TmySQLQuery;
    QrInicialSdoFimB: TFloatField;
    QrAnteriorMovimento: TFloatField;
    QrFlxoQUITACAO: TDateField;
    dmkPermissoes1: TdmkPermissoes;
    QrPagRecXTERCEIRO: TIntegerField;
    QrFlxoDESCRI_TXT: TWideStringField;
    QrFlxoNOMECONTA: TWideStringField;
    QrFlxoSit: TIntegerField;
    QrFlxoPago: TFloatField;
    QrFlxoTipo: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSair: TBitBtn;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    GroupBox4: TGroupBox;
    PainelDados: TPanel;
    CkGrade: TCheckBox;
    EdA: TdmkEdit;
    EdZ: TdmkEdit;
    CkNiveis: TCheckBox;
    GBOmiss: TGroupBox;
    CkAnos: TCheckBox;
    CkMeses: TCheckBox;
    CkDias: TCheckBox;
    EdAnos: TdmkEdit;
    EdMeses: TdmkEdit;
    EdDias: TdmkEdit;
    BtSalvar: TBitBtn;
    CkOmiss: TCheckBox;
    GBPerdido: TGroupBox;
    CkAnos2: TCheckBox;
    CkMeses2: TCheckBox;
    CkDias2: TCheckBox;
    EdAnos2: TdmkEdit;
    EdMeses2: TdmkEdit;
    EdDias2: TdmkEdit;
    BtGravar: TBitBtn;
    CkSubstituir: TCheckBox;
    CkExclusivo: TCheckBox;
    PainelE: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    CkOrdem1: TCheckBox;
    Panel2: TPanel;
    CkOrdem2: TCheckBox;
    RGHistorico: TRadioGroup;
    RGFonte: TRadioGroup;
    PainelA: TPanel;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    RGTipo: TRadioGroup;
    GroupBox6: TGroupBox;
    PainelC: TPanel;
    PnDatas2: TPanel;
    GBVencto: TGroupBox;
    LaVenctI: TLabel;
    LaVenctF: TLabel;
    TPVctoIni: TdmkEditDateTimePicker;
    TPVctoFim: TdmkEditDateTimePicker;
    CkVencto: TCheckBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    TPDocIni: TdmkEditDateTimePicker;
    TPDocFim: TdmkEditDateTimePicker;
    CkDataDoc: TCheckBox;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    TPCompIni: TdmkEditDateTimePicker;
    TPCompFim: TdmkEditDateTimePicker;
    CkDataComp: TCheckBox;
    PnDatas1: TPanel;
    GroupBox1: TGroupBox;
    Label101: TLabel;
    Label102: TLabel;
    TPEmissIni: TdmkEditDateTimePicker;
    TPEmissFim: TdmkEditDateTimePicker;
    CkEmissao: TCheckBox;
    PnFluxo: TPanel;
    Label3: TLabel;
    Label7: TLabel;
    GroupBox5: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    TPCre: TdmkEditDateTimePicker;
    TPDeb: TdmkEditDateTimePicker;
    PainelD: TPanel;
    LaAccount: TLabel;
    LaTerceiro: TLabel;
    Label4: TLabel;
    LaVendedor: TLabel;
    CBAccount: TdmkDBLookupComboBox;
    EdAccount: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    EdTerceiro: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    EdConta: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    EdVendedor: TdmkEditCB;
    CGPendencias: TdmkCheckGroup;
    BtParar: TBitBtn;
    PnDepto: TPanel;
    EdDepto: TdmkEditCB;
    CBDepto: TdmkDBLookupComboBox;
    QrDeptos: TmySQLQuery;
    DsDeptos: TDataSource;
    QrDeptosNome: TWideStringField;
    QrDeptosCodigo: TIntegerField;
    CkDepto: TCheckBox;
    frxFin_Relat_005_03_A08: TfrxReport;
    frxFin_Relat_005_03_A06: TfrxReport;
    frxFin_Relat_005_03_A07: TfrxReport;
    frxFin_Relat_005_04_A06: TfrxReport;
    frxFin_Relat_005_04_A07: TfrxReport;
    frxFin_Relat_005_04_A08: TfrxReport;
    QrPagRecMoraVal: TFloatField;
    QrPagRecMultaVal: TFloatField;
    QrPagRecDescoVal: TFloatField;
    RGPagRec: TRadioGroup;
    QrGruSem: TmySQLQuery;
    QrGruSemVencimento: TDateField;
    QrGruSemGenero: TIntegerField;
    QrGruSemCredito: TFloatField;
    QrGruSemDebito: TFloatField;
    QrGruSemNO_CTA: TWideStringField;
    Qr10: TmySQLQuery;
    Qr10Tipo: TSmallintField;
    Qr10Genero: TIntegerField;
    Qr10NO_CTA: TWideStringField;
    Qr10Valor01: TFloatField;
    Qr10Valor02: TFloatField;
    Qr10Valor03: TFloatField;
    Qr10Valor04: TFloatField;
    Qr10Valor05: TFloatField;
    Qr10Valor06: TFloatField;
    Qr10Valor07: TFloatField;
    Qr10Valor08: TFloatField;
    frxFin_Relat_005_10_A: TfrxReport;
    frxDs10: TfrxDBDataset;
    Qr10NO_TIPO: TWideStringField;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    Label11: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrPagRec1NOMECARTEIRA: TWideStringField;
    QrCarteiras: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrCarteirasNome: TWideStringField;
    DsCarteiras: TDataSource;
    QrFCMIni: TmySQLQuery;
    QrFCMIniSaldo: TFloatField;
    frxDsFCMIni: TfrxDBDataset;
    RGFlxCxaMes_FimSem: TRadioGroup;
    QrFCMMov: TmySQLQuery;
    QrFCMMovDataX: TDateField;
    QrFCMMovCredi: TFloatField;
    QrFCMMovDebit: TFloatField;
    frxFin_Relat_005_11_A: TfrxReport;
    QrFCMDia: TmySQLQuery;
    QrFCMDiaData: TDateField;
    QrFCMDiaSaldoAnt: TFloatField;
    QrFCMDiaReceber: TFloatField;
    QrFCMDiaDisponiv: TFloatField;
    QrFCMDiaPagar: TFloatField;
    QrFCMDiaAcumulou: TFloatField;
    QrFCMDiaAtivo: TSmallintField;
    frxDsFCMDia: TfrxDBDataset;
    frxReport1: TfrxReport;
    QrPagRec1Data_TXT: TWideStringField;
    QrPagRec1DataDoc_TXT: TWideStringField;
    QrPagRec1Vencimento_TXT: TWideStringField;
    QrPagRecData_TXT: TWideStringField;
    QrPagRecDataDoc_TXT: TWideStringField;
    QrPagRecVencimento_TXT: TWideStringField;
    QrExtrDataE_TXT: TWideStringField;
    QrExtrDataV_TXT: TWideStringField;
    QrExtrDataQ_TXT: TWideStringField;
    QrExtrDataX_TXT: TWideStringField;
    Label12: TLabel;
    EdSubGrupo: TdmkEditCB;
    CBSubGrupo: TdmkDBLookupComboBox;
    QrSubGrupos: TMySQLQuery;
    QrSubGruposCodigo: TIntegerField;
    QrSubGruposNome: TWideStringField;
    DsSubGrupos: TDataSource;
    QrPagRecXQtd2: TFloatField;
    QrPagRecQtd2: TFloatField;
    QrPagRec1Qtd2: TFloatField;
    QrPagRec1SdoQtd1: TFloatField;
    QrPagRec1SdoQtd2: TFloatField;
    procedure FormActivate(Sender: TObject);
    procedure BtSairClick(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrPagRecCalcFields(DataSet: TDataSet);
    procedure CkOmissClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure BtGravarClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPagRecItsCalcFields(DataSet: TDataSet);
    procedure QrPagRecXCalcFields(DataSet: TDataSet);
    procedure QrPagRec1CalcFields(DataSet: TDataSet);
    procedure QrTerceirosCalcFields(DataSet: TDataSet);
    procedure frxFin_Relat_005_02_B1GetValue(const VarName: String;
      var Value: Variant);
    procedure frxFin_Relat_005_00GetValue(const VarName: String;
      var Value: Variant);
    procedure QrExtrCalcFields(DataSet: TDataSet);
    procedure QrFlxoCalcFields(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtPararClick(Sender: TObject);
    procedure frxFin_Relat_005_10_AGetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    FParar: Boolean;
    //
    FEmpresa, FCliInt, FEntidade: Integer;
    FTabLctA, FTabLctB, FTabLctD, FEntidade_TXT, FSQLDepto_TXT: String;
    FDtEncer, FDtMorto: TDateTime;
    FRelSel: Integer;
    FPagRecSem, FFlxCxaMes: String;
    FDia1: Integer;
    //
    procedure R00_ImprimeExtratoConsolidado();
    procedure R01_ImprimeFluxoDeCaixa(SoGera: Boolean);
    procedure R02_ImprimeContasAPagar(Sender: TObject; Saida: TTipoSaidaRelatorio);
    procedure R10_ImprimeContasPagRec(Sender: TObject; Saida: TTipoSaidaRelatorio);
    procedure R11_ImprimeContasPagRec(Sender: TObject; Saida: TTipoSaidaRelatorio);
    procedure SQLContasAPagar(Query: TmySQLQuery; Tipo: Integer);
    procedure CreateDockableWindows;
    procedure GeraRelatorio(Saida: TTipoSaidaRelatorio);
    procedure ReopenCarteiras();
    function SQL_Ordenar(): String;
  public
    { Public declarations }
  end;

var
  FmExtratos2: TFmExtratos2;

implementation

uses UCreate, Module, ModuleGeral, UMySQLModule, UnFinanceiro, ModuleFin,
  UnMyObjects, DmkDAC_PF, UCreateFin;

{$R *.DFM}

const
  Colors: array[0..0] of TColor = (clWhite);
  ColStr: array[0..0] of string = ('White');

var
  Ext_EmisI, Ext_EmisF, Ext_VctoI, Ext_VctoF, (*Ext_DataH, Ext_DataA,*)
  Ext_DocI,  Ext_DocF,  Ext_CompI, Ext_CompF: String;
  Ext_Saldo, Ext_SdIni,
  Ext_VALORA,     Ext_VALORB,     Ext_VALORC,
  Ext_PENDENTEA,  Ext_PENDENTEB,  Ext_PENDENTEC,
  Ext_ATUALIZADOA, Ext_ATUALIZADOB, Ext_ATUALIZADOC,
  Ext_DEVIDOA, Ext_DEVIDOB, Ext_DEVIDOC,
  Ext_PAGO_REALA, Ext_PAGO_REALB, Ext_PAGO_REALC: Double;
  Ext_Vencto: TDate;
  DockWindows: array[0..0] of TFmDockForm;


procedure TFmExtratos2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmExtratos2.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmExtratos2.RGTipoClick(Sender: TObject);
var
  Tipo: Integer;
begin
  Tipo := RGTipo.ItemIndex;
  //
  CkExclusivo.Visible := Tipo in ([0,1,11]);
  PnDatas2.Visible    := Tipo in ([2,3,4,5,6,7,8,9,10]);
  PainelD.Visible     := Tipo in ([2,3,4,5,6,7,8,9,10]);
  PnFluxo.Visible     := Tipo in ([1,11]);
  RGFlxCxaMes_FimSem.Visible := Tipo = 11;
  //
  RGPagRec.ItemIndex := 0;
  //
  if Tipo in [2,4,6,3,5,7] then
    RGPagRec.Visible := True
  else
    RGPagRec.Visible := False;
  //
  if Tipo in [1,10] then
  begin
    CkOrdem1.Visible := False;
    RGOrdem1.Visible := False;
    CkOrdem2.Visible := False;
    RGOrdem2.Visible := False;
  end else
  begin
    CkOrdem1.Visible := True;
    RGOrdem1.Visible := True;
    CkOrdem2.Visible := True;
    RGOrdem2.Visible := True;
  end;
  RGHistorico.Visible := Tipo in ([6,7]);
  RGFonte.Visible     := Tipo in ([1,2,3,4,5,8,9]);
  //
  EdA.Visible        := True;
  EdZ.Visible        := True;
  CkNiveis.Visible   := True;
  //CkNiveis.Checked   := False;
  CkOmiss.Visible    := False;
  GBOmiss.Visible    := False;
  GBPerdido.Visible  := False;
  LaAccount.Visible  := False;
  EdAccount.Visible  := False;
  CBAccount.Visible  := False;
  LaVendedor.Visible := False;
  EdVendedor.Visible := False;
  CBVendedor.Visible := False;
  //
  //CkVencto.Visible     := True;
  GBVencto.Visible     := True;
  CkSubstituir.Visible := False;
  CkEmissao.Caption    := 'Data da emiss�o:';
  case Tipo of
    0:
    begin
      GBVencto.Visible := True;
      //
      EdA.Visible := False;
      EdZ.Visible := False;
      CkNiveis.Visible := False;
      //
      if TPEmissIni.Date > date then TPEmissIni.Date := date;
      if TPEmissFim.Date < date then TPEmissFim.Date := date;
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date;
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      if TPVctoIni.Date > date then TPVctoIni.Date := date;
      if TPVctoFim.Date < date then TPVctoFim.Date := date;
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date;
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CBTerceiro.Enabled := False;
      EdTerceiro.Enabled := False;
      LaTerceiro.Enabled := False;
    end;
    1:
    begin
      EdA.Visible          := False;
      EdZ.Visible          := False;
      CkNiveis.Visible     := False;
      CkGrade.Visible      := False;
      CkSubstituir.Visible := True;
      CkEmissao.Caption    := 'Per�odo:';
      //
      if TPEmissFim.Date < date then TPEmissFim.Date := date;
      TPEmissIni.MinDate := 1;
      TPEmissIni.MaxDate := Date+7305;//Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := Date;
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      if TPVctoFim.Date < date then TPVctoFim.Date := date;
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := Date;
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      CBTerceiro.Enabled := False;
      EdTerceiro.Enabled := False;
      LaTerceiro.Enabled := False;
    end;
    2, 11:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Fornecedor';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    3:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
      ////
      LaAccount.Visible := True;
      EdAccount.Visible := True;
      CBAccount.Visible := True;
      LaVendedor.Visible := True;
      EdVendedor.Visible := True;
      CBVendedor.Visible := True;
      // ini 2021-01-19
      CkEmissao.Checked  := False;
      CkVencto.Checked   := True;
      CkDataDoc.Checked  := False;
      CkDataComp.Checked := False;
      CkOrdem1.Checked   := True;
      CkOrdem2.Checked   := False;
      RGOrdem1.ItemIndex := 3;
      // fim 2021-01-19
    end;
    4:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
     LaTerceiro.Caption := 'Fornecedor';
     ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    5:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    6:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Fornecedor';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    7:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
      ////
      LaAccount.Visible := True;
      EdAccount.Visible := True;
      CBAccount.Visible := True;
      LaVendedor.Visible := True;
      EdVendedor.Visible := True;
      CBVendedor.Visible := True;
    end;
    8:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      GBPerdido.Visible := True;
    end;
    9:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      GBPerdido.Visible := True;
    end;
    10:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente / Fornecedor';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
      ////
      LaAccount.Visible := True;
      EdAccount.Visible := True;
      CBAccount.Visible := True;
      LaVendedor.Visible := True;
      EdVendedor.Visible := True;
      CBVendedor.Visible := True;
      //
    end;
  end;
  if not RGOrdem1.Enabled then RGOrdem1.ItemIndex := 0;
  if not RGOrdem2.Enabled then RGOrdem2.ItemIndex := 1;
  if Tipo in ([0,1]) then
  begin
    CkVencto.Enabled  := False;
    GBVencto.Enabled  := False;
    LaVenctI.Enabled  := False;
    LaVenctF.Enabled  := False;
    TPVctoIni.Enabled := False;
    TPVctoFim.Enabled := False;
  end else begin
    CkVencto.Enabled  := True;
    GBVencto.Enabled  := True;
    LaVenctI.Enabled  := True;
    LaVenctF.Enabled  := True;
    TPVctoIni.Enabled := True;
    TPVctoFim.Enabled := True;
  end;
end;

procedure TFmExtratos2.BtImprimeClick(Sender: TObject);
begin
  //BtImprime.Enabled := False;
  try
    FRelSel := RGTipo.ItemIndex;
    //
    FParar := False;
    //
    FEntidade := DModG.QrEmpresasCodigo.Value;
    if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa!') then Exit;
    FCliInt   := DModG.QrEmpresasFilial.Value;
    //
    DModG.Def_EM_ABD(TMeuDB, FEntidade, FCliInt, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
    //
    GeraRelatorio(tsrImpressao);
  finally
    //BtImprime.Enabled := True;
  end;
end;

procedure TFmExtratos2.BtPararClick(Sender: TObject);
begin
  FParar := True;
end;

procedure TFmExtratos2.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  ImgTipo.SQLType := stPsq;
// continuar aqui! testar no FmExtratos2 caso confirme ver na cria��o dos forms tabbed
  for I := 0 to FmExtratos2.ComponentCount - 1 do
  begin
    if FmExtratos2.Components[I] is TFrxDataSet then
    begin
      TFrxDataSet(FmExtratos2.Components[I]).Enabled := False;
      TFrxDataSet(FmExtratos2.Components[I]).OpenDataSource := False;
    end;
  end;
  //
  RGPagRec.ItemIndex := 0;
  //
  CreateDockableWindows;
  TPEmissIni.Date := Date -60;
  TPVctoIni.Date  := Date -60;
  TPDocIni.Date   := Date -90;
  TPDocFim.Date   := Date +60;
  TPCompIni.Date  := Date;
  TPCompFim.Date  := Date;
  RGTipo.ItemIndex := 1;
  UnDmkDAC_PF.AbreQuery(QrTerceiros, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrAccounts, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVendedores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSubGrupos, Dmod.MyDB);
  ///////
  EdAnos.Text := Geral.ReadAppKeyCU('Anos', 'Dermatek\Extratos\Venctos',
                ktString,'0');
  EdMeses.Text := Geral.ReadAppKeyCU('Meses', 'Dermatek\Extratos\Venctos',
                ktString,'0');
  EdDias.Text := Geral.ReadAppKeyCU('Dias', 'Dermatek\Extratos\Venctos',
                ktString,'0');
  CkAnos.Checked := Geral.ReadAppKeyCU('Ck_Anos', 'Dermatek\Extratos\Venctos',
                ktBoolean, False);
  CkMeses.Checked := Geral.ReadAppKeyCU('Ck_Meses', 'Dermatek\Extratos\Venctos',
                ktBoolean, False);
  CkDias.Checked := Geral.ReadAppKeyCU('Ck_Dias', 'Dermatek\Extratos\Venctos',
                ktBoolean, False);
  ///////
  EdAnos2.Text := Geral.ReadAppKeyCU('Anos', 'Dermatek\Extratos\Perdidos',
                ktString,'0');
  EdMeses2.Text := Geral.ReadAppKeyCU('Meses', 'Dermatek\Extratos\Perdidos',
                ktString,'0');
  EdDias2.Text := Geral.ReadAppKeyCU('Dias', 'Dermatek\Extratos\Perdidos',
                ktString,'0');
  CkAnos2.Checked := Geral.ReadAppKeyCU('Ck_Anos', 'Dermatek\Extratos\Perdidos',
                ktBoolean, False);
  CkMeses2.Checked := Geral.ReadAppKeyCU('Ck_Meses', 'Dermatek\Extratos\Perdidos',
                ktBoolean, False);
  CkDias2.Checked := Geral.ReadAppKeyCU('Ck_Dias', 'Dermatek\Extratos\Perdidos',
                ktBoolean, False);
  //
  QrExtrato.Close;
  QrExtrato.Database := DModG.MyPID_DB;
  //
  QrFlxo.Close;
  QrFlxo.Database := DModG.MyPID_DB;
  //
  CGPendencias.SetMaxValue;
  //
  RGOrdem1.Items[7] := dmkPF.TxtUH();
  RGOrdem2.Items[7] := dmkPF.TxtUH();
  //
  if VAR_KIND_DEPTO = kdObra then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDeptos, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM obrascab ',
    'ORDER BY Nome ',
    '']);
    //
    CkDepto.Caption := 'Obra:';
    PnDepto.Visible := True;
  end;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmExtratos2.R00_ImprimeExtratoConsolidado();
  procedure GeraParteSQL(TabLct: String);
  begin
    QrExtrato.SQL.Add('SELECT lct.*, cta.Nome NOMECONTA, car.Nome NOMECARTEIRA');
    QrExtrato.SQL.Add('FROM ' + TabLct + ' lct, ' + TMeuDB +
                      '.Contas cta, ' + TMeuDB + '.Carteiras car');
    QrExtrato.SQL.Add('WHERE lct.Data BETWEEN "' + Ext_EmisI + '" AND "' + Ext_EmisF + '"');
    QrExtrato.SQL.Add('AND (lct.Tipo=1 OR (lct.Tipo=0 AND Vencimento<="' + Ext_EmisF + '"))');
    QrExtrato.SQL.Add('AND cta.Codigo=lct.Genero');
    QrExtrato.SQL.Add('AND car.Codigo=lct.Carteira');
    QrExtrato.SQL.Add('AND car.ForneceI=' + FEntidade_TXT);
    QrExtrato.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);
    QrExtrato.SQL.Add(FSQLDepto_TXT);
    if CkExclusivo.Checked then
      QrExtrato.SQL.Add('AND car.Exclusivo = 0');

  end;
var
  FldIni, TabIni: String;
  Inicial: Double;
begin
  FldIni := UFinanceiro.DefLctFldSdoIni(TPEmissIni.Date, FDtEncer, FDtMorto);
  TabIni := UFinanceiro.DefLctTab(TPEmissIni.Date, FDtEncer, FDtMorto,
            FTabLcta, FTabLctB, FTabLctD);
  //
  if Uppercase(TabIni) = Uppercase(FTabLctA)then
  begin
    QrInicial.Close;
    QrInicial.SQL.Clear;
    QrInicial.SQL.Add('SELECT SUM(SdoFimB) SdoFimB');
    QrInicial.SQL.Add('FROM carteiras');
    QrInicial.SQL.Add('WHERE Tipo < 2');
    QrInicial.SQL.Add('AND ForneceI=' + FEntidade_Txt);
    if CkExclusivo.Checked then
      QrInicial.SQL.Add('AND Exclusivo = 0');
    UnDmkDAC_PF.AbreQuery(QrInicial, Dmod.MyDB);
    Inicial := QrInicialSdoFimB.Value;
  end else Inicial := 0;
  //
  QrAnterior.Close;
  QrAnterior.SQL.Clear;
  QrAnterior.SQL.Add('SELECT SUM(lct.Credito - lct.Debito)  Movimento');
  QrAnterior.SQL.Add('FROM ' + TabIni + ' lct');
  QrAnterior.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrAnterior.SQL.Add('WHERE lct.Data<"' + Ext_EmisI + '"');
  QrAnterior.SQL.Add('AND (lct.Tipo=1 OR (lct.Tipo=0 AND lct.Vencimento<"' + Ext_EmisI + '"))');
  QrAnterior.SQL.Add('AND car.ForneceI=' + FEntidade_TXT);
  QrAnterior.SQL.Add(FSQLDepto_TXT);
  if CkExclusivo.Checked then
    QrAnterior.SQL.Add('AND car.Exclusivo = 0');
  UnDmkDAC_PF.AbreQuery(QrAnterior, Dmod.MyDB);
  Ext_SdIni := Inicial + QrAnteriorMovimento.Value;
  QrAnterior.Close;
  //
  QrExtrato.Close;
  QrExtrato.SQL.Clear;
  QrExtrato.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_0;');
  QrExtrato.SQL.Add('CREATE TABLE _FIN_RELAT_005_0');
  QrExtrato.SQL.Add('');
  GeraParteSQL(FTabLctA);
  QrExtrato.SQL.Add('UNION');
  GeraParteSQL(FTabLctB);
  QrExtrato.SQL.Add('UNION');
  GeraParteSQL(FTabLctD);
  QrExtrato.SQL.Add(';');
  QrExtrato.SQL.Add('');
  QrExtrato.SQL.Add('SELECT * FROM _FIN_RELAT_005_0');
  QrExtrato.SQL.Add('ORDER BY Data, Carteira, Controle;');
  QrExtrato.SQL.Add('');
  QrExtrato.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_0;');
  QrExtrato.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrExtrato, DModG.MyPID_DB);
  //
  MyObjects.frxMostra(frxFin_Relat_005_00, 'Extrato');
  QrExtrato.Close;
end;

procedure TFmExtratos2.R01_ImprimeFluxoDeCaixa(SoGera: Boolean);
  function Fmt(Data: TDateTime): String;
  begin
    Result := '"' + FormatDateTime('YYYY-MM-DD', Data) + '"';
  end;
  procedure GeraParteSQL(TabLct, Quitacao, Sits: String);
  begin
    QrFlxo.SQL.Add('SELECT ' + Quitacao + ' QUITACAO, cta.Nome NOMECONTA, ');
    QrFlxo.SQL.Add('lct.Credito, lct.Debito, lct.Data, lct.Vencimento, ');
    QrFlxo.SQL.Add('lct.Compensado, lct.Descricao, lct.Qtde, lct.Documento, ');
    QrFlxo.SQL.Add('lct.SerieCH, lct.NotaFiscal, lct.Carteira, lct.Controle, ');
    QrFlxo.SQL.Add('lct.ID_Pgto, lct.Sub, lct.Sit, lct.Tipo, lct.Pago, ');
    QrFlxo.SQL.Add('car.Nome NOMECART, car.Tipo TP_CART, ');
    QrFlxo.SQL.Add('IF(lct.Cliente>0,');
    QrFlxo.SQL.Add('  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),');
    QrFlxo.SQL.Add('  IF (lct.Fornecedor>0,');
    QrFlxo.SQL.Add('    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO');
    QrFlxo.SQL.Add('FROM ' + TabLct + ' lct');
    QrFlxo.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrFlxo.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    cta ON cta.Codigo=lct.Genero');
    QrFlxo.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades cl ON cl.Codigo=lct.Cliente');
    QrFlxo.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades fo ON fo.Codigo=lct.Fornecedor');
    //
    QrFlxo.SQL.Add('WHERE car.Fornecei=' + IntToStr(FEmpresa));
    QrFlxo.SQL.Add(FSQLDepto_TXT);
    QrFlxo.SQL.Add(dmkPF.SQL_Periodo('AND ' + Quitacao,
      TPEmissIni.Date, TPEmissFim.Date, CkEmissao.Checked, CkEmissao.Checked));
    QrFlxo.SQL.Add('AND lct.Genero > 0');
    QrFlxo.SQL.Add('AND ((car.Tipo<>2) OR (lct.Sit IN (' + Sits + ')))');
    if CkExclusivo.Checked then
      QrFlxo.SQL.Add('AND car.Exclusivo = 0');
  end;
var
  Quitacao, Texto, Docum, NotaF: String;
  DataE, DataV, DataQ, DataX: Variant;
  I, Codig: Integer;
  Credito, Debito, Valor, Saldo, Inicial, SdoCr, SdoDB: Double;
  DataI: TDateTime;
  FldIni, TabIni: String;
  MaxVal, Sits: String;
  Relatorio: TfrxReport;
begin
  Screen.Cursor := crHourGlass;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela do saldo anterior');
  //
  PB1.Position := 0;
  PB1.Visible  := True;
  FldIni := UFinanceiro.DefLctFldSdoIni(TPEmissIni.Date, FDtEncer, FDtMorto);
  TabIni := UFinanceiro.DefLctTab(TPEmissIni.Date, FDtEncer, FDtMorto,
            FTabLcta, FTabLctB, FTabLctD);
  //
  Sits := '';
  for I := 0 to CGPendencias.Items.Count - 1 do
  begin
    if CGPendencias.Checked[I] then
      Sits := Sits + ',' + CGPendencias.Items[I][1];
  end;
  if Sits <> '' then
    Sits := Copy(Sits, 2)
  else
    Sits := '-999999999';
  //
  //   S A L D O    I N I C I A L
  if CkEmissao.Checked then DataI := TPEmissIni.Date else DataI := 0;
  //
  if Uppercase(TabIni) = Uppercase(FTabLctA)then
  begin
    QrInicial.Close;
    QrInicial.SQL.Clear;
    QrInicial.SQL.Add('SELECT SUM(SdoFimB) SdoFimB');
    QrInicial.SQL.Add('FROM carteiras');
    QrInicial.SQL.Add('WHERE Tipo < 2');
    QrInicial.SQL.Add('AND ForneceI=' + FEntidade_Txt);
    if CkExclusivo.Checked then
      QrInicial.SQL.Add('AND Exclusivo = 0');
    UnDmkDAC_PF.AbreQuery(QrInicial, Dmod.MyDB);
    Inicial := QrInicialSdoFimB.Value;
  end else Inicial := 0;
  QrAnterior.Close;
  QrAnterior.SQL.Clear;
  QrAnterior.SQL.Add('SELECT ');
  QrAnterior.SQL.Add('SUM(lct.Credito - lct.Debito) Movimento');
  QrAnterior.SQL.Add('FROM ' + TabIni + ' lct');
  QrAnterior.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrAnterior.SQL.Add('WHERE car.Tipo <> 2');
  QrAnterior.SQL.Add('AND car.ForneceI=' + FEntidade_TXT);
  QrAnterior.SQL.Add('AND lct.Data<"' + Geral.FDT(DataI, 1) + '"');
  QrAnterior.SQL.Add(FSQLDepto_TXT);
  if CkExclusivo.Checked then
    QrAnterior.SQL.Add('AND car.Exclusivo = 0');
  UnDmkDAC_PF.AbreQuery(QrAnterior, Dmod.MyDB);

  //   L A N � A M E N T O S
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela de lan�amentos');
  //
  // 2014-10-30
(*
  Quitacao := 'Date(IF(car.Tipo <> 2, lct.Data,IF(lct.Sit IN (2,3), lct.Compensado, ' +
  'IF(lct.Vencimento<SYSDATE(), IF(lct.Debito>0, ' + Fmt(TPDeb.Date) +
  ', ' + Fmt(TPCre.Date) + '), lct.Vencimento))))';
*)
  Quitacao :=
  'Date(IF(car.Tipo <> 2, lct.Data,IF(lct.Sit IN (2,3), lct.Compensado, ' +
  'IF(lct.Vencimento<"' + Geral.FDT(DModG.ObtemAgora(), 1) + '", IF(lct.Debito>0, '
  + Fmt(TPDeb.Date) + ', ' + Fmt(TPCre.Date) + '), lct.Vencimento))))';
  // FIM 2014-10-30
  //
  QrFlxo.Close;
  QrFlxo.SQL.Clear;
  QrFlxo.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_1;');
  QrFlxo.SQL.Add('CREATE TABLE _FIN_RELAT_005_1');
  QrFlxo.SQL.Add('');
  GeraParteSQL(FTabLctA, Quitacao, Sits);
  QrFlxo.SQL.Add('UNION');
  GeraParteSQL(FTabLctB, Quitacao, Sits);
  QrFlxo.SQL.Add('UNION');
  GeraParteSQL(FTabLctD, Quitacao, Sits);
  QrFlxo.SQL.Add(';');
  QrFlxo.SQL.Add('');
  QrFlxo.SQL.Add('SELECT * FROM _FIN_RELAT_005_1');
  QrFlxo.SQL.Add('ORDER BY QUITACAO, Credito DESC, Debito;');
  QrFlxo.SQL.Add('');
  //QrFlxo.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_1;');
  QrFlxo.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrFlxo, DModG.MyPID_DB);
  //
  PB1.Max := QrFlxo.RecordCount + 1;

  // INSERE DADOS TABELA LOCAL
  UCriar.RecriaTempTable('extratocc2', DModG.QrUpdPID1, False);
  //
  Codig := 1;
  PB1.Position := Codig;
  PB1.Update;
  Application.ProcessMessages;
  //
  Saldo := Inicial + QrAnteriorMovimento.Value;
  //
  UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'ExtratoCC2', false,
  [
    'DataE', 'DataV', 'DataQ',
    'DataX', 'Texto', 'Docum',
    'NotaF', 'Credi', 'Debit',
    'Saldo', 'CartC', 'CartN',
    'TipoI', 'Ctrle', 'ID_Pg'
  ], ['Codig'], [
  // Ini 2019-01-24
    //Null, Null, Null,
    //dmkPF.FDT_NULL(DataI, 1), 'SALDO ANTERIOR', '',
    '0000-00-00', '0000-00-00', '0000-00-00',
    Geral.FDT(DataI, 1), 'SALDO ANTERIOR', '',
  // Fim 2019-01-24
    '', 0, 0,
    Saldo, 0, '*** T O D A S ***', 1, 0, 0
  ], [Codig], False);
  //
  MaxVal := Geral.FF0(QrFlxo.RecordCount + 1);
  QrFlxo.First;
  BtParar.Visible := True;
  //
  while not QrFlxo.Eof do
  begin
    if FParar then
    begin
      FParar := False;
      BtParar.Visible := False;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      PB1.Position := 0;
      Screen.Cursor := crDefault;
      Exit;
    end;
    Codig := Codig + 1;

    PB1.Position := Codig;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Calculado saldo do item ' +
      Geral.FF0(Codig) + ' de ' + MaxVal);
    PB1.Update;
    Application.ProcessMessages;

    if not DmodFin.DefineValorSit(QrFlxoTipo.Value, QrFlxoSit.Value,
      QrFlxoCredito.Value, QrFlxoDebito.Value, QrFlxoPago.Value, True, True, 
      Credito, Debito, Valor, SdoCr, SdoDB) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    Saldo := Saldo + Valor;
  // Ini 2019-01-24
    (*
    DataE := dmkPF.FDT_NULL(QrFlxoData.Value, 1);
    DataV := dmkPF.FDT_NULL(QrFlxoVencimento.Value, 1);
    DataQ := dmkPF.FDT_NULL(QrFlxoCompensado.Value, 1);
    *)
    DataE := Geral.FDT(QrFlxoData.Value, 1);
    DataV := Geral.FDT(QrFlxoVencimento.Value, 1);
    DataQ := Geral.FDT(QrFlxoCompensado.Value, 1);
  // Ini 2019-01-24
    DataX := QrFlxoQUITACAO.Value;
    //
    if CkSubstituir.Checked then
      Texto := QrFlxoNOMERELACIONADO.Value
    else
      Texto := QrFlxoDESCRI_TXT.Value;
    //
    if QrFlxoQtde.Value > 0 then
      Texto := FloatToStr(QrFlxoQtde.Value) + ' ' + Texto;
    if QrFlxoDocumento.Value > 0 then
      Docum := FormatFloat('000000', QrFlxoDocumento.Value) else Docum := '';
    if QrFlxoSerieCH.Value <> '' then Docum := QrFlxoSerieCH.Value + Docum;
    if QrFlxoNotaFiscal.Value > 0 then
      NotaF := FormatFloat('000000', QrFlxoNotaFiscal.Value) else NotaF := '';
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'ExtratoCC2', false,
    [
      'DataE', 'DataV', 'DataQ',
      'DataX', 'Texto', 'Docum',
      'NotaF', 'Credi', 'Debit',
      'Saldo', 'CartC', 'CartN',
      'TipoI', 'Ctrle', 'ID_Pg',
      'CtSub', 'SdoCr', 'SdoDb'
    ], ['Codig'], [
      DataE, DataV, DataQ,
      DataX, Texto, Docum,
      NotaF, Credito, Debito,
      Saldo, QrFlxoCarteira.Value, QrFlxoNOMECART.Value,
      2, QrFlxoControle.Value, QrFlxoID_Pgto.Value,
      QrFlxoSub.Value, SdoCr, SdoDB
    ], [Codig], False);
    //
    QrFlxo.Next;
  end;
  BtParar.Visible := False;
  PB1.Visible := False;

  if SoGera then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela do extrato calculado');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrExtr, DModG.MyPID_DB, [
    'SELECT *, ',
    'IF(DataE <= "1899-12-30", "", DATE_FORMAT(DataE, "%d/%m/%y")) DataE_TXT, ',
    'IF(DataV <= "1899-12-30", "", DATE_FORMAT(DataV, "%d/%m/%y")) DataV_TXT, ',
    'IF(DataQ <= "1899-12-30", "", DATE_FORMAT(DataQ, "%d/%m/%y")) DataQ_TXT, ',
    'IF(DataX <= "1899-12-30", "", DATE_FORMAT(DataX, "%d/%m/%y")) DataX_TXT ',
    'FROM extratocc2 ',
    'ORDER BY TipoI, DataX, Credi DESC, Debit ',
    '']);
  Screen.Cursor := crDefault;

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando relat�rio');
  //
  case RGFonte.ItemIndex of
    0: Relatorio := frxFin_Relat_005_01_A06;
    1: Relatorio := frxFin_Relat_005_01_A07;
    2: Relatorio := frxFin_Relat_005_01_A08;
    else
    begin
      Relatorio := nil;
      Geral.MB_Erro('Relat�rio indefinido (01)');
    end;
  end;
  if Relatorio <> nil then
  begin
    MyObjects.frxDefineDataSets(Relatorio, [
    DModG.frxDsDono,
    frxDsExtr
    ]);
    MyObjects.frxMostra(Relatorio, 'Relat�rio de Fluxo de Caixa');
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  Screen.Cursor := crDefault;
end;


procedure TFmExtratos2.R02_ImprimeContasAPagar(Sender: TObject;
 Saida: TTipoSaidaRelatorio);
var
  CtrlPai: Double;
  DockWindow: TFmDockForm;
  Maximo, Conta: Integer;
  RelFrx: TfrxReport;
begin
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  VAR_ORDENAR := RGOrdem1.ItemIndex * 10 + RGOrdem2.ItemIndex;
  ////
  SQLContasAPagar(QrPagRecX,   0);
  if FRelSel in ([4,5]) then
    // ERRADO! mudei 2008.07.22
    //SQLContasAPagar(QrPagRec,    0)
    SQLContasAPagar(QrPagRec,    3)
  else
    SQLContasAPagar(QrPagRec,    1);
  // M L A G e r a l.LeMeuSQL(QrPagRec, '', nil, True, True);
  SQLContasAPagar(QrPagRecIts, 2);
  //
  if FRelSel in ([6,7]) then
  begin
    DockWindow := DockWindows[(Sender as TComponent).Tag];
    DockWindow.Show;
    DockWindow.Refresh;
    UCriar.RecriaTempTable('PagRec1', DModG.QrUpdPID1, False);
    UCriar.RecriaTempTable('PagRec2', DModG.QrUpdPID1, False);
    Maximo := QrPagRecX.RecordCount;
    QrPagRecX.First;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO pagrec1 SET ');
    DModG.QrUpdPID1.SQL.Add('Data=:P0, Documento=:P1, NotaFiscal=:P2, ');
    DModG.QrUpdPID1.SQL.Add('NOMEVENCIDO=:P3, NOMECONTA=:P4, Descricao=:P5, ');
    DModG.QrUpdPID1.SQL.Add('Controle=:P6, Valor=:P7, MoraDia=:P8, ');
    DModG.QrUpdPID1.SQL.Add('PAGO_REAL=:P9, ATRAZODD=:P10, ATUALIZADO=:P11, ');
    DModG.QrUpdPID1.SQL.Add('MULTA_REAL=:P12, DataDoc=:P13, Vencimento=:P14, ');
    DModG.QrUpdPID1.SQL.Add('TERCEIRO=:P15, NOME_TERCEIRO=:P16, CtrlPai=:P17, ');
    DModG.QrUpdPID1.SQL.Add('Tipo=:P18, Pendente=:P19, ID_Pgto=:P20, ');
    DModG.QrUpdPID1.SQL.Add('CtrlIni=:P21, Duplicata=:P22, Qtde=:P23, ');
    DModG.QrUpdPID1.SQL.Add('NO_UH=:P24, Qtd2=:P25 ');
    Screen.Cursor := VAR_CURSOR;
    Conta := 0;
    DockWindow.AtualizaDados(Conta, Maximo);
    while not QrPagRecX.Eof do
    begin
      Conta := Conta + 1;
      DockWindow.AtualizaDados(Conta, Maximo);
      Update;
      Application.ProcessMessages;
      if VAR_PARAR then
      begin
        UCriar.RecriaTempTable('PagRec1', DModG.QrUpdPID1, False);
        UCriar.RecriaTempTable('PagRec2', DModG.QrUpdPID1, False);
        Exit;
      end;
      if QrPagRecXCtrlIni.Value > 0 then
        CtrlPai := QrPagRecXCtrlIni.Value
      else CtrlPai := QrPagRecXControle.Value;
      DModG.QrUpdPID1.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, QrPagRecXData.Value);
      DModG.QrUpdPID1.Params[01].AsFloat   := QrPagRecXDocumento.Value;
      DModG.QrUpdPID1.Params[02].AsFloat   := QrPagRecXNotaFiscal.Value;
      DModG.QrUpdPID1.Params[03].AsString  := QrPagRecXNOMEVENCIDO.Value;
      DModG.QrUpdPID1.Params[04].AsString  := QrPagRecXNOMECONTA.Value;
      DModG.QrUpdPID1.Params[05].AsString  := QrPagRecXDescricao.Value;
      DModG.QrUpdPID1.Params[06].AsFloat   := QrPagRecXControle.Value;
      DModG.QrUpdPID1.Params[07].AsFloat   := QrPagRecXValor.Value;
      DModG.QrUpdPID1.Params[08].AsFloat   := QrPagRecXMoraDia.Value;
      DModG.QrUpdPID1.Params[09].AsFloat   := QrPagRecXPAGO_REAL.Value;
      DModG.QrUpdPID1.Params[10].AsFloat   := QrPagRecXATRAZODD.Value;
      DModG.QrUpdPID1.Params[11].AsFloat   := QrPagRecXATUALIZADO.Value;
      DModG.QrUpdPID1.Params[12].AsFloat   := QrPagRecXMULTA_REAL.Value;
      //
      DModG.QrUpdPID1.Params[13].AsString  := FormatDateTime(VAR_FORMATDATE, QrPagRecXDataDoc.Value);
      DModG.QrUpdPID1.Params[14].AsString  := FormatDateTime(VAR_FORMATDATE, QrPagRecXVencimento.Value);
      DModG.QrUpdPID1.Params[15].AsFloat   := QrPagRecXTerceiro.Value;
      DModG.QrUpdPID1.Params[16].AsString  := QrPagRecXNOME_TERCEIRO.Value;
      DModG.QrUpdPID1.Params[17].AsFloat   := CtrlPai;
      DModG.QrUpdPID1.Params[18].AsFloat   := QrPagRecXTipo.Value;
      DModG.QrUpdPID1.Params[19].AsFloat   := QrPagRecXPENDENTE.Value;
      DModG.QrUpdPID1.Params[20].AsFloat   := QrPagRecXID_Pgto.Value;
      DModG.QrUpdPID1.Params[21].AsFloat   := QrPagRecXCtrlIni.Value;
      DModG.QrUpdPID1.Params[22].AsString  := QrPagRecXDuplicata.Value;
      DModG.QrUpdPID1.Params[23].AsFloat   := QrPagRecXQtde.Value;
      DModG.QrUpdPID1.Params[24].AsString  := QrPagRecXNO_UH.Value;
      DModG.QrUpdPID1.Params[25].AsFloat   := QrPagRecXQtd2.Value;

      DModG.QrUpdPID1.ExecSQL;
      QrPagRecX.Next;
    end;
    DockWindow.Hide;
    with QrPagRec1.SQL do
    begin
      Clear;
      Add('SELECT *, ');
      Add('IF(Data <= "1899-12-30", "", DATE_FORMAT(Data, "%d/%m/%y")) Data_TXT, ');
      Add('IF(DataDoc <= "1899-12-30", "", DATE_FORMAT(DataDoc, "%d/%m/%y")) DataDoc_TXT, ');
      Add('IF(Vencimento <= "1899-12-30", "", DATE_FORMAT(Vencimento, "%d/%m/%y")) Vencimento_TXT ');
      Add('FROM pagrec1');
      Add(SQL_Ordenar());
    end;
    UnDmkDAC_PF.AbreQuery(QrPagRec1, DModG.MyPID_DB);
    case RGHistorico.ItemIndex of
      0: Relfrx := frxFin_Relat_005_02_B1;
      1: Relfrx := frxFin_Relat_005_02_B2;
      else Relfrx := nil;
    end;
  end else
  begin
    if FRelSel in ([2,3]) then
    begin
      case RGFonte.ItemIndex of
        0: RelFrx := frxFin_Relat_005_03_A06;
        1: RelFrx := frxFin_Relat_005_03_A07;
        2: RelFrx := frxFin_Relat_005_03_A08;
        else RelFrx := nil;
      end;
    end else
    if FRelSel in ([4,5]) then
    begin
      case RGFonte.ItemIndex of
        0: RelFrx := frxFin_Relat_005_04_A06;
        1: RelFrx := frxFin_Relat_005_04_A07;
        2: RelFrx := frxFin_Relat_005_04_A08;
        else RelFrx := nil;
      end;
    end else
    begin
      case RGFonte.ItemIndex of
        0: RelFrx := frxFin_Relat_005_02_A06;
        1: RelFrx := frxFin_Relat_005_02_A07;
        2: RelFrx := frxFin_Relat_005_02_A08;
        else RelFrx := nil;
      end;
    end;
  end;
  if Relfrx <> nil then
  begin
    MyObjects.frxDefineDataSets(Relfrx, [
    DModG.frxDsDono,
    frxdsPagRec,
    frxdsPagRec1
    ]);
    MyObjects.frxMostra(Relfrx, 'Hist�rico de lan�amentos');
  end else
    Geral.MB_Erro('Relat�rio n�o definido!');
{ ini 2021-01-18
  QrPagRec1.Close;
  QrPagRec.Close;
  QrPagRecIts.Close;
    fim 2021-01-18}
  Screen.Cursor := VAR_CURSOR;
end;

procedure TFmExtratos2.R10_ImprimeContasPagRec(Sender: TObject;
  Saida: TTipoSaidaRelatorio);
var
  CtrlPai: Double;
  DockWindow: TFmDockForm;
  Maximo, Conta: Integer;
  RelFrx: TfrxReport;
  //
  Vencimento, NO_CTA: String;
  DiaSem, Tipo, Genero: Integer;
  Valor01, Valor02, Valor03, Valor04, Valor05, Valor06, Valor07, Valor08: Double;
  procedure InsereAtual(Valor: Double);
  begin
    DiaSem         := Trunc(QrGruSemVencimento.Value) - FDia1 + 1;
    Vencimento     := Geral.FDT(QrGruSemVencimento.Value, 1);
    Genero         := QrGruSemGenero.Value;
    NO_CTA         := QrGruSemNO_CTA.Value;
    Valor01        := 0;
    Valor02        := 0;
    Valor03        := 0;
    Valor04        := 0;
    Valor05        := 0;
    Valor06        := 0;
    Valor07        := 0;
    Valor08        := 0;
    case DiaSem of
      01: Valor01  := Valor;
      02: Valor02  := Valor;
      03: Valor03  := Valor;
      04: Valor04  := Valor;
      05: Valor05  := Valor;
      06: Valor06  := Valor;
      07: Valor07  := Valor;
      else Valor08 := Valor;
    end;
    //
    //if
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FPagRecSem, False, [
    'Tipo', 'Vencimento', 'Genero',
    'NO_CTA', 'Valor01', 'Valor02',
    'Valor03', 'Valor04', 'Valor05',
    'Valor06', 'Valor07', 'Valor08'], [
    ], [
    Tipo, Vencimento, Genero,
    NO_CTA, Valor01, Valor02,
    Valor03, Valor04, Valor05,
    Valor06, Valor07, Valor08], [
    ], False);
  end;
begin
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  FDia1 := Trunc(TPVctoIni.Date);
  VAR_ORDENAR := RGOrdem1.ItemIndex * 10 + RGOrdem2.ItemIndex;
  ////
  //SQLContasAPagar(QrPagRecX,   0);

  //
  FPagRecSem := UCriarFin.RecriaTempTableNovo(ntrtt_PagRecSem, DMOdG.QrUpdPID1, False);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando d�bitos!');
  FRelSel := 2;
  SQLContasAPagar(QrPagRec, 1);
  UnDmkDAC_PF.AbreMySQLQuery0(QrGruSem, DModG.MyPID_DB, [
  'SELECT fr5.Vencimento, fr5.Genero, ',
  'SUM(fr5.Credito) Credito, SUM(fr5.Debito) Debito, ',
  'cta.Nome NO_CTA ',
  'FROM _fin_relat_005_2 fr5 ',
  'LEFT JOIN ' + TMeuDB + '.Contas cta ON cta.Codigo=fr5.Genero ',
  'GROUP BY fr5.Vencimento, fr5.Genero ',
  '']);
  Tipo := FRelSel;
  QrGruSem.First;
  while not QrGruSem.Eof do
  begin
    InsereAtual(-QrGruSemDebito.Value);
    //
    QrGruSem.Next;
  end;
  //

  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando cr�ditos!');
  FRelSel := 3;
  SQLContasAPagar(QrPagRec, 1);
  UnDmkDAC_PF.AbreMySQLQuery0(QrGruSem, DModG.MyPID_DB, [
  'SELECT fr5.Vencimento, fr5.Genero, ',
  'SUM(fr5.Credito) Credito, SUM(fr5.Debito) Debito, ',
  'cta.Nome NO_CTA ',
  'FROM _fin_relat_005_2 fr5 ',
  'LEFT JOIN ' + TMeuDB + '.Contas cta ON cta.Codigo=fr5.Genero ',
  'GROUP BY fr5.Vencimento, fr5.Genero ',
  '']);
  Tipo := FRelSel;
  QrGruSem.First;
  while not QrGruSem.Eof do
  begin
    InsereAtual(QrGruSemCredito.Value);
    //
    //
    QrGruSem.Next;
  end;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(Qr10, DModG.MyPID_DB, [
  'SELECT Tipo, Genero, NO_CTA,  ',
  'SUM(Valor01) Valor01,  SUM(Valor02) Valor02,  ',
  'SUM(Valor03) Valor03,  SUM(Valor04) Valor04,  ',
  'SUM(Valor05) Valor05,  SUM(Valor06) Valor06,  ',
  'SUM(Valor07) Valor07,  SUM(Valor08) Valor08, ',
  'ELT(Tipo, "???", "DESPESAS", "RECEITAS", "???") NO_TIPO  ',
  'FROM ' + FPagRecSem,
  'GROUP BY Tipo, Genero ',
  'ORDER BY Tipo, NO_CTA ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxFin_Relat_005_10_A, [
  DModG.frxDsDono,
  frxDs10
  ]);
  MyObjects.frxMostra(frxFin_Relat_005_10_A, 'Receitas e despesas por conta');
  //
  QrPagRec.Close;
  Qr10.Close;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  Screen.Cursor := VAR_CURSOR;
end;

procedure TFmExtratos2.R11_ImprimeContasPagRec(Sender: TObject;
  Saida: TTipoSaidaRelatorio);
const
  TipoI2 = 2;
var
  Dia: TDateTime;
  Dias: Integer;
  NewData: String;
  //
  Data: String;
  SaldoAnt, Receber, Disponiv, Pagar, Acumulou: Double;
  Qry: TmySQLQuery;
begin
  if MyObjects.FIC(RGFlxCxaMes_FimSem.ItemIndex < 1, nil,
  '"Fins de semana do fluxo de caixa mensal" n�o definido!') then
    Exit;
  //
  R01_ImprimeFluxoDeCaixa(True);
  FFlxCxaMes := UCriarFin.RecriaTempTableNovo(ntrtt_FlxCxaMes, DMOdG.QrUpdPID1, False);
  //
  if RGFlxCxaMes_FimSem.ItemIndex = 2 then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Alterando s�bados e domingos!');
    //
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, DmodG.MyPID_DB, [
      'SELECT DISTINCT DataX, WEEKDAY(DataX) DiaSem ',
      'FROM extratocc2 ',
      'WHERE WEEKDAY(DataX) IN (5,6) ',
      'AND TipoI=2 ',
      '']);
      //
      Qry.First;
      while not Qry.Eof do
      begin
        if Qry.FieldByName('DiaSem').AsInteger = 5 then
          Dias := 2
        else
          Dias := 1;
        //
        Dia := Qry.FieldByName('DataX').AsDateTime;
        Data := Geral.FDT(Dia, 1);
        Dia := Dia + Dias;
        NewData := Geral.FDT(Dia, 1);
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, 'extratocc2', False, [
        'DataX'], ['DataX', 'TipoI'], [NewData], [Data, TipoI2], False);
        //
        Qry.Next;
      end;
    finally
      Qry.Free;
    end;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando dados gerados!');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFCMIni, DmodG.MyPID_DB, [
  'SELECT SUM(Saldo) Saldo ',
  'FROM extratocc2 ',
  'WHERE TipoI=1 ',
  '']);
  SaldoAnt := QrFCMIniSaldo.Value;
  Acumulou := SaldoAnt;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFCMMov, DmodG.MyPID_DB, [
  'SELECT DataX, SUM(SdoCr) Credi, SUM(SdoDb) Debit ',
  'FROM extratocc2 ',
  'WHERE TipoI=2 ',
  'GROUP BY DataX ',
  'ORDER BY DataX ',
  '']);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando dados diarios!');

  QrFCMMov.First;
  while not QrFCMMov.Eof do
  begin
    Dia :=  QrFCMMovDataX.Value;
    //
    Data           := Geral.FDT(Dia, 1);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando dados diarios! ' + Data);
    SaldoAnt       := Acumulou;
    Receber        := QrFCMMovCredi.Value;
    Disponiv       := Acumulou + Receber;
    Pagar          := QrFCMMovDebit.Value;
    Acumulou       := Disponiv - Pagar;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FFlxCxaMes, False, [
    'SaldoAnt', 'Receber', 'Disponiv',
    'Pagar', 'Acumulou'], [
    'Data'], [
    SaldoAnt, Receber, Disponiv,
    Pagar, Acumulou], [
    Data], False);
    //
    QrFCMMov.Next;
  end;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrFCMDia, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM _flxcxames_ ',
  'ORDER BY Data ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxFin_Relat_005_11_A, [
  DModG.frxDsDono,
  frxDsFCMIni,
  frxDsFCMDia
  ]);
  MyObjects.frxMostra(frxFin_Relat_005_11_A, 'Fluxo de caixa sint�tico - por dia');
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  Screen.Cursor := VAR_CURSOR;
end;

procedure TFmExtratos2.ReopenCarteiras;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCarteiras, Dmod.MyDB, [
    'SELECT Codigo, Nome',
    'FROM carteiras',
    'WHERE ForneceI=' + Geral.FF0(DModG.QrEmpresasCodigo.Value),
    'ORDER BY Nome',
    '']);
end;

procedure TFmExtratos2.SQLContasAPagar(Query: TmySQLQuery; Tipo: Integer);
  procedure GeraParteSQL(TabLct: String);
  var
    Anos, Meses, Dias: Word;
  begin
    Query.SQL.Add('SELECT lct.*, co.Nome NOMECONTA, ca.Nome NOMECARTEIRA,');
    Query.SQL.Add('IF(lct.Sit IN (0, 5, 6), (lct.Credito-lct.Debito),');
    Query.SQL.Add('IF(lct.Sit=1, (lct.Credito-lct.Debito-lct.Pago), 0)) SALDO,');
    Query.SQL.Add('co.Credito EhCRED, co.Debito EhDEB, ');
    Query.SQL.Add('IF ((lct.Fornecedor>0) AND (fo.Tipo=0), fo.RazaoSocial, ');
    Query.SQL.Add('IF ((lct.Fornecedor>0) AND (fo.Tipo=1), fo.Nome, ');
    Query.SQL.Add('IF ((lct.Cliente>0) AND (cl.Tipo=0), cl.RazaoSocial, ');
    Query.SQL.Add('IF ((lct.Cliente>0) AND (cl.Tipo=1), cl.Nome, "???")))) NOME_TERCEIRO, ');
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
    //Query.SQL.Add('CASE WHEN lct.Fornecedor>0 then lct.Fornecedor ELSE lct.Cliente END TERCEIRO,');
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
    // 2014-04-17 - Lcts vistos pela "Caracteristica da Carteira"  e nao pelo
    //              debito ou credito do lancamento!
(*
  'IF(fcd.PagRec=-1, -(fcd.ValARec-fcd.ValAPag), ',
  '    IF(fcd.PagRec=1, 0, fcd.ValAPag)) IMP_ValAPag, ',
  'IF(fcd.PagRec=-1, 0, IF(fcd.PagRec=1, ',
  '   (fcd.ValARec-fcd.ValAPag), fcd.ValARec)) IMP_ValARec, ',
  //
*)
    // FIM 2014-04-17
    Query.SQL.Add('0 CtrlPai, ');
    case VAR_KIND_DEPTO of
      kdUH:     Query.SQL.Add('imv.Unidade NO_UH');
      kdObra:   Query.SQL.Add('obr.Sigla NO_UH');
      kdOS1:    Query.SQL.Add('"" NO_UH');
      kdNenhum: Query.SQL.Add('"" NO_UH');
      else
      begin
        Query.SQL.Add('"" NO_UH');
        dmkPF.InfoKIND_DEPTO('SQLContasAPAgar');
      end;
    end;
    Query.SQL.Add('FROM ' + TabLct + ' lct');
    Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=lct.Genero');
    Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=co.SubGrupo');
    Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras ca ON ca.Codigo=lct.Carteira');
    Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades cl ON cl.Codigo=lct.Cliente');
    Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades fo ON fo.Codigo=lct.Fornecedor');

    case VAR_KIND_DEPTO of
      kdUH:
        Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.condimov imv ON imv.Conta=lct.Depto');
      kdObra:
        Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.obrascab obr ON obr.Codigo=lct.Depto');
    end;

    Query.SQL.Add('WHERE lct.Carteira<>0');
    Query.SQL.Add('AND lct.Genero<>-1');

    Query.SQL.Add(FSQLDepto_TXT);

    //  2010-08-02 - N�o usa mais Genero=-5 > Outras carteiras!
    Query.SQL.Add('AND ( lct.Tipo<>2 OR (lct.Tipo=2 AND Sit IN (0,1,5,6)) )');
    // fim 2010-08-02
    Query.SQL.Add('AND ca.ForneceI =' + FormatFloat('0', FEmpresa));

    //if FRelSel in ([6,7]) then
    if CkEmissao.Checked then
      Query.SQL.Add('AND lct.Data BETWEEN "'+Ext_EmisI+'" AND "'+Ext_EmisF+'"');
    //else
    if CkVencto.Checked then
      Query.SQL.Add('AND lct.Vencimento BETWEEN "'+Ext_VctoI+'" AND "'+Ext_VctoF+'"');
    if CkDataDoc.Checked then
      Query.SQL.Add('AND lct.DataDoc BETWEEN "'+Ext_DocI+'" AND "'+Ext_DocF+'"');
    if CkDataComp.Checked then
    begin
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE IGNORE ' + FTabLctA + ' SET Compensado=Vencimento');
      Dmod.QrUpdU.SQL.Add('WHERE Tipo<>2');
      Dmod.QrUpdU.ExecSQL;
      Query.SQL.Add('AND lct.Compensado BETWEEN "'+Ext_CompI+'" AND "'+Ext_CompF+'"');
    end;
    if FRelSel in ([2,3]) then
    begin
      Query.SQL.Add('AND lct.Tipo=2');
      Query.SQL.Add('AND lct.Sit IN (0,1,5,6)');
    end else
    if FRelSel in ([5]) then
    begin
      //Query.SQL.Add('AND lct.Tipo=2');
      Query.SQL.Add('AND lct.Sit IN (2,3)');
    end;
    if FRelSel in ([2,4,6]) then
    begin
      if RGPagRec.ItemIndex = 0 then
        Query.SQL.Add('AND lct.Debito>0')
      else
        Query.SQL.Add('AND ca.PagRec IN (-1,0)');
      //
      if EdTerceiro.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Fornecedor ='+IntToStr(CBTerceiro.KeyValue));
    end else if FRelSel in ([3,5,7]) then
    begin
      if RGPagRec.ItemIndex = 0 then
        Query.SQL.Add('AND lct.Credito>0')
      else
        Query.SQL.Add('AND ca.PagRec IN (1,0)');
      //
      if EdTerceiro.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Cliente='+IntToStr(CBTerceiro.KeyValue));
    end;
    if FRelSel in ([3,7]) then
    begin
      if EdAccount.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Account ='+IntToStr(CBAccount.KeyValue));
      if EdVendedor.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Vendedor ='+IntToStr(CBVendedor.KeyValue));
    end;
    if CkNiveis.Checked then
    begin
      if FRelSel in ([2,4,6]) then
      Query.SQL.Add('AND fo.Nivel BETWEEN "'+EdA.Text+'" AND "'+EdZ.Text+'"');
      if FRelSel in ([3,5,7]) then
      Query.SQL.Add('AND cl.Nivel BETWEEN "'+EdA.Text+'" AND "'+EdZ.Text+'"');
    end;
    if CkOmiss.Checked then
    begin
      if CkAnos.Checked  then Anos :=  Geral.IMV(EdAnos.Text)  else Anos := 0;
      if CkMeses.Checked then Meses := Geral.IMV(EdMeses.Text) else Meses := 0;
      if CkDias.Checked  then Dias :=  Geral.IMV(EdDias.Text)  else Dias := 0;
      Ext_Vencto := IncMonth(Date, -1 * ((Anos*12)+Meses)) - Dias;
      Query.SQL.Add('AND lct.Vencimento >= "'+
      FormatDateTime(VAR_FORMATDATE, Ext_Vencto)+'"');
    end;
    if FRelSel in ([8,9]) then
    begin
      if CkAnos2.Checked  then Anos :=  Geral.IMV(EdAnos2.Text)  else Anos := 0;
      if CkMeses2.Checked then Meses := Geral.IMV(EdMeses2.Text) else Meses := 0;
      if CkDias2.Checked  then Dias :=  Geral.IMV(EdDias2.Text)  else Dias := 0;
      Ext_Vencto := IncMonth(Date, -1 * ((Anos*12)+Meses)) - Dias;
      Query.SQL.Add('AND lct.Vencimento < "'+
      FormatDateTime(VAR_FORMATDATE, Ext_Vencto)+'"');
    end;
    //
    if FRelSel in ([2,3,4,5,6,7,8,9]) then
    begin
      if EdConta.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Genero ='+IntToStr(CBConta.KeyValue));
      if EdSubGrupo.ValueVariant <> 0 then
        Query.SQL.Add('AND sgr.Codigo ='+IntToStr(EdSubGrupo.ValueVariant));
      if EdCarteira.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Carteira =' + Geral.FF0(CBCarteira.KeyValue));
    end;
    case Tipo of
      0: Query.SQL.Add('');
      1: Query.SQL.Add('AND ID_Pgto=0');
      2: Query.SQL.Add('AND ID_Pgto<>0');
      // Novo 2008.07.22
      3: Query.SQL.Add('AND Compensado>0');
    end;
  end;
var
  NomeTempTab: String;
begin
  VAR_SEQ_TMP_REL := VAR_SEQ_TMP_REL + 1;
  NomeTempTab := '_FIN_RELAT_005_2_' + IntToStr(VAR_SEQ_TMP_REL);
  //
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE IF EXISTS ' + NomeTempTab + ';');
  Query.SQL.Add('CREATE TABLE ' + NomeTempTab + '');
  Query.SQL.Add('');
  GeraParteSQL(FTabLctA);
  Query.SQL.Add('UNION');
  GeraParteSQL(FTabLctB);
  Query.SQL.Add('UNION');
  GeraParteSQL(FTabLctD);
  Query.SQL.Add(';');
  Query.SQL.Add('');
  Query.SQL.Add('SELECT *, ');
  Query.SQL.Add('IF(Data <= "1899-12-30", "", DATE_FORMAT(Data, "%d/%m/%y")) Data_TXT, ');
  Query.SQL.Add('IF(DataDoc <= "1899-12-30", "", DATE_FORMAT(DataDoc, "%d/%m/%y")) DataDoc_TXT, ');
  Query.SQL.Add('IF(Vencimento <= "1899-12-30", "", DATE_FORMAT(Vencimento, "%d/%m/%y")) Vencimento_TXT ');
  Query.SQL.Add('FROM ' + NomeTempTab + '');
  Query.SQL.Add(SQL_Ordenar() + ';');
  Query.SQL.Add('');
  Query.SQL.Add('DROP TABLE IF EXISTS ' + NomeTempTab + ';');
  Query.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(Query, DModG.MyPID_DB);
end;

function TFmExtratos2.SQL_Ordenar(): String;
begin
  case VAR_ORDENAR of
    00: Result := 'ORDER BY NOME_TERCEIRO, DataDoc, CtrlPai, Data, Vencimento, Controle';
    01: Result := 'ORDER BY NOME_TERCEIRO, DataDoc, CtrlPai, Data, Vencimento, Controle';
    02: Result := 'ORDER BY NOME_TERCEIRO, Data, CtrlPai, DataDoc, Vencimento, Controle';
    03: Result := 'ORDER BY NOME_TERCEIRO, Vencimento, CtrlPai, DataDoc, Data, Controle';
    04: Result := 'ORDER BY NOME_TERCEIRO, Documento, Vencimento, CtrlPai, DataDoc, Data, Controle';
    05: Result := 'ORDER BY NOME_TERCEIRO, NotaFiscal, Vencimento, CtrlPai, DataDoc, Data, Controle';
    06: Result := 'ORDER BY NOME_TERCEIRO, Duplicata, NotaFiscal, Vencimento, CtrlPai, DataDoc, Data, Controle';
    07: Result := 'ORDER BY NOME_TERCEIRO, NO_UH, Vencimento, CtrlPai, DataDoc, Data, Controle';
    08: Result := 'ORDER BY NOME_TERCEIRO, NOMECARTEIRA, CtrlPai, Data, Vencimento, Controle';
    //
    10: Result := 'ORDER BY DataDoc, NOME_TERCEIRO, CtrlPai, Data, Vencimento, Controle';
    11: Result := 'ORDER BY DataDoc, NOME_TERCEIRO, CtrlPai, Data, Vencimento, Controle';
    12: Result := 'ORDER BY DataDoc, Data, CtrlPai, NOME_TERCEIRO, Vencimento, Controle';
    13: Result := 'ORDER BY DataDoc, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    14: Result := 'ORDER BY DataDoc, Documento, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    15: Result := 'ORDER BY DataDoc, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    16: Result := 'ORDER BY DataDoc, Duplicata, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    17: Result := 'ORDER BY DataDoc, NO_UH, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    18: Result := 'ORDER BY DataDoc, NOMECARTEIRA, CtrlPai, Data, Vencimento, Controle';
    //
    20: Result := 'ORDER BY Data, NOME_TERCEIRO, CtrlPai, DataDoc, Vencimento, Controle';
    21: Result := 'ORDER BY Data, DataDoc, CtrlPai, NOME_TERCEIRO, Vencimento, Controle';
    22: Result := 'ORDER BY Data, NOME_TERCEIRO, CtrlPai, DataDoc, Vencimento, Controle';
    23: Result := 'ORDER BY Data, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    24: Result := 'ORDER BY Data, Documento, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    25: Result := 'ORDER BY Data, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    26: Result := 'ORDER BY Data, Duplicata, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    27: Result := 'ORDER BY Data, NO_UH, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    28: Result := 'ORDER BY Data, NOMECARTEIRA, CtrlPai, DataDoc, Vencimento, Controle';
    //
    30: Result := 'ORDER BY Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    31: Result := 'ORDER BY Vencimento, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    32: Result := 'ORDER BY Vencimento, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    33: Result := 'ORDER BY Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    34: Result := 'ORDER BY Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    35: Result := 'ORDER BY Vencimento, NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    36: Result := 'ORDER BY Vencimento, Duplicata,NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    37: Result := 'ORDER BY Vencimento, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    38: Result := 'ORDER BY Vencimento, NOMECARTEIRA, CtrlPai, DataDoc, Data, Controle';
    //
    40: Result := 'ORDER BY Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    41: Result := 'ORDER BY Documento, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    42: Result := 'ORDER BY Documento, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    43: Result := 'ORDER BY Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    44: Result := 'ORDER BY Documento, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    45: Result := 'ORDER BY Documento, NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    46: Result := 'ORDER BY Documento, Duplicata, NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    47: Result := 'ORDER BY Documento, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    48: Result := 'ORDER BY Documento, NOMECARTEIRA, CtrlPai, DataDoc, Data, Controle';
    //
    50: Result := 'ORDER BY NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    51: Result := 'ORDER BY NotaFiscal, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    52: Result := 'ORDER BY NotaFiscal, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    53: Result := 'ORDER BY NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    54: Result := 'ORDER BY NotaFiscal, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    55: Result := 'ORDER BY NotaFiscal, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    56: Result := 'ORDER BY NotaFiscal, Duplicata, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    57: Result := 'ORDER BY NotaFiscal, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    58: Result := 'ORDER BY NotaFiscal, NOMECARTEIRA, CtrlPai, DataDoc, Data, Controle';
    //
    60: Result := 'ORDER BY Duplicata, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    61: Result := 'ORDER BY Duplicata, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    62: Result := 'ORDER BY Duplicata, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    63: Result := 'ORDER BY Duplicata, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    64: Result := 'ORDER BY Duplicata, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    65: Result := 'ORDER BY Duplicata, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    66: Result := 'ORDER BY Duplicata, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    67: Result := 'ORDER BY Duplicata, NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    68: Result := 'ORDER BY Duplicata, NOMECARTEIRA, CtrlPai, DataDoc, Data, Controle';
    //
    70: Result := 'ORDER BY NO_UH, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    71: Result := 'ORDER BY NO_UH, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    72: Result := 'ORDER BY NO_UH, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    73: Result := 'ORDER BY NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    74: Result := 'ORDER BY NO_UH, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    75: Result := 'ORDER BY NO_UH, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    76: Result := 'ORDER BY NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    77: Result := 'ORDER BY NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    78: Result := 'ORDER BY NO_UH, NOMECARTEIRA, CtrlPai, DataDoc, Data, Controle';
    //
    80: Result := 'ORDER BY NOMECARTEIRA, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    81: Result := 'ORDER BY NOMECARTEIRA, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    82: Result := 'ORDER BY NOMECARTEIRA, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    83: Result := 'ORDER BY NOMECARTEIRA, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    84: Result := 'ORDER BY NOMECARTEIRA, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    85: Result := 'ORDER BY NOMECARTEIRA, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    86: Result := 'ORDER BY NOMECARTEIRA, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    87: Result := 'ORDER BY NOMECARTEIRA, NO_UH, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    88: Result := 'ORDER BY NOMECARTEIRA, Vencimento, CtrlPai, DataDoc, Data, Controle';
    //
    else Result := '';
  end;
end;

procedure TFmExtratos2.QrExtrCalcFields(DataSet: TDataSet);
begin
  QrExtrVALOR.Value := QrExtrCredi.Value - QrExtrDebit.Value;
end;

procedure TFmExtratos2.QrFlxoCalcFields(DataSet: TDataSet);
begin
  if QrFlxoDescricao.Value = '' then
    QrFlxoDESCRI_TXT.Value := QrFlxoNOMECONTA.Value
  else
    QrFlxoDESCRI_TXT.Value := QrFlxoDescricao.Value;
end;

procedure TFmExtratos2.QrPagRecCalcFields(DataSet: TDataSet);
var
  MoraDia: Double;
  SerDoc: String;
begin
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  if QrPagRecFornecedor.Value > 0 then
    QrPagRecTERCEIRO.Value := QrPagRecFornecedor.Value
  else
    QrPagRecTERCEIRO.Value := QrPagRecCliente.Value;
    // FIM 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  //
  QrPagRecVENCER.Value := QrPagRecVencimento.Value;
  QrPagRecVALOR.Value := QrPagRecCredito.Value - QrPagRecDebito.Value;
  ///////////////////////////////
  QrPagRecVENCIDO.Value := 0;
  if (QrPagRecVencimento.Value < Date) and (QrPagRecTipo.Value <> 1) then
    if QrPagRecSit.Value < 2 then
      QrPagRecVENCIDO.Value := QrPagRecVencimento.Value;
  ///////////////////////////////
  if QrPagRecVENCIDO.Value > 0 then
//  begin
  begin
     QrPagRecATRAZODD.Value := Trunc(Date) - QrPagRecVENCIDO.Value;
     QrPagRecMULTA_REAL.Value := QrPagRecMulta.Value / 100 * QrPagRecVALOR.Value;
  end else begin
     QrPagRecATRAZODD.Value := 0;
     QrPagRecMULTA_REAL.Value := 0;
  end;
  if (QrPagRecEhCRED.Value = 'V') and (QrPagRecEhDEB.Value = 'F') then
    MoraDia := QrPagRecMoraDia.Value
  else if (QrPagRecEhCRED.Value = 'F') and (QrPagRecEhDEB.Value = 'V') then
    MoraDia := QrPagRecMoraDia.Value else MoraDia := 0;
  QrPagRecATUALIZADO.Value := QrPagRecSALDO.Value + (QrPagRecSALDO.Value *
  (QrPagRecATRAZODD.Value / 30) * MoraDia / 100) + QrPagRecMULTA_REAL.Value;
  ///////////////////////////////
  if FRelSel in [4,5] then
  begin
    if FRelSel = 4 then //Contas pagas
      QrPagRecPAGO_REAL.Value := QrPagRecDebito.Value - QrPagRecMoraVal.Value -
                                   QrPagRecMultaVal.Value + QrPagRecDescoVal.Value
    else
      QrPagRecPAGO_REAL.Value := QrPagRecCredito.Value - QrPagRecMoraVal.Value -
                                   QrPagRecMultaVal.Value + QrPagRecDescoVal.Value
  end else
  begin
    if QrPagRecTipo.Value = 1 then
    begin
      QrPagRecPAGO_REAL.Value := QrPagRecVALOR.Value;
    end else if QrPagRecTipo.Value = 0 then
    begin
      if QrPagRecVENCIDO.Value > 0 then
      QrPagRecPAGO_REAL.Value := QrPagRecVALOR.Value
      else QrPagRecPAGO_REAL.Value := 0;
    end else
    begin
      QrPagRecPAGO_REAL.Value := QrPagRecPago.Value;
    end;
  end;
  ///////////////////////////////
  if QrPagRecVencimento.Value < Date then
    QrPagRecNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagRecVencimento.Value)
  else QrPagRecNOMEVENCIDO.Value := CO_VAZIO;
  //////////////////////////
  if QrPagRecDocumento.Value > 0 then
    SerDoc := FormatFloat('000000', QrPagRecDocumento.Value) else SerDoc := '';
  if Length(QrPagRecSerieCH.Value) > 0 then SerDoc := QrPagRecSerieCH.Value + SerDoc;
  QrPagRecSERIEDOC.Value := SerDoc;
  //
{
  if QrPagRecDescricao.Value = '' then
    QrPagRecDESCRI_TXT.Value := QrPagRecNOMECONTA.Value
  else
    QrPagRecDESCRI_TXT.Value := QrPagRecDescricao.Value;
}


end;

procedure TFmExtratos2.CkOmissClick(Sender: TObject);
begin
  GBOmiss.Visible := CkOmiss.Checked;
end;

procedure TFmExtratos2.BtSalvarClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Anos',  'Dermatek\Extratos\Venctos', EdAnos.Text,  ktString);
  Geral.WriteAppKeyCU('Meses', 'Dermatek\Extratos\Venctos', EdMeses.Text, ktString);
  Geral.WriteAppKeyCU('Dias',  'Dermatek\Extratos\Venctos', EdDias.Text,  ktString);
  /////
  Geral.WriteAppKeyCU('Ck_Anos',  'Dermatek\Extratos\Venctos', CkAnos.Checked,  ktBoolean);
  Geral.WriteAppKeyCU('Ck_Meses', 'Dermatek\Extratos\Venctos', CkMeses.Checked, ktBoolean);
  Geral.WriteAppKeyCU('Ck_Dias',  'Dermatek\Extratos\Venctos', CkDias.Checked,  ktBoolean);
end;

procedure TFmExtratos2.BtGravarClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Anos',  'Dermatek\Extratos\Perdidos', EdAnos2.Text,  ktString);
  Geral.WriteAppKeyCU('Meses', 'Dermatek\Extratos\Perdidos', EdMeses2.Text, ktString);
  Geral.WriteAppKeyCU('Dias',  'Dermatek\Extratos\Perdidos', EdDias2.Text,  ktString);
  /////
  Geral.WriteAppKeyCU('Ck_Anos',  'Dermatek\Extratos\Perdidos', CkAnos2.Checked,  ktBoolean);
  Geral.WriteAppKeyCU('Ck_Meses', 'Dermatek\Extratos\Perdidos', CkMeses2.Checked, ktBoolean);
  Geral.WriteAppKeyCU('Ck_Dias',  'Dermatek\Extratos\Perdidos', CkDias2.Checked,  ktBoolean);
end;

procedure TFmExtratos2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmExtratos2.QrPagRecItsCalcFields(DataSet: TDataSet);
var
  MoraDia: Double;
begin
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  if QrPagRecItsFornecedor.Value > 0 then
    QrPagRecItsTERCEIRO.Value := QrPagRecItsFornecedor.Value
  else
    QrPagRecItsTERCEIRO.Value := QrPagRecItsCliente.Value;
    // fim 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  QrPagRecItsVENCER.Value := QrPagRecItsVencimento.Value;
  QrPagRecItsVALOR.Value := QrPagRecItsCredito.Value - QrPagRecItsDebito.Value;
  ///////////////////////////////
  QrPagRecItsVENCIDO.Value := 0;
  if (QrPagRecItsVencimento.Value < Date) and (QrPagRecItsTipo.Value <> 1) then
    if QrPagRecItsSit.Value < 2 then
      QrPagRecItsVENCIDO.Value := QrPagRecItsVencimento.Value;
  ///////////////////////////////
  if QrPagRecItsVENCIDO.Value > 0 then
//  begin
  begin
     QrPagRecItsATRAZODD.Value := Trunc(Date) - QrPagRecItsVENCIDO.Value;
     QrPagRecItsMULTA_REAL.Value := QrPagRecItsMulta.Value / 100 * QrPagRecItsVALOR.Value;
  end else begin
     QrPagRecItsATRAZODD.Value := 0;
     QrPagRecItsMULTA_REAL.Value := 0;
  end;
  if (QrPagRecItsEhCRED.Value = 'V') and (QrPagRecItsEhDEB.Value = 'F') then
    MoraDia := QrPagRecItsMoraDia.Value
  else if (QrPagRecItsEhCRED.Value = 'F') and (QrPagRecItsEhDEB.Value = 'V') then
    MoraDia := QrPagRecItsMoraDia.Value else MoraDia := 0;
  QrPagRecItsATUALIZADO.Value := QrPagRecItsSALDO.Value + (QrPagRecItsSALDO.Value *
  (QrPagRecItsATRAZODD.Value / 30) * MoraDia / 100) + QrPagRecItsMULTA_REAL.Value;
  ///////////////////////////////
  if QrPagRecItsTipo.Value = 1 then
  begin
    QrPagRecItsPAGO_REAL.Value := QrPagRecItsVALOR.Value;
  end else if QrPagRecItsTipo.Value = 0 then
  begin
    if QrPagRecItsVENCIDO.Value > 0 then
    QrPagRecItsPAGO_REAL.Value := QrPagRecItsVALOR.Value
    else QrPagRecItsPAGO_REAL.Value := 0;
  end else
  begin
    QrPagRecItsPAGO_REAL.Value := QrPagRecItsPago.Value;
  end;
  ///////////////////////////////
  if QrPagRecItsVencimento.Value < Date then
    QrPagRecItsNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagRecItsVencimento.Value)
  else QrPagRecItsNOMEVENCIDO.Value := CO_VAZIO;
  //////////////////////////
end;

procedure TFmExtratos2.QrPagRec1CalcFields(DataSet: TDataSet);
var
  PagRec: String;
  Aberto: Double;
begin
  case Trunc(QrPagRec1Tipo.Value) of
    0: PagRec := ' Pago';
    1: PagRec := ' Depositado';
    2: PagRec := ' Rolado';
  end;
  PagRec := PagRec + ' ['+FormatFloat('000000', QrPagRec1ID_Pgto.Value)+ '] ';
  QrPagRec1PAGO_ROLADO.Value := PagRec;
  /////////////////////////////////////
  QrPagRec1DEVIDO.Value     := QrPagRec1ATUALIZADO.Value;
  QrPagRec1PEND_TOTAL.Value := QrPagRec1ATUALIZADO.Value;
  //
  if QrPagRec1Valor.Value <> 0 then
  begin
    Aberto := QrPagRec1PENDENTE.Value / QrPagRec1Valor.Value;
    QrPagRec1SdoQtd1.Value := QrPagRec1Qtde.Value * Aberto;
    QrPagRec1SdoQtd2.Value := QrPagRec1Qtd2.Value * Aberto;
  end else
  begin
    QrPagRec1SdoQtd1.Value := 0;
    QrPagRec1SdoQtd2.Value := 0;
  end;
  //
end;

procedure TFmExtratos2.CreateDockableWindows;
var
  I: Integer;
begin
  for I := 0 to High(DockWindows) do
  begin
    DockWindows[I] := TFmDockForm.Create(Application);
    DockWindows[I].Caption := 'C�lculos em Progresso';
//    DockWindows[I].Memo1.Color := Colors[I];
//    DockWindows[I].Memo1.Font.Color := Colors[I] xor $00FFFFFF;
//    DockWindows[I].Memo1.Text := ColStr[I] + ' window ';
  end;
end;

procedure TFmExtratos2.EdEmpresaChange(Sender: TObject);
var
  Emp: Integer;
  Tab, Ano: String;
  Dta: TDateTime;
begin
  {
  2012-01-01 Redefinido para o fim do ano seguinte ao �ltimo la�amento!
  TPCre.Date := Int(Date);
  TPDeb.Date := Int(Date);
  }
  Emp := EdEmpresa.ValueVariant;
  if Emp <> 0 then
  begin
    Tab := DModG.NomeTab(TMeuDB, ntLct, False, ttA, EdEmpresa.ValueVariant);
    Dta := DModFin.DataUltimoLct(Tab);
    if Dta < 2 then
      Dta := Date;
    Ano := Geral.FDT(Dta, 25);
    Dta := EncodeDate(Geral.IMV(Ano) + 1, 12, 31);
    //
    TPCre.Date := Dta;
    TPDeb.Date := Dta;
    //
    TPEmissFim.Date := Dta;
    TPVctoFim.Date := Dta;
    //
  end;
  // Fim 2012-01-01
  //
  ReopenCarteiras();
end;

procedure TFmExtratos2.QrPagRecXCalcFields(DataSet: TDataSet);
var
  MoraDia: Double;
begin
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  if QrPagRecXFornecedor.Value > 0 then
    QrPagRecXTERCEIRO.Value := QrPagRecXFornecedor.Value
  else
    QrPagRecXTERCEIRO.Value := QrPagRecXCliente.Value;
    // fim 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  QrPagRecXVENCER.Value := QrPagRecXVencimento.Value;
  QrPagRecXVALOR.Value := QrPagRecXCredito.Value - QrPagRecXDebito.Value;
  ///////////////////////////////
  QrPagRecXVENCIDO.Value := 0;
  if (QrPagRecXVencimento.Value < Date) and (QrPagRecXTipo.Value <> 1) then
    if QrPagRecXSit.Value < 2 then
      QrPagRecXVENCIDO.Value := QrPagRecXVencimento.Value;
  ///////////////////////////////
  if QrPagRecXVENCIDO.Value > 0 then
//  begin
  begin
     QrPagRecXATRAZODD.Value := Trunc(Date) - QrPagRecXVENCIDO.Value;
     QrPagRecXMULTA_REAL.Value := QrPagRecXMulta.Value / 100 * QrPagRecXVALOR.Value;
  end else begin
     QrPagRecXATRAZODD.Value := 0;
     QrPagRecXMULTA_REAL.Value := 0;
  end;
  if (QrPagRecXEhCRED.Value = 'V') and (QrPagRecXEhDEB.Value = 'F') then
    MoraDia := QrPagRecXMoraDia.Value
  else if (QrPagRecXEhCRED.Value = 'F') and (QrPagRecXEhDEB.Value = 'V') then
    MoraDia := QrPagRecXMoraDia.Value
  else if (QrPagRecXID_Pgto.Value <> QrPagRecXControle.Value) then
    MoraDia := QrPagRecXMoraDia.Value
  else MoraDia := 0;
  QrPagRecXATUALIZADO.Value := QrPagRecXSALDO.Value + (QrPagRecXSALDO.Value *
  (QrPagRecXATRAZODD.Value / 30) * MoraDia / 100) + QrPagRecXMULTA_REAL.Value;
  ///////////////////////////////
  if QrPagRecXTipo.Value = 1 then
  begin
    QrPagRecXPAGO_REAL.Value := QrPagRecXVALOR.Value;
  end else if QrPagRecXTipo.Value = 0 then
  begin
    if QrPagRecXVENCIDO.Value > 0 then
    QrPagRecXPAGO_REAL.Value := QrPagRecXVALOR.Value
    else QrPagRecXPAGO_REAL.Value := 0;
  end else
  begin
    if QrPagRecXCompensado.Value > 0 then
      QrPagRecXPAGO_REAL.Value := QrPagRecXVALOR.Value
    else QrPagRecXPAGO_REAL.Value := QrPagRecXPago.Value;
  end;
  QrPagRecXPENDENTE.Value := QrPagRecXVALOR.Value - QrPagRecXPAGO_REAL.Value;
  ///////////////////////////////
  if QrPagRecXVencimento.Value < Date then
    QrPagRecXNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagRecXVencimento.Value)
  else QrPagRecXNOMEVENCIDO.Value := CO_VAZIO;
  //////////////////////////
end;

procedure TFmExtratos2.GeraRelatorio(Saida: TTipoSaidaRelatorio);
begin
  FRelSel := RGTipo.ItemIndex;
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  FEmpresa      := DModG.QrEmpresasCodigo.Value;
  FEntidade_TXT := FormatFloat('0', FEmpresa);
  if PnDepto.Visible then
  begin
    if CkDepto.Checked then
      FSQLDepto_TXT := 'AND lct.Depto=' + Geral.FF0(EdDepto.ValueVariant)
    else
      FSQLDepto_TXT := '';
  end
  else FSQLDepto_TXT := '';
  //
  VAR_PARAR := False;
  Ext_VALORA := 0;
  Ext_VALORB := 0;
  Ext_VALORC := 0;
  Ext_PENDENTEA := 0;
  Ext_PENDENTEB := 0;
  Ext_PENDENTEC := 0;
  Ext_ATUALIZADOA := 0;
  Ext_ATUALIZADOB := 0;
  Ext_ATUALIZADOC := 0;
  Ext_DEVIDOA := 0;
  Ext_DEVIDOB := 0;
  Ext_DEVIDOC := 0;
  Ext_PAGO_REALA := 0;
  Ext_PAGO_REALB := 0;
  Ext_PAGO_REALC := 0;
  if (RGOrdem1.Enabled) and (RGOrdem1.ItemIndex = -1) then
  begin
    Geral.MB_Aviso('Defina a "Ordem 1"!');
    Exit;
  end;
  if (RGOrdem2.Enabled) and (RGOrdem2.ItemIndex = -1) then
  begin
    Geral.MB_Aviso('Defina uma "Ordem 2"!');
    Exit;
  end;
  dmkPF.ExcluiSQLsDermatek();
  if TPEmissFim.Date < TPEmissIni.Date then
  begin
    Geral.MB_Aviso('"Data de emiss�o" final menor que inicial!');
    Exit;
  end;
  if TPVctoFim.Date < TPVctoIni.Date then
  begin
    Geral.MB_Aviso('"Data de vencimento" final menor que inicial!');
    Exit;
  end;
  if TPDocFim.Date < TPDocIni.Date then
  begin
    Geral.MB_Aviso('"Data do documento" final menor que inicial!');
    Exit;
  end;
{ desmarcado 2012-11-21
  if (EdTerceiro.ValueVariant <> 0) then
  begin
    Geral.MB_Aviso('Fornecedor/Cliente sem nome. Tecle "DEL" para limpar.);
    EdTerceiro.SetFocus;
    Exit;
  end;
}
  Ext_Saldo := 0;
  Ext_EmisI := Geral.FDT(dmkPF.EscolhaDe2Dta(CkEmissao.Checked, TPEmissIni.Date, 0), 1);
  Ext_EmisF := Geral.FDT(dmkPF.EscolhaDe2Dta(CkEmissao.Checked, TPEmissFim.Date, Date), 1) + ' 23:59:59';
  Ext_VctoI := Geral.FDT(dmkPF.EscolhaDe2Dta(CkVencto.Checked, TPVctoIni.Date, 0), 1);
  Ext_VctoF := Geral.FDT(dmkPF.EscolhaDe2Dta(CkVencto.Checked, TPVctoFim.Date, Date), 1) + ' 23:59:59';
  Ext_CompI := Geral.FDT(dmkPF.EscolhaDe2Dta(CkDataComp.Checked, TPCompIni.Date, 0), 1);
  Ext_CompF := Geral.FDT(dmkPF.EscolhaDe2Dta(CkDataComp.Checked, TPCompFim.Date, Date), 1) + ' 23:59:59';
  Ext_DocI  := Geral.FDT(dmkPF.EscolhaDe2Dta(CkDataDoc.Checked, TPDocIni.Date, 0), 1);
  Ext_DocF  := Geral.FDT(dmkPF.EscolhaDe2Dta(CkDataDoc.Checked, TPDocFim.Date, Date), 1) + ' 23:59:59';
{
  Ext_DataH := Geral.FDT(dmkPF.EscolhaDe2Dta(Ck.Checked, Date);
  Ext_DataA := Geral.FDT(dmkPF.EscolhaDe2Dta(Ck.Checked, TPEmissIni.Date-1);
}
  //
{ N�o precisa, � definido antes!
  DModG.ReopenDtEncerMorto(DModG.QrEmpresasFilial.Value);
  FDtEncer := DModG.QrLastEncerData.Value;
  FDtMorto := DModG.QrLastMortoData.Value;
  FTabLctA := DModG.NomeTab(ntLct, False, ttA, FEmpresa);
  FTabLctB := DModG.NomeTab(ntLct, False, ttB, FEmpresa);
  FTabLctD := DModG.NomeTab(ntLct, False, ttD, FEmpresa);
}  //
  case FRelSel of
    0: R00_ImprimeExtratoConsolidado();
    1: R01_ImprimeFluxoDeCaixa(False);
    2: R02_ImprimeContasAPagar(Self, Saida);
    3: R02_ImprimeContasAPagar(Self, Saida);
    4: R02_ImprimeContasAPagar(Self, Saida);
    5: R02_ImprimeContasAPagar(Self, Saida);
    6: R02_ImprimeContasAPagar(Self, Saida);
    7: R02_ImprimeContasAPagar(Self, Saida);
    8: R02_ImprimeContasAPagar(Self, Saida);
    9: R02_ImprimeContasAPagar(Self, Saida);
    10: R10_ImprimeContasPagRec(Self, Saida);
    11: R11_ImprimeContasPagRec(Self, Saida);
    else Geral.MB_Erro('Relat�rio n�o implementado');
  end;
end;

procedure TFmExtratos2.QrTerceirosCalcFields(DataSet: TDataSet);
begin
  if QrTerceirosTel1.Value = '' then QrTerceirosTEL1_TXT.Value := '' else
     QrTerceirosTEL1_TXT.Value := '[ ' + Geral.FormataTelefone_TT(
     QrTerceirosTel1.Value) + ' ]';
  //
  if QrTerceirosTel2.Value = '' then QrTerceirosTEL2_TXT.Value := '' else
     QrTerceirosTEL2_TXT.Value := '[ ' + Geral.FormataTelefone_TT(
     QrTerceirosTel2.Value) + ' ]';
  //
  if QrTerceirosTel3.Value = '' then QrTerceirosTEL3_TXT.Value := '' else
     QrTerceirosTEL3_TXT.Value := '[ ' + Geral.FormataTelefone_TT(
     QrTerceirosTel3.Value) + ' ]';
  //
  QrTerceirosTELEFONES.Value := QrTerceirosTEL1_TXT.Value +
     QrTerceirosTEL2_TXT.Value + QrTerceirosTEL3_TXT.Value;
end;

procedure TFmExtratos2.frxFin_Relat_005_02_B1GetValue(const VarName: String;
  var Value: Variant);
var
  Liga: String;
begin
  if VarName = 'REPRESENTANTE' then
  begin
    if EdAccount.ValueVariant = 0  then
      Value := 'TODOS'
    else
      Value := CBAccount.Text;
  end else if VarName = 'CLI_INT' then
    Value := CBEmpresa.Text
  else if VarName = 'VENDEDOR' then
  begin
    if EdVendedor.ValueVariant = 0 then Value := 'TODOS'
    else Value := CBVendedor.Text;
  end else if VarName = 'VARF_NIVEL' then
  begin
    if EdSubgrupo.ValueVariant <> 0 then
      Value := 'Subgr.'
    else
      Value := 'Conta';
  end else if VarName = 'NOME_CONTA' then
  begin
(*
    if EdConta.ValueVariant = 0 then Value := 'TODAS'
    else Value := CBConta.Text;
*)
    if EdSubGrupo.ValueVariant > 0 then
      Value := CBSubGrupo.Text
    else
    if EdConta.ValueVariant <> 0 then
      Value := CBConta.Text
    else
      Value := 'TODAS';
  end else if VarName = 'NOME_SUBGRUPO' then
  begin
    if EdSubGrupo.ValueVariant = 0 then Value := 'TODOS'
    else Value := CBSubGrupo.Text;
  end else if VarName = 'NOME_CARTEIRA' then
  begin
    if EdCarteira.ValueVariant = 0 then Value := 'TODAS'
    else Value := CBCarteira.Text;
  end else
  ////////////////////////////////////////
  if VarName = 'VALOR_A' then Value := Ext_VALORA;
  if VarName = 'VALOR_B' then Value := Ext_VALORB;
  if VarName = 'VALOR_C' then Value := Ext_VALORC / 2; // two pass
  ///////////////////////////////////////
  if VarName = 'PAGO_REAL_A' then Value := Ext_PAGO_REALA;
  if VarName = 'PAGO_REAL_B' then Value := Ext_PAGO_REALB;
  if VarName = 'PAGO_REAL_C' then Value := Ext_PAGO_REALC / 2;
  ///////////////////////////////////////
  if VarName = 'ATUALIZADO_A' then Value := Ext_ATUALIZADOA;
  if VarName = 'ATUALIZADO_B' then Value := Ext_ATUALIZADOB;
  if VarName = 'ATUALIZADO_C' then Value := Ext_ATUALIZADOC / 2;
  ///////////////////////////////////////
  if VarName = 'DEVIDO_A' then Value := Ext_DEVIDOA;
  if VarName = 'DEVIDO_B' then Value := Ext_DEVIDOB;
  if VarName = 'DEVIDO_C' then Value := Ext_DEVIDOC / 2;
  ///////////////////////////////////////
  if VarName = 'PENDENTE_A' then Value := Ext_PENDENTEA;
  if VarName = 'PENDENTE_B' then Value := Ext_PENDENTEB;
  if VarName = 'PENDENTE_C' then Value := Ext_PENDENTEC / 2;
  ///////////////////////////////////////

  if VarName = 'GRUPO1' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRecNO_UH.Value;
      8: Value := 'Carteira: ' + QrPagRecNOMECARTEIRA.Value;
    end;
  end;
  if VarName = 'GRUPO2' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRecNO_UH.Value;
      8: Value := 'Carteira: ' + QrPagRecNOMECARTEIRA.Value;
    end;
  end;
  if VarName = 'GRUPOA' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
      8: Value := 'Carteira: ' + QrPagRecNOMECARTEIRA.Value;
    end;
  end;
  if VarName = 'GRUPOB' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
      8: Value := 'Carteira: ' + QrPagRecNOMECARTEIRA.Value;
    end;
  end;
  if VarName = 'GRUPOC' then
  begin
    Value := 'geral';
  end;
  if VarName = 'OMITIDOS' then
  begin
    if CkOmiss.Checked then Value := 'Omitidos valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else if FRelSel in ([8,9]) then Value :=
      'Somente valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else Value := '  ';
  end;
  if VarName = 'NIVEIS' then
  begin
    if CkNiveis.Checked then Value := 'N�veis '+EdA.Text+' a '+EdZ.Text
    else Value := '  ';
  end;
  if VarName = 'TERCEIRO' then
  begin
    if FRelSel = 2 then Value := 'Fornecedor: ';
    if FRelSel = 3 then Value := 'Cliente: ';
    if FRelSel = 4 then Value := 'Fornecedor: ';
    if FRelSel = 5 then Value := 'Cliente: ';
    if FRelSel = 6 then Value := 'Fornecedor: ';
    if FRelSel = 7 then Value := 'Cliente: ';
    if FRelSel = 8 then Value := 'Fornecedor: ';
    if FRelSel = 9 then Value := 'Cliente: ';
  end;
{
  if VarName = 'INICIAL' then
  begin
    Value := Ext_SdIni;
    Ext_Saldo := Ext_SdIni
  end else
}
  if VarName = 'NOMEREL' then
  begin
    case FRelSel of
      2: Value := 'RELAT�RIO DE CONTAS A PAGAR';
      3: Value := 'RELAT�RIO DE CONTAS A RECEBER';
      4: Value := 'RELAT�RIO DE CONTAS PAGAS';
      5: Value := 'RELAT�RIO DE CONTAS RECEBIDAS';
      6: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A PAGAR';
      7: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A RECEBER';
      8: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A PAGAR';
      9: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A RECEBER';
      10: Value := 'RELAT�RIO DE CONTAS A PAGAR E RECEBER SEMANAL';
    end;
  end;

  if VarName = 'SALDO' then
  begin
    Value := QrExtratoCredito.Value - QrExtratoDebito.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'SALDO2' then
  begin
    Value := 0(*QrFluxoSALDO.Value*);
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'SALDO4' then
  begin
    Value := QrPagRecSALDO.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'FORNECEDOR' then
    if EdTerceiro.ValueVariant = 0 then Value := 'TODOS' else
    Value := IntToStr(CBTerceiro.KeyValue)+' - '+CBTerceiro.Text + ' ' +
    QrTerceirosTELEFONES.Text;
  if VarName = 'SALDODIA' then
    Value := Ext_Saldo;
  if VarName = 'PERIODO' then
  begin
    Value := '';
    if not CkEmissao.Checked and not CkVencto.Checked
    and not CkDataDoc.Checked then Value := 'N�o definido';
    if CkEmissao.Checked and CkVencto.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkEmissao.Checked then
      Value := 'Emiss�o de '+
        FormatDateTime(VAR_FORMATDATE3, TPEmissIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPEmissFim.Date) + Liga;
    if (CkEmissao.Checked or CkVencto.Checked) and CkDataDoc.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkVencto.Checked then
      Value := Value + 'Vencimento de '+
        FormatDateTime(VAR_FORMATDATE3, TPVctoIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPVctoFim.Date) + Liga;
    if CkDataDoc.Checked then
      Value := Value + 'Datadoc de '+
        FormatDateTime(VAR_FORMATDATE3, TPDocIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPDocFim.Date);
  end

  // user function

  else if VarName = 'VFR_CODITION_A' then
  begin
    case RGOrdem1.ItemIndex of
      00: Value := 'frxDsPagRec1."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec1."DataDoc"';
      02: Value := 'frxDsPagRec1."Data"';
      03: Value := 'frxDsPagRec1."Vencimento"';
      04: Value := 'frxDsPagRec1."Documento"';
      05: Value := 'frxDsPagRec1."NotaFiscal"';
      06: Value := 'frxDsPagRec1."Duplicata"';
      07: Value := 'frxDsPagRec1."NO_UH"';
      08: Value := 'frxDsPagRec1."NOMECARTEIRA"';
    end;
  end
  else
  if VarName = 'VFR_CODITION_B' then
  begin
    case RGOrdem2.ItemIndex of
      00: Value := 'frxDsPagRec1."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec1."DataDoc"';
      02: Value := 'frxDsPagRec1."Data"';
      03: Value := 'frxDsPagRec1."Vencimento"';
      04: Value := 'frxDsPagRec1."Documento"';
      05: Value := 'frxDsPagRec1."NotaFiscal"';
      06: Value := 'frxDsPagRec1."Duplicata"';
      07: Value := 'frxDsPagRec1."NO_UH"';
      08: Value := 'frxDsPagRec1."NOMECARTEIRA"';
    end;
  end else if VarName = 'VFR_CODITION_A1' then
  begin
    case RGOrdem1.ItemIndex of
      00: Value := 'frxDsPagRec."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec."DataDoc"';
      02: Value := 'frxDsPagRec."Data"';
      03: Value := 'frxDsPagRec."Vencimento"';
      04: Value := 'frxDsPagRec."Documento"';
      05: Value := 'frxDsPagRec."NotaFiscal"';
      06: Value := 'frxDsPagRec."Duplicata"';
      07: Value := 'frxDsPagRec."NO_UH"';
      08: Value := 'frxDsPagRec."NOMECARTEIRA"';
    end;
  end
  else
  if VarName = 'VFR_CODITION_B1' then
  begin
    case RGOrdem2.ItemIndex of
      00: Value := 'frxDsPagRec."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec."DataDoc"';
      02: Value := 'frxDsPagRec."Data"';
      03: Value := 'frxDsPagRec."Vencimento"';
      04: Value := 'frxDsPagRec."Documento"';
      05: Value := 'frxDsPagRec."NotaFiscal"';
      06: Value := 'frxDsPagRec."Duplicata"';
      07: Value := 'frxDsPagRec."NO_UH"';
      08: Value := 'frxDsPagRec."NOMECARTEIRA"';
    end;
  end;
  if VarName = 'VFR_ORD' then if EdTerceiro.ValueVariant = 0 then
  Value := False else Value := True;
  if VarName = 'VARF_GRADE' then Value := CkGrade.Checked;
  if VarName = 'VFR_GRUPO1' then
  begin
    if CkOrdem1.Checked then Value := RGOrdem1.ItemIndex else Value := -1;
  end;
  if VarName = 'VFR_GRUPO2' then
  begin
    if CkOrdem2.Checked then Value := RGOrdem2.ItemIndex else Value := -1;
  end;
  if VarName = 'VFR_GRUPOA' then
  begin
    if CkOrdem1.Checked then Value := RGOrdem1.ItemIndex else Value := -1;
    Ext_VALORA := 0;
    Ext_DEVIDOA := 0;
    Ext_PENDENTEA := 0;
    Ext_PAGO_REALA := 0;
    Ext_ATUALIZADOA := 0;
  end;
  if VarName = 'VFR_GRUPOB' then
  begin
    if CkOrdem2.Checked then Value := RGOrdem2.ItemIndex else Value := -1;
    Ext_VALORB := 0;
    Ext_DEVIDOB := 0;
    Ext_PENDENTEB := 0;
    Ext_PAGO_REALB := 0;
    Ext_ATUALIZADOB := 0;
  end;
  (*if VarName = 'VFR_GRUPOC' then
  begin
    Value := 0;
    Ext_ValueORC := 0;
    Ext_DEVIDOC := 0;
    Ext_PENDENTEC := 0;
    Ext_PAGO_REALC := 0;
    Ext_ATUALIZADOC := 0;
  end; *)
  if VarName = 'VFR_ID_PAGTO' then
  begin
    if QrPagRec1CtrlPai.Value = QrPagRec1Controle.Value then
    begin
      Value := 0;
      Ext_VALORA := Ext_VALORA + QrPagRec1VALOR.Value;
      Ext_VALORB := Ext_VALORB + QrPagRec1VALOR.Value;
      Ext_VALORC := Ext_VALORC + QrPagRec1VALOR.Value;
      /////
      Ext_PAGO_REALA := Ext_PAGO_REALA + QrPagRec1PAGO_REAL.Value;
      Ext_PAGO_REALB := Ext_PAGO_REALB + QrPagRec1PAGO_REAL.Value;
      Ext_PAGO_REALC := Ext_PAGO_REALC + QrPagRec1PAGO_REAL.Value;
      /////
      Ext_ATUALIZADOA := Ext_ATUALIZADOA + QrPagRec1ATUALIZADO.Value;
      Ext_ATUALIZADOB := Ext_ATUALIZADOB + QrPagRec1ATUALIZADO.Value;
      Ext_ATUALIZADOC := Ext_ATUALIZADOC + QrPagRec1ATUALIZADO.Value;
      /////
      Ext_DEVIDOA := Ext_DEVIDOA + QrPagRec1DEVIDO.Value;
      Ext_DEVIDOB := Ext_DEVIDOB + QrPagRec1DEVIDO.Value;
      Ext_DEVIDOC := Ext_DEVIDOC + QrPagRec1DEVIDO.Value;
      /////
    end else Value := 1;
    Ext_PENDENTEA := Ext_PENDENTEA + QrPagRec1PEND_TOTAL.Value;
    Ext_PENDENTEB := Ext_PENDENTEB + QrPagRec1PEND_TOTAL.Value;
    Ext_PENDENTEC := Ext_PENDENTEC + QrPagRec1PEND_TOTAL.Value;
  end else
  if VarName = 'VAR_NOMEEMPRESA' then
  begin
    if CkDepto.Checked then
    begin
      Value := CkDepto.Caption + ' ' + EdDepto.Text + ' - ' + CBDepto.Text;
    end else
      Value := CBEmpresa.Text
  end
  else
  if VarName = 'VAR_UH' then
      Value := dmkPF.TxtUH()
  else
end;

procedure TFmExtratos2.frxFin_Relat_005_10_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName = 'VARF_DATA' then
    Value := Now()
   //
  else
  if VarName = 'VARF_DIA_01' then
    Value := Geral.FDT(FDia1, 2)
  else
  if VarName = 'VARF_DIA_02' then
    Value := Geral.FDT(FDia1 + 1, 2)
  else
  if VarName = 'VARF_DIA_03' then
    Value := Geral.FDT(FDia1 + 2, 2)
  else
  if VarName = 'VARF_DIA_04' then
    Value := Geral.FDT(FDia1 + 3, 2)
  else
  if VarName = 'VARF_DIA_05' then
    Value := Geral.FDT(FDia1 + 4, 2)
  else
  if VarName = 'VARF_DIA_06' then
    Value := Geral.FDT(FDia1 + 5, 2)
  else
  if VarName = 'VARF_DIA_07' then
    Value := Geral.FDT(FDia1 + 6, 2)
  else
  if VarName = 'VARF_DIA_08' then
    Value := Geral.FDT(FDia1 + 7, 2)
  else
end;

procedure TFmExtratos2.frxFin_Relat_005_00GetValue(const VarName: String;
  var Value: Variant);
var
  Liga: String;
begin
  if VarName = 'SUBST' then
  begin
    if CkSubstituir.Checked then
      Value := 'Cliente / Fornecedor'
    else
      Value := 'Descri��o';
  end else
  if VarName = 'REPRESENTANTE' then
  begin
    if EdAccount.ValueVariant = 0 then Value := 'TODOS'
    else Value := CBAccount.Text;
  end else if VarName = 'CLI_INT' then
    Value := CBEmpresa.Text
  else if VarName = 'VENDEDOR' then
  begin
    if EdVendedor.ValueVariant = 0 then Value := 'TODOS'
    else Value := CBVendedor.Text;
  end else if VarName = 'VARF_NIVEL' then
  begin
    if EdSubgrupo.ValueVariant <> 0 then
      Value := 'Subgr.'
    else
      Value := 'Conta';
  end else if VarName = 'NOME_CONTA' then
  begin
(*
    if EdConta.ValueVariant = 0 then Value := 'TODAS'
    else Value := CBConta.Text;
*)
    if EdSubGrupo.ValueVariant > 0 then
      Value := CBSubGrupo.Text
    else
    if EdConta.ValueVariant <> 0 then
      Value := CBConta.Text
    else
      Value := 'TODAS';
  end else if VarName = 'NOME_SUBGRUPO' then
  begin
    if EdSubGrupo.ValueVariant = 0 then Value := 'TODOS'
    else Value := CBSubGrupo.Text;
  end else
  ////////////////////////////////////////
  if VarName = 'VALOR_A' then Value := Ext_VALORA else
  if VarName = 'VALOR_B' then Value := Ext_VALORB else
  if VarName = 'VALOR_C' then Value := Ext_VALORC / 2 else
  ///////////////////////////////////////
  if VarName = 'PAGO_REAL_A' then Value := Ext_PAGO_REALA else
  if VarName = 'PAGO_REAL_B' then Value := Ext_PAGO_REALB else
  if VarName = 'PAGO_REAL_C' then Value := Ext_PAGO_REALC / 2 else
  ///////////////////////////////////////
  if VarName = 'ATUALIZADO_A' then Value := Ext_ATUALIZADOA else
  if VarName = 'ATUALIZADO_B' then Value := Ext_ATUALIZADOB else
  if VarName = 'ATUALIZADO_C' then Value := Ext_ATUALIZADOC / 2 else
  ///////////////////////////////////////
  if VarName = 'DEVIDO_A' then Value := Ext_DEVIDOA else
  if VarName = 'DEVIDO_B' then Value := Ext_DEVIDOB else
  if VarName = 'DEVIDO_C' then Value := Ext_DEVIDOC / 2 else
  ///////////////////////////////////////
  if VarName = 'PENDENTE_A' then Value := Ext_PENDENTEA else
  if VarName = 'PENDENTE_B' then Value := Ext_PENDENTEB else
  if VarName = 'PENDENTE_C' then Value := Ext_PENDENTEC / 2 else
  ///////////////////////////////////////

  if VarName = 'GRUPO1' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRecNO_UH.Value;
      8: Value := 'Carteira: ' + QrPagRecNOMECARTEIRA.Value;
    end;
  end else
  if VarName = 'GRUPO2' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRecNO_UH.Value;
      8: Value := 'Carteira: ' + QrPagRecNOMECARTEIRA.Value;
    end;
  end else
  if VarName = 'GRUPOA' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
      8: Value := 'Carteira: ' + QrPagRec1NOMECARTEIRA.Value;
    end;
  end else
  if VarName = 'GRUPOB' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
      8: Value := 'Carteira: ' + QrPagRec1NOMECARTEIRA.Value;
    end;
  end else
  if VarName = 'GRUPOC' then
    Value := 'geral'
  else
  if VarName = 'OMITIDOS' then
  begin
    if CkOmiss.Checked then Value := 'Omitidos valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else if FRelSel in ([8,9]) then Value :=
      'Somente valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else Value := '  ';
  end else
  if VarName = 'NIVEIS' then
  begin
    if CkNiveis.Checked then Value := 'N�veis '+EdA.Text+' a '+EdZ.Text
    else Value := '  ';
  end else
  if VarName = 'TERCEIRO' then
  begin
    if FRelSel = 2 then Value := 'Fornecedor: ';
    if FRelSel = 3 then Value := 'Cliente: ';
    if FRelSel = 4 then Value := 'Fornecedor: ';
    if FRelSel = 5 then Value := 'Cliente: ';
    if FRelSel = 6 then Value := 'Fornecedor: ';
    if FRelSel = 7 then Value := 'Cliente: ';
    if FRelSel = 8 then Value := 'Fornecedor: ';
    if FRelSel = 9 then Value := 'Cliente: ';
    if FRelSel = 10 then Value := 'Terceiro: ';
  end else
  if VarName = 'INICIAL' then
  begin
    Value     := Ext_SdIni;
    Ext_Saldo := Ext_SdIni;
  end else
  if VarName = 'NOMEREL' then
  begin
    case FrelSel of
      1: Value := 'RELAT�RIO FLUXO DE CAIXA';
      2: Value := 'RELAT�RIO DE CONTAS A PAGAR';
      3: Value := 'RELAT�RIO DE CONTAS A RECEBER';
      4: Value := 'RELAT�RIO DE CONTAS PAGAS';
      5: Value := 'RELAT�RIO DE CONTAS RECEBIDAS';
      6: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A PAGAR';
      7: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A RECEBER';
      8: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A PAGAR';
      9: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A RECEBER';
      10: Value := 'RELAT�RIO DE CONTAS A PAGAR E RECEBER';
    end;
  end else

  if VarName = 'SALDO' then
  begin
    Value := QrExtratoCredito.Value - QrExtratoDebito.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end else
  if VarName = 'SALDO2' then
  begin
    Value := 0(*QrFluxoSALDO.Value*);
    Ext_Saldo := Ext_Saldo + Value;
  end else
  if VarName = 'SALDO4' then
  begin
    Value := QrPagRecSALDO.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end else
  if VarName = 'FORNECEDOR' then
  begin
    if EdTerceiro.ValueVariant = 0 then Value := 'TODOS' else
    Value := IntToStr(CBTerceiro.KeyValue)+' - '+CBTerceiro.Text + ' ' +
    QrTerceirosTELEFONES.Text;
  end else
  if VarName = 'SALDODIA' then
    Value := Ext_Saldo else

  if VarName = 'PERIODO' then
  begin
    Value := '';
    if not CkEmissao.Checked and not CkVencto.Checked
    and not CkDataDoc.Checked then Value := 'N�o definido';
    if CkEmissao.Checked and CkVencto.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkEmissao.Checked then
      Value := 'Emiss�o de '+
        FormatDateTime(VAR_FORMATDATE3, TPEmissIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPEmissFim.Date) + Liga;
    if (CkEmissao.Checked or CkVencto.Checked) and CkDataDoc.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkVencto.Checked then
      Value := Value + 'Vencimento de '+
        FormatDateTime(VAR_FORMATDATE3, TPVctoIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPVctoFim.Date) + Liga;
    if CkDataDoc.Checked then
      Value := Value + 'Datadoc de '+
        FormatDateTime(VAR_FORMATDATE3, TPDocIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPDocFim.Date);
  end else
  if VarName = 'VAR_NOMEEMPRESA' then
  begin
    if CkDepto.Checked then
    begin
      Value := CkDepto.Caption + ' ' + EdDepto.Text + ' - ' + CBDepto.Text;
    end else
      Value := CBEmpresa.Text
  end
  else
    //Geral.MB_Aviso('Vari�vel n�o definida! "' + VarName + '"');
end;

end.

