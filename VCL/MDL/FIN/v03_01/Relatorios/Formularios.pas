unit Formularios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Db, (*DBTables,*) DBCtrls, Variants,
  ComCtrls, UnInternalConsts, mySQLDbTables, Grids, DBGrids, printers, frxClass,
  frxDBSet, dmkGeral, dmkEditDateTimePicker, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkImage, UnDmkEnums;

type
  TFmFormularios = class(TForm)
    PainelDados: TPanel;
    RGRelatorio: TRadioGroup;
    CBSubgrupo: TdmkDBLookupComboBox;
    QrSubGrupos: TmySQLQuery;
    DsSubgrupos: TDataSource;
    QrContas1: TmySQLQuery;
    Label1: TLabel;
    RGFormulario: TRadioGroup;
    RGMensalidade: TRadioGroup;
    QrContas2: TmySQLQuery;
    QrContas2ESPERADO: TFloatField;
    QrContas2MINIMO: TFloatField;
    GroupBox1: TGroupBox;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    Label3: TLabel;
    Label2: TLabel;
    QrSoma2: TmySQLQuery;
    QrContas2SCREDITO: TFloatField;
    QrContas2SDEBITO: TFloatField;
    QrContas2STOTAL: TFloatField;
    QrContas2QTDE: TFloatField;
    QrContas2MEDIA: TFloatField;
    QrMalaDireta: TmySQLQuery;
    QrContas3: TmySQLQuery;
    QrMalaDiretaCodigo: TIntegerField;
    QrMalaDiretaNome: TWideStringField;
    QrListaconta: TmySQLQuery;
    QrListacontaCODIGOCONTA: TIntegerField;
    QrListacontaNOMECONTA: TWideStringField;
    QrListacontaCODIGOSUBGRUPO: TIntegerField;
    QrListacontaNOMESUBGRUPO: TWideStringField;
    QrListacontaCODIGOGRUPO: TIntegerField;
    QrListacontaNOMEGRUPO: TWideStringField;
    QrListacontaCODIGOCONJUNTO: TIntegerField;
    QrListacontaNOMECONJUNTO: TWideStringField;
    QrContas3L1C1: TWideStringField;
    QrContas3L1C2: TWideStringField;
    QrContas3L1C3: TWideStringField;
    QrContas3L1C4: TWideStringField;
    QrContas3L2C1: TWideStringField;
    QrContas3L2C2: TWideStringField;
    QrContas3L2C3: TWideStringField;
    QrContas3L2C4: TWideStringField;
    QrContas3L3C1: TWideStringField;
    QrContas3L3C2: TWideStringField;
    QrContas3L3C3: TWideStringField;
    QrContas3L3C4: TWideStringField;
    QrContas3L4C1: TWideStringField;
    QrContas3L4C2: TWideStringField;
    QrContas3L4C3: TWideStringField;
    QrContas3L4C4: TWideStringField;
    QrContas3L5C1: TWideStringField;
    QrContas3L5C2: TWideStringField;
    QrContas3L5C3: TWideStringField;
    QrContas3L5C4: TWideStringField;
    QrCadastros: TmySQLQuery;
    DsCadastros: TDataSource;
    QrEntidades: TmySQLQuery;
    QrCadastrosNOMEENTIDADE: TWideStringField;
    QrUFs: TmySQLQuery;
    DsUFs: TDataSource;
    QrSubGruposCodigo: TIntegerField;
    QrSubGruposNome: TWideStringField;
    QrSubGruposGrupo: TIntegerField;
    QrSubGruposLk: TIntegerField;
    QrSubGruposDataCad: TDateField;
    QrSubGruposDataAlt: TDateField;
    QrSubGruposUserCad: TSmallintField;
    QrSubGruposUserAlt: TSmallintField;
    QrContas1Codigo: TIntegerField;
    QrContas1Nome: TWideStringField;
    QrContas1Nome2: TWideStringField;
    QrContas1Nome3: TWideStringField;
    QrContas1ID: TWideStringField;
    QrContas1Subgrupo: TIntegerField;
    QrContas1Empresa: TIntegerField;
    QrContas1Credito: TWideStringField;
    QrContas1Debito: TWideStringField;
    QrContas1Mensal: TWideStringField;
    QrContas1Exclusivo: TWideStringField;
    QrContas1Mensdia: TSmallintField;
    QrContas1Mensdeb: TFloatField;
    QrContas1Mensmind: TFloatField;
    QrContas1Menscred: TFloatField;
    QrContas1Mensminc: TFloatField;
    QrContas1Lk: TIntegerField;
    QrContas1Terceiro: TIntegerField;
    QrContas1Excel: TWideStringField;
    QrContas1DataCad: TDateField;
    QrContas1DataAlt: TDateField;
    QrContas1UserCad: TSmallintField;
    QrContas1UserAlt: TSmallintField;
    QrContas2Codigo: TIntegerField;
    QrContas2Nome: TWideStringField;
    QrContas2Nome2: TWideStringField;
    QrContas2Nome3: TWideStringField;
    QrContas2ID: TWideStringField;
    QrContas2Subgrupo: TIntegerField;
    QrContas2Empresa: TIntegerField;
    QrContas2Credito: TWideStringField;
    QrContas2Debito: TWideStringField;
    QrContas2Mensal: TWideStringField;
    QrContas2Exclusivo: TWideStringField;
    QrContas2Mensdia: TSmallintField;
    QrContas2Mensdeb: TFloatField;
    QrContas2Mensmind: TFloatField;
    QrContas2Menscred: TFloatField;
    QrContas2Mensminc: TFloatField;
    QrContas2Lk: TIntegerField;
    QrContas2Terceiro: TIntegerField;
    QrContas2Excel: TWideStringField;
    QrContas2DataCad: TDateField;
    QrContas2DataAlt: TDateField;
    QrContas2UserCad: TSmallintField;
    QrContas2UserAlt: TSmallintField;
    QrCadastrosCodigo: TIntegerField;
    QrCadastrosRazaoSocial: TWideStringField;
    QrCadastrosFantasia: TWideStringField;
    QrCadastrosRespons1: TWideStringField;
    QrCadastrosRespons2: TWideStringField;
    QrCadastrosPai: TWideStringField;
    QrCadastrosMae: TWideStringField;
    QrCadastrosCNPJ: TWideStringField;
    QrCadastrosIE: TWideStringField;
    QrCadastrosNome: TWideStringField;
    QrCadastrosApelido: TWideStringField;
    QrCadastrosCPF: TWideStringField;
    QrCadastrosRG: TWideStringField;
    QrCadastrosERua: TWideStringField;
    QrCadastrosENumero: TIntegerField;
    QrCadastrosECompl: TWideStringField;
    QrCadastrosEBairro: TWideStringField;
    QrCadastrosECIDADE: TWideStringField;
    QrCadastrosEUF: TSmallintField;
    QrCadastrosECEP: TIntegerField;
    QrCadastrosEPais: TWideStringField;
    QrCadastrosETe1: TWideStringField;
    QrCadastrosETe2: TWideStringField;
    QrCadastrosETe3: TWideStringField;
    QrCadastrosECel: TWideStringField;
    QrCadastrosEFax: TWideStringField;
    QrCadastrosEEMail: TWideStringField;
    QrCadastrosEContato: TWideStringField;
    QrCadastrosENatal: TDateField;
    QrCadastrosPRua: TWideStringField;
    QrCadastrosPNumero: TIntegerField;
    QrCadastrosPCompl: TWideStringField;
    QrCadastrosPBairro: TWideStringField;
    QrCadastrosPCIDADE: TWideStringField;
    QrCadastrosPUF: TSmallintField;
    QrCadastrosPCEP: TIntegerField;
    QrCadastrosPPais: TWideStringField;
    QrCadastrosPTe1: TWideStringField;
    QrCadastrosPTe2: TWideStringField;
    QrCadastrosPTe3: TWideStringField;
    QrCadastrosPCel: TWideStringField;
    QrCadastrosPFax: TWideStringField;
    QrCadastrosPEMail: TWideStringField;
    QrCadastrosPContato: TWideStringField;
    QrCadastrosPNatal: TDateField;
    QrCadastrosSexo: TWideStringField;
    QrCadastrosResponsavel: TWideStringField;
    QrCadastrosProfissao: TWideStringField;
    QrCadastrosCargo: TWideStringField;
    QrCadastrosRecibo: TSmallintField;
    QrCadastrosDiaRecibo: TSmallintField;
    QrCadastrosAjudaEmpV: TFloatField;
    QrCadastrosAjudaEmpP: TFloatField;
    QrCadastrosCliente1: TWideStringField;
    QrCadastrosCliente2: TWideStringField;
    QrCadastrosFornece1: TWideStringField;
    QrCadastrosFornece2: TWideStringField;
    QrCadastrosFornece3: TWideStringField;
    QrCadastrosFornece4: TWideStringField;
    QrCadastrosTerceiro: TWideStringField;
    QrCadastrosCadastro: TDateField;
    QrCadastrosInformacoes: TWideStringField;
    QrCadastrosLogo: TBlobField;
    QrCadastrosVeiculo: TIntegerField;
    QrCadastrosMensal: TWideStringField;
    QrCadastrosObservacoes: TWideMemoField;
    QrCadastrosTipo: TSmallintField;
    QrCadastrosLk: TIntegerField;
    QrCadastrosDataCad: TDateField;
    QrCadastrosDataAlt: TDateField;
    QrCadastrosUserCad: TSmallintField;
    QrCadastrosUserAlt: TSmallintField;
    QrCadastrosCRua: TWideStringField;
    QrCadastrosCNumero: TIntegerField;
    QrCadastrosCCompl: TWideStringField;
    QrCadastrosCBairro: TWideStringField;
    QrCadastrosCCIDADE: TWideStringField;
    QrCadastrosCUF: TSmallintField;
    QrCadastrosCCEP: TIntegerField;
    QrCadastrosCPais: TWideStringField;
    QrCadastrosCTel: TWideStringField;
    QrCadastrosCFax: TWideStringField;
    QrCadastrosCCel: TWideStringField;
    QrCadastrosCContato: TWideStringField;
    QrCadastrosLRua: TWideStringField;
    QrCadastrosLNumero: TIntegerField;
    QrCadastrosLCompl: TWideStringField;
    QrCadastrosLBairro: TWideStringField;
    QrCadastrosLCIDADE: TWideStringField;
    QrCadastrosLUF: TSmallintField;
    QrCadastrosLCEP: TIntegerField;
    QrCadastrosLPais: TWideStringField;
    QrCadastrosLTel: TWideStringField;
    QrCadastrosLFax: TWideStringField;
    QrCadastrosLCel: TWideStringField;
    QrCadastrosLContato: TWideStringField;
    QrCadastrosComissao: TFloatField;
    QrCadastrosSituacao: TSmallintField;
    QrCadastrosNivel: TWideStringField;
    QrCadastrosGrupo: TIntegerField;
    QrCadastrosAccount: TIntegerField;
    QrCadastrosLogo2: TBlobField;
    QrCadastrosNOMEEUF: TWideStringField;
    QrCadastrosNOMEPUF: TWideStringField;
    QrSoma2Genero: TIntegerField;
    QrSoma2Credito: TFloatField;
    QrSoma2Debito: TFloatField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrEntidadesRUA: TWideStringField;
    QrEntidadesNUMERO: TLargeintField;
    QrEntidadesCOMPL: TWideStringField;
    QrEntidadesCIDADE: TWideStringField;
    QrEntidadesCEP: TLargeintField;
    QrEntidadesNOMEUF: TWideStringField;
    QrSoma2QTDE: TFloatField;
    QrUFsCodigo: TIntegerField;
    QrUFsNome: TWideStringField;
    QrListacontaCODIGOPLANO: TIntegerField;
    QrListacontaNOMEPLANO: TWideStringField;
    QrEntiCart: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    DsEntiCart: TDataSource;
    ProgressBar1: TProgressBar;
    frxContas1: TfrxReport;
    frxDsContas1: TfrxDBDataset;
    frxContas2: TfrxReport;
    frxDsContas2: TfrxDBDataset;
    frxListaContas: TfrxReport;
    frxDsListaConta: TfrxDBDataset;
    EdSubGrupo: TdmkEditCB;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    BtSair: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RGRelatorioClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure QrContas2CalcFields(DataSet: TDataSet);
    procedure BtSairClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure frxContas2GetValue(const VarName: String;
      var Value: Variant);

  private
    { Private declarations }
    procedure Formulario();
    procedure Mensalidade();

  public
    { Public declarations }
  end;

var
  FmFormularios: TFmFormularios;

implementation

uses UnMyObjects, Module, FormulariosCidades, FormulariosResult, ModuleGeral,
  UnFinanceiro, DmkDAC_PF;

{$R *.DFM}

procedure TFmFormularios.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrSoma2.Close;
  QrSoma2.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreQuery(QrSubgrupos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCadastros, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUFs, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntiCart, Dmod.MyDB);
  TPIni.Date := Date - 30;
  TPFim.Date := Date;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmFormularios.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFormularios.RGRelatorioClick(Sender: TObject);
begin
  if RGRelatorio.ItemIndex = 0 then RGFormulario.Enabled := True
  else RGFormulario.Enabled := False;
  if RGRelatorio.ItemIndex = 1 then RGMensalidade.Enabled := True
  else RGMensalidade.Enabled := False;
end;

procedure TFmFormularios.BtConfirmaClick(Sender: TObject);
begin
  case RGRelatorio.ItemIndex of
    0: Formulario();
    1: Mensalidade();
  end;
end;

procedure TFmFormularios.Mensalidade();
  procedure GeraParteSQL(TabLct, DataI, DataF: String);
  begin
    QrSoma2.SQL.Add('SELECT Genero, SUM(Credito) Credito,');
    QrSoma2.SQL.Add('SUM(Debito) Debito,');
    QrSoma2.SQL.Add('(Count(Genero) + 0.00) QTDE');
    QrSoma2.SQL.Add('FROM ' + TabLct);
    QrSoma2.SQL.Add('WHERE Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
    QrSoma2.SQL.Add('GROUP BY Genero');
  end;
var
  Ativo, DataI, DataF: String;
  Entidade, CliInt: Integer;
  Data, FldIni, TabLctA, TabLctB, TabLctD, TabLctX: String;
  DtIni, DtEncer, DtMorto: TDateTime;
begin
  Entidade := DModG.QrEmpresasCodigo.Value;
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  CliInt   := DModG.QrEmpresasFilial.Value;
  //
  DtIni := Int(TPIni.Date);
  Data := FormatDateTime(VAR_FORMATDATE, DtIni);
  //
  DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, DtEncer, DtMorto, TabLctA, TabLctB, TabLctD);
  TabLctX := UFinanceiro.DefLctTab(DtIni, DtEncer, DtMorto, TabLctA, TabLctB, TabLctD);
  FldIni := UFinanceiro.DefLctFldSdoIni(DtIni, DtEncer, DtMorto);
  //
  DataI := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  DataF := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  //
  QrSoma2.Close;
  QrSoma2.SQL.Clear;
{
SELECT Genero, SUM(Credito) Credito,
SUM(Debito) Debito,
(Count(Genero) + 0.00) QTDE
FROM VAR LCT
WHERE Data BETWEEN :P0 AND :P1
GROUP BY Genero
  QrSoma2.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrSoma2.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
}

  QrSoma2.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_009_1;');
  QrSoma2.SQL.Add('CREATE TABLE _FIN_RELAT_009_1');
  GeraParteSQL(TabLctA, DataI, DataF);
  QrSoma2.SQL.Add('UNION');
  GeraParteSQL(TabLctB, DataI, DataF);
  QrSoma2.SQL.Add('UNION');
  GeraParteSQL(TabLctD, DataI, DataF);
  QrSoma2.SQL.Add(';');

  QrSoma2.SQL.Add('SELECT Genero, SUM(Credito) Credito,');
  QrSoma2.SQL.Add('SUM(Debito) Debito, SUM(QTDE) QTDE');
  QrSoma2.SQL.Add('FROM _FIN_RELAT_009_1');
  QrSoma2.SQL.Add('GROUP BY Genero;');
  QrSoma2.SQL.Add('');
  QrSoma2.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_009_1;');
  QrSoma2.SQL.Add('');
  //
  UnDmkDAC_PF.AbreQuery(QrSoma2, DModG.MyPID_DB);
  QrContas2.Close;
  QrContas2.SQL.Clear;
  QrContas2.SQL.Add('SELECT * FROM contas');
  QrContas2.SQL.Add('WHERE Mensal="V"');
  //if CkAtivos.Checked then Ativo := ' "V" ' else
    Ativo := '"V", "F"';
  QrContas1.SQL.Add('AND Ativo in ('+Ativo+')');
  if EdSubGrupo.ValueVariant <> 0 then
    QrContas2.SQL.Add('AND Subgrupo='''+FormatFloat('0', EdSubGrupo.ValueVariant)+'''');
  case RGMensalidade.ItemIndex of
    1: QrContas2.SQL.Add('AND MensCred>0');
    2: QrContas2.SQL.Add('AND MensDeb>0');
    3: QrContas2.SQL.Add('AND (MensCred>0 OR MensDeb>0)');
  end;
  QrContas2.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrContas2, Dmod.MyDB);
  MyObjects.frxDefineDataSets(frxContas2, [
    DModG.frxDsDono,
    frxDsContas2
  ]);
  MyObjects.frxMostra(frxContas2, 'Mensalidade de contas');
  QrContas2.Close;
  UnDmkDAC_PF.AbreQuery(QrSoma2, DModG.MyPID_DB);
end;

procedure TFmFormularios.Formulario;
var
  Ativo: String;
begin
  if RGFormulario.ItemIndex = 0 then
  begin
    QrContas1.Close;
    QrContas1.SQL.Clear;
    QrContas1.SQL.Add('SELECT * FROM contas');
    //if CkAtivos.Checked then Ativo := ' "V" ' else
      Ativo := '"V", "F"';
    QrContas1.SQL.Add('WHERE Ativo in ('+Ativo+')');
    if EdSubGrupo.ValueVariant <> 0 then
      QrContas1.SQL.Add('AND Subgrupo='''+
      FormatFloat('0', EdSubGrupo.ValueVariant)+'''');
    QrContas1.SQL.Add('ORDER BY Nome');
    UnDmkDAC_PF.AbreQuery(QrContas1, Dmod.MyDB);
  MyObjects.frxDefineDataSets(frxContas1, [
    DModG.frxDsDono,
    frxDsContas1
  ]);
    MyObjects.frxMostra(frxContas1, 'Cronograma de contas mensais');
    QrContas1.Close;
  end;
end;

procedure TFmFormularios.QrContas2CalcFields(DataSet: TDataSet);
begin
  QrContas2ESPERADO.Value := QrContas2Menscred.Value - QrContas2Mensdeb.Value;
  QrContas2MINIMO.Value := QrContas2Mensminc.Value - QrContas2Mensmind.Value;
  QrContas2STOTAL.Value := QrContas2SCREDITO.Value - QrContas2SDEBITO.Value;
  if QrContas2QTDE.Value > 0 then
    QrContas2MEDIA.Value := QrContas2STOTAL.Value / QrContas2QTDE.Value
  else QrContas2MEDIA.Value := 0;
end;

procedure TFmFormularios.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormularios.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormularios.frxContas2GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'PERIODO' then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIni.Date)+ CO_ATE+
    FormatDateTime(VAR_FORMATDATE3, TPFim.Date);
end;

end.

