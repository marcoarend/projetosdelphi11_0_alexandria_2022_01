unit ReceDesp2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, dmkEditCB, Db,
  mySQLDbTables, DBCtrls, dmkDBLookupComboBox, ComCtrls, dmkEditDateTimePicker,
  frxClass, frxDBSet, Variants, dmkCheckGroup, Grids, DBGrids, dmkDBGrid, Menus,
  dmkGeral, MyDBCheck, UmySQLModule, UnDmkProcFunc, dmkImage, UnFinanceiro,
  UnDmkEnums, dmkCheckBox, dmkDBGridZTO;

type
  TFormaPsqPeriodo = (fppIndefinido, fppDtaEmis, fppDtaQuit, fppMezCmpt);
  TFmReceDesp2 = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DataSource1: TDataSource;
    QrDC: TmySQLQuery;
    frxDC_Mes: TfrxReport;
    frxDsDC: TfrxDBDataset;
    QrDCNOMEMES: TWideStringField;
    QrDCGenero: TIntegerField;
    QrDCMez: TIntegerField;
    QrDCDepto: TIntegerField;
    QrDCValor: TFloatField;
    QrDCNOMECON: TWideStringField;
    QrDCUNIDADE: TWideStringField;
    QrDCControle: TIntegerField;
    frxDC_Uni: TfrxReport;
    QrUHs: TmySQLQuery;
    QrUHsUnidade: TWideStringField;
    QrUHsConta: TIntegerField;
    DsUHs: TDataSource;
    TabSheet3: TTabSheet;
    QrRet: TmySQLQuery;
    DsRet: TDataSource;
    Panel4: TPanel;
    EdEmpresa: TdmkEditCB;
    Label1: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Panel5: TPanel;
    GroupBox4: TGroupBox;
    Label7: TLabel;
    EdValPagMin: TdmkEdit;
    EdValPagMax: TdmkEdit;
    Label8: TLabel;
    GroupBox5: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    EdValTitMin: TdmkEdit;
    EdValTitMax: TdmkEdit;
    QrRetCodigo: TIntegerField;
    QrRetBanco: TIntegerField;
    QrRetNossoNum: TWideStringField;
    QrRetSeuNum: TIntegerField;
    QrRetIDNum: TIntegerField;
    QrRetOcorrCodi: TWideStringField;
    QrRetOcorrData: TDateField;
    QrRetValTitul: TFloatField;
    QrRetValAbati: TFloatField;
    QrRetValDesco: TFloatField;
    QrRetValPago: TFloatField;
    QrRetValJuros: TFloatField;
    QrRetValMulta: TFloatField;
    QrRetValJuMul: TFloatField;
    QrRetMotivo1: TWideStringField;
    QrRetMotivo2: TWideStringField;
    QrRetMotivo3: TWideStringField;
    QrRetMotivo4: TWideStringField;
    QrRetMotivo5: TWideStringField;
    QrRetQuitaData: TDateField;
    QrRetDiretorio: TIntegerField;
    QrRetArquivo: TWideStringField;
    QrRetItemArq: TIntegerField;
    QrRetStep: TSmallintField;
    QrRetEntidade: TIntegerField;
    QrRetCarteira: TIntegerField;
    QrRetDevJuros: TFloatField;
    QrRetDevMulta: TFloatField;
    QrRetValOutro: TFloatField;
    QrRetValTarif: TFloatField;
    QrRetLk: TIntegerField;
    QrRetDataCad: TDateField;
    QrRetDataAlt: TDateField;
    QrRetUserCad: TIntegerField;
    QrRetUserAlt: TIntegerField;
    QrRetDtaTarif: TDateField;
    QrRetAlterWeb: TSmallintField;
    QrRetAtivo: TSmallintField;
    QrRetTamReg: TIntegerField;
    GroupBox6: TGroupBox;
    Label9: TLabel;
    EdBloqIni: TdmkEdit;
    EdBloqFim: TdmkEdit;
    Label12: TLabel;
    PMCreditos: TPopupMenu;
    Alteracontadoslanamentosselecionados_Cred: TMenuItem;
    PMDebitos: TPopupMenu;
    Alteracontadoslanamentosselecionados_Debi: TMenuItem;
    Alteradescriodolanamentoselecionado1: TMenuItem;
    Alteralanamentoselecionado1: TMenuItem;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctControle: TIntegerField;
    QrLctSub: TSmallintField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctQtde: TFloatField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctFatID: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctFatNum: TFloatField;
    QrLctFatParcela: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctID_Sub: TSmallintField;
    QrLctFatura: TWideStringField;
    QrLctEmitente: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctContaCorrente: TWideStringField;
    QrLctCNPJCPF: TWideStringField;
    QrLctLocal: TIntegerField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctPago: TFloatField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctCliente: TIntegerField;
    QrLctCliInt: TIntegerField;
    QrLctForneceI: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctMulta: TFloatField;
    QrLctProtesto: TDateField;
    QrLctDataDoc: TDateField;
    QrLctCtrlIni: TIntegerField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctDepto: TIntegerField;
    QrLctDescoPor: TIntegerField;
    QrLctDescoVal: TFloatField;
    QrLctDescoControle: TIntegerField;
    QrLctUnidade: TIntegerField;
    QrLctNFVal: TFloatField;
    QrLctAntigo: TWideStringField;
    QrLctExcelGru: TIntegerField;
    QrLctLk: TIntegerField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TIntegerField;
    QrLctUserAlt: TIntegerField;
    QrLctSerieCH: TWideStringField;
    QrLctDoc2: TWideStringField;
    QrLctMoraVal: TFloatField;
    QrLctMultaVal: TFloatField;
    QrLctCNAB_Sit: TSmallintField;
    QrLctTipoCH: TSmallintField;
    QrLctAlterWeb: TSmallintField;
    QrLctReparcel: TIntegerField;
    QrLctID_Quit: TIntegerField;
    QrLctAtrelado: TIntegerField;
    QrLctAtivo: TSmallintField;
    QrLctPagMul: TFloatField;
    QrLctPagJur: TFloatField;
    QrLctSubPgto1: TIntegerField;
    QrLctMultiPgto: TIntegerField;
    QrLctProtocolo: TIntegerField;
    QrLctCtrlQuitPg: TIntegerField;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasNome2: TWideStringField;
    QrCarteirasContato1: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasPagRec: TIntegerField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasAntigo: TWideStringField;
    QrCarteirasContab: TWideStringField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TIntegerField;
    QrCarteirasUserAlt: TIntegerField;
    QrCarteirasOrdem: TIntegerField;
    QrCarteirasForneceN: TSmallintField;
    QrCarteirasFuturoC: TFloatField;
    QrCarteirasFuturoD: TFloatField;
    QrCarteirasFuturoS: TFloatField;
    QrCarteirasExclusivo: TSmallintField;
    QrCarteirasAlterWeb: TSmallintField;
    QrCarteirasAtivo: TSmallintField;
    QrCarteirasRecebeBloq: TSmallintField;
    QrCarteirasEntiDent: TIntegerField;
    Alteralanamentoselecionado2: TMenuItem;
    Panel9: TPanel;
    Label4: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label41: TLabel;
    CBUH: TDBLookupComboBox;
    CGValores: TdmkCheckGroup;
    RGOrdem: TRadioGroup;
    QrDebitos: TmySQLQuery;
    QrDebitosCOMPENSADO_TXT: TWideStringField;
    QrDebitosDATA: TWideStringField;
    QrDebitosDescricao: TWideStringField;
    QrDebitosDEBITO: TFloatField;
    QrDebitosNOTAFISCAL: TLargeintField;
    QrDebitosSERIECH: TWideStringField;
    QrDebitosDOCUMENTO: TFloatField;
    QrDebitosMEZ: TLargeintField;
    QrDebitosCompensado: TDateField;
    QrDebitosNOMECON: TWideStringField;
    QrDebitosNOMESGR: TWideStringField;
    QrDebitosNOMEGRU: TWideStringField;
    QrDebitosITENS: TLargeintField;
    QrDebitosMES: TWideStringField;
    QrDebitosSERIE_DOC: TWideStringField;
    QrDebitosNF_TXT: TWideStringField;
    QrDebitosMES2: TWideStringField;
    QrDebitosControle: TIntegerField;
    QrDebitosSub: TSmallintField;
    QrDebitosCarteira: TIntegerField;
    QrDebitosCartao: TIntegerField;
    QrDebitosVencimento: TDateField;
    QrDebitosSit: TIntegerField;
    QrDebitosGenero: TIntegerField;
    QrDebitosTipo: TSmallintField;
    DsDebitos: TDataSource;
    frxDsDebitos: TfrxDBDataset;
    frxDsCreditos: TfrxDBDataset;
    DsCreditos: TDataSource;
    QrCreditos: TmySQLQuery;
    QrCreditosMez: TIntegerField;
    QrCreditosCredito: TFloatField;
    QrCreditosNOMECON: TWideStringField;
    QrCreditosNOMESGR: TWideStringField;
    QrCreditosNOMEGRU: TWideStringField;
    QrCreditosMES: TWideStringField;
    QrCreditosNOMECON_2: TWideStringField;
    QrCreditosSubPgto1: TIntegerField;
    QrCreditosControle: TIntegerField;
    QrCreditosSub: TSmallintField;
    QrCreditosCarteira: TIntegerField;
    QrCreditosCartao: TIntegerField;
    QrCreditosVencimento: TDateField;
    QrCreditosCompensado: TDateField;
    QrCreditosSit: TIntegerField;
    QrCreditosGenero: TIntegerField;
    QrCreditosTipo: TSmallintField;
    QrResumo: TmySQLQuery;
    QrResumoCredito: TFloatField;
    QrResumoDebito: TFloatField;
    QrResumoSALDO: TFloatField;
    QrResumoFINAL: TFloatField;
    frxDsResumo: TfrxDBDataset;
    frxDsSaldoA: TfrxDBDataset;
    QrSaldoA: TmySQLQuery;
    QrSaldoAInicial: TFloatField;
    QrSaldoASALDO: TFloatField;
    QrSaldoATOTAL: TFloatField;
    QrDebitosTbLct: TWideStringField;
    QrDebitosGRU_OL: TIntegerField;
    QrDebitosCON_OL: TIntegerField;
    QrDebitosSGR_OL: TIntegerField;
    QrCreditosTbLct: TWideStringField;
    QrCreditosData: TDateField;
    QrCreditosGRU_OL: TIntegerField;
    QrCreditosCON_OL: TIntegerField;
    QrCreditosSGR_OL: TIntegerField;
    frxReceDesp: TfrxReport;
    QrDebitosNO_FORNECE: TWideStringField;
    QrLctIndiPag: TIntegerField;
    QrRetID_Link: TLargeintField;
    QrLctAgencia: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel10: TPanel;
    BtOK: TBitBtn;
    PageControl3: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Panel6: TPanel;
    CkNaoAgruparNada: TCheckBox;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    DBGCreditos: TdmkDBGridZTO;
    Panel7: TPanel;
    BtCreditos: TBitBtn;
    TabSheet7: TTabSheet;
    DBGDebitos: TdmkDBGridZTO;
    Panel8: TPanel;
    BtDebitos: TBitBtn;
    LaAvisoUsuario: TLabel;
    LaAvisoDesenvolvedor: TLabel;
    GBReceitas: TGroupBox;
    LBReceitas: TListBox;
    BtReceUp: TBitBtn;
    BtReceDown: TBitBtn;
    GBDespesas: TGroupBox;
    LBDespesas: TListBox;
    BtDespDown: TBitBtn;
    BtDespUp: TBitBtn;
    Panel11: TPanel;
    BtCancela: TBitBtn;
    BtSalvar: TBitBtn;
    TabSheet8: TTabSheet;
    Panel12: TPanel;
    Panel13: TPanel;
    LaNivelSel1: TLabel;
    RGNivel: TRadioGroup;
    EdNivelSel1: TdmkEditCB;
    CBNivelSel1: TdmkDBLookupComboBox;
    LaNivelSel2: TLabel;
    EdNivelSel2: TdmkEditCB;
    CBNivelSel2: TdmkDBLookupComboBox;
    QrNivelSel2: TmySQLQuery;
    QrNivelSel2Codigo: TIntegerField;
    QrNivelSel2Nome: TWideStringField;
    DsNivelSel2: TDataSource;
    QrNivelSel1: TmySQLQuery;
    QrNivelSel1Codigo: TIntegerField;
    QrNivelSel1Nome: TWideStringField;
    DsNivelSel1: TDataSource;
    Panel14: TPanel;
    QrLctSerieNF: TWideStringField;
    QrLctEndossas: TSmallintField;
    QrLctEndossan: TFloatField;
    QrLctEndossad: TFloatField;
    QrLctCancelado: TSmallintField;
    QrLctEventosCad: TIntegerField;
    QrLctEncerrado: TIntegerField;
    QrLctErrCtrl: TIntegerField;
    QrLctFatParcRef: TIntegerField;
    QrLctFatSit: TSmallintField;
    QrLctFatSitSub: TSmallintField;
    QrLctFatGrupo: TIntegerField;
    QrLctTaxasVal: TFloatField;
    QrLctFisicoSrc: TSmallintField;
    QrLctFisicoCod: TIntegerField;
    QrDebitosDESCRICAO2: TWideStringField;
    CGCentroRes: TdmkCheckGroup;
    Panel15: TPanel;
    RGRelat01: TRadioGroup;
    RGPagRec: TRadioGroup;
    QrPlaniCus: TmySQLQuery;
    QrPlaniCusCredito: TFloatField;
    QrPlaniCusDebito: TFloatField;
    QrPlaniCusPERCENT: TFloatField;
    QrPlaniCusORDEM_CC2: TIntegerField;
    QrPlaniCusORDEM_CC1: TIntegerField;
    QrPlaniCusGenero: TIntegerField;
    QrPlaniCusNOMECON: TWideStringField;
    QrPlaniCusNO_CC1: TWideStringField;
    QrPlaniCusNO_CC2: TWideStringField;
    frxDsPlaniCus: TfrxDBDataset;
    frxFIN_RELAT_010_001_002: TfrxReport;
    QrPlaniCusVALOR: TFloatField;
    QrAllCus: TmySQLQuery;
    QrAllCusCREDITOS: TFloatField;
    QrAllCusDEBITOS: TFloatField;
    frxDsAllCus: TfrxDBDataset;
    QrAllCusSALDO: TFloatField;
    QrAllCusPERCENTUAL: TFloatField;
    Panel16: TPanel;
    GroupBox2: TGroupBox;
    CkAcordos: TCheckBox;
    CkPeriodos: TCheckBox;
    CkTextos: TCheckBox;
    CkCentroRes: TdmkCheckBox;
    Panel17: TPanel;
    PCPeriodos1: TPageControl;
    TabSheet9: TTabSheet;
    Panel18: TPanel;
    TabSheet10: TTabSheet;
    Panel19: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    TPDataIni01: TdmkEditDateTimePicker;
    TPDataFim01: TdmkEditDateTimePicker;
    GroupBox7: TGroupBox;
    LaAno: TLabel;
    LaMes: TLabel;
    CBMesIni01: TComboBox;
    CBAnoIni01: TComboBox;
    Panel20: TPanel;
    DBGrid1: TdmkDBGrid;
    QrLctVctoOriginal: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxReceDesp_GetValue(const VarName: String;
      var Value: Variant);
    procedure QrDCCalcFields(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure CBUHKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdValPagMinChange(Sender: TObject);
    procedure EdValTitMinChange(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure CkNaoAgruparNadaClick(Sender: TObject);
    procedure Alteracontadoslanamentosselecionados_CredClick(Sender: TObject);
    procedure BtCreditosClick(Sender: TObject);
    procedure Alteracontadoslanamentosselecionados_DebiClick(Sender: TObject);
    procedure BtDebitosClick(Sender: TObject);
    procedure Alteradescriodolanamentoselecionado1Click(Sender: TObject);
    procedure Alteralanamentoselecionado1Click(Sender: TObject);
    procedure Alteralanamentoselecionado2Click(Sender: TObject);
    procedure QrDebitosCalcFields(DataSet: TDataSet);
    procedure QrCreditosCalcFields(DataSet: TDataSet);
    procedure QrResumoCalcFields(DataSet: TDataSet);
    procedure QrSaldoACalcFields(DataSet: TDataSet);
    procedure BtReceUpClick(Sender: TObject);
    procedure BtReceDownClick(Sender: TObject);
    procedure BtDespUpClick(Sender: TObject);
    procedure BtDespDownClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure RGNivelClick(Sender: TObject);
    procedure QrAllCusCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FMez: Integer;
    FEntidade, FEmpresa, FGenero, FUH: Integer;
    FEntidTXT, FEmprTXT,
    FDataIniA, FDataFimA, FDataIniB, FDataFimB,
    FTabLctA, FTabLctB, FTabLctD,
    FCentrosRes: String;
    FDtIni, FDtEncer, FDtMorto: TDateTime;
    FNivPlaCtaUsar: Boolean;
    FNivPlaCtaCred, FNivPlaCtaDebi: Integer;
    FDataIniA_Date, FDataFimA_Date: TdateTime;
    function  ContinuaSemCompetencia(TabLctA: String): Boolean;
    procedure DefineDatas(DataAIni, DataAFim, DataBIni, DataBFim: TDateTime;
              Ano, Mes: Integer);
    procedure DemostrativoDeReceitasEDespesas();
    procedure HistoricoConta();
    procedure RetornosCNAB();
    function  ReopenCreditosEDebitos(CliInt: String; (*DataI, DataF: String;*)
              Acordos, Periodos, Textos, NaoAgruparNada: Boolean;
              TabLctA, TabLctB, TabLctD: String;
              FormaPsqPeriodo: TFormaPsqPeriodo): Boolean;
    procedure MoveItensListBox(ListBox: TListBox; Cima: Boolean);
    procedure CarregaOrdenacao();
    procedure ReopenNivelSel();
    //
    function  InsAlt(Acao: TGerencimantoDeRegistro; Qry: TmySQLQuery): Boolean;
  public
    { Public declarations }
  end;

  var
  FmReceDesp2: TFmReceDesp2;

implementation

uses UnMyObjects, UnInternalConsts, Module, LctMudaConta, LctEdit, Principal,
  ModuleGeral, DmkDAC_PF(*, Imprime*);

{$R *.DFM}

procedure TFmReceDesp2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReceDesp2.BtSalvarClick(Sender: TObject);
var
  KeyRecDes, Texto: String;
  i: Integer;
begin
  KeyRecDes := Application.Title + '\ReceitasDespesas';
  Texto     := '';
  //
  for i := 0 to LBReceitas.Count - 1 do
  begin
    if i = 0 then
      Texto := LBReceitas.Items[i]
    else
      Texto := Texto + '||' + LBReceitas.Items[i];
  end;
  Geral.WriteAppKeyCU('Receitas', KeyRecDes, Texto, ktString);
  //
  Texto := '';
  //
  for i := 0 to LBDespesas.Count - 1 do
  begin
    if i = 0 then
      Texto := LBDespesas.Items[i]
    else
      Texto := Texto + '||' + LBDespesas.Items[i];
  end;
  Geral.WriteAppKeyCU('Despesas', KeyRecDes, Texto, ktString);
end;

procedure TFmReceDesp2.CarregaOrdenacao;
var
  Lista: TStringList;
  KeyRecDes, Receitas, Despesas: String;
begin
  Lista := TStringList.Create;
  KeyRecDes := Application.Title + '\ReceitasDespesas';
  //
  Receitas := Geral.ReadAppKeyCU('Receitas', KeyRecDes, ktString, '');
  Despesas := Geral.ReadAppKeyCU('Despesas', KeyRecDes, ktString, '');
  //
  if Length(Receitas) > 0 then
  begin
    Lista := Geral.Explode(Receitas, '||', 3);
    //
    LBReceitas.Items := Lista;
  end;
  if Length(Despesas) > 0 then
  begin
    Lista := Geral.Explode(Despesas, '||', 3);
    //
    LBDespesas.Items := Lista;
  end;
  Lista.Free;
end;

procedure TFmReceDesp2.CBUHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_DELETE then
    CBUH.KeyValue := Null;
end;

procedure TFmReceDesp2.CkNaoAgruparNadaClick(Sender: TObject);
begin
  Pagecontrol2.Visible := CkNaoAgruparNada.Checked;
  QrCreditos.Close;
  QrDebitos.Close;
end;

function TFmReceDesp2.ContinuaSemCompetencia(TabLctA: String): Boolean;
var
  Qry: TMySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT COUNT(*) ITENS ',
    'FROM ' + TabLctA,
    'WHERE Mez=0 ',
    'AND Controle<>0 ',
    '']);
    Result := Qry.RecordCount = 0;
    if not Result then
      Result := Geral.MB_Pergunta('Existem ' + Geral.FF0(
        Qry.FieldByName('ITENS').AsInteger) +
        ' lan�amentos sem m�s de compet�ncia!' + sLineBreak +
        'Deseja continuar assim mesmo?') = ID_YES;
  finally
    Qry.Free;
  end;
end;

procedure TFmReceDesp2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  CGValores.Value := 3;
end;

procedure TFmReceDesp2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmReceDesp2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  QrCreditos.Database := DModG.MyPID_DB;
  QrDebitos.Database  := DModG.MyPID_DB;
  QrSaldoA.Database   := DModG.MyPID_DB;
  QrResumo.Database   := DModG.MyPID_DB;
  QrDC.Database       := DModG.MyPID_DB;
  //
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  //
  TPDataIni01.Date := Date - 30;
  TPDataFim01.Date := Date;
  MyObjects.PreencheCBAnoECBMes(CBAnoIni01, CBMesIni01, -1);
  //
  //TPDataIni02.Date := Date - 30;
  //TPDataFim02.Date := Date;
  PageControl1.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PCPeriodos1.ActivePageIndex  := 0;
  CarregaOrdenacao;
end;

procedure TFmReceDesp2.Alteracontadoslanamentosselecionados_CredClick(
  Sender: TObject);
var
  I, N, Genero, Controle, CtrlMae: Integer;
  //
begin
  if UFinanceiro.ImpedePelaTabelaLct(QrCreditosTbLct.Value,
  QrCreditosControle.Value) then
    Exit;
  //
  Application.CreateForm(TFmLctMudaConta, FmLctMudaConta);
  FmLctMudaConta.ShowModal;
  Genero := FmLctMudaConta.FContaSel;
  FmLctMudaConta.Destroy;
  if Genero = QrCreditosGenero.Value then Exit;
  if Genero <> -1000 then
  begin
    Screen.Cursor := crHourGlass;
    N := 0;
    with DBGCreditos.DataSource.DataSet do
    for i:= 0 to DBGCreditos.SelectedRows.Count-1 do
    begin
      if QrCreditosGenero.Value < 1 then
      begin
        Geral.MB_Aviso('O lan�amento '+IntToStr(
        QrCreditosControle.Value)+' � protegido. Para editar '+
        'este item selecione seu caminho correto!');
        Exit;
      end;
      if QrCreditosCartao.Value > 0 then
      begin
        Geral.MB_Aviso('O lan�amento '+IntToStr(
        QrCreditosControle.Value)+' n�o pode ser editado pois '+
        'pertence a uma fatura!');
        Exit;
      end;
      //GotoBookmark(DBGCreditos.SelectedRows.Items[i]);
      GotoBookmark(DBGCreditos.SelectedRows.Items[i]);
      if not UFinanceiro.ImpedePelaTabelaLct(QrCreditosTbLct.Value,
      QrCreditosControle.Value) then
      begin
        CtrlMae := QrCreditosControle.Value;
        N := N + UFinanceiro.AlteraGenero(CtrlMae, FTabLctA, Genero);
      end;
    end;
    //
    Controle := QrCreditosControle.Value;
    UnDmkDAC_PF.AbreQuery(QrCreditos, Dmod.MyDB);
    QrCreditos.Locate('Controle', Controle, []);
    Geral.MB_Info('Entre selecionados e suas origens e quita��es, ' +
      Geral.FF0(N) + ' lan�amentos foram alterados!');
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmReceDesp2.Alteracontadoslanamentosselecionados_DebiClick(
  Sender: TObject);
var
  I, N, Genero, Controle: Integer;
  CtrlMae: Integer;
begin
  Application.CreateForm(TFmLctMudaConta, FmLctMudaConta);
  FmLctMudaConta.ShowModal;
  Genero := FmLctMudaConta.FContaSel;
  FmLctMudaConta.Destroy;
  if Genero = QrDebitosGenero.Value then Exit;
  if Genero <> -1000 then
  begin
    Screen.Cursor := crHourGlass;
    N := 0;
    with DBGDebitos.DataSource.DataSet do
    for i:= 0 to DBGDebitos.SelectedRows.Count-1 do
    begin
      if QrDebitosGenero.Value < 1 then
      begin
        Geral.MB_Aviso('O lan�amento '+IntToStr(
        QrDebitosControle.Value)+' � protegido. Para editar '+
        'este item selecione seu caminho correto!');
        Exit;
      end;
      if QrDebitosCartao.Value > 0 then
      begin
        Geral.MB_Aviso('O lan�amento '+IntToStr(
        QrDebitosControle.Value)+' n�o pode ser editado pois '+
        'pertence a uma fatura!');
        Exit;
      end;
      //GotoBookmark(DBGDebitos.SelectedRows.Items[i]);
      GotoBookmark(DBGDebitos.SelectedRows.Items[i]);
      if not UFinanceiro.ImpedePelaTabelaLct(QrDebitosTbLct.Value,
      QrDebitosControle.Value) then
      begin
        CtrlMae := QrDebitosControle.Value;
        N := N + UFinanceiro.AlteraGenero(CtrlMae, FTabLctA, Genero);
      end;
    end;
    //
    Controle := QrDebitosControle.Value;
    UnDmkDAC_PF.AbreQuery(QrDebitos, Dmod.MyDB);
    QrDebitos.Locate('Controle', Controle, []);
    Geral.MB_Info('Entre selecionados e suas origens e quita��es, ' +
      Geral.FF0(N) + ' lan�amentos foram alterados!');
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmReceDesp2.Alteradescriodolanamentoselecionado1Click(
  Sender: TObject);
var
  Descricao: String;
  Controle: Integer;
begin
  if UFinanceiro.ImpedePelaTabelaLct(QrDebitosTbLct.Value,
  QrDebitosControle.Value) then
    Exit;
  //
  Descricao := QrDebitosDescricao.Value;
  if InputQuery('Altera��o de Lan�amento', 'Informa o novo hist�rico', Descricao) then
  begin
    Screen.Cursor := crHourGlass;
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'Descricao'], ['Controle', 'Sub'], [Descricao], [
    QrDebitosControle.Value, QrDebitosSub.Value], True, '', FTabLctA);
    //
    Controle := QrDebitosControle.Value;
    UnDmkDAC_PF.AbreQuery(QrDebitos, Dmod.MyDB);
    QrDebitos.Locate('Controle', Controle, []);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmReceDesp2.Alteralanamentoselecionado1Click(Sender: TObject);
const
  OneAccount = 0;
  OneCliInt = 0;
var
  Controle: Integer;
  Ctrl_TXT, Sub_TXT: String;
begin
  if UFinanceiro.ImpedePelaTabelaLct(QrCreditosTbLct.Value,
    QrCreditosControle.Value) then
    Exit;
  //
  Ctrl_TXT := FormatFloat('0', QrCreditosControle.Value);
  Sub_TXT  := FormatFloat('0', QrCreditosSub.Value);
  //
  QrLct.Close;
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT *');
  QrLct.SQL.Add('FROM ' + FTabLctA);
  QrLct.SQL.Add('WHERE Controle=' + Ctrl_Txt);
  QrLct.SQL.Add('AND Sub=' + Sub_TXT);
  UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
  //
  if QrLct.RecordCount = 1 then
  begin
    if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
    DModG.ReopenEmpresas(VAR_USUARIO, 0);
    //
    if InsAlt(tgrAltera, QrLct) then
(*
    if UFinanceiro.AlteracaoLancamento(QrCarteiras, QrLct, lfProprio,
    OneAccount, QrCarteirasForneceI.Value, FTabLctA, False) then
*)
    begin
      Controle := QrCreditosControle.Value;
      UnDmkDAC_PF.AbreQuery(QrCreditos, Dmod.MyDB);
      QrCreditos.Locate('Controle', Controle, []);
    end;
  end else Geral.MB_Aviso('N�o foi poss�vel localizar o lan�amento ' +
  Ctrl_TXT + '. Verifique se ele n�o pertence a um periodo encerrado!');
end;

procedure TFmReceDesp2.Alteralanamentoselecionado2Click(Sender: TObject);
var
  Controle: Integer;
  Ctrl_TXT, Sub_TXT: String;
begin
  if UFinanceiro.ImpedePelaTabelaLct(QrDebitosTbLct.Value,
  QrDebitosControle.Value) then
    Exit;
  //
  Ctrl_TXT := FormatFloat('0', QrDebitosControle.Value);
  Sub_TXT  := FormatFloat('0', QrDebitosSub.Value);
  //
  QrLct.Close;
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT *');
  QrLct.SQL.Add('FROM ' + FTabLctA);
  QrLct.SQL.Add('WHERE Controle=' + Ctrl_Txt);
  QrLct.SQL.Add('AND Sub=' + Sub_TXT);
(*
  QrLct.SQL.Add('UNION');
  QrLct.SQL.Add('SELECT *');
  QrLct.SQL.Add('FROM ' + FTabLctB);
  QrLct.SQL.Add('WHERE Controle=' + Ctrl_Txt);
  QrLct.SQL.Add('AND Sub=' + Sub_TXT);
  QrLct.SQL.Add('UNION');
  QrLct.SQL.Add('SELECT *');
  QrLct.SQL.Add('FROM ' + FTabLctD);
  QrLct.SQL.Add('WHERE Controle=' + Ctrl_Txt);
  QrLct.SQL.Add('AND Sub=' + Sub_TXT);
*)
  UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
  //
  if QrLct.RecordCount = 1 then
  begin
    {
    if UFinanceiro.InsAltLancamento(TFmLctEdit, FmLctEdit, lfCondominio,
    afmoNegarComAviso, QrLct, QrCarteiras,
    tgrAltera, QrLctControle.Value, QrLctSub.Value,
    0(*Genero.Value*), 2, 1, nil, 0, 0, 0, 0, 0, 0, True,
    0(*Cliente*), 0(*Fornecedor*), FEmpresa(*cliInt*),
    0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
    False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
    0, 0, 0, 2, 0, FTabLctA) > 0}
    //
    if VAR_LIB_EMPRESAS = '' then
      VAR_LIB_EMPRESAS := '-100000000';
    DModG.ReopenEmpresas(VAR_USUARIO, 0);
    //
    if UFinanceiro.AlteracaoLancamento(QrCarteiras, QrLct, lfProprio,
    0(*OneAccount*), QrCarteirasForneceI.Value(*OneCliInt*), FTabLctA, False) then
    begin
      Controle := QrDebitosControle.Value;
      UnDmkDAC_PF.AbreQuery(QrDebitos, Dmod.MyDB);
      QrDebitos.Locate('Controle', Controle, []);
    end;
  end else Geral.MB_Aviso('N�o foi poss�vel localizar o lan�amento ' +
  Ctrl_TXT + '. Verifique se ele n�o pertence a um periodo encerrado!');
end;

procedure TFmReceDesp2.BtDebitosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDebitos, BtDebitos);
end;

procedure TFmReceDesp2.BtDespDownClick(Sender: TObject);
begin
  MoveItensListBox(LBDespesas, False);
end;

procedure TFmReceDesp2.BtDespUpClick(Sender: TObject);
begin
  MoveItensListBox(LBDespesas, True);
end;

procedure TFmReceDesp2.BtCancelaClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Receitas', Application.Title + '\ReceitasDespesas', '', ktString);
  Geral.WriteAppKeyCU('Despesas', Application.Title + '\ReceitasDespesas', '', ktString);
  //
  Geral.MB_Info('Reabra a janela para que a configura��o padr�o seja restaurada!');
end;

procedure TFmReceDesp2.BtCreditosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCreditos, BtCreditos);
end;

procedure TFmReceDesp2.BtOKClick(Sender: TObject);
begin
  if EdEmpresa.ValueVariant = 0 then
  begin
    Geral.MB_Aviso('Informe a Empresa!');
    EdEmpresa.SetFocus;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    FEntidade := DModG.QrEmpresasCodigo.Value;
    FEntidTXT := FormatFloat('0', FEntidade);
    FEmpresa  := DModG.QrEmpresasFilial.Value;
    FEmprTXT  := FormatFloat('0', FEmpresa);
    FGenero := Geral.IMV(EdConta.Text);
    if CBUH.KeyValue <> Null then
      FUH := CBUH.KeyValue
    else FUH := 0;
    //
    case PageControl1.ActivePageIndex of
      0: DemostrativoDeReceitasEDespesas();
      1: HistoricoConta();
      2: RetornosCNAB();
      else Geral.MB_Aviso('Relat�rio (aba) n�o definido');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmReceDesp2.BtReceDownClick(Sender: TObject);
begin
  MoveItensListBox(LBReceitas, False);
end;

procedure TFmReceDesp2.BtReceUpClick(Sender: TObject);
begin
  MoveItensListBox(LBReceitas, True);
end;

procedure TFmReceDesp2.DBGrid1CellClick(Column: TColumn);
var
  Codigo, Status: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  Codigo := QrRetCodigo.Value;
  if QrRetStep.Value = 0 then
    Status := 1
  else
    Status := 0;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET ');
  Dmod.QrUpd.SQL.Add('Step=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
  Dmod.QrUpd.Params[00].AsInteger := Status;
  Dmod.QrUpd.Params[01].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  UnDmkDAC_PF.AbreQuery(QrRet, Dmod.MyDB);
  QrRet.Locate('Codigo', Codigo, []);
  Screen.Cursor := crDefault;
end;

procedure TFmReceDesp2.DefineDatas(DataAIni, DataAFim, DataBIni,
  DataBFim: TDateTime; Ano, Mes: Integer);
begin
  FDataIniA_Date := DataAIni;
  FDataFimA_Date := DataAFim;
  FDataIniA := Geral.FDT(DataAIni, 1);
  FDataFimA := Geral.FDT(DataAFim, 1);
  FDataIniB := Geral.FDT(DataBIni, 1);
  FDataFimB := Geral.FDT(DataBFim, 1);
  if Ano > 0 then
  begin
    FMez := Geral.AnoEMesToMez(Ano, Mes);
    UFinanceiro.CorrigeMez(FTabLctA, True);
  end else
    FMez := 0;
end;

procedure TFmReceDesp2.DemostrativoDeReceitasEDespesas();
  procedure GeraParteSQL_SaldoA(TabLct, Entidade, DataI, DataF, FldIni: String);
  begin
    QrSaldoA.SQL.Add('SELECT ' + FldIni + ' Inicial,');
    QrSaldoA.SQL.Add('(');
    QrSaldoA.SQL.Add('  SELECT SUM(lct.Credito-lct.Debito)');
    QrSaldoA.SQL.Add('  FROM ' + TabLct + ' lct');
    QrSaldoA.SQL.Add('  WHERE lct.Carteira=car.Codigo');
    QrSaldoA.SQL.Add('  AND lct.Data < "' + FDataIniA + '"');
    //Desativado por causa de condom�nios que foram para o morto e voltaram
    //nestes condom�nios duplica e nos demais n�o apresentaram problemas ao desabilitar
    //QrSaldoA.SQL.Add('  AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!

    // N�o precisa
    // QrSaldoA.SQL.Add('  AND car.ForneceI=' + Entidade);
    QrSaldoA.SQL.Add(') SALDO');
    QrSaldoA.SQL.Add('FROM ' + TMeuDB + '.carteiras car');
    QrSaldoA.SQL.Add('WHERE car.ForneceI=' + Entidade);
    QrSaldoA.SQL.Add('AND car.Tipo <> 2');
{
    QrSaldoA.SQL.Add('SELECT SUM(' + FldIni + ') Inicial,');
    QrSaldoA.SQL.Add('(');
    QrSaldoA.SQL.Add('  SELECT SUM(lct.Credito-lct.Debito)');
    QrSaldoA.SQL.Add('  FROM ' + TabLct + ' lct');
    QrSaldoA.SQL.Add('  LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrSaldoA.SQL.Add('  WHERE car.Tipo <> 2');
    QrSaldoA.SQL.Add('  AND car.ForneceI=' + Entidade);
    QrSaldoA.SQL.Add('  AND lct.Data < ' + FDataIniA);
    QrSaldoA.SQL.Add(') SALDO');
    QrSaldoA.SQL.Add('FROM ' + TMeuDB + '.carteiras car');
    QrSaldoA.SQL.Add('WHERE car.ForneceI=' + Entidade);
    QrSaldoA.SQL.Add('AND car.Tipo <> 2');
}
  end;

  procedure GeraParteSQL_Resumo(TabLct: String);
  begin
    case RGPagRec.ItemIndex of
      0: QrResumo.SQL.Add('SELECT SUM(lct.Credito) Credito, -SUM(lct.Debito) Debito ');
      1:
      begin
        QrResumo.SQL.Add('SELECT SUM(IF(con.PagRec=1 OR (con.PagRec=0 AND');
        QrResumo.SQL.Add('lct.Credito-lct.Debito>0), lct.Credito - ');
        QrResumo.SQL.Add('lct.Debito, 0))Credito, SUM(IF(con.PagRec=-1 OR ');
        QrResumo.SQL.Add('(con.PagRec=0 AND lct.Credito-lct.Debito<0), ');
        QrResumo.SQL.Add('lct.Credito-lct.Debito, 0)) Debito ');
      end;
    end;
    QrResumo.SQL.Add('FROM ' + TabLct + ' lct');
    QrResumo.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrResumo.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lct.Genero');
    case RGPagRec.ItemIndex of
      0: ; // nada!!
      1:
      begin
        QrResumo.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
        QrResumo.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
        QrResumo.SQL.Add('LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto');
      end;
    end;
    case RGPagRec.ItemIndex of
      0: QrResumo.SQL.Add('WHERE lct.Tipo <> 2');
      1: QrResumo.SQL.Add('WHERE lct.ID_Pgto=0 ');
      else QrResumo.SQL.Add('WHERE PagRec ????');
    end;
    QrResumo.SQL.Add('AND lct.Genero > 0');
    QrResumo.SQL.Add('AND car.ForneceI=' + FEntidTXT);
    QrResumo.SQL.Add('AND lct.Data BETWEEN "' + FDataIniA + '" AND "' + FDataFimA + '"');
    //Desativado por causa de condom�nios que foram para o morto e voltaram
    //nestes condom�nios duplica e nos demais n�o apresentaram problemas ao desabilitar
    //QrResumo.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
    if FNivPlaCtaUsar then
    begin
      QrResumo.SQL.Add('AND ' +
      dmkPF.NomeDoNivelSelecionado(4, RGNivel.ItemIndex) + '.' +
      dmkPF.NomeDoNivelSelecionado(5, RGNivel.ItemIndex) + ' IN (' +
      Geral.FF0(FNivPlaCtaCred) + ', ' + Geral.FF0(FNivPlaCtaDebi) + ')');
    end;
    if CkCentroRes.Checked then
      QrResumo.SQL.Add('AND con.CentroRes IN (' + FCentrosRes + ')');
  end;

var
  Imprime: Boolean;
  FldIni, TabIni, CREDITOS_TXT: String;
  I: Integer;
  Ano, Mes, Dias: Word;
  DataI, DataF: TDateTime;
  FormaPsqPeriodo: TFormaPsqPeriodo;
begin
  FormaPsqPeriodo := fppIndefinido;
  case PCPeriodos1.ActivePageIndex of
    0:
    begin
      DefineDatas(TPDataIni01.Date, TPDataFim01.Date, 0, 0, 0, 0);
      FormaPsqPeriodo := fppDtaEmis;
      // ou FormaPsqPeriodo := fppDtaQuit);  ?????
    end;
    1:
    begin
      Ano := Geral.IMV(CBAnoIni01.Text);
      Mes := CBMesIni01.ItemIndex + 1;
      DataI := EncodeDate(Ano, Mes, 1);
      DataF := IncMonth(DataI, 1) - 1;
      DefineDatas(DataI, DataF, 0, 0, Ano, Mes);
      if not ContinuaSemCompetencia(FTabLctA) then
        Exit;
      FormaPsqPeriodo := fppMezCmpt;
    end;
    else
    begin
      Geral.MB_Erro('ERRO! Datas n�o definidas!');
      Exit;
    end;
  end;
  //
  FNivPlaCtaUsar := RGNivel.ItemIndex > 0;
  if FNivPlaCtaUsar then
  begin
    FNivPlaCtaCred := EdNivelSel1.ValueVariant;
    FNivPlaCtaDebi := EdNivelSel2.ValueVariant;
    //
    if MyObjects.FIC(FNivPlaCtaCred = 0, EdNivelSel1, 'Informe o "' +
    LaNivelSel1.Caption + '"!') then
      Exit;
    if MyObjects.FIC(FNivPlaCtaDebi = 0, EdNivelSel2, 'Informe o "' +
    LaNivelSel2.Caption + '"!') then
      Exit;
  end else
  begin
    FNivPlaCtaCred := 0;
    FNivPlaCtaDebi := 0;
  end;
  if MyObjects.FIC(CkCentroRes.Checked and (CGCentroRes.Value = 0),
  CGCentroRes, 'Informe pelo menos um "' + CkCentrores.Caption + '"!') then
    Exit;
  //
  FCentrosRes := '';
  for I := 0 to CGCentroRes.Items.Count -1 do
  begin
    if CGCentroRes.Checked[I] = True then
      FCentrosRes := FCentrosRes + ', ' + Geral.FF0(I);
  end;
  FCentrosRes := Copy(FCentrosRes, 3);
  //
  if not ReopenCreditosEDebitos(FEntidTXT, (*FDataIniA, FDataFimA,*) CkAcordos.Checked,
  CkPeriodos.Checked, CkTextos.Checked, CkNaoAgruparNada.Checked,
  FTabLctA, FTabLctB, FTabLctD, FormaPsqPeriodo) then
    Exit;
  //
  FldIni := UFinanceiro.DefLctFldSdoIni(FDataIniA_Date, FDtEncer, FDtMorto);
  TabIni := UFinanceiro.DefLctTab(FDtIni, FDtEncer, FDtMorto,
            FTabLcta, FTabLctB, FTabLctD);
  //
  QrSaldoA.Close;
  QrSaldoA.SQL.Clear;

  //
  QrSaldoA.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_SALDOA_1_;');
  QrSaldoA.SQL.Add('CREATE TABLE _MOD_COND_SALDOA_1_');
  QrSaldoA.SQL.Add('');
  GeraParteSQL_SaldoA(FTabLctA, FEntidTXT, FDataIniA, FDataFimA, FldIni);
  QrSaldoA.SQL.Add(';');
  QrSaldoA.SQL.Add('');
  QrSaldoA.SQL.Add('SELECT SUM(Inicial) Inicial, SUM(SALDO) SALDO');
  QrSaldoA.SQL.Add('FROM _MOD_COND_SALDOA_1_;');
  QrSaldoA.SQL.Add('');
  // N�o pode !!
  //QrSaldoA.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_SALDOA_1_;');
  QrSaldoA.SQL.Add('');
  //
  UMyMod.AbreQuery(QrSaldoA, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
  // Deve ser ap�s SaldoA
  QrResumo.Close;
  QrResumo.SQL.Clear;
  //
  QrResumo.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_RESUMO_1_;');
  QrResumo.SQL.Add('CREATE TABLE _MOD_COND_RESUMO_1_');
  QrResumo.SQL.Add('');
  GeraParteSQL_Resumo(FTabLctA);
  QrResumo.SQL.Add('UNION');
  GeraParteSQL_Resumo(FTabLctB);
  QrResumo.SQL.Add('UNION');
  GeraParteSQL_Resumo(FTabLctD);
  QrResumo.SQL.Add(';');
  QrResumo.SQL.Add('');
  QrResumo.SQL.Add('SELECT SUM(Credito) Credito, -SUM(Debito) Debito ');
  QrResumo.SQL.Add('FROM _MOD_COND_RESUMO_1_;');
  QrResumo.SQL.Add('');
  // N�o pode !!
  //QrResumo.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_RESUMO_1_;');
  QrResumo.SQL.Add('');
  //
  UMyMod.AbreQuery(QrResumo, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
  //
  case RGRelat01.ItemIndex of
    0: // Receitas e despesas
    begin
      Screen.Cursor := crDefault;
      //Imprime := not CkNaoAgruparNada.Checked;
      Imprime := True;
      if Imprime then
      begin
        MyObjects.frxDefineDataSets(frxReceDesp, [
          DModG.frxDsDono,
          frxDsDebitos,
          frxDsCreditos,
          frxDsResumo,
          frxDsSaldoA
        ]);
        MyObjects.frxMostra(frxReceDesp, 'Demonstrativo de receitas e despesas');
      end;
    end;
    1: // Planilha de custos
    begin
      //
      QrPlaniCus.Close;
      UnDmkDAC_PF.AbreMySQLQuery0(QrAllCus, DModG.MyPID_DB, [
      //
      'DROP TABLE IF EXISTS _MOD_COND_PLANI_CUS_1_; ',
      'CREATE TABLE _MOD_COND_PLANI_CUS_1_ ',
      '',
      'SELECT 0.00 Credito, SUM(mcd.Debito) Debito, ',
      '-SUM(mcd.Debito) VALOR, "D" TipoDC, ',
      'IF(cc2.PagRec IS NULL, 0, cc2.PagRec) PagRec, ',
      '999999999.999999 PERCENT, cta.OrdemLista, ',
      'cc2.Ordem ORDEM_CC2, cc1.Ordem ORDEM_CC1, ',
      'mcd.Genero, mcd.NOMECON, cc1.Nome NO_CC1, cc2.Nome NO_CC2 ',
      'FROM _mod_cond_debi_1_ mcd ',
      'LEFT JOIN ' + TMeuDB + '.contas cta ON cta.Codigo=mcd.Genero ',
      'LEFT JOIN ' + TMeuDB + '.centrocusto cc1 ON cc1.Codigo=cta.CentroCusto ',
      'LEFT JOIN ' + TMeuDB + '.centrocust2 cc2 ON cc2.Codigo=cc1.CentroCust2 ',
      'GROUP BY mcd.Genero ',
      ' UNION ',
      'SELECT SUM(mcd.Credito) Credito, 0.00 Debito, ',
      'SUM(mcd.Credito) VALOR, "C" TipoDC, ',
      'IF(cc2.PagRec IS NULL, 0, cc2.PagRec) PagRec, ',
      '999999999.999999 PERCENT, cta.OrdemLista, ',
      'cc2.Ordem ORDEM_CC2, cc1.Ordem ORDEM_CC1, ',
      'mcd.Genero, mcd.NOMECON, cc1.Nome NO_CC1, cc2.Nome NO_CC2 ',
      'FROM _mod_cond_cred_1_ mcd ',
      'LEFT JOIN ' + TMeuDB + '.contas cta ON cta.Codigo=mcd.Genero ',
      'LEFT JOIN ' + TMeuDB + '.centrocusto cc1 ON cc1.Codigo=cta.CentroCusto ',
      'LEFT JOIN ' + TMeuDB + '.centrocust2 cc2 ON cc2.Codigo=cc1.CentroCust2 ',
      'GROUP BY mcd.Genero; ',
      //
      'SELECT',
      'SUM(IF(Pagrec=-1, Debito-Credito, 0)) DEBITOS, ',
      'SUM(IF(Pagrec=1, Credito-Debito, 0)) CREDITOS ',
      'FROM _MOD_COND_PLANI_CUS_1_; ',
      '']);
      //

      // Fazer % apenas aqui, pois creditos e debitos poder ter sidos invertidos!
      if QrAllCusCREDITOS.Value <> 0 then
      begin
        CREDITOS_TXT := Geral.FFT_Dot(QrAllCusCREDITOS.Value / 100, 4, siNegativo);
        //
        DModG.MyPID_DB.Execute(Geral.ATS([
        'UPDATE _MOD_COND_PLANI_CUS_1_ ',
        'SET PERCENT=(VALOR * PagRec)/' + CREDITOS_TXT + '; ',
        '']));
      end;
      UnDmkDAC_PF.AbreMySQLQuery0(QrPlaniCus, DModG.MyPID_DB, [
      'SELECT SUM(Credito) Credito, SUM(Debito) Debito,',
      'SUM(VALOR * PagRec) VALOR, TipoDC, PagRec, SUM(PERCENT) PERCENT,',
      'ORDEM_CC2, ORDEM_CC1, Genero, NOMECON, NO_CC1, NO_CC2 ',
      'FROM _MOD_COND_PLANI_CUS_1_',
      'GROUP BY Genero, Ordem_CC2, Ordem_CC1',
      'ORDER BY Ordem_CC2, Ordem_CC1, NO_CC2, NO_CC1, OrdemLista; ',
      '']);
      //
      MyObjects.frxDefineDataSets(frxFIN_RELAT_010_001_002, [
      DModG.frxDsDono,
      frxDsAllCus,
      frxDsPlaniCus
      ]);
      MyObjects.frxMostra(frxFIN_RELAT_010_001_002, 'Planilha de Custos');
    end;
  end;
  //
  (*
  if Imprime then
    PageControl3.ActivePageIndex := 1;
  *)
end;

procedure TFmReceDesp2.EdEmpresaChange(Sender: TObject);
begin
  CBUH.KeyValue := Null;
  QrUHs.Close;
  FEntidade := DModG.QrEmpresasCodigo.Value;
  FEntidTXT := FormatFloat('0', FEntidade);
  FEmpresa  := DModG.QrEmpresasFilial.Value;
  FEmprTXT  := FormatFloat('0', FEmpresa);

  { desmarcado
  FTabLctA  := DModG.NomeTab(ntLct, False, ttA, FEmpresa);
  FTabLctB  := DModG.NomeTab(ntLct, False, ttB, FEmpresa);
  FTabLctD  := DModG.NomeTab(ntLct, False, ttD, FEmpresa);
  }
  DModG.Def_EM_ABD(TMeuDB, 
    FEntidade, FEmpresa, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  //
  if (VAR_KIND_DEPTO = kdUH) and (FEmpresa > 0) then
  begin
    QrUHs.Params[0].AsInteger := FEmpresa;
    UnDmkDAC_PF.AbreQuery(QrUHs, Dmod.MyDB);
  end;
end;

procedure TFmReceDesp2.EdValPagMinChange(Sender: TObject);
begin
  EdValPagMax.ValueVariant := EdValPagMin.ValueVariant;
end;

procedure TFmReceDesp2.EdValTitMinChange(Sender: TObject);
begin
  EdValTitMax.ValueVariant := EdValTitMin.ValueVariant;
end;

procedure TFmReceDesp2.frxReceDesp_GetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2, FDataIniA_Date) + ' at� ' +
    FormatDateTime(VAR_FORMATDATE2, FDataFimA_Date)
  else if AnsiCompareText(VarName, 'VARF_NOMECOND') = 0 then
    Value := CBEmpresa.Text
  else if AnsiCompareText(VarName, 'VARF_CONTA_SEL') = 0 then
    Value := CBConta.Text
  else if AnsiCompareText(VarName, 'VARF_UH') = 0 then
  begin
    if CBUH.KeyValue = Null then
    begin
      if VAR_KIND_DEPTO = kdUH then
        Value := 'TODAS UHs'
      else
        Value := '';
    end else
      Value := CBUH.Text;
  end
  else
  if VarName = 'VARF_DATA' then Value := Now()
  else
  if VarName = 'VARF_EMPRESA' then
    Value := CBEmpresa.Text
  else
  if VarName = 'VARF_MOSTRASOMAS_RECEDESP' then
    Value := RGPagRec.ItemIndex = 0
  else
end;

procedure TFmReceDesp2.HistoricoConta();
  procedure GeraParteSQL(TabLct, CliInt, Genero, UH: String);
  begin
    QrDC.SQL.Add('SELECT lan.Genero, lan.Mez, lan.Depto, ');
    QrDC.SQL.Add('lan.Credito-lan.Debito Valor, con.OrdemLista, ');
    QrDC.SQL.Add('lan.Controle, con.Nome NOMECON, ');
    if VAR_KIND_DEPTO = kdUH then
      QrDC.SQL.Add('cim.Unidade UNIDADE')
    else
      QrDC.SQL.Add('"" UNIDADE');
    QrDC.SQL.Add('FROM ' + TabLct + ' lan');
    QrDC.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QrDC.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lan.Genero');
    if VAR_KIND_DEPTO = kdUH then
      QrDC.SQL.Add('LEFT JOIN ' + TMeuDB + '.condimov  cim ON cim.Conta=lan.Depto');
    QrDC.SQL.Add('WHERE lan.Tipo <> 2');
    case CGValores.Value of
      1: QrDC.SQL.Add('AND lan.Credito > lan.Debito');
      2: QrDC.SQL.Add('AND lan.Debito > lan.Credito');
    end;
    QrDC.SQL.Add('AND lan.Genero>0');
    QrDC.SQL.Add('AND car.ForneceI = ' + CliInt);
    if FGenero > 0 then
      QrDC.SQL.Add('AND Genero=' + Genero);
    if FUH > 0 then
      QrDC.SQL.Add('AND Depto=' + UH);
    QrDC.SQL.Add('AND lan.Data BETWEEN "' + FDataIniA + '" AND "' + FDataFimB + '"');
  end;
var
  CliInt, Genero, UH: String;
begin
  Geral.MB_Info('N�o implementado');
  // Est� funcionando normalmente, s� precisa mudar (customizar) o componente
  // da origem das datas para pesquisa!
(*
    FDataIniA := Geral.FDT(FDataIniA_Date, 1);
    FDataFimA := Geral.FDT(FDataFimA_Date, 1);
    FDataIniB := Geral.FDT(TPDataIni2.Date, 1);
    FDataFimB := Geral.FDT(TPDataFim2.Date, 1);
/
  CliInt := FormatFloat('0', FEntidade);
  Genero := FormatFloat('0', FGenero);
  UH     := FormatFloat('0', FUH);
  //
  QrDC.Close;
  QrDC.SQL.Clear;
  //
  QrDC.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_010_DC_;');
  QrDC.SQL.Add('CREATE TABLE _FIN_RELAT_010_DC_');
  QrDC.SQL.Add('');
  GeraParteSQL(FTabLctA, CliInt, Genero, UH);
  QrDC.SQL.Add('UNION');
  GeraParteSQL(FTabLctB, CliInt, Genero, UH);
  QrDC.SQL.Add('UNION');
  GeraParteSQL(FTabLctD, CliInt, Genero, UH);
  QrDC.SQL.Add(';');
  QrDC.SQL.Add('');
  QrDC.SQL.Add('SELECT * FROM _FIN_RELAT_010_DC_');
  case RGOrdem.ItemIndex of
    0: QrDC.SQL.Add('ORDER BY OrdemLista, NOMECON, Mez, Depto;');
    1: QrDC.SQL.Add('ORDER BY OrdemLista, NOMECON, Depto, Mez;');
  end;
  QrDC.SQL.Add('');
  QrDC.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_010_DC_;');
  QrDC.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrDC, Dmod.MyDB);
  //
  case RGOrdem.ItemIndex of
    0: MyObjects.frxMostra(frxDC_Mes, 'Hist�rico de Conta (Plano de contas) por M�s');
    1: MyObjects.frxMostra(frxDC_Uni, 'Hist�rico de Conta (Plano de contas) por Unidade');
  end;
*)
end;

function TFmReceDesp2.InsAlt(Acao: TGerencimantoDeRegistro; Qry: TmySQLQuery): Boolean;
const
  AlteraAtehFatID = False;
  Genero = 0;
  SetaVars = nil;
  FatID = 0;
  FatID_Sub = 0;
  FatNum = 0;
  Carteira = 0;
  Credito = 0;
  Debito = 0;
  Cliente = 0;
  Fornecedor = 0;
  LockCliInt = True;
  ForneceI = 0;
  Account = 0;
  Vendedor = 0;
  LockForneceI = False;
  LockAccount = False;
  LockVendedor = False;
  Data = 0;
  Vencto = 0;
  DataDoc = 0;
  IDFinalidade = 1;
  Mes = 0;
  FisicoSrc = 0;
  FisicoCod = 0;
var
  Continua: Boolean;
  PercJuroM, PercMulta: Double;
  CliInt: Integer;
begin
  //
  CliInt := QrLctCliInt.Value; //FThisEntidade;
  PercJuroM := Dmod.QrControle.FieldByName('MyPerJuros').AsFloat;
  PercMulta := Dmod.QrControle.FieldByName('MyPerMulta').AsFloat;
  //
  Result := UFinanceiro.InclusaoLancamento((*TFmLctEdit, FmLctEdit,*) lfProprio,
    afmoNegarComAviso, QrLct, QrCarteiras,
    Acao, QrLctControle.Value,
    QrLctSub.Value, Genero,
    PercJuroM, PercMulta,
    SetaVars, FatID, FatID_Sub, FatNum, Carteira,
    Credito, Debito, AlteraAtehFatID,
    Cliente, Fornecedor, CliInt,
    ForneceI, Account, Vendedor, LockCliInt,
    LockForneceI, LockAccount, LockVendedor,
    Data, Vencto, DataDoc, IDFinalidade, Mes,
    FtabLctA, FisicoSrc, FisicoCod) > 0;
  //
  (*
  if Result then
  begin
    TDmLct2(FModuleLctX).LocCod(QrCarteiraCodigo.Value, QrCarteiraCodigo.Value,
    QrCarteira, '');
    QrLct.Locate('Controle', FLAN_CONTROLE, []);
    //
    if TPDataFim.Date < VAR_DATAINSERIR then
    begin
      TPDataFim.Date := VAR_DATAINSERIR;
      TPDataFimChange(Self);
    end;
  end;
  *)
end;

procedure TFmReceDesp2.MoveItensListBox(ListBox: TListBox; Cima: Boolean);
var
  NovoIndex: Integer;
  Executa: Boolean;
begin
  if Cima then
    Executa := ListBox.ItemIndex > 0
  else
    Executa := ListBox.ItemIndex < ListBox.Items.Count - 1;
  if Executa then
  begin
    if Cima then
      NovoIndex := ListBox.ItemIndex - 1
    else
      NovoIndex := ListBox.ItemIndex + 1;
    ListBox.Items.Move(ListBox.ItemIndex, NovoIndex);
    ListBox.ItemIndex := NovoIndex;
    ListBox.SetFocus;
  end;
end;

procedure TFmReceDesp2.QrAllCusCalcFields(DataSet: TDataSet);
begin
  QrAllCusSALDO.Value := QrAllCusCREDITOS.Value - QrAllCusDEBITOS.Value;
  if QrAllCusCREDITOS.Value = 0 then
    QrAllCusPERCENTUAL.Value := 0
  else
    QrAllCusPERCENTUAL.Value :=
    QrAllCusSALDO.Value / QrAllCusCREDITOS.Value * 100;
end;

procedure TFmReceDesp2.QrCreditosCalcFields(DataSet: TDataSet);
var
  SubPgto1: String;
begin
  if QrCreditosSubPgto1.Value = 0 then SubPgto1 := ''
    else SubPgto1 := ' RECEB. ACORDO EXTRAJUDICIAL';
  //
  if CkNaoAgruparNada.Checked then
    QrCreditosNOMECON_2.Value := Geral.FDT(QrCreditosData.Value, 3) + ' - ' +
    Geral.FF0(QrCreditosControle.Value) + ' ' +
    QrCreditosNOMECON.Value + SubPgto1
  else
    QrCreditosNOMECON_2.Value := QrCreditosNOMECON.Value + SubPgto1;
  //
  QrCreditosMES.Value := dmkPF.MezToMesEAno(QrCreditosMez.Value);
end;

procedure TFmReceDesp2.QrDCCalcFields(DataSet: TDataSet);
begin
   QrDCNOMEMES.Value := dmkPF.MezToFDT(QrDCMez.Value, 0, 14);
end;

procedure TFmReceDesp2.QrDebitosCalcFields(DataSet: TDataSet);
begin
  QrDebitosMES.Value := dmkPF.MezToMesEAno(QrDebitosMez.Value);
  QrDebitosMES2.Value := dmkPF.MezToMesEAnoCurto(QrDebitosMez.Value);
  //
  QrDebitosSERIE_DOC.Value := Trim(QrDebitosSerieCH.Value);
  if QrDebitosDocumento.Value <> 0 then QrDebitosSERIE_DOC.Value :=
    QrDebitosSERIE_DOC.Value + FormatFloat('000000', QrDebitosDocumento.Value);
  //
  QrDebitosNF_TXT.Value := FormatFloat('000000;-000000; ', QrDebitosNotaFiscal.Value);
  //
  if CkNaoAgruparNada.Checked then
    QrDebitosDESCRICAO2.Value := QrDebitosData.Value + ' - ' +
    Geral.FF0(QrDebitosControle.Value) + ' ' +
    QrDebitosDescricao.Value
  else
    QrDebitosDESCRICAO2.Value := QrDebitosDescricao.Value;
end;

procedure TFmReceDesp2.QrResumoCalcFields(DataSet: TDataSet);
begin
  QrResumoSALDO.Value := QrResumoCredito.Value - QrResumoDebito.Value;
  QrResumoFINAL.Value := QrResumoSALDO.Value + QrSaldoATOTAL.Value;
end;

procedure TFmReceDesp2.QrSaldoACalcFields(DataSet: TDataSet);
begin
  QrSaldoATOTAL.Value := QrSaldoAInicial.Value + QrSaldoASALDO.Value;
end;

function TFmReceDesp2.ReopenCreditosEDebitos(CliInt: String; (*DataI,
  DataF: String;*) Acordos, Periodos, Textos, NaoAgruparNada: Boolean; TabLctA,
  TabLctB, TabLctD: String; FormaPsqPeriodo: TFormaPsqPeriodo): Boolean;
  function OrdenaItens(ListBox: TListBox; Receitas: Boolean): String;
    function TraduzCampos(Texto: String; Receitas: Boolean): String;
    begin
      if Receitas then
      begin
        ///////////////////////////////////////////////////////////////////
        //GRU_OL  = Ordem do Grupo (Cadastro de grupos)
        //NOMEGRU = Nome do Grupo
        //SGR_OL  = Ordem do subgrupo (cadastro de subgrupos)
        //NOMESGR = Nome do Subgrupo
        //CON_OL  = Ordem da Conta (Cadastro de contas)
        //NOMECON = Nome da Conta
        //Mez     = M�s de compet�ncia
        //Data    = Data do Lan�amento
        ///////////////////////////////////////////////////////////////////
        if      LowerCase(Texto) = 'ordem do grupo (cadastro de grupos)' then
          Result := 'GRU_OL'
        else if LowerCase(Texto) = 'nome do grupo' then
          Result := 'NOMEGRU'
        else if LowerCase(Texto) = 'ordem do subgrupo (cadastro de subgrupos)' then
          Result := 'SGR_OL'
        else if LowerCase(Texto) = 'nome do subgrupo' then
          Result := 'NOMESGR'
        else if LowerCase(Texto) = 'ordem da conta (cadastro de contas)' then
          Result := 'CON_OL'
        else if LowerCase(Texto) = 'nome da conta' then
          Result := 'NOMECON'
        else if LowerCase(Texto) = 'm�s de compet�ncia' then
          Result := 'Mez'
        else if LowerCase(Texto) = 'data do lan�amento' then
          Result := 'Data'
        else
          Result := '';
      end else
      begin
        ///////////////////////////////////////////////////////////////////
        //GRU_OL  = Ordem do Grupo (Cadastro de grupos)
        //NOMEGRU = Nome do Grupo
        //Ordem do subgrupo (cadastro de subgrupos)
        //NOMESGR = Nome do Subgrupo
        //CON_OL  = Ordem da Conta (Cadastro de contas)
        //NOMECON = Nome da Conta
        //MEZ     = M�s de compet�ncia
        //Data    = Data do Lan�amento      
        ///////////////////////////////////////////////////////////////////
        if      LowerCase(Texto) = 'ordem do grupo (cadastro de grupos)' then
          Result := 'GRU_OL'
        else if LowerCase(Texto) = 'nome do grupo' then
          Result := 'NOMEGRU'
        else if LowerCase(Texto) = 'ordem do subgrupo (cadastro de subgrupos)' then
          Result := 'SGR_OL'
        else if LowerCase(Texto) = 'nome do subgrupo' then
          Result := 'NOMESGR'
        else if LowerCase(Texto) = 'ordem da conta (cadastro de contas)' then
          Result := 'CON_OL'
        else if LowerCase(Texto) = 'nome da conta' then
          Result := 'NOMECON'
        else if LowerCase(Texto) = 'm�s de compet�ncia' then
          Result := 'MEZ'
        else if LowerCase(Texto) = 'data do lan�amento' then
          Result := 'Data'
        else
          Result := '';
      end;
    end;
  var
    Campo, Ordem: String;
    i, TotReg: Integer;
  begin
    Ordem  := '';
    TotReg := ListBox.Count - 1;
    for i := 0 to TotReg do
    begin
      if i = 0 then
      begin
        Campo := TraduzCampos(ListBox.Items[i], Receitas);
        if Length(Campo) > 0 then
          Ordem := Campo + ', ';
      end
      else if i = TotReg then
      begin
        Campo := TraduzCampos(ListBox.Items[i], Receitas);
        if Length(Campo) > 0 then
          Ordem := Ordem + Campo;// + ';';
      end
      else
      begin
        Campo := TraduzCampos(ListBox.Items[i], Receitas);
        if Length(Campo) > 0 then
          Ordem := Ordem + Campo + ', ';
      end;
    end;
    Result := Ordem + ', Controle;';
  end;
  procedure GeraParteSQL_Cred(TabLct: String);
  begin
    if NaoAgruparNada = True then
    begin
      case RGPagRec.Itemindex of
        0: QrCreditos.SQL.Add('SELECT lct.Mez, lct.Credito, "' +
           Uppercase(Copy(TabLct, Length(TabLct))) + '" TbLct, ');
        1: QrCreditos.SQL.Add(
           'SELECT lct.Mez, lct.Credito - lct.Debito Credito, "' +
           Uppercase(Copy(TabLct, Length(TabLct))) + '" TbLct, ');
      end;
    end else
    begin
      case RGPagRec.Itemindex of
        0: QrCreditos.SQL.Add(
           'SELECT lct.Mez, SUM(lct.Credito) Credito, "" TbLct, ');
        1: QrCreditos.SQL.Add(
           'SELECT lct.Mez, SUM(lct.Credito-lct.Debito) Credito, "" TbLct, ');
      end;
    end;
    QrCreditos.SQL.Add('lct.Controle, lct.Sub, lct.Carteira, lct.Cartao, lct.Tipo,');
    QrCreditos.SQL.Add('lct.Vencimento, lct.Compensado, lct.Sit, lct.Genero, ');
    QrCreditos.SQL.Add('lct.SubPgto1, lct.Data, gru.OrdemLista GRU_OL, ');
    QrCreditos.SQL.Add('con.OrdemLista CON_OL, sgr.OrdemLista SGR_OL,');
    QrCreditos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU');
    QrCreditos.SQL.Add('FROM ' + TabLct + ' lct');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lct.Genero');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    case RGPagRec.Itemindex of
      0:
      begin
        QrCreditos.SQL.Add('WHERE lct.Tipo <> 2');
        QrCreditos.SQL.Add('AND lct.Credito > 0');
      end;
      1:
      begin
        QrCreditos.SQL.Add('WHERE lct.ID_Pgto=0 ');
        QrCreditos.SQL.Add(
        'AND (con.PagRec=1 OR (con.PagRec=0 AND lct.Credito - lct.Debito > 0))');
      end else begin
        QrCreditos.SQL.Add('WHERE PagRec ????');
      end;
    end;
    QrCreditos.SQL.Add('AND lct.Genero>0');
    QrCreditos.SQL.Add('AND lct.Sit IN (' + CO_LIST_SITS_OKA + ')');
    QrCreditos.SQL.Add('AND car.ForneceI=' + FEntidTXT);
    case FormaPsqPeriodo of
      fppMezCmpt: QrCreditos.SQL.Add('AND lct.Mez=' + Geral.FF0(FMez));
      else (*fppDtaEmis,
      fppDtaQuit:*) QrCreditos.SQL.Add('AND lct.Data BETWEEN "' + FDataIniA +
        '" AND "' + FDataFimA + '"');
      (*
      else
      begin
        Geral.MB_Erro('"FormaPsqPeriodo" n�o implementado!');
      end;
      *)
    end;
    if FNivPlaCtaUsar then
    begin
      QrCreditos.SQL.Add('AND ' +
      dmkPF.NomeDoNivelSelecionado(4, RGNivel.ItemIndex) + '.' +
      dmkPF.NomeDoNivelSelecionado(5, RGNivel.ItemIndex) + ' IN (' +
      Geral.FF0(FNivPlaCtaCred) + ', ' + Geral.FF0(FNivPlaCtaDebi) + ')');
    end;
    if CkCentroRes.Checked then
      QrCreditos.SQL.Add('AND con.CentroRes IN (' + FCentrosRes + ')');
    // FIM 2014-04-18

    //Desativado por causa de condom�nios que foram para o morto e voltaram
    //nestes condom�nios duplica e nos demais n�o apresentaram problemas ao desabilitar
    //QrCreditos.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
    if NaoAgruparNada = False then
    begin
      QrCreditos.SQL.Add('GROUP BY lct.Genero ');
      if Periodos then
        QrCreditos.SQL.Add(', lct.Mez');
      if Acordos then
        QrCreditos.SQL.Add(', lct.SubPgto1');
    end;
  end;

  procedure GeraParteSQL_Debi(TabLct: String);
  begin
    if Textos and (NaoAgruparNada = False) then
    begin
      QrDebitos.SQL.Add('SELECT IF(COUNT(lct.Compensado) > 1, "V�rias", IF(lct.Compensado<2,"",');
      QrDebitos.SQL.Add('DATE_FORMAT(lct.Compensado, "%d/%m/%y"))) COMPENSADO_TXT,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, "V�rias", DATE_FORMAT(lct.Data, "%d/%m/%y")) DATA,');
      QrDebitos.SQL.Add('lct.Descricao, ');
      case RGPagRec.Itemindex of
        0: QrDebitos.SQL.Add('SUM(lct.Debito) DEBITO,');
        1: QrDebitos.SQL.Add('SUM(lct.Debito-lct.Credito) DEBITO,');
      end;
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.NotaFiscal) NOTAFISCAL,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, "", lct.SerieCH) SERIECH,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.Documento) DOCUMENTO,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.Mez) MEZ, lct.Compensado,');
      QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
      QrDebitos.SQL.Add('COUNT(lct.Data) ITENS, "" TbLct, ');
    end else begin
      QrDebitos.SQL.Add('SELECT IF(lct.Compensado<2,"", DATE_FORMAT(');
      QrDebitos.SQL.Add('lct.Compensado, "%d/%m/%y")) COMPENSADO_TXT,');
      QrDebitos.SQL.Add('DATE_FORMAT(lct.Data, "%d/%m/%y") DATA, ');
      QrDebitos.SQL.Add('lct.Descricao, ');
      case RGPagRec.Itemindex of
        0: QrDebitos.SQL.Add('lct.Debito, ');
        1: QrDebitos.SQL.Add('lct.Debito-lct.Credito Debito, ');
      end;
      QrDebitos.SQL.Add('IF(lct.NotaFiscal = 0, 0, lct.NotaFiscal) NOTAFISCAL,');
      QrDebitos.SQL.Add('IF(lct.SerieCH = "", "", lct.SerieCH) SERIECH,');
      QrDebitos.SQL.Add('IF(lct.Documento = 0, 0, lct.Documento) DOCUMENTO,');
      QrDebitos.SQL.Add('IF(lct.Mez = 0, 0, lct.Mez) MEZ, lct.Compensado,');
      QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
      QrDebitos.SQL.Add('0 ITENS, "' + Uppercase(Copy(TabLct, Length(TabLct))) + '" TbLct, ')
    end;
    QrDebitos.SQL.Add('lct.Vencimento, lct.Sit, lct.Genero, ');
    QrDebitos.SQL.Add('lct.Tipo, lct.Controle, lct.Sub, lct.Carteira, lct.Cartao,');
    QrDebitos.SQL.Add('gru.OrdemLista GRU_OL, con.OrdemLista CON_OL, ');
    QrDebitos.SQL.Add('sgr.OrdemLista SGR_OL, IF(frn.Codigo=0, "", ');
    QrDebitos.SQL.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)) NO_FORNECE');
    QrDebitos.SQL.Add('FROM ' + TabLct + ' lct');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lct.Genero');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades frn ON frn.Codigo=lct.Fornecedor');
    case RGPagRec.Itemindex of
      0:
      begin
        QrDebitos.SQL.Add('WHERE lct.Tipo <> 2');
        QrDebitos.SQL.Add('AND lct.Debito > 0');
      end;
      1:
      begin
        QrDebitos.SQL.Add('WHERE lct.ID_Pgto=0 ');
        QrDebitos.SQL.Add(
        'AND (con.PagRec=-1 OR (con.PagRec=0 AND lct.Credito - lct.Debito < 0))');
      end else begin
        QrDebitos.SQL.Add('WHERE PagRec ????');
      end;
    end;
    QrDebitos.SQL.Add('AND lct.Genero>0');
    // 2011-12-31
    QrDebitos.SQL.Add('AND lct.Sit IN (' + CO_LIST_SITS_OKA + ')');
    // Fim 2011-12-31
    QrDebitos.SQL.Add('AND car.ForneceI=' + FEntidTXT);
    QrDebitos.SQL.Add('AND lct.Data BETWEEN "' + FDataIniA + '" AND "' + FDataFimA + '"');
    if FNivPlaCtaUsar then
    begin
      QrDebitos.SQL.Add('AND ' +
      dmkPF.NomeDoNivelSelecionado(4, RGNivel.ItemIndex) + '.' +
      dmkPF.NomeDoNivelSelecionado(5, RGNivel.ItemIndex) + ' IN (' +
      Geral.FF0(FNivPlaCtaCred) + ', ' + Geral.FF0(FNivPlaCtaDebi) + ')');
    end;
    if CkCentroRes.Checked then
      QrDebitos.SQL.Add('AND con.CentroRes IN (' + FCentrosRes + ')');
    //Desativado por causa de condom�nios que foram para o morto e voltaram
    //nestes condom�nios duplica e nos demais n�o apresentaram problemas ao desabilitar
    //QrDebitos.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
    //
    if CkNaoAgruparNada.Checked = False then
    begin
      if Textos then
        QrDebitos.SQL.Add('GROUP BY lct.Descricao');
    end;
  end;

begin
  Result := False;
  QrCreditos.Database := DModG.MyPID_DB;
  QrDebitos.Database  := DModG.MyPID_DB;
  //
  //// C R � D I T O S
  QrCreditos.Close;
  QrCreditos.SQL.Clear;
  //
  QrCreditos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_CRED_1_;');
  QrCreditos.SQL.Add('CREATE TABLE _MOD_COND_CRED_1_');
  QrCreditos.SQL.Add('');
  GeraParteSQL_Cred(TabLctA);
  QrCreditos.SQL.Add('UNION');
  GeraParteSQL_Cred(TabLctB);
  QrCreditos.SQL.Add('UNION');
  GeraParteSQL_Cred(TabLctD);
  QrCreditos.SQL.Add(';');
  QrCreditos.SQL.Add('');
  if NaoAgruparNada = False then
  begin
    QrCreditos.SQL.Add('SELECT Mez, SUM(Credito) Credito, Controle, Sub, ');
    QrCreditos.SQL.Add('Carteira, Cartao, Tipo, Vencimento, Compensado, Sit,');
    QrCreditos.SQL.Add('Genero, SubPgto1, Data, GRU_OL, CON_OL, SGR_OL, ');
    QrCreditos.SQL.Add('NOMECON, NOMESGR, NOMEGRU, TbLct');
    QrCreditos.SQL.Add('FROM _MOD_COND_CRED_1_');
    QrCreditos.SQL.Add('GROUP BY Genero ');
    if Periodos then
      QrCreditos.SQL.Add(', Mez');
    if Acordos then
      QrCreditos.SQL.Add(', SubPgto1');
  end else
    QrCreditos.SQL.Add('SELECT * FROM _MOD_COND_CRED_1_');
  //QrCreditos.SQL.Add('ORDER BY GRU_OL, NOMEGRU, SGR_OL, ');
  //QrCreditos.SQL.Add('NOMESGR, CON_OL, NOMECON, Mez, Data;');
  QrCreditos.SQL.Add('ORDER BY ' + OrdenaItens(LBReceitas, True));
  QrCreditos.SQL.Add('');
  // N�o pode !!
  //QrCreditos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_CRED_1_;');
  QrCreditos.SQL.Add('');
  //
  UMyMod.AbreQuery(QrCreditos, DModG.MyPID_DB, 'TDCond.ReopenCreditosEDebitos()');
  //
  //
  //// D � B I T O S
  QrDebitos.Close;
  QrDebitos.SQL.Clear;
  //
  QrDebitos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_DEBI_1_;');
  QrDebitos.SQL.Add('CREATE TABLE _MOD_COND_DEBI_1_');
  QrDebitos.SQL.Add('');
  GeraParteSQL_Debi(TabLctA);
  QrDebitos.SQL.Add('UNION');
  GeraParteSQL_Debi(TabLctB);
  QrDebitos.SQL.Add('UNION');
  GeraParteSQL_Debi(TabLctD);
  QrDebitos.SQL.Add(';');
  QrDebitos.SQL.Add('');
  if (NaoAgruparNada = False) and (Textos) then
  begin
    QrDebitos.SQL.Add('SELECT COMPENSADO_TXT, DATA,Descricao, NO_FORNECE, ');
    QrDebitos.SQL.Add('SUM(DEBITO) DEBITO, NOTAFISCAL, SERIECH, DOCUMENTO, ');
    QrDebitos.SQL.Add('MEZ, Compensado, NOMECON, NOMESGR, NOMEGRU, ');
    QrDebitos.SQL.Add('ITENS, Vencimento, Sit, Genero, Tipo, Controle,');
    QrDebitos.SQL.Add('Sub, Carteira, Cartao, GRU_OL, CON_OL, SGR_OL, TbLct');
    QrDebitos.SQL.Add('FROM _MOD_COND_DEBI_1_');
    QrDebitos.SQL.Add('GROUP BY Descricao')
  end else
    QrDebitos.SQL.Add('SELECT * FROM _MOD_COND_DEBI_1_');
  //QrDebitos.SQL.Add('ORDER BY GRU_OL, NOMEGRU, SGR_OL,');
  //QrDebitos.SQL.Add('NOMESGR, CON_OL, NOMECON, Data;');
  QrDebitos.SQL.Add('ORDER BY ' + OrdenaItens(LBDespesas, False));
  //
  // N�o pode !!
  //QrDebitos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_DEBI_1_;');
  UMyMod.AbreQuery(QrDebitos, DModG.MyPID_DB, 'procedure TDmCond.ReopenDEBIitosEDebitos()');
  Result := True;
end;

procedure TFmReceDesp2.ReopenNivelSel();
var
  Nivel: String;
begin
  EdNivelSel1.ValueVariant := 0;
  CBNivelSel1.KeyValue     := Null;
  QrNivelSel1.Close;
  //
  EdNivelSel2.ValueVariant := 0;
  CBNivelSel2.KeyValue     := Null;
  QrNivelSel2.Close;
  //
  if RGNivel.ItemIndex > 0 then
  begin
    Nivel := dmkPF.NomeDoNivelSelecionado(1, RGNivel.ItemIndex);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNivelSel1, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM ' + Nivel,
    'ORDER BY Nome ',
    '']);
    UnDmkDAC_PF.AbreMySQLQuery0(QrNivelSel2, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM ' + Nivel,
    'ORDER BY Nome ',
    '']);
  end;
end;

procedure TFmReceDesp2.RetornosCNAB();
begin
  Geral.MB_Info('N�o implementado');
  // Est� funcionando normalmente, s� precisa mudar (customizar) o componente
  // da origem das datas para pesquisa!
(*
    FDataIniA := Geral.FDT(FDataIniA_Date, 1);
    FDataFimA := Geral.FDT(FDataFimA_Date, 1);
    FDataIniB := Geral.FDT(TPDataIni2.Date, 1);
    FDataFimB := Geral.FDT(TPDataFim2.Date, 1);
/
  if FEmpresa > 0 then
  begin
    QrRet.Close;
    QrRet.SQL.Clear;
    QrRet.SQL.Add('SELECT *');
    QrRet.SQL.Add('FROM cnab_lei');
    QrRet.SQL.Add('WHERE Entidade=' + dmkPF.FFP(FEntidade, 0));
    QrRet.SQL.Add(dmkPF.SQL_Periodo('AND QuitaData ', TPDataIni2.Date,
      TPDataFim2.Date, True, True));
    QrRet.SQL.Add(dmkPF.SQL_Periodo('AND OcorrData ', TPDataIni2.Date,
      TPDataFim2.Date, True, True));
    QrRet.SQL.Add('AND ValTitul BETWEEN ' +
      dmkPF.FFP(EdValTitMin.ValueVariant, 2) + ' AND '  +
      dmkPF.FFP(EdValTitMax.ValueVariant, 2));
    QrRet.SQL.Add('AND ValPago BETWEEN ' +
      dmkPF.FFP(EdValPagMin.ValueVariant, 2) + ' AND '  +
      dmkPF.FFP(EdValPagMax.ValueVariant, 2));
    if EdBloqIni.ValueVariant > 0 then
      QrRet.SQL.Add('AND IDNum >= ' + dmkPF.FFP(EdBloqIni.ValueVariant, 0));
    if EdBloqFim.ValueVariant > 0 then
      QrRet.SQL.Add('AND IDNum <= ' + dmkPF.FFP(EdBloqIni.ValueVariant, 0));
    UnDmkDAC_PF.AbreQuery(QrRet, Dmod.MyDB);
  end else begin
    Geral.MB_Aviso('Informe a empresa!');
    EdEmpresa.SetFocus;
  end;
*)
end;

procedure TFmReceDesp2.RGNivelClick(Sender: TObject);
begin
  ReopenNivelSel();
end;

{
SELECT pla.OrdemLista OL_PLA, cjt.Plano, pla.Nome NO_PLA,
cjt.OrdemLista OL_CJT, gru.Conjunto, cjt.Nome NO_CJT,
gru.OrdemLista OL_GRU, sgr.Grupo, gru.Nome NO_GRU,
sgr.OrdemLista OL_SGR, cta.SubGrupo, sgr.Nome NO_SGR,
cta.OrdemLista OL_CTA, lct.Genero, cta.Nome NO_CTA,
SUM(lct.Credito) Credito
FROM lct0046a lct
LEFT JOIN contas cta ON cta.Codigo=lct.Genero
LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo
LEFT JOIN grupos gru ON gru.Codigo=sgr.Grupo
LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto
LEFT JOIN plano pla ON pla.Codigo=cjt.Plano
LEFT JOIN carteiras car ON car.Codigo=lct.Carteira
WHERE lct.Genero > 0
AND lct.Data BETWEEN "2011-01-01" AND "2012-04-30"
AND lct.Credito > 0
AND car.Tipo IN (0,1)
GROUP BY lct.Genero
ORDER BY OL_PLA, NO_PLA, OL_CJT, NO_CJT, OL_GRU, NO_GRU,
OL_SGR, NO_SGR, OL_CTA, NO_CTA
}

end.

