object FmResultados1: TFmResultados1
  Left = 402
  Top = 166
  Caption = 'FIN-RELAT-002 :: Resultados Financeiros por Per'#237'odo'
  ClientHeight = 494
  ClientWidth = 780
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 780
    Height = 332
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 780
      Height = 56
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object RGTipoRel: TRadioGroup
        Left = 8
        Top = 4
        Width = 761
        Height = 45
        Caption = ' Tipo de relat'#243'rio: '
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Resultado por CONTAS'
          'Resultado por LAN'#199'AMENTOS'
          'Balancete industrial modelo 01')
        TabOrder = 0
        OnClick = RGTipoRelClick
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 56
      Width = 780
      Height = 276
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label3: TLabel
        Left = 392
        Top = 8
        Width = 45
        Height = 13
        Caption = 'Conjunto:'
      end
      object Label4: TLabel
        Left = 392
        Top = 52
        Width = 32
        Height = 13
        Caption = 'Grupo:'
      end
      object Label5: TLabel
        Left = 392
        Top = 96
        Width = 52
        Height = 13
        Caption = 'Sub-grupo:'
      end
      object Label6: TLabel
        Left = 392
        Top = 140
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object CkDataIni: TCheckBox
        Left = 8
        Top = 4
        Width = 170
        Height = 17
        Caption = 'Data inicial:'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object TPDataIni: TDateTimePicker
        Left = 8
        Top = 24
        Width = 186
        Height = 21
        Date = 37660.516375590300000000
        Time = 37660.516375590300000000
        TabOrder = 1
      end
      object CkVctoIni: TCheckBox
        Left = 8
        Top = 48
        Width = 170
        Height = 17
        Caption = 'Vencimento inicial:'
        TabOrder = 2
      end
      object TPVctoIni: TDateTimePicker
        Left = 8
        Top = 68
        Width = 186
        Height = 21
        Date = 37660.516375590300000000
        Time = 37660.516375590300000000
        TabOrder = 3
      end
      object CkDtDocIni: TCheckBox
        Left = 8
        Top = 92
        Width = 170
        Height = 17
        Caption = 'Data doc. inicial'
        TabOrder = 4
      end
      object TPDtDocIni: TDateTimePicker
        Left = 8
        Top = 112
        Width = 186
        Height = 21
        Date = 37660.516375590300000000
        Time = 37660.516375590300000000
        TabOrder = 5
      end
      object CkDataFim: TCheckBox
        Left = 200
        Top = 4
        Width = 170
        Height = 17
        Caption = 'Data final:'
        Checked = True
        State = cbChecked
        TabOrder = 6
      end
      object TPDataFim: TDateTimePicker
        Left = 200
        Top = 24
        Width = 186
        Height = 21
        Date = 37660.516439456000000000
        Time = 37660.516439456000000000
        TabOrder = 7
      end
      object CkVctoFim: TCheckBox
        Left = 200
        Top = 48
        Width = 170
        Height = 17
        Caption = 'Vencimento final:'
        TabOrder = 8
      end
      object TPVctoFim: TDateTimePicker
        Left = 200
        Top = 68
        Width = 186
        Height = 21
        Date = 37660.516439456000000000
        Time = 37660.516439456000000000
        TabOrder = 9
      end
      object CkDtDocFim: TCheckBox
        Left = 200
        Top = 92
        Width = 170
        Height = 17
        Caption = 'Data doc. final:'
        TabOrder = 10
      end
      object TPDtDocFim: TDateTimePicker
        Left = 200
        Top = 112
        Width = 186
        Height = 21
        Date = 37660.516439456000000000
        Time = 37660.516439456000000000
        TabOrder = 11
      end
      object EdConjunto: TdmkEditCB
        Left = 392
        Top = 24
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBConjunto
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBConjunto: TdmkDBLookupComboBox
        Left = 460
        Top = 24
        Width = 309
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsConjuntos
        TabOrder = 13
        OnKeyDown = CBConjuntoKeyDown
        dmkEditCB = EdConjunto
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBGrupo: TdmkDBLookupComboBox
        Left = 460
        Top = 68
        Width = 309
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGrupos
        TabOrder = 14
        OnKeyDown = CBGrupoKeyDown
        dmkEditCB = EdGrupo
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGrupo: TdmkEditCB
        Left = 392
        Top = 68
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGrupo
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdSubgrupo: TdmkEditCB
        Left = 392
        Top = 112
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSubgrupo
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSubgrupo: TdmkDBLookupComboBox
        Left = 460
        Top = 112
        Width = 309
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSubgrupos
        TabOrder = 17
        OnKeyDown = CBSubgrupoKeyDown
        dmkEditCB = EdSubgrupo
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBConta: TdmkDBLookupComboBox
        Left = 460
        Top = 156
        Width = 309
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 18
        OnKeyDown = CBContaKeyDown
        dmkEditCB = EdConta
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdConta: TdmkEditCB
        Left = 392
        Top = 156
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 19
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBConta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object RGExclusivos: TRadioGroup
        Left = 8
        Top = 137
        Width = 377
        Height = 41
        Caption = ' Tipo de lan'#231'amentos: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Sem exclusivos'
          'Com exclusivos'
          'S'#243' exclusivos')
        TabOrder = 20
      end
      object CkNome2: TCheckBox
        Left = 8
        Top = 184
        Width = 377
        Height = 17
        Caption = 'Utilizar a descri'#231#227'o substituta nas contas.'
        Checked = True
        State = cbChecked
        TabOrder = 21
      end
      object CkSaldo: TCheckBox
        Left = 392
        Top = 184
        Width = 377
        Height = 17
        Caption = 'Informar saldo final do fluxo de caixa.'
        Checked = True
        State = cbChecked
        TabOrder = 22
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 204
        Width = 761
        Height = 65
        Caption = ' Balancete industrial: '
        TabOrder = 23
        object Label1: TLabel
          Left = 352
          Top = 20
          Width = 71
          Height = 13
          Caption = 'Cliente Interno:'
        end
        object RGPeriodoBalInd: TRadioGroup
          Left = 8
          Top = 16
          Width = 341
          Height = 45
          Caption = ' Per'#237'odo a ser pesquisado: '
          Columns = 3
          ItemIndex = 2
          Items.Strings = (
            'Data'
            'Vencimento'
            'Data doc.')
          TabOrder = 0
        end
        object EdCliInt: TdmkEditCB
          Left = 352
          Top = 36
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCliInt
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCliInt: TdmkDBLookupComboBox
          Left = 408
          Top = 36
          Width = 345
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMECLI'
          ListSource = DsCliInt
          TabOrder = 2
          dmkEditCB = EdCliInt
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 780
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 732
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 684
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 151
        Height = 32
        Caption = '??? ??? ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 151
        Height = 32
        Caption = '??? ??? ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 151
        Height = 32
        Caption = '??? ??? ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 380
    Width = 780
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 776
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 424
    Width = 780
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 634
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 632
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImprime: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImprimeClick
      end
    end
  end
  object QrGrupos: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrGruposAfterScroll
    SQL.Strings = (
      'SELECT * FROM Grupos'
      'ORDER BY Nome')
    Left = 300
    Top = 180
    object QrGruposCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.grupos.Codigo'
    end
    object QrGruposNome: TWideStringField
      DisplayWidth = 50
      FieldName = 'Nome'
      Origin = 'DBMMONEY.grupos.Nome'
      Size = 50
    end
    object QrGruposConjunto: TIntegerField
      FieldName = 'Conjunto'
      Origin = 'DBMMONEY.grupos.Conjunto'
    end
    object QrGruposLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.grupos.Lk'
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 328
    Top = 180
  end
  object QrConjuntos: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrConjuntosAfterScroll
    SQL.Strings = (
      'SELECT * FROM Conjuntos'
      'ORDER BY Nome')
    Left = 300
    Top = 132
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.conjuntos.Codigo'
    end
    object StringField1: TWideStringField
      DisplayWidth = 50
      FieldName = 'Nome'
      Origin = 'DBMMONEY.conjuntos.Nome'
      Size = 50
    end
    object IntegerField2: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.conjuntos.Lk'
    end
  end
  object DsConjuntos: TDataSource
    DataSet = QrConjuntos
    Left = 328
    Top = 132
  end
  object QrSubgrupos: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrSubgruposAfterScroll
    SQL.Strings = (
      'SELECT * FROM Subgrupos'
      'ORDER BY Nome')
    Left = 672
    Top = 124
    object QrSubgruposCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.subgrupos.Codigo'
    end
    object QrSubgruposNome: TWideStringField
      DisplayWidth = 50
      FieldName = 'Nome'
      Origin = 'DBMMONEY.subgrupos.Nome'
      Size = 50
    end
    object QrSubgruposGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'DBMMONEY.subgrupos.Grupo'
    end
    object QrSubgruposLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.subgrupos.Lk'
    end
  end
  object DsSubgrupos: TDataSource
    DataSet = QrSubgrupos
    Left = 700
    Top = 124
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM Contas'
      'ORDER BY Nome')
    Left = 672
    Top = 228
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.contas.Codigo'
    end
    object QrContasNome: TWideStringField
      DisplayWidth = 50
      FieldName = 'Nome'
      Origin = 'DBMMONEY.contas.Nome'
      Size = 50
    end
    object QrContasNome2: TWideStringField
      DisplayWidth = 50
      FieldName = 'Nome2'
      Origin = 'DBMMONEY.contas.Nome2'
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Origin = 'DBMMONEY.contas.ID'
      Size = 128
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Origin = 'DBMMONEY.contas.Subgrupo'
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'DBMMONEY.contas.Empresa'
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.contas.Credito'
      FixedChar = True
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.contas.Debito'
      FixedChar = True
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Origin = 'DBMMONEY.contas.Mensal'
      FixedChar = True
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Origin = 'DBMMONEY.contas.Exclusivo'
      FixedChar = True
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
      Origin = 'DBMMONEY.contas.Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
      Origin = 'DBMMONEY.contas.Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
      Origin = 'DBMMONEY.contas.Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
      Origin = 'DBMMONEY.contas.Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
      Origin = 'DBMMONEY.contas.Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.contas.Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Origin = 'DBMMONEY.contas.Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Origin = 'DBMMONEY.contas.Excel'
      Size = 128
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 700
    Top = 228
  end
  object QrResultLA: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrResultLACalcFields
    Left = 64
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrResultLANOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'DBMMONEY.contas.Nome'
      Size = 128
    end
    object QrResultLANOMECONTA2: TWideStringField
      FieldName = 'NOMECONTA2'
      Origin = 'DBMMONEY.contas.Nome2'
      Size = 128
    end
    object QrResultLANOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'DBMMONEY.subgrupos.Nome'
      Size = 128
    end
    object QrResultLANOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'DBMMONEY.grupos.Nome'
      Size = 128
    end
    object QrResultLANOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'DBMMONEY.conjuntos.Nome'
      Size = 128
    end
    object QrResultLANOMECONTA3: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECONTA3'
      Size = 50
      Calculated = True
    end
    object QrResultLANOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrResultLAData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrResultLATipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrResultLACarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrResultLAControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrResultLASub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrResultLAAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrResultLAGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrResultLADescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrResultLANotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrResultLADebito: TFloatField
      FieldName = 'Debito'
    end
    object QrResultLACredito: TFloatField
      FieldName = 'Credito'
    end
    object QrResultLACompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrResultLADocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrResultLASit: TIntegerField
      FieldName = 'Sit'
    end
    object QrResultLAVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrResultLALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrResultLAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrResultLAFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrResultLAID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrResultLAID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrResultLAFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrResultLABanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrResultLALocal: TIntegerField
      FieldName = 'Local'
    end
    object QrResultLACartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrResultLALinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrResultLAOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrResultLALancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrResultLAPago: TFloatField
      FieldName = 'Pago'
    end
    object QrResultLAMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrResultLAFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrResultLACliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrResultLAMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrResultLAMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrResultLAProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrResultLADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrResultLADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrResultLAUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrResultLAUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrResultLADataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrResultLACtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrResultLANivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrResultLAVendedor: TIntegerField
      FieldName = 'Vendedor'
      Required = True
    end
    object QrResultLAAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
    object QrResultLASubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrResultLAGrupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrResultLAConjunto: TIntegerField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrResultLAFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrResultCO: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrResultCOCalcFields
    Left = 152
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrResultCODEBITO: TFloatField
      FieldName = 'DEBITO'
    end
    object QrResultCOCREDITO: TFloatField
      FieldName = 'CREDITO'
    end
    object QrResultCONOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 128
    end
    object QrResultCONOMECONTA2: TWideStringField
      FieldName = 'NOMECONTA2'
      Size = 128
    end
    object QrResultCONOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 128
    end
    object QrResultCONOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 128
    end
    object QrResultCONOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 128
    end
    object QrResultCONOMECONTA3: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECONTA3'
      Size = 50
      Calculated = True
    end
    object QrResultCOData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrResultCODocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrResultCONotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrResultCOSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrResultCOGrupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrResultCOConjunto: TIntegerField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrResultCOcjOrdemLista: TIntegerField
      FieldName = 'cjOrdemLista'
      Required = True
    end
    object QrResultCOgrOrdemLista: TIntegerField
      FieldName = 'grOrdemLista'
      Required = True
    end
    object QrResultCOsgOrdemLista: TIntegerField
      FieldName = 'sgOrdemLista'
      Required = True
    end
    object QrResultCOcoOrdemLista: TIntegerField
      FieldName = 'coOrdemLista'
      Required = True
    end
  end
  object QrSInicial: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Inicial) Inicial FROM Carteiras')
    Left = 744
    Top = 8
    object QrSInicialInicial: TFloatField
      FieldName = 'Inicial'
      Origin = 'DBMMONEY.carteiras.Inicial'
    end
  end
  object QrMovimento: TMySQLQuery
    Database = Dmod.MyDB
    Left = 716
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovimentoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrMovimentoDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object frxResultCO: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39652.739028310200000000
    ReportOptions.LastChange = 39652.739028310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxResultCOGetValue
    Left = 96
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsResultCO
        DataSetName = 'frxDsResultCO'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 60.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 544.881880000000000000
          Top = 4.881880000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 8.000000000000000000
          Top = 21.102350000000000000
          Width = 716.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Resultados por CONTAS')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 42.897650000000000000
        Top = 718.110700000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 473.102350000000000000
          Top = 2.550710000000090000
          Width = 244.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 20.897340000000000000
        Top = 483.779840000000000000
        Width = 718.110700000000000000
        object Memo8: TfrxMemoView
          Left = 411.968477090000000000
          Top = 2.897339999999990000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."DEBITO">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 80.440940000000000000
          Top = 2.897340000000040000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 514.015728500000000000
          Top = 2.897339999999990000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."CREDITO">)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 144.440940000000000000
          Top = 2.897340000000040000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultCO."NOMESUBGRUPO"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 616.062982360000000000
          Top = 2.897339999999990000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<SUM(<frxDsResultCO."CREDITO">)> - <SUM(<frxDsResultCO."DEBITO"' +
              '>)>]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 936.661720000000000000
          Top = 17.795300000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."DEBITO">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 1024.661720000000000000
          Top = 17.795300000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."CREDITO">)]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 442.205010000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsResultCO
        DataSetName = 'frxDsResultCO'
        RowCount = 0
        object Memo15: TfrxMemoView
          Left = 116.000000000000000000
          Width = 292.000000000000000000
          Height = 18.000000000000000000
          DataField = 'NOMECONTA3'
          DataSet = frxDsResultCO
          DataSetName = 'frxDsResultCO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsResultCO."NOMECONTA3"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 411.968477090000000000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          DataSet = frxDsResultCO
          DataSetName = 'frxDsResultCO'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResultCO."DEBITO"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 514.015728500000000000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          DataField = 'CREDITO'
          DataSet = frxDsResultCO
          DataSetName = 'frxDsResultCO'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResultCO."CREDITO"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 74.000000000000000000
        Top = 102.047310000000000000
        Width = 718.110700000000000000
        object Memo55: TfrxMemoView
          Left = 176.000000000000000000
          Top = 38.968460000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Vecto]')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 4.000000000000000000
          Top = 5.291280000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo46: TfrxMemoView
          Left = 176.000000000000000000
          Top = 22.968460000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Periodo]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 175.637910000000000000
          Top = 54.692950000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[DtDoc]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 176.000000000000000000
          Width = 552.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 37.559060000000000000
        Top = 657.638220000000000000
        Width = 718.110700000000000000
        object Memo14: TfrxMemoView
          Left = 411.968770000000000000
          Top = 11.338590000000100000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."DEBITO">)]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 514.016021410000000000
          Top = 11.338590000000100000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."CREDITO">)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 616.063275270000000000
          Top = 11.338590000000100000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<SUM(<frxDsResultCO."CREDITO">)> - <SUM(<frxDsResultCO."DEBITO"' +
              '>)>]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 79.370130000000000000
          Top = 11.338590000000100000
          Width = 322.110390000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'RESULTADO DO PER'#205'ODO:')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 11.338590000000100000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 200.315090000000000000
        Width = 718.110700000000000000
        object Memo29: TfrxMemoView
          Left = 0.440940000000000000
          Top = 2.582560000000000000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 60.440940000000000000
          Top = 2.582560000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'Doc.')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 112.440940000000000000
          Top = 2.582560000000000000
          Width = 284.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 400.440940000000000000
          Top = 2.582560000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 453.543307086614000000
          Top = 2.582560000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 544.440940000000000000
          Top = 2.582560000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 631.181102362205100000
          Top = 2.582560000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.000000000000000000
        Top = 396.850650000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsResultCO."NOMESUBGRUPO"'
        object Memo2: TfrxMemoView
          Left = 49.574830000000000000
          Top = 1.826529999999990000
          Width = 664.787570000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultCO."NOMESUBGRUPO"]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.645330000000000000
        Top = 529.134200000000000000
        Width = 718.110700000000000000
        object Memo7: TfrxMemoView
          Left = 80.440940000000000000
          Top = 1.322509999999970000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 144.440940000000000000
          Top = 1.322509999999970000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultCO."NOMEGRUPO"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 411.968477090000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."DEBITO">)]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 514.015728500000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."CREDITO">)]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 616.062982360000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<SUM(<frxDsResultCO."CREDITO">)> - <SUM(<frxDsResultCO."DEBITO"' +
              '>)>]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        Height = 21.779530000000000000
        Top = 574.488560000000000000
        Width = 718.110700000000000000
        object Memo37: TfrxMemoView
          Left = 80.440940000000000000
          Top = 3.527209999999970000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 144.440940000000000000
          Top = 3.527209999999970000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultCO."NOMECONJUNTO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 411.968477090000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."DEBITO">)]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 514.015728500000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."CREDITO">)]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 616.062982360000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<SUM(<frxDsResultCO."CREDITO">)> - <SUM(<frxDsResultCO."DEBITO"' +
              '>)>]')
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 28.000000000000000000
        Top = 347.716760000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsResultCO."NOMEGRUPO"'
        object Line2: TfrxLineView
          Left = 8.000000000000000000
          Top = 25.180890000000000000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo39: TfrxMemoView
          Left = 8.000000000000000000
          Top = 3.180889999999980000
          Width = 706.362400000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultCO."NOMEGRUPO"]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 287.244280000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsResultCO."NOMECONJUNTO"'
        object Line1: TfrxLineView
          Left = 8.000000000000000000
          Top = 30.094310000000000000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Style = fsDouble
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo40: TfrxMemoView
          Left = 8.000000000000000000
          Top = 0.094310000000007220
          Width = 706.362400000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultCO."NOMECONJUNTO"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsResultCO: TfrxDBDataset
    UserName = 'frxDsResultCO'
    CloseDataSource = False
    DataSet = QrResultCO
    BCDToCurrency = False
    Left = 124
    Top = 8
  end
  object frxResultLA: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39652.818819074100000000
    ReportOptions.LastChange = 39652.818819074100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxResultCOGetValue
    Left = 8
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsResultLA
        DataSetName = 'frxDsResultLA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 60.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 534.000000000000000000
          Top = 1.102350000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 26.000000000000000000
          Top = 21.102350000000000000
          Width = 672.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Resultados por LAN'#199'AMENTOS')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 24.000000000000000000
        Top = 782.362710000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 472.677180000000000000
          Top = 2.330240000000000000
          Width = 244.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 525.354670000000000000
        Width = 718.110700000000000000
        object Memo8: TfrxMemoView
          Left = 442.204724410000000000
          Width = 86.929133860000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 529.133858270000000000
          Width = 86.929133860000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 146.000000000000000000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMECONTA3"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 616.062992130000000000
          Width = 99.118120000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">) - SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 19.826530000000000000
        Top = 396.850650000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsResultLA."Genero"'
        object Memo2: TfrxMemoView
          Left = 26.000000000000000000
          Top = 1.826529999999990000
          Width = 404.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMESUBGRUPO"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 483.779840000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsResultLA
        DataSetName = 'frxDsResultLA'
        RowCount = 0
        object Memo13: TfrxMemoView
          Left = 26.000000000000000000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          DataField = 'Data'
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsResultLA."Data"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.000000000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haBlock
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsResultLA."Documento">)]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 130.000000000000000000
          Width = 204.000000000000000000
          Height = 18.000000000000000000
          DataField = 'Descricao'
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsResultLA."Descricao"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 334.000000000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsResultLA."NotaFiscal">)]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 442.204724410000000000
          Width = 86.929133860000000000
          Height = 18.000000000000000000
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResultLA."Debito"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 529.133860710000000000
          Width = 86.929133860000000000
          Height = 18.000000000000000000
          DataField = 'Credito'
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResultLA."Credito"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 386.000000000000000000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsResultLA."Controle">)]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 616.062992130000000000
          Width = 99.559060000000000000
          Height = 18.000000000000000000
          DataField = 'NOMECARTEIRA'
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsResultLA."NOMECARTEIRA"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 74.000000000000000000
        Top = 102.047310000000000000
        Width = 718.110700000000000000
        object Picture2: TfrxPictureView
          Left = 26.000000000000000000
          Top = 5.291280000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo55: TfrxMemoView
          Left = 196.897650000000000000
          Top = 39.031540000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Vecto]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 196.897650000000000000
          Top = 23.031540000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Periodo]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 196.535560000000000000
          Top = 54.756030000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[DtDoc]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 196.897650000000000000
          Width = 552.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.000000000000000000
        Top = 729.449290000000000000
        Width = 718.110700000000000000
        object Memo12: TfrxMemoView
          Left = 30.000000000000000000
          Top = 6.582250000000040000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo anterior:')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 126.000000000000000000
          Top = 6.582250000000040000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[Anterior]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 222.000000000000000000
          Top = 6.582250000000040000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 422.000000000000000000
          Top = 6.582250000000040000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[SCredito]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 362.000000000000000000
          Top = 6.582250000000040000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 506.000000000000000000
          Top = 6.582250000000040000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo Final:')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 602.000000000000000000
          Top = 6.582250000000040000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[Final]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 278.000000000000000000
          Top = 6.582250000000040000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[SDebito]')
          ParentFont = False
        end
        object Line13: TfrxLineView
          Left = 30.000000000000000000
          Top = 3.779530000000020000
          Width = 668.000000000000000000
          Color = clBlack
          Frame.Style = fsDouble
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 200.315090000000000000
        Width = 718.110700000000000000
        object Memo29: TfrxMemoView
          Left = 26.000000000000000000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 82.000000000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'Doc.')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 130.000000000000000000
          Width = 204.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 334.000000000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 442.000000000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 526.000000000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 610.000000000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Nome carteira')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 386.000000000000000000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.000000000000000000
        Top = 438.425480000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsResultLA."NOMESUBGRUPO"'
        object Memo1: TfrxMemoView
          Left = 26.000000000000000000
          Top = 0.251700000000028000
          Width = 404.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMECONTA3"]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 566.929500000000000000
        Width = 718.110700000000000000
        object Memo4: TfrxMemoView
          Left = 82.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 146.000000000000000000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMESUBGRUPO"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 442.205010000000000000
          Width = 86.929133860000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 529.134143860000000000
          Width = 86.929133860000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">)]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 616.063277720000000000
          Width = 99.118120000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">) - SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 608.504330000000000000
        Width = 718.110700000000000000
        object Memo7: TfrxMemoView
          Left = 82.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 146.000000000000000000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMEGRUPO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 442.205010000000000000
          Width = 86.929133860000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 529.134143860000000000
          Width = 86.929133860000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">)]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 616.063277720000000000
          Width = 99.118120000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">) - SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 650.079160000000000000
        Width = 718.110700000000000000
        object Memo37: TfrxMemoView
          Left = 82.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 146.000000000000000000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMECONJUNTO"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 442.205010000000000000
          Width = 86.929133860000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 529.134143860000000000
          Width = 86.929133860000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">)]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 616.063277720000000000
          Width = 99.118120000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">) - SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 28.000000000000000000
        Top = 347.716760000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsResultLA."NOMEGRUPO"'
        object Line2: TfrxLineView
          Left = 26.000000000000000000
          Top = 22.677165354330700000
          Width = 660.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo39: TfrxMemoView
          Left = 26.000000000000000000
          Width = 404.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMEGRUPO"]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 287.244280000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsResultLA."NOMECONJUNTO"'
        object Line1: TfrxLineView
          Left = 26.000000000000000000
          Top = 30.094310000000000000
          Width = 660.000000000000000000
          Color = clBlack
          Frame.Style = fsDouble
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo40: TfrxMemoView
          Left = 26.000000000000000000
          Top = 0.094310000000007220
          Width = 404.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMECONJUNTO"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsResultLA: TfrxDBDataset
    UserName = 'frxDsResultLA'
    CloseDataSource = False
    DataSet = QrResultLA
    BCDToCurrency = False
    Left = 36
    Top = 8
  end
  object frxInd01: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39652.739028310200000000
    ReportOptions.LastChange = 39652.739028310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxResultCOGetValue
    Left = 552
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsInd01
        DataSetName = 'frxDsInd01'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 60.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 544.881880000000000000
          Top = 4.881880000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 8.000000000000000000
          Top = 21.102350000000000000
          Width = 716.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'BALANCETE MENSAL')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 42.897650000000000000
        Top = 718.110700000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 473.102350000000000000
          Top = 2.550710000000090000
          Width = 244.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 20.897340000000000000
        Top = 483.779840000000000000
        Width = 718.110700000000000000
        object Memo8: TfrxMemoView
          Left = 411.968477090000000000
          Top = 2.897339999999990000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."DEBITO">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 80.440940000000000000
          Top = 2.897340000000040000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 514.015728500000000000
          Top = 2.897339999999990000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."CREDITO">)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 144.440940000000000000
          Top = 2.897340000000040000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsInd01."NOMESUBGRUPO"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 616.062982360000000000
          Top = 2.897339999999990000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[<SUM(<frxDsInd01."CREDITO">)> - <SUM(<frxDsInd01."DEBITO">)>]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 936.661720000000000000
          Top = 17.795300000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."DEBITO">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 1024.661720000000000000
          Top = 17.795300000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."CREDITO">)]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 442.205010000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsInd01
        DataSetName = 'frxDsInd01'
        RowCount = 0
        object Memo15: TfrxMemoView
          Left = 116.000000000000000000
          Width = 292.000000000000000000
          Height = 18.000000000000000000
          DataField = 'NOMECONTA3'
          DataSet = frxDsInd01
          DataSetName = 'frxDsInd01'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsInd01."NOMECONTA3"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 411.968477090000000000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          DataSet = frxDsInd01
          DataSetName = 'frxDsInd01'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInd01."DEBITO"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 514.015728500000000000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          DataField = 'CREDITO'
          DataSet = frxDsInd01
          DataSetName = 'frxDsInd01'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInd01."CREDITO"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 74.000000000000000000
        Top = 102.047310000000000000
        Width = 718.110700000000000000
        object Memo55: TfrxMemoView
          Left = 176.000000000000000000
          Top = 38.968460000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Vecto]')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 4.000000000000000000
          Top = 5.291280000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo46: TfrxMemoView
          Left = 176.000000000000000000
          Top = 22.968460000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Periodo]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 175.637910000000000000
          Top = 54.692950000000000000
          Width = 264.094310000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[DtDoc]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 176.000000000000000000
          Width = 552.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 445.984540000000000000
          Top = 54.803149610000000000
          Width = 267.873840000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[DI01]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 37.559060000000000000
        Top = 657.638220000000000000
        Width = 718.110700000000000000
        object Memo14: TfrxMemoView
          Left = 411.968770000000000000
          Top = 11.338590000000100000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."DEBITO">)]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 514.016021410000000000
          Top = 11.338590000000100000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."CREDITO">)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 616.063275270000000000
          Top = 11.338590000000000000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<SUM(<frxDsInd01."CREDITO">)> - <SUM(<frxDsInd01."DEBITO">)>]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 79.370130000000000000
          Top = 11.338590000000100000
          Width = 322.110390000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'RESULTADO DO PER'#205'ODO:')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 11.338590000000100000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 200.315090000000000000
        Width = 718.110700000000000000
        object Memo29: TfrxMemoView
          Left = 0.440940000000000000
          Top = 2.582560000000000000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 60.440940000000000000
          Top = 2.582560000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'Doc.')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 112.440940000000000000
          Top = 2.582560000000000000
          Width = 284.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 400.440940000000000000
          Top = 2.582560000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 453.543307086614000000
          Top = 2.582560000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 544.440940000000000000
          Top = 2.582560000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 631.181102362205100000
          Top = 2.582560000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.000000000000000000
        Top = 396.850650000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsInd01."NOMESUBGRUPO"'
        object Memo2: TfrxMemoView
          Left = 49.574830000000000000
          Top = 1.826529999999990000
          Width = 664.787570000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsInd01."NOMESUBGRUPO"]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.645330000000000000
        Top = 529.134200000000000000
        Width = 718.110700000000000000
        object Memo7: TfrxMemoView
          Left = 80.440940000000000000
          Top = 1.322509999999970000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 144.440940000000000000
          Top = 1.322509999999970000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsInd01."NOMEGRUPO"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 411.968477090000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."DEBITO">)]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 514.015728500000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."CREDITO">)]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 616.062982360000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[<SUM(<frxDsInd01."CREDITO">)> - <SUM(<frxDsInd01."DEBITO">)>]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        Height = 21.779530000000000000
        Top = 574.488560000000000000
        Width = 718.110700000000000000
        object Memo37: TfrxMemoView
          Left = 80.440940000000000000
          Top = 3.527209999999970000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 144.440940000000000000
          Top = 3.527209999999970000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsInd01."NOMECONJUNTO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 411.968477090000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."DEBITO">)]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 514.015728500000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."CREDITO">)]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 616.062982360000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[<SUM(<frxDsInd01."CREDITO">)> - <SUM(<frxDsInd01."DEBITO">)>]')
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 28.000000000000000000
        Top = 347.716760000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsInd01."NOMEGRUPO"'
        object Line2: TfrxLineView
          Left = 8.000000000000000000
          Top = 25.180890000000000000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo39: TfrxMemoView
          Left = 8.000000000000000000
          Top = 3.180889999999980000
          Width = 706.362400000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsInd01."NOMEGRUPO"]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 287.244280000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsInd01."NOMECONJUNTO"'
        object Line1: TfrxLineView
          Left = 8.000000000000000000
          Top = 30.094310000000000000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Style = fsDouble
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo40: TfrxMemoView
          Left = 8.000000000000000000
          Top = 0.094310000000007220
          Width = 706.362400000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsInd01."NOMECONJUNTO"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsInd01: TfrxDBDataset
    UserName = 'frxDsInd01'
    CloseDataSource = False
    DataSet = QrInd01
    BCDToCurrency = False
    Left = 580
    Top = 8
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMECLI'
      'FROM entidades'
      'WHERE Cliente1="V"'
      ''
      'OR Cliente2="V"'
      'OR Cliente3="V" '
      'OR Cliente4="V"'
      ''
      'ORDER BY NOMECLI'
      ' '
      '')
    Left = 644
    Top = 336
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 672
    Top = 336
  end
  object QrInd01: TMySQLQuery
    Database = Dmod.MyDB
    Left = 636
    Top = 8
  end
end
