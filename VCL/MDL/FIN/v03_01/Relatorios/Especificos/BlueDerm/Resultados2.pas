unit Resultados2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, UnGOTOy, DBCtrls, Db, (*DBTables,*) Buttons,
  UnInternalConsts, ExtCtrls, mySQLDbTables, frxClass, frxDBSet, Variants,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Grids, DBGrids, dmkGeral,
  dmkEditDateTimePicker, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmResultados2 = class(TForm)
    QrGrupos: TMySQLQuery;
    DsGrupos: TDataSource;
    QrConjuntos: TMySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    DsConjuntos: TDataSource;
    QrSubgrupos: TMySQLQuery;
    DsSubgrupos: TDataSource;
    QrContas: TMySQLQuery;
    DsContas: TDataSource;
    QrGruposCodigo: TIntegerField;
    QrGruposNome: TWideStringField;
    QrGruposConjunto: TIntegerField;
    QrGruposLk: TIntegerField;
    QrSubgruposCodigo: TIntegerField;
    QrSubgruposNome: TWideStringField;
    QrSubgruposGrupo: TIntegerField;
    QrSubgruposLk: TIntegerField;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrResultLA: TMySQLQuery;
    QrResultLANOMECONTA: TWideStringField;
    QrResultLANOMESUBGRUPO: TWideStringField;
    QrResultLANOMEGRUPO: TWideStringField;
    QrResultLANOMECONJUNTO: TWideStringField;
    QrResultLANOMECONTA3: TWideStringField;
    QrResultLANOMECONTA2: TWideStringField;
    QrResultCO: TMySQLQuery;
    QrResultCODEBITO: TFloatField;
    QrResultCOCREDITO: TFloatField;
    QrResultCONOMECONTA: TWideStringField;
    QrResultCONOMECONTA2: TWideStringField;
    QrResultCONOMESUBGRUPO: TWideStringField;
    QrResultCONOMEGRUPO: TWideStringField;
    QrResultCONOMECONJUNTO: TWideStringField;
    QrResultCONOMECONTA3: TWideStringField;
    QrSInicial: TMySQLQuery;
    QrSInicialInicial: TFloatField;
    QrMovimento: TMySQLQuery;
    QrMovimentoCredito: TFloatField;
    QrMovimentoDebito: TFloatField;
    QrResultLANOMECARTEIRA: TWideStringField;
    Panel2: TPanel;
    QrResultLAData: TDateField;
    QrResultLATipo: TSmallintField;
    QrResultLACarteira: TIntegerField;
    QrResultLAControle: TIntegerField;
    QrResultLASub: TSmallintField;
    QrResultLAAutorizacao: TIntegerField;
    QrResultLAGenero: TIntegerField;
    QrResultLADescricao: TWideStringField;
    QrResultLANotaFiscal: TIntegerField;
    QrResultLADebito: TFloatField;
    QrResultLACredito: TFloatField;
    QrResultLACompensado: TDateField;
    QrResultLADocumento: TFloatField;
    QrResultLASit: TIntegerField;
    QrResultLAVencimento: TDateField;
    QrResultLALk: TIntegerField;
    QrResultLAFatID: TIntegerField;
    QrResultLAFatParcela: TIntegerField;
    QrResultLAID_Pgto: TIntegerField;
    QrResultLAID_Sub: TSmallintField;
    QrResultLAFatura: TWideStringField;
    QrResultLABanco: TIntegerField;
    QrResultLALocal: TIntegerField;
    QrResultLACartao: TIntegerField;
    QrResultLALinha: TIntegerField;
    QrResultLAOperCount: TIntegerField;
    QrResultLALancto: TIntegerField;
    QrResultLAPago: TFloatField;
    QrResultLAMez: TIntegerField;
    QrResultLAFornecedor: TIntegerField;
    QrResultLACliente: TIntegerField;
    QrResultLAMoraDia: TFloatField;
    QrResultLAMulta: TFloatField;
    QrResultLAProtesto: TDateField;
    QrResultLADataCad: TDateField;
    QrResultLADataAlt: TDateField;
    QrResultLAUserCad: TSmallintField;
    QrResultLAUserAlt: TSmallintField;
    QrResultLADataDoc: TDateField;
    QrResultLACtrlIni: TIntegerField;
    QrResultLANivel: TIntegerField;
    QrResultLAVendedor: TIntegerField;
    QrResultLAAccount: TIntegerField;
    QrResultLASubgrupo: TIntegerField;
    QrResultLAGrupo: TIntegerField;
    QrResultLAConjunto: TIntegerField;
    QrResultCOData: TDateField;
    QrResultCODocumento: TFloatField;
    QrResultCONotaFiscal: TIntegerField;
    QrResultCOSubgrupo: TIntegerField;
    QrResultCOGrupo: TIntegerField;
    QrResultCOConjunto: TIntegerField;
    frxResultCO: TfrxReport;
    frxDsResultCO: TfrxDBDataset;
    frxResultLA: TfrxReport;
    frxDsResultLA: TfrxDBDataset;
    QrResultLAFatNum: TFloatField;
    frxInd01: TfrxReport;
    frxDsInd01: TfrxDBDataset;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMECLI: TWideStringField;
    Panel4: TPanel;
    Panel5: TPanel;
    CkDataIni: TCheckBox;
    TPDataIni: TdmkEditDateTimePicker;
    CkVctoIni: TCheckBox;
    TPVctoIni: TdmkEditDateTimePicker;
    CkDtDocIni: TCheckBox;
    TPDtDocIni: TdmkEditDateTimePicker;
    CkDataFim: TCheckBox;
    TPDataFim: TdmkEditDateTimePicker;
    CkVctoFim: TCheckBox;
    TPVctoFim: TdmkEditDateTimePicker;
    CkDtDocFim: TCheckBox;
    TPDtDocFim: TdmkEditDateTimePicker;
    Label3: TLabel;
    EdConjunto: TdmkEditCB;
    CBConjunto: TdmkDBLookupComboBox;
    CBGrupo: TdmkDBLookupComboBox;
    EdGrupo: TdmkEditCB;
    Label4: TLabel;
    Label5: TLabel;
    EdSubgrupo: TdmkEditCB;
    CBSubgrupo: TdmkDBLookupComboBox;
    CBConta: TdmkDBLookupComboBox;
    EdConta: TdmkEditCB;
    Label6: TLabel;
    RGExclusivos: TRadioGroup;
    CkNome2: TCheckBox;
    CkSaldo: TCheckBox;
    QrResultCOcjOrdemLista: TIntegerField;
    QrResultCOgrOrdemLista: TIntegerField;
    QrResultCOsgOrdemLista: TIntegerField;
    QrResultCOcoOrdemLista: TIntegerField;
    PCTipoRel: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    RGPeriodoBalInd: TRadioGroup;
    Label1: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    TabSheet4: TTabSheet;
    Panel10: TPanel;
    frxExtratLA: TfrxReport;
    DBGrid1: TDBGrid;
    DsResultLA: TDataSource;
    Panel11: TPanel;
    CkAgrupaCta: TCheckBox;
    CkZeraAcumCta: TCheckBox;
    QrInd01: TmySQLQuery;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtImprime: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrResultLACalcFields(DataSet: TDataSet);
    procedure QrResultCOCalcFields(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure frxResultCOGetValue(const VarName: String;
      var Value: Variant);
    procedure PCTipoRelChange(Sender: TObject);
    procedure EdSubgrupoChange(Sender: TObject);
    procedure EdConjuntoChange(Sender: TObject);
    procedure EdGrupoChange(Sender: TObject);
    procedure CkAgrupaCtaClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    //
    FInd01: String;
    FTabLctA: String;
  public
    { Public declarations }
  end;

var
  FmResultados2: TFmResultados2;
  RES_Anterior, RES_Final, RES_Credito, RES_Debito, RES_Periodo: Double;

implementation

uses UnMyObjects, Module, Principal, ModuleGeral, UCreate, DmkDAC_PF;

{$R *.DFM}

procedure TFmResultados2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmResultados2.CkAgrupaCtaClick(Sender: TObject);
begin
  CkZeraAcumCta.Enabled := CkAgrupaCta.Checked;
end;

procedure TFmResultados2.EdConjuntoChange(Sender: TObject);
begin
  QrGrupos.Close;
  QrGrupos.SQL.Clear;
  QrGrupos.SQL.Add('SELECT * FROM grupos');
  if CBConjunto.KeyValue <> NULL then
    QrGrupos.SQL.Add('WHERE Conjunto=:P0');
  QrGrupos.SQL.Add('ORDER BY Nome');
  if CBConjunto.KeyValue <> NULL then
    QrGrupos.Params[0].AsInteger := CBConjunto.KeyValue;
  UnDmkDAC_PF.AbreQuery(QrGrupos, Dmod.MyDB);
  CBGrupo.KeyValue := NULL;
end;

procedure TFmResultados2.EdGrupoChange(Sender: TObject);
begin
  QrSubgrupos.Close;
  QrSubgrupos.SQL.Clear;
  QrSubgrupos.SQL.Add('SELECT * FROM subgrupos');
  if CBGrupo.KeyValue <> NULL then
    QrSubgrupos.SQL.Add('WHERE Grupo=:P0');
  QrSubgrupos.SQL.Add('ORDER BY Nome');
  if CBGrupo.KeyValue <> NULL then
    QrSubgrupos.Params[0].AsInteger := CBGrupo.KeyValue;
  UnDmkDAC_PF.AbreQuery(QrSubgrupos, Dmod.MyDB);
  CBSubgrupo.KeyValue := NULL;
end;

procedure TFmResultados2.EdSubgrupoChange(Sender: TObject);
begin
  QrContas.Close;
  QrContas.SQL.Clear;
  QrContas.SQL.Add('SELECT * FROM contas');
  if CBSubGrupo.KeyValue <> NULL then
    QrContas.SQL.Add('WHERE Subgrupo=:P0');
  QrContas.SQL.Add('ORDER BY Nome');
  if CBSubGrupo.KeyValue <> NULL then
    QrContas.Params[0].AsInteger := CBSubGrupo.KeyValue;
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  CBConta.KeyValue := NULL;
end;

procedure TFmResultados2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmResultados2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PCTipoRel.ActivePageIndex := 1;
  UnDmkDAC_PF.AbreQuery(QrConjuntos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSubGrupos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGrupos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  TPDataIni.Date  := Geral.Data1(IncMonth(Date, -1));
  TPDataFim.Date  := Geral.Data1(Date)-1;
  TPVctoIni.Date  := Geral.Data1(IncMonth(Date, -1));
  TPVctoFim.Date  := Geral.Data1(Date)-1;
  TPDtDocIni.Date := Geral.Data1(IncMonth(Date, -1));
  TPDtDocFim.Date := Geral.Data1(Date)-1;
  //
  EdCliInt.ValueVariant := VAR_LIB_EMPRESA_SEL;
  CBCliInt.KeyValue     := VAR_LIB_EMPRESA_SEL;
end;

{
procedure TFmResultados2.BtImprimeClick(Sender: TObject);
  procedure AbrirMovimento(Data, Vcto: String);
  begin
    QrMovimento.Close;
    QrMovimento.SQL.Clear;
    QrMovimento.SQL.Add('SELECT SUM(la.Credito) Credito, SUM(la.Debito) Debito');
    QrMovimento.SQL.Add('FROM ' + FTabLctA + ' la, Contas co');
    QrMovimento.SQL.Add('WHERE la.Genero > 0');

    if CkDataFim.Checked then
      QrMovimento.SQL.Add('AND la.Data <= "' + Data + '"');
    if CkVctoFim.Checked then
      QrMovimento.SQL.Add('AND la.Vencimento <= "' + Vcto + '"');

    QrMovimento.SQL.Add('AND co.Codigo=la.Genero');
    case RGExclusivos.ItemIndex of
      0: QrMovimento.SQL.Add('AND co.Exclusivo=''F''');
      2: QrMovimento.SQL.Add('AND co.Exclusivo=''V''');
    end;
    //QrMovimento.Params[0].AsString := Fim;
    UnDmkDAC_PF.AbreQuery(QrMovimento. O p e n ;
  end;
var
  Conjunto, Grupo, Subgrupo, Conta: Integer;
  DataAnt, DataIni, DataFim,
  VctoAnt, VctoIni, VctoFim,
  DDocAnt, DDocIni, DDocFim,
  Campos, Linha: String;
  DataIDate, DataFDate: TDateTime;
  DataITrue, DataFTrue: Boolean;
begin
  //Parei Aqui !! Saldos da C/C

  //Acertar saldos finais rel novo

  DataAnt := FormatDateTime(VAR_FORMATDATE, TPDataIni.Date-1);
  DataIni := FormatDateTime(VAR_FORMATDATE, TPDataIni.Date);
  DataFim := FormatDateTime(VAR_FORMATDATE, TPDataFim.Date);

  VctoAnt := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date-1);
  VctoIni := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date);
  VctoFim := FormatDateTime(VAR_FORMATDATE, TPVctoFim.Date);

  DDocAnt := FormatDateTime(VAR_FORMATDATE, TPDtDocIni.Date-1);
  DDocIni := FormatDateTime(VAR_FORMATDATE, TPDtDocIni.Date);
  DDocFim := FormatDateTime(VAR_FORMATDATE, TPDtDocFim.Date);
  ////////////
  QrSInicial.Close;
  UnDmkDAC_PF.AbreQuery(QrSInicial. O p e n ;

  AbrirMovimento(DataFim, VctoFim);

  RES_Credito := QrMovimentoCredito.Value;
  RES_Debito := QrMovimentoDebito.Value;
  if RGExclusivos.ItemIndex <> 2 then
    RES_Final := QrSInicialInicial.Value + RES_Credito - RES_Debito
  else RES_Final := RES_Credito - RES_Debito;
  RES_Periodo := RES_Credito - RES_Debito;
  ///////////
  AbrirMovimento(DataAnt, VctoAnt);
  //
  if RGExclusivos.ItemIndex <> 2 then
  RES_Anterior := QrSInicialInicial.Value else RES_Anterior := 0;
  RES_Anterior := RES_Anterior + QrMovimentoCredito.Value - QrMovimentoDebito.Value;
  RES_Credito := RES_Credito - QrMovimentoCredito.Value;
  RES_Debito := RES_Debito - QrMovimentoDebito.Value;
  ///////////
  if CBConjunto.KeyValue = NULL then Conjunto := -1000
  else Conjunto := CBConjunto.KeyValue;
  //
  if CBGrupo.KeyValue = NULL then Grupo := -1000
  else Grupo := CBGrupo.KeyValue;
  //
  if CBSubgrupo.KeyValue = NULL then Subgrupo := -1000
  else Subgrupo := CBSubgrupo.KeyValue;
  //
  if CBConta.KeyValue = NULL then Conta := -1000
  else Conta := CBConta.KeyValue;
  //
  // Resultado por CONTAS
  // Balancete industrial modelo 01
  if PCTipoRel.ActivePageIndex in ([0,3]) then
  begin
    QrResultCO.Close;
    QrResultCO.SQL.Clear;
    QrResultCO.SQL.Add('SELECT SUM(la.Debito) DEBITO, SUM(la.Credito) CREDITO,');
    QrResultCO.SQL.Add('la.Data, la.Documento, la.NotaFiscal,');
    QrResultCO.SQL.Add('co.Subgrupo, sg.Grupo, gr.Conjunto,');
    QrResultCO.SQL.Add('co.Nome NOMECONTA, co.Nome2 NOMECONTA2,');
    QrResultCO.SQL.Add('sg.Nome NOMESUBGRUPO, gr.Nome NOMEGRUPO,');
    QrResultCO.SQL.Add('cj.Nome NOMECONJUNTO, cj.OrdemLista cjOrdemLista,');
    QrResultCO.SQL.Add('gr.OrdemLista grOrdemLista, sg.OrdemLista sgOrdemLista,');
    QrResultCO.SQL.Add('co.OrdemLista coOrdemLista');
    QrResultCO.SQL.Add('FROM ' + FTabLctA + ' la, Contas co, Subgrupos sg,');
    QrResultCO.SQL.Add('Grupos gr, Conjuntos cj');
    QrResultCO.SQL.Add('WHERE la.Genero>0');
    QrResultCO.SQL.Add('AND la.Tipo <> 2');

    QrResultCO.SQL.Add(dmkPF.SQL_Periodo('AND Data ',
      TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked));
    QrResultCO.SQL.Add(dmkPF.SQL_Periodo('AND Vencimento ',
      TPVctoIni.Date, TPVctoFim.Date, CkVctoIni.Checked, CkVctoFim.Checked));
    QrResultCO.SQL.Add(dmkPF.SQL_Periodo('AND DataDoc ',
      TPDtDocIni.Date, TPDtDocFim.Date, CkDtDocIni.Checked, CkDtDocFim.Checked));

    QrResultCO.SQL.Add('AND co.Codigo=la.Genero');
    QrResultCO.SQL.Add('AND sg.Codigo=co.Subgrupo');
    QrResultCO.SQL.Add('AND gr.Codigo=sg.Grupo');
    QrResultCO.SQL.Add('AND cj.Codigo=gr.Conjunto');
    ////////////
    if Conjunto <> -1000 then
      QrResultCO.SQL.Add('AND cj.Codigo='+IntToStr(Conjunto));
    ////////////
    if Grupo <> -1000 then
      QrResultCO.SQL.Add('AND gr.Codigo='+IntToStr(Grupo));
    ////////////
    if Subgrupo <> -1000 then
      QrResultCO.SQL.Add('AND sg.Codigo='+IntToStr(Subgrupo));
    ////////////
    if Conta <> -1000 then
      QrResultCO.SQL.Add('AND co.Codigo='+IntToStr(Conta));
    ////////////
    case RGExclusivos.ItemIndex of
      0: QrResultCO.SQL.Add('AND co.Exclusivo=''F''');
      2: QrResultCO.SQL.Add('AND co.Exclusivo=''V''');
    end;
    ////////////
    if EdCliInt.ValueVariant <> 0 then
      QrResultCO.SQL.Add('AND la.CliInt=' + FormatFloat('0', EdCliInt.ValueVariant));
    ////////////
    // Balancete industrial modelo 01
    if PCTipoRel.ActivePageIndex = 3 then
      QrResultCO.SQL.Add('AND NOT (co.NotPrntBal & 1)');

    ///
    QrResultCO.SQL.Add('GROUP BY la.Genero');
    QrResultCO.SQL.Add('ORDER BY cj.OrdemLista, cj.Nome, gr.OrdemLista, ');
    QrResultCO.SQL.Add('gr.Nome, sg.OrdemLista, sg.Nome, co.OrdemLista, ');
    QrResultCO.SQL.Add('co.Nome, co.Nome2');
    UnDmkDAC_PF.AbreQuery(QrResultCO. O p e  n;
    //
    if GOTOy.Registros(QrResultCO) = 0 then
      ShowMessage('N�o foi encontrado nenhum registro!')
    else
      if PCTipoRel.ActivePageIndex = 0 then
        MyObjects.frxMostra(frxResultCO, 'Resultado por contas')
  end;

  // Resultado por LAN�AMENTOS
  // Extrato por Lan�amentos
  if PCTipoRel.ActivePageIndex in ([1,2]) then
  begin
    QrResultLA.Close;
    QrResultLA.SQL.Clear;
    QrResultLA.SQL.Add('SELECT la.*, co.Subgrupo, sg.Grupo, ca.Nome NOMECARTEIRA, ');
    QrResultLA.SQL.Add('gr.Conjunto, co.Nome NOMECONTA, co.Nome2 NOMECONTA2, ');
    QrResultLA.SQL.Add('sg.Nome NOMESUBGRUPO, gr.Nome NOMEGRUPO,');
    QrResultLA.SQL.Add('cj.Nome NOMECONJUNTO');
    QrResultLA.SQL.Add('FROM ' + FTabLctA + ' la, Contas co, Subgrupos sg,');
    QrResultLA.SQL.Add('Grupos gr, Conjuntos cj, Carteiras ca');
    QrResultLA.SQL.Add('WHERE la.Genero>0');
    QrResultLA.SQL.Add('AND la.Tipo <> 2');

    QrResultLA.SQL.Add(dmkPF.SQL_Periodo('AND Data ',
      TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked));
    QrResultLA.SQL.Add(dmkPF.SQL_Periodo('AND Vencimento ',
      TPVctoIni.Date, TPVctoFim.Date, CkVctoIni.Checked, CkVctoFim.Checked));
    QrResultLA.SQL.Add(dmkPF.SQL_Periodo('AND DataDoc ',
      TPDtDocIni.Date, TPDtDocFim.Date, CkDtDocIni.Checked, CkDtDocFim.Checked));

    QrResultLA.SQL.Add('AND ca.Codigo=la.Carteira');
    QrResultLA.SQL.Add('AND co.Codigo=la.Genero');
    QrResultLA.SQL.Add('AND sg.Codigo=co.Subgrupo');
    QrResultLA.SQL.Add('AND gr.Codigo=sg.Grupo');
    QrResultLA.SQL.Add('AND cj.Codigo=gr.Conjunto');
    ////////////
    if Conjunto <> -1000 then
      QrResultLA.SQL.Add('AND cj.Codigo='+IntToStr(Conjunto));
    ////////////
    if Grupo <> -1000 then
      QrResultLA.SQL.Add('AND gr.Codigo='+IntToStr(Grupo));
    ////////////
    if Subgrupo <> -1000 then
      QrResultLA.SQL.Add('AND sg.Codigo='+IntToStr(Subgrupo));
    ////////////
    if Conta <> -1000 then
      QrResultLA.SQL.Add('AND co.Codigo='+IntToStr(Conta));
    ////////////
    case RGExclusivos.ItemIndex of
      0: QrResultLA.SQL.Add('AND co.Exclusivo=''F''');
      2: QrResultLA.SQL.Add('AND co.Exclusivo=''V''');
    end;
    ////////////
    if EdCliInt.ValueVariant <> 0 then
      QrResultLA.SQL.Add('AND la.CliInt=' + FormatFloat('0', EdCliInt.ValueVariant));
    ////////////
    QrResultLA.SQL.Add('');
    case PCTipoRel.ActivePageIndex of
      1: QrResultLA.SQL.Add('ORDER BY cj.Nome, gr.Nome, sg.Nome, co.Nome, la.Data');
      2:
      begin
        if CkAgrupaCta.Checked then
          QrResultLA.SQL.Add('ORDER BY co.Nome, la.Data, la.Controle')
        else
          QrResultLA.SQL.Add('ORDER BY la.Data, la.Controle');
      end;
    end;
    UnDmkDAC_PF.AbreQuery(QrResultLA.O p e n ;
    if GOTOy.Registros(QrResultLA) = 0 then
      Application.MessageBox('N�o foi encontrado nenhum registro!', 'Erro',
        MB_OK+MB_ICONERROR)
    else begin
      case PCTipoRel.ActivePageIndex of
        1: MyObjects.frxMostra(frxResultLA, 'Resultado por lan�amentos');
        2: MyObjects.frxMostra(frxExtratLA, 'Extrato por lan�amentos');
        else Geral.MensagemBox('Tipo de relat�rio n�o implementado!',
        'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
  end;
  if PCTipoRel.ActivePageIndex = 3 then
  begin
    /// Parei aqui !!!
    QrInd01.Close;
    QrInd01.SQL.Clear;
    QrInd01.SQL.Add('DROP TABLE Ind01;');
    QrInd01.SQL.Add('CREATE TABLE Ind01 (');
    QrInd01.SQL.Add('  DEBITO       float        ,');
    QrInd01.SQL.Add('  CREDITO      float        ,');
    QrInd01.SQL.Add('  NOMECONTA    varchar(100) ,');
    QrInd01.SQL.Add('  NOMECONTA2   varchar(100) ,');
    QrInd01.SQL.Add('  NOMESUBGRUPO varchar(100) ,');
    QrInd01.SQL.Add('  NOMEGRUPO    varchar(100) ,');
    QrInd01.SQL.Add('  NOMECONJUNTO varchar(100) ,');
    QrInd01.SQL.Add('  NOMECONTA3   varchar(100) ,');
    QrInd01.SQL.Add('  Data         date         ,');
    QrInd01.SQL.Add('  Documento    float        ,');
    QrInd01.SQL.Add('  NotaFiscal   integer      ,');
    QrInd01.SQL.Add('  Subgrupo     integer      ,');
    QrInd01.SQL.Add('  Grupo        integer      ,');
    QrInd01.SQL.Add('  Conjunto     integer      ,');
    QrInd01.SQL.Add('  cjOrdemLista integer      ,');
    QrInd01.SQL.Add('  grOrdemLista integer      ,');
    QrInd01.SQL.Add('  sgOrdemLista integer      ,');
    QrInd01.SQL.Add('  coOrdemLista integer       ');
    QrInd01.SQL.Add(');');
    //
    Campos := 'INSERT INTO ind01 (DEBITO, CREDITO, NOMECONTA, NOMECONTA2, ' +
    'NOMESUBGRUPO, NOMEGRUPO, NOMECONJUNTO, NOMECONTA3, Data, Documento, ' +
    'NotaFiscal, Subgrupo, Grupo, Conjunto, cjOrdemLista, grOrdemLista, ' +
    'sgOrdemLista, coOrdemLista) VALUES (';
    //
    QrResultCO.First;
    while not QrResultCO.Eof do
    begin
      (*
      if QrResultCODEBITO.Value > QrResultCOCREDITO.Value then
      begin
        Credito := 0;
        Debito  := QrResultCODEBITO.Value - QrResultCOCREDITO.Value;
      end else begin
        Credito := QrResultCOCREDITO.Value - QrResultCODEBITO.Value;
        Debito  := 0;
      end;
      *)
      Linha := Campos +
      dmkPF.FFP(QrResultCODEBITO.Value, 2) +
      ', ' + dmkPF.FFP(QrResultCOCREDITO.Value, 2) +
      ', "' + QrResultCONOMECONTA.Value + '"' +
      ', "' + QrResultCONOMECONTA2.Value + '"' +
      ', "' + QrResultCONOMESUBGRUPO.Value + '"' +
      ', "' + QrResultCONOMEGRUPO.Value + '"' +
      ', "' + QrResultCONOMECONJUNTO.Value + '"' +
      ', "' + QrResultCONOMECONTA3.Value + '"' +
      ', "' + Geral.FDT(QrResultCOData.Value, 1) + '"' +
      ',  ' + FormatFloat('0', QrResultCODocumento.Value) +
      ',  ' + FormatFloat('0', QrResultCONotaFiscal.Value) +

      ',  ' + FormatFloat('0', QrResultCOSubgrupo.Value) +
      ',  ' + FormatFloat('0', QrResultCOGrupo.Value) +
      ',  ' + FormatFloat('0', QrResultCOConjunto.Value) +

      ',  ' + FormatFloat('0', QrResultCOcjOrdemLista.Value) +
      ',  ' + FormatFloat('0', QrResultCOgrOrdemLista.Value) +
      ',  ' + FormatFloat('0', QrResultCOsgOrdemLista.Value) +
      ',  ' + FormatFloat('0', QrResultCOcoOrdemLista.Value) +
      ');';
      QrInd01.SQL.Add(Linha);
      //
      QrResultCO.Next;
    end;
    //
    case RGPeriodoBalInd.ItemIndex of
      0:
      begin
        DataIDate := TPDataIni.Date;
        DataFDate := TPDataFim.Date;
        DataITrue := CkDataIni.Checked;
        DataFTrue := CkDataFim.Checked;
      end;
      1:
      begin
        DataIDate := TPVctoIni.Date;
        DataFDate := TPVctoFim.Date;
        DataITrue := CkVctoIni.Checked;
        DataFTrue := CkVctoFim.Checked;
      end;
      2:
      begin
        DataIDate := TPDtDocIni.Date;
        DataFDate := TPDtDocFim.Date;
        DataITrue := CkDtDocIni.Checked;
        DataFTrue := CkDtDocFim.Checked;
      end;
      else
      begin
        DataIDate := 0;
        DataFDate := 0;
        DataITrue := False;
        DataFTrue := False;
      end;
    end;
    Dmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01, DataIDate, DataFDate,
    DataITrue, DataFTrue, Geral.IMV(EdCliInt.Text), Campos, CkNome2.Checked);
    //
    QrInd01.SQL.Add('SELECT * FROM ind01 ');
    QrInd01.SQL.Add('ORDER BY cjOrdemLista, NOMECONJUNTO, grOrdemLista, ');
    QrInd01.SQL.Add('NOMEGRUPO, sgOrdemLista, NOMESUBGRUPO, coOrdemLista, ');
    QrInd01.SQL.Add('NOMECONTA, NOMECONTA2;');
    UnDmkDAC_PF.AbreQuery(QrInd01. O p e  n;
    //
    MyObjects.frxMostra(frxInd01, 'Balancete Mensal');
  end;
  QrSInicial.Close;
  QrMovimento.Close;
end;
}

procedure TFmResultados2.BtImprimeClick(Sender: TObject);
  procedure AbrirMovimento(Data, Vcto: String);
  begin
    QrMovimento.Close;
    QrMovimento.SQL.Clear;
    QrMovimento.SQL.Add('SELECT SUM(la.Credito) Credito, SUM(la.Debito) Debito');
    QrMovimento.SQL.Add('FROM ' + FTabLctA + ' la, Contas co');
    QrMovimento.SQL.Add('WHERE la.Genero > 0');

    if CkDataFim.Checked then
      QrMovimento.SQL.Add('AND la.Data <= "' + Data + '"');
    if CkVctoFim.Checked then
      QrMovimento.SQL.Add('AND la.Vencimento <= "' + Vcto + '"');

    QrMovimento.SQL.Add('AND co.Codigo=la.Genero');
    case RGExclusivos.ItemIndex of
      0: QrMovimento.SQL.Add('AND co.Exclusivo=''F''');
      2: QrMovimento.SQL.Add('AND co.Exclusivo=''V''');
    end;
    //QrMovimento.Params[0].AsString := Fim;
    UnDmkDAC_PF.AbreQuery(QrMovimento, Dmod.MyDB);
  end;
var
  Conjunto, Grupo, Subgrupo, Conta: Integer;
  DataAnt, DataIni, DataFim,
  VctoAnt, VctoIni, VctoFim,
  DDocAnt, DDocIni, DDocFim,
  Campos, Linha: String;
  DataIDate, DataFDate: TDateTime;
  DataITrue, DataFTrue: Boolean;
  //
  Entidade, Filial: Integer;
begin
  //Parei Aqui !! Saldos da C/C

  //Acertar saldos finais rel novo

  Entidade := EdCliInt.ValueVariant;
  if MyObjects.FIC(Entidade = 0, EdCliInt, 'Informe o cliente interno') then
    Exit;
  Filial := DModG.ObtemFilialDeEntidade(Entidade);
  if Filial <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial)
  else
    FTabLctA := sTabLctErr;
  //
  DataAnt := FormatDateTime(VAR_FORMATDATE, TPDataIni.Date-1);
  DataIni := FormatDateTime(VAR_FORMATDATE, TPDataIni.Date);
  DataFim := FormatDateTime(VAR_FORMATDATE, TPDataFim.Date);

  VctoAnt := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date-1);
  VctoIni := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date);
  VctoFim := FormatDateTime(VAR_FORMATDATE, TPVctoFim.Date);

  DDocAnt := FormatDateTime(VAR_FORMATDATE, TPDtDocIni.Date-1);
  DDocIni := FormatDateTime(VAR_FORMATDATE, TPDtDocIni.Date);
  DDocFim := FormatDateTime(VAR_FORMATDATE, TPDtDocFim.Date);
  ////////////
  QrSInicial.Close;
  UnDmkDAC_PF.AbreQuery(QrSInicial, Dmod.MyDB);

  AbrirMovimento(DataFim, VctoFim);

  RES_Credito := QrMovimentoCredito.Value;
  RES_Debito := QrMovimentoDebito.Value;
  if RGExclusivos.ItemIndex <> 2 then
    RES_Final := QrSInicialInicial.Value + RES_Credito - RES_Debito
  else RES_Final := RES_Credito - RES_Debito;
  RES_Periodo := RES_Credito - RES_Debito;
  ///////////
  AbrirMovimento(DataAnt, VctoAnt);
  //
  if RGExclusivos.ItemIndex <> 2 then
  RES_Anterior := QrSInicialInicial.Value else RES_Anterior := 0;
  RES_Anterior := RES_Anterior + QrMovimentoCredito.Value - QrMovimentoDebito.Value;
  RES_Credito := RES_Credito - QrMovimentoCredito.Value;
  RES_Debito := RES_Debito - QrMovimentoDebito.Value;
  ///////////
  if CBConjunto.KeyValue = NULL then Conjunto := -1000
  else Conjunto := CBConjunto.KeyValue;
  //
  if CBGrupo.KeyValue = NULL then Grupo := -1000
  else Grupo := CBGrupo.KeyValue;
  //
  if CBSubgrupo.KeyValue = NULL then Subgrupo := -1000
  else Subgrupo := CBSubgrupo.KeyValue;
  //
  if CBConta.KeyValue = NULL then Conta := -1000
  else Conta := CBConta.KeyValue;
  //
  // Resultado por CONTAS
  // Balancete industrial modelo 01
  if PCTipoRel.ActivePageIndex in ([0,3]) then
  begin
    QrResultCO.Close;
    QrResultCO.SQL.Clear;
    QrResultCO.SQL.Add('SELECT SUM(la.Debito) DEBITO, SUM(la.Credito) CREDITO,');
    QrResultCO.SQL.Add('la.Data, la.Documento, la.NotaFiscal,');
    QrResultCO.SQL.Add('co.Subgrupo, sg.Grupo, gr.Conjunto,');
    QrResultCO.SQL.Add('co.Nome NOMECONTA, co.Nome2 NOMECONTA2,');
    QrResultCO.SQL.Add('sg.Nome NOMESUBGRUPO, gr.Nome NOMEGRUPO,');
    QrResultCO.SQL.Add('cj.Nome NOMECONJUNTO, cj.OrdemLista cjOrdemLista,');
    QrResultCO.SQL.Add('gr.OrdemLista grOrdemLista, sg.OrdemLista sgOrdemLista,');
    QrResultCO.SQL.Add('co.OrdemLista coOrdemLista');
    QrResultCO.SQL.Add('FROM ' + FTabLctA + ' la, Contas co, Subgrupos sg,');
    QrResultCO.SQL.Add('Grupos gr, Conjuntos cj');
    QrResultCO.SQL.Add('WHERE la.Genero>0');
    QrResultCO.SQL.Add('AND la.Tipo <> 2');

    QrResultCO.SQL.Add(dmkPF.SQL_Periodo('AND Data ',
      TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked));
    QrResultCO.SQL.Add(dmkPF.SQL_Periodo('AND Vencimento ',
      TPVctoIni.Date, TPVctoFim.Date, CkVctoIni.Checked, CkVctoFim.Checked));
    QrResultCO.SQL.Add(dmkPF.SQL_Periodo('AND DataDoc ',
      TPDtDocIni.Date, TPDtDocFim.Date, CkDtDocIni.Checked, CkDtDocFim.Checked));

    QrResultCO.SQL.Add('AND co.Codigo=la.Genero');
    QrResultCO.SQL.Add('AND sg.Codigo=co.Subgrupo');
    QrResultCO.SQL.Add('AND gr.Codigo=sg.Grupo');
    QrResultCO.SQL.Add('AND cj.Codigo=gr.Conjunto');
    ////////////
    if Conjunto <> -1000 then
      QrResultCO.SQL.Add('AND cj.Codigo='+IntToStr(Conjunto));
    ////////////
    if Grupo <> -1000 then
      QrResultCO.SQL.Add('AND gr.Codigo='+IntToStr(Grupo));
    ////////////
    if Subgrupo <> -1000 then
      QrResultCO.SQL.Add('AND sg.Codigo='+IntToStr(Subgrupo));
    ////////////
    if Conta <> -1000 then
      QrResultCO.SQL.Add('AND co.Codigo='+IntToStr(Conta));
    ////////////
    case RGExclusivos.ItemIndex of
      0: QrResultCO.SQL.Add('AND co.Exclusivo=''F''');
      2: QrResultCO.SQL.Add('AND co.Exclusivo=''V''');
    end;
    ////////////
    if EdCliInt.ValueVariant <> 0 then
      QrResultCO.SQL.Add('AND la.CliInt=' + FormatFloat('0', EdCliInt.ValueVariant));
    ////////////
    // Balancete industrial modelo 01
    if PCTipoRel.ActivePageIndex = 3 then
      QrResultCO.SQL.Add('AND NOT (co.NotPrntBal & 1)');

    ///
    QrResultCO.SQL.Add('GROUP BY la.Genero');
    QrResultCO.SQL.Add('ORDER BY cj.OrdemLista, cj.Nome, gr.OrdemLista, ');
    QrResultCO.SQL.Add('gr.Nome, sg.OrdemLista, sg.Nome, co.OrdemLista, ');
    QrResultCO.SQL.Add('co.Nome, co.Nome2');
    UnDmkDAC_PF.AbreQuery(QrResultCO, Dmod.MyDB);
    //
    if GOTOy.Registros(QrResultCO) = 0 then
      ShowMessage('N�o foi encontrado nenhum registro!')
    else
      if PCTipoRel.ActivePageIndex = 0 then
        MyObjects.frxMostra(frxResultCO, 'Resultado por contas')
  end;

  // Resultado por LAN�AMENTOS
  // Extrato por Lan�amentos
  if PCTipoRel.ActivePageIndex in ([1,2]) then
  begin
    QrResultLA.Close;
    QrResultLA.SQL.Clear;
    QrResultLA.SQL.Add('SELECT la.*, co.Subgrupo, sg.Grupo, ca.Nome NOMECARTEIRA, ');
    QrResultLA.SQL.Add('gr.Conjunto, co.Nome NOMECONTA, co.Nome2 NOMECONTA2, ');
    QrResultLA.SQL.Add('sg.Nome NOMESUBGRUPO, gr.Nome NOMEGRUPO,');
    QrResultLA.SQL.Add('cj.Nome NOMECONJUNTO');
    QrResultLA.SQL.Add('FROM ' + FTabLctA + ' la, Contas co, Subgrupos sg,');
    QrResultLA.SQL.Add('Grupos gr, Conjuntos cj, Carteiras ca');
    QrResultLA.SQL.Add('WHERE la.Genero>0');
    QrResultLA.SQL.Add('AND la.Tipo <> 2');

    QrResultLA.SQL.Add(dmkPF.SQL_Periodo('AND Data ',
      TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked));
    QrResultLA.SQL.Add(dmkPF.SQL_Periodo('AND Vencimento ',
      TPVctoIni.Date, TPVctoFim.Date, CkVctoIni.Checked, CkVctoFim.Checked));
    QrResultLA.SQL.Add(dmkPF.SQL_Periodo('AND DataDoc ',
      TPDtDocIni.Date, TPDtDocFim.Date, CkDtDocIni.Checked, CkDtDocFim.Checked));

    QrResultLA.SQL.Add('AND ca.Codigo=la.Carteira');
    QrResultLA.SQL.Add('AND co.Codigo=la.Genero');
    QrResultLA.SQL.Add('AND sg.Codigo=co.Subgrupo');
    QrResultLA.SQL.Add('AND gr.Codigo=sg.Grupo');
    QrResultLA.SQL.Add('AND cj.Codigo=gr.Conjunto');
    ////////////
    if Conjunto <> -1000 then
      QrResultLA.SQL.Add('AND cj.Codigo='+IntToStr(Conjunto));
    ////////////
    if Grupo <> -1000 then
      QrResultLA.SQL.Add('AND gr.Codigo='+IntToStr(Grupo));
    ////////////
    if Subgrupo <> -1000 then
      QrResultLA.SQL.Add('AND sg.Codigo='+IntToStr(Subgrupo));
    ////////////
    if Conta <> -1000 then
      QrResultLA.SQL.Add('AND co.Codigo='+IntToStr(Conta));
    ////////////
    case RGExclusivos.ItemIndex of
      0: QrResultLA.SQL.Add('AND co.Exclusivo=''F''');
      2: QrResultLA.SQL.Add('AND co.Exclusivo=''V''');
    end;
    ////////////
    if EdCliInt.ValueVariant <> 0 then
      QrResultLA.SQL.Add('AND la.CliInt=' + FormatFloat('0', EdCliInt.ValueVariant));
    ////////////
    QrResultLA.SQL.Add('');
    case PCTipoRel.ActivePageIndex of
      1: QrResultLA.SQL.Add('ORDER BY cj.Nome, gr.Nome, sg.Nome, co.Nome, la.Data');
      2:
      begin
        if CkAgrupaCta.Checked then
          QrResultLA.SQL.Add('ORDER BY co.Nome, la.Data, la.Controle')
        else
          QrResultLA.SQL.Add('ORDER BY la.Data, la.Controle');
      end;
    end;
    UnDmkDAC_PF.AbreQuery(QrResultLA, Dmod.MyDB);
    if GOTOy.Registros(QrResultLA) = 0 then
      Application.MessageBox('N�o foi encontrado nenhum registro!', 'Erro',
        MB_OK+MB_ICONERROR)
    else begin
      case PCTipoRel.ActivePageIndex of
        1: MyObjects.frxMostra(frxResultLA, 'Resultado por lan�amentos');
        2: MyObjects.frxMostra(frxExtratLA, 'Extrato por lan�amentos');
        else Geral.MensagemBox('Tipo de relat�rio n�o implementado!',
        'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
  end;
  if PCTipoRel.ActivePageIndex = 3 then
  begin
    FInd01 := UCriar.RecriaTempTable('Ind01', DmodG.QrUpdPID1, False);
    QrInd01.SQL.Clear;
    /// Parei aqui !!!
    QrInd01.Close;
    QrInd01.SQL.Clear;
    QrInd01.SQL.Add('DROP TABLE Ind01;');
    QrInd01.SQL.Add('CREATE TABLE Ind01 (');
    QrInd01.SQL.Add('  DEBITO       float        ,');
    QrInd01.SQL.Add('  CREDITO      float        ,');
    QrInd01.SQL.Add('  NOMECONTA    varchar(100) ,');
    QrInd01.SQL.Add('  NOMECONTA2   varchar(100) ,');
    QrInd01.SQL.Add('  NOMESUBGRUPO varchar(100) ,');
    QrInd01.SQL.Add('  NOMEGRUPO    varchar(100) ,');
    QrInd01.SQL.Add('  NOMECONJUNTO varchar(100) ,');
    QrInd01.SQL.Add('  NOMECONTA3   varchar(100) ,');
    QrInd01.SQL.Add('  Data         date         ,');
    QrInd01.SQL.Add('  Documento    float        ,');
    QrInd01.SQL.Add('  NotaFiscal   integer      ,');
    QrInd01.SQL.Add('  Subgrupo     integer      ,');
    QrInd01.SQL.Add('  Grupo        integer      ,');
    QrInd01.SQL.Add('  Conjunto     integer      ,');
    QrInd01.SQL.Add('  cjOrdemLista integer      ,');
    QrInd01.SQL.Add('  grOrdemLista integer      ,');
    QrInd01.SQL.Add('  sgOrdemLista integer      ,');
    QrInd01.SQL.Add('  coOrdemLista integer       ');
    QrInd01.SQL.Add(');');
    //
    Campos := 'INSERT INTO ind01 (DEBITO, CREDITO, NOMECONTA, NOMECONTA2, ' +
    'NOMESUBGRUPO, NOMEGRUPO, NOMECONJUNTO, NOMECONTA3, Data, Documento, ' +
    'NotaFiscal, Subgrupo, Grupo, Conjunto, cjOrdemLista, grOrdemLista, ' +
    'sgOrdemLista, coOrdemLista) VALUES (';
    //
    QrResultCO.First;
    while not QrResultCO.Eof do
    begin
      (*
      if QrResultCODEBITO.Value > QrResultCOCREDITO.Value then
      begin
        Credito := 0;
        Debito  := QrResultCODEBITO.Value - QrResultCOCREDITO.Value;
      end else begin
        Credito := QrResultCOCREDITO.Value - QrResultCODEBITO.Value;
        Debito  := 0;
      end;
      *)
      Linha := Campos +
      dmkPF.FFP(QrResultCODEBITO.Value, 2) +
      ', ' + dmkPF.FFP(QrResultCOCREDITO.Value, 2) +
      ', "' + QrResultCONOMECONTA.Value + '"' +
      ', "' + QrResultCONOMECONTA2.Value + '"' +
      ', "' + QrResultCONOMESUBGRUPO.Value + '"' +
      ', "' + QrResultCONOMEGRUPO.Value + '"' +
      ', "' + QrResultCONOMECONJUNTO.Value + '"' +
      ', "' + QrResultCONOMECONTA3.Value + '"' +
      ', "' + Geral.FDT(QrResultCOData.Value, 1) + '"' +
      ',  ' + FormatFloat('0', QrResultCODocumento.Value) +
      ',  ' + FormatFloat('0', QrResultCONotaFiscal.Value) +

      ',  ' + FormatFloat('0', QrResultCOSubgrupo.Value) +
      ',  ' + FormatFloat('0', QrResultCOGrupo.Value) +
      ',  ' + FormatFloat('0', QrResultCOConjunto.Value) +

      ',  ' + FormatFloat('0', QrResultCOcjOrdemLista.Value) +
      ',  ' + FormatFloat('0', QrResultCOgrOrdemLista.Value) +
      ',  ' + FormatFloat('0', QrResultCOsgOrdemLista.Value) +
      ',  ' + FormatFloat('0', QrResultCOcoOrdemLista.Value) +
      ');';
      QrInd01.SQL.Add(Linha);
      //
      QrResultCO.Next;
    end;
    //
    case RGPeriodoBalInd.ItemIndex of
      0:
      begin
        DataIDate := TPDataIni.Date;
        DataFDate := TPDataFim.Date;
        DataITrue := CkDataIni.Checked;
        DataFTrue := CkDataFim.Checked;
      end;
      1:
      begin
        DataIDate := TPVctoIni.Date;
        DataFDate := TPVctoFim.Date;
        DataITrue := CkVctoIni.Checked;
        DataFTrue := CkVctoFim.Checked;
      end;
      2:
      begin
        DataIDate := TPDtDocIni.Date;
        DataFDate := TPDtDocFim.Date;
        DataITrue := CkDtDocIni.Checked;
        DataFTrue := CkDtDocFim.Checked;
      end;
      else
      begin
        DataIDate := 0;
        DataFDate := 0;
        DataITrue := False;
        DataFTrue := False;
      end;
    end;
    Dmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01, DataIDate, DataFDate,
    DataITrue, DataFTrue, Geral.IMV(EdCliInt.Text), Campos, CkNome2.Checked);
    //
    QrInd01.Close;
    QrInd01.DataBase := DModG.MyPID_DB;
    //
    QrInd01.ExecSQL;
    QrInd01.SQL.Clear;
    QrInd01.SQL.Add('SELECT * FROM ind01 ');
    QrInd01.SQL.Add('ORDER BY cjOrdemLista, NOMECONJUNTO, grOrdemLista, ');
    QrInd01.SQL.Add('NOMEGRUPO, sgOrdemLista, NOMESUBGRUPO, coOrdemLista, ');
    QrInd01.SQL.Add('NOMECONTA, NOMECONTA2;');
    UnDmkDAC_PF.AbreQuery(QrInd01, DModG.MyPID_DB);
    //
    MyObjects.frxMostra(frxInd01, 'Balancete Mensal');
  end;
  QrSInicial.Close;
  QrMovimento.Close;
end;

procedure TFmResultados2.QrResultLACalcFields(DataSet: TDataSet);
begin
  if CkNome2.Checked then QrResultLANOMECONTA3.Value := QrResultLANOMECONTA2.Value
  else QrResultLANOMECONTA3.Value := QrResultLANOMECONTA.Value;
end;

procedure TFmResultados2.QrResultCOCalcFields(DataSet: TDataSet);
begin
  if CkNome2.Checked then QrResultCONOMECONTA3.Value := QrResultCONOMECONTA2.Value
  else QrResultCONOMECONTA3.Value := QrResultCONOMECONTA.Value;
end;

procedure TFmResultados2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmResultados2.frxResultCOGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'Periodo' then
      Value := dmkPF.PeriodoImp2(TPDataIni.Date, TPdataFim.Date,
    CkDataIni.Checked, CkDataFim.Checked, 'Quita��o: ', '', ' ')
  else
  if VarName = 'Vecto' then
      Value := dmkPF.PeriodoImp2(TPVctoIni.Date, TPVctoFim.Date,
    CkVctoIni.Checked, CkVctoFim.Checked, 'Vencimento: ', '', ' ')
  else
  if VarName = 'DtDoc' then
      Value := dmkPF.PeriodoImp2(TPDtDocIni.Date, TPDtDocFim.Date,
    CkDtDocIni.Checked, CkDtDocFim.Checked, 'Data do documento: ', '', ' ')
  else
  if VarName = 'DI01' then
  begin
    case RGPeriodoBalInd.ItemIndex of
      0: Value := dmkPF.PeriodoImp2(TPDtDocIni.Date, TPDtDocFim.Date,
    CkDtDocIni.Checked, CkDtDocFim.Checked, 'Consumo: ', '', ' ');
      1: Value := dmkPF.PeriodoImp2(TPVctoIni.Date, TPVctoFim.Date,
    CkVctoIni.Checked, CkVctoFim.Checked, 'Consumo: ', '', ' ');
      2: Value := dmkPF.PeriodoImp2(TPDtDocIni.Date, TPDtDocFim.Date,
    CkDtDocIni.Checked, CkDtDocFim.Checked, 'Consumo: ', '', ' ');
      else Value := 'Per�odo do consumo de insumos qu�micos = ??/??/????';
    end;
  end


  // no relat�rio de contas est� errado retirei no dia 2008-07-23
  else if VarName = 'Anterior' then Value := RES_Anterior
  else if VarName = 'Final' then Value := RES_Final
  else if VarName = 'SCredito' then Value := RES_Credito
  else if VarName = 'SDebito' then Value := RES_Debito
  else if VarName = 'SPeriodo' then Value := RES_Periodo

  // 2009-10-14
  else if VarName = 'VARF_SALDOFINAL_VISIBLE' then Value := CkSaldo.Checked
  else if VarName = 'VARF_NOME_EMPRESA' then Value := CBCliInt.Text
  else if VarName = 'VARF_NOME_NIVEL' then
  begin
    if EdConta.ValueVariant <> 0 then
      Value := 'CONTA: ' + CBConta.Text
    else
    if EdSubGrupo.ValueVariant <> 0 then
      Value := 'SUB-GRUPO: ' + CBSubGrupo.Text
    else
    if EdGrupo.ValueVariant <> 0 then
      Value := 'GRUPO: ' + CBGrupo.Text
    else
    if EdConjunto.ValueVariant <> 0 then
      Value := 'CONJUNTO: ' + CBConjunto.Text
    else Value := 'TODO PLANO DE CONTAS';
  end
  else if VarName = 'VARF_PERIODOS' then
  begin
    Value :=
    dmkPF.PeriodoImp2(TPDataIni.Date, TPdataFim.Date,
    CkDataIni.Checked, CkDataFim.Checked, 'Emiss�o: ', '', ' ')
    +
    dmkPF.PeriodoImp2(TPVctoIni.Date, TPVctoFim.Date,
    CkVctoIni.Checked, CkVctoFim.Checked, 'Vencimento: ', '', ' ')
    +
    dmkPF.PeriodoImp2(TPDtDocIni.Date, TPDtDocFim.Date,
    CkDtDocIni.Checked, CkDtDocFim.Checked, 'Data do documento: ', '', ' ');
    //
    if Trim(Value) = '' then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT MIN(Data) Minimo,');
      Dmod.QrAux.SQL.Add('MAX(Data) Maximo');
      Dmod.QrAux.SQL.Add('FROM ' + FTabLctA + '');
      Dmod.QrAux.SQL.Add('WHERE Data <> 0');
      UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
      Value := dmkPF.PeriodoImp2(
        Dmod.QrAux.FieldByName('Minimo').AsDateTime,
        Dmod.QrAux.FieldByName('Maximo').AsDateTime,
        True, True, 'Quita��o: ', '', ' ');
    end;
  end
  else if VarName = 'VARF_AGRUPA_CTA' then
    Value := CkAgrupaCta.Checked
  else if VarName = 'VARF_ZERA_ACUM_CTA' then
  Value := CkZeraAcumCta.Enabled and CkZeraAcumCta.Checked;
end;

procedure TFmResultados2.PCTipoRelChange(Sender: TObject);
begin
  if PCTipoRel.ActivePageIndex = 3 then
  begin
    CkDataIni.Checked := False;
    CkDataFim.Checked := False;
    //
    CkDtDocIni.Checked := True;
    CkDtDocFim.Checked := True;
    //
  end;
end;

end.

