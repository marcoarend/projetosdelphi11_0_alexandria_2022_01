unit Extratos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnInternalConsts, ExtCtrls, StdCtrls, Buttons, ComCtrls, Db,
  (*DBTables,*) DBCtrls, UnGOTOy, Variants, UnDmkProcFunc, mySQLDbTables, DockForm,
  frxClass, frxDBSet, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkEditDateTimePicker, dmkPermissoes, dmkImage, dmkCheckGroup, UnDmkEnums;

type
  TTipoSaidaRelatorio = (tsrImpressao, tsrExportRTF);
  TFmExtratos = class(TForm)
    QrExtrato: TmySQLQuery;
    QrAnterior: TmySQLQuery;
    QrTerceiros: TmySQLQuery;
    DsTerceiros: TDataSource;
    QrTerceirosCodigo: TIntegerField;
    QrTerceirosNOMECONTATO: TWideStringField;
    QrPagRecIts: TmySQLQuery;
    StringField5: TWideStringField;
    QrPagRecItsNOMEVENCIDO: TWideStringField;
    QrPagRecItsVALOR: TFloatField;
    QrPagRecItsPAGO_REAL: TFloatField;
    QrPagRecItsATRAZODD: TFloatField;
    QrPagRecItsATUALIZADO: TFloatField;
    QrPagRecItsMULTA_REAL: TFloatField;
    QrPagRecItsVENCER: TDateField;
    QrPagRecItsVENCIDO: TDateField;
    QrPagRecX: TmySQLQuery;
    StringField7: TWideStringField;
    StringField8: TWideStringField;
    QrPagRecXNOMEVENCIDO: TWideStringField;
    QrPagRecXVALOR: TFloatField;
    QrPagRecXPAGO_REAL: TFloatField;
    QrPagRecXATRAZODD: TFloatField;
    QrPagRecXATUALIZADO: TFloatField;
    QrPagRecXMULTA_REAL: TFloatField;
    QrPagRecXVENCER: TDateField;
    QrPagRecXVENCIDO: TDateField;
    QrPagRec1: TmySQLQuery;
    QrPagRec1PAGO_ROLADO: TWideStringField;
    QrPagRecXPENDENTE: TFloatField;
    QrPagRec1DEVIDO: TFloatField;
    QrPagRec1PEND_TOTAL: TFloatField;
    QrPagRec: TmySQLQuery;
    QrPagRecXData: TDateField;
    QrPagRecXTipo: TSmallintField;
    QrPagRecXCarteira: TIntegerField;
    QrPagRecXSub: TSmallintField;
    QrPagRecXAutorizacao: TIntegerField;
    QrPagRecXGenero: TIntegerField;
    QrPagRecXDescricao: TWideStringField;
    QrPagRecXNotaFiscal: TIntegerField;
    QrPagRecXDebito: TFloatField;
    QrPagRecXCredito: TFloatField;
    QrPagRecXCompensado: TDateField;
    QrPagRecXDocumento: TFloatField;
    QrPagRecXSit: TIntegerField;
    QrPagRecXVencimento: TDateField;
    QrPagRecXLk: TIntegerField;
    QrPagRecXFatID: TIntegerField;
    QrPagRecXFatParcela: TIntegerField;
    QrPagRecXID_Sub: TSmallintField;
    QrPagRecXFatura: TWideStringField;
    QrPagRecXBanco: TIntegerField;
    QrPagRecXLocal: TIntegerField;
    QrPagRecXCartao: TIntegerField;
    QrPagRecXLinha: TIntegerField;
    QrPagRecXOperCount: TIntegerField;
    QrPagRecXLancto: TIntegerField;
    QrPagRecXPago: TFloatField;
    QrPagRecXFornecedor: TIntegerField;
    QrPagRecXCliente: TIntegerField;
    QrPagRecXMoraDia: TFloatField;
    QrPagRecXMulta: TFloatField;
    QrPagRecXProtesto: TDateField;
    QrPagRecXDataCad: TDateField;
    QrPagRecXDataAlt: TDateField;
    QrPagRecXUserCad: TSmallintField;
    QrPagRecXUserAlt: TSmallintField;
    QrPagRecXDataDoc: TDateField;
    QrPagRecXNivel: TIntegerField;
    QrPagRecXVendedor: TIntegerField;
    QrPagRecXAccount: TIntegerField;
    QrPagRecXNOMECONTA: TWideStringField;
    QrPagRecXNOMECARTEIRA: TWideStringField;
    QrPagRecXSALDO: TFloatField;
    QrPagRecXEhCRED: TWideStringField;
    QrPagRecXEhDEB: TWideStringField;
    QrPagRecXNOME_TERCEIRO: TWideStringField;
    QrPagRec1Data: TDateField;
    QrPagRec1DataDoc: TDateField;
    QrPagRec1Vencimento: TDateField;
    QrPagRec1TERCEIRO: TFloatField;
    QrPagRec1NOME_TERCEIRO: TWideStringField;
    QrPagRec1NOMEVENCIDO: TWideStringField;
    QrPagRec1NOMECONTA: TWideStringField;
    QrPagRec1Descricao: TWideStringField;
    QrPagRec1CtrlPai: TFloatField;
    QrPagRec1Tipo: TFloatField;
    QrPagRec1Documento: TFloatField;
    QrPagRec1NotaFiscal: TFloatField;
    QrPagRec1Valor: TFloatField;
    QrPagRec1MoraDia: TFloatField;
    QrPagRec1PAGO_REAL: TFloatField;
    QrPagRec1PENDENTE: TFloatField;
    QrPagRec1ATUALIZADO: TFloatField;
    QrPagRec1MULTA_REAL: TFloatField;
    QrPagRec1ATRAZODD: TFloatField;
    QrPagRecItsData: TDateField;
    QrPagRecItsTipo: TSmallintField;
    QrPagRecItsCarteira: TIntegerField;
    QrPagRecItsSub: TSmallintField;
    QrPagRecItsAutorizacao: TIntegerField;
    QrPagRecItsGenero: TIntegerField;
    QrPagRecItsDescricao: TWideStringField;
    QrPagRecItsNotaFiscal: TIntegerField;
    QrPagRecItsDebito: TFloatField;
    QrPagRecItsCredito: TFloatField;
    QrPagRecItsCompensado: TDateField;
    QrPagRecItsDocumento: TFloatField;
    QrPagRecItsSit: TIntegerField;
    QrPagRecItsVencimento: TDateField;
    QrPagRecItsLk: TIntegerField;
    QrPagRecItsFatID: TIntegerField;
    QrPagRecItsFatParcela: TIntegerField;
    QrPagRecItsID_Sub: TSmallintField;
    QrPagRecItsFatura: TWideStringField;
    QrPagRecItsBanco: TIntegerField;
    QrPagRecItsLocal: TIntegerField;
    QrPagRecItsCartao: TIntegerField;
    QrPagRecItsLinha: TIntegerField;
    QrPagRecItsOperCount: TIntegerField;
    QrPagRecItsLancto: TIntegerField;
    QrPagRecItsPago: TFloatField;
    QrPagRecItsFornecedor: TIntegerField;
    QrPagRecItsCliente: TIntegerField;
    QrPagRecItsMoraDia: TFloatField;
    QrPagRecItsMulta: TFloatField;
    QrPagRecItsProtesto: TDateField;
    QrPagRecItsDataCad: TDateField;
    QrPagRecItsDataAlt: TDateField;
    QrPagRecItsUserCad: TSmallintField;
    QrPagRecItsUserAlt: TSmallintField;
    QrPagRecItsDataDoc: TDateField;
    QrPagRecItsNivel: TIntegerField;
    QrPagRecItsVendedor: TIntegerField;
    QrPagRecItsAccount: TIntegerField;
    QrPagRecItsNOMECONTA: TWideStringField;
    QrPagRecItsNOMECARTEIRA: TWideStringField;
    QrPagRecItsSALDO: TFloatField;
    QrPagRecItsEhCRED: TWideStringField;
    QrPagRecItsEhDEB: TWideStringField;
    QrPagRecItsNOME_TERCEIRO: TWideStringField;
    QrPagRecItsTERCEIRO: TLargeintField;
    QrExtratoNOMECONTA: TWideStringField;
    QrExtratoNOMECARTEIRA: TWideStringField;
    DsAccounts: TDataSource;
    QrAccounts: TmySQLQuery;
    QrAccountsCodigo: TIntegerField;
    QrAccountsNOMECONTATO: TWideStringField;
    QrVendedores: TmySQLQuery;
    DsVendedores: TDataSource;
    QrVendedoresCodigo: TIntegerField;
    QrVendedoresNOMECONTATO: TWideStringField;
    QrPagRecXControle: TIntegerField;
    QrPagRecXID_Pgto: TIntegerField;
    QrPagRecXMez: TIntegerField;
    QrPagRecXCtrlIni: TIntegerField;
    QrPagRec1ID_Pgto: TFloatField;
    QrPagRec1CtrlIni: TFloatField;
    QrPagRec1Controle: TFloatField;
    QrPagRecItsControle: TIntegerField;
    QrPagRecItsID_Pgto: TIntegerField;
    QrPagRecItsMez: TIntegerField;
    QrPagRecItsCtrlIni: TIntegerField;
    QrPagRecData: TDateField;
    QrPagRecTipo: TSmallintField;
    QrPagRecCarteira: TIntegerField;
    QrPagRecControle: TIntegerField;
    QrPagRecSub: TSmallintField;
    QrPagRecAutorizacao: TIntegerField;
    QrPagRecGenero: TIntegerField;
    QrPagRecDescricao: TWideStringField;
    QrPagRecNotaFiscal: TIntegerField;
    QrPagRecDebito: TFloatField;
    QrPagRecCredito: TFloatField;
    QrPagRecCompensado: TDateField;
    QrPagRecDocumento: TFloatField;
    QrPagRecSit: TIntegerField;
    QrPagRecVencimento: TDateField;
    QrPagRecLk: TIntegerField;
    QrPagRecFatID: TIntegerField;
    QrPagRecFatParcela: TIntegerField;
    QrPagRecID_Pgto: TIntegerField;
    QrPagRecID_Sub: TSmallintField;
    QrPagRecFatura: TWideStringField;
    QrPagRecBanco: TIntegerField;
    QrPagRecLocal: TIntegerField;
    QrPagRecCartao: TIntegerField;
    QrPagRecLinha: TIntegerField;
    QrPagRecOperCount: TIntegerField;
    QrPagRecLancto: TIntegerField;
    QrPagRecPago: TFloatField;
    QrPagRecMez: TIntegerField;
    QrPagRecFornecedor: TIntegerField;
    QrPagRecCliente: TIntegerField;
    QrPagRecMoraDia: TFloatField;
    QrPagRecMulta: TFloatField;
    QrPagRecProtesto: TDateField;
    QrPagRecDataCad: TDateField;
    QrPagRecDataAlt: TDateField;
    QrPagRecUserCad: TSmallintField;
    QrPagRecUserAlt: TSmallintField;
    QrPagRecDataDoc: TDateField;
    QrPagRecCtrlIni: TIntegerField;
    QrPagRecNivel: TIntegerField;
    QrPagRecVendedor: TIntegerField;
    QrPagRecAccount: TIntegerField;
    QrPagRecNOMECONTA: TWideStringField;
    QrPagRecNOMECARTEIRA: TWideStringField;
    QrPagRecSALDO: TFloatField;
    QrPagRecEhCRED: TWideStringField;
    QrPagRecEhDEB: TWideStringField;
    QrPagRecNOME_TERCEIRO: TWideStringField;
    QrPagRecTERCEIRO: TLargeintField;
    QrPagRecVENCER: TDateField;
    QrPagRecVALOR: TFloatField;
    QrPagRecVENCIDO: TDateField;
    QrPagRecATRAZODD: TFloatField;
    QrPagRecMULTA_REAL: TFloatField;
    QrPagRecATUALIZADO: TFloatField;
    QrPagRecPAGO_REAL: TFloatField;
    QrPagRecNOMEVENCIDO: TWideStringField;
    QrExtratoData: TDateField;
    QrExtratoTipo: TSmallintField;
    QrExtratoCarteira: TIntegerField;
    QrExtratoControle: TIntegerField;
    QrExtratoSub: TSmallintField;
    QrExtratoAutorizacao: TIntegerField;
    QrExtratoGenero: TIntegerField;
    QrExtratoDescricao: TWideStringField;
    QrExtratoNotaFiscal: TIntegerField;
    QrExtratoDebito: TFloatField;
    QrExtratoCredito: TFloatField;
    QrExtratoCompensado: TDateField;
    QrExtratoDocumento: TFloatField;
    QrExtratoSit: TIntegerField;
    QrExtratoVencimento: TDateField;
    QrExtratoLk: TIntegerField;
    QrExtratoFatID: TIntegerField;
    QrExtratoFatParcela: TIntegerField;
    QrExtratoID_Pgto: TIntegerField;
    QrExtratoID_Sub: TSmallintField;
    QrExtratoFatura: TWideStringField;
    QrExtratoBanco: TIntegerField;
    QrExtratoLocal: TIntegerField;
    QrExtratoCartao: TIntegerField;
    QrExtratoLinha: TIntegerField;
    QrExtratoOperCount: TIntegerField;
    QrExtratoLancto: TIntegerField;
    QrExtratoPago: TFloatField;
    QrExtratoMez: TIntegerField;
    QrExtratoFornecedor: TIntegerField;
    QrExtratoCliente: TIntegerField;
    QrExtratoMoraDia: TFloatField;
    QrExtratoMulta: TFloatField;
    QrExtratoProtesto: TDateField;
    QrExtratoDataCad: TDateField;
    QrExtratoDataAlt: TDateField;
    QrExtratoUserCad: TSmallintField;
    QrExtratoUserAlt: TSmallintField;
    QrExtratoDataDoc: TDateField;
    QrExtratoCtrlIni: TIntegerField;
    QrExtratoNivel: TIntegerField;
    QrExtratoVendedor: TIntegerField;
    QrExtratoAccount: TIntegerField;
    QrTerceirosTel1: TWideStringField;
    QrTerceirosTel2: TWideStringField;
    QrTerceirosTel3: TWideStringField;
    QrTerceirosTEL1_TXT: TWideStringField;
    QrTerceirosTEL2_TXT: TWideStringField;
    QrTerceirosTEL3_TXT: TWideStringField;
    QrTerceirosTELEFONES: TWideStringField;
    QrPagRecXDuplicata: TWideStringField;
    QrPagRecFatID_Sub: TIntegerField;
    QrPagRecICMS_P: TFloatField;
    QrPagRecICMS_V: TFloatField;
    QrPagRecDuplicata: TWideStringField;
    QrPagRec1Duplicata: TWideStringField;
    QrPagRecXFatID_Sub: TIntegerField;
    QrPagRecXICMS_P: TFloatField;
    QrPagRecXICMS_V: TFloatField;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    frxFin_Relat_005_02_B1: TfrxReport;
    frxDsPagRec1: TfrxDBDataset;
    frxFin_Relat_005_02_B2: TfrxReport;
    frxDsPagRec: TfrxDBDataset;
    frxFin_Relat_005_00: TfrxReport;
    frxDsExtrato: TfrxDBDataset;
    frxFin_Relat_005_01_A08: TfrxReport;
    frxDsFluxo: TfrxDBDataset;
    QrExtratoQtde: TFloatField;
    QrPagRecQtde: TFloatField;
    frxFin_Relat_005_02_A08: TfrxReport;
    QrPagRecXQtde: TFloatField;
    QrPagRec1Qtde: TFloatField;
    frxFin_Relat_005_02_A06: TfrxReport;
    QrPagRecXNO_UH: TWideStringField;
    QrPagRecNO_UH: TWideStringField;
    QrPagRecItsNO_UH: TWideStringField;
    frxFin_Relat_005_02_A07: TfrxReport;
    QrPagRec1NO_UH: TWideStringField;
    DsFlxo: TDataSource;
    QrFlxo: TmySQLQuery;
    QrExtr: TmySQLQuery;
    QrExtratoDataE: TDateField;
    QrExtratoDataV: TDateField;
    QrExtratoDataQ: TDateField;
    QrExtratoDataX: TDateField;
    QrExtratoTexto: TWideStringField;
    QrExtratoDocum: TWideStringField;
    QrExtratoNotaF: TWideStringField;
    QrExtratoSaldo: TFloatField;
    QrExtratoCartC: TIntegerField;
    QrExtratoCartN: TWideStringField;
    QrExtratoCodig: TIntegerField;
    QrExtratoCtrle: TIntegerField;
    QrExtratoCtSub: TIntegerField;
    QrExtratoID_Pg: TIntegerField;
    QrExtratoTipoI: TIntegerField;
    DsExtr: TDataSource;
    QrFlxoData: TDateField;
    QrFlxoCarteira: TIntegerField;
    QrFlxoControle: TIntegerField;
    QrFlxoSub: TSmallintField;
    QrFlxoQtde: TFloatField;
    QrFlxoDescricao: TWideStringField;
    QrFlxoNotaFiscal: TIntegerField;
    QrFlxoDebito: TFloatField;
    QrFlxoCredito: TFloatField;
    QrFlxoCompensado: TDateField;
    QrFlxoSerieCH: TWideStringField;
    QrFlxoDocumento: TFloatField;
    QrFlxoVencimento: TDateField;
    QrFlxoNOMECART: TWideStringField;
    frxDsExtr: TfrxDBDataset;
    QrExtrVALOR: TFloatField;
    QrExtrCredi: TFloatField;
    QrExtrDebit: TFloatField;
    QrFlxoNOMERELACIONADO: TWideStringField;
    frxFin_Relat_005_01_A07: TfrxReport;
    frxFin_Relat_005_01_A06: TfrxReport;
    QrPagRecSERIEDOC: TWideStringField;
    QrPagRecSerieCH: TWideStringField;
    QrFlxoID_Pgto: TIntegerField;
    QrInicial: TmySQLQuery;
    QrInicialSdoFimB: TFloatField;
    QrAnteriorMovimento: TFloatField;
    QrFlxoQUITACAO: TDateField;
    dmkPermissoes1: TdmkPermissoes;
    QrPagRecXTERCEIRO: TIntegerField;
    QrFlxoDESCRI_TXT: TWideStringField;
    QrFlxoNOMECONTA: TWideStringField;
    QrFlxoSit: TIntegerField;
    QrFlxoPago: TFloatField;
    QrFlxoTipo: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSair: TBitBtn;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox4: TGroupBox;
    PainelDados: TPanel;
    CkGrade: TCheckBox;
    EdA: TdmkEdit;
    EdZ: TdmkEdit;
    CkNiveis: TCheckBox;
    GBOmiss: TGroupBox;
    CkAnos: TCheckBox;
    CkMeses: TCheckBox;
    CkDias: TCheckBox;
    EdAnos: TdmkEdit;
    EdMeses: TdmkEdit;
    EdDias: TdmkEdit;
    BtSalvar: TBitBtn;
    CkOmiss: TCheckBox;
    GBPerdido: TGroupBox;
    CkAnos2: TCheckBox;
    CkMeses2: TCheckBox;
    CkDias2: TCheckBox;
    EdAnos2: TdmkEdit;
    EdMeses2: TdmkEdit;
    EdDias2: TdmkEdit;
    BtGravar: TBitBtn;
    CkSubstituir: TCheckBox;
    CkExclusivo: TCheckBox;
    PainelE: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    CkOrdem1: TCheckBox;
    Panel2: TPanel;
    CkOrdem2: TCheckBox;
    RGHistorico: TRadioGroup;
    RGFonte: TRadioGroup;
    PB1: TProgressBar;
    PainelA: TPanel;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    RGTipo: TRadioGroup;
    GroupBox6: TGroupBox;
    PainelC: TPanel;
    PnDatas2: TPanel;
    GBVencto: TGroupBox;
    LaVenctI: TLabel;
    LaVenctF: TLabel;
    TPVctoIni: TdmkEditDateTimePicker;
    TPVctoFim: TdmkEditDateTimePicker;
    CkVencto: TCheckBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    TPDocIni: TdmkEditDateTimePicker;
    TPDocFim: TdmkEditDateTimePicker;
    CkDataDoc: TCheckBox;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    TPCompIni: TdmkEditDateTimePicker;
    TPCompFim: TdmkEditDateTimePicker;
    CkDataComp: TCheckBox;
    PnDatas1: TPanel;
    GroupBox1: TGroupBox;
    Label101: TLabel;
    Label102: TLabel;
    TPEmissIni: TdmkEditDateTimePicker;
    TPEmissFim: TdmkEditDateTimePicker;
    CkEmissao: TCheckBox;
    PnFluxo: TPanel;
    Label3: TLabel;
    Label7: TLabel;
    GroupBox5: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    TPCre: TdmkEditDateTimePicker;
    TPDeb: TdmkEditDateTimePicker;
    PainelD: TPanel;
    LaAccount: TLabel;
    LaTerceiro: TLabel;
    Label4: TLabel;
    LaVendedor: TLabel;
    CBAccount: TdmkDBLookupComboBox;
    EdAccount: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    EdTerceiro: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    EdConta: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    EdVendedor: TdmkEditCB;
    CGPendencias: TdmkCheckGroup;
    BtParar: TBitBtn;
    PnDepto: TPanel;
    EdDepto: TdmkEditCB;
    CBDepto: TdmkDBLookupComboBox;
    QrDeptos: TmySQLQuery;
    DsDeptos: TDataSource;
    QrDeptosNome: TWideStringField;
    QrDeptosCodigo: TIntegerField;
    CkDepto: TCheckBox;
    frxFin_Relat_005_03_A08: TfrxReport;
    frxFin_Relat_005_03_A06: TfrxReport;
    frxFin_Relat_005_03_A07: TfrxReport;
    frxFin_Relat_005_04_A06: TfrxReport;
    frxFin_Relat_005_04_A07: TfrxReport;
    frxFin_Relat_005_04_A08: TfrxReport;
    QrPagRecMoraVal: TFloatField;
    QrPagRecMultaVal: TFloatField;
    QrPagRecDescoVal: TFloatField;
    procedure FormActivate(Sender: TObject);
    procedure BtSairClick(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrPagRecCalcFields(DataSet: TDataSet);
    procedure CkOmissClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure BtGravarClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPagRecItsCalcFields(DataSet: TDataSet);
    procedure QrPagRecXCalcFields(DataSet: TDataSet);
    procedure QrPagRec1CalcFields(DataSet: TDataSet);
    procedure QrTerceirosCalcFields(DataSet: TDataSet);
    procedure frxFin_Relat_005_02_B1GetValue(const VarName: String;
      var Value: Variant);
    procedure frxFin_Relat_005_00GetValue(const VarName: String;
      var Value: Variant);
    procedure QrExtrCalcFields(DataSet: TDataSet);
    procedure QrFlxoCalcFields(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtPararClick(Sender: TObject);
  private
    { Private declarations }
    FParar: Boolean;
    //
    FEmpresa, FCliInt, FEntidade: Integer;
    FTabLctA, FTabLctB, FTabLctD, FEntidade_TXT, FSQLDepto_TXT: String;
    FDtEncer, FDtMorto: TDateTime;
    procedure R00_ImprimeExtratoConsolidado();
    procedure R01_ImprimeFluxoDeCaixa();
    procedure R02_ImprimeContasAPagar(Sender: TObject; Saida: TTipoSaidaRelatorio);
    procedure SQLContasAPagar(Query: TmySQLQuery; Tipo: Integer);
    procedure CreateDockableWindows;
    procedure GeraRelatorio(Saida: TTipoSaidaRelatorio);
    function SQL_Ordenar(): String;
  public
    { Public declarations }
  end;

var
  FmExtratos: TFmExtratos;

implementation

uses UCreate, Module, ModuleGeral, UMySQLModule, UnFinanceiro, ModuleFin,
  UnMyObjects, DmkDAC_PF;

{$R *.DFM}

{FmMyGlyfs}

const
  Colors: array[0..0] of TColor = (clWhite);
  ColStr: array[0..0] of string = ('White');

var
  Ext_EmisI, Ext_EmisF, Ext_VctoI, Ext_VctoF, (*Ext_DataH, Ext_DataA,*)
  Ext_DocI,  Ext_DocF,  Ext_CompI, Ext_CompF: String;
  Ext_Saldo, Ext_SdIni,
  Ext_VALORA,     Ext_VALORB,     Ext_VALORC,
  Ext_PENDENTEA,  Ext_PENDENTEB,  Ext_PENDENTEC,
  Ext_ATUALIZADOA, Ext_ATUALIZADOB, Ext_ATUALIZADOC,
  Ext_DEVIDOA, Ext_DEVIDOB, Ext_DEVIDOC,
  Ext_PAGO_REALA, Ext_PAGO_REALB, Ext_PAGO_REALC: Double;
  Ext_Vencto: TDate;
  DockWindows: array[0..0] of TFmDockForm;


procedure TFmExtratos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmExtratos.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmExtratos.RGTipoClick(Sender: TObject);
begin
  CkExclusivo.Visible := RGTipo.ItemIndex in ([0,1]);
  PnDatas2.Visible    := RGTipo.ItemIndex in ([2,3,4,5,6,7,8,9]);
  PainelD.Visible     := RGTipo.ItemIndex in ([2,3,4,5,6,7,8,9]);
  PnFluxo.Visible     := RGTipo.ItemIndex = 1;
  CkOrdem1.Visible    := RGTipo.ItemIndex <> 1;
  RGOrdem1.Visible    := RGTipo.ItemIndex <> 1;
  CkOrdem2.Visible    := RGTipo.ItemIndex <> 1;
  RGOrdem2.Visible    := RGTipo.ItemIndex <> 1;
  RGHistorico.Visible := RGTipo.ItemIndex in ([6,7]);
  RGFonte.Visible     := RGTipo.ItemIndex in ([1,2,3,4,5,8,9]);
  //
  EdA.Visible        := True;
  EdZ.Visible        := True;
  CkNiveis.Visible   := True;
  //CkNiveis.Checked   := False;
  CkOmiss.Visible    := False;
  GBOmiss.Visible    := False;
  GBPerdido.Visible  := False;
  LaAccount.Visible  := False;
  EdAccount.Visible  := False;
  CBAccount.Visible  := False;
  LaVendedor.Visible := False;
  EdVendedor.Visible := False;
  CBVendedor.Visible := False;
  //
  //CkVencto.Visible     := True;
  GBVencto.Visible     := True;
  CkSubstituir.Visible := False;
  CkEmissao.Caption    := 'Data da emiss�o:';
  case RGTipo.ItemIndex of
    0:
    begin
      GBVencto.Visible := True;
      //
      EdA.Visible := False;
      EdZ.Visible := False;
      CkNiveis.Visible := False;
      //
      if TPEmissIni.Date > date then TPEmissIni.Date := date;
      if TPEmissFim.Date < date then TPEmissFim.Date := date;
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date;
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      if TPVctoIni.Date > date then TPVctoIni.Date := date;
      if TPVctoFim.Date < date then TPVctoFim.Date := date;
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date;
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CBTerceiro.Enabled := False;
      EdTerceiro.Enabled := False;
      LaTerceiro.Enabled := False;
    end;
    1:
    begin
      EdA.Visible          := False;
      EdZ.Visible          := False;
      CkNiveis.Visible     := False;
      CkGrade.Visible      := False;
      CkSubstituir.Visible := True;
      CkEmissao.Caption    := 'Per�odo:';
      //
      if TPEmissFim.Date < date then TPEmissFim.Date := date;
      TPEmissIni.MinDate := 1;
      TPEmissIni.MaxDate := Date+7305;//Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := Date;
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      if TPVctoFim.Date < date then TPVctoFim.Date := date;
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := Date;
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      CBTerceiro.Enabled := False;
      EdTerceiro.Enabled := False;
      LaTerceiro.Enabled := False;
    end;
    2:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Fornecedor';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    3:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
      ////
      LaAccount.Visible := True;
      EdAccount.Visible := True;
      CBAccount.Visible := True;
      LaVendedor.Visible := True;
      EdVendedor.Visible := True;
      CBVendedor.Visible := True;
    end;
    4:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    5:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    6:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Fornecedor';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    7:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
      ////
      LaAccount.Visible := True;
      EdAccount.Visible := True;
      CBAccount.Visible := True;
      LaVendedor.Visible := True;
      EdVendedor.Visible := True;
      CBVendedor.Visible := True;
    end;
    8:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      GBPerdido.Visible := True;
    end;
    9:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      GBPerdido.Visible := True;
    end;
  end;
  if not RGOrdem1.Enabled then RGOrdem1.ItemIndex := 0;
  if not RGOrdem2.Enabled then RGOrdem2.ItemIndex := 1;
  if RGTipo.ItemIndex in ([0,1]) then
  begin
    CkVencto.Enabled  := False;
    GBVencto.Enabled  := False;
    LaVenctI.Enabled  := False;
    LaVenctF.Enabled  := False;
    TPVctoIni.Enabled := False;
    TPVctoFim.Enabled := False;
  end else begin
    CkVencto.Enabled  := True;
    GBVencto.Enabled  := True;
    LaVenctI.Enabled  := True;
    LaVenctF.Enabled  := True;
    TPVctoIni.Enabled := True;
    TPVctoFim.Enabled := True;
  end;
end;

procedure TFmExtratos.BtImprimeClick(Sender: TObject);
begin
  FParar := False;
  //
  FEntidade := DModG.QrEmpresasCodigo.Value;
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  FCliInt   := DModG.QrEmpresasFilial.Value;
  //
  DModG.Def_EM_ABD(TMeuDB, FEntidade, FCliInt, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  //
  GeraRelatorio(tsrImpressao);
end;

procedure TFmExtratos.BtPararClick(Sender: TObject);
begin
  FParar := True;
end;

procedure TFmExtratos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  CreateDockableWindows;
  //
  TPEmissIni.Date  := Date - 60;
  TPVctoIni.Date   := Date - 60;
  TPDocIni.Date    := Date - 90;
  TPDocFim.Date    := Date + 60;
  TPCompIni.Date   := Date;
  TPCompFim.Date   := Date;
  RGTipo.ItemIndex := 1;
  UnDmkDAC_PF.AbreQuery(QrTerceiros, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrAccounts, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVendedores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  ///////
  EdAnos.Text := Geral.ReadAppKeyCU('Anos', 'Dermatek\Extratos\Venctos',
                ktString,'0');
  EdMeses.Text := Geral.ReadAppKeyCU('Meses', 'Dermatek\Extratos\Venctos',
                ktString,'0');
  EdDias.Text := Geral.ReadAppKeyCU('Dias', 'Dermatek\Extratos\Venctos',
                ktString,'0');
  CkAnos.Checked := Geral.ReadAppKeyCU('Ck_Anos', 'Dermatek\Extratos\Venctos',
                ktBoolean, False);
  CkMeses.Checked := Geral.ReadAppKeyCU('Ck_Meses', 'Dermatek\Extratos\Venctos',
                ktBoolean, False);
  CkDias.Checked := Geral.ReadAppKeyCU('Ck_Dias', 'Dermatek\Extratos\Venctos',
                ktBoolean, False);
  ///////
  EdAnos2.Text := Geral.ReadAppKeyCU('Anos', 'Dermatek\Extratos\Perdidos',
                ktString,'0');
  EdMeses2.Text := Geral.ReadAppKeyCU('Meses', 'Dermatek\Extratos\Perdidos',
                ktString,'0');
  EdDias2.Text := Geral.ReadAppKeyCU('Dias', 'Dermatek\Extratos\Perdidos',
                ktString,'0');
  CkAnos2.Checked := Geral.ReadAppKeyCU('Ck_Anos', 'Dermatek\Extratos\Perdidos',
                ktBoolean, False);
  CkMeses2.Checked := Geral.ReadAppKeyCU('Ck_Meses', 'Dermatek\Extratos\Perdidos',
                ktBoolean, False);
  CkDias2.Checked := Geral.ReadAppKeyCU('Ck_Dias', 'Dermatek\Extratos\Perdidos',
                ktBoolean, False);
  //
  QrExtrato.Close;
  QrExtrato.Database := DModG.MyPID_DB;
  //
  QrFlxo.Close;
  QrFlxo.Database := DModG.MyPID_DB;
  //
  CGPendencias.SetMaxValue;
  //
  RGOrdem1.Items[7] := dmkPF.TxtUH();
  RGOrdem2.Items[7] := dmkPF.TxtUH();
  //
  if VAR_KIND_DEPTO = kdObra then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDeptos, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM obrascab ',
    'ORDER BY Nome ',
    '']);
    //
    CkDepto.Caption := 'Obra:';
    PnDepto.Visible := True;
  end;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmExtratos.R00_ImprimeExtratoConsolidado();
  procedure GeraParteSQL(TabLct: String);
  begin
    QrExtrato.SQL.Add('SELECT lct.*, cta.Nome NOMECONTA, car.Nome NOMECARTEIRA');
    QrExtrato.SQL.Add('FROM ' + TabLct + ' lct, ' + TMeuDB +
                      '.Contas cta, ' + TMeuDB + '.Carteiras car');
    QrExtrato.SQL.Add('WHERE lct.Data BETWEEN "' + Ext_EmisI + '" AND "' + Ext_EmisF + '"');
    QrExtrato.SQL.Add('AND (lct.Tipo=1 OR (lct.Tipo=0 AND Vencimento<="' + Ext_EmisF + '"))');
    QrExtrato.SQL.Add('AND cta.Codigo=lct.Genero');
    QrExtrato.SQL.Add('AND car.Codigo=lct.Carteira');
    QrExtrato.SQL.Add('AND car.ForneceI=' + FEntidade_TXT);
    QrExtrato.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);
    QrExtrato.SQL.Add(FSQLDepto_TXT);
    if CkExclusivo.Checked then
      QrExtrato.SQL.Add('AND car.Exclusivo = 0');

  end;
var
  FldIni, TabIni: String;
  Inicial: Double;
begin
  FldIni := UFinanceiro.DefLctFldSdoIni(TPEmissIni.Date, FDtEncer, FDtMorto);
  TabIni := UFinanceiro.DefLctTab(TPEmissIni.Date, FDtEncer, FDtMorto,
            FTabLcta, FTabLctB, FTabLctD);
  //
  if Uppercase(TabIni) = Uppercase(FTabLctA)then
  begin
    QrInicial.Close;
    QrInicial.SQL.Clear;
    QrInicial.SQL.Add('SELECT SUM(SdoFimB) SdoFimB');
    QrInicial.SQL.Add('FROM carteiras');
    QrInicial.SQL.Add('WHERE Tipo < 2');
    QrInicial.SQL.Add('AND ForneceI=' + FEntidade_Txt);
    if CkExclusivo.Checked then
      QrInicial.SQL.Add('AND Exclusivo = 0');
    UnDmkDAC_PF.AbreQuery(QrInicial, Dmod.MyDB);
    Inicial := QrInicialSdoFimB.Value;
  end else Inicial := 0;
  //
  QrAnterior.Close;
  QrAnterior.SQL.Clear;
  QrAnterior.SQL.Add('SELECT SUM(lct.Credito - lct.Debito)  Movimento');
  QrAnterior.SQL.Add('FROM ' + TabIni + ' lct');
  QrAnterior.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrAnterior.SQL.Add('WHERE lct.Data<"' + Ext_EmisI + '"');
  QrAnterior.SQL.Add('AND (lct.Tipo=1 OR (lct.Tipo=0 AND lct.Vencimento<"' + Ext_EmisI + '"))');
  QrAnterior.SQL.Add('AND car.ForneceI=' + FEntidade_TXT);
  QrAnterior.SQL.Add(FSQLDepto_TXT);
  if CkExclusivo.Checked then
    QrAnterior.SQL.Add('AND car.Exclusivo = 0');
  UnDmkDAC_PF.AbreQuery(QrAnterior, Dmod.MyDB);
  Ext_SdIni := Inicial + QrAnteriorMovimento.Value;
  QrAnterior.Close;
  //
  QrExtrato.Close;
  QrExtrato.SQL.Clear;
  QrExtrato.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_0;');
  QrExtrato.SQL.Add('CREATE TABLE _FIN_RELAT_005_0');
  QrExtrato.SQL.Add('');
  GeraParteSQL(FTabLctA);
  QrExtrato.SQL.Add('UNION');
  GeraParteSQL(FTabLctB);
  QrExtrato.SQL.Add('UNION');
  GeraParteSQL(FTabLctD);
  QrExtrato.SQL.Add(';');
  QrExtrato.SQL.Add('');
  QrExtrato.SQL.Add('SELECT * FROM _FIN_RELAT_005_0');
  QrExtrato.SQL.Add('ORDER BY Data, Carteira, Controle;');
  QrExtrato.SQL.Add('');
  QrExtrato.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_0;');
  QrExtrato.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrExtrato, DModG.MyPID_DB);
  //
  MyObjects.frxMostra(frxFin_Relat_005_00, 'Extrato');
  QrExtrato.Close;
end;

procedure TFmExtratos.R01_ImprimeFluxoDeCaixa();
  function Fmt(Data: TDateTime): String;
  begin
    Result := '"' + FormatDateTime('YYYY-MM-DD', Data) + '"';
  end;
  procedure GeraParteSQL(TabLct, Quitacao, Sits: String);
  begin
    QrFlxo.SQL.Add('SELECT ' + Quitacao + ' QUITACAO, cta.Nome NOMECONTA, ');
    QrFlxo.SQL.Add('lct.Credito, lct.Debito, lct.Data, lct.Vencimento, ');
    QrFlxo.SQL.Add('lct.Compensado, lct.Descricao, lct.Qtde, lct.Documento, ');
    QrFlxo.SQL.Add('lct.SerieCH, lct.NotaFiscal, lct.Carteira, lct.Controle, ');
    QrFlxo.SQL.Add('lct.ID_Pgto, lct.Sub, lct.Sit, lct.Tipo, lct.Pago, ');
    QrFlxo.SQL.Add('car.Nome NOMECART, car.Tipo TP_CART, ');
    QrFlxo.SQL.Add('IF(lct.Cliente>0,');
    QrFlxo.SQL.Add('  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),');
    QrFlxo.SQL.Add('  IF (lct.Fornecedor>0,');
    QrFlxo.SQL.Add('    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO');
    QrFlxo.SQL.Add('FROM ' + TabLct + ' lct');
    QrFlxo.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrFlxo.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    cta ON cta.Codigo=lct.Genero');
    QrFlxo.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades cl ON cl.Codigo=lct.Cliente');
    QrFlxo.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades fo ON fo.Codigo=lct.Fornecedor');
    //
    QrFlxo.SQL.Add('WHERE car.Fornecei=' + IntToStr(FEmpresa));
    QrFlxo.SQL.Add(FSQLDepto_TXT);
    QrFlxo.SQL.Add(dmkPF.SQL_Periodo('AND ' + Quitacao,
      TPEmissIni.Date, TPEmissFim.Date, CkEmissao.Checked, CkEmissao.Checked));
    QrFlxo.SQL.Add('AND lct.Genero > 0');
    QrFlxo.SQL.Add('AND ((car.Tipo<>2) OR (lct.Sit IN (' + Sits + ')))');
    if CkExclusivo.Checked then
      QrFlxo.SQL.Add('AND car.Exclusivo = 0');
  end;
var
  Quitacao, Texto, Docum, NotaF: String;
  DataE, DataV, DataQ, DataX: Variant;
  I, Codig: Integer;
  Credito, Debito, Valor, Saldo, Inicial, SdoCr, SdoDb: Double;
  DataI: TDateTime;
  FldIni, TabIni, Tabela: String;
  MaxVal, Sits: String;
begin
  Screen.Cursor := crHourGlass;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela do saldo anterior');
  //
  PB1.Position := 0;
  PB1.Visible  := True;
  FldIni := UFinanceiro.DefLctFldSdoIni(TPEmissIni.Date, FDtEncer, FDtMorto);
  TabIni := UFinanceiro.DefLctTab(TPEmissIni.Date, FDtEncer, FDtMorto,
            FTabLcta, FTabLctB, FTabLctD);
  //
  Sits := '';
  for I := 0 to CGPendencias.Items.Count - 1 do
  begin
    if CGPendencias.Checked[I] then
      Sits := Sits + ',' + CGPendencias.Items[I][1];
  end;
  if Sits <> '' then
    Sits := Copy(Sits, 2)
  else
    Sits := '-999999999';
  //
  //   S A L D O    I N I C I A L
  if CkEmissao.Checked then DataI := TPEmissIni.Date else DataI := 0;
  //
  if Uppercase(TabIni) = Uppercase(FTabLctA)then
  begin
    QrInicial.Close;
    QrInicial.SQL.Clear;
    QrInicial.SQL.Add('SELECT SUM(SdoFimB) SdoFimB');
    QrInicial.SQL.Add('FROM carteiras');
    QrInicial.SQL.Add('WHERE Tipo < 2');
    QrInicial.SQL.Add('AND ForneceI=' + FEntidade_Txt);
    if CkExclusivo.Checked then
      QrInicial.SQL.Add('AND Exclusivo = 0');
    UnDmkDAC_PF.AbreQuery(QrInicial, Dmod.MyDB);
    Inicial := QrInicialSdoFimB.Value;
  end else Inicial := 0;
  QrAnterior.Close;
  QrAnterior.SQL.Clear;
  QrAnterior.SQL.Add('SELECT ');
  QrAnterior.SQL.Add('SUM(lct.Credito - lct.Debito) Movimento');
  QrAnterior.SQL.Add('FROM ' + TabIni + ' lct');
  QrAnterior.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrAnterior.SQL.Add('WHERE car.Tipo <> 2');
  QrAnterior.SQL.Add('AND car.ForneceI=' + FEntidade_TXT);
  QrAnterior.SQL.Add('AND lct.Data<"' + Geral.FDT(DataI, 1) + '"');
  QrAnterior.SQL.Add(FSQLDepto_TXT);
  if CkExclusivo.Checked then
    QrAnterior.SQL.Add('AND car.Exclusivo = 0');
  UnDmkDAC_PF.AbreQuery(QrAnterior, Dmod.MyDB);

  //   L A N � A M E N T O S
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela de lan�amentos');
  //
   Quitacao := 'Date(IF(car.Tipo <> 2, lct.Data,IF(lct.Sit IN (2,3), lct.Compensado, ' +
  'IF(lct.Vencimento<SYSDATE(), IF(lct.Debito>0, ' + Fmt(TPDeb.Date) +
  ', ' + Fmt(TPCre.Date) + '), lct.Vencimento))))';
  //
  QrFlxo.Close;
  QrFlxo.SQL.Clear;
  QrFlxo.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_1;');
  QrFlxo.SQL.Add('CREATE TABLE _FIN_RELAT_005_1');
  QrFlxo.SQL.Add('');
  GeraParteSQL(FTabLctA, Quitacao, Sits);
  QrFlxo.SQL.Add('UNION');
  GeraParteSQL(FTabLctB, Quitacao, Sits);
  QrFlxo.SQL.Add('UNION');
  GeraParteSQL(FTabLctD, Quitacao, Sits);
  QrFlxo.SQL.Add(';');
  QrFlxo.SQL.Add('');
  QrFlxo.SQL.Add('SELECT * FROM _FIN_RELAT_005_1');
  QrFlxo.SQL.Add('ORDER BY QUITACAO, Credito DESC, Debito;');
  QrFlxo.SQL.Add('');
  QrFlxo.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_1;');
  QrFlxo.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrFlxo, DModG.MyPID_DB);
  //
  PB1.Max := QrFlxo.RecordCount + 1;

  // INSERE DADOS TABELA LOCAL
  Tabela := UCriar.RecriaTempTableNovo(ntrttExtratoCC2, DmodG.QrUpdPID1, False, 0, 'extratocc2');
  //
  Codig := 1;
  PB1.Position := Codig;
  PB1.Update;
  Application.ProcessMessages;
  //
  Saldo := Inicial + QrAnteriorMovimento.Value;
  //
  UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, Tabela, false,
  [
    'DataE', 'DataV', 'DataQ',
    'DataX', 'Texto', 'Docum',
    'NotaF', 'Credi', 'Debit',
    'Saldo', 'CartC', 'CartN',
    'TipoI', 'Ctrle', 'ID_Pg'
  ], ['Codig'], [
    Null, Null, Null,
    dmkPF.FDT_NULL(DataI, 1), 'SALDO ANTERIOR', '',
    '', 0, 0,
    Saldo, 0, '*** T O D A S ***', 1, 0, 0
  ], [Codig], False);
  //
  MaxVal := Geral.FF0(QrFlxo.RecordCount + 1);
  QrFlxo.First;
  BtParar.Visible := True;
  //
  while not QrFlxo.Eof do
  begin
    if FParar then
    begin
      FParar := False;
      BtParar.Visible := False;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      PB1.Position := 0;
      Screen.Cursor := crDefault;
      Exit;
    end;
    Codig := Codig + 1;

    PB1.Position := Codig;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Calculado saldo do item ' +
      Geral.FF0(Codig) + ' de ' + MaxVal);
    PB1.Update;
    Application.ProcessMessages;

    if not DmodFin.DefineValorSit(QrFlxoTipo.Value, QrFlxoSit.Value,
      QrFlxoCredito.Value, QrFlxoDebito.Value, QrFlxoPago.Value, True, True, 
      Credito, Debito, Valor, SdoCr, SdoDb) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    Saldo := Saldo + Valor;
    DataE := dmkPF.FDT_NULL(QrFlxoData.Value, 1);
    DataV := dmkPF.FDT_NULL(QrFlxoVencimento.Value, 1);
    DataQ := dmkPF.FDT_NULL(QrFlxoCompensado.Value, 1);
    DataX := QrFlxoQUITACAO.Value;
    //
    if CkSubstituir.Checked then
      Texto := QrFlxoNOMERELACIONADO.Value
    else
      Texto := QrFlxoDESCRI_TXT.Value;
    //
    if QrFlxoQtde.Value > 0 then
      Texto := FloatToStr(QrFlxoQtde.Value) + ' ' + Texto;
    if QrFlxoDocumento.Value > 0 then
      Docum := FormatFloat('000000', QrFlxoDocumento.Value) else Docum := '';
    if QrFlxoSerieCH.Value <> '' then Docum := QrFlxoSerieCH.Value + Docum;
    if QrFlxoNotaFiscal.Value > 0 then
      NotaF := FormatFloat('000000', QrFlxoNotaFiscal.Value) else NotaF := '';
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, Tabela, false,
    [
      'DataE', 'DataV', 'DataQ',
      'DataX', 'Texto', 'Docum',
      'NotaF', 'Credi', 'Debit',
      'Saldo', 'CartC', 'CartN',
      'TipoI', 'Ctrle', 'ID_Pg', 'CtSub'
    ], ['Codig'], [
      DataE, DataV, DataQ,
      DataX, Texto, Docum,
      NotaF, Credito, Debito,
      Saldo, QrFlxoCarteira.Value, QrFlxoNOMECART.Value,
      2, QrFlxoControle.Value, QrFlxoID_Pgto.Value, QrFlxoSub.Value
    ], [Codig], False);
    //
    QrFlxo.Next;
  end;
  BtParar.Visible := False;
  PB1.Visible := False;

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela do extrato calculado');
  //
  QrExtr.Close;
  UnDmkDAC_PF.AbreQuery(QrExtr, DModG.MyPID_DB);
  Screen.Cursor := crDefault;

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando relat�rio');
  //
  case RGFonte.ItemIndex of
    0: MyObjects.frxMostra(frxFin_Relat_005_01_A06, 'Relat�rio de Fluxo de Caixa');
    1: MyObjects.frxMostra(frxFin_Relat_005_01_A07, 'Relat�rio de Fluxo de Caixa');
    2: MyObjects.frxMostra(frxFin_Relat_005_01_A08, 'Relat�rio de Fluxo de Caixa');
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
end;


procedure TFmExtratos.R02_ImprimeContasAPagar(Sender: TObject;
 Saida: TTipoSaidaRelatorio);
var
  CtrlPai: Double;
  DockWindow: TFmDockForm;
  Maximo, Conta: Integer;
  RelFrx: TfrxReport;
  TbPagRec1, TbPagRec2: String;
begin
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  VAR_ORDENAR := RGOrdem1.ItemIndex * 10 + RGOrdem2.ItemIndex;
  ////
  SQLContasAPagar(QrPagRecX,   0);
  if RGTipo.ItemIndex in ([4,5]) then
    // ERRADO! mudei 2008.07.22
    //SQLContasAPagar(QrPagRec,    0)
    SQLContasAPagar(QrPagRec,    3)
  else
    SQLContasAPagar(QrPagRec,    1);
  //
  SQLContasAPagar(QrPagRecIts, 2);
  if RGTipo.ItemIndex in ([6,7]) then
  begin
    DockWindow := DockWindows[(Sender as TComponent).Tag];
    DockWindow.Show;
    DockWindow.Refresh;
    //
    TbPagRec1 := UCriar.RecriaTempTableNovo(ntrttPagRec1, DmodG.QrUpdPID1, False, 0, 'PagRec1');
    TbPagRec2 := UCriar.RecriaTempTableNovo(ntrttPagRec2, DmodG.QrUpdPID1, False, 0, 'PagRec2');
    //
    Maximo := QrPagRecX.RecordCount;
    QrPagRecX.First;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + TbPagRec1 + ' SET ');
    DModG.QrUpdPID1.SQL.Add('Data=:P0, Documento=:P1, NotaFiscal=:P2, ');
    DModG.QrUpdPID1.SQL.Add('NOMEVENCIDO=:P3, NOMECONTA=:P4, Descricao=:P5, ');
    DModG.QrUpdPID1.SQL.Add('Controle=:P6, Valor=:P7, MoraDia=:P8, ');
    DModG.QrUpdPID1.SQL.Add('PAGO_REAL=:P9, ATRAZODD=:P10, ATUALIZADO=:P11, ');
    DModG.QrUpdPID1.SQL.Add('MULTA_REAL=:P12, DataDoc=:P13, Vencimento=:P14, ');
    DModG.QrUpdPID1.SQL.Add('TERCEIRO=:P15, NOME_TERCEIRO=:P16, CtrlPai=:P17, ');
    DModG.QrUpdPID1.SQL.Add('Tipo=:P18, Pendente=:P19, ID_Pgto=:P20, ');
    DModG.QrUpdPID1.SQL.Add('CtrlIni=:P21, Duplicata=:P22, Qtde=:P23, ');
    DModG.QrUpdPID1.SQL.Add('NO_UH=:P24 ');
    Screen.Cursor := VAR_CURSOR;
    Conta := 0;
    DockWindow.AtualizaDados(Conta, Maximo);
    while not QrPagRecX.Eof do
    begin
      Conta := Conta + 1;
      DockWindow.AtualizaDados(Conta, Maximo);
      Update;
      Application.ProcessMessages;
      if VAR_PARAR then
      begin
        TbPagRec1 := UCriar.RecriaTempTableNovo(ntrttPagRec1, DmodG.QrUpdPID1, False, 0, 'PagRec1');
        TbPagRec2 := UCriar.RecriaTempTableNovo(ntrttPagRec2, DmodG.QrUpdPID1, False, 0, 'PagRec2');
        //
        Exit;
      end;
      if QrPagRecXCtrlIni.Value > 0 then
        CtrlPai := QrPagRecXCtrlIni.Value
      else
        CtrlPai := QrPagRecXControle.Value;
      //
      DModG.QrUpdPID1.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, QrPagRecXData.Value);
      DModG.QrUpdPID1.Params[01].AsFloat   := QrPagRecXDocumento.Value;
      DModG.QrUpdPID1.Params[02].AsFloat   := QrPagRecXNotaFiscal.Value;
      DModG.QrUpdPID1.Params[03].AsString  := QrPagRecXNOMEVENCIDO.Value;
      DModG.QrUpdPID1.Params[04].AsString  := QrPagRecXNOMECONTA.Value;
      DModG.QrUpdPID1.Params[05].AsString  := QrPagRecXDescricao.Value;
      DModG.QrUpdPID1.Params[06].AsFloat   := QrPagRecXControle.Value;
      DModG.QrUpdPID1.Params[07].AsFloat   := QrPagRecXValor.Value;
      DModG.QrUpdPID1.Params[08].AsFloat   := QrPagRecXMoraDia.Value;
      DModG.QrUpdPID1.Params[09].AsFloat   := QrPagRecXPAGO_REAL.Value;
      DModG.QrUpdPID1.Params[10].AsFloat   := QrPagRecXATRAZODD.Value;
      DModG.QrUpdPID1.Params[11].AsFloat   := QrPagRecXATUALIZADO.Value;
      DModG.QrUpdPID1.Params[12].AsFloat   := QrPagRecXMULTA_REAL.Value;
      //
      DModG.QrUpdPID1.Params[13].AsString  := FormatDateTime(VAR_FORMATDATE, QrPagRecXDataDoc.Value);
      DModG.QrUpdPID1.Params[14].AsString  := FormatDateTime(VAR_FORMATDATE, QrPagRecXVencimento.Value);
      DModG.QrUpdPID1.Params[15].AsFloat   := QrPagRecXTerceiro.Value;
      DModG.QrUpdPID1.Params[16].AsString  := QrPagRecXNOME_TERCEIRO.Value;
      DModG.QrUpdPID1.Params[17].AsFloat   := CtrlPai;
      DModG.QrUpdPID1.Params[18].AsFloat   := QrPagRecXTipo.Value;
      DModG.QrUpdPID1.Params[19].AsFloat   := QrPagRecXPENDENTE.Value;
      DModG.QrUpdPID1.Params[20].AsFloat   := QrPagRecXID_Pgto.Value;
      DModG.QrUpdPID1.Params[21].AsFloat   := QrPagRecXCtrlIni.Value;
      DModG.QrUpdPID1.Params[22].AsString  := QrPagRecXDuplicata.Value;
      DModG.QrUpdPID1.Params[23].AsFloat   := QrPagRecXQtde.Value;
      DModG.QrUpdPID1.Params[24].AsString  := QrPagRecXNO_UH.Value;

      DModG.QrUpdPID1.ExecSQL;
      QrPagRecX.Next;
    end;
    DockWindow.Hide;
    with QrPagRec1.SQL do
    begin
      Clear;
      Add('SELECT * FROM ' + TbPagRec1);
      Add(SQL_Ordenar());
    end;
    QrPagRec1.Database := DModG.MyPID_DB;
    UnDmkDAC_PF.AbreQuery(QrPagRec1, DModG.MyPID_DB);
    case RGHistorico.ItemIndex of
      0: Relfrx := frxFin_Relat_005_02_B1;
      1: Relfrx := frxFin_Relat_005_02_B2;
      else Relfrx := nil;
    end;
  end else
  begin
    if RGTipo.ItemIndex in ([2,3]) then
    begin
      case RGFonte.ItemIndex of
        0: RelFrx := frxFin_Relat_005_03_A06;
        1: RelFrx := frxFin_Relat_005_03_A07;
        2: RelFrx := frxFin_Relat_005_03_A08;
        else RelFrx := nil;
      end;
    end else
    if RGTipo.ItemIndex in ([4,5]) then
    begin
      case RGFonte.ItemIndex of
        0: RelFrx := frxFin_Relat_005_04_A06;
        1: RelFrx := frxFin_Relat_005_04_A07;
        2: RelFrx := frxFin_Relat_005_04_A08;
        else RelFrx := nil;
      end;
    end else
    begin
      case RGFonte.ItemIndex of
        0: RelFrx := frxFin_Relat_005_02_A06;
        1: RelFrx := frxFin_Relat_005_02_A07;
        2: RelFrx := frxFin_Relat_005_02_A08;
        else RelFrx := nil;
      end;
    end;
  end;
  if Relfrx <> nil then
    MyObjects.frxMostra(Relfrx, 'Hist�rico de lan�amentos')
  else
    Geral.MB_Erro('Relat�rio n�o definido!');
  QrPagRec1.Close;
  QrPagRec.Close;
  QrPagRecIts.Close;
  Screen.Cursor := VAR_CURSOR;
end;

procedure TFmExtratos.SQLContasAPagar(Query: TmySQLQuery; Tipo: Integer);
  procedure GeraParteSQL(TabLct: String);
  var
    Anos, Meses, Dias: Word;
  begin
    Query.SQL.Add('SELECT lct.*, co.Nome NOMECONTA, ca.Nome NOMECARTEIRA,');
    Query.SQL.Add('IF(lct.Sit IN (0, 5, 6), (lct.Credito-lct.Debito),');
    Query.SQL.Add('IF(lct.Sit=1, (lct.Credito-lct.Debito-lct.Pago), 0)) SALDO,');
    Query.SQL.Add('co.Credito EhCRED, co.Debito EhDEB, ');
    Query.SQL.Add('IF ((lct.Fornecedor>0) AND (fo.Tipo=0), fo.RazaoSocial, ');
    Query.SQL.Add('IF ((lct.Fornecedor>0) AND (fo.Tipo=1), fo.Nome, ');
    Query.SQL.Add('IF ((lct.Cliente>0) AND (cl.Tipo=0), cl.RazaoSocial, ');
    Query.SQL.Add('IF ((lct.Cliente>0) AND (cl.Tipo=1), cl.Nome, "???")))) NOME_TERCEIRO, ');
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
    //Query.SQL.Add('CASE WHEN lct.Fornecedor>0 then lct.Fornecedor ELSE lct.Cliente END TERCEIRO,');
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
    Query.SQL.Add('0 CtrlPai, ');
    case VAR_KIND_DEPTO of
      kdUH:     Query.SQL.Add('imv.Unidade NO_UH');
      kdObra:   Query.SQL.Add('obr.Sigla NO_UH');
      kdOS1:    Query.SQL.Add('"" NO_UH');
      kdNenhum: Query.SQL.Add('"" NO_UH');
      else
      begin
        Query.SQL.Add('"" NO_UH');
        dmkPF.InfoKIND_DEPTO('SQLContasAPAgar');
      end;
    end;
    Query.SQL.Add('FROM ' + TabLct + ' lct, ' + TMeuDB + '.contas co, ' +
      TMeuDB + '.carteiras ca, ' + TMeuDB + '.entidades cl, ' +
      TMeuDB + '.entidades fo');
    case VAR_KIND_DEPTO of
      kdUH: Query.SQL.Add(', ' + TMeuDB + '.condimov imv');
      kdObra: Query.SQL.Add(', ' + TMeuDB + '.obrascab obr');
    end;

    Query.SQL.Add('WHERE fo.Codigo=lct.Fornecedor AND cl.Codigo=lct.Cliente');
    Query.SQL.Add('AND lct.Genero<>-1');

    Query.SQL.Add(FSQLDepto_TXT);

    //  2010-08-02 - N�o usa mais Genero=-5 > Outras carteiras!
    Query.SQL.Add('AND ( lct.Tipo<>2 OR (lct.Tipo=2 AND Sit IN (0,1,5,6)) )');
    // fim 2010-08-02
    Query.SQL.Add('AND ca.ForneceI =' + FormatFloat('0', FEmpresa));

    //if RGTipo.ItemIndex in ([6,7]) then
    if CkEmissao.Checked then
      Query.SQL.Add('AND lct.Data BETWEEN "'+Ext_EmisI+'" AND "'+Ext_EmisF+'"');
    //else
    if CkVencto.Checked then
      Query.SQL.Add('AND lct.Vencimento BETWEEN "'+Ext_VctoI+'" AND "'+Ext_VctoF+'"');
    if CkDataDoc.Checked then
      Query.SQL.Add('AND lct.DataDoc BETWEEN "'+Ext_DocI+'" AND "'+Ext_DocF+'"');
    if CkDataComp.Checked then
    begin
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE IGNORE ' + FTabLctA + ' SET Compensado=Vencimento');
      Dmod.QrUpdU.SQL.Add('WHERE Tipo<>2');
      Dmod.QrUpdU.ExecSQL;
      Query.SQL.Add('AND lct.Compensado BETWEEN "'+Ext_CompI+'" AND "'+Ext_CompF+'"');
    end;
    if RGTipo.ItemIndex in ([2,3]) then
    begin
      Query.SQL.Add('AND lct.Tipo=2');
      Query.SQL.Add('AND lct.Sit IN (0,1,5,6)');
    end else
    if RGTipo.ItemIndex in ([5]) then
    begin
      //Query.SQL.Add('AND lct.Tipo=2');
      Query.SQL.Add('AND lct.Sit IN (2,3)');
    end;
    Query.SQL.Add('AND co.Codigo=lct.Genero');
    Query.SQL.Add('AND ca.Codigo=lct.Carteira');
    case VAR_KIND_DEPTO of
      kdUH: Query.SQL.Add('AND lct.Depto=imv.Conta');
      kdObra: Query.SQL.Add('AND lct.Depto=obr.Codigo');
    end;
    if RGTipo.ItemIndex in ([2,4,6]) then
    begin
      Query.SQL.Add('AND lct.Debito>0');
      if EdTerceiro.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Fornecedor ='+IntToStr(CBTerceiro.KeyValue));
    end else if RGTipo.ItemIndex in ([3,5,7]) then
    begin
      Query.SQL.Add('AND lct.Credito>0');
      if EdTerceiro.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Cliente='+IntToStr(CBTerceiro.KeyValue));
    end;
    if RGTipo.ItemIndex in ([3,7]) then
    begin
      if EdAccount.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Account ='+IntToStr(CBAccount.KeyValue));
      if EdVendedor.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Vendedor ='+IntToStr(CBVendedor.KeyValue));
    end;
    if CkNiveis.Checked then
    begin
      if RGTipo.ItemIndex in ([2,4,6]) then
      Query.SQL.Add('AND fo.Nivel BETWEEN "'+EdA.Text+'" AND "'+EdZ.Text+'"');
      if RGTipo.ItemIndex in ([3,5,7]) then
      Query.SQL.Add('AND cl.Nivel BETWEEN "'+EdA.Text+'" AND "'+EdZ.Text+'"');
    end;
    if CkOmiss.Checked then
    begin
      if CkAnos.Checked  then Anos :=  Geral.IMV(EdAnos.Text)  else Anos := 0;
      if CkMeses.Checked then Meses := Geral.IMV(EdMeses.Text) else Meses := 0;
      if CkDias.Checked  then Dias :=  Geral.IMV(EdDias.Text)  else Dias := 0;
      Ext_Vencto := IncMonth(Date, -1 * ((Anos*12)+Meses)) - Dias;
      Query.SQL.Add('AND lct.Vencimento >= "'+
      FormatDateTime(VAR_FORMATDATE, Ext_Vencto)+'"');
    end;
    if RGTipo.ItemIndex in ([8,9]) then
    begin
      if CkAnos2.Checked  then Anos :=  Geral.IMV(EdAnos2.Text)  else Anos := 0;
      if CkMeses2.Checked then Meses := Geral.IMV(EdMeses2.Text) else Meses := 0;
      if CkDias2.Checked  then Dias :=  Geral.IMV(EdDias2.Text)  else Dias := 0;
      Ext_Vencto := IncMonth(Date, -1 * ((Anos*12)+Meses)) - Dias;
      Query.SQL.Add('AND lct.Vencimento < "'+
      FormatDateTime(VAR_FORMATDATE, Ext_Vencto)+'"');
    end;
    //
    if RGTipo.ItemIndex in ([2,3,4,5,6,7,8,9]) then
    begin
      if EdConta.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Genero ='+IntToStr(CBConta.KeyValue));
    end;
    case Tipo of
      0: Query.SQL.Add('');
      1: Query.SQL.Add('AND ID_Pgto=0');
      2: Query.SQL.Add('AND ID_Pgto<>0');
      // Novo 2008.07.22
      3: Query.SQL.Add('AND Compensado>0');
    end;
  end;
begin
  Query.Close;
  Query.SQL.Clear;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_2;');
  Query.SQL.Add('CREATE TABLE _FIN_RELAT_005_2');
  Query.SQL.Add('');
  GeraParteSQL(FTabLctA);
  Query.SQL.Add('UNION');
  GeraParteSQL(FTabLctB);
  Query.SQL.Add('UNION');
  GeraParteSQL(FTabLctD);
  Query.SQL.Add(';');
  Query.SQL.Add('');
  Query.SQL.Add('SELECT * FROM _FIN_RELAT_005_2');
  Query.SQL.Add(SQL_Ordenar() + ';');
  Query.SQL.Add('');
  Query.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_2;');
  Query.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(Query, DModG.MyPID_DB);
end;

function TFmExtratos.SQL_Ordenar(): String;
begin
  case VAR_ORDENAR of
    00: Result := 'ORDER BY NOME_TERCEIRO, DataDoc, CtrlPai, Data, Vencimento, Controle';
    01: Result := 'ORDER BY NOME_TERCEIRO, DataDoc, CtrlPai, Data, Vencimento, Controle';
    02: Result := 'ORDER BY NOME_TERCEIRO, Data, CtrlPai, DataDoc, Vencimento, Controle';
    03: Result := 'ORDER BY NOME_TERCEIRO, Vencimento, CtrlPai, DataDoc, Data, Controle';
    04: Result := 'ORDER BY NOME_TERCEIRO, Documento, Vencimento, CtrlPai, DataDoc, Data, Controle';
    05: Result := 'ORDER BY NOME_TERCEIRO, NotaFiscal, Vencimento, CtrlPai, DataDoc, Data, Controle';
    06: Result := 'ORDER BY NOME_TERCEIRO, Duplicata, NotaFiscal, Vencimento, CtrlPai, DataDoc, Data, Controle';
    07: Result := 'ORDER BY NOME_TERCEIRO, NO_UH, Vencimento, CtrlPai, DataDoc, Data, Controle';
    //
    10: Result := 'ORDER BY DataDoc, NOME_TERCEIRO, CtrlPai, Data, Vencimento, Controle';
    11: Result := 'ORDER BY DataDoc, NOME_TERCEIRO, CtrlPai, Data, Vencimento, Controle';
    12: Result := 'ORDER BY DataDoc, Data, CtrlPai, NOME_TERCEIRO, Vencimento, Controle';
    13: Result := 'ORDER BY DataDoc, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    14: Result := 'ORDER BY DataDoc, Documento, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    15: Result := 'ORDER BY DataDoc, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    16: Result := 'ORDER BY DataDoc, Duplicata, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    17: Result := 'ORDER BY DataDoc, NO_UH, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    //
    20: Result := 'ORDER BY Data, NOME_TERCEIRO, CtrlPai, DataDoc, Vencimento, Controle';
    21: Result := 'ORDER BY Data, DataDoc, CtrlPai, NOME_TERCEIRO, Vencimento, Controle';
    22: Result := 'ORDER BY Data, NOME_TERCEIRO, CtrlPai, DataDoc, Vencimento, Controle';
    23: Result := 'ORDER BY Data, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    24: Result := 'ORDER BY Data, Documento, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    25: Result := 'ORDER BY Data, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    26: Result := 'ORDER BY Data, Duplicata, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    27: Result := 'ORDER BY Data, NO_UH, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    //
    30: Result := 'ORDER BY Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    31: Result := 'ORDER BY Vencimento, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    32: Result := 'ORDER BY Vencimento, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    33: Result := 'ORDER BY Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    34: Result := 'ORDER BY Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    35: Result := 'ORDER BY Vencimento, NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    36: Result := 'ORDER BY Vencimento, Duplicata,NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    37: Result := 'ORDER BY Vencimento, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    //
    40: Result := 'ORDER BY Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    41: Result := 'ORDER BY Documento, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    42: Result := 'ORDER BY Documento, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    43: Result := 'ORDER BY Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    44: Result := 'ORDER BY Documento, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    45: Result := 'ORDER BY Documento, NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    46: Result := 'ORDER BY Documento, Duplicata, NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    47: Result := 'ORDER BY Documento, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    //
    50: Result := 'ORDER BY NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    51: Result := 'ORDER BY NotaFiscal, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    52: Result := 'ORDER BY NotaFiscal, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    53: Result := 'ORDER BY NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    54: Result := 'ORDER BY NotaFiscal, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    55: Result := 'ORDER BY NotaFiscal, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    56: Result := 'ORDER BY NotaFiscal, Duplicata, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    57: Result := 'ORDER BY NotaFiscal, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    //
    60: Result := 'ORDER BY Duplicata, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    61: Result := 'ORDER BY Duplicata, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    62: Result := 'ORDER BY Duplicata, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    63: Result := 'ORDER BY Duplicata, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    64: Result := 'ORDER BY Duplicata, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    65: Result := 'ORDER BY Duplicata, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    66: Result := 'ORDER BY Duplicata, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    67: Result := 'ORDER BY Duplicata, NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    //
    70: Result := 'ORDER BY NO_UH, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    71: Result := 'ORDER BY NO_UH, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    72: Result := 'ORDER BY NO_UH, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    73: Result := 'ORDER BY NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    74: Result := 'ORDER BY NO_UH, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    75: Result := 'ORDER BY NO_UH, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    76: Result := 'ORDER BY NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    77: Result := 'ORDER BY NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    //
    else Result := '';
  end;
end;

procedure TFmExtratos.QrExtrCalcFields(DataSet: TDataSet);
begin
  QrExtrVALOR.Value := QrExtrCredi.Value - QrExtrDebit.Value;
end;

procedure TFmExtratos.QrFlxoCalcFields(DataSet: TDataSet);
begin
  if QrFlxoDescricao.Value = '' then
    QrFlxoDESCRI_TXT.Value := QrFlxoNOMECONTA.Value
  else
    QrFlxoDESCRI_TXT.Value := QrFlxoDescricao.Value;
end;

procedure TFmExtratos.QrPagRecCalcFields(DataSet: TDataSet);
var
  MoraDia: Double;
  SerDoc: String;
begin
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  if QrPagRecFornecedor.Value > 0 then
    QrPagRecTERCEIRO.Value := QrPagRecFornecedor.Value
  else
    QrPagRecTERCEIRO.Value := QrPagRecCliente.Value;
    // FIM 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  //
  QrPagRecVENCER.Value := QrPagRecVencimento.Value;
  QrPagRecVALOR.Value := QrPagRecCredito.Value - QrPagRecDebito.Value;
  ///////////////////////////////
  QrPagRecVENCIDO.Value := 0;
  if (QrPagRecVencimento.Value < Date) and (QrPagRecTipo.Value <> 1) then
    if QrPagRecSit.Value < 2 then
      QrPagRecVENCIDO.Value := QrPagRecVencimento.Value;
  ///////////////////////////////
  if QrPagRecVENCIDO.Value > 0 then
//  begin
  begin
     QrPagRecATRAZODD.Value := Trunc(Date) - QrPagRecVENCIDO.Value;
     QrPagRecMULTA_REAL.Value := QrPagRecMulta.Value / 100 * QrPagRecVALOR.Value;
  end else begin
     QrPagRecATRAZODD.Value := 0;
     QrPagRecMULTA_REAL.Value := 0;
  end;
  if (QrPagRecEhCRED.Value = 'V') and (QrPagRecEhDEB.Value = 'F') then
    MoraDia := QrPagRecMoraDia.Value
  else if (QrPagRecEhCRED.Value = 'F') and (QrPagRecEhDEB.Value = 'V') then
    MoraDia := QrPagRecMoraDia.Value else MoraDia := 0;
  QrPagRecATUALIZADO.Value := QrPagRecSALDO.Value + (QrPagRecSALDO.Value *
  (QrPagRecATRAZODD.Value / 30) * MoraDia / 100) + QrPagRecMULTA_REAL.Value;
  ///////////////////////////////
  if RGTipo.ItemIndex in [4,5] then
  begin
    if RGTipo.ItemIndex = 4 then //Contas pagas
      QrPagRecPAGO_REAL.Value := QrPagRecDebito.Value - QrPagRecMoraVal.Value -
                                   QrPagRecMultaVal.Value + QrPagRecDescoVal.Value
    else
      QrPagRecPAGO_REAL.Value := QrPagRecCredito.Value - QrPagRecMoraVal.Value -
                                   QrPagRecMultaVal.Value + QrPagRecDescoVal.Value
  end else
  begin
    if QrPagRecTipo.Value = 1 then
    begin
      QrPagRecPAGO_REAL.Value := QrPagRecVALOR.Value;
    end else if QrPagRecTipo.Value = 0 then
    begin
      if QrPagRecVENCIDO.Value > 0 then
      QrPagRecPAGO_REAL.Value := QrPagRecVALOR.Value
      else QrPagRecPAGO_REAL.Value := 0;
    end else
    begin
      QrPagRecPAGO_REAL.Value := QrPagRecPago.Value;
    end;
  end;
  ///////////////////////////////
  if QrPagRecVencimento.Value < Date then
    QrPagRecNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagRecVencimento.Value)
  else QrPagRecNOMEVENCIDO.Value := CO_VAZIO;
  //////////////////////////
  if QrPagRecDocumento.Value > 0 then
    SerDoc := FormatFloat('000000', QrPagRecDocumento.Value) else SerDoc := '';
  if Length(QrPagRecSerieCH.Value) > 0 then SerDoc := QrPagRecSerieCH.Value + SerDoc;
  QrPagRecSERIEDOC.Value := SerDoc;
  //
{
  if QrPagRecDescricao.Value = '' then
    QrPagRecDESCRI_TXT.Value := QrPagRecNOMECONTA.Value
  else
    QrPagRecDESCRI_TXT.Value := QrPagRecDescricao.Value;
}
end;

procedure TFmExtratos.CkOmissClick(Sender: TObject);
begin
  GBOmiss.Visible := CkOmiss.Checked;
end;

procedure TFmExtratos.BtSalvarClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Anos',  'Dermatek\Extratos\Venctos', EdAnos.Text,  ktString);
  Geral.WriteAppKeyCU('Meses', 'Dermatek\Extratos\Venctos', EdMeses.Text, ktString);
  Geral.WriteAppKeyCU('Dias',  'Dermatek\Extratos\Venctos', EdDias.Text,  ktString);
  /////
  Geral.WriteAppKeyCU('Ck_Anos',  'Dermatek\Extratos\Venctos', CkAnos.Checked,  ktBoolean);
  Geral.WriteAppKeyCU('Ck_Meses', 'Dermatek\Extratos\Venctos', CkMeses.Checked, ktBoolean);
  Geral.WriteAppKeyCU('Ck_Dias',  'Dermatek\Extratos\Venctos', CkDias.Checked,  ktBoolean);
end;

procedure TFmExtratos.BtGravarClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Anos',  'Dermatek\Extratos\Perdidos', EdAnos2.Text,  ktString);
  Geral.WriteAppKeyCU('Meses', 'Dermatek\Extratos\Perdidos', EdMeses2.Text, ktString);
  Geral.WriteAppKeyCU('Dias',  'Dermatek\Extratos\Perdidos', EdDias2.Text,  ktString);
  /////
  Geral.WriteAppKeyCU('Ck_Anos',  'Dermatek\Extratos\Perdidos', CkAnos2.Checked,  ktBoolean);
  Geral.WriteAppKeyCU('Ck_Meses', 'Dermatek\Extratos\Perdidos', CkMeses2.Checked, ktBoolean);
  Geral.WriteAppKeyCU('Ck_Dias',  'Dermatek\Extratos\Perdidos', CkDias2.Checked,  ktBoolean);
end;

procedure TFmExtratos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmExtratos.QrPagRecItsCalcFields(DataSet: TDataSet);
var
  MoraDia: Double;
begin
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  if QrPagRecItsFornecedor.Value > 0 then
    QrPagRecItsTERCEIRO.Value := QrPagRecItsFornecedor.Value
  else
    QrPagRecItsTERCEIRO.Value := QrPagRecItsCliente.Value;
    // fim 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  QrPagRecItsVENCER.Value := QrPagRecItsVencimento.Value;
  QrPagRecItsVALOR.Value := QrPagRecItsCredito.Value - QrPagRecItsDebito.Value;
  ///////////////////////////////
  QrPagRecItsVENCIDO.Value := 0;
  if (QrPagRecItsVencimento.Value < Date) and (QrPagRecItsTipo.Value <> 1) then
    if QrPagRecItsSit.Value < 2 then
      QrPagRecItsVENCIDO.Value := QrPagRecItsVencimento.Value;
  ///////////////////////////////
  if QrPagRecItsVENCIDO.Value > 0 then
//  begin
  begin
     QrPagRecItsATRAZODD.Value := Trunc(Date) - QrPagRecItsVENCIDO.Value;
     QrPagRecItsMULTA_REAL.Value := QrPagRecItsMulta.Value / 100 * QrPagRecItsVALOR.Value;
  end else begin
     QrPagRecItsATRAZODD.Value := 0;
     QrPagRecItsMULTA_REAL.Value := 0;
  end;
  if (QrPagRecItsEhCRED.Value = 'V') and (QrPagRecItsEhDEB.Value = 'F') then
    MoraDia := QrPagRecItsMoraDia.Value
  else if (QrPagRecItsEhCRED.Value = 'F') and (QrPagRecItsEhDEB.Value = 'V') then
    MoraDia := QrPagRecItsMoraDia.Value else MoraDia := 0;
  QrPagRecItsATUALIZADO.Value := QrPagRecItsSALDO.Value + (QrPagRecItsSALDO.Value *
  (QrPagRecItsATRAZODD.Value / 30) * MoraDia / 100) + QrPagRecItsMULTA_REAL.Value;
  ///////////////////////////////
  if QrPagRecItsTipo.Value = 1 then
  begin
    QrPagRecItsPAGO_REAL.Value := QrPagRecItsVALOR.Value;
  end else if QrPagRecItsTipo.Value = 0 then
  begin
    if QrPagRecItsVENCIDO.Value > 0 then
    QrPagRecItsPAGO_REAL.Value := QrPagRecItsVALOR.Value
    else QrPagRecItsPAGO_REAL.Value := 0;
  end else
  begin
    QrPagRecItsPAGO_REAL.Value := QrPagRecItsPago.Value;
  end;
  ///////////////////////////////
  if QrPagRecItsVencimento.Value < Date then
    QrPagRecItsNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagRecItsVencimento.Value)
  else QrPagRecItsNOMEVENCIDO.Value := CO_VAZIO;
  //////////////////////////
end;

procedure TFmExtratos.QrPagRec1CalcFields(DataSet: TDataSet);
var
  PagRec: String;
begin
  case Trunc(QrPagRec1Tipo.Value) of
    0: PagRec := ' Pago';
    1: PagRec := ' Depositado';
    2: PagRec := ' Rolado';
  end;
  PagRec := PagRec + ' ['+FormatFloat('000000', QrPagRec1ID_Pgto.Value)+ '] ';
  QrPagRec1PAGO_ROLADO.Value := PagRec;
  /////////////////////////////////////
  QrPagRec1DEVIDO.Value     := QrPagRec1ATUALIZADO.Value;
  QrPagRec1PEND_TOTAL.Value := QrPagRec1ATUALIZADO.Value;
end;

procedure TFmExtratos.CreateDockableWindows;
var
  I: Integer;
begin
  for I := 0 to High(DockWindows) do
  begin
    DockWindows[I] := TFmDockForm.Create(Application);
    DockWindows[I].Caption := 'C�lculos em Progresso';
//    DockWindows[I].Memo1.Color := Colors[I];
//    DockWindows[I].Memo1.Font.Color := Colors[I] xor $00FFFFFF;
//    DockWindows[I].Memo1.Text := ColStr[I] + ' window ';
  end;
end;

procedure TFmExtratos.EdEmpresaChange(Sender: TObject);
var
  Emp: Integer;
  Tab, Ano: String;
  Dta: TDateTime;
begin
  {
  2012-01-01 Redefinido para o fim do ano seguinte ao �ltimo la�amento!
  TPCre.Date := Int(Date);
  TPDeb.Date := Int(Date);
  }
  Emp := EdEmpresa.ValueVariant;
  if Emp <> 0 then
  begin
    Tab := DModG.NomeTab(TMeuDB, ntLct, False, ttA, EdEmpresa.ValueVariant);
    Dta := DModFin.DataUltimoLct(Tab);
    if Dta < 2 then
      Dta := Date;
    Ano := Geral.FDT(Dta, 25);
    Dta := EncodeDate(Geral.IMV(Ano) + 1, 12, 31);
    //
    TPCre.Date := Dta;
    TPDeb.Date := Dta;
    //
    TPEmissFim.Date := Dta;
    TPVctoFim.Date := Dta;
    //
  end;
  // Fim 2012-01-01
  //
end;

procedure TFmExtratos.QrPagRecXCalcFields(DataSet: TDataSet);
var
  MoraDia: Double;
begin
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  if QrPagRecXFornecedor.Value > 0 then
    QrPagRecXTERCEIRO.Value := QrPagRecXFornecedor.Value
  else
    QrPagRecXTERCEIRO.Value := QrPagRecXCliente.Value;
    // fim 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  QrPagRecXVENCER.Value := QrPagRecXVencimento.Value;
  QrPagRecXVALOR.Value := QrPagRecXCredito.Value - QrPagRecXDebito.Value;
  ///////////////////////////////
  QrPagRecXVENCIDO.Value := 0;
  if (QrPagRecXVencimento.Value < Date) and (QrPagRecXTipo.Value <> 1) then
    if QrPagRecXSit.Value < 2 then
      QrPagRecXVENCIDO.Value := QrPagRecXVencimento.Value;
  ///////////////////////////////
  if QrPagRecXVENCIDO.Value > 0 then
//  begin
  begin
     QrPagRecXATRAZODD.Value := Trunc(Date) - QrPagRecXVENCIDO.Value;
     QrPagRecXMULTA_REAL.Value := QrPagRecXMulta.Value / 100 * QrPagRecXVALOR.Value;
  end else begin
     QrPagRecXATRAZODD.Value := 0;
     QrPagRecXMULTA_REAL.Value := 0;
  end;
  if (QrPagRecXEhCRED.Value = 'V') and (QrPagRecXEhDEB.Value = 'F') then
    MoraDia := QrPagRecXMoraDia.Value
  else if (QrPagRecXEhCRED.Value = 'F') and (QrPagRecXEhDEB.Value = 'V') then
    MoraDia := QrPagRecXMoraDia.Value
  else if (QrPagRecXID_Pgto.Value <> QrPagRecXControle.Value) then
    MoraDia := QrPagRecXMoraDia.Value
  else MoraDia := 0;
  QrPagRecXATUALIZADO.Value := QrPagRecXSALDO.Value + (QrPagRecXSALDO.Value *
  (QrPagRecXATRAZODD.Value / 30) * MoraDia / 100) + QrPagRecXMULTA_REAL.Value;
  ///////////////////////////////
  if QrPagRecXTipo.Value = 1 then
  begin
    QrPagRecXPAGO_REAL.Value := QrPagRecXVALOR.Value;
  end else if QrPagRecXTipo.Value = 0 then
  begin
    if QrPagRecXVENCIDO.Value > 0 then
    QrPagRecXPAGO_REAL.Value := QrPagRecXVALOR.Value
    else QrPagRecXPAGO_REAL.Value := 0;
  end else
  begin
    if QrPagRecXCompensado.Value > 0 then
      QrPagRecXPAGO_REAL.Value := QrPagRecXVALOR.Value
    else QrPagRecXPAGO_REAL.Value := QrPagRecXPago.Value;
  end;
  QrPagRecXPENDENTE.Value := QrPagRecXVALOR.Value - QrPagRecXPAGO_REAL.Value;
  ///////////////////////////////
  if QrPagRecXVencimento.Value < Date then
    QrPagRecXNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagRecXVencimento.Value)
  else QrPagRecXNOMEVENCIDO.Value := CO_VAZIO;
  //////////////////////////
end;

procedure TFmExtratos.GeraRelatorio(Saida: TTipoSaidaRelatorio);
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  FEmpresa      := DModG.QrEmpresasCodigo.Value;
  FEntidade_TXT := FormatFloat('0', FEmpresa);
  if PnDepto.Visible then
  begin
    if CkDepto.Checked then
      FSQLDepto_TXT := 'AND lct.Depto=' + Geral.FF0(EdDepto.ValueVariant)
    else
      FSQLDepto_TXT := '';
  end
  else FSQLDepto_TXT := '';
  //
  VAR_PARAR := False;
  Ext_VALORA := 0;
  Ext_VALORB := 0;
  Ext_VALORC := 0;
  Ext_PENDENTEA := 0;
  Ext_PENDENTEB := 0;
  Ext_PENDENTEC := 0;
  Ext_ATUALIZADOA := 0;
  Ext_ATUALIZADOB := 0;
  Ext_ATUALIZADOC := 0;
  Ext_DEVIDOA := 0;
  Ext_DEVIDOB := 0;
  Ext_DEVIDOC := 0;
  Ext_PAGO_REALA := 0;
  Ext_PAGO_REALB := 0;
  Ext_PAGO_REALC := 0;
  if (RGOrdem1.Enabled) and (RGOrdem1.ItemIndex = -1) then
  begin
    Geral.MB_Aviso('Defina a "Ordem 1"!');
    Exit;
  end;
  if (RGOrdem2.Enabled) and (RGOrdem2.ItemIndex = -1) then
  begin
    Geral.MB_Aviso('Defina uma "Ordem 2"!');
    Exit;
  end;
  dmkPF.ExcluiSQLsDermatek();
  if TPEmissFim.Date < TPEmissIni.Date then
  begin
    Geral.MB_Aviso('"Data de emiss�o" final menor que inicial!');
    Exit;
  end;
  if TPVctoFim.Date < TPVctoIni.Date then
  begin
    Geral.MB_Aviso('"Data de vencimento" final menor que inicial!');
    Exit;
  end;
  if TPDocFim.Date < TPDocIni.Date then
  begin
    Geral.MB_Aviso('"Data do documento" final menor que inicial!');
    Exit;
  end;
{ desmarcado 2012-11-21
  if (EdTerceiro.ValueVariant <> 0) then
  begin
    Geral.MB_Aviso('Fornecedor/Cliente sem nome. Tecle "DEL" para limpar.');
    EdTerceiro.SetFocus;
    Exit;
  end;
}
  Ext_Saldo := 0;
  Ext_EmisI := Geral.FDT(dmkPF.EscolhaDe2Dta(CkEmissao.Checked, TPEmissIni.Date, 0), 1);
  Ext_EmisF := Geral.FDT(dmkPF.EscolhaDe2Dta(CkEmissao.Checked, TPEmissFim.Date, Date), 1) + ' 23:59:59';
  Ext_VctoI := Geral.FDT(dmkPF.EscolhaDe2Dta(CkVencto.Checked, TPVctoIni.Date, 0), 1);
  Ext_VctoF := Geral.FDT(dmkPF.EscolhaDe2Dta(CkVencto.Checked, TPVctoFim.Date, Date), 1) + ' 23:59:59';
  Ext_CompI := Geral.FDT(dmkPF.EscolhaDe2Dta(CkDataComp.Checked, TPCompIni.Date, 0), 1);
  Ext_CompF := Geral.FDT(dmkPF.EscolhaDe2Dta(CkDataComp.Checked, TPCompFim.Date, Date), 1) + ' 23:59:59';
  Ext_DocI  := Geral.FDT(dmkPF.EscolhaDe2Dta(CkDataDoc.Checked, TPDocIni.Date, 0), 1);
  Ext_DocF  := Geral.FDT(dmkPF.EscolhaDe2Dta(CkDataDoc.Checked, TPDocFim.Date, Date), 1) + ' 23:59:59';
{
  Ext_DataH := Geral.FDT(dmkPF.EscolhaDe2Dta(Ck.Checked, Date);
  Ext_DataA := Geral.FDT(dmkPF.EscolhaDe2Dta(Ck.Checked, TPEmissIni.Date-1);
}
  //
{ N�o precisa, � definido antes!
  DModG.ReopenDtEncerMorto(DModG.QrEmpresasFilial.Value);
  FDtEncer := DModG.QrLastEncerData.Value;
  FDtMorto := DModG.QrLastMortoData.Value;
  FTabLctA := DModG.NomeTab(ntLct, False, ttA, FEmpresa);
  FTabLctB := DModG.NomeTab(ntLct, False, ttB, FEmpresa);
  FTabLctD := DModG.NomeTab(ntLct, False, ttD, FEmpresa);
}  //
  case RGTipo.ItemIndex of
    0: R00_ImprimeExtratoConsolidado();
    1: R01_ImprimeFluxoDeCaixa();
    2: R02_ImprimeContasAPagar(Self, Saida);
    3: R02_ImprimeContasAPagar(Self, Saida);
    4: R02_ImprimeContasAPagar(Self, Saida);
    5: R02_ImprimeContasAPagar(Self, Saida);
    6: R02_ImprimeContasAPagar(Self, Saida);
    7: R02_ImprimeContasAPagar(Self, Saida);
    8: R02_ImprimeContasAPagar(Self, Saida);
    9: R02_ImprimeContasAPagar(Self, Saida);
    else Geral.MB_Erro('Relat�rio n�o implementado');
  end;
end;

procedure TFmExtratos.QrTerceirosCalcFields(DataSet: TDataSet);
begin
  if QrTerceirosTel1.Value = '' then QrTerceirosTEL1_TXT.Value := '' else
     QrTerceirosTEL1_TXT.Value := '[ ' + Geral.FormataTelefone_TT(
     QrTerceirosTel1.Value) + ' ]';
  //
  if QrTerceirosTel2.Value = '' then QrTerceirosTEL2_TXT.Value := '' else
     QrTerceirosTEL2_TXT.Value := '[ ' + Geral.FormataTelefone_TT(
     QrTerceirosTel2.Value) + ' ]';
  //
  if QrTerceirosTel3.Value = '' then QrTerceirosTEL3_TXT.Value := '' else
     QrTerceirosTEL3_TXT.Value := '[ ' + Geral.FormataTelefone_TT(
     QrTerceirosTel3.Value) + ' ]';
  //
  QrTerceirosTELEFONES.Value := QrTerceirosTEL1_TXT.Value +
     QrTerceirosTEL2_TXT.Value + QrTerceirosTEL3_TXT.Value;
end;

procedure TFmExtratos.frxFin_Relat_005_02_B1GetValue(const VarName: String;
  var Value: Variant);
var
  Liga: String;
begin
  if VarName = 'REPRESENTANTE' then
  begin
    if EdAccount.ValueVariant = 0  then
      Value := 'TODOS'
    else
      Value := CBAccount.Text;
  end else if VarName = 'CLI_INT' then
    Value := CBEmpresa.Text
  else if VarName = 'VENDEDOR' then
  begin
    if EdVendedor.ValueVariant = 0 then Value := 'TODOS'
    else Value := CBVendedor.Text;
  end else if VarName = 'NOME_CONTA' then
  begin
    if EdConta.ValueVariant = 0 then Value := 'TODAS'
    else Value := CBConta.Text;
  end;
  ////////////////////////////////////////
  if VarName = 'VALOR_A' then Value := Ext_VALORA;
  if VarName = 'VALOR_B' then Value := Ext_VALORB;
  if VarName = 'VALOR_C' then Value := Ext_VALORC / 2; // two pass
  ///////////////////////////////////////
  if VarName = 'PAGO_REAL_A' then Value := Ext_PAGO_REALA;
  if VarName = 'PAGO_REAL_B' then Value := Ext_PAGO_REALB;
  if VarName = 'PAGO_REAL_C' then Value := Ext_PAGO_REALC / 2;
  ///////////////////////////////////////
  if VarName = 'ATUALIZADO_A' then Value := Ext_ATUALIZADOA;
  if VarName = 'ATUALIZADO_B' then Value := Ext_ATUALIZADOB;
  if VarName = 'ATUALIZADO_C' then Value := Ext_ATUALIZADOC / 2;
  ///////////////////////////////////////
  if VarName = 'DEVIDO_A' then Value := Ext_DEVIDOA;
  if VarName = 'DEVIDO_B' then Value := Ext_DEVIDOB;
  if VarName = 'DEVIDO_C' then Value := Ext_DEVIDOC / 2;
  ///////////////////////////////////////
  if VarName = 'PENDENTE_A' then Value := Ext_PENDENTEA;
  if VarName = 'PENDENTE_B' then Value := Ext_PENDENTEB;
  if VarName = 'PENDENTE_C' then Value := Ext_PENDENTEC / 2;
  ///////////////////////////////////////

  if VarName = 'GRUPO1' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRecNO_UH.Value;
    end;
  end;
  if VarName = 'GRUPO2' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRecNO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOA' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOB' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOC' then
  begin
    Value := 'geral';
  end;
  if VarName = 'OMITIDOS' then
  begin
    if CkOmiss.Checked then Value := 'Omitidos valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else if RGTipo.ItemIndex in ([8,9]) then Value :=
      'Somente valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else Value := '  ';
  end;
  if VarName = 'NIVEIS' then
  begin
    if CkNiveis.Checked then Value := 'N�veis '+EdA.Text+' a '+EdZ.Text
    else Value := '  ';
  end;
  if VarName = 'TERCEIRO' then
  begin
    if RGTipo.ItemIndex = 2 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 3 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 4 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 5 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 6 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 7 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 8 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 9 then Value := 'Cliente: ';
  end;
{
  if VarName = 'INICIAL' then
  begin
    Value := Ext_SdIni;
    Ext_Saldo := Ext_SdIni
  end else
}
  if VarName = 'NOMEREL' then
  begin
    case RGTipo.ItemIndex of
      2: Value := 'RELAT�RIO DE CONTAS A PAGAR';
      3: Value := 'RELAT�RIO DE CONTAS A RECEBER';
      4: Value := 'RELAT�RIO DE CONTAS PAGAS';
      5: Value := 'RELAT�RIO DE CONTAS RECEBIDAS';
      6: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A PAGAR';
      7: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A RECEBER';
      8: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A PAGAR';
      9: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A RECEBER';
    end;
  end;

  if VarName = 'SALDO' then
  begin
    Value := QrExtratoCredito.Value - QrExtratoDebito.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'SALDO2' then
  begin
    Value := 0(*QrFluxoSALDO.Value*);
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'SALDO4' then
  begin
    Value := QrPagRecSALDO.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'FORNECEDOR' then
    if EdTerceiro.ValueVariant = 0 then Value := 'TODOS' else
    Value := IntToStr(CBTerceiro.KeyValue)+' - '+CBTerceiro.Text + ' ' +
    QrTerceirosTELEFONES.Text;
  if VarName = 'SALDODIA' then
    Value := Ext_Saldo;
  if VarName = 'PERIODO' then
  begin
    Value := '';
    if not CkEmissao.Checked and not CkVencto.Checked
    and not CkDataDoc.Checked then Value := 'N�o definido';
    if CkEmissao.Checked and CkVencto.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkEmissao.Checked then
      Value := 'Emiss�o de '+
        FormatDateTime(VAR_FORMATDATE3, TPEmissIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPEmissFim.Date) + Liga;
    if (CkEmissao.Checked or CkVencto.Checked) and CkDataDoc.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkVencto.Checked then
      Value := Value + 'Vencimento de '+
        FormatDateTime(VAR_FORMATDATE3, TPVctoIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPVctoFim.Date) + Liga;
    if CkDataDoc.Checked then
      Value := Value + 'Datadoc de '+
        FormatDateTime(VAR_FORMATDATE3, TPDocIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPDocFim.Date);
  end

  // user function

  else if VarName = 'VFR_CODITION_A' then
  begin
    case RGOrdem1.ItemIndex of
      00: Value := 'frxDsPagRec1."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec1."DataDoc"';
      02: Value := 'frxDsPagRec1."Data"';
      03: Value := 'frxDsPagRec1."Vencimento"';
      04: Value := 'frxDsPagRec1."Documento"';
      05: Value := 'frxDsPagRec1."NotaFiscal"';
      06: Value := 'frxDsPagRec1."Duplicata"';
      07: Value := 'frxDsPagRec1."NO_UH"';
    end;
  end
  else
  if VarName = 'VFR_CODITION_B' then
  begin
    case RGOrdem2.ItemIndex of
      00: Value := 'frxDsPagRec1."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec1."DataDoc"';
      02: Value := 'frxDsPagRec1."Data"';
      03: Value := 'frxDsPagRec1."Vencimento"';
      04: Value := 'frxDsPagRec1."Documento"';
      05: Value := 'frxDsPagRec1."NotaFiscal"';
      06: Value := 'frxDsPagRec1."Duplicata"';
      07: Value := 'frxDsPagRec1."NO_UH"';
    end;
  end else if VarName = 'VFR_CODITION_A1' then
  begin
    case RGOrdem1.ItemIndex of
      00: Value := 'frxDsPagRec."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec."DataDoc"';
      02: Value := 'frxDsPagRec."Data"';
      03: Value := 'frxDsPagRec."Vencimento"';
      04: Value := 'frxDsPagRec."Documento"';
      05: Value := 'frxDsPagRec."NotaFiscal"';
      06: Value := 'frxDsPagRec."Duplicata"';
      07: Value := 'frxDsPagRec."NO_UH"';
    end;
  end
  else
  if VarName = 'VFR_CODITION_B1' then
  begin
    case RGOrdem2.ItemIndex of
      00: Value := 'frxDsPagRec."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec."DataDoc"';
      02: Value := 'frxDsPagRec."Data"';
      03: Value := 'frxDsPagRec."Vencimento"';
      04: Value := 'frxDsPagRec."Documento"';
      05: Value := 'frxDsPagRec."NotaFiscal"';
      06: Value := 'frxDsPagRec."Duplicata"';
      07: Value := 'frxDsPagRec."NO_UH"';
    end;
  end;
  if VarName = 'VFR_ORD' then if EdTerceiro.ValueVariant = 0 then
  Value := False else Value := True;
  if VarName = 'VARF_GRADE' then Value := CkGrade.Checked;
  if VarName = 'VFR_GRUPO1' then
  begin
    if CkOrdem1.Checked then Value := RGOrdem1.ItemIndex else Value := -1;
  end;
  if VarName = 'VFR_GRUPO2' then
  begin
    if CkOrdem2.Checked then Value := RGOrdem2.ItemIndex else Value := -1;
  end;
  if VarName = 'VFR_GRUPOA' then
  begin
    if CkOrdem1.Checked then Value := RGOrdem1.ItemIndex else Value := -1;
    Ext_VALORA := 0;
    Ext_DEVIDOA := 0;
    Ext_PENDENTEA := 0;
    Ext_PAGO_REALA := 0;
    Ext_ATUALIZADOA := 0;
  end;
  if VarName = 'VFR_GRUPOB' then
  begin
    if CkOrdem2.Checked then Value := RGOrdem2.ItemIndex else Value := -1;
    Ext_VALORB := 0;
    Ext_DEVIDOB := 0;
    Ext_PENDENTEB := 0;
    Ext_PAGO_REALB := 0;
    Ext_ATUALIZADOB := 0;
  end;
  (*if VarName = 'VFR_GRUPOC' then
  begin
    Value := 0;
    Ext_ValueORC := 0;
    Ext_DEVIDOC := 0;
    Ext_PENDENTEC := 0;
    Ext_PAGO_REALC := 0;
    Ext_ATUALIZADOC := 0;
  end; *)
  if VarName = 'VFR_ID_PAGTO' then
  begin
    if QrPagRec1CtrlPai.Value = QrPagRec1Controle.Value then
    begin
      Value := 0;
      Ext_VALORA := Ext_VALORA + QrPagRec1VALOR.Value;
      Ext_VALORB := Ext_VALORB + QrPagRec1VALOR.Value;
      Ext_VALORC := Ext_VALORC + QrPagRec1VALOR.Value;
      /////
      Ext_PAGO_REALA := Ext_PAGO_REALA + QrPagRec1PAGO_REAL.Value;
      Ext_PAGO_REALB := Ext_PAGO_REALB + QrPagRec1PAGO_REAL.Value;
      Ext_PAGO_REALC := Ext_PAGO_REALC + QrPagRec1PAGO_REAL.Value;
      /////
      Ext_ATUALIZADOA := Ext_ATUALIZADOA + QrPagRec1ATUALIZADO.Value;
      Ext_ATUALIZADOB := Ext_ATUALIZADOB + QrPagRec1ATUALIZADO.Value;
      Ext_ATUALIZADOC := Ext_ATUALIZADOC + QrPagRec1ATUALIZADO.Value;
      /////
      Ext_DEVIDOA := Ext_DEVIDOA + QrPagRec1DEVIDO.Value;
      Ext_DEVIDOB := Ext_DEVIDOB + QrPagRec1DEVIDO.Value;
      Ext_DEVIDOC := Ext_DEVIDOC + QrPagRec1DEVIDO.Value;
      /////
    end else Value := 1;
    Ext_PENDENTEA := Ext_PENDENTEA + QrPagRec1PEND_TOTAL.Value;
    Ext_PENDENTEB := Ext_PENDENTEB + QrPagRec1PEND_TOTAL.Value;
    Ext_PENDENTEC := Ext_PENDENTEC + QrPagRec1PEND_TOTAL.Value;
  end else
  if VarName = 'VAR_NOMEEMPRESA' then
  begin
    if CkDepto.Checked then
    begin
      Value := CkDepto.Caption + ' ' + EdDepto.Text + ' - ' + CBDepto.Text;
    end else
      Value := CBEmpresa.Text
  end
  else
  if VarName = 'VAR_UH' then
      Value := dmkPF.TxtUH()
  else
end;

procedure TFmExtratos.frxFin_Relat_005_00GetValue(const VarName: String;
  var Value: Variant);
var
  Liga: String;
begin
  if VarName = 'SUBST' then
  begin
    if CkSubstituir.Checked then
      Value := 'Cliente / Fornecedor'
    else
      Value := 'Descri��o';
  end else
  if VarName = 'REPRESENTANTE' then
  begin
    if EdAccount.ValueVariant = 0 then Value := 'TODOS'
    else Value := CBAccount.Text;
  end else if VarName = 'CLI_INT' then
    Value := CBEmpresa.Text
  else if VarName = 'VENDEDOR' then
  begin
    if EdVendedor.ValueVariant = 0 then Value := 'TODOS'
    else Value := CBVendedor.Text;
  end else if VarName = 'NOME_CONTA' then
  begin
    if EdConta.ValueVariant = 0 then Value := 'TODAS'
    else Value := CBConta.Text;
  end else
  ////////////////////////////////////////
  if VarName = 'VALOR_A' then Value := Ext_VALORA else
  if VarName = 'VALOR_B' then Value := Ext_VALORB else
  if VarName = 'VALOR_C' then Value := Ext_VALORC / 2 else
  ///////////////////////////////////////
  if VarName = 'PAGO_REAL_A' then Value := Ext_PAGO_REALA else
  if VarName = 'PAGO_REAL_B' then Value := Ext_PAGO_REALB else
  if VarName = 'PAGO_REAL_C' then Value := Ext_PAGO_REALC / 2 else
  ///////////////////////////////////////
  if VarName = 'ATUALIZADO_A' then Value := Ext_ATUALIZADOA else
  if VarName = 'ATUALIZADO_B' then Value := Ext_ATUALIZADOB else
  if VarName = 'ATUALIZADO_C' then Value := Ext_ATUALIZADOC / 2 else
  ///////////////////////////////////////
  if VarName = 'DEVIDO_A' then Value := Ext_DEVIDOA else
  if VarName = 'DEVIDO_B' then Value := Ext_DEVIDOB else
  if VarName = 'DEVIDO_C' then Value := Ext_DEVIDOC / 2 else
  ///////////////////////////////////////
  if VarName = 'PENDENTE_A' then Value := Ext_PENDENTEA else
  if VarName = 'PENDENTE_B' then Value := Ext_PENDENTEB else
  if VarName = 'PENDENTE_C' then Value := Ext_PENDENTEC / 2 else
  ///////////////////////////////////////

  if VarName = 'GRUPO1' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRecNO_UH.Value;
    end;
  end else
  if VarName = 'GRUPO2' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRecNO_UH.Value;
    end;
  end else
  if VarName = 'GRUPOA' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
    end;
  end else
  if VarName = 'GRUPOB' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
    end;
  end else
  if VarName = 'GRUPOC' then
    Value := 'geral'
  else
  if VarName = 'OMITIDOS' then
  begin
    if CkOmiss.Checked then Value := 'Omitidos valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else if RGTipo.ItemIndex in ([8,9]) then Value :=
      'Somente valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else Value := '  ';
  end else
  if VarName = 'NIVEIS' then
  begin
    if CkNiveis.Checked then Value := 'N�veis '+EdA.Text+' a '+EdZ.Text
    else Value := '  ';
  end else
  if VarName = 'TERCEIRO' then
  begin
    if RGTipo.ItemIndex = 2 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 3 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 4 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 5 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 6 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 7 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 8 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 9 then Value := 'Cliente: ';
  end else
  if VarName = 'INICIAL' then
  begin
    Value     := Ext_SdIni;
    Ext_Saldo := Ext_SdIni;
  end else
  if VarName = 'NOMEREL' then
  begin
    case RGTipo.ItemIndex of
      1: Value := 'RELAT�RIO FLUXO DE CAIXA';
      2: Value := 'RELAT�RIO DE CONTAS A PAGAR';
      3: Value := 'RELAT�RIO DE CONTAS A RECEBER';
      4: Value := 'RELAT�RIO DE CONTAS PAGAS';
      5: Value := 'RELAT�RIO DE CONTAS RECEBIDAS';
      6: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A PAGAR';
      7: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A RECEBER';
      8: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A PAGAR';
      9: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A RECEBER';
    end;
  end else

  if VarName = 'SALDO' then
  begin
    Value := QrExtratoCredito.Value - QrExtratoDebito.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end else
  if VarName = 'SALDO2' then
  begin
    Value := 0(*QrFluxoSALDO.Value*);
    Ext_Saldo := Ext_Saldo + Value;
  end else
  if VarName = 'SALDO4' then
  begin
    Value := QrPagRecSALDO.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end else
  if VarName = 'FORNECEDOR' then
  begin
    if EdTerceiro.ValueVariant = 0 then Value := 'TODOS' else
    Value := IntToStr(CBTerceiro.KeyValue)+' - '+CBTerceiro.Text + ' ' +
    QrTerceirosTELEFONES.Text;
  end else
  if VarName = 'SALDODIA' then
    Value := Ext_Saldo else

  if VarName = 'PERIODO' then
  begin
    Value := '';
    if not CkEmissao.Checked and not CkVencto.Checked
    and not CkDataDoc.Checked then Value := 'N�o definido';
    if CkEmissao.Checked and CkVencto.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkEmissao.Checked then
      Value := 'Emiss�o de '+
        FormatDateTime(VAR_FORMATDATE3, TPEmissIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPEmissFim.Date) + Liga;
    if (CkEmissao.Checked or CkVencto.Checked) and CkDataDoc.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkVencto.Checked then
      Value := Value + 'Vencimento de '+
        FormatDateTime(VAR_FORMATDATE3, TPVctoIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPVctoFim.Date) + Liga;
    if CkDataDoc.Checked then
      Value := Value + 'Datadoc de '+
        FormatDateTime(VAR_FORMATDATE3, TPDocIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPDocFim.Date);
  end else
  if VarName = 'VAR_NOMEEMPRESA' then
  begin
    if CkDepto.Checked then
    begin
      Value := CkDepto.Caption + ' ' + EdDepto.Text + ' - ' + CBDepto.Text;
    end else
      Value := CBEmpresa.Text
  end
  else
    //Geral.MB_Erro('Vari�vel n�o definida! "' + VarName + '"');
end;

end.

