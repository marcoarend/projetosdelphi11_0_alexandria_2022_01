unit Resmes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, StdCtrls, Buttons, UnInternalConsts,
  Db, mySQLDbTables, ComCtrls, Menus, UnDmkProcFunc,
  frxClass, frxDBSet, frxChart, VCLTee.Chart, VCLTee.Series,
  dmkGeral, UnMyObjects, UnFinanceiro, dmkImage, UnDmkEnums;

type
  TFmResMes = class(TForm)
    Panel2: TPanel;
    QrRes: TmySQLQuery;
    QrResNOMESUBGRUPO: TWideStringField;
    QrResSubGrupo: TIntegerField;
    QrResNOMECONTA: TWideStringField;
    QrResGenero: TIntegerField;
    QrResValor: TFloatField;
    QrResTipoAgrupa: TIntegerField;
    QrResOrdemLista: TIntegerField;
    QrAtiCts: TmySQLQuery;
    QrAtiCtsNOMESUBGRUPO: TWideStringField;
    QrAtiCtsTipoAgrupa: TIntegerField;
    QrAtiCtsOrdemLista: TIntegerField;
    QrAtiCtsSubGrupo: TIntegerField;
    QrAtiCtsNOMECONTA: TWideStringField;
    QrAtiCtsGenero: TIntegerField;
    QrResmes: TmySQLQuery;
    QrResmesTipoAgrupa: TIntegerField;
    QrResmesOrdemLista: TIntegerField;
    QrResmesSubGrupo: TIntegerField;
    QrResmesNomeSubGrupo: TWideStringField;
    QrResmesGenero: TIntegerField;
    QrResmesNomeConta: TWideStringField;
    QrResmesMes01: TFloatField;
    QrResmesMes02: TFloatField;
    QrResmesMes03: TFloatField;
    QrResmesMes04: TFloatField;
    QrResmesMes05: TFloatField;
    QrResmesMes06: TFloatField;
    QrResmesMes07: TFloatField;
    QrResmesMes08: TFloatField;
    QrResmesMes09: TFloatField;
    QrResmesMes10: TFloatField;
    QrResmesMes11: TFloatField;
    QrResmesMes12: TFloatField;
    QrResmesSubOrdem: TIntegerField;
    QrAnt: TmySQLQuery;
    QrAntNOMESUBGRUPO: TWideStringField;
    QrAntTipoAgrupa: TIntegerField;
    QrAntOrdemLista: TIntegerField;
    QrAntSubGrupo: TIntegerField;
    QrAntNOMECONTA: TWideStringField;
    QrAntGenero: TIntegerField;
    QrAntValor: TFloatField;
    QrResmesAnoAn: TFloatField;
    QrResmesMESTO: TFloatField;
    QrT01: TmySQLQuery;
    QrT01Debito: TFloatField;
    QrT01Credito: TFloatField;
    QrT01Saldo: TFloatField;
    QrT02: TmySQLQuery;
    QrT02Debito: TFloatField;
    QrT02Credito: TFloatField;
    QrT02Saldo: TFloatField;
    QrT00: TmySQLQuery;
    QrT03: TmySQLQuery;
    QrT04: TmySQLQuery;
    QrT05: TmySQLQuery;
    QrT06: TmySQLQuery;
    QrT07: TmySQLQuery;
    QrT08: TmySQLQuery;
    QrT09: TmySQLQuery;
    QrT10: TmySQLQuery;
    QrT11: TmySQLQuery;
    QrT12: TmySQLQuery;
    QrA00: TmySQLQuery;
    QrA00Debito: TFloatField;
    QrA00Credito: TFloatField;
    QrA00Saldo: TFloatField;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    QrInicial: TmySQLQuery;
    QrInicialSaldo: TFloatField;
    QrT00SDO_PER: TFloatField;
    QrT00Debito: TFloatField;
    QrT00Credito: TFloatField;
    QrT00Saldo: TFloatField;
    QrT01SDO_PER: TFloatField;
    QrT02SDO_PER: TFloatField;
    QrT03SDO_PER: TFloatField;
    QrT04SDO_PER: TFloatField;
    QrT05SDO_PER: TFloatField;
    QrT06SDO_PER: TFloatField;
    QrT07SDO_PER: TFloatField;
    QrT08SDO_PER: TFloatField;
    QrT09SDO_PER: TFloatField;
    QrT10SDO_PER: TFloatField;
    QrT11SDO_PER: TFloatField;
    QrT12SDO_PER: TFloatField;
    QrT03Debito: TFloatField;
    QrT03Credito: TFloatField;
    QrT03Saldo: TFloatField;
    QrT05Debito: TFloatField;
    QrT05Credito: TFloatField;
    QrT05Saldo: TFloatField;
    QrT06Debito: TFloatField;
    QrT06Credito: TFloatField;
    QrT06Saldo: TFloatField;
    QrT07Debito: TFloatField;
    QrT07Credito: TFloatField;
    QrT07Saldo: TFloatField;
    QrT08Debito: TFloatField;
    QrT08Credito: TFloatField;
    QrT08Saldo: TFloatField;
    QrT09Debito: TFloatField;
    QrT09Credito: TFloatField;
    QrT09Saldo: TFloatField;
    QrT10Debito: TFloatField;
    QrT10Credito: TFloatField;
    QrT10Saldo: TFloatField;
    QrT11Debito: TFloatField;
    QrT11Credito: TFloatField;
    QrT11Saldo: TFloatField;
    QrT12Debito: TFloatField;
    QrT12Credito: TFloatField;
    QrT12Saldo: TFloatField;
    QrT04Debito: TFloatField;
    QrT04Credito: TFloatField;
    QrT04Saldo: TFloatField;
    QrSubGrupos: TmySQLQuery;
    QrSubGruposSubGrupo: TIntegerField;
    QrBef: TmySQLQuery;
    QrSum: TmySQLQuery;
    QrSumAnoAn: TFloatField;
    QrSumMes01: TFloatField;
    QrSumMes02: TFloatField;
    QrSumMes03: TFloatField;
    QrSumMes04: TFloatField;
    QrSumMes05: TFloatField;
    QrSumMes06: TFloatField;
    QrSumMes07: TFloatField;
    QrSumMes08: TFloatField;
    QrSumMes09: TFloatField;
    QrSumMes10: TFloatField;
    QrSumMes11: TFloatField;
    QrSumMes12: TFloatField;
    QrBefDebito: TFloatField;
    QrBefCredito: TFloatField;
    QrBefSaldo: TFloatField;
    QrT12TDebito: TFloatField;
    QrT12TCredito: TFloatField;
    QrT12TSaldo: TFloatField;
    TbResMes: TmySQLTable;
    TbResMesTipoAgrupa: TIntegerField;
    TbResMesOrdemLista: TIntegerField;
    TbResMesSubOrdem: TIntegerField;
    TbResMesSubGrupo: TIntegerField;
    TbResMesNomeSubGrupo: TWideStringField;
    TbResMesGenero: TIntegerField;
    TbResMesNomeConta: TWideStringField;
    TbResMesAnoAn: TFloatField;
    TbResMesMes01: TFloatField;
    TbResMesMes02: TFloatField;
    TbResMesMes03: TFloatField;
    TbResMesMes04: TFloatField;
    TbResMesMes05: TFloatField;
    TbResMesMes06: TFloatField;
    TbResMesMes07: TFloatField;
    TbResMesMes08: TFloatField;
    TbResMesMes09: TFloatField;
    TbResMesMes10: TFloatField;
    TbResMesMes11: TFloatField;
    TbResMesMes12: TFloatField;
    TbResMesPendencias: TFloatField;
    QrResmesPendencias: TFloatField;
    QrCtasResMes: TmySQLQuery;
    QrCtasResMesConta: TIntegerField;
    QrCtasResMesNome: TWideStringField;
    QrCtasResMesPeriodo: TIntegerField;
    QrCtasResMesTipo: TIntegerField;
    QrCtasResMesFator: TFloatField;
    QrCtasResMesValFator: TFloatField;
    QrCtasResMesDevido: TFloatField;
    QrCtasResMesPago: TFloatField;
    QrCtasResMesDiferenca: TFloatField;
    QrCtasResMesAcumulado: TFloatField;
    PMImprime: TPopupMenu;
    Resultadogeral1: TMenuItem;
    Contascontroladas1: TMenuItem;
    QrCtasResMesNOME_PERIODO: TWideStringField;
    QrAtiCtsExclusivo: TWideStringField;
    QrResmesExclusivo: TWideStringField;
    Tudojunto1: TMenuItem;
    Separarexclusivo1: TMenuItem;
    QrResMesV: TmySQLQuery;
    QrResMesF: TmySQLQuery;
    QrResMesFExclusivo: TWideStringField;
    QrResMesFTipoAgrupa: TIntegerField;
    QrResMesFOrdemLista: TIntegerField;
    QrResMesFSubOrdem: TIntegerField;
    QrResMesFSubGrupo: TIntegerField;
    QrResMesFNomeSubGrupo: TWideStringField;
    QrResMesFGenero: TIntegerField;
    QrResMesFNomeConta: TWideStringField;
    QrResMesFAnoAn: TFloatField;
    QrResMesFMes01: TFloatField;
    QrResMesFMes02: TFloatField;
    QrResMesFMes03: TFloatField;
    QrResMesFMes04: TFloatField;
    QrResMesFMes05: TFloatField;
    QrResMesFMes06: TFloatField;
    QrResMesFMes07: TFloatField;
    QrResMesFMes08: TFloatField;
    QrResMesFMes09: TFloatField;
    QrResMesFMes10: TFloatField;
    QrResMesFMes11: TFloatField;
    QrResMesFMes12: TFloatField;
    QrResMesFPendencias: TFloatField;
    QrResMesFMESTO: TFloatField;
    QrResMesFAnoAt: TFloatField;
    QrResMesVExclusivo: TWideStringField;
    QrResMesVTipoAgrupa: TIntegerField;
    QrResMesVOrdemLista: TIntegerField;
    QrResMesVSubOrdem: TIntegerField;
    QrResMesVSubGrupo: TIntegerField;
    QrResMesVNomeSubGrupo: TWideStringField;
    QrResMesVGenero: TIntegerField;
    QrResMesVNomeConta: TWideStringField;
    QrResMesVAnoAn: TFloatField;
    QrResMesVMes01: TFloatField;
    QrResMesVMes02: TFloatField;
    QrResMesVMes03: TFloatField;
    QrResMesVMes04: TFloatField;
    QrResMesVMes05: TFloatField;
    QrResMesVMes06: TFloatField;
    QrResMesVMes07: TFloatField;
    QrResMesVMes08: TFloatField;
    QrResMesVMes09: TFloatField;
    QrResMesVMes10: TFloatField;
    QrResMesVMes11: TFloatField;
    QrResMesVMes12: TFloatField;
    QrResMesVPendencias: TFloatField;
    QrResMesVMESTO: TFloatField;
    QrB00: TmySQLQuery;
    QrU00: TmySQLQuery;
    QrU01: TmySQLQuery;
    QrU02: TmySQLQuery;
    QrU03: TmySQLQuery;
    QrU04: TmySQLQuery;
    QrU05: TmySQLQuery;
    QrU06: TmySQLQuery;
    QrU07: TmySQLQuery;
    QrU08: TmySQLQuery;
    QrU09: TmySQLQuery;
    QrU10: TmySQLQuery;
    QrU11: TmySQLQuery;
    QrU12: TmySQLQuery;
    QrC00: TmySQLQuery;
    QrV00: TmySQLQuery;
    QrV01: TmySQLQuery;
    QrV02: TmySQLQuery;
    QrV03: TmySQLQuery;
    QrV04: TmySQLQuery;
    QrV05: TmySQLQuery;
    QrV06: TmySQLQuery;
    QrV07: TmySQLQuery;
    QrV08: TmySQLQuery;
    QrV09: TmySQLQuery;
    QrV10: TmySQLQuery;
    QrV11: TmySQLQuery;
    QrV12: TmySQLQuery;
    QrB00Debito: TFloatField;
    QrB00Credito: TFloatField;
    QrB00Saldo: TFloatField;
    QrU00Debito: TFloatField;
    QrU00Credito: TFloatField;
    QrU00Saldo: TFloatField;
    QrU01Debito: TFloatField;
    QrU01Credito: TFloatField;
    QrU01Saldo: TFloatField;
    QrU01SDO_PER: TFloatField;
    QrU02Debito: TFloatField;
    QrU02Credito: TFloatField;
    QrU02Saldo: TFloatField;
    QrU02SDO_PER: TFloatField;
    QrU00SDO_PER: TFloatField;
    QrU12SDO_PER: TFloatField;
    QrU12Debito: TFloatField;
    QrU12Credito: TFloatField;
    QrU12Saldo: TFloatField;
    QrU12TDebito: TFloatField;
    QrU12TCredito: TFloatField;
    QrU12TSaldo: TFloatField;
    QrU03Debito: TFloatField;
    QrU03Credito: TFloatField;
    QrU03Saldo: TFloatField;
    QrU03SDO_PER: TFloatField;
    QrU04Debito: TFloatField;
    QrU04Credito: TFloatField;
    QrU04Saldo: TFloatField;
    QrU04SDO_PER: TFloatField;
    QrU05Debito: TFloatField;
    QrU05Credito: TFloatField;
    QrU05Saldo: TFloatField;
    QrU05SDO_PER: TFloatField;
    QrU06Debito: TFloatField;
    QrU06Credito: TFloatField;
    QrU06Saldo: TFloatField;
    QrU06SDO_PER: TFloatField;
    QrU07Debito: TFloatField;
    QrU07Credito: TFloatField;
    QrU07Saldo: TFloatField;
    QrU07SDO_PER: TFloatField;
    QrU08Debito: TFloatField;
    QrU08Credito: TFloatField;
    QrU08Saldo: TFloatField;
    QrU08SDO_PER: TFloatField;
    QrU09Debito: TFloatField;
    QrU09Credito: TFloatField;
    QrU09Saldo: TFloatField;
    QrU09SDO_PER: TFloatField;
    QrU10Debito: TFloatField;
    QrU10Credito: TFloatField;
    QrU10Saldo: TFloatField;
    QrU10SDO_PER: TFloatField;
    QrU11Debito: TFloatField;
    QrU11Credito: TFloatField;
    QrU11Saldo: TFloatField;
    QrU11SDO_PER: TFloatField;
    QrC00Debito: TFloatField;
    QrC00Credito: TFloatField;
    QrC00Saldo: TFloatField;
    QrV00Debito: TFloatField;
    QrV00Credito: TFloatField;
    QrV00Saldo: TFloatField;
    QrV00SDO_PER: TFloatField;
    QrV01Debito: TFloatField;
    QrV01Credito: TFloatField;
    QrV01Saldo: TFloatField;
    QrV01SDO_PER: TFloatField;
    QrV02Debito: TFloatField;
    QrV02Credito: TFloatField;
    QrV02Saldo: TFloatField;
    QrV02SDO_PER: TFloatField;
    QrV03Debito: TFloatField;
    QrV03Credito: TFloatField;
    QrV03Saldo: TFloatField;
    QrV03SDO_PER: TFloatField;
    QrV04Debito: TFloatField;
    QrV04Credito: TFloatField;
    QrV04Saldo: TFloatField;
    QrV04SDO_PER: TFloatField;
    QrV05Debito: TFloatField;
    QrV05Credito: TFloatField;
    QrV05Saldo: TFloatField;
    QrV05SDO_PER: TFloatField;
    QrV06Debito: TFloatField;
    QrV06Credito: TFloatField;
    QrV06Saldo: TFloatField;
    QrV06SDO_PER: TFloatField;
    QrV07Debito: TFloatField;
    QrV07Credito: TFloatField;
    QrV07Saldo: TFloatField;
    QrV07SDO_PER: TFloatField;
    QrV08Debito: TFloatField;
    QrV08Credito: TFloatField;
    QrV08Saldo: TFloatField;
    QrV08SDO_PER: TFloatField;
    QrV09Debito: TFloatField;
    QrV09Credito: TFloatField;
    QrV09Saldo: TFloatField;
    QrV09SDO_PER: TFloatField;
    QrV10Debito: TFloatField;
    QrV10Credito: TFloatField;
    QrV10Saldo: TFloatField;
    QrV10SDO_PER: TFloatField;
    QrV11Debito: TFloatField;
    QrV11Credito: TFloatField;
    QrV11Saldo: TFloatField;
    QrV11SDO_PER: TFloatField;
    QrV12SDO_PER: TFloatField;
    QrV12Debito: TFloatField;
    QrV12Credito: TFloatField;
    QrV12Saldo: TFloatField;
    QrV12TDebito: TFloatField;
    QrV12TCredito: TFloatField;
    QrV12TSaldo: TFloatField;
    QrConsigIts: TmySQLQuery;
    QrConsigItsSaldo: TFloatField;
    QrConsigItsDescricao: TWideStringField;
    QrConsigItsCodigo: TIntegerField;
    QrConsMes: TmySQLQuery;
    QrConsMesCodigo: TIntegerField;
    QrConsMesNome: TWideStringField;
    QrConsMesMes01: TFloatField;
    QrConsMesMes02: TFloatField;
    QrConsMesMes03: TFloatField;
    QrConsMesMes04: TFloatField;
    QrConsMesMes05: TFloatField;
    QrConsMesMes06: TFloatField;
    QrConsMesMes07: TFloatField;
    QrConsMesMes08: TFloatField;
    QrConsMesMes09: TFloatField;
    QrConsMesMes10: TFloatField;
    QrConsMesMes11: TFloatField;
    QrConsMesMes12: TFloatField;
    TamanhoA4210x2971: TMenuItem;
    TamanhoA3297x420cm1: TMenuItem;
    Exportaparaexcel1: TMenuItem;
    SaveDialog1: TSaveDialog;
    Retrato1: TMenuItem;
    Paisagem1: TMenuItem;
    QrRMA: TmySQLQuery;
    QrRMASeqImp: TIntegerField;
    QrRMAConta: TIntegerField;
    QrRMANome: TWideStringField;
    QrRMAPeriodo: TIntegerField;
    QrRMATipo: TIntegerField;
    QrRMAFator: TFloatField;
    QrRMAValFator: TFloatField;
    QrRMADevido: TFloatField;
    QrRMAPago: TFloatField;
    QrRMADiferenca: TFloatField;
    QrRMAAcumulado: TFloatField;
    QrCtas: TmySQLQuery;
    QrCtasConta: TIntegerField;
    QrCtasNome: TWideStringField;
    QrResPenM: TmySQLQuery;
    QrResPenMGenero: TIntegerField;
    QrResPenMNomeConta: TWideStringField;
    QrResPenMAnoAn: TFloatField;
    QrResPenMMes01: TFloatField;
    QrResPenMMes02: TFloatField;
    QrResPenMMes03: TFloatField;
    QrResPenMMes04: TFloatField;
    QrResPenMMes05: TFloatField;
    QrResPenMMes06: TFloatField;
    QrResPenMMes07: TFloatField;
    QrResPenMMes08: TFloatField;
    QrResPenMMes09: TFloatField;
    QrResPenMMes10: TFloatField;
    QrResPenMMes11: TFloatField;
    QrResPenMMes12: TFloatField;
    QrResPenMPendencias: TFloatField;
    QrResPenMGrupo: TIntegerField;
    QrResMesTNE: TmySQLQuery;
    QrResMesTNESOMA: TFloatField;
    QrSaldos: TmySQLQuery;
    QrSaldosNome: TWideStringField;
    QrSaldosSaldo: TFloatField;
    QrConsigna: TmySQLQuery;
    QrConsignaNome: TWideStringField;
    QrConsignaSaldo: TFloatField;
    QrAVencer: TmySQLQuery;
    QrAVencerNome: TWideStringField;
    QrAVencerSALDO: TFloatField;
    QrTotalSaldo: TmySQLQuery;
    QrTotalSaldoNome: TWideStringField;
    QrTotalSaldoSaldo: TFloatField;
    QrTotalSaldoTipo: TIntegerField;
    QrTotalSaldoNOMETIPO: TWideStringField;
    frxResMes: TfrxReport;
    frxDsResMes: TfrxDBDataset;
    frxDsT00: TfrxDBDataset;
    frxDsT01: TfrxDBDataset;
    frxDsT02: TfrxDBDataset;
    frxDsT03: TfrxDBDataset;
    frxDsT04: TfrxDBDataset;
    frxDsT05: TfrxDBDataset;
    frxDsT06: TfrxDBDataset;
    frxDsT07: TfrxDBDataset;
    frxDsT08: TfrxDBDataset;
    frxDsT09: TfrxDBDataset;
    frxDsT10: TfrxDBDataset;
    frxDsT11: TfrxDBDataset;
    frxDsT12: TfrxDBDataset;
    QrTSG: TmySQLQuery;
    QrTSGSubGrupo: TIntegerField;
    QrTSGT00: TFloatField;
    QrTSGT01: TFloatField;
    QrTSGT02: TFloatField;
    QrTSGT03: TFloatField;
    QrTSGT04: TFloatField;
    QrTSGT05: TFloatField;
    QrTSGT06: TFloatField;
    QrTSGT07: TFloatField;
    QrTSGT08: TFloatField;
    QrTSGT09: TFloatField;
    QrTSGT10: TFloatField;
    QrTSGT11: TFloatField;
    QrTSGT12: TFloatField;
    QrTSGTPN: TFloatField;
    QrTSGTTO: TFloatField;
    frxResMesFV_A41: TfrxReport;
    frxDsresMesV: TfrxDBDataset;
    Panel6: TPanel;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    RGExclusivos: TRadioGroup;
    Panel4: TPanel;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    CkSaldo: TCheckBox;
    TPSaldo: TDateTimePicker;
    CkNaoZero: TCheckBox;
    CkExclusivo: TCheckBox;
    frxDsU00: TfrxDBDataset;
    frxDsU01: TfrxDBDataset;
    frxDsU02: TfrxDBDataset;
    frxDsU03: TfrxDBDataset;
    frxDsU04: TfrxDBDataset;
    frxDsU05: TfrxDBDataset;
    frxDsU06: TfrxDBDataset;
    frxDsU07: TfrxDBDataset;
    frxDsU08: TfrxDBDataset;
    frxDsU09: TfrxDBDataset;
    frxDsU10: TfrxDBDataset;
    frxDsU11: TfrxDBDataset;
    frxDsU12: TfrxDBDataset;
    frxDsV00: TfrxDBDataset;
    frxDsV01: TfrxDBDataset;
    frxDsV02: TfrxDBDataset;
    frxDsV03: TfrxDBDataset;
    frxDsV04: TfrxDBDataset;
    frxDsV05: TfrxDBDataset;
    frxDsV06: TfrxDBDataset;
    frxDsV07: TfrxDBDataset;
    frxDsV08: TfrxDBDataset;
    frxDsV09: TfrxDBDataset;
    frxDsV10: TfrxDBDataset;
    frxDsV11: TfrxDBDataset;
    frxDsV12: TfrxDBDataset;
    frxDsConsMes: TfrxDBDataset;
    frxDsResMesF: TfrxDBDataset;
    frxResMesFV_A4: TfrxReport;
    frxDsresPenM: TfrxDBDataset;
    frxDsTotalSaldo: TfrxDBDataset;
    frxResMesFV_A3: TfrxReport;
    frxDsresMesTNE: TfrxDBDataset;
    frxCtasResMes: TfrxReport;
    frxDsCtasResMes: TfrxDBDataset;
    frxResMesFV_A4B: TfrxReport;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    LaAviso3: TLabel;
    ProgressBar1: TProgressBar;
    ProgressBar2: TProgressBar;
    ProgressBar3: TProgressBar;
    frxDsTSG: TfrxDBDataset;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtGera: TBitBtn;
    BtImprime: TBitBtn;
    CheckBox1: TCheckBox;
    frxResMesFV_A4C: TfrxReport;
    QrResmesPercTot: TFloatField;
    QrResMesFPercTot: TFloatField;
    QrResMesVPercTot: TFloatField;
    QrDC43: TmySQLQuery;
    QrDC43M01_D: TFloatField;
    QrDC43M01_C: TFloatField;
    QrDC43M01: TFloatField;
    QrDC43M02_D: TFloatField;
    QrDC43M02_C: TFloatField;
    QrDC43M02: TFloatField;
    QrDC43M03_D: TFloatField;
    QrDC43M03_C: TFloatField;
    QrDC43M03: TFloatField;
    QrDC43M04_D: TFloatField;
    QrDC43M04_C: TFloatField;
    QrDC43M04: TFloatField;
    QrDC43M05_D: TFloatField;
    QrDC43M05_C: TFloatField;
    QrDC43M05: TFloatField;
    QrDC43M06_D: TFloatField;
    QrDC43M06_C: TFloatField;
    QrDC43M06: TFloatField;
    QrDC43M07_D: TFloatField;
    QrDC43M07_C: TFloatField;
    QrDC43M07: TFloatField;
    QrDC43M08_D: TFloatField;
    QrDC43M08_C: TFloatField;
    QrDC43M08: TFloatField;
    QrDC43M09_D: TFloatField;
    QrDC43M09_C: TFloatField;
    QrDC43M09: TFloatField;
    QrDC43M10_D: TFloatField;
    QrDC43M10_C: TFloatField;
    QrDC43M10: TFloatField;
    QrDC43M11_D: TFloatField;
    QrDC43M11_C: TFloatField;
    QrDC43M11: TFloatField;
    QrDC43M12_D: TFloatField;
    QrDC43M12_C: TFloatField;
    QrDC43M12: TFloatField;
    frxDsDC43: TfrxDBDataset;
    QrDC43ALL_C: TFloatField;
    QrDC43ALL_D: TFloatField;
    QrDC43ALL: TFloatField;
    QrTSGTipoAgrupa: TIntegerField;
    QrD43: TmySQLQuery;
    frxDsD43: TfrxDBDataset;
    QrD43Nome: TWideStringField;
    QrD43Valor: TFloatField;
    QrC43: TmySQLQuery;
    frxDsC43: TfrxDBDataset;
    QrC43Nome: TWideStringField;
    QrC43Valor: TFloatField;
    QrDouC43: TmySQLQuery;
    QrDouC43Mes01: TFloatField;
    QrDouC43Mes02: TFloatField;
    QrDouC43Mes03: TFloatField;
    QrDouC43Mes04: TFloatField;
    QrDouC43Mes05: TFloatField;
    QrDouC43Mes06: TFloatField;
    QrDouC43Mes07: TFloatField;
    QrDouC43Mes08: TFloatField;
    QrDouC43Mes09: TFloatField;
    QrDouC43Mes10: TFloatField;
    QrDouC43Mes11: TFloatField;
    QrDouC43Mes12: TFloatField;
    QrResMesFSeqID: TIntegerField;
    Qr43LM: TmySQLQuery;
    Qr43LMLimChrtLin: TIntegerField;
    frxDs43LM: TfrxDBDataset;
    Qr43LMTipoAgrupa: TIntegerField;
    QrAtiCtsGrupo: TIntegerField;
    QrAtiCtsNomeGrupo: TWideStringField;
    QrResMesFNO_TipoAgrupa: TWideStringField;
    QrResMesFPercCta: TFloatField;
    QrTSGPerc: TFloatField;
    QrDouC43Nome: TWideStringField;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure RGExclusivosClick(Sender: TObject);
    procedure CBMesIChange(Sender: TObject);
    procedure CBAnoIChange(Sender: TObject);
    procedure CBMesChange(Sender: TObject);
    procedure CBAnoChange(Sender: TObject);
    procedure QrResmesCalcFields(DataSet: TDataSet);
    procedure QrT00CalcFields(DataSet: TDataSet);
    procedure QrT01CalcFields(DataSet: TDataSet);
    procedure QrT02CalcFields(DataSet: TDataSet);
    procedure QrT03CalcFields(DataSet: TDataSet);
    procedure QrT04CalcFields(DataSet: TDataSet);
    procedure QrT05CalcFields(DataSet: TDataSet);
    procedure QrT06CalcFields(DataSet: TDataSet);
    procedure QrT07CalcFields(DataSet: TDataSet);
    procedure QrT08CalcFields(DataSet: TDataSet);
    procedure QrT09CalcFields(DataSet: TDataSet);
    procedure QrT10CalcFields(DataSet: TDataSet);
    procedure QrT11CalcFields(DataSet: TDataSet);
    procedure QrT12CalcFields(DataSet: TDataSet);
    procedure BtGeraClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure Contascontroladas1Click(Sender: TObject);
    procedure QrCtasResMesCalcFields(DataSet: TDataSet);
    procedure Tudojunto1Click(Sender: TObject);
    procedure QrResMesFCalcFields(DataSet: TDataSet);
    procedure QrResMesVCalcFields(DataSet: TDataSet);
    procedure QrU00CalcFields(DataSet: TDataSet);
    procedure QrU02CalcFields(DataSet: TDataSet);
    procedure QrU03CalcFields(DataSet: TDataSet);
    procedure QrU04CalcFields(DataSet: TDataSet);
    procedure QrU01CalcFields(DataSet: TDataSet);
    procedure QrU05CalcFields(DataSet: TDataSet);
    procedure QrU06CalcFields(DataSet: TDataSet);
    procedure QrU07CalcFields(DataSet: TDataSet);
    procedure QrU08CalcFields(DataSet: TDataSet);
    procedure QrU09CalcFields(DataSet: TDataSet);
    procedure QrU10CalcFields(DataSet: TDataSet);
    procedure QrU11CalcFields(DataSet: TDataSet);
    procedure QrU12CalcFields(DataSet: TDataSet);
    procedure PMImprimePopup(Sender: TObject);
    procedure QrV00CalcFields(DataSet: TDataSet);
    procedure QrV01CalcFields(DataSet: TDataSet);
    procedure QrV02CalcFields(DataSet: TDataSet);
    procedure QrV03CalcFields(DataSet: TDataSet);
    procedure QrV04CalcFields(DataSet: TDataSet);
    procedure QrV05CalcFields(DataSet: TDataSet);
    procedure QrV06CalcFields(DataSet: TDataSet);
    procedure QrV07CalcFields(DataSet: TDataSet);
    procedure QrV08CalcFields(DataSet: TDataSet);
    procedure QrV09CalcFields(DataSet: TDataSet);
    procedure QrV10CalcFields(DataSet: TDataSet);
    procedure QrV11CalcFields(DataSet: TDataSet);
    procedure QrV12CalcFields(DataSet: TDataSet);
    procedure TamanhoA3297x420cm1Click(Sender: TObject);
    procedure Exportaparaexcel1Click(Sender: TObject);
    procedure Retrato1Click(Sender: TObject);
    procedure Paisagem1Click(Sender: TObject);
    procedure frxResMesGetValue(const VarName: String; var Value: Variant);
    procedure QrTSGCalcFields(DataSet: TDataSet);
    procedure QrResMesFAfterScroll(DataSet: TDataSet);
    procedure QrResmesAfterScroll(DataSet: TDataSet);
    procedure QrDC43CalcFields(DataSet: TDataSet);
    procedure frxResMesFV_A4CGetValue(const VarName: string;
      var Value: Variant);
    procedure frxResMesFV_A4BGetValue(const VarName: string;
      var Value: Variant);
    procedure frxResMesFV_A41GetValue(const VarName: string;
      var Value: Variant);
    procedure frxResMesFV_A3GetValue(const VarName: string; var Value: Variant);
    procedure frxCtasResMesGetValue(const VarName: string; var Value: Variant);
    procedure frxResMesFV_A4GetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    FResMes, FConsMes, FResPenM, FSaldos: String;
    FPrintNo, FPeriodo: Integer;
    FTotalAnDeb, FTotalAnCre, FTotalAnDeC: Double;
    procedure DesfazGeracao;
    procedure ImprimeResMes;
    procedure GeraPendencias;
    function  MesStrPeriodo(Periodo: Integer): String;
    procedure MontaChartLines(const frxReport: TfrxReport; const Objeto: String;
              (*const TipoAgrupa: Integer;*) var Value: Variant);
    procedure ImprimeSaldoEm();
    //function  ObtemTotalSubGrupo(Mes: Integer): Double;
    function  DefineLctTabela(Data: TDateTime): String;
    function  DefineLctFieldSdoIni(Data: TDateTime): String;
    //function  DefineLctTipo(Data: TDateTime): TTabLctToWork;
    procedure DefineLimitBy();
    procedure DefinePercentualConta(var Value: Variant);
    procedure DefinePercentualSubGrupo(var Value: Variant);
    function  PreparaDadosPara_frxResMesFV_A4B(): TfrxReport;
    function  PreparaDadosPara_frxResMesFV_A4C(): TfrxReport;
    procedure ReopenAtiCts(EntiTxt: String);
    procedure ReopenResMesF(Ax: Integer);
    procedure ReopenTSG(SubGrupo: Integer);

  public
    { Public declarations }
    FEntidade, FHowShowA: Integer;
    FPaginar, FPontoMilhar, FResMes43: Boolean;
    // Por causa do CashBal
    FDtEncer, FDtMorto: TDateTime;
    FTabLctA, FTabLctB, FTabLctD,
    FTabelaChart, FCampoChart, FOutrosChart: String;
    //
    procedure frxReport000GetValue(frxReport: TfrxReport;
              const VarName: string; var Value: Variant);
    procedure ResultadoGeral(Periodo: Integer; PB_1, PB_2, PB_3: TProgressBar;
              LaSub_1, LaSub_2, LaSub_3: TLabel);
    function ImprimeResMesExclusivo(Ax: Integer; PB1, PB2: TProgressBar;
              LaSub1: TLabel): TfrxReport;
    procedure TiraPontoMilhar(frxReport: TfrxReport);
    procedure VerificaTipoAgrupa();

  end;

var
  FmResMes: TFmResMes;

implementation

{$R *.DFM}

uses UnMsgInt, Module, UCreateFin, ModuleFin, ModuleGeral, DmkDAC_PF, CashBal,
  UMySQLModule;

procedure TFmResMes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmResMes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmResMes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FTabelaChart := 'Genero';
  FCampoChart := 'NomeConta';
  FOutrosChart := 'Outras Contas';
  FHowShowA := 0;
  FPontoMilhar := True;
  FResMes43 := False;
  FPaginar := True;
  TPSaldo.Date := Date;
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa(LaAviso1, False, '...');
  MyObjects.Informa(LaAviso2, False, '...');
  MyObjects.Informa(LaAviso3, False, '...');
  //////////////////////////////////////////////////////////////////////////
  TbResMes.Database      := DModG.MyPID_DB;
  QrResMes.Database      := DModG.MyPID_DB;
  QrSubGrupos.Database   := DModG.MyPID_DB;
  QrSum.Database         := DModG.MyPID_DB;
  QrCtasResMes.Database  := DModG.MyPID_DB;
  QrResMesF.Database     := DModG.MyPID_DB;
  QrResMesV.Database     := DModG.MyPID_DB;
  QrTSG.Database         := DModG.MyPID_DB;
  QrRMA.Database         := DModG.MyPID_DB;
  QrResPenM.Database     := DModG.MyPID_DB;
  QrCtas.Database        := DModG.MyPID_DB;
  QrResMesTNE.Database   := DModG.MyPID_DB;
  QrConsMes.Database     := DModG.MyPID_DB;
  QrTotalSaldo.Database  := DModG.MyPID_DB;
  QrAtiCts.Database      := DModG.MyPID_DB;
  QrAnt.Database         := DModG.MyPID_DB;
  QrBef.Database         := DModG.MyPID_DB;
  QrA00.Database         := DModG.MyPID_DB;
  QrB00.Database         := DModG.MyPID_DB;
  QrC00.Database         := DModG.MyPID_DB;
  QrT00.Database         := DModG.MyPID_DB;
  QrT01.Database         := DModG.MyPID_DB;
  QrT02.Database         := DModG.MyPID_DB;
  QrT03.Database         := DModG.MyPID_DB;
  QrT04.Database         := DModG.MyPID_DB;
  QrT05.Database         := DModG.MyPID_DB;
  QrT06.Database         := DModG.MyPID_DB;
  QrT07.Database         := DModG.MyPID_DB;
  QrT08.Database         := DModG.MyPID_DB;
  QrT09.Database         := DModG.MyPID_DB;
  QrT10.Database         := DModG.MyPID_DB;
  QrT11.Database         := DModG.MyPID_DB;
  QrT12.Database         := DModG.MyPID_DB;
  QrU00.Database         := DModG.MyPID_DB;
  QrU01.Database         := DModG.MyPID_DB;
  QrU02.Database         := DModG.MyPID_DB;
  QrU03.Database         := DModG.MyPID_DB;
  QrU04.Database         := DModG.MyPID_DB;
  QrU05.Database         := DModG.MyPID_DB;
  QrU06.Database         := DModG.MyPID_DB;
  QrU07.Database         := DModG.MyPID_DB;
  QrU08.Database         := DModG.MyPID_DB;
  QrU09.Database         := DModG.MyPID_DB;
  QrU10.Database         := DModG.MyPID_DB;
  QrU11.Database         := DModG.MyPID_DB;
  QrU12.Database         := DModG.MyPID_DB;
  QrV00.Database         := DModG.MyPID_DB;
  QrV01.Database         := DModG.MyPID_DB;
  QrV02.Database         := DModG.MyPID_DB;
  QrV03.Database         := DModG.MyPID_DB;
  QrV04.Database         := DModG.MyPID_DB;
  QrV05.Database         := DModG.MyPID_DB;
  QrV06.Database         := DModG.MyPID_DB;
  QrV07.Database         := DModG.MyPID_DB;
  QrV08.Database         := DModG.MyPID_DB;
  QrV09.Database         := DModG.MyPID_DB;
  QrV10.Database         := DModG.MyPID_DB;
  QrV11.Database         := DModG.MyPID_DB;
  QrV12.Database         := DModG.MyPID_DB;
end;

procedure TFmResMes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmResMes.RGExclusivosClick(Sender: TObject);
begin
  DesfazGeracao;
end;

function TFmResMes.DefineLctTabela(Data: TDateTime): String;
begin
  if Data > FDtEncer then
    Result := FTabLctA
  else
  if Data > FDtMorto then
    Result := FTabLctB
  else Result := FTabLctD;
end;

procedure TFmResMes.DefineLimitBy();
var
  Qry: TmySQLQuery;
  LimChrtLin, SeqID: Integer;
  procedure AbreQry(TipoAgrupa: Integer);
  var
    SQLDesc: String;
  begin
    if  TipoAgrupa = 1 then
      SQLDesc := 'DESC'
    else
      SQLDesc := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'SELECT DISTINCT ' + FTabelaChart,
    'FROM ' + FResMes,
    'WHERE TipoAgrupa=' + Geral.FF0(TipoAgrupa),
    'AND Genero > 0 ',
    'AND Exclusivo = "F" ',
    'ORDER BY AnoAt ' + SQLDesc,
    '']);
    Qry.First;
  end;
  procedure SetaLimitBy(TipoAgrupa: Integer);
  begin
    AbreQry(TipoAgrupa);
    while not Qry.Eof do
    begin
      LimChrtLin := (Qry.RecNo div 12) + 1;
      SeqID := Qry.FieldByName(FTabelaChart).AsInteger;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FResMes, False, [
      'LimChrtLin'], [FTabelaChart], [
      LimChrtLin], [SeqID], False);
      //
      Qry.Next;
    end;
  end;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    SetaLimitBy(0);
    SetaLimitBy(1);
  finally
    Qry.Free;
  end;
end;

procedure TFmResMes.DefinePercentualConta(var Value: Variant);
begin
  case QrTSGTipoAgrupa.Value of
    0: // Debito
    begin
      if FTotalAnDeb <> 0 then
        Value := QrResmesFMESTO.Value / FTotalAnDeb * 100
      else
        Value := 0;
    end;
    1: // Credito
    begin
      if FTotalAnCre <> 0 then
        Value := QrResmesFMESTO.Value / FTotalAnCre * 100
      else
        Value := 0;
    end;
    2: // Ambos
    begin
      if FTotalAnDeC <> 0 then
        Value := QrResmesFMESTO.Value / FTotalAnDeC * 100
      else
        Value := 0;
    end;
    else // Erro
    begin
      Value := 0;
      Geral.MB_Erro('FTotalAn??? sem defini��o de % de SGR!');
    end;
  end;
end;

procedure TFmResMes.DefinePercentualSubGrupo(var Value: Variant);
begin
  case QrTSGTipoAgrupa.Value of
    0: // Debito
    begin
      if FTotalAnDeb <> 0 then
        Value := QrTSGTTO.Value / FTotalAnDeb * 100
      else
        Value := 0;
    end;
    1: // Credito
    begin
      if FTotalAnCre <> 0 then
        Value := QrTSGTTO.Value / FTotalAnCre * 100
      else
        Value := 0;
    end;
    2: // Ambos
    begin
      if FTotalAnDeC <> 0 then
        Value := QrTSGTTO.Value / FTotalAnDeC * 100
      else
        Value := 0;
    end;
    else // Erro
    begin
      Value := 0;
      Geral.MB_Erro('FTotalAn??? sem defini��o de % de SGR!');
    end;
  end;
end;

{
function TFmResMes.DefineLctTipo(Data: TDateTime): TTabLctToWork;
begin
  if Data > FDtEncer then
    Result := tlwA
  else
  if Data > FDtMorto then
    Result := tlwB
  else Result := tlwD;
end;
}

function TFmResMes.DefineLctFieldSdoIni(Data: TDateTime): String;
begin
  if Data > FDtEncer then
    Result := 'car.SdoFimB'
  else
  if Data > FDtMorto then
    Result := '0'
  else Result := '0';
end;

procedure TFmResMes.DesfazGeracao;
begin
  QrRes.Close;
  BtImprime.Enabled := False;
  TbResMes.Close;
end;

procedure TFmResMes.CBMesIChange(Sender: TObject);
begin
  DesfazGeracao;
end;

procedure TFmResMes.CBAnoIChange(Sender: TObject);
begin
  DesfazGeracao;
end;

procedure TFmResMes.CBMesChange(Sender: TObject);
begin
  DesfazGeracao;
end;

procedure TFmResMes.CBAnoChange(Sender: TObject);
begin
  DesfazGeracao;
end;

procedure TFmResMes.QrResmesAfterScroll(DataSet: TDataSet);
begin
  ReopenTSG(QrResMesSubGrupo.Value);
end;

procedure TFmResMes.QrResmesCalcFields(DataSet: TDataSet);
begin
  if QrResmesSubOrdem.Value = 0 then QrResmesMESTO.Value :=
    QrResmesMes01.Value + QrResmesMes02.Value + QrResmesMes03.Value +
    QrResmesMes04.Value + QrResmesMes05.Value + QrResmesMes06.Value +
    QrResmesMes07.Value + QrResmesMes08.Value + QrResmesMes09.Value +
    QrResmesMes10.Value + QrResmesMes11.Value + QrResmesMes12.Value
  else QrResmesMESTO.Value := QrResmesMes12.Value;
(*&�%
PercTot
*)
end;

procedure TFmResMes.QrT00CalcFields(DataSet: TDataSet);
begin
  QrT00SDO_PER.Value := QrT00Saldo.Value + QrA00Saldo.Value + QrInicialSaldo.Value;
end;

procedure TFmResMes.QrT01CalcFields(DataSet: TDataSet);
begin
  QrT01SDO_PER.Value := QrT01Saldo.Value + QrT00SDO_PER.Value;
end;

procedure TFmResMes.QrT02CalcFields(DataSet: TDataSet);
begin
  QrT02SDO_PER.Value :=
  QrT02Saldo.Value +
  QrT01SDO_PER.Value;
end;

procedure TFmResMes.QrT03CalcFields(DataSet: TDataSet);
begin
  QrT03SDO_PER.Value :=
  QrT03Saldo.Value +
  QrT02SDO_PER.Value;
end;

procedure TFmResMes.QrT04CalcFields(DataSet: TDataSet);
begin
  QrT04SDO_PER.Value :=
  QrT04Saldo.Value +
  QrT03SDO_PER.Value;
end;

procedure TFmResMes.QrT05CalcFields(DataSet: TDataSet);
begin
  QrT05SDO_PER.Value :=
  QrT05Saldo.Value +
  QrT04SDO_PER.Value;
end;

procedure TFmResMes.QrT06CalcFields(DataSet: TDataSet);
begin
  QrT06SDO_PER.Value :=
  QrT06Saldo.Value +
  QrT05SDO_PER.Value;
end;

procedure TFmResMes.QrT07CalcFields(DataSet: TDataSet);
begin
  QrT07SDO_PER.Value :=
  QrT07Saldo.Value +
  QrT06SDO_PER.Value;
end;

procedure TFmResMes.QrT08CalcFields(DataSet: TDataSet);
begin
  QrT08SDO_PER.Value :=
  QrT08Saldo.Value +
  QrT07SDO_PER.Value;
end;

procedure TFmResMes.QrT09CalcFields(DataSet: TDataSet);
begin
  QrT09SDO_PER.Value :=
  QrT09Saldo.Value +
  QrT08SDO_PER.Value;
end;

procedure TFmResMes.QrT10CalcFields(DataSet: TDataSet);
begin
  QrT10SDO_PER.Value :=
  QrT10Saldo.Value +
  QrT09SDO_PER.Value;
end;

procedure TFmResMes.QrT11CalcFields(DataSet: TDataSet);
begin
  QrT11SDO_PER.Value :=
  QrT11Saldo.Value +
  QrT10SDO_PER.Value;
end;

procedure TFmResMes.QrT12CalcFields(DataSet: TDataSet);
begin
  QrT12SDO_PER.Value := QrT12Saldo.Value + QrT11SDO_PER.Value;
  //
  QrT12TCredito.Value :=
  QrT01Credito.Value + QrT02Credito.Value + QrT03Credito.Value +
  QrT04Credito.Value + QrT05Credito.Value + QrT06Credito.Value +
  QrT07Credito.Value + QrT08Credito.Value + QrT09Credito.Value +
  QrT10Credito.Value + QrT11Credito.Value + QrT12Credito.Value;
  //
  QrT12TDebito.Value :=
  QrT01Debito.Value + QrT02Debito.Value + QrT03Debito.Value +
  QrT04Debito.Value + QrT05Debito.Value + QrT06Debito.Value +
  QrT07Debito.Value + QrT08Debito.Value + QrT09Debito.Value +
  QrT10Debito.Value + QrT11Debito.Value + QrT12Debito.Value;
  //
  QrT12TSaldo.Value := QrT12TCredito.Value - QrT12TDebito.Value;
  //
end;

procedure TFmResMes.BtGeraClick(Sender: TObject);
var
  Ano, Mes: Word;
  Periodo: Integer;
begin
  Ano := Geral.IMV(CBAno.Text);
  if Ano = 0 then Exit;
  Mes := CBMes.ItemIndex + 1;
  FPeriodo := ((Ano - 2000) * 12) + Mes;
  Periodo := FPeriodo - 11;
  ResultadoGeral(Periodo, ProgressBar1, ProgressBar2, ProgressBar3,
    LaAviso1, LaAviso2, LaAviso3);
  //////////////////////////////////////////////////////////////////////////////
  //  ContasMensais
  //////////////////////////////////////////////////////////////////////////////
  DmodFin.AtualizaContasMensais2(Periodo, Periodo + 11, 0,
    ProgressBar1, ProgressBar2, ProgressBar3,
    LaAviso1, LaAviso2, LaAviso3,
    FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
end;

procedure TFmResMes.ReopenAtiCts(EntiTxt: String);
  function SQLTbX(FTabLctX: String): String;
  var
    SQL_Exclusivo: String;
  begin
    SQL_Exclusivo := '';
    case RGExclusivos.ItemIndex of
      0: SQL_Exclusivo := 'AND co.Exclusivo="F" ';
      1: SQL_Exclusivo := '';
      2: SQL_Exclusivo := 'AND co.Exclusivo="V" ';
    end;
    //
    Result := Geral.ATS([
    'SELECT DISTINCT ',
    'sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista, ',
    'co.SubGrupo, co.Nome NOMECONTA, la.Genero, co.Exclusivo, ',
    'gr.Codigo Grupo, gr.Nome NomeGrupo ',
    'FROM ' + FTabLctX + ' la ',
    'LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero ',
    'LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo ',
    'LEFT JOIN ' + TMeuDB + '.grupos    gr ON gr.Codigo=sg.Grupo ',
    'WHERE la.Genero>0 ',
    'AND la.Tipo <> 2 ',
    SQL_Exclusivo,
    'AND la.CliInt=' + EntiTxt]);
  end;
begin
  QrAtiCts.Close;
  // � preciso todas contas por causa do saldo anterior
  UnDmKDAC_PF.AbreMySQLQuery0(QrAtiCts, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _ati_cts_; ',
  'CREATE TABLE _ati_cts_ ',
  '',
  SQLTbX(FTabLctA),
  'UNION ',
  SQLTbX(FTabLctB),
  'UNION ',
  SQLTbX(FTabLctD),
  '; ',
  'SELECT DISTINCT * ',
  'FROM _ati_cts_ ',
  'ORDER BY TipoAgrupa, OrdemLista, NomeSubGrupo, NomeConta; ',
  '']);
(*
  QrAtiCts.SQL.Clear;
  QrAtiCts.SQL.Add('DROP TABLE IF EXISTS _ati_cts_;');
  QrAtiCts.SQL.Add('CREATE TABLE _ati_cts_');
  QrAtiCts.SQL.Add('');

  QrAtiCts.SQL.Add('SELECT DISTINCT');
  QrAtiCts.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
  QrAtiCts.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero, co.Exclusivo');
  QrAtiCts.SQL.Add('FROM ' + FTabLctA + ' la');
  QrAtiCts.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrAtiCts.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
  QrAtiCts.SQL.Add('WHERE la.Genero>0');
  QrAtiCts.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrAtiCts.SQL.Add('AND co.Exclusivo="F"');
    1: QrAtiCts.SQL.Add('');
    2: QrAtiCts.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrAtiCts.SQL.Add('AND la.CliInt=' + EntiTxt);

  QrAtiCts.SQL.Add('UNION');

  QrAtiCts.SQL.Add('SELECT DISTINCT');
  QrAtiCts.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
  QrAtiCts.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero, co.Exclusivo');
  QrAtiCts.SQL.Add('FROM ' + FTabLctB + ' la');
  QrAtiCts.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrAtiCts.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
  QrAtiCts.SQL.Add('WHERE la.Genero>0');
  QrAtiCts.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrAtiCts.SQL.Add('AND co.Exclusivo="F"');
    1: QrAtiCts.SQL.Add('');
    2: QrAtiCts.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrAtiCts.SQL.Add('AND la.CliInt=' + EntiTxt);

  QrAtiCts.SQL.Add('UNION');

  QrAtiCts.SQL.Add('SELECT DISTINCT');
  QrAtiCts.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
  QrAtiCts.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero, co.Exclusivo');
  QrAtiCts.SQL.Add('FROM ' + FTabLctD + ' la');
  QrAtiCts.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrAtiCts.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
  QrAtiCts.SQL.Add('WHERE la.Genero>0');
  QrAtiCts.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrAtiCts.SQL.Add('AND co.Exclusivo="F"');
    1: QrAtiCts.SQL.Add('');
    2: QrAtiCts.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrAtiCts.SQL.Add('AND la.CliInt=' + EntiTxt);

  QrAtiCts.SQL.Add(';');
  QrAtiCts.SQL.Add('SELECT DISTINCT *');
  QrAtiCts.SQL.Add('FROM _ati_cts_');
  QrAtiCts.SQL.Add('ORDER BY TipoAgrupa, OrdemLista, NomeSubGrupo, NomeConta;');
  QrAtiCts . O p e n ;
*)
end;

procedure TFmResMes.ReopenResMesF(Ax: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrResMesF, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FResMes,
  'WHERE ((AnoAn+Mes01+Mes02+Mes03+Mes04+Mes05+Mes06+ ',
  'Mes07+Mes08+Mes09+Mes10+Mes11+Mes12+Pendencias) <>0) ',
  'AND Exclusivo="F" ',
  Geral.ATS_if(Ax = 43, ['AND Genero <> 0']), // Ocultar Saldo Atual!
  'ORDER BY TipoAgrupa, OrdemLista, NomeSubGrupo, SubOrdem, NomeConta ',
  '']);
end;

procedure TFmResMes.ReopenTSG(SubGrupo: Integer);
begin
(*
  QrTSG.Close;
  QrTSG.SQL.Clear;
  QrTSG.SQL.Add('SELECT rm.SubGrupo, SUM(rm.AnoAn) T00, SUM(rm.Mes01) T01,');
  QrTSG.SQL.Add('SUM(rm.Mes02) T02, SUM(rm.Mes03) T03, SUM(rm.Mes04) T04,');
  QrTSG.SQL.Add('SUM(rm.Mes05) T05, SUM(rm.Mes06) T06, SUM(rm.Mes07) T07,');
  QrTSG.SQL.Add('SUM(rm.Mes08) T08, SUM(rm.Mes09) T09, SUM(rm.Mes10) T10,');
  QrTSG.SQL.Add('SUM(rm.Mes11) T11, SUM(rm.Mes12) T12, SUM(rm.Pendencias) TPN');
  QrTSG.SQL.Add('FROM '+FResMes+' rm');
  QrTSG.SQL.Add('WHERE SubOrdem=0');
  QrTSG.SQL.Add('AND SubGrupo=' + FormatFloat('0', SubGrupo));
  QrTSG.SQL.Add('GROUP BY rm.SubGrupo');
  QrTSG . O p e n ;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrTSG, DModG.MyPID_DB, [
  'SELECT rm.SubGrupo, SUM(rm.AnoAn) T00, SUM(rm.Mes01) T01, ',
  'SUM(rm.Mes02) T02, SUM(rm.Mes03) T03, SUM(rm.Mes04) T04, ',
  'SUM(rm.Mes05) T05, SUM(rm.Mes06) T06, SUM(rm.Mes07) T07, ',
  'SUM(rm.Mes08) T08, SUM(rm.Mes09) T09, SUM(rm.Mes10) T10, ',
  'SUM(rm.Mes11) T11, SUM(rm.Mes12) T12, SUM(rm.Pendencias) TPN, ',
  'rm.TipoAgrupa ',
  'FROM ' + FResMes + ' rm ',
  'WHERE SubOrdem=0 ',
  'AND SubGrupo=' + Geral.FF0(SubGrupo),
  'GROUP BY rm.SubGrupo ',
  '']);
end;

procedure TFmResMes.ResultadoGeral(Periodo: Integer; PB_1, PB_2, PB_3: TProgressBar;
LaSub_1, LaSub_2, LaSub_3: TLabel);
var
  MeuID: Integer;
  function NextID(): Integer;
  begin
    MeuID := MeuID + 1;
    Result := MeuID;
  end;
var
  i, AntTipoAgrupa, AntOrdemLista, AtuSubGrupo, AntSubGrupo: Integer;
  InsereSaldoAtual: Boolean;
  Valor: Double;
  AntNomeSubGrupo, AntNomeConta, AntExclusivo, PerIni, PerFim, PerSeq,
  EntiTxt, NomeTab, SGCodigo: String;
  Query: TmySQLQuery;
  //
  Exclusivo, NomeConta, NomeGrupo, NomeSubGrupo, AntNomeGrupo: String;
  TipoAgrupa, OrdemLista, SubOrdem, Grupo, SubGrupo, Genero, SeqID,
  AntGrupo, AtuGrupo: Integer;
begin
  MeuID := 0;
  if FEntidade = 0 then
  begin
    Geral.MB_Aviso('Empresa (Cliente Interno) n�o definido!' +
    sLineBreak + 'Gera��o de resultados gerais cancelada!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  //UCriar.RecriaTempTable('CtasResMes', DModG.QrUpdPID1, False);
  DmodFin.FCtasResMes :=
    UCriarFin.RecriaTempTableNovo(ntrtt_CtasResMes, DMOdG.QrUpdPID1, False);
  EntiTxt := FormatFloat('0', FEntidade);
  PerIni  := FormatFloat('0', Periodo-12);
  PerFim  := FormatFloat('0', Periodo-1);
  // predisa quando chama pelo CashBal !!!
  FPeriodo := Periodo + 11;
  //
  AtuSubGrupo := 0;
  AtuGrupo := 0;
  AntTipoAgrupa := 0;
  AntOrdemLista := 0;
  AntSubGrupo := 0;
  AntGrupo := 0;
  InsereSaldoAtual := False;
  //
  TbResMes.Close;
  PB_1.Position := 0;
  PB_2.Position := 0;
  PB_3.Position := 0;
  //
(*
  QrAtiCts.Close;
  // � preciso todas contas por causa do saldo anterior
  QrAtiCts.SQL.Clear;
  QrAtiCts.SQL.Add('DROP TABLE IF EXISTS _ati_cts_;');
  QrAtiCts.SQL.Add('CREATE TABLE _ati_cts_');
  QrAtiCts.SQL.Add('');
  QrAtiCts.SQL.Add('SELECT DISTINCT');
  QrAtiCts.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
  QrAtiCts.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero, co.Exclusivo');
  QrAtiCts.SQL.Add('FROM ' + FTabLctA + ' la');
  QrAtiCts.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrAtiCts.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
  QrAtiCts.SQL.Add('WHERE la.Genero>0');
  QrAtiCts.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrAtiCts.SQL.Add('AND co.Exclusivo="F"');
    1: QrAtiCts.SQL.Add('');
    2: QrAtiCts.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrAtiCts.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrAtiCts.SQL.Add('UNION');
  QrAtiCts.SQL.Add('SELECT DISTINCT');
  QrAtiCts.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
  QrAtiCts.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero, co.Exclusivo');
  QrAtiCts.SQL.Add('FROM ' + FTabLctB + ' la');
  QrAtiCts.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrAtiCts.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
  QrAtiCts.SQL.Add('WHERE la.Genero>0');
  QrAtiCts.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrAtiCts.SQL.Add('AND co.Exclusivo="F"');
    1: QrAtiCts.SQL.Add('');
    2: QrAtiCts.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrAtiCts.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrAtiCts.SQL.Add('UNION');
  QrAtiCts.SQL.Add('SELECT DISTINCT');
  QrAtiCts.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
  QrAtiCts.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero, co.Exclusivo');
  QrAtiCts.SQL.Add('FROM ' + FTabLctD + ' la');
  QrAtiCts.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrAtiCts.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
  QrAtiCts.SQL.Add('WHERE la.Genero>0');
  QrAtiCts.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrAtiCts.SQL.Add('AND co.Exclusivo="F"');
    1: QrAtiCts.SQL.Add('');
    2: QrAtiCts.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrAtiCts.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrAtiCts.SQL.Add(';');
  QrAtiCts.SQL.Add('SELECT DISTINCT *');
  QrAtiCts.SQL.Add('FROM _ati_cts_');
  QrAtiCts.SQL.Add('ORDER BY TipoAgrupa, OrdemLista, NomeSubGrupo, NomeConta;');
  QrAtiCts . O p e n ;
*)
  ReopenAtiCts(EntiTxt);
  //
  MyObjects.Informa(LaSub_2, True, 'Verificando saldos atuais de contas ativas');
  PB_2.Max := QrAtiCts.RecordCount;
  Update;
  Application.ProcessMessages;
  //FResMes := UCriar.RecriaTempTable('ResMes', DModG.QrUpdPID1, False);
  FResMes :=
    UCriarFin.RecriaTempTableNovo(ntrtt_ResMes, DMOdG.QrUpdPID1, False);
  DModG.QrUpdPID1.SQL.Clear;
  (*
  DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FResMes + ' SET');
  DModG.QrUpdPID1.SQL.Add('Exclusivo    =:P00, ');
  DModG.QrUpdPID1.SQL.Add('TipoAgrupa   =:P01, ');
  DModG.QrUpdPID1.SQL.Add('OrdemLista   =:P02, ');
  DModG.QrUpdPID1.SQL.Add('SubOrdem     =:P03, ');
  DModG.QrUpdPID1.SQL.Add('SubGrupo     =:P04, ');
  DModG.QrUpdPID1.SQL.Add('NomeSubGrupo =:P05, ');
  DModG.QrUpdPID1.SQL.Add('Genero       =:P06, ');
  DModG.QrUpdPID1.SQL.Add('NomeConta    =:P07, ');
  DModG.QrUpdPID1.SQL.Add('SeqID        =:P08 ');
  *)
  while not QrAtiCts.Eof do
  begin
    PB_2.Position := PB_2.Position + 1;
    if (InsereSaldoAtual = True) and (AtuSubGrupo <> QrAtiCtsSubGrupo.Value) then
    begin
      InsereSaldoAtual := False;
      //
      (*
      DModG.QrUpdPID1.Params[00].AsString  := AntExclusivo;
      DModG.QrUpdPID1.Params[01].AsInteger := AntTipoAgrupa;
      DModG.QrUpdPID1.Params[02].AsInteger := AntOrdemLista;
      DModG.QrUpdPID1.Params[03].AsInteger := 1;
      DModG.QrUpdPID1.Params[04].AsInteger := AntSubGrupo;
      DModG.QrUpdPID1.Params[05].AsString  := AntNOMESUBGRUPO;
      DModG.QrUpdPID1.Params[06].AsInteger := 0;//AntGenero;
      DModG.QrUpdPID1.Params[07].AsString  := 'Saldo atual';
      DModG.QrUpdPID1.Params[08].AsInteger := NextID();
      DModG.QrUpdPID1.ExecSQL;
      *)
      Exclusivo    := AntExclusivo;
      TipoAgrupa   := AntTipoAgrupa;
      OrdemLista   := AntOrdemLista;
      SubOrdem     := 1;
      SubGrupo     := AntSubGrupo;
      NomeSubGrupo := AntNomeSubGrupo;
      Genero       := 0;//AntGenero;
      NomeConta    := 'Saldo atual';
      SeqID        := NextID();
      Grupo        := AntGrupo;
      NomeGrupo    := AntNomeGrupo;
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FResMes, False, [
      'Exclusivo', 'TipoAgrupa', 'OrdemLista',
      'SubOrdem', 'SubGrupo', 'NomeSubGrupo',
      'Genero', 'NomeConta', 'Grupo',
      'NomeGrupo'
      ], ['SeqID'], [
      Exclusivo, TipoAgrupa, OrdemLista,
      SubOrdem, SubGrupo, NomeSubGrupo,
      Genero, NomeConta, Grupo,
      NomeGrupo
      ], [SeqID], False);
    end;
    (*
    DModG.QrUpdPID1.Params[00].AsString  := QrAtiCtsExclusivo.Value;
    DModG.QrUpdPID1.Params[01].AsInteger := QrAtiCtsTipoAgrupa.Value;
    DModG.QrUpdPID1.Params[02].AsInteger := QrAtiCtsOrdemLista.Value;
    DModG.QrUpdPID1.Params[03].AsInteger := 0;
    DModG.QrUpdPID1.Params[04].AsInteger := QrAtiCtsSubGrupo.Value;
    DModG.QrUpdPID1.Params[05].AsString  := QrAtiCtsNOMESUBGRUPO.Value;
    DModG.QrUpdPID1.Params[06].AsInteger := QrAtiCtsGenero.Value;
    DModG.QrUpdPID1.Params[07].AsString  := QrAtiCtsNOMECONTA.Value;
    DModG.QrUpdPID1.Params[08].AsInteger := NextID();
    DModG.QrUpdPID1.ExecSQL;
    *)
    Exclusivo    := QrAtiCtsExclusivo.Value;
    TipoAgrupa   := QrAtiCtsTipoAgrupa.Value;
    OrdemLista   := QrAtiCtsOrdemLista.Value;
    SubOrdem     := 0;
    SubGrupo     := QrAtiCtsSubGrupo.Value;
    NomeSubGrupo := QrAtiCtsNOMESUBGRUPO.Value;
    Genero       := QrAtiCtsGenero.Value;
    NomeConta    := QrAtiCtsNOMECONTA.Value;
    SeqID        := NextID();
    Grupo        := QrAtiCtsGrupo.Value;
    NomeGrupo    := QrAtiCtsNomeGRUPO.Value;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FResMes, False, [
    'Exclusivo', 'TipoAgrupa', 'OrdemLista',
    'SubOrdem', 'SubGrupo', 'NomeSubGrupo',
    'Genero', 'NomeConta', 'Grupo',
    'NomeGrupo'
    ], ['SeqID'], [
    Exclusivo, TipoAgrupa, OrdemLista,
    SubOrdem, SubGrupo, NomeSubGrupo,
    Genero, NomeConta, Grupo,
    NomeGrupo
    ], [SeqID], False);
    if QrAtiCtsTipoAgrupa.Value = 2 then
    begin
      InsereSaldoAtual := True;
      AntExclusivo     := QrAtiCtsExclusivo.Value;
      AntTipoAgrupa    := QrAtiCtsTipoAgrupa.Value;
      AntOrdemLista    := QrAtiCtsOrdemLista.Value;
      AntSubGrupo      := QrAtiCtsSubGrupo.Value;
      //AntGenero        := QrAtiCtsGenero.Value;
      AntNomeSubGrupo  := QrAtiCtsNOMESUBGRUPO.Value;
      AntNomeConta     := QrAtiCtsNOMECONTA.Value;
      AntGrupo         := QrAtiCtsGrupo.Value;
      AntNomeGrupo     := QrAtiCtsNomeGRUPO.Value;
    end;
    AtuSubGrupo := QrAtiCtsSubGrupo.Value;
    AtuGrupo    := QrAtiCtsGrupo.Value;
    //
    QrAtiCts.Next;
  end;
  MyObjects.Informa(LaSub_2, False, '...');
  Update;
  Application.ProcessMessages;
  if InsereSaldoAtual = True then
  begin
    (*
    DModG.QrUpdPID1.Params[00].AsString  := AntExclusivo;
    DModG.QrUpdPID1.Params[01].AsInteger := AntTipoAgrupa;
    DModG.QrUpdPID1.Params[02].AsInteger := AntOrdemLista;
    DModG.QrUpdPID1.Params[03].AsInteger := 1;
    DModG.QrUpdPID1.Params[04].AsInteger := AntSubGrupo;
    DModG.QrUpdPID1.Params[05].AsString  := AntNOMESUBGRUPO;
    DModG.QrUpdPID1.Params[06].AsInteger := 0;//AntGenero;
    DModG.QrUpdPID1.Params[07].AsString  := 'Saldo atual';
    DModG.QrUpdPID1.Params[08].AsInteger := NextID();
    DModG.QrUpdPID1.ExecSQL;
    *)
    Exclusivo    := AntExclusivo;
    TipoAgrupa   := AntTipoAgrupa;
    OrdemLista   := AntOrdemLista;
    SubOrdem     := 1;
    SubGrupo     := AntSubGrupo;
    NomeSubGrupo := AntNOMESUBGRUPO;
    Genero       := 0;//AntGenero;
    NomeConta    := 'Saldo atual';
    SeqID        := NextID();
    Grupo        := AntGrupo;
    NomeGrupo    := AntNomeGrupo;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FResMes, False, [
    'Exclusivo', 'TipoAgrupa', 'OrdemLista',
    'SubOrdem', 'SubGrupo', 'NomeSubGrupo',
    'Genero', 'NomeConta', 'Grupo',
    'NomeGrupo'
    ], ['SeqID'], [
    Exclusivo, TipoAgrupa, OrdemLista,
    SubOrdem, SubGrupo, NomeSubGrupo,
    Genero, NomeConta, Grupo,
    NomeGrupo
    ], [SeqID], False);
  end;
  //
  PB_1.Position := 0;
  PB_2.Position := 0;
  PB_3.Position := 0;
  MyObjects.Informa(LaSub_1, True, 'Verificando dados anteriores...');
  PB_1.Max := 13;
  Update;
  Application.ProcessMessages;
  //////////////////////////////////////////////////////////////////////////////
  PB_1.Position := PB_1.Position +1;
  QrAnt.Close;
{
SELECT
sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,
co.SubGrupo, co.Nome NOMECONTA, la.Genero,
SUM(la.Credito-la.Debito) Valor
FROM lct la
LEFT JOIN contas    co ON co.Codigo=la.Genero
LEFT JOIN subgrupos sg ON sg.Codigo=co.SubGrupo
WHERE la.Genero>0
AND la.Tipo <> 2
AND co.Exclusivo="F"
AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN :P0 AND :P1
AND la.CliInt=:P2
GROUP BY la.Genero
ORDER BY sg.TipoAgrupa, sg.OrdemLista, NOMESUBGRUPO, NOMECONTA

}
  QrAnt.SQL.Clear;
{
  QrAnt.SQL.Add('SELECT');
  QrAnt.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
  QrAnt.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero,');
  QrAnt.SQL.Add('SUM(la.Credito-la.Debito) Valor');
  QrAnt.SQL.Add('FROM lct la');
  QrAnt.SQL.Add('LEFT JOIN contas    co ON co.Codigo=la.Genero');
  QrAnt.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=co.SubGrupo');
  QrAnt.SQL.Add('WHERE la.Genero>0');
  QrAnt.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrAnt.SQL.Add('AND co.Exclusivo="F"');
    1: QrAnt.SQL.Add('');
    2: QrAnt.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrAnt.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
    PerIni + ' AND ' + PerFim);
  QrAnt.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrAnt.SQL.Add('GROUP BY la.Genero');
  QrAnt.SQL.Add('ORDER BY sg.TipoAgrupa, sg.OrdemLista, NOMESUBGRUPO, NOMECONTA');
}
  QrAnt.SQL.Add('DROP TABLE IF EXISTS _ant_cts_;');
  QrAnt.SQL.Add('CREATE TABLE _ant_cts_');
  QrAnt.SQL.Add('');
  QrAnt.SQL.Add('SELECT');
  QrAnt.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
  QrAnt.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero,');
  QrAnt.SQL.Add('SUM(la.Credito-la.Debito) Valor');
  QrAnt.SQL.Add('FROM ' + FTabLctA + ' la');
  QrAnt.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrAnt.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
  QrAnt.SQL.Add('WHERE la.Genero>0');
  QrAnt.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrAnt.SQL.Add('AND co.Exclusivo="F"');
    1: QrAnt.SQL.Add('');
    2: QrAnt.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrAnt.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
                 PerIni + ' AND ' + PerFim);
  QrAnt.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrAnt.SQL.Add('GROUP BY la.Genero');
  QrAnt.SQL.Add('UNION');
  QrAnt.SQL.Add('SELECT');
  QrAnt.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
  QrAnt.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero,');
  QrAnt.SQL.Add('SUM(la.Credito-la.Debito) Valor');
  QrAnt.SQL.Add('FROM ' + FTabLctB + ' la');
  QrAnt.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrAnt.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
  QrAnt.SQL.Add('WHERE la.Genero>0');
  QrAnt.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrAnt.SQL.Add('AND co.Exclusivo="F"');
    1: QrAnt.SQL.Add('');
    2: QrAnt.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrAnt.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
                 PerIni + ' AND ' + PerFim);
  QrAnt.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrAnt.SQL.Add('GROUP BY la.Genero');
  QrAnt.SQL.Add('UNION');
  QrAnt.SQL.Add('SELECT');
  QrAnt.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
  QrAnt.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero,');
  QrAnt.SQL.Add('SUM(la.Credito-la.Debito) Valor');
  QrAnt.SQL.Add('FROM ' + FTabLctD + ' la');
  QrAnt.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrAnt.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
  QrAnt.SQL.Add('WHERE la.Genero>0');
  QrAnt.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrAnt.SQL.Add('AND co.Exclusivo="F"');
    1: QrAnt.SQL.Add('');
    2: QrAnt.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrAnt.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
                 PerIni + ' AND ' + PerFim);
  QrAnt.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrAnt.SQL.Add('GROUP BY la.Genero');
  QrAnt.SQL.Add(';');
  QrAnt.SQL.Add('SELECT NOMESUBGRUPO, TipoAgrupa, OrdemLista,');
  QrAnt.SQL.Add('SubGrupo, NOMECONTA, Genero,  SUM(Valor) Valor');
  QrAnt.SQL.Add('FROM _ant_cts_');
  QrAnt.SQL.Add('GROUP BY Genero');
  QrAnt.SQL.Add('ORDER BY TipoAgrupa, OrdemLista, NOMESUBGRUPO, NOMECONTA');
  {
  QrAnt.Params[00].AsInteger := Periodo-12;
  QrAnt.Params[01].AsInteger := Periodo-1;
  QrAnt.Params[02].AsInteger := FEntidade;
  }
  //QrAnt . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrAnt, DModG.MyPID_DB);
  MyObjects.Informa(LaSub_2, True, 'Atualizando saldos anteriores...');
  PB_2.Max := QrAnt.RecordCount;
  PB_2.Position := 0;
  Update;
  Application.ProcessMessages;
  while not QrAnt.Eof do
  begin
    PB_2.Position := PB_2.Position + 1;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE '+FResMes+' SET');
    DModG.QrUpdPID1.SQL.Add('AnoAn=:P0');
    DModG.QrUpdPID1.SQL.Add('WHERE Genero=:P1 AND SubOrdem=0');
    DModG.QrUpdPID1.Params[0].AsFloat   := QrAntValor.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := QrAntGenero.Value;
    DModG.QrUpdPID1.ExecSQL;
    QrAnt.Next;
  end;
  MyObjects.Informa(LaSub_1, True, 'Verificando dados atuais...');
  MyObjects.Informa(LaSub_2, False, '...');
  Update;
  Application.ProcessMessages;
  //////////////////////////////////////////////////////////////////////////////
  for i := 0 to 11 do
  begin
    PB_1.Position := PB_1.Position+1;
    PerSeq := FormatFloat('0', Periodo + i);
    QrRes.Close;
{
SELECT
sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,
co.SubGrupo, co.Nome NOMECONTA, la.Genero,
SUM(la.Credito-la.Debito) Valor
FROM lct la
LEFT JOIN contas    co ON co.Codigo=la.Genero
LEFT JOIN subgrupos sg ON sg.Codigo=co.SubGrupo
WHERE la.Genero>0
AND la.Tipo <> 2
AND co.Exclusivo="F"
AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=:P0
AND la.CliInt=:P1
GROUP BY la.Genero
ORDER BY sg.TipoAgrupa, sg.OrdemLista, NOMESUBGRUPO, NOMECONTA
}
    QrRes.SQL.Clear;
{
    QrRes.SQL.Add('SELECT');
    QrRes.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
    QrRes.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero,');
    QrRes.SQL.Add('SUM(la.Credito-la.Debito) Valor');
    QrRes.SQL.Add('FROM lct la');
    QrRes.SQL.Add('LEFT JOIN contas    co ON co.Codigo=la.Genero');
    QrRes.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=co.SubGrupo');
    QrRes.SQL.Add('WHERE la.Genero>0');
    QrRes.SQL.Add('AND la.Tipo <> 2');
    case RGExclusivos.ItemIndex of
      0: QrRes.SQL.Add('AND co.Exclusivo="F"');
      1: QrRes.SQL.Add('');
      2: QrRes.SQL.Add('AND co.Exclusivo="V"');
    end;
    QrRes.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' + PerSeq);
    QrRes.SQL.Add('AND la.CliInt=' + EntiTxt);
    QrRes.SQL.Add('GROUP BY la.Genero');
    QrRes.SQL.Add('ORDER BY sg.TipoAgrupa, sg.OrdemLista, NOMESUBGRUPO, NOMECONTA');
    QrRes.SQL.Add('');
}
{
    QrRes.Params[00].AsInteger := Periodo+i;
    QrRes.Params[01].AsInteger := FEntidade;
}
    QrRes.SQL.Add('DROP TABLE IF EXISTS _res_cts_;');
    QrRes.SQL.Add('CREATE TABLE _res_cts_');
    QrRes.SQL.Add('');
    QrRes.SQL.Add('SELECT');
    QrRes.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
    QrRes.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero,');
    QrRes.SQL.Add('SUM(la.Credito-la.Debito) Valor');
    QrRes.SQL.Add('FROM ' + FTabLctA + ' la');
    QrRes.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
    QrRes.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
    QrRes.SQL.Add('WHERE la.Genero>0');
    QrRes.SQL.Add('AND la.Tipo <> 2');
    case RGExclusivos.ItemIndex of
      0: QrRes.SQL.Add('AND co.Exclusivo="F"');
      1: QrRes.SQL.Add('');
      2: QrRes.SQL.Add('AND co.Exclusivo="V"');
    end;
    QrRes.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' + PerSeq);
    QrRes.SQL.Add('AND la.CliInt=' + EntiTxt);
    QrRes.SQL.Add('GROUP BY la.Genero');
    QrRes.SQL.Add('UNION');
    QrRes.SQL.Add('SELECT');
    QrRes.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
    QrRes.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero,');
    QrRes.SQL.Add('SUM(la.Credito-la.Debito) Valor');
    QrRes.SQL.Add('FROM ' + FTabLctB + ' la');
    QrRes.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
    QrRes.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
    QrRes.SQL.Add('WHERE la.Genero>0');
    QrRes.SQL.Add('AND la.Tipo <> 2');
    case RGExclusivos.ItemIndex of
      0: QrRes.SQL.Add('AND co.Exclusivo="F"');
      1: QrRes.SQL.Add('');
      2: QrRes.SQL.Add('AND co.Exclusivo="V"');
    end;
    QrRes.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' + PerSeq);
    QrRes.SQL.Add('AND la.CliInt=' + EntiTxt);
    QrRes.SQL.Add('GROUP BY la.Genero');
    QrRes.SQL.Add('UNION');
    QrRes.SQL.Add('SELECT');
    QrRes.SQL.Add('sg.Nome NOMESUBGRUPO, sg.TipoAgrupa, sg.OrdemLista,');
    QrRes.SQL.Add('co.SubGrupo, co.Nome NOMECONTA, la.Genero,');
    QrRes.SQL.Add('SUM(la.Credito-la.Debito) Valor');
    QrRes.SQL.Add('FROM ' + FTabLctD + ' la');
    QrRes.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
    QrRes.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
    QrRes.SQL.Add('WHERE la.Genero>0');
    QrRes.SQL.Add('AND la.Tipo <> 2');
    case RGExclusivos.ItemIndex of
      0: QrRes.SQL.Add('AND co.Exclusivo="F"');
      1: QrRes.SQL.Add('');
      2: QrRes.SQL.Add('AND co.Exclusivo="V"');
    end;
    QrRes.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' + PerSeq);
    QrRes.SQL.Add('AND la.CliInt=' + EntiTxt);
    QrRes.SQL.Add('GROUP BY la.Genero');
    QrRes.SQL.Add(';');
    QrRes.SQL.Add('SELECT NOMESUBGRUPO, TipoAgrupa, OrdemLista,');
    QrRes.SQL.Add('SubGrupo, NOMECONTA, Genero,  SUM(Valor) Valor');
    QrRes.SQL.Add('FROM _res_cts_');
    QrRes.SQL.Add('GROUP BY Genero');
    QrRes.SQL.Add('ORDER BY TipoAgrupa, OrdemLista, NOMESUBGRUPO, NOMECONTA');
    //QrRes . O p e n ;
    UnDmkDAC_PF.AbreQuery(QrRes, DModG.MyPID_DB);
    PB_2.Max := QrRes.RecordCount;
    PB_2.Position := 0;
    MyObjects.Informa(LaSub_2, True, 'Atualizando saldos do ' + IntToStr(i+1) + '� m�s ...');
    Update;
    Application.ProcessMessages;
    while not QrRes.Eof do
    begin
      PB_2.Position := PB_2.Position + 1;
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('UPDATE '+FResMes+' SET');
      DModG.QrUpdPID1.SQL.Add('Mes'+FormatFloat('00', i+1)+'=:P0');
      DModG.QrUpdPID1.SQL.Add('WHERE Genero=:P1 AND SubOrdem=0');
      DModG.QrUpdPID1.Params[0].AsFloat   := QrResValor.Value;
      DModG.QrUpdPID1.Params[1].AsInteger := QrResGenero.Value;
      DModG.QrUpdPID1.ExecSQL;
      QrRes.Next;
    end;
  end;
  MyObjects.Informa(LaSub_2, False, '...');
  MyObjects.Informa(LaSub_1, False, '...');
  Update;
  Application.ProcessMessages;
  QrA00.Close;
{
SELECT
SUM(la.Debito) Debito,
SUM(la.Credito) Credito,
SUM(la.Credito-la.Debito) Saldo
FROM lct la
LEFT JOIN contas    co ON co.Codigo=la.Genero
WHERE la.Genero>0
AND la.Tipo <> 2
AND co.Exclusivo="F"
AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) < :P0

AND la.CliInt=:P1

}
{
  // Buscar dados s� da tabela em quest�o?
  Data :=  M L A G e r  a l .PeriodoToDate(Periodo - 12, 1);
  TabLct := DefineLctTabela(Data);
  TipoTabToWork := DefineLctTipo(Data);
  //
  case TipoTabToWork of
    tlwA:
    begin

    end;
    else begin

    end;
  end;
}

  QrA00.SQL.Clear;
{
  QrA00.SQL.Add('SELECT');
  QrA00.SQL.Add('SUM(la.Debito) Debito,');
  QrA00.SQL.Add('SUM(la.Credito) Credito,');
  QrA00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrA00.SQL.Add('FROM lct la');
  QrA00.SQL.Add('LEFT JOIN contas    co ON co.Codigo=la.Genero');
  QrA00.SQL.Add('WHERE la.Genero>0');
  QrA00.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrA00.SQL.Add('AND co.Exclusivo="F"');
    1: QrA00.SQL.Add('');
    2: QrA00.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrA00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
  QrA00.SQL.Add('');
  QrA00.SQL.Add('AND la.CliInt=' + EntiTxt);
  {
  QrA00.Params[00].AsInteger := Periodo-12; // PerIni
  QrA00.Params[01].AsString  := EntiTxt;
  }
  QrA00.SQL.Add('DROP TABLE IF EXISTS _a00_cts_;');
  QrA00.SQL.Add('CREATE TABLE _a00_cts_');
  QrA00.SQL.Add('');
  QrA00.SQL.Add('SELECT');
  QrA00.SQL.Add('SUM(la.Debito) Debito,');
  QrA00.SQL.Add('SUM(la.Credito) Credito,');
  QrA00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrA00.SQL.Add('FROM ' + FTabLctA + ' la');
  QrA00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrA00.SQL.Add('WHERE la.Genero>0');
  QrA00.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrA00.SQL.Add('AND co.Exclusivo="F"');
    1: QrA00.SQL.Add('');
    2: QrA00.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrA00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
  QrA00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrA00.SQL.Add('UNION');
  QrA00.SQL.Add('SELECT');
  QrA00.SQL.Add('SUM(la.Debito) Debito,');
  QrA00.SQL.Add('SUM(la.Credito) Credito,');
  QrA00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrA00.SQL.Add('FROM ' + FTabLctB + ' la');
  QrA00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrA00.SQL.Add('WHERE la.Genero>0');
  QrA00.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrA00.SQL.Add('AND co.Exclusivo="F"');
    1: QrA00.SQL.Add('');
    2: QrA00.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrA00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
  QrA00.SQL.Add('AND la.CliInt=' + EntiTxt);
  // N�o pega saldos iniciais da tabel�a B!
  QrA00.SQL.Add('UNION');
  QrA00.SQL.Add('SELECT');
  QrA00.SQL.Add('SUM(la.Debito) Debito,');
  QrA00.SQL.Add('SUM(la.Credito) Credito,');
  QrA00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrA00.SQL.Add('FROM ' + FTabLctD + ' la');
  QrA00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrA00.SQL.Add('WHERE la.Genero>0');
  QrA00.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrA00.SQL.Add('AND co.Exclusivo="F"');
    1: QrA00.SQL.Add('');
    2: QrA00.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrA00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
  QrA00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrA00.SQL.Add(';');
  QrA00.SQL.Add('SELECT SUM(Debito) Debito, SUM(Credito) Credito,  SUM(Saldo) Saldo');
  QrA00.SQL.Add('FROM _a00_cts_;');

  //QrA00 . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrA00, DModG.MyPID_DB);
  //
  QrB00.Close;
{
SELECT
SUM(la.Debito) Debito,
SUM(la.Credito) Credito,
SUM(la.Credito-la.Debito) Saldo
FROM lctla
LEFT JOIN contas    co ON co.Codigo=la.Genero
WHERE la.Genero>0
AND la.Tipo <> 2
AND co.Exclusivo="F"
AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) < :P0
AND la.CliInt=:P1
}
  QrB00.SQL.Clear;
{  QrB00.SQL.Add('SELECT');
  QrB00.SQL.Add('SUM(la.Debito) Debito,');
  QrB00.SQL.Add('SUM(la.Credito) Credito,');
  QrB00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrB00.SQL.Add('FROM lct la');
  QrB00.SQL.Add('LEFT JOIN contas    co ON co.Codigo=la.Genero');
  QrB00.SQL.Add('WHERE la.Genero>0');
  QrB00.SQL.Add('AND la.Tipo <> 2');
  QrB00.SQL.Add('AND co.Exclusivo="F"');
  QrB00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
  QrB00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrB00.Params[00].AsInteger := Periodo-12; // PerIni
  QrB00.Params[01].AsString  := EntiTxt;
}
  QrB00.SQL.Add('DROP TABLE IF EXISTS _b00_cts_;');
  QrB00.SQL.Add('CREATE TABLE _b00_cts_');
  QrB00.SQL.Add('');
  QrB00.SQL.Add('SELECT');
  QrB00.SQL.Add('SUM(la.Debito) Debito,');
  QrB00.SQL.Add('SUM(la.Credito) Credito,');
  QrB00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrB00.SQL.Add('FROM ' + FTabLctA + ' la');
  QrB00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrB00.SQL.Add('WHERE la.Genero>0');
  QrB00.SQL.Add('AND la.Tipo <> 2');
  QrB00.SQL.Add('AND co.Exclusivo="F"');
  QrB00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
  QrB00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrB00.SQL.Add('UNION');
  QrB00.SQL.Add('SELECT');
  QrB00.SQL.Add('SUM(la.Debito) Debito,');
  QrB00.SQL.Add('SUM(la.Credito) Credito,');
  QrB00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrB00.SQL.Add('FROM ' + FTabLctB + ' la');
  QrB00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrB00.SQL.Add('WHERE la.Genero>0');
  QrB00.SQL.Add('AND la.Tipo <> 2');
  QrB00.SQL.Add('AND co.Exclusivo="F"');
  QrB00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
  QrB00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrB00.SQL.Add('UNION');
  QrB00.SQL.Add('SELECT');
  QrB00.SQL.Add('SUM(la.Debito) Debito,');
  QrB00.SQL.Add('SUM(la.Credito) Credito,');
  QrB00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrB00.SQL.Add('FROM ' + FTabLctD + ' la');
  QrB00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrB00.SQL.Add('WHERE la.Genero>0');
  QrB00.SQL.Add('AND la.Tipo <> 2');
  QrB00.SQL.Add('AND co.Exclusivo="F"');
  QrB00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
  QrB00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrB00.SQL.Add(';');
  QrB00.SQL.Add('SELECT SUM(Debito) Debito, SUM(Credito) Credito,  SUM(Saldo) Saldo');
  QrB00.SQL.Add('FROM _b00_cts_;');
  //QrB00 . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrB00, DModG.MyPID_DB);
  //
  QrC00.Close;
{
SELECT
SUM(la.Debito) Debito,
SUM(la.Credito) Credito,
SUM(la.Credito-la.Debito) Saldo
FROM lct la
LEFT JOIN contas    co ON co.Codigo=la.Genero
WHERE la.Genero>0
AND la.Tipo <> 2
AND co.Exclusivo="V"
AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) < :P0
AND la.CliInt=:P1
}
  QrC00.SQL.Clear;
{
  QrC00.SQL.Add('SELECT');
  QrC00.SQL.Add('SUM(la.Debito) Debito,');
  QrC00.SQL.Add('SUM(la.Credito) Credito,');
  QrC00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrC00.SQL.Add('FROM lct la');
  QrC00.SQL.Add('LEFT JOIN contas    co ON co.Codigo=la.Genero');
  QrC00.SQL.Add('WHERE la.Genero>0');
  QrC00.SQL.Add('AND la.Tipo <> 2');
  QrC00.SQL.Add('AND co.Exclusivo="V"');
  QrC00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
  QrC00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrC00.Params[00].AsInteger := Periodo-12;
  QrC00.Params[01].AsString  := EntiTxt;
  }
  QrC00.SQL.Add('DROP TABLE IF EXISTS _c00_cts_;');
  QrC00.SQL.Add('CREATE TABLE _c00_cts_');
  QrC00.SQL.Add('');
  QrC00.SQL.Add('SELECT');
  QrC00.SQL.Add('SUM(la.Debito) Debito,');
  QrC00.SQL.Add('SUM(la.Credito) Credito,');
  QrC00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrC00.SQL.Add('FROM ' + FTabLctA + ' la');
  QrC00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrC00.SQL.Add('WHERE la.Genero>0');
  QrC00.SQL.Add('AND la.Tipo <> 2');
  QrC00.SQL.Add('AND co.Exclusivo="V"');
  QrC00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
  QrC00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrC00.SQL.Add('UNION');
  QrC00.SQL.Add('SELECT');
  QrC00.SQL.Add('SUM(la.Debito) Debito,');
  QrC00.SQL.Add('SUM(la.Credito) Credito,');
  QrC00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrC00.SQL.Add('FROM ' + FTabLctB + ' la');
  QrC00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrC00.SQL.Add('WHERE la.Genero>0');
  QrC00.SQL.Add('AND la.Tipo <> 2');
  QrC00.SQL.Add('AND co.Exclusivo="V"');
  QrC00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
  QrC00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrC00.SQL.Add('UNION');
  QrC00.SQL.Add('SELECT');
  QrC00.SQL.Add('SUM(la.Debito) Debito,');
  QrC00.SQL.Add('SUM(la.Credito) Credito,');
  QrC00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrC00.SQL.Add('FROM ' + FTabLctD + ' la');
  QrC00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrC00.SQL.Add('WHERE la.Genero>0');
  QrC00.SQL.Add('AND la.Tipo <> 2');
  QrC00.SQL.Add('AND co.Exclusivo="V"');
  QrC00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
  QrC00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrC00.SQL.Add(';');
  QrC00.SQL.Add('SELECT SUM(Debito) Debito, SUM(Credito) Credito,  SUM(Saldo) Saldo');
  QrC00.SQL.Add('FROM _c00_cts_;');
  //QrC00 . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrC00, DModG.MyPID_DB);
  //
(*
  QrInicial.Close;
  QrInicial.Params[0].AsString := EntiTxt;
  //QrInicial . O p e n ;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrInicial, Dmod.MyDB, [
  'SELECT SUM(Inicial) Saldo ',
  'FROM carteiras ca ',
  'WHERE ForneceI=' + EntiTxt,
  '']);
  //
{
SELECT
SUM(la.Debito) Debito,
SUM(la.Credito) Credito,
SUM(la.Credito-la.Debito) Saldo
FROM lct la
LEFT JOIN contas    co ON co.Codigo=la.Genero
WHERE la.Genero>0
AND la.Tipo <> 2
AND co.Exclusivo="F"
AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN :P0 AND :P1
AND la.CliInt=:P2
}
  QrT00.Close;
  QrT00.SQL.Clear;
{
  QrT00.SQL.Add('SELECT');
  QrT00.SQL.Add('SUM(la.Debito) Debito,');
  QrT00.SQL.Add('SUM(la.Credito) Credito,');
  QrT00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrT00.SQL.Add('FROM lct la');
  QrT00.SQL.Add('LEFT JOIN contas    co ON co.Codigo=la.Genero');
  QrT00.SQL.Add('WHERE la.Genero>0');
  QrT00.SQL.Add('AND la.Tipo <> 2');
  case RGExclusivos.ItemIndex of
    0: QrT00.SQL.Add('AND co.Exclusivo="F"');
    1: QrT00.SQL.Add('');
    2: QrT00.SQL.Add('AND co.Exclusivo="V"');
  end;
  QrT00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
                 PerIni + ' AND ' + PerFim);
  QrT00.SQL.Add('');
  QrT00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrT00.SQL.Add('');
  QrT00.Params[00].AsInteger := Periodo-12;
  QrT00.Params[01].AsInteger := Periodo-1;
  QrT00.Params[02].AsString  := EntiTxt;
}
  QrT00.SQL.Add('DROP TABLE IF EXISTS _t00_cts_;');
  QrT00.SQL.Add('CREATE TABLE _t00_cts_');
  QrT00.SQL.Add('');
  QrT00.SQL.Add('SELECT');
  QrT00.SQL.Add('SUM(la.Debito) Debito,');
  QrT00.SQL.Add('SUM(la.Credito) Credito,');
  QrT00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrT00.SQL.Add('FROM ' + FTabLctA + ' la');
  QrT00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrT00.SQL.Add('WHERE la.Genero>0');
  QrT00.SQL.Add('AND la.Tipo <> 2');
  QrT00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
                 PerIni + ' AND ' + PerFim);
  QrT00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrT00.SQL.Add('UNION');
  QrT00.SQL.Add('SELECT');
  QrT00.SQL.Add('SUM(la.Debito) Debito,');
  QrT00.SQL.Add('SUM(la.Credito) Credito,');
  QrT00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrT00.SQL.Add('FROM ' + FTabLctB + ' la');
  QrT00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrT00.SQL.Add('WHERE la.Genero>0');
  QrT00.SQL.Add('AND la.Tipo <> 2');
  QrT00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
                 PerIni + ' AND ' + PerFim);
  QrT00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrT00.SQL.Add('UNION');
  QrT00.SQL.Add('SELECT');
  QrT00.SQL.Add('SUM(la.Debito) Debito,');
  QrT00.SQL.Add('SUM(la.Credito) Credito,');
  QrT00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrT00.SQL.Add('FROM ' + FTabLctD + ' la');
  QrT00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
  QrT00.SQL.Add('WHERE la.Genero>0');
  QrT00.SQL.Add('AND la.Tipo <> 2');
  QrT00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
                 PerIni + ' AND ' + PerFim);
  QrT00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrT00.SQL.Add(';');
  QrT00.SQL.Add('SELECT SUM(Debito) Debito, SUM(Credito) Credito,  SUM(Saldo) Saldo');
  QrT00.SQL.Add('FROM _t00_cts_;');

  //QrT00 . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrT00, DModG.MyPID_DB);
  PB_1.Position := 0;
  MyObjects.Informa(LaSub_1, True, 'Verificando total de cr�ditos, d�bitos e saldos mensais ...');
  PB_1.Max      := 11;
  PB_2.Position := 0;
  Update;
  Application.ProcessMessages;
  for i := 0 to 11 do
  begin
    PB_1.Position := PB_1.Position + 1;
    case i of
      00: Query := QrT01;
      01: Query := QrT02;
      02: Query := QrT03;
      03: Query := QrT04;
      04: Query := QrT05;
      05: Query := QrT06;
      06: Query := QrT07;
      07: Query := QrT08;
      08: Query := QrT09;
      09: Query := QrT10;
      10: Query := QrT11;
      11: Query := QrT12;
      else Query := nil;
    end;
    if Query <> nil then
    begin
      PerSeq := FormatFloat('0', Periodo + i );
      NomeTab := '_t' + FormatFloat('00', i + 1) + '_cts_';
      Query.Close;
      Query.SQL.Clear;
      Query.SQL.Add('DROP TABLE IF EXISTS ' + NomeTab + ';');
      Query.SQL.Add('CREATE TABLE ' + NomeTab);
      Query.SQL.Add('');
      Query.SQL.Add('SELECT');
      Query.SQL.Add('SUM(la.Debito) Debito,');
      Query.SQL.Add('SUM(la.Credito) Credito,');
      Query.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
      Query.SQL.Add('FROM ' + FTabLctA + ' la');
      Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
      Query.SQL.Add('WHERE la.Genero>0');
      Query.SQL.Add('AND la.Tipo <> 2');
      case RGExclusivos.ItemIndex of
        0: Query.SQL.Add('AND co.Exclusivo="F"');
        1: Query.SQL.Add('');
        2: Query.SQL.Add('AND co.Exclusivo="V"');
      end;
      Query.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' + PerSeq);
      Query.SQL.Add('');
      Query.SQL.Add('AND la.CliInt=' + EntiTxt);
      Query.SQL.Add('UNION');
      Query.SQL.Add('SELECT');
      Query.SQL.Add('SUM(la.Debito) Debito,');
      Query.SQL.Add('SUM(la.Credito) Credito,');
      Query.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
      Query.SQL.Add('FROM ' + FTabLctB + ' la');
      Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
      Query.SQL.Add('WHERE la.Genero>0');
      Query.SQL.Add('AND la.Tipo <> 2');
      case RGExclusivos.ItemIndex of
        0: Query.SQL.Add('AND co.Exclusivo="F"');
        1: Query.SQL.Add('');
        2: Query.SQL.Add('AND co.Exclusivo="V"');
      end;
      Query.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' + PerSeq);
      Query.SQL.Add('');
      Query.SQL.Add('AND la.CliInt=' + EntiTxt);
      Query.SQL.Add('UNION');
      Query.SQL.Add('SELECT');
      Query.SQL.Add('SUM(la.Debito) Debito,');
      Query.SQL.Add('SUM(la.Credito) Credito,');
      Query.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
      Query.SQL.Add('FROM ' + FTabLctD + ' la');
      Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
      Query.SQL.Add('WHERE la.Genero>0');
      Query.SQL.Add('AND la.Tipo <> 2');
      case RGExclusivos.ItemIndex of
        0: Query.SQL.Add('AND co.Exclusivo="F"');
        1: Query.SQL.Add('');
        2: Query.SQL.Add('AND co.Exclusivo="V"');
      end;
      Query.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' + PerSeq);
      Query.SQL.Add('');
      Query.SQL.Add('AND la.CliInt=' + EntiTxt);
      Query.SQL.Add(';');
      Query.SQL.Add('SELECT SUM(Debito) Debito, SUM(Credito) Credito,  SUM(Saldo) Saldo');
      Query.SQL.Add('FROM ' + NomeTab + ';');
{
SELECT
SUM(la.Debito) Debito,
SUM(la.Credito) Credito,
SUM(la.Credito-la.Debito) Saldo
FROM lct la
LEFT JOIN contas    co ON co.Codigo=la.Genero
WHERE la.Genero>0
AND la.Tipo <> 2
AND co.Exclusivo="F"
AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=:P0

AND la.CliInt=:P1
}
{
      case RGExclusivos.ItemIndex of
        0: Query.SQL[8] := 'AND co.Exclusivo="F"';
        1: Query.SQL[8] := '';
        2: Query.SQL[8] := 'AND co.Exclusivo="V"';
      end;
      Query.Params[00].AsInteger := Periodo + i;
      Query.Params[01].AsString  := EntiTxt;
}
      //Query . O p e n ;
      UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
    end;
  end;
  MyObjects.Informa(LaSub_1, False, '...');
  Update;
  Application.ProcessMessages;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE '+FResMes+' SET ');
  DModG.QrUpdPID1.SQL.Add('AnoAn=:P00,');
  DModG.QrUpdPID1.SQL.Add('Mes01=:P01,');
  DModG.QrUpdPID1.SQL.Add('Mes02=:P02,');
  DModG.QrUpdPID1.SQL.Add('Mes03=:P03,');
  DModG.QrUpdPID1.SQL.Add('Mes04=:P04,');
  DModG.QrUpdPID1.SQL.Add('Mes05=:P05,');
  DModG.QrUpdPID1.SQL.Add('Mes06=:P06,');
  DModG.QrUpdPID1.SQL.Add('Mes07=:P07,');
  DModG.QrUpdPID1.SQL.Add('Mes08=:P08,');
  DModG.QrUpdPID1.SQL.Add('Mes09=:P09,');
  DModG.QrUpdPID1.SQL.Add('Mes10=:P10,');
  DModG.QrUpdPID1.SQL.Add('Mes11=:P11,');
  DModG.QrUpdPID1.SQL.Add('Mes12=:P12');
  DModG.QrUpdPID1.SQL.Add('WHERE SubGrupo=:Pa AND SubOrdem=1');
  QrSubGrupos.Close;
  QrSubGrupos.SQL.Clear;
  QrSubGrupos.SQL.Add('SELECT SubGrupo');
  QrSubGrupos.SQL.Add('FROM '+FResMes+' rm');
  QrSubGrupos.SQL.Add('WHERE SubOrdem=1');
  //QrSubGrupos . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrSubGrupos, DModG.MyPID_DB);
  PB_1.Position := 0;
  MyObjects.Informa(LaSub_1, True, 'Atualizando valores de subgrupos ...');
  PB_1.Max      := QrSubGrupos.RecordCount;
  Update;
  Application.ProcessMessages;
  while not QrSubGrupos.Eof do
  begin
    PB_1.Position := PB_1.Position + 1;
    SGCodigo := FormatFloat('0', QrSubGruposSubGrupo.Value);
    QrBef.Close;
{
SELECT
SUM(la.Debito) Debito,
SUM(la.Credito) Credito,
SUM(la.Credito-la.Debito) Saldo
FROM lct la
LEFT JOIN contas    co ON co.Codigo=la.Genero
LEFT JOIN subgrupos sg ON sg.Codigo=co.SubGrupo
WHERE la.Genero>0
AND la.Tipo <> 2
AND co.Exclusivo="F"
AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) < :P0
AND sg.Codigo=:P1
AND la.CliInt=:P2
}
    QrBef.SQL.Clear;
    {
    QrBef.SQL.Add('SELECT');
    QrBef.SQL.Add('SUM(la.Debito) Debito,');
    QrBef.SQL.Add('SUM(la.Credito) Credito,');
    QrBef.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
    QrBef.SQL.Add('FROM lct la');
    QrBef.SQL.Add('LEFT JOIN contas    co ON co.Codigo=la.Genero');
    QrBef.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=co.SubGrupo');
    QrBef.SQL.Add('WHERE la.Genero>0');
    QrBef.SQL.Add('AND la.Tipo <> 2');
    case RGExclusivos.ItemIndex of
      0: QrBef.SQL.Add('AND co.Exclusivo="F"');
      1: QrBef.SQL.Add('');
      2: QrBef.SQL.Add('AND co.Exclusivo="V"');
    end;
    QrBef.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
    QrBef.SQL.Add('AND sg.Codigo=' + FormatFloat('0', QrSubGruposSubGrupo.Value));
    QrBef.SQL.Add('AND la.CliInt=' + EntiTxt);
    QrBef.Params[00].AsInteger := Periodo-12;
    QrBef.Params[01].AsInteger := QrSubGruposSubGrupo.Value;
    QrBef.Params[02].AsInteger := FEntidade;
    }
    QrBef.SQL.Add('DROP TABLE IF EXISTS _bef_cts_;');
    QrBef.SQL.Add('CREATE TABLE _bef_cts_');
    QrBef.SQL.Add('');
    QrBef.SQL.Add('SELECT');
    QrBef.SQL.Add('SUM(la.Debito) Debito,');
    QrBef.SQL.Add('SUM(la.Credito) Credito,');
    QrBef.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
    QrBef.SQL.Add('FROM ' + FTabLctA + ' la');
    QrBef.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
    QrBef.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
    QrBef.SQL.Add('WHERE la.Genero>0');
    QrBef.SQL.Add('AND la.Tipo <> 2');
    QrBef.SQL.Add('');
    QrBef.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
    QrBef.SQL.Add('AND sg.Codigo=' + SGCodigo);
    QrBef.SQL.Add('AND la.CliInt=' + EntiTxt);
    QrBef.SQL.Add('UNION');
    QrBef.SQL.Add('SELECT');
    QrBef.SQL.Add('SUM(la.Debito) Debito,');
    QrBef.SQL.Add('SUM(la.Credito) Credito,');
    QrBef.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
    QrBef.SQL.Add('FROM ' + FTabLctB + ' la');
    QrBef.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
    QrBef.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
    QrBef.SQL.Add('WHERE la.Genero>0');
    QrBef.SQL.Add('AND la.Tipo <> 2');
    QrBef.SQL.Add('');
    QrBef.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
    QrBef.SQL.Add('AND sg.Codigo=' + SGCodigo);
    QrBef.SQL.Add('AND la.CliInt=' + EntiTxt);
    QrBef.SQL.Add('UNION');
    QrBef.SQL.Add('SELECT');
    QrBef.SQL.Add('SUM(la.Debito) Debito,');
    QrBef.SQL.Add('SUM(la.Credito) Credito,');
    QrBef.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
    QrBef.SQL.Add('FROM ' + FTabLctD + ' la');
    QrBef.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
    QrBef.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.SubGrupo');
    QrBef.SQL.Add('WHERE la.Genero>0');
    QrBef.SQL.Add('AND la.Tipo <> 2');
    QrBef.SQL.Add('');
    QrBef.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))<' + PerIni);
    QrBef.SQL.Add('AND sg.Codigo=' + SGCodigo);
    QrBef.SQL.Add('AND la.CliInt=' + EntiTxt);
    QrBef.SQL.Add(';');
    QrBef.SQL.Add('SELECT SUM(Debito) Debito, SUM(Credito) Credito,  SUM(Saldo) Saldo');
    QrBef.SQL.Add('FROM _bef_cts_;');
    //QrBef . O p e n ;
    UnDmkDAC_PF.AbreQuery(QrBef, DModG.MyPID_DB);
    //
    QrSum.Close;
    QrSum.SQL.Clear;
    QrSum.SQL.Add('SELECT SUM(AnoAn) AnoAn, SUM(Mes01) Mes01,');
    QrSum.SQL.Add('SUM(Mes02) Mes02, SUM(Mes03) Mes03,');
    QrSum.SQL.Add('SUM(Mes04) Mes04, SUM(Mes05) Mes05,');
    QrSum.SQL.Add('SUM(Mes06) Mes06, SUM(Mes07) Mes07,');
    QrSum.SQL.Add('SUM(Mes08) Mes08, SUM(Mes09) Mes09,');
    QrSum.SQL.Add('SUM(Mes10) Mes10, SUM(Mes11) Mes11,');
    QrSum.SQL.Add('SUM(Mes12) Mes12');
    QrSum.SQL.Add('FROM '+FResMes);
    QrSum.SQL.Add('WHERE SubOrdem=0');
    QrSum.SQL.Add('AND SubGrupo=:P0');
    QrSum.Params[00].AsInteger := QrSubGruposSubGrupo.Value;
    //QrSum . O p e n ;
    UnDmkDAC_PF.AbreQuery(QrSum, DModG.MyPID_DB);
    //
    Valor := QrBefSaldo.Value + QrSumAnoAn.Value;
    DModG.QrUpdPID1.Params[00].AsFloat   := Valor;
    Valor := Valor + QrSumMes01.Value;
    DModG.QrUpdPID1.Params[01].AsFloat   := Valor;
    Valor := Valor + QrSumMes02.Value;
    DModG.QrUpdPID1.Params[02].AsFloat   := Valor;
    Valor := Valor + QrSumMes03.Value;
    DModG.QrUpdPID1.Params[03].AsFloat   := Valor;
    Valor := Valor + QrSumMes04.Value;
    DModG.QrUpdPID1.Params[04].AsFloat   := Valor;
    Valor := Valor + QrSumMes05.Value;
    DModG.QrUpdPID1.Params[05].AsFloat   := Valor;
    Valor := Valor + QrSumMes06.Value;
    DModG.QrUpdPID1.Params[06].AsFloat   := Valor;
    Valor := Valor + QrSumMes07.Value;
    DModG.QrUpdPID1.Params[07].AsFloat   := Valor;
    Valor := Valor + QrSumMes08.Value;
    DModG.QrUpdPID1.Params[08].AsFloat   := Valor;
    Valor := Valor + QrSumMes09.Value;
    DModG.QrUpdPID1.Params[09].AsFloat   := Valor;
    Valor := Valor + QrSumMes10.Value;
    DModG.QrUpdPID1.Params[10].AsFloat   := Valor;
    Valor := Valor + QrSumMes11.Value;
    DModG.QrUpdPID1.Params[11].AsFloat   := Valor;
    Valor := Valor + QrSumMes12.Value;
    DModG.QrUpdPID1.Params[12].AsFloat   := Valor;
    DModG.QrUpdPID1.Params[13].AsInteger := QrSubGruposSubGrupo.Value;
    DModG.QrUpdPID1.ExecSQL;
    //
    QrSubGrupos.Next;
  end;
  MyObjects.Informa(LaSub_1, False, '...');
  Update;
  Application.ProcessMessages;
  //////////////////////////////////////////////////////////////////////////////
  //  Consigna��es
  //////////////////////////////////////////////////////////////////////////////
  //FConsMes := UCriar.RecriaTempTable('ConsMes', DModG.QrUpdPID1, False);
  FConsMes :=
    UCriarFin.RecriaTempTableNovo(ntrtt_ConsMes, DMOdG.QrUpdPID1, False);
  PB_1.Max := 12;
  MyObjects.Informa(LaSub_1, True, 'Verificando pend�ncias de contas mensais obrigat�rias...');
  PB_1.Position := 0;
  Update;
  Application.ProcessMessages;
  (* A tabela consignacoesits n�o existe mais
  for i := 1 to 12 do
  begin
    PB_1.Position := PB_1.Position + 1;

    UnDmkDAC_PF.AbreMySQLQuery0(QrConsigIts, Dmod.MyDB, [
    'SELECT cs.Codigo, SUM(cs.Credito-cs.Debito) Saldo, ',
    'co.Descricao ',
    'FROM consignacoesits cs ',
    'LEFT JOIN consignacoes co ON co.Codigo=cs.Codigo ',
    'WHERE  (((YEAR(cs.Data)-2000)*12)+MONTH(cs.Data)) < ' + Geral.FF0(Periodo + I),
    'GROUP BY cs.Codigo ',
    '']);
    //
    while not QrConsigIts.Eof do
    begin
      if QrConsigItsSaldo.Value <> 0 then
      begin
        DModG.QrUpdPID1.SQL.Clear;
        DModG.QrUpdPID1.SQL.Add('INSERT INTO '+FConsMes+' SET ');
        DModG.QrUpdPID1.SQL.Add('Codigo='+IntTOStr(QrConsigItsCodigo.Value));
        DModG.QrUpdPID1.SQL.Add(', Nome="'+QrConsigItsDescricao.Value+'"');
        DModG.QrUpdPID1.SQL.Add(', Mes'+FormatFloat('00', i)+'=:P0');
        //
        DModG.QrUpdPID1.Params[0].AsFloat   := QrConsigItsSaldo.Value;
        DModG.QrUpdPID1.ExecSQL;
        //
      end;
      QrConsigIts.Next;
    end;
  end;
  *)
  MyObjects.Informa(LaSub_1, False, '...');
  Update;
  Application.ProcessMessages;
  //////////////////////////////////////////////////////////////////////////////
  GeraPendencias;
  //////////////////////////////////////////////////////////////////////////////
  BtImprime.Enabled := True;
  BtGera.Enabled    := False;   // valores errados se recalcula. Porque?
  Screen.Cursor := crDefault;
  TbResMes.Close;
  TbResMes.TableName := FResMes;
  //TbResMes. O p e n ;
  UnDmkDAC_PF.AbreTable(TbResMes, DModG.MyPID_DB);
  MyObjects.Informa(LaSub_1, False, '...');
  MyObjects.Informa(LaSub_2, False, '...');
  MyObjects.Informa(LaSub_3, False, '...');
  PB_1.Position := 0;
  PB_2.Position := 0;
  PB_3.Position := 0;
  Update;
{
  QrTSG.Close;
  QrTSG.SQL.Clear;
  QrTSG.SQL.Add('SELECT rm.SubGrupo, SUM(rm.AnoAn) T00, SUM(rm.Mes01) T01,');
  QrTSG.SQL.Add('SUM(rm.Mes02) T02, SUM(rm.Mes03) T03, SUM(rm.Mes04) T04,');
  QrTSG.SQL.Add('SUM(rm.Mes05) T05, SUM(rm.Mes06) T06, SUM(rm.Mes07) T07,');
  QrTSG.SQL.Add('SUM(rm.Mes08) T08, SUM(rm.Mes09) T09, SUM(rm.Mes10) T10,');
  QrTSG.SQL.Add('SUM(rm.Mes11) T11, SUM(rm.Mes12) T12, SUM(rm.Pendencias) TPN');
  QrTSG.SQL.Add('FROM '+FResMes+' rm');
  QrTSG.SQL.Add('WHERE SubOrdem=0');
  QrTSG.SQL.Add('AND SubGrupo=:P0');
  QrTSG.SQL.Add('GROUP BY rm.SubGrupo');
  //QrTSG . O p e n ;
  Memo1.Lines.Clear;
}
  Application.ProcessMessages;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'UPDATE ' + FResMes,
  'SET AnoAt = Mes01 + Mes02 + Mes03 + Mes04 + Mes05 + Mes06 ',
  ' + Mes07 + Mes08 + Mes09 + Mes10 + Mes11 + Mes12  ',
  '']);
  //
  if FResMes43 then
    DefineLimitBy();
end;

procedure TFmResMes.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmResMes.ImprimeResMes;
begin
  FPrintNo := 1;
  QrResMes.Close;
  QrResMes.SQL.Clear;
  QrResMes.SQL.Add('SELECT * FROM '+FResMes);
  QrResMes.SQL.Add('WHERE ((AnoAn+Mes01+Mes02+Mes03+Mes04+Mes05+Mes06+');
  QrResMes.SQL.Add('Mes07+Mes08+Mes09+Mes10+Mes11+Mes12+Pendencias) <>0)');
  QrResMes.SQL.Add('ORDER BY TipoAgrupa, OrdemLista, NomeSubGrupo, SubOrdem, NomeConta');
  //QrResMes . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrResMes, DModG.MyPID_DB);
  MyObjects.frxMostra(frxResMes, 'Resultado mensal');
  QrResMes.Close;
end;

function TFmResMes.ImprimeResMesExclusivo(Ax: Integer; PB1, PB2: TProgressBar;
LaSub1: TLabel): TfrxReport;
var
  Query: TmySQLQuery;
  i, Periodo: Integer;
  PerIni, PerFim, PerSeq, EntiTxt, NomeTab: String;
begin
  Result := nil;
  if FEntidade = 0 then
  begin
    Geral.MB_Aviso('Empresa (Cliente Interno) n�o definido!' +
    sLineBreak + 'Gera��o de resultados gerais cancelada!');
    Exit;
  end;
  //
  FPrintNo := Ax;
  Periodo := FPeriodo - 11;
  //
  EntiTxt := FormatFloat('0', FEntidade);
  PerIni  := FormatFloat('0', Periodo-12);
  PerFim  := FormatFloat('0', Periodo-1);
  //
  QrU00.Close;
  QrU00.SQL.Clear;
  QrU00.SQL.Add('DROP TABLE IF EXISTS _u00_cts_;');
  QrU00.SQL.Add('CREATE TABLE _u00_cts_');
  QrU00.SQL.Add('');
  QrU00.SQL.Add('SELECT');
  QrU00.SQL.Add('SUM(la.Debito) Debito,');
  QrU00.SQL.Add('SUM(la.Credito) Credito,');
  QrU00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrU00.SQL.Add('FROM ' + FTabLctA + ' la');
  QrU00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
  QrU00.SQL.Add('WHERE la.Genero>0');
  QrU00.SQL.Add('AND la.Tipo <> 2');
  QrU00.SQL.Add('AND co.Exclusivo="F"');
  QrU00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
    PerIni + ' AND ' + PerFim);
  QrU00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrU00.SQL.Add('UNION');
  QrU00.SQL.Add('SELECT');
  QrU00.SQL.Add('SUM(la.Debito) Debito,');
  QrU00.SQL.Add('SUM(la.Credito) Credito,');
  QrU00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrU00.SQL.Add('FROM ' + FTabLctB + ' la');
  QrU00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
  QrU00.SQL.Add('WHERE la.Genero>0');
  QrU00.SQL.Add('AND la.Tipo <> 2');
  QrU00.SQL.Add('AND co.Exclusivo="F"');
  QrU00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
    PerIni + ' AND ' + PerFim);
  QrU00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrU00.SQL.Add('UNION');
  QrU00.SQL.Add('SELECT');
  QrU00.SQL.Add('SUM(la.Debito) Debito,');
  QrU00.SQL.Add('SUM(la.Credito) Credito,');
  QrU00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrU00.SQL.Add('FROM ' + FTabLctD + ' la');
  QrU00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
  QrU00.SQL.Add('WHERE la.Genero>0');
  QrU00.SQL.Add('AND la.Tipo <> 2');
  QrU00.SQL.Add('AND co.Exclusivo="F"');
  QrU00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
    PerIni + ' AND ' + PerFim);
  QrU00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrU00.SQL.Add(';');
  QrU00.SQL.Add('SELECT SUM(Debito) Debito, SUM(Credito) Credito,  SUM(Saldo) Saldo');
  QrU00.SQL.Add('FROM _u00_cts_;');
  {
  QrU00.Params[00].AsInteger := Periodo-12;
  QrU00.Params[01].AsInteger := Periodo-1;
  QrU00.Params[02].AsString  := EntiTxt;
  }
  //QrU00 . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrU00, DModG.MyPID_DB);
  PB1.Position := 0;
  MyObjects.Informa(LaSub1, True, 'Abrindo tabelas secund�rias de impress�o ...');
  PB1.Max      := 24;
  PB2.Position := 0;
  Update;
  Application.ProcessMessages;
  for i := 0 to 11 do
  begin
    PB1.Position := PB1.Position + 1;
    case i of
      00: Query := QrU01;
      01: Query := QrU02;
      02: Query := QrU03;
      03: Query := QrU04;
      04: Query := QrU05;
      05: Query := QrU06;
      06: Query := QrU07;
      07: Query := QrU08;
      08: Query := QrU09;
      09: Query := QrU10;
      10: Query := QrU11;
      11: Query := QrU12;
      else Query := nil;
    end;
    if Query <> nil then
    begin
      PerSeq := FormatFloat('0', Periodo + i );
      NomeTab := '_u' + FormatFloat('00', i + 1) + '_cts_';
      Query.Close;
      Query.SQL.Clear;
      Query.SQL.Add('DROP TABLE IF EXISTS ' + NomeTab + ';');
      Query.SQL.Add('CREATE TABLE ' + NomeTab);
      Query.SQL.Add('');
      Query.SQL.Add('SELECT');
      Query.SQL.Add('SUM(la.Debito) Debito,');
      Query.SQL.Add('SUM(la.Credito) Credito,');
      Query.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
      Query.SQL.Add('FROM ' + FTabLctA + ' la');
      Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
      Query.SQL.Add('WHERE la.Genero>0');
      Query.SQL.Add('AND la.Tipo <> 2');
      Query.SQL.Add('AND co.Exclusivo="F"');
      Query.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' + PerSeq);
      Query.SQL.Add('AND la.CliInt=' + EntiTxt);
      Query.SQL.Add('UNION');
      Query.SQL.Add('SELECT');
      Query.SQL.Add('SUM(la.Debito) Debito,');
      Query.SQL.Add('SUM(la.Credito) Credito,');
      Query.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
      Query.SQL.Add('FROM ' + FTabLctB + ' la');
      Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
      Query.SQL.Add('WHERE la.Genero>0');
      Query.SQL.Add('AND la.Tipo <> 2');
      Query.SQL.Add('AND co.Exclusivo="F"');
      Query.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' + PerSeq);
      Query.SQL.Add('AND la.CliInt=' + EntiTxt);
      Query.SQL.Add('UNION');
      Query.SQL.Add('SELECT');
      Query.SQL.Add('SUM(la.Debito) Debito,');
      Query.SQL.Add('SUM(la.Credito) Credito,');
      Query.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
      Query.SQL.Add('FROM ' + FTabLctD + ' la');
      Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
      Query.SQL.Add('WHERE la.Genero>0');
      Query.SQL.Add('AND la.Tipo <> 2');
      Query.SQL.Add('AND co.Exclusivo="F"');
      Query.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' + PerSeq);
      Query.SQL.Add('AND la.CliInt=' + EntiTxt);
      Query.SQL.Add(';');
      Query.SQL.Add('SELECT SUM(Debito) Debito, SUM(Credito) Credito,  SUM(Saldo) Saldo');
      Query.SQL.Add('FROM ' + NomeTab + ';');
      {
      Query.Params[00].AsInteger := Periodo+i;
      Query.Params[01].AsString  := EntiTxt;
      }
      //Query . O p e n ;
      UnDmkDAC_PF.AbreQuery(Query, DModG.MyPID_DB);
      //
    end;
  end;
  //
  QrV00.Close;
  QrV00.SQL.Clear;
  QrV00.SQL.Add('DROP TABLE IF EXISTS _v00_cts_;');
  QrV00.SQL.Add('CREATE TABLE _v00_cts_');
  QrV00.SQL.Add('');
  QrV00.SQL.Add('SELECT');
  QrV00.SQL.Add('SUM(la.Debito) Debito,');
  QrV00.SQL.Add('SUM(la.Credito) Credito,');
  QrV00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrV00.SQL.Add('FROM ' + FTabLctA + ' la');
  QrV00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
  QrV00.SQL.Add('WHERE la.Genero>0');
  QrV00.SQL.Add('AND la.Tipo <> 2');
  QrV00.SQL.Add('AND co.Exclusivo="V"');
  QrV00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
    PerIni + ' AND ' + PerFim);
  QrV00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrV00.SQL.Add('UNION');
  QrV00.SQL.Add('SELECT');
  QrV00.SQL.Add('SUM(la.Debito) Debito,');
  QrV00.SQL.Add('SUM(la.Credito) Credito,');
  QrV00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrV00.SQL.Add('FROM ' + FTabLctB + ' la');
  QrV00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
  QrV00.SQL.Add('WHERE la.Genero>0');
  QrV00.SQL.Add('AND la.Tipo <> 2');
  QrV00.SQL.Add('AND co.Exclusivo="V"');
  QrV00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
    PerIni + ' AND ' + PerFim);
  QrV00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrV00.SQL.Add('UNION');
  QrV00.SQL.Add('SELECT');
  QrV00.SQL.Add('SUM(la.Debito) Debito,');
  QrV00.SQL.Add('SUM(la.Credito) Credito,');
  QrV00.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
  QrV00.SQL.Add('FROM ' + FTabLctD + ' la');
  QrV00.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
  QrV00.SQL.Add('WHERE la.Genero>0');
  QrV00.SQL.Add('AND la.Tipo <> 2');
  QrV00.SQL.Add('AND co.Exclusivo="V"');
  QrV00.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) BETWEEN ' +
    PerIni + ' AND ' + PerFim);
  QrV00.SQL.Add('AND la.CliInt=' + EntiTxt);
  QrV00.SQL.Add(';');
  QrV00.SQL.Add('SELECT SUM(Debito) Debito, SUM(Credito) Credito,  SUM(Saldo) Saldo');
  QrV00.SQL.Add('FROM _v00_cts_;');
  {
  QrV00.Params[00].AsInteger := Periodo-12;
  QrV00.Params[01].AsInteger := Periodo-1;
  QrV00.Params[02].AsString  := EntiTxt;
  }
  //QrV00 . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrV00, DModG.MyPID_DB);
  for i := 0 to 11 do
  begin
    PB1.Position := PB1.Position + 1;
    case i of
      00: Query := QrV01;
      01: Query := QrV02;
      02: Query := QrV03;
      03: Query := QrV04;
      04: Query := QrV05;
      05: Query := QrV06;
      06: Query := QrV07;
      07: Query := QrV08;
      08: Query := QrV09;
      09: Query := QrV10;
      10: Query := QrV11;
      11: Query := QrV12;
      else Query := nil;
    end;
    if Query <> nil then
    begin
      PerSeq := FormatFloat('0', Periodo + i );
      NomeTab := '_v' + FormatFloat('00', i + 1) + '_cts_';
      Query.Close;
      Query.SQL.Clear;
      Query.SQL.Add('DROP TABLE IF EXISTS ' + NomeTab + ';');
      Query.SQL.Add('CREATE TABLE ' + NomeTab);
      Query.SQL.Add('');
      Query.SQL.Add('SELECT');
      Query.SQL.Add('SUM(la.Debito) Debito,');
      Query.SQL.Add('SUM(la.Credito) Credito,');
      Query.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
      Query.SQL.Add('FROM ' + FTabLctA + ' la');
      Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
      Query.SQL.Add('WHERE la.Genero>0');
      Query.SQL.Add('AND la.Tipo <> 2');
      Query.SQL.Add('AND co.Exclusivo="V"');
      Query.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' + PerSeq);
      Query.SQL.Add('AND la.CliInt=' + EntiTxt);
      Query.SQL.Add('UNION');
      Query.SQL.Add('SELECT');
      Query.SQL.Add('SUM(la.Debito) Debito,');
      Query.SQL.Add('SUM(la.Credito) Credito,');
      Query.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
      Query.SQL.Add('FROM ' + FTabLctB + ' la');
      Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
      Query.SQL.Add('WHERE la.Genero>0');
      Query.SQL.Add('AND la.Tipo <> 2');
      Query.SQL.Add('AND co.Exclusivo="V"');
      Query.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' + PerSeq);
      Query.SQL.Add('AND la.CliInt=' + EntiTxt);
      Query.SQL.Add('UNION');
      Query.SQL.Add('SELECT');
      Query.SQL.Add('SUM(la.Debito) Debito,');
      Query.SQL.Add('SUM(la.Credito) Credito,');
      Query.SQL.Add('SUM(la.Credito-la.Debito) Saldo');
      Query.SQL.Add('FROM ' + FTabLctD + ' la');
      Query.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co ON co.Codigo=la.Genero');
      Query.SQL.Add('WHERE la.Genero>0');
      Query.SQL.Add('AND la.Tipo <> 2');
      Query.SQL.Add('AND co.Exclusivo="V"');
      Query.SQL.Add('AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=' + PerSeq);
      Query.SQL.Add('AND la.CliInt=' + EntiTxt);
      Query.SQL.Add(';');
      Query.SQL.Add('SELECT SUM(Debito) Debito, SUM(Credito) Credito,  SUM(Saldo) Saldo');
      Query.SQL.Add('FROM ' + NomeTab + ';');
      {
      Query.Params[00].AsInteger := Periodo+i;
      Query.Params[01].AsString  := EntiTxt;
      }
      //Query . O p e n ;
      UnDmkDAC_PF.AbreQuery(Query, DModG.MyPID_DB);
    end;
  end;
  //
  QrResMesV.Close;
  QrResMesV.SQL.Clear;
  QrResMesV.SQL.Add('SELECT * FROM '+FResMes);
  QrResMesV.SQL.Add('WHERE ((AnoAn+Mes01+Mes02+Mes03+Mes04+Mes05+Mes06+');
  QrResMesV.SQL.Add('Mes07+Mes08+Mes09+Mes10+Mes11+Mes12+Pendencias) <>0)');
  QrResMesV.SQL.Add('AND Exclusivo="V"');
  QrResMesV.SQL.Add('ORDER BY TipoAgrupa, OrdemLista, NomeSubGrupo, SubOrdem, NomeConta');
  //QrResMesV . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrResMesV, DModG.MyPID_DB);
  //
  QrConsMes.Close;
  QrConsMes.SQL.Clear;
  QrConsMes.SQL.Add('SELECT Codigo, Nome,');
  QrConsMes.SQL.Add('SUM(Mes01) Mes01,');
  QrConsMes.SQL.Add('SUM(Mes02) Mes02,');
  QrConsMes.SQL.Add('SUM(Mes03) Mes03,');
  QrConsMes.SQL.Add('SUM(Mes04) Mes04,');
  QrConsMes.SQL.Add('SUM(Mes05) Mes05,');
  QrConsMes.SQL.Add('SUM(Mes06) Mes06,');
  QrConsMes.SQL.Add('SUM(Mes07) Mes07,');
  QrConsMes.SQL.Add('SUM(Mes08) Mes08,');
  QrConsMes.SQL.Add('SUM(Mes09) Mes09,');
  QrConsMes.SQL.Add('SUM(Mes10) Mes10,');
  QrConsMes.SQL.Add('SUM(Mes11) Mes11,');
  QrConsMes.SQL.Add('SUM(Mes12) Mes12');
  QrConsMes.SQL.Add('FROM '+FConsMes);
  QrConsMes.SQL.Add('GROUP BY Codigo');
  //QrConsMes . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrConsMes, DModG.MyPID_DB);
  //
  ReopenResMesF(Ax);
  //
  QrResMesTNE.Close;
  QrResMesTNE.SQL.Clear;
  QrResMesTNE.SQL.Add('SELECT SUM(Pendencias) SOMA FROM '+FResMes);
  QrResMesTNE.SQL.Add('WHERE ((AnoAn+Mes01+Mes02+Mes03+Mes04+Mes05+Mes06+');
  QrResMesTNE.SQL.Add('Mes07+Mes08+Mes09+Mes10+Mes11+Mes12+Pendencias) <>0)');
  QrResMesTNE.SQL.Add('AND Exclusivo="F"');
  QrResMesTNE.SQL.Add('ORDER BY TipoAgrupa, OrdemLista, NomeSubGrupo, SubOrdem, NomeConta');
  QrResMesTNE.SQL.Add('');
  //QrResMesTNE . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrResMesTNE, DModG.MyPID_DB);
  //
  case Ax of
    3: MyObjects.frxMostra(frxResMesFV_A3, 'Demosntrativo de movimento Mensal');
    4: ImprimeSaldoEm();
    41: MyObjects.frxMostra(frxResMesFV_A41, 'Demosntrativo de movimento Mensal');
    42: Result := PreparaDadosPara_frxResMesFV_A4B();
    43: Result := PreparaDadosPara_frxResMesFV_A4C();
  end;
  //
  if Ax in ([3, 4]) then
  begin
    QrResMesF.Close;
    QrResMesV.Close;
  end;
  MyObjects.Informa(LaSub1, False, '...');
  PB1.Position := 0;
  Update;
  Application.ProcessMessages;
end;

procedure TFmResMes.Contascontroladas1Click(Sender: TObject);
begin
  QrCtasResMes.Close;
  QrCtasResMes.SQL.Clear;
  QrCtasResMes.SQL.Add('SELECT * FROM ' + DModFin.FCtasResMes);
  QrCtasResMes.SQL.Add('WHERE SeqImp=-1 OR Periodo BETWEEN :P0 AND :P1');
  QrCtasResMes.SQL.Add('ORDER BY Nome, Conta, SeqImp, Periodo');
  QrCtasResMes.Params[0].AsInteger := FPeriodo-11;
  QrCtasResMes.Params[1].AsInteger := FPeriodo;
  //QrCtasResMes . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrCtasResMes, DModG.MyPID_DB);
  MyObjects.frxMostra(frxCtasResMes, 'Mensalidades pr�-estipuladas');
  QrCtasResMes.Close;
end;

procedure TFmResMes.QrCtasResMesCalcFields(DataSet: TDataSet);
begin
  QrCtasResMesNOME_PERIODO.Value :=
    dmkPF.MesEAnoDoPeriodo(QrCtasResMesPeriodo.Value);
end;

procedure TFmResMes.QrDC43CalcFields(DataSet: TDataSet);
begin
  QrDC43All_D.Value :=
  QrDC43M01_D.Value + QrDC43M02_D.Value + QrDC43M03_D.Value +
  QrDC43M04_D.Value + QrDC43M05_D.Value + QrDC43M06_D.Value +
  QrDC43M07_D.Value + QrDC43M08_D.Value + QrDC43M09_D.Value +
  QrDC43M10_D.Value + QrDC43M11_D.Value + QrDC43M12_D.Value;
  //
  QrDC43All_C.Value :=
  QrDC43M01_C.Value + QrDC43M02_C.Value + QrDC43M03_C.Value +
  QrDC43M04_C.Value + QrDC43M05_C.Value + QrDC43M06_C.Value +
  QrDC43M07_C.Value + QrDC43M08_C.Value + QrDC43M09_C.Value +
  QrDC43M10_C.Value + QrDC43M11_C.Value + QrDC43M12_C.Value;
  //
  //QrDC43All.Value := QrDC43All_C.Value - QrDC43All_D.Value;
  QrDC43All.Value :=
  QrDC43M01.Value + QrDC43M02.Value + QrDC43M03.Value +
  QrDC43M04.Value + QrDC43M05.Value + QrDC43M06.Value +
  QrDC43M07.Value + QrDC43M08.Value + QrDC43M09.Value +
  QrDC43M10.Value + QrDC43M11.Value + QrDC43M12.Value;
  //
end;

procedure TFmResMes.Tudojunto1Click(Sender: TObject);
begin
  ImprimeResMes;
end;

procedure TFmResMes.VerificaTipoAgrupa();
var
  Qry: TmySQLQuery;
  Texto: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
      'SELECT DISTINCT SubGrupo ',
      'FROM ' + FResMes,
      'WHERE  NOT TipoAgrupa IN (0,1) ',
      '']);
    //
    if Qry.RecordCount > 0 then
    begin
      Texto := '';
      //
      Qry.First;
      while not Qry.Eof do
      begin
        Texto := Texto + ', ' + Geral.FF0(Qry.FieldByName('SubGrupo').AsInteger);
        //
        Qry.Next;
      end;
      Texto := Copy(Texto, 3);
      //
      Geral.MB_Aviso(
        'O relat�rio selecionado para exibir os Resultados Mensais ' + sLineBreak +
        'necessita que todos Subgrupos do plano de contas tenham '  + sLineBreak +
        'definido o "Tipo de Agrupamento" como "D�bito" ou "Cr�dito" ' + sLineBreak +
        'e n�o como "Ambos" para que os percentuais e gr�ficos '  + sLineBreak +
        'fiquem preenchidos corretamente com despesas e receitas! '  + sLineBreak +
        '' + sLineBreak +
        'SubGrupos que devem ser alterados: ' + sLineBreak +
        Texto + sLineBreak +
        '');
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmResMes.QrResMesFAfterScroll(DataSet: TDataSet);
begin
  ReopenTSG(QrResMesFSubGrupo.Value);
end;

procedure TFmResMes.QrResMesFCalcFields(DataSet: TDataSet);
  function TipoAgrupa_TXT(): String;
  begin
    case QrResMesFTipoAgrupa.Value of
      0: Result := 'D�bito';
      1: Result := 'Cr�dito';
      2: Result := 'D�bito e Cr�dito';
      else Result := '? ? ?';
    end;
    Result := 'Tipo de Agrupamento do Sub Grupo: ' + Result;
  end;
  function TotalPorcento(): Double;
  begin
    case QrTSGTipoAgrupa.Value of
      0: Result := FTotalAnDeb;
      1: Result := FTotalAnCre;
      2: Result := FTotalAnDeC;
      else // Erro
      begin
        Result := 0;
        Geral.MB_Erro('FTotalAn??? sem defini��o de $ para %!');
      end;
    end;
  end;
var
  Valor: Variant;
begin
  if QrResMesFSubOrdem.Value = 0 then QrResMesFMESTO.Value :=
    QrResMesFMes01.Value + QrResMesFMes02.Value + QrResMesFMes03.Value +
    QrResMesFMes04.Value + QrResMesFMes05.Value + QrResMesFMes06.Value +
    QrResMesFMes07.Value + QrResMesFMes08.Value + QrResMesFMes09.Value +
    QrResMesFMes10.Value + QrResMesFMes11.Value + QrResMesFMes12.Value
  else QrResMesFMESTO.Value := QrResMesFMes12.Value;
  QrResMesFNO_TipoAgrupa.Value := TipoAgrupa_TXT();
  QrResMesFPercTot.Value := TotalPorcento();
  DefinePercentualConta(Valor);
  QrResMesFPercCta.Value := Valor;
  //
(*&�%
PercTot
*)
end;

procedure TFmResMes.QrResMesVCalcFields(DataSet: TDataSet);
begin
  if QrResMesVSubOrdem.Value = 0 then QrResMesVMESTO.Value :=
    QrResMesVMes01.Value + QrResMesVMes02.Value + QrResMesVMes03.Value +
    QrResMesVMes04.Value + QrResMesVMes05.Value + QrResMesVMes06.Value +
    QrResMesVMes07.Value + QrResMesVMes08.Value + QrResMesVMes09.Value +
    QrResMesVMes10.Value + QrResMesVMes11.Value + QrResMesVMes12.Value
  else QrResMesVMESTO.Value := QrResMesVMes12.Value;
  //
(*&�%
PercTot
*)
end;

procedure TFmResMes.QrU00CalcFields(DataSet: TDataSet);
begin
  QrU00SDO_PER.Value := QrU00Saldo.Value + QrB00Saldo.Value + QrInicialSaldo.Value;
end;

procedure TFmResMes.QrU01CalcFields(DataSet: TDataSet);
begin
  QrU01SDO_PER.Value := QrU01Saldo.Value + QrU00SDO_PER.Value;
end;

procedure TFmResMes.QrU02CalcFields(DataSet: TDataSet);
begin
  QrU02SDO_PER.Value := QrU02Saldo.Value + QrU01SDO_PER.Value;
end;

procedure TFmResMes.QrU03CalcFields(DataSet: TDataSet);
begin
  QrU03SDO_PER.Value := QrU03Saldo.Value + QrU02SDO_PER.Value;
end;

procedure TFmResMes.QrU04CalcFields(DataSet: TDataSet);
begin
  QrU04SDO_PER.Value := QrU04Saldo.Value + QrU03SDO_PER.Value;
end;

procedure TFmResMes.QrU05CalcFields(DataSet: TDataSet);
begin
  QrU05SDO_PER.Value := QrU05Saldo.Value + QrU04SDO_PER.Value;
end;

procedure TFmResMes.QrU06CalcFields(DataSet: TDataSet);
begin
  QrU06SDO_PER.Value := QrU06Saldo.Value + QrU05SDO_PER.Value;
end;

procedure TFmResMes.QrU07CalcFields(DataSet: TDataSet);
begin
  QrU07SDO_PER.Value := QrU07Saldo.Value + QrU06SDO_PER.Value;
end;

procedure TFmResMes.QrU08CalcFields(DataSet: TDataSet);
begin
  QrU08SDO_PER.Value := QrU08Saldo.Value + QrU07SDO_PER.Value;
end;

procedure TFmResMes.QrU09CalcFields(DataSet: TDataSet);
begin
  QrU09SDO_PER.Value := QrU09Saldo.Value + QrU08SDO_PER.Value;
end;

procedure TFmResMes.QrU10CalcFields(DataSet: TDataSet);
begin
  QrU10SDO_PER.Value := QrU10Saldo.Value + QrU09SDO_PER.Value;
end;

procedure TFmResMes.QrU11CalcFields(DataSet: TDataSet);
begin
  QrU11SDO_PER.Value := QrU11Saldo.Value + QrU10SDO_PER.Value;
end;

procedure TFmResMes.QrU12CalcFields(DataSet: TDataSet);
begin
  QrU12SDO_PER.Value := QrU12Saldo.Value + QrU11SDO_PER.Value;
  //
  QrU12TCredito.Value :=
  QrU01Credito.Value + QrU02Credito.Value + QrU03Credito.Value +
  QrU04Credito.Value + QrU05Credito.Value + QrU06Credito.Value +
  QrU07Credito.Value + QrU08Credito.Value + QrU09Credito.Value +
  QrU10Credito.Value + QrU11Credito.Value + QrU12Credito.Value;
  //
  QrU12TDebito.Value :=
  QrU01Debito.Value + QrU02Debito.Value + QrU03Debito.Value +
  QrU04Debito.Value + QrU05Debito.Value + QrU06Debito.Value +
  QrU07Debito.Value + QrU08Debito.Value + QrU09Debito.Value +
  QrU10Debito.Value + QrU11Debito.Value + QrU12Debito.Value;
  //
  QrU12TSaldo.Value := QrU12TCredito.Value - QrU12TDebito.Value;
  //
end;

procedure TFmResMes.PMImprimePopup(Sender: TObject);
begin
  if RGExclusivos.ItemIndex <> 1 then Separarexclusivo1.Enabled := False
  else Separarexclusivo1.Enabled := True;
end;

function TFmResMes.PreparaDadosPara_frxResMesFV_A4B: TfrxReport;
begin
  TiraPontoMilhar(frxResMesFV_A4B);
  frxResMesFV_A4B.Variables['VARF_HOW_MOSTRA_PLACTA'] := FHowShowA;
  frxResMesFV_A4B.Variables['VARF_ResmesV_TemRec']    := (QrResMesV.State <> dsInactive) and (QrResMesV.RecordCount > 0);
  frxResMesFV_A4B.Variables['MES_01']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-11));
  frxResMesFV_A4B.Variables['MES_02']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-10));
  frxResMesFV_A4B.Variables['MES_03']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-09));
  frxResMesFV_A4B.Variables['MES_04']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-08));
  frxResMesFV_A4B.Variables['MES_05']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-07));
  frxResMesFV_A4B.Variables['MES_06']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-06));
  frxResMesFV_A4B.Variables['MES_07']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-05));
  frxResMesFV_A4B.Variables['MES_08']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-04));
  frxResMesFV_A4B.Variables['MES_09']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-03));
  frxResMesFV_A4B.Variables['MES_10']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-02));
  frxResMesFV_A4B.Variables['MES_11']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-01));
  frxResMesFV_A4B.Variables['MES_12']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-00));
  //
  MyObjects.frxDefineDataSets(frxResMesFV_A4B, [
  frxDs43LM,
  //frxDsC43,
  frxDsConsMes,
  //frxDsD43,
  //frxDsDC43,
  frxDsResMesF,
  frxDsResMesV,
  frxDsTSG,
  frxDsU00,
  frxDsU01,
  frxDsU02,
  frxDsU03,
  frxDsU04,
  frxDsU05,
  frxDsU06,
  frxDsU07,
  frxDsU08,
  frxDsU09,
  frxDsU10,
  frxDsU11,
  frxDsU12,
  frxDsV00,
  frxDsV01,
  frxDsV02,
  frxDsV03,
  frxDsV04,
  frxDsV05,
  frxDsV06,
  frxDsV07,
  frxDsV08,
  frxDsV09,
  frxDsV10,
  frxDsV11,
  frxDsV12,
  //
  FmCashBal.frxDsEntiCfgRel_01,
  FmCashBal.frxDsEmp,
  DModG.frxDsDono
  ]);
  //
  Result := frxResMesFV_A4B;
end;

function TFmResMes.PreparaDadosPara_frxResMesFV_A4C(): TfrxReport;
var
  Qry: TmySQLQuery;
  Chart1: TfrxChartView;
  I: Integer;
begin
  TiraPontoMilhar(frxResMesFV_A4C);
  UnDmkDAC_PF.AbreMySQLQuery0(QrDC43, DModG.MyPID_DB, [
  'SELECT ',
  'SUM(IF(Mes01 < 0, -Mes01, 0)) M01_D, SUM(IF(Mes01 > 0, Mes01, 0)) M01_C, SUM(Mes01) M01, ',
  'SUM(IF(Mes02 < 0, -Mes02, 0)) M02_D, SUM(IF(Mes02 > 0, Mes02, 0)) M02_C, SUM(Mes02) M02, ',
  'SUM(IF(Mes03 < 0, -Mes03, 0)) M03_D, SUM(IF(Mes03 > 0, Mes03, 0)) M03_C, SUM(Mes03) M03, ',
  'SUM(IF(Mes04 < 0, -Mes04, 0)) M04_D, SUM(IF(Mes04 > 0, Mes04, 0)) M04_C, SUM(Mes04) M04, ',
  'SUM(IF(Mes05 < 0, -Mes05, 0)) M05_D, SUM(IF(Mes05 > 0, Mes05, 0)) M05_C, SUM(Mes05) M05, ',
  'SUM(IF(Mes06 < 0, -Mes06, 0)) M06_D, SUM(IF(Mes06 > 0, Mes06, 0)) M06_C, SUM(Mes06) M06, ',
  'SUM(IF(Mes07 < 0, -Mes07, 0)) M07_D, SUM(IF(Mes07 > 0, Mes07, 0)) M07_C, SUM(Mes07) M07, ',
  'SUM(IF(Mes08 < 0, -Mes08, 0)) M08_D, SUM(IF(Mes08 > 0, Mes08, 0)) M08_C, SUM(Mes08) M08, ',
  'SUM(IF(Mes09 < 0, -Mes09, 0)) M09_D, SUM(IF(Mes09 > 0, Mes09, 0)) M09_C, SUM(Mes09) M09, ',
  'SUM(IF(Mes10 < 0, -Mes10, 0)) M10_D, SUM(IF(Mes10 > 0, Mes10, 0)) M10_C, SUM(Mes10) M10, ',
  'SUM(IF(Mes11 < 0, -Mes11, 0)) M11_D, SUM(IF(Mes11 > 0, Mes11, 0)) M11_C, SUM(Mes11) M11, ',
  'SUM(IF(Mes12 < 0, -Mes12, 0)) M12_D, SUM(IF(Mes12 > 0, Mes12, 0)) M12_C, SUM(Mes12) M12 ',
  'FROM ' + FResMes,
  'WHERE Exclusivo="F" ',
  '']);
  //
  FTotalAnDeb := 0;
  FTotalAnCre := 0;
  FTotalAnDeC := 0;
  Qry := TmySQLQuery.Create(DModG);
  try
    for I := 0 to 2 do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
      'SELECT SUM( ',
      'Mes01+Mes02+Mes03+Mes04+ ',
      'Mes05+Mes06+Mes07+Mes08+ ',
      'Mes09+Mes10+Mes11+Mes12) Valor ',
      'FROM ' + FResMes,
      'WHERE TipoAgrupa=' + Geral.FF0(I),
      '']);
      case I of
        0: FTotalAnDeb := Qry.FieldByName('Valor').AsFloat;
        1: FTotalAnCre := Qry.FieldByName('Valor').AsFloat;
        2: FTotalAnDeC := Qry.FieldByName('Valor').AsFloat;
        else Geral.MB_Erro('FTotAn??? n�o implementado!');
      end;
    end;
  finally
    Qry.Free;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr43LM, DModG.MyPID_DB, [
  'SELECT DISTINCT TipoAgrupa, LimChrtLin ',
  'FROM ' + FResMes,
  'WHERE TipoAgrupa IN (0,1) ',
  'ORDER BY TipoAgrupa, LimChrtLin ',
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrD43, DModG.MyPID_DB, [
  'SELECT ' + FCampoChart+ ' Nome, -SUM( ',
  'Mes01+Mes02+Mes03+Mes04+ ',
  'Mes05+Mes06+Mes07+Mes08+ ',
  'Mes09+Mes10+Mes11+Mes12) Valor ',
  'FROM ' + FResMes,
  'WHERE TipoAgrupa=0 ',
  'GROUP BY ' + FTabelaChart,
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrC43, DModG.MyPID_DB, [
  'SELECT ' + FCampoChart + ' Nome, -SUM( ',
  'Mes01+Mes02+Mes03+Mes04+ ',
  'Mes05+Mes06+Mes07+Mes08+ ',
  'Mes09+Mes10+Mes11+Mes12) Valor ',
  'FROM ' + FResMes,
  'WHERE TipoAgrupa=1 ',
  'GROUP BY ' + FTabelaChart,
  '']);
  //////////// Saber (registrar) as mudancas para outros gr�ficos
  // Chart 1  - Pizza de Debitos!
  Chart1 := frxResMesFV_A4C.FindObject('ChartPieDeb') as TfrxChartView;
  Chart1.Chart.Legend.Alignment := laBottom;
  Chart1.Chart.Legend.Shadow.Visible := False;
  Chart1.Chart.View3D := False;
  Chart1.SeriesData[0].TopN := 20;
  Chart1.SeriesData[0].TopNCaption := FOutrosChart;
  //Chart1.SeriesData.Items[0].Circled := True;
  //TCustomChart(Chart1.SeriesData.Items[0]).Circled := True;
  // FIM Chart 1
  // Chart 2  - Pizza de Creditos!
  Chart1 := frxResMesFV_A4C.FindObject('ChartPieCre') as TfrxChartView;
  Chart1.Chart.Legend.Alignment := laBottom;
  Chart1.Chart.Legend.Shadow.Visible := False;
  Chart1.Chart.View3D := False;
  Chart1.SeriesData[0].TopN := 20;
  Chart1.SeriesData[0].TopNCaption := FOutrosChart;
  //Chart1.SeriesData.Items[0].Circled := True;
  //TCustomChart(Chart1.SeriesData.Items[0]).Circled := True;
  // FIM Chart 2
  // Chart 3  - Lines de Debitos!
  Chart1 := frxResMesFV_A4C.FindObject('ChartLinesDeb') as TfrxChartView;
  Chart1.Chart.LeftAxis.Axis.Color := clSilver;
  Chart1.Chart.LeftAxis.Axis.Width := 1;
  Chart1.Chart.LeftAxis.Grid.Color := clSilver;
  Chart1.Chart.LeftAxis.Grid.Width := 1;
  Chart1.Chart.LeftAxis.Grid.SmallDots := True;
  Chart1.Chart.BottomAxis.Axis.Color := clSilver;
  Chart1.Chart.BottomAxis.Axis.Width := 1;
  Chart1.Chart.BottomAxis.Grid.Color := clSilver;
  Chart1.Chart.BottomAxis.Grid.Width := 1;
  Chart1.Chart.BottomAxis.Grid.SmallDots := True;
  Chart1.Chart.Legend.LegendStyle := lsSeries;
  // FIM Chart 3
  //
  //
  frxResMesFV_A4C.Variables['VARF_HOW_MOSTRA_PLACTA'] := FHowShowA;
  frxResMesFV_A4C.Variables['VARF_ResmesV_TemRec']    := (QrResMesV.State <> dsInactive) and (QrResMesV.RecordCount > 0);
  frxResMesFV_A4C.Variables['MES_01']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-11));
  frxResMesFV_A4C.Variables['MES_02']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-10));
  frxResMesFV_A4C.Variables['MES_03']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-09));
  frxResMesFV_A4C.Variables['MES_04']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-08));
  frxResMesFV_A4C.Variables['MES_05']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-07));
  frxResMesFV_A4C.Variables['MES_06']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-06));
  frxResMesFV_A4C.Variables['MES_07']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-05));
  frxResMesFV_A4C.Variables['MES_08']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-04));
  frxResMesFV_A4C.Variables['MES_09']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-03));
  frxResMesFV_A4C.Variables['MES_10']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-02));
  frxResMesFV_A4C.Variables['MES_11']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-01));
  frxResMesFV_A4C.Variables['MES_12']    := QuotedStr(dmkPF.MesEAnoDoPeriodo(FPeriodo-00));
  //
  MyObjects.frxDefineDataSets(frxResMesFV_A4C, [
  frxDs43LM,
  frxDsC43,
  frxDsConsMes,
  frxDsD43,
  frxDsDC43,
  frxDsResMesF,
  frxDsResMesV,
  frxDsTSG,
  frxDsU00,
  frxDsU01,
  frxDsU02,
  frxDsU03,
  frxDsU04,
  frxDsU05,
  frxDsU06,
  frxDsU07,
  frxDsU08,
  frxDsU09,
  frxDsU10,
  frxDsU11,
  frxDsU12,
  frxDsV00,
  frxDsV01,
  frxDsV02,
  frxDsV03,
  frxDsV04,
  frxDsV05,
  frxDsV06,
  frxDsV07,
  frxDsV08,
  frxDsV09,
  frxDsV10,
  frxDsV11,
  frxDsV12,
  //
  FmCashBal.frxDsEntiCfgRel_01,
  FmCashBal.frxDsEmp,
  DModG.frxDsDono
  ]);
  Result := frxResMesFV_A4C;
end;

procedure TFmResMes.QrV00CalcFields(DataSet: TDataSet);
begin
  QrV00SDO_PER.Value := QrV00Saldo.Value + QrC00Saldo.Value;// + QrInicialSaldo.Value;
end;

procedure TFmResMes.QrV01CalcFields(DataSet: TDataSet);
begin
  QrV01SDO_PER.Value := QrV01Saldo.Value + QrV00SDO_PER.Value;
end;

procedure TFmResMes.QrV02CalcFields(DataSet: TDataSet);
begin
  QrV02SDO_PER.Value := QrV02Saldo.Value + QrV01SDO_PER.Value;
end;

procedure TFmResMes.QrV03CalcFields(DataSet: TDataSet);
begin
  QrV03SDO_PER.Value := QrV03Saldo.Value + QrV02SDO_PER.Value;
end;

procedure TFmResMes.QrV04CalcFields(DataSet: TDataSet);
begin
  QrV04SDO_PER.Value := QrV04Saldo.Value + QrV03SDO_PER.Value;
end;

procedure TFmResMes.QrV05CalcFields(DataSet: TDataSet);
begin
  QrV05SDO_PER.Value := QrV05Saldo.Value + QrV04SDO_PER.Value;
end;

procedure TFmResMes.QrV06CalcFields(DataSet: TDataSet);
begin
  QrV06SDO_PER.Value := QrV06Saldo.Value + QrV05SDO_PER.Value;
end;

procedure TFmResMes.QrV07CalcFields(DataSet: TDataSet);
begin
  QrV07SDO_PER.Value := QrV07Saldo.Value + QrV06SDO_PER.Value;
end;

procedure TFmResMes.QrV08CalcFields(DataSet: TDataSet);
begin
  QrV08SDO_PER.Value := QrV08Saldo.Value + QrV07SDO_PER.Value;
end;

procedure TFmResMes.QrV09CalcFields(DataSet: TDataSet);
begin
  QrV09SDO_PER.Value := QrV09Saldo.Value + QrV08SDO_PER.Value;
end;

procedure TFmResMes.QrV10CalcFields(DataSet: TDataSet);
begin
  QrV10SDO_PER.Value := QrV10Saldo.Value + QrV09SDO_PER.Value;
end;

procedure TFmResMes.QrV11CalcFields(DataSet: TDataSet);
begin
  QrV11SDO_PER.Value := QrV11Saldo.Value + QrV10SDO_PER.Value;
end;

procedure TFmResMes.QrV12CalcFields(DataSet: TDataSet);
begin
  QrV12SDO_PER.Value := QrV12Saldo.Value + QrV11SDO_PER.Value;
  //
  QrV12TCredito.Value :=
  QrV01Credito.Value + QrV02Credito.Value + QrV03Credito.Value +
  QrV04Credito.Value + QrV05Credito.Value + QrV06Credito.Value +
  QrV07Credito.Value + QrV08Credito.Value + QrV09Credito.Value +
  QrV10Credito.Value + QrV11Credito.Value + QrV12Credito.Value;
  //
  QrV12TDebito.Value :=
  QrV01Debito.Value + QrV02Debito.Value + QrV03Debito.Value +
  QrV04Debito.Value + QrV05Debito.Value + QrV06Debito.Value +
  QrV07Debito.Value + QrV08Debito.Value + QrV09Debito.Value +
  QrV10Debito.Value + QrV11Debito.Value + QrV12Debito.Value;
  //
  QrV12TSaldo.Value := QrV12TCredito.Value - QrV12TDebito.Value;
  //
end;

procedure TFmResMes.TamanhoA3297x420cm1Click(Sender: TObject);
begin
  ImprimeResMesExclusivo(3, ProgressBar1, ProgressBar2, LaAviso1);
end;

procedure TFmResMes.TiraPontoMilhar(frxReport: TfrxReport);
var
  I: Integer;
  m: TfrxMemoView;
begin
  if FPontoMilhar then
    Exit;
  //
  for I := 0 to frxReport.ComponentCount - 1 do
  begin
    if frxReport.Components[I] is TfrxMemoView then
    begin
      with TfrxMemoView(frxReport.Components[I]) do
      if DisplayFormat.FormatStr = '%2.2n' then
        DisplayFormat.FormatStr := '%2.2f'
    end;
  end;
end;

procedure TFmResMes.Exportaparaexcel1Click(Sender: TObject);
begin
  (*ImprimeResMesExclusivo(0);
  If SaveDialog1.Execute then
  begin
     Dataset2Excel1.WorksheetName := 'Teste_afwserf.xls';//Table1.TableName;
     Dataset2Excel1.SaveDatasetAs(SaveDialog1.FileName);
     Dataset2Excel1.Workbook := nil;
  end;*)
end;

procedure TFmResMes.Retrato1Click(Sender: TObject);
begin
  ImprimeResMesExclusivo(4, ProgressBar1, ProgressBar2, LaAviso1);
end;

procedure TFmResMes.Paisagem1Click(Sender: TObject);
begin
  ImprimeResMesExclusivo(41, ProgressBar1, ProgressBar2, LaAviso1);
end;

procedure TFmResMes.GeraPendencias;
begin
  //FResPenM := UCriar.RecriaTempTable('ResPenM', DModG.QrUpdPID1, False);
  FResPenM :=
    UCriarFin.RecriaTempTableNovo(ntrtt_ResPenM, DMOdG.QrUpdPID1, False);
  //
(*
  QrCtas.Close;
  QrCtas.Params[00].AsInteger := FPeriodo-12;
  QrCtas.Params[01].AsInteger := FPeriodo;
  //QrCtas . O p e n ;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtas, DModG.MyPID_DB, [
  'SELECT DISTINCT(Conta) Conta, Nome ',
  'FROM ' + DmodFin.FCtasResMes,
  'WHERE Periodo BETWEEN ' + Geral.FF0(FPeriodo - 12),
  ' AND ' + Geral.FF0(FPeriodo),
  '']);
  //
  if QrCtas.RecordCount > 0 then
  begin
(*
    QrRMA.Close;
    QrRMA.Params[00].AsInteger := FPeriodo-12;
    QrRMA.Params[01].AsInteger := FPeriodo;
    //QrRMA . O p e n ;
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrRMA, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + DmodFin.FCtasResMes,
    'WHERE Periodo BETWEEN ' + Geral.FF0(FPeriodo - 12),
    ' AND ' + Geral.FF0(FPeriodo),
    '']);
    //
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FResPenM +
    ' SET Genero=:P0, NomeConta=:P1');
    while not QrCtas.Eof do
    begin
      DModG.QrUpdPID1.Params[0].AsInteger := QrCtasConta.Value;
      DModG.QrUpdPID1.Params[1].AsString  := QrCtasNome.Value;
      DModG.QrUpdPID1.ExecSQL;
      //
      QrCtas.Next;
    end;
    //
    while not QrRMA.Eof do
    begin
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('UPDATE ' + FResPenM + ' SET ' +
        MesStrPeriodo(QrRMAPeriodo.Value)+'=:P0 ');
      DModG.QrUpdPID1.SQL.Add('WHERE Genero=:P1');
      DModG.QrUpdPID1.Params[00].AsFloat := QrRMAAcumulado.Value;
      DModG.QrUpdPID1.Params[01].AsInteger := QrRMAConta.Value;
      DModG.QrUpdPID1.ExecSQL;
      //
      QrRMA.Next;
    end;
  end;
end;

function TFmResMes.MesStrPeriodo(Periodo: Integer): String;
var
  Mes: Integer;
begin
  Mes := Periodo - FPeriodo + 12;
  if Mes = 0 then Result := 'AnoAn' else
    Result := 'Mes' + FormatFloat('00', Mes);
end;

procedure TFmResMes.MontaChartLines(const frxReport: TFrxReport; const Objeto:
String; (*const TipoAgrupa: Integer;*) var Value: Variant);
var
  Chart1: TfrxChartView;
  //x: TfrxSeriesItem;
  XSource, YSource, SQL_LimChrtLin: String;
  I(*, J*): Integer;
  Qry: TmySQLQuery;
begin
  Value := True;
  XSource :=
    dmkPF.MesEAnoDoPeriodo(FPeriodo - 11) + ';' +
    dmkPF.MesEAnoDoPeriodo(FPeriodo - 10) + ';' +
    dmkPF.MesEAnoDoPeriodo(FPeriodo - 09) + ';' +
    dmkPF.MesEAnoDoPeriodo(FPeriodo - 08) + ';' +
    dmkPF.MesEAnoDoPeriodo(FPeriodo - 07) + ';' +
    dmkPF.MesEAnoDoPeriodo(FPeriodo - 06) + ';' +
    dmkPF.MesEAnoDoPeriodo(FPeriodo - 05) + ';' +
    dmkPF.MesEAnoDoPeriodo(FPeriodo - 04) + ';' +
    dmkPF.MesEAnoDoPeriodo(FPeriodo - 03) + ';' +
    dmkPF.MesEAnoDoPeriodo(FPeriodo - 02) + ';' +
    dmkPF.MesEAnoDoPeriodo(FPeriodo - 01) + ';' +
    dmkPF.MesEAnoDoPeriodo(FPeriodo - 00);
  //
  SQL_LimChrtLin := '';
(*
  case TipoAgrupa of
    0: SQL_LimChrtLin := 'AND LimChrtLin=' + Geral.FF0()

    end;
  end;
*)
  Chart1 := frxReport.FindObject(Objeto) as TfrxChartView;
  while Chart1.SeriesData.Count > 0 do
    Chart1.SeriesData[0].Free;
  while Chart1.Chart.SeriesCount > 0 do
    Chart1.Chart.Series[0].Free;
  //
  //Chart1.Chart.Legend.LegendStyle := lsSerie;
  /////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrDouC43, DModG.MyPID_DB, [
  'SELECT ' + FCampoChart + ' Nome, ',
  'SUM(Mes01) Mes01, SUM(Mes02) Mes02, SUM(Mes03) Mes03, ',
  'SUM(Mes04) Mes04, SUM(Mes05) Mes05, SUM(Mes06) Mes06, ',
  'SUM(Mes07) Mes07, SUM(Mes08) Mes08, SUM(Mes09) Mes09, ',
  'SUM(Mes10) Mes10, SUM(Mes11) Mes11, SUM(Mes12) Mes12 ',
  'FROM ' + FResMes,
  'WHERE TipoAgrupa=' + Geral.FF0(Qr43LMTipoAgrupa.Value),
  'AND LimChrtLin=' + Geral.FF0(Qr43LMLimChrtLin.Value),
  'AND Exclusivo = "F" ',
  SQL_LimChrtLin,
  'GROUP BY ' + FTabelaChart,
  '']);
  QrDouC43.First;
  while not QrDouC43.Eof do
  begin
    YSource :=
      Geral.FFI(QrDouC43Mes01.Value) + ';' +
      Geral.FFI(QrDouC43Mes02.Value) + ';' +
      Geral.FFI(QrDouC43Mes03.Value) + ';' +
      Geral.FFI(QrDouC43Mes04.Value) + ';' +
      Geral.FFI(QrDouC43Mes05.Value) + ';' +
      Geral.FFI(QrDouC43Mes06.Value) + ';' +
      Geral.FFI(QrDouC43Mes07.Value) + ';' +
      Geral.FFI(QrDouC43Mes08.Value) + ';' +
      Geral.FFI(QrDouC43Mes08.Value) + ';' +
      Geral.FFI(QrDouC43Mes10.Value) + ';' +
      Geral.FFI(QrDouC43Mes11.Value) + ';' +
      Geral.FFI(QrDouC43Mes12.Value);
    Chart1.AddSeries(TfrxChartSeries.csLine);
    I := QrDouC43.RecNo - 1;
    // do VCLTee.Chart.CustomChart:
    Chart1.Chart.Series[I].Pen.Width := 4;
    Chart1.Chart.Series[I].LegendTitle := QrDouC43Nome.Value;
    //Chart1.Chart.Series[I].Color := FCores[I];

    // do frxChat
    Chart1.SeriesData[I].DataType := dtFixedData;
    Chart1.SeriesData[I].XSource := XSource; //'Mes1; Mes2; Mes3'; Titulos!
    Chart1.SeriesData[I].YSource := YSource; //'31.5;28.54;31.58';
    Chart1.SeriesData[I].SortOrder := soNone;
    Chart1.SeriesData[I].TopN := 0;
    Chart1.SeriesData[I].XType := xtText;
    //
    QrDouC43.Next;
  end;
end;

procedure TFmresMes.ImprimeSaldoEm();
var
  Data, TabLct, FldSdoIni: String;
begin
  //if UMyMod.AcessoNegadoAoForm(ivTabPerfis, 'Relatorios', 0) then Exit;
  Data       := FormatDateTime(VAR_FORMATDATE, Int(TPSaldo.Date));
  TabLct     := DefineLctTabela(Int(TPSaldo.Date));
  FldSdoIni  := DefineLctFieldSdoIni(Int(TPSaldo.Date));
  FSaldos    := UCriarFin.RecriaTempTableNovo(ntrtt_Saldos, DModG.QrUpdPID1, False);
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO '+FSaldos+' SET ');
  DModG.QrUpdPID1.SQL.Add('Nome=:P0, Saldo=:P1, Tipo=:P2');
  QrSaldos.Close;
  QrSaldos.SQL.Clear;
{
SELECT car.Nome,
(SUM(la.Credito-la.Debito) + car.Inicial) Saldo
FROM lct la
LEFT JOIN carteiras car ON car.Codigo=la.carteira
WHERE la.Tipo < 2
AND la.Data <= :P0
AND car.Exclusivo <= :P1
AND la.CliInt=:P2
GROUP BY car.Nome
}
  QrSaldos.SQL.Add('SELECT car.Nome,');
  QrSaldos.SQL.Add('(SUM(la.Credito-la.Debito) + ' + FldSdoIni + ') Saldo');
  QrSaldos.SQL.Add('FROM ' + TabLct + ' la');
  QrSaldos.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=la.Carteira');
  QrSaldos.SQL.Add('WHERE la.Tipo < 2');
  QrSaldos.SQL.Add('AND la.Data <= :P0');
  QrSaldos.SQL.Add('AND car.Exclusivo <= :P1');
  QrSaldos.SQL.Add('AND la.CliInt=:P2');
  QrSaldos.SQL.Add('GROUP BY car.Nome');
  QrSaldos.Params[00].AsString  := Data;
  QrSaldos.Params[01].AsInteger := Geral.BoolToInt(CkExclusivo.Checked);
  QrSaldos.Params[02].AsInteger := FEntidade;
  //QrSaldos . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrSaldos, Dmod.MyDB);
  while not QrSaldos.Eof do
  begin
    if (CkNaoZero.Checked = False) or (QrSaldosSaldo.Value <> 0) then
    begin
      DModG.QrUpdPID1.Params[0].AsString  := QrSaldosNome.Value;
      DModG.QrUpdPID1.Params[1].AsFloat   := QrSaldosSaldo.Value;
      DModG.QrUpdPID1.Params[2].AsInteger := 1;
      DModG.QrUpdPID1.ExecSQL;
    end;
    QrSaldos.Next;
  end;
  //
  (* A tabela consignacoesits n�o existe mais
  UnDmkDAC_PF.AbreMySQLQuery0(QrConsigna, Dmod.MyDB, [
  'SELECT co.Descricao Nome, SUM(ci.Debito-ci.Credito) Saldo ',
  'FROM consignacoesits ci, Consignacoes co ',
  'WHERE ci.Codigo=co.Codigo ',
  'AND ci.Data<="' + Data + '" ',
  'GROUP BY co.Codigo ',
  '']);
  //
  while not QrConsigna.Eof do
  begin
    if (CkNaoZero.Checked = False) or (QrConsignaSaldo.Value <> 0) then
    begin
      DModG.QrUpdPID1.Params[0].AsString  := QrConsignaNome.Value;
      DModG.QrUpdPID1.Params[1].AsFloat   := QrConsignaSaldo.Value;
      DModG.QrUpdPID1.Params[2].AsInteger := 2;
      DModG.QrUpdPID1.ExecSQL;
    end;
    QrConsigna.Next;
  end;
  *)
  //
  QrAVencer.Close;
{
SELECT ca.Nome,
  IF(la.Sit = 0, SUM(la.Credito - la.Debito),
  IF(la.Sit = 1, SUM(la.Credito - la.Debito - la.Pago), 0)) SALDO
FROM lct la
LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira
WHERE la.Tipo=2
AND IF(la.Sit = 0, la.Credito - la.Debito,
    IF(la.Sit = 1, la.Credito - la.Debito - la.Pago, 0)) <> 0
AND la.CliInt=:P0
GROUP BY la.Carteira
}
  QrAVencer.SQL.Clear;
  QrAVencer.SQL.Add('SELECT ca.Nome,');
  QrAVencer.SQL.Add('  IF(la.Sit = 0, SUM(la.Credito - la.Debito),');
  QrAVencer.SQL.Add('  IF(la.Sit = 1, SUM(la.Credito - la.Debito - la.Pago), ');
  QrAVencer.SQL.Add('    0)) SALDO');
  QrAVencer.SQL.Add('FROM ' + FTabLctA + ' la');
  QrAVencer.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  QrAVencer.SQL.Add('WHERE la.Tipo=2');
  QrAVencer.SQL.Add('AND IF(la.Sit = 0, la.Credito - la.Debito,');
  QrAVencer.SQL.Add('    IF(la.Sit = 1, la.Credito - la.Debito - la.Pago, 0))');
  QrAVencer.SQL.Add('      <> 0');
  QrAVencer.SQL.Add('AND la.CliInt=:P0');
  QrAVencer.SQL.Add('GROUP BY la.Carteira');
  QrAVencer.Params[0].AsInteger := FEntidade;
  //QrAVencer . O p e n ;
  UnDmkDAC_PF.AbreQuery(QrAVencer, DModG.MyPID_DB);
  while not QrAVencer.Eof do
  begin
    DModG.QrUpdPID1.Params[00].AsString  := QrAVencerNome.Value;
    DModG.QrUpdPID1.Params[01].AsFloat   := QrAVencerSaldo.Value;
    DModG.QrUpdPID1.Params[02].AsInteger := 3;
    DModG.QrUpdPID1.ExecSQL;
    QrAVencer.Next;
  end;
  //
  QrSaldos.Close;
  QrConsigna.Close;
  QrTotalSaldo.Close;
  //QrTotalSaldo . O p e n ;
  UnDmkDAC_PF.AbreMySQLQuery0(QrTotalSaldo, Dmod.MyDB, [
  'SELECT * FROM saldos ',
  '']);
  //
  MyObjects.frxMostra(frxResMesFV_A4, 'Demosntrativo de movimento Mensal');
end;

procedure TFmResMes.frxCtasResMesGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxCtasResMes, VarName, Value);
end;

procedure TFmResMes.frxReport000GetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
begin
  if VarName = 'PERIODO'
    then Value := dmkPF.PrimeiroDiaDoPeriodo(FPeriodo, dtTexto)
(*
  else if VarName = 'MES_01' then Value := dmkPF.MesEAnoDoPeriodo(FPeriodo-11)
  else if VarName = 'MES_02' then Value := dmkPF.MesEAnoDoPeriodo(FPeriodo-10)
  else if VarName = 'MES_03' then Value := dmkPF.MesEAnoDoPeriodo(FPeriodo-09)
  else if VarName = 'MES_04' then Value := dmkPF.MesEAnoDoPeriodo(FPeriodo-08)
  else if VarName = 'MES_05' then Value := dmkPF.MesEAnoDoPeriodo(FPeriodo-07)
  else if VarName = 'MES_06' then Value := dmkPF.MesEAnoDoPeriodo(FPeriodo-06)
  else if VarName = 'MES_07' then Value := dmkPF.MesEAnoDoPeriodo(FPeriodo-05)
  else if VarName = 'MES_08' then Value := dmkPF.MesEAnoDoPeriodo(FPeriodo-04)
  else if VarName = 'MES_09' then Value := dmkPF.MesEAnoDoPeriodo(FPeriodo-03)
  else if VarName = 'MES_10' then Value := dmkPF.MesEAnoDoPeriodo(FPeriodo-02)
  else if VarName = 'MES_11' then Value := dmkPF.MesEAnoDoPeriodo(FPeriodo-01)
  else if VarName = 'MES_12' then Value := dmkPF.MesEAnoDoPeriodo(FPeriodo-00)
*)
  else if VarName = 'VARF_SALDOS' then Value := Geral.BoolToInt(CkSaldo.Checked)
  else if VarName = 'VAR_DATA_SALDO' then Value := Geral.FDT(TPSaldo.Date, 3)
(*  ATEN��O!!! Usar [frxDsTSG."T00"]  a [frxDsTSG."PEN"]
  else
  if VarName = 'VARF_PERC_SGR' then
    DefinePercentualSubGrupo(Value)
  else if VarName = 'VARF_PERC_CTA' then
    DefinePercentualConta(Value)
  else if VarName = 'VARF_VAL_TO_PERC' then
  begin
    case QrTSGTipoAgrupa.Value of
      0: Value := FTotalAnDeb;
      1: Value := FTotalAnCre;
      2: Value := FTotalAnDeC;
      else // Erro
      begin
        Value := 0;
        Geral.MB_Erro('FTotalAn??? sem defini��o de $ para %!');
      end;
    end;
  end
*)
  else if VarName = 'VARF_PAGINAR' then
    Value := FPaginar
(*
  else if VarName = 'VARF_NO_TIPOAGRUPA' then
  begin
    case QrResMesFTipoAgrupa.Value of
      0: Value := 'D�bito';
      1: Value := 'Cr�bito';
      2: Value := 'D�bito e Cr�dito';
      else Value := '? ? ?';
    end;
    Value := 'Tipo de Agrupamento do Sub Grupo: ' + Value;
  end
  else if VarName = 'VARF_ResmesV_TemRec' then
    Value := (QrResMesV.State <> dsInactive) and (QrResMesV.RecordCount > 0)
  else if VarName = 'VARF_HOW_MOSTRA_PLACTA' then
    Value := FHowShowA
*)
  else
  if VarName = 'VARF_ChartLinesDeb' then
    MontaChartLines(frxReport, 'ChartLinesDeb', (*0,*) Value)
  else
end;

procedure TFmResMes.frxResMesFV_A3GetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxResMesFV_A3, VarName, Value);
end;

procedure TFmResMes.frxResMesFV_A41GetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxResMesFV_A41, VarName, Value);
end;

procedure TFmResMes.frxResMesFV_A4BGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxResMesFV_A4B, VarName, Value);
end;

procedure TFmResMes.frxResMesFV_A4CGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxResMesFV_A4C, VarName, Value);
end;

procedure TFmResMes.frxResMesFV_A4GetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxResMesFV_A4, VarName, Value);
end;

procedure TFmResMes.frxResMesGetValue(const VarName: String;
  var Value: Variant);
begin
  frxReport000GetValue(frxResMes, VarName, Value);
end;

{
function TFmResMes.ObtemTotalSubGrupo(Mes: Integer): Double;
var
  SubGrupo: Integer;
begin
  case FPrintNo of
     1: SubGrupo := QrResmesSubGrupo.Value;
     3: SubGrupo := QrResmesFSubGrupo.Value;
     4: SubGrupo := QrResmesFSubGrupo.Value;
    41: SubGrupo := QrResmesFSubGrupo.Value;
    42: SubGrupo := QrResmesFSubGrupo.Value;
    else begin
      SubGrupo := 0;
      Geral.MB_('"FPrintNo" sem a��o definida na function ' +
      '"ObtemTotalSubGrupo"!'));
    end;
  end;
(*  if QrTSG.Locate('SubGrupo', SubGrupo, []) then
  begin
  QrTSG.Close;
  QrTSG.Params[0].AsInteger := SubGrupo;
  QrTSG . O p e n ;
*)
    case Mes of
      00: Result := QrTSGT00.Value;
      01: Result := QrTSGT01.Value;
      02: Result := QrTSGT02.Value;
      03: Result := QrTSGT03.Value;
      04: Result := QrTSGT04.Value;
      05: Result := QrTSGT05.Value;
      06: Result := QrTSGT06.Value;
      07: Result := QrTSGT07.Value;
      08: Result := QrTSGT08.Value;
      09: Result := QrTSGT09.Value;
      10: Result := QrTSGT10.Value;
      11: Result := QrTSGT11.Value;
      12: Result := QrTSGT12.Value;
      13: Result := QrTSGTTO.Value;
      14: Result := QrTSGTPN.Value;
      else Result := 0;
    end;
(*
  end else Result := 0;
  Memo1.Lines.Add(
  'FPrintNo = ' + FormatFloat('0000', FPrintNo) +
  'Mes = ' + FormatFloat('00', Mes) +
  'SubGrupo = ' + FormatFloat('000000', SubGrupo) +
  'Valor = ' + FormatFloat('#,###,###,###,###,##0.00', Result));
*)
end;
}

procedure TFmResMes.QrTSGCalcFields(DataSet: TDataSet);
var
  Valor: Variant;
begin
  QrTSGTTO.Value :=
  //QrTSGT00.Value +
  QrTSGT01.Value +
  QrTSGT02.Value +
  QrTSGT03.Value +
  QrTSGT04.Value +
  QrTSGT05.Value +
  QrTSGT06.Value +
  QrTSGT07.Value +
  QrTSGT08.Value +
  QrTSGT09.Value +
  QrTSGT10.Value +
  QrTSGT11.Value +
  QrTSGT12.Value;
  //
  DefinePercentualSubGrupo(Valor);
  QrTSGPerc.Value := Valor;
end;

end.

