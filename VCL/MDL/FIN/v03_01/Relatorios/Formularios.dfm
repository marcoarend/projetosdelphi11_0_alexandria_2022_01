object FmFormularios: TFmFormularios
  Left = 440
  Top = 183
  Caption = 'FIN-RELAT-009 :: Impress'#245'es Diversas'
  ClientHeight = 364
  ClientWidth = 758
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 758
    Height = 202
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 92
      Width = 52
      Height = 13
      Caption = 'Sub-grupo:'
    end
    object Label4: TLabel
      Left = 12
      Top = 8
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object RGRelatorio: TRadioGroup
      Left = 12
      Top = 52
      Width = 401
      Height = 37
      Caption = ' Relat'#243'rio: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Formul'#225'rios'
        'Mensalidades')
      TabOrder = 0
      OnClick = RGRelatorioClick
    end
    object CBSubgrupo: TdmkDBLookupComboBox
      Left = 68
      Top = 108
      Width = 345
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsSubgrupos
      TabOrder = 3
      dmkEditCB = EdSubGrupo
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object RGFormulario: TRadioGroup
      Left = 420
      Top = 52
      Width = 160
      Height = 105
      Caption = ' Formul'#225'rio: '
      ItemIndex = 0
      Items.Strings = (
        'Sub-grupos - meses')
      TabOrder = 1
    end
    object RGMensalidade: TRadioGroup
      Left = 584
      Top = 52
      Width = 160
      Height = 105
      Caption = ' Mensalidade: '
      Enabled = False
      ItemIndex = 0
      Items.Strings = (
        'Tudo'
        'Positivos'
        'Negativos'
        'Positivos ou negativos')
      TabOrder = 2
    end
    object GroupBox1: TGroupBox
      Left = 12
      Top = 136
      Width = 401
      Height = 53
      Caption = ' Per'#237'odo: '
      TabOrder = 4
      object Label3: TLabel
        Left = 222
        Top = 12
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object Label2: TLabel
        Left = 60
        Top = 12
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object TPIni: TdmkEditDateTimePicker
        Left = 60
        Top = 28
        Width = 131
        Height = 21
        Date = 37660.516375590300000000
        Time = 37660.516375590300000000
        TabOrder = 0
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object TPFim: TdmkEditDateTimePicker
        Left = 222
        Top = 28
        Width = 131
        Height = 21
        Date = 37660.516439456000000000
        Time = 37660.516439456000000000
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
    end
    object ProgressBar1: TProgressBar
      Left = 420
      Top = 172
      Width = 325
      Height = 17
      TabOrder = 5
    end
    object EdSubGrupo: TdmkEditCB
      Left = 12
      Top = 108
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBSubgrupo
      IgnoraDBLookupComboBox = False
    end
    object EdEmpresa: TdmkEditCB
      Left = 12
      Top = 24
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 68
      Top = 24
      Width = 677
      Height = 21
      KeyField = 'FILIAL'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 8
      dmkEditCB = EdEmpresa
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 758
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 710
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 662
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 255
        Height = 32
        Caption = 'Impress'#245'es Diversas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 255
        Height = 32
        Caption = 'Impress'#245'es Diversas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 255
        Height = 32
        Caption = 'Impress'#245'es Diversas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 294
    Width = 758
    Height = 70
    Align = alBottom
    TabOrder = 2
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 754
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object PainelControle: TPanel
        Left = 612
        Top = 0
        Width = 142
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSair: TBitBtn
          Tag = 13
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSairClick
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 250
    Width = 758
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 754
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrSubGrupos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM subgrupos')
    Left = 348
    Top = 128
    object QrSubGruposCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSubGruposNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrSubGruposGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrSubGruposLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSubGruposDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSubGruposDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSubGruposUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrSubGruposUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
  end
  object DsSubgrupos: TDataSource
    DataSet = QrSubGrupos
    Left = 376
    Top = 128
  end
  object QrContas1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas')
    Left = 12
    Top = 160
    object QrContas1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContas1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrContas1Nome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrContas1Nome3: TWideStringField
      FieldName = 'Nome3'
      Size = 50
    end
    object QrContas1ID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContas1Subgrupo: TIntegerField
      FieldName = 'Subgrupo'
    end
    object QrContas1Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrContas1Credito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContas1Debito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContas1Mensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContas1Exclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContas1Mensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContas1Mensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContas1Mensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContas1Menscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContas1Mensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContas1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContas1Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContas1Excel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContas1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContas1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContas1UserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContas1UserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
  end
  object QrContas2: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrContas2CalcFields
    SQL.Strings = (
      'SELECT * FROM contas'
      'WHERE Mensal="V"'
      'ORDER BY Nome')
    Left = 168
    Top = 144
    object QrContas2ESPERADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ESPERADO'
      Calculated = True
    end
    object QrContas2MINIMO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MINIMO'
      Calculated = True
    end
    object QrContas2SCREDITO: TFloatField
      FieldKind = fkLookup
      FieldName = 'SCREDITO'
      LookupDataSet = QrSoma2
      LookupKeyFields = 'Genero'
      LookupResultField = 'Credito'
      KeyFields = 'Codigo'
      Lookup = True
    end
    object QrContas2SDEBITO: TFloatField
      FieldKind = fkLookup
      FieldName = 'SDEBITO'
      LookupDataSet = QrSoma2
      LookupKeyFields = 'Genero'
      LookupResultField = 'Debito'
      KeyFields = 'Codigo'
      Lookup = True
    end
    object QrContas2STOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'STOTAL'
      Calculated = True
    end
    object QrContas2QTDE: TFloatField
      FieldKind = fkLookup
      FieldName = 'QTDE'
      LookupDataSet = QrSoma2
      LookupKeyFields = 'Genero'
      LookupResultField = 'QTDE'
      KeyFields = 'Codigo'
      Lookup = True
    end
    object QrContas2MEDIA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MEDIA'
      Calculated = True
    end
    object QrContas2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContas2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrContas2Nome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrContas2Nome3: TWideStringField
      FieldName = 'Nome3'
      Size = 50
    end
    object QrContas2ID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContas2Subgrupo: TIntegerField
      FieldName = 'Subgrupo'
    end
    object QrContas2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrContas2Credito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContas2Debito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContas2Mensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContas2Exclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContas2Mensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContas2Mensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContas2Mensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContas2Menscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContas2Mensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContas2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContas2Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContas2Excel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContas2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContas2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContas2UserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContas2UserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
  end
  object QrSoma2: TmySQLQuery
   
    OnCalcFields = QrContas2CalcFields
    Left = 168
    Top = 172
    object QrSoma2Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrSoma2Credito: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Credito'
    end
    object QrSoma2Debito: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Debito'
    end
    object QrSoma2QTDE: TFloatField
      FieldName = 'QTDE'
      Required = True
    end
  end
  object QrMalaDireta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome FROM contas')
    Left = 224
    Top = 200
    object QrMalaDiretaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMalaDiretaNome: TWideStringField
      FieldName = 'Nome'
      Size = 29
    end
  end
  object QrContas3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM maladireta')
    Left = 196
    Top = 172
    object QrContas3L1C1: TWideStringField
      FieldName = 'L1C1'
      Size = 128
    end
    object QrContas3L1C2: TWideStringField
      FieldName = 'L1C2'
      Size = 128
    end
    object QrContas3L1C3: TWideStringField
      FieldName = 'L1C3'
      Size = 128
    end
    object QrContas3L1C4: TWideStringField
      FieldName = 'L1C4'
      Size = 128
    end
    object QrContas3L2C1: TWideStringField
      FieldName = 'L2C1'
      Size = 128
    end
    object QrContas3L2C2: TWideStringField
      FieldName = 'L2C2'
      Size = 128
    end
    object QrContas3L2C3: TWideStringField
      FieldName = 'L2C3'
      Size = 128
    end
    object QrContas3L2C4: TWideStringField
      FieldName = 'L2C4'
      Size = 128
    end
    object QrContas3L3C1: TWideStringField
      FieldName = 'L3C1'
      Size = 128
    end
    object QrContas3L3C2: TWideStringField
      FieldName = 'L3C2'
      Size = 128
    end
    object QrContas3L3C3: TWideStringField
      FieldName = 'L3C3'
      Size = 128
    end
    object QrContas3L3C4: TWideStringField
      FieldName = 'L3C4'
      Size = 128
    end
    object QrContas3L4C1: TWideStringField
      FieldName = 'L4C1'
      Size = 128
    end
    object QrContas3L4C2: TWideStringField
      FieldName = 'L4C2'
      Size = 128
    end
    object QrContas3L4C3: TWideStringField
      FieldName = 'L4C3'
      Size = 128
    end
    object QrContas3L4C4: TWideStringField
      FieldName = 'L4C4'
      Size = 128
    end
    object QrContas3L5C1: TWideStringField
      FieldName = 'L5C1'
      Size = 128
    end
    object QrContas3L5C2: TWideStringField
      FieldName = 'L5C2'
      Size = 128
    end
    object QrContas3L5C3: TWideStringField
      FieldName = 'L5C3'
      Size = 128
    end
    object QrContas3L5C4: TWideStringField
      FieldName = 'L5C4'
      Size = 128
    end
  end
  object QrListaconta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.Codigo CODIGOCONTA, co.Nome NOMECONTA, '
      'sg.Codigo CODIGOSUBGRUPO, sg.Nome NOMESUBGRUPO, '
      'gr.Codigo CODIGOGRUPO, gr.Nome NOMEGRUPO, '
      'cj.Codigo CODIGOCONJUNTO, cj.Nome NOMECONJUNTO,'
      'pl.Codigo CODIGOPLANO, pl.Nome NOMEPLANO'
      'FROM contas co, Subgrupos sg, Grupos gr,'
      'Conjuntos cj, Plano pl'
      'WHERE sg.Codigo=co.Subgrupo '
      'AND gr.Codigo=sg.Grupo'
      'AND cj.Codigo=gr.Conjunto'
      'ORDER BY cj.Nome, gr.Nome, sg.Nome, co.Nome, pl.Nome')
    Left = 252
    Top = 200
    object QrListacontaCODIGOCONTA: TIntegerField
      FieldName = 'CODIGOCONTA'
    end
    object QrListacontaNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 25
    end
    object QrListacontaCODIGOSUBGRUPO: TIntegerField
      FieldName = 'CODIGOSUBGRUPO'
    end
    object QrListacontaNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 4
    end
    object QrListacontaCODIGOGRUPO: TIntegerField
      FieldName = 'CODIGOGRUPO'
    end
    object QrListacontaNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 4
    end
    object QrListacontaCODIGOCONJUNTO: TIntegerField
      FieldName = 'CODIGOCONJUNTO'
    end
    object QrListacontaNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 4
    end
    object QrListacontaCODIGOPLANO: TIntegerField
      FieldName = 'CODIGOPLANO'
      Required = True
    end
    object QrListacontaNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Required = True
      Size = 50
    end
  end
  object QrCadastros: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTIDADE,'
      'ent.*,'
      'euf.nome NOMEEUF, puf.nome NOMEPUF'
      'FROM entidades ent, Ufs euf, UFs puf'
      'WHERE euf.Codigo=ent.EUF'
      'AND puf.Codigo=ent.PUF'
      'ORDER BY ent.Codigo')
    Left = 56
    Top = 120
    object QrCadastrosNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 49
    end
    object QrCadastrosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCadastrosRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrCadastrosFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrCadastrosRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrCadastrosRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrCadastrosPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrCadastrosMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrCadastrosCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrCadastrosIE: TWideStringField
      FieldName = 'IE'
    end
    object QrCadastrosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCadastrosApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrCadastrosCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrCadastrosRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrCadastrosERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrCadastrosENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrCadastrosECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrCadastrosEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrCadastrosECIDADE: TWideStringField
      FieldName = 'ECIDADE'
      Size = 25
    end
    object QrCadastrosEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrCadastrosECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrCadastrosEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrCadastrosETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrCadastrosETe2: TWideStringField
      FieldName = 'ETe2'
    end
    object QrCadastrosETe3: TWideStringField
      FieldName = 'ETe3'
    end
    object QrCadastrosECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrCadastrosEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrCadastrosEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 100
    end
    object QrCadastrosEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrCadastrosENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrCadastrosPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrCadastrosPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrCadastrosPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrCadastrosPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrCadastrosPCIDADE: TWideStringField
      FieldName = 'PCIDADE'
      Size = 25
    end
    object QrCadastrosPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrCadastrosPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrCadastrosPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrCadastrosPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrCadastrosPTe2: TWideStringField
      FieldName = 'PTe2'
    end
    object QrCadastrosPTe3: TWideStringField
      FieldName = 'PTe3'
    end
    object QrCadastrosPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrCadastrosPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrCadastrosPEMail: TWideStringField
      FieldName = 'PEMail'
      Size = 100
    end
    object QrCadastrosPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrCadastrosPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrCadastrosSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrCadastrosResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrCadastrosProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrCadastrosCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrCadastrosRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrCadastrosDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrCadastrosAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrCadastrosAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrCadastrosCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrCadastrosCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrCadastrosFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrCadastrosFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrCadastrosFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrCadastrosFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrCadastrosTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrCadastrosCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrCadastrosInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrCadastrosLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrCadastrosVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrCadastrosMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrCadastrosObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCadastrosTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCadastrosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCadastrosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCadastrosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCadastrosUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrCadastrosUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrCadastrosCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrCadastrosCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrCadastrosCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrCadastrosCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrCadastrosCCIDADE: TWideStringField
      FieldName = 'CCIDADE'
      Size = 25
    end
    object QrCadastrosCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrCadastrosCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrCadastrosCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrCadastrosCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrCadastrosCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrCadastrosCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrCadastrosCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrCadastrosLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrCadastrosLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrCadastrosLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrCadastrosLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrCadastrosLCIDADE: TWideStringField
      FieldName = 'LCIDADE'
      Size = 25
    end
    object QrCadastrosLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrCadastrosLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrCadastrosLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrCadastrosLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrCadastrosLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrCadastrosLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrCadastrosLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrCadastrosComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrCadastrosSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrCadastrosNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrCadastrosGrupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrCadastrosAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
    object QrCadastrosLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrCadastrosNOMEEUF: TWideStringField
      FieldName = 'NOMEEUF'
      Required = True
      Size = 2
    end
    object QrCadastrosNOMEPUF: TWideStringField
      FieldName = 'NOMEPUF'
      Required = True
      Size = 2
    end
  end
  object DsCadastros: TDataSource
    DataSet = QrCadastros
    Left = 88
    Top = 120
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTIDADE,'
      'CASE 1 WHEN ent.Tipo=0 THEN ent.ERua'
      'ELSE ent.PRua END RUA,'
      'CASE 1 WHEN ent.Tipo=0 THEN ent.ENumero'
      'ELSE ent.PNumero END NUMERO,'
      'CASE 1 WHEN ent.Tipo=0 THEN ent.ECompl'
      'ELSE ent.PCompl END COMPL,'
      'CASE 1 WHEN ent.Tipo=0 THEN ent.ECidade'
      'ELSE ent.PCidade END CIDADE,'
      'CASE 1 WHEN ent.Tipo=0 THEN ent.ECEP'
      'ELSE ent.PCEP END CEP,'
      'CASE 1 WHEN ent.Tipo=0 THEN euf.Nome'
      'ELSE puf.Nome END NOMEUF'
      'FROM entidades ent, Ufs euf, UFs puf'
      'WHERE euf.Codigo=ent.EUF'
      'AND puf.Codigo=ent.PUF'
      'ORDER BY ent.Codigo')
    Left = 20
    Top = 120
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrEntidadesRUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 30
    end
    object QrEntidadesNUMERO: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'NUMERO'
    end
    object QrEntidadesCOMPL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEntidadesCIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEntidadesCEP: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'CEP'
    end
    object QrEntidadesNOMEUF: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEUF'
      Size = 2
    end
  end
  object QrUFs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ufs'
      'ORDER BY Nome')
    Left = 340
    Top = 196
    object QrUFsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUFsNome: TWideStringField
      FieldName = 'Nome'
      Size = 2
    end
  end
  object DsUFs: TDataSource
    DataSet = QrUFs
    Left = 368
    Top = 196
  end
  object QrEntiCart: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'WHERE Codigo IN ('
      '  SELECT ForneceI '
      '  FROM carteiras'
      '  WHERE ForneceI <> 0)'
      'ORDER BY NOMEENT')
    Left = 348
    Top = 8
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsEntiCart: TDataSource
    DataSet = QrEntiCart
    Left = 376
    Top = 8
  end
  object frxContas1: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.680326597200000000
    ReportOptions.LastChange = 39720.680326597200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 68
    Top = 160
    Datasets = <
      item
        DataSet = frxDsContas1
        DataSetName = 'frxDsContas1'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 28.000000000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Memo32: TfrxMemoView
          Left = 820.000000000000000000
          Top = 17.102350000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 27.559060000000000000
        Top = 340.157700000000000000
        Width = 1046.929810000000000000
        object Memo31: TfrxMemoView
          Left = 724.881880000000000000
          Top = 5.102039999999988000
          Width = 244.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Top = 3.653369999999996000
          Width = 972.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
      end
      object MasterData1: TfrxMasterData
        Height = 37.795275590000000000
        Top = 241.889920000000000000
        Width = 1046.929810000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsContas1
        DataSetName = 'frxDsContas1'
        RowCount = 0
        object Memo15: TfrxMemoView
          Width = 140.000000000000000000
          Height = 37.795275590551200000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsContas1."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Height = 40.000000000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo1: TfrxMemoView
          Left = 139.842610000000000000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 203.842610000000000000
          Width = 64.000000000000000000
          Height = 18.897637795275600000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 267.842610000000000000
          Width = 64.000000000000000000
          Height = 18.897637795275600000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 331.842610000000000000
          Width = 64.000000000000000000
          Height = 18.897637795275600000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 395.842610000000000000
          Width = 64.000000000000000000
          Height = 18.897637795275600000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 459.842610000000000000
          Width = 64.000000000000000000
          Height = 18.897637795275600000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 523.842610000000000000
          Width = 64.000000000000000000
          Height = 18.897637795275600000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 587.842610000000000000
          Width = 64.000000000000000000
          Height = 18.897637795275600000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 651.842610000000000000
          Width = 64.000000000000000000
          Height = 18.897637795275600000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 715.842610000000000000
          Width = 64.000000000000000000
          Height = 18.897637795275600000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 779.842610000000000000
          Width = 64.000000000000000000
          Height = 18.897637795275600000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 843.842610000000000000
          Width = 64.000000000000000000
          Height = 18.897637795275600000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 907.842610000000000000
          Width = 64.000000000000000000
          Height = 18.897637795275600000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 139.842610000000000000
          Top = 18.897650000000030000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 203.842610000000000000
          Top = 18.897650000000030000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 267.842610000000000000
          Top = 18.897650000000030000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 331.842610000000000000
          Top = 18.897650000000030000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 395.842610000000000000
          Top = 18.897650000000030000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 459.842610000000000000
          Top = 18.897650000000030000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 523.842610000000000000
          Top = 18.897650000000030000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 587.842610000000000000
          Top = 18.897650000000030000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 651.842610000000000000
          Top = 18.897650000000030000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 715.842610000000000000
          Top = 18.897650000000030000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 779.842610000000000000
          Top = 18.897650000000030000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 843.842610000000000000
          Top = 18.897650000000030000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 907.842610000000000000
          Top = 18.897650000000030000
          Width = 64.000000000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 70.000000000000000000
        Top = 68.031540000000010000
        Width = 1046.929810000000000000
        object Picture2: TfrxPictureView
          Left = 3.779530000000000000
          Top = 5.070809999999994000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 179.779530000000000000
          Top = 45.070809999999990000
          Width = 500.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 179.779530000000000000
          Top = 5.070809999999994000
          Width = 408.000000000000000000
          Height = 30.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Lista de contas')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 18.000000000000000000
        Top = 162.519790000000000000
        Width = 1046.929810000000000000
        object Memo28: TfrxMemoView
          Width = 140.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 140.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Janeiro')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 204.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Fevereiro')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 268.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Mar'#231'o')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 332.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Abril')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 396.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Maio')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 460.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Junho')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 524.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Julho')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 588.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Agosto')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 652.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Setembro')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 716.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Outubro')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 780.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Novembro')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 844.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Dezembro')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 908.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '13'#186)
          ParentFont = False
        end
      end
    end
  end
  object frxDsContas1: TfrxDBDataset
    UserName = 'frxDsContas1'
    CloseDataSource = False
    DataSet = QrContas1
    BCDToCurrency = False
    Left = 40
    Top = 160
  end
  object frxContas2: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.695500578700000000
    ReportOptions.LastChange = 39720.695500578700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxContas2GetValue
    Left = 296
    Top = 144
    Datasets = <
      item
        DataSet = frxDsContas2
        DataSetName = 'frxDsContas2'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 71.779530000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 508.000000000000000000
          Top = 9.763760000000001000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 4.000000000000000000
          Top = 5.763760000000001000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo46: TfrxMemoView
          Left = 252.000000000000000000
          Top = 45.763760000000010000
          Width = 420.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[PERIODO]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 180.000000000000000000
          Top = 45.763760000000010000
          Width = 68.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 20.000000000000000000
        Top = 419.527830000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 416.440940000000000000
          Top = 1.322510000000023000
          Width = 244.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 82.000000000000000000
        Top = 113.385900000000000000
        Width = 718.110700000000000000
        object Memo26: TfrxMemoView
          Left = 4.000000000000000000
          Top = 47.275509999999980000
          Width = 492.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 4.000000000000000000
          Top = 7.275509999999997000
          Width = 408.000000000000000000
          Height = 30.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Mensalidades de Contas')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 18.000000000000000000
        Top = 219.212740000000000000
        Width = 718.110700000000000000
        object Memo28: TfrxMemoView
          Left = 88.000000000000000000
          Width = 204.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 292.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Esperado')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 372.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'M'#237'nimo')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 44.000000000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'd.')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 452.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Realizado')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 532.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 592.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'M'#233'dia')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.000000000000000000
        Top = 298.582870000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsContas2
        DataSetName = 'frxDsContas2'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 88.000000000000000000
          Width = 204.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsContas2."Nome"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 292.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsContas2."ESPERADO"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 372.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsContas2."MINIMO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <LINE#>)]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 44.000000000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsContas2."Codigo">)]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 452.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsContas2."STOTAL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 532.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0;-#,###,##0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsContas2."QTDE"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 592.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsContas2."MEDIA"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 18.000000000000000000
        Top = 377.953000000000000000
        Width = 718.110700000000000000
        object Memo5: TfrxMemoView
          Left = 88.000000000000000000
          Width = 204.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 292.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsContas2."ESPERADO">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 372.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsContas2."MINIMO">)]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 452.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsContas2."STOTAL">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 532.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0;-#,###,##0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsContas2."QTDE">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 592.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsContas2."MEDIA">)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsContas2: TfrxDBDataset
    UserName = 'frxDsContas2'
    CloseDataSource = False
    DataSet = QrContas2
    BCDToCurrency = False
    Left = 268
    Top = 144
  end
  object frxListaContas: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.697092187500000000
    ReportOptions.LastChange = 39720.697092187500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 280
    Top = 200
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 28.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 558.000000000000000000
          Top = 17.102350000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 35.118120000000000000
        Top = 495.118430000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 470.661410000000000000
          Top = 1.322510000000023000
          Width = 244.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 22.000000000000000000
        Top = 291.023810000000000000
        Width = 718.110700000000000000
        Condition = '[frxDsListaconta."CODIGOCONJUNTO"]'
        object Memo1: TfrxMemoView
          Left = 128.503937010000000000
          Width = 585.763760000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsListaconta."NOMECONJUNTO"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 72.677180000000000000
          Width = 56.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsListaconta."CODIGOCONJUNTO"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 14.000000000000000000
        Top = 419.527830000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSetName = 'frDsListaconta'
        RowCount = 0
        object Memo14: TfrxMemoView
          Left = 233.779530000000000000
          Width = 56.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsListaconta."CODIGOCONTA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 291.023622050000000000
          Width = 423.307086614173000000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsListaconta."NOMECONTA"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 70.000000000000000000
        Top = 68.031540000000010000
        Width = 718.110700000000000000
        object Picture2: TfrxPictureView
          Left = 5.338590000000000000
          Top = 1.070809999999995000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 181.338590000000000000
          Top = 41.070809999999990000
          Width = 500.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 181.338590000000000000
          Top = 1.070809999999995000
          Width = 408.000000000000000000
          Height = 30.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Lista de contas')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 22.000000000000000000
        Top = 162.519790000000000000
        Width = 718.110700000000000000
        object Memo28: TfrxMemoView
          Left = 128.503937007874000000
          Top = 1.007730000000009000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Conjunto')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 188.976377952756000000
          Top = 1.007730000000009000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 245.669291338583000000
          Top = 1.007730000000009000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Sub-grupo')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 291.023622047244000000
          Top = 1.007730000000009000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 68.031496062992100000
          Top = 1.007730000000009000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Plano')
          ParentFont = False
        end
      end
      object Band1: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 336.378170000000000000
        Width = 718.110700000000000000
        Condition = '[frxDsListaconta."CODIGOGRUPO"]'
        object Memo2: TfrxMemoView
          Left = 188.976377950000000000
          Width = 525.354328270000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsListaconta."NOMEGRUPO"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 128.897650000000000000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsListaconta."CODIGOGRUPO"]')
          ParentFont = False
        end
      end
      object Band2: TfrxGroupHeader
        Height = 20.000000000000000000
        Top = 377.953000000000000000
        Width = 718.110700000000000000
        Condition = '[frxDsListaconta."CODIGOSUBGRUPO"]'
        object Memo5: TfrxMemoView
          Left = 245.669291340000000000
          Width = 468.661475910000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsListaconta."NOMESUBGRUPO"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 188.897650000000000000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsListaconta."CODIGOSUBGRUPO"]')
          ParentFont = False
        end
      end
      object Band3: TfrxGroupHeader
        Height = 22.094310000000000000
        Top = 245.669450000000000000
        Width = 718.110700000000000000
        Condition = '[frxDsListaconta."CODIGOPLANO"]'
        object Memo9: TfrxMemoView
          Left = 9.118120000000000000
          Top = 0.094310000000007220
          Width = 59.779530000000000000
          Height = 22.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsListaconta."CODIGOPLANO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 68.031496060000000000
          Top = 0.094310000000007220
          Width = 646.299239450000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsListaconta."NOMEPLANO"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsListaConta: TfrxDBDataset
    UserName = 'frxDsListaConta'
    CloseDataSource = False
    DataSet = QrListaconta
    BCDToCurrency = False
    Left = 308
    Top = 200
  end
end
