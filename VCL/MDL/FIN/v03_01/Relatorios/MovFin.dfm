object FmMovFin: TFmMovFin
  Left = 339
  Top = 185
  Caption = 'FIN-RELAT-013 :: Movimento do Plano de Contas'
  ClientHeight = 554
  ClientWidth = 772
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 756
    Top = 47
    Height = 383
    Align = alRight
    Constraints.MaxWidth = 4
    Constraints.MinWidth = 3
    ExplicitLeft = 757
    ExplicitTop = 53
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 772
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 713
      Top = 0
      Width = 59
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 654
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 443
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Movimento do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 443
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Movimento do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 443
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Movimento do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 485
    Width = 772
    Height = 69
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 593
      Top = 15
      Width = 177
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 591
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 756
    Height = 383
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 769
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 756
      Height = 383
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 769
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 756
        Height = 383
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 769
        object PainelC: TPanel
          Left = 2
          Top = 51
          Width = 752
          Height = 100
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitWidth = 765
          object PnDatas2: TPanel
            Left = 0
            Top = 0
            Width = 752
            Height = 100
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 765
            object GBVencto: TGroupBox
              Left = 207
              Top = 5
              Width = 192
              Height = 89
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '                                           '
              Enabled = False
              TabOrder = 0
              object LaVenctI: TLabel
                Left = 5
                Top = 30
                Width = 30
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'In'#237'cio:'
              end
              object LaVenctF: TLabel
                Left = 5
                Top = 59
                Width = 19
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Fim:'
              end
              object TPVctoIni: TdmkEditDateTimePicker
                Left = 49
                Top = 25
                Width = 138
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 44582.000000000000000000
                Time = 0.516375590297684500
                TabOrder = 1
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPVctoFim: TdmkEditDateTimePicker
                Left = 49
                Top = 54
                Width = 138
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 44582.000000000000000000
                Time = 0.516439455997897300
                TabOrder = 2
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object CkVencto: TCheckBox
                Left = 17
                Top = 0
                Width = 149
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Data do vencimento: '
                TabOrder = 0
              end
            end
            object GroupBox2: TGroupBox
              Left = 404
              Top = 5
              Width = 192
              Height = 89
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '                                          '
              TabOrder = 1
              object Label2: TLabel
                Left = 5
                Top = 30
                Width = 30
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'In'#237'cio:'
              end
              object Label3: TLabel
                Left = 5
                Top = 59
                Width = 19
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Fim:'
              end
              object TPDocIni: TdmkEditDateTimePicker
                Left = 49
                Top = 25
                Width = 138
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 44582.000000000000000000
                Time = 0.516375590297684500
                TabOrder = 1
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPDocFim: TdmkEditDateTimePicker
                Left = 49
                Top = 54
                Width = 138
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 44582.000000000000000000
                Time = 0.516439455997897300
                TabOrder = 2
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object CkDataDoc: TCheckBox
                Left = 20
                Top = 0
                Width = 144
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Data do documento: '
                TabOrder = 0
              end
            end
            object GroupBox3: TGroupBox
              Left = 601
              Top = 5
              Width = 192
              Height = 89
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '                               '
              TabOrder = 2
              object Label5: TLabel
                Left = 5
                Top = 30
                Width = 30
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'In'#237'cio:'
              end
              object Label6: TLabel
                Left = 5
                Top = 59
                Width = 19
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Fim:'
              end
              object TPCompIni: TdmkEditDateTimePicker
                Left = 49
                Top = 25
                Width = 138
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 44582.000000000000000000
                Time = 0.516375590297684500
                TabOrder = 1
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPCompFim: TdmkEditDateTimePicker
                Left = 49
                Top = 54
                Width = 138
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 44582.000000000000000000
                Time = 0.516439455997897300
                TabOrder = 2
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object CkDataComp: TCheckBox
                Left = 20
                Top = 0
                Width = 109
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Compensado: '
                TabOrder = 0
              end
            end
            object GroupBox4: TGroupBox
              Left = 10
              Top = 5
              Width = 192
              Height = 89
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '                                     '
              TabOrder = 3
              object Label101: TLabel
                Left = 5
                Top = 30
                Width = 30
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'In'#237'cio:'
              end
              object Label102: TLabel
                Left = 5
                Top = 59
                Width = 19
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Fim:'
              end
              object TPEmissIni: TdmkEditDateTimePicker
                Left = 49
                Top = 25
                Width = 138
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 44582.000000000000000000
                Time = 0.516375590297684500
                TabOrder = 1
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPEmissFim: TdmkEditDateTimePicker
                Left = 49
                Top = 54
                Width = 138
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 44582.000000000000000000
                Time = 0.516439455997897300
                TabOrder = 2
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object CkEmissao: TCheckBox
                Left = 20
                Top = 0
                Width = 129
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Data da emiss'#227'o: '
                TabOrder = 0
              end
            end
            object RGSit: TRadioGroup
              Left = 798
              Top = 5
              Width = 118
              Height = 89
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Situa'#231#227'o: '
              ItemIndex = 0
              Items.Strings = (
                'Quitados'
                'Abertos'
                'Ambos')
              TabOrder = 4
            end
          end
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 752
          Height = 36
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          ExplicitWidth = 765
          object LaEmpresa: TLabel
            Left = 10
            Top = 10
            Width = 44
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Empresa:'
          end
          object EdEmpresa: TdmkEditCB
            Left = 74
            Top = 5
            Width = 64
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 142
            Top = 5
            Width = 775
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 151
          Width = 752
          Height = 54
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          ExplicitWidth = 765
          object CGAgrup: TdmkCheckGroup
            Left = 0
            Top = 0
            Width = 694
            Height = 54
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Caption = ' Agrupamentos: '
            Columns = 5
            ItemIndex = 4
            Items.Strings = (
              'N'#237'vel 5 (Plano)'
              'N'#237'vel 4 (Conjunto)'
              'N'#237'vel 3 (Grupo)'
              'N'#237'vel 2 (Subgrupo)'
              'N'#237'vel 1 (Conta)')
            TabOrder = 0
            UpdType = utYes
            Value = 16
            OldValor = 0
          end
          object RGDemonstrativo: TRadioGroup
            Left = 694
            Top = 0
            Width = 222
            Height = 54
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Caption = ' Demonstrativo '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Sint'#233'tico'
              'Anal'#237'tico')
            TabOrder = 1
          end
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 205
          Width = 752
          Height = 176
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet3
          Align = alClient
          TabOrder = 3
          ExplicitWidth = 765
          object TabSheet3: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Texto SQL '
            ImageIndex = 2
            object Memo1: TMemo
              Left = 0
              Top = 0
              Width = 744
              Height = 148
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              Visible = False
              ExplicitWidth = 757
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 430
    Width = 772
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 768
      Height = 38
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 17
        Width = 768
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 0
        Visible = False
      end
    end
  end
  object DockTabSet1: TTabSet
    Left = 759
    Top = 47
    Width = 13
    Height = 383
    Align = alRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    ExplicitLeft = 756
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 10
    OnTimer = Timer1Timer
  end
  object QrEmpresas: TMySQLQuery
   
    SQL.Strings = (
      'SELECT * FROM _empresas_')
    Left = 20
    Top = 12
    object QrEmpresasCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object QrEmpresasCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object QrEmpresasCodFilial: TIntegerField
      FieldName = 'CodFilial'
    end
    object QrEmpresasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrLcts: TMySQLQuery
   
    OnCalcFields = QrLctsCalcFields
    SQL.Strings = (
      'SELECT * FROM _FIN_RELAT_013_1')
    Left = 220
    Top = 328
    object QrLctsCodCliInt: TLargeintField
      FieldName = 'CodCliInt'
    end
    object QrLctsNO_CodCliInt: TWideStringField
      FieldName = 'NO_CodCliInt'
      Size = 33
    end
    object QrLctsData: TDateField
      FieldName = 'Data'
    end
    object QrLctsVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLctsCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLctsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctsDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLctsCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctsDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLctsNO_CLI_FOR: TWideStringField
      FieldName = 'NO_CLI_FOR'
      Size = 100
    end
    object QrLctscN1: TIntegerField
      FieldName = 'cN1'
    end
    object QrLctsoN1: TIntegerField
      FieldName = 'oN1'
    end
    object QrLctsnN1: TWideStringField
      FieldName = 'nN1'
      Size = 50
    end
    object QrLctscN2: TIntegerField
      FieldName = 'cN2'
    end
    object QrLctsoN2: TIntegerField
      FieldName = 'oN2'
    end
    object QrLctsnN2: TWideStringField
      FieldName = 'nN2'
      Size = 50
    end
    object QrLctscN3: TIntegerField
      FieldName = 'cN3'
    end
    object QrLctsoN3: TIntegerField
      FieldName = 'oN3'
    end
    object QrLctsnN3: TWideStringField
      FieldName = 'nN3'
      Size = 50
    end
    object QrLctscN4: TIntegerField
      FieldName = 'cN4'
    end
    object QrLctsoN4: TIntegerField
      FieldName = 'oN4'
    end
    object QrLctsnN4: TWideStringField
      FieldName = 'nN4'
      Size = 50
    end
    object QrLctscN5: TIntegerField
      FieldName = 'cN5'
    end
    object QrLctsoN5: TIntegerField
      FieldName = 'oN5'
    end
    object QrLctsnN5: TWideStringField
      FieldName = 'nN5'
      Size = 50
    end
    object QrLctsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctsCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
  end
  object frxFIN_RELAT_013_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.491125578700000000
    ReportOptions.LastChange = 39720.491125578700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  case <VARF_MIN_GRUP> of'
      
        '    1: GH_001.Child := Child1;                                  ' +
        '                           '
      
        '    2: GH_002.Child := Child1;                                  ' +
        '                           '
      
        '    3: GH_003.Child := Child1;                                  ' +
        '                           '
      
        '    4: GH_004.Child := Child1;                                  ' +
        '                           '
      
        '    5: GH_005.Child := Child1;                                  ' +
        '                           '
      '  end;'
      '  //    '
      '  if not <VARF_VISIVEL_1> then'
      '  begin'
      
        '    GH_001.Visible := False;                                    ' +
        '                  '
      
        '    GF_001.Visible := False;                                    ' +
        '                  '
      '  end;            '
      '  //    '
      '  if not <VARF_VISIVEL_2> then'
      '  begin'
      
        '    GH_002.Visible := False;                                    ' +
        '                  '
      
        '    GF_002.Visible := False;                                    ' +
        '                  '
      '  end;            '
      '  //    '
      '  if not <VARF_VISIVEL_3> then'
      '  begin'
      
        '    GH_003.Visible := False;                                    ' +
        '                  '
      
        '    GF_003.Visible := False;                                    ' +
        '                  '
      '  end;            '
      '  //    '
      '  if not <VARF_VISIVEL_4> then'
      '  begin'
      
        '    GH_004.Visible := False;                                    ' +
        '                  '
      
        '    GF_004.Visible := False;                                    ' +
        '                  '
      '  end;            '
      '  //    '
      '  if not <VARF_VISIVEL_5> then'
      '  begin'
      
        '    GH_005.Visible := False;                                    ' +
        '                  '
      
        '    GF_005.Visible := False;                                    ' +
        '                  '
      '  end;            '
      '  //          '
      '{'
      '  if <VARF_SINTETICO> then'
      '  begin              '
      '    MD_001.Height := 0;'
      '    //          '
      '    Me_001.Height := 0;                      '
      '    Me_002.Height := 0;                      '
      '    Me_003.Height := 0;                      '
      '    Me_004.Height := 0;                      '
      '    Me_005.Height := 0;                      '
      '    Me_006.Height := 0;                      '
      '    Me_007.Height := 0;                      '
      '    Me_008.Height := 0;                      '
      '  end;              '
      '}'
      'end.')
    OnGetValue = frxFIN_RELAT_013_01GetValue
    Left = 276
    Top = 328
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsLcts
        DataSetName = 'frxDsLcts'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MD_001: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338582680000000000
        Top = 393.071120000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLcts
        DataSetName = 'frxDsLcts'
        RowCount = 0
        object Me_002: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795285350000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          DataField = 'Data'
          DataSet = frxDsLcts
          DataSetName = 'frxDsLcts'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLcts."Data"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_001: TfrxMemoView
          AllowVectorExport = True
          Left = -0.000034170000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          DataField = 'Controle'
          DataSet = frxDsLcts
          DataSetName = 'frxDsLcts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLcts."Controle"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_007: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929133860000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsLcts
          DataSetName = 'frxDsLcts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsLcts.' +
              '"Debito">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_008: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622047240000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          DataSet = frxDsLcts
          DataSetName = 'frxDsLcts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsLcts.' +
              '"Credito">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_006: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858214020000000000
          Width = 393.070919840000000000
          Height = 11.338582680000000000
          DataField = 'Descricao'
          DataSet = frxDsLcts
          DataSetName = 'frxDsLcts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLcts."Descricao"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_005: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503873540000000000
          Width = 45.354340470000000000
          Height = 11.338582680000000000
          DataSet = frxDsLcts
          DataSetName = 'frxDsLcts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39',<frxDsLcts."Documento">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_003: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          DataField = 'Vencimento'
          DataSet = frxDsLcts
          DataSetName = 'frxDsLcts'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLcts."Vencimento"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_004: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          DataField = 'COMPENSADO_TXT'
          DataSet = frxDsLcts
          DataSetName = 'frxDsLcts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLcts."COMPENSADO_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 672.756340000000000000
        Width = 680.315400000000000000
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 566.503963860000000000
          Top = 3.810759999999959000
          Width = 56.692913390000000000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 623.196877240000000000
          Top = 3.810759999999959000
          Width = 56.692913390000000000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Credito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Top = 3.810759999999959000
          Width = 510.236220470000000000
          Height = 10.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total pesquisado ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 3.779530000000022000
          Width = 56.692913390000000000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Credito">-<frxDsLcts."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 71.811070000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'MOVIMENTO DE CONTAS - ANAL'#205'TICO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[PERIODO]')
          ParentFont = False
        end
      end
      object GH_001: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.976190000000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLcts."cN1"'
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472440944881890000
          Top = 0.976189999999974100
          Width = 619.842519685039400000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLcts."nN1"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Top = 0.976189999999974100
          Width = 60.472440944881890000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsLcts."cN1">)]')
          ParentFont = False
        end
      end
      object GF_001: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.590290000000000000
        Top = 427.086890000000000000
        Width = 680.315400000000000000
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929133860000000000
          Top = 3.810760000000016000
          Width = 56.692913390000000000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622047240000000000
          Top = 3.810760000000016000
          Width = 56.692913390000000000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Credito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Top = 3.810760000000016000
          Width = 510.236220472440900000
          Height = 10.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de [frxDsLcts."nN1"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236220472440900000
          Top = 3.779530000000022000
          Width = 56.692913385826770000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Credito">-<frxDsLcts."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 714.331170000000000000
        Width = 680.315400000000000000
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 13.228348900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH_002: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.976190000000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLcts."cN2"'
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472440944881890000
          Top = 0.976189999999974100
          Width = 619.842519685039400000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLcts."nN2"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 0.976189999999974100
          Width = 60.472440944881890000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsLcts."cN2">)]')
          ParentFont = False
        end
      end
      object GF_002: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.590290000000000000
        Top = 468.661720000000000000
        Width = 680.315400000000000000
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929133860000000000
          Top = 3.810760000000016000
          Width = 56.692913390000000000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622047240000000000
          Top = 3.810760000000016000
          Width = 56.692913390000000000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Credito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Top = 3.810760000000016000
          Width = 510.236220472440900000
          Height = 10.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de [frxDsLcts."nN2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236220472440900000
          Top = 3.779530000000022000
          Width = 56.692913385826770000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Credito">-<frxDsLcts."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_003: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.976190000000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLcts."cN3"'
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472440944881890000
          Top = 0.976190000000002600
          Width = 619.842519685039400000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLcts."nN3"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 0.976190000000002600
          Width = 60.472440944881890000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsLcts."cN3">)]')
          ParentFont = False
        end
      end
      object GH_004: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.976190000000000000
        Top = 192.756030000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLcts."cN4"'
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472440944881890000
          Top = 0.976190000000002600
          Width = 619.842519685039400000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLcts."nN4"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 0.976190000000002600
          Width = 60.472440944881890000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsLcts."cN4">)]')
          ParentFont = False
        end
      end
      object GH_005: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.976190000000000000
        Top = 151.181200000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLcts."cN5"'
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472440944881890000
          Top = 0.976190000000002600
          Width = 619.842519685039400000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLcts."nN5"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 0.976190000000002600
          Width = 60.472440944881890000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsLcts."cN5">)]')
          ParentFont = False
        end
      end
      object GF_003: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.590290000000000000
        Top = 510.236550000000000000
        Width = 680.315400000000000000
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929133860000000000
          Top = 3.810760000000016000
          Width = 56.692913390000000000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622047240000000000
          Top = 3.810760000000016000
          Width = 56.692913390000000000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Credito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 3.810760000000016000
          Width = 510.236220472440900000
          Height = 10.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de [frxDsLcts."nN3"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236220472440900000
          Top = 3.779529999999965000
          Width = 56.692913385826770000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Credito">-<frxDsLcts."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GF_004: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.590290000000000000
        Top = 551.811380000000000000
        Width = 680.315400000000000000
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929133860000000000
          Top = 3.810759999999959000
          Width = 56.692913390000000000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622047240000000000
          Top = 3.810759999999959000
          Width = 56.692913390000000000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Credito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Top = 3.810759999999959000
          Width = 510.236220472440900000
          Height = 10.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de [frxDsLcts."nN4"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236220472440900000
          Top = 3.779530000000022000
          Width = 56.692913385826770000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Credito">-<frxDsLcts."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GF_005: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.590290000000000000
        Top = 593.386210000000000000
        Width = 680.315400000000000000
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929133860000000000
          Top = 3.810759999999959000
          Width = 56.692913390000000000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622047240000000000
          Top = 3.810759999999959000
          Width = 56.692913390000000000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Credito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Top = 3.810759999999959000
          Width = 510.236220472440900000
          Height = 10.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de [frxDsLcts."nN5"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236220472440900000
          Top = 3.779530000000022000
          Width = 56.692913385826770000
          Height = 10.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsL' +
              'cts."Credito">-<frxDsLcts."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Child1: TfrxChild
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338582680000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795285350000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858214020000000000
          Width = 393.070919840000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929133860000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622047240000000000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = -0.000034170000000000
          Width = 37.795275590000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503873540000000000
          Width = 45.354340470000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencim.')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Width = 30.236220470000000000
          Height = 11.338582680000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Compens.')
          ParentFont = False
        end
      end
    end
  end
  object frxDsLcts: TfrxDBDataset
    UserName = 'frxDsLcts'
    CloseDataSource = False
    DataSet = QrLcts
    BCDToCurrency = False
    
    Left = 248
    Top = 328
  end
  object QrSums: TMySQLQuery
   
    SQL.Strings = (
      'SELECT '
      'cN5, oN5, nN5,'
      'cN4, oN4, nN4,'
      'cN3, oN3, nN3,'
      'cN2, oN2, nN2,'
      'cN1, oN1, nN1,'
      'SUM(Credito) Credito, SUM(Debito) Debito'
      'FROM _fin_relat_013_1'
      'GROUP BY cN5, cN4, cN3, cN2, cN1'
      'ORDER BY oN5, nN5, oN4, nN4, oN3, nN3, oN2, nN2, oN1, nN1')
    Left = 220
    Top = 300
    object QrSumscN5: TIntegerField
      FieldName = 'cN5'
    end
    object QrSumsoN5: TIntegerField
      FieldName = 'oN5'
    end
    object QrSumsnN5: TWideStringField
      FieldName = 'nN5'
      Size = 50
    end
    object QrSumscN4: TIntegerField
      FieldName = 'cN4'
    end
    object QrSumsoN4: TIntegerField
      FieldName = 'oN4'
    end
    object QrSumsnN4: TWideStringField
      FieldName = 'nN4'
      Size = 50
    end
    object QrSumscN3: TIntegerField
      FieldName = 'cN3'
    end
    object QrSumsoN3: TIntegerField
      FieldName = 'oN3'
    end
    object QrSumsnN3: TWideStringField
      FieldName = 'nN3'
      Size = 50
    end
    object QrSumscN2: TIntegerField
      FieldName = 'cN2'
    end
    object QrSumsoN2: TIntegerField
      FieldName = 'oN2'
    end
    object QrSumsnN2: TWideStringField
      FieldName = 'nN2'
      Size = 50
    end
    object QrSumscN1: TIntegerField
      FieldName = 'cN1'
    end
    object QrSumsoN1: TIntegerField
      FieldName = 'oN1'
    end
    object QrSumsnN1: TWideStringField
      FieldName = 'nN1'
      Size = 50
    end
    object QrSumsCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrSumsDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object frxDsSums: TfrxDBDataset
    UserName = 'frxDsSums'
    CloseDataSource = False
    DataSet = QrSums
    BCDToCurrency = False
    
    Left = 248
    Top = 300
  end
  object frxFIN_RELAT_013_00: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.491125578700000000
    ReportOptions.LastChange = 39720.491125578700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  case <VARF_MIN_GRUP> of'
      
        '    2: GH_002.Child := Child1;                                  ' +
        '                           '
      
        '    3: GH_003.Child := Child1;                                  ' +
        '                           '
      
        '    4: GH_004.Child := Child1;                                  ' +
        '                           '
      
        '    5: GH_005.Child := Child1;                                  ' +
        '                           '
      '  end;'
      '  //    '
      '  if not <VARF_VISIVEL_2> then'
      '  begin'
      
        '    GH_002.Visible := False;                                    ' +
        '                  '
      
        '    GF_002.Visible := False;                                    ' +
        '                  '
      '  end;            '
      '  //    '
      '  if not <VARF_VISIVEL_3> then'
      '  begin'
      
        '    GH_003.Visible := False;                                    ' +
        '                  '
      
        '    GF_003.Visible := False;                                    ' +
        '                  '
      '  end;            '
      '  //    '
      '  if not <VARF_VISIVEL_4> then'
      '  begin'
      
        '    GH_004.Visible := False;                                    ' +
        '                  '
      
        '    GF_004.Visible := False;                                    ' +
        '                  '
      '  end;            '
      '  //    '
      '  if not <VARF_VISIVEL_5> then'
      '  begin'
      
        '    GH_005.Visible := False;                                    ' +
        '                  '
      
        '    GF_005.Visible := False;                                    ' +
        '                  '
      '  end;            '
      '  //          '
      'end.')
    OnGetValue = frxFIN_RELAT_013_01GetValue
    Left = 276
    Top = 300
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsSums
        DataSetName = 'frxDsSums'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MD_001: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsSums
        DataSetName = 'frxDsSums'
        RowCount = 0
        object Me_001: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913395590000000000
          Height = 18.897637800000000000
          DataField = 'cN1'
          DataSet = frxDsSums
          DataSetName = 'frxDsSums'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSums."cN1"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_007: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543233860000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsSums
          DataSetName = 'frxDsSums'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsSums.' +
              '"Debito">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_008: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133797240000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsSums
          DataSetName = 'frxDsSums'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsSums.' +
              '"Credito">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_006: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913254020000000000
          Width = 400.629979840000000000
          Height = 18.897637800000000000
          DataField = 'nN1'
          DataSet = frxDsSums
          DataSetName = 'frxDsSums'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSums."nN1"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataSet = frxDsSums
          DataSetName = 'frxDsSums'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', <frxDsSums.' +
              '"Credito">-<frxDsSums."Debito">)]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.708397800000000000
        Top = 619.842920000000000000
        Width = 680.315400000000000000
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Top = 3.810759999999959000
          Width = 453.543270470000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total pesquisado ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.810759999999959000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134163380000000000
          Top = 3.810759999999959000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Credito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724836610000000000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Credito">-<frxDsSums."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 71.811070000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'MOVIMENTO DE CONTAS - SINT'#201'TICO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[PERIODO]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 665.197280000000000000
        Width = 680.315400000000000000
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 13.228348900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH_002: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 19.873827800000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSums."cN2"'
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472440940000000000
          Top = 0.976189999999974100
          Width = 619.842519690000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSums."nN2"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 0.976189999999974100
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsSums."cN2">)]')
          ParentFont = False
        end
      end
      object GF_002: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.708397800000000000
        Top = 400.630180000000000000
        Width = 680.315400000000000000
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543233860000000000
          Top = 3.810760000000016000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133797240000000000
          Top = 3.810760000000016000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Credito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Top = 3.810760000000016000
          Width = 453.543270470000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de [frxDsSums."nN2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724470470000000000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Credito">-<frxDsSums."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_003: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 19.873827800000000000
        Top = 234.330860000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSums."cN3"'
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472440940000000000
          Top = 0.976190000000002600
          Width = 619.842519690000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSums."nN3"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 0.976190000000002600
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsSums."cN3">)]')
          ParentFont = False
        end
      end
      object GH_004: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 19.873827800000000000
        Top = 192.756030000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSums."cN4"'
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472440940000000000
          Top = 0.976190000000002600
          Width = 619.842519690000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSums."nN4"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 0.976190000000002600
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsSums."cN4">)]')
          ParentFont = False
        end
      end
      object GH_005: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 19.873827800000000000
        Top = 151.181200000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSums."cN5"'
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472440940000000000
          Top = 0.976190000000002600
          Width = 619.842519690000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSums."nN5"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 0.976190000000002600
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsSums."cN5">)]')
          ParentFont = False
        end
      end
      object GF_003: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.708397800000000000
        Top = 445.984540000000000000
        Width = 680.315400000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 3.810760000000016000
          Width = 453.543307086614200000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de [frxDsSums."nN3"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.810760000000016000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134163380000000000
          Top = 3.810760000000016000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Credito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724836610000000000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Credito">-<frxDsSums."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GF_004: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.708397800000000000
        Top = 491.338900000000000000
        Width = 680.315400000000000000
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Top = 3.810760000000016000
          Width = 453.543307086614200000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de [frxDsSums."nN4"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.810760000000016000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134163380000000000
          Top = 3.810760000000016000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Credito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724836610000000000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Credito">-<frxDsSums."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GF_005: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677165360000000000
        Top = 536.693260000000000000
        Width = 680.315400000000000000
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779527559055168000
          Width = 453.543307086614200000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de [frxDsSums."nN5"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.779527559055168000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134163380000000000
          Top = 3.779527559055168000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Credito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724836610000000000
          Top = 3.779527559055168000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39', SUM(<frxDsS' +
              'ums."Credito">-<frxDsSums."Debito">))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Child1: TfrxChild
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913254020000000000
          Width = 400.629979840000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543233860000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 529.133797240000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913395590000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Resultado')
          ParentFont = False
        end
      end
    end
  end
end
