unit UnFinanceiro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, ComCtrls, dmkDBGrid, MyDBCheck, DBCtrls, DmkDAC_PF,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, dmkEditDateTimePicker, TypInfo,
  stdctrls, UnMyVCLref, dmkGeral, dmkLabel, dmkRadioGroup, dmkMemo, dmkdbEdit,
  dmkCheckBox, dmkValUsu, dmkPopOutFntCBox, DBGrids, dmkImage,
  Vcl.Buttons, Variants, UnDmkProcFunc, ExtCtrls, UnDmkEnums, UnGrl_Geral,
  mySQLDirectQuery;

type
  TTabLctToWork = (tlwA, tlwB, tlwD);
  TDataCompetencia = (dcData, dcVencimento, dcMesAnteriorAoVencto, dcDoisMesesAntes);
  TCampoData = (cdData, cdVencimento, cdCompensado);
  TGerencimantoDeRegistro = (tgrInclui, tgrAltera, tgrDuplica, tgrExclui, tgrTeste);
  TInsAltReopenLct = function(): Boolean;
  TLanctoFinalidade = (lfUnknow, lfProprio, lfCondominio, lfCicloCurso);

  TUnFinanceiro = class(TObject)
  private
    { Private declarations }
    function TextoFinalidadeLancto(Finalidade: TLanctoFinalidade): String;
    //function InsAltReopenLct(f : TInsAltReopenLct): Boolean;
    function  CriaLanctoEditor(InstanceClass: TComponentClass; var Reference;
              ModoAcesso: TAcessFmModo; QrSelect: TmySQLQuery;
              Controle, Sub: Integer; Finalidade: TLanctoFinalidade): Boolean;
  public
    { Public declarations }
// 'lan ctos'
    function  AlteraGenero(CtrlMae: Integer; TabLctA: String; Genero:
              Integer): Integer;
    procedure AcertarCreditoPeloValorPago(QrCrt, QrLct: TmySQLQuery;
              DBGLct: TDBGrid; TabLctA: String);
    procedure AtualizaEmissaoMasterExtra_Novo(ID_Pgto: Integer; FQuery:
              TmySQLQuery; EdDeb, EdCred, EdSaldoDeb, EdSaldoCred: TdmkEdit;
              DataCompQuit, TabLctA: String; RecalcCart: Boolean = True);
    procedure AtualizaEmissaoMasterRapida(ID_Pgto, _Sit, Controle: Integer;
              DataCompQuit, TabLctA: String);
{
    function  AtualizaSaldosDeContas(Entidade, Periodo: Integer;
              MostraVerif: Boolean): Boolean;
}
    procedure AtualizaTodasCarteiras(QrCrt, QrLct: TmySQLQuery; TabLctA: String;
              Carteira: Integer = 0);
    procedure AtzDataAtzSdoPlaCta(Entidade: Integer; Data: TDateTime);
    procedure ColocarMesDeCompetenciaOndeNaoTem(TipoData: TDataCompetencia;
              QrCrt, QrLct: TmySQLQuery; TabLctA: String);
    procedure ColocarUHOndeNaoTem(QrCrt, QrLct: TmySQLQuery;
              DBGLct: TDBGrid; TabLctA: String);
    procedure ConfiguraFmTransferCart();
    procedure ConfiguraFmTransferCtas;
    procedure CriarTransferCart(Tipo: Integer; QrLct, QrCrt: TmySQLQuery;
              MostraForm: Boolean; CliInt, FatID, FatID_Sub, FatNum: Integer;
              TabLctA: String);
    procedure CriarTransferCtas(Tipo: Integer; QrLct, QrCrt:
              TmySQLQuery; MostraForm: Boolean; CliInt, FatID, FatID_Sub,
              FatNum: Integer; TabLctA: String);
    function  ImpedePelaTabelaLct(Letra: String; Controle: Double): Boolean;
    function  AlteracaoLancamento(QrCrt, QrLct: TmySQLQuery;
              Finalidade: TLanctoFinalidade; OneAccount, OneCliInt: Integer;
              TabLctA: String; AlteraAtehFatID: Boolean): Boolean;
            //InsAltLancamento
    function  InclusaoLancamento(
              //InstanceClass: TComponentClass; var Reference;
              Finalidade: TLanctoFinalidade; ModoAcesso: TAcessFmModo;
              QrLct, QrCrt: TmySQLQuery; Acao: TGerencimantoDeRegistro;
              Controle, Sub, Genero: Integer; PercJuroM, PercMulta: Double;
              SetaVars: TInsAltReopenLct; FatID, FatID_Sub, FatNum, Carteira:
              Integer; Credito, Debito: Double; AlteraAtehFatID: Boolean;
              Cliente, Fornecedor, CliInt, ForneceI, Account, Vendedor: Integer;
              LockCliInt, LockForneceI, LockAccount, LockVendedor: Boolean;
              Data, Vencto, DataDoc: TDateTime; IDFinalidade: Integer;
              Mes: TDateTime; TabLctA: String;
              // 2012-07-17
              FisicoSrc, FisicoCod: Integer): Integer;
    {function  InclusaoLancamentoC(
              InstanceClass: TComponentClass; var Reference;
              Finalidade: TLanctoFinalidade; ModoAcesso: TAcessFmModo;
              QrLct, QrCrt: TmySQLQuery; Acao: TGerencimantoDeRegistro;
              Controle, Sub, Genero: Integer; PercJuroM, PercMulta: Double;
              SetaVars: TInsAltReopenLct; FatID, FatID_Sub, FatNum, Carteira:
              Integer; Credito, Debito: Double; AlteraAtehFatID: Boolean;
              Cliente, Fornecedor, CliInt, ForneceI, Account, Vendedor: Integer;
              LockCliInt, LockForneceI, LockAccount, LockVendedor: Boolean;
              Data, Vencto, DataDoc: TDateTime; IDFinalidade: Integer;
              Mes: TDateTime; TabLctA: String;
              // 2012-07-17
              FisicoSrc, FisicoCod: Integer): Integer;}
    procedure ExcluiItemCarteira(Controle: Int64;
              // Adicionado 2011-01-28
              Data: TDateTime; Carteira,
              // fim adicionado 2011-01-28
              Sub, Genero, Cartao, Sit, Tipo,
              Chamada, ID_Pgto: Integer; QrLct, QrCrt: TmySQLQuery;
              Pergunta: Boolean; CartCalcSdo, MotvDel: Integer; TabLctA: String;
              // Adicionado em 2014-04-03
              PermiteExclusaoDeLctFatID: Boolean;
              // fim 2014-04-03
              // Adicionado em 2011-05-19
              ExcluiMesmoSeNegativo: Boolean = False);
              // fim 2011-05-19
    procedure ExclusaoIncondicionalDeLancamento(QrCrt, QrLct: TmySQLQuery;
              TabLctA: String);
    //procedure ImprimePlanoContas(); 2017-09-23 => Movido para o FinanceiroJan.ImpressaoDoPlanoDeContas
    function  InsereLancamento(TabLctA: String): Boolean;
    procedure LocalizarLancamento(DTPickerI, DTPickerF: TDateTimePicker;
              TipoData: Integer; QrCrt, QrLct: TmySQLQuery; LocSohCliInt: Boolean;
              QuemChamou: Integer; TabLctA: String; ModuleLctX: TDataModule;
              Controle: Integer; ConfirmarLocalizar: Boolean);
    function  LocalizarlanctoCliInt(const Controle, Sub, CliInt: Integer;
              TPDataIni: TDateTimePicker; QrLct, QrCrt, QrCrtSum: TmySQLQuery;
              const AvisaQuandoNaoEncontra: Boolean; const TabLct, TabLctB,
              TabLctD: String; var CtrlToLoc: Integer): Boolean;
    procedure ConciliacaoBancaria(QrCrt, QrLct: TmySQLQuery;
              TPDataIni: TdmkEditDateTimePicker; NomeCliInt: String; Entidade,
              CliInt, CartConcilia: Integer; TabLctA: String;
              ModuleLctX: TDataModule);
    procedure MudaCarteiraLancamentosSelecionados(QrCrt, QrLct: TmySQLQuery;
              DBGLct: TDBGrid; TabLctA: String; EntCliInt: Integer);
    procedure MudaTextLancamentosSelecionados(QrCrt, QrLct: TmySQLQuery;
              DBGLct: TDBGrid; TabLctA: String);
    procedure MudaContaLancamentosSelecionados(QrCrt, QrLct: TmySQLQuery;
              DBGLct: TDBGrid; TabLctA: String);
    procedure MudaDataLancamentosSelecionados(CampoData: TCampoData;
              QrCrt, QrLct: TmySQLQuery; DBGLct: TDBGrid; TabLctA: String);
    procedure MudaDataVenctoLLctEFatID(QrCrt, QrLct: TmySQLQuery; DBGLct:
              TDBGrid; TabLctA: String);
    procedure MudaValorEmCaixa(QrLct, QrCrt: TmySQLQuery);
    // 2012-05-15
    //function  NaoDuplicarLancto(LaTipo: String; Cheque: Extended; PesqCH:
    function  NaoDuplicarLancto(SQLType: TSQLType; Cheque: Extended; PesqCH:
              Boolean; SerieCH: String; NF: Integer; PesqNF: Boolean;
              SerieNF: String; Credito, Debito: Double; Conta, Mes: Integer;
              PesqVal: Boolean; CliInt: Integer; EstahEmLoop: Boolean;
              // 2011-01-23
              Carteira, Cliente, Fornecedor: Integer;
              // fim 2011-01-23
              LaAviso1, LaAviso2: TLabel; TabLctA, TabLctB, TabLctD: String;
              PesqEntValData: Boolean = False; Data: TDate = 0): Integer;
    // FIM 2012-05-15
    function  ObtemSaldoCarteira(Carteira: Integer): Double;
    procedure PagarAVistaEmCaixa(QrCrt, QrLct: TmySQLQuery; DBGLct: TDBGrid;
              TabLctA: String; AlteraAtehFatID: Boolean = False);
    procedure PagarRolarEmissao(QrCrt, QrLct: TmySQLQuery; TabLctA: String;
              AlteraAtehFatID: Boolean; var CodLctLocaliz: Integer);
    procedure Quitacao_CompensaNaContaCorrente(QrCrt, QrLct: TmySQLQuery;
              TabLctA: String; AlteraAtehFatID: Boolean = False);
    procedure Quitacao_CriaFmQuitaDoc2(QrCrt, QrLct: TmySQLQuery;
              Data: TDateTime; Documento, Controle,
              CtrlIni: Double; Sub: Integer; TabLctA: String);
    procedure Quitacao_QuitarVariosItens(DBGLct: TDBGrid; QrCrt, QrLct: TmySQLQuery;
              TabLctA: String; AlteraAtehFatID: Boolean = False);
    procedure QuitacaoDeDocumentos(DBGLct: TDBGrid; QrCrt, QrLct: TmySQLQuery;
              TabLctA: String; AlteraAtehFatID: Boolean = False);
    procedure RecalcSaldoCarteira(Carteira: Integer;
              QrCrt, QrLct: TmySQLQuery; Reabre, Localiza: Boolean);
    procedure ReverterPagtoEmissao(QrLct, QrCrt: TmySQLQuery;
              Pergunta, LocLancto, LocCarteira: Boolean; TabLctA: String);
    function  SaldoAqui(Tipo: Integer; Atual: String;
              QrLct, QrCrt: TmySQLQuery): String;
    procedure SomaLinhas_2(DBGLct: TDBGrid; QrCrt, QrLct: TmySQLQuery;
              EdCred, EdDebi, EdSoma: TdmkEdit);
    procedure SomaLinhas_3(DBGLct: TDBGrid; QrCrt, QrLct: TmySQLQuery;
              EdCred, EdDebi, EdSoma: TdmkEdit);
    function  TabLctNaoDefinida(TabLct, FunctionOrigem: String): Boolean;
    function  TransferenciaCorrompidaCart(Lancto: Double; QrCrt, QrLct:
              TmySQLQuery; TabLctA: String): Boolean;
    function  TransferenciaCorrompidaCtas(Lancto: Double; QrCrt, QrLct:
              TmySQLQuery; TabLctA: String): Boolean;
    procedure TransformarLancamentoEmItemDeBloqueto(FatID: Integer;
              QrCrt, QrLct: TmySQLQuery; DBGLct: TDBGrid; TabLctA: String);
    function  SQLInsUpd_Lct(QrUpd: TmySQLQuery; Tipo: TSQLType;
              Auto_increment: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean; ComplUpd, TabLctA: String(*;
              DtEncer, Data: TDateTime*)): Boolean;
    procedure VerificaID_Pgto_x_Compensado(const TabLctA: String; const LaAviso1,
              LaAviso2: TLabel; const PB1: TProgressBar; const QrNC: TmySQLQuery;
              var Corrigidos, NaoCorrigidos: Integer);
    procedure QuitacaoAutomaticaDmk(dmkDBGLct: TdmkDBGrid; QrLct,
              QrCrt: TmySQLQuery; TabLctA: String; AlteraAtehFatID: Boolean = False);
    procedure QuitacaoAutomaticaDmk2(dmkDBGLct: TdmkDBGrid; QrLct,
              QrCrt: TmySQLQuery; TabLctA: String; AlteraAtehFatID: Boolean = False);
    procedure QuitaItemLanctoAutomatico(Data: TDateTime; QrLct, QrCrt: TmySQLQuery;
              TabLctA: String; OutraCartCompensa: Integer = 0;
              AlteraAtehFatID: Boolean = False);
{###
    //  Forms
    procedure CriaFmQuitaDocs(Data: TDateTime; Documento, Controle,
              CtrlIni: Double; Sub, Carteira: Integer);
    procedure ConsertaLctQuitados();
    procedure LocalizaPagamentosDeEmissao(Controle: Integer;
              TPDataIni, TPDataFim: TDateTimePicker; Query: TmySQLQuery);
    procedure ExcluiLanctoGrade(DBGLct: TDBGrid;
              QrLct, QrCrt: TmySQLQuery; ExcluiAtehFatID: Boolean);
}
    {
    procedure CriarTransfer(Tipo: Integer; QrLct, QrCrt: TmySQLQuery;
              MostraForm: Boolean);
    }         // CriarTransfer2
    //  Fim forms?

    //procedure ConfiguraFmTransfer;
    procedure LancamentoDefaultVARS;
    procedure LancamentoDuplica(QrAux: TmySQLQuery; Controle, Sub: Integer;
              TabLctX: String);
    {
    procedure AtualizaEmissaoMasterExtra_Antigo(ID_Pgto: Integer; FQuery: TmySQLQuery;
              EdDeb, EdCred, EdSaldoDeb, EdSaldoCred: TdmkEdit);
    }
    procedure AtualizaVencimentos(TabLctA: String);
    {
    function CarregaSQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex: array of String;
             ValCampos, ValIndex: array of Variant;
             UserDataAlterweb, IGNORE: Boolean): Boolean;
    function SQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex: array of String;
             ValCampos, ValIndex: array of Variant;
             UserDataAlterweb: Boolean): Boolean;
    function SQLInsUpd_IGNORE(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex: array of String;
             ValCampos, ValIndex: array of Variant;
             UserDataAlterweb: Boolean): Boolean;
    function SQLInsUpd_UpdExtra(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex, SQLSoUpd: array of String;
             ValCampos, ValIndex, ValSoUpd: array of Variant;
             UserDataAlterWeb: Boolean): Boolean;
    procedure CriaFormInsUpd(InstanceClass: TComponentClass;
              var Reference; ModoAcesso: TAcessFormModo; Query: TmySQLQuery;
              Acao: TSQLType);
    function ExecSQLInsUpdFm(Form: TForm; Acao: TSQLType; Tabela:
             String; NewItem: Variant; QrExec: TmySQLQuery): Boolean;
    function BuscaEmLivreY_Def(Table, Field: String; Acao: TSQLType; Atual: Integer): Integer;
    }
    function SQLLoc1(Query: TmySQLQuery; Tabela, Campo: String;
             Valor: Variant; MsgLoc, MsgNaoLoc: String): Integer;
    function AtualizaPagamentosAVista(QrLctos: TmySQLQuery; TabLctA: String): Boolean;
    function ProximoRegistro(Query: TmySQLQuery; Campo: String; Atual:
             Integer): Integer;
    {
    N�o usar. usar do UMySQLModule!
    function SQLDel1(QrExec, QrData: TmySQLQuery; Tabela, Campo: String;
             Valor: Variant; PerguntaSeExclui: Boolean;
             PerguntaAlternativa: String): Boolean;
    }
    function CST_A_Get(Codigo: Integer): String;
    function CST_B_Get(Codigo: Integer): String;
    function CST_A_Lista(): MyArrayLista;
    function CST_B_Lista(): MyArrayLista;
    function ListaDeSituacaoTributaria(): String;
    function ListaDeTributacaoPeloICMS(): String;
    //
    function CST_IPI_Get(CodTxt: String): String;
    function CST_IPI_Lista(): MyArrayLista;
    function ListaDeCST_IPI(): String;
    //
    function CST_PIS_Get(CodTxt: String): String;
    function CST_PIS_Lista(): MyArrayLista;
    function ListaDeCST_PIS(): String;
    //
    function CST_COFINS_Get(CodTxt: String): String;
    function CST_COFINS_Lista(): MyArrayLista;
    function ListaDeCST_COFINS(): String;
    //
    function CSOSN_Get(CRT, Codigo: Integer): String;
    function CSOSN_Lista(): MyArrayLista;
    function ListaDeCSOSN(): String;
    //
    function CRT_Get(Codigo: Integer): String;
    function CRT_Lista(): MyArrayLista;
    function ListaDeCRT(): String;
    //
    function IndProc_Get(Codigo: Integer): String;
    function IndProc_Lista(): MyArrayLista;
    function ListaDeIndProc(): String;
    //
    function SPED_Tipo_Item_Get(Codigo: Integer): String;
    function SPED_Tipo_Item_Lista(): MyArrayLista;
    function ListaDeSPED_Tipo_Item(): String;
    //
    function SPED_EFD_IND_PERFIL_Get(Codigo: String): String;
    function SPED_EFD_IND_PERFIL_Lista(): MyArrayLista;
    function ListaDeSPED_EFD_IND_PERFIL(): String;

    //
    function SPED_EFD_IND_ATIV_Get(Codigo: Integer): String;
    function SPED_EFD_IND_ATIV_Lista(): MyArrayLista;
    function ListaDeSPED_EFD_IND_ATIV(): String;

    //
     //procedure AtualizaEndossos(Controle, Sub, OriCtrl, OriSub: Integer);
    //
    procedure ReabreCarteirasCliInt(CliInt, LocCart: Integer;
              QrCrt, QrCrtSum: TmySQLQuery);
    function  Mensal2(Mes2: Largeint; Ano: Double): String;
    function  NomeRelacionado(Cliente, Fornecedor: Integer; NomeCliente,
              NomeFornecedor: String): String;
    function  NomeSitLancto(LctSit, LanctoTipo, CarteiraPrazo:
              Integer; LanctoVencto: TDateTime; Reparcelamento: Integer;
              ConverteCompensadoEmQuitado: Boolean = False): String;
    function  DefLctFldSdoIni(DtIni, DtEncer, DtMorto: TDateTime): String;
    function  DefLctTab(DtIni, DtEncer, DtMorto: TDateTime;
              TabLctA, TabLctB, TabLctD: String): String;

    procedure ExcluiLct_Unico(TabLctA: String; DB: TmySQLDataBase;
              Data: TDateTime; Tipo, Carteira: Integer; Controle: Double; Sub,
              Motivo: Integer; Pergunta: Boolean; EnviaAoMorto: Boolean = True);
    (*
    2017-08-07 => Substituida pela fun��o: TUModule.ExcluiRegistro_EnviaArquivoMorto
    function  ExcluiLct_EnviaArquivoMorto(TabLctMorto, TabLct: String; Motivo,
              Tipo, Carteira, Sub: Integer; Controle: Double;
              Query: TmySQLQuery; DataBase: TmySQLDatabase): Boolean;
    *)
    function  ExcluiLct_FatParcela(QrDados: TmySQLQuery;
              FatID: Integer; FatNum: Double; FatParcela, Carteira, Sit, Tipo,
              MotvDel: Integer; TabLctA: String; PermiteFatNum_Zero: Boolean = False;
              PermiteFatParcela_Zero: Boolean = False): Boolean;
    function  ExcluiLct_FatNum(QrDados: TmySQLQuery; FatID: Integer;
              FatNum: Double; Entidade, Carteira, MotvDel: Integer;
              Pergunta: Boolean; TabLctA: String): Boolean;

    function  StatusContasConf_Codigo(Qtde, QtdeMin, QtdeMax: Integer;
              Debito, ValorMin, ValorMax: Double; Periodo, PeriodoIni,
              PeriodoFim: Integer): Integer;
    function  StatusContasConf_Texto(Status, QtdeMin, QtdeExe: Integer): String;
    function  Pesquisa4_Cartas(const RGModoItemIndex: Integer;
              const Data, Vencto: String;
              const EntiCond, Entidade, Apto, Propriet, Mez: Integer;
              const FatNum: Double;
              const TabLctA: String;
              var QrInadFiltered: Boolean): String;
    procedure CalculaValMultaEJuros(ValOrig: Double; DataNew, DataVencto: TDateTime;
              var MultaPerc, JuroPerc, ValorNew, MultaVal, JuroVal: Double;
              const PermitePercZero: Boolean);
    procedure CalculaPercMultaEJuros(ValOrig, MultaVal, JuroVal: Double;
              DataNew, DataVencto: TDateTime; var ValorNew, MultaPerc, JuroPerc: Double);
    function  FatIDProibidoEmLct(FatID: Integer; var Msg: String): Boolean;
    function  ObtemSequenciaDeCodigo_MotivExclusaoLct(Campo, NO_Campo: String): String;
    function  ObtemListaNomeFatID(AppId: Integer): MyArrayLista;
    function  VerificaSeLanctoEParcelamento(QueryLct: TmySQLQuery; var Msg: String): Boolean;
    function  CorrigeMez(TabLctA: String; Avisa: Boolean): Integer;
    function  TabLctNaoDef(TabLct: String; Avisa: Boolean = True): Boolean;
    //
    function  NomeDoNivelSelecionado(Tipo, Nivel: Integer): String;
    function  MontaSPED_COD_CTA(Nivel, Genero: Integer): String;
    function  DesmontaSPED_COD_CTA(const COD_CTA: String; var Nivel, Genero:
              Integer): Boolean;
    //Pagtos
    procedure ReopenCarteirasPgto(Query: TmySQLQuery);
    procedure ConfiguraGBCarteiraPgto(GBCarteira: TGroupBox;
              EditCarteira: TdmkEditCB; ComboBoxCarteira: TdmkDBLookupComboBox;
              Carteira: Integer);

    //Quita��es
    procedure DesfazerCompensacao(GridLct: TDBGrid; QueryCrt, QueryLct:
              TmySQLQuery; TabLctA: String);
    function  PagamentoEmBanco_DefineGenerosECarteira(const Controle, Sub:
              Integer; const Credito, Debito: Double; const OutraCartCompensa,
              BancoCar, _GenCtbD, _GenCtbC: Integer; var Carteira, GenCtbD,
              GenCtbC: Integer): Boolean;

              (*const Controle, Sub: Integer;
              const Credito, Debito: Double; const OutraCartCompensa, BancoCar,
              _GenCtbD, _GenCtbC: Integer; var Carteira, GenCtbD, GenCtbC:
              Integer): Boolean;*)
    function  ImpedePorFaltaDeContaContabil(Controle, Sub, Carteira, GenCtbD,
              GenCtbC: Integer; Avisa: Boolean = True): Boolean;
    function  DefineDescricaoMyPagtos(FatID: Integer; DescrEspecif, Caption: String): String;
    function  ObtemProximaOrdemNoNivelDoPlanoDeContas(Tabela, CampoIdx,
              CampoAnd: String; IDAnd: Integer): Integer;
    function  ObtemProximoPlaFluCad(SQLType: TSQLType; Codigo, Nivel: Integer): Integer;
  end;
const
  // Certid�es > TFmCertidaoNeg
  CO_INADIMPL_NENHUM                = 0;
  CO_INADIMPL_APENAS_COM_PENDENCIAS = 1;
  CO_INADIMPL_APENAS_SEM_PENDENCIAS = 2;
  CO_INADIMPL_COM_E_SEM_PENDENCIAS  = 3;
  CO_INADIMPL_BOLETO_ESPECIFICO     = 4;  // Carta autom�tica!
  //
  (* 2017-02-15 => Movido para o UnDmkEnums => Usar dmkPF.MotivDel_ValidaCodigo(_CODIGO_AQUI_)
  //Motivos de exclus�o
  CO_MOTVDEL_100_ALTERALCT                = 100;
  CO_MOTVDEL_101_ALTERALCTESPECIFICO      = 101;
  CO_MOTVDEL_102_ALTERALCTTRANFCARTE      = 102;
  CO_MOTVDEL_103_ALTERALCTTRANFCONTA      = 103;
  CO_MOTVDEL_104_ALTERALCTFISICO          = 104;
  CO_MOTVDEL_105_ALTERAQUITACAOPARCIAL    = 105;
  CO_MOTVDEL_200_DESFAZQUITACAO           = 200;
  CO_MOTVDEL_300_EXCLUILCT                = 300;
  CO_MOTVDEL_301_EXCLUILCTINCONDICIONAL   = 301;
  CO_MOTVDEL_302_EXCLUITRANSFCARTEIRA     = 302;
  CO_MOTVDEL_303_EXCLUITRANSFCONTA        = 303;
  CO_MOTVDEL_304_EXCLUIQUITACAOPARCIAL    = 304;
  CO_MOTVDEL_305_EXCLUILCTSEMCLIINT       = 305;
  CO_MOTVDEL_306_EXCLUILCTARRECADACAO     = 306;
  CO_MOTVDEL_307_EXCLUILCTLEITURA         = 307;
  CO_MOTVDEL_308_EXCLUILCTBOLETO          = 308;
  CO_MOTVDEL_309_EXCLUILCTRATIFREPARC     = 309;
  CO_MOTVDEL_310_EXCLUILCTREPARC          = 310;
  CO_MOTVDEL_311_EXCLUILCTFATURAMENTO     = 311;
  CO_MOTVDEL_312_EXCLUILCTFATURAMENTOPARC = 312;
  CO_MOTVDEL_313_EXCLUILCTSELFGER2        = 313;
  CO_MOTVDEL_314_EXCLUILCTPQEDUPLICATAF   = 314;
  CO_MOTVDEL_315_EXCLUILCTPQEDUPLICATAT   = 315;
  CO_MOTVDEL_316_EXCLUILCTMPINDUPLICATAF  = 316;
  CO_MOTVDEL_317_EXCLUILCTMPINDUPLICATAT  = 317;
  CO_MOTVDEL_318_EXCLUILCTFATURAPROD      = 318;
  //CO_MOTVDEL_319_EXCLUILCTMPINDUPLICATAT  = 319;
  CO_MOTVDEL_320_EXCLUILCTFATURAFRETE     = 320;

  CO_MOTVDEL_MAX = 22;
  ListaMotivosExclusaoLct_Str: array[0..CO_MOTVDEL_MAX] of string =
  (
  'Indefinido', 
  'Altera��o de lan�amento',
  'Altera��o de lan�amento (Janela espec�fica de altera��o)',
  'Altera��o de transfer�ncia entre carteiras',
  'Altera��o de transfer�ncia entre contas',
  'Aletar��o de lan�amento com documento f�sico',
  'Aletar��o de quita��o parcial',
  'Desfazimento de quita��o',
  'Exclus�o de lan�amento',
  'Exclus�o incondicional',
  'Exclus�o de transfer�ncia entre carteiras',
  'Exclus�o de transfer�ncia entre contas',
  'Exclus�o de quita��o parcial',
  'Exclus�o de lan�amento sem cliente interno',
  'Exclus�o de lan�amento de arrecada��o',
  'Exclus�o de lan�amento de consumo por leitura',
  'Exclus�o de lan�amento de boleto',
  'Exclus�o de ratifica��o de parcelamento',
  'Exclus�o de parcelamento',
  'Exclus�o de lan�amento de faturamento',
  'Exclus�o de lan�amento de faturamento parcial',
  'Exclus�o de lan�amento (SelfGer2)',
  'Exclus�o de lan�amento faturamento de frete'
  );

  ListaMotivosExclusaoLct_Int: array[0..CO_MOTVDEL_MAX] of Integer =
  (
    0, 100, 101, 102, 103, 104, 105, 200, 300, 301, 302, 303, 304, 305, 306, 307,
    308, 309, 310, 311, 312, 313, 320
  );
  *)
var
  UFinanceiro: TUnFinanceiro;
    FLAN_VERIFICA_CLIINT: Boolean;
    //
    FLAN_Data,
    FLAN_Vencimento,
    FLAN_DataCad,
    FLAN_Descricao,
    FLAN_Compensado,
    FLAN_DataDoc,
    FLAN_Duplicata,
    FLAN_Doc2,
    FLAN_SerieCH,
    FLAN_Emitente,
    FLAN_CNPJCPF,
    //FLAN_Banco, int
    FLAN_ContaCorrente,
    FLAN_SerieNF,
    FLAN_Antigo,
    FLAN_VctoOriginal,
    FLAN_ModeloNF: String;

    FLAN_Documento,
    FLAN_FatNum: Double;

    FLAN_Agencia,
    FLAN_Mez,
    FLAN_Tipo,
    FLAN_Carteira,
    FLAN_Genero,
    FLAN_GenCtb,
    FLAN_GenCtbD,
    FLAN_GenCtbC,
    FLAN_NotaFiscal,
    FLAN_Sit,
    FLAN_Controle,
    FLAN_ID_Pgto,
    FLAN_Cartao,
    FLAN_Linha,
    FLAN_Fornecedor,
    FLAN_Cliente,
    FLAN_UserCad,
    FLAN_Vendedor,
    FLAN_Account,
    FLAN_CentroCusto,
    FLAN_CliInt,
    FLAN_Depto,
    FLAN_DescoPor,
    FLAN_ForneceI,
    FLAN_Unidade,
    FLAN_FatID,
    FLAN_FatID_Sub,
    FLAN_FatParcela,
    FLAN_FatParcRef,
    FLAN_FatGrupo,
    FLAN_CtrlIni,
    FLAN_Nivel,
    FLAN_CNAB_Sit,
    FLAN_TipoCH,
    FLAN_Atrelado,
    FLAN_Sub,
    FLAN_Banco,
    FLAN_SubPgto1,
    FLAN_MultiPgto,
    FLAN_EventosCad,
    FLAN_IndiPag,
    FLAN_FisicoSrc,
    FLAN_FisicoCod: Integer;

    FLAN_Credito,
    FLAN_Debito,
    FLAN_MoraDia,
    FLAN_Multa,
    FLAN_ICMS_P,
    FLAN_ICMS_V,
    FLAN_DescoVal,
    FLAN_NFVal,
    FLAN_Qtde,
    FLAN_Qtd2,
    FLAN_MoraVal,
    FLAN_MultaVal,
    FLAN_TaxasVal: Double;

    VLAN_QRCARTEIRAS, VLAN_QRLCT: TmySQLQuery;

const
  //
  //TTypCtbCadMoF = (
  CO_TXT_tccmfIndef         = 'Indefinido';
  CO_TXT_tccmfUsoConsumoIn  = 'Entrada uso/consumo';
  CO_TXT_tccmfEstoqueIn     = 'Entrada estoque';
  CO_TXT_tccmfDevolucao     = 'Devolu��o de compra';
  CO_TXT_tccmfVenda         = 'Venda';
  CO_TXT_tccmfServicoTomado = 'Servi�o tomado';
  MaxTypCtbCadMoF = Integer(High(TTypCtbCadMoF));
  sTypCtbCadMoF: array[0..MaxTypCtbCadMoF] of string = (
  CO_TXT_tccmfIndef               , //=0,
  CO_TXT_tccmfUsoConsumoIn        , //=1,
  CO_TXT_tccmfEstoqueIn           , //=2,
  CO_TXT_tccmfDevolucao           , //=3,
  CO_TXT_tccmfVenda               , //=4
  CO_TXT_tccmfServicoTomado        //=5
  );


implementation

uses
{$IfNDef NAO_CMEB} Concilia, {$EndIf}
  ModuleGeral, UMySQLModule, UnInternalConsts, UnMsgInt, UnMyObjects,
  //Forms:
  {###
  LocPgto,
  ContasHistAtz2, ContasHisVerif,
  }
  Transfer2, TransferCtas, QuitaDoc2, LctPgVarios, SomaDinheiro, CartPgto,
  LctPgEmCxa, LocDefs, LctMudaCart, LctDuplic, LctEdit, LctEdit2, LctEdita,
  //
  GetData, GetDataECartBco, SelOnStringGrid, ModuleFin, Module, UnMySQLCuringa,
  UCreate, MudaTexto;


{###
procedure TUnFinanceiro.LocalizaPagamentosDeEmissao(Controle: Integer;
TPDataIni, TPDataFim: TDateTimePicker; Query: TmySQLQuery);
begin
  DmodFin.QrLPE.Close;
  DmodFin.QrLPE.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(DmodFin.QrLPE, Dmod.MyDB);
  //
  if DmodFin.QrLPE.RecordCount = 0 then
  begin
    Geral.MB_Aviso(
      'N�o foi localizado nunhum pagamento para este lan�amento!', 'Aviso',
      );
  end else begin
    MyObjects.CriaForm_AcessoTotal(TFmLocPgto, FmLocPgto);
    FmLocPgto.FQuery := Query;
    FmLocPgto.FDTPDataIni := TPDataIni;
    FmLocPgto.FDTPDataFim := TPDataFim;
    FmLocPgto.ShowModal;
    FmLocPgto.Destroy;
  end;
end;
}

{###
procedure TUnFinanceiro.ExcluiLanctoGrade(DBGLct: TDBGrid;
QrLct, QrCrt: TmySQLQuery; ExcluiAtehFatID: Boolean);
var
  i, k: Integer;
begin
  if QrLct = nil then
  begin
    Geral.MB_Aviso('A grade ' + DBGLct.Name +
      ' n�o tem defini��o de "QrLct"!'), 'Aviso', MB_OK+MB_ICONERROR);
    Exit;
  end;

  if (ExcluiAtehFatID = False)
  and (QrLct.FieldByName('FatID').AsInteger <> 0) then
  begin
    Geral.MB_Aviso('Exclus�o cancelada! Este lan�amento tem ' +
    'seu controle espec�fico em outra janela!'));
    Exit;
  end;

  if DBGLct.SelectedRows.Count > 1 then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o '+
    'dos itens selecionados?')) =
    ID_YES then
    begin
      with DBGLct.DataSource.DataSet do
      for i:= 0 to DBGLct.SelectedRows.Count-1 do
      begin
        GotoBookmark(DBGLct.SelectedRows.Items[i]);
        ExcluiItemCarteira(
          QrLct.FieldByName('Controle').AsInteger,
          QrLct.FieldByName('Sub').AsInteger,
          QrLct.FieldByName('Genero').AsInteger,
          QrLct.FieldByName('Cartao').AsInteger,
          QrLct.FieldByName('Sit').AsInteger,
          QrLct.FieldByName('Tipo').AsInteger,
          0,
          QrLct.FieldByName('ID_Pgto').AsInteger,
          QrLct, QrCrt, False,
          QrLct.FieldByName('Carteira').AsInteger);
      end;
      k := UMyMod.ProximoRegistro(QrLct, 'Controle',  0);
      RecalcSaldoCarteira(QrLct.FieldByName('Carteira').AsInteger,
        QrCrt, True, True);
      QrLct.Locate('Controle', k, []);
    end;
  end else ExcluiItemCarteira(
    QrLct.FieldByName('Controle').AsInteger,
    QrLct.FieldByName('Sub').AsInteger,
    QrLct.FieldByName('Genero').AsInteger,
    QrLct.FieldByName('Cartao').AsInteger,
    QrLct.FieldByName('Sit').AsInteger,
    QrLct.FieldByName('Tipo').AsInteger, 0,
    QrLct.FieldByName('ID_Pgto').AsInteger,
    QrLct, QrCrt, True,
    QrLct.FieldByName('Carteira').AsInteger);
end;
}

procedure TUnFinanceiro.ExcluiItemCarteira(Controle: Int64;
  // Adicionado 2011-01-28
  Data: TDateTime; Carteira,
  // fim adicionado 2011-01-28
  Sub, Genero, Cartao, Sit, Tipo, Chamada, ID_Pgto: Integer;
  QrLct, QrCrt: TmySQLQuery; Pergunta: Boolean;
  CartCalcSdo, MotvDel: Integer; TabLctA: String;
  // Adicionado 2014-04-03
  PermiteExclusaoDeLctFatID: Boolean;
  // fim adicionado 2014-04-03
  // Adicionado 2011-05-19
  ExcluiMesmoSeNegativo: Boolean);
  // fim adicionado 2011-05-19
var
  Continua, Prox, CliInt: Integer;
begin
  CliInt := QrLct.FieldByName('CliInt').AsInteger;
  //if UMyMod.SelLockInt64Y(Lancto, Dmod.MyDB, TabLctA, 'Controle') then Exit;
  VAR_LANCTO2 := Controle;
  //
  if (not PermiteExclusaoDeLctFatID) and (QrLct.FieldByName('FatID').AsInteger <> 0) then
  begin
    if (QrLct.FieldByName('FatID').AsInteger = -1) then
    begin
      CriarTransferCtas(2, QrLct, QrCrt, True, CliInt, 0, 0, 0, TabLctA);
      RecalcSaldoCarteira(QrCrt.FieldByName('Codigo').AsInteger, QrCrt, QrLct,
        True, True);
      Exit;
    end else
    begin
      Geral.MB_Aviso('Lan�amento espec�fico. Para excluir este item selecione sua janela correta!');
      Exit;
    end;
  end;
  if (Cartao > 0) then
  begin
    Geral.MB_Aviso('Esta emiss�o n�o pode ser exclu�da pois pertence a uma fatura!');
    Exit;
  end;
  if (Sit > 0) and (Tipo = 2) then
  begin
    Geral.MB_Aviso('Esta emiss�o n�o pode ser exclu�da pois est� quitada ou possui quita��o parcial!');
    Exit;
  end;
  if (Genero < 0) or (Sub > 0) then
  begin
    if Genero = -1 then
    begin
      CriarTransferCart(2, QrLct, QrCrt, True, CliInt, 0, 0, 0, TabLctA);
      RecalcSaldoCarteira(QrCrt.FieldByName('Codigo').AsInteger, QrCrt, QrLct,
        True, True);
      Exit;
    end else if Chamada = 1 (*FmCartPagto*) then
    begin
      // Exclui normalmente
    end else
    if (Genero < 0)and ExcluiMesmoSeNegativo then
    begin
      // Exclui normalmente
    end else
    begin
      Geral.MB_Aviso('Em constru��o. Para editar este item selecione o menu de op��es');
      Exit;
    end;
  end else if Genero = 0 then
  begin
    Geral.MB_Aviso('Exclus�o cancelada. Lan�amento sem conta!');
    Exit;
  end;
  if (Data > 2) and (Data < VAR_DATA_MINIMA) then
  begin
    Geral.MB_Aviso('Lan�amento Encerrado. Para editar '+
      'este item desfa�a o encerramento do m�s correspondente!');
    Exit;
  end;
  if Pergunta then Continua := Geral.MB_Pergunta(FIN_MSG_ASKESCLUI)
  else Continua := ID_YES;
  if Continua = ID_YES then
  begin
{
    (*DmodFin.QrUpd*)Dmod.QrUpdM.SQL.Clear;
    (*DmodFin.QrUpd*)Dmod.QrUpdM.SQL.Add(EXCLUI_DE + TabLctA + ' WHERE Controle=:P0');
    (*DmodFin.QrUpd*)Dmod.QrUpdM.Params[0].AsFloat := Lancto;
    (*DmodFin.QrUpd*)Dmod.QrUpdM.ExecSQL;
}
    ExcluiLct_Unico(TabLctA, QrLct.Database, Data, Tipo, Carteira, Controle,
      Sub, MotvDel, False);
    if ID_Pgto > 0 then
      AtualizaEmissaoMasterExtra_Novo(ID_Pgto, QrLct, nil, nil, nil, nil, '0',
      TabLctA);
    if Pergunta then
    begin
      Prox := UMyMod.ProximoRegistro(QrLct, 'Controle',  Controle);
      RecalcSaldoCarteira(CartCalcSdo, QrCrt, QrLct, True, True);
      QrLct.Locate('Controle', Prox, []);
    end;
  end;
end;

function TUnFinanceiro.ExcluiLct_FatNum(QrDados: TmySQLQuery; FatID: Integer;
  FatNum: Double; Entidade, Carteira, MotvDel: Integer; Pergunta: Boolean;
  TabLctA: String): Boolean;
var
  DB: TmySQLDataBase;
begin
  Result := False;
  if QrDados <> nil then
  begin
    if (QrDados.State = dsInactive) or (QrDados.RecordCount=0) then
    begin
      Geral.MB_Aviso('Remo��o de lan�amento cancelada! (1)' + sLineBreak +
      'Tabela sem registros!');
      Exit;
    end;
  end;
  if (FatID = 0) or (FatNum = 0) or (Entidade = 0) then
  begin
    Geral.MB_Aviso('Remo��o de lan�amento cancelada (2)!' + sLineBreak +
    '"FatID" ou "FatNum" ou "Empresa" igual a zero n�o pode ser exclu�do.' +
    sLineBreak + 'FatID = ' + FormatFloat('0', FatID) + sLineBreak + 'FatNum = ' +
    FormatFloat('0', FatNum) + sLineBreak + 'Empresa = ' + FormatFloat('0', Entidade));
    Exit;
  end;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrFats2, Dmod.MyDB, [
    'SELECT Data, Tipo, Carteira, Controle, Sub ',
    'FROM ' + TabLctA,
    'WHERE FatID=' + FormatFloat('0', FatID),
    'AND FatNum=' + FormatFloat('0', FatNum),
    'AND CliInt=' + FormatFloat('0', Entidade),
    '']);
    //
    if DModFin.QrFats2.RecordCount > 0 then
    begin
      if (Pergunta = False) or (Geral.MB_Pergunta(
      'Confirma a EXCLUS�O DE TODOS LAN�AMENTOS do faturamento selecionado?' + sLineBreak +
      'FatID = ' + FormatFloat('0', FatID) + sLineBreak +
      'FatNum = ' + FormatFloat('0', FatNum)) = ID_YES) then
      begin
        DModFin.QrFats2.First;
        while not DModFin.QrFats2.Eof do
        begin
          if QrDados <> nil then
            DB := QrDados.DataBase
          else
            DB := Dmod.MyDB;
          ExcluiLct_Unico(TabLctA, DB, DModFin.QrFats2Data.Value,
            DModFin.QrFats2Tipo.Value, DModFin.QrFats2Carteira.Value,
            DModFin.QrFats2Controle.Value, DModFin.QrFats2Sub.Value, MotvDel, False);
          //
          DModFin.QrFats2.Next;
        end;
        //Result := True;
        RecalcSaldoCarteira(Carteira, nil, nil, False, False);
      end;
      Result := True;
    end;
  end;
end;

function TUnFinanceiro.ExcluiLct_FatParcela(QrDados: TmySQLQuery; FatID: Integer;
  FatNum: Double; FatParcela, Carteira, Sit, Tipo, MotvDel: Integer;
  TabLctA: String; PermiteFatNum_Zero: Boolean = False;
  PermiteFatParcela_Zero: Boolean = False): Boolean;
begin
  Result := False;
  if (QrDados.State = dsInactive) or (QrDados.RecordCount=0) then
  begin
    Geral.MB_Aviso('Remo��o de lan�amento cancelada (3)!' + sLineBreak +
    'Tabela sem registros!');
    Exit;
  end;
  if (FatID = 0) then
  begin
    Geral.MB_Aviso('Remo��o de lan�amento cancelada (4)!' + sLineBreak +
    '"FatID" igual a zero n�o pode ser exclu�do'
    );
    Exit;
  end;
  if (not PermiteFatNum_Zero) and (FatNum = 0) then
  begin
    Geral.MB_Aviso('Remo��o de lan�amento cancelada (5)!' + sLineBreak +
    '"FatNum" igual a zero n�o pode ser exclu�do'
    );
    Exit;
  end;
  if (not PermiteFatParcela_Zero) and (FatParcela = 0) then
  begin
    Geral.MB_Aviso('Remo��o de lan�amento cancelada (6)!' + sLineBreak +
    '"FatParcela" igual a zero n�o pode ser exclu�do'
    );
    Exit;
  end;
  if (Sit > 0) and (Tipo = 2) then
  begin
    Geral.MB_Aviso('Esta emiss�o n�o pode ser exclu�da pois est� quitada ou possui quita��o parcial!');
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrFats3, Dmod.MyDB, [
  'SELECT Data, Tipo, Carteira, Controle, Sub ',
  'FROM ' + TabLctA,
  'WHERE FatID=' + FormatFloat('0', FatID),
  'AND FatNum=' + FormatFloat('0', FatNum),
  'AND FatParcela=' + FormatFloat('0', FatParcela),
  '']);
  //
  if DModFin.QrFats3.RecordCount = 1 then
  begin
    ExcluiLct_Unico(TabLctA, QrDados.Database, DModFin.QrFats3Data.Value,
    DModFin.QrFats3Tipo.Value, DModFin.QrFats3Carteira.Value,
    DModFin.QrFats3Controle.Value, DModFin.QrFats3Sub.Value, MotvDel, True);
    Result := True;
    //
    DMod.QrUpdM.Close;
    DMod.QrUpdM.SQL.Clear;
    DMod.QrUpdM.SQL.Add('UPDATE ' + TabLctA + ' SET FatParcela=FatParcela-1');
    DMod.QrUpdM.SQL.Add('WHERE FatParcela>:P0 AND FatID=:P1 AND FatNum=:P2');
    DMod.QrUpdM.Params[00].AsInteger := FatParcela;
    DMod.QrUpdM.Params[01].AsInteger := FatID;
    DMod.QrUpdM.Params[02].AsFloat   := FatNum;
    DMod.QrUpdM.ExecSQL;
    //
    RecalcSaldoCarteira(Carteira, nil, nil, False, False);
  end else begin
    if DModFin.QrFats3.RecordCount = 0 then
      Geral.MB_Aviso('Remo��o de lan�amento cancelada (7)!' + sLineBreak +
      'Lan�amento com "FatID" e "FatNum" e "FatParcela" n�o presente na tabela "' +
      TabLctA + '"!')
    else
      Geral.MB_Aviso('Remo��o de lan�amento cancelada (8)!' + sLineBreak +
      'Foi localizado mais de um lan�amento para o mesmo "FatID" e "FatNum" e "FatParcela"!');
  end;
end;

(*
2017-08-07 => Substituida pela fun��o: TUModule.ExcluiRegistro_EnviaArquivoMorto
function TUnFinanceiro.ExcluiLct_EnviaArquivoMorto(TabLctMorto, TabLct: String;
  Motivo, Tipo, Carteira, Sub: Integer; Controle: Double; Query: TmySQLQuery;
  DataBase: TmySQLDatabase): Boolean;
var
  CamposLctz, Dta: String;
begin
  Dta := Geral.FDT(DModG.ObtemAgora(), 105);
  //
  CamposLctz := UMyMod.ObtemCamposDeTabelaIdentica(DataBase, TabLctMorto, '');
  CamposLctz := Geral.Substitui(CamposLctz,
    ', DataDel', ', "' + Dta + '" DataDel');
  CamposLctz := Geral.Substitui(CamposLctz,
    ', UserDel', ', ' + FormatFloat('0', VAR_USUARIO) + ' UserDel');
  CamposLctz := Geral.Substitui(CamposLctz,
    ', MotvDel', ', ' + FormatFloat('0', Motivo) + ' MotvDel');
  //
  CamposLctz := 'INSERT INTO ' + TabLctMorto + ' SELECT ' + sLineBreak +
  CamposLctz + sLineBreak +
  'FROM ' + TabLct + sLineBreak +
  'WHERE Tipo=:P0 AND Carteira=:P1 AND Controle=:P2 AND Sub=:P3;';
  //
  Query.SQL.Text := CamposLctz;
  //
  if Query.Database <> DataBase then
    Query.Database := DataBase;
  //
  Query.Params[00].AsInteger := Tipo;
  Query.Params[01].AsInteger := Carteira;
  Query.Params[02].AsFloat   := Controle;
  Query.Params[03].AsInteger := Sub;
  //
  UMyMod.ExecutaQuery(Query);
  //
  Result := True;
end;
*)

procedure TUnFinanceiro.ExcluiLct_Unico(TabLctA: String; DB: TmySQLDataBase;
  Data: TDateTime; Tipo, Carteira: Integer; Controle: Double; Sub, Motivo: Integer;
  Pergunta: Boolean; EnviaAoMorto: Boolean = True);
var
  Dta, CamposLctz: String;
begin
  if Pergunta then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o do lan�amento:' + sLineBreak +
    'Controle: ' + FormatFloat('0', Controle) + ' - ' + FormatFloat('0', Sub) +
    sLineBreak + 'Data: ' + Geral.FDT(Data, 2) + sLineBreak + 'Carteira: ' +
    FormatFloat('0', Carteira))
    <> ID_YES then Exit;
  end;
  //if ResetaConfig then
  //begin
  Dta := Geral.FDT(DModG.ObtemAgora(), 105);
  if EnviaAoMorto then
  begin
    (*
    CamposLctz := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, 'lanctoz', '');
    CamposLctz := Geral.Substitui(CamposLctz,
      ', DataDel', ', "' + Dta + '" DataDel');
    CamposLctz := Geral.Substitui(CamposLctz,
      ', UserDel', ', ' + FormatFloat('0', VAR_USUARIO) + ' UserDel');
    CamposLctz := Geral.Substitui(CamposLctz,
      ', MotvDel', ', ' + FormatFloat('0', Motivo) + ' MotvDel');
    //
    CamposLctz := 'INSERT INTO lanctoz SELECT ' + sLineBreak +
    CamposLctz + sLineBreak +
    'FROM ' + TabLctA + sLineBreak +
    'WHERE Data=:P0 AND Tipo=:P1 AND Carteira=:P2 AND Controle=:P3 AND Sub=:P4;' +
    sLineBreak + DELETE_FROM + TabLctA + sLineBreak +
    'WHERE Data=:P5 AND Tipo=:P6 AND Carteira=:P7 AND Controle=:P8 AND Sub=:P9;';
    *)
    //
    (*
    ExcluiLct_EnviaArquivoMorto('lanctoz', TabLctA, Motivo, Tipo, Carteira,
      Sub, Controle, DModFin.QrDelLct, Dmod.MyDB);
    *)
    UMyMod.ExcluiRegistro_EnviaArquivoMorto('lanctoz', TabLctA, Motivo, ['Tipo',
      'Carteira', 'Sub', 'Controle'], [Tipo, Carteira, Sub, Controle], Dmod.MyDB);
  end;
  CamposLctz := DELETE_FROM + TabLctA + sLineBreak +
    'WHERE Data=:P0 AND Tipo=:P1 AND Carteira=:P2 AND Controle=:P3 AND Sub=:P4;';
  //
  DModFin.QrDelLct.SQL.Text := CamposLctz;
  //
  if DModFin.QrDelLct.Database <> DB then
    DModFin.QrDelLct.Database := DB;
  //
  Dta := Geral.FDT(Data, 1);
  DModFin.QrDelLct.Params[00].AsString  := Dta;
  DModFin.QrDelLct.Params[01].AsInteger := Tipo;
  DModFin.QrDelLct.Params[02].AsInteger := Carteira;
  DModFin.QrDelLct.Params[03].AsFloat   := Controle;
  DModFin.QrDelLct.Params[04].AsInteger := Sub;
  UMyMod.ExecutaQuery(DModFin.QrDelLct);
  (*
    DModFin.QrDelLct.SQL.Text := CamposLctz;
  //end;
  if DModFin.QrDelLct.Database <> DB then
    DModFin.QrDelLct.Database := DB;
  Dta := Geral.FDT(Data, 1);
  DModFin.QrDelLct.Params[00].AsString  := Dta;
  DModFin.QrDelLct.Params[01].AsInteger := Tipo;
  DModFin.QrDelLct.Params[02].AsInteger := Carteira;
  DModFin.QrDelLct.Params[03].AsFloat   := Controle;
  DModFin.QrDelLct.Params[04].AsInteger := Sub;
  if EnviaAoMorto then
  begin
    DModFin.QrDelLct.Params[05].AsString  := Dta;
    DModFin.QrDelLct.Params[06].AsInteger := Tipo;
    DModFin.QrDelLct.Params[07].AsInteger := Carteira;
    DModFin.QrDelLct.Params[08].AsFloat   := Controle;
    DModFin.QrDelLct.Params[09].AsInteger := Sub;
  end;
  UMyMod.ExecutaQuery(DModFin.QrDelLct);
  *)
end;

procedure TUnFinanceiro.ExclusaoIncondicionalDeLancamento(QrCrt,
  QrLct: TmySQLQuery; TabLctA: String);
var
  Data: TDateTime;
  Controle, Sub, Tipo, Carteira: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  Data := QrLct.FieldByName('Data').AsDateTime;
  if Data < VAR_DATA_MINIMA then
  begin
    Geral.MB_Aviso(
    'O lan�amento selecionado n�o ser� excluido pois pertence a um m�s encerrado!');
    Exit;
  end;
  if Geral.MB_Pergunta(
  'Confirma a exclus�o INCONDICIONAL deste lan�amento?') = ID_YES then
  begin
    Controle := QrLct.FieldByName('Controle').AsInteger;
    Sub      := QrLct.FieldByName('Sub').AsInteger;
    Carteira := QrLct.FieldByName('Carteira').AsInteger;
    Tipo     := QrLct.FieldByName('Tipo').AsInteger;
    //
    ExcluiLct_Unico(TabLctA, QrLct.Database, Data, Tipo, Carteira, Controle,
      Sub, dmkPF.MotivDel_ValidaCodigo(301), False);
    //
    RecalcSaldoCarteira(
      QrLct.FieldByName('Carteira').AsInteger, QrCrt, QrLct, True, True);
  end;
end;

function TUnFinanceiro.FatIDProibidoEmLct(FatID: Integer; var Msg: String): Boolean;
begin
  //True  = N�o pode editar
  //False = Pode editar
  //
  Msg    := '';
  Result := False;
  //
  if FatID <> 0 then
  begin
    case FatID of
      300, 399: Result      := True;  //Credito2 => N�o libera
      600, 601, 610: Result := False; //Syndi2   => Libera para quita��o de documentos apenas
    end;
  end;
  if Result = True then
    Msg := 'Este lan�amento � gerenciado por janela espec�fica!' +
      'N�o � permitido seu pagamento aqui!';
end;

procedure TUnFinanceiro.RecalcSaldoCarteira(Carteira: Integer;
  QrCrt, QrLct: TmySQLQuery; Reabre, Localiza: Boolean);
var
  Controle, Sub: Integer;
  Saldo: Double;
begin
  if Localiza and (QrLct <> nil) then
  begin
    Controle := QrLct.FieldByName('Controle').AsInteger;
    Sub      := QrLct.FieldByName('Sub').AsInteger;
  end else begin
    Controle := 0;
    Sub      := 0;
  end;
{
  N�o calcular mais? Toma muito tempo?
  AtualizaVencimentos;
  //
  DmodFin.QrAtzCar.Close;
  DmodFin.QrAtzCar.Params[0].AsInteger := Carteira;
  UnDmkDAC_PF.AbreQuery(DmodFin.QrAtzCar, Dmod.MyDB);
  //
  (*DmodFin.QrUpd*)Dmod.QrUpdM.SQL.Clear;
  (*DmodFin.QrUpd*)Dmod.QrUpdM.SQL.Add('UPDATE carteiras SET Saldo=:P0, ');
  (*DmodFin.QrUpd*)Dmod.QrUpdM.SQL.Add('FuturoC=:P1, FuturoD=:P2, FuturoS=:P3 ');
  (*DmodFin.QrUpd*)Dmod.QrUpdM.SQL.Add('');
  (*DmodFin.QrUpd*)Dmod.QrUpdM.SQL.Add('WHERE Codigo=:Pa ');
  (*DmodFin.QrUpd*)Dmod.QrUpdM.Params[00].AsFloat   := DmodFin.QrAtzCarINICIAL.Value + DmodFin.QrAtzCarATU_S.Value;
  (*DmodFin.QrUpd*)Dmod.QrUpdM.Params[01].AsFloat   := DmodFin.QrAtzCarFUT_C.Value;
  (*DmodFin.QrUpd*)Dmod.QrUpdM.Params[02].AsFloat   := DmodFin.QrAtzCarFUT_D.Value;
  (*DmodFin.QrUpd*)Dmod.QrUpdM.Params[03].AsFloat   := DmodFin.QrAtzCarFUT_S.Value;
  (*DmodFin.QrUpd*)Dmod.QrUpdM.Params[04].AsInteger := Carteira;
  (*DmodFin.QrUpd*)Dmod.QrUpdM.ExecSQL;
  //
}
  // 2010-09-07
  Saldo := ObtemSaldoCarteira(Carteira);
  (*DmodFin.QrUpd*)Dmod.QrUpdM.SQL.Clear;
  (*DmodFin.QrUpd*)Dmod.QrUpdM.SQL.Add('UPDATE carteiras SET Saldo=:P0 ');
  //(*DmodFin.QrUpd*)Dmod.QrUpdM.SQL.Add('FuturoC=:P1, FuturoD=:P2, FuturoS=:P3 ');
  (*DmodFin.QrUpd*)Dmod.QrUpdM.SQL.Add('');
  (*DmodFin.QrUpd*)Dmod.QrUpdM.SQL.Add('WHERE Codigo=:Pa ');
  (*DmodFin.QrUpd*)Dmod.QrUpdM.Params[00].AsFloat   := Saldo;
  //(*DmodFin.QrUpd*)Dmod.QrUpdM.Params[01].AsFloat   := DmodFin.QrAtzCarFUT_C.Value;
  //(*DmodFin.QrUpd*)Dmod.QrUpdM.Params[02].AsFloat   := DmodFin.QrAtzCarFUT_D.Value;
  //(*DmodFin.QrUpd*)Dmod.QrUpdM.Params[03].AsFloat   := DmodFin.QrAtzCarFUT_S.Value;
  (*DmodFin.QrUpd*)Dmod.QrUpdM.Params[01].AsInteger := Carteira;
  (*DmodFin.QrUpd*)Dmod.QrUpdM.ExecSQL;
  //
  //  Fim 2010-09-07

  if Reabre and (QrCrt <> nil) then
  begin
    QrCrt.Close;
    UnDmkDAC_PF.AbreQuery(QrCrt, Dmod.MyDB);
  end;
  if Localiza and (QrCrt <> nil) and (QrCrt.State <> dsInactive) then
    QrCrt.Locate('Codigo', Carteira, []);
  // ini 2021-04-20
  if Reabre and (QrLct <> nil) then
  begin
    QrLct.Close;
    UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
  end;
  // fim 2021-04-20
  if Localiza and (QrLct <> nil) then
    QrLct.Locate('Controle;Sub', VarArrayOf([Controle,Sub]), []);
end;

procedure TUnFinanceiro.AcertarCreditoPeloValorPago(QrCrt, QrLct: TmySQLQuery;
  DBGLct: TDBGrid; TabLctA: String);
var
  i, Carteira: Integer;
  Valor: Double;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  if Geral.MB_Pergunta('Aten��o CUIDADO!' + sLineBreak + 'Todos itens ' +
    'selecionados ser�o modificados quando houver valor da multa e/ou juros!') = ID_YES then
  begin
    //Tipo     := QrLct.FieldByName('Tipo').AsInteger;
    Carteira := QrLct.FieldByName('Carteira').AsInteger;
    with DBGLct.DataSource.DataSet do
    begin
      for i:= 0 to DBGLct.SelectedRows.Count-1 do
      begin
        //GotoBookmark(DBGLct.SelectedRows.Items[i]);
        GotoBookmark(DBGLct.SelectedRows.Items[i]);
        if (QrLct.FieldByName('Sit').AsInteger > 1) then
        begin
          Valor := QrLct.FieldByName('PagJur').AsFloat +
                   QrLct.FieldByName('PagMul').AsFloat;
          if Valor < 0 then
          begin
            Valor := Valor + QrLct.FieldByName('Credito').AsFloat;
            UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
            'Credito', 'PagJur', 'PagMul', 'MoraVal', 'MultaVal'], ['Controle',
            'Sub'], [Valor, 0, 0, 0, 0], [QrLct.FieldByName('Controle').AsInteger,
            QrLct.FieldByName('Sub').AsInteger], True, '', TabLctA);
          end;
        end;
      end;
    end;
    RecalcSaldoCarteira(Carteira, QrCrt, QrLct, True, True);
  end;
end;

function TUnFinanceiro.AlteracaoLancamento(QrCrt, QrLct: TmySQLQuery;
  Finalidade: TLanctoFinalidade; OneAccount, OneCliInt: Integer;
  TabLctA: String; AlteraAtehFatID: Boolean): Boolean;
  function ObtemFatIDDaOrigem(ID_Pgto, Sub: Integer): Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrAux, Dmod.MyDB, [
    'SELECT FatID ',
    'FROM lct0001a ',
    'WHERE Controle=' + Geral.FF0(ID_Pgto),
    'AND Sub=' + Geral.FF0(Sub),
    '']);
    if DModG.QrAux.RecordCount > 0 then
      Result := DModG.QrAux.FieldByName('FatID').AsInteger
    else
      Result := 0;
  end;
var
  Sit, Tipo, Cartao: Integer;
begin
  Result := False;
  if MyObjects.FIC(QrLct = nil, nil, 'Tabela de lan�amentos n�o definida!') then
    Exit;
  if MyObjects.FIC(QrLct.RecordCount = 0, nil,
    'Nenhum lan�amento foi selecionado!') then
    Exit;
  if MyObjects.FIC(QrLct.FieldByName('Genero').AsInteger = -1, nil,
    'Altera��o cancelada!' + sLineBreak +
    'Lan�amento pertence a uma transfer�ncia entre carteiras!') then
    Exit;
  if MyObjects.FIC(QrLct.FieldByName('FatID').AsInteger = -1, nil,
    'Altera��o cancelada!' + sLineBreak +
    'Lan�amento pertence a uma transfer�ncia entre contas do plano de contas!') then
    Exit;
  if MyObjects.FIC((QrLct.FieldByName('FatID').AsInteger <> 0) and (AlteraAtehFatID = False), nil,
    'Altera��o cancelada!' + sLineBreak +
    'Lan�amento foi gerado em uma janela espec�fica!' + sLineBreak +
    'FatID c�d. ' + Geral.FF0(QrLct.FieldByName('FatID').Value) + ': ' +
    DmodFin.ObtemDescricaoDeFatID(QrLct.FieldByName('FatID').AsInteger)) then
      Exit;
  //
  if (AlteraAtehFatID = False) and (QrLct <> nil) and
    (QrLct.FieldByName('FatID').AsInteger <> 0 ) then
  begin
    Geral.MB_Aviso('Lan�amento espec�fico. Para editar este item selecione sua janela correta!');
    Exit;
  end;
  if (AlteraAtehFatID = False) and (QrLct <> nil) then
  begin
    if ObtemFatIDDaOrigem(QrLct.FieldByName('ID_Pgto').AsInteger,
      QrLct.FieldByName('Sub').AsInteger) <> 0 then
    begin
      Geral.MB_Aviso('Lan�amento espec�fico. Para editar este item selecione sua janela correta!');
      Exit;
    end;
  end;
  //
  Sit    := QrLct.FieldByName('Sit').AsInteger;
  Tipo   := QrLct.FieldByName('Tipo').AsInteger;
  Cartao := QrLct.FieldByName('Cartao').AsInteger;
  //
  if UmyMod.FormInsUpd_Cria(TFmLctEdita, FmLctEdita,
  afmoNegarComAviso, QrLct, stUpd) then
  begin
    with FmLctEdita do
    begin
      ImgTipo.SQLType := stUpd;
      FTabLctA      := TabLctA;
      FQrCrt        := QrCrt;
      FQrLct        := QrLct;
      FFinalidade   := Finalidade;
      FOneAccount   := OneAccount;
      FOneCliInt    := OneCliInt;

      //
      if (Sit > 1) and (Tipo = 2) then
      begin
        if (Cartao <> 0) and not VAR_FATURANDO then
        begin
          Geral.MB_Aviso('Esta emiss�o j� est� quitada!');
          EdDeb.Enabled       := False;
          EdCred.Enabled      := False;
          EdDoc.Enabled       := False;
          EdDescricao.Enabled := False;
        end;
        EdCarteira.Enabled := False;
        CBCarteira.Enabled := False;
        VAR_BAIXADO := Sit;
      end else VAR_BAIXADO := -2;
      //
      ShowModal;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.AlteraGenero(CtrlMae: Integer; TabLctA: String; Genero:
Integer): Integer;
var
  Tipo, Carteira, Sub: Integer;
  Data, CtrlTxt: String;
  Qry: TmySQLQuery;
  Controle: Int64;
begin
  Result := 0;
  //MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
  if CtrlMae <> 0 then
  begin
    CtrlTxt := Geral.FF0(CtrlMae);
    //
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDb, [
      'SELECT Data, Tipo, Carteira, Controle, Sub, Genero ',
      'FROM ' + TabLctA,
      'WHERE ID_Pgto=' + CtrlTxt,
      'OR CtrlQuitPg=' + CtrlTxt,
      'OR Controle=' + CtrlTxt,
      '']);
      //
      Qry.First;
      while not Qry.Eof do
      begin
        Data      := Geral.FDT(Qry.FieldByName('Data').AsDateTime, 1);
        Tipo      := Qry.FieldByName('Tipo').AsInteger;
        Carteira  := Qry.FieldByName('Carteira').AsInteger;
        Controle  := Qry.FieldByName('Controle').AsLargeInt;
        Sub       := Qry.FieldByName('Sub').AsInteger;
        //
        if SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Genero'], [
        'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
        Genero], [
        Data, Tipo, Carteira, Controle, Sub], True, '',TabLctA) then
          Result := Result + 1;
        //
        Qry.Next;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

procedure TUnFinanceiro.AtualizaEmissaoMasterExtra_Novo(ID_Pgto: Integer; FQuery: TmySQLQuery;
 EdDeb, EdCred, EdSaldoDeb, EdSaldoCred: TdmkEdit; DataCompQuit, TabLctA: String; RecalcCart: Boolean);
var
  Pago, MoraVal, MultaVal, DescoVal: Double;
  Controle, _Sit, Sit : Integer;
  Data: String;
begin
  _Sit := 0;     // Mudar ? J� trazer valor atual como no AtualizaEmissaoMasterRapida (_Sit) ?
  Controle := 0; // Mudar ? J� trazer valor atual como no AtualizaEmissaoMasterRapida (Controle) ?
  if not DmodFin.Reopen_APL_VLA(ID_Pgto, _Sit, Data,
    Pago, MoraVal, MultaVal, DescoVal, Sit, DataCompQuit, TabLctA) then
      Exit;
  //
  //
  (* Fazer mostrar ??  Parei Aqui
  if EdDeb <> nil then
    EdDeb.Text := Geral.TFT(FloatToStr(Debito), 2, siNegativo);
  if EdCred <> nil then
    EdCred.Text := Geral.TFT(FloatToStr(Credito), 2, siNegativo);
  if EdSaldoDeb <> nil then
    EdSaldoDeb.Text := Geral.TFT(FloatToStr(SaldoDeb), 2, siNegativo);
  if EdSaldoCred <> nil then
    EdSaldoCred.Text := Geral.TFT(FloatToStr(Credito -
      FQuery.FieldByName('Credito').AsFloat), 2, siNegativo);
  *)
  //
(*
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET Sit=:P0, Pago=:P1, Compensado=:P2, ');
  Dmod.QrUpdM.SQL.Add('PagJur=:P3, PagMul=:P4, CtrlQuitPg=:P5');
  Dmod.QrUpdM.SQL.Add('WHERE Controle=:P6');
  Dmod.QrUpdM.Params[00].AsInteger := Sit;
  Dmod.QrUpdM.Params[01].AsFloat   := Pago;
  Dmod.QrUpdM.Params[02].AsString  := Data;
  Dmod.QrUpdM.Params[03].AsFloat   := MoraVal;
  Dmod.QrUpdM.Params[04].AsFloat   := MultaVal;
  Dmod.QrUpdM.Params[05].AsInteger := Controle;
  //
  Dmod.QrUpdM.Params[06].AsInteger := ID_Pgto;
  Dmod.QrUpdM.ExecSQL;
*)
  if (Data = '') or (Data = '0') then
    Data := '0000-00-00';
  //
  SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False,
    ['Sit', 'Pago', 'Compensado', 'PagJur', 'PagMul', 'RecDes', 'CtrlQuitPg'],
    ['Controle'],
    [Sit, Pago, Data, MoraVal, MultaVal, DescoVal, Controle],
    [ID_Pgto], True, '', TabLctA);
  //
  if RecalcCart then
    RecalcSaldoCarteira(FQuery.FieldByName('Carteira').AsInteger,
      nil, nil, False, False);
end;

procedure TUnFinanceiro.AtualizaEmissaoMasterRapida(ID_Pgto, _Sit, Controle:
Integer; DataCompQuit, TabLctA: String);
var
  Pago, MoraVal, MultaVal, DescoVal: Double;
  Sit : Integer;
  Data: String;
begin
  if not DmodFin.Reopen_APL_VLA(ID_Pgto, _Sit, Data,
    Pago, MoraVal, MultaVal, DescoVal, Sit, DataCompQuit, TabLctA) then
      Exit;
  //
  if (Data = '') or (Data = '0') then
    Data := '0000-00-00';
  //
  SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
  'Sit', 'Pago', 'Compensado', 'PagJur', 'PagMul', 'CtrlQuitPg'], ['Controle'], [
  Sit, Pago, Data, MoraVal, MultaVal, Controle], [ID_Pgto], True, '', TabLctA);
end;

function TUnFinanceiro.InsereLancamento(TabLctA: String): Boolean;
begin
  if DModG.QrCtrlGeralUsarFinCtb.Value = 1 then
  begin
    if (FLAN_GenCtbD < 1) or (FLAN_GenCtbC < 1) then
    begin
      Geral.MB_Aviso('Inclus�o de lan�amento abortada!' + sLineBreak +
      'As duas contas cont�beis devem ser maior que zero!' + sLineBreak +
      'Conta de d�bito: ' + Geral.FF0(FLAN_GenCtbD) + sLineBreak +
      'Conta de cr�dito: ' + Geral.FF0(FLAN_GenCtbC) + sLineBreak +
      '');
      Exit;
    end;
  end;
  //
  if FLAN_Tipo = 2 then
  begin
    //FLAN_ID_Pgto := 0;
  end else begin
    if (FLAN_Compensado = '') or (FLAN_Compensado = '0000-00-00') then
    begin
      if FLAN_Tipo = 1 then
      begin
        Dmod.QrLocY.Database := Dmod.MyDB;
        Dmod.QrLocY.Close;
        Dmod.QrLocY.SQL.Clear;
        Dmod.QrLocY.SQL.Add('SELECT Prazo Record');
        Dmod.QrLocY.SQL.Add('FROM carteiras');
        Dmod.QrLocY.SQL.Add('WHERE Codigo=' + IntToStr(FLAN_Carteira));
        UnDmkDAC_PF.AbreQuery(Dmod.QrLocY, Dmod.MyDB);
        //
        if Dmod.QrLocYRecord.Value = 0 then
          FLAN_Compensado := FLAN_Data;
      end else
        FLAN_Compensado := FLAN_Data;
    end;
  end;
  if FLAN_VERIFICA_CLIINT then
  begin
    if (FLAN_CliInt < 1) and (FLAN_CliInt > -11) then
    begin
      Dmod.QrLocY.Database := Dmod.MyDB;
      Dmod.QrLocY.Close;
      Dmod.QrLocY.SQL.Clear;
      Dmod.QrLocY.SQL.Add('SELECT ForneceI Record');
      Dmod.QrLocY.SQL.Add('FROM carteiras');
      Dmod.QrLocY.SQL.Add('WHERE Codigo=' + IntToStr(FLAN_Carteira));
      UnDmkDAC_PF.AbreQuery(Dmod.QrLocY, Dmod.MyDB);
      if Dmod.QrLocYRecord.Value <> FLAN_CliInt then
      begin
        if VAR_CliIntUnico <> 0 then
          FLAN_CliInt := VAR_CliIntUnico
        else begin
          if Dmod.QrLocYRecord.Value = 0 then
          begin
            Geral.MB_Aviso('CUIDADO!! Cliente interno n�o definido ' +
            'para o lan�amento ' + FormatFloat('#,###,###,###', FLAN_Controle) +
            '. A carteira selecionada (n� ' + IntToStr(FLAN_Carteira) +
            ') tamb�m n�o possui cliente interno!' + sLineBreak + 'AVISE A DERMATEK');
          end else begin
            FLAN_CliInt := Dmod.QrLocYRecord.Value;
            Geral.MB_Aviso('CUIDADO!! Cliente interno n�o definido ' +
            'para o lan�amento ' + FormatFloat('#,###,###,###', FLAN_Controle) +
            '. Ser� considerado o cliente interno n� ' + IntToStr(FLAN_CliInt) +
            ' que � dono da carteira informada (n� ' + IntToStr(FLAN_Carteira) +
            ')!' + sLineBreak + 'AVISE A DERMATEK');
          end;
        end;
      end;
    end;
  end;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO ' + TabLctA + ' SET Documento=:P0, Data=:P1, ');
  Dmod.QrUpdM.SQL.Add('Tipo=:P2, Carteira=:P3, Credito=:P4, Debito=:P5, ');
  Dmod.QrUpdM.SQL.Add('Genero=:P6, NotaFiscal=:P7, Vencimento=:P8, Mez=:P9, ');
  Dmod.QrUpdM.SQL.Add('Descricao=:P10, Sit=:P11, Controle=:P12, Cartao=:P13, ');
  Dmod.QrUpdM.SQL.Add('Compensado=:P14, Linha=:P15, Fornecedor=:P16, ');
  Dmod.QrUpdM.SQL.Add('Cliente=:P17, MoraDia=:P18, Multa=:P19, DataCad=:P20, ');
  Dmod.QrUpdM.SQL.Add('UserCad=:P21, DataDoc=:P22, Vendedor=:P23, ');
  Dmod.QrUpdM.SQL.Add('Account=:P24, FatID=:P25, FatID_Sub=:P26, ');
  Dmod.QrUpdM.SQL.Add('ICMS_P=:P27, ICMS_V=:P28, Duplicata=:P29, ');
  Dmod.QrUpdM.SQL.Add('CliInt=:P30, Depto=:P31, DescoPor=:P32, ');
  Dmod.QrUpdM.SQL.Add('ForneceI=:P33, DescoVal=:P34, NFVal=:P35, ');
  Dmod.QrUpdM.SQL.Add('Unidade=:P36, Qtde=:P37, FatNum=:P38, ');
  Dmod.QrUpdM.SQL.Add('FatParcela=:P39, Doc2=:P40, SerieCH=:P41, ');
  Dmod.QrUpdM.SQL.Add('MultaVal=:P42, MoraVal=:P43, CtrlIni=:P44, ');
  Dmod.QrUpdM.SQL.Add('Nivel=:P45, ID_Pgto=:P46, CNAB_Sit=:P47, ');
  Dmod.QrUpdM.SQL.Add('TipoCH=:P48, Atrelado=:P49, Sub=:P50, ');
  Dmod.QrUpdM.SQL.Add('Emitente=:P51, CNPJCPF=:P52, Banco=:P53, ');
  Dmod.QrUpdM.SQL.Add('Agencia=:P54, ContaCorrente=:P55, SubPgto1=:P56, ');
  Dmod.QrUpdM.SQL.Add('MultiPgto=:P57, SerieNF=:P58, EventosCad=:P59, ');
  Dmod.QrUpdM.SQL.Add('IndiPag=:P60, FatParcRef=:P61, FatGrupo=:P62, ');
  Dmod.QrUpdM.SQL.Add('TaxasVal=:P63, Antigo=:P64, ');
  Dmod.QrUpdM.SQL.Add('FisicoSrc=:P65, FisicoCod=:P66, CentroCusto=:67, ');
  Dmod.QrUpdM.SQL.Add('Qtd2=:P68, VctoOriginal=:P69, ModeloNF=:P70, ');
  Dmod.QrUpdM.SQL.Add('GenCtb=:P71, GenCtbD=:P72, GenCtbC=:P73');
  //
  Dmod.QrUpdM.Params[00].AsFloat   := FLAN_Documento;
  Dmod.QrUpdM.Params[01].AsString  := FLAN_Data;
  Dmod.QrUpdM.Params[02].AsInteger := FLAN_Tipo;
  Dmod.QrUpdM.Params[03].AsInteger := FLAN_Carteira;
  Dmod.QrUpdM.Params[04].AsFloat   := FLAN_Credito;
  Dmod.QrUpdM.Params[05].AsFloat   := FLAN_Debito;
  Dmod.QrUpdM.Params[06].AsInteger := FLAN_Genero;
  Dmod.QrUpdM.Params[07].AsInteger := FLAN_NotaFiscal;
  Dmod.QrUpdM.Params[08].AsString  := FLAN_Vencimento;
  Dmod.QrUpdM.Params[09].AsInteger := FLAN_Mez;
  Dmod.QrUpdM.Params[10].AsString  := FLAN_Descricao;
  Dmod.QrUpdM.Params[11].AsInteger := FLAN_Sit;
  Dmod.QrUpdM.Params[12].AsFloat   := FLAN_Controle;
  Dmod.QrUpdM.Params[13].AsInteger := FLAN_Cartao;
  Dmod.QrUpdM.Params[14].AsString  := FLAN_Compensado;
  Dmod.QrUpdM.Params[15].AsInteger := FLAN_Linha;
  Dmod.QrUpdM.Params[16].AsInteger := FLAN_Fornecedor;
  Dmod.QrUpdM.Params[17].AsInteger := FLAN_Cliente;
  Dmod.QrUpdM.Params[18].AsFloat   := FLAN_MoraDia;
  Dmod.QrUpdM.Params[19].AsFloat   := FLAN_Multa;
  Dmod.QrUpdM.Params[20].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdM.Params[21].AsInteger := VAR_USUARIO;
  Dmod.QrUpdM.Params[22].AsString  := FLAN_DataDoc;
  Dmod.QrUpdM.Params[23].AsInteger := FLAN_Vendedor;
  Dmod.QrUpdM.Params[24].AsInteger := FLAN_Account;
  Dmod.QrUpdM.Params[25].AsInteger := FLAN_FatID;
  Dmod.QrUpdM.Params[26].AsInteger := FLAN_FatID_Sub;
  Dmod.QrUpdM.Params[27].AsFloat   := FLAN_ICMS_P;
  Dmod.QrUpdM.Params[28].AsFloat   := FLAN_ICMS_V;
  Dmod.QrUpdM.Params[29].AsString  := FLAN_Duplicata;
  Dmod.QrUpdM.Params[30].AsInteger := FLAN_CliInt;
  Dmod.QrUpdM.Params[31].AsInteger := FLAN_Depto;
  Dmod.QrUpdM.Params[32].AsInteger := FLAN_DescoPor;
  Dmod.QrUpdM.Params[33].AsInteger := FLAN_ForneceI;
  Dmod.QrUpdM.Params[34].AsFloat   := FLAN_DescoVal;
  Dmod.QrUpdM.Params[35].AsFloat   := FLAN_NFVal;
  Dmod.QrUpdM.Params[36].AsInteger := FLAN_Unidade;
  Dmod.QrUpdM.Params[37].AsFloat   := FLAN_Qtde;
  Dmod.QrUpdM.Params[38].AsFloat   := FLAN_FatNum;
  Dmod.QrUpdM.Params[39].AsInteger := FLAN_FatParcela;
  Dmod.QrUpdM.Params[40].AsString  := FLAN_Doc2;
  Dmod.QrUpdM.Params[41].AsString  := FLAN_SerieCH;
  Dmod.QrUpdM.Params[42].AsFloat   := FLAN_MultaVal;
  Dmod.QrUpdM.Params[43].AsFloat   := FLAN_MoraVal;
  Dmod.QrUpdM.Params[44].AsInteger := FLAN_CtrlIni;
  Dmod.QrUpdM.Params[45].AsInteger := FLAN_Nivel;
  Dmod.QrUpdM.Params[46].AsFloat   := FLAN_ID_Pgto;
  Dmod.QrUpdM.Params[47].AsInteger := FLAN_CNAB_Sit;
  Dmod.QrUpdM.Params[48].AsInteger := FLAN_TipoCH;
  Dmod.QrUpdM.Params[49].AsInteger := FLAN_Atrelado;
  Dmod.QrUpdM.Params[50].AsInteger := FLAN_Sub;
  Dmod.QrUpdM.Params[51].AsString  := FLAN_Emitente;
  Dmod.QrUpdM.Params[52].AsString  := FLAN_CNPJCPF;
  Dmod.QrUpdM.Params[53].AsInteger := FLAN_Banco;
  Dmod.QrUpdM.Params[54].AsInteger := FLAN_Agencia;
  Dmod.QrUpdM.Params[55].AsString  := FLAN_ContaCorrente;
  Dmod.QrUpdM.Params[56].AsInteger := FLAN_SubPgto1;
  Dmod.QrUpdM.Params[57].AsInteger := FLAN_MultiPgto;
  Dmod.QrUpdM.Params[58].AsString  := FLAN_SerieNF;
  Dmod.QrUpdM.Params[59].AsInteger := FLAN_EventosCad;
  Dmod.QrUpdM.Params[60].AsInteger := FLAN_IndiPag;
  Dmod.QrUpdM.Params[61].AsInteger := FLAN_FatParcRef;
  Dmod.QrUpdM.Params[62].AsInteger := FLAN_FatGrupo;
  Dmod.QrUpdM.Params[63].AsFloat   := FLAN_TaxasVal;
  Dmod.QrUpdM.Params[64].AsString  := FLAN_Antigo;
  Dmod.QrUpdM.Params[65].AsInteger := FLAN_FisicoSrc;
  Dmod.QrUpdM.Params[66].AsInteger := FLAN_FisicoCod;
  Dmod.QrUpdM.Params[67].AsInteger := FLAN_CentroCusto;
  Dmod.QrUpdM.Params[68].AsFloat   := FLAN_Qtd2;
  Dmod.QrUpdM.Params[69].AsString  := FLAN_VctoOriginal;
  Dmod.QrUpdM.Params[70].AsString  := FLAN_ModeloNF;
  Dmod.QrUpdM.Params[71].AsInteger := FLAN_GenCtb;
  Dmod.QrUpdM.Params[72].AsInteger := FLAN_GenCtbD;
  Dmod.QrUpdM.Params[73].AsInteger := FLAN_GenCtbC;

  Dmod.QrUpdM.ExecSQL;
  Result := True;
  //
  if FLAN_ID_Pgto <> 0 then
    AtualizaEmissaoMasterRapida(FLAN_ID_Pgto, FLAN_Sit, FLAN_Controle,
      FLAN_Data, TabLctA);
  Application.ProcessMessages;
end;

procedure TUnFinanceiro.LancamentoDefaultVARS;
begin
  // N�o permite lan�amento para outro cliint que n�o seja o dono da carteira!
  FLAN_VERIFICA_CLIINT := True;
  //
  FLAN_Data          := '0000-00-00';
  FLAN_Vencimento    := Geral.FDT(Date, 1);
  FLAN_DataCad       := Geral.FDT(Date, 1);
  FLAN_Mez           := 0;
  FLAN_Descricao     := '';
  FLAN_Compensado    := '0000-00-00';
  FLAN_Duplicata     := '';
  FLAN_Doc2          := '';
  FLAN_SerieCH       := '';

  FLAN_Documento     := 0;
  FLAN_Tipo          := -1;
  FLAN_Carteira      := -1;
  FLAN_Credito       := 0;
  FLAN_Debito        := 0;
  FLAN_Genero        := -1000;
  FLAN_GenCtb        := -1000;
  FLAN_GenCtbD       := -1000;
  FLAN_GenCtbC       := -1000;
  FLAN_SerieNF       := '';
  FLAN_Antigo        := '';
  FLAN_NotaFiscal    := 0;
  FLAN_Sit           := 0;
  FLAN_Controle      := 0;
  FLAN_Sub           := 0;
  FLAN_ID_Pgto       := 0;
  FLAN_Cartao        := 0;
  FLAN_Linha         := 0;
  FLAN_Fornecedor    := 0;
  FLAN_Cliente       := 0;
  FLAN_MoraDia       := 0;
  FLAN_Multa         := 0;
  FLAN_UserCad       := VAR_USUARIO;
  FLAN_DataDoc       := Geral.FDT(Date, 1);
  FLAN_Vendedor      := 0;
  FLAN_Account       := 0;
  FLAN_CentroCusto   := 0;
  FLAN_ICMS_P        := 0;
  FLAN_ICMS_V        := 0;
  FLAN_CliInt        := VAR_CliIntUnico;
  FLAN_Depto         := 0;
  FLAN_DescoPor      := 0;
  FLAN_ForneceI      := 0;
  FLAN_DescoVal      := 0;
  FLAN_NFVal         := 0;
  FLAN_Unidade       := 0;
  FLAN_Qtde          := 0;
  FLAN_Qtd2          := 0;
  FLAN_FatID         := 0;
  FLAN_FatID_Sub     := 0;
  FLAN_FatNum        := 0;
  FLAN_FatParcela    := 0;
  FLAN_FatParcRef    := 0;
  FLAN_FatGrupo      := 0;
  //
  FLAN_MultaVal      := 0;
  FLAN_TaxasVal      := 0;
  FLAN_MoraVal       := 0;
  FLAN_CtrlIni       := 0;
  FLAN_Nivel         := 0;
  FLAN_CNAB_Sit      := 0;
  FLAN_TipoCH        := 0;
  FLAN_Atrelado      := 0;
  FLAN_SubPgto1      := 0;
  FLAN_MultiPgto     := 0;
  FLAN_EventosCad    := 0;
  FLAN_IndiPag       := 0;
  FLAN_FisicoSrc     := 0;
  FLAN_FisicoCod     := 0;
  //
  FLAN_Emitente      := '';
  FLAN_CNPJCPF       := '';
  FLAN_Banco         := 0;
  FLAN_Agencia       := 0;
  FLAN_ContaCorrente := '';
  FLAN_VctoOriginal  := '0000-00-00';
  FLAN_ModeloNF      := '';
end;

procedure TUnFinanceiro.LancamentoDuplica(QrAux: TmySQLQuery; Controle, Sub:
Integer; TabLctX: String);
begin
  UFinanceiro.LancamentoDefaultVARS;
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SELECT * FROM ' + TabLctX + ' ');
  QrAux.SQL.Add('WHERE Controle=' + FormatFloat('0', Controle));
  QrAux.SQL.Add('AND SUB=' + FormatFloat('0', Sub));
  UnDmkDAC_PF.AbreQuery(QrAux, Dmod.MyDB);
  //
  FLAN_Data          := Geral.FDT(QrAux.FieldByName('Data'      ).AsDateTime, 1);
  FLAN_Vencimento    := Geral.FDT(QrAux.FieldByName('Vencimento').AsDateTime, 1);
  FLAN_DataCad       := Geral.FDT(QrAux.FieldByName('DataCad'   ).AsDateTime, 1);
  //
  FLAN_Mez           := QrAux.FieldByName('Mez').AsInteger;
  //
  FLAN_Descricao     := QrAux.FieldByName('Descricao' ).AsString;
  FLAN_Compensado    := Geral.FDT(QrAux.FieldByName('Compensado').AsDateTime, 1);
  FLAN_Duplicata     := QrAux.FieldByName('Duplicata' ).AsString;
  FLAN_Doc2          := QrAux.FieldByName('Doc2'      ).AsString;
  FLAN_SerieCH       := QrAux.FieldByName('SerieCH'   ).AsString;

  FLAN_Documento     := QrAux.FieldByName('Documento' ).AsInteger;
  FLAN_Tipo          := QrAux.FieldByName('Tipo'      ).AsInteger;
  FLAN_Carteira      := QrAux.FieldByName('Carteira'  ).AsInteger;
  FLAN_Credito       := QrAux.FieldByName('Credito'   ).AsFloat;
  FLAN_Debito        := QrAux.FieldByName('Debito'    ).AsFloat;
  FLAN_Genero        := QrAux.FieldByName('Genero'    ).AsInteger;
  FLAN_GenCtb        := QrAux.FieldByName('GenCtb'    ).AsInteger;
  FLAN_GenCtbD       := QrAux.FieldByName('GenCtbD'   ).AsInteger;
  FLAN_GenCtbC       := QrAux.FieldByName('GenCtbC'   ).AsInteger;
  FLAN_SerieNF       := QrAux.FieldByName('SerieNF'   ).AsString;
  //FLAN_Antigo        := '';
  FLAN_NotaFiscal    := QrAux.FieldByName('NotaFiscal').AsInteger;
  FLAN_Sit           := QrAux.FieldByName('Sit'       ).AsInteger;
  FLAN_Controle      := QrAux.FieldByName('Controle'  ).AsInteger;
  FLAN_Sub           := QrAux.FieldByName('Sub'       ).AsInteger;
  FLAN_ID_Pgto       := QrAux.FieldByName('ID_Pgto'   ).AsInteger;
  FLAN_Cartao        := QrAux.FieldByName('Cartao'    ).AsInteger;
  FLAN_Linha         := QrAux.FieldByName('Linha'     ).AsInteger;
  FLAN_Fornecedor    := QrAux.FieldByName('Fornecedor').AsInteger;
  FLAN_Cliente       := QrAux.FieldByName('Cliente'   ).AsInteger;
  FLAN_MoraDia       := QrAux.FieldByName('MoraDia'   ).AsFloat;
  FLAN_Multa         := QrAux.FieldByName('Multa'     ).AsFloat;
  FLAN_UserCad       := QrAux.FieldByName('UserCad'   ).AsInteger;
  FLAN_DataDoc       := Geral.FDT(QrAux.FieldByName('DataDoc'   ).AsDateTime, 1);
  FLAN_Vendedor      := QrAux.FieldByName('Vendedor'   ).AsInteger;
  FLAN_Account       := QrAux.FieldByName('Account'    ).AsInteger;
  FLAN_CentroCusto   := QrAux.FieldByName('CentroCusto').AsInteger;
  FLAN_ICMS_P        := QrAux.FieldByName('ICMS_P'     ).AsFloat;
  FLAN_ICMS_V        := QrAux.FieldByName('ICMS_V'     ).AsFloat;
  FLAN_CliInt        := QrAux.FieldByName('CliInt'     ).AsInteger;
  FLAN_Depto         := QrAux.FieldByName('Depto'      ).AsInteger;
  FLAN_DescoPor      := QrAux.FieldByName('DescoPor'   ).AsInteger;
  FLAN_ForneceI      := QrAux.FieldByName('ForneceI'   ).AsInteger;
  FLAN_DescoVal      := QrAux.FieldByName('DescoVal'   ).AsFloat;
  FLAN_NFVal         := QrAux.FieldByName('NFVal'      ).AsFloat;
  FLAN_Unidade       := QrAux.FieldByName('Unidade'    ).AsInteger;
  FLAN_Qtde          := QrAux.FieldByName('Qtde'       ).AsFloat;
  FLAN_Qtd2          := QrAux.FieldByName('Qtd2'       ).AsFloat;
  FLAN_FatID         := QrAux.FieldByName('FatID'      ).AsInteger;
  FLAN_FatID_Sub     := QrAux.FieldByName('FatID_Sub'  ).AsInteger;
  FLAN_FatNum        := QrAux.FieldByName('FatNum'     ).AsInteger;
  FLAN_FatParcela    := QrAux.FieldByName('FatParcela' ).AsInteger;
  FLAN_FatParcRef    := QrAux.FieldByName('FatParcRef' ).AsInteger;
  FLAN_FatGrupo      := QrAux.FieldByName('FatGrupo'   ).AsInteger;
  //                    //
  FLAN_MultaVal      := QrAux.FieldByName('MultaVal'  ).AsFloat;
  FLAN_TaxasVal      := QrAux.FieldByName('TaxasVal'  ).AsFloat;
  FLAN_MoraVal       := QrAux.FieldByName('MoraVal'   ).AsFloat;
  FLAN_CtrlIni       := QrAux.FieldByName('CtrlIni'   ).AsInteger;
  FLAN_Nivel         := QrAux.FieldByName('Nivel'     ).AsInteger;
  FLAN_CNAB_Sit      := QrAux.FieldByName('CNAB_Sit'  ).AsInteger;
  FLAN_TipoCH        := QrAux.FieldByName('TipoCH'    ).AsInteger;
  FLAN_Atrelado      := QrAux.FieldByName('Atrelado'  ).AsInteger;
  //                    //
  FLAN_Emitente      := QrAux.FieldByName('Emitente'  ).AsString;
  FLAN_CNPJCPF       := QrAux.FieldByName('CNPJCPF'   ).AsString;
  FLAN_Banco         := QrAux.FieldByName('Banco'     ).AsInteger;
  FLAN_Agencia       := QrAux.FieldByName('Agencia'   ).AsInteger;
  FLAN_ContaCorrente := QrAux.FieldByName('ContaCorrente').AsString;
  FLAN_SubPgto1      := QrAux.FieldByName('SubPgto1'     ).AsInteger;
  FLAN_MultiPgto     := QrAux.FieldByName('MultiPgto'    ).AsInteger;
  FLAN_EventosCad    := QrAux.FieldByName('EventosCad'   ).AsInteger;
  FLAN_IndiPag       := QrAux.FieldByName('IndiPag'      ).AsInteger;
  FLAN_FisicoSrc     := QrAux.FieldByName('FisicoSrc'    ).AsInteger;
  FLAN_FisicoCod     := QrAux.FieldByName('FisicoCod'    ).AsInteger;
  //
  FLAN_VctoOriginal  := Geral.FDT(QrAux.FieldByName('VctoOriginal'   ).AsDateTime, 1);
  FLAN_ModeloNF      := QrAux.FieldByName('ModeloNF'  ).AsString;
  //
end;

function TUnFinanceiro.ListaDeCRT(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 16;
      Grade.ColWidths[01] := 44;
      Grade.ColWidths[02] := 715;
      Width := 800;
      FColSel := 1;
      Caption := 'SEL-LISTA-000 :: CRT';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := CRT_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeCSOSN(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 4;
      Grade.ColWidths[00] := 16;
      Grade.ColWidths[01] := 16;
      Grade.ColWidths[02] := 44;
      Grade.ColWidths[03] := 800;
      Width := 1024;
      FColSel := 2;
      Caption := 'SEL-LISTA-000 :: CSOSN do CRT=1';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[00,00] := 'CRT';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := CSOSN_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
        Grade.Cells[3, j] := Lista[i][2];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeCST_COFINS: String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'SEL-LISTA-000 :: CST COFINS';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := CST_COFINS_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeCST_IPI(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'SEL-LISTA-000 :: CST IPI';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := UFinanceiro.CST_IPI_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeCST_PIS(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'SEL-LISTA-000 :: CST PIS';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := CST_PIS_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

(*function TUnFinanceiro.ListaDeIndProc(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 16;
      Grade.ColWidths[01] := 44;
      Grade.ColWidths[02] := 715;
      Width := 800;
      FColSel := 1;
      Caption := 'SEL-LISTA-000 :: Origem de Processo';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := IndProc_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;*)

function TUnFinanceiro.ListaDeSituacaoTributaria(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'SEL-LISTA-000 :: Situa��o Tribut�ria';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := UFinanceiro.CST_A_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeSPED_EFD_IND_ATIV(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'SEL-LISTA-000 :: Indicador de tipo de atividade';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := UFinanceiro.SPED_EFD_IND_ATIV_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeSPED_EFD_IND_PERFIL(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'SEL-LISTA-000 :: Perfil de apresenta��o do arquivo fiscal';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := UFinanceiro.SPED_EFD_IND_PERFIL_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeSPED_Tipo_Item(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'SEL-LISTA-000 :: Tipo de Item';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := UFinanceiro.SPED_Tipo_Item_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeTributacaoPeloICMS(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'GER-SELEC-001 :: Tributa��o pelo ICMS';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := CST_B_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

{###
procedure TUnFinanceiro.ConsertaLctQuitados();
var
  Atz: Integer;
begin
  Screen.Cursor := crHourGlass;
  Atz := 0;
  DmodFin.QrErrQuit.Close;
  UnDmkDAC_PF.AbreQuery(DmodFin.QrErrQuit, Dmod.MyDB);
  if DmodFin.QrErrQuit.RecordCount > 0 then
  begin
    (*
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET ID_Quit=:P0, Compensado=:P1 ');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=0');
    Dmod.QrUpd.SQL.Add('');
    *)
    while not DmodFin.QrErrQuit.Eof do
    begin
      Atz := Atz + 1;
      (*
      Dmod.QrUpd.Params[00].AsInteger := DmodFin.QrErrQuitControle2.Value;
      Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DmodFin.QrErrQuitData.Value, 1);
      //
      Dmod.QrUpd.Params[02].AsInteger := DmodFin.QrErrQuitControle1.Value;
      Dmod.QrUpd.ExecSQL;
      //
      DmodFin.QrErrQuit.Next;
      *)
      UmyMod.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'ID_Quit', 'Compensado'], ['Controle'], [DmodFin.QrErrQuitControle2.Value,
      Geral.FDT(DmodFin.QrErrQuitData.Value, 1)], [
      DmodFin.QrErrQuitControle1.Value], True, '');
    end;
  end;
  Screen.Cursor := crDefault;
  Geral.MB_Aviso('Foram atualizados ' + IntToStr(Atz) +
  ' registros de ' + IntToStr(DmodFin.QrErrQuit.RecordCount));
end;
}

procedure TUnFinanceiro.AtualizaVencimentos(TabLctA: String);
begin
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE ' + TabLctA + ' SET Vencimento=Data');
  Dmod.QrUpdM.SQL.Add('WHERE (Vencimento+Data)=Data');
  Dmod.QrUpdM.ExecSQL;
end;

procedure TUnFinanceiro.ReverterPagtoEmissao(QrLct, QrCrt: TmySQLQuery;
 Pergunta, LocLancto, LocCarteira: Boolean; TabLctA: String);
var
  Reverte: Boolean;
  Controle: Double;
  //, Tipo, Carteira
  Continua, Genero, Sub, CartLoc: Integer;
  //Data: TDateTime;
begin
{###  Parei Aqui! ver certo como fazer pois o pagamento pode estar no B ou D?}
{
  Data     := QrLct.FieldByName('Data').AsInteger;
  Tipo     := QrLct.FieldByName('Tipo').AsInteger;
  Carteira := QrLct.FieldByName('Carteira').AsInteger;
}
  Controle := QrLct.FieldByName('Controle').AsInteger;
  Sub      := QrLct.FieldByName('Sub').AsInteger;
  Genero   := QrLct.FieldByName('Genero').AsInteger;
  CartLoc  := QrCrt.FieldByName('Codigo').AsInteger;
  Reverte  := False;
  if not Pergunta then Continua := ID_YES else Continua :=
    Geral.MB_Pergunta('Confirma a revers�o da compensa��o?');
  if Continua = ID_YES then
  begin
    DmodFin.QrLocLct.Close;
    DmodFin.QrLocLct.SQL.Clear;
    DmodFin.QrLocLct.SQL.Add('SELECT * FROM ' + TabLctA + '');
    DmodFin.QrLocLct.SQL.Add('WHERE ID_Pgto=:P0 AND Sub=:P1');//para achar em qualquer carteira
    DmodFin.QrLocLct.Params[0].AsFloat := Controle;
    DmodFin.QrLocLct.Params[1].AsInteger := Sub;
    UnDmkDAC_PF.AbreQuery(DmodFin.QrLocLct, Dmod.MyDB);
    if DmodFin.QrLocLct.RecordCount = 0 then
      Geral.MB_Aviso('AVISO. N�o foi localizado nenhum registro.');
    if DmodFin.QrLocLct.RecordCount in [0, 1] then Reverte := True;
    if DmodFin.QrLocLct.RecordCount > 1 then
    begin
      if Geral.MB_Pergunta(
      'Foi localizada mais de uma compensa��o. Todas ser�o excluidas. Deseja continuar?') =
      ID_YES then Reverte := True else
      begin
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE ' + TabLctA + ' SET Sit=2, Genero=:P0 ');
        Dmod.QrUpdM.SQL.Add('WHERE (ID_Pgto=:P1 OR Controle=:P2) AND Sub=:P3');
        Dmod.QrUpdM.Params[00].AsInteger := Genero;
        Dmod.QrUpdM.Params[01].AsFloat   := Controle;
        Dmod.QrUpdM.Params[02].AsFloat   := Controle;
        Dmod.QrUpdM.Params[03].AsInteger := Sub;
        Dmod.QrUpdM.ExecSQL;
        Geral.MB_Aviso('Esta compensa��o foi convertida para quita��o. Assim � poss�vel edit�-la.');
      end;
    end;
    //
    if Reverte then
    begin
{
      if Controle <> 0 then
      begin
        Dmod.QrUpdM.SQL.Clear;                      // Tipo=1???
        Dmod.QrUpdM.SQL.Add(EXCLUI_DE + TabLctA + ' WHERE ID_Pgto=:P0');
        Dmod.QrUpdM.Params[0].AsFloat := Controle;
        Dmod.QrUpdM.ExecSQL;
      end else Geral.MB_Aviso('ID_Pgto a ser exclu�do n�o pode ser zero!',
      );
}
      DmodFin.QrLocLct.First;
      while not DmodFin.QrLocLct.Eof do
      begin
        ExcluiLct_Unico(TabLctA, Dmod.QrUpdM.Database, DmodFin.QrLocLctData.Value,
          DmodFin.QrLocLctTipo.Value, DmodFin.QrLocLctCarteira.Value,
          DmodFin.QrLocLctControle.Value, DmodFin.QrLocLctSub.Value,
          dmkPF.MotivDel_ValidaCodigo(200), False);
        UFinanceiro.RecalcSaldoCarteira(DmodFin.QrLocLctCarteira.Value,
          nil, nil, False, False);
        DmodFin.QrLocLct.Next;
      end;
      SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Compensado', 'Sit', 'Genero'], ['Controle', 'Sub'], [
        '0000-00-00', 0, Genero], [Controle, Sub], True, '', TabLctA);
      if Pergunta then
        UFinanceiro.RecalcSaldoCarteira(
          QrCrt.FieldByName('Codigo').AsInteger, nil, nil, False, False)
      else
        UFinanceiro.RecalcSaldoCarteira(
          QrCrt.FieldByName('Codigo').AsInteger, nil, nil, False, False);
    end;
  end;
  AtualizaEmissaoMasterExtra_Novo(QrLct.FieldByName('Controle').AsInteger,
    QrLct, nil, nil, nil, nil, '', TabLctA);
  if LocCarteira then
    QrCrt.Locate('Codigo', CartLoc, []);
  if LocLancto then
    QrLct.Locate('Controle', Controle, []);
end;

function TUnFinanceiro.SaldoAqui(Tipo: Integer; Atual: String;
QrLct, QrCrt: TmySQLQuery): String;
var
  Saldo: Double;
  TabLctA, Data, Real: String;
begin
  case Tipo of
    0: Result := '';
    1:
    begin
      Data := FormatDateTime(VAR_FORMATDATE, QrLct.FieldByName('Data').AsDateTime);
      TabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, QrCrt.FieldByName('CliInt').AsInteger);
      //
      DmodFin.QrSaldo.Close;
      DmodFin.QrSaldo.SQL.Clear;
      DmodFin.QrSaldo.SQL.Add('SELECT SUM(Credito-Debito) Valor FROM ' + TabLctA + '');
      DmodFin.QrSaldo.SQL.Add('WHERE Tipo=:P0 AND Carteira=:P1');
      DmodFin.QrSaldo.SQL.Add('AND (Data<:P2 OR (Data=:P3 AND Controle<=:P4))');
      DmodFin.QrSaldo.Params[00].AsInteger := QrLct.FieldByName('Tipo').AsInteger;
      DmodFin.QrSaldo.Params[01].AsInteger := QrLct.FieldByName('Carteira').AsInteger;
      DmodFin.QrSaldo.Params[02].AsString  := Data;
      DmodFin.QrSaldo.Params[03].AsString  := Data;
      DmodFin.QrSaldo.Params[04].AsFloat := QrLct.FieldByName('Controle').AsInteger;
      UnDmkDAC_PF.AbreQuery(DmodFin.QrSaldo, Dmod.MyDB);
      //
      Saldo := DmodFin.QrSaldoValor.Value + QrCrt.FieldByName('SdoFimB').AsFloat;
      //
      DmodFin.QrSaldo.Close;
      Result := Geral.TFT(FloatToStr(Saldo), 2, siNegativo);
    end;
    2:
    begin
      Real := InputBox(Application.Title, 'Informe o saldo real:', CO_VAZIO);
      Saldo := Geral.DMV(Real);
      Saldo := Saldo - Geral.DMV(Atual);
      Geral.MB_Aviso(Geral.TFT(Real, 2, siNegativo)+' - '+Atual+' = '+
        Geral.TFT(FloatToStr(Saldo), 2, siNegativo));
    end;
    else Geral.MB_Erro('Tipo de a��o n�o definido!');
  end;
end;

procedure TUnFinanceiro.SomaLinhas_2(DBGLct: TDBGrid; QrCrt, QrLct: TmySQLQuery;
EdCred, EdDebi, EdSoma: TdmkEdit);
var
  Debito, Credito: Double;
  i: Integer;
begin
  Debito   := 0;
  Credito  := 0;
  if DBGLct <> nil then
  begin
    with DBGLct.DataSource.DataSet do
    for i:= 0 to DBGLct.SelectedRows.Count-1 do
    begin
      //GotoBookmark(DBGLct.SelectedRows.Items[i]);
      GotoBookmark(DBGLct.SelectedRows.Items[i]);
      Debito := Debito + QrLct.FieldByName('Debito').AsFloat;
      Credito := Credito + QrLct.FieldByName('Credito').AsFloat;
    end;
  {
  end else
  if DBGLct <> nil then
  begin
    with DBGLct.DataSource.DataSet do
    for i:= 0 to DBGLct.SelectedRows.Count-1 do
    begin
      GotoBookmark(DBGLct.SelectedRows.Items[i]);
      Debito := Debito + QrLct.FieldByName('Debito').AsFloat;
      Credito := Credito + QrLct.FieldByName('Credito').AsFloat;
    end;
  }
  end;
  EdSoma.ValueVariant := Credito-Debito;
  EdDebi.ValueVariant := Debito;
  EdCred.ValueVariant := Credito;
end;

procedure TUnFinanceiro.SomaLinhas_3(DBGLct: TDBGrid; QrCrt, QrLct: TmySQLQuery;
  EdCred, EdDebi, EdSoma: TdmkEdit);
var
  Debito, Credito: Double;
  I: Integer;
begin
  DBGLct.DataSource.DataSet.DisableControls;
  try
    Debito   := 0;
    Credito  := 0;
    if DBGLct <> nil then
    begin
      if DBGLct.SelectedRows.Count > 0 then
      begin
        with DBGLct.DataSource.DataSet do
        for I := 0 to DBGLct.SelectedRows.Count-1 do
        begin
          GotoBookmark(DBGLct.SelectedRows.Items[i]);
          Debito := Debito + QrLct.FieldByName('Debito').AsFloat;
          Credito := Credito + QrLct.FieldByName('Credito').AsFloat;
        end;
      end
      else
      begin
        DBGLct.DataSource.DataSet.First;
        while not DBGLct.DataSource.DataSet.Eof do
        begin
          Debito := Debito + QrLct.FieldByName('Debito').AsFloat;
          Credito := Credito + QrLct.FieldByName('Credito').AsFloat;
          //
          DBGLct.DataSource.DataSet.Next;
        end;
      end;
    end;
  finally
    DBGLct.DataSource.DataSet.EnableControls;
  end;
  EdSoma.ValueVariant := Credito-Debito;
  EdDebi.ValueVariant := Debito;
  EdCred.ValueVariant := Credito;
end;

function TUnFinanceiro.SPED_EFD_IND_ATIV_Get(Codigo: Integer): String;
begin
// Indicador de tipo de atividade:
  case Codigo of
    00: Result := 'Industrial ou equiparado a industrial';
    01: Result := 'Outros';
    else Result := '?';
  end;
end;

function TUnFinanceiro.SPED_EFD_IND_ATIV_Lista(): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA IND_ATIV
  AddLinha(Result, Linha, '0', 'Industrial ou equiparado a industrial');
  AddLinha(Result, Linha, '1', 'Outros');
end;

function TUnFinanceiro.SPED_EFD_IND_PERFIL_Get(Codigo: String): String;
begin
// Indicador de tipo de atividade:
  if Uppercase(Codigo) = 'A' then Result := 'Perfil A' else
  if Uppercase(Codigo) = 'B' then Result := 'Perfil B' else
  if Uppercase(Codigo) = 'C' then Result := 'Perfil C' else
  Result := '?';
end;

function TUnFinanceiro.SPED_EFD_IND_PERFIL_Lista(): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA IND_PERFIL
  AddLinha(Result, Linha, 'A', 'Perfil A');
  AddLinha(Result, Linha, 'B', 'Perfil B');
  AddLinha(Result, Linha, 'C', 'Perfil C');
end;

function TUnFinanceiro.ListaDeIndProc(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 16;
      Grade.ColWidths[01] := 44;
      Grade.ColWidths[02] := 715;
      Width := 800;
      FColSel := 1;
      Caption := 'SEL-LISTA-000 :: Origem de Processo';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := IndProc_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.SPED_Tipo_Item_Get(Codigo: Integer): String;
begin
// Tipo de Item
  case Codigo of
    -2: Result := 'Sem controle fiscal (texto, etc)';
    -1: Result := 'Indefinido!';
    00: Result := 'Mercadoria para revenda';
    01: Result := 'Mat�ria-Prima';
    02: Result := 'Embalagem';
    03: Result := 'Produto em Processo';
    04: Result := 'Produto Acabado';
    05: Result := 'Subproduto';
    06: Result := 'Produto intermedi�rio';
    07: Result := 'Material de Uso e Consumo';
    08: Result := 'Ativo Imobilizado';
    09: Result := 'Servi�os';
    10: Result := 'Outros Insumos';
    99: Result := 'Outras';
    else Result := '';
  end;
end;

function TUnFinanceiro.SPED_Tipo_Item_Lista: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA IndProc
  AddLinha(Result, Linha, '-2', 'Sem controle fiscal (texto, etc)');
  AddLinha(Result, Linha, '-1', 'Indefinido');
  AddLinha(Result, Linha, '00', 'Mercadoria para revenda');
  AddLinha(Result, Linha, '01', 'Mat�ria-Prima');
  AddLinha(Result, Linha, '02', 'Embalagem');
  AddLinha(Result, Linha, '03', 'Produto em Processo');
  AddLinha(Result, Linha, '04', 'Produto Acabado');
  AddLinha(Result, Linha, '05', 'Subproduto');
  AddLinha(Result, Linha, '06', 'Produto intermedi�rio');
  AddLinha(Result, Linha, '07', 'Material de Uso e Consumo');
  AddLinha(Result, Linha, '08', 'Ativo Imobilizado');
  AddLinha(Result, Linha, '09', 'Servi�os');
  AddLinha(Result, Linha, '10', 'Outros Insumos');
  AddLinha(Result, Linha, '99', 'Outras');
end;

function TUnFinanceiro.MontaSPED_COD_CTA(Nivel, Genero: Integer): String;
begin
  Result := Geral.FF0(Nivel) + '.' + Geral.FF0(Genero);
end;

procedure TUnFinanceiro.MudaCarteiraLancamentosSelecionados(QrCrt,
  QrLct: TmySQLQuery; DBGLct: TDBGrid; TabLctA: String; EntCliInt: Integer);

  procedure AlteraCarteira(Tipo, Sub, Situ, Genero, Controle, Cartao,
    Carteira: Integer; Vencimento, Compensado: TDateTime; Pago: Double);
  var
    Sit: Integer;
    Comp: String;
  begin
    if Tipo <> 2 then
    begin
      Sit := 3;
      Comp := Geral.FDT(Vencimento, 1)
    end else
    begin
      if (QrLct.FieldByName('ID_Quit').AsInteger = 0) and
        (QrLct.FieldByName('ID_Pgto').AsInteger = 0) then
      begin
        Sit  := 0;
        Comp := '0000-00-00';
      end else
      begin
        Sit  := Situ;
        Comp := Geral.FDT(Compensado, 1);
      end;
    end;
    if Genero < 1 then
    begin
      Geral.MB_Aviso('O lan�amento ' + Geral.FF0(Controle) +
        ' � protegido. Para editar este item selecione seu caminho correto!');
      Exit;
    end;
    if Cartao > 0 then
    begin
      Geral.MB_Aviso('O lan�amento ' + Geral.FF0(Controle) +
        ' n�o pode ser editado pois pertence a uma fatura!');
      Exit;
    end;
    if ((Sit < 2) or (Sit = 3)) and (Pago = 0) then //Verifica se esta pago / Sit 3 para carteira caixa
    begin
      SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Carteira', 'Tipo', 'Sit', 'Compensado'], ['Controle', 'Sub'], [
        Carteira, Tipo, Sit, Comp], [Controle, Sub], True, '', TabLctA);
    end else
    begin
      Geral.MB_Aviso('O lan�amento ' + Geral.FF0(Controle) +
        ' possui quita��o ou n�o pode ser movido para a carteira selecionada!');
      Exit;
    end;
  end;

var
  i, Carteira, Tipo: Integer;
begin
  if MyObjects.CriaForm_AcessoTotal(TFmLctMudaCart, FmLctMudaCart) then
  begin
    FmLctMudaCart.FEntCliInt := EntCliInt;
    //
    FmLctMudaCart.ShowModal;
    Carteira := FmLctMudaCart.FCarteiraSel;
    Tipo     := FmLctMudaCart.FCarteiraTip;
    FmLctMudaCart.Destroy;
    if Carteira = QrLct.FieldByName('Carteira').AsInteger then Exit;
    if Carteira <> -1000 then
    begin
      if DBGLct.SelectedRows.Count > 1 then
      begin
        with DBGLct.DataSource.DataSet do
        begin
          for i:= 0 to DBGLct.SelectedRows.Count-1 do
          begin
            //GotoBookmark(DBGLct.SelectedRows.Items[i]);
            GotoBookmark(DBGLct.SelectedRows.Items[i]);
            //
            try
              AlteraCarteira(Tipo, QrLct.FieldByName('Sub').AsInteger,
                QrLct.FieldByName('Sit').AsInteger,
                QrLct.FieldByName('Genero').AsInteger,
                QrLct.FieldByName('Controle').AsInteger,
                QrLct.FieldByName('Cartao').AsInteger, Carteira,
                QrLct.FieldByName('Vencimento').AsDateTime,
                QrLct.FieldByName('Compensado').AsDateTime,
                QrLct.FieldByName('Pago').AsFloat);
            except
              Exit;
            end;
          end;
        end;
      end else
      begin
        AlteraCarteira(Tipo, QrLct.FieldByName('Sub').AsInteger,
          QrLct.FieldByName('Sit').AsInteger,
          QrLct.FieldByName('Genero').AsInteger,
          QrLct.FieldByName('Controle').AsInteger,
          QrLct.FieldByName('Cartao').AsInteger, Carteira,
          QrLct.FieldByName('Vencimento').AsDateTime,
          QrLct.FieldByName('Compensado').AsDateTime,
          QrLct.FieldByName('Pago').AsFloat);
      end;
      RecalcSaldoCarteira(Carteira, nil, nil, False, False);
      RecalcSaldoCarteira(
        QrLct.FieldByName('Carteira').AsInteger, QrCrt, QrLct, True, True);
    end;
  end;
end;

procedure TUnFinanceiro.MudaContaLancamentosSelecionados(QrCrt,
  QrLct: TmySQLQuery; DBGLct: TDBGrid; TabLctA: String);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Conta do Plano de Contas';
  Prompt = 'Informe a conta:';
  Campo  = 'Descricao';
var
  i: Integer;
  Conta: Variant;
begin
  if (QrLct.State = dsInactive) or (QrLct.RecordCount = 0) then Exit;
  //
  Conta := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Codigo, Nome ' + Campo,
  'FROM contas ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, True);
  if Conta <> Null then
  begin
    if Conta = QrLct.FieldByName('Genero').AsInteger then Exit;
    if Conta <> -1000 then
    begin
      try
        Screen.Cursor := crHourGlass;
        //
        if DBGLct.SelectedRows.Count = 1 then
        begin
          if QrLct.FieldByName('Genero').AsInteger < 1 then
          begin
            Geral.MB_Aviso('O lan�amento ' + IntToStr(
              QrLct.FieldByName('Controle').AsInteger) +
              ' � protegido. Para editar ' +
              'este item selecione seu caminho correto!');
            Exit;
          end;
          SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['Genero'], ['Controle', 'Sub'],
            [Conta], [QrLct.FieldByName('Controle').AsInteger,
            QrLct.FieldByName('Sub').AsInteger], True, '', TabLctA);
        end else
        begin
          with DBGLct.DataSource.DataSet do
          for i:= 0 to DBGLct.SelectedRows.Count-1 do
          begin
            if QrLct.FieldByName('Genero').AsInteger < 1 then
            begin
              Geral.MB_Aviso('O lan�amento '+IntToStr(
              QrLct.FieldByName('Controle').AsInteger)+' � protegido. Para editar '+
              'este item selecione seu caminho correto!');
              Exit;
            end;
            //GotoBookmark(DBGLct.SelectedRows.Items[i]);
            GotoBookmark(DBGLct.SelectedRows.Items[i]);
            //
            SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['Genero'], ['Controle', 'Sub'],
              [Conta], [QrLct.FieldByName('Controle').AsInteger,
              QrLct.FieldByName('Sub').AsInteger], True, '', TabLctA);
          end;
        end;
        RecalcSaldoCarteira(QrLct.FieldByName('Carteira').AsInteger, QrCrt, QrLct, True, True);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TUnFinanceiro.MudaDataLancamentosSelecionados(CampoData: TCampoData;
  QrCrt, QrLct: TmySQLQuery; DBGLct: TDBGrid; TabLctA: String);

  procedure AlteraData(Sit, Sub, Controle: Integer; NomeCampo: String;
    DataSel: TDateTime);
  begin
    if (Sit < 2) and (CampoData = cdCompensado) then
    begin
      Geral.MB_Aviso('O lan�amento ' + Geral.FF0(Controle)+' n�o foi compensado ainda!');
      Exit;
    end;
    SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [NomeCampo], ['Controle', 'Sub'],
      [Geral.FDT(DataSel, 1)], [Controle, Sub], True, '', TabLctA);
  end;

var
  i: Integer;
  NomeCampo: String;
  DataDef, DataSel: TDateTime;
begin
  //Tipo     := -1000;
  //Carteira := -1000;
  case CampoData of
    cdData      : DataDef := QrLct.FieldByName('Data').AsDateTime;
    cdVencimento: DataDef := QrLct.FieldByName('Vencimento').AsDateTime;
    cdCompensado: DataDef := QrLct.FieldByName('Compensado').AsDateTime;
    else DataDef := Date;
  end;
  //Application.CreateForm(TFmGetData, FmGetData);
  if DBCheck.ObtemData(DataDef, DataSel, VAR_DATA_MINIMA) then
  begin
    if DataSel < 2 then
      Exit
    else
    begin
      case CampoData of
        cdData      : NomeCampo := 'Data';
        cdVencimento: NomeCampo := 'Vencimento';
        cdCompensado: NomeCampo := 'Compensado';
        else NomeCampo := '';
      end;
      if DBGLct.SelectedRows.Count > 1 then
      begin
        with DBGLct.DataSource.DataSet do
        for i:= 0 to DBGLct.SelectedRows.Count-1 do
        begin
          //GotoBookmark(DBGLct.SelectedRows.Items[i]);
          GotoBookmark(DBGLct.SelectedRows.Items[i]);
          //
          try
            AlteraData(QrLct.FieldByName('Sit').AsInteger,
              QrLct.FieldByName('Sub').AsInteger,
              QrLct.FieldByName('Controle').AsInteger, NomeCampo, DataSel);
          except
            Exit;
          end;
        end;
      end else
      begin
        AlteraData(QrLct.FieldByName('Sit').AsInteger,
          QrLct.FieldByName('Sub').AsInteger,
          QrLct.FieldByName('Controle').AsInteger, NomeCampo, DataSel);
      end;
      RecalcSaldoCarteira(
        QrLct.FieldByName('Carteira').AsInteger, QrCrt, QrLct, True, True);
    end;
  end;
end;

procedure TUnFinanceiro.MudaDataVenctoLLctEFatID(QrCrt, QrLct: TmySQLQuery;
  DBGLct: TDBGrid; TabLctA: String);
var
  DataSel: TDateTime;
  Controle, Sub: Integer;
  FatParcRef: Integer;
begin
  if QrLct.FieldByName('FatID').AsInteger = 0  then
  begin
    Geral.MB_Aviso('Lan�amento n�o � de loca��o!');
    Exit;
  end else
  begin
    Controle := QrLct.FieldByName('Controle').AsInteger;
    Sub := QrLct.FieldByName('Sub').AsInteger;
    FatParcRef := QrLct.FieldByName('FatParcRef').AsInteger;
    //
    if DBCheck.ObtemData(QrLct.FieldByName('Vencimento').AsDateTime, DataSel, VAR_DATA_MINIMA) then
    begin
      if SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['Vencimento'], ['Controle', 'Sub'],
      [Geral.FDT(DataSel, 1)], [Controle, Sub], True, '', TabLctA) then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'lctfatref', False, [
        'Vencto'], [
        'Conta'], [
        Geral.FDT(DataSel, 1)], [
        FatParcRef], True);
      end;
    end;
  end;
end;

procedure TUnFinanceiro.MudaTextLancamentosSelecionados(QrCrt,
  QrLct: TmySQLQuery; DBGLct: TDBGrid; TabLctA: String);

  procedure MudaText(Atual, Novo: String; Controle, Sub: Integer);
  var
    TodoTexto: String;
  begin
    TodoTexto := QrLct.FieldByName('Descricao').AsString;
    //
    if pos(Atual, TodoTexto) > 0 then
    begin
      TodoTexto := StringReplace(TodoTexto, Atual, Novo, [rfReplaceAll, rfIgnoreCase]);
      //
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Descricao'], ['Controle', 'Sub'], [TodoTexto],
        [Controle, Sub], True, '', TabLctA);
    end;
  end;

var
  i: Integer;
  Atual, Novo: String;
  Muda: Boolean;
begin
  if DBCheck.CriaFm(TFmMudaTexto, FmMudaTexto, afmoNegarComAviso) then
  begin
    FmMudaTexto.ShowModal;
    Muda  := FmMudaTexto.FMuda;
    Atual := FmMudaTexto.FAtual;
    Novo  := FmMudaTexto.FNovo;
    FmMudaTexto.Destroy;
    //
    if Muda then
    begin
      if DBGLct.SelectedRows.Count > 1 then
      begin
        with DBGLct.DataSource.DataSet do
        for i:= 0 to DBGLct.SelectedRows.Count-1 do
        begin
          //GotoBookmark(DBGLct.SelectedRows.Items[i]);
          GotoBookmark(DBGLct.SelectedRows.Items[i]);
          //
          MudaText(Atual, Novo, QrLct.FieldByName('Controle').AsInteger,
            QrLct.FieldByName('Sub').AsInteger);
        end;
      end else
      begin
        MudaText(Atual, Novo, QrLct.FieldByName('Controle').AsInteger,
          QrLct.FieldByName('Sub').AsInteger);
      end;
    end;
    RecalcSaldoCarteira(
      QrLct.FieldByName('Carteira').AsInteger, QrCrt, QrLct, True, True);
  end;
end;

procedure TUnFinanceiro.MudaValorEmCaixa(QrLct, QrCrt: TmySQLQuery);
var
  Lancto: Int64;
  Tipo: Integer;
  Valor: Double;
  OK: Boolean;
begin
  Lancto := QrLct.FieldByName('Controle').AsInteger;
  Tipo   := QrCrt.FieldByName('Tipo').AsInteger;
  if Tipo > 0 then
  begin
    Geral.MB_Aviso('Carteira inv�lida.');
    Exit;
  end;
  if QrCrt.FieldByName('Codigo').AsInteger = 0 then
  begin
    Geral.MB_Aviso('Este caixa n�o pode ter saldo.');
    Exit;
  end;
  OK := False;
  //Numero := FloatToStr(QrCrtEmCaixa.Value);
  VAR_CALC := 1;
  VAR_QRCARTEIRAS := QrCrt;
  MyObjects.CriaForm_AcessoTotal(TFmSomaDinheiro, FmSomaDinheiro);
  FmSomaDinheiro.ShowModal;
  FmSomaDinheiro.Destroy;
  if VAR_CALC = 1 then Valor := VAR_VALOR else Exit;
  if Valor = 0 then begin
    if Geral.MB_Pergunta('Confirma o zeramento do caixa?') = ID_YES then  OK := True;
  end else OK := True;
  if OK then
  begin
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('UPDATE carteiras SET EmCaixa=:P0');
    Dmod.QrUpdM.SQL.Add('WHERE Codigo=:P1');
    Dmod.QrUpdM.Params[0].AsFloat := Valor;
    Dmod.QrUpdM.Params[1].AsInteger := QrCrt.FieldByName('Codigo').AsInteger;
    Dmod.QrUpdM.ExecSQL;
    VAR_LANCTO2 := Lancto;
    UFinanceiro.RecalcSaldoCarteira(QrCrt.FieldByName('Codigo').AsInteger,
      QrCrt, QrLct, True, True);
  end;
  VAR_LANCTO2 := Lancto;
end;

procedure TUnFinanceiro.QuitacaoAutomaticaDmk(dmkDBGLct: TdmkDBGrid;
  QrLct, QrCrt: TmySQLQuery; TabLctA: String; AlteraAtehFatID: Boolean = False);
var
  i(*, Controle*), Carteira: Integer;
begin
  Carteira := QrLct.FieldByName('Carteira').AsInteger;
  if Carteira < 1 then
  begin
    Geral.MB_Aviso('A carteira ' + Geral.FF0(Carteira) +
      ' n�o pode ter seus lan�amentos quitados automaticamente!' + sLineBreak +
      'Favor quitar na janela apropriada!');
    Exit;
  end;
  //Controle := QrLct.FieldByName('Controle').AsInteger;
  //
  MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
  FmGetData.TPData.Date := Date;
  //
  FmGetData.LaHora.Visible  := False;
  FmGetData.EdHora.Visible  := False;
  //
  FmGetData.OcultaJurosEMulta;
  //
  UFinanceiro.ReopenCarteirasPgto(FmGetData.QrCarteiras);
  UFinanceiro.ConfiguraGBCarteiraPgto(FmGetData.GBCarteira,
    FmGetData.EdCarteira, FmGetData.CBCarteira,
    Carteira(*QrLct.FieldByName('Carteira').AsInteger*));
  //
  FmGetData.ShowModal;
  FmGetData.Destroy;
  //
  if VAR_GETDATA = 0 then Exit;
  //
  if dmkDBGLct.SelectedRows.Count > 1 then
  begin
    if Geral.MB_Pergunta('Confirma a quita��o autom�tica ' +
      'dos itens selecionados?') = ID_YES then
    begin
      with dmkDBGLct.DataSource.DataSet do
      for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
      begin
        //GotoBookmark(dmkDBGLct.SelectedRows.Items[i]);
        GotoBookmark(dmkDBGLct.SelectedRows.Items[i]);
        QuitaItemLanctoAutomatico(VAR_GETDATA, QrLct, QrCrt, TabLctA,
          VAR_CARTEIRA, AlteraAtehFatID);
        //Controle := QrLct.FieldByName('Controle').AsInteger;
      end;
    end;
  end else
    QuitaItemLanctoAutomatico(VAR_GETDATA, QrLct, QrCrt, TabLctA,
      VAR_CARTEIRA, AlteraAtehFatID);
  RecalcSaldoCarteira(QrCrt.FieldByName('Codigo').AsInteger, QrCrt, QrLct,
    False, True);
end;

procedure TUnFinanceiro.QuitacaoAutomaticaDmk2(dmkDBGLct: TdmkDBGrid; QrLct,
  QrCrt: TmySQLQuery; TabLctA: String; AlteraAtehFatID: Boolean);
var
  i(*, Controle*), Carteira: Integer;
  Selecionou: Boolean;
begin
(*
  Carteira := QrLct.FieldByName('Carteira').AsInteger;
  if Carteira < 1 then
  begin
    Geral.MB_Aviso('A carteira ' + Geral.FF0(Carteira) +
      ' n�o pode ter seus lan�amentos quitados automaticamente!' + sLineBreak +
      'Favor quitar na janela apropriada!');
    Exit;
  end;
*)
  //Controle := QrLct.FieldByName('Controle').AsInteger;
  //
  MyObjects.CriaForm_AcessoTotal(TFmGetDataECartBco, FmGetDataECartBco);
  FmGetDataECartBco.TPData.Date := Date;
  //
  FmGetDataECartBco.LaHora.Visible  := False;
  FmGetDataECartBco.EdHora.Visible  := False;
  //
{
  UFinanceiro.ReopenCarteirasPgto(FmGetDataECartBco.QrCarteiras);
  UFinanceiro.ConfiguraGBCarteiraPgto(FmGetDataECartBco.GBCarteira,
    FmGetDataECartBco.EdCarteira, FmGetDataECartBco.CBCarteira,
    Carteira(*QrLct.FieldByName('Carteira').AsInteger*));*)
}
  //
  FmGetDataECartBco.ShowModal;
  Selecionou := FmGetDataECartBco.FSelecionou;
  FmGetDataECartBco.Destroy;
  //
  if Selecionou = False then Exit;
  if VAR_GETDATA = 0 then Exit;
  if VAR_GETCARTBCO = 0 then Exit;
  //
  if dmkDBGLct.SelectedRows.Count > 1 then
  begin
    if Geral.MB_Pergunta('Confirma a quita��o autom�tica ' +
      'dos itens selecionados?') = ID_YES then
    begin
      with dmkDBGLct.DataSource.DataSet do
      for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
      begin
        //GotoBookmark(dmkDBGLct.SelectedRows.Items[i]);
        GotoBookmark(dmkDBGLct.SelectedRows.Items[i]);
        QuitaItemLanctoAutomatico(VAR_GETDATA, QrLct, QrCrt, TabLctA,
          (*VAR_CARTEIRA*)VAR_GETCARTBCO, AlteraAtehFatID);
        //Controle := QrLct.FieldByName('Controle').AsInteger;
      end;
    end;
  end else
    QuitaItemLanctoAutomatico(VAR_GETDATA, QrLct, QrCrt, TabLctA,
      (*VAR_CARTEIRA*)VAR_GETCARTBCO, AlteraAtehFatID);
  RecalcSaldoCarteira(QrCrt.FieldByName('Codigo').AsInteger, QrCrt, QrLct,
    False, True);
end;

procedure TUnFinanceiro.QuitaItemLanctoAutomatico(Data: TDateTime;
  QrLct, QrCrt: TmySQLQuery; TabLctA: String; OutraCartCompensa: Integer = 0;
  AlteraAtehFatID: Boolean = False);
var
  Controle2: Integer;
  Txt: String;
begin
  if not AlteraAtehFatID then
  begin
    if dmkPF.FatIDProibidoEmLct(QrLct.FieldByName('FatID').AsInteger) then
      Exit;
  end;
  if QrLct.FieldByName('Sit').AsInteger > 0 then
  begin
    case QrLct.FieldByName('Sit').AsInteger of
      1: Txt := 'j� tem pagamentos parciais atrelados a ele';
      2: Txt := 'j� est� quitado';
      3: Txt := 'j� est� compensado';
      else Txt := 'j� possui algum tipo de atrelamento desconhecido';
    end;
    Geral.MB_Aviso('O lan�amento n� ' + Geral.FF0(
    QrLct.FieldByName('Controle').AsInteger) + ' n�o ser� compensada pois ' +
    Txt + '!');
    Exit;
  end;
  Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', TabLctA,
    LAN_CTOS, 'Controle');
  //
  if QrLct.FieldByName('Sit').AsInteger > 0 then
  begin
    Geral.MB_Aviso('O lan�amento n� ' +
      Geral.FF0(QrLct.FieldByName('Controle').AsInteger) +
      ' j� possui pagamento parcial ou total, portanto n�o ser� quitado!'
      );
    Exit;
  end;
  //
  // ini 2022-03-23
(*
  GenCtbBco := 0;
  if OutraCartCompensa = 0 then
    Carteira := QrLct.FieldByName('BancoCar').AsInteger
  else
    Carteira := OutraCartCompensa;
    //
  if DModG.QrCtrlGeralUsarFinCtb.Value = 1 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrCartCtb, Dmod.MyDB, [
    'SELECT Banco, GenCtb ',
    'FROM carteiras',
    'WHERE Codigo=' + Geral.FF0(Carteira),
    '']);
    GenCtbBco := DModFin.QrCartCtbGenCtb.Value;
    if MyObjects.FIC(GenCtbBco = 0, nil,
    'Banco sem conta cont�bil da carteira de emiss�o selecionada!') then
      Exit;
  end;
*)
  // fim 2022-03-23
  LancamentoDefaultVARS;
  FLAN_Data       := Geral.FDT(VAR_GETDATA, 1);
  FLAN_Vencimento := Geral.FDT(QrLct.FieldByName('Vencimento').AsDateTime, 1);
  FLAN_DataCad    := Geral.FDT(Date, 1);
  FLAN_Mez        := QrLct.FieldByName('Mez').AsInteger;
  FLAN_Descricao  := QrLct.FieldByName('Descricao').AsString;
  FLAN_Compensado := Geral.FDT(VAR_GETDATA, 1);
  FLAN_Duplicata  := QrLct.FieldByName('Duplicata').AsString;
  FLAN_Doc2       := QrLct.FieldByName('Doc2').AsString;
  FLAN_SerieCH    := QrLct.FieldByName('SerieCH').AsString;

  FLAN_Documento  := Trunc(QrLct.FieldByName('Documento').AsInteger);
  FLAN_Tipo       := 1;
  //FLAN_Carteira   := Carteira;
  FLAN_Credito    := QrLct.FieldByName('Credito').AsFloat;
  FLAN_Debito     := QrLct.FieldByName('Debito').AsFloat;
  FLAN_Genero     := QrLct.FieldByName('Genero').AsInteger;
  FLAN_GenCtb     := QrLct.FieldByName('GenCtb').AsInteger;
  // ini 2022-03-23
  FLAN_GenCtbD    := 0;
  FLAN_GenCtbC    := 0;
(*
  if FLAN_Credito > 0 then
  begin
    FLAN_GenCtbD    := GenCtbBco;
    FLAN_GenCtbC    := QrLct.FieldByName('GenCtbD').AsInteger;
  end else
  if FLAN_Debito > 0 then
  begin
    FLAN_GenCtbD    := QrLct.FieldByName('GenCtbC').AsInteger;
    FLAN_GenCtbC    := GenCtbBco;
  end;
*)
  // fim 2022-03-23
  FLAN_SerieNF    := QrLct.FieldByName('SerieNF').AsString;
  FLAN_NotaFiscal := QrLct.FieldByName('NotaFiscal').AsInteger;
  FLAN_Sit        := 3;
  FLAN_Cartao     := 0;
  FLAN_Linha      := 0;
  FLAN_Fornecedor := QrLct.FieldByName('Fornecedor').AsInteger;
  FLAN_Cliente    := QrLct.FieldByName('Cliente').AsInteger;
  FLAN_MoraDia    := QrLct.FieldByName('MoraDia').AsFloat;
  FLAN_Multa      := QrLct.FieldByName('Multa').AsFloat;
  //FLAN_UserCad    := VAR_USUARIO;
  //FLAN_DataDoc    := Geral.FDT(Date, 1);
  FLAN_Vendedor    := QrLct.FieldByName('Vendedor').AsInteger;
  FLAN_Account     := QrLct.FieldByName('Account').AsInteger;
  FLAN_CentroCusto := QrLct.FieldByName('CentroCusto').AsInteger;
  FLAN_ICMS_P      := QrLct.FieldByName('ICMS_P').AsFloat;
  FLAN_ICMS_V      := QrLct.FieldByName('ICMS_V').AsFloat;
  FLAN_CliInt      := QrLct.FieldByName('CliInt').AsInteger;
  FLAN_Depto       := QrLct.FieldByName('Depto').AsInteger;
  FLAN_DescoPor    := QrLct.FieldByName('DescoPor').AsInteger;
  FLAN_ForneceI    := QrLct.FieldByName('ForneceI').AsInteger;
  FLAN_DescoVal    := QrLct.FieldByName('DescoVal').AsFloat;
  FLAN_NFVal       := QrLct.FieldByName('NFVal').AsFloat;
  FLAN_Unidade     := QrLct.FieldByName('Unidade').AsInteger;
  FLAN_Qtde        := QrLct.FieldByName('Qtde').AsFloat;
  FLAN_Qtd2        := QrLct.FieldByName('Qtd2').AsFloat;
  FLAN_FatID       := QrLct.FieldByName('FatID').AsInteger;
  FLAN_FatID_Sub   := QrLct.FieldByName('FatID_Sub').AsInteger;
  FLAN_FatNum      := QrLct.FieldByName('FatNum').AsInteger;
  FLAN_FatParcela  := QrLct.FieldByName('FatParcela').AsInteger;
  FLAN_MultaVal    := QrLct.FieldByName('MultaVal').AsFloat;
  FLAN_MoraVal     := QrLct.FieldByName('MoraVal').AsFloat;
  FLAN_ID_Pgto     := QrLct.FieldByName('Controle').AsInteger;
  FLAN_Controle    := Round(Controle2);
  //
  FLAN_ModeloNF     := QrLct.FieldByName('ModeloNF').AsString;
  FLAN_VctoOriginal := Geral.FDT(QrLct.FieldByName('VctoOriginal').AsDateTime, 1);
  //
  if not PagamentoEmBanco_DefineGenerosECarteira(
    QrLct.FieldByName('Controle').AsInteger,
    QrLct.FieldByName('Sub').AsInteger,
    FLAN_Credito, FLAN_Debito,
    OutraCartCompensa, QrLct.FieldByName('BancoCar').AsInteger,
    QrLct.FieldByName('GenCtbD').AsInteger,
    QrLct.FieldByName('GenCtbC').AsInteger,
    FLAN_Carteira,
    FLAN_GenCtbD, FLAN_GenCtbC) then Exit;
  //
  InsereLancamento(TabLctA);
end;

function TUnFinanceiro.LocalizarlanctoCliInt(const Controle, Sub, CliInt: Integer;
  TPDataIni: TDateTimePicker; QrLct, QrCrt, QrCrtSum: TmySQLQuery; const
  AvisaQuandoNaoEncontra: Boolean; const TabLct, TabLctB, TabLctD: String;
  var CtrlToLoc: Integer): Boolean;
var
  Lancto: Double;
  Tentativas: Integer;
  SQL: String;
begin
  CtrlToLoc := 0;
  Screen.Cursor := crHourGlass;
  try
    if Controle = 0 then
      Exit;
    //
    if Sub <> -1 then
      SQL := 'AND Sub=' + Geral.FF0(Sub)
    else
      SQL := 'AND Carteira=' + Geral.FF0(QrCrt.FieldByName('Codigo').AsInteger);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DmodFin.QrLocLcto, Dmod.MyDB, [
      'SELECT Data, Controle, Tipo, Carteira, CliInt, "'+ TabLct +'" Tab ',
      'FROM ' + TabLct,
      'WHERE Controle=' + Geral.FF0(Controle),
      SQL,
      ' ',
      'UNION ',
      ' ',
      'SELECT Data, Controle, Tipo, Carteira, CliInt, "'+ TabLctB +'" Tab ',
      'FROM ' + TabLctB,
      'WHERE Controle=' + Geral.FF0(Controle),
      SQL,
      ' ',
      'UNION ',
      ' ',
      'SELECT Data, Controle, Tipo, Carteira, CliInt, "'+ TabLctD +'" Tab ',
      'FROM ' + TabLctD,
      'WHERE Controle=' + Geral.FF0(Controle),
      SQL,
      '']);
    if DModFin.QrLocLcto.RecordCount > 0 then
    begin
      if DmodFin.QrLocLctoCliInt.Value <> CliInt then
      begin
        if AvisaQuandoNaoEncontra then
          Geral.MB_Aviso('O lan�amento solicitado n�o pertence ao cliente interno atual!');
        Result := False;
        Exit;
      end;
    end;
    if TPDataIni <> nil then
    begin
      if DmodFin.QrLocLctoData.Value < TPDataIni.Date then
      TPDataIni.Date := DmodFin.QrLocLctoData.Value;
    end;
    //
    VAR_LANCTO2 := DmodFin.QrLocLctoControle.Value;
    Lancto      := DmodFin.QrLocLctoControle.Value;
    //
    ReabreCarteirasCliInt(CliInt, DmodFin.QrLocLctoCarteira.Value, QrCrt, QrCrtSum);
    //
    if QrLct.State = dsInactive then
      Result := False
    else
      Result :=  QrLct.Locate('Controle', Lancto, []);
    if not Result then
    begin
      if AvisaQuandoNaoEncontra then
      begin
        if DModFin.QrLocLctoTab.Value <> TabLct then
          Geral.MB_Aviso('N�o foi poss�vel localizar o lan�amento n� ' +
            Geral.FF0(Controle) + '-' + Geral.FF0(Sub) + '!' + sLineBreak +
            'Motivo: O lan�amento est� no arquivo morto!')
        else
          Geral.MB_Aviso('N�o foi poss�vel localizar o lan�amento n� ' +
            Geral.FF0(Controle) + '-' + Geral.FF0(Sub) + '!');
      end else
        CtrlToLoc := VAR_LANCTO2;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnFinanceiro.LocalizarLancamento(DTPickerI, DTPickerF: TDateTimePicker;
  TipoData: Integer; QrCrt, QrLct: TmySQLQuery; LocSohCliInt: Boolean;
  QuemChamou: Integer; TabLctA: String; ModuleLctX: TDataModule;
  Controle: Integer; ConfirmarLocalizar: Boolean);
var
  CliInt: Integer;
begin
  CliInt := 0;
  VAR_Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    if MyObjects.CriaForm_AcessoTotal(TFmLocDefs, FmLocDefs) then
    begin
      FmLocDefs.FModuleLctX := ModuleLctX;
      FmLocDefs.FTabLctA    := TabLctA;
      FmLocDefs.FQuemChamou := QuemChamou;
      case VAR_LIB_ARRAY_EMPRESAS_CONTA of
        0:
        begin
          Geral.MB_Aviso('Cliente(s) interno(s) n�o definido(s)!');
          Exit;
        end;
        1:
        begin
          CliInt := VAR_LIB_ARRAY_EMPRESAS_LISTA[0];
          if CliInt = 0 then
          begin
            Geral.MB_Aviso('Cliente interno inv�lido: ' +
            IntToStr(CliInt));
            Exit;
          end else begin
            FmLocDefs.ReopenPCliInt();
            FmLocDefs.EdCliInt.Text     := IntToStr(CliInt);
            FmLocDefs.CBCliInt.KeyValue := CliInt;
            FmLocDefs.CkCliInt.Checked  := True;
          end;
        // 2 ou mais
        end else FmLocDefs.ReopenPCliInt();
      end;
      FmLocDefs.FQrLct        := QrLct;
      FmLocDefs.FQrCrt        := QrCrt;
      FmLocDefs.FDTPDataIni   := DTPickerI;
      FmLocDefs.FDTPDataFim   := DTPickerF;
      FmLocDefs.F_CliInt      := CliInt;
      FmLocDefs.FLocSohCliInt := LocSohCliInt;
      FmLocDefs.FQuemChamou   := QuemChamou;
      FmLocDefs.FTipoData     := TipoData;
      //
      if Controle <> 0 then
      begin
        FmLocDefs.EdControle.ValueVariant := Controle;
        FmLocDefs.CkControle.Checked      := True;
        if ConfirmarLocalizar then
        begin
          FmLocDefs.BtConfirmaClick(FmLocDefs.BtConfirma);
        end;
      end;
      FmLocDefs.ShowModal;
      FmLocDefs.Destroy;
    end;
  finally
    Screen.Cursor := VAR_Cursor;
  end;
end;

function TUnFinanceiro.ImpedePelaTabelaLct(Letra: String; Controle: Double): Boolean;
begin
  Result := Uppercase(Letra) <> 'A';
  if Result then
  begin
    if Controle <> 0 then Geral.MB_Aviso('A��o cancelada para o lan�amento ' +
    FormatFloat('0', Controle) + '! Ele j� foi encerrado ou arquivado!'
    )
    else Geral.MB_Aviso(
    'A��o cancelada! O lan�amento j� foi encerrado ou arquivado!'
    );
  end;
end;

function TUnFinanceiro.ImpedePorFaltaDeContaContabil(Controle, Sub, Carteira,
  GenCtbD, GenCtbC: Integer; Avisa: Boolean = True): Boolean;
begin
  if DModG.QrCtrlGeralUsarFinCtb.Value = 1 then
  begin
    Result := (Carteira = 0) or (GenCtbD < 1) or (GenCtbC < 1);
    if Result and Avisa then
      Geral.MB_Aviso('A��o cancelada para o lan�amento ' + Geral.FF0(Controle) +
      '.' + Geral.FF0(Sub) + sLineBreak + 'Motivo:' + slineBreak +
      'Carteira: ' + Geral.FF0(Carteira) + sLineBreak +
      'Conta de d�bito: ' + Geral.FF0(GenCtbD) + sLineBreak +
      'Conta de cr�dito: ' + Geral.FF0(GenCtbC) + sLineBreak +
      '');
  end else
    Result := False;
end;

{###
procedure TUnFinanceiro.CriaFmQuitaDocs(Data: TDateTime; Documento, Controle,
 CtrlIni: Double; Sub, Carteira: Integer);
begin
  if DBCheck.CriaFm(TFmContasSdoImp, FmContasSdoImp, afmoNegarComAviso) then
  begin
    FmContasSdoImp.ShowModal;
    FmContasSdoImp.Destroy;
  end;
end;
}

{
procedure TUnFinanceiro.CriarTransfer(Tipo: Integer; QrLct, QrCrt:
TmySQLQuery; MostraForm: Boolean);
var
  Localiza: Double;
  Lock: Int64;
begin
  lock := QrLct.FieldByName('Controle').AsInteger;
  if not Tipo in [0,1,2,3] then
  begin
    Geral.MB_Aviso('Tipo de transfer�ncia n�o definido.');
    Exit;
  end;
  //if UMyMod.AcessoNegadoAoForm('Perfis', 'MovContas', 0) and
  UMyMod.AcessoNegadoAoForm('Perfis', 'Transfer�ncia de dinheiro entre carteiras', 0) then Exit;
  if (QrLct.FieldByName('Genero').AsInteger = -1) and (Tipo in [1,2])
  and (TransferenciaCorrompida(QrLct.FieldByName('Controle').AsInteger,
    QrCrt)) then
  begin
    UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
    Exit;
  end;
  if DBCheck.CriaFm(TFmTransfer, FmTransfer, afmNegarComAviso) then
  begin
    with FmTransfer do
    begin
      if Tipo = 0 then
      begin
        LaTipo.Caption := Geral.LaTipo(stIns);
        TPData.Date := dmkPF.ObtemDataInserir();

      end else LaTipo.Caption := CO_ALTERACAO;
      if Tipo in [0,1,3] then BtConfirma.Enabled := True;
      if Tipo in [1,2,3] then BtExclui.Enabled := True;
      if Tipo in [1,2] then
      begin
        UMyMod.UpdLockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
        if QrLct.FieldByName('Genero').AsInteger <> -1 then
        begin
          Geral.MB_Aviso('Este lan�amento n�o � uma transfer�ncia.');
          UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
          Exit;
        end else ConfiguraFmTransfer;
      end;
      if Tipo = 3 then
      begin
        Localiza := CuringaLoc.CriaForm2('Controle', 'Descricao', TabLctA,
        VAR_GOTOMySQLDBNAME, 'AND Genero=-1');
        if TransferenciaCorrompida(Localiza, QrCrt) then
        begin
          FmTransfer.Destroy;
          UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
          Exit;
        end;
        ConfiguraFmTransfer;
      end;
    end;
    if MostraForm then
    begin
      FmTransfer.ShowModal;
      FmTransfer.Destroy;
    end;
  end;
  UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
end;
}

procedure TUnFinanceiro.CriarTransferCart(Tipo: Integer; QrLct, QrCrt:
TmySQLQuery; MostraForm: Boolean; CliInt, FatID, FatID_Sub, FatNum: Integer;
TabLctA: String);
var
  Localiza: Double;
  Lock: Int64;
begin
  // Tipo
  // 0 - Inclui
  // 1 - Altera
  // 2 - Exclui
  // 3 - Localiza?
  //
  if QrLct = nil then lock := 0 else
    lock := QrLct.FieldByName('Controle').AsInteger;
  if not Tipo in [0,1,2,3] then
  begin
    Geral.MB_Aviso('Tipo de transfer�ncia n�o definido.');
    Exit;
  end;
  if QrLct <> nil then
  begin
    if (QrLct.FieldByName('Genero').AsInteger = -1) and (Tipo in [1,2])
    and (TransferenciaCorrompidaCart(QrLct.FieldByName('Controle').AsInteger,
      QrCrt, QrLct, TabLctA)) then
    begin
      UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
      Exit;
    end;
  end;
  if DBCheck.CriaFm(TFmTransfer2, FmTransfer2, afmoNegarComAviso) then
  begin
    with FmTransfer2 do
    begin
      FTabLctA   := TabLctA;
      FQrCrt     := QrCrt;
      FQrLct     := QrLct;
      F_CliInt   := CliInt;
      FFatID     := FatID;
      FFatNum    := FatNum;
      FFatId_Sub := FatID_Sub;
      QrOrigem.Close;
      QrOrigem.SQL.Clear;
      QrOrigem.SQL.Add('SELECT Codigo, Nome, Tipo');
      QrOrigem.SQL.Add('FROM carteiras');
      QrOrigem.SQL.Add('WHERE Ativo = 1 ');
      if CliInt <> 0 then
        QrOrigem.SQL.Add('AND ForneceI=' + FormatFloat('0', CliInt));
      QrOrigem.SQL.Add('ORDER BY Nome');
      QrDestino.SQL := QrOrigem.SQL;
      UnDmkDAC_PF.AbreQuery(QrOrigem, Dmod.MyDB);
      UnDmkDAC_PF.AbreQuery(QrDestino, Dmod.MyDB);
      if Tipo = 0 then
      begin
        ImgTipo.SQLType := stIns;
        TPData.Date := dmkPF.ObtemDataInserir;

      end else ImgTipo.SQLType := stUpd;
      if Tipo in [0,1,3] then BtConfirma.Enabled := True;
      if Tipo in [1,2,3] then BtExclui.Enabled := True;
      if Tipo in [1,2] then
      begin
        if DmodFin.QrTransfData.Value < VAR_DATA_MINIMA then
        begin
          FmTransfer2.Destroy;
          Geral.MB_Aviso(
          'A��o cancelada. Esta transfer�ncia pertence a um m�s encerrado!'
          );
          Exit;
        end;
        UMyMod.UpdLockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
        if QrLct <> nil then
        begin
          if QrLct.FieldByName('Genero').AsInteger <> -1 then
          begin
            FmTransfer2.Destroy;
            Geral.MB_Aviso('Este lan�amento n�o � uma transfer�ncia.'
            );
            UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
            Exit;
          end else ConfiguraFmTransferCart();
        end else ConfiguraFmTransferCart();
      end;
      if Tipo = 3 then
      begin
        Localiza := CuringaLoc.CriaForm2('Controle', 'Descricao', TabLctA,
        VAR_GOTOMySQLDBNAME, 'AND Genero=-1');
        if TransferenciaCorrompidaCart(Localiza, QrCrt, QrLct, TabLctA) then
        begin
          FmTransfer2.Destroy;
          UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
          Exit;
        end;
        ConfiguraFmTransferCart();
      end;
    end;
    if MostraForm then
    begin
      FmTransfer2.ShowModal;
      FmTransfer2.Destroy;
    end;
  end;
  UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
end;

procedure TUnFinanceiro.CriarTransferCtas(Tipo: Integer; QrLct, QrCrt:
TmySQLQuery; MostraForm: Boolean; CliInt, FatID, FatID_Sub, FatNum: Integer;
TabLctA: String);
var
  Localiza: Double;
  Lock: Int64;
begin
  // Tipo
  // 0 - Inclui
  // 1 - Altera
  // 2 - Exclui
  // 3 - Localiza?
  //
  if QrLct = nil then lock := 0 else
    lock := QrLct.FieldByName('Controle').AsInteger;
  if not Tipo in [0,1,2,3] then
  begin
    Geral.MB_Aviso('Tipo de transfer�ncia entre contas n�o definido.'
    );
    Exit;
  end;
  if QrLct <> nil then
  begin
    if (QrLct.FieldByName('FatID').AsInteger = -1) //  and (Tipo in [1,2]) (?? 2008.12.19)
    and (TransferenciaCorrompidaCtas(QrLct.FieldByName('Controle').AsInteger,
      QrCrt, QrLct, TabLctA)) then
    begin
      UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
      Exit;
    end;
  end;
  if DBCheck.CriaFm(TFmTransferCtas, FmTransferCtas, afmoNegarComAviso) then
  begin
    with FmTransferCtas do
    begin
      FTabLctA    := TabLctA;
      FQrCrt      := QrCrt;
      FQrLct      := QrLct;
      F_CliInt    := CliInt;
      QrOrigem.Close;
      QrOrigem.SQL.Clear;
      QrOrigem.SQL.Add('SELECT Codigo, Nome, Tipo');
      QrOrigem.SQL.Add('FROM carteiras');
      if CliInt <> 0 then
        QrOrigem.SQL.Add('WHERE ForneceI=' + FormatFloat('0', CliInt));
      QrOrigem.SQL.Add('ORDER BY Nome');
      QrDestino.SQL := QrOrigem.SQL;
      UnDmkDAC_PF.AbreQuery(QrOrigem, Dmod.MyDB);
      UnDmkDAC_PF.AbreQuery(QrDestino, Dmod.MyDB);
      if Tipo = 0 then
      begin
        ImgTipo.SQLType := stIns;
        TPData.Date := dmkPF.ObtemDataInserir();

      end else ImgTipo.SQLType := stUpd;
      if Tipo in [0,1,3] then BtConfirma.Enabled := True;
      if Tipo in [1,2,3] then BtExclui.Enabled := True;
      if Tipo in [1,2] then
      begin
        UMyMod.UpdLockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
        if QrLct <> nil then
        begin
          if QrLct.FieldByName('FatID').AsInteger <> -1 then
          begin
            Geral.MB_Aviso(
              'Este lan�amento n�o � uma transfer�ncia entre contas!'
              );
            UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
            Exit;
          end else ConfiguraFmTransferCtas;
        end else ConfiguraFmTransferCtas;
      end;
      if Tipo = 3 then
      begin
        Localiza := CuringaLoc.CriaForm2('Controle', 'Descricao', TabLctA,
        VAR_GOTOMySQLDBNAME, 'AND FatID=-1');
        if TransferenciaCorrompidaCtas(Localiza, QrCrt, QrLct, TabLctA) then
        begin
          FmTransferCtas.Destroy;
          UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
          Exit;
        end;
        ConfiguraFmTransferCtas;
      end;
    end;
    if MostraForm then
    begin
      FmTransferCtas.ShowModal;
      FmTransferCtas.Destroy;
    end;
  end;
  UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, TabLctA, 'Controle');
end;

function TUnFinanceiro.CRT_Get(Codigo: Integer): String;
begin
// C�digo do Regime Tribut�rio
  case Codigo of
    1: Result := 'Simples Nacional.';
    2: Result := 'Simples Nacional - excesso de sublimite de receita bruta';
    3: Result := 'Regime Normal.';
    else Result := '';
  end;
end;

function TUnFinanceiro.CRT_Lista(): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA CRT
  AddLinha(Result, Linha, '1', 'Simples Nacional.');
  AddLinha(Result, Linha, '2', 'Simples Nacional - excesso de sublimite de receita bruta.');
  AddLinha(Result, Linha, '3', 'Regime Normal.');
end;

function TUnFinanceiro.TransferenciaCorrompidaCart(Lancto: Double; QrCrt, QrLct:
TmySQLQuery; TabLctA: String): Boolean;
var
  Texto: PChar;
  Localiza: Boolean;
begin
  Result := False;
  DmodFin.QrTransf.Close;
  DmodFin.QrTransf.SQL.Clear;
  DmodFin.QrTransf.SQL.Add('SELECT Data, Tipo, Carteira, Controle, Genero, ');
  DmodFin.QrTransf.SQL.Add('Debito, Credito, Documento, SerieCH, Sub, Duplicata');
  DmodFin.QrTransf.SQL.Add('FROM ' + TabLcta);
  DmodFin.QrTransf.SQL.Add('WHERE Controle=:P0');
  DmodFin.QrTransf.Params[0].AsFloat := Lancto;
  UnDmkDAC_PF.AbreQuery(DmodFin.QrTransf, Dmod.MyDB);
  DmodFin.QrTransf.Last;
  DmodFin.QrTransf.First;
  if DmodFin.QrTransf.RecordCount <> 2 then
  begin
    Result := True;
    if DmodFin.QrTransf.RecordCount <> 0 then
    begin
      Texto := PChar('Existem '+IntToStr(DmodFin.QrTransf.RecordCount)+
      'registros na transfer�ncia, quando deveria haver 2. Deseja exclu�los?');
      if Geral.MB_Pergunta(Texto) = ID_YES then
      begin
{
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add(EXCLUI_DE + TabLctA + ' WHERE Controle=:P0');
        Dmod.QrUpdM.Params[0].AsFloat := DmodFin.QrTransfControle.Value;
        Dmod.QrUpdM.ExecSQL;
}
        while not DmodFin.QrTransf.Eof do
        begin
          ExcluiLct_Unico(TabLctA, DmodFin.QrTransf.Database,
            DmodFin.QrTransfData.Value, DmodFin.QrTransfTipo.Value,
            DmodFin.QrTransfCarteira.Value, DmodFin.QrTransfControle.Value,
            DmodFin.QrTransfSub.Value, dmkPF.MotivDel_ValidaCodigo(302), False);
          if (DmodFin.QrTransfTipo.Value = QrCrt.FieldByName('Tipo').AsInteger)
          and (DmodFin.QrTransfCarteira.Value = QrCrt.FieldByName('Codigo').AsInteger)
          then Localiza := True else Localiza := False;
          UFinanceiro.RecalcSaldoCarteira(DmodFin.QrTransfCarteira.Value,
            QrCrt, QrLct, Localiza, Localiza);
          DmodFin.QrTransf.Next;
        end;
      end;
    end;
    DmodFin.QrTransf.Close;
  end;
end;

function TUnFinanceiro.TransferenciaCorrompidaCtas(Lancto: Double; QrCrt, QrLct:
TmySQLQuery; TabLctA: String): Boolean;
var
  Texto: PChar;
  Localiza: Boolean;
begin
  Result := False;
  DmodFin.QrTrfCtas.Close;
  DmodFin.QrTrfCtas.SQL.Clear;
  DmodFin.QrTrfCtas.SQL.Add('SELECT Data, Tipo, Carteira, Controle, Genero, ');
  DmodFin.QrTrfCtas.SQL.Add('Debito, Credito, Documento, SerieCH, Sub');
  DmodFin.QrTrfCtas.SQL.Add('FROM ' + TabLctA);
  DmodFin.QrTrfCtas.SQL.Add('WHERE Controle=:P0');
  DmodFin.QrTrfCtas.Params[0].AsFloat := Lancto;
  UnDmkDAC_PF.AbreQuery(DmodFin.QrTrfCtas, Dmod.MyDB);
  DmodFin.QrTrfCtas.Last;
  DmodFin.QrTrfCtas.First;
  if DmodFin.QrTrfCtas.RecordCount <> 2 then
  begin
    Result := True;
    if DmodFin.QrTrfCtas.RecordCount <> 0 then
    begin
      Texto := PChar('Existem '+IntToStr(DmodFin.QrTrfCtas.RecordCount)+
      'registros na transfer�ncia entre contas, quando deveria haver 2. Deseja exclu�los?');
      if Geral.MB_Pergunta(Texto) = ID_YES then
      begin
{
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add(EXCLUI_DE + TabLctA + ' WHERE Controle=:P0');
        Dmod.QrUpdM.Params[0].AsFloat := DmodFin.QrTrfCtasControle.Value;
        Dmod.QrUpdM.ExecSQL;
}
        while not DmodFin.QrTrfCtas.Eof do
        begin
          ExcluiLct_Unico(TabLctA, DmodFin.QrTrfCtas.Database,
            DmodFin.QrTrfCtasData.Value, DmodFin.QrTrfCtasTipo.Value,
            DmodFin.QrTrfCtasCarteira.Value, DmodFin.QrTrfCtasControle.Value,
            DmodFin.QrTrfCtasSub.Value, dmkPF.MotivDel_ValidaCodigo(303), False);
          if (DmodFin.QrTrfCtasTipo.Value = QrCrt.FieldByName('Tipo').AsInteger)
          and (DmodFin.QrTrfCtasCarteira.Value = QrCrt.FieldByName('Codigo').AsInteger)
          then Localiza := True else Localiza := False;
          UFinanceiro.RecalcSaldoCarteira(DmodFin.QrTrfCtasCarteira.Value,
            QrCrt, QrLct, Localiza, Localiza);
          DmodFin.QrTrfCtas.Next;
        end;
      end;
    end;
    DmodFin.QrTrfCtas.Close;
  end;
end;

procedure TUnFinanceiro.TransformarLancamentoEmItemDeBloqueto(FatID: Integer;
QrCrt, QrLct: TmySQLQuery; DBGLct: TDBGrid; TabLctA: String);
var
  i: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  if FatID = 0 then
  begin
    Geral.MB_Erro('"FatID" inv�lido para transforma��o em bloqueto!'
    );
  end else begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + TabLctA + ' SET AlterWeb=1, ');
    Dmod.QrUpd.SQL.Add('FatNum=Documento, FatID=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1 AND Sub=:P2');
    with DBGLct.DataSource.DataSet do
    for i:= 0 to DBGLct.SelectedRows.Count-1 do
    begin
      //GotoBookmark(DBGLct.SelectedRows.Items[i]);
      GotoBookmark(DBGLct.SelectedRows.Items[i]);
      Dmod.QrUpd.Params[00].AsInteger := FatID;
      Dmod.QrUpd.Params[01].AsInteger := QrLct.FieldByName('Controle').AsInteger;
      Dmod.QrUpd.Params[02].AsInteger := QrLct.FieldByName('Sub').AsInteger;
      Dmod.QrUpd.ExecSQL;
    end;
    RecalcSaldoCarteira(
      QrLct.FieldByName('Carteira').AsInteger, QrCrt, QrLct, True, True);
  end;
end;

procedure TUnFinanceiro.VerificaID_Pgto_x_Compensado(const TabLctA: String;
  const LaAviso1, LaAviso2: TLabel; const PB1: TProgressBar; const QrNC: TmySQLQuery;
  var Corrigidos, NaoCorrigidos: Integer);
var
  Qry1, Qry2, Qry3: TmySQLQuery;
  Data: TDateTime;
  MyCursor: TCursor;
  Itens: Integer;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  {
  Qry1 := nil;
  Qry2 := nil;
  Qry3 := nil;
  }
  //
  MyObjects. Informa2(LaAviso1, LaAviso2, True, 'Verificando compensados');
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE IGNORE ' + TabLctA + ' SET Compensado=Data');
  Dmod.QrUpd.SQL.Add('WHERE Tipo<2 AND Compensado < 2');
  Dmod.QrUpd.ExecSQL;
  //
  Qry1 := TmySQLQuery.Create(Dmod);
  Qry1.Close;
  Qry1.Database := Dmod.MyDB;
  Qry1.SQL.Clear;
  Qry1.SQL.Add('SELECT Data, Vencimento, Controle, CtrlQuitPg');
  Qry1.SQL.Add('FROM ' + TabLctA + '');
  Qry1.SQL.Add('WHERE Sit > 1');
  Qry1.SQL.Add('AND Compensado < 2');
  Qry1.SQL.Add('AND Tipo=2');
  Qry1.SQL.Add('ORDER BY DATA');
  UnDmkDAC_PF.AbreQuery(Qry1, Dmod.MyDB);
  //
  if PB1 <> nil then
  begin
    PB1.Max := Qry1.RecordCount;
    PB1.Position := 0;
  end;
  //
  {
  Corrigidos    := 0;
  NaoCorrigidos := 0;
  }
  Itens         := Qry1.RecordCount;
  if Itens > 0 then
  begin
    Qry2 := TmySQLQuery.Create(Dmod);
    Qry2.Close;
    Qry2.Database := Dmod.MyDB;
    Qry2.SQL.Clear;
    Qry2.SQL.Add('SELECT Data');
    Qry2.SQL.Add('FROM ' + TabLctA + ' WHERE');
    Qry2.SQL.Add('Controle=:P0');
    //
    Qry3 := TmySQLQuery.Create(Dmod);
    Qry3.Close;
    Qry3.Database := Dmod.MyDB;
    Qry3.SQL.Clear;
    Qry3.SQL.Add('SELECT Data');
    Qry3.SQL.Add('FROM ' + TabLctA + ' WHERE');
    Qry3.SQL.Add('ID_Pgto=:P0');
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + TabLctA + ' SET Compensado=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Qry1.First;
    while not Qry1.Eof do
    begin
      if Qry1.FieldByName('CtrlQuitPg').AsInteger > 0 then
      begin
        Qry2.Close;
        Qry2.Params[00].AsInteger := Qry1.FieldByName('CtrlQuitPg').AsInteger;
        UnDmkDAC_PF.AbreQuery(Qry2, Dmod.MyDB);
        Data := Qry2.FieldByName('Data').AsDateTime;
        if Data > 1 then
        begin
          if PB1 <> nil then
          begin
            PB1.Position := PB1.Position + 1;
            PB1.Update;
            Application.ProcessMessages;
          end;
          Dmod.QrUpd.Params[00].AsString  := Geral.FDT(Data, 1);
          Dmod.QrUpd.Params[01].AsInteger := Qry1.FieldByName('Controle').AsInteger;
          Dmod.QrUpd.ExecSQL;
        end;
      end else
      begin
        Qry3.Close;
        Qry3.Params[00].AsInteger := Qry1.FieldByName('Controle').AsInteger;
        UnDmkDAC_PF.AbreQuery(Qry3, Dmod.MyDB);
        Data := Qry3.FieldByName('Data').AsDateTime;
        if Data > 1 then
        begin
          if PB1 <> nil then
          begin
            PB1.Position := PB1.Position + 1;
            PB1.Update;
            Application.ProcessMessages;
          end;
          Dmod.QrUpd.Params[00].AsString  := Geral.FDT(Data, 1);
          Dmod.QrUpd.Params[01].AsInteger := Qry1.FieldByName('Controle').AsInteger;
          Dmod.QrUpd.ExecSQL;
        end else begin
          // Atualizar quita��o pelo vencimento?
        end;
      end;
      //
      Qry1.Next;
    end;
    if Qry2 <> nil then
      Qry2.Free;
    if Qry3 <> nil then
      Qry3.Free;
    Qry1.Close;
    UnDmkDAC_PF.AbreQuery(Qry1, Dmod.MyDB);
  end;
  //
  NaoCorrigidos := Qry1.RecordCount;
  Corrigidos := Itens - NaoCorrigidos;
  if Qry1.RecordCount > 0 then
  begin
    MyObjects. Informa2(LaAviso1, LaAviso2, False,
      IntToStr(Qry1.RecordCount) + ' lan�amentos n�o foram corrigidos!');
    if QrNC <> nil then
    begin
      QrNC.Close;
      UnDmkDAC_PF.AbreQueryApenas(QrNC);
    end;
  end
  else
    MyObjects. Informa2(LaAviso1, LaAviso2, False, '...');
  //
  if Qry1 <> nil then
    Qry1.Free;
  //
  Screen.Cursor := MyCursor;
end;

function TUnFinanceiro.VerificaSeLanctoEParcelamento(QueryLct: TmySQLQuery;
  var Msg: String): Boolean;
begin
  (*
  if (QueryLct.FieldByName('Reparcel').AsInteger <> 0) or
    (Pos('(Reparc.', QueryLct.FieldByName('Descricao').AsString) > 0) or
    (Pos('[Reparc]', QueryLct.FieldByName('Descricao').AsString) > 0) then
  *)
  if (QueryLct.FieldByName('Reparcel').AsInteger <> 0) then
  begin
    Msg    := 'Este lan�amento n�o pode ser editado!' + sLineBreak +
    'Motivo: O lan�amento foi parcelado!';
    Result := True;
  end else
  begin
    Msg    := '';
    Result := False;
  end;
end;

{
procedure TUnFinanceiro.ConfiguraFmTransfer;
var
  Debito, Credito: Double;
begin
  with FmTransfer do
  begin
    EdCodigo.Text := FloatToStr(DmodFin.QrTransfControle.Value);
    TPData.Date := DmodFin.QrTransfData.Value;
    Debito := 0;
    Credito := 0;
    while not DmodFin.QrTransf.Eof do
    begin
      if (DmodFin.QrTransfDebito.Value > 0) then begin
        if (Debito > 0) then
          Geral.MB_Erro('Erro. Mais de um d�bito'+sLineBreak+
          'D�bito [1]: '+Geral.TFT(FloatToStr(Debito), 2, siNegativo)+sLineBreak+
          'D�bito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTransfDebito.Value), 2, siNegativo)),
          );
        Debito := Debito + DmodFin.QrTransfDebito.Value;
        EdDocDeb.Text := FloatToStr(DmodFin.QrTransfDocumento.Value);
      end;
      if (DmodFin.QrTransfCredito.Value > 0) then begin
        if (Credito > 0) then
          Geral.MB_Erro('Erro. Mais de um d�bito'+sLineBreak+
          'Cr�dito [1]: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+sLineBreak+
          'Cr�dito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTransfCredito.Value), 2, siNegativo)),
          );
        Credito := Credito + DmodFin.QrTransfCredito.Value;
        EdDocCred.Text := FloatToStr(DmodFin.QrTransfDocumento.Value);
      end;
      EdValor.Text := Geral.TFT(FloatToStr(
      DmodFin.QrTransfDebito.Value+DmodFin.QrTransfCredito.Value), 2, siPositivo);
      if DmodFin.QrTransfDebito.Value > 0 then
      begin
        RGOrigem.ItemIndex := DmodFin.QrTransfTipo.Value;
        CBOrigem.KeyValue := DmodFin.QrTransfCarteira.Value;
      end else begin
        RGDestino.ItemIndex := DmodFin.QrTransfTipo.Value;
        CBDestino.KeyValue := DmodFin.QrTransfCarteira.Value;
      end;
      DmodFin.QrTransf.Next;
    end;
    if Credito <> Debito then
      Geral.MB_Aviso('Erro. Cr�dito diferente do d�bito'+sLineBreak+
      'Cr�dito: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+sLineBreak+
      'D�bito : '+Geral.TFT(FloatToStr(Debito), 2, siNegativo));
  end;
end;
}

procedure TUnFinanceiro.CalculaPercMultaEJuros(ValOrig, MultaVal, JuroVal: Double;
  DataNew, DataVencto: TDateTime; var ValorNew, MultaPerc, JuroPerc: Double);
var
  Dias, MultPer, JuroPer: Double;
  DataUtil: TDateTime;
begin
  DataUtil := UMyMod.CalculaDataDeposito(DataVencto);
  Dias     := Int(DataNew) - Int(DataUtil);
  //
  if (Dias >= 1) and (ValOrig > 0) then
  begin
    MultPer := (100 * MultaVal) / ValOrig;
    JuroPer := (((JuroVal * 100) / ValOrig) / Dias) * 30;
  end else
  begin
    MultPer := 0;
    JuroPer := 0;
  end;
  //
  ValorNew  := ValOrig + MultaVal + JuroVal;
  MultaPerc := MultPer;
  JuroPerc  := JuroPer;
end;

procedure TUnFinanceiro.CalculaValMultaEJuros(ValOrig: Double; DataNew,
  DataVencto: TDateTime; var MultaPerc, JuroPerc, ValorNew, MultaVal,
  JuroVal: Double; const PermitePercZero: Boolean);
  procedure ObtemMultaEJuros(const MultaPer, JuroPer: Double; var MultaP,
    JuroP: Double);
  begin
    if MultaPer = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
      'SELECT Multa FROM controle ',
      '']);
      if DModG.QrAux.RecordCount > 0 then
        MultaP := DModG.QrAux.FieldByName('Multa').AsFloat
      else
        MultaP := 2;
    end else
      MultaP := MultaPer;
    if JuroPer = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
      'SELECT MoraDD FROM controle ',
      '']);
      if DModG.QrAux.RecordCount > 0 then
        JuroP := DModG.QrAux.FieldByName('MoraDD').AsFloat
      else
        JuroP := 1;
    end else
      JuroP := JuroPer;
  end;
var
  Dias, Multa, TaxJu, Juros: Double;
  DataUtil: TDateTime;
begin
  DataUtil := UMyMod.CalculaDataDeposito(DataVencto);
  Dias     := Int(DataNew) - Int(DataUtil);
  //
  if (Dias >= 1) and (ValOrig > 0) then
  begin
    if PermitePercZero then
    begin
      Multa := MultaPerc;
      TaxJu := JuroPerc;
    end else
      ObtemMultaEJuros(MultaPerc, JuroPerc, Multa, TaxJu);
    //
    Juros := TaxJu / 30 * Dias;
    //
    MultaVal  := Multa * ValOrig / 100;
    JuroVal   := Juros * ValOrig / 100;
    ValorNew  := ValOrig + MultaVal + JuroVal;
    MultaPerc := Multa;
    JuroPerc  := TaxJu; 
  end else
  begin
    ValorNew := ValOrig;
    MultaVal := 0;
    JuroVal  := 0;
  end;
end;

procedure TUnFinanceiro.ColocarMesDeCompetenciaOndeNaoTem(
  TipoData: TDataCompetencia; QrCrt, QrLct: TmySQLQuery; TabLctA: String);
var
  Data: TDateTime;
  Ano, Mes, Dia: Word;
  Mez, Controle: Integer;
  Txt: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  Data := -1000;
  Controle := QrLct.FieldByName('Controle').AsInteger;
  if Geral.MB_Pergunta('Somente (todos) os lan�amentos presentes na grade ' +
  ' (conforme sele��o de carteira e per�odo) ser�o analisados! Deseja ' +
  'continuar assim mesmo?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    QrLct.First;
    while not QrLct.Eof do
    begin
      Application.ProcessMessages;
      DmodFin.QrLocCta.Close;
      DmodFin.QrLocCta.SQL.Clear;
      DmodFin.QrLocCta.SQL.Add('SELECT Mensal');
      DmodFin.QrLocCta.SQL.Add('FROM contas');
      DmodFin.QrLocCta.SQL.Add('WHERE Codigo=:P0');
      DmodFin.QrLocCta.Params[0].AsInteger := QrLct.FieldByName('Genero').AsInteger;
      UnDmkDAC_PF.AbreQuery(DmodFin.QrLocCta, Dmod.MyDB);
      if (Uppercase(DmodFin.QrLocCtaMensal.Value) = 'V') then
      begin
        case TipoData of
          dcData                : Data := QrLct.FieldByName('Data').AsDateTime;
          dcVencimento          : Data := QrLct.FieldByName('Vencimento').AsDateTime;
          dcMesAnteriorAoVencto : Data := QrLct.FieldByName('Vencimento').AsDateTime;
          dcDoisMesesAntes      : Data := QrLct.FieldByName('Vencimento').AsDateTime;
          else begin
            Geral.MB_Erro('Tipo de data n�o definido! AVISE A DERMATEK'
              );
            Screen.Cursor := crHourGlass;
            Exit;
          end;
        end;
        DecodeDate(Data, Ano, Mes, Dia);
        while Ano > 100 do Ano := Ano - 100;
        if TipoData = dcMesAnteriorAoVencto then
        begin
          if Mes = 1 then
          begin
            Ano := Ano -1;
            Mes := 12;
          end else Mes := Mes -1;
        end;
        if TipoData = dcDoisMesesAntes then
        begin
          if Mes = 1 then
          begin
            Ano := Ano -1;
            Mes := 11;
          end else
          if Mes = 2 then
          begin
            Ano := Ano -1;
            Mes := 12;
          end else Mes := Mes -1;
        end;
        Mez := Ano * 100 + Mes;
      end else Mez := 0;
      //
      SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['Mez'], ['Controle'], [
      Mez], [QrLct.FieldByName('Controle').AsInteger], True, '', TabLctA);
      //
      Txt := Txt + FormatFloat('0000000000',
        QrLct.FieldByName('Controle').AsInteger) + ' - '  + sLineBreak;
      //
      QrLct.Next;
    end;
    QrLct.Close;
    UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
    QrLct.Locate('Controle', Controle, []);
    Screen.Cursor := crDefault;
  end;
  if Txt <> '' then
  begin
    Txt := 'LAN�AMENTO - ' + sLineBreak + Txt;
    Geral.MB_Info(Txt);
  end;
end;

procedure TUnFinanceiro.ColocarUHOndeNaoTem(QrCrt, QrLct: TmySQLQuery;
DBGLct: TDBGrid; TabLctA: String);
var
  Txt: String;
  Controle: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  Txt := '';
  Controle := QrLct.FieldByName('Controle').AsInteger;
  if Geral.MB_Pergunta('Somente os lan�amentos presentes na grade ' +
  ' (conforme sele��o de carteira e per�odo) ser�o analisados! Deseja ' +
  'continuar assim mesmo?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    QrLct.First;
    while not QrLct.Eof do
    begin
      Application.ProcessMessages;
      if (QrLct.FieldByName('Cliente').AsInteger > 0) and
      (QrLct.FieldByName('Depto').AsInteger = 0) then
      begin
        if (Uppercase(Application.Title) = 'SYNDIC')
        or (Uppercase(Application.Title) = 'SYNKER')
        then begin
          DModFin.QrLocUHs.Close;
          DModFin.QrLocUHs.Params[0].AsInteger := QrLct.FieldByName('Cliente').AsInteger;
          UnDmkDAC_PF.AbreQuery(DModFin.QrLocUHs, Dmod.MyDB);
          //
          case DModFin.QrLocUHs.RecordCount of
            0: ;// nada
            1:
            begin
              SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
              'Depto'], ['Controle'], [DModFin.QrLocUHsDepto.Value], [
              QrLct.FieldByName('Controle').AsInteger], True, '', TabLctA);
              Txt := Txt + FormatFloat('0000000000',
                QrLct.FieldByName('Controle').AsInteger) + ' - ' +
                Geral.CompletaString(DModFin.QrLocUHsNO_DEPTO.Value, ' ', 30,
                  taLeftJustify, False) + ' - ' + sLineBreak;
            end;
            else Geral.MB_Aviso('O lan�amento ' + IntToStr(
            QrLct.FieldByName('Controle').AsInteger) + ' ficou sem atualiza��o ' +
            'porque o ' + DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O) +
            ' tem mais de uma ' + DModG.ReCaptionTexto(VAR_U_H) +
            ' no aplicativo!');
          end;
        end else begin
          Geral.MB_Aviso('O aplicativo "' + Application.Title +
          '" n�o possui implmenta��o para este procedimento!');
          //
          Exit;
        end;
      end;
      QrLct.Next;
    end;
    QrLct.Close;
    UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
    QrLct.Locate('Controle', Controle, []);
    Screen.Cursor := crDefault;
  end;
  if Txt <> '' then
  begin
    Txt := 'LAN�AMENTO - UNIDADE' + sLineBreak + Txt;
    Geral.MB_Info(Txt);
  end;
end;

procedure TUnFinanceiro.ConciliacaoBancaria(QrCrt, QrLct: TmySQLQuery;
TPDataIni: TdmkEditDateTimePicker; NomeCliInt: String; Entidade, CliInt,
CartConcilia: Integer; TabLctA: String; ModuleLctX: TDataModule);
begin
{$IfNDef NAO_CMEB}
  if QrCrt.FieldByName('Tipo').AsInteger = 1 then
  begin
    if DBCheck.CriaFm(TFmConcilia, FmConcilia, afmoNegarComAviso) then
    begin
      FmConcilia.FModuleLctX   := ModuleLctX;
      FmConcilia.FTabLctA      := TabLctA;
      FmConcilia.ReopenConcilia(0);
      //
      FmConcilia.FNomeCliInt   := NomeCliInt;
      FmConcilia.FQrLct        := QrLct;
      FmConcilia.FQrCrt        := QrCrt;
      FmConcilia.F_CliInt      := Entidade;
      FmConcilia.FCondominio   := CliInt;
      FmConcilia.FCartConcilia := CartConcilia;
      FmConcilia.FDTPicker     := TPDataIni;
      FmConcilia.FMostra       := (FmConcilia.QrConcilia0.State <> dsInactive)
                              and (FmConcilia.QrConcilia0.RecordCount > 0);
      //
      DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, FmConcilia.FDtEncer, FmConcilia.FDtMorto,
        FmConcilia.FTabLctA, FmConcilia.FTabLctB, FmConcilia.FTabLctD);
      //
      FmConcilia.ShowModal;
      FmConcilia.Destroy;
    end;
  end else Geral.MB_Aviso('Carteira deve ser de extrato banc�rio!');
{$Else}
  Grl_Geral.InfoSemModulo(mdlappConciBco);
{$EndIf}
end;

procedure TUnFinanceiro.ConfiguraFmTransferCart();
var
  Debito, Credito: Double;
begin
  with FmTransfer2 do
  begin
    EdCodigo.Text := FloatToStr(DmodFin.QrTransfControle.Value);
    TPData.Date := DmodFin.QrTransfData.Value;
    Debito := 0;
    Credito := 0;
    while not DmodFin.QrTransf.Eof do
    begin
      if (DmodFin.QrTransfDebito.Value > 0) then begin
        if (Debito > 0) then
          Geral.MB_Erro('Erro. Mais de um d�bito'+sLineBreak+
          'D�bito [1]: '+Geral.TFT(FloatToStr(Debito), 2, siNegativo)+sLineBreak+
          'D�bito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTransfDebito.Value), 2, siNegativo));
        Debito := Debito + DmodFin.QrTransfDebito.Value;
        EdDocDeb.Text := FloatToStr(DmodFin.QrTransfDocumento.Value);
      end;
      if (DmodFin.QrTransfCredito.Value > 0) then begin
        if (Credito > 0) then
          Geral.MB_Erro('Erro. Mais de um d�bito'+sLineBreak+
          'Cr�dito [1]: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+sLineBreak+
          'Cr�dito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTransfCredito.Value), 2, siNegativo)
          );
        Credito := Credito + DmodFin.QrTransfCredito.Value;
        EdDocCred.Text := FloatToStr(DmodFin.QrTransfDocumento.Value);
      end;
      EdValor.Text := Geral.TFT(FloatToStr(
      DmodFin.QrTransfDebito.Value+DmodFin.QrTransfCredito.Value), 2, siPositivo);
      if DmodFin.QrTransfDebito.Value > 0 then
      begin
        EdOrigem.ValueVariant := DmodFin.QrTransfCarteira.Value;
        CBOrigem.KeyValue     := DmodFin.QrTransfCarteira.Value;
        EdSerieChDeb.Text     := DmodFin.QrTransfSerieCH.Value;
        TPOldData1.Date        := DmodFin.QrTransfData.Value;
        EdOldTipo1.ValueVariant     := DmodFin.QrTransfTipo.Value;
        EdOldCarteira1.ValueVariant := DmodFin.QrTransfCarteira.Value;
        EdOldControle1.ValueVariant := DmodFin.QrTransfControle.Value;
        EdOldSub1.ValueVariant      := DmodFin.QrTransfSub.Value;
        EdDuplicataDeb.Text         := DmodFin.QrTransfDuplicata.Value;
      end else begin
        EdDestino.ValueVariant := DmodFin.QrTransfCarteira.Value;
        CBDestino.KeyValue     := DmodFin.QrTransfCarteira.Value;
        EdSerieChCred.Text     := DmodFin.QrTransfSerieCH.Value;
        TPOldData2.Date        := DmodFin.QrTransfData.Value;
        EdOldTipo2.ValueVariant     := DmodFin.QrTransfTipo.Value;
        EdOldCarteira2.ValueVariant := DmodFin.QrTransfCarteira.Value;
        EdOldControle2.ValueVariant := DmodFin.QrTransfControle.Value;
        EdOldSub2.ValueVariant      := DmodFin.QrTransfSub.Value;
        EdDuplicataCred.Text        := DmodFin.QrTransfDuplicata.Value;
      end;
      DmodFin.QrTransf.Next;
    end;
    if Credito <> Debito then
      Geral.MB_Aviso('Erro. Cr�dito diferente do d�bito'+sLineBreak+
      'Cr�dito: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+sLineBreak+
      'D�bito : '+Geral.TFT(FloatToStr(Debito), 2, siNegativo));
  end;
end;

procedure TUnFinanceiro.ConfiguraFmTransferCtas;
var
  Debito, Credito: Double;
begin
  with FmTransferCtas do
  begin
    EdCodigo.Text := FloatToStr(DmodFin.QrTrfCtasControle.Value);
    TPData.Date := DmodFin.QrTrfCtasData.Value;
    Debito := 0;
    Credito := 0;
    while not DmodFin.QrTrfCtas.Eof do
    begin
      if (DmodFin.QrTrfCtasDebito.Value > 0) then begin
        if (Debito > 0) then
          Geral.MB_Erro('Erro. Mais de um d�bito'+sLineBreak+
          'D�bito [1]: '+Geral.TFT(FloatToStr(Debito), 2, siNegativo)+sLineBreak+
          'D�bito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTrfCtasDebito.Value), 2, siNegativo)
          );
        Debito := Debito + DmodFin.QrTrfCtasDebito.Value;
        EdDocDeb.Text := FloatToStr(DmodFin.QrTrfCtasDocumento.Value);
      end;
      if (DmodFin.QrTrfCtasCredito.Value > 0) then begin
        if (Credito > 0) then
          Geral.MB_Erro('Erro. Mais de um d�bito'+sLineBreak+
          'Cr�dito [1]: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+sLineBreak+
          'Cr�dito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTrfCtasCredito.Value), 2, siNegativo));
        Credito := Credito + DmodFin.QrTrfCtasCredito.Value;
        EdDocCred.Text := FloatToStr(DmodFin.QrTrfCtasDocumento.Value);
      end;
      EdValor.Text := Geral.TFT(FloatToStr(
      DmodFin.QrTrfCtasDebito.Value+DmodFin.QrTrfCtasCredito.Value), 2, siPositivo);
      if DmodFin.QrTrfCtasDebito.Value > 0 then
      begin
        EdOrigem.ValueVariant  := DmodFin.QrTrfCtasCarteira.Value;
        CBOrigem.KeyValue      := DmodFin.QrTrfCtasCarteira.Value;
        EdCtaDebt.ValueVariant := DmodFin.QrTrfCtasGenero.Value;
        CBCtaDebt.KeyValue     := DmodFin.QrTrfCtasGenero.Value;
        EdSerieChDeb.Text      := DmodFin.QrTrfCtasSerieCH.Value;
        TPOldData1.Date             := DmodFin.QrTrfCtasData.Value;
        EdOldTipo1.ValueVariant     := DmodFin.QrTrfCtasTipo.Value;
        EdOldCarteira1.ValueVariant := DmodFin.QrTrfCtasCarteira.Value;
        EdOldControle1.ValueVariant := DmodFin.QrTrfCtasControle.Value;
        EdOldSub1.ValueVariant      := DmodFin.QrTrfCtasSub.Value;
      end else begin
        EdDestino.ValueVariant := DmodFin.QrTrfCtasCarteira.Value;
        CBDestino.KeyValue     := DmodFin.QrTrfCtasCarteira.Value;
        EdCtaCred.ValueVariant := DmodFin.QrTrfCtasGenero.Value;
        CBCtaCred.KeyValue     := DmodFin.QrTrfCtasGenero.Value;
        EdSerieChCred.Text     := DmodFin.QrTrfCtasSerieCH.Value;
        TPOldData2.Date             := DmodFin.QrTrfCtasData.Value;
        EdOldTipo2.ValueVariant     := DmodFin.QrTrfCtasTipo.Value;
        EdOldCarteira2.ValueVariant := DmodFin.QrTrfCtasCarteira.Value;
        EdOldControle2.ValueVariant := DmodFin.QrTrfCtasControle.Value;
        EdOldSub2.ValueVariant      := DmodFin.QrTrfCtasSub.Value;
      end;
      DmodFin.QrTrfCtas.Next;
    end;
    if Credito <> Debito then
      Geral.MB_Aviso('Erro. Cr�dito diferente do d�bito'+sLineBreak+
      'Cr�dito: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+sLineBreak+
      'D�bito : '+Geral.TFT(FloatToStr(Debito), 2, siNegativo));
  end;
end;

procedure TUnFinanceiro.ReopenCarteirasPgto(Query: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Codigo, Nome, Tipo ',
    'FROM carteiras ',
    'WHERE Tipo IN(0,1) ',
    'AND ForneceI=' + Geral.FF0(DModG.EmpresaAtual_ObtemCodigo(tecEntidade)),
    'AND Codigo > 0 ',
    'AND Ativo = 1 ',
    'ORDER BY Nome ',
    '']);
end;

procedure TUnFinanceiro.ConfiguraGBCarteiraPgto(GBCarteira: TGroupBox;
  EditCarteira: TdmkEditCB; ComboBoxCarteira: TdmkDBLookupComboBox;
  Carteira: Integer);
var
  Qry: TmySQLQuery;
begin
  GBCarteira.Visible := False;
  //
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Banco, MudaBco ',
      'FROM carteiras ',
      'WHERE Codigo=' + Geral.FF0(Carteira),
      '']);
    //
    GBCarteira.Visible        := Qry.FieldByName('MudaBco').AsInteger = 1;
    EditCarteira.ValueVariant := Qry.FieldByName('Banco').AsInteger;
    ComboBoxCarteira.KeyValue := Qry.FieldByName('Banco').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnFinanceiro.CorrigeMez(TabLctA: String; Avisa: Boolean): Integer;
var
  Qry: TmySQLQuery;
  Mez: Integer;
  //
  Tipo, Carteira, Sub: Integer;
  Data, CtrlTxt: String;
  Controle: Int64;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + TabLctA,
    'WHERE Mez BETWEEN 20000000 AND 29999999 ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      Mez := Qry.FieldByName('Mez').AsInteger;
      Mez := Mez - 20000000;
      Mez := Mez div 100;
      //
      Data      := Geral.FDT(Qry.FieldByName('Data').AsDateTime, 1);
      Tipo      := Qry.FieldByName('Tipo').AsInteger;
      Carteira  := Qry.FieldByName('Carteira').AsInteger;
      Controle  := Qry.FieldByName('Controle').AsLargeInt;
      Sub       := Qry.FieldByName('Sub').AsInteger;
      //
      if SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Mez'], [
      'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
      Mez], [
      Data, Tipo, Carteira, Controle, Sub], True, '',TabLctA) then
        Result := Result + 1;
        //
      Qry.Next;
    end;
    if Avisa then
    begin
      if Result > 0 then
        Geral.MB_Aviso(Geral.FF0(Result) +
        ' itens tiveram seu m�s de compet�ncia corrigido!');
    end;
  finally
    Qry.Free;
  end;
end;

function TUnFinanceiro.TabLctNaoDef(TabLct: String; Avisa: Boolean): Boolean;
begin
  Result := True;
  //
  if (TabLct = sTabLctErr) then
  begin
    if Avisa then
      Geral.MB_Aviso(
      'Cliente interno sem cadastro em tabela de clientes internos!');
    Exit;
  end;
  if (TabLct = '') then
  begin
    if Avisa then
      Geral.MB_Aviso(
      'Cliente interno sem tabelas de lan�amentos financeiros definidas!');
    Exit;
  end;
  //
  Result := False;
end;

function TUnFinanceiro.TabLctNaoDefinida(TabLct, FunctionOrigem: String): Boolean;
begin
  Result := Trim(TabLct) <> '';
  if not Result then
  begin
    Geral.MB_Erro('Tabela de lan�amentos n�o definida!' + sLineBreak +
    FunctionOrigem + sLineBreak + 'AVISE A DERMATEK!');
    // Provis�rio?
    Halt(0);
  end;
end;

function TUnFinanceiro.TextoFinalidadeLancto(Finalidade: TLanctoFinalidade): String;
  function Frase(Numero: Integer; Texto: String): String;
  begin
    Result := FormatFloat('000', Numero) +
      ' - Inclus�o / altera��o de lan�amentos ' + Texto;
  end;
begin
  case Finalidade of
    lfProprio:    Result := Frase(1, 'pr�prios');
    lfCondominio: Result := Frase(2, 'de condom�nios');
    lfCicloCurso: Result := Frase(3, 'de cursos c�clicos');
    else          Result := Frase(0, 'de finalidade desconhecida');
  end;
end;

function TUnFinanceiro.CriaLanctoEditor(InstanceClass: TComponentClass; var Reference;
ModoAcesso: TAcessFmModo; QrSelect: TmySQLQuery; Controle, Sub: Integer;
Finalidade: TLanctoFinalidade): Boolean;
  procedure AvisaFalta(Nome: String);
  begin
    Geral.MB_Erro('O componente ' + Nome + ' n�o foi setado!');
  end;
var
  MyLabel: TLabel;
begin
  Result := False;
  if not DBCheck.CriaFm(InstanceClass, Reference, ModoAcesso) then Exit;
  with TForm(Reference) do
  begin
    if (FindComponent('LaFinalidade') as TLabel) <> nil then
      MyLabel := FindComponent('LaFinalidade') as TLabel
    else MyLabel := nil;
    if MyLabel <> nil then
      MyLabel.Caption := TextoFinalidadeLancto(Finalidade);
    //
    Result := True;
  end;
end;

{function TUnFinanceiro.InsAltReopenLct(f : TInsAltReopenLct): Boolean;
begin
  Result := f;
end;
}

function TUnFinanceiro.InclusaoLancamento((*InstanceClass: TComponentClass;
  var Reference;*) Finalidade: TLanctoFinalidade; ModoAcesso: TAcessFmModo; QrLct,
  QrCrt: TmySQLQuery; Acao: TGerencimantoDeRegistro; Controle, Sub,
  Genero: Integer; PercJuroM, PercMulta: Double; SetaVars: TInsAltReopenLct;
  FatID, FatID_Sub, FatNum, Carteira: Integer; Credito, Debito: Double;
  AlteraAtehFatID: Boolean; Cliente, Fornecedor, CliInt, ForneceI, Account,
  Vendedor: Integer; LockCliInt, LockForneceI, LockAccount,
  LockVendedor: Boolean; Data, Vencto, DataDoc: TDateTime;
  IDFinalidade: Integer; Mes: TDateTime; TabLctA: String;
  // 2012-07-17
  FisicoSrc, FisicoCod: Integer): Integer;

const
  sProcName = 'TUnFinanceiro.InclusaoLancamento()';

  function ObtemFatIDDaOrigem(ID_Pgto, Sub: Integer): Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrAux, Dmod.MyDB, [
      'SELECT FatID ',
      'FROM lct0001a ',
      'WHERE Controle=' + Geral.FF0(ID_Pgto),
      'AND Sub=' + Geral.FF0(Sub),
      '']);
    if DModG.QrAux.RecordCount > 0 then
      Result := DModG.QrAux.FieldByName('FatID').AsInteger
    else
      Result := 0;
  end;

var
  //Txt_LaTipo,
  SQLType: TSQLType;
  Txt_Ctrl, Campo: String;
  MyCursor: TCursor;
  i, Sit, Tipo: Integer;
  PropInfo: PPropInfo;
  // ini 2022-03-23
  Reference: TForm;
  _GenCtbD, _GenCtbC, _TpLct: Integer;
begin
  Result  := 0;
  SQLType := stLok;
  //
  VLAN_QRCARTEIRAS := QrCrt;
  VLAN_QRLCT       := QrLct;
  //if VLAN_QRCARTEIRAS = nil then
    //Geral.MB_Erro('"VLAN_QRCARTEIRAS" n�o definido em "InsAltLancto"!',
    //);
  if Acao = tgrAltera then
  begin
    if (Data > 2) and (Data < VAR_DATA_MINIMA) then
    begin
      Geral.MB_Aviso('Lan�amento Encerrado. Para editar '+
      'este item desfa�a o encerramento do m�s correspondente!');
      Exit;
    end;
    //
    if UMyMod.SelLockInt64Y(Controle, Dmod.MyDB, TabLctA, 'Controle') then
      Exit;
    if (AlteraAtehFatID = False) and (QrLct <> nil)
    and (QrLct.FieldByName('FatID').AsInteger <> 0 ) then
    begin
      Geral.MB_Aviso('Lan�amento espec�fico. Para editar este item selecione sua janela correta! [1]');
      //
      if VAR_SENHA <> CO_MASTER then
      begin
        if not DBCheck.LiberaPelaSenhaBoss then
          Exit;
      end;
      Geral.MB_Aviso('CUIDADO!!! Lan�amento espec�fico. Dados do lan�amento podem ser perdidos ao editar! [1]');

    end;
    if (AlteraAtehFatID = False) and (QrLct <> nil) then
    begin
      if ObtemFatIDDaOrigem(QrLct.FieldByName('ID_Pgto').AsInteger,
        QrLct.FieldByName('Sub').AsInteger) <> 0 then
      begin
        Geral.MB_Aviso('Lan�amento espec�fico. Para editar este item selecione sua janela correta! [2]');
        //
        if VAR_SENHA <> CO_MASTER then
        begin
          if not DBCheck.LiberaPelaSenhaBoss then
            Exit;
        end;
        Geral.MB_Aviso('CUIDADO!!! Lan�amento espec�fico. Dados do lan�amento podem ser perdidos ao editar! [2]');
      end;
    end;
  end else
  if Acao <> tgrDuplica then
  begin
    Controle := 0;
    Sub := 0;
    // N�o tem! ID_Pgto := 0;
  end;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if Acao = tgrAltera then
    begin
      UMyMod.UpdLockInt64Y(Controle, Dmod.MyDB, TabLctA, 'Controle');
      if (Acao = tgrTeste) and (Genero < 0) then
      begin
        if Acao = tgrTeste then Result := -1
        else begin
          case Genero of
            -1: UFinanceiro.CriarTransferCart(1, QrLct, QrCrt, True,
              CliInt, FatID, FatID_Sub, FatNum, TabLctA);
            else
            begin
              Geral.MB_Aviso('Lan�amento protegido. Para editar '+
              'este item selecione seu caminho correto!');
              Exit;
            end;
          end;
        end;
        Screen.Cursor := crDefault;
        Exit;
      end;
      if (Acao = tgrTeste) and (Genero = 0) and (Controle = 0) then
      begin
        if Acao = tgrTeste then Result := -2 else
        Geral.MB_Aviso('N�o h� dados para editar');
        Screen.Cursor := MyCursor;
        Screen.Cursor := crDefault;
        Exit;
      end;
      (*
      if (Cartao > 0) and not VAR_FATURANDO then
      begin
        if SoTestar then Result := -3 else
        Geral.MB_Aviso('Esta emiss�o n�o pode ser editada pois '+
        'pertence a uma fatura!'));
        Exit;
      end;
      *)
      if (Genero = -1) then
      begin
        Geral.MB_Aviso('Esta emiss�o n�o pode ser editada pois '+
        'pertence a transfer�ncia entre carteiras!');
        Screen.Cursor := crDefault;
        Exit;
      end;
      if (Genero > 0) and (Sub > 0) then
      begin
        Geral.MB_Aviso('Esta emiss�o n�o pode ser editada pois '+
        'pertence a transfer�ncia entre contas!');
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    if Acao = tgrTeste then
    begin
      Result := 1;
      Screen.Cursor := crDefault;
      Exit;
    end;
    if Finalidade = lfUnknow then
    begin
      Geral.MB_Aviso(
      'N�o foi definida nenhuma finalidade para o lan�amento. AVISE A DERMATEK!');
      Screen.Cursor := crDefault;
      Exit;
    end;
    case DModG.QrCtrlGeralUsarFinCtb.Value of
      0: // s� financeiro
      begin
        if not CriaLanctoEditor(TFmLctEdit, FmLctEdit, ModoAcesso, QrLct,
        Controle, Sub, Finalidade) then
        begin
          Screen.Cursor := crDefault;
          Exit;
        end;
        Reference := FmLctEdit;
      end;
      1: // Financeiro + Cont�bil
      begin
        if not CriaLanctoEditor(TFmLctEdit2, FmLctEdit2, ModoAcesso, QrLct,
        Controle, Sub, Finalidade) then
        begin
          Screen.Cursor := crDefault;
          Exit;
        end;
        Reference := FmLctEdit2;
      end;
      else
      begin
        Geral.MB_Erro('Tipo de financeiro n�o definido em ' + sProcName);
        Exit;
      end;
    end;
    //
    try
    try
      if Controle <> 0 then
      begin
        LancamentoDuplica(DMod.QrAux, Controle, Sub, TabLctA);
        with TForm(Reference) do
        begin
          for i := 0 to ComponentCount -1 do
          begin
            PropInfo := GetPropInfo(TComponent(Components[i]).ClassInfo, 'QryCampo');
            if PropInfo <> nil then
            begin
              Campo := GetStrProp(TComponent(Components[i]), PropInfo);
              //if Lowercase(Campo) = 'genctb' then
                //Geral.MB_Teste(Campo);
              if Campo <> '' then
              begin
                if (Components[i] is TdmkEdit) then
                begin
                  if TdmkEdit(Components[i]).FormatType = dmktfMesAno then
                    TdmkEdit(Components[i]).Text :=
                    Geral.MesEAnoDoMez(
                    TmySQLQuery(QrLct).FieldByName(Campo).AsString)
                  else
                  if TdmkEdit(Components[i]).FormatType = dmktf_AAAAMM then
                    TdmkEdit(Components[i]).Text :=
                    Geral.AnoEMesDoMez(
                    TmySQLQuery(QrLct).FieldByName(Campo).AsString)
                  else
                  TdmkEdit(Components[i]).ValueVariant :=
                  TmySQLQuery(QrLct).FieldByName(Campo).AsVariant
                end
                //
                else if (Components[i] is TdmkDBLookupCombobox) then
                  TdmkDBLookupCombobox(Components[i]).KeyValue :=
                  TmySQLQuery(QrLct).FieldByName(Campo).AsVariant
                //
                else if (Components[i] is TdmkEditCB) then
                  TdmkEditCB(Components[i]).ValueVariant :=
                  TmySQLQuery(QrLct).FieldByName(Campo).AsVariant
                //
                else if (Components[i] is TdmkEditDateTimePicker) then
                  TdmkEditDateTimePicker(Components[i]).Date :=
                  TmySQLQuery(QrLct).FieldByName(Campo).AsDateTime
                //
                else if (Components[i] is TdmkPopOutFntCBox) then
                  TdmkPopOutFntCBox(Components[i]).FonteNome :=
                  TmySQLQuery(QrLct).FieldByName(Campo).AsVariant
                //
                else if (Components[i] is TdmkRadioGroup) then
                  TdmkRadioGroup(Components[i]).ItemIndex :=
                  TmySQLQuery(QrLct).FieldByName(Campo).AsVariant
                //
                else if (Components[i] is TdmkValUsu) then
                  TdmkValUsu(Components[i]).ValueVariant :=
                  TmySQLQuery(QrLct).FieldByName(Campo).AsVariant;
              //
              end;
            end;
          end;
        end;
      end;
      with TForm(Reference) do
      begin
        // Parei Aqui! Mudar para Finalidade: TLanctoFinalidade
        MyVCLref.SET_Component(TForm(Reference), 'LaFinalidade', 'Caption', FormatFloat('000', IDFinalidade), TLabel);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdOneCliente', 'ValueVariant', Cliente, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdOneFornecedor', 'ValueVariant', Fornecedor, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdOneCliInt', 'ValueVariant', CliInt, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdOneForneceI', 'ValueVariant', ForneceI, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdOneAccount', 'ValueVariant', Account, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdOneVendedor', 'ValueVariant', Vendedor, TdmkEdit);
        //
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdCBCliInt', 'Enabled', not LockCliInt, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkCBCliInt', 'Enabled', not LockCliInt, TdmkDBLookupCombobox);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdCBForneceI', 'Enabled', not LockForneceI, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdCBAccount', 'Enabled', not LockAccount, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdCBVendedor', 'Enabled', not LockVendedor, TdmkEdit);
        //
        MyVCLref.SET_Component(TForm(Reference), 'LaCliInt', 'Enabled', not LockCliInt, TLabel);
        MyVCLref.SET_Component(TForm(Reference), 'LaForneceI', 'Enabled', not LockForneceI, TLabel);
        MyVCLref.SET_Component(TForm(Reference), 'LaAccount', 'Enabled', not LockAccount, TLabel);
        MyVCLref.SET_Component(TForm(Reference), 'LaVendedor', 'Enabled', not LockVendedor, TLabel);
        //
        MyVCLref.SET_Component(TForm(Reference), 'dmkCBCliInt', 'Enabled', not LockCliInt, TDBLookupComboBox);
        MyVCLref.SET_Component(TForm(Reference), 'dmkCBForneceI', 'Enabled', not LockForneceI, TDBLookupComboBox);
        MyVCLref.SET_Component(TForm(Reference), 'dmkCBAccount', 'Enabled', not LockAccount, TDBLookupComboBox);
        MyVCLref.SET_Component(TForm(Reference), 'dmkCBVendedor', 'Enabled', not LockVendedor, TDBLookupComboBox);
        //
        if Acao = tgrInclui then
        begin
          //Txt_LaTipo := Geral.LaTipo(stIns);
          SQLType := stIns;

          MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference),
            'dmkEdTPCompensado', 'Date', 0);
          MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference),
            'dmkEdTPData', 'Date', Int(Date));
          MyVCLref.SET_dmkEdit(TForm(Reference), 'dmkEdFatID', 'ValueVariant', FatID);
          MyVCLref.SET_dmkEdit(TForm(Reference), 'dmkEdFatID_Sub', 'ValueVariant', FatID_Sub);
          MyVCLref.SET_dmkEdit(TForm(Reference), 'dmkEdFatNum', 'ValueVariant', FatNum);

          if Data > 1 then
            MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference), 'dmkEdTPData', 'Date', Data);
          if Vencto > 1 then
            MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference), 'dmkEdTPVencto', 'Date', Vencto);
          if DataDoc > 1 then
            MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference), 'dmkEdTPDataDoc', 'Date', DataDoc);

          MyVCLref.SET_dmkEditCB(TForm(Reference), 'dmkEdCBFornece', 'ValueVariant', Fornecedor);
          MyVCLref.SET_dmkDBLookupComboBox(TForm(Reference), 'dmkCBFornece', 'KeyValue', Fornecedor);

          MyVCLref.SET_dmkEditCB(TForm(Reference), 'dmkEdCBGenero', 'ValueVariant', Genero);
          MyVCLref.SET_dmkDBLookupComboBox(TForm(Reference), 'dmkCBGenero', 'KeyValue', Genero);


          MyVCLref.SET_dmkEditCB(TForm(Reference), 'dmkEdCBCliente', 'ValueVariant', Cliente);
          MyVCLref.SET_dmkDBLookupComboBox(TForm(Reference), 'dmkCBCliente', 'KeyValue', Cliente);

          MyVCLref.SET_dmkEditCB(TForm(Reference), 'dmkEdCarteira', 'ValueVariant', Carteira);
          MyVCLref.SET_dmkDBLookupComboBox(TForm(Reference), 'dmkCBCarteira', 'KeyValue', Carteira);

          //MyVCLref.SET_dmkEditCB(TForm(Reference), 'EdGenCtb', 'ValueVariant', GenCtb);
          //MyVCLref.SET_dmkDBLookupComboBox(TForm(Reference), 'CBGenCtb', 'KeyValue', GenCtb);

          MyVCLref.SET_dmkEdit(TForm(Reference), 'dmkEdDeb', 'ValueVariant', Debito);
          MyVCLref.SET_dmkEdit(TForm(Reference), 'dmkEdCred', 'ValueVariant', Credito);
          if Mes > 2 then
            MyVCLref.SET_dmkEdit(TForm(Reference), 'dmkEdMes', 'ValueVariant', Mes);

          MyVCLref.SET_Component(TForm(Reference), 'RGFisicoSrc', 'ItemIndex', FisicoSrc, TdmkRadioGroup);
          MyVCLref.SET_Component(TForm(Reference), 'EdFisicoCod', 'ValueVariant', FisicoCod, TdmkEdit);

          // N�o se sabe ainda?
          Sit  := -1;
          Tipo := -1;
          // 2014-04-13 Liberar apenas para inclusao. Alterar carteira gera erro de compensacao em relatorios
          MyVCLref.SET_dmkEditCB(TForm(Reference), 'dmkEdCarteira', 'Enabled', True);
          MyVCLref.SET_dmkDBLookupComboBox(TForm(Reference), 'dmkCBCarteira', 'Enabled', True);
          MyVCLref.SET_Label(TForm(Reference), 'LaCarteira', 'Enabled', True);
          MyVCLref.SET_Component(TForm(Reference), 'SbCarteira', 'Enabled', True, TSpeedButton);
          //
        end else
        begin
          Sit  := TmySQLQuery(QrLct).FieldByName('Sit').AsInteger;
          Tipo := TmySQLQuery(QrLct).FieldByName('Tipo').AsInteger;
          //
          case Acao of
            tgrAltera:
            begin
              //Txt_LaTipo := CO_ALTERACAO;
              SQLType := stUpd;
              MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference), 'TPOldData', 'Date',
                TmySQLQuery(QrLct).FieldByName('Data').AsDateTime);
              MyVCLref.SET_dmkEdit(TForm(Reference), 'EdOldControle', 'ValueVariant',
                TmySQLQuery(QrLct).FieldByName('Controle').AsFloat);
              MyVCLref.SET_dmkEdit(TForm(Reference), 'EdOldSub', 'ValueVariant',
                TmySQLQuery(QrLct).FieldByName('Sub').AsInteger);
              MyVCLref.SET_dmkEdit(TForm(Reference), 'EdOldTipo', 'ValueVariant',
                TmySQLQuery(QrLct).FieldByName('Tipo').AsInteger);
              MyVCLref.SET_dmkEdit(TForm(Reference), 'EdOldCarteira', 'ValueVariant',
                TmySQLQuery(QrLct).FieldByName('Carteira').AsInteger);
              // ini 2022-03-24
              try //Habilitar?
                _GenCtbD := TmySQLQuery(QrLct).FieldByName('GenCtbD').AsInteger;
                _GenCtbC := TmySQLQuery(QrLct).FieldByName('GenCtbC').AsInteger;

                MyVCLref.SET_dmkEditCB(TForm(Reference), 'EdGenCtbD', 'ValueVariant', _GenCtbD);
                MyVCLref.SET_dmkDBLookupComboBox(TForm(Reference), 'CBGenCtbD', 'KeyValue', _GenCtbD);

                MyVCLref.SET_dmkEditCB(TForm(Reference), 'EdGenCtbC', 'ValueVariant', _GenCtbC);
                MyVCLref.SET_dmkDBLookupComboBox(TForm(Reference), 'CBGenCtbC', 'KeyValue', _GenCtbC);

                if TmySQLQuery(QrLct).FieldByName('Debito').AsFloat > 0 then
                  _TpLct := 1
                else
                if TmySQLQuery(QrLct).FieldByName('Credito').AsFloat > 0 then
                  _TpLct := 2
                else
                  _TpLct := 0;
                //MyVCLref.SET_DmkRadioGroup(TForm(Reference), 'RgTpLct', 'ItemIndex', _TpLct);
                MyVCLref.SET_Component(TForm(Reference), 'RgTpLct', 'ItemIndex', Integer(_TpLct), TRadioGroup);

              except
                // nada
              end;
              // fim 2022-03-24
              MyVCLref.SET_Component(TForm(Reference), 'RGFisicoSrc', 'Enabled', True, TdmkRadioGroup);
              MyVCLref.SET_Component(TForm(Reference), 'EdFisicoCod', 'Enabled', True, TdmkEdit);
              //
              if FatID <> 0 then
              begin
                MyVCLref.SET_dmkEditCB(TForm(Reference), 'dmkEdCarteira', 'Enabled', False);
                MyVCLref.SET_dmkDBLookupComboBox(TForm(Reference), 'dmkCBCarteira', 'Enabled', False);
                MyVCLref.SET_Label(TForm(Reference), 'LaCarteira', 'Enabled', False);
                MyVCLref.SET_Component(TForm(Reference), 'SbCarteira', 'Enabled', False, TSpeedButton);
              end else
              begin
                MyVCLref.SET_dmkEditCB(TForm(Reference), 'dmkEdCarteira', 'Enabled', True);
                MyVCLref.SET_dmkDBLookupComboBox(TForm(Reference), 'dmkCBCarteira', 'Enabled', True);
                MyVCLref.SET_Label(TForm(Reference), 'LaCarteira', 'Enabled', True);
                MyVCLref.SET_Component(TForm(Reference), 'SbCarteira', 'Enabled', True, TSpeedButton);
              end;
              MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference), 'TPVctoOriginal', 'Date',
                TmySQLQuery(QrLct).FieldByName('VctoOriginal').AsDateTime);
            end;
            tgrDuplica:
            begin
              //Txt_Latipo := Geral.LaTipo(stIns);
              SQLType := stIns;
              //
              MyVCLref.SET_dmkVariable(TForm(Reference), 'VAVcto', 'ValueVariant', True);
              MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference),
                'dmkEdTPCompensado', 'Date', 0);
              MyVCLref.SET_Component(TForm(Reference),
                'CkDuplicando', 'Checked', True, TCheckBox);
            end;
          end;
          if Acao = tgrDuplica then Txt_Ctrl := '0' else Txt_ctrl := IntToStr(
            TmySQLQuery(QrLct).FieldByName('Controle').AsInteger);
        end;
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdControle', 'Texto',
          Txt_Ctrl, TdmkEdit);
        // 2012-05-15
        //MyVCLref.SET_Label(TForm(Reference), 'LaTipo', 'Caption', Txt_LaTipo);
        MyVCLref.SET_Component(TForm(Reference), 'ImgTipo', 'SQLType', SQLType, TdmkImage);
        // FIM 2012-05-15
        MyVCLref.SET_dmkEdit(TForm(Reference), 'EdTabLctA', 'ValueVariant', TabLctA);
        //
        if (Sit > 0) and (Tipo = 2) and (Acao <> tgrDuplica) then
        begin
          Geral.MB_Aviso('Esta emiss�o j� est� quitada ou possui quita��o parcial!');
          //
          MyVCLref.SET_Component(TForm(Reference), 'dmkEdDeb', 'Enabled', False, TdmkEdit);
          MyVCLref.SET_Component(TForm(Reference), 'dmkEdCred', 'Enabled', False, TdmkEdit);
          MyVCLref.SET_Component(TForm(Reference), 'dmkEdDoc', 'Enabled', False, TdmkEdit);
          MyVCLref.SET_Component(TForm(Reference), 'dmkEdDescricao', 'Enabled', False, TdmkEdit);
          MyVCLref.SET_Component(TForm(Reference), 'dmkEdCarteira', 'Enabled', False, TdmkEdit);
          MyVCLref.SET_Component(TForm(Reference), 'dmkCBCarteira', 'Enabled', False, TdmkDBLookupComboBox);
          //
          VAR_BAIXADO := Sit;
          //
          UMyMod.UpdUnLockInt64Y(Controle, Dmod.MyDB, TabLctA, 'Controle');
          //
          Exit;
        end else
          VAR_BAIXADO := -2;
        try
          //SetaVars();
        except
          Geral.MB_Aviso('N�o foi poss�vel setar vari�veis ' +
            'secund�rias para a janela de lan�amentos!');
        end;
        //
      end;
      Screen.Cursor := MyCursor;
    finally
      Screen.Cursor := MyCursor;
    end;
    except
      raise;
    end;
    //if Cria_e_Mostra then
    //begin
      TForm(Reference).ShowModal;
      //
      Result := MyVCLref.GET_dmkEdit(TForm(Reference), 'dmkEdExecs', 'ValueVariant');
      //
      TForm(Reference).Destroy;
      UMyMod.UpdUnLockInt64Y(Controle, Dmod.MyDB, TabLctA, 'Controle');
    {end else
    begin
      Result := -9;
    end;}
  finally
    ;
  end;
end;

{function TUnFinanceiro.InclusaoLancamentoC(InstanceClass: TComponentClass;
  var Reference; Finalidade: TLanctoFinalidade; ModoAcesso: TAcessFmModo; QrLct,
  QrCrt: TmySQLQuery; Acao: TGerencimantoDeRegistro; Controle, Sub,
  Genero: Integer; PercJuroM, PercMulta: Double; SetaVars: TInsAltReopenLct;
  FatID, FatID_Sub, FatNum, Carteira: Integer; Credito, Debito: Double;
  AlteraAtehFatID: Boolean; Cliente, Fornecedor, CliInt, ForneceI, Account,
  Vendedor: Integer; LockCliInt, LockForneceI, LockAccount,
  LockVendedor: Boolean; Data, Vencto, DataDoc: TDateTime;
  IDFinalidade: Integer; Mes: TDateTime; TabLctA: String; FisicoSrc,
  FisicoCod: Integer): Integer;
const
  Cria_e_Mostra = False;
begin
  if InclusaoLancamentoC(InstanceClass, Reference, Finalidade, ModoAcesso, QrLct,
  QrCrt, Acao, Controle, Sub, Genero, PercJuroM, PercMulta, SetaVars,
  FatID, FatID_Sub, FatNum, Carteira, Credito, Debito, AlteraAtehFatID,
  Cliente, Fornecedor, CliInt, ForneceI, Account, Vendedor,
  LockCliInt, LockForneceI, LockAccount, LockVendedor,
  Data, Vencto, DataDoc, IDFinalidade, Mes, TabLctA, Cria_e_Mostra) = -9 then
  begin
    MyVCLref.SET_Component(TForm(Reference), 'dmkEdDoc', 'Enabled', False, TdmkEdit);
    FisicoSrc,
    FisicoCod: Integer): Integer;
  end;
end;}

function TUnFinanceiro.IndProc_Get(Codigo: Integer): String;
begin
// C�digo de Origem do Processo
  case Codigo of
    0: Result := 'SEFAZ';
    1: Result := 'Justi�a Federal';
    2: Result := 'Justi�a Estadual';
    3: Result := 'Secex/RFB';
    9: Result := 'Outros';
    else Result := '';
  end;
end;

function TUnFinanceiro.IndProc_Lista: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA IndProc
  AddLinha(Result, Linha, '0', 'SEFAZ');
  AddLinha(Result, Linha, '1', 'Justi�a Federal');
  AddLinha(Result, Linha, '2', 'Justi�a Estadual');
  AddLinha(Result, Linha, '3', 'Secex/RFB');
  AddLinha(Result, Linha, '9', 'Outros');
end;

// 2012-05-15
//function TUnFinanceiro.NaoDuplicarLancto(LaTipo: String; Cheque: Extended; PesqCH:
function TUnFinanceiro.NaoDuplicarLancto(SQLType: TSQLType; Cheque: Extended; PesqCH:
// FIM 2012-05-15
              Boolean; SerieCH: String; NF: Integer; PesqNF: Boolean;
              SerieNF: String; Credito, Debito: Double; Conta, Mes: Integer;
              PesqVal: Boolean; CliInt: Integer; EstahEmLoop: Boolean;
              Carteira, Cliente, Fornecedor: Integer;
              LaAviso1, LaAviso2: TLabel; TabLctA, TabLctB, TabLctD: String;
              PesqEntValData: Boolean = False; Data: TDate = 0): Integer;
(*
function TUnFinanceiro.NaoDuplicarLancto(LaTipo: String; Cheque: Integer; PesqCH:
  Boolean; SerieCH: String; NF: Integer; PesqNF: Boolean;
  Credito, Debito: Double; Conta: Integer; Mez: String; PesqVal: Boolean;
  CliInt: Integer): Boolean;
*)
var
  //NotaF: Integer;
  SCH, SNF: String;
  Mostra: Boolean;
begin
   //Result := 1; // Insere
  //Result := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Iniciando pesquisas');
  //NotaF := 0;
  Mostra := False;
  if SQLType <> stIns then
  begin
    Result := 1;
    Exit;
  end;// else Result := 0;
  //
  //Cheque := EdDoc.ValueVariant;
  if PesqCH and (Cheque > 0) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando cheques');
    //
    SCH := Trim(SerieCh);
    //
    DModFin.QrDuplCH.Close;
    DModFin.QrDuplCH.SQL.Clear;
    DModFin.QrDuplCH.SQL.Add('SELECT lan.Data, lan.Controle, lan.Descricao, lan.Credito,');
    DModFin.QrDuplCH.SQL.Add('lan.Debito, lan.NotaFiscal, lan.Compensado, lan.Mez,');
    DModFin.QrDuplCH.SQL.Add('lan.Fornecedor, lan.Cliente, car.Nome NOMECART,');
    DModFin.QrDuplCH.SQL.Add('lan.Documento, lan.SerieCH, lan.Carteira,');
    DModFin.QrDuplCH.SQL.Add('IF(lan.Cliente<>0, CASE WHEN cli.Tipo=0 THEN');
    DModFin.QrDuplCH.SQL.Add('cli.RazaoSocial ELSE cli.Nome END, "") NOMECLI,');
    DModFin.QrDuplCH.SQL.Add('IF(lan.Fornecedor<>0, CASE WHEN fnc.Tipo=0 THEN');
    DModFin.QrDuplCH.SQL.Add('fnc.RazaoSocial ELSE fnc.Nome END, "") NOMEFNC');
    DModFin.QrDuplCH.SQL.Add('FROM ' + TabLctA + ' lan');
    DModFin.QrDuplCH.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    DModFin.QrDuplCH.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente');
    DModFin.QrDuplCH.SQL.Add('LEFT JOIN entidades fnc ON fnc.Codigo=lan.Fornecedor');
    DModFin.QrDuplCH.SQL.Add('WHERE ID_Pgto = 0');
    DModFin.QrDuplCH.SQL.Add('AND car.ForneceI=' + FormatFloat('0', CliInt));
    // 2011-01-23
    DModFin.QrDuplCH.SQL.Add('AND car.Codigo=' + FormatFloat('0', Carteira));
    // Fim 2011-01-23
    DModFin.QrDuplCH.SQL.Add('AND lan.Documento=:P0');

    DModFin.QrDuplCH.Params[0].AsFloat := Cheque;

    if SCH <> '' then
    begin
      DModFin.QrDuplCH.SQL.Add('AND lan.SerieCH=:P1');
      DModFin.QrDuplCH.Params[1].AsString := SCH;
    end;
    UnDmkDAC_PF.AbreQuery(DModFin.QrDuplCH, Dmod.MyDB);
    Mostra := DModFin.QrDuplCH.RecordCount > 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisa de cheques aberta');
  end;

  if PesqNF and (NF > 0) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando notas fiscais');
    //
    SNF := Trim(SerieNF);
    //
    DModFin.QrDuplNF.Close;
    DModFin.QrDuplNF.SQL.Clear;
    DModFin.QrDuplNF.SQL.Add('SELECT lan.Data, lan.Controle, lan.Descricao, lan.Credito,');
    DModFin.QrDuplNF.SQL.Add('lan.Debito, lan.NotaFiscal, lan.Compensado, lan.Mez, SerieCH,');
    DModFin.QrDuplNF.SQL.Add('lan.Fornecedor, lan.Cliente, car.Nome NOMECART, Documento,');
    DModFin.QrDuplNF.SQL.Add('IF(lan.Cliente<>0, CASE WHEN cli.Tipo=0 THEN');
    DModFin.QrDuplNF.SQL.Add('cli.RazaoSocial ELSE cli.Nome END, "") NOMECLI,');
    DModFin.QrDuplNF.SQL.Add('IF(lan.Fornecedor<>0, CASE WHEN fnc.Tipo=0 THEN');
    DModFin.QrDuplNF.SQL.Add('fnc.RazaoSocial ELSE fnc.Nome END, "") NOMEFNC,');
    DModFin.QrDuplNF.SQL.Add('lan.Carteira');
    DModFin.QrDuplNF.SQL.Add('FROM ' + TabLctA + ' lan');
    DModFin.QrDuplNF.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    DModFin.QrDuplNF.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente');
    DModFin.QrDuplNF.SQL.Add('LEFT JOIN entidades fnc ON fnc.Codigo=lan.Fornecedor');
    DModFin.QrDuplNF.SQL.Add('WHERE lan.ID_Pgto = 0');
    DModFin.QrDuplNF.SQL.Add('AND lan.NotaFiscal=:P0');
    DModFin.QrDuplNF.SQL.Add('AND car.ForneceI=:P1');
    DModFin.QrDuplNF.Params[00].AsInteger := NF;
    DModFin.QrDuplNF.Params[01].AsInteger := CliInt;

    if SNF <> '' then
    begin
      DModFin.QrDuplCH.SQL.Add('AND lan.SerieNF=:P1');
      DModFin.QrDuplCH.Params[1].AsString := SNF;
    end;

    UnDmkDAC_PF.AbreQuery(DModFin.QrDuplNF, Dmod.MyDB);
    Mostra := Mostra or (DModFin.QrDuplNF.RecordCount > 0);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisa de NFs aberta');
  end;

  if PesqVal and ((Credito <> 0) or (Debito <> 0)) and (Conta <> 0) and (Mes <> 0) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando valores');
    DModFin.QrDuplVal.Close;
    DModFin.QrDuplVal.SQL.Clear;
    DModFin.QrDuplVal.SQL.Add('SELECT lan.Data, lan.Controle, lan.Descricao, lan.Credito,');
    DModFin.QrDuplVal.SQL.Add('lan.Debito, lan.NotaFiscal, lan.Compensado, lan.Mez, SerieCH,');
    DModFin.QrDuplVal.SQL.Add('lan.Fornecedor, lan.Cliente, car.Nome NOMECART, Documento,');
    DModFin.QrDuplVal.SQL.Add('IF(lan.Cliente<>0, CASE WHEN cli.Tipo=0 THEN');
    DModFin.QrDuplVal.SQL.Add('cli.RazaoSocial ELSE cli.Nome END, "") NOMECLI,');
    DModFin.QrDuplVal.SQL.Add('IF(lan.Fornecedor<>0, CASE WHEN fnc.Tipo=0 THEN');
    DModFin.QrDuplVal.SQL.Add('fnc.RazaoSocial ELSE fnc.Nome END, "") NOMEFNC,');
    DModFin.QrDuplVal.SQL.Add('lan.Carteira');
    DModFin.QrDuplVal.SQL.Add('FROM ' + TabLctA + ' lan');
    DModFin.QrDuplVal.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    DModFin.QrDuplVal.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente');
    DModFin.QrDuplVal.SQL.Add('LEFT JOIN entidades fnc ON fnc.Codigo=lan.Fornecedor');
    DModFin.QrDuplVal.SQL.Add('WHERE ID_Pgto = 0');
    DModFin.QrDuplVal.SQL.Add('AND lan.Credito=:P0');
    DModFin.QrDuplVal.SQL.Add('AND lan.Debito=:P1');
    DModFin.QrDuplVal.SQL.Add('AND lan.Genero=:P2');
    DModFin.QrDuplVal.SQL.Add('AND lan.Mez=:P3');
    DModFin.QrDuplVal.SQL.Add('AND lan.Cliente=:P4');
    DModFin.QrDuplVal.SQL.Add('AND lan.Fornecedor=:P5');
    DModFin.QrDuplVal.SQL.Add('AND car.ForneceI=:P6');
    DModFin.QrDuplVal.Params[00].AsFloat   := Credito;
    DModFin.QrDuplVal.Params[01].AsFloat   := Debito;
    DModFin.QrDuplVal.Params[02].AsInteger := Conta;
    DModFin.QrDuplVal.Params[03].AsInteger := Mes;
    // 2011-01-23
    DModFin.QrDuplVal.Params[04].AsInteger := Cliente;
    DModFin.QrDuplVal.Params[05].AsInteger := Fornecedor;
    // Fim 2011-01-23
    // 2011-07-22
    DModFin.QrDuplVal.Params[06].AsInteger := CliInt;
    // Fim 2011-07-22
    UnDmkDAC_PF.AbreQuery(DModFin.QrDuplVal, Dmod.MyDB);
    Mostra := Mostra or (DModFin.QrDuplVal.RecordCount > 0);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisa de valores aberta');
  end;

  if PesqEntValData and ((Credito <> 0) or (Debito <> 0)) and
    ((Cliente <> 0) or (Fornecedor <> 0)) and (Conta <> 0) and (Data <> 0) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando lan�amentos');
    //
    DModFin.QrDuplEntValData.Close;
    DModFin.QrDuplEntValData.SQL.Clear;
    DModFin.QrDuplEntValData.SQL.Add('SELECT lan.Data, lan.Controle, lan.Descricao, lan.Credito,');
    DModFin.QrDuplEntValData.SQL.Add('lan.Debito, lan.NotaFiscal, lan.Compensado, lan.Mez, SerieCH,');
    DModFin.QrDuplEntValData.SQL.Add('lan.Fornecedor, lan.Cliente, car.Nome NOMECART, Documento,');
    DModFin.QrDuplEntValData.SQL.Add('IF(lan.Cliente<>0, CASE WHEN cli.Tipo=0 THEN');
    DModFin.QrDuplEntValData.SQL.Add('cli.RazaoSocial ELSE cli.Nome END, "") NOMECLI,');
    DModFin.QrDuplEntValData.SQL.Add('IF(lan.Fornecedor<>0, CASE WHEN fnc.Tipo=0 THEN');
    DModFin.QrDuplEntValData.SQL.Add('fnc.RazaoSocial ELSE fnc.Nome END, "") NOMEFNC,');
    DModFin.QrDuplEntValData.SQL.Add('lan.Carteira');
    DModFin.QrDuplEntValData.SQL.Add('FROM ' + TabLctA + ' lan');
    DModFin.QrDuplEntValData.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    DModFin.QrDuplEntValData.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente');
    DModFin.QrDuplEntValData.SQL.Add('LEFT JOIN entidades fnc ON fnc.Codigo=lan.Fornecedor');
    DModFin.QrDuplEntValData.SQL.Add('WHERE ID_Pgto = 0');
    DModFin.QrDuplEntValData.SQL.Add('AND lan.Credito=:P0');
    DModFin.QrDuplEntValData.SQL.Add('AND lan.Debito=:P1');
    DModFin.QrDuplEntValData.SQL.Add('AND lan.Genero=:P2');
    DModFin.QrDuplEntValData.SQL.Add('AND lan.Cliente=:P3');
    DModFin.QrDuplEntValData.SQL.Add('AND lan.Fornecedor=:P4');
    DModFin.QrDuplEntValData.SQL.Add('AND car.ForneceI=:P5');
    DModFin.QrDuplEntValData.SQL.Add('AND lan.Data=:P6');
    DModFin.QrDuplEntValData.Params[00].AsFloat   := Credito;
    DModFin.QrDuplEntValData.Params[01].AsFloat   := Debito;
    DModFin.QrDuplEntValData.Params[02].AsInteger := Conta;
    DModFin.QrDuplEntValData.Params[03].AsInteger := Cliente;
    DModFin.QrDuplEntValData.Params[04].AsInteger := Fornecedor;
    DModFin.QrDuplEntValData.Params[05].AsInteger := CliInt;
    DModFin.QrDuplEntValData.Params[06].AsString  := Geral.FDT(Data, 01);
    UnDmkDAC_PF.AbreQuery(DModFin.QrDuplEntValData, Dmod.MyDB);
    //
    Mostra := Mostra or (DModFin.QrDuplEntValData.RecordCount > 0);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisa de valores aberta');
  end;

  if Mostra then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo janela para mostrar valores');
    MyObjects.CriaForm_AcessoTotal(TFmLctDuplic, FmLctDuplic);
    FmLctDuplic.FConfirma := 0;
    if Cheque = 0 then
      FmLctDuplic.STCheque.Caption := 'Cheque: N�o pesquisado'
    else
      FmLctDuplic.STCheque.Caption := 'Cheque: ' + SCH + ' ' + FormatFloat('0', Cheque);
    if NF = 0 then
      FmLctDuplic.STNotaF.Caption := 'Nota Fiscal: N�o pesquisado'
    else
      FmLctDuplic.STNotaF.Caption := 'Nota Fiscal: '+IntToStr(NF);
    //
    FmLctDuplic.BtEstahEmLoop.Visible := EstahEmLoop;
    FmLctDuplic.ShowModal;
    Result := FmLctDuplic.FConfirma;
    FmLctDuplic.Destroy;
  end else Result := 1;

  if Result = 1 then Screen.Cursor := crDefault;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisa de duplica��o finalizada');
end;

function TUnFinanceiro.AtualizaPagamentosAVista(QrLctos: TmySQLQuery; TabLctA:
String): Boolean;
var
  Controle: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE IGNORE  ' + TabLctA + ' SET Compensado=Data ');
  Dmod.QrUpd.SQL.Add('WHERE Tipo<2 AND Compensado < 2');
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + TabLctA + ' SET Sit=3 ');
  Dmod.QrUpd.SQL.Add('WHERE Tipo<2 AND Sit < 2');
  Dmod.QrUpd.ExecSQL;
  Result := True;
  //
  if (QrLctos <> nil) and (QrLctos.State <> dsInactive) then
  begin
    Controle := QrLctos.FieldByName('Controle').AsInteger;
    QrLctos.Close;
    UnDmkDAC_PF.AbreQuery(QrLctos, Dmod.MyDB);
    if Controle > 0 then
      QrLctos.Locate('Controle', Controle, []);
  end;
  Screen.Cursor := crDefault;
end;

{###
function TUnFinanceiro.AtualizaSaldosDeContas(Entidade, Periodo: Integer;
  MostraVerif: Boolean): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmContasHistAtz2, FmContasHistAtz2, afmoNegarComAviso) then
  begin
    FmContasHistAtz2.FEntiCod := Entidade;
    FmContasHistAtz2.ShowModal;
    Result := FmContasHistAtz2.FContinua;
    FmContasHistAtz2.Destroy;
    //
    if Result and MostraVerif then
    begin
      DmodFin.QrVerifHis.Close;
      DmodFin.QrVerifHis.Params[00].AsInteger := Entidade;
      DmodFin.QrVerifHis.Params[01].AsInteger := Periodo;
      UnDmkDAC_PF.AbreQuery(DmodFin.QrVerifHis, Dmod.MyDB);
      if DmodFin.QrVerifHis.RecordCount > 0 then
      begin
        if DBCheck.CriaFm(TFmContasHisVerif, FmContasHisVerif, afmoNegarComAviso) then
        begin
          FmContasHisVerif.ShowModal;
          FmContasHisVerif.Destroy;
        end;
      end;
    end;
  end;
end;
}

procedure TUnFinanceiro.AtualizaTodasCarteiras(QrCrt, QrLct: TmySQLQuery;
  TabLctA: String; Carteira: Integer = 0);
var
  Cart, Controle: Integer;
begin
  Screen.Cursor := crHourGlass;
  if QrLct <> nil then
    Controle := QrLct.FieldByName('Controle').AsInteger
  else
    Controle := 0;
  //
  if Carteira <> 0 then
    Cart := Carteira
  else
    Cart := QrCrt.FieldByName('Codigo').AsInteger;
  //
  AtualizaVencimentos(TabLctA);
  QrCrt.DisableControls;
  QrCrt.First;
  while not QrCrt.Eof do
  begin
    RecalcSaldoCarteira(QrCrt.FieldByName('Codigo').AsInteger, nil, nil, False, False);
    QrCrt.Next;
  end;
  QrCrt.Close;
  UnDmkDAC_PF.AbreQuery(QrCrt, Dmod.MyDB);
  QrCrt.Locate('Codigo', Cart, []);
  QrCrt.EnableControls;
  //
  if Controle <> 0 then
    QrLct.Locate('Controle', Controle, []);
  //  
  Screen.Cursor := crDefault;
end;

function TUnFinanceiro.SQLInsUpd_Lct(QrUpd: TmySQLQuery; Tipo: TSQLType;
  Auto_increment: Boolean; SQLCampos, SQLIndex: array of String; ValCampos,
  ValIndex: array of Variant; UserDataAlterweb: Boolean;
  ComplUpd, TabLctA: String(*; DtEncer, Data: TDateTime*)): Boolean;
begin
  (*
  if Int(Data) <= Int(DtEncer) then
  begin
    Result := False;
    Geral.MB_Aviso('Data inv�lida! Pertence a um m�s j� encerrado!',
    );
    Exit;
  end;
  *)
  if Uppercase(TabLctA[Length(TabLctA)]) <> 'A' then
  begin
    Result := False;
    Geral.MB_Aviso(
    'Tabela de lan�amentos inv�lida! Pertence a um m�s j� encerrado!');
    Exit;
  end;
  Result := UMyMod.SQLInsUpd(QrUpd, Tipo, TabLctA, Auto_increment,
    SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb, ComplUpd);
end;

function TUnFinanceiro.SQLLoc1(Query: TmySQLQuery; Tabela, Campo: String;
  Valor: Variant; MsgLoc, MsgNaoLoc: String): Integer;
var
  Txt: String;
begin
  Txt := Geral.VariavelToString(Valor);
  //
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT ' + Campo);
  Query.SQL.Add('FROM ' + Lowercase(Tabela));
  Query.SQL.Add('WHERE ' + Campo + '=' + Txt);
  UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
  //
  Result := Query.RecordCount;
  //
  if (Result = 0) and (Trim(MsgNaoLoc) <> '') then
    Geral.MB_Aviso(MsgNaoLoc);
  if (Result > 0) and (Trim(MsgLoc) <> '') then
    Geral.MB_Aviso(MsgLoc + sLineBreak + 'Itens = ' +
    IntToStr(Result));
end;

function TUnFinanceiro.StatusContasConf_Codigo(Qtde, QtdeMin, QtdeMax: Integer;
  Debito, ValorMin, ValorMax: Double; Periodo, PeriodoIni, PeriodoFim: Integer):
  Integer;
begin
  //Result := -999999999;
  if (PeriodoFim > 0) and (PeriodoFim < Periodo) then
    Result := -2
  else
  begin
    if (Qtde < QtdeMin)
    or (Debito < ValorMin) then
      Result := 0 else
    if (Qtde > QtdeMax)
    and (Debito < ValorMin) then
      Result := 1 else
    if (Qtde > QtdeMax)
    and (Debito < ValorMax) then
      Result := 2 else
    if (Qtde > QtdeMax)
    and (Debito > ValorMin) then
      Result := 4 else
    begin
      if (ValorMin <= 0 ) then
        Result := -1
      else
        Result := 3;
    end;
  end;
end;

function TUnFinanceiro.StatusContasConf_Texto(Status, QtdeMin,
  QtdeExe: Integer): String;
var
  p1, p2, p3: String;
begin
  if QtdeMin - QtdeExe > 1 then p1 := 's' else p1 := '';
  if QtdeExe > 1 then p2 := 's' else p2 := '';
  if QtdeExe > 1 then p3 := 'm' else p3 := '';
  //
  case Status of
    -2: Result := 'Per�odo j� passou! Reconfigure ou exclua a configura��o';
    -1: Result := 'Valor m�nimo inv�lido';
    0: Result := 'Lan�amento' + p1 + ' pendente' + p1;
    1: Result := 'Lan�amento' + p2 + ' excede' + p3 + ', e o valor total � insuficiente';
    2: Result := 'Lan�amento' + p2 + ' excede' + p3 + ', mas o valor total � suficiente';
    3: Result := 'Lan�amento' + p2 + ' e valor total conferem';
    4: Result := 'Lan�amento' + p2 + ' e valor total excedem';
    else Result := '### STATUS DESCONHECIDO ###';
  end;
end;

function TUnFinanceiro.PagamentoEmBanco_DefineGenerosECarteira(const Controle,
  Sub: Integer; const Credito, Debito: Double; const OutraCartCompensa,
  BancoCar, _GenCtbD, _GenCtbC: Integer; var Carteira, GenCtbD, GenCtbC:
  Integer): Boolean;
var
  GenCtbBco: Integer;
begin
  GenCtbBco := 0;
  if OutraCartCompensa = 0 then
    Carteira := BancoCar
  else
    Carteira := OutraCartCompensa;
    //
  if DModG.QrCtrlGeralUsarFinCtb.Value = 1 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DModFin.QrCartCtb, Dmod.MyDB, [
    'SELECT Banco, GenCtb ',
    'FROM carteiras',
    'WHERE Codigo=' + Geral.FF0(Carteira),
    '']);
    GenCtbBco := DModFin.QrCartCtbGenCtb.Value;
    if MyObjects.FIC(GenCtbBco = 0, nil,
    'Carteira de destino sem conta cont�bil!') then
      Exit;
  end;
  if Credito > 0 then
  begin
    GenCtbD    := GenCtbBco;
    GenCtbC    := _GenCtbD;
  end else
  if Debito > 0 then
  begin
    GenCtbD    := _GenCtbC;
    GenCtbC    := GenCtbBco;
  end;
  Result := not ImpedePorFaltaDeContaContabil(
    Controle, Sub, Carteira, GenCtbD, GenCtbC);
end;

procedure TUnFinanceiro.PagarAVistaEmCaixa(QrCrt, QrLct: TmySQLQuery;
  DBGLct: TDBGrid; TabLctA: String; AlteraAtehFatID: Boolean = False);

  procedure InsereEmLctoEdit(var Erros: Integer);
  begin
    if AlteraAtehFatID = False then
    begin
      if dmkPF.FatIDProibidoEmLct(QrLct.FieldByName('FatID').AsInteger) then
      begin
        Erros := Erros + 1;
        Exit;
      end;
    end;
    DModG.QrUpdPID1.Params[00].AsInteger := QrLct.FieldByName('Controle').AsInteger;
    DModG.QrUpdPID1.Params[01].AsInteger := QrLct.FieldByName('Sub').AsInteger;
    DModG.QrUpdPID1.Params[02].AsString  := QrLct.FieldByName('Descricao').AsString;
    DModG.QrUpdPID1.Params[03].AsString  := Geral.FDT(QrLct.FieldByName('Data').AsDateTime, 1);
    DModG.QrUpdPID1.Params[04].AsString  := Geral.FDT(QrLct.FieldByName('Vencimento').AsDateTime, 1);
    DModG.QrUpdPID1.Params[05].AsFloat   := QrLct.FieldByName('Credito').AsFloat - QrLct.FieldByName('Debito').AsFloat;
    //
    DModG.QrUpdPID1.ExecSQL;
  end;

var
  I, N: Integer;
  TmpLctoEdit, Msg: String;
begin
  N := 0;
  //
  TmpLctoEdit := UCriar.RecriaTempTableNovo(ntrttLctoEdit, DmodG.QrUpdPID1, False);

  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM ' + TmpLctoEdit);
  DModG.QrUpdPID1.ExecSQL;
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + TmpLctoEdit + ' SET ');
  DModG.QrUpdPID1.SQL.Add('Controle=:P0, Sub=:P1, Descricao=:P2, Data=:P3, ');
  DModG.QrUpdPID1.SQL.Add('Vencimento=:P4, ValorOri=:P5');
  DModG.QrUpdPID1.SQL.Add('');
  if DBGLct.SelectedRows.Count > 1 then
  begin
    with DBGLct.DataSource.DataSet do
    for I := 0 to DBGLct.SelectedRows.Count-1 do
    begin
      //GotoBookmark(DBGLct.SelectedRows.Items[i]);
      GotoBookmark(DBGLct.SelectedRows.Items[i]);
      //
      if VerificaSeLanctoEParcelamento(QrLct, Msg) then
      begin
        Geral.MB_Aviso(Msg);
        N := 1;
        Exit;
      end else
      begin
        InsereEmLctoEdit(N);
        if N > 0 then
          Exit;
      end;
    end;
  end else
  begin
    if VerificaSeLanctoEParcelamento(QrLct, Msg) then
    begin
      Geral.MB_Aviso(Msg);
      N := 1;
      Exit;
    end;
    InsereEmLctoEdit(N);
  end;
  if N > 0 then
    Exit;
  if MyObjects.CriaForm_AcessoTotal(TFmLctPgEmCxa, FmLctPgEmCxa) then
  begin
    FmLctPgEmCxa.FTabLctA     := TabLctA;
    FmLctPgEmCxa.FTmpLctoEdit := TmpLctoEdit;
    FmLctPgEmCxa.FQrCrt       := QrCrt;
    FmLctPgEmCxa.FQrLct       := QrLct;
    FmLctPgEmCxa.EdMulta.Text := Geral.FFT(QrLct.FieldByName('Multa').AsFloat, 6, siPositivo);
    FmLctPgEmCxa.EdTaxaM.Text := Geral.FFT(QrLct.FieldByName('MoraDia').AsFloat, 6, siPositivo);
    FmLctPgEmCxa.ShowModal;
    FmLctPgEmCxa.Destroy;
  end;
end;

procedure TUnFinanceiro.PagarRolarEmissao(QrCrt, QrLct: TmySQLQuery;
  TabLctA: String; AlteraAtehFatID: Boolean; var CodLctLocaliz: Integer);
var
  Lancto: Int64;
  Carteira: Integer;
  Msg: String;
begin
  CodLctLocaliz := 0;
  //
  if VerificaSeLanctoEParcelamento(QrLct, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    //
    Exit;
  end;
  Lancto   := QrLct.FieldByName('Controle').AsInteger;
  Carteira := QrLct.FieldByName('Carteira').AsInteger;
  //
  if not AlteraAtehFatID then
  begin
    if dmkPF.FatIDProibidoEmLct(QrLct.FieldByName('FatID').AsInteger) then
      Exit;
  end;
  //
  if DBCheck.CriaFm(TFmCartPgto, FmCartPgto, afmoNegarComAviso) then
  begin
    FmCartPgto.FTabLctA            := TabLctA;
    FmCartPgto.FQrLct              := QrLct;
    FmCartPgto.FQrCarteira         := QrCrt;
    FmCartPgto.DataSource1.DataSet := QrLct;
    FmCartPgto.LaControle.Caption := FormatFloat('0', QrLct.FieldByName('Controle').AsInteger);
    FmCartPgto.PagtosReopen(0);
    FmCartPgto.ShowModal;
    //
    CodLctLocaliz := FmCartPgto.FCodLctLocaliz;
    //
    FmCartPgto.Destroy;
  end;
  VAR_LANCTO2 := Lancto;
  RecalcSaldoCarteira(Carteira, QrCrt, QrLct, True, True);
end;

function TUnFinanceiro.Pesquisa4_Cartas(const RGModoItemIndex: Integer;
  const Data, Vencto: String; const EntiCond, Entidade, Apto, Propriet,
  Mez: Integer; const FatNum: Double; const TabLctA: String;
  var QrInadFiltered: Boolean): String;
var
  //FTemDtP: Boolean;
  //FDtaPgt: String;
  SQL: TStringList;
begin
  SQL := TStringList.Create;
  try
    //ver juros, multas etc sobre pago!
    SQL.Add('SELECT CONCAT(RIGHT(Mez, 2), "/",  LEFT(Mez + 200000, 4)) MES,');
    SQL.Add('');
    SQL.Add('lan.Data, lan.FatNum, lan.Vencimento, lan.Mez, lan.Descricao,');
    SQL.Add('');
    SQL.Add('SUM(lan.Credito) CREDITO, SUM(lan.Pago) PAGO,');
    SQL.Add('');

    // Juros
    SQL.Add('IF(lan.Sit>1, SUM(PagJur),');
    SQL.Add('  SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    SQL.Add('  lan.MoraDia  / 3000, 2), 0))) Juros,');
    SQL.Add('');
    // Multa
    SQL.Add('IF(lan.Sit>1, SUM(PagMul),');
    SQL.Add('  SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * lan.Multa  / 100, 2), 0))) Multa,');
    SQL.Add('');
    // Total
    SQL.Add('SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    SQL.Add('  lan.MoraDia  / 3000 +');
    SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * lan.Multa  / 100, 2), 2), 0)+ Credito) TOTAL,');
    SQL.Add('');
    // Saldo
    SQL.Add('IF(lan.Sit>1, 0,');
    SQL.Add('  SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    SQL.Add('  lan.MoraDia  / 3000 +');
    SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago)) SALDO,');
    SQL.Add('');
    // Atualizado
    SQL.Add('');
    SQL.Add('IF(SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    SQL.Add('  lan.MoraDia  / 3000 +');
    SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * lan.Multa  / 100, 2), 2), 0)+ Credito)');
    SQL.Add('  < SUM(lan.Pago) , SUM(lan.Pago) ,');
    SQL.Add('  SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    SQL.Add('  lan.MoraDia  / 3000 +');
    SQL.Add('  ROUND(IF(lan.Sit<2,lan.Credito-lan.Pago,0) * lan.Multa  / 100, 2), 2), 0)+ Credito))');
    SQL.Add('  ATZ_VAL,');
    SQL.Add('');
    // Pendente
    SQL.Add('IF(');
    SQL.Add('  (SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    SQL.Add('  lan.MoraDia  / 3000 +');
    SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago) < 0)');
    SQL.Add('  OR (SUM(IF(lan.Sit<2,lan.Credito-lan.Pago,0))<=0), 0,');
    SQL.Add('  SUM(IF(TO_DAYS(SYSDATE())-TO_DAYS(lan.Vencimento) > 0,');
    SQL.Add('  ROUND(lan.Credito * (TO_DAYS(SYSDATE()) - TO_DAYS(lan.Vencimento)) *');
    SQL.Add('  lan.MoraDia  / 3000 +');
    SQL.Add('  ROUND(lan.Credito * lan.Multa  / 100, 2), 2), 0)+ Credito - Pago)) PEND_VAL,');

    // fim mudan�a 2010-10-12

    SQL.Add('SUM(IF(lan.Sit<2,lan.Credito-lan.Pago,0)) VALOR');
    SQL.Add('');
    SQL.Add('FROM ' + TabLctA + ' lan');
    SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    SQL.Add('WHERE car.Tipo=2');
    case RGModoItemIndex of
      CO_INADIMPL_APENAS_COM_PENDENCIAS(*1*):
      begin
        // Reparcelados n�o s�o d�bitos
        SQL.Add('AND lan.Reparcel=0');
        SQL.Add('AND lan.Sit < 2');
        SQL.Add('AND lan.Credito > lan.Pago');
        //
        QrInadFiltered := False;
      end;
      CO_INADIMPL_APENAS_SEM_PENDENCIAS(*2*):
      begin
        // Errado!! Usar filtro
        //SQL.Add('AND lan.Sit = 2');
        QrInadFiltered := True;
      end;
      CO_INADIMPL_COM_E_SEM_PENDENCIAS(*3*):
      begin
        //qualquer coisa
        QrInadFiltered := False;
      end;
      CO_INADIMPL_BOLETO_ESPECIFICO: (*4*)
      begin
        SQL.Add('AND lan.Data = "' + Data + '"');
        SQL.Add('AND lan.Mez = ' + Geral.FF0(Mez));
        SQL.Add('AND lan.FatNum = ' + Geral.FFI(FatNum));
      end;
      else Geral.MB_Erro('"Tipo de pesquisa" n�o defenido!');
    end;
    //SQL.Add('AND lan.Sit < 2');  ?????
    if RGModoItemIndex = CO_INADIMPL_BOLETO_ESPECIFICO then
      SQL.Add('AND lan.Vencimento = "' + Vencto + '"')
    else
      SQL.Add('AND lan.Vencimento < "' + Vencto + '"');
    //
    SQL.Add('AND lan.Depto=' + dmkPF.FFP(Apto, 0));
    if EntiCond <> 0 then
      SQL.Add('AND lan.CliInt=' + dmkPF.FFP(Entidade, 0));
    if Propriet <> 0 then
      SQL.Add('AND lan.ForneceI=' + dmkPF.FFP(Propriet, 0));
    SQL.Add('GROUP BY lan.FatNum, lan.Vencimento');
    SQL.Add('ORDER BY lan.Mez');
    SQL.Add('');
    Result := SQL.Text;
    //
  finally
    SQL.Free;
  end;
end;

function TUnFinanceiro.ProximoRegistro(Query: TmySQLQuery; Campo: String; Atual:
Integer): Integer;
begin
  Query.Next;
  if Query.FieldByName(Campo).AsInteger = Atual then Query.Prior;
  Result := Query.FieldByName(Campo).AsInteger;
end;

procedure TUnFinanceiro.QuitacaoDeDocumentos(DBGLct: TDBGrid; QrCrt, QrLct:
  TmySQLQuery; TabLctA: String; AlteraAtehFatID: Boolean = False);
begin
  if DBGLct.SelectedRows.Count < 2 then
    Quitacao_CompensaNaContaCorrente(QrCrt, QrLct, TabLctA, AlteraAtehFatID)
  else
    Quitacao_QuitarVariosItens(DBGLct, QrCrt, QrLct, TabLctA, AlteraAtehFatID);
end;

procedure TUnFinanceiro.Quitacao_CompensaNaContaCorrente(QrCrt, QrLct:
  TmySQLQuery; TabLctA: String; AlteraAtehFatID: Boolean = False);
var
  Controle, Sub, CtrlIni: Integer;
  Documento: Double;
  Data: TDateTime;
  Msg: String;
begin
  if VerificaSeLanctoEParcelamento(QrLct, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    Exit;
  end;
  //
  if not AlteraAtehFatID then
  begin
    if dmkPF.FatIDProibidoEmLct(QrLct.FieldByName('FatID').AsInteger) then
      Exit;
  end;
  //Carteira  := QrLct.FieldByName('Carteira').AsInteger;
  Controle  := QrLct.FieldByName('Controle').AsInteger;
  Data      := QrLct.FieldByName('Data').AsDateTime;
  Sub       := QrLct.FieldByName('Sub').AsInteger;
  Documento := QrLct.FieldByName('Documento').AsInteger;
  CtrlIni   := QrLct.FieldByName('CtrlIni').AsInteger;
  //
  Quitacao_CriaFmQuitaDoc2(QrCrt, QrLct, Data, Documento, Controle, CtrlIni,
    Sub, TabLctA);
  //
  QrLct.Locate('Controle;Sub', VarArrayOf([Controle, Sub]), []);
end;

procedure TUnFinanceiro.Quitacao_CriaFmQuitaDoc2(QrCrt, QrLct: TmySQLQuery;
Data: TDateTime; Documento, Controle, CtrlIni: Double; Sub: Integer;
TabLctA: String);
begin
  if DBCheck.CriaFm(TFmQuitaDoc2, FmQuitaDoc2, afmoNegarComAviso) then
  begin
    FmQuitaDoc2.FTabLctA  := TabLctA;
    FmQuitaDoc2.FQrCrt    := QrCrt;
    FmQuitaDoc2.FQrLct    := QrLct;
    FmQuitaDoc2.FCtrlIni  := CtrlIni;
    FmQuitaDoc2.FControle := Controle;
    //FmQuitaDoc2.LaQuitar.Caption := IntToStr(Acao);
    FmQuitaDoc2.TPData1.Date             := Data;
    FmQuitaDoc2.EdDocumento.ValueVariant := Documento;
    FmQuitaDoc2.EdLancto.ValueVariant    := Controle;
    FmQuitaDoc2.EdSub.ValueVariant       := Sub;
    FmQuitaDoc2.ShowModal;
    FmQuitaDoc2.Destroy;
  end;
end;


procedure TUnFinanceiro.Quitacao_QuitarVariosItens(DBGLct: TDBGrid; QrCrt,
  QrLct: TmySQLQuery; TabLctA: String; AlteraAtehFatID: Boolean = False);

  procedure InsereEmLctoEdit(var Erros: Integer);
  begin
    if not AlteraAtehFatID then
    begin
      if dmkPF.FatIDProibidoEmLct(QrLct.FieldByName('FatID').AsInteger) then
      begin
        Erros := Erros + 1;
        Exit;
      end;
    end;
    DModG.QrUpdPID1.Params[00].AsInteger := QrLct.FieldByName('Controle').AsInteger;
    DModG.QrUpdPID1.Params[01].AsInteger := QrLct.FieldByName('Sub').AsInteger;
    DModG.QrUpdPID1.Params[02].AsString  := QrLct.FieldByName('Descricao').AsString;
    DModG.QrUpdPID1.Params[03].AsString  := Geral.FDT(QrLct.FieldByName('Data').AsDateTime, 1);
    DModG.QrUpdPID1.Params[04].AsString  := Geral.FDT(QrLct.FieldByName('Vencimento').AsDateTime, 1);
    DModG.QrUpdPID1.Params[05].AsFloat   := QrLct.FieldByName('Credito').AsFloat
                                            - QrLct.FieldByName('Debito').AsFloat;
    DModG.QrUpdPID1.Params[06].AsFloat   := QrLct.FieldByName('Carteira').AsInteger;
    //
    DModG.QrUpdPID1.ExecSQL;
  end;

var
  I, N: Integer;
  Msg: String;
begin
  N := 0;
  UCriar.RecriaTempTable('lctoedit', DModG.QrUpdPID1, False);
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO lctoedit SET ');
  DModG.QrUpdPID1.SQL.Add('Controle=:P0, Sub=:P1, Descricao=:P2, Data=:P3, ');
  DModG.QrUpdPID1.SQL.Add('Vencimento=:P4, ValorOri=:P5, Carteira=:P6 ');
  DModG.QrUpdPID1.SQL.Add('');
  if DBGLct.SelectedRows.Count > 1 then
  begin
    with DBGLct.DataSource.DataSet do
    for I := 0 to DBGLct.SelectedRows.Count-1 do
    begin
      //GotoBookmark(DBGLct.SelectedRows.Items[I]);
      GotoBookmark(DBGLct.SelectedRows.Items[I]);
      //
      if VerificaSeLanctoEParcelamento(QrLct, Msg) then
      begin
        Geral.MB_Aviso(Msg);
        N := 1;
        Exit;
      end else
      begin
        InsereEmLctoEdit(N);
        if N > 0 then
          Exit;
      end;
    end;
  end else
  begin
    if VerificaSeLanctoEParcelamento(QrLct, Msg) then
    begin
      Geral.MB_Aviso(Msg);
      Exit;
    end;
    //
    InsereEmLctoEdit(N);
  end;
  if N > 0 then
    Exit;
  if MyObjects.CriaForm_AcessoTotal(TFmLctPgVarios, FmLctPgVarios) then
  begin
    FmLctPgVarios.FTabLctA := TabLctA;
    FmLctPgVarios.FQrCrt   := QrCrt;
    FmLctPgVarios.FQrLct   := QrLct;
    FmLctPgVarios.EdMulta.Text := Geral.FFT(QrLct.FieldByName('Multa').AsFloat, 6, siPositivo);
    FmLctPgVarios.EdTaxaM.Text := Geral.FFT(QrLct.FieldByName('MoraDia').AsFloat, 6, siPositivo);
    FmLctPgVarios.ShowModal;
    FmLctPgVarios.Destroy;
  end;
end;

procedure TUnFinanceiro.AtzDataAtzSdoPlaCta(Entidade: Integer; Data: TDateTime);
begin
  DmodFin.QrUDAPC.SQL.Clear;
  DmodFin.QrUDAPC.SQL.Add('UPDATE entidades SET');
  DmodFin.QrUDAPC.SQL.Add('AltDtPlaCt=:P0');
  DmodFin.QrUDAPC.SQL.Add('WHERE Codigo=:P1');
  DmodFin.QrUDAPC.SQL.Add('AND AltDtPlaCt > :P2');
  DmodFin.QrUDAPC.Params[00].AsDate    := Data;
  DmodFin.QrUDAPC.Params[01].AsInteger := Entidade;
  DmodFin.QrUDAPC.Params[02].AsDate    := Data;
  DmodFin.QrUDAPC.ExecSQL;
end;

{###
procedure TUnFinanceiro.CriaFmQuitaDoc2(Data: TDateTime; Documento, Controle,
 CtrlIni: Double; Sub: Integer; QrCrt: TmySQLQuery);
//var
  //Cursor : TCursor;
begin
  if DBCheck.CriaFm(TFmQuitaDoc2, FmQuitaDoc2, afmoNegarComAviso) then
  begin
    FmQuitaDoc2.FCtrlIni  := CtrlIni;
    FmQuitaDoc2.FControle := Controle;
    //FmQuitaDoc2.LaQuitar.Caption := IntToStr(Acao);
    FmQuitaDoc2.TPData1.Date     := Data;//Dmod.QrLctData.Value;
    FmQuitaDoc2.EdDoc.Text       := FloatToStr(Documento);
    FmQuitaDoc2.EdLancto.Caption := FloatToStr(Controle);
    FmQuitaDoc2.EdSub.Caption    := Geral.TFT(IntToStr(Sub), 0, siPositivo);
    FmQuitaDoc2.ShowModal;
    FmQuitaDoc2.Destroy;
    DefParams_Cart(QrCrt, nil, True, 'TUnFinanceiro.CriaFmQuitaDoc2()');
  end;
end;
}

function TUnFinanceiro.CSOSN_Get(CRT, Codigo: Integer): String;
begin
// C�digo de Situa��o da Opera��o no Simples Nacional
  if CRT = 1 then
  begin
    case Codigo of
      101: Result := 'Tributada pelo Simples Nacional com permiss�o de cr�dito.';
      102: Result := 'Tributada pelo Simples Nacional sem permiss�o de cr�dito.';
      103: Result := 'Isen��o do ICMS no Simples Nacional para faixa de receita bruta.';
      201: Result := 'Tributada pelo Simples Nacional com permiss�o de cr�dito e com cobran�a do ICMS por Substitui��o Tribut�ria';
      202: Result := 'Tributada pelo Simples Nacional sem permiss�o de cr�dito e com cobran�a do ICMS por Substitui��o Tribut�ria';
      203: Result := 'Isen��o do ICMS nos Simples Nacional para faixa de receita bruta e com cobran�a do ICMS por Substitui��o Tribut�ria';
      300: Result := 'Imune.';
      400: Result := 'N�o tributada pelo Simples Nacional';
      500: Result := 'ICMS cobrado anteriormente por substitui��o tribut�ria (substitu�do) ou por antecipa��o';
      900: Result := 'Outros';
      else Result := 'CSOSN inv�lido ou n�o implementado!';
    end;
  end else Result := 'CRT selecionado n�o prev� nenhum CSOSN!';
end;

function TUnFinanceiro.CSOSN_Lista: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const CRT, Codigo, Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  CRT;
    Res[Linha][1] :=  Codigo;
    Res[Linha][2] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA A ORIGEM DA MERCADORIA
  AddLinha(Result, Linha, '1', '101', 'Tributada pelo Simples Nacional com permiss�o de cr�dito.');
  AddLinha(Result, Linha, '1', '102', 'Tributada pelo Simples Nacional sem permiss�o de cr�dito.');
  AddLinha(Result, Linha, '1', '103', 'Isen��o do ICMS no Simples Nacional para faixa de receita bruta.');
  AddLinha(Result, Linha, '1', '201', 'Tributada pelo Simples Nacional com permiss�o de cr�dito e com cobran�a do ICMS por Substitui��o Tribut�ria');
  AddLinha(Result, Linha, '1', '202', 'Tributada pelo Simples Nacional sem permiss�o de cr�dito e com cobran�a do ICMS por Substitui��o Tribut�ria');
  AddLinha(Result, Linha, '1', '203', 'Isen��o do ICMS nos Simples Nacional para faixa de receita bruta e com cobran�a do ICMS por Substitui��o Tribut�ria');
  AddLinha(Result, Linha, '1', '300', 'Imune.');
  AddLinha(Result, Linha, '1', '400', 'N�o tributada pelo Simples Nacional');
  AddLinha(Result, Linha, '1', '500', 'ICMS cobrado anteriormente por substitui��o tribut�ria (substitu�do) ou por antecipa��o');
  AddLinha(Result, Linha, '1', '900', 'Outros');
end;

function TUnFinanceiro.CST_A_Get(Codigo: Integer): String;
begin
//TABELA A ORIGEM DA MERCADORIA
  case Codigo of
    0: Result := 'Nacional, exceto as indicadas nos c�digos 3 a 5';
    1: Result := 'Estrangeira - Importa��o direta, exceto a indicada no c�digo 6';
    2: Result := 'Estrangeira - Adquirida no mercado interno, exceto a indicada no c�digo 7';
    3: Result := 'Nacional, mercadoria ou bem com Conte�do de Importa��o superior a 40%';
    4: Result := 'Nacional, cuja produ��o tenha sido feita em conformidade com os processos produtivos b�sicos de que tratam as legisla��es citadas nos Ajustes';
    5: Result := 'Nacional, mercadoria ou bem com Conte�do de Importa��o inferior ou igual a 40%';
    6: Result := 'Estrangeira - Importa��o direta, sem similar nacional, constante em lista da CAMEX';
    7: Result := 'Estrangeira - Adquirida no mercado interno, sem similar nacional, constante em lista da CAMEX';
    else Result := '';
  end;
end;

function TUnFinanceiro.CST_A_Lista(): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 2);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA A ORIGEM DA MERCADORIA
//  Mudado em 2013-02-09  -  Nota T�cnica 2012.005c de 08/02/2013
{
  AddLinha(Result, Linha, '0', 'Nacional');
  AddLinha(Result, Linha, '1', 'Estrangeira Importa��o direta');
  AddLinha(Result, Linha, '2', 'Estrangeira Adquirida no mercado interno');
}
  AddLinha(Result, Linha, '0', 'Nacional, exceto as indicadas nos c�digos 3 a 5;');
  AddLinha(Result, Linha, '1', 'Estrangeira - Importa��o direta, exceto a indicada no c�digo 6;');
  AddLinha(Result, Linha, '2', 'Estrangeira - Adquirida no mercado interno, exceto a indicada no c�digo 7;');
  AddLinha(Result, Linha, '3', 'Nacional, mercadoria ou bem com Conte�do de Importa��o superior a 40%;');
  AddLinha(Result, Linha, '4', 'Nacional, cuja produ��o tenha sido feita em conformidade com os processos produtivos b�sicos de que tratam as legisla��es citadas nos Ajustes;');
  AddLinha(Result, Linha, '5', 'Nacional, mercadoria ou bem com Conte�do de Importa��o inferior ou igual a 40%;');
  AddLinha(Result, Linha, '6', 'Estrangeira - Importa��o direta, sem similar nacional, constante em lista da CAMEX;');
  AddLinha(Result, Linha, '7', 'Estrangeira - Adquirida no mercado interno, sem similar nacional, constante em lista da CAMEX.');
//  FIM Mudado em 2013-02-09
end;

function TUnFinanceiro.CST_B_Get(Codigo: Integer): String;
begin
//TABELA B TRIBUTA��O PELO ICMS
  case Codigo of
    00: Result := 'Tributada integralmente';
    10: Result := 'Tributada e com cobran�a do ICMS por substitui��o tribut�ria';
    20: Result := 'Com redu��o de base de c�lculo';
    30: Result := 'Isenta ou n�o tributada e com cobran�a do ICMS por substitui��o tribut�ria';
    40: Result := 'Isenta';
    41: Result := 'N�o tributada';
    50: Result := 'Suspens�o';
    51: Result := 'Diferimento';
    60: Result := 'ICMS cobrado anteriormente por substitui��o tribut�ria';
    70: Result := 'Com redu��o de base de c�lculo e cobran�a do ICMS por substitui��o tribut�ria';
    90: Result := 'Outras';
    else Result := '';
  end;
end;

function TUnFinanceiro.CST_B_Lista(): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 2);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA B TRIBUTA��O PELO ICMS
  AddLinha(Result, Linha, '00', 'Tributada integralmente');
  AddLinha(Result, Linha, '10', 'Tributada e com cobran�a do ICMS por substitui��o tribut�ria');
  AddLinha(Result, Linha, '20', 'Com redu��o de base de c�lculo');
  AddLinha(Result, Linha, '30', 'Isenta ou n�o tributada e com cobran�a do ICMS por substitui��o tribut�ria');
  AddLinha(Result, Linha, '40', 'Isenta');
  AddLinha(Result, Linha, '41', 'N�o Tributado (v2.0)');
  AddLinha(Result, Linha, '50', 'Suspens�o');
  AddLinha(Result, Linha, '51', 'Diferimento');
  AddLinha(Result, Linha, '60', 'Cobrado anteriormente por substitui��o tribut�ria');
  AddLinha(Result, Linha, '70', 'Com redu��o de base de c�lculo e cobran�a do ICMS por substitui��o tribut�ria');
  AddLinha(Result, Linha, '90', 'Outras');
end;

function TUnFinanceiro.CST_IPI_Get(CodTxt: String): String;
var
  Codigo: Integer;
begin
 if CodTxt <> EmptyStr then
 begin
   Codigo := Geral.IMV(CodTxt);
    //TABELA TRIBUTA��O PELO IPI
    case Codigo of
      00: Result := 'Entrada com recupera��o de cr�dito';
      01: Result := 'Entrada tributada com aliquota zero';
      02: Result := 'Entrada isenta';
      03: Result := 'Entrada n�o tributada';
      04: Result := 'Entrada imune';
      05: Result := 'Entrada com suspens�o';
      49: Result := 'Outras entradas';
      50: Result := 'Sa�da tributada';
      51: Result := 'Sa�da tributada com aliquota zero';
      52: Result := 'Sa�da isenta';
      53: Result := 'Sa�da n�o tributada';
      54: Result := 'Sa�da imune';
      55: Result := 'Sa�da com suspens�o';
      99: Result := 'Outras sa�das';
      else Result := '? ? ? ? ?';
    end;
 end else
   Result := '';
end;

function TUnFinanceiro.CST_IPI_Lista: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 2);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA TRIBUTA��O PELO IPI
  AddLinha(Result, Linha, '00', 'Entrada com recupera��o de cr�dito');
  AddLinha(Result, Linha, '01', 'Entrada tributada com aliquota zero');
  AddLinha(Result, Linha, '02', 'Entrada isenta');
  AddLinha(Result, Linha, '03', 'Entrada n�o tributada');
  AddLinha(Result, Linha, '04', 'Entrada imune');
  AddLinha(Result, Linha, '05', 'Entrada com suspens�o');
  AddLinha(Result, Linha, '49', 'Outras entradas');
  AddLinha(Result, Linha, '50', 'Sa�da tributada');
  AddLinha(Result, Linha, '51', 'Sa�da tributada com aliquota zero');
  AddLinha(Result, Linha, '52', 'Sa�da isenta');
  AddLinha(Result, Linha, '53', 'Sa�da n�o tributada');
  AddLinha(Result, Linha, '54', 'Sa�da imune');
  AddLinha(Result, Linha, '55', 'Sa�da com suspens�o');
  AddLinha(Result, Linha, '99', 'Outras sa�das');
end;

function TUnFinanceiro.CST_PIS_Get(CodTxt: String): String;
var
  Codigo: Integer;
begin
  if CodTxt <> EmptyStr then
  begin
    Codigo := Geral.IMV(CodTxt);
    //TABELA TRIBUTA��O PELO PIS
    case Codigo of
      01: Result := 'Opera��o tribut�vel (base de c�lculo = valor da opera��o x al�quota normal (cumulativo / n�o cumulativo))';
      02: Result := 'Opera��o tribut�vel (base de c�lculo = valor da opera��o x (al�quota diferenciada))';
      03: Result := 'Opera��o tribut�vel (base de c�lculo = quantidade vendida x al�quota por unidade de produto)';
      04: Result := 'Opera��o tribut�vel (tributa��o monof�sica (al�quota zero))';
      05: Result := 'Opera��o Tribut�vel (Substitui��o Tribut�ria);';
      06: Result := 'Opera��o tribut�vel (al�quota zero)';
      07: Result := 'Opera��o isenta da contribui��o';
      08: Result := 'Opera��o sem incid�ncia da contribui��o';
      09: Result := 'Opera��o com suspens�o da contribui��o';
      //
      49: Result := 'Outras Opera��es de Sa�da;';
      50: Result := 'Opera��o com Direito a Cr�dito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno;';
      51: Result := 'Opera��o com Direito a Cr�dito - Vinculada Exclusivamente a Receita N�o Tributada no Mercado Interno;';
      52: Result := 'Opera��o com Direito a Cr�dito � Vinculada Exclusivamente a Receita de Exporta��o;';
      53: Result := 'Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno;';
      54: Result := 'Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas no Mercado Interno e de Exporta��o;';
      55: Result := 'Opera��o com Direito a Cr�dito - Vinculada a Receitas N�o-Tributadas no Mercado Interno e de Exporta��o;';
      56: Result := 'Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno, e de Exporta��o;';
      //
      60: Result := 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita Tributada no Mercado Interno;';
      61: Result := 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita N�o-Tributada no Mercado Interno;';
      62: Result := 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita de Exporta��o;';
      63: Result := 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno;';
      64: Result := 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas no Mercado Interno e de Exporta��o;';
      65: Result := 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas N�o-Tributadas no Mercado Interno e de Exporta��o;';
      66: Result := 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno, e de Exporta��o;';
      67: Result := 'Cr�dito Presumido - Outras Opera��es;';
      //
      70: Result := 'Opera��o de Aquisi��o sem Direito a Cr�dito;';
      71: Result := 'Opera��o de Aquisi��o com Isen��o;';
      72: Result := 'Opera��o de Aquisi��o com Suspens�o;';
      73: Result := 'Opera��o de Aquisi��o a Al�quota Zero;';
      74: Result := 'Opera��o de Aquisi��o; sem Incid�ncia da Contribui��o;';
      75: Result := 'Opera��o de Aquisi��o por Substitui��o Tribut�ria;';
      //
      98: Result := 'Outras Opera��es de Entrada;';
      99: Result := 'Outras Opera��es;'
      else Result := '? ? ? ? ?';
    end;
  end;
end;

function TUnFinanceiro.CST_PIS_Lista: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 2);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA TRIBUTA��O PELO PIS
  AddLinha(Result, Linha, '01', 'Opera��o tribut�vel (base de c�lculo = valor da opera��o x al�quota normal (cumulativo / n�o cumulativo))');
  AddLinha(Result, Linha, '02', 'Opera��o tribut�vel (base de c�lculo = valor da opera��o x (al�quota diferenciada))');
  AddLinha(Result, Linha, '03', 'Opera��o tribut�vel (base de c�lculo = quantidade vendida x al�quota por unidade de produto)');
  AddLinha(Result, Linha, '04', 'Opera��o tribut�vel (tributa��o monof�sica (al�quota zero))');
  AddLinha(Result, Linha, '05', 'Opera��o Tribut�vel (Substitui��o Tribut�ria);');
  AddLinha(Result, Linha, '06', 'Opera��o tribut�vel (al�quota zero)');
  AddLinha(Result, Linha, '07', 'Opera��o isenta da contribui��o');
  AddLinha(Result, Linha, '08', 'Opera��o sem incid�ncia da contribui��o');
  AddLinha(Result, Linha, '09', 'Opera��o com suspens�o da contribui��o');
  //
  AddLinha(Result, Linha, '49', 'Outras Opera��es de Sa�da;');
  AddLinha(Result, Linha, '50', 'Opera��o com Direito a Cr�dito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno;');
  AddLinha(Result, Linha, '51', 'Opera��o com Direito a Cr�dito - Vinculada Exclusivamente a Receita N�o Tributada no Mercado Interno;');
  AddLinha(Result, Linha, '52', 'Opera��o com Direito a Cr�dito � Vinculada Exclusivamente a Receita de Exporta��o;');
  AddLinha(Result, Linha, '53', 'Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno;');
  AddLinha(Result, Linha, '54', 'Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas no Mercado Interno e de Exporta��o;');
  AddLinha(Result, Linha, '55', 'Opera��o com Direito a Cr�dito - Vinculada a Receitas N�o-Tributadas no Mercado Interno e de Exporta��o;');
  AddLinha(Result, Linha, '56', 'Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno, e de Exporta��o;');
    //
  AddLinha(Result, Linha, '60', 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita Tributada no Mercado Interno;');
  AddLinha(Result, Linha, '61', 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita N�o-Tributada no Mercado Interno;');
  AddLinha(Result, Linha, '62', 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita de Exporta��o;');
  AddLinha(Result, Linha, '63', 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno;');
  AddLinha(Result, Linha, '64', 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas no Mercado Interno e de Exporta��o;');
  AddLinha(Result, Linha, '65', 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas N�o-Tributadas no Mercado Interno e de Exporta��o;');
  AddLinha(Result, Linha, '66', 'Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno, e de Exporta��o;');
  AddLinha(Result, Linha, '67', 'Cr�dito Presumido - Outras Opera��es;');
    //
  AddLinha(Result, Linha, '70', 'Opera��o de Aquisi��o sem Direito a Cr�dito;');
  AddLinha(Result, Linha, '71', 'Opera��o de Aquisi��o com Isen��o;');
  AddLinha(Result, Linha, '72', 'Opera��o de Aquisi��o com Suspens�o;');
  AddLinha(Result, Linha, '73', 'Opera��o de Aquisi��o a Al�quota Zero;');
  AddLinha(Result, Linha, '74', 'Opera��o de Aquisi��o; sem Incid�ncia da Contribui��o;');
  AddLinha(Result, Linha, '75', 'Opera��o de Aquisi��o por Substitui��o Tribut�ria;');
    //
  AddLinha(Result, Linha, '98', 'Outras Opera��es de Entrada;');
  AddLinha(Result, Linha, '99', 'Outras opera��es');
end;

function TUnFinanceiro.DefineDescricaoMyPagtos(FatID: Integer;
  DescrEspecif, Caption: String): String;
begin
  if DescrEspecif <> EmptyStr then
    Result := DescrEspecif
  else
  begin
    case VAR_FATIDTXT of
      5 : Result := 'Entrada por compra';
      7 :
      begin
        if TMeuDB = 'Emporium' then
        Result := 'Devolu��o de mercadorias de cliente'
        else Result := VAR_MSG_FATURAMPPS;
      end;
      8 :
      begin
        if TMeuDB = 'Emporium' then
        Result := 'Frete de devolu��o de mercadorias de cliente'
        else Result := VAR_MSG_FRETEMPPS;
      end;
      9 : Result := 'Devolu��o de mercadorias a fornecedor';
      10 : Result := 'Frete de devolu��o de mercadorias a fornecedor';
      11 : Result := 'Compra de mercadorias';
      12 : Result := 'Frete de compra de mercadorias';
      13 : Result := 'Venda de mercadorias';
      14 : Result := 'Frete de Venda de mercadorias';
      15 : Result := 'Venda de Servi�os';
      16 : Result := 'Frete de Servi�os';
      17 : Result := 'C/MM';                               // Recovery
      18 : Result := 'F/MM';                               // Recovery
      19 : Result := 'C/M';                                // Recovery
      20 : Result := 'F/C';                                // Recovery
      21 : Result := 'V/M';                                // Recovery
      22 : Result := 'F/V';                                // Recovery
      23 : Result := 'C/D';                                // Recovery
      24 : Result := 'M/D';                                // Recovery

      30 : Result := 'Compra de mercadorias diversas';  // Seven
      31 : Result := 'Frete de mercadorias diversas';   // Seven
      32 : Result := 'Compra de filmes';                // Seven
      33 : Result := 'Frete de filmes';                 // Seven
      34 : Result := 'Venda de filmes';                 // Seven
      35 : Result := 'L/V';                             // Seven
      //
      43 : Result := '';                                // EstoqueM
      44 : Result := '';                                // EstoqueM
      45 : Result := '';                                // EstoqueM
      46 : Result := '';                                // EstoqueM
      47 : Result := '';                                // EstoqueM
      48 : Result := '';                                // EstoqueM
      49 : Result := '';                                // EstoqueM
      50 : Result := '';                                // EstoqueM
      51 : Result := '';                                // EstoqueM
      52 : Result := '';                                // EstoqueM
      63 : Result := '';                                // EstoqueM
      64 : Result := '';                                // EstoqueM
      65 : Result := '';                                // EstoqueM
      66 : Result := '';                                // EstoqueM
      67 : Result := '';                                // EstoqueM
      68 : Result := '';                                // EstoqueM
      69 : Result := '';                                // EstoqueM
      //70 : Result := FResult;                        // Entrada por importa��o de conta telef�nica
      //71 : Result := FResult;                        // Entrada por consumo por leitura (empresa)
      72 : Result := '';                                // EstoqueM
      81 : Result := 'Servi�o';                         // Servicos
      101: Result := 'Matr�cula';                       // Teach 2
      102: Result := 'F/M';
      103: Result := 'C/M';

      105: Result := 'V/M';

      119: Result := 'Compra de Mercadorias';           // LetWear
      120: Result := 'Frete de Compra de Mercadorias';  // LetWear
      121: Result := 'Loca��o de Mercadorias';          // LetWear

      300..399: Result := '';                          // Cred itor

      400: Result := 'PA';                              // IRent
      //
      500: Result := 'CM';                              // GigaStore
      510: Result := 'VM';                              // GigaStore
      721: Result := 'VMC';                             // LeSew
      810: Result := 'VML';                             // LeSew
      901: Result := 'Faturamento de Servi�o';          // DControl
      902: Result := 'PE';                              // SafeCar
      903: Result := 'FR';                              // SafeCar
      1001: Result := VAR_MSG_INSUMOQUIMICO;
      1002: Result := VAR_MSG_FRETEPQ;
      1003: Result := VAR_MSG_MATERIAPRIMA;
      1004: Result := VAR_MSG_FRETEMP;
      1005: Result := VAR_MSG_INSUMOQUIMICOPS;
      1006: Result := VAR_MSG_FRETEPQPS;
      1007: Result := 'VCV'; //Venda de couro verde
      1008: Result := 'FVCV';//Frete de venda de couro verde
      1010: Result := 'Comiss�o CMP';//Comiss�o compra de Mat�ria-Prima
      1013: Result := 'FP';//Faturamento de pedido
      1014: Result := 'FFP';//Frete de faturamento de pedido
      2003: Result := 'CM'; // Academy - Compra de mercadorias diversas
      2101: Result := 'PM'; // Academy - Pagamento de matr�cula
      2102: Result := 'CM'; // Academy - Compra de mercadorias diversas
      2103: Result := 'TM'; // Academy - Transporte de mercadorias compradas
      2105: Result := 'CM'; // Academy - Compra de mercadorias diversas
      else if (Caption <> '') and (VAR_FATIDTXT < 0)  then Result := Caption
      else
      Geral.MensagemBox('VAR_FATIDTXT Indefinido'+sLineBreak+
      'VAR_FATIDTXT = '+IntToStr(VAR_FATIDTXT), 'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
end;

function TUnFinanceiro.DefLctFldSdoIni(DtIni, DtEncer, DtMorto: TDateTime): String;
begin
  if DtIni > DtEncer then
    Result := 'car.SdoFimB'
  else
  if DtIni > DtMorto then
    Result := '0.00' // Evitar erro de Float para LargeInt
  else Result := '0.00'; // Evitar erro de Float para LargeInt
end;

function TUnFinanceiro.DefLctTab(DtIni, DtEncer, DtMorto: TDateTime;
TabLctA, TabLctB, TabLctD: String): String;
begin
  if DtIni > DtEncer then
    Result := TabLctA
  else
  if DtIni > DtMorto then
    Result := TabLctB
  else Result := TabLctD;
end;

procedure TUnFinanceiro.DesfazerCompensacao(GridLct: TDBGrid; QueryCrt,
  QueryLct: TmySQLQuery; TabLctA: String);
var
  Msg: String;
  i, k: Integer;
  Carteira(*, Tipo*): Integer;
begin
  k := 0;
  if GridLct.SelectedRows.Count > 1 then
  begin
    if Geral.MB_Pergunta('Confirma a revers�o da compensa��o dos itens selecionados?') = ID_YES then
    begin
      Carteira := QueryCrt.FieldByName('Codigo').AsInteger;
      //
      with GridLct.DataSource.DataSet do
      for i:= 0 to GridLct.SelectedRows.Count-1 do
      begin
        // precisa ser aqui pois abaixo busca zero se desfazer tudo!!!!
        k := QueryLct.FieldByName('Controle').AsInteger;
        //
        //GotoBookmark(GridLct.SelectedRows.Items[i]);
        GotoBookmark(GridLct.SelectedRows.Items[i]);
        //
        if VerificaSeLanctoEParcelamento(QueryLct, Msg) then
        begin
          Geral.MB_Aviso(Msg);
          Exit;
        end;
        //
        if QueryLct.FieldByName('Sit').AsInteger = 3 then
          ReverterPagtoEmissao(QueryLct, QueryCrt, False, False, False, TabLctA);
      end;
      RecalcSaldoCarteira(Carteira, QueryCrt, QueryLct, True, True);
      QueryLct.Locate('Controle', k, []);
    end;
  end else
  begin
    if VerificaSeLanctoEParcelamento(QueryLct, Msg) then
    begin
      Geral.MB_Aviso(Msg);
      Exit;
    end;
    ReverterPagtoEmissao(QueryLct, QueryCrt, True, True, True, TabLctA);
  end;
end;

function TUnFinanceiro.DesmontaSPED_COD_CTA(const COD_CTA: String; var Nivel,
  Genero: Integer): Boolean;
var
  Ponto: Integer;
begin
  Result := False;
  Ponto := pos('.', COD_CTA);
  if Ponto > 0 then
  begin
    Nivel  := Geral.IMV(Copy(COD_CTA, 1, Ponto - 1));
    Genero := Geral.IMV(Copy(COD_CTA, Ponto + 1));
    Result := True;
  end;
end;

function TUnFinanceiro.CST_COFINS_Get(CodTxt: String): String;
begin
//TABELA TRIBUTA��O PELO COFINS (� igual ao PIS)
  Result := CST_PIS_Get(CodTxt);
end;

function TUnFinanceiro.CST_COFINS_Lista: MyArrayLista;
begin
//TABELA TRIBUTA��O PELO COFINS (� igual ao PIS)
  Result := CST_PIS_Lista();
end;

{
procedure TUnFinanceiro.AtualizaEndossos(Controle, Sub, OriCtrl, OriSub: Integer);
  procedure Atualiza_Atual(C, S: Integer; Endossado: Boolean);
  var
    Status: Integer;
    Sit1, Sit2: Boolean;
    Valor: Double;
  begin
    if (C = 0) and (S = 0) then Exit;
    //
    Sit1 := False;
    Sit2 := False;
    Valor := 0;
    DModFin.QrEndosso.First;
    while not DModFin.QrEndosso.Eof do
    begin
      if (DModFin.QrEndossoControle.Value = C)
      and (DModFin.QrEndossoSub.Value = S) then
      begin
        Sit1 := True;
        Valor := Valor + DModFin.QrEndossoValor.Value;
      end;
      if (DModFin.QrEndossoOriCtrl.Value = C)
      and (DModFin.QrEndossoOriSub.Value = S) then
        Sit2 := True;
      //
      DModFin.QrEndosso.Next;
    end;
    if Sit1 and Sit2 then
      Status := 3 //  Ambos
    else
    if Sit1 then
      Status := 1 // Endossado
    else
    if Sit2 then
      Status := 2  // Endossante
    else Status := 0; // Nenhum
    //
    if Endossado then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Endossado=:P0, EndossVal=:P1 ');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
      Dmod.QrUpd.Params[00].AsInteger := Status;
      Dmod.QrUpd.Params[01].AsFloat   := Valor;
      //
      Dmod.QrUpd.Params[02].AsInteger := C;
      Dmod.QrUpd.Params[03].AsInteger := S;
      Dmod.QrUpd.ExecSQL;
    end else begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Endossado=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
      Dmod.QrUpd.Params[00].AsInteger := Status;
      //
      Dmod.QrUpd.Params[01].AsInteger := C;
      Dmod.QrUpd.Params[02].AsInteger := S;
      Dmod.QrUpd.ExecSQL;
    end;
    AtualizaEmissaoMasterRapida(C, 3, 0);
  end;
begin
  DModFin.QrEndosso.Close;
  DModFin.QrEndosso.Params[00].AsInteger := Controle;
  DModFin.QrEndosso.Params[01].AsInteger := Sub;
  DModFin.QrEndosso.Params[02].AsInteger := Controle;
  DModFin.QrEndosso.Params[03].AsInteger := Sub;
  UnDmkDAC_PF.AbreQuery(DModFin.QrEndosso, Dmod.MyDB);
  Atualiza_Atual(Controle, Sub, True);
  //
  DModFin.QrEndosso.Close;
  DModFin.QrEndosso.Params[00].AsInteger := OriCtrl;
  DModFin.QrEndosso.Params[01].AsInteger := OriSub;
  DModFin.QrEndosso.Params[02].AsInteger := OriCtrl;
  DModFin.QrEndosso.Params[03].AsInteger := OriSub;
  UnDmkDAC_PF.AbreQuery(DModFin.QrEndosso, Dmod.MyDB);
  Atualiza_Atual(OriCtrl, OriSub, False);
  //
end;
}

procedure TUnFinanceiro.ReabreCarteirasCliInt(CliInt, LocCart: Integer;
  QrCrt, QrCrtSum: TmySQLQuery);
var
  N: Integer;
begin
  QrCrt.Close;
  N := QrCrt.Params.Count;
  if N = 1 then
    QrCrt.Params[0].AsInteger := CliInt;
  UnDmkDAC_PF.AbreQuery(QrCrt, Dmod.MyDB);
  //
  if (not QrCrt.Locate('Codigo', LocCart, [])) and (N <> 1) then
  begin
    Geral.MB_Erro(
    'Query de carteiras com n�mero de par�mentros diferente de 1!');
    UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrCrt, '', nil, True, True);
  end;
  //
  if QrCrtSum <> nil then
  begin
    QrCrtSum.Close;
    if QrCrt.Params.Count = 1 then
      QrCrtSum.Params[0].AsInteger := CliInt;
    UnDmkDAC_PF.AbreQuery(QrCrtSum, Dmod.MyDB);
    //
    {
    if ? then

      Geral.MB_Aviso(
      'Query de soma de carteiras com n�mero de par�mentros diferente de 1!',
      );
      LeMeuSQLy(QrCrtSum, '', nil, True, True);
    end;
    }
  end;
end;

function TUnFinanceiro.NomeDoNivelSelecionado(Tipo, Nivel: Integer): String;
begin
  case Tipo of
    1:
    case Nivel of
      1: Result := 'contas';
      2: Result := 'subgrupos';
      3: Result := 'grupos';
      4: Result := 'conjunto';
      5: Result := 'plano';
      else Result := '?';
    end;
    2:
    case Nivel of
      1: Result := 'la.Genero';
      2: Result := 'co.Subgrupo';
      3: Result := 'sg.Grupo';
      4: Result := 'gr.Conjunto';
      5: Result := 'cj.Plano';
      else Result := '?';
    end;
    3:
    case Nivel of
      1: Result := 'da Conta';
      2: Result := 'do Sub-grupo';
      3: Result := 'do Grupo';
      4: Result := 'do Conjunto';
      5: Result := 'do Plano';
      else Result := ' do Nivel ? ';
    end;
    else Result := '(Tipo)=?';
  end;
end;

function TUnFinanceiro.NomeRelacionado(Cliente, Fornecedor: Integer;
  NomeCliente, NomeFornecedor: String): String;
begin
  Result := CO_VAZIO;
  //
  if Cliente <> 0 then
    Result := NomeCliente;
  if Fornecedor <> 0 then
    Result := Result + NomeFornecedor;
end;

function TUnFinanceiro.Mensal2(Mes2: Largeint; Ano: Double): String;
begin
  if Mes2 > 0 then
    Result := FormatFloat('0000', Ano)+'/' + FormatFloat('00', Mes2)+'/01'
  else
    Result := CO_VAZIO;
end;

function TUnFinanceiro.NomeSitLancto(LctSit, LanctoTipo, CarteiraPrazo:
         Integer; LanctoVencto: TDateTime; Reparcelamento: Integer;
         ConverteCompensadoEmQuitado: Boolean = False): String;
begin
//  if LctSit = -2 then (Editando em LocLancto - n�o � setado em -2, � usado VAR_BAIXADO setado a -2)
  if Reparcelamento <> 0 then
     //reparcelado nos bloquetos ?
     Result := CO_REPARCELADO
  else
  case LctSit of
    -1: Result := CO_IMPORTACAO;
    00:
    begin
      if Int(LanctoVencto) < int(Date) then
         Result := CO_VENCIDA
      else
         Result := CO_EMABERTO;
    end;
    01: Result := CO_PAGTOPARCIAL;
    02: Result := CO_QUITADA;
    03:
    begin
      // Caixa com prazo
      if (CarteiraPrazo = 1) and (LanctoTipo = 0) then
      begin
        if LanctoVencto < Date then
          Result := CO_QUIT_AUTOM
        else
          Result := CO_PREDATADO;
      end else
      begin
       if ConverteCompensadoEmQuitado then
         Result := CO_QUITADA
       else
         Result := CO_COMPENSADA;
      end;
    end;
    04: Result := CO_CANCELADO;
    05: Result := CO_BLOQUEADO; // Comiss�o de vendedores condicionado ao pagamento pelo fornecedor
    06: Result := CO_DEVOLVIDO; //
    //N�o usar n�meros acima de 3 para quitados!!!!

    else Result := '? ? ? ? ?';
  end;
end;

function TUnFinanceiro.ObtemListaNomeFatID(AppId: Integer): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer;
    const FatID: Integer; const Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 2);
    Res[Linha][0] := Geral.FF0( FatID);
    Res[Linha][1] := Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  //
  case AppId of
    24: //Bugstrol
    begin
      AddLinha(Result, Linha, VAR_FATID_4001, 'Faturamento de servi�o');
    end;
    28: //Toolrent
    begin
      AddLinha(Result, Linha, VAR_FatID_3001, 'Faturamento parcial/ final de loca��o');
      AddLinha(Result, Linha, VAR_FatID_3002, 'Faturamento de servi�o');
    end;
    55: //YROd
    begin
      AddLinha(Result, Linha, VAR_FatID_7001, 'Faturamento de venda/servi�o');
    end;
  end;
end;

function TUnFinanceiro.ObtemProximaOrdemNoNivelDoPlanoDeContas(
  Tabela, CampoIdx, CampoAnd: String; IDAnd: Integer): Integer;
var
  FormaLoc: Integer;
  I, N, NewID: Integer;
  Mdq: TMySQLDirectQuery;
  SQL_WHERE: String;
begin
  SQL_WHERE := '';
  Mdq := TMySQLDirectQuery.Create(Dmod);
  try
    Mdq.Database := Dmod.MyDB;
    Result := -1;
    FormaLoc := MyObjects.SelRadioGroup('Forma de procura',
    'Escolha a forma de procura', ['Primeiro livre dispon�vel',
    'Pr�ximo incremento'], 2, 1, (*SelOnClick*)True);
    //
    if FormaLoc = 0 then
    begin
      if CampoAnd <> EmptyStr then
        SQL_WHERE := 'AND ' + CampoAnd + '=' + Geral.FF0(IDAnd)
      else SQL_WHERE := '';
      I := 0;
      Mdq.SQL.Text := 'SELECT ' + CampoIdx + ' ID FROM ' +
        Lowercase(Tabela) + ' WHERE ' + CampoIdx + ' > 0 ' +
        SQL_WHERE + ' ORDER BY ID';
      Mdq.Open;
      Screen.Cursor := crHourGlass;
      try
        while not Mdq.Eof do
        begin
          I := I + 1;
          N := StrToInt('0' + Mdq.FieldValueByFieldName('ID'));
          if I < N then
          begin
            Result := I;
            Exit;
          end;
          Mdq.Next;
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
    if (FormaLoc = 1) or (Result = -1) then
    begin
      if CampoAnd <> EmptyStr then
        SQL_WHERE := ' WHERE ' + CampoAnd + '=' + Geral.FF0(IDAnd)
      else
        SQL_WHERE := '';
      Dmod.QrAux.Close;
      Dmod.QrAux.Database := Dmod.MyDB;
      Dmod.QrAux.SQL.Text := 'SELECT MAX(' + CampoIdx + ') ID FROM ' +
        Lowercase(Tabela) + SQL_WHERE;
      Dmod.QrAux.Open;
      Result := Dmod.QrAux.Fields[0].AsInteger;
      Result := Result + 1;
    end;
    Mdq.Close;
  finally
    Mdq.Free;
  end;
  if Result = -1 then
    result := 0;
end;

function TUnFinanceiro.ObtemProximoPlaFluCad(SQLType: TSQLType; Codigo, Nivel: Integer): Integer;
begin
  if SQLType = stUpd then
    Result := Codigo
  else
  begin
    Codigo := UMyMod.BPGS1I32('plaflucad', 'Codigo', '', '', tsPos, SQLType, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'plaflucad', False, [
    'Nivel'], [
    'Codigo'], [
    Nivel], [
    Codigo], True) then
    begin
      Result := Codigo;
    end;
  end;
end;

function TUnFinanceiro.ObtemSaldoCarteira(Carteira: Integer): Double;
var
  Qry: TMySQLQuery;
  MyCursor: TCursor;
  Empresa: Integer;
  Inicial: Double;
begin
  Qry := nil;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    //
    //AtualizaVencimentos();
    //
    Qry:= TmySQLQuery.Create(Dmod);
    Qry.Database := Dmod.MyDB;
    Qry.SQL.Add('SELECT car.Tipo, car.SdoFimB, eci.CodCliInt');
    Qry.SQL.Add('FROM carteiras car');
    Qry.SQL.Add('LEFT JOIN enticliint eci ON eci.CodEnti=car.ForneceI');
    Qry.SQL.Add('WHERE Codigo=' + FormatFloat('0', Carteira));
    UnDmkDAC_PF.AbreQuery(Qry, Dmod.MyDB);
    //
    if Qry.FieldByName('Tipo').AsInteger <> 2 then
    begin
      Empresa := Qry.FieldByName('CodCliInt').AsInteger;
      Inicial := Qry.FieldByName('SdoFimB').AsFloat;
      //
      Qry.Close;
      Qry.SQL.Clear;
      Qry.SQL.Add('SELECT ');
      Qry.SQL.Add('SUM(Credito-Debito) Saldo');
      Qry.SQL.Add('FROM ' + DModG.NomeTab(TMeuDB, ntLct, False, ttA, Empresa));
      Qry.SQL.Add('WHERE Carteira=' + FormatFloat('0', Carteira));
      UnDmkDAC_PF.AbreQuery(Qry, Dmod.MyDB);
      //LeMeuSQLy(Qry, '', nil, True, True);
      Result := Qry.FieldByName('Saldo').AsFloat + Inicial;
    end else Result := 0;
    //
  finally
    Screen.Cursor := MyCursor;
    if Qry <> nil then
      Qry.Free;
    //
  end;
end;

function TUnFinanceiro.ObtemSequenciaDeCodigo_MotivExclusaoLct(Campo,
  NO_Campo: String): String;
var
  Texto, MotivoStr: String;
  I, MotivoCod: Integer;
  LstCod, LstStr: TStringList;
begin
  LstCod := TStringList.Create;
  LstStr := TStringList.Create;
  try
    for I := Low(ListaMotivosExclusao_Int) to High(ListaMotivosExclusao_Int) do
    begin
      MotivoCod := ListaMotivosExclusao_Int[I];
      MotivoStr := ListaMotivosExclusao_Str[I];
      //
      if (MotivoCod = 0) or ((MotivoCod >= 100) and (MotivoCod <= 320)) then
      begin
        LstCod.Add(Geral.FF0(MotivoCod));
        LstStr.Add(MotivoStr);
      end;
    end;
    //
    Texto := 'CASE ' + Campo;
    //
    for I := 0 to LstCod.Count - 1 do
    begin
      Texto := Texto + ' WHEN ' + LstCod[I] + ' THEN "' + LstStr[i] + '" ';
    end;
    Texto := Texto + ' ELSE "? ? ? ? ? " END ' + NO_Campo;
  finally
    LstCod.Free;
    LstStr.Free;
  end;
  Result := Texto;
end;

end.

