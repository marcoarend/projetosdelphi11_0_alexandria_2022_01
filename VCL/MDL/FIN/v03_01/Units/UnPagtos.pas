unit UnPagtos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  mySQLDbTables, ComCtrls, dmkDBGrid, MyDBCheck, DBCtrls, dmkGeral, DBGrids,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, dmkEditDateTimePicker, stdctrls, Db,
  dmkLabel, dmkRadioGroup, dmkMemo, dmkdbEdit, dmkCheckBox, Variants,
  UnDmkEnums;

type
  TArr_dVenc = array of String;
  TArr_vDup =  array of Double;
  //
  TUnPagtos = class(TObject)
  private
    { Private declarations }
    function PreparaPagto1(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
              Terceiro, FatID, Genero, GenCtb: Integer; SQLType: TSQLType; Titulo:
              String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
              UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig:
              Boolean; Carteira, Parcelas, Periodic, DiasParc: Integer;
              NotaFiscal: Integer; TabLctA: String; Descricao: String = '';
              SerieNF: String = ''; Qtde: Double = 0; Qtd2: Double = 0): Boolean;
    function PreparaPagto2(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
              Terceiro, FatID, Genero, GenCtb: Integer; SQLType: TSQLType; Titulo:
              String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
              UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig:
              Boolean; Carteira, Parcelas, Periodic, DiasParc: Integer;
              NotaFiscal: Integer; TabLctA: String; Descricao: String = '';
              SerieNF: String = ''; Qtde: Double = 0; Qtd2: Double = 0;
              GenCtbD: Integer = 0; GenCtbC: Integer = 0; CentroCusto: Integer
              = 0; FatParcRef_PgtoAdiantado: Integer = 0): Boolean;

  public
    { Public declarations }
    // ini 2022-03-22 - Tempor�rio
    procedure Pagto(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
              Terceiro, FatID, Genero, GenCtb: Integer; SQLType: TSQLType; Titulo:
              String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
              UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig:
              Boolean; Carteira, Parcelas, Periodic, DiasParc: Integer;
              NotaFiscal: Integer; TabLctA: String; Descricao: String = '';
              SerieNF: String = ''; Qtde: Double = 0; Qtd2: Double = 0;
              GenCtbD: Integer = 0; GenCtbC: Integer = 0;
              CentroCusto: Integer = 0; FatParcRef_PgtoAdiantado: Integer = 0);
    procedure PgFatID_XML(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
              Terceiro, FatID, UpdGener: Integer; SQLType: TSQLType; Titulo:
              String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
              UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig:
              Boolean; Carteira, Parcelas, Periodic, DiasParc: Integer;
              TabLctA: String; Arr_dVenc: TArr_dVenc; Arr_vDup: TArr_vDup;
              Duplicata: String; Descricao: String = '';
              SerieNF: String = ''; Qtde: Double = 0; Qtd2: Double = 0;
              MeAvisos: TMemo = nil);
    // fim 2022-03-22 - Tempor�rio

    procedure PgFatID1(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
              Terceiro, FatID, UpdGener: Integer; SQLType: TSQLType; Titulo:
              String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
              UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig:
              Boolean; Carteira, Parcelas, Periodic, DiasParc: Integer;
              TabLctA: String);
    procedure PgFatID_XML1(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
              Terceiro, FatID, UpdGener: Integer; SQLType: TSQLType; Titulo:
              String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
              UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig:
              Boolean; Carteira, Parcelas, Periodic, DiasParc: Integer;
              TabLctA: String; Arr_dVenc: TArr_dVenc; Arr_vDup: TArr_vDup;
              Duplicata: String; Descricao: String = '';
              SerieNF: String = ''; Qtde: Double = 0; Qtd2: Double = 0);
    procedure Pagto1(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
              Terceiro, FatID, Genero, GenCtb: Integer; SQLType: TSQLType; Titulo:
              String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
              UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig:
              Boolean; Carteira, Parcelas, Periodic, DiasParc: Integer;
              NotaFiscal: Integer; TabLctA: String; Descricao: String = '';
              SerieNF: String = ''; Qtde: Double = 0; Qtd2: Double = 0);
    procedure Pagto2(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
              Terceiro, FatID, Genero, GenCtb: Integer; SQLType: TSQLType; Titulo:
              String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
              UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig:
              Boolean; Carteira, Parcelas, Periodic, DiasParc: Integer;
              NotaFiscal: Integer; TabLctA: String; Descricao: String = '';
              SerieNF: String = ''; Qtde: Double = 0; Qtd2: Double = 0;
              GenCtbD: Integer = 0; GenCtbC: Integer = 0;
              CentroCusto: Integer = 0; FatParcRef_PgtoAdiantado: Integer = 0);
    procedure Pagto_Arr1(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
              Terceiro, FatID, Genero, GenCtb: Integer; SQLType: TSQLType; Titulo:
              String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
              UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig:
              Boolean; Carteira, Parcelas, Periodic, DiasParc: Integer;
              NotaFiscal: Integer; TabLctA: String;

              Arr_dVenc: TArr_dVenc; Arr_vDup: TArr_vDup; Duplicata: String;

              Descricao: String = '';
              SerieNF: String = ''; Qtde: Double = 0; Qtd2: Double = 0);

  end;

var
  UPagtos: TUnPagtos;

const
  CO_GenCtb_0 = 0;

implementation

uses UnMyObjects, MyPagtos, MyPagtos2, UnInternalConsts, UnInternalConsts3,
  ModuleFin, Module, UMySQLModule, DmkDAC_PF, ModuleGeral;

{ TUnPagtos }

{ 2021-04-28
procedure TUnPagtos.Pagto(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
  Terceiro, FatID, Genero, GenCtb: Integer; SQLType: TSQLType; Titulo:
  String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
  UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig:
  Boolean; Carteira, Parcelas, Periodic, DiasParc: Integer;
  NotaFiscal: Integer; TabLctA: String; Descricao: String = '';
  SerieNF: String = ''; Qtde: Double = 0; Qtd2: Double = 0);
var
  Cursor : TCursor;
  Parcela: Integer;
begin
  Parcela := 0;
  if (SQLType = stUpd) and (QrEmiss <> nil) then
  begin
    if QrEmiss.RecordCount = 0 then
    begin
      Geral.MensagemBox('N�o h� dados para editar!', 'Informa��o',
      MB_OK+MB_ICONINFORMATION);
      Exit;
    end;
    if (QrEmiss.FieldByName('Tipo').AsInteger = 2)
    and (QrEmiss.FieldByName('Sit').AsInteger in ([2,3])) then
    begin
      Geral.MensagemBox('Lan�amentos do tipo "emiss�o" n�o podem ' +
      'ser modificados ap�s a sua baixa/quita��o/pagamento. Para modific�-lo '+
      'desfa�a a baixa/quita��o/pagamento!', 'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;
  end;
  VAR_FATIDTXT := FatID;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Screen.ActiveForm.Refresh;
  if DBCheck.CriaFm(TFmMyPagtos, FmMyPagtos, afmoNegarComAviso) then
  begin
    try
      with FmMyPagtos do
      begin
        FQrLct := QrEmiss;
        FTabLctA := TabLctA;
        //
(*      ini 2021-02-02
        if Uppercase(Application.Title) = 'LESEW' then
        begin
          QrGeneros.SQL.Clear;
          QrGeneros.SQL.Add('SELECT DISTINCT con.Codigo, con.Nome');
          QrGeneros.SQL.Add('FROM contas con');
          QrGeneros.SQL.Add('LEFT JOIN contasu cau ON cau.Codigo = con.Codigo ');
          QrGeneros.SQL.Add('WHERE (con.Codigo > 0 OR con.Codigo < -99)');
          if VAR_USUARIO > 0 then
            QrGeneros.SQL.Add('AND cau.Usuario =' + Geral.FF0(VAR_USUARIO));
        end else
        begin
          QrGeneros.Close;
          QrGeneros.SQL.Clear;
          QrGeneros.SQL.Add('SELECT con.Codigo, con.Nome');
          QrGeneros.SQL.Add('FROM contas con');
          QrGeneros.SQL.Add('WHERE (con.Codigo > 0 OR con.Codigo < -99)');
        end;
        fim 2021-02-02
*)
        if tpPagto = tpCred then
        begin
          LaValor.Caption := CO_CREDITO;
          LaDevedor.Enabled := True;
          EdDevedor.Enabled := True;
          CBDevedor.Enabled := True;
          EdDevedor.Text := Geral.FF0(Terceiro);
          CBDevedor.KeyValue := Terceiro;
          //QrGeneros.SQL.Add('AND Credito="V"');
          FSQL_Extra1 := FSQL_Extra1 + ' AND Credito="V"';
          //FSQL_Extra2 := FSQLExtra2 + ' AND Debito="V"';
        end else begin
          LaValor.Caption := CO_DEBITO;
          LaCredor.Enabled := True;
          EdCredor.Enabled := True;
          CBCredor.Enabled := True;
          EdCredor.Text := Geral.FF0(Terceiro);
          CBCredor.KeyValue := Terceiro;
          //QrGeneros.SQL.Add('AND Debito="V"');
          FSQL_Extra1 := FSQL_Extra1 + ' AND Debito="V"';
          //FSQL_Extra2 := FSQL_Extra2 + ' AND Credito="V"';
        end;
        //QrGeneros.SQL.Add('ORDER BY Nome');
        //UnDmkDAC_PF.AbreQuery(QrGeneros, DMod.MyDB);
        ReopenContas1e2();
        //fim 2021-02-02
        //
        EdSerieNF.Text    := IC3_ED_SerieNF;
        EdNotaFiscal.Text := Geral.FF0(IC3_ED_NF);
        //
        if (SQLType = stUpd) and (QrEmiss <> nil) then
        begin
          EdCodigo.Enabled    := False;
          //RGTipo.ItemIndex    := QrEmiss.FieldByName('Tipo').AsInteger;
          EdCarteira.Text     := Geral.FF0(QrEmiss.FieldByName('Carteira').AsInteger);
          CBCarteira.KeyVAlue := QrEmiss.FieldByName('Carteira').AsInteger;
          EdDocumento.Text    := FloatToStr(QrEmiss.FieldByName('Documento').AsFloat);
          TPVencimento.Date   := QrEmiss.FieldByName('Vencimento').AsDateTime;
          //
          EdBanco.Text              := FormatFloat('000', QrEmiss.FieldByName('Banco').AsInteger);
          EdNome.Text               := QrEmiss.FieldByName('Emitente').AsString;
          EdAgencia.Text            := QrEmiss.FieldByName('Agencia').AsString;
          EdConta.Text              := QrEmiss.FieldByName('ContaCorrente').AsString;
          EdCNPJCPF.Text            := Geral.FormataCNPJ_TT(QrEmiss.FieldByName('CNPJCPF').AsString);
          EdGenero.ValueVariant     := Geral.FF0(QrEmiss.FieldByName('Genero').AsInteger);
          CBGenero.KeyValue         := QrEmiss.FieldByName('Genero').AsInteger;
          try
            EdGenCtb.ValueVariant     := Geral.FF0(QrEmiss.FieldByName('GenCtb').AsInteger);
            CBGenCtb.KeyValue         := QrEmiss.FieldByName('GenCtb').AsInteger;
          except
            // nada!
          end;
          EdSerieNF.ValueVariant    := QrEmiss.FieldByName('SerieNF').AsString;
          EdNotaFiscal.ValueVariant := QrEmiss.FieldByName('NotaFiscal').AsInteger;
        end else begin
          if IC3_ED_Vencto >= IC3_ED_Data then
            TPVencimento.Date   := IC3_ED_Vencto
          else
            TPVencimento.Date   := IC3_ED_Data;

          EdGenero.ValueVariant     := Genero;
          CBGenero.KeyValue         := Genero;
          EdGenCtb.ValueVariant     := GenCtb;
          CBGenCtb.KeyValue         := GenCtb;
          EdSerieNF.ValueVariant    := SerieNF;
          EdNotaFiscal.ValueVariant := NotaFiscal;

          if Reconfig then
          begin
            EdCarteira.Text        := Geral.FF0(Carteira);
            CBCarteira.KeyValue    := Carteira;
            CkParcelamento.Checked := Parcelas > 0;
            EdParcelas.Text        := Geral.FF0(Parcelas);
            RGPeriodo.ItemIndex    := Periodic;
            EdDias.Text            := Geral.FF0(DiasParc);
          end;
          / desmarcar
          {}
          {
          $IfDef DEF_MYPAGTOSCONFIG
          begin
            if Dmod.QrControle.FieldByName('VendaParcPg').AsInteger > 1 then
            begin
              CkParcelamento.Checked := True;
              EdParcelas.Text        := Geral.FF0(Dmod.QrControle.FieldByName('VendaParcPg').AsInteger);
              RGPeriodo.ItemIndex    := Dmod.QrControle.FieldByName('VendaPeriPg').AsInteger;
              EdDias.Text            := Geral.FF0(Dmod.QrControle.FieldByName('VendaDiasPg').AsInteger);
              //RGTipo.ItemIndex       := GOTOy.VerificaTipoDaCarteira(
                                        //Dmod.QrControle.FieldByName('VendaCartPg').AsInteger);
              EdCarteira.Text        := Geral.FF0(Dmod.QrControle.FieldByName('VendaCartPg').AsInteger);
              CBCarteira.KeyValue    := Dmod.QrControle.FieldByName('VendaCartPg').AsInteger;
              FmMyPagtos.CalculaParcelas;
            end;
            CkParcelamento.Checked := Dmod.QrControle.FieldByName('MyPgParc').AsInteger > 0;
            EdParcelas.Text := Geral.FF0(Dmod.QrControle.FieldByName('MyPgQtdP').AsInteger);
            RGPeriodo.ItemIndex := Dmod.QrControle.FieldByName('MyPgPeri').AsInteger;
            EdDias.Text := Geral.FF0(Dmod.QrControle.FieldByName('MyPgDias').AsInteger);
            CalculaParcelas;
          end;
          $EndIf
        end;
        FFatID_Sub := FatID_Sub;
        FUsaMinMax := UsaMinMax;
        FValMin    := ValMin;
        FValMax    := ValMax;
        FDescricao := Descricao;
        //
        FUsuario := Usuario;
        ImgTipo.SQLType := SQLType;
        FDefDuplicata := Titulo;
        if QrEmiss <> nil then
        begin
          if (SQLType = stIns) then
            Parcela := QrEmiss.FieldByName('FatParcela').AsInteger + 1
          else Parcela := QrEmiss.FieldByName('FatParcela').AsInteger;
          FMaxCod := QrEmiss.RecordCount + 1;
        end else begin
          Parcela := 1;
          FMaxCod := 1;
        end;
        EdCodigo.Text         := Geral.FF0(Parcela);
        EdCodigo.Enabled      := True;
        EdCliInt.ValueVariant := CliInt;
        CBCliInt.KeyValue     := CliInt;
        //
        if Valor < 0 then Valor := Valor * -1;
        EdValor.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
        //
        EdQtde.ValueVariant := Qtde;
        EdQtd2.ValueVariant := Qtd2;
      end;
    finally
      Screen.Cursor := Cursor;
    end;
    if Show then FmMyPagtos.ShowModal else FmMyPagtos.BtConfirmaClick(Self);
    FmMyPagtos.Destroy;
  end;
  if QrEmiss <> nil then
  begin
    QrEmiss.Close;
    if QrEmiss.Params.Count > 0 then
      QrEmiss.Params[0].AsInteger := LocCod;
    UnDmkDAC_PF.AbreQuery(QrEmiss, DMod.MyDB);
    QrEmiss.Locate('FatParcela', Parcela, []);
  end;
end;
}

procedure TUnPagtos.Pagto(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
  Terceiro, FatID, Genero, GenCtb: Integer; SQLType: TSQLType; Titulo: String;
  Valor: Double; Usuario, FatID_Sub, CliInt: Integer; UsaMinMax: TMinMax;
  ValMin, ValMax: Double; Show, Reconfig: Boolean; Carteira, Parcelas, Periodic,
  DiasParc, NotaFiscal: Integer; TabLctA, Descricao, SerieNF: String; Qtde,
  Qtd2: Double; GenCtbD: Integer; GenCtbC: Integer; CentroCusto: Integer;
  FatParcRef_PgtoAdiantado: Integer);
begin
  if DModG.QrCtrlGeralUsarFinCtb.Value = 0 then
  begin
    Pagto1(QrEmiss, tpPagto, LocCod, Terceiro, FatID, Genero, GenCtb, SQLType,
    Titulo, Valor, Usuario, FatID_Sub, CliInt, UsaMinMax, ValMin, ValMax, Show,
    Reconfig, Carteira, Parcelas, Periodic, DiasParc, NotaFiscal, TabLctA,
    Descricao, SerieNF, Qtde, Qtd2);
  end else
  begin
    Pagto2(QrEmiss, tpPagto, LocCod, Terceiro, FatID, Genero, GenCtb, SQLType,
    Titulo, Valor, Usuario, FatID_Sub, CliInt, UsaMinMax, ValMin, ValMax, Show,
    Reconfig, Carteira, Parcelas, Periodic, DiasParc, NotaFiscal, TabLctA,
    Descricao, SerieNF, Qtde, Qtd2, GenCtbD, GenCtbC, CentroCusto,
    FatParcRef_PgtoAdiantado);
  end;
end;

procedure TUnPagtos.Pagto1(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
  Terceiro, FatID, Genero, GenCtb: Integer; SQLType: TSQLType; Titulo:
  String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
  UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig:
  Boolean; Carteira, Parcelas, Periodic, DiasParc: Integer;
  NotaFiscal: Integer; TabLctA: String; Descricao: String = '';
  SerieNF: String = ''; Qtde: Double = 0; Qtd2: Double = 0);
begin
  if PreparaPagto1(QrEmiss, tpPagto, LocCod, Terceiro, FatID, Genero, GenCtb,
  SQLType, Titulo, Valor, Usuario, FatID_Sub, CliInt, UsaMinMax, ValMin, ValMax,
  Show, Reconfig, Carteira, Parcelas, Periodic, DiasParc, NotaFiscal, TabLctA,
  Descricao, SerieNF, Qtde, Qtd2) then
  begin
    if Show then FmMyPagtos.ShowModal else FmMyPagtos.BtConfirmaClick(Self);
  end;
  FmMyPagtos.Destroy;
  //
  if QrEmiss <> nil then
  begin
    QrEmiss.Close;
    if QrEmiss.Params.Count > 0 then
      QrEmiss.Params[0].AsInteger := LocCod;
    UnDmkDAC_PF.AbreQuery(QrEmiss, DMod.MyDB);
    //QrEmiss.Locate('FatParcela', Parcela, []);
  end;
end;

procedure TUnPagtos.Pagto2(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
  Terceiro, FatID, Genero, GenCtb: Integer; SQLType: TSQLType; Titulo:
  String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
  UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig:
  Boolean; Carteira, Parcelas, Periodic, DiasParc: Integer;
  NotaFiscal: Integer; TabLctA: String; Descricao: String = '';
  SerieNF: String = ''; Qtde: Double = 0; Qtd2: Double = 0;
  GenCtbD: Integer = 0; GenCtbC: Integer = 0; CentroCusto: Integer = 0;
  FatParcRef_PgtoAdiantado: Integer = 0);
const
  sProcName = 'TUnPagtos.Pagto2()';
begin
  if tpPagto in ([TTipoPagto.tpDeb, TTipoPagto.tpCred]) then
  begin
    if PreparaPagto2(QrEmiss, tpPagto, LocCod, Terceiro, FatID, Genero, GenCtb,
    SQLType, Titulo, Valor, Usuario, FatID_Sub, CliInt, UsaMinMax, ValMin, ValMax,
    Show, Reconfig, Carteira, Parcelas, Periodic, DiasParc, NotaFiscal, TabLctA,
    Descricao, SerieNF, Qtde, Qtd2, GenCtbD, GenCtbC, CentroCusto,
    FatParcRef_PgtoAdiantado) then
    begin
      if Show then FmMyPagtos2.ShowModal else FmMyPagtos2.BtConfirmaClick(Self);
    end;
    FmMyPagtos2.Destroy;
    //
    if QrEmiss <> nil then
    begin
      QrEmiss.Close;
      if QrEmiss.Params.Count > 0 then
        QrEmiss.Params[0].AsInteger := LocCod;
      UnDmkDAC_PF.AbreQuery(QrEmiss, DMod.MyDB);
      //QrEmiss.Locate('FatParcela', Parcela, []);
    end;
  end else
    Geral.MB_Aviso('Tipo de lan�amento n�o implementado em ' + sProcName);
end;

procedure TUnPagtos.Pagto_Arr1(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto; LocCod,
  Terceiro, FatID, Genero, GenCtb: Integer; SQLType: TSQLType; Titulo: String;
  Valor: Double; Usuario, FatID_Sub, CliInt: Integer; UsaMinMax: TMinMax;
  ValMin, ValMax: Double; Show, Reconfig: Boolean; Carteira, Parcelas, Periodic,
  DiasParc, NotaFiscal: Integer; TabLctA: String;

  Arr_dVenc: TArr_dVenc; Arr_vDup: TArr_vDup; Duplicata: String;

  Descricao, SerieNF: String; Qtde,
  Qtd2: Double);
var
  I: Integer;
begin
  if PreparaPagto1(QrEmiss, tpPagto, LocCod, Terceiro, FatID, Genero, GenCtb,
  SQLType, Titulo, Valor, Usuario, FatID_Sub, CliInt, UsaMinMax, ValMin, ValMax,
  Show, Reconfig, Carteira, Parcelas, Periodic, DiasParc, NotaFiscal, TabLctA,
  Descricao, SerieNF, Qtde, Qtd2) then
  begin
    //
    FmMyPagtos.EdDuplicata.Text := Duplicata;
    FmMyPagtos.GBCheque.Visible := False;
    //
    if Length(Arr_dVenc) > 0 then
    begin
      // Crair Itens qtd de parcelas corretas
      FmMyPagtos.CkParcelamento.Checked := False;
      FmMyPagtos.EdDias.ValueVariant := 1;
      FmMyPagtos.RGPeriodo.ItemIndex := 1;
      FmMyPagtos.CkParcelamento.Checked := True;
      //
      FmMyPagtos.RGPeriodo.ItemIndex := -1;
      FmMyPagtos.EdDias.ValueVariant := -1;
      FmMyPagtos.CkParcelamento.Checked := True;
      //
      FmMyPagtos.TbParcPagtos.First;
      for I := Low(Arr_dVenc) to High(Arr_dVenc) do
      begin
        FmMyPagtos.TbParcPagtos.Edit;
        FmMyPagtos.TbParcpagtosData.Value := Geral.ValidaData_YYYY_MM_DD(Arr_dVenc[I]);
        if tpPagto = TTipoPagto.tpCred then
          FmMyPagtos.TbParcpagtosCredito.Value := Arr_vDup[I]
        else
          FmMyPagtos.TbParcpagtosDebito.Value := Arr_vDup[I];
        FmMyPagtos.TbParcPagtos.Post;
        //
        FmMyPagtos.TbParcPagtos.Next;
      end;
    end;
    //
    if Show then FmMyPagtos.ShowModal else FmMyPagtos.BtConfirmaClick(Self);
  end;
  FmMyPagtos.Destroy;
  if QrEmiss <> nil then
  begin
    QrEmiss.Close;
    if QrEmiss.Params.Count > 0 then
      QrEmiss.Params[0].AsInteger := LocCod;
    UnDmkDAC_PF.AbreQuery(QrEmiss, DMod.MyDB);
    //QrEmiss.Locate('FatParcela', Parcela, []);
  end;
end;

procedure TUnPagtos.PgFatID1(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto;
  LocCod, Terceiro, FatID, UpdGener: Integer; SQLType: TSQLType; Titulo: String;
  Valor: Double; Usuario, FatID_Sub, CliInt: Integer; UsaMinMax: TMinMax;
  ValMin, ValMax: Double; Show, Reconfig: Boolean; Carteira, Parcelas, Periodic,
  DiasParc: Integer; TabLctA: String);
var
  Genero, GenCtb: Integer;
begin
  if MyObjects.FIC(FatID = 0, nil,
  'ERRO! "FatID" n�o definido para inclus�o de lan�amento!') then
    Exit;
  if SQLType = stIns then
  begin
    if not DModFin.ObtemGeneroDeFatID(FatID, Genero, tpPagto, True) then
      Exit;
    if not DModFin.ObtemGenCtbDeFatID(FatID, GenCtb, tpPagto, True) then
      Exit;
    //
  end else Genero := UpdGener;
  //
  Pagto1(QrEmiss, tpPagto, LocCod, Terceiro, FatID, Genero, GenCtb, SQLType,
    Titulo, Valor, Usuario, FatID_Sub, CliInt, UsaMinMax, ValMin, ValMax, Show,
    Reconfig, Carteira, Parcelas, Periodic, DiasParc, 0, TabLctA);
end;

procedure TUnPagtos.PgFatID_XML(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto;
  LocCod, Terceiro, FatID, UpdGener: Integer; SQLType: TSQLType; Titulo: String;
  Valor: Double; Usuario, FatID_Sub, CliInt: Integer; UsaMinMax: TMinMax;
  ValMin, ValMax: Double; Show, Reconfig: Boolean; Carteira, Parcelas, Periodic,
  DiasParc: Integer; TabLctA: String; Arr_dVenc: TArr_dVenc;
  Arr_vDup: TArr_vDup; Duplicata, Descricao, SerieNF: String; Qtde,
  Qtd2: Double; MeAvisos: TMemo);
var
  Msg: String;
begin
  Msg := 'Fazer!!! TUnPagtos.PgFatID_XML()';
  if MeAvisos <> nil then
    MeAvisos.Text :=    '****************************************' + sLineBreak +
    Msg +  sLineBreak + '****************************************' + sLineBreak +
    MeAvisos.Text
  else
    Geral.MB_Info(Msg);
end;

procedure TUnPagtos.PgFatID_XML1(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto;
  LocCod, Terceiro, FatID, UpdGener: Integer; SQLType: TSQLType; Titulo: String;
  Valor: Double; Usuario, FatID_Sub, CliInt: Integer; UsaMinMax: TMinMax;
  ValMin, ValMax: Double; Show, Reconfig: Boolean; Carteira, Parcelas, Periodic,
  DiasParc: Integer; TabLctA: String; Arr_dVenc: TArr_dVenc;
  Arr_vDup: TArr_vDup; Duplicata: String; Descricao: String = '';
  SerieNF: String = ''; Qtde: Double = 0; Qtd2: Double = 0);
var
  Genero, GenCtb: Integer;
begin
{
  if MyObjects.FIC(FatID = 0, nil,
  'ERRO! "FatID" n�o definido para inclus�o de lan�amento!') then
    Exit;
  if SQLType = stIns then
  begin
    if not DModFin.ObtemGeneroDeFatID(FatID, Genero, tpPagto, True) then
      Exit;
    if not DModFin.ObtemGenCtbDeFatID(FatID, GenCtb, tpPagto, True) then
      Exit;
    //
  end else Genero := UpdGener;
  //
}
  Genero := UpdGener;
  Pagto_Arr1(QrEmiss, tpPagto, LocCod, Terceiro, FatID, Genero, UpdGener, SQLType,
    Titulo, Valor, Usuario, FatID_Sub, CliInt, UsaMinMax, ValMin, ValMax, Show,
    Reconfig, Carteira, Parcelas, Periodic, DiasParc, 0, TabLctA,
    Arr_dVenc, Arr_vDup, Duplicata, Descricao, SerieNF, Qtde, Qtd2);
end;

function TUnPagtos.PreparaPagto1(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto;
  LocCod, Terceiro, FatID, Genero, GenCtb: Integer; SQLType: TSQLType;
  Titulo: String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
  UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig: Boolean; Carteira,
  Parcelas, Periodic, DiasParc, NotaFiscal: Integer; TabLctA, Descricao,
  SerieNF: String; Qtde, Qtd2: Double): Boolean;
var
  //Cursor : TCursor;
  Parcela: Integer;
begin
  Result := False;
  //
  Parcela := 0;
  if (SQLType = stUpd) and (QrEmiss <> nil) then
  begin
    if QrEmiss.RecordCount = 0 then
    begin
      Geral.MensagemBox('N�o h� dados para editar!', 'Informa��o',
      MB_OK+MB_ICONINFORMATION);
      Exit;
    end;
    if (QrEmiss.FieldByName('Tipo').AsInteger = 2)
    and (QrEmiss.FieldByName('Sit').AsInteger in ([2,3])) then
    begin
      Geral.MensagemBox('Lan�amentos do tipo "emiss�o" n�o podem ' +
      'ser modificados ap�s a sua baixa/quita��o/pagamento. Para modific�-lo '+
      'desfa�a a baixa/quita��o/pagamento!', 'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;
  end;
  VAR_FATIDTXT := FatID;
(*  Erro na segunda cria��o aqui!
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Screen.ActiveForm.Refresh;
*)
  if Assigned(FmMyPagtos) then
  begin
    //FmMyPagtos.DisposeOf;
  end;

  if DBCheck.CriaFm(TFmMyPagtos, FmMyPagtos, afmoNegarComAviso) then
  begin
    try
      with FmMyPagtos do
      begin
        FQrLct := QrEmiss;
        FTabLctA := TabLctA;
        //
(*      ini 2021-02-02
        if Uppercase(Application.Title) = 'LESEW' then
        begin
          QrGeneros.SQL.Clear;
          QrGeneros.SQL.Add('SELECT DISTINCT con.Codigo, con.Nome');
          QrGeneros.SQL.Add('FROM contas con');
          QrGeneros.SQL.Add('LEFT JOIN contasu cau ON cau.Codigo = con.Codigo ');
          QrGeneros.SQL.Add('WHERE (con.Codigo > 0 OR con.Codigo < -99)');
          if VAR_USUARIO > 0 then
            QrGeneros.SQL.Add('AND cau.Usuario =' + Geral.FF0(VAR_USUARIO));
        end else
        begin
          QrGeneros.Close;
          QrGeneros.SQL.Clear;
          QrGeneros.SQL.Add('SELECT con.Codigo, con.Nome');
          QrGeneros.SQL.Add('FROM contas con');
          QrGeneros.SQL.Add('WHERE (con.Codigo > 0 OR con.Codigo < -99)');
        end;
        fim 2021-02-02
*)
        if tpPagto = tpCred then
        begin
          LaValor.Caption := CO_CREDITO;
          LaDevedor.Enabled := True;
          EdDevedor.Enabled := True;
          CBDevedor.Enabled := True;
          EdDevedor.Text := Geral.FF0(Terceiro);
          CBDevedor.KeyValue := Terceiro;
          //QrGeneros.SQL.Add('AND Credito="V"');
          // ini 2022-03-16
          // FSQL_Extra1 := FSQL_Extra1 + ' AND Credito="V"';
          // fim 2022-03-16
          //FSQL_Extra2 := FSQLExtra2 + ' AND Debito="V"';
        end else begin
          LaValor.Caption := CO_DEBITO;
          LaCredor.Enabled := True;
          EdCredor.Enabled := True;
          CBCredor.Enabled := True;
          EdCredor.Text := Geral.FF0(Terceiro);
          CBCredor.KeyValue := Terceiro;
          //QrGeneros.SQL.Add('AND Debito="V"');
          // ini 2022-03-16
          //FSQL_Extra1 := FSQL_Extra1 + ' AND Debito="V"';
          // ini 2022-03-16
          //FSQL_Extra2 := FSQL_Extra2 + ' AND Credito="V"';
        end;
        //QrGeneros.SQL.Add('ORDER BY Nome');
        //UnDmkDAC_PF.AbreQuery(QrGeneros, DMod.MyDB);
        ReopenContas1e2();
        //fim 2021-02-02
        //
        EdSerieNF.Text    := IC3_ED_SerieNF;
        EdNotaFiscal.Text := Geral.FF0(IC3_ED_NF);
        //
        if (SQLType = stUpd) and (QrEmiss <> nil) then
        begin
          EdCodigo.Enabled    := False;
          //RGTipo.ItemIndex    := QrEmiss.FieldByName('Tipo').AsInteger;
          EdCarteira.Text     := Geral.FF0(QrEmiss.FieldByName('Carteira').AsInteger);
          CBCarteira.KeyVAlue := QrEmiss.FieldByName('Carteira').AsInteger;
          EdDocumento.Text    := FloatToStr(QrEmiss.FieldByName('Documento').AsFloat);
          TPVencimento.Date   := QrEmiss.FieldByName('Vencimento').AsDateTime;
          //
          EdBanco.Text              := FormatFloat('000', QrEmiss.FieldByName('Banco').AsInteger);
          EdNome.Text               := QrEmiss.FieldByName('Emitente').AsString;
          EdAgencia.Text            := QrEmiss.FieldByName('Agencia').AsString;
          EdConta.Text              := QrEmiss.FieldByName('ContaCorrente').AsString;
          EdCNPJCPF.Text            := Geral.FormataCNPJ_TT(QrEmiss.FieldByName('CNPJCPF').AsString);
          EdGenero.ValueVariant     := Geral.FF0(QrEmiss.FieldByName('Genero').AsInteger);
          CBGenero.KeyValue         := QrEmiss.FieldByName('Genero').AsInteger;
          try
            EdGenCtb.ValueVariant     := Geral.FF0(QrEmiss.FieldByName('GenCtb').AsInteger);
            CBGenCtb.KeyValue         := QrEmiss.FieldByName('GenCtb').AsInteger;
          except
            // nada!
            //Geral.MB_Teste(QrEmiss.SQL.Text);
          end;
          EdSerieNF.ValueVariant    := QrEmiss.FieldByName('SerieNF').AsString;
          EdNotaFiscal.ValueVariant := QrEmiss.FieldByName('NotaFiscal').AsInteger;
        end else begin
          if IC3_ED_Vencto >= IC3_ED_Data then
            TPVencimento.Date   := IC3_ED_Vencto
          else
            TPVencimento.Date   := IC3_ED_Data;

          EdGenero.ValueVariant     := Genero;
          CBGenero.KeyValue         := Genero;
          EdGenCtb.ValueVariant     := GenCtb;
          CBGenCtb.KeyValue         := GenCtb;
          EdSerieNF.ValueVariant    := SerieNF;
          EdNotaFiscal.ValueVariant := NotaFiscal;

          if Reconfig then
          begin
            EdCarteira.Text        := Geral.FF0(Carteira);
            CBCarteira.KeyValue    := Carteira;
            CkParcelamento.Checked := Parcelas > 0;
            EdParcelas.Text        := Geral.FF0(Parcelas);
            RGPeriodo.ItemIndex    := Periodic;
            EdDias.Text            := Geral.FF0(DiasParc);
          end;
          {$IfDef DEF_MYPAGTOSCONFIG}
          begin
            if Dmod.QrControle.FieldByName('VendaParcPg').AsInteger > 1 then
            begin
              CkParcelamento.Checked := True;
              EdParcelas.Text        := Geral.FF0(Dmod.QrControle.FieldByName('VendaParcPg').AsInteger);
              RGPeriodo.ItemIndex    := Dmod.QrControle.FieldByName('VendaPeriPg').AsInteger;
              EdDias.Text            := Geral.FF0(Dmod.QrControle.FieldByName('VendaDiasPg').AsInteger);
              //RGTipo.ItemIndex       := GOTOy.VerificaTipoDaCarteira(
                                        //Dmod.QrControle.FieldByName('VendaCartPg').AsInteger);
              EdCarteira.Text        := Geral.FF0(Dmod.QrControle.FieldByName('VendaCartPg').AsInteger);
              CBCarteira.KeyValue    := Dmod.QrControle.FieldByName('VendaCartPg').AsInteger;
              FmMyPagtos.CalculaParcelas;
            end;
            CkParcelamento.Checked := Dmod.QrControle.FieldByName('MyPgParc').AsInteger > 0;
            EdParcelas.Text := Geral.FF0(Dmod.QrControle.FieldByName('MyPgQtdP').AsInteger);
            RGPeriodo.ItemIndex := Dmod.QrControle.FieldByName('MyPgPeri').AsInteger;
            EdDias.Text := Geral.FF0(Dmod.QrControle.FieldByName('MyPgDias').AsInteger);
            CalculaParcelas;
          end;
          {$EndIf}
        end;
        FFatID_Sub := FatID_Sub;
        FUsaMinMax := UsaMinMax;
        FValMin    := ValMin;
        FValMax    := ValMax;
        FDescricao := Descricao;
        //
        FUsuario := Usuario;
        ImgTipo.SQLType := SQLType;
        FDefDuplicata := Titulo;
        if QrEmiss <> nil then
        begin
          if (SQLType = stIns) then
            Parcela := QrEmiss.FieldByName('FatParcela').AsInteger + 1
          else Parcela := QrEmiss.FieldByName('FatParcela').AsInteger;
          FMaxCod := QrEmiss.RecordCount + 1;
        end else begin
          Parcela := 1;
          FMaxCod := 1;
        end;
        EdCodigo.Text         := Geral.FF0(Parcela);
        EdCodigo.Enabled      := True;
        EdCliInt.ValueVariant := CliInt;
        CBCliInt.KeyValue     := CliInt;
        //
        if Valor < 0 then Valor := Valor * -1;
        EdValor.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
        //
        EdQtde.ValueVariant := Qtde;
        EdQtd2.ValueVariant := Qtd2;
        //
        Result := True;
      end;
    finally
      //Screen.Cursor := Cursor;
    end;
  end;
end;

function TUnPagtos.PreparaPagto2(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto;
  LocCod, Terceiro, FatID, Genero, GenCtb: Integer; SQLType: TSQLType;
  Titulo: String; Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
  UsaMinMax: TMinMax; ValMin, ValMax: Double; Show, Reconfig: Boolean; Carteira,
  Parcelas, Periodic, DiasParc, NotaFiscal: Integer; TabLctA, Descricao,
  SerieNF: String; Qtde, Qtd2: Double; GenCtbD, GenCtbC: Integer; CentroCusto:
  Integer; FatParcRef_PgtoAdiantado: Integer): Boolean;
var
  //Cursor : TCursor;
  Parcela: Integer;
begin
  Result := False;
  //
  Parcela := 0;
  if (SQLType = stUpd) and (QrEmiss <> nil) then
  begin
    if QrEmiss.RecordCount = 0 then
    begin
      Geral.MensagemBox('N�o h� dados para editar!', 'Informa��o',
      MB_OK+MB_ICONINFORMATION);
      Exit;
    end;
    if (QrEmiss.FieldByName('Tipo').AsInteger = 2)
    and (QrEmiss.FieldByName('Sit').AsInteger in ([2,3])) then
    begin
      Geral.MensagemBox('Lan�amentos do tipo "emiss�o" n�o podem ' +
      'ser modificados ap�s a sua baixa/quita��o/pagamento. Para modific�-lo '+
      'desfa�a a baixa/quita��o/pagamento!', 'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;
  end;
  VAR_FATIDTXT := FatID;
(*  Erro na segunda cria��o aqui!
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Screen.ActiveForm.Refresh;
*)
  if Assigned(FmMyPagtos2) then
  begin
    //FmMyPagtos2.DisposeOf;
  end;

  if DBCheck.CriaFm(TFmMyPagtos2, FmMyPagtos2, afmoNegarComAviso) then
  begin
    try
      with FmMyPagtos2 do
      begin
        ImgTipo.SQLType := SQLType;
        FtpPagto := tpPagto;
        FFatParcRef_PgtoAdiantado := FatParcRef_PgtoAdiantado;
        ReopenCentroCusto();
        //
        FQrLct := QrEmiss;
        FTabLctA := TabLctA;
        //
(*      ini 2021-02-02
        if Uppercase(Application.Title) = 'LESEW' then
        begin
          QrGeneros.SQL.Clear;
          QrGeneros.SQL.Add('SELECT DISTINCT con.Codigo, con.Nome');
          QrGeneros.SQL.Add('FROM contas con');
          QrGeneros.SQL.Add('LEFT JOIN contasu cau ON cau.Codigo = con.Codigo ');
          QrGeneros.SQL.Add('WHERE (con.Codigo > 0 OR con.Codigo < -99)');
          if VAR_USUARIO > 0 then
            QrGeneros.SQL.Add('AND cau.Usuario =' + Geral.FF0(VAR_USUARIO));
        end else
        begin
          QrGeneros.Close;
          QrGeneros.SQL.Clear;
          QrGeneros.SQL.Add('SELECT con.Codigo, con.Nome');
          QrGeneros.SQL.Add('FROM contas con');
          QrGeneros.SQL.Add('WHERE (con.Codigo > 0 OR con.Codigo < -99)');
        end;
        fim 2021-02-02
*)
        if tpPagto = tpCred then
        begin
          LaValor.Caption := CO_CREDITO;
          LaDevedor.Enabled := True;
          EdDevedor.Enabled := True;
          CBDevedor.Enabled := True;
          EdDevedor.Text := Geral.FF0(Terceiro);
          CBDevedor.KeyValue := Terceiro;
          //QrGeneros.SQL.Add('AND Credito="V"');
          // ini 2022-03-16
          // FSQL_Extra1 := FSQL_Extra1 + ' AND Credito="V"';
          // fim 2022-03-16
          //FSQL_Extra2 := FSQLExtra2 + ' AND Debito="V"';
        end else begin
          LaValor.Caption := CO_DEBITO;
          LaCredor.Enabled := True;
          EdCredor.Enabled := True;
          CBCredor.Enabled := True;
          EdCredor.Text := Geral.FF0(Terceiro);
          CBCredor.KeyValue := Terceiro;
          //QrGeneros.SQL.Add('AND Debito="V"');
          // ini 2022-03-16
          //FSQL_Extra1 := FSQL_Extra1 + ' AND Debito="V"';
          // ini 2022-03-16
          //FSQL_Extra2 := FSQL_Extra2 + ' AND Credito="V"';
        end;
        //QrGeneros.SQL.Add('ORDER BY Nome');
        //UnDmkDAC_PF.AbreQuery(QrGeneros, DMod.MyDB);
        ReopenContas1e2();
        //fim 2021-02-02
        //
        EdSerieNF.Text    := IC3_ED_SerieNF;
        EdNotaFiscal.Text := Geral.FF0(IC3_ED_NF);
        //
        if (SQLType = stUpd) and (QrEmiss <> nil) then
        begin
          EdCodigo.Enabled    := False;
          //RGTipo.ItemIndex    := QrEmiss.FieldByName('Tipo').AsInteger;
          EdCarteira.Text     := Geral.FF0(QrEmiss.FieldByName('Carteira').AsInteger);
          CBCarteira.KeyVAlue := QrEmiss.FieldByName('Carteira').AsInteger;
          EdDocumento.Text    := FloatToStr(QrEmiss.FieldByName('Documento').AsFloat);
          TPVencimento.Date   := QrEmiss.FieldByName('Vencimento').AsDateTime;
          //
          EdBanco.Text              := FormatFloat('000', QrEmiss.FieldByName('Banco').AsInteger);
          EdNome.Text               := QrEmiss.FieldByName('Emitente').AsString;
          EdAgencia.Text            := QrEmiss.FieldByName('Agencia').AsString;
          EdConta.Text              := QrEmiss.FieldByName('ContaCorrente').AsString;
          EdCNPJCPF.Text            := Geral.FormataCNPJ_TT(QrEmiss.FieldByName('CNPJCPF').AsString);
          // ini 2022-03-23
          EdGenCtbD.ValueVariant     := Geral.FF0(QrEmiss.FieldByName('GenCtbD').AsInteger);
          CBGenCtbD.KeyValue         := QrEmiss.FieldByName('GenCtbD').AsInteger;
          try
            EdGenCtbC.ValueVariant     := Geral.FF0(QrEmiss.FieldByName('GenCtbC').AsInteger);
            CBGenCtbC.KeyValue         := QrEmiss.FieldByName('GenCtbC').AsInteger;
          except
            // nada!
            //Geral.MB_Teste(QrEmiss.SQL.Text);
          end;
          // ini 2022-03-2
          EdSerieNF.ValueVariant    := QrEmiss.FieldByName('SerieNF').AsString;
          EdNotaFiscal.ValueVariant := QrEmiss.FieldByName('NotaFiscal').AsInteger;
          EdCentroCusto.ValueVariant := QrEmiss.FieldByName('CentroCusto').AsInteger;
          CBCentroCusto.KeyValue := QrEmiss.FieldByName('CentroCusto').AsInteger;

        end else begin
          if IC3_ED_Vencto >= IC3_ED_Data then
            TPVencimento.Date   := IC3_ED_Vencto
          else
            TPVencimento.Date   := IC3_ED_Data;
          if GenCtbD <> 0 then
          begin
            EdGenCtbD.ValueVariant     := GenCtbD;
            CBGenCtbD.KeyValue         := GenCtbD;
          end;
          if GenCtbC <> 0 then
          begin
            EdGenCtbC.ValueVariant     := GenCtbC;
            CBGenCtbC.KeyValue         := GenCtbC;
          end;
          EdSerieNF.ValueVariant    := SerieNF;
          EdNotaFiscal.ValueVariant := NotaFiscal;

          if Reconfig then
          begin
            EdCarteira.ValueVariant    := Carteira;
            CBCarteira.KeyValue        := Carteira;
            EdCentroCusto.ValueVariant := CentroCusto;
            CBCentroCusto.KeyValue     := CentroCusto;
            CkParcelamento.Checked     := Parcelas > 0;
            EdParcelas.Text            := Geral.FF0(Parcelas);
            RGPeriodo.ItemIndex        := Periodic;
            EdDias.Text                := Geral.FF0(DiasParc);
          end;
          {$IfDef DEF_MYPAGTOSCONFIG}
          begin
            if Dmod.QrControle.FieldByName('VendaParcPg').AsInteger > 1 then
            begin
              CkParcelamento.Checked := True;
              EdParcelas.Text        := Geral.FF0(Dmod.QrControle.FieldByName('VendaParcPg').AsInteger);
              RGPeriodo.ItemIndex    := Dmod.QrControle.FieldByName('VendaPeriPg').AsInteger;
              EdDias.Text            := Geral.FF0(Dmod.QrControle.FieldByName('VendaDiasPg').AsInteger);
              //RGTipo.ItemIndex       := GOTOy.VerificaTipoDaCarteira(
                                        //Dmod.QrControle.FieldByName('VendaCartPg').AsInteger);
              EdCarteira.Text        := Geral.FF0(Dmod.QrControle.FieldByName('VendaCartPg').AsInteger);
              CBCarteira.KeyValue    := Dmod.QrControle.FieldByName('VendaCartPg').AsInteger;
              FmMyPagtos2.CalculaParcelas;
            end;
            CkParcelamento.Checked := Dmod.QrControle.FieldByName('MyPgParc').AsInteger > 0;
            EdParcelas.Text := Geral.FF0(Dmod.QrControle.FieldByName('MyPgQtdP').AsInteger);
            RGPeriodo.ItemIndex := Dmod.QrControle.FieldByName('MyPgPeri').AsInteger;
            EdDias.Text := Geral.FF0(Dmod.QrControle.FieldByName('MyPgDias').AsInteger);
            CalculaParcelas;
          end;
          {$EndIf}
        end;
        FFatID_Sub := FatID_Sub;
        FUsaMinMax := UsaMinMax;
        FValMin    := ValMin;
        FValMax    := ValMax;
        FDescricao := Descricao;
        //
        FUsuario := Usuario;
        //ImgTipo.SQLType := SQLType;
        FDefDuplicata := Titulo;
        if QrEmiss <> nil then
        begin
          if (SQLType = stIns) then
            Parcela := QrEmiss.FieldByName('FatParcela').AsInteger + 1
          else Parcela := QrEmiss.FieldByName('FatParcela').AsInteger;
          FMaxCod := QrEmiss.RecordCount + 1;
        end else begin
          Parcela := 1;
          FMaxCod := 1;
        end;
        EdCodigo.Text         := Geral.FF0(Parcela);
        EdCodigo.Enabled      := True;
        EdCliInt.ValueVariant := CliInt;
        CBCliInt.KeyValue     := CliInt;
        //
        if Valor < 0 then Valor := Valor * -1;
        EdValor.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
        //
        EdQtde.ValueVariant := Qtde;
        EdQtd2.ValueVariant := Qtd2;
        //
        Result := True;
      end;
    finally
      //Screen.Cursor := Cursor;
    end;
  end;
end;

end.
