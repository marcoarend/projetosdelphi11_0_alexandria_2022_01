unit CopiaDoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, mySQLDbTables,
  dmkGeral, frxRich, frxChBox, frxClass, frxDBSet, UnDmkProcFunc, dmkImage,
  frxBarcode, UnDmkEnums;

type
  TFmCopiaDoc = class(TForm)
    Panel1: TPanel;
    QrCorrige: TmySQLQuery;
    QrCorrigeTipoDoc: TSmallintField;
    QrCorrigeID_Pgto: TIntegerField;
    QrCopiaCH1: TmySQLQuery;
    QrCopiaCH1Itens: TLargeintField;
    QrCopiaCH1Banco1: TIntegerField;
    QrCopiaCH1Conta1: TWideStringField;
    QrCopiaCH1Agencia1: TIntegerField;
    QrCopiaCH1SerieCH: TWideStringField;
    QrCopiaCH1Data: TDateField;
    QrCopiaCH1Documento: TFloatField;
    QrCopiaCH1NOMEBANCO: TWideStringField;
    QrCopiaCH1TipoDoc: TIntegerField;
    QrCopiaCH1Tipo: TIntegerField;
    QrCopiaCH1ID_Copia: TIntegerField;
    QrCopiaCH1DEBITO: TFloatField;
    QrItens: TmySQLQuery;
    QrItensNOMEFORNECE: TWideStringField;
    QrItensDescricao: TWideStringField;
    QrItensControle: TIntegerField;
    QrCopiaCH2: TmySQLQuery;
    DsCopiaCH2: TDataSource;
    QrCopiaCH2Banco1: TIntegerField;
    QrCopiaCH2Conta1: TWideStringField;
    QrCopiaCH2Agencia1: TIntegerField;
    QrCopiaCH2SerieCH: TWideStringField;
    QrCopiaCH2Data: TDateField;
    QrCopiaCH2Documento: TFloatField;
    QrCopiaCH2Debito: TFloatField;
    QrCopiaCH2NOMEFORNECE: TWideMemoField;
    QrCopiaCH2Descricao: TWideMemoField;
    QrCopiaCH2Controles: TWideMemoField;
    QrCopiaCH2ID_Pgto: TIntegerField;
    QrCopiaCH2NOMEBANCO: TWideStringField;
    QrCopiaCH2TipoDoc: TIntegerField;
    QrCopiaCH2Tipo: TIntegerField;
    QrCopiaCH2ID_Copia: TIntegerField;
    QrCopiaCH2QtdDocs: TIntegerField;
    DBGrid2: TDBGrid;
    frxCopiaDOC: TfrxReport;
    frxCheckBoxObject1: TfrxCheckBoxObject;
    frxRichObject1: TfrxRichObject;
    frxDsCopiaDOC: TfrxDBDataset;
    QrCopiaCH1NOMEEMPRESA: TWideStringField;
    QrCopiaCH1NotaFiscal: TIntegerField;
    QrCopiaCH2NOMEEMPRESA: TWideStringField;
    QrCopiaCH2NotaFiscal: TIntegerField;
    QrCopiaCH1Duplicata: TWideStringField;
    QrCopiaCH2Duplicata: TWideStringField;
    QrContasEnt: TmySQLQuery;
    QrContasEntCntrDebCta: TWideStringField;
    QrCopiaCH2Genero: TIntegerField;
    QrCopiaCH1Genero: TIntegerField;
    QrCopiaCH1NOMEFORNECE: TWideStringField;
    QrCopiaCH1Controle: TIntegerField;
    QrCopiaCH1Descricao: TWideStringField;
    frxCopiaDOC2: TfrxReport;
    Panel4: TPanel;
    CkDono: TCheckBox;
    RGDataTipo: TRadioGroup;
    RGItensPagina: TRadioGroup;
    CkNaoAgrupar: TCheckBox;
    QrCopiaCH1Protocolo: TIntegerField;
    QrCopiaCH1EAN128I: TWideStringField;
    QrCopiaCH2Protocolo: TIntegerField;
    QrCopiaCH2EAN128I: TWideStringField;
    QrCopiaCH2EANTem: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtPesquisa: TBitBtn;
    BtImprime: TBitBtn;
    QrCopiaCH1EANTem: TIntegerField;
    frxBarCodeObject1: TfrxBarCodeObject;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure frxCopiaDOCGetValue(const VarName: string; var Value: Variant);
    procedure QrCopiaCH2BeforeClose(DataSet: TDataSet);
    procedure QrCopiaCH2AfterOpen(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CkNaoAgruparClick(Sender: TObject);
    procedure QrCopiaCH1CalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    (*FCopDocCtrl, *)FCopiaCH1, FCopiaCH2: String;

  public
    { Public declarations }
    FDBG: TDBGrid;
    FQrLct: TmySQLQuery;
    FTabLct: String;
  end;

  var
  FmCopiaDoc: TFmCopiaDoc;

implementation

uses UCreate, ModuleGeral, UMySQLModule, UnMyObjects, UnInternalConsts, Module,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmCopiaDoc.BtImprimeClick(Sender: TObject);
begin
  DModG.ReopenEndereco(DmodG.QrCliIntLogCodEnti.Value);
  //
  if RGItensPagina.ItemIndex > 1 then
  begin
    MyObjects.frxDefineDataSets(frxCopiaDOC2, [
      frxDsCopiaDOC,
      DModG.frxDsEndereco
      ]);
    //
    MyObjects.frxMostra(frxCopiaDOC2, 'C�pia de lan�amentos');
  end else
  begin
    MyObjects.frxDefineDataSets(frxCopiaDOC, [
      frxDsCopiaDOC,
      DModG.frxDsEndereco
      ]);
    //
    MyObjects.frxMostra(frxCopiaDOC, 'C�pia de lan�amentos');
  end;
end;

procedure TFmCopiaDoc.BtPesquisaClick(Sender: TObject);
var
  Documento: Double;
  Ctrls: String;
  //Controle, Sub, Carteira,
  Tipo, i: Integer;
  Data, SerieCH: String;
  //
  Genero, NotaFiscal, Banco1, Agencia1, ID_Pgto, TipoDoc, ID_Copia, QtdDocs,
  Protocolo, EANTem: Integer;
  NOMEEMPRESA, Duplicata, Conta1, NOMEFORNECE, Descricao, NOMEBANCO, Controles,
  EAN128I: String;
  Debito: Double;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas tempor�rias');
  FCopiaCH1 := UCriar.RecriaTempTableNovo(ntrttCopiaCH1, DmodG.QrUpdPID1, False);
  FCopiaCH2 := UCriar.RecriaTempTableNovo(ntrttCopiaCH2, DmodG.QrUpdPID1, False);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando dados selecionados');
  Ctrls := '';
  if FDBG.SelectedRows.Count > 1 then
  begin
    with FDBG.DataSource.DataSet do
    for i:= 0 to FDBG.SelectedRows.Count-1 do
    begin
      //GotoBookmark(FDBG.SelectedRows.Items[i]);
      GotoBookmark(FDBG.SelectedRows.Items[i]);
      Ctrls := Ctrls + FormatFloat('0', FQrLct.FieldByName('Controle').AsInteger) + ',';
    end;
    Ctrls := Copy(Ctrls, 1, Length(Ctrls)-1);
  end else Ctrls := FormatFloat('0', FQrLct.FieldByName('Controle').AsInteger);
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FCopiaCH1);
  DModG.QrUpdPID1.SQL.Add('SELECT car.Banco1, car.Conta1, car.Agencia1,');
  DModG.QrUpdPID1.SQL.Add('lan.SerieCH,');
  case RGDataTipo.ItemIndex of
    0:   DModG.QrUpdPID1.SQL.Add('lan.Data,');
    else DModG.QrUpdPID1.SQL.Add('lan.Vencimento,');
  end;
  DmodG.QrUpdPID1.SQL.Add('lan.Documento, lan.Debito,');
  DModG.QrUpdPID1.SQL.Add('IF(lan.Fornecedor=0,"",IF(frn.Tipo=0,');
  DModG.QrUpdPID1.SQL.Add('frn.RazaoSocial,frn.Nome)) NOMEFORNECE,');
  DModG.QrUpdPID1.SQL.Add('lan.Descricao, lan.Controle, lan.ID_Pgto,');
  DModG.QrUpdPID1.SQL.Add('ban.Nome NOMEBANCO, car.TipoDoc, car.Tipo, ');
  DModG.QrUpdPID1.SQL.Add('0 ID_Copia, ');
  DModG.QrUpdPID1.SQL.Add('IF(lan.CliInt=0,"",IF(emp.Tipo=0,');
  DModG.QrUpdPID1.SQL.Add('emp.RazaoSocial,emp.Nome)) NOMEEMPRESA,');
  DModG.QrUpdPID1.SQL.Add('lan.NotaFiscal, lan.Duplicata, ');
  DModG.QrUpdPID1.SQL.Add('lan.Genero, lan.Protocolo, ');
  DModG.QrUpdPID1.SQL.Add('IF(lan.Protocolo <> 0, 1, 0) EANTem');
  DModG.QrUpdPID1.SQL.Add('FROM '+ FTabLct + ' lan');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades frn ON frn.Codigo=lan.Fornecedor');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades emp ON emp.Codigo=lan.CliInt');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.carteiras car ON car.Codigo=lan.Carteira');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.bancos    ban ON ban.Codigo=car.Banco1 AND ban.Codigo<>0');
  // 2011-07-22 - Imprimir zerados tamb�m!
  //DModG.QrUpdPID1.SQL.Add('WHERE lan.Sub=0 AND Debito>0');
  DModG.QrUpdPID1.SQL.Add('WHERE lan.Sub=0'); //AND Debito>0');
  DModG.QrUpdPID1.SQL.Add('AND lan.Controle IN (' + Ctrls + ')');
  DModG.QrUpdPID1.ExecSQL;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando carteiras de origem');
  QrCorrige.Close;
  QrCorrige.SQL.Clear;
  QrCorrige.SQL.Add('SELECT DISTINCT car.TipoDoc, cc1.ID_Pgto');
  QrCorrige.SQL.Add('FROM copiach1 cc1');
  QrCorrige.SQL.Add('LEFT JOIN '+ FTabLct + ' lan ON lan.Controle=cc1.ID_Pgto');
  QrCorrige.SQL.Add('LEFT JOIN '+ TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
  QrCorrige.SQL.Add('WHERE cc1.ID_Pgto <> 0');
  QrCorrige.SQL.Add('');
  QrCorrige.SQL.Add('');
  QrCorrige.SQL.Add('');
  QrCorrige.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrCorrige, DModG.MyPID_DB);
  if QrCorrige.RecordCount > 0 then
  begin
    QrCorrige.First;
    while not QrCorrige.Eof do
    begin
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stUpd, FCopiaCH1, False, [
      'TipoDoc'], ['ID_Pgto'], [QrCorrigeTipoDoc.Value], [QrCorrigeID_Pgto.Value
      ], False);
      //
      QrCorrige.Next;
    end;  
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Separando itens sem documento');
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + FCopiaCH1);
  DModG.QrUpdPID1.SQL.Add('SET ID_Copia=Controle ');
  DModG.QrUpdPID1.SQL.Add('WHERE Documento=0');
  DModG.QrUpdPID1.ExecSQL;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando c�pias');
  QrCopiaCH1.Close;
  QrCopiaCH1.SQL.Clear;
  if CkNaoAgrupar.Checked then
  begin
    QrCopiaCH1.SQL.Add('SELECT 1 Itens, Banco1, Conta1,');
    QrCopiaCH1.SQL.Add('Agencia1, SerieCH, Data, Documento, NOMEBANCO,');
    QrCopiaCH1.SQL.Add('TipoDoc, Tipo, ID_Copia, Debito DEBITO,');
    QrCopiaCH1.SQL.Add('NOMEEMPRESA, NotaFiscal, Duplicata, Genero,');
    QrCopiaCH1.SQL.Add('NOMEFORNECE, Controle, Descricao, ');
    QrCopiaCH1.SQL.Add('Protocolo, EANTem ');
    QrCopiaCH1.SQL.Add('FROM copiach1 cc1');
  end else
  begin
    QrCopiaCH1.SQL.Add('SELECT COUNT(cc1.Debito) Itens, Banco1, Conta1,');
    QrCopiaCH1.SQL.Add('Agencia1, SerieCH, Data, Documento, NOMEBANCO,');
    QrCopiaCH1.SQL.Add('TipoDoc, Tipo, ID_Copia, SUM(Debito) DEBITO,');
    QrCopiaCH1.SQL.Add('NOMEEMPRESA, NotaFiscal, Duplicata, Genero,');
    QrCopiaCH1.SQL.Add('NOMEFORNECE, Controle, Descricao, ');
    QrCopiaCH1.SQL.Add('Protocolo, EANTem ');
    QrCopiaCH1.SQL.Add('FROM copiach1 cc1');
    QrCopiaCH1.SQL.Add('GROUP BY Banco1, Conta1,');
    QrCopiaCH1.SQL.Add('Agencia1, SerieCH, Data, Documento, NOMEBANCO,');
    QrCopiaCH1.SQL.Add('TipoDoc, Tipo, ID_Copia');
  end;
  UnDmkDAC_PF.AbreQuery(QrCopiaCH1, DModG.MyPID_DB);
  QrCopiaCH1.First;
  while not QrCopiaCH1.Eof do
  begin
    Banco1        := QrCopiaCH1Banco1.Value;
    Conta1        := QrCopiaCH1Conta1.Value;
    ID_Pgto       := 0;
    TipoDoc       := QrCopiaCH1TipoDoc.Value;
    Tipo          := QrCopiaCH1Tipo.Value;
    ID_Copia      := QrCopiaCH1ID_Copia.Value;
    QtdDocs       := QrCopiaCH1Itens.Value;
    Agencia1      := QrCopiaCH1Agencia1.Value;
    SerieCH       := QrCopiaCH1SerieCH.Value;
    Documento     := QrCopiaCH1Documento.Value;
    NOMEBANCO     := QrCopiaCH1NOMEBANCO.Value;
    Debito        := QrCopiaCH1DEBITO.Value;
    Data          := Geral.FDT(QrCopiaCH1Data.Value, 1);
    NOMEEMPRESA   := QrCopiaCH1NOMEEMPRESA.Value;
    NotaFiscal    := QrCopiaCH1NotaFiscal.Value;
    Duplicata     := QrCopiaCH1Duplicata.Value;
    Genero        := QrCopiaCH1Genero.Value;
    Protocolo     := QrCopiaCH1Protocolo.Value;
     if Protocolo <> 0 then
       EANTem := 1
     else
       EANTem := 0;
    EAN128I       := QrCopiaCH1EAN128I.Value;
    //
    QrItens.Close;
    QrItens.Params[00].AsInteger := Banco1;
    QrItens.Params[01].AsString  := Conta1;
    QrItens.Params[02].AsInteger := Agencia1;
    QrItens.Params[03].AsString  := SerieCH;
    QrItens.Params[04].AsString  := Data;
    QrItens.Params[05].AsFloat   := Documento;
    QrItens.Params[06].AsString  := NOMEBANCO;
    QrItens.Params[07].AsInteger := TipoDoc;
    QrItens.Params[08].AsInteger := Tipo;
    QrItens.Params[09].AsInteger := ID_Copia;
    UnDmkDAC_PF.AbreQuery(QrItens, DModG.MyPID_DB);
    //if QrItens.RecordCount > 0 then
    if (QrItens.RecordCount > 0) and (not CkNaoAgrupar.Checked) then
    begin
      QrItens.First;
      NOMEFORNECE   := '';
      Descricao     := '';
      Controles     := '';
      while not QrItens.Eof do
      begin
        NOMEFORNECE   := NOMEFORNECE + QrItensNOMEFORNECE.Value;
        Descricao     := Descricao   + QrItensDescricao.Value;
        Controles     := Controles   + FormatFloat('0', QrItensControle.Value);
        if QrItens.RecNo < QrItens.RecordCount then
        begin
          NOMEFORNECE   := NOMEFORNECE + '; ';
          Descricao     := Descricao   + '; ';
          Controles     := Controles   + '; ';
        end else
          Descricao     := Descricao   + '. ';
        //
        QrItens.Next;
      end;
    end else begin
      NOMEFORNECE   := QrCopiaCH1NOMEFORNECE.Value;
      Descricao     := QrCopiaCH1Descricao.Value;
      Controles     := FormatFloat('0', QrCopiaCH1Controle.Value);
    end;
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, FCopiaCH2, False, [
    'Banco1', 'Conta1', 'Agencia1',
    'SerieCH', 'Data', 'Documento',
    'Debito', 'NOMEFORNECE', 'Descricao',
    'Controles', 'ID_Pgto', 'NOMEBANCO',
    'TipoDoc', 'Tipo', 'ID_Copia',
    'QtdDocs', 'NOMEEMPRESA', 'NotaFiscal',
    'Duplicata', 'Genero', 'Protocolo',
    'EANTem', 'EAN128I'], [
    ], [
    Banco1, Conta1, Agencia1,
    SerieCH, Data, Documento,
    Debito, NOMEFORNECE, Descricao,
    Controles, ID_Pgto, NOMEBANCO,
    TipoDoc, Tipo, ID_Copia,
    QtdDocs, NOMEEMPRESA, NotaFiscal,
    Duplicata, Genero, Protocolo,
    EANTem, EAN128I], [
    ], False);
    QrCopiaCH1.Next;
  end;
  QrCopiaCH2.Close;
  UnDmkDAC_PF.AbreQuery(QrCopiaCH2, DModG.MyPID_DB);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, IntToStr(QrCopiaCH2.RecordCount) + ' itens localizados.');
end;

procedure TFmCopiaDoc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCopiaDoc.CkNaoAgruparClick(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmCopiaDoc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCopiaDoc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Geral.WriteAppKeyCU(
    'PageIts', Application.Title + '\CopiaDoc', RGItensPagina.ItemIndex, ktInteger);
  Geral.WriteAppKeyCU(
    'DataTip', Application.Title + '\CopiaDoc', RGDataTipo.ItemIndex, ktInteger);
  Geral.WriteAppKeyCU(
    'NomeEmp', Application.Title + '\CopiaDoc', CkDono.Checked, ktBoolean);
end;

procedure TFmCopiaDoc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  RGItensPagina.ItemIndex := Geral.ReadAppKeyCU('PageIts',
    Application.Title + '\CopiaDoc', ktInteger, 0);
  RGDataTipo.ItemIndex    := Geral.ReadAppKeyCU('DataTip',
    Application.Title + '\CopiaDoc', ktInteger, 0);
  CkDono.Checked          := Geral.ReadAppKeyCU('NomeEmp',
    Application.Title + '\CopiaDoc', ktBoolean, 0);
end;

procedure TFmCopiaDoc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCopiaDoc.frxCopiaDOCGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TEM_CODBARRA' then Value := QrCopiaCH2EANTem.Value > 0 else
  if VarName = 'VARF_EHDEBITO'   then Value := QrCopiaCH2TipoDoc.Value <> 1 else
  if VarName = 'VARF_ITENS'      then Value := RGItensPagina.ItemIndex + 1 else
  if VarName = 'VAR_IMPDONO'     then Value := CkDono.Checked;
  if VarName = 'VARF_EXTENSO'    then Value :=
    dmkPF.ExtensoMoney(Geral.FFT(QrCopiaCH2Debito.Value, 2, siPositivo)) else
  // .... terminando abaixo
  if QrCopiaCH2TipoDoc.Value = 1 then
  begin
    if VarName = 'TIT_COPIA'     then Value := 'C�PIA DE CHEQUE' else
    if VarName = 'TIT_DOC'       then Value := 'Cheque N�' else
    if VarName = 'TXT_EXTENSO'   then Value := 'Pague por este cheque a quantia de ' else
    if VarName = 'TXT_PARA'      then Value := 'a ' else
    if VarName = 'TXT_OUPARA'    then Value := ' ou a sua ordem.' else
    if VarName = 'CANHOTO'       then Value := 'C A N H O T O ' else
    if VarName = 'VARF_CONTRATO' then Value := ' ' else
    if VarName = 'TIT_RESPONSABILIDADE' then Value := 'CHEQUE ASSINADO POR' else
  end else begin
    if VarName = 'TIT_COPIA'     then Value := 'C�PIA DE D�BITO EM CONTA' else
    if VarName = 'TIT_DOC'       then Value := 'Documento' else
    if VarName = 'TXT_EXTENSO'   then Value := 'Foi debitado na conta corrente acima o valor de ' else
    if VarName = 'TXT_PARA'      then Value := 'referente ao pagamento do fornecedor ' else
    if VarName = 'TXT_OUPARA'    then Value := '.' else
    if VarName = 'CANHOTO'       then Value := ' ' else
    if VarName = 'VARF_CONTRATO' then
    begin
      if QrCopiaCH2Genero.Value <> 0 then
      begin
        QrContasEnt.Close;
        QrContasEnt.Params[0].AsInteger := QrCopiaCH2Genero.Value;
        UnDmkDAC_PF.AbreQuery(QrContasEnt, Dmod.MyDB);
        Value := QrContasEntCntrDebCta.Value;
      end else Value := ' ';
    end else
    if VarName = 'TIT_RESPONSABILIDADE' then Value := 'Lan�amento ratificado por' else
  end;
  // n�o colocar nada aqui
end;

procedure TFmCopiaDoc.QrCopiaCH1CalcFields(DataSet: TDataSet);
begin
  QrCopiaCH1EAN128I.Value := '>I<' + Geral.FFN(QrCopiaCH1Protocolo.Value, 11);
end;

procedure TFmCopiaDoc.QrCopiaCH2AfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := QrCopiaCH2.RecordCount > 0;
end;

procedure TFmCopiaDoc.QrCopiaCH2BeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

end.
