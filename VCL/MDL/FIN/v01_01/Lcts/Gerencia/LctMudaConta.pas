unit LctMudaConta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, DBCtrls, ComCtrls,
  Grids, DBGrids, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmLctMudaConta = class(TForm)
    PainelDados: TPanel;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label15: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtSaida_: TBitBtn;
    BtOK: TBitBtn;
    procedure BtOKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtSaida_Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SohPermiteTipo1: Boolean;
    FContaSel: Integer;
  end;

  var
  FmLctMudaConta: TFmLctMudaConta;

implementation

{$R *.DFM}

uses UnMyObjects, Module, DmkDAC_PF;

procedure TFmLctMudaConta.BtOKClick(Sender: TObject);
begin
  FContaSel := Geral.IMV(EdConta.Text);
  Close;
end;

procedure TFmLctMudaConta.BtSaida_Click(Sender: TObject);
begin
  Close;
end;

procedure TFmLctMudaConta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctMudaConta.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctMudaConta.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  FContaSel := -1000;
end;

end.
