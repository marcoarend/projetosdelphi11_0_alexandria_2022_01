unit LocDefs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, UnGOTOy, Buttons, UnInternalConsts, ExtCtrls,
  Db, (*DBTables,*) DBCtrls, mySQLDbTables, Variants, dmkGeral, dmkEditCB, dmkEdit,
  dmkDBLookupComboBox, dmkEditDateTimePicker, dmkCheckGroup, dmkImage,
  DmkDAC_PF, UnDmkEnums;

type
  TFmLocDefs = class(TForm)
    QrPContas: TmySQLQuery;
    DsPContas: TDataSource;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    DsPCliInt: TDataSource;
    QrPCliInt: TmySQLQuery;
    QrPCliIntCodigo: TIntegerField;
    QrPCliIntNOMEENT: TWideStringField;
    QrPFornece: TmySQLQuery;
    DsPFornece: TDataSource;
    QrPCliente: TmySQLQuery;
    DsPCliente: TDataSource;
    QrPForneceCodigo: TIntegerField;
    QrPForneceNOMEENT: TWideStringField;
    QrPClienteCodigo: TIntegerField;
    QrPClienteNOMEENT: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox1: TGroupBox;
    PainelDados: TPanel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    CkDataIni: TCheckBox;
    CkDataFim: TCheckBox;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    CkConta: TCheckBox;
    CBConta: TdmkDBLookupComboBox;
    CkDescricao: TCheckBox;
    EdDescricao: TEdit;
    CkDebito: TCheckBox;
    CkCredito: TCheckBox;
    CkNF: TCheckBox;
    CkDoc: TCheckBox;
    CkControle: TCheckBox;
    EdDebMin: TdmkEdit;
    EdCredMin: TdmkEdit;
    EdNF: TdmkEdit;
    EdDoc: TdmkEdit;
    EdControle: TdmkEdit;
    RgOrdem: TRadioGroup;
    EdDuplicata: TdmkEdit;
    CkDuplicata: TCheckBox;
    EdDebMax: TdmkEdit;
    EdCredMax: TdmkEdit;
    CkExcelGru: TCheckBox;
    EdConta: TdmkEditCB;
    CGCarteiras: TdmkCheckGroup;
    CkCliInt: TCheckBox;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    CkVctoIni: TCheckBox;
    TPVctoIni: TdmkEditDateTimePicker;
    CkVctoFim: TCheckBox;
    TPVctoFim: TdmkEditDateTimePicker;
    CBFornece: TdmkDBLookupComboBox;
    EdFornece: TdmkEditCB;
    CkFornece: TCheckBox;
    CkCliente: TCheckBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    RGTabela: TRadioGroup;
    LaForneceRN: TLabel;
    LaForneceFA: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    CkCarteira: TCheckBox;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasNome2: TWideStringField;
    QrCarteirasContato1: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasPagRec: TIntegerField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasAntigo: TWideStringField;
    QrCarteirasContab: TWideStringField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TIntegerField;
    QrCarteirasUserAlt: TIntegerField;
    QrCarteirasOrdem: TIntegerField;
    QrCarteirasForneceN: TSmallintField;
    QrCarteirasFuturoC: TFloatField;
    QrCarteirasFuturoD: TFloatField;
    QrCarteirasFuturoS: TFloatField;
    QrCarteirasExclusivo: TSmallintField;
    QrCarteirasAlterWeb: TSmallintField;
    QrCarteirasAtivo: TSmallintField;
    QrCarteirasRecebeBloq: TSmallintField;
    QrCarteirasEntiDent: TIntegerField;
    DsCarteiras: TDataSource;
    CGContrlCred: TdmkCheckGroup;
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CkDataFimClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdDebMinExit(Sender: TObject);
    procedure EdCredMinExit(Sender: TObject);
    procedure CBForneceKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdForneceKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenCliente(NomeFantasia: Boolean);
    procedure ReopenFornece(NomeFantasia: Boolean);
  public
    { Public declarations }
    FModuleLctX: TDataModule;
    FTabLctA: String;
    FQuemChamou: Integer;
    FQrCrt, FQrLct: TmySQLQuery;
    FDTDataIni, FDTDataFim, FTipoData: Integer;
    FDTPDataIni, FDTPDataFim: TDateTimePicker;
    FComponentClass: TComponentClass;
    FReference: TComponent;
    F_CliInt, FShowForm: Integer;
    FQrLoc: TmySQLQuery;
    FLocSohCliInt: Boolean;
    procedure ReopenPCliInt();
  end;

var
  FmLocDefs: TFmLocDefs;

implementation

uses UnMyObjects, Module, LocLancto, UnFinanceiro, UMySQLModule, UnDmkProcFunc;

{$R *.DFM}

procedure TFmLocDefs.BtConfirmaClick(Sender: TObject);
{
var
  Cursor : TCursor;
  DataIni, DataFim, VctoIni, VctoFim: String;
  i: Integer;
  TabLct: String;
begin
  TabLct := Copy(FTabLctA, 1, Length(FTabLctA) -1) + Lowercase(RGTabela.Items[RGTabela.ItemIndex][1]);
  //
  if FShowForm = 0 then
  begin
    MyObjects.CriaForm_AcessoTotal(TFmLocLancto, FmLocLancto);
    FmLocLancto.FModuleLctX := FModuleLctX;
    FmLocLancto.PnTabLctA.Visible := RGTabela.ItemIndex = 0;
    FmLocLancto.FConfirmou    := False;
    FmLocLancto.FQrCrt        := FQrCrt;
    FmLocLancto.FQrLct        := FQrLct;
    FmLocLancto.FLocSohCliInt := FLocSohCliInt;
    FmLocLancto.FCliLoc       := Geral.IMV(EdCliInt.Text);
    FmLocLancto.FDTPDataIni   := FDTPDataIni;
    FmLocLancto.FDTPDataFim   := FDTPDataFim;
    FmLocLancto.FTipoData     := FTipoData;
    if RGTabela.ItemIndex = 0 then
      FmLocLancto.FTabLctA      := TabLct
    else
      FmLocLancto.FTabLctA      := '';
    FQrLoc := FmLocLancto.QrLoc;
    case FQuemChamou of
      0: ; // Nada
      1:
      begin
        FmLocLancto.GBConfirma2.Visible := True;
        FmLocLancto.GBConfirma1.Visible := False;
      end;
      2: ;// EventosCad
    end;
  end;
  //
  i := 0;
  DataIni := FormatDateTime(VAR_FORMATDATE, TPDataIni.Date);
  DataFim := FormatDateTime(VAR_FORMATDATE, TPDataFim.Date);
  VctoIni := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date);
  VctoFim := FormatDateTime(VAR_FORMATDATE, TPVctoFim.Date);
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  Refresh;
  try
    FQrLoc.SQL.Clear;
    //FQrLoc.SQL.Add('SELECT MONTH(la.Mes) Mes2, YEAR(la.Mes) Ano, ');
    FQrLoc.SQL.Add('SELECT IF(la.Fornecedor<>0, la.Fornecedor, la.Cliente) + 0.000 Terceiro,');
    FQrLoc.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO,');
    FQrLoc.SQL.Add('MOD(la.Mez, 100) Mes2,');
    FQrLoc.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
    FQrLoc.SQL.Add('ca.Nome NOMECARTEIRA, la.*, co.Nome NOMEGENERO, ');
    FQrLoc.SQL.Add('ca.ForneceI CLIENTE_INTERNO ');
    FQrLoc.SQL.Add('FROM ' + FTabLctA + ' la ');
    FQrLoc.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ');
    FQrLoc.SQL.Add('LEFT JOIN contas co ON co.Codigo=la.Genero');
    FQrLoc.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=(');
    FQrLoc.SQL.Add('  IF(la.Fornecedor<>0, la.Fornecedor, la.Cliente))');
    FQrLoc.SQL.Add('WHERE la.Controle <> 0');
    // configura data de emiss�o
    if CkDataFim.Checked then
      FQrLoc.SQL.Add('AND la.Data BETWEEN '''+DataIni+''' AND '''+DataFim+'''')
      else if CkDataIni.Checked then
        FQrLoc.SQL.Add('AND la.Data='''+DataIni+'''');
    // configura data de vencimento
    if CkVctoFim.Checked then
      FQrLoc.SQL.Add('AND la.Vencimento BETWEEN '''+VctoIni+''' AND '''+VctoFim+'''')
      else if CkVctoIni.Checked then
        FQrLoc.SQL.Add('AND la.Vencimento='''+VctoIni+'''');
    // configara Conta
    if CkConta.Checked = True then
    begin
      if CBConta.KeyValue <> Null then
      FQrLoc.SQL.Add('AND la.Genero = '''+IntToStr(CBConta.KeyValue)+'''');
    end;
    if CkCarteira.Checked = True then
    begin
      if CBCarteira.KeyValue <> Null then
      FQrLoc.SQL.Add('AND la.Carteira = '''+IntToStr(CBCarteira.KeyValue)+'''');
    end;
    // configara Fornecedor
    if CkFornece.Checked = True then
    begin
      if CBFornece.KeyValue <> Null then
      FQrLoc.SQL.Add('AND la.Fornecedor = '''+IntToStr(CBFornece.KeyValue)+'''');
    end;
    // configara Cliente
    if CkCliente.Checked = True then
    begin
      if CBCliente.KeyValue <> Null then
      FQrLoc.SQL.Add('AND la.Cliente = '''+IntToStr(CBCliente.KeyValue)+'''');
    end;
    // configara descri��o
    if CkDescricao.Checked = True then
      FQrLoc.SQL.Add('AND la.Descricao LIKE '''+EdDescricao.Text+'''');
    // configara valores
    if CkDebito.Checked then i := i + 1;
    if CkCredito.Checked then i := i + 2;
    case i of
      1: FQrLoc.SQL.Add('AND la.Debito BETWEEN :P0 AND :P1');
      2: FQrLoc.SQL.Add('AND la.Credito BETWEEN :P0 AND :P1');
      3:
      begin
        FQrLoc.SQL.Add('AND la.Debito BETWEEN :P0 AND :P1');
        FQrLoc.SQL.Add('AND la.Credito BETWEEN :P2 AND :P3');
      end;
    end;
    case i of
      1:
      begin
        FQrLoc.Params[0].AsFloat := Geral.DMV(EdDebMin.Text);
        FQrLoc.Params[1].AsFloat := Geral.DMV(EdDebMax.Text);
      end;
      2:
      begin
        FQrLoc.Params[0].AsFloat := Geral.DMV(EdCredMin.Text);
        FQrLoc.Params[1].AsFloat := Geral.DMV(EdCredMax.Text);
      end;
      3:
      begin
        FQrLoc.Params[0].AsFloat := Geral.DMV(EdDebMin.Text);
        FQrLoc.Params[1].AsFloat := Geral.DMV(EdDebMax.Text);
        FQrLoc.Params[2].AsFloat := Geral.DMV(EdCredMin.Text);
        FQrLoc.Params[3].AsFloat := Geral.DMV(EdCredMax.Text);
      end;
    end;
    if CkNF.Checked = True then
      FQrLoc.SQL.Add('AND la.NotaFiscal='+
      IntToStr(Geral.IMV(EdNF.Text)));
    // configara Duplicata
    if CkDuplicata.Checked = True then
      FQrLoc.SQL.Add('AND la.Duplicata="'+
      EdDuplicata.Text + '"');
    // configara Controle
    if CkControle.Checked = True then
      FQrLoc.SQL.Add('AND la.Controle='+IntToStr(Geral.IMV(EdControle.Text)));
    // configara Documento
    if CkDoc.Checked = True then
      FQrLoc.SQL.Add('AND la.Documento='+IntToStr(Geral.IMV(EdDoc.Text)));
    // Configura ExcelGru
    if CkExcelGru.Checked = True then
      FQrLoc.SQL.Add('AND (la.ExcelGru IS Null OR la.ExcelGru = 0)');
    // Tipo de carteiras
    case CGcarteiras.Value of
      1: FQrLoc.SQL.Add('AND ca.Tipo = 0');
      2: FQrLoc.SQL.Add('AND ca.Tipo = 1');
      3: FQrLoc.SQL.Add('AND ca.Tipo in (0,1)');
      4: FQrLoc.SQL.Add('AND ca.Tipo = 2');
      5: FQrLoc.SQL.Add('AND ca.Tipo in (0,2)');
      6: FQrLoc.SQL.Add('AND ca.Tipo in (1,2)');
      7: ; // Qualquer coisa
      else Geral.MB_Erro('Nenhum tipo de carteira foi definido!');
    end;
    if CkCliInt.Checked then FQrLoc.SQL.Add('AND ca.ForneceI = ' +
      FormatFloat('0', Geral.IMV(EdCliInt.Text)));
    // 2014-04-18
    case RGEmissoes.ItemIndex of
      1: FQrLoc.SQL.Add('AND (IF(ca.Tipo=2,la.Sit<2, True))');
      2: FQrLoc.SQL.Add('AND (IF(ca.Tipo=2,la.Sit>1, True))');
      else(*0:*) ;// nada
    end;
    // FIM 2014-04-18

    // Ordem
    case RGOrdem.ItemIndex of
      0: FQrLoc.SQL.Add('ORDER BY la.Data, la.Controle');
      1: FQrLoc.SQL.Add('ORDER BY la.Controle');
      2: FQrLoc.SQL.Add('ORDER BY la.NotaFiscal');
      3: FQrLoc.SQL.Add('ORDER BY la.Documento');
      4: FQrLoc.SQL.Add('ORDER BY la.Descricao');
      5: FQrLoc.SQL.Add('ORDER BY la.Debito, la.Credito');
      6: FQrLoc.SQL.Add('ORDER BY la.Credito, la.Debito');
    end;
    try
      UMyMod.AbreQuery(FQrLoc, Dmod.MyDB);
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(FQrLoc, '', nil, True, True);
    end;
  finally
    Screen.Cursor := Cursor;
  end;

  //

  case FShowForm of
    0:
    begin
      FmLocLancto.ShowModal;
      //
      VAR_LOC_LCTO_TRUE := FmLocLancto.FConfirmou;
      VAR_LOC_LCTO_CTRL := FmLocLancto.QrLocControle.Value;
      VAR_LOC_LCTO_SUB  := FmLocLancto.QrLocSub.Value;
      VAR_LOC_LCTO_DEBI := FmLocLancto.QrLocDebito.Value;
      VAR_LOC_LCTO_CRED := FmLocLancto.QrLocCredito.Value;
      //
      FmLocLancto.Destroy;
      if FQuemChamou = 1 then
        Close;
    end;
    1: Close;
    2: Close; // EventosCad
  end;
end;

procedure TFmLocDefs.BtConfirmaClick(Sender: TObject);
}
  function SQLi(Checked: Boolean; Value: Integer; SQLStr: String): String;
  begin
    Result := '';
    if Checked then
      Result := SQLStr + Geral.FF0(Value);
  end;
  function SQLs(Checked: Boolean; Value: String; SQLStr: String): String;
  begin
    Result := '';
    if Checked then
      Result := SQLStr + ' "' + Value + '" ';
  end;
  function SQLbd(Checked: Boolean; Min, Max: Double; SQLStr: String): String;
  begin
    Result := '';
    if Checked then
      Result := SQLStr + Geral.FFT_Dot(Min, 2, siNegativo) +
               ' AND ' + Geral.FFT_Dot(Max, 2, siNegativo);
  end;
var
  Cursor : TCursor;
  DataIni, DataFim, VctoIni, VctoFim: String;
  TabLct: String;
  // 2021-04-20
  CredMin, CredMax, DebiMin, DebiMax: Double;
  SQL, SQL_Data, SQL_Vencto,
  SQL_Conta, SQLCart, SQL_Fornece, SQL_Cli,
  SQL_Descri,
  SQL_Debi, SQL_Cred,
  SQL_NotaFiscal, SQL_Duplicata, SQL_Controle,
  SQL_Documento, SQL_ExcelGru, SQL_Carteiras,
  SQL_CliInt, SQL_ContrlCred,
  SQL_ORDEM: String;
  sAVencer, sVencido: String;
  Indice: Integer;
begin
  TabLct := Copy(FTabLctA, 1, Length(FTabLctA) -1) + Lowercase(RGTabela.Items[RGTabela.ItemIndex][1]);
  //
  if FShowForm = 0 then
  begin
    MyObjects.CriaForm_AcessoTotal(TFmLocLancto, FmLocLancto);
    FmLocLancto.FModuleLctX := FModuleLctX;
    FmLocLancto.PnTabLctA.Visible := RGTabela.ItemIndex = 0;
    FmLocLancto.FConfirmou    := False;
    FmLocLancto.FQrCrt        := FQrCrt;
    FmLocLancto.FQrLct        := FQrLct;
    FmLocLancto.FLocSohCliInt := FLocSohCliInt;
    FmLocLancto.FCliLoc       := Geral.IMV(EdCliInt.Text);
    FmLocLancto.FDTPDataIni   := FDTPDataIni;
    FmLocLancto.FDTPDataFim   := FDTPDataFim;
    FmLocLancto.FTipoData     := FTipoData;
    if RGTabela.ItemIndex = 0 then
      FmLocLancto.FTabLctA      := TabLct
    else
      FmLocLancto.FTabLctA      := '';
    FQrLoc := FmLocLancto.QrLoc;
    case FQuemChamou of
      0: ; // Nada
      1:
      begin
        FmLocLancto.GBConfirma2.Visible := True;
        FmLocLancto.GBConfirma1.Visible := False;
      end;
      2: ;// EventosCad
    end;
  end;
  //
  DataIni := FormatDateTime(VAR_FORMATDATE, TPDataIni.Date);
  DataFim := FormatDateTime(VAR_FORMATDATE, TPDataFim.Date);
  VctoIni := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date);
  VctoFim := FormatDateTime(VAR_FORMATDATE, TPVctoFim.Date);
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  Refresh;
  try
    SQL_Data       := '';
    SQL_Vencto     := '';
    SQL_Conta      := '';
    SQLCart        := '';
    SQL_Fornece    := '';
    SQL_Cli        := '';
    SQL_Descri     := '';
    SQL_Debi       := '';
    SQL_Cred       := '';
    SQL_NotaFiscal := '';
    SQL_Duplicata  := '';
    SQL_Controle   := '';
    SQL_Documento  := '';
    SQL_ExcelGru   := '';
    SQL_Carteiras  := '';
    SQL_CliInt     := '';
    SQL_ContrlCred := '';
    SQL_ORDEM      := '';


    CredMin     := Geral.DMV(EdCredMin.Text);
    CredMax     := Geral.DMV(EdCredMax.Text);
    DebiMin     := Geral.DMV(EdDebMin.Text);
    DebiMax     := Geral.DMV(EdDebMax.Text);
    //
    SQL := Geral.ATS([
    'SELECT IF(la.Fornecedor<>0, la.Fornecedor, la.Cliente) + 0.000 Terceiro, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, ',
    'MOD(la.Mez, 100) Mes2, ',
    '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, ',
    'ca.Nome NOMECARTEIRA, la.*, co.Nome NOMEGENERO,  ',
    'ca.ForneceI CLIENTE_INTERNO  ',
    'FROM ' + FTabLctA + ' la  ',
    'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira  ',
    'LEFT JOIN contas co ON co.Codigo=la.Genero ',
    'LEFT JOIN entidades ent ON ent.Codigo=( ',
    '  IF(la.Fornecedor<>0, la.Fornecedor, la.Cliente)) ',
    'WHERE la.Controle <> 0 ']);
    // configura data de emiss�o
    if CkDataFim.Checked then
      SQL_Data := 'AND la.Data BETWEEN "' + DataIni + '" AND "'+DataFim+'"'
    else
    if CkDataIni.Checked then
      SQL_Data := 'AND la.Data="' + DataIni + '"';
    // configura data de vencimento
    if CkVctoFim.Checked then
      SQL_Vencto := 'AND la.Vencimento BETWEEN "' + VctoIni + '" AND "' + VctoFim + '"'
    else
    if CkVctoIni.Checked then
      SQL_Vencto := 'AND la.Vencimento="' + VctoIni + '"';
    // configara Conta, Carteira, fornecedor, cliente
    SQL_Conta   := SQLi(CkConta.Checked, EdConta.ValueVariant, 'AND la.Genero=');
    SQLCart     := SQLi(CkCarteira.Checked, EdCarteira.ValueVariant, 'AND la.Carteira=');
    SQL_Fornece := SQLi(CkFornece.Checked, EdFornece.ValueVariant, 'AND la.Fornecedor=');
    SQL_Cli     := SQLi(CkCliente.Checked, EdCliente.ValueVariant, 'AND la.Cliente=');
    // configara descri��o
    SQL_Descri  := SQLs(CkDescricao.Checked, EdDescricao.Text, 'AND la.Descricao LIKE ');
    // configara valores
    SQL_Debi := SQLbd(CkDebito.Checked, DebiMin, DebiMax, 'AND la.Debito BETWEEN ');
    SQL_Cred := SQLbd(CkCredito.Checked, CredMin, CredMax, 'AND la.Credito BETWEEN ');
    // NotaFiscal, Duplicata, Controle, Documento, ExcelGru, Carteiras
    SQL_NotaFiscal := SQLi(CkNF.Checked, EdNF.ValueVariant, 'AND la.NotaFiscal=');
    SQL_Duplicata  := SQLs(CkDuplicata.Checked, EdDuplicata.ValueVariant, 'AND la.Duplicata=');
    SQL_Controle   := SQLi(CkControle.Checked, EdControle.ValueVariant, 'AND la.Controle=');
    SQL_Documento  := SQLi(CkDoc.Checked, EdDoc.ValueVariant, 'AND la.Documento=');
    //
    if CkExcelGru.Checked = True then
      SQL_ExcelGru   := 'AND (la.ExcelGru IS Null OR la.ExcelGru = 0)';
    // Tipo de carteiras
    case CGcarteiras.Value of
      1: SQL_Carteiras := 'AND ca.Tipo = 0';
      2: SQL_Carteiras := 'AND ca.Tipo = 1';
      3: SQL_Carteiras := 'AND ca.Tipo in (0,1)';
      4: SQL_Carteiras := 'AND ca.Tipo = 2';
      5: SQL_Carteiras := 'AND ca.Tipo in (0,2)';
      6: SQL_Carteiras := 'AND ca.Tipo in (1,2)';
      7: ; // Qualquer coisa
      else Geral.MB_Erro('Nenhum tipo de carteira foi definido!');
    end;
    // Situa��o
    SQL_CliInt   := SQLi(CkCliInt.Checked, EdCliInt.ValueVariant, 'AND ca.ForneceI=');
    //sAVencer := '((la.Tipo = 2 AND la.Compensado < "1899-12-31 12:00:00" AND la.Vencimento >= SYSDATE())';
    sAVencer := '((la.Tipo = 2) AND (la.Compensado < "1899-12-31 12:00:00") AND (la.Vencimento >= SYSDATE()))';
    sVencido := '((la.Tipo = 2) AND (la.Compensado < "1899-12-31 12:00:00") AND (la.Vencimento <  SYSDATE()))';
    Indice := 0;
    case CGContrlCred.Value of
      0: SQL_ContrlCred := '';
      1: SQL_ContrlCred := 'AND ' + sAVencer;
      2: SQL_ContrlCred := 'AND ' + sVencido;
      3: SQL_ContrlCred := 'AND ((la.Tipo = 2) AND (la.Compensado < "1899-12-31 12:00:00"))';
      4: SQL_ContrlCred := 'AND ((la.Tipo in (0,1)) OR ((la.Tipo = 2) AND (la.Compensado >= "1899-12-31 12:00:00")))';
      5: SQL_ContrlCred := 'AND (NOT ' + sVencido + ')';
      6: SQL_ContrlCred := 'AND (NOT ' + sAVencer + ')';
      7: SQL_ContrlCred := '';
    end;
    // Ordem
    case RGOrdem.ItemIndex of
      0: SQL_ORDEM := 'ORDER BY la.Data, la.Controle';
      1: SQL_ORDEM := 'ORDER BY la.Controle';
      2: SQL_ORDEM := 'ORDER BY la.NotaFiscal';
      3: SQL_ORDEM := 'ORDER BY la.Documento';
      4: SQL_ORDEM := 'ORDER BY la.Descricao';
      5: SQL_ORDEM := 'ORDER BY la.Debito, la.Credito';
      6: SQL_ORDEM := 'ORDER BY la.Credito, la.Debito';
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(FQrLoc, Dmod.MyDB, [
    SQL,
    SQL_Data,
    SQL_Vencto,
    SQL_Conta,
    SQLCart,
    SQL_Fornece,
    SQL_Cli,
    SQL_Descri,
    SQL_Debi,
    SQL_Cred,
    SQL_NotaFiscal,
    SQL_Duplicata,
    SQL_Controle,
    SQL_Documento,
    SQL_ExcelGru,
    SQL_Carteiras,
    SQL_CliInt,
    SQL_ContrlCred,
    //
    SQL_ORDEM,
    '']);
    //Geral.MB_Teste(FQrLoc.SQL.Text);
  finally
    Screen.Cursor := Cursor;
  end;

  //

  case FShowForm of
    0:
    begin
      FmLocLancto.ShowModal;
      //
      VAR_LOC_LCTO_TRUE := FmLocLancto.FConfirmou;
      VAR_LOC_LCTO_CTRL := FmLocLancto.QrLocControle.Value;
      VAR_LOC_LCTO_SUB  := FmLocLancto.QrLocSub.Value;
      VAR_LOC_LCTO_DEBI := FmLocLancto.QrLocDebito.Value;
      VAR_LOC_LCTO_CRED := FmLocLancto.QrLocCredito.Value;
      //
      FmLocLancto.Destroy;
      if FQuemChamou = 1 then
        Close;
    end;
    1: Close;
    2: Close; // EventosCad
  end;
end;

procedure TFmLocDefs.FormCreate(Sender: TObject);
begin
  TPDataIni.Date := 0;
  TPDataFim.Date := 0;
  TPVctoIni.Date := 0;
  TPVctoFim.Date := 0;
  FShowForm      := 0;
  UnDmkDAC_PF.AbreQuery(QrPContas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPFornece, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  CGCarteiras.SetMaxValue();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmLocDefs.CBClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then ReopenCliente(False) else
  if Key = VK_F6 then ReopenCliente(True);
end;

procedure TFmLocDefs.CBForneceKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then ReopenFornece(False) else
  if Key = VK_F6 then ReopenFornece(True);
end;

procedure TFmLocDefs.CkDataFimClick(Sender: TObject);
begin
  if CkDataFim.Checked = True then CkDataIni.Checked := True;
end;

procedure TFmLocDefs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if UFinanceiro.TabLctNaoDefinida(FTabLctA, 'TFmLocDefs.FormActivate()') then
    Close;
end;

procedure TFmLocDefs.BtDesisteClick(Sender: TObject);
begin
  FShowForm := -1;
  Close;
end;

procedure TFmLocDefs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocDefs.ReopenCliente(NomeFantasia: Boolean);
var
  SQL: String;
begin
  if NomeFantasia then
    SQL := 'IF(Tipo=0, Fantasia, Apelido)'
  else
    SQL := 'IF(Tipo=0, RazaoSocial, Nome)';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPCliente, Dmod.MyDB, [
  'SELECT Codigo, '+ SQL +' NOMEENT ',
  'FROM entidades ',
  'ORDER BY NOMEENT ',
  '']);
end;

procedure TFmLocDefs.ReopenFornece(NomeFantasia: Boolean);
var
  SQL: String;
begin
  if NomeFantasia then
    SQL := 'IF(Tipo=0, Fantasia, Apelido)'
  else
    SQL := 'IF(Tipo=0, RazaoSocial, Nome)';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPFornece, Dmod.MyDB, [
  'SELECT Codigo, '+ SQL +' NOMEENT ',
  'FROM entidades ',
  'ORDER BY NOMEENT ',
  '']);
end;

procedure TFmLocDefs.ReopenPCliInt();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPCliInt, Dmod.MyDB, [
  'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT ',
  'FROM entidades ',
  'WHERE Codigo IN (' + VAR_LIB_EMPRESAS + ') ',
  'ORDER BY NOMEENT ',
  '']);
end;

procedure TFmLocDefs.EdDebMinExit(Sender: TObject);
begin
   if Geral.DMV(EdDebMax.Text) < Geral.DMV(EdDebMin.Text) then
     EdDebMax.Text := EdDebMin.Text;
end;

procedure TFmLocDefs.EdForneceKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then ReopenFornece(False) else
  if Key = VK_F6 then ReopenFornece(True);
end;

procedure TFmLocDefs.EdClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then ReopenCliente(False) else
  if Key = VK_F6 then ReopenCliente(True);
end;

procedure TFmLocDefs.EdCredMinExit(Sender: TObject);
begin
   if Geral.DMV(EdCredMax.Text) < Geral.DMV(EdCredMin.Text) then
     EdCredMax.Text := EdCredMin.Text;
end;

end.

