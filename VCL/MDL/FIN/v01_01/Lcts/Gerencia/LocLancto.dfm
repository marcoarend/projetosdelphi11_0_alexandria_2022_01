object FmLocLancto: TFmLocLancto
  Left = 333
  Top = 177
  Caption = '???-?????-999 :: Localiza'#231#227'o de Lan'#231'amentos'
  ClientHeight = 536
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 104
    Width = 1008
    Height = 362
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGLct: TdmkDBGridZTO
      Left = 0
      Top = 0
      Width = 1008
      Height = 296
      Align = alClient
      DataSource = DsLoc
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = DBGLctDblClick
      OnKeyDown = DBGLctKeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Compensado'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Documento'
          Title.Caption = 'Doc.'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECARTEIRA'
          Title.Caption = 'Carteira'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEGENERO'
          Title.Caption = 'Conta'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 170
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Terceiro'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_TERCEIRO'
          Title.Caption = 'Nome do terceiro'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Title.Caption = 'N.F.'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Autorizacao'
          Title.Caption = 'Autoriz.'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MENSAL'
          Title.Caption = 'M'#234's'
          Width = 40
          Visible = True
        end>
    end
    object GBConfirma2: TGroupBox
      Left = 0
      Top = 296
      Width = 1008
      Height = 66
      Align = alBottom
      TabOrder = 1
      Visible = False
      object PnConfirmaGB2: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 49
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaidaB: TBitBtn
          Tag = 13
          Left = 689
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaAClick
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 352
        Height = 32
        Caption = 'Localiza'#231#227'o de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 352
        Height = 32
        Caption = 'Localiza'#231#227'o de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 352
        Height = 32
        Caption = 'Localiza'#231#227'o de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 56
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBConfirma1: TGroupBox
    Left = 0
    Top = 466
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnConfirmaGB1: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel2: TPanel
        Left = 808
        Top = 0
        Width = 196
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaidaA: TBitBtn
          Tag = 13
          Left = 97
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaAClick
        end
        object BtSoma: TBitBtn
          Tag = 254
          Left = 6
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Soma'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtSomaClick
        end
      end
      object PnTabLctA: TPanel
        Left = 0
        Top = 0
        Width = 808
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object BtLocaliza1: TBitBtn
          Tag = 22
          Left = 8
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Localizar'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtLocaliza1Click
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 100
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 192
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtAlteraClick
        end
        object BtCopiar: TBitBtn
          Tag = 56
          Left = 284
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Copiar'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          Visible = False
          OnClick = BtAlteraClick
        end
      end
    end
    object BtAcao: TBitBtn
      Tag = 14
      Left = 378
      Top = 19
      Width = 90
      Height = 40
      Caption = '&A'#231#227'o'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtAcaoClick
    end
  end
  object DsLoc: TDataSource
    DataSet = QrLoc
    Left = 188
    Top = 72
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLocAfterOpen
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, '
      'co.Nome NOMEGENERO, ca.Nome NOMECARTEIRA'
      'FROM lan-ctos la, Contas co, Carteiras ca'
      'WHERE co.Codigo=la.Genero'
      'AND ca.Codigo=la.Carteira'
      '')
    Left = 158
    Top = 72
    object QrLocMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLocAno: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Ano'
    end
    object QrLocData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLocTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrLocCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrLocSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrLocAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrLocGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLocDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLocNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrLocDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLocDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000;-000000; '
    end
    object QrLocSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLocVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLocLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLocFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLocID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLocFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrLocBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLocLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLocCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLocLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLocOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLocLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLocPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLocFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLocCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLocMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLocMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLocProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLocDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLocUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLocDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLocNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrLocNOMEGENERO: TWideStringField
      FieldName = 'NOMEGENERO'
      Required = True
      Size = 50
    end
    object QrLocNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Required = True
      Size = 100
    end
    object QrLocMENSAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 100
      Calculated = True
    end
    object QrLocVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLocAccount: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Account'
      Calculated = True
    end
    object QrLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLocID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLocMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLocCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrLocFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLocICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrLocICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrLocDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLocCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLocDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLocDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLocForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLocQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrLocEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrLocAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrLocContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrLocCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrLocDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrLocNFVal: TFloatField
      FieldName = 'NFVal'
    end
    object QrLocDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrLocAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrLocUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrLocFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLocCLIENTE_INTERNO: TIntegerField
      FieldName = 'CLIENTE_INTERNO'
    end
    object QrLocNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrLocTerceiro: TFloatField
      FieldName = 'Terceiro'
    end
  end
  object QrLct: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data FROM lan-ctos')
    Left = 232
    Top = 264
    object QrLctData: TDateField
      FieldName = 'Data'
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 260
    Top = 264
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 10
    OnTimer = Timer1Timer
    Left = 396
    Top = 140
  end
  object PMAcao: TPopupMenu
    Left = 520
    Top = 432
    object Alterar1: TMenuItem
      Caption = '&Alterar'
      object Contadoplanodecontas1: TMenuItem
        Caption = '&Conta do plano de contas'
        object Selecionados1: TMenuItem
          Caption = '&Selecionados'
          OnClick = Selecionados1Click
        end
        object odos1: TMenuItem
          Caption = '&Todos'
          OnClick = odos1Click
        end
      end
    end
  end
  object QrLLC: TMySQLQuery
    Database = Dmod.MyDB
    Left = 404
    Top = 268
    object QrLLCData: TDateField
      FieldName = 'Data'
    end
    object QrLLCTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLLCCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLLCControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLLCSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLLCGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
end
