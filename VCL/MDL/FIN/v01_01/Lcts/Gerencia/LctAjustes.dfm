object FmLctAjustes: TFmLctAjustes
  Left = 339
  Top = 185
  Caption = 'FIN-AJUST-001 :: Ajustes de Lan'#231'amentos'
  ClientHeight = 582
  ClientWidth = 835
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 835
    Height = 408
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 835
      Height = 319
      ActivePage = TabSheet3
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Compensa'#231#245'es n'#227'o corrigidas '
        object DBGNC: TDBGrid
          Left = 0
          Top = 0
          Width = 827
          Height = 291
          Align = alClient
          DataSource = DsNC
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CtrlQuitPg'
              Title.Caption = 'Ctrl Quit/pg'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Empresa'
              Title.Caption = 'Dono cart'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Carteira'
              Title.Caption = 'ID Carteira'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CART'
              Title.Caption = 'Nome da carteira'
              Width = 355
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Compensados sem atualiza'#231#227'o de m'#234's '
        ImageIndex = 1
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 827
          Height = 291
          Align = alClient
          DataSource = DsAtzMes
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Mez'
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Emiss'#245'es com link de pgto incorreto '
        ImageIndex = 2
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 827
          Height = 291
          Align = alClient
          DataSource = DsPagtos1
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Ctrl_Tip_1'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Pgto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ctrl_Tip_2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CtrlQuitPg'
              Visible = True
            end>
        end
      end
    end
    object Memo1: TMemo
      Left = 0
      Top = 319
      Width = 835
      Height = 89
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 835
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 787
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 739
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 299
        Height = 32
        Caption = 'Ajustes de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 299
        Height = 32
        Caption = 'Ajustes de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 299
        Height = 32
        Caption = 'Ajustes de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 456
    Width = 835
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 831
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 831
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 512
    Width = 835
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 831
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 687
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
    end
    object BtOK: TBitBtn
      Tag = 14
      Left = 15
      Top = 16
      Width = 120
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtOKClick
    end
  end
  object QrNC: TMySQLQuery
    Database = Dmod.MyDB
    Left = 232
    Top = 164
    object QrNCData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNCVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNCControle: TIntegerField
      FieldName = 'Controle'
      DisplayFormat = '0;-0; '
    end
    object QrNCCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
      DisplayFormat = '0;-0; '
    end
    object QrNCEmpresa: TIntegerField
      FieldName = 'Empresa'
      DisplayFormat = '0;-0; '
    end
    object QrNCCarteira: TIntegerField
      FieldName = 'Carteira'
      DisplayFormat = '0;-0; '
    end
    object QrNCNO_CART: TWideStringField
      FieldName = 'NO_CART'
      Size = 100
    end
  end
  object DsNC: TDataSource
    DataSet = QrNC
    Left = 260
    Top = 164
  end
  object QrAtzMes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la1.Controle, la2.Mez '
      'FROM VAR LCT la1'
      'LEFT JOIN VAR_ LCT la2 ON la2.Controle=la1.CtrlIni'
      'WHERE la1.CtrlIni>0'
      'AND la1.Mez = 0'
      'AND la2.Mez <> la1.Mez')
    Left = 232
    Top = 192
    object QrAtzMesControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrAtzMesMez: TIntegerField
      FieldName = 'Mez'
    end
  end
  object DsAtzMes: TDataSource
    DataSet = QrAtzMes
    Left = 260
    Top = 192
  end
  object QrPagtos1: TMySQLQuery
    SQL.Strings = (
      'DROP TABLE IF EXISTS _TESTE_TIPO1_;'
      'CREATE TABLE _TESTE_TIPO1_'
      'SELECT Controle Ctrl_Tip_1, ID_Pgto'
      'FROM syndic.lct0001a'
      'WHERE Tipo=1'
      'AND ID_Pgto > 0;'
      ''
      'DROP TABLE IF EXISTS _TESTE_TIPO2_;'
      'CREATE TABLE _TESTE_TIPO2_'
      'SELECT Controle Ctrl_Tip_2, CtrlQuitPg'
      'FROM syndic.lct0001a'
      'WHERE Tipo=2;'
      ''
      'SELECT t1.*, t2.* '
      'FROM _TESTE_TIPO1_ t1, _TESTE_TIPO2_ t2'
      'WHERE t1.ID_Pgto=t2.Ctrl_Tip_2'
      'AND t1.Ctrl_Tip_1<>t2.CtrlQuitPg')
    Left = 232
    Top = 220
    object QrPagtos1Ctrl_Tip_1: TIntegerField
      FieldName = 'Ctrl_Tip_1'
    end
    object QrPagtos1ID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrPagtos1Ctrl_Tip_2: TIntegerField
      FieldName = 'Ctrl_Tip_2'
    end
    object QrPagtos1CtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
  end
  object DsPagtos1: TDataSource
    DataSet = QrPagtos1
    Left = 260
    Top = 220
  end
  object QrLctTipCart: TMySQLQuery
    Database = Dmod.MyDB
    Left = 232
    Top = 248
    object QrLctTipCartTipoA: TSmallintField
      FieldName = 'TipoA'
    end
    object QrLctTipCartTipoB: TIntegerField
      FieldName = 'TipoB'
      Required = True
    end
    object QrLctTipCartData: TDateField
      FieldName = 'Data'
    end
    object QrLctTipCartCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctTipCartControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctTipCartSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLctTipCartCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctTipCartDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object DsLctTipCart: TDataSource
    DataSet = QrLctTipCart
    Left = 260
    Top = 248
  end
end
