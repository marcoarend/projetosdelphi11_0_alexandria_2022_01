unit PreLctCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, dmkDBGridZTO,
  frxClass, frxDBSet;

type
  TFmPreLctCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrPreLctCab: TmySQLQuery;
    DsPreLctCab: TDataSource;
    QrPreLctIts: TmySQLQuery;
    DsPreLctIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrPreLctCabCodigo: TIntegerField;
    QrPreLctCabNome: TWideStringField;
    QrPreLctCabCliInt: TIntegerField;
    QrPreLctCabNO_CliInt: TWideStringField;
    QrPreLctItsCodigo: TIntegerField;
    QrPreLctItsControle: TIntegerField;
    QrPreLctItsCarteira: TIntegerField;
    QrPreLctItsGenero: TIntegerField;
    QrPreLctItsCliInt: TIntegerField;
    QrPreLctItsCliente: TIntegerField;
    QrPreLctItsFornecedor: TIntegerField;
    QrPreLctItsMez: TIntegerField;
    QrPreLctItsDebito: TFloatField;
    QrPreLctItsCredito: TFloatField;
    QrPreLctItsDescricao: TWideStringField;
    QrPreLctItsLk: TIntegerField;
    QrPreLctItsDataCad: TDateField;
    QrPreLctItsDataAlt: TDateField;
    QrPreLctItsUserCad: TIntegerField;
    QrPreLctItsUserAlt: TIntegerField;
    QrPreLctItsAlterWeb: TSmallintField;
    QrPreLctItsAtivo: TSmallintField;
    QrPreLctItsNO_Carteira: TWideStringField;
    QrPreLctItsNOMECONTA: TWideStringField;
    QrPreLctItsNOMEEMPRESA: TWideStringField;
    QrPreLctItsNOMECLIENTE: TWideStringField;
    QrPreLctItsNOMEFORNECEDOR: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrPreLctItsDiaVencto: TIntegerField;
    N1: TMenuItem;
    Importa1: TMenuItem;
    QrPreLctItsNOMETERCEIRO: TWideStringField;
    BtLanca: TBitBtn;
    DGDados: TDBGrid;
    frxFIN_PRELC_001: TfrxReport;
    frxDsPreLctIts: TfrxDBDataset;
    QrPreLctItsVALOR: TFloatField;
    Panel6: TPanel;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    EdTerceiro: TEdit;
    EdValor: TdmkEdit;
    CkValor: TCheckBox;
    CkTerceiro: TCheckBox;
    RGDebito: TRadioButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPreLctCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPreLctCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrPreLctCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrPreLctCabBeforeClose(DataSet: TDataSet);
    procedure Importa1Click(Sender: TObject);
    procedure BtLancaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxFIN_PRELC_001GetValue(const VarName: string;
      var Value: Variant);
    procedure CkTerceiroClick(Sender: TObject);
  private
    FReabrindo: Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraFormPreLctIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenPreLctIts(Filtra: Boolean; Controle: Integer; OrderBy: String);

  end;

var
  FmPreLctCab: TFmPreLctCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, PreLctIts, ModuleGeral,
PreLctLoa, PreLctExe;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPreLctCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPreLctCab.MostraFormPreLctIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmPreLctIts, FmPreLctIts, afmoNegarComAviso) then
  begin
    FmPreLctIts.ImgTipo.SQLType := SQLType;
    FmPreLctIts.FQrCab := QrPreLctCab;
    FmPreLctIts.FDsCab := DsPreLctCab;
    FmPreLctIts.FQrIts := QrPreLctIts;
    UnDmkDAC_PF.AbreMySQLQuery0(FmPreLctIts.QrCarteiras, Dmod.MyDB, [
    'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, ',
    'Tipo, ExigeNumCheque, ForneceI, Banco1, UsaTalao ',
    'FROM carteiras ',
    'WHERE ForneceI=' + Geral.FF0(QrPreLctCabCliInt.Value),
    'ORDER BY Nome ',
    '']);
    //
    if SQLType = stIns then
      //
    else
    begin
      FmPreLctIts.EdControle.ValueVariant := QrPreLctItsControle.Value;
      FmPreLctIts.FCliInt := QrPreLctCabCliInt.Value;
      //
      FmPreLctIts.EdControle.ValueVariant   := QrPreLctItsControle.Value;
      FmPreLctIts.EdCarteira.ValueVariant   := QrPreLctItsCarteira.Value;
      FmPreLctIts.CBCarteira.KeyValue       := QrPreLctItsCarteira.Value;
      FmPreLctIts.EdGenero.ValueVariant     := QrPreLctItsGenero.Value;
      FmPreLctIts.CBGenero.KeyValue         := QrPreLctItsGenero.Value;
      FmPreLctIts.EdCliente.ValueVariant    := QrPreLctItsCliente.Value;
      FmPreLctIts.CBCliente.KeyValue        := QrPreLctItsCliente.Value;
      FmPreLctIts.EdFornecedor.ValueVariant := QrPreLctItsFornecedor.Value;
      FmPreLctIts.EdDebito.ValueVariant     := QrPreLctItsDebito.Value;
      FmPreLctIts.EdCredito.ValueVariant    := QrPreLctItsCredito.Value;
      FmPreLctIts.EdDescricao.ValueVariant  := QrPreLctItsDescricao.Value;
      FmPreLctIts.RGDiaVencto.ItemIndex     := QrPreLctItsDiaVencto.Value - 1;
      FmPreLctIts.RGMez.ItemIndex           := QrPreLctItsMez.Value;
    end;
    FmPreLctIts.ShowModal;
    FmPreLctIts.Destroy;
  end;
end;

procedure TFmPreLctCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrPreLctCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrPreLctCab, QrPreLctIts);
end;

procedure TFmPreLctCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrPreLctCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrPreLctIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrPreLctIts);
end;

procedure TFmPreLctCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPreLctCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPreLctCab.DefParams;
begin
  VAR_GOTOTABELA := 'prelctcab';
  VAR_GOTOMYSQLTABLE := QrPreLctCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT plc.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CliInt');
  VAR_SQLx.Add('FROM prelctcab plc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=plc.CliInt');
  //
  VAR_SQL1.Add('WHERE plc.Codigo=:P0');
  //
  //VAR_SQL2.Add('WHERE plc.CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE plc.Nome Like :P0');
  //
end;

procedure TFmPreLctCab.Importa1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPreLctLoa, FmPreLctLoa, afmoNegarComAviso) then
  begin
    FmPreLctLoa.FCodigo := QrPreLctCabCodigo.Value;
    FmPreLctLoa.ShowModal;
    FmPreLctLoa.Destroy;
    //
    ReopenPreLctIts(False, 0, 'la.Controle');
  end;
end;

procedure TFmPreLctCab.ItsAltera1Click(Sender: TObject);
begin
  MostraFormPreLctIts(stUpd);
end;

procedure TFmPreLctCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmPreLctCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPreLctCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPreLctCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'prelctits', 'Controle', QrPreLctItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrPreLctIts,
      QrPreLctItsControle, QrPreLctItsControle.Value);
    ReopenPreLctIts(True, Controle, 'la.Controle');
  end;
end;

procedure TFmPreLctCab.ReopenPreLctIts(Filtra: Boolean; Controle: Integer; OrderBy: String);
var
  ValTxt, SQL_Ent, SQL_Val: String;
begin
  if FReabrindo then
    Exit;
  //
  FReabrindo := True;
  try
    if not Filtra then
    begin
       CkTerceiro.Checked := False;
       CkValor.Checked := False;
    end;
    if CkTerceiro.Checked then
      SQL_Ent :=
      'AND (IF(Cliente<>0, IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome), ' + sLineBreak +
      'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome)) LIKE "%' + EdTerceiro.Text + '%" ' + sLineBreak +
      'OR IF(Cliente<>0, IF(cl.Tipo=0, cl.Fantasia, cl.Apelido), ' + sLineBreak +
      'IF(fo.Tipo=0, fo.Fantasia, fo.Apelido)) LIKE "%' + EdTerceiro.Text + '%") '
    else
      SQL_Ent := '';
    //
    ValTxt := Geral.FFT_Dot(EdValor.ValueVariant, 2, siNegativo);
    if CkValor.Checked then
    begin
      if RGDebito.Checked then
        SQL_Val := 'AND la.Debito=' + ValTxt
      else
        SQL_Val := 'AND la.Credito=' + ValTxt
    end else
      SQL_Val := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPreLctIts, Dmod.MyDB, [
    'SELECT la.*, ca.Nome NO_Carteira, ',
    'la.Credito - la.Debito VALOR, ',
    'ct.Nome NOMECONTA,  ',
    'IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA, ',
    'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE, ',
    'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR, ',
    'IF(Cliente<>0, IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome), ',
    'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome)) NOMETERCEIRO ',
    'FROM prelctits la ',
    'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
    'LEFT JOIN contas ct    ON ct.Codigo=la.Genero  ',
    'LEFT JOIN entidades em ON em.Codigo=ct.Empresa ',
    'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente ',
    'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor ',
    'WHERE la.Codigo=' + Geral.FF0(QrPreLctCabCodigo.Value),
    SQL_Ent,
    SQL_Val,
    'ORDER BY ' + OrderBy,
    '']);
    //
    QrPreLctIts.Locate('Controle', Controle, []);
  finally
    FReabrindo := False;
  end;
end;

procedure TFmPreLctCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPreLctCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPreLctCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPreLctCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPreLctCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPreLctCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPreLctCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPreLctCabCodigo.Value;
  Close;
end;

procedure TFmPreLctCab.ItsInclui1Click(Sender: TObject);
begin
  MostraFormPreLctIts(stIns);
end;

procedure TFmPreLctCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrPreLctCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'prelctcab');
  EdEmpresa.Enabled := QrPreLctIts.RecordCount = 0;
  CBEmpresa.Enabled := QrPreLctIts.RecordCount = 0;
end;

procedure TFmPreLctCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, CliInt: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, CliInt) then Exit;
  //
  Codigo := UMyMod.BPGS1I32('prelctcab', 'Codigo', '', '', tsPos,
    ImgTipo.SQLType, QrPreLctCabCodigo.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'prelctcab', False, [
  'Nome', 'CliInt'], [
  'Codigo'], [
  Nome, CliInt], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmPreLctCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'prelctcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'prelctcab', 'Codigo');
end;

procedure TFmPreLctCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmPreLctCab.BtLancaClick(Sender: TObject);
var
  Empresa: Integer;
begin
  if DBCheck.CriaFm(TFmPreLctExe, FmPreLctExe, afmoNegarComAviso) then
  begin
    Empresa := DModG.ObtemFilialDeEntidade(QrPreLctCabCliInt.Value);
    FmPreLctExe.QrPreLctIts.SQL.Text := QrPreLctIts.SQL.Text;
    UnDmkDAC_PF.AbreQuery(FmPreLctExe.QrPreLctIts, Dmod.MyDB);
    FmPreLctExe.FCodigo := QrPreLctCabCodigo.Value;
    FmPreLctExe.FCliInt := QrPreLctCabCliInt.Value;
    FmPreLctExe.FTabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, Empresa);

    FmPreLctExe.ShowModal;
    FmPreLctExe.Destroy;
  end;
end;

procedure TFmPreLctCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmPreLctCab.FormCreate(Sender: TObject);
begin
  FReabrindo := False;
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmPreLctCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPreLctCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPreLctCab.SbImprimeClick(Sender: TObject);
begin
  ReopenPreLctIts(True, 0, 'la.Carteira, la.Controle');
  MyObjects.frxMostra(frxFIN_PRELC_001, LaTitulo1A.Caption);
  ReopenPreLctIts(True, 0, 'la.Controle');
end;

procedure TFmPreLctCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPreLctCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPreLctCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPreLctCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPreLctCab.QrPreLctCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPreLctCab.QrPreLctCabAfterScroll(DataSet: TDataSet);
begin
  ReopenPreLctIts(False, 0, 'la.Controle');
end;

procedure TFmPreLctCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrPreLctCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmPreLctCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPreLctCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'prelctcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPreLctCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPreLctCab.frxFIN_PRELC_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_TITULO' then
    Value := LaTitulo1A.Caption
  else
end;

procedure TFmPreLctCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrPreLctCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'prelctcab');
  EdEmpresa.Enabled := True;
  CBEmpresa.Enabled := True;
end;

procedure TFmPreLctCab.CkTerceiroClick(Sender: TObject);
begin
  ReopenPreLctIts(True, QrPreLctItsControle.Value, 'la.Controle');
end;

procedure TFmPreLctCab.QrPreLctCabBeforeClose(
  DataSet: TDataSet);
begin
  QrPreLctIts.Close;
end;

procedure TFmPreLctCab.QrPreLctCabBeforeOpen(DataSet: TDataSet);
begin
  QrPreLctCabCodigo.DisplayFormat := FFormatFloat;
end;

end.


