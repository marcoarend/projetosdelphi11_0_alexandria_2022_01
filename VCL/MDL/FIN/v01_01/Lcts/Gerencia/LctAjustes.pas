unit LctAjustes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, ComCtrls,
  DB, mySQLDbTables, dmkGeral, dmkImage, DmkDAC_PF, UnDmkEnums;

type
  TFmLctAjustes = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGNC: TDBGrid;
    QrNC: TmySQLQuery;
    DsNC: TDataSource;
    QrNCData: TDateField;
    QrNCVencimento: TDateField;
    QrNCControle: TIntegerField;
    QrNCCtrlQuitPg: TIntegerField;
    QrNCEmpresa: TIntegerField;
    QrNCCarteira: TIntegerField;
    QrNCNO_CART: TWideStringField;
    QrAtzMes: TmySQLQuery;
    QrAtzMesControle: TIntegerField;
    QrAtzMesMez: TIntegerField;
    Memo1: TMemo;
    TabSheet2: TTabSheet;
    DsAtzMes: TDataSource;
    DBGrid1: TDBGrid;
    QrPagtos1: TmySQLQuery;
    DsPagtos1: TDataSource;
    QrPagtos1Ctrl_Tip_1: TIntegerField;
    QrPagtos1ID_Pgto: TIntegerField;
    QrPagtos1Ctrl_Tip_2: TIntegerField;
    QrPagtos1CtrlQuitPg: TIntegerField;
    TabSheet3: TTabSheet;
    DBGrid2: TDBGrid;
    QrLctTipCart: TmySQLQuery;
    QrLctTipCartTipoA: TSmallintField;
    QrLctTipCartTipoB: TIntegerField;
    QrLctTipCartData: TDateField;
    QrLctTipCartCarteira: TIntegerField;
    QrLctTipCartControle: TIntegerField;
    QrLctTipCartSub: TSmallintField;
    QrLctTipCartCredito: TFloatField;
    QrLctTipCartDebito: TFloatField;
    DsLctTipCart: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    ////////////////////////////////////////////////////////////////////////////
    //// N�o tirar daqui! Usar a procedure DefineTabLctA(... ///////////////////
    (**) FTabLctA: String; /////////////////////////////////////////////////////
    //// Fim n�o tirar daqui! //////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    ///
    FResult: Integer;
    procedure AtualizaMensalDeCompensados();
    procedure CorrigePagamentos();
    procedure CorrigeTipoDeCarteira();
    procedure AtualizaStatusDePagamentoParcial();
  public
    { Public declarations }
    procedure DefineTabLctA(TabLctA: String);
    function  ErrosEncontrados(): Integer;
  end;

  var
  FmLctAjustes: TFmLctAjustes;

implementation

uses UnMyObjects, Module, UnFinanceiro, ModuleGeral, ModuleFin;

{$R *.DFM}

procedure TFmLctAjustes.AtualizaMensalDeCompensados();
var
  Atualizados: Integer;
begin
  Screen.Cursor := crHourGlass;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando mensal de compensados');
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctA + ' SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('Mez=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  Dmod.QrUpd.SQL.Add('');
  //
  QrAtzMes.Close;
  QrAtzMes.SQL.Clear;
{
SELECT la1.Controle, la2.Mez
FROM VAR LCT la1
LEFT JOIN VAR_ LCT la2 ON la2.Controle=la1.CtrlIni
WHERE la1.CtrlIni>0
AND la1.Mez = 0
AND la2.Mez <> la1.Mez
}
  QrAtzMes.SQL.Add('SELECT la1.Controle, la2.Mez');
  QrAtzMes.SQL.Add('FROM ' + FTabLctA + ' la1');
  QrAtzMes.SQL.Add('LEFT JOIN ' + FTabLctA + ' la2 ON la2.Controle=la1.CtrlIni');
  QrAtzMes.SQL.Add('WHERE la1.CtrlIni>0');
  QrAtzMes.SQL.Add('AND la1.Mez = 0');
  QrAtzMes.SQL.Add('AND la2.Mez <> la1.Mez');
  UnDmkDAC_PF.AbreQuery(QrAtzMes, Dmod.MyDB);
  //
  PB1.Position := 0;
  PB1.Visible := True;
  PB1.Max := QrAtzMes.RecordCount;
  QrAtzMes.First;
  while not QrAtzMes.Eof do
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando mensal de compensados.' +
    IntToStr(QrAtzMes.RecNo) + ' de ' + IntToStr(QrAtzMes.RecordCount));
    //
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
    //
    Dmod.QrUpd.Params[00].AsInteger := QrAtzMesMez.Value;
    Dmod.QrUpd.Params[01].AsInteger := QrAtzMesControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrAtzMes.Next;
  end;
  Atualizados := QrAtzMes.RecordCount;
  QrAtzMes.Close;
  UnDmkDAC_PF.AbreQuery(QrAtzMes, Dmod.MyDB);
  Atualizados := Atualizados - QrAtzMes.RecordCount;
  if Atualizados > 0 then
    Memo1.Lines.Add(IntToStr(Atualizados) + ' lan�amentos compensados ' +
      'tiveram seu m�s corrigido!');
  PB1.Visible := False;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  Screen.Cursor := crDefault;
end;

procedure TFmLctAjustes.AtualizaStatusDePagamentoParcial;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM lct0001a ',
    'WHERE Sit = 1 ',
    '']);
  if DModG.QrAux.RecordCount > 0 then
  begin
    DModG.QrAux.First;
    while not DModG.QrAux.Eof do
    begin
      UFinanceiro.AtualizaEmissaoMasterExtra_Novo(
        DModG.QrAux.FieldByName('Controle').AsInteger, DModG.QrAux, nil, nil, nil, nil, '0',
        FTabLctA);
      //
      DModG.QrAux.Next;
    end;
  end;
end;

procedure TFmLctAjustes.BtOKClick(Sender: TObject);
var
  Corrigidos, NaoCorrigidos: Integer;
begin
  FResult := 0;
///
////////////////////////////////////////////////////////////////////////////////
///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo situa��o dos lan�amentos');
  Screen.Cursor := crHourGlass;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctA + ' SET AlterWeb=1, Sit=0 WHERE Sit > 4 AND Tipo=2');
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctA + ' SET AlterWeb=1, Sit=2 WHERE Sit > 4 AND Tipo<>2');
  Dmod.QrUpd.ExecSQL;
  //
  Screen.Cursor := crDefault;
///
////////////////////////////////////////////////////////////////////////////////
///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando pagamentos � vista');
  UFinanceiro.AtualizaPagamentosAVista(nil, FTabLctA);
///
////////////////////////////////////////////////////////////////////////////////
///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Cancelando pagamentos de lan�amentos abertos');
  Screen.Cursor := crHourGlass;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctA + ' SET Pago=0 WHERE Sit=0');
  Dmod.QrUpd.ExecSQL;
  //
  Screen.Cursor := crDefault;
///
//////////////////////////////////////////////////////////////////////
///
{ // Parei Aqui! ver o que fazer!
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Limpando parcelamentos exclu�dos');
  Screen.Cursor := crHourGlass;
  //
  if DBCheck.CriaFm(TFmBloqParcDel, FmBloqParcDel, afmoSoBoss) then
  begin
    FmBloqParcDel.ShowModal;
    FmBloqParcDel.Destroy;
  end;
  //
  Screen.Cursor := crDefault;
}
///
//////////////////////////////////////////////////////////////////////
///
//////////////////////////////////////////////////////////////////////

{
Corrige compensados sem data de compensa��o
N�o precisa! � parecido ao VerificaID_Pgto_x_Compensado logo abaixo!
procedure TFmPrincipal.Corrigedatadequitadosquandocompensado01Click(
  Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCorrigeCompensados, FmCorrigeCompensados, afmoSoBoss) then
  begin
    FmCorrigeCompensados.ShowModal;
    FmCorrigeCompensados.Destroy;
  end;
end;
}

  QrNC.Close;
  QrNC.SQL.Clear;
  QrNC.SQL.Add('SELECT lct.Data, lct.Vencimento, lct.Controle, lct.CtrlQuitPg,');
  QrNC.SQL.Add('car.ForneceI Empresa, lct.Carteira, car.Nome NO_CART');
  QrNC.SQL.Add('FROM ' + FTabLctA + ' lct');
  QrNC.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrNC.SQL.Add('WHERE lct.Sit in (2,3)');
  QrNC.SQL.Add('AND lct.Compensado < 2');
  QrNC.SQL.Add('AND car.Tipo=2');
  QrNC.SQL.Add('ORDER BY NO_CART, lct.Data');
  QrNC.SQL.Add('');
  UFinanceiro.VerificaID_Pgto_x_Compensado(FTabLctA, LaAviso1, LaAviso2, PB1,
    QrNC, Corrigidos, NaoCorrigidos);
///
////////////////////////////////////////////////////////////////////////////////
///
  AtualizaMensalDeCompensados();
///
////////////////////////////////////////////////////////////////////////////////
///
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo m�s de compet�ncia de extratos quando o m�s � igual a 1');
  DModG.CorrigeMesDeCompetenciaDeExtratos(PB1, FTabLctA);
///
////////////////////////////////////////////////////////////////////////////////
///
{
Ver o que fazer! � demorado e talvez n�o precise
Corrige pagamentos
procedure TFmPrincipal.CorrigirPagamentos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCorrigePagtos, FmCorrigePagtos, afmoSoMaster) then
  begin
    FmCorrigePagtos.ShowModal;
    FmCorrigePagtos.Destroy;
  end;
end;
}
  CorrigePagamentos();
///
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
{ N�o tem como

Corrige controles duplicados
procedure TFmPrincipal.CorrigeControlesduplicados1Click(Sender: TObject);
begin
  DModG.CorrigeDuplicacaoDeLctControle(VAR LCT, LaAviso, PB1);
end;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

{
precisa fazer?
Acho que pode gerar mais erros do que tem!
Recalcula todos pagamentos
procedure TFmPrincipal.Recalculatodospagamentos1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Label4.Visible := True;
  QrPg.Close;
  UnDmkDAC_PF.AbreQuery(QrPg, Dmod.MyDB);
  PB1.Position := 0;
  PB1.Max := QrPg.RecordCount;
  while not QrPg.Eof do
  begin
    UFinanceiro.AtualizaEmissaoMasterRapida(QrPgID_Pgto.Value, 3,
      QrPgControle.Value);
    //
    PB1.Position := PB1.Position + 1;
    Label4.Caption := IntToStr(PB1.Position);
    Update;
    Application.ProcessMessages;
    QrPg.Next;
  end;
  Screen.Cursor := crDefault;
end;
}
  CorrigeTipoDeCarteira();

  AtualizaStatusDePagamentoParcial();

  if ErrosEncontrados() > 0 then
    BtSaida.Visible := True
  else
    Close;
end;

procedure TFmLctAjustes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctAjustes.CorrigePagamentos();
const
  Step = 10;
var
  Conta, Total: Integer;
  Texto: String;
begin
  Screen.Cursor := crHourGlass;
  QrPagtos1.Close;
  QrPagtos1.SQL.Clear;
  QrPagtos1.SQL.Add('DROP TABLE IF EXISTS _TESTE_TIPO1_;');
  QrPagtos1.SQL.Add('CREATE TABLE _TESTE_TIPO1_');
  QrPagtos1.SQL.Add('SELECT Controle Ctrl_Tip_1, ID_Pgto,');
  QrPagtos1.SQL.Add('Credito Cred1, Debito Debi1');
  QrPagtos1.SQL.Add('FROM ' + FTabLctA);
  QrPagtos1.SQL.Add('WHERE Tipo=1');
  QrPagtos1.SQL.Add('AND ID_Pgto > 0;');
  QrPagtos1.SQL.Add('');
  QrPagtos1.SQL.Add('DROP TABLE IF EXISTS _TESTE_TIPO2_;');
  QrPagtos1.SQL.Add('CREATE TABLE _TESTE_TIPO2_');
  QrPagtos1.SQL.Add('SELECT Controle Ctrl_Tip_2, CtrlQuitPg,');
  QrPagtos1.SQL.Add('Credito Cred2, Debito Debi2');
  QrPagtos1.SQL.Add('FROM ' + FTabLctA);
  QrPagtos1.SQL.Add('WHERE Tipo=2;');
  QrPagtos1.SQL.Add('');
  QrPagtos1.SQL.Add('SELECT t1.*, t2.*');
  QrPagtos1.SQL.Add('FROM _TESTE_TIPO1_ t1, _TESTE_TIPO2_ t2');
  QrPagtos1.SQL.Add('WHERE t1.ID_Pgto=t2.Ctrl_Tip_2');
  QrPagtos1.SQL.Add('AND t1.Ctrl_Tip_1<>t2.CtrlQuitPg');
  QrPagtos1.SQL.Add('AND t1.Cred1=t2.Cred2');
  QrPagtos1.SQL.Add('AND t1.Debi1=t2.Debi2');
  UnDmkDAC_PF.AbreQuery(QrPagtos1, DModG.MyPID_DB);
{
DROP TABLE IF EXISTS _TESTE_TIPO1_;
CREATE TABLE _TESTE_TIPO1_
SELECT Controle Ctrl_Tip_1, ID_Pgto
FROM syndic.lct 0001a
WHERE Tipo=1
AND ID_Pgto > 0;

DROP TABLE IF EXISTS _TESTE_TIPO2_;
CREATE TABLE _TESTE_TIPO2_
SELECT Controle Ctrl_Tip_2, CtrlQuitPg
FROM syndic.lct 0001a
WHERE Tipo=2;

SELECT t1.*, t2.*
FROM _TESTE_TIPO1_ t1, _TESTE_TIPO2_ t2
WHERE t1.ID_Pgto=t2.Ctrl_Tip_2
AND t1.Ctrl_Tip_1<>t2.CtrlQuitPg
}
  //
  PB1.Position := 0;
  Total := QrPagtos1.RecordCount;
  if Total = 0 then Total := 1;
  //
  {@2
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET CtrlQuitPg=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  }
  PB1.Max := Total;
  Conta := 0;
  Texto := '';
  while not QrPagtos1.Eof do
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo pagamento ' +
      IntToStr(QrPagtos1.RecNo) + ' de ' + IntToStr(Total));
    //
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['CtrlQuitPg'], [
    'Controle'], [QrPagtos1Ctrl_Tip_1.Value], [QrPagtos1Ctrl_Tip_2.Value], True,
    '', FTabLctA);
    {}
    //
    QrPagtos1.Next;
  end;
  PB1.Position := PB1.Position + Conta;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  Screen.Cursor := crDefault;
end;

procedure TFmLctAjustes.CorrigeTipoDeCarteira();
var
  Data: String;
  TipoA, TipoB, Carteira, Controle, Sub: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    PB1.Position := 0;
    QrLctTipCart.Close;
    QrLctTipCart.SQL.Clear;
    QrLctTipCart.SQL.Add('SELECT lct.Tipo TipoA, crt.Tipo TipoB, lct.Data,');
    QrLctTipCart.SQL.Add('lct.Carteira, lct.Controle, lct.Sub,');
    QrLctTipCart.SQL.Add('lct.Credito, lct.Debito');
    QrLctTipCart.SQL.Add('FROM ' + FTabLctA + ' lct');
    QrLctTipCart.SQL.Add('LEFT JOIN carteiras crt ON crt.Codigo=lct.Carteira');
    QrLctTipCart.SQL.Add('WHERE crt.Tipo<>lct.Tipo');
    QrLctTipCart.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(QrLctTipCart, Dmod.MyDB);
    if QrLctTipCart.RecordCount > 0 then
    begin
      PageControl1.ActivePageIndex := 7;
      PB1.Max := QrLctTipCart.RecordCount;
      while not QrLctTipCart.Eof do
      begin
        PB1.Position := PB1.Position + 1;
        //
        Data      := Geral.FDT(QrLctTipCartData.Value, 1);
        TipoA     := QrLctTipCartTipoA.Value;
        TipoB     := QrLctTipCartTipoB.Value;
        Carteira  := QrLctTipCartCarteira.Value;
        Controle  := QrLctTipCartControle.Value;
        Sub       := QrLctTipCartSub.Value;
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Tipo'], ['Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
        TipoB], [Data, TipoA, Carteira, Controle, Sub], True, '', FTabLctA);
        //
        QrLctTipCart.Next;
      end;
      Memo1.Lines.Add(IntToStr(PB1.Max ) +
      ' lan�amentos com problemas foram encontrados e corrigidos. ');
    end;
  finally
    PB1.Position := 0;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctAjustes.DefineTabLctA(TabLctA: String);
begin
  // 2012-01-01
  FTabLctA := DModFin.CorrigeTabXxxY(TabLctA);
  // Fim 2012-01-01
end;

function TFmLctAjustes.ErrosEncontrados(): Integer;
begin
  Result := FResult;
  if QrNC.State = dsBrowse then
    Result := Result + QrNC.RecordCount;
  if QrAtzMes.State = dsBrowse then
    Result := Result + QrAtzMes.RecordCount;
end;

procedure TFmLctAjustes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctAjustes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FResult := 1;
  QrPagtos1.Database := DModG.MyPID_DB;
end;

procedure TFmLctAjustes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
