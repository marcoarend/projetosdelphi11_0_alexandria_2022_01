object FmLocDefs: TFmLocDefs
  Left = 334
  Top = 177
  Caption = '???-?????-999 :: Defini'#231#245'es para Localiza'#231#227'o de Lan'#231'amentos'
  ClientHeight = 644
  ClientWidth = 741
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelControle: TPanel
    Left = 0
    Top = 595
    Width = 741
    Height = 49
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 0
    ExplicitTop = 628
    ExplicitWidth = 865
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 20
      Top = 5
      Width = 110
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel1: TPanel
      Left = 581
      Top = 1
      Width = 159
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      Caption = 'Panel1'
      TabOrder = 1
      ExplicitLeft = 705
      ExplicitHeight = 57
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 18
        Top = 4
        Width = 111
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 741
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 865
    object GB_R: TGroupBox
      Left = 682
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 806
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 52
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      ExplicitHeight = 59
    end
    object GB_M: TGroupBox
      Left = 52
      Top = 0
      Width = 630
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      ExplicitLeft = 59
      ExplicitWidth = 747
      ExplicitHeight = 59
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 561
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Defini'#231#245'es para Localiza'#231#227'o de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 561
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Defini'#231#245'es para Localiza'#231#227'o de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 561
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Defini'#231#245'es para Localiza'#231#227'o de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 741
    Height = 38
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 59
    ExplicitWidth = 865
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 737
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitTop = 18
      ExplicitWidth = 861
      ExplicitHeight = 34
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 329
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'INFORME SOMENTE OS DADOS QUE TEM CERTEZA!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 329
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'INFORME SOMENTE OS DADOS QUE TEM CERTEZA!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 90
    Width = 741
    Height = 505
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 3
    ExplicitTop = 113
    ExplicitWidth = 865
    ExplicitHeight = 678
    object PainelDados: TPanel
      Left = 2
      Top = 15
      Width = 737
      Height = 488
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitTop = 18
      ExplicitWidth = 861
      ExplicitHeight = 658
      object Bevel1: TBevel
        Left = 30
        Top = 272
        Width = 227
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
      end
      object Bevel2: TBevel
        Left = 266
        Top = 272
        Width = 228
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
      end
      object LaForneceRN: TLabel
        Left = 256
        Top = 182
        Width = 107
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '[F5] Raz'#227'o/Nome'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LaForneceFA: TLabel
        Left = 369
        Top = 182
        Width = 129
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '[F6] Fantasia/Apelido'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 256
        Top = 228
        Width = 107
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '[F5] Raz'#227'o/Nome'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 369
        Top = 228
        Width = 129
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '[F6] Fantasia/Apelido'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object CkDataIni: TCheckBox
        Left = 30
        Top = 4
        Width = 183
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data inicial ou '#250'nica'
        TabOrder = 0
      end
      object CkDataFim: TCheckBox
        Left = 266
        Top = 4
        Width = 119
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data final'
        TabOrder = 2
        OnClick = CkDataFimClick
      end
      object TPDataIni: TdmkEditDateTimePicker
        Left = 30
        Top = 24
        Width = 228
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        CalColors.TextColor = clMenuText
        Date = 37636.777157974500000000
        Time = 37636.777157974500000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPDataFim: TdmkEditDateTimePicker
        Left = 266
        Top = 24
        Width = 229
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 37636.777203761600000000
        Time = 37636.777203761600000000
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object CkConta: TCheckBox
        Left = 30
        Top = 92
        Width = 459
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Conta'
        TabOrder = 8
      end
      object CBConta: TdmkDBLookupComboBox
        Left = 89
        Top = 112
        Width = 405
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPContas
        TabOrder = 10
        dmkEditCB = EdConta
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CkDescricao: TCheckBox
        Left = 30
        Top = 136
        Width = 459
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o parcial ou total (Utilize "%" para m'#225'scara):'
        TabOrder = 11
      end
      object EdDescricao: TEdit
        Left = 30
        Top = 156
        Width = 464
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 12
        Text = '%%'
      end
      object CkDebito: TCheckBox
        Left = 35
        Top = 276
        Width = 189
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'D'#233'bito (m'#237'nimo e m'#225'ximo)'
        TabOrder = 19
      end
      object CkCredito: TCheckBox
        Left = 278
        Top = 276
        Width = 190
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cr'#233'dito (m'#237'nimo e m'#225'ximo)'
        TabOrder = 22
      end
      object CkNF: TCheckBox
        Left = 30
        Top = 326
        Width = 60
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N.F.'
        TabOrder = 25
      end
      object CkDoc: TCheckBox
        Left = 226
        Top = 326
        Width = 95
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Doc. (Cheq.)'
        TabOrder = 29
      end
      object CkControle: TCheckBox
        Left = 326
        Top = 326
        Width = 75
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Controle'
        TabOrder = 31
      end
      object EdDebMin: TdmkEdit
        Left = 35
        Top = 296
        Width = 94
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 20
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdDebMinExit
      end
      object EdCredMin: TdmkEdit
        Left = 278
        Top = 296
        Width = 94
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 23
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdCredMinExit
      end
      object EdNF: TdmkEdit
        Left = 30
        Top = 348
        Width = 93
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 26
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdDoc: TdmkEdit
        Left = 226
        Top = 348
        Width = 94
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 30
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdControle: TdmkEdit
        Left = 326
        Top = 348
        Width = 168
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 32
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object RgOrdem: TRadioGroup
        Left = 516
        Top = 2
        Width = 213
        Height = 179
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Ordem dos resultados: '
        ItemIndex = 0
        Items.Strings = (
          'Data, Controle'
          'Controle'
          'Nota Fiscal'
          'Documento'
          'Descri'#231#227'o'
          'D'#233'bito, Cr'#233'dito'
          'Cr'#233'dito, D'#233'bito')
        TabOrder = 37
      end
      object EdDuplicata: TdmkEdit
        Left = 128
        Top = 348
        Width = 94
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 28
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkDuplicata: TCheckBox
        Left = 128
        Top = 326
        Width = 90
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Duplicata'
        TabOrder = 27
      end
      object EdDebMax: TdmkEdit
        Left = 136
        Top = 296
        Width = 94
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 21
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCredMax: TdmkEdit
        Left = 377
        Top = 296
        Width = 93
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 24
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CkExcelGru: TCheckBox
        Left = 30
        Top = 461
        Width = 468
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Somente lan'#231'amentos sem contas sazonais.'
        TabOrder = 36
      end
      object EdConta: TdmkEditCB
        Left = 30
        Top = 112
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBConta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CGCarteiras: TdmkCheckGroup
        Left = 516
        Top = 184
        Width = 213
        Height = 89
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Carteiras: '
        Items.Strings = (
          'Caixa (esp'#233'cie)'
          'Banco (conta corrente)'
          'Emiss'#227'o banc'#225'ria')
        TabOrder = 38
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object CkCliInt: TCheckBox
        Left = 30
        Top = 372
        Width = 459
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente interno:'
        Enabled = False
        TabOrder = 33
      end
      object EdCliInt: TdmkEditCB
        Left = 30
        Top = 392
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 34
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 89
        Top = 392
        Width = 405
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsPCliInt
        TabOrder = 35
        dmkEditCB = EdCliInt
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CkVctoIni: TCheckBox
        Left = 30
        Top = 48
        Width = 183
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Vencimento inicial ou '#250'nico'
        TabOrder = 4
      end
      object TPVctoIni: TdmkEditDateTimePicker
        Left = 30
        Top = 68
        Width = 228
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        CalColors.TextColor = clMenuText
        Date = 37636.777157974500000000
        Time = 37636.777157974500000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object CkVctoFim: TCheckBox
        Left = 266
        Top = 48
        Width = 119
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Vencimento final'
        TabOrder = 6
        OnClick = CkDataFimClick
      end
      object TPVctoFim: TdmkEditDateTimePicker
        Left = 266
        Top = 68
        Width = 229
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 37636.777203761600000000
        Time = 37636.777203761600000000
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object CBFornece: TdmkDBLookupComboBox
        Left = 89
        Top = 204
        Width = 405
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsPFornece
        TabOrder = 15
        OnKeyDown = CBForneceKeyDown
        dmkEditCB = EdFornece
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdFornece: TdmkEditCB
        Left = 30
        Top = 204
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdForneceKeyDown
        DBLookupComboBox = CBFornece
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CkFornece: TCheckBox
        Left = 30
        Top = 182
        Width = 103
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Fornecedor'
        TabOrder = 13
      end
      object CkCliente: TCheckBox
        Left = 30
        Top = 228
        Width = 103
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente'
        TabOrder = 16
      end
      object EdCliente: TdmkEditCB
        Left = 30
        Top = 248
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdClienteKeyDown
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 89
        Top = 248
        Width = 405
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsPCliente
        TabOrder = 18
        OnKeyDown = CBClienteKeyDown
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object RGTabela: TRadioGroup
        Left = 516
        Top = 362
        Width = 213
        Height = 105
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Arquivo: '
        ItemIndex = 0
        Items.Strings = (
          'A - Ativo'
          'B - Encerrado'
          'D - Morto')
        TabOrder = 39
      end
      object CkCarteira: TCheckBox
        Left = 30
        Top = 416
        Width = 459
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Carteira'
        TabOrder = 40
      end
      object EdCarteira: TdmkEditCB
        Left = 30
        Top = 439
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 41
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCarteira
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCarteira: TdmkDBLookupComboBox
        Left = 89
        Top = 439
        Width = 405
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 42
        dmkEditCB = EdCarteira
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CGContrlCred: TdmkCheckGroup
        Left = 516
        Top = 272
        Width = 213
        Height = 89
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Situa'#231#227'o: '
        Items.Strings = (
          'A Vencer'
          'Vencido'
          'Quitado')
        TabOrder = 43
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
    end
  end
  object QrPContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas'
      'ORDER BY Nome')
    Left = 444
    Top = 188
  end
  object DsPContas: TDataSource
    DataSet = QrPContas
    Left = 444
    Top = 236
  end
  object DsPCliInt: TDataSource
    DataSet = QrPCliInt
    Left = 444
    Top = 332
  end
  object QrPCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 444
    Top = 284
    object QrPCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPCliIntNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object QrPFornece: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 444
    Top = 376
    object QrPForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPForneceNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsPFornece: TDataSource
    DataSet = QrPFornece
    Left = 444
    Top = 420
  end
  object QrPCliente: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 444
    Top = 464
    object QrPClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPClienteNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsPCliente: TDataSource
    DataSet = QrPCliente
    Left = 444
    Top = 512
  end
  object QrCarteiras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM carteiras'
      'ORDER BY Nome')
    Left = 444
    Top = 96
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 100
    end
    object QrCarteirasContato1: TWideStringField
      FieldName = 'Contato1'
      Size = 100
    end
    object QrCarteirasInicial: TFloatField
      FieldName = 'Inicial'
    end
    object QrCarteirasBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCarteirasID: TWideStringField
      FieldName = 'ID'
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteirasID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Size = 50
    end
    object QrCarteirasSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrCarteirasEmCaixa: TFloatField
      FieldName = 'EmCaixa'
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteirasPagRec: TIntegerField
      FieldName = 'PagRec'
    end
    object QrCarteirasDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCarteirasTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCarteirasAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrCarteirasConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCarteirasCheque1: TIntegerField
      FieldName = 'Cheque1'
    end
    object QrCarteirasAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrCarteirasContab: TWideStringField
      FieldName = 'Contab'
    end
    object QrCarteirasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCarteirasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCarteirasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCarteirasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCarteirasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCarteirasOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCarteirasForneceN: TSmallintField
      FieldName = 'ForneceN'
    end
    object QrCarteirasFuturoC: TFloatField
      FieldName = 'FuturoC'
    end
    object QrCarteirasFuturoD: TFloatField
      FieldName = 'FuturoD'
    end
    object QrCarteirasFuturoS: TFloatField
      FieldName = 'FuturoS'
    end
    object QrCarteirasExclusivo: TSmallintField
      FieldName = 'Exclusivo'
    end
    object QrCarteirasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCarteirasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCarteirasRecebeBloq: TSmallintField
      FieldName = 'RecebeBloq'
    end
    object QrCarteirasEntiDent: TIntegerField
      FieldName = 'EntiDent'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 444
    Top = 140
  end
end
