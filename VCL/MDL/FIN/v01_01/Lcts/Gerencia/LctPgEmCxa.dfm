object FmLctPgEmCxa: TFmLctPgEmCxa
  Left = 404
  Top = 197
  Caption = 'FIN-PGTOS-004 :: Mudan'#231'a da Carteira'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 55
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label3: TLabel
      Left = 468
      Top = 8
      Width = 85
      Height = 13
      Caption = 'Data da quita'#231#227'o:'
    end
    object Label2: TLabel
      Left = 584
      Top = 8
      Width = 40
      Height = 13
      Caption = 'Multa %:'
    end
    object Label4: TLabel
      Left = 668
      Top = 8
      Width = 63
      Height = 13
      Caption = 'Juros %/m'#234's:'
    end
    object Label5: TLabel
      Left = 8
      Top = 8
      Width = 133
      Height = 13
      Caption = 'Carteira do caixa recebedor:'
    end
    object EdCarteira: TdmkEditCB
      Left = 8
      Top = 24
      Width = 41
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCarteira
      IgnoraDBLookupComboBox = False
    end
    object CBCarteira: TdmkDBLookupComboBox
      Left = 51
      Top = 24
      Width = 414
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCarteiras
      TabOrder = 1
      dmkEditCB = EdCarteira
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object TPData: TdmkEditDateTimePicker
      Left = 468
      Top = 24
      Width = 112
      Height = 21
      Date = 39411.393054282400000000
      Time = 39411.393054282400000000
      TabOrder = 2
      OnExit = TPDataExit
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object EdMulta: TdmkEdit
      Left = 584
      Top = 24
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdMultaExit
    end
    object EdTaxaM: TdmkEdit
      Left = 668
      Top = 24
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdTaxaMExit
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 103
    Width = 792
    Height = 279
    Align = alClient
    DataSource = DsLctoEdit
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Data'
        Title.Alignment = taCenter
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ValorOri'
        Title.Caption = 'Valor original'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ValorPgt'
        Title.Caption = 'Valor pagto'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'JurosVal'
        Title.Caption = '$ Juros'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MultaVal'
        Title.Caption = '$ Multa'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Vencimento'
        Title.Alignment = taCenter
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descricao'
        Title.Caption = 'Descri'#231#227'o'
        Width = 339
        Visible = True
      end>
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 255
        Height = 32
        Caption = 'Mudan'#231'a da Carteira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 255
        Height = 32
        Caption = 'Mudan'#231'a da Carteira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 255
        Height = 32
        Caption = 'Mudan'#231'a da Carteira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 382
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 426
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 4
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 644
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtCancela: TBitBtn
          Tag = 15
          Left = 10
          Top = 3
          Width = 120
          Height = 40
          Caption = '&Desiste'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtCancelaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 16
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo, ForneceI'
      'FROM carteiras'
      'WHERE Tipo=0'
      'AND ForneceI=:P0'
      'ORDER BY Nome')
    Left = 152
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 180
    Top = 72
  end
  object TbLctoEdit: TmySQLTable
   
    BeforePost = TbLctoEditBeforePost
    AfterPost = TbLctoEditAfterPost
    TableName = 'LctoEdit'
    Left = 312
    Top = 188
    object TbLctoEditControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lctoedit.Controle'
      ReadOnly = True
    end
    object TbLctoEditSub: TIntegerField
      FieldName = 'Sub'
      Origin = 'lctoedit.Sub'
      ReadOnly = True
    end
    object TbLctoEditDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lctoedit.Descricao'
      ReadOnly = True
      Size = 255
    end
    object TbLctoEditData: TDateField
      FieldName = 'Data'
      Origin = 'lctoedit.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbLctoEditVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lctoedit.Vencimento'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yy'
    end
    object TbLctoEditMultaVal: TFloatField
      FieldName = 'MultaVal'
      Origin = 'lctoedit.MultaVal'
      DisplayFormat = '#,###,##0.00'
    end
    object TbLctoEditJurosVal: TFloatField
      FieldName = 'JurosVal'
      Origin = 'lctoedit.JurosVal'
      DisplayFormat = '#,###,##0.00'
    end
    object TbLctoEditValorOri: TFloatField
      FieldName = 'ValorOri'
      Origin = 'lctoedit.ValorOri'
      ReadOnly = True
      DisplayFormat = '#,###,##0.00'
    end
    object TbLctoEditValorPgt: TFloatField
      FieldName = 'ValorPgt'
      Origin = 'lctoedit.ValorPgt'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsLctoEdit: TDataSource
    DataSet = TbLctoEdit
    Left = 340
    Top = 188
  end
  object QrContas1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 448
    Top = 260
    object QrContas1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContas1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas1: TDataSource
    DataSet = QrContas1
    Left = 476
    Top = 260
  end
  object QrContas2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 448
    Top = 289
    object QrContas2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContas2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas2: TDataSource
    DataSet = QrContas2
    Left = 476
    Top = 289
  end
end
