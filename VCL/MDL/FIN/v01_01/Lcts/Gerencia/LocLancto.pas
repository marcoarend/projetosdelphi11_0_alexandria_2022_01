unit LocLancto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, StdCtrls, Buttons, Db, (*DBTables,*)
  UnInternalConsts, UnGOTOy, mySQLDbTables, ComCtrls, dmkGeral, dmkImage,
  UnDmkEnums, dmkEditDateTimePicker, Vcl.Menus, dmkDBGridZTO, Variants;

type
  TFmLocLancto = class(TForm)
    Panel1: TPanel;
    DBGLct: TdmkDBGridZTO;
    DsLoc: TDataSource;
    QrLoc: TmySQLQuery;
    QrLocMes2: TLargeintField;
    QrLocAno: TFloatField;
    QrLocData: TDateField;
    QrLocTipo: TSmallintField;
    QrLocCarteira: TIntegerField;
    QrLocSub: TSmallintField;
    QrLocAutorizacao: TIntegerField;
    QrLocGenero: TIntegerField;
    QrLocDescricao: TWideStringField;
    QrLocNotaFiscal: TIntegerField;
    QrLocDebito: TFloatField;
    QrLocCredito: TFloatField;
    QrLocCompensado: TDateField;
    QrLocDocumento: TFloatField;
    QrLocSit: TIntegerField;
    QrLocVencimento: TDateField;
    QrLocLk: TIntegerField;
    QrLocFatID: TIntegerField;
    QrLocFatParcela: TIntegerField;
    QrLocID_Sub: TSmallintField;
    QrLocFatura: TWideStringField;
    QrLocBanco: TIntegerField;
    QrLocLocal: TIntegerField;
    QrLocCartao: TIntegerField;
    QrLocLinha: TIntegerField;
    QrLocOperCount: TIntegerField;
    QrLocLancto: TIntegerField;
    QrLocPago: TFloatField;
    QrLocFornecedor: TIntegerField;
    QrLocCliente: TIntegerField;
    QrLocMoraDia: TFloatField;
    QrLocMulta: TFloatField;
    QrLocProtesto: TDateField;
    QrLocDataCad: TDateField;
    QrLocDataAlt: TDateField;
    QrLocUserCad: TSmallintField;
    QrLocUserAlt: TSmallintField;
    QrLocDataDoc: TDateField;
    QrLocNivel: TIntegerField;
    QrLocNOMEGENERO: TWideStringField;
    QrLocNOMECARTEIRA: TWideStringField;
    QrLocMENSAL: TWideStringField;
    QrLocVendedor: TIntegerField;
    QrLocAccount: TIntegerField;
    QrLocControle: TIntegerField;
    QrLocID_Pgto: TIntegerField;
    QrLocMez: TIntegerField;
    QrLocCtrlIni: TIntegerField;
    QrLocFatID_Sub: TIntegerField;
    QrLocICMS_P: TFloatField;
    QrLocICMS_V: TFloatField;
    QrLocDuplicata: TWideStringField;
    QrLocCliInt: TIntegerField;
    QrLocDepto: TIntegerField;
    QrLocDescoPor: TIntegerField;
    QrLocForneceI: TIntegerField;
    QrLocQtde: TFloatField;
    QrLocEmitente: TWideStringField;
    QrLocContaCorrente: TWideStringField;
    QrLocCNPJCPF: TWideStringField;
    QrLocDescoVal: TFloatField;
    QrLocNFVal: TFloatField;
    QrLct: TmySQLQuery;
    DsLct: TDataSource;
    QrLctData: TDateField;
    QrLocDescoControle: TIntegerField;
    QrLocAntigo: TWideStringField;
    QrLocUnidade: TIntegerField;
    QrLocFatNum: TFloatField;
    QrLocCLIENTE_INTERNO: TIntegerField;
    QrLocAgencia: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma1: TGroupBox;
    PnConfirmaGB1: TPanel;
    Panel2: TPanel;
    BtSaidaA: TBitBtn;
    BtSoma: TBitBtn;
    PnTabLctA: TPanel;
    BtLocaliza1: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtCopiar: TBitBtn;
    GBConfirma2: TGroupBox;
    PnConfirmaGB2: TPanel;
    BtSaidaB: TBitBtn;
    BtConfirma: TBitBtn;
    Timer1: TTimer;
    BtAcao: TBitBtn;
    PMAcao: TPopupMenu;
    Alterar1: TMenuItem;
    Contadoplanodecontas1: TMenuItem;
    Selecionados1: TMenuItem;
    odos1: TMenuItem;
    PB1: TProgressBar;
    QrLLC: TmySQLQuery;
    QrLLCSub: TSmallintField;
    QrLLCCarteira: TIntegerField;
    QrLLCData: TDateField;
    QrLLCControle: TIntegerField;
    QrLLCTipo: TSmallintField;
    QrLLCGenero: TIntegerField;
    QrLocNO_TERCEIRO: TWideStringField;
    QrLocTerceiro: TFloatField;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtSaidaAClick(Sender: TObject);
    procedure BtLocaliza1Click(Sender: TObject);
    procedure QrLocCalcFields(DataSet: TDataSet);
    procedure DBGLctKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtSomaClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrLocAfterOpen(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure DBGLctDblClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Selecionados1Click(Sender: TObject);
    procedure odos1Click(Sender: TObject);
  private
    { Private declarations }
    //procedure AlteraRegistro(Altera: Integer);
    procedure SomaLinhas;
    function  BtLocalizaTryAgain(Avisa: Boolean): Boolean;
    procedure AlteraConta(Quais: TSelType);

  public
    { Public declarations }
    FTipoData: Integer;
    FDTPDataIni, FDTPDataFim: TDateTimePicker;
    FQrCrt, FQrLct: TmySQLQuery;
    FCliLoc: Integer;
    FConfirmou, FLocSohCliInt: Boolean;
    FModuleLctX: TDataModule;
    FTabLctA: String;
  end;

var
  FmLocLancto: TFmLocLancto;

implementation

uses UnMyObjects, Principal, Module, LocDefs, ModuleGeral, UnFinanceiro,
  ModuleFin, ModuleLct2, MyDBCheck, DmkDAC_PF, UMySQLModule;

{$R *.DFM}

procedure TFmLocLancto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FQrLct := FmLocDefs.FQrLct;
  FDTPDataIni := FmLocDefs.FDTPDataIni;
end;

procedure TFmLocLancto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  //
  if Screen.Width > 800 then DBGLct.Columns[4].Width := 246;
  //
  {FmLocLancto.FQrLct := FQrLct;
  FmLocLancto.FDTPDataIni := FDTPDataIni;
  FmLocLancto.ShowModal;
  FmLocLancto.Destroy;}
end;

procedure TFmLocLancto.BtSaidaAClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocLancto.BtLocaliza1Click(Sender: TObject);
begin
  if(QrLoc.State <> dsInactive) and (QrLoc.RecordCount > 0) then
  begin
    if not BtLocalizaTryAgain(False) then
    begin
      Screen.Cursor := crHourGlass;
      Timer1.Enabled := True;
    end;
  end;
end;

function TFmLocLancto.BtLocalizaTryAgain(Avisa: Boolean): Boolean;
var
{$IFDEF DEFINE_VARLCT}
  Lancamento, Carteira: Integer;
{$ENDIF}
  Data: TDateTime;
  TentouLocalizar: Boolean;
  Achou: Boolean;

  function TentaLocalizarMaisUmaVez(Avisa: Boolean): Boolean;
  begin
    Result := False;
    //
    TDmLct2(FModuleLctX).FTipoData := FTipoData;
    //
    TDmLct2(FModuleLctX).VeSeReabreLct(TdmkEditDateTimePicker(FDTPDataIni),
      TdmkEditDateTimePicker(FDTPDataFim), 0, Carteira,
      TDmLct2(FModuleLctX).QrLctControle.Value,
      TDmLct2(FModuleLctX).QrLctSub.Value, TDmLct2(FModuleLctX).QrCrt,
      TDmLct2(FModuleLctX).QrLct, True);
    //
{$IFDEF DEFINE_VARLCT}
    Result := TDmLct2(FModuleLctX).LocalizaLancamento(Carteira, Lancamento,
      FQrCrt, FQrLct, FmLocDefs, FmLocLancto, Avisa);
{$Else}
    Result := False;
{$ENDIF}
  end;

begin
  if Avisa then
  begin
    if not DmodG.LocalizaClienteInterno(QrLocCLIENTE_INTERNO.Value,
      'Localiza��o n�o permitida! Voc� est� tentando localizar um lan�amento ' +
      'de um cliente interno diferente do que est� ' +
      'em gerenciamento no momento!' + sLineBreak +
      //'Cliente interno gerenciado:' + IntToStr() + sLineBreak +
      'Cliente interno do lan�amento:' +
      IntToStr(QrLocCLIENTE_INTERNO.Value)) then
        Exit;
  end;
  Data := 0;
  try
    Screen.Cursor := crHourGlass;
    if (FDTPDataIni <> nil) and (FDTPDataFim <> nil) then
    begin
      case FTipoData of
        0: Data := QrLocData.Value;
        1: Data := QrLocVencimento.Value;
        2: Data := QrLocCompensado.Value;
      end;

      if Data < FDTPDataIni.Date then
      begin
        FDTPDataIni.Date := Data;
        VAR_FL_DataIni   := Data;
      end else
      begin
        FDTPDataIni.Date := FDTPDataIni.Date;
        VAR_FL_DataIni   := FDTPDataIni.Date;
      end;

      if Data > FDTPDataFim.Date then
      begin
        FDTPDataFim.Date := Data;
        VAR_FL_DataFim   := Data;
      end else
      begin
        FDTPDataFim.Date := FDTPDataFim.Date;
        VAR_FL_DataFim   := FDTPDataFim.Date;
      end;
    end else
    begin
      //FDTPDataIni.Date := 0;
      //FDTPDataFim.Date := 0;
      VAR_FL_DataIni   := 0;
      VAR_FL_DataFim   := 0;
    end;
    //
    VAR_LANCTO2 := QrLocControle.Value;
    //Sub         := QrLocSub.Value;
    //
    //TentouLocalizar := False;
    //
    TentouLocalizar := True;
    {$IFDEF DEFINE_VARLCT}
      Lancamento  := QrLocControle.Value;
      Carteira    := QrLocCarteira.Value;
      if FModuleLctX <> nil then
      begin
        if not TentaLocalizarMaisUmaVez(False) then
          TentaLocalizarMaisUmaVez(Avisa);
      end;
    {$ENDIF}
    //
    if not TentouLocalizar then Geral.MB_Erro(
      'M�dulo de dados n�o definido para localizar lancamento!' + sLineBreak +
      'Avise a Dermatek!');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLocLancto.QrLocCalcFields(DataSet: TDataSet);
begin
  if QrLocMes2.Value > 0 then
    QrLocMENSAL.Value := FormatFloat('00', QrLocMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrLocAno.Value), 3, 2)
   else QrLocMENSAL.Value := CO_VAZIO;
end;

procedure TFmLocLancto.DBGLctDblClick(Sender: TObject);
begin
  if GBConfirma2.Visible then
    BtConfirmaClick(Self);
end;

procedure TFmLocLancto.DBGLctKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //if key=13 then AlteraRegistro(1);
end;

procedure TFmLocLancto.BtSomaClick(Sender: TObject);
begin
  if(QrLoc.State <> dsInactive) and (QrLoc.RecordCount > 0) then
  begin
    if BtAltera.Enabled = True then
    begin
      BtInclui.Enabled := False;
      BtAltera.Enabled := False;
      BtLocaliza1.Enabled := False;
      //DBGLct.Options := DBGLct.Options + [dgMultiSelect];
    end else begin
      SomaLinhas;
      BtInclui.Enabled := True;
      BtAltera.Enabled := True;
      BtLocaliza1.Enabled := True;
      //DBGLct.Options := DBGLct.Options - [dgMultiSelect];
    end;
  end;
end;

procedure TFmLocLancto.Selecionados1Click(Sender: TObject);
begin
  AlteraConta(istSelecionados);
end;

procedure TFmLocLancto.SomaLinhas;
var
  Debito, Credito: Double;
  i: Integer;
begin
  Debito := 0;
  Credito := 0;
  with DBGLct.DataSource.DataSet do
  for i:= 0 to DBGLct.SelectedRows.Count-1 do
  begin
    //GotoBookmark(DBGLct.SelectedRows.Items[i]);
    GotoBookmark(DBGLct.SelectedRows.Items[i]);
    Debito := Debito + QrLocDebito.Value;
    Credito := Credito + QrLocCredito.Value;
  end;
  Geral.MB_Aviso('Resultado:' + sLineBreak +
  Geral.TFT(FloatToStr(Credito), 2, siNegativo)+ ' - '+
  Geral.TFT(FloatToStr(Debito), 2, siNegativo)+' = '+
  Geral.TFT(FloatToStr(Credito-Debito), 2, siNegativo));
end;

procedure TFmLocLancto.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  BtLocalizaTryAgain(True);
end;

procedure TFmLocLancto.BtConfirmaClick(Sender: TObject);
begin
  FConfirmou := True;
  Close;
end;

procedure TFmLocLancto.BtIncluiClick(Sender: TObject);
begin
  //AlteraRegistro(0);
  //N�o da para fazer por causa dos juros e multas e etc...
  //FmPrincipal.IncluiNovoLancto();
end;

procedure TFmLocLancto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocLancto.odos1Click(Sender: TObject);
begin
  AlteraConta(istTodos);
end;

procedure TFmLocLancto.QrLocAfterOpen(DataSet: TDataSet);
begin
  if GOTOy.Registros(QrLoc) = 0 then
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'N�o foram encontrados registros.')
  else
  if GOTOy.Registros(QrLoc) = 1 then
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Foi encontrado 1 registro.')
  else
  if GOTOy.Registros(QrLoc) > 1 then
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Foram encontrados ' +
    Geral.FF0(QrLoc.RecordCount) + ' registros.');
end;

procedure TFmLocLancto.AlteraConta(Quais: TSelType);
const
  Aviso   = '...';
  Titulo  = 'Conta do Plano de Contas';
  Prompt  = 'Informe a nova conta:';
  Campo   = 'Descricao';
var
  VarCta: Variant;
  N, Genero, Itens: Integer;
  //
(*
  Tipo, Carteira, Controle, Sub: Integer;
  Data, CtrlTxt: String;
  procedure AltGeneroAtual();
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    if QrLocControle.Value <> 0 then
    begin
      CtrlTxt := Geral.FF0(QrLocControle.Value);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrLLC, Dmod.MyDb, [
      'SELECT Data, Tipo, Carteira, Controle, Sub, Genero ',
      'FROM ' + FTabLctA,
      'WHERE ID_Pgto=' + CtrlTxt,
      'OR CtrlQuitPg=' + CtrlTxt,
      'OR Controle=' + CtrlTxt,
      '']);
      //
      QrLLC.First;
      while not QrLLC.Eof do
      begin
        Data      := Geral.FDT(QrLLCData.Value, 1);
        Tipo      := QrLLCTipo.Value;
        Carteira  := QrLLCCarteira.Value;
        Controle  := QrLLCControle.Value;
        Sub       := QrLLCSub.Value;
        //
        if UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, FTabLctA, False, [
        'Genero'], [
        'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
        Genero], [
        Data, Tipo, Carteira, Controle, Sub], True) then
          N := N + 1;
        //
        QrLLC.Next;
      end;
    end;
    //
  end;
*)
var
  I, CtrlMae: Integer;
begin
  VarCta := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Codigo, Nome ' + Campo,
  'FROM contas ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, False);
  if VarCta <> Null then
    Genero := VarCta
  else
    Genero := 0;
  //
  if Genero <> 0 then
  begin
    case Quais of
      istSelecionados:
      begin
        Itens := DBGLct.SelectedRows.Count;
      end;
      istTodos:
      begin
        Itens := QrLoc.RecordCount;
      end;
      else
      begin
        Itens := 0;
        Geral.MB_Aviso('"TSelType" n�o implementado!');
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    if Itens = 0 then
    begin
      Geral.MB_Aviso('Nenhum item foi selecionado!');
      Screen.Cursor := crDefault;
      Exit;
    end;
    if Geral.MB_Pergunta(
    'Confirma a altera��o da conta (do plano de contas) dos ' +
    Geral.FF0(Itens) +
    ' itens e de todos seus lan�amentos de origem e quita��o?') <> ID_YES then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    N := 0;
    PB1.Position := 0;
    PB1.Max := Itens;
    //
    QrLoc.DisableControls;
    try
      case Quais of
        istSelecionados:
        //if DGDados.SelectedRows.Count >= 1 then
        begin
          with DBGLct.DataSource.DataSet do
          for I := 0 to DBGLct.SelectedRows.Count-1 do
          begin
            //GotoBookmark(DBGLct.SelectedRows.Items[I]);
            GotoBookmark(DBGLct.SelectedRows.Items[I]);
            //AltGeneroAtual();
            CtrlMae := QrLocControle.Value;
            N := N + UFinanceiro.AlteraGenero(CtrlMae, FTabLctA, Genero);
          end;
        end;
        istTodos:
        begin
          Qrloc.First;
          while not QrLoc.Eof do
          begin
            //AltGeneroAtual();
            CtrlMae := QrLocControle.Value;
            N := N + UFinanceiro.AlteraGenero(CtrlMae, FTabLctA, Genero);
            //
            QrLoc.Next;
          end;
        end;
        else
        begin
          Geral.MB_Aviso('"TSelType" n�o implementado!');
          Screen.Cursor := crDefault;
          Exit;
        end;
      end;
      Geral.MB_Info('Entre selecionados e suas origens e quita��es, ' +
        Geral.FF0(N) + ' lan�amentos foram alterados!');
    finally
      QrLoc.EnableControls;
    end;
  end;
end;

procedure TFmLocLancto.BtAcaoClick(Sender: TObject);
begin
  if FTabLctA = '' then
    Geral.MB_Aviso('Altera��es n�o permitidas no arquivo selecionado!')
  else
    MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmLocLancto.BtAlteraClick(Sender: TObject);
begin
  //AlteraRegistro(0);
  //N�o da para fazer por cousa dos juros e multas e etc...
  //FmPrincipal.IncluiNovoLancto();
end;

end.
