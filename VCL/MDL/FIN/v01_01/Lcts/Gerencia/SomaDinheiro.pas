unit SomaDinheiro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, UnInternalConsts, Grids, DBGrids, Db,
  (*DBTables,*) UnGOTOy, UnMsgInt, mySQLDbTables, dmkEdit, dmkGeral, dmkImage,
  UnDmkEnums;

type
  TFmSomaDinheiro = class(TForm)
    QrSomaIts: TMySQLQuery;
    QrSoma: TMySQLQuery;
    QrSomaCaixa: TIntegerField;
    QrSomaC200: TIntegerField;
    QrSomaC100: TIntegerField;
    QrSomaC050: TIntegerField;
    QrSomaC020: TIntegerField;
    QrSomaC010: TIntegerField;
    QrSomaC005: TIntegerField;
    QrSomaC002: TIntegerField;
    QrSomaC001: TIntegerField;
    QrSomaM100: TIntegerField;
    QrSomaM050: TIntegerField;
    QrSomaM025: TIntegerField;
    QrSomaM010: TIntegerField;
    QrSomaM005: TIntegerField;
    QrSomaM001: TIntegerField;
    DsSomaIts: TDataSource;
    QrSTIts: TMySQLQuery;
    QrSomaItsCaixa: TIntegerField;
    QrSomaItsControle: TIntegerField;
    QrSomaItsValor: TFloatField;
    QrSomaItsDescricao: TWideStringField;
    QrSTItsValor: TFloatField;
    Panel1: TPanel;
    Panel4: TPanel;
    DBGrid1: TDBGrid;
    PnControla: TPanel;
    Label2: TLabel;
    BtInclui: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    EdVTotal: TDmkEdit;
    PainelDados: TPanel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Edit1: TDmkEdit;
    EdC200: TDmkEdit;
    EdVC200: TDmkEdit;
    Edit4: TDmkEdit;
    EdC100: TDmkEdit;
    EdVC100: TDmkEdit;
    Edit6: TDmkEdit;
    EdC050: TDmkEdit;
    EdVC50: TDmkEdit;
    Edit10: TDmkEdit;
    EdC020: TDmkEdit;
    EdVC20: TDmkEdit;
    Edit12: TDmkEdit;
    EdC010: TDmkEdit;
    EdVC10: TDmkEdit;
    Edit14: TDmkEdit;
    EdC005: TDmkEdit;
    EdVC5: TDmkEdit;
    Edit16: TDmkEdit;
    EdC002: TDmkEdit;
    EdVC2: TDmkEdit;
    Edit18: TDmkEdit;
    EdC001: TDmkEdit;
    EdVC1: TDmkEdit;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Edit20: TDmkEdit;
    EdM100: TDmkEdit;
    EdVM100: TDmkEdit;
    Edit22: TDmkEdit;
    EdM050: TDmkEdit;
    EdVM50: TDmkEdit;
    Edit24: TDmkEdit;
    EdM025: TDmkEdit;
    EdVM25: TDmkEdit;
    Edit26: TDmkEdit;
    EdM010: TDmkEdit;
    EdVM10: TDmkEdit;
    Edit28: TDmkEdit;
    EdM005: TDmkEdit;
    EdVM5: TDmkEdit;
    Edit30: TDmkEdit;
    EdM001: TDmkEdit;
    EdVM1: TDmkEdit;
    QrSomaItsVencimento: TDateField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtSair: TBitBtn;
    BtConfirma: TBitBtn;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdC200Exit(Sender: TObject);
    procedure BtSairClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrSomaItsAfterOpen(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);

  private
    { Private declarations }
    procedure SomaTudo;
    procedure IncluiCheque(Tipo: Integer);
    procedure ExcluiCheque;
  public
    { Public declarations }
    procedure ReopenSomaIts(Controle: Integer);
  end;

var
  FmSomaDinheiro: TFmSomaDinheiro;
  SD_Valendo: Boolean;

implementation

uses UnMyObjects, Module, SomaDinheiroCH, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmSomaDinheiro.SomaTudo;
var
  C200,  C100,  C050,  C020,  C010,  C005,  C002,  C001:  Integer;
  M100,  M050,  M025,  M010,  M005,  M001:  Integer;
  VC200, VC100, VC050, VC020, VC010, VC005, VC002, VC001: Double;
  VM100, VM050, VM025, VM010, VM005, VM001: Double;
  VTotal : Double;
begin
  if not SD_Valendo then Exit;
  C200 := EdC200.ValueVariant;
  C100 := EdC100.ValueVariant;
  C050 := EdC050.ValueVariant;
  C020 := EdC020.ValueVariant;
  C010 := EdC010.ValueVariant;
  C005 := EdC005.ValueVariant;
  C002 := EdC002.ValueVariant;
  C001 := EdC001.ValueVariant;
  M100 := EdM100.ValueVariant;
  M050 := EdM050.ValueVariant;
  M025 := EdM025.ValueVariant;
  M010 := EdM010.ValueVariant;
  M005 := EdM005.ValueVariant;
  M001 := EdM001.ValueVariant;
  //
  VC200 := C200 * 200.00;
  VC100 := C100 * 100.00;
  VC050 := C050 *  50.00;
  VC020 := C020 *  20.00;
  VC010 := C010 *  10.00;
  VC005 := C005 *   5.00;
  VC002 := C002 *   2.00;
  VC001 := C001 *   1.00;
  VM100 := M100 *   1.00;
  VM050 := M050 *   0.50;
  VM025 := M025 *   0.25;
  VM010 := M010 *   0.10;
  VM005 := M005 *   0.05;
  VM001 := M001 *   0.01;
  //
  QrSTIts.Close;
  QrSTIts.Params[0].AsInteger := VAR_QRCARTEIRAS.FieldByName('Codigo').AsInteger;
  UnDmkDAC_PF.AbreQuery(QrSTIts, Dmod.MyDB);
  //
  VTotal := VC200+VC100+VC050+VC020+VC010+VC005+VC002+VC001+
            VM100+VM050+VM025+VM010+VM005+VM001+QrSTItsValor.Value;
  //
  EdC200.ValueVariant  := C200;
  EdC100.ValueVariant  := C100;
  EdC050.ValueVariant  := C050;
  EdC020.ValueVariant  := C020;
  EdC010.ValueVariant  := C010;
  EdC005.ValueVariant  := C005;
  EdC002.ValueVariant  := C002;
  EdC001.ValueVariant  := C001;
  EdM100.ValueVariant  := M100;
  EdM050.ValueVariant  := M050;
  EdM025.ValueVariant  := M025;
  EdM010.ValueVariant  := M010;
  EdM005.ValueVariant  := M005;
  EdM001.ValueVariant  := M001;

  EdVC200.ValueVariant := VC200;
  EdVC100.ValueVariant := VC100;
  EdVC50.ValueVariant  := VC050;
  EdVC20.ValueVariant  := VC020;
  EdVC10.ValueVariant  := VC010;
  EdVC5.ValueVariant   := VC005;
  EdVC2.ValueVariant   := VC002;
  EdVC1.ValueVariant   := VC001;
  EdVM100.ValueVariant := VM100;
  EdVM50.ValueVariant  := VM050;
  EdVM25.ValueVariant  := VM025;
  EdVM10.ValueVariant  := VM010;
  EdVM5.ValueVariant   := VM005;
  EdVM1.ValueVariant   := VM001;

  EdVTotal.ValueVariant:= VTotal;
  VAR_VALOR := VTotal;

  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE soma SET C200=:P0, C100=:P1, C050=:P2, ');
  Dmod.QrUpdM.SQL.Add('C020=:P3, C010=:P4, C005=:P5, C002=:P6, C001=:P7, ');
  Dmod.QrUpdM.SQL.Add('M100=:P8, M050=:P9, M025=:P10, M010=:P11, ');
  Dmod.QrUpdM.SQL.Add('M005=:P12, M001=:P13 WHERE Caixa=:P14');
  Dmod.QrUpdM.Params[0].AsInteger := C200;
  Dmod.QrUpdM.Params[1].AsInteger := C100;
  Dmod.QrUpdM.Params[2].AsInteger := C050;
  Dmod.QrUpdM.Params[3].AsInteger := C020;
  Dmod.QrUpdM.Params[4].AsInteger := C010;
  Dmod.QrUpdM.Params[5].AsInteger := C005;
  Dmod.QrUpdM.Params[6].AsInteger := C002;
  Dmod.QrUpdM.Params[7].AsInteger := C001;
  Dmod.QrUpdM.Params[8].AsInteger := M100;
  Dmod.QrUpdM.Params[9].AsInteger := M050;
  Dmod.QrUpdM.Params[10].AsInteger := M025;
  Dmod.QrUpdM.Params[11].AsInteger := M010;
  Dmod.QrUpdM.Params[12].AsInteger := M005;
  Dmod.QrUpdM.Params[13].AsInteger := M001;
  Dmod.QrUpdM.Params[14].AsInteger := VAR_QRCARTEIRAS.FieldByName('Codigo').AsInteger;
  Dmod.QrUpdM.ExecSQL;
end;

procedure TFmSomaDinheiro.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then SomaTudo;
end;

procedure TFmSomaDinheiro.BtConfirmaClick(Sender: TObject);
begin
  SomaTudo;
  Close;
end;

procedure TFmSomaDinheiro.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdC200.SetFocus;
end;

procedure TFmSomaDinheiro.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  SD_Valendo := False;
  ReopenSomaIts(0);
  QrSoma.Params[0].AsInteger := VAR_QRCARTEIRAS.FieldByName('Codigo').AsInteger;
  UnDmkDAC_PF.AbreQuery(QrSoma, Dmod.MyDB);
  if GOTOy.Registros(QrSoma) <> 1 then
  begin
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM soma WHERE Caixa=:P0');
    Dmod.QrUpdM.Params[0].AsInteger := VAR_QRCARTEIRAS.FieldByName('Codigo').AsInteger;
    Dmod.QrUpdM.ExecSQL;
    //
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('INSERT INTO soma SET Caixa=:P0');
    Dmod.QrUpdM.Params[0].AsInteger := VAR_QRCARTEIRAS.FieldByName('Codigo').AsInteger;
    Dmod.QrUpdM.ExecSQL;
    //
    QrSoma.Close;
    UnDmkDAC_PF.AbreQuery(QrSoma, Dmod.MyDB);
  end;
  EdC200.ValueVariant := QrSomaC200.Value;
  EdC100.ValueVariant := QrSomaC100.Value;
  EdC050.ValueVariant := QrSomaC050.Value;
  EdC020.ValueVariant := QrSomaC020.Value;
  EdC010.ValueVariant := QrSomaC010.Value;
  EdC005.ValueVariant := QrSomaC005.Value;
  EdC002.ValueVariant := QrSomaC002.Value;
  EdC001.ValueVariant := QrSomaC001.Value;
  EdM100.ValueVariant := QrSomaM100.Value;
  EdM050.ValueVariant := QrSomaM050.Value;
  EdM025.ValueVariant := QrSomaM025.Value;
  EdM010.ValueVariant := QrSomaM010.Value;
  EdM005.ValueVariant := QrSomaM005.Value;
  EdM001.ValueVariant := QrSomaM001.Value;
  SD_Valendo := True;
  SomaTudo;
end;

procedure TFmSomaDinheiro.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_INSERT then IncluiCheque(0);
  if (key=13) and BtAltera.Enabled then IncluiCheque(1);
  if (key=VK_DELETE) and (Shift=[ssCtrl]) and BtExclui.Enabled then ExcluiCheque;
end;

procedure TFmSomaDinheiro.ReopenSomaIts(Controle: Integer);
begin
  QrSomaIts.Close;
  QrSomaIts.Params[0].AsInteger := VAR_QRCARTEIRAS.FieldByName('Codigo').AsInteger;
  UnDmkDAC_PF.AbreQuery(QrSomaIts, Dmod.MyDB);
  if Controle <> -1000 then
    QrSomaIts.Locate('Controle', Controle, [])
  else QrSomaIts.Last;
  SomaTudo;
end;

procedure TFmSomaDinheiro.EdC200Exit(Sender: TObject);
begin
  SomaTudo;
end;

procedure TFmSomaDinheiro.BtSairClick(Sender: TObject);
begin
  VAR_CALC := 0;
  Close;
end;

procedure TFmSomaDinheiro.IncluiCheque(Tipo: Integer);
begin
  MyObjects.CriaForm_AcessoTotal(TFmSomaDinheiroCH, FmSomaDinheiroCH);
  if Tipo = 1 then with FmSomaDinheiroCH do
  begin
    ImgTipo.SQLType            := stUpd;
    EdControle.ValueVariant   := QrSomaItsControle.Value;
    EdValor.ValueVariant      := QrSomaItsValor.Value;
    EdDescricao.Text          := QrSomaItsDescricao.Value;
    TPVencimento.Date         := QrSomaItsVencimento.Value;
  end else FmSomaDinheiroCH.ImgTipo.SQLType := stIns;
  FmSomaDinheiroCH.ShowModal;
  FmSomaDinheiroCH.Destroy;
end;

procedure TFmSomaDinheiro.ExcluiCheque;
var
  Prox: Integer;
begin
  if GOTOy.Registros(QrSomaIts) > 0 then
  begin
    if Geral.MB_Pergunta(FIN_MSG_ASKESCLUI) = ID_YES then
    begin
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('DELETE FROM somaits WHERE Caixa=:P0 AND Controle=:P1');
      Dmod.QrUpdM.Params[0].AsInteger := VAR_QRCARTEIRAS.FieldByName('Codigo').AsInteger;
      Dmod.QrUpdM.Params[1].AsInteger := QrSomaItsControle.Value;
      Dmod.QrUpdM.ExecSQL;
      Prox := UMyMod.ProximoRegistro(QrSomaIts, 'Controle', 0);
      ReopenSomaIts(Prox);
    end;
  end else ShowMessage('N�o existe registro a ser deletado!');
end;

procedure TFmSomaDinheiro.BtIncluiClick(Sender: TObject);
begin
  IncluiCheque(0);
end;

procedure TFmSomaDinheiro.BtExcluiClick(Sender: TObject);
begin
  ExcluiCheque;
end;

procedure TFmSomaDinheiro.BtAlteraClick(Sender: TObject);
begin
  IncluiCheque(1);
end;

procedure TFmSomaDinheiro.QrSomaItsAfterOpen(DataSet: TDataSet);
begin
  if GOTOy.Registros(QrSomaIts) = 0 then
  begin
    BtAltera.Enabled := False;
    BtExclui.Enabled := False;
  end else begin
    BtAltera.Enabled := True;
    BtExclui.Enabled := True;
  end;
end;

procedure TFmSomaDinheiro.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

