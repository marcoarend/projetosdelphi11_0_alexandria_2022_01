object FmLctGer2: TFmLctGer2
  Left = 339
  Top = 185
  Caption = 'FIN-SELFG-001 :: Finan'#231'as'
  ClientHeight = 603
  ClientWidth = 1264
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1264
    Height = 485
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnLct: TPanel
      Left = 0
      Top = 0
      Width = 1264
      Height = 485
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Shape1: TShape
        Left = 492
        Top = 16
        Width = 65
        Height = 57
        Brush.Color = clBlue
        Visible = False
      end
      object Label14: TLabel
        Left = 492
        Top = 76
        Width = 65
        Height = 13
        Caption = '{                   }'
        Visible = False
      end
      object DBGLct: TdmkDBGridZTO
        Left = 0
        Top = 240
        Width = 1264
        Height = 206
        Align = alClient
        DataSource = DmLct2.DsLct
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Visible = False
        RowColors = <>
        OnDrawColumnCell = DBGLctDrawColumnCell
        OnDblClick = DBGLctDblClick
        OnKeyDown = DBGLctKeyDown
        FieldsCalcToOrder.Strings = (
          'COMPENSADO_TXT=Compensado'
          'MENSAL=Ano,Mes2'
          'NOMESIT=Sit')
        Columns = <
          item
            Expanded = False
            FieldName = 'Carteira'
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Data'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataCad'
            Title.Caption = 'Cadastro'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SerieCH'
            Title.Caption = 'S'#233'rie'
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Documento'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Duplicata'
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'SerieNF'
            Title.Caption = 'S'#233'rie NF'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NotaFiscal'
            Title.Caption = 'Nota Fiscal'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtde'
            Title.Alignment = taRightJustify
            Title.Caption = 'Quantidade 1'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtd2'
            Title.Caption = 'Quantidade 2'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descri'#231#227'o'
            Width = 164
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatParcRef'
            Title.Caption = 'Origem'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatID'
            Title.Caption = 'Orig. Fat.'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatNum'
            Title.Caption = 'Fat. Num.'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Debito'
            Title.Caption = 'D'#233'bito'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Credito'
            Title.Caption = 'Cr'#233'dito'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Vencimento'
            Title.Caption = 'Vencim.'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COMPENSADO_TXT'
            Title.Caption = 'Compen.'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MENSAL'
            Title.Caption = 'M'#234's'
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMESIT'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Situa'#231#227'o'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'Lan'#231'to'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Sub'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SALDO'
            Title.Caption = 'Saldo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMERELACIONADO'
            Title.Caption = 'Terceiro'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MoraVal'
            Title.Caption = 'Juros'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MultaVal'
            Title.Caption = 'Multa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DescoVal'
            Title.Caption = 'Desconto'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UH'
            Width = 132
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'USERCAD_TXT'
            Title.Caption = 'Cadastrado por'
            Width = 130
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATACAD_TXT'
            Title.Caption = 'Data Cadastro'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'USERALT_TXT'
            Title.Caption = 'Alterado por'
            Width = 130
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATAALT_TXT'
            Title.Caption = 'Data Altera'#231#227'o'
            Width = 60
            Visible = True
          end>
      end
      object PainelDados2: TPanel
        Left = 0
        Top = 0
        Width = 1264
        Height = 240
        Align = alTop
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 797
          Height = 240
          Align = alLeft
          BevelOuter = bvNone
          Caption = 'Panel3'
          TabOrder = 0
          object Panel4: TPanel
            Left = 0
            Top = 49
            Width = 797
            Height = 191
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel10: TPanel
              Left = 660
              Top = 64
              Width = 137
              Height = 127
              Align = alRight
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object GroupBox5: TGroupBox
                Left = 0
                Top = 0
                Width = 137
                Height = 127
                Align = alClient
                Caption = ' Per'#237'odo de visualiza'#231#227'o:'
                TabOrder = 0
                object Label8: TLabel
                  Left = 4
                  Top = 80
                  Width = 13
                  Height = 13
                  Caption = 'ini:'
                end
                object Label5: TLabel
                  Left = 4
                  Top = 104
                  Width = 16
                  Height = 13
                  Caption = 'fim:'
                end
                object TPDataIni: TdmkEditDateTimePicker
                  Left = 24
                  Top = 78
                  Width = 106
                  Height = 21
                  Date = 37617.000000000000000000
                  Time = 0.122587314799602600
                  TabOrder = 0
                  Visible = False
                  OnClick = TPDataIniClick
                  OnChange = TPDataIniChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object TPDataFim: TdmkEditDateTimePicker
                  Left = 24
                  Top = 102
                  Width = 106
                  Height = 21
                  Date = 37617.000000000000000000
                  Time = 0.122587314799602600
                  TabOrder = 1
                  Visible = False
                  OnClick = TPDataFimClick
                  OnChange = TPDataFimChange
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object RGTipoData: TRadioGroup
                  Left = 2
                  Top = 15
                  Width = 133
                  Height = 60
                  Align = alTop
                  Caption = ' Per'#237'odo: '
                  Columns = 2
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ItemIndex = 3
                  Items.Strings = (
                    'Emiss'#227'o'
                    'Vencim.'
                    'Compe.'
                    'Tudo')
                  ParentFont = False
                  TabOrder = 2
                  OnClick = RGTipoDataClick
                end
              end
            end
            object GBAcoes: TGroupBox
              Left = 0
              Top = 0
              Width = 797
              Height = 64
              Align = alTop
              Caption = 
                ' A'#231#245'es em lan'#231'amentos: (h'#225' restri'#231#245'es para carteiras negativas!)' +
                ' '
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 1
              object PnAcoesTravaveis: TPanel
                Left = 2
                Top = 15
                Width = 634
                Height = 47
                Align = alLeft
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object BtMenu: TBitBtn
                  Tag = 237
                  Left = 4
                  Top = 4
                  Width = 90
                  Height = 40
                  Caption = '&Menu'
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtMenuClick
                end
                object BtInclui: TBitBtn
                  Tag = 10
                  Left = 94
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Inclui novo banco'
                  Caption = '&Inclui'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BtIncluiClick
                end
                object BtAltera: TBitBtn
                  Tag = 11
                  Left = 184
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Altera banco atual'
                  Caption = '&Altera'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                  OnClick = BtAlteraClick
                end
                object BtExclui: TBitBtn
                  Tag = 12
                  Left = 274
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Exclui banco atual'
                  Caption = 'Excl&ui'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 3
                  OnClick = BtExcluiClick
                end
                object BtQuita: TBitBtn
                  Tag = 10024
                  Left = 454
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Quita'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 4
                  OnClick = BtQuitaClick
                end
                object BtDuplica: TBitBtn
                  Tag = 56
                  Left = 364
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Exclui banco atual'
                  Caption = '&Duplica'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 5
                  OnClick = BtDuplicaClick
                end
                object BtConcilia: TBitBtn
                  Tag = 10011
                  Left = 544
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = 'Concilia'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 6
                  OnClick = BtConciliaClick
                end
              end
              object PnAcoesNoLock: TPanel
                Left = 636
                Top = 15
                Width = 159
                Height = 47
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object BtTrfCta: TBitBtn
                  Tag = 330
                  Left = 0
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = 'Transf.'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtTrfCtaClick
                end
                object BtSalvaPosicaoGrade: TBitBtn
                  Tag = 58
                  Left = 90
                  Top = 4
                  Width = 40
                  Height = 40
                  Cursor = crHandPoint
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BtSalvaPosicaoGradeClick
                end
              end
            end
            object Panel15: TPanel
              Left = 0
              Top = 64
              Width = 660
              Height = 127
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 2
              object GroupBox7: TGroupBox
                Left = 0
                Top = 0
                Width = 660
                Height = 64
                Align = alTop
                Caption = ' Outras a'#231#245'es / impress'#245'es: '
                TabOrder = 0
                object Panel12: TPanel
                  Left = 2
                  Top = 15
                  Width = 656
                  Height = 47
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object BtPlaCtas: TBitBtn
                    Tag = 323
                    Left = 4
                    Top = 3
                    Width = 40
                    Height = 40
                    NumGlyphs = 2
                    TabOrder = 0
                    OnClick = BtPlaCtasClick
                  end
                  object BtCheque: TBitBtn
                    Tag = 10042
                    Left = 44
                    Top = 3
                    Width = 100
                    Height = 40
                    Caption = 'Cheque(s)'
                    TabOrder = 1
                    OnClick = BtChequeClick
                  end
                  object BtSaldoTotal: TBitBtn
                    Tag = 238
                    Left = 144
                    Top = 3
                    Width = 100
                    Height = 40
                    Caption = 'Sdo Total'
                    TabOrder = 2
                    OnClick = BtSaldoTotalClick
                  end
                  object BtConfPagtosCad: TBitBtn
                    Tag = 316
                    Left = 404
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 5
                    OnClick = BtConfPagtosCadClick
                  end
                  object BtConfPgtosExe: TBitBtn
                    Tag = 318
                    Left = 444
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 6
                    OnClick = BtConfPgtosExeClick
                  end
                  object BtRefresh: TBitBtn
                    Tag = 1000073
                    Left = 484
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 7
                    OnClick = BtRefreshClick
                  end
                  object BtDesfazOrdenacao: TBitBtn
                    Tag = 329
                    Left = 524
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 8
                    OnClick = BtDesfazOrdenacaoClick
                  end
                  object BtLocaliza: TBitBtn
                    Tag = 22
                    Left = 364
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 4
                    OnClick = BtLocalizaClick
                  end
                  object BtBloqueto: TBitBtn
                    Tag = 10007
                    Left = 604
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 9
                    OnClick = BtBloquetoClick
                  end
                  object BtProtocolo: TBitBtn
                    Tag = 10051
                    Left = 324
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 3
                    OnClick = BtProtocoloClick
                  end
                  object BtAutom: TBitBtn
                    Tag = 174
                    Left = 564
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 10
                    OnClick = BtAutomClick
                  end
                  object BtArrecada: TBitBtn
                    Tag = 10014
                    Left = 244
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 11
                    OnClick = BtArrecadaClick
                  end
                  object BtProvisoes: TBitBtn
                    Tag = 10016
                    Left = 284
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 12
                    OnClick = BtProvisoesClick
                  end
                end
              end
              object GroupBox8: TGroupBox
                Left = 0
                Top = 64
                Width = 660
                Height = 64
                Align = alTop
                Caption = ' Relat'#243'rios e pesquisas: '
                TabOrder = 1
                object Panel13: TPanel
                  Left = 2
                  Top = 15
                  Width = 656
                  Height = 47
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object BtRelatorios: TBitBtn
                    Tag = 5
                    Left = 4
                    Top = 3
                    Width = 170
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'Re&lat'#243'rios e pesquisas'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtRelatoriosClick
                  end
                  object BtFluxCxaDia: TBitBtn
                    Tag = 616
                    Left = 174
                    Top = 3
                    Width = 120
                    Height = 40
                    Caption = 'Fechamento'
                    TabOrder = 1
                    OnClick = BtFluxCxaDiaClick
                  end
                  object BtCopiaCH: TBitBtn
                    Tag = 10019
                    Left = 294
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 2
                    OnClick = BtCopiaCHClick
                  end
                  object BtFluxoCxa: TBitBtn
                    Tag = 10025
                    Left = 334
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 3
                    OnClick = BtFluxoCxaClick
                  end
                  object BtRecibo: TBitBtn
                    Tag = 199
                    Left = 374
                    Top = 3
                    Width = 95
                    Height = 40
                    Caption = '&Recibo'
                    TabOrder = 4
                    OnClick = BtReciboClick
                  end
                  object BtReabre: TBitBtn
                    Tag = 1000020
                    Left = 604
                    Top = 3
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 5
                    OnClick = BtReabreClick
                  end
                end
              end
            end
          end
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 797
            Height = 49
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Label1: TLabel
              Left = 8
              Top = 4
              Width = 42
              Height = 13
              Caption = 'Terceiro:'
            end
            object Bevel1: TBevel
              Left = 380
              Top = 0
              Width = 229
              Height = 20
            end
            object Bevel2: TBevel
              Left = 232
              Top = 0
              Width = 145
              Height = 20
              Visible = False
            end
            object Bevel3: TBevel
              Left = 76
              Top = 0
              Width = 153
              Height = 20
            end
            object Label6: TLabel
              Left = 616
              Top = 4
              Width = 39
              Height = 13
              Caption = 'Carteira:'
            end
            object EdEntidade: TdmkEditCB
              Left = 8
              Top = 20
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdEntidadeRedefinido
              DBLookupComboBox = CBEntidade
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBEntidade: TdmkDBLookupComboBox
              Left = 76
              Top = 20
              Width = 533
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsEntidades
              TabOrder = 1
              dmkEditCB = EdEntidade
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object CkCredito: TCheckBox
              Left = 84
              Top = 2
              Width = 66
              Height = 17
              Caption = 'A receber'
              Checked = True
              State = cbChecked
              TabOrder = 2
              OnClick = CkCreditoClick
            end
            object CkDebito: TCheckBox
              Left = 156
              Top = 2
              Width = 66
              Height = 17
              Caption = 'A Pagar'
              Checked = True
              State = cbChecked
              TabOrder = 3
              OnClick = CkDebitoClick
            end
            object CkGenese: TCheckBox
              Left = 240
              Top = 2
              Width = 66
              Height = 17
              Caption = 'Genese'
              Checked = True
              State = cbChecked
              TabOrder = 4
              Visible = False
              OnClick = CkCreditoClick
            end
            object CkSuplente: TCheckBox
              Left = 312
              Top = 2
              Width = 66
              Height = 17
              Caption = 'Suplente'
              Checked = True
              State = cbChecked
              TabOrder = 5
              Visible = False
              OnClick = CkDebitoClick
            end
            object CkAVencer: TCheckBox
              Left = 388
              Top = 2
              Width = 66
              Height = 17
              Caption = 'A Vencer'
              Checked = True
              State = cbChecked
              TabOrder = 6
              OnClick = CkDebitoClick
            end
            object CkQuitado: TCheckBox
              Left = 536
              Top = 2
              Width = 66
              Height = 17
              Caption = 'Quitado'
              TabOrder = 8
              OnClick = CkDebitoClick
            end
            object CkVencido: TCheckBox
              Left = 460
              Top = 2
              Width = 66
              Height = 17
              Caption = 'Vencido'
              Checked = True
              State = cbChecked
              TabOrder = 7
              OnClick = CkDebitoClick
            end
            object EdCarteira: TdmkEdit
              Left = 616
              Top = 20
              Width = 49
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdCarteiraRedefinido
            end
          end
        end
        object Panel7: TPanel
          Left = 797
          Top = 0
          Width = 467
          Height = 240
          Align = alClient
          ParentBackground = False
          TabOrder = 1
          object PageControl1: TPageControl
            Left = 1
            Top = 1
            Width = 465
            Height = 238
            ActivePage = TabSheet3
            Align = alClient
            TabOrder = 0
            object TabSheet1: TTabSheet
              Caption = ' Carteiras '
              object DBGCrt: TdmkDBGrid
                Left = 0
                Top = 21
                Width = 457
                Height = 189
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'C'#243'digo'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 265
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EmCaixa'
                    Title.Alignment = taRightJustify
                    Title.Caption = 'Em caixa'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Saldo'
                    Title.Alignment = taRightJustify
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DIFERENCA'
                    Title.Caption = 'Diferen'#231'a'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEDOBANCO'
                    Title.Caption = 'Banco '
                    Width = 198
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DmLct2.DsCrt
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnDblClick = DBGCrtDblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'C'#243'digo'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 265
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EmCaixa'
                    Title.Alignment = taRightJustify
                    Title.Caption = 'Em caixa'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Saldo'
                    Title.Alignment = taRightJustify
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DIFERENCA'
                    Title.Caption = 'Diferen'#231'a'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEDOBANCO'
                    Title.Caption = 'Banco '
                    Width = 198
                    Visible = True
                  end>
              end
              object Panel11: TPanel
                Left = 0
                Top = 0
                Width = 457
                Height = 21
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object EdCarteiraPsq: TdmkEdit
                  Left = 64
                  Top = 0
                  Width = 385
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnRedefinido = EdCarteiraPsqRedefinido
                end
              end
            end
            object TabSheet2: TTabSheet
              Caption = ' Soma linhas '
              ImageIndex = 1
              object Panel8: TPanel
                Left = 0
                Top = 0
                Width = 457
                Height = 210
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label20: TLabel
                  Left = 49
                  Top = 6
                  Width = 80
                  Height = 13
                  Caption = 'Soma das linhas:'
                end
                object Label15: TLabel
                  Left = 49
                  Top = 46
                  Width = 90
                  Height = 13
                  Caption = 'Soma dos cr'#233'ditos:'
                end
                object Label16: TLabel
                  Left = 49
                  Top = 86
                  Width = 84
                  Height = 13
                  Caption = 'Soma dos d'#233'bitos'
                end
                object EdSoma: TdmkEdit
                  Left = 48
                  Top = 20
                  Width = 100
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object BtSomaLinhas: TBitBtn
                  Tag = 254
                  Left = 1
                  Top = 4
                  Width = 40
                  Height = 40
                  Cursor = crHandPoint
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BtSomaLinhasClick
                end
                object EdCred: TdmkEdit
                  Left = 48
                  Top = 60
                  Width = 100
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdDebi: TdmkEdit
                  Left = 48
                  Top = 100
                  Width = 100
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
            object TabSheet3: TTabSheet
              Caption = ' Saldo '
              ImageIndex = 2
              object Panel14: TPanel
                Left = 0
                Top = 0
                Width = 457
                Height = 210
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object GroupBox1: TGroupBox
                  Left = 0
                  Top = 147
                  Width = 457
                  Height = 63
                  Align = alBottom
                  Caption = ' Saldo aqui: '
                  TabOrder = 0
                  object BtSaldoAqui: TBitBtn
                    Tag = 238
                    Left = 44
                    Top = 20
                    Width = 40
                    Height = 40
                    TabOrder = 1
                    OnClick = BtSaldoAquiClick
                  end
                  object EdSdoAqui: TdmkEdit
                    Left = 88
                    Top = 29
                    Width = 100
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object BtCalculadora: TBitBtn
                    Tag = 180
                    Left = 4
                    Top = 20
                    Width = 40
                    Height = 40
                    TabOrder = 0
                    OnClick = BtCalculadoraClick
                  end
                end
                object GroupBox2: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 457
                  Height = 147
                  Align = alClient
                  Caption = ' Saldo em caixa: '
                  TabOrder = 1
                  object LaDiferenca: TLabel
                    Left = 88
                    Top = 65
                    Width = 49
                    Height = 13
                    Caption = 'Diferen'#231'a:'
                  end
                  object LaSaldo: TLabel
                    Left = 12
                    Top = 25
                    Width = 30
                    Height = 13
                    Caption = 'Saldo:'
                  end
                  object LaCaixa: TLabel
                    Left = 12
                    Top = 65
                    Width = 46
                    Height = 13
                    Caption = 'Em caixa:'
                  end
                  object EdSaldo: TDBEdit
                    Left = 12
                    Top = 40
                    Width = 105
                    Height = 21
                    TabStop = False
                    Color = clBtnFace
                    DataField = 'Saldo'
                    DataSource = DmLct2.DsCrt
                    Enabled = False
                    TabOrder = 0
                  end
                  object EdDiferenca: TDBEdit
                    Left = 88
                    Top = 80
                    Width = 72
                    Height = 21
                    TabStop = False
                    Color = clBtnFace
                    DataField = 'DIFERENCA'
                    DataSource = DmLct2.DsCrt
                    Enabled = False
                    TabOrder = 1
                  end
                  object EdCaixa: TDBEdit
                    Left = 12
                    Top = 80
                    Width = 72
                    Height = 21
                    Cursor = crHandPoint
                    Color = clWhite
                    DataField = 'EmCaixa'
                    DataSource = DmLct2.DsCrt
                    TabOrder = 2
                  end
                  object BtContarDinheiro: TBitBtn
                    Tag = 239
                    Left = 121
                    Top = 20
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 3
                    OnClick = BtContarDinheiroClick
                  end
                end
              end
            end
          end
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 446
        Width = 1264
        Height = 39
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        Visible = False
        object Label9: TLabel
          Left = 4
          Top = 2
          Width = 62
          Height = 13
          Caption = 'Lan'#231'amento:'
          FocusControl = DBEdit6
        end
        object Label10: TLabel
          Left = 80
          Top = 2
          Width = 17
          Height = 13
          Caption = 'NF:'
          FocusControl = DBEdit7
        end
        object Label11: TLabel
          Left = 148
          Top = 2
          Width = 57
          Height = 13
          Caption = 'Fornecedor:'
          FocusControl = DBEdit8
        end
        object Label13: TLabel
          Left = 492
          Top = 2
          Width = 69
          Height = 13
          Caption = 'Transportador:'
          FocusControl = DBEdit10
        end
        object Label12: TLabel
          Left = 792
          Top = 2
          Width = 39
          Height = 13
          Caption = 'Parcela:'
          FocusControl = DBEdit12
        end
        object Label18: TLabel
          Left = 860
          Top = 2
          Width = 35
          Height = 13
          Caption = 'Cliente:'
          FocusControl = DBEdit13
        end
        object DBEdit6: TDBEdit
          Left = 4
          Top = 16
          Width = 73
          Height = 21
          TabStop = False
          DataField = 'CODIGO'
          ReadOnly = True
          TabOrder = 0
        end
        object DBEdit7: TDBEdit
          Left = 80
          Top = 15
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'NF'
          ReadOnly = True
          TabOrder = 1
        end
        object DBEdit8: TDBEdit
          Left = 148
          Top = 15
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'FORNECEDOR'
          ReadOnly = True
          TabOrder = 2
        end
        object DBEdit9: TDBEdit
          Left = 216
          Top = 15
          Width = 273
          Height = 21
          TabStop = False
          DataField = 'NOMEFORNECEDOR'
          ReadOnly = True
          TabOrder = 3
        end
        object DBEdit10: TDBEdit
          Left = 492
          Top = 15
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'FORNECEDOR'
          ReadOnly = True
          TabOrder = 4
        end
        object DBEdit11: TDBEdit
          Left = 560
          Top = 15
          Width = 225
          Height = 21
          TabStop = False
          DataField = 'NOMETRANSPORTADOR'
          ReadOnly = True
          TabOrder = 5
        end
        object DBEdit12: TDBEdit
          Left = 792
          Top = 15
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'FatParcela'
          ReadOnly = True
          TabOrder = 6
        end
        object DBEdit13: TDBEdit
          Left = 860
          Top = 15
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'FORNECEDOR'
          ReadOnly = True
          TabOrder = 7
        end
        object DBEdit14: TDBEdit
          Left = 928
          Top = 15
          Width = 273
          Height = 21
          TabStop = False
          DataField = 'NOMECLIENTE'
          ReadOnly = True
          TabOrder = 8
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1216
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 818
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 111
        Height = 32
        Caption = 'Finan'#231'as'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 111
        Height = 32
        Caption = 'Finan'#231'as'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 111
        Height = 32
        Caption = 'Finan'#231'as'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 866
      Top = 0
      Width = 350
      Height = 48
      Align = alRight
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 346
        Height = 31
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 17
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 533
    Width = 1264
    Height = 70
    Align = alBottom
    TabOrder = 2
    object GroupBox4: TGroupBox
      Left = 2
      Top = 15
      Width = 131
      Height = 53
      Align = alLeft
      Caption = 'Localiza lan'#231'amento...'
      TabOrder = 0
      object Label17: TLabel
        Left = 8
        Top = 12
        Width = 76
        Height = 13
        Caption = '... pelo controle:'
      end
      object BtLocCtrl: TBitBtn
        Left = 104
        Top = 28
        Width = 21
        Height = 21
        Caption = '>'
        TabOrder = 0
        OnClick = BtLocCtrlClick
      end
      object EdLocCtrl: TdmkEditCB
        Left = 8
        Top = 28
        Width = 93
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
    end
    object GroupBox3: TGroupBox
      Left = 133
      Top = 15
      Width = 994
      Height = 53
      Align = alClient
      Caption = ' Plano de contas '
      TabOrder = 1
      object Panel9: TPanel
        Left = 2
        Top = 15
        Width = 990
        Height = 36
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 4
          Top = -1
          Width = 31
          Height = 13
          Caption = 'Conta:'
        end
        object Label3: TLabel
          Left = 238
          Top = -1
          Width = 52
          Height = 13
          Caption = 'Sub-grupo:'
        end
        object Label4: TLabel
          Left = 402
          Top = -1
          Width = 32
          Height = 13
          Caption = 'Grupo:'
        end
        object Label7: TLabel
          Left = 566
          Top = -1
          Width = 45
          Height = 13
          Caption = 'Conjunto:'
        end
        object DBEdit1: TDBEdit
          Left = 4
          Top = 13
          Width = 230
          Height = 21
          DataField = 'NOMECONTA'
          DataSource = DmLct2.DsLct
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 238
          Top = 13
          Width = 160
          Height = 21
          DataField = 'NOMESUBGRUPO'
          DataSource = DmLct2.DsLct
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 402
          Top = 13
          Width = 160
          Height = 21
          DataField = 'NOMEGRUPO'
          DataSource = DmLct2.DsLct
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 566
          Top = 13
          Width = 160
          Height = 21
          DataField = 'NOMECONJUNTO'
          DataSource = DmLct2.DsLct
          TabOrder = 3
        end
      end
    end
    object PnSaiDesis: TPanel
      Left = 1127
      Top = 15
      Width = 135
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object BtSaida: TBitBtn
        Tag = 13
        Left = 10
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PMTrfCta: TPopupMenu
    Left = 692
    Top = 100
    object Entrecarteitas1: TMenuItem
      Caption = 'Entre c&arteitas'
      object Novatransferncia1: TMenuItem
        Caption = '&Nova transfer'#234'ncia entre carteiras'
        OnClick = Novatransferncia1Click
      end
      object Alteratransferncia1: TMenuItem
        Caption = '&Altera transfer'#234'ncia  entre carteiras'
        OnClick = Alteratransferncia1Click
      end
      object Excluitransferncia1: TMenuItem
        Caption = '&Exclui transfer'#234'ncia entre carteiras'
        OnClick = Excluitransferncia1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Localizatransferncia1: TMenuItem
        Caption = '&Localiza transfer'#234'ncia entre carteiras'
        OnClick = Localizatransferncia1Click
      end
    end
    object Entrecontas1: TMenuItem
      Caption = 'Entre c&ontas'
      object Novatransfernciaentrecontas1: TMenuItem
        Caption = '&Nova transfer'#234'ncia entre contas'
        OnClick = Novatransfernciaentrecontas1Click
      end
      object Alteratransfernciaentrecontas1: TMenuItem
        Caption = '&Altera transfer'#234'ncia entre contas'
        OnClick = Alteratransfernciaentrecontas1Click
      end
      object Excluitransfernciaentrecontas1: TMenuItem
        Caption = '&Exclui transfer'#234'ncia entre contas'
        OnClick = Excluitransfernciaentrecontas1Click
      end
    end
  end
  object PMSaldoAqui: TPopupMenu
    Left = 820
    Top = 232
    object Calcula1: TMenuItem
      Caption = '&Calcula'
      OnClick = Calcula1Click
    end
    object Limpa1: TMenuItem
      Caption = '&Limpa'
      OnClick = Limpa1Click
    end
    object Diferena1: TMenuItem
      Caption = '&Diferen'#231'a'
      OnClick = Diferena1Click
    end
  end
  object PMMenu: TPopupMenu
    OnPopup = PMMenuPopup
    Left = 72
    Top = 124
    object Transferir1: TMenuItem
      Caption = '&Transfer'#234'ncia'
      object Entrecarteiras1: TMenuItem
        Caption = 'Entre c&arteiras'
        object Incluinovatransfernciaentrecarteiras1: TMenuItem
          Caption = '&Inclui nova transfer'#234'ncia entre carteiras'
          OnClick = Incluinovatransfernciaentrecarteiras1Click
        end
        object Alteratransfernciaentrecarteirasatual1: TMenuItem
          Caption = '&Altera transfer'#234'ncia entre carteiras atual'
          OnClick = Alteratransfernciaentrecarteirasatual1Click
        end
        object Excluitransfernciaentrecarteirasatual1: TMenuItem
          Caption = '&Exclui transfer'#234'ncia entre carteiras atual'
          OnClick = Excluitransfernciaentrecarteirasatual1Click
        end
        object N2: TMenuItem
          Caption = '-'
        end
        object Localizatransfernciaentrecarteiras1: TMenuItem
          Caption = 'Localiza transfer'#234'ncia entre carteiras'
          OnClick = Localizatransfernciaentrecarteiras1Click
        end
      end
      object Entrecontas2: TMenuItem
        Caption = 'Entre c&ontas'
        object Incluinovatransfernciaentrecontas1: TMenuItem
          Caption = '&Inclui nova transfer'#234'ncia entre contas'
          OnClick = Incluinovatransfernciaentrecontas1Click
        end
        object Alteratransfernciaentrecontasatual1: TMenuItem
          Caption = '&Altera transfer'#234'ncia entre contas atual'
          OnClick = Alteratransfernciaentrecontasatual1Click
        end
        object Excluitransfernciaentrecontasatual1: TMenuItem
          Caption = '&Exclui transfer'#234'ncia entre contas atual'
          OnClick = Excluitransfernciaentrecontasatual1Click
        end
      end
    end
    object Quitar1: TMenuItem
      Caption = '&Quita'#231#227'o'
      object Compensar1: TMenuItem
        Caption = '&Compensar na conta corrente (Banco)'
        Enabled = False
        OnClick = Compensar1Click
      end
      object Pagar1: TMenuItem
        Caption = '&Pagar / Rolar com emiss'#227'o  (cheque, etc)'
        Enabled = False
        OnClick = Pagar1Click
      end
      object Pagamentorpido1: TMenuItem
        Caption = 'Pagamento &r'#225'pido'
        OnClick = Pagamentorpido1Click
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object PagarAVista1: TMenuItem
        Caption = 'Pagar '#224' vista para um caixa'
        OnClick = PagarAVista1Click
      end
      object N11: TMenuItem
        Caption = '-'
      end
      object Reverter1: TMenuItem
        Caption = 'D&esfazer compensa'#231#227'o'
        Enabled = False
        OnClick = Reverter1Click
      end
      object N19: TMenuItem
        Caption = '-'
      end
      object Localizarlanamentoorigem1: TMenuItem
        Caption = 'Localizar lan'#231'amento origem'
        OnClick = Localizarlanamentoorigem1Click
      end
    end
    object Lanamento1: TMenuItem
      Caption = '&Lan'#231'amento'
      object Localizar2: TMenuItem
        Caption = '&Localizar'
        OnClick = Localizar2Click
      end
      object Copiar1: TMenuItem
        Caption = '&Copiar'
        OnClick = Copiar1Click
      end
    end
    object Recibo1: TMenuItem
      Caption = '&Recibo'
      OnClick = Recibo1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Mudacarteiradelanamentosselecionados1: TMenuItem
      Caption = '&Muda lan'#231'amentos selecionados'
      object Carteiras1: TMenuItem
        Caption = '&Carteira'
        OnClick = Carteiras1Click
      end
      object Conta1: TMenuItem
        Caption = 'Co&nta'
        OnClick = Conta1Click
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object Descrio1: TMenuItem
        Caption = '&Descri'#231#227'o'
        OnClick = Descrio1Click
      end
      object N9: TMenuItem
        Caption = '-'
      end
      object Data1: TMenuItem
        Caption = '&Data'
        OnClick = Data1Click
      end
      object Vencimento2: TMenuItem
        Caption = '&Vencimento'
        OnClick = Vencimento2Click
      end
      object Compensao1: TMenuItem
        Caption = 'C&ompensa'#231#227'o'
        OnClick = Compensao1Click
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object Transformaremitemdebloqueto1: TMenuItem
        Caption = '&Transformar em item de bloqueto'
        OnClick = Transformaremitemdebloqueto1Click
      end
      object Acertarcreditopelovalorpago1: TMenuItem
        Caption = '&Acertar credito pelo valor pago (juro / multa negativos)'
        OnClick = Acertarcreditopelovalorpago1Click
      end
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Importaes1: TMenuItem
      Caption = '&Importa'#231#245'es'
      object Salrios1: TMenuItem
        Caption = '&Sal'#225'rios'
        object Modelo01RelExactus1: TMenuItem
          Caption = 'Modelo 0&1 - Rel. Exactus'
          OnClick = Modelo01RelExactus1Click
        end
        object Modelo02txtPholha1: TMenuItem
          Caption = 'Modelo 0&2 - txt Pholha'
          OnClick = Modelo02txtPholha1Click
        end
      end
    end
    object N18: TMenuItem
      Caption = '-'
    end
    object Exclusoincondicional1: TMenuItem
      Caption = 'Exclus'#227'o incondicional'
      OnClick = Exclusoincondicional1Click
    end
    object Lanamentos1: TMenuItem
      Caption = 'Lan'#231'amentos exclu'#237'dos'
      OnClick = Lanamentos1Click
    end
    object N14: TMenuItem
      Caption = '-'
    end
    object Correes1: TMenuItem
      Caption = '&Corre'#231#245'es'
      object Colocarmsdecompetnciaondenotem1: TMenuItem
        Caption = 'Colocar m'#234's de compet'#234'ncia onde n'#227'o tem'
        object Datalancto1: TMenuItem
          Caption = '&Data lancto'
          OnClick = Datalancto1Click
        end
        object Vencimento1: TMenuItem
          Caption = '&Vencimento'
          OnClick = Vencimento1Click
        end
        object MsanterioraoVencimento1: TMenuItem
          Caption = '&M'#234's anterior ao Vencimento '
          OnClick = MsanterioraoVencimento1Click
        end
      end
      object Lanamentoscomproblemas1: TMenuItem
        Caption = '&Lan'#231'amentos com problemas'
        OnClick = Lanamentoscomproblemas1Click
      end
    end
    object MudarvencimentoLocao1: TMenuItem
      Caption = 'Mudar vencimento (Loca'#231#227'o)'
      OnClick = MudarvencimentoLocao1Click
    end
  end
  object PMQuita: TPopupMenu
    OnPopup = PMQuitaPopup
    Left = 489
    Top = 174
    object Compensar2: TMenuItem
      Caption = '&Compensar na conta corrente (Banco)'
      OnClick = Compensar2Click
    end
    object Pagar2: TMenuItem
      Caption = '&Pagar / Rolar com emiss'#227'o  (cheque, etc)'
      OnClick = Pagar2Click
    end
    object Pagamentorpido2: TMenuItem
      Caption = 'Pagamento &r'#225'pido'
      OnClick = Pagamentorpido2Click
    end
    object Pagtorpidoselbanco1: TMenuItem
      Caption = 'Pagamento r'#225'pido selecinando o banco'
      OnClick = Pagtorpidoselbanco1Click
    end
    object N13: TMenuItem
      Caption = '-'
    end
    object Reverter2: TMenuItem
      Caption = 'D&esfazer compensa'#231#227'o'
      OnClick = Reverter2Click
    end
    object N12: TMenuItem
      Caption = '-'
    end
    object Localizarlanamentoorigem2: TMenuItem
      Caption = '&Localizar lan'#231'amento origem'
      OnClick = Localizarlanamentoorigem2Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object PagarAVista2: TMenuItem
      Caption = 'Pagar em carteira para um cai&xa'
      OnClick = PagarAVista2Click
    end
  end
  object PMConfPagtosCad: TPopupMenu
    Left = 427
    Top = 259
    object ContasMensaisCadastrodePagamentos1: TMenuItem
      Caption = 'Contas Mensais - Cadastro de &Pagamentos'
      OnClick = ContasMensaisCadastrodePagamentos1Click
    end
    object ContasMensaisCadastrodeEmisses1: TMenuItem
      Caption = 'Contas Mensais - Cadastro de &Emiss'#245'es'
      OnClick = ContasMensaisCadastrodeEmisses1Click
    end
  end
  object PMConfPgtosExe: TPopupMenu
    Left = 469
    Top = 258
    object Pagamentosexecutados1: TMenuItem
      Caption = 'Contas Mensais - Confer'#234'ncia de &Pagamentos'
      OnClick = Pagamentosexecutados1Click
    end
    object Contasmensais1: TMenuItem
      Caption = 'Contas Mensais - &Confer'#234'ncia de Emiss'#245'es'
      OnClick = Contasmensais1Click
    end
  end
  object PMBloqueto: TPopupMenu
    OnPopup = PMBloquetoPopup
    Left = 624
    Top = 256
    object Gerabloqueto1: TMenuItem
      Caption = '&Gera bloqueto'
      OnClick = Gerabloqueto1Click
    end
    object ImprimeGerenciabloqueto1: TMenuItem
      Caption = '&Imprime / Gerencia bloqueto'
      OnClick = ImprimeGerenciabloqueto1Click
    end
  end
  object PMProtocolo: TPopupMenu
    Left = 356
    Top = 260
    object Adicionalanamentos1: TMenuItem
      Caption = '&Adiciona lan'#231'amentos'
      object Atual4: TMenuItem
        Caption = '&Atual'
        OnClick = Atual4Click
      end
      object Selecionados5: TMenuItem
        Caption = '&Selecionados'
        object UmporDocumento1: TMenuItem
          Caption = 'Um por &Documento '
          OnClick = UmporDocumento1Click
        end
        object UmparacadaLanamento1: TMenuItem
          Caption = 'Um para cada &Lan'#231'amento'
          OnClick = UmparacadaLanamento1Click
        end
      end
    end
    object Localizalote1: TMenuItem
      Caption = '&Localiza lote'
      OnClick = Localizalote1Click
    end
  end
  object PMSalvaPosicaoGrade: TPopupMenu
    Left = 752
    Top = 104
    object SalvarOrdenaodascolunasdagrade2: TMenuItem
      Caption = '&Salvar ordena'#231#227'o das colunas da grade'
      OnClick = SalvarOrdenaodascolunasdagrade2Click
    end
    object CarregarOrdenaopadrodascolunasdagrade1: TMenuItem
      Caption = '&Carregar ordena'#231#227'o padr'#227'o das colunas da grade'
      OnClick = CarregarOrdenaopadrodascolunasdagrade1Click
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 12
    Top = 12
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 40
    Top = 12
  end
  object PMRelatorios: TPopupMenu
    Left = 216
    Top = 312
    object Balancete1: TMenuItem
      Caption = 'Balancete'
      OnClick = Balancete1Click
    end
    object Extratos1: TMenuItem
      Caption = 'Extratos'
      object PagarReceber2: TMenuItem
        Caption = 'Pagar/Receber (antigo)'
        Visible = False
        OnClick = PagarReceber2Click
      end
      object PagarReceber31: TMenuItem
        Caption = 'Pagar/Receber'
        OnClick = PagarReceber31Click
      end
      object Movimento2: TMenuItem
        Caption = 'Movimento'
        OnClick = Movimento2Click
      end
      object ResultadosMensais2: TMenuItem
        Caption = 'Resultados Mensais'
        Visible = False
        OnClick = ResultadosMensais2Click
      end
      object ReceitaseDespesas21: TMenuItem
        Caption = '&Receitas e Despesas'
        object ModeloA2: TMenuItem
          Caption = 'Modelo A'
          OnClick = ModeloA2Click
        end
        object ModeloB2: TMenuItem
          Caption = 'Modelo B'
          OnClick = ModeloB2Click
        end
      end
      object PrestaodeContas1: TMenuItem
        Caption = 'Presta'#231#227'o de &Contas'
        OnClick = PrestaodeContas1Click
      end
    end
    object Pesquisas1: TMenuItem
      Caption = 'Pesquisas'
      object PorNveldoPLanodeContas2: TMenuItem
        Caption = 'Por N'#237'vel do PLano de Contas'
        object ModeloA1: TMenuItem
          Caption = 'Modelo A'
          OnClick = ModeloA1Click
        end
        object ModeloB1: TMenuItem
          Caption = 'Modelo B'
          OnClick = ModeloB1Click
        end
      end
      object ContasControladas2: TMenuItem
        Caption = 'Contas Controladas'
        OnClick = ContasControladas2Click
      end
      object Contassazonais2: TMenuItem
        Caption = 'Contas sazonais'
        OnClick = Contassazonais2Click
      end
      object MovimentoPlanodecontas1: TMenuItem
        Caption = 'Movimento (Plano de contas)'
        OnClick = MovimentoPlanodecontas1Click
      end
    end
    object Saldos1: TMenuItem
      Caption = 'Saldos'
      object Futuros2: TMenuItem
        Caption = 'Futuros'
        OnClick = Futuros2Click
      end
      object Em2: TMenuItem
        Caption = 'Em....'
        OnClick = Em2Click
      end
      object N15: TMenuItem
        Caption = '-'
      end
      object Saldos2: TMenuItem
        Caption = 'Saldos'
        OnClick = Saldos2Click
      end
    end
    object Listas1: TMenuItem
      Caption = 'Listas'
      object Contas2: TMenuItem
        Caption = 'Contas'
        OnClick = Contas2Click
      end
    end
    object Diversos1: TMenuItem
      Caption = 'Diversos'
      object Etiquetas2: TMenuItem
        Caption = 'Etiquetas'
        OnClick = Etiquetas2Click
      end
      object Formulrios2: TMenuItem
        Caption = 'Formul'#225'rios'
        OnClick = Formulrios2Click
      end
      object Mensalidades2: TMenuItem
        Caption = 'Mensalidades'
        OnClick = Mensalidades2Click
      end
    end
    object Receitasedespesas1: TMenuItem
      Caption = 'Receitas e despesas'
      OnClick = Receitasedespesas1Click
    end
    object Relatriodecheques1: TMenuItem
      Caption = 'Relat'#243'rio de cheques'
      OnClick = Relatriodecheques1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Eventos1: TMenuItem
      Caption = 'Eventos'
      OnClick = Eventos1Click
    end
  end
  object PMCopiaCH: TPopupMenu
    Left = 300
    Top = 308
    object Cpiadecheque1: TMenuItem
      Caption = 'C'#243'pia de &cheque (Apenas um lan'#231'amento)'
      OnClick = Cpiadecheque1Click
    end
    object CpiadechequeNovo1: TMenuItem
      Caption = 'C'#243'pia de cheque (Diversos lan'#231'amentos)'
      OnClick = CpiadechequeNovo1Click
    end
    object Cpiadedbitoemconta1: TMenuItem
      Caption = 'C'#243'pia de d'#233'bito em conta'
      OnClick = Cpiadedbitoemconta1Click
    end
    object N23: TMenuItem
      Caption = '-'
    end
    object Cpiaautomtica1: TMenuItem
      Caption = 'C'#243'pia autom'#225'tica'
      OnClick = Cpiaautomtica1Click
    end
  end
  object PMProvisoes: TPopupMenu
    Left = 360
    Top = 392
    object Agendamentodeproviso1: TMenuItem
      Caption = 'Agendamento de provis'#227'o'
      OnClick = Agendamentodeproviso1Click
    end
    object Pesquisaagendamentodeprovisoefetivado1: TMenuItem
      Caption = 'Pesquisa agendamento de provis'#227'o efetivado'
      OnClick = Pesquisaagendamentodeprovisoefetivado1Click
    end
  end
  object PMRecibo: TPopupMenu
    OnPopup = PMReciboPopup
    Left = 536
    Top = 248
    object partirdolanamentoselecionado1: TMenuItem
      Caption = 'Lan'#231'amento selecionado'
      OnClick = partirdolanamentoselecionado1Click
    end
    object Avulso1: TMenuItem
      Caption = 'Avulso'
      OnClick = Avulso1Click
    end
    object Multiplo1: TMenuItem
      Caption = 'M'#250'ltiplo (Recebe e imprime)'
      object Recebepagamentoeimprime1: TMenuItem
        Caption = 'Recebe pagamento e imprime o  Recibo'
        OnClick = Recebepagamentoeimprime1Click
      end
      object JaneladeRecibos1: TMenuItem
        Caption = 'Ir para Janela de Recibos'
        OnClick = JaneladeRecibos1Click
      end
      object ImprimirReciboCriado1: TMenuItem
        Caption = 'Imprimir Recibo Criado'
        OnClick = ImprimirReciboCriado1Click
      end
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 440
    Top = 382
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 468
    Top = 382
  end
  object QrRecibos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Controle '
      'FROM reciboimpori'
      'WHERE LctCtrl>0'
      ''
      'UNION'
      ''
      'SELECT Codigo, Controle '
      'FROM reciboimpdst'
      'WHERE LctCtrl>0')
    Left = 584
    Top = 404
    object QrRecibosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRecibosControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsRecibos: TDataSource
    DataSet = QrRecibos
    Left = 584
    Top = 456
  end
end
