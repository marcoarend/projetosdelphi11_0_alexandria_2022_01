object FmPreLctExe: TFmPreLctExe
  Left = 339
  Top = 185
  Caption = 'FIN-PRELC-003 :: Gera'#231#227'o de Lan'#231'amentos Pr'#233' Cadastrados'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 525
        Height = 32
        Caption = 'Gera'#231#227'o de Lan'#231'amentos Pr'#233' Cadastrados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 525
        Height = 32
        Caption = 'Gera'#231#227'o de Lan'#231'amentos Pr'#233' Cadastrados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 525
        Height = 32
        Caption = 'Gera'#231#227'o de Lan'#231'amentos Pr'#233' Cadastrados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 456
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 456
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 456
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 30
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LaEmissIni: TLabel
            Left = 12
            Top = 8
            Width = 99
            Height = 13
            Caption = 'Data do lan'#231'amento:'
          end
          object TPData: TdmkEditDateTimePicker
            Left = 116
            Top = 4
            Width = 112
            Height = 21
            Time = 0.516375590297684500
            TabOrder = 0
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object DGDados: TdmkDBGridZTO
          Left = 2
          Top = 45
          Width = 808
          Height = 409
          Align = alClient
          DataSource = DsPreLctIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Carteira'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Carteira'
              Title.Caption = 'Nome da carteira'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cliente'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fornecedor'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMETERCEIRO'
              Title.Caption = 'Nome do cliente'
              Width = 280
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Genero'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECONTA'
              Title.Caption = 'Nome da conta do plano de contas'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DiaVencto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Mez'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 504
    Width = 812
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 808
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtLanca: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Lan'#231'a'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtLancaClick
      end
    end
  end
  object QrPreLctIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.*, ca.Nome NO_Carteira, '
      'ct.Nome NOMECONTA, '
      'IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,'
      'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR, '
      'IF(Cliente<>0, IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome), '
      'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome)) NOMETERCEIRO '
      'FROM prelctits la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'LEFT JOIN contas ct    ON ct.Codigo=la.Genero '
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'WHERE la.Codigo=0'
      'ORDER BY la.Controle')
    Left = 436
    Top = 5
    object QrPreLctItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreLctItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPreLctItsCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrPreLctItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPreLctItsDiaVencto: TIntegerField
      FieldName = 'DiaVencto'
    end
    object QrPreLctItsCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPreLctItsCliente: TIntegerField
      FieldName = 'Cliente'
      DisplayFormat = '0;-0; '
    end
    object QrPreLctItsFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      DisplayFormat = '0;-0; '
    end
    object QrPreLctItsMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPreLctItsDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPreLctItsCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPreLctItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPreLctItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPreLctItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPreLctItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPreLctItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPreLctItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPreLctItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPreLctItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPreLctItsNO_Carteira: TWideStringField
      FieldName = 'NO_Carteira'
      Size = 100
    end
    object QrPreLctItsNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrPreLctItsNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrPreLctItsNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrPreLctItsNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Size = 100
    end
    object QrPreLctItsNOMETERCEIRO: TWideStringField
      FieldName = 'NOMETERCEIRO'
      Size = 100
    end
  end
  object DsPreLctIts: TDataSource
    DataSet = QrPreLctIts
    Left = 436
    Top = 53
  end
  object PMLanca: TPopupMenu
    Left = 152
    Top = 372
    object odos1: TMenuItem
      Caption = '&Todos'
      OnClick = odos1Click
    end
    object Selecionados1: TMenuItem
      Caption = '&Selecionados'
      OnClick = Selecionados1Click
    end
  end
  object QrCarteira: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, '
      'Tipo, ExigeNumCheque, ForneceI, Banco1, UsaTalao'
      'FROM carteiras '
      'WHERE Codigo=0')
    Left = 476
    Top = 412
    object QrCarteiraCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteiraNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteiraFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteiraFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteiraPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteiraTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteiraExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCarteiraForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCarteiraBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCarteiraUsaTalao: TSmallintField
      FieldName = 'UsaTalao'
    end
  end
  object QrConta: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.*'
      'FROM contas co'
      'WHERE co.Codigo>0')
    Left = 352
    Top = 320
    object QrContaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContaNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrContaNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrContaNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 50
    end
    object QrContaID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContaSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
    end
    object QrContaCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object QrContaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrContaCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContaDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContaMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContaExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContaMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContaMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContaMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContaMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContaMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContaTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContaExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContaRateio: TIntegerField
      FieldName = 'Rateio'
    end
    object QrContaEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrContaAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrContaPendenMesSeg: TSmallintField
      FieldName = 'PendenMesSeg'
    end
    object QrContaCalculMesSeg: TSmallintField
      FieldName = 'CalculMesSeg'
    end
    object QrContaOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object QrContaContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object QrContaContasSum: TIntegerField
      FieldName = 'ContasSum'
    end
    object QrContaCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
    object QrContaNotPrntBal: TIntegerField
      FieldName = 'NotPrntBal'
    end
    object QrContaSigla: TWideStringField
      FieldName = 'Sigla'
    end
    object QrContaProvRat: TSmallintField
      FieldName = 'ProvRat'
    end
    object QrContaNotPrntFin: TIntegerField
      FieldName = 'NotPrntFin'
    end
    object QrContaTemDocFisi: TSmallintField
      FieldName = 'TemDocFisi'
    end
    object QrContaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrContaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
end
