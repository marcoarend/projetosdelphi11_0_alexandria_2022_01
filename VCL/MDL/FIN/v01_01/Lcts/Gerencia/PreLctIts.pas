unit PreLctIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmPreLctIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdDescricao: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrFornecedores: TmySQLQuery;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOMEENTIDADE: TWideStringField;
    DsFornecedores: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasUsaTalao: TSmallintField;
    DsCarteiras: TDataSource;
    LaCarteira: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesAccount: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    Label2: TLabel;
    EdGenero: TdmkEditCB;
    CBGenero: TdmkDBLookupComboBox;
    LaCliente: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdFornecedor: TdmkEditCB;
    LaFornecedor: TLabel;
    CBFornecedor: TdmkDBLookupComboBox;
    EdDebito: TdmkEdit;
    LaDeb: TLabel;
    LaCred: TLabel;
    EdCredito: TdmkEdit;
    RGDiaVencto: TRadioGroup;
    RGMez: TRadioGroup;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    QrContasNOMEPLANO: TWideStringField;
    DsContas: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPreLctIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    FCliInt: Integer;
  end;

  var
  FmPreLctIts: TFmPreLctIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmPreLctIts.BtOKClick(Sender: TObject);
var
  Descricao: String;
  Codigo, Controle, Carteira, Genero, Cliente, Fornecedor, Mez, DiaVencto: Integer;
  Debito, Credito: Double;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Carteira       := EdCarteira.ValueVariant;
  DiaVencto      := RGDiaVencto.ItemIndex + 1;
  Genero         := EdGenero.ValueVariant;
  //CliInt         := ;
  Cliente        := EdCliente.ValueVariant;
  Fornecedor     := EdFornecedor.ValueVariant;
  Mez            := RGMez.ItemIndex;
  Debito         := EdDebito.ValueVariant;
  Credito        := EdCredito.ValueVariant;
  Descricao      := EdDescricao.Text;

  //
  if MyObjects.FIC(Carteira = 0, EdCarteira, 'Informe a carteira!') then
    Exit;
  if MyObjects.FIC(Genero = 0, EdGenero, 'Informe uma descri��o!') then
    Exit;
  Controle := UMyMod.BPGS1I32('prelctits', 'Controle', '', '', tsPos,
    ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'prelctits', False, [
  'Codigo', 'Carteira', 'DiaVencto',
  'Genero', 'Cliente',
  'Fornecedor', 'Mez', 'Debito',
  'Credito', 'Descricao'], [
  'Controle'], [
  Codigo, Carteira, DiaVencto,
  Genero, Cliente,
  Fornecedor, Mez, Debito,
  Credito, Descricao], [
  Controle], True) then
  begin
    ReopenPreLctIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdCarteira.SetFocus;
    end else Close;
  end;
end;

procedure TFmPreLctIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPreLctIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmPreLctIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornecedores, Dmod.MyDB);
end;

procedure TFmPreLctIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPreLctIts.ReopenPreLctIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
