unit LctAtrelaFat;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, dmkEditDateTimePicker;

type
  TFmLctAtrelaFat = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Panel3: TPanel;
    QrPFornece: TMySQLQuery;
    QrPForneceCodigo: TIntegerField;
    QrPForneceNOMEENT: TWideStringField;
    DsPFornece: TDataSource;
    QrPCliente: TMySQLQuery;
    QrPClienteCodigo: TIntegerField;
    QrPClienteNOMEENT: TWideStringField;
    DsPCliente: TDataSource;
    CkDataIni: TCheckBox;
    TPDataIni: TdmkEditDateTimePicker;
    CkDataFim: TCheckBox;
    TPDataFim: TdmkEditDateTimePicker;
    CkFornece: TCheckBox;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    CkCliente: TCheckBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Bevel1: TBevel;
    CkDebito: TCheckBox;
    EdDebMin: TdmkEdit;
    EdDebMax: TdmkEdit;
    CkCredito: TCheckBox;
    Bevel2: TBevel;
    EdCredMin: TdmkEdit;
    EdCredMax: TdmkEdit;
    CkNF: TCheckBox;
    EdNF: TdmkEdit;
    CkDuplicata: TCheckBox;
    EdDuplicata: TdmkEdit;
    CkDoc: TCheckBox;
    EdDoc: TdmkEdit;
    CkControle: TCheckBox;
    EdControle: TdmkEdit;
    QrPesq: TMySQLQuery;
    QrPesqData: TDateField;
    QrPesqTipo: TSmallintField;
    QrPesqCarteira: TIntegerField;
    QrPesqControle: TIntegerField;
    QrPesqSub: TSmallintField;
    QrPesqAutorizacao: TIntegerField;
    QrPesqGenero: TIntegerField;
    QrPesqQtde: TFloatField;
    QrPesqDescricao: TWideStringField;
    QrPesqSerieNF: TWideStringField;
    QrPesqNotaFiscal: TIntegerField;
    QrPesqDebito: TFloatField;
    QrPesqCredito: TFloatField;
    QrPesqCompensado: TDateField;
    QrPesqSerieCH: TWideStringField;
    QrPesqDocumento: TFloatField;
    QrPesqSit: TIntegerField;
    QrPesqVencimento: TDateField;
    QrPesqFatID: TIntegerField;
    QrPesqFatID_Sub: TIntegerField;
    QrPesqFatNum: TFloatField;
    QrPesqFatParcela: TIntegerField;
    QrPesqID_Pgto: TIntegerField;
    QrPesqID_Quit: TIntegerField;
    QrPesqID_Sub: TSmallintField;
    QrPesqFatura: TWideStringField;
    QrPesqEmitente: TWideStringField;
    QrPesqBanco: TIntegerField;
    QrPesqAgencia: TIntegerField;
    QrPesqContaCorrente: TWideStringField;
    QrPesqCNPJCPF: TWideStringField;
    QrPesqLocal: TIntegerField;
    QrPesqCartao: TIntegerField;
    QrPesqLinha: TIntegerField;
    QrPesqOperCount: TIntegerField;
    QrPesqLancto: TIntegerField;
    QrPesqPago: TFloatField;
    QrPesqMez: TIntegerField;
    QrPesqFornecedor: TIntegerField;
    QrPesqCliente: TIntegerField;
    QrPesqCliInt: TIntegerField;
    QrPesqForneceI: TIntegerField;
    QrPesqMoraDia: TFloatField;
    QrPesqMulta: TFloatField;
    QrPesqMoraVal: TFloatField;
    QrPesqMultaVal: TFloatField;
    QrPesqProtesto: TDateField;
    QrPesqDataDoc: TDateField;
    QrPesqCtrlIni: TIntegerField;
    QrPesqNivel: TIntegerField;
    QrPesqVendedor: TIntegerField;
    QrPesqAccount: TIntegerField;
    QrPesqICMS_P: TFloatField;
    QrPesqICMS_V: TFloatField;
    QrPesqDuplicata: TWideStringField;
    QrPesqDepto: TIntegerField;
    QrPesqDescoPor: TIntegerField;
    QrPesqDescoVal: TFloatField;
    QrPesqDescoControle: TIntegerField;
    QrPesqUnidade: TIntegerField;
    QrPesqNFVal: TFloatField;
    QrPesqAntigo: TWideStringField;
    QrPesqExcelGru: TIntegerField;
    QrPesqDoc2: TWideStringField;
    QrPesqCNAB_Sit: TSmallintField;
    QrPesqTipoCH: TSmallintField;
    QrPesqReparcel: TIntegerField;
    QrPesqAtrelado: TIntegerField;
    QrPesqPagMul: TFloatField;
    QrPesqPagJur: TFloatField;
    QrPesqRecDes: TFloatField;
    QrPesqSubPgto1: TIntegerField;
    QrPesqMultiPgto: TIntegerField;
    QrPesqProtocolo: TIntegerField;
    QrPesqCtrlQuitPg: TIntegerField;
    QrPesqEndossas: TSmallintField;
    QrPesqEndossan: TFloatField;
    QrPesqEndossad: TFloatField;
    QrPesqCancelado: TSmallintField;
    QrPesqEventosCad: TIntegerField;
    QrPesqEncerrado: TIntegerField;
    QrPesqErrCtrl: TIntegerField;
    QrPesqIndiPag: TIntegerField;
    QrPesqCentroCusto: TIntegerField;
    QrPesqFatParcRef: TIntegerField;
    QrPesqFatSit: TSmallintField;
    QrPesqFatSitSub: TSmallintField;
    QrPesqFatGrupo: TIntegerField;
    QrPesqTaxasVal: TFloatField;
    QrPesqFisicoSrc: TSmallintField;
    QrPesqFisicoCod: TIntegerField;
    QrPesqTemCROLct: TSmallintField;
    QrPesqlanctos: TLargeintField;
    QrPesqQtd2: TFloatField;
    QrPesqVctoOriginal: TDateField;
    QrPesqModeloNF: TWideStringField;
    QrPesqHoraCad: TTimeField;
    QrPesqHoraAlt: TTimeField;
    QrPesqGenCtb: TIntegerField;
    QrPesqQtDtPg: TIntegerField;
    QrPesqGenCtbD: TIntegerField;
    QrPesqGenCtbC: TIntegerField;
    DsPesq: TDataSource;
    DBGLct: TdmkDBGridZTO;
    BtPesquisa: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdDebMinExit(Sender: TObject);
    procedure EdCredMinExit(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure DBGLctDblClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure SelecionaItem();
  public
    { Public declarations }
    FTabLct: String;
    FFatID, FFatNum, FFatParcela: Integer;
  end;

  var
  FmLctAtrelaFat: TFmLctAtrelaFat;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkProcFunc, UMySQLModule;

{$R *.DFM}

procedure TFmLctAtrelaFat.BtOKClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmLctAtrelaFat.BtPesquisaClick(Sender: TObject);
var
  Fornece, Cliente: Integer;
  DataIni, DataFim: TDateTime;
  DebMin, DebMax, CredMin, CredMax: Double;
  //
  SQL_Periodo, SQL_Fornece, SQL_Cliente, SQL_Debito, SQL_Credito: String;
begin
  Fornece := EdFornece.ValueVariant;
  Cliente := EdCliente.ValueVariant;
  DebMin  := EdDebMin.ValueVariant;
  DebMax  := EdDebMax.ValueVariant;
  CredMin := EdCredMin.ValueVariant;
  CredMax := EdCredMax.ValueVariant;
  //
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE Data BETWEEN ', TPDataIni.Date,
    TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked);
  //
  if CkFornece.Checked then
    SQL_Fornece := 'AND Fornecedor=' + Geral.FF0(Fornece);
  //
  if CkCliente.Checked then
    SQL_Cliente := 'AND Cliente=' + Geral.FF0(Cliente);
  //
  if CkDebito.Checked then
    SQL_Debito := 'AND Debito BETWEEN ' + Geral.FFT(DebMin, 2, siNegativo) +
      ' AND ' + Geral.FFT(DebMax, 2, siNegativo);
  //
  if CkCredito.Checked then
    SQL_Credito := 'AND Credito BETWEEN ' + Geral.FFT(DebMin, 2, siNegativo) +
      ' AND ' + Geral.FFT(DebMax, 2, siNegativo);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
  'SELECT *  ',
  'FROM ' + FTabLct,
  SQL_Periodo,
  SQL_Fornece,
  SQL_Cliente,
  SQL_Debito,
  SQL_Credito,
  '']);
end;

procedure TFmLctAtrelaFat.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctAtrelaFat.DBGLctDblClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmLctAtrelaFat.EdCredMinExit(Sender: TObject);
begin
   if Geral.DMV(EdCredMax.Text) < Geral.DMV(EdCredMin.Text) then
     EdCredMax.Text := EdCredMin.Text;
end;

procedure TFmLctAtrelaFat.EdDebMinExit(Sender: TObject);
begin
   if Geral.DMV(EdDebMax.Text) < Geral.DMV(EdDebMin.Text) then
     EdDebMax.Text := EdDebMin.Text;
end;

procedure TFmLctAtrelaFat.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctAtrelaFat.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmLctAtrelaFat.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctAtrelaFat.SelecionaItem();
var
  Data(*, Descricao, SerieNF, Compensado, SerieCH, Vencimento, Fatura, Emitente, ContaCorrente, CNPJCPF, Protesto, DataDoc, Duplicata, Antigo, Doc2, VctoOriginal, ModeloNF, HoraCad, HoraAlt*): String;
  Tipo, Carteira, Controle, Sub, FatID, FatID_Sub, FatNum, FatParcela: Integer;
  //Autorizacao, Genero, NotaFiscal, Sit, ID_Pgto, ID_Quit, ID_Sub, Banco, Agencia, Local, Cartao, Linha, OperCount, Lancto, Mez, Fornecedor, Cliente, CliInt, ForneceI, CtrlIni, Nivel, Vendedor, Account, Depto, DescoPor, DescoControle, Unidade, ExcelGru, CNAB_Sit, TipoCH, Reparcel, Atrelado, SubPgto1, MultiPgto, Protocolo, CtrlQuitPg, Endossas, Cancelado, EventosCad, Encerrado, ErrCtrl, IndiPag, CentroCusto, FatParcRef, FatSit, FatSitSub, FatGrupo, FisicoSrc, FisicoCod, TemCROLct, GenCtb, QtDtPg, GenCtbD, GenCtbC: Integer;
  //Qtde, Debito, Credito, Documento,
  //Pago, MoraDia, Multa, MoraVal, MultaVal, ICMS_P, ICMS_V, DescoVal, NFVal, PagMul, PagJur, RecDes, Endossan, Endossad, TaxasVal, lanctos, Qtd2: Double;
  SQLType: TSQLType;
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
  begin
    if (QrPesqFatID.Value = 0) or (QrPesqFatNum.Value = 0) then
    begin
      Data       := Geral.FDT(QrPesqData.Value, 1);
      Tipo       := QrPesqTipo.Value;
      Carteira   := QrPesqCarteira.Value;
      Controle   := QrPesqControle.Value;
      Sub        := QrPesqSub.Value;
      //
      FatID      := FFatID;
      FatID_Sub  := 0;
      FatNum     := FFatNum;
      FatParcela := FFatParcela;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, FTabLct, False, [
      'FatID', 'FatID_Sub', 'FatNum',
      'FatParcela'], [
      'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
      FatID, FatID_Sub, FatNum,
      FatParcela], [
      Data, Tipo, Carteira, Controle, Sub], True) then
      begin
        Close;
      end;
    end else
      Geral.MB_Aviso('Este item j� foi selecionado para o seguinte link:' +
      sLineBreak + 'Fat.ID = ' + Geral.FF0(QrPesqFatID.Value) + sLineBreak +
      'Fat. N� = ' + Geral.FF0(Trunc(QrPesqFatNum.Value)) + sLineBreak +
      'Parcela = ' + Geral.FF0(QrPesqFatParcela.Value) + sLineBreak);
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

end.
