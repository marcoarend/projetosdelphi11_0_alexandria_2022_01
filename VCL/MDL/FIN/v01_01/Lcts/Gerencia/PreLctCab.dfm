object FmPreLctCab: TFmPreLctCab
  Left = 368
  Top = 194
  Caption = 'FIN-PRELC-001 :: Pr'#233' Lan'#231'amentos'
  ClientHeight = 451
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 355
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 16
        Top = 54
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 513
        Height = 21
        MaxLength = 50
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 16
        Top = 70
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 72
        Top = 70
        Width = 517
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 292
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 898
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 355
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 57
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 400
        Top = 16
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
        FocusControl = DBEdit1
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPreLctCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 321
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsPreLctCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 400
        Top = 32
        Width = 56
        Height = 21
        DataField = 'CliInt'
        DataSource = DsPreLctCab
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 456
        Top = 32
        Width = 320
        Height = 21
        DataField = 'NO_CliInt'
        DataSource = DsPreLctCab
        TabOrder = 3
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 291
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Grupo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Item'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtLanca: TBitBtn
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lan'#231'a'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtLancaClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 105
      Width = 1008
      Height = 186
      Align = alClient
      DataSource = DsPreLctIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Carteira'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Carteira'
          Title.Caption = 'Nome da carteira'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cliente'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fornecedor'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETERCEIRO'
          Title.Caption = 'Nome do cliente'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECONTA'
          Title.Caption = 'Nome da conta do plano de contas'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DiaVencto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Mez'
          Visible = True
        end>
    end
    object Panel6: TPanel
      Left = 0
      Top = 57
      Width = 1008
      Height = 48
      Align = alTop
      TabOrder = 3
      object GroupBox1: TGroupBox
        Left = 1
        Top = 1
        Width = 1006
        Height = 46
        Align = alClient
        Caption = ' Filtros: '
        TabOrder = 0
        ExplicitHeight = 78
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 1002
          Height = 29
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitLeft = 208
          ExplicitTop = 36
          ExplicitWidth = 185
          ExplicitHeight = 41
          object EdTerceiro: TEdit
            Left = 116
            Top = 4
            Width = 225
            Height = 21
            TabOrder = 1
            OnChange = CkTerceiroClick
          end
          object EdValor: TdmkEdit
            Left = 416
            Top = 4
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = CkTerceiroClick
          end
          object CkValor: TCheckBox
            Left = 348
            Top = 6
            Width = 61
            Height = 17
            Caption = 'Valor:'
            TabOrder = 2
            OnClick = CkTerceiroClick
          end
          object CkTerceiro: TCheckBox
            Left = 8
            Top = 6
            Width = 97
            Height = 17
            Caption = 'Nome terceiro'
            TabOrder = 0
            OnClick = CkTerceiroClick
          end
          object RGDebito: TRadioButton
            Left = 504
            Top = 8
            Width = 49
            Height = 17
            Caption = 'D'#233'bito.'
            TabOrder = 4
            OnClick = CkTerceiroClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 212
        Height = 32
        Caption = 'Pr'#233' Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 212
        Height = 32
        Caption = 'Pr'#233' Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 212
        Height = 32
        Caption = 'Pr'#233' Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrPreLctCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPreLctCabBeforeOpen
    AfterOpen = QrPreLctCabAfterOpen
    BeforeClose = QrPreLctCabBeforeClose
    AfterScroll = QrPreLctCabAfterScroll
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_cab'
      'WHERE Codigo > 0')
    Left = 344
    Top = 5
    object QrPreLctCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreLctCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPreLctCabCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPreLctCabNO_CliInt: TWideStringField
      FieldName = 'NO_CliInt'
      Size = 100
    end
  end
  object DsPreLctCab: TDataSource
    DataSet = QrPreLctCab
    Left = 344
    Top = 49
  end
  object QrPreLctIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.*, ca.Nome NO_Carteira, '
      'ct.Nome NOMECONTA, '
      'IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,'
      'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR, '
      'IF(Cliente<>0, IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome), '
      'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome)) NOMETERCEIRO '
      'FROM prelctits la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'LEFT JOIN contas ct    ON ct.Codigo=la.Genero '
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'WHERE la.Codigo=0'
      'ORDER BY la.Controle')
    Left = 436
    Top = 5
    object QrPreLctItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreLctItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPreLctItsCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrPreLctItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPreLctItsDiaVencto: TIntegerField
      FieldName = 'DiaVencto'
    end
    object QrPreLctItsCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPreLctItsCliente: TIntegerField
      FieldName = 'Cliente'
      DisplayFormat = '0;-0; '
    end
    object QrPreLctItsFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      DisplayFormat = '0;-0; '
    end
    object QrPreLctItsMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPreLctItsDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPreLctItsCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPreLctItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPreLctItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPreLctItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPreLctItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPreLctItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPreLctItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPreLctItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPreLctItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPreLctItsNO_Carteira: TWideStringField
      FieldName = 'NO_Carteira'
      Size = 100
    end
    object QrPreLctItsNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrPreLctItsNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrPreLctItsNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrPreLctItsNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Size = 100
    end
    object QrPreLctItsNOMETERCEIRO: TWideStringField
      FieldName = 'NOMETERCEIRO'
      Size = 100
    end
    object QrPreLctItsVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object DsPreLctIts: TDataSource
    DataSet = QrPreLctIts
    Left = 436
    Top = 53
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Importa1: TMenuItem
      Caption = 'Importa'
      OnClick = Importa1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object frxFIN_PRELC_001: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41690.713452719910000000
    ReportOptions.LastChange = 41690.713452719910000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxFIN_PRELC_001GetValue
    Left = 568
    Top = 16
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPreLctIts
        DataSetName = 'frxDsPreLctIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        Height = 64.251997800000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line_PH_01: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 529.134199999999900000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 45.354360000000000000
          Width = 45.354360000000000000
          Height = 18.897637800000000000
          ShowHint = False
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 45.354360000000000000
          Top = 45.354360000000000000
          Width = 215.433112360000000000
          Height = 18.897637800000000000
          ShowHint = False
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente / Fornecedor')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 619.842920000000000000
          Top = 45.354360000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 563.149970000000100000
          Top = 45.354360000000000000
          Width = 56.692852360000010000
          Height = 18.897637800000000000
          ShowHint = False
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 260.787570000000000000
          Top = 45.354360000000000000
          Width = 151.181102360000000000
          Height = 18.897637800000000000
          ShowHint = False
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Conta do plano de contas')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 411.968770000000000000
          Top = 45.354360000000000000
          Width = 151.181102360000000000
          Height = 18.897637800000000000
          ShowHint = False
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 351.496290000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        Height = 17.007874020000000000
        Top = 185.196970000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPreLctIts
        DataSetName = 'frxDsPreLctIts'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 45.354360000000000000
          Height = 17.007874015748030000
          ShowHint = False
          DataField = 'Controle'
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPreLctIts."Controle"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 45.354360000000000000
          Width = 215.433112360000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NOMETERCEIRO'
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPreLctIts."NOMETERCEIRO"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 17.007874015748030000
          ShowHint = False
          DataField = 'VALOR'
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPreLctIts."VALOR"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 563.149970000000100000
          Width = 56.692852360000010000
          Height = 17.007874015748030000
          ShowHint = False
          DataField = 'DiaVencto'
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPreLctIts."DiaVencto"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 260.787570000000000000
          Width = 151.181102360000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPreLctIts."NOMECONTA"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 411.968770000000000000
          Width = 151.181102360000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'Descricao'
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPreLctIts."Descricao"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 143.622140000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPreLctIts."Carteira"'
        object Memo13: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            
              'Carteira: [frxDsPreLctIts."Carteira"] - [frxDsPreLctIts."NO_Cart' +
              'eira"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 17.007874020000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        object Memo5: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 17.007874015748030000
          ShowHint = False
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPreLctIts."VALOR">)]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Width = 616.063390000000000000
          Height = 17.007874015748030000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Total da carteira [frxDsPreLctIts."NO_Carteira"]: ')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 22.677180000000000000
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        object Memo14: TfrxMemoView
          Left = 619.842920000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DataSet = frxDsPreLctIts
          DataSetName = 'frxDsPreLctIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPreLctIts."VALOR">)]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Top = 3.779530000000022000
          Width = 616.063390000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Total Geral: ')
          ParentFont = False
        end
      end
    end
  end
  object frxDsPreLctIts: TfrxDBDataset
    UserName = 'frxDsPreLctIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Carteira=Carteira'
      'Genero=Genero'
      'DiaVencto=DiaVencto'
      'CliInt=CliInt'
      'Cliente=Cliente'
      'Fornecedor=Fornecedor'
      'Mez=Mez'
      'Debito=Debito'
      'Credito=Credito'
      'Descricao=Descricao'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_Carteira=NO_Carteira'
      'NOMECONTA=NOMECONTA'
      'NOMEEMPRESA=NOMEEMPRESA'
      'NOMECLIENTE=NOMECLIENTE'
      'NOMEFORNECEDOR=NOMEFORNECEDOR'
      'NOMETERCEIRO=NOMETERCEIRO'
      'VALOR=VALOR')
    DataSet = QrPreLctIts
    BCDToCurrency = False
    Left = 568
    Top = 64
  end
end
