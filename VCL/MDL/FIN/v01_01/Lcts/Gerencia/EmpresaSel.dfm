object FmEmpresaSel: TFmEmpresaSel
  Left = 339
  Top = 185
  Caption = 'FIN-SELFG-000 :: Sele'#231#227'o de Empresa'
  ClientHeight = 509
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object dmkDBGrid1: TdmkDBGrid
    Left = 0
    Top = 153
    Width = 784
    Height = 242
    Align = alClient
    Columns = <
      item
        Expanded = False
        FieldName = 'Codigo'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Filial'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEFILIAL'
        Title.Caption = 'Raz'#227'o Social'
        Width = 615
        Visible = True
      end>
    Color = clWindow
    DataSource = DModG.DsEmpresas
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = dmkDBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Codigo'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Filial'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEFILIAL'
        Title.Caption = 'Raz'#227'o Social'
        Width = 615
        Visible = True
      end>
  end
  object Panel5: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 105
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 260
      Height = 105
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object RGLetraLct: TRadioGroup
        Left = 0
        Top = 0
        Width = 260
        Height = 105
        Align = alClient
        Caption = ' Fonte dos dados: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'a: Ativo'
          'b: Env'#233's'
          'c: Ativo + env'#233's'
          'd: Morto'
          'e: Env'#233's + Morto'
          'f: Tudo')
        TabOrder = 0
      end
    end
    object Panel3: TPanel
      Left = 260
      Top = 0
      Width = 524
      Height = 105
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 524
        Height = 52
        Align = alTop
        Caption = ' Pesquisa por descri'#231#227'o parcial: '
        TabOrder = 0
        object EdNome: TdmkEdit
          Left = 12
          Top = 20
          Width = 500
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdNomeChange
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 52
        Width = 524
        Height = 53
        Align = alClient
        Caption = ' Empresa: '
        TabOrder = 1
        object EdEmpresa: TdmkEditCB
          Left = 12
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 68
          Top = 20
          Width = 444
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 252
        Height = 32
        Caption = 'Sele'#231#227'o de Empresa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 252
        Height = 32
        Caption = 'Sele'#231#227'o de Empresa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 252
        Height = 32
        Caption = 'Sele'#231#227'o de Empresa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 439
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 395
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
end
