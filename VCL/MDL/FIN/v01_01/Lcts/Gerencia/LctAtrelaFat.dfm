object FmLctAtrelaFat: TFmLctAtrelaFat
  Left = 339
  Top = 185
  Caption = 'FIN-ATREL-001 :: Atrelamento de Lan'#231'amento Financeiro'
  ClientHeight = 544
  ClientWidth = 710
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 710
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 662
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Picture.Data = {
          07544269746D6170360C0000424D360C00000000000036000000280000002000
          0000200000000100180000000000000C00000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000BD946BBD7B39BD946B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000BD7B42DE8C29EF9C42DE8C29BD844200000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000BD7B4ADE8C29FFBD63FFB54AFFC66BDE8C31C6844A00000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000BD7B42DE8C29FFC663FFAD39FFA521FFB54AFFCE84DE9439DEB58C00
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00BD7B4ADE8C29FFC66BFFAD42FFA531FFAD39FFB54AFFC684EFAD63D6945200
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000BD7B
          4ADE8C31FFC673FFB54AFFAD31FFAD42FFB552FFC673FFD69CDE9442E7BD9400
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000BD844ADE8C
          31FFC67BFFB552FFAD42FFAD4AFFB55AFFC67BFFD6A5DE944ADEA56300000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000BD8442DE8C31FFCE
          84FFBD63FFB54AFFB552FFBD63FFC684FFD6A5DE944ADEA56300000000000000
          0000000000000000000000000000000000000000000000000000ADADA59C9C9C
          9C9C9C9C9C9CA59C9C9C9C9C9C9C9CA5A5A5B5B5B5AD7342DE8C39FFCE8CFFBD
          6BFFB55AFFBD63FFBD6BFFCE8CFFDEADDE944ADEA56300000000000000000000
          0000000000000000000000000000000000000000ADA5ADADA5ADBDBDBDCECECE
          DEDEDEE7E7E7E7E7E7DEDEDECECECEBDBDBDADA5AD9C7B5ADEB57BF7B56BFFB5
          63FFBD6BFFC673FFCE94FFDEB5DE944ADEA56B00000000000000000000000000
          0000000000000000000000000000000000A59CA5BDBDBDD6D6D6EFE7E7E7E7E7
          DEDEDEDEDEDEDEDEDEDEDEDEE7E7E7E7E7E7D6D6DEBDBDBDADADADCE9C63F7BD
          6BFFC684FFD69CFFDEB5DE944ADEA56300000000000000000000000000000000
          0000000000000000000000000000BDBDBDE7E7E7EFEFEFEFEFEFEFEFEFEFEFEF
          EFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFC6C6C6ADADADC69C
          84E7BDA5E7CEBDCE8C63CE947300000000000000000000000000000000000000
          0000000000000000000000A59CA5C6C6C6EFEFEFDEDEDEDEDEDEDED6C6E7BD7B
          F7AD39FF9C10FF9C10F7AD39EFBD7BDED6C6DEDEDEDEDEDEE7EFEFCEC6CEADAD
          ADE7CEADDE9452DEA56B00000000000000000000000000000000000000000000
          0000000000000000BDBDBDBDBDBDEFEFEFEFEFEFDEDEDEEFC684E79429E7A56B
          EFBD9CEFC684EFC684EFC684E7AD6BE79429EFC684DEDEDEEFEFEFEFEFEFADAD
          AD9C8473C68C7300000000000000000000000000000000000000000000000000
          0000000000000000ADADADDEDEDEE7E7E7E7DEDEE7CEA5FFAD31FFCE94FFE7CE
          FFDEBDFFD6ADFFD6ADFFDEBDFFE7CEFFCE94FFAD31E7CEA5DEDEDEE7E7E7DEDE
          DEADADAD00000000000000000000000000000000000000000000000000000000
          0000000000BDBDBDADADADEFEFEFDEDEDEF7EFDEE79431EFC684FFE7CEFFD6AD
          FFDEB5FFD6ADFFD6ADFFD6ADFFD6ADFFE7CEEFC684E79431DEDEDEEFEFEFEFEF
          EFADADADADADAD00000000000000000000000000000000000000000000000000
          0000000000ADADADD6D6D6EFEFEFE7E7E7F7C684FFBD63FFEFDEFFDEBDFFDEB5
          FFDEB5FFDEB5FFDEB5FFDEB5FFDEB5FFDEBDFFEFDEFFBD63EFC684E7E7E7EFEF
          EFD6D6D6B5B5B500000000000000000000000000000000000000000000000000
          0000000000ADADADE7E7E7EFEFEFEFEFEFE79C52FFCE94FFEFD6FFDEB5FFDEB5
          FFDEB5FFDEB5FFDEB5FFDEB5FFDEB5FFDEB5FFEFD6EFC684E79C52EFEFEFF7F7
          F7EFEFEFADADAD00000000000000000000000000000000000000000000000000
          0000000000ADADADF7F7F7EFEFEFE7EFE7F79C10FFEFD6FFEFD6FFE7D6FFE7CE
          FFE7CEFFE7CEFFE7CEFFE7CEFFE7CEFFE7D6FFEFD6FFEFD6FF9C10EFEFEFF7F7
          F7EFEFEFA5A5A500000000000000000000000000000000000000000000000000
          0000000000ADADADE7E7E7EFEFEFEFEFEFE78C29FFF7E7FFEFD6FFEFD6FFEFD6
          FFEFD6FFEFD6FFEFD6FFEFD6FFEFD6FFEFD6FFEFD6FFF7E7E78C29EFEFEFF7F7
          F7EFEFEFADADAD00000000000000000000000000000000000000000000000000
          0000000000ADADADE7E7E7F7F7F7EFEFEFF7B542FFDEB5FFF7EFFFF7E7FFEFE7
          FFEFE7FFEFE7F7EFE7FFEFE7FFF7E7FFF7E7FFF7EFFFDEB5FFAD42EFF7EFF7F7
          F7E7E7E7ADADAD00000000000000000000000000000000000000000000000000
          0000000000BDBDBDDEDED6F7F7F7EFF7F7F7CE8CFFC66BFFFFF7FFF7EFFFF7EF
          FFF7EFFFF7EFFFF7EFFFF7EFFFF7EFFFF7EFFFFFF7FFC66BFFCE8CEFF7F7F7F7
          F7DED6D6BDBDBD00000000000000000000000000000000000000000000000000
          0000000000D6D6D6C6C6C6F7F7F7F7F7F7F7EFDEFFA521FFDEADFFFFFFFFFFF7
          FFF7F7FFFFF7FFFFF7FFFFF7FFFFF7FFFFFFFFDEB5FFA521F7EFD6F7F7F7FFFF
          F7C6C6C6D6D6D600000000000000000000000000000000000000000000000000
          0000000000000000ADADADE7E7E7FFFFFFF7F7F7F7DEB5FFAD39FFDEADFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEB5FFAD39F7DEB5F7F7F7FFF7FFE7E7
          E7ADADAD00000000000000000000000000000000000000000000000000000000
          0000000000000000D6D6D6C6C6C6FFFFFFFFF7FFF7F7F7F7DEB5FFA518FFC673
          FFE7B5FFF7E7FFF7E7FFDEB5FFC66BFFA518F7DEB5FFF7FFF7FFF7FFFFFFC6C6
          C6D6D6D600000000000000000000000000000000000000000000000000000000
          0000000000000000000000A5A5A5D6CED6FFFFFFF7FFFFFFFFFFFFEFDEFFCE8C
          FFB542FF9C18FF9C18FFB54AFFCE8CFFEFDEFFFFFFFFFFF7F7FFFFD6D6D69C9C
          A500000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000A5A5A5CED6D6FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D6D6A5A5A50000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000009C9C9CC6C6C6E7E7E7FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE7E7C6C6C6A59CA50000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000D6D6D6ADADADC6C6C6D6DEDE
          EFEFEFFFF7FFF7F7F7EFEFEFDEDEDEC6C6C6ADADADD6D6D60000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000DEDEDEC6C6C6
          B5ADB5A5A5A5A5A5A5B5B5B5C6C6C6DED6DE0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
        Transparent = True
        SQLType = stPsq
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 614
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 476
        Height = 32
        Caption = 'Atrelamento de Lan'#231'amento Financeiro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 476
        Height = 32
        Caption = 'Atrelamento de Lan'#231'amento Financeiro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 476
        Height = 32
        Caption = 'Atrelamento de Lan'#231'amento Financeiro'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 430
    Width = 710
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 706
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 474
    Width = 710
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 564
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 562
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 710
    Height = 382
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 812
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 710
      Height = 241
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      object Bevel1: TBevel
        Left = 30
        Top = 140
        Width = 227
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
      end
      object Bevel2: TBevel
        Left = 266
        Top = 140
        Width = 228
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
      end
      object CkDataIni: TCheckBox
        Left = 30
        Top = 4
        Width = 183
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data inicial ou '#250'nica'
        TabOrder = 0
      end
      object TPDataIni: TdmkEditDateTimePicker
        Left = 30
        Top = 24
        Width = 228
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        CalColors.TextColor = clMenuText
        Date = 37636.000000000000000000
        Time = 0.777157974502188200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object CkDataFim: TCheckBox
        Left = 266
        Top = 4
        Width = 119
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data final'
        TabOrder = 2
      end
      object TPDataFim: TdmkEditDateTimePicker
        Left = 266
        Top = 24
        Width = 229
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 37636.000000000000000000
        Time = 0.777203761601413100
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object CkFornece: TCheckBox
        Left = 30
        Top = 50
        Width = 103
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Fornecedor'
        TabOrder = 4
      end
      object EdFornece: TdmkEditCB
        Left = 30
        Top = 72
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornece
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornece: TdmkDBLookupComboBox
        Left = 89
        Top = 72
        Width = 405
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsPFornece
        TabOrder = 6
        dmkEditCB = EdFornece
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CkCliente: TCheckBox
        Left = 30
        Top = 96
        Width = 103
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente'
        TabOrder = 7
      end
      object EdCliente: TdmkEditCB
        Left = 30
        Top = 116
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 89
        Top = 116
        Width = 405
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsPCliente
        TabOrder = 9
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CkDebito: TCheckBox
        Left = 35
        Top = 144
        Width = 189
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'D'#233'bito (m'#237'nimo e m'#225'ximo)'
        TabOrder = 10
      end
      object EdDebMin: TdmkEdit
        Left = 35
        Top = 164
        Width = 94
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdDebMinExit
      end
      object EdDebMax: TdmkEdit
        Left = 136
        Top = 164
        Width = 94
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CkCredito: TCheckBox
        Left = 278
        Top = 144
        Width = 190
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cr'#233'dito (m'#237'nimo e m'#225'ximo)'
        TabOrder = 13
      end
      object EdCredMin: TdmkEdit
        Left = 278
        Top = 164
        Width = 94
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdCredMinExit
      end
      object EdCredMax: TdmkEdit
        Left = 377
        Top = 164
        Width = 93
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CkNF: TCheckBox
        Left = 30
        Top = 194
        Width = 60
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N.F.'
        TabOrder = 16
      end
      object EdNF: TdmkEdit
        Left = 30
        Top = 216
        Width = 93
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CkDuplicata: TCheckBox
        Left = 128
        Top = 194
        Width = 90
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Duplicata'
        TabOrder = 18
      end
      object EdDuplicata: TdmkEdit
        Left = 128
        Top = 216
        Width = 94
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 19
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkDoc: TCheckBox
        Left = 226
        Top = 194
        Width = 95
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Doc. (Cheq.)'
        TabOrder = 20
      end
      object EdDoc: TdmkEdit
        Left = 226
        Top = 216
        Width = 94
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 21
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CkControle: TCheckBox
        Left = 326
        Top = 194
        Width = 75
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Controle'
        TabOrder = 22
      end
      object EdControle: TdmkEdit
        Left = 326
        Top = 216
        Width = 168
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 23
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object BtPesquisa: TBitBtn
        Tag = 18
        Left = 512
        Top = 196
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 24
        OnClick = BtPesquisaClick
      end
    end
    object DBGLct: TdmkDBGridZTO
      Left = 0
      Top = 241
      Width = 710
      Height = 141
      Align = alClient
      DataSource = DsPesq
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Visible = False
      RowColors = <>
      OnDblClick = DBGLctDblClick
      FieldsCalcToOrder.Strings = (
        'COMPENSADO_TXT=Compensado'
        'MENSAL=Ano,Mes2'
        'NOMESIT=Sit')
      Columns = <
        item
          Expanded = False
          FieldName = 'Carteira'
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Documento'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Duplicata'
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'SerieNF'
          Title.Caption = 'S'#233'rie NF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Title.Caption = 'Nota Fiscal'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Title.Alignment = taRightJustify
          Title.Caption = 'Quantidade 1'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtd2'
          Title.Caption = 'Quantidade 2'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 164
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatParcRef'
          Title.Caption = 'Origem'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatID'
          Title.Caption = 'Orig. Fat.'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Fat. Num.'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vencim.'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Compensado'
          Title.Caption = 'Compen.'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Lan'#231'to'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sub'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataCad'
          Title.Caption = 'Cadastro'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SALDO'
          Title.Caption = 'Saldo'
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 11
  end
  object QrPFornece: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 20
    Top = 316
    object QrPForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPForneceNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsPFornece: TDataSource
    DataSet = QrPFornece
    Left = 20
    Top = 360
  end
  object QrPCliente: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 20
    Top = 404
    object QrPClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPClienteNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsPCliente: TDataSource
    DataSet = QrPCliente
    Left = 20
    Top = 452
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM lct0001a'
      'WHERE Fornecedor=1'
      'AND Debito BETWEEN 0.50 AND 38098.50'
      'AND Data BETWEEN "2022-02-10" AND "2022-06-30"')
    Left = 180
    Top = 316
    object QrPesqData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPesqTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPesqCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPesqSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrPesqAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Required = True
    end
    object QrPesqGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrPesqQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrPesqDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPesqSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrPesqNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Required = True
    end
    object QrPesqDebito: TFloatField
      FieldName = 'Debito'
      Required = True
    end
    object QrPesqCredito: TFloatField
      FieldName = 'Credito'
      Required = True
    end
    object QrPesqCompensado: TDateField
      FieldName = 'Compensado'
      Required = True
    end
    object QrPesqSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrPesqDocumento: TFloatField
      FieldName = 'Documento'
      Required = True
    end
    object QrPesqSit: TIntegerField
      FieldName = 'Sit'
      Required = True
    end
    object QrPesqVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPesqFatID: TIntegerField
      FieldName = 'FatID'
      Required = True
    end
    object QrPesqFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
      Required = True
    end
    object QrPesqFatNum: TFloatField
      FieldName = 'FatNum'
      Required = True
    end
    object QrPesqFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Required = True
    end
    object QrPesqID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrPesqID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrPesqID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Required = True
    end
    object QrPesqFatura: TWideStringField
      FieldName = 'Fatura'
      Required = True
      Size = 1
    end
    object QrPesqEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrPesqBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrPesqAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrPesqContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPesqCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrPesqLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object QrPesqCartao: TIntegerField
      FieldName = 'Cartao'
      Required = True
    end
    object QrPesqLinha: TIntegerField
      FieldName = 'Linha'
      Required = True
    end
    object QrPesqOperCount: TIntegerField
      FieldName = 'OperCount'
      Required = True
    end
    object QrPesqLancto: TIntegerField
      FieldName = 'Lancto'
      Required = True
    end
    object QrPesqPago: TFloatField
      FieldName = 'Pago'
      Required = True
    end
    object QrPesqMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPesqFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Required = True
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPesqCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrPesqForneceI: TIntegerField
      FieldName = 'ForneceI'
      Required = True
    end
    object QrPesqMoraDia: TFloatField
      FieldName = 'MoraDia'
      Required = True
    end
    object QrPesqMulta: TFloatField
      FieldName = 'Multa'
      Required = True
    end
    object QrPesqMoraVal: TFloatField
      FieldName = 'MoraVal'
      Required = True
    end
    object QrPesqMultaVal: TFloatField
      FieldName = 'MultaVal'
      Required = True
    end
    object QrPesqProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrPesqDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrPesqCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrPesqNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPesqVendedor: TIntegerField
      FieldName = 'Vendedor'
      Required = True
    end
    object QrPesqAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
    object QrPesqICMS_P: TFloatField
      FieldName = 'ICMS_P'
      Required = True
    end
    object QrPesqICMS_V: TFloatField
      FieldName = 'ICMS_V'
      Required = True
    end
    object QrPesqDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 15
    end
    object QrPesqDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrPesqDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrPesqDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrPesqDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrPesqUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrPesqNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrPesqAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrPesqExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrPesqDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrPesqCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrPesqTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrPesqReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrPesqAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrPesqPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrPesqPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrPesqRecDes: TFloatField
      FieldName = 'RecDes'
      Required = True
    end
    object QrPesqSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrPesqMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrPesqProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrPesqCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
      Required = True
    end
    object QrPesqEndossas: TSmallintField
      FieldName = 'Endossas'
      Required = True
    end
    object QrPesqEndossan: TFloatField
      FieldName = 'Endossan'
      Required = True
    end
    object QrPesqEndossad: TFloatField
      FieldName = 'Endossad'
      Required = True
    end
    object QrPesqCancelado: TSmallintField
      FieldName = 'Cancelado'
      Required = True
    end
    object QrPesqEventosCad: TIntegerField
      FieldName = 'EventosCad'
      Required = True
    end
    object QrPesqEncerrado: TIntegerField
      FieldName = 'Encerrado'
      Required = True
    end
    object QrPesqErrCtrl: TIntegerField
      FieldName = 'ErrCtrl'
      Required = True
    end
    object QrPesqIndiPag: TIntegerField
      FieldName = 'IndiPag'
      Required = True
    end
    object QrPesqCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
      Required = True
    end
    object QrPesqFatParcRef: TIntegerField
      FieldName = 'FatParcRef'
      Required = True
    end
    object QrPesqFatSit: TSmallintField
      FieldName = 'FatSit'
      Required = True
    end
    object QrPesqFatSitSub: TSmallintField
      FieldName = 'FatSitSub'
      Required = True
    end
    object QrPesqFatGrupo: TIntegerField
      FieldName = 'FatGrupo'
      Required = True
    end
    object QrPesqTaxasVal: TFloatField
      FieldName = 'TaxasVal'
      Required = True
    end
    object QrPesqFisicoSrc: TSmallintField
      FieldName = 'FisicoSrc'
      Required = True
    end
    object QrPesqFisicoCod: TIntegerField
      FieldName = 'FisicoCod'
      Required = True
    end
    object QrPesqTemCROLct: TSmallintField
      FieldName = 'TemCROLct'
      Required = True
    end
    object QrPesqlanctos: TLargeintField
      FieldName = 'lanctos'
      Required = True
    end
    object QrPesqQtd2: TFloatField
      FieldName = 'Qtd2'
    end
    object QrPesqVctoOriginal: TDateField
      FieldName = 'VctoOriginal'
      Required = True
    end
    object QrPesqModeloNF: TWideStringField
      FieldName = 'ModeloNF'
      Size = 6
    end
    object QrPesqHoraCad: TTimeField
      FieldName = 'HoraCad'
      Required = True
    end
    object QrPesqHoraAlt: TTimeField
      FieldName = 'HoraAlt'
      Required = True
    end
    object QrPesqGenCtb: TIntegerField
      FieldName = 'GenCtb'
      Required = True
    end
    object QrPesqQtDtPg: TIntegerField
      FieldName = 'QtDtPg'
      Required = True
    end
    object QrPesqGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
      Required = True
    end
    object QrPesqGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
      Required = True
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 180
    Top = 368
  end
end
