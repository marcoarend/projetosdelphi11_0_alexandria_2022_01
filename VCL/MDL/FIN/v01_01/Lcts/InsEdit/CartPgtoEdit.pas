unit CartPgtoEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, ComCtrls, UnGOTOy, Buttons, Db, (*DBTables,*)
  UnInternalConsts, ExtCtrls, ZCF2, UMySQLModule, mySQLDbTables,
  UnMyLinguas, Variants, dmkGeral, dmkEditDateTimePicker, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkImage, UnDmkEnums;

type
  TFmCartPgtoEdit = class(TForm)
    QrContas: TMySQLQuery;
    DsContas: TDataSource;
    QrCarteiras: TMySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    QrDescoPor: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    IntegerField4: TIntegerField;
    DsDescoPor: TDataSource;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    EdOldControle: TdmkEdit;
    TPOldData: TdmkEditDateTimePicker;
    Panel2: TPanel;
    Label1: TLabel;
    LaDeb: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    LaDescoPor: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdValor: TdmkEdit;
    EdDescricao: TdmkEdit;
    EdCodigo: TdmkEdit;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    RGCarteira: TRadioGroup;
    EdMoraVal: TdmkEdit;
    EdMultaVal: TdmkEdit;
    CBDescoPor: TdmkDBLookupComboBox;
    EdDescoPor: TdmkEditCB;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    Label16: TLabel;
    EdDescoVal: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdMoraDia: TdmkEdit;
    Label9: TLabel;
    EdMulta: TdmkEdit;
    LaDoc: TLabel;
    EdSerieCH: TdmkEdit;
    EdDoc: TdmkEdit;
    LaVencimento: TLabel;
    TPVencimento: TdmkEditDateTimePicker;
    EdOldSaldo: TdmkEdit;
    EdOldDescricao: TdmkEdit;
    LaNF: TLabel;
    dmkEdNF: TdmkEdit;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RGCarteiraClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdMoraValKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMultaValKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure TPDataChange(Sender: TObject);
    procedure TPDataClick(Sender: TObject);
    procedure EdMoraValExit(Sender: TObject);
    procedure EdMultaValExit(Sender: TObject);
    procedure EdMoraDiaExit(Sender: TObject);
    procedure EdMultaExit(Sender: TObject);
    procedure EdDescoValExit(Sender: TObject);
  private
    { Private declarations }
    procedure CarteirasReopen(Tipo: Integer);
    procedure CalculaMultaJuroVal;
    procedure CalculaMultaJuroPer;
  public
    { Public declarations }
    FTabLctA: String;
    FOldCarteira, FOldTipo, FOldSub: Integer;
  end;

var
  FmCartPgtoEdit: TFmCartPgtoEdit;

implementation

uses UnMyObjects, Module, CartPgto, UnFinanceiro, ModuleFin, ModuleGeral,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmCartPgtoEdit.CalculaMultaJuroVal;
var
  MultaPer, JuroPer, ValAtualiz, ValMulta, ValJuro: Double;
begin
  MultaPer := EdMulta.ValueVariant;
  JuroPer  := EdMoraDia.ValueVariant;
  //   
  UFinanceiro.CalculaValMultaEJuros(EdOldSaldo.ValueVariant, TPData.Date,
    TPOldData.Date, MultaPer, JuroPer, ValAtualiz, ValMulta, ValJuro, True);
  //
  EdMoraVal.ValueVariant  := ValJuro;
  EdMultaVal.ValueVariant := ValMulta;
  EdValor.ValueVariant    := ValAtualiz;
end;

procedure TFmCartPgtoEdit.CarteirasReopen(Tipo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCarteiras, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM carteiras ',
    'WHERE Codigo > 0 ',
    'AND Ativo = 1 ',
    'AND Tipo=' + Geral.FF0(Tipo),
    'AND ForneceI=' + Geral.FF0(FmCartPgto.FQrLct.FieldByName('CliInt').AsInteger),
    '']);
end;

procedure TFmCartPgtoEdit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCartPgtoEdit.BtConfirmaClick(Sender: TObject);
const
  QrCrt = nil;
  QrLct = nil;
  RecalcSdoCart = False;
  UsaOutrosFatDfs = False;
  FatGrupo = 0;
var
  OldTipo, OldCarteira, OldControle, OldSub, Carteira: Integer;
  Controle: Int64;
  Valor, Credito, Debito: Double;
  OldData: TDate;
  //
  Sit, Nivel, CtrlOri, Tipo, Genero, NotaFiscal, Cliente, Fornecedor, ID_Pgto,
  Vendedor, Account, CentroCusto, DescoPor, CliInt, ForneceI, Depto: Integer;
  Descricao, SerieCH, Duplicata, Mes, Msg: String;
  Compensado, Vencimento, DataPag, DataDoc: TDateTime;
  MoraDia, Multa, DescoVal, MultaVal, MoraVal, TaxasVal: Double;
  FatID, FatID_Sub, FatParcela, FatParcRef, Documento, CtrlIni: Integer;
  FatNum: Double;
  _Carteira, _GenCtbD, _GenCtbC, GenCtb, GenCtbD, GenCtbC: Integer;
  //
begin
  Nivel := FmCartPgto.FQrLct.FieldByName('Nivel').AsInteger + 1;
  //
  if FmCartPgto.FQrLct.FieldByName('CtrlIni').AsInteger > 0 then
    CtrlIni := FmCartPgto.FQrLct.FieldByName('CtrlIni').AsInteger
  else
    CtrlIni := FmCartPgto.FQrLct.FieldByName('Controle').AsInteger;
  //
  Valor := Geral.DMV(EdValor.Text);
  if FmCartPgto.FQrLct.FieldByName('Credito').AsFloat > 0 then Credito := Valor else Credito := 0;
  if FmCartPgto.FQrLct.FieldByName('Debito').AsFloat > 0 then Debito := Valor else Debito := 0;
  if MyObjects.FIC(RGCarteira.ItemIndex < 0, RGCarteira, 'Defina um tipo de carteira!') then
    Exit;
  Carteira := EdCarteira.ValueVariant;
  if MyObjects.FIC(Carteira < 1, EdCarteira, 'Defina um item de carteira maior que zero!') then
    Exit;
  if MyObjects.FIC(Valor = 0, nil, 'Defina um valor diferente de zero!') then
    Exit;
  //Verifica FatID
  if UFinanceiro.FatIDProibidoEmLct(FmCartPgto.FQrLct.FieldByName('FatID').AsInteger, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    Exit;
  end;
  Mes := FmCartPgto.FQrLct.FieldByName('MENSAL2').AsString;
  if (EdDoc.Enabled = False) then Documento := 0
  else Documento := Geral.IMV(EdDoc.Text);
  //
  if (TPVencimento.Visible = False) then
    Vencimento := TPOldData.Date
  else
    Vencimento := TPVencimento.Date;
  //
  if RGCarteira.ItemIndex = 2 then
  begin
    Sit := 0;
    Compensado := 0;
  end else begin
    Sit := 3;
    Compensado := TPData.Date;
  end;
  //TipoAnt := StrToInt(EdTipoAnt.Caption);
  OldCarteira := FOldCarteira;
  //
  /////////// 2012-01-05
  Valor := Credito - Debito;
  //
  CtrlOri := 0;
  Duplicata := '';

  SerieCH        := EdSerieCH.Text;
  DataPag        := TPData.Date;
  Tipo           := RGCarteira.ItemIndex;
  _Carteira       := Carteira;
  Credito        := Credito;
  Debito         := Debito;
  Genero         := FmCartPgto.FQrLct.FieldByName('Genero').AsInteger;  // OK para Cont�bil!!!
  GenCtb         := FmCartPgto.FQrLct.FieldByName('GenCtb').AsInteger;
  _GenCtbD        := FmCartPgto.FQrLct.FieldByName('GenCtbD').AsInteger;
  _GenCtbC        := FmCartPgto.FQrLct.FieldByName('GenCtbC').AsInteger;
  NotaFiscal     := dmkEdNF.ValueVariant;
  //Vencimento     := Vencimento;
  Descricao      := EdDescricao.Text;
  Sit            := Sit;
  Cliente        := FmCartPgto.FQrLct.FieldByName('Cliente').AsInteger;
  Fornecedor     := FmCartPgto.FQrLct.FieldByName('Fornecedor').AsInteger;
  ID_Pgto        := StrToInt(FmCartPgto.LaControle.Caption);
  DataDoc        := FmCartPgto.FQrLct.FieldByName('DataDoc').AsDateTime;
  MoraDia        := 0;
  Multa          := 0;
  TaxasVal       := 0;
  MultaVal       := EdMultaVal.ValueVariant;
  MoraVal        := EdMoraVal.ValueVariant;
  CtrlIni        := Trunc(CtrlIni + 0.01);
  Nivel          := Nivel;
  Vendedor       := FmCartPgto.FQrLct.FieldByName('Vendedor').AsInteger;
  Account        := FmCartPgto.FQrLct.FieldByName('Account').AsInteger;
  CentroCusto    := FmCartPgto.FQrLct.FieldByName('CentroCusto').AsInteger;
  DescoPor       := Geral.IMV(EdDescoPor.Text);
  DescoVal       := Geral.DMV(EdDescoVal.Text);
  //Synker
  CliInt         := FmCartPgto.FQrLct.FieldByName('CliInt').AsInteger;
  ForneceI       := FmCartPgto.FQrLct.FieldByName('ForneceI').AsInteger;
  Depto          := FmCartPgto.FQrLct.FieldByName('Depto').AsInteger;
  FatID          := FmCartPgto.FQrLct.FieldByName('FatID').AsInteger;
  FatID_Sub      := FmCartPgto.FQrLct.FieldByName('FatID_Sub').AsInteger;
  FatNum         := FmCartPgto.FQrLct.FieldByName('FatNum').AsInteger;
  FatParcela     := FmCartPgto.FQrLct.FieldByName('FatParcela').AsInteger;
  FatParcRef     := 0; // Cuidado ao usar! Usa no Creditor!
  //
  // ini 2022-03-24
  GenCtbD := 0;
  GenCtbC := 0;
  if not UFinanceiro.PagamentoEmBanco_DefineGenerosECarteira(
    FmCartPgto.FQrLct.FieldByName('Controle').AsInteger,
    FmCartPgto.FQrLct.FieldByName('Sub').AsInteger,
    Credito, Debito, (*OutraCartCompensa*)0, _Carteira, _GenCtbD, _GenCtbC,
    Carteira, GenCtbD, GenCtbC) then Exit;
  // fim 2022-03-24
  if ImgTipo.SQLType = stIns then
  begin
    if DModFin.PagamtParcialDeDocumento(Valor, Credito, Debito,
    CtrlIni, CtrlOri, Documento,
    SerieCH, Duplicata, DataPag, Vencimento, DataDoc, Compensado, Descricao, QrCrt,
    QrLct, RecalcSdoCart, FTabLctA, UsaOutrosFatDfs, FatID, FatID_Sub, FatNum,
    FatParcela, FatParcRef, FatGrupo,   // Novo
    Nivel, Carteira, Sit, Tipo, Genero, NotaFiscal, Cliente, Fornecedor, ID_Pgto,
    Vendedor, Account, DescoPor, CliInt, ForneceI, Depto, CentroCusto,
    Mes, MoraDia, Multa, DescoVal, TaxasVal, MultaVal, MoraVal, OldCarteira,
    GenCtb, GenCtbD, GenCtbC, '', False) then
    //if UFinanceiro.InsereLancamento(FTabLctA) then
    begin
      UFinanceiro.RecalcSaldoCarteira(Carteira, nil, nil, False, False);
      UFinanceiro.RecalcSaldoCarteira(OldCarteira,  nil, nil, False, False);
    end;
  end else begin
    // 2013-07-25 N�o altera mais nesta janela!
    Geral.MB_Aviso('Altera��o desativada. Solicite implementa��o � Dermatek!');
{
    OldData     := TPOldData.Date;
    OldTipo     := FOldTipo;
    OldControle := EdOldControle.ValueVariant;
    OldSub      := FOldSub;
    UMyMod.LctUpd(Dmod.QrUpd, FTabLctA, [
    'Descricao', 'Debito', 'Credito',
    'SerieCH', 'Documento', 'Vencimento',
    'MoraDia', 'Multa', 'DescoPor',
    'DescoVal', 'Data'], [
    FLAN_Descricao, FLAN_Debito, FLAN_Credito,
    FLAN_SerieCH, FLAN_Documento, FLAN_Vencimento,
    FLAN_MoraDia, FLAN_Multa, FLAN_DescoPor,
    FLAN_DescoVal, FLAN_Data], OldData,
    OldTipo, OldCarteira, OldControle, OldSub);
    VAR_LANCTO2 := FmCartPgto.FQrLct.FieldByName('Controle').AsInteger;
    UFinanceiro.RecalcSaldoCarteira(OldCarteira,  nil, nil, False, False);
}
  end;
  //
  Close;
end;

procedure TFmCartPgtoEdit.FormCreate(Sender: TObject);
begin
  DmodG.ReopenControle();
  //
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  //
  LaVencimento.Visible := False;
  TPVencimento.Visible := False;
  // ini 2021-06-12
  TPOldData.Date := Trunc(Date());
  TPData.Date := Trunc(Date());
  TPVencimento.Date := Trunc(Date());
  // fim 2021-06-12
end;

procedure TFmCartPgtoEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  CarteirasReopen(RGCarteira.ItemIndex);
  if UFinanceiro.TabLctNaoDefinida(FTabLctA, 'TFmCartPgtoEdit.FormActivate()') then
    Close;
  //
end;

procedure TFmCartPgtoEdit.RGCarteiraClick(Sender: TObject);
begin
  EdCarteira.Text := '0';
  CBCarteira.KeyValue := 0;
  CarteirasReopen(RGCarteira.ItemIndex);
  case RGCarteira.ItemIndex of
    0:
    begin
      EdDoc.Enabled := False;
      LaDoc.Enabled := False;
      TPVencimento.Visible := False;
      LaVencimento.Visible := False;
    end;
    1:
    begin
      EdDoc.Enabled := True;
      LaDoc.Enabled := True;
      TPVencimento.Visible := False;
      LaVencimento.Visible := False;
    end;
    2:
    begin
      EdDoc.Enabled := True;
      LaDoc.Enabled := True;
      TPVencimento.Visible := True;
      LaVencimento.Visible := True;
    end;
  end;

end;

procedure TFmCartPgtoEdit.TPDataChange(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmCartPgtoEdit.TPDataClick(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmCartPgtoEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCartPgtoEdit.FormShow(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := ImgTipo.SQLType = stIns;
  //Desabilita o multa e juros na edi��o por causa dos lan�amentos individuais
  EdMultaVal.Enabled := Enab;
  EdMoraVal.Enabled  := Enab;
end;

procedure TFmCartPgtoEdit.CalculaMultaJuroPer;
var
  ValorNew, MultaPerc, JuroPerc: Double;
begin
  UFinanceiro.CalculaPercMultaEJuros(EdOldSaldo.ValueVariant,
    EdMultaVal.ValueVariant, EdMoraVal.ValueVariant, TPData.Date,
    TPOldData.Date, ValorNew, MultaPerc, JuroPerc);
  //
  EdMoraDia.ValueVariant := JuroPerc;
  EdMulta.ValueVariant   := MultaPerc;
  EdValor.ValueVariant   := ValorNew;
end;

procedure TFmCartPgtoEdit.EdDescoValExit(Sender: TObject);
begin
  EdValor.ValueVariant := EdValor.ValueVariant - EdDescoVal.ValueVariant;
end;

procedure TFmCartPgtoEdit.EdMoraDiaExit(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmCartPgtoEdit.EdMoraValExit(Sender: TObject);
begin
  CalculaMultaJuroPer;
end;

procedure TFmCartPgtoEdit.EdMoraValKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {
  if Key=VK_F4 then
  begin
    FmPrincipal.CriaCalcPercent(EdValor.Text, '1,0000', cpJurosMes);
    EdMoraDia.Text := Geral.TFT(FloatToStr(VAR_VALOR), 2, siPositivo);
  end;
  }
end;

procedure TFmCartPgtoEdit.EdMultaExit(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmCartPgtoEdit.EdMultaValExit(Sender: TObject);
begin
  CalculaMultaJuroPer;
end;

procedure TFmCartPgtoEdit.EdMultaValKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {
  if Key=VK_F4 then
  begin
    FmPrincipal.CriaCalcPercent(EdValor.Text, '2,0000', cpMulta);
    EdMulta.Text := Geral.TFT(FloatToStr(VAR_VALOR), 2, siPositivo);
  end;
  }
end;

end.

