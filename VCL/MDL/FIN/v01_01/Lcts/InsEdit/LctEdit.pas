unit LctEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, UnGOTOy, ComCtrls, UnMyLinguas, Db,
  mySQLDbTables, ExtCtrls, Buttons, UnInternalConsts, UMySQLModule, Grids,
  DBGrids, frxChBox, frxClass, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkEditDateTimePicker, dmkRadioGroup, Variants, dmkGeral, dmkCheckBox,
  dmkValUsu, dmkImage, DmkDAC_PF, UnDmkProcFunc, UnDmkEnums, dmkVariable,
  UnProjGroup_Consts, UnGrl_Geral;

type
  TEntiTipo = (tetCliente = 0, tetFornece = 1);
  TTipoValor = (tvNil, tvCred, tvDeb);
  TFmLctEdit = class(TForm)
    DsContas1: TDataSource;
    DsCarteiras: TDataSource;
    DsFornecedores: TDataSource;
    DsClientes: TDataSource;
    QrContas1: TMySQLQuery;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasFechamento: TIntegerField;
    QrFatura: TmySQLQuery;
    QrFaturaData: TDateField;
    QrContas1Codigo: TIntegerField;
    QrContas1Nome: TWideStringField;
    QrContas1Nome2: TWideStringField;
    QrContas1Nome3: TWideStringField;
    QrContas1ID: TWideStringField;
    QrContas1Subgrupo: TIntegerField;
    QrContas1Empresa: TIntegerField;
    QrContas1Credito: TWideStringField;
    QrContas1Debito: TWideStringField;
    QrContas1Mensal: TWideStringField;
    QrContas1Exclusivo: TWideStringField;
    QrContas1Mensdia: TSmallintField;
    QrContas1Mensdeb: TFloatField;
    QrContas1Mensmind: TFloatField;
    QrContas1Menscred: TFloatField;
    QrContas1Mensminc: TFloatField;
    QrContas1Lk: TIntegerField;
    QrContas1Terceiro: TIntegerField;
    QrContas1Excel: TWideStringField;
    QrContas1DataCad: TDateField;
    QrContas1DataAlt: TDateField;
    QrContas1UserCad: TSmallintField;
    QrContas1UserAlt: TSmallintField;
    QrContas1NOMESUBGRUPO: TWideStringField;
    QrContas1NOMEGRUPO: TWideStringField;
    QrContas1NOMECONJUNTO: TWideStringField;
    QrContas1NOMEEMPRESA: TWideStringField;
    QrFornecedores: TmySQLQuery;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOMEENTIDADE: TWideStringField;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVendedores: TDataSource;
    DsAccounts: TDataSource;
    QrAccounts: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    QrFunci: TmySQLQuery;
    DsFunci: TDataSource;
    QrFunciCodigo: TIntegerField;
    QrFunciNOMEENTIDADE: TWideStringField;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMEENTIDADE: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PainelDados: TPanel;
    LaFunci: TLabel;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctLk: TIntegerField;
    QrLctFatID: TIntegerField;
    QrLctFatParcela: TIntegerField;
    QrLctCONTA: TIntegerField;
    QrLctNOMECONTA: TWideStringField;
    QrLctNOMEEMPRESA: TWideStringField;
    QrLctNOMESUBGRUPO: TWideStringField;
    QrLctNOMEGRUPO: TWideStringField;
    QrLctNOMECONJUNTO: TWideStringField;
    QrLctNOMESIT: TWideStringField;
    QrLctAno: TFloatField;
    QrLctMENSAL: TWideStringField;
    QrLctMENSAL2: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctLocal: TIntegerField;
    QrLctFatura: TWideStringField;
    QrLctSub: TSmallintField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctPago: TFloatField;
    QrLctSALDO: TFloatField;
    QrLctID_Sub: TSmallintField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctcliente: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctNOMECLIENTE: TWideStringField;
    QrLctNOMEFORNECEDOR: TWideStringField;
    QrLctTIPOEM: TWideStringField;
    QrLctNOMERELACIONADO: TWideStringField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctMulta: TFloatField;
    QrLctATRASO: TFloatField;
    QrLctJUROS: TFloatField;
    QrLctDataDoc: TDateField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctMes2: TLargeintField;
    QrLctProtesto: TDateField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TSmallintField;
    QrLctUserAlt: TSmallintField;
    QrLctControle: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctCtrlIni: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctCOMPENSADO_TXT: TWideStringField;
    QrLctCliInt: TIntegerField;
    QrLctNOMECARTEIRA: TWideStringField;
    QrLctSALDOCARTEIRA: TFloatField;
    DsLct: TDataSource;
    TabSheet2: TTabSheet;
    DsDeptos: TDataSource;
    QrDeptos: TmySQLQuery;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTipo: TIntegerField;
    QrForneceI: TmySQLQuery;
    DsForneceI: TDataSource;
    QrForneceICodigo: TIntegerField;
    QrForneceINOMEENTIDADE: TWideStringField;
    TabSheet3: TTabSheet;
    QrCarteirasExigeNumCheque: TSmallintField;
    Panel2: TPanel;
    PnMaskPesq: TPanel;
    TabSheet4: TTabSheet;
    TabControl1: TTabControl;
    DBGrid1: TDBGrid;
    Panel7: TPanel;
    GBParcelamento: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    RGArredondar: TRadioGroup;
    RGPeriodo: TRadioGroup;
    EdDias: TdmkEdit;
    RGIncremCH: TRadioGroup;
    EdParcela1: TdmkEdit;
    EdParcelaX: TdmkEdit;
    CkArredondar: TCheckBox;
    EdSoma: TdmkEdit;
    CkParcelamento: TCheckBox;
    DBGParcelas: TDBGrid;
    TbParcpagtos: TmySQLTable;
    TbParcpagtosParcela: TIntegerField;
    TbParcpagtosData: TDateField;
    TbParcpagtosCredito: TFloatField;
    TbParcpagtosDebito: TFloatField;
    TbParcpagtosDoc: TLargeintField;
    TbParcpagtosMora: TFloatField;
    TbParcpagtosMulta: TFloatField;
    TbParcpagtosICMS_V: TFloatField;
    TbParcpagtosDuplicata: TWideStringField;
    TbParcpagtosDescricao: TWideStringField;
    DsParcPagtos: TDataSource;
    QrSoma: TmySQLQuery;
    QrSomaVALOR: TFloatField;
    CkIncremDU: TCheckBox;
    RGIncremDupl: TGroupBox;
    Label27: TLabel;
    EdDuplSep: TEdit;
    RGDuplSeq: TRadioGroup;
    GBIncremTxt: TGroupBox;
    Label30: TLabel;
    EdSepTxt: TEdit;
    RGSepTxt: TRadioGroup;
    CkIncremTxt: TCheckBox;
    QrDeptosCODI_1: TIntegerField;
    QrDeptosCODI_2: TLargeintField;
    QrDeptosNOME_1: TWideStringField;
    TabSheet5: TTabSheet;
    Panel8: TPanel;
    MeConfig: TMemo;
    dmkEdParcelas: TdmkEdit;
    Panel10: TPanel;
    Label5: TLabel;
    dmkEdCNAB_Sit: TdmkEdit;
    Label6: TLabel;
    dmkEdID_pgto: TdmkEdit;
    dmkEdFatID: TdmkEdit;
    Label12: TLabel;
    Label16: TLabel;
    dmkEdTipoCH: TdmkEdit;
    dmkEdFatNum: TdmkEdit;
    Label17: TLabel;
    Label31: TLabel;
    dmkEdFatID_Sub: TdmkEdit;
    dmkEdNivel: TdmkEdit;
    Label32: TLabel;
    Label33: TLabel;
    dmkEdFatParcela: TdmkEdit;
    dmkEdCtrlIni: TdmkEdit;
    Label34: TLabel;
    Label35: TLabel;
    dmkEdDescoPor: TdmkEdit;
    dmkEdDescoVal: TdmkEdit;
    Label36: TLabel;
    Label37: TLabel;
    dmkEdUnidade: TdmkEdit;
    dmkEdDoc2: TdmkEdit;
    Label38: TLabel;
    Label39: TLabel;
    dmkEdNFVal: TdmkEdit;
    Label40: TLabel;
    Label41: TLabel;
    dmkEdTipoAnt: TdmkEdit;
    dmkEdCartAnt: TdmkEdit;
    Label42: TLabel;
    dmkEdPercJuroM: TdmkEdit;
    dmkEdPercMulta: TdmkEdit;
    Label43: TLabel;
    Label44: TLabel;
    dmkEd_: TdmkEdit;
    Label45: TLabel;
    dmkEdExecs: TdmkEdit;
    Label46: TLabel;
    dmkEdOneCliInt: TdmkEdit;
    QrLctFatNum: TFloatField;
    PnLancto1: TPanel;
    Label14: TLabel;
    dmkEdControle: TdmkEdit;
    dmkEdCarteira: TdmkEditCB;
    dmkCBCarteira: TdmkDBLookupComboBox;
    SbCarteira: TSpeedButton;
    BtContas: TSpeedButton;
    dmkCBGenero: TdmkDBLookupComboBox;
    dmkEdCBGenero: TdmkEditCB;
    dmkEdTPData: TdmkEditDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    LaMes: TLabel;
    dmkEdMes: TdmkEdit;
    dmkEdDeb: TdmkEdit;
    LaDeb: TLabel;
    dmkEdCred: TdmkEdit;
    LaCred: TLabel;
    LaNF: TLabel;
    dmkEdNF: TdmkEdit;
    dmkEdSerieCH: TdmkEdit;
    LaDoc: TLabel;
    dmkEdDoc: TdmkEdit;
    dmkEdDuplicata: TdmkEdit;
    Label11: TLabel;
    LaVencimento: TLabel;
    dmkEdTPVencto: TdmkEditDateTimePicker;
    LaCliInt: TLabel;
    dmkEdCBCliInt: TdmkEditCB;
    dmkCBCliInt: TdmkDBLookupComboBox;
    dmkEdCBCliente: TdmkEditCB;
    LaCliente: TLabel;
    dmkCBCliente: TdmkDBLookupComboBox;
    dmkEdCBFornece: TdmkEditCB;
    LaFornecedor: TLabel;
    dmkCBFornece: TdmkDBLookupComboBox;
    SBFornece: TSpeedButton;
    LaForneceRN: TLabel;
    LaForneceFA: TLabel;
    LaCarteira: TLabel;
    PnDepto: TPanel;
    dmkCBDepto: TdmkDBLookupComboBox;
    dmkEdCBDepto: TdmkEditCB;
    LaDepto: TLabel;
    PnForneceI: TPanel;
    dmkCBForneceI: TdmkDBLookupComboBox;
    dmkEdCBForneceI: TdmkEditCB;
    LaForneceI: TLabel;
    PnAccount: TPanel;
    LaAccount: TLabel;
    dmkEdCBAccount: TdmkEditCB;
    dmkCBAccount: TdmkDBLookupComboBox;
    PnLancto2: TPanel;
    dmkEdDescricao: TdmkEdit;
    Label13: TLabel;
    dmkEdQtde: TdmkEdit;
    Label20: TLabel;
    Label3: TLabel;
    dmkEdOneForneceI: TdmkEdit;
    Label47: TLabel;
    dmkEdOneVendedor: TdmkEdit;
    Label48: TLabel;
    dmkEdOneAccount: TdmkEdit;
    Label49: TLabel;
    dmkEdOneCliente: TdmkEdit;
    dmkEdOneFornecedor: TdmkEdit;
    Label50: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesAccount: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrContas1NOMEPLANO: TWideStringField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasBanco1: TIntegerField;
    CkDuplicando: TCheckBox;
    SBCliente: TSpeedButton;
    QrEventosCad: TmySQLQuery;
    QrEventosCadCodigo: TIntegerField;
    QrEventosCadCodUsu: TIntegerField;
    QrEventosCadNome: TWideStringField;
    DsEventosCad: TDataSource;
    LaEventosCad: TLabel;
    EdEventosCad: TdmkEditCB;
    CBEventosCad: TdmkDBLookupComboBox;
    SbEventosCad: TSpeedButton;
    dmkValUsu1: TdmkValUsu;
    QrCarteirasUsaTalao: TSmallintField;
    Label9: TLabel;
    EdTabLctA: TdmkEdit;
    QrLocMes: TmySQLQuery;
    QrLocMesMEZ: TIntegerField;
    Label4: TLabel;
    LaFinalidade: TLabel;
    Panel13: TPanel;
    GroupBox5: TGroupBox;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    EdOldControle: TdmkEdit;
    TPOldData: TdmkEditDateTimePicker;
    EdOldSub: TdmkEdit;
    EdOldTipo: TdmkEdit;
    EdOldCarteira: TdmkEdit;
    Label60: TLabel;
    EdIndiPag: TdmkEditCB;
    CBIndiPag: TdmkDBLookupComboBox;
    SbIndiPag: TSpeedButton;
    QrIndiPag: TmySQLQuery;
    DsIndiPag: TDataSource;
    QrIndiPagCodigo: TIntegerField;
    QrIndiPagNome: TWideStringField;
    QrCliIntCtaCfgCab: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel6: TPanel;
    BtConfirma: TBitBtn;
    CkContinuar: TCheckBox;
    CkCopiaCH: TCheckBox;
    BitBtn1: TBitBtn;
    CkNaoPesquisar: TCheckBox;
    BtReabreLct: TBitBtn;
    GroupBox6: TGroupBox;
    Panel5: TPanel;
    Label18: TLabel;
    LaContas: TLabel;
    GroupBox7: TGroupBox;
    Panel11: TPanel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Panel111: TPanel;
    GBVendas: TGroupBox;
    PnVendas: TPanel;
    Label10: TLabel;
    LaVendedor: TLabel;
    LaICMS_P: TLabel;
    LaICMS_V: TLabel;
    dmkCBFunci: TdmkDBLookupComboBox;
    dmkEdCBFunci: TdmkEditCB;
    dmkEdCBVendedor: TdmkEditCB;
    dmkCBVendedor: TdmkDBLookupComboBox;
    dmkEdICMS_P: TdmkEdit;
    dmkEdICMS_V: TdmkEdit;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    LaDataDoc: TLabel;
    Label22: TLabel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label8: TLabel;
    Label7: TLabel;
    dmkEdMoraDia: TdmkEdit;
    dmkEdMulta: TdmkEdit;
    GroupBox4: TGroupBox;
    Label19: TLabel;
    Label21: TLabel;
    dmkEdMultaVal: TdmkEdit;
    dmkEdMoraVal: TdmkEdit;
    GroupBox9: TGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    BitBtn2: TBitBtn;
    dmkEdValNovo: TdmkEdit;
    dmkEdPerMult: TdmkEdit;
    dmkEdTPDataDoc: TdmkEditDateTimePicker;
    dmkEdTPCompensado: TdmkEditDateTimePicker;
    dmkRGTipoCH: TdmkRadioGroup;
    GBLancto3: TGroupBox;
    CkPesqNF: TCheckBox;
    CkPesqCH: TCheckBox;
    CkPesqVal: TCheckBox;
    CkCancelado: TdmkCheckBox;
    RGFisicoSrc: TdmkRadioGroup;
    EdFisicoCod: TdmkEdit;
    Label62: TLabel;
    GroupBox8: TGroupBox;
    VAVcto: TdmkVariable;
    BtCalculadora: TBitBtn;
    dmkEdSerieNF: TdmkEdit;
    Panel3: TPanel;
    CkPesqEntValData: TCheckBox;
    dmkEdCBClienteCNPJ: TdmkEdit;
    dmkEdCBForneceCNPJ: TdmkEdit;
    Label15: TLabel;
    Label61: TLabel;
    QrClientesDocum: TWideStringField;
    QrFornecedoresDocum: TWideStringField;
    PnCentroCusto: TPanel;
    Label63: TLabel;
    dmkEdCBCentroCusto: TdmkEditCB;
    dmkCBCentroCusto: TdmkDBLookupComboBox;
    QrCentroCusto: TmySQLQuery;
    IntegerField3: TIntegerField;
    DsCentroCusto: TDataSource;
    QrCentroCustoNome: TWideStringField;
    Label64: TLabel;
    dmkEdQtd2: TdmkEdit;
    Label65: TLabel;
    TPVctoOriginal: TdmkEditDateTimePicker;
    EdModeloNF: TdmkEdit;
    QrCarteirasFornecePadrao: TIntegerField;
    QrCarteirasClientePadrao: TIntegerField;
    EdGenCtb: TdmkEditCB;
    Label66: TLabel;
    CBGenCtb: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    QrContas2: TMySQLQuery;
    QrContas2Codigo: TIntegerField;
    QrContas2Nome: TWideStringField;
    QrContas2Nome2: TWideStringField;
    QrContas2Nome3: TWideStringField;
    QrContas2ID: TWideStringField;
    QrContas2Subgrupo: TIntegerField;
    QrContas2Empresa: TIntegerField;
    QrContas2Credito: TWideStringField;
    QrContas2Debito: TWideStringField;
    QrContas2Mensal: TWideStringField;
    QrContas2Exclusivo: TWideStringField;
    QrContas2Mensdia: TSmallintField;
    QrContas2Mensdeb: TFloatField;
    QrContas2Mensmind: TFloatField;
    QrContas2Menscred: TFloatField;
    QrContas2Mensminc: TFloatField;
    QrContas2Lk: TIntegerField;
    QrContas2Terceiro: TIntegerField;
    QrContas2Excel: TWideStringField;
    QrContas2DataCad: TDateField;
    QrContas2DataAlt: TDateField;
    QrContas2UserCad: TSmallintField;
    QrContas2UserAlt: TSmallintField;
    QrContas2NOMESUBGRUPO: TWideStringField;
    QrContas2NOMEGRUPO: TWideStringField;
    QrContas2NOMECONJUNTO: TWideStringField;
    QrContas2NOMEEMPRESA: TWideStringField;
    QrContas2NOMEPLANO: TWideStringField;
    DsContas2: TDataSource;
    QrContas2Niveis: TWideStringField;
    QrContas1Niveis: TWideStringField;
    DBNiveis1: TDBEdit;
    DBNiveis2: TDBEdit;
    QrContas1Ordens: TWideStringField;
    QrContas2Ordens: TWideStringField;
    SbPsqGCFin: TSpeedButton;
    SbPsqGCCtb: TSpeedButton;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbCarteiraClick(Sender: TObject);
    procedure QrContas1AfterScroll(DataSet: TDataSet);
    procedure EdDuplicataKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabSheet2Resize(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure TPDataChange(Sender: TObject);
    procedure QrCarteirasAfterScroll(DataSet: TDataSet);
    procedure QrCliIntAfterScroll(DataSet: TDataSet);
    procedure SBForneceClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CkParcelamentoClick(Sender: TObject);
    procedure RGPeriodoClick(Sender: TObject);
    procedure DBGParcelasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkArredondarClick(Sender: TObject);
    procedure RGArredondarClick(Sender: TObject);
    procedure EdDiasExit(Sender: TObject);
    procedure RGIncremCHClick(Sender: TObject);
    procedure CkIncremDUClick(Sender: TObject);
    procedure EdDuplSepExit(Sender: TObject);
    procedure RGDuplSeqClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure CkIncremTxtClick(Sender: TObject);
    procedure QrDeptosAfterScroll(DataSet: TDataSet);
    procedure dmkEdCBGeneroChange(Sender: TObject);
    procedure dmkEdMesExit(Sender: TObject);
    procedure dmkEdDebExit(Sender: TObject);
    procedure dmkCBGeneroClick(Sender: TObject);
    procedure dmkEdCredExit(Sender: TObject);
    procedure dmkEdDocChange(Sender: TObject);
    procedure dmkEdTPVenctoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkCBForneceKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdCBDeptoChange(Sender: TObject);
    procedure dmkCBDeptoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkCBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdICMS_PExit(Sender: TObject);
    procedure dmkEdICMS_PKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdICMS_VExit(Sender: TObject);
    procedure dmkEdICMS_VKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdParcelasExit(Sender: TObject);
    procedure dmkEdCBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdTPVenctoChange(Sender: TObject);
    procedure dmkEdTPVenctoClick(Sender: TObject);
    procedure dmkEdDuplicataExit(Sender: TObject);
    procedure dmkEdDocExit(Sender: TObject);
    procedure dmkEdSerieCHExit(Sender: TObject);
    procedure SBClienteClick(Sender: TObject);
    procedure SbEventosCadClick(Sender: TObject);
    procedure dmkEdCBCliIntChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dmkEdCarteiraChange(Sender: TObject);
    procedure BtReabreLctClick(Sender: TObject);
    procedure dmkEdMesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbIndiPagClick(Sender: TObject);
    procedure QrContas1BeforeClose(DataSet: TDataSet);
    procedure QrContas1AfterOpen(DataSet: TDataSet);
    procedure dmkEdTPDataExit(Sender: TObject);
    procedure dmkEdTPVenctoExit(Sender: TObject);
    procedure TbParcpagtosICMS_VSetText(Sender: TField; const Text: string);
    procedure TbParcpagtosCreditoSetText(Sender: TField; const Text: string);
    procedure TbParcpagtosDebitoSetText(Sender: TField; const Text: string);
    procedure TbParcpagtosDataSetText(Sender: TField; const Text: string);
    procedure BtCalculadoraClick(Sender: TObject);
    procedure dmkCBClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrClientesBeforeClose(DataSet: TDataSet);
    procedure QrClientesAfterScroll(DataSet: TDataSet);
    procedure QrFornecedoresBeforeClose(DataSet: TDataSet);
    procedure QrFornecedoresAfterScroll(DataSet: TDataSet);
    procedure dmkEdCBClienteCNPJExit(Sender: TObject);
    procedure dmkEdCBClienteChange(Sender: TObject);
    procedure dmkEdCBClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdCBForneceKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdCBForneceChange(Sender: TObject);
    procedure dmkEdCBForneceCNPJExit(Sender: TObject);
    procedure dmkEdQtdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdQtd2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdCarteiraRedefinido(Sender: TObject);
    procedure dmkEdCBGeneroRedefinido(Sender: TObject);
    procedure EdGenCtbRedefinido(Sender: TObject);
    procedure SbPsqGCFinClick(Sender: TObject);
    procedure SbPsqGCCtbClick(Sender: TObject);
  private
    { Private declarations }
    FCriandoForm: Boolean;
    FValorAParcelar,
    FICMS: Double;
    FIDFinalidade: Integer;
    FParcPagtos: String;
    procedure VerificaEdits;
    procedure CarteirasReopen;
    procedure SetaAccount(var Key: Word; Shift: TShiftState);
    procedure ExigeFuncionario;
    procedure CalculaICMS;
    function ReopenLct: Boolean;

    function VerificaVencimento: Boolean;
    procedure ConfiguraVencimento;
    procedure ConfiguraComponentesCarteira;
    procedure ReopenFornecedores(Tipo, Entidade: Integer);
    procedure ReopenClientes(Tipo, Entidade: Integer);
    procedure ReopenIndiPag();
    // Parcelamento
    procedure CalculaParcelas;
    function GetTipoValor: TTipoValor;
    procedure ConfiguracoesIniciais();
    procedure IncrementaExecucoes();
    procedure EventosCadReopen(Entidade: Integer);
    procedure ReopenContas(Entidade, Codigo, Genero: Integer);
    procedure CadastraEntidade(EntTipo: TEntiTipo; Docum: String);
    procedure ConfiguraNomeRazao(EntTipo: TEntiTipo; Entidade: Integer; Key: Word);
    procedure ConfiguraDadosDocum(EntiTipo: TEntiTipo; EdCNPJ: TdmkEdit;
              EdEntidade: TdmkEditCB; CBEntidade: TdmkDBLookupComboBox;
              Query: TmySQLQuery);
  public
    { Public declarations }
    //FQrCarteiras: TmySQLQuery; Usar UFinanceiro.VLAN_QrCarteiras
    // ??? FCarteira,
    {
    FCNAB_Sit, FFatID, FFatNum, FFAtPArcela, FFatID_Sub, FID_Pgto,
    FNivel, FDescoPor, FCtrlIni, FUnidade: Integer;
    FDescoVal, FNFVal, FPercJur, FPercMulta: Double;
    FDoc2: String;
    FCondPercJuros, FCondPercMulta: Double;
    FTPDataIni, FTPDataFim: TDateTimePicker;
    FDepto: Variant;
    FCondominio: Integer;
    }
    //
    FTabLctA: String;
  end;

const
  FLargMaior = 800;
  FLargMenor = 604;

var
  FmLctEdit: TFmLctEdit;
  Pagto_Doc: Double;

implementation

uses
{$IfNDef NAO_USA_IMP_CHEQUE} UnMyPrinters, {$EndIf}
  UnMyObjects, Module, Entidades, UnFinanceiro, MyDBCheck, Entidade2,
  ModuleFin, ModuleGeral, UCreate, Contas, {###FinForm,}
  {$IfDef UsaContabil}
  UnContabil_Jan,
  {$EndIf}
  EventosCad, IndiPag, UnFinanceiroJan, MyListas;

{$R *.DFM}

procedure TFmLctEdit.CalculaParcelas;
var
  i, Parce, DiasP, Casas: Integer;
  Valor, Valor1, ValorX, ValorA, ValorC, ValorD, Total, Mora, Multa, Fator: Double;
  Data: TDate;
  TipoValor: TTipoValor;
  Duplicata, Descricao, EdDescricao, DuplSeq, DuplSep, Fmt, TxtSeq: String;
begin
  Screen.Cursor := crHourGlass;
  try
    TbParcpagtos.Close;
    FParcPagtos := UCriar.RecriaTempTable('ParcPagtos', DModG.QrUpdPID1, False);
    TbParcpagtos.Database := DModG.MyPID_DB;
    (*
    N�o precisa mais com a recria��o autom�tica!
    TbParcpagtos. Open;
    TbParcpagtos.DisableControls;
    while not TbParcpagtos.Eof do TbParcpagtos.Delete;
    TbParcpagtos.EnableControls;
    TbParcpagtos.Close;
    *)
    if GBParcelamento.Visible then
    begin
      TipoValor := GetTipoValor;
      Pagto_Doc := Geral.DMV(dmkEdDoc.Text);
      DiasP := Geral.IMV(EdDias.Text);
      Parce := Geral.IMV(dmkEdParcelas.Text);
      Mora  := Geral.DMV(dmkEdMoraDia.Text);
      Multa := Geral.DMV(dmkEdMulta.Text);
      Total := FValorAParcelar;
      if Total > 0 then
        Fator := Geral.DMV(dmkEdICMS_V.Text) / Total else Fator := 0;
      if Total <= 0 then Valor := 0
      else
      begin
        Valor := (Total / Parce)*100;
        Valor := (Trunc(Valor))/100;
      end;
      if CkArredondar.Checked then Valor := int(Valor);
      Valor1 := Valor;
      ValorX := Valor;
      if RGArredondar.ItemIndex = 0 then
      begin
        EdParcela1.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
        ValorX := Total - ((Parce - 1) * Valor);
        EdParcelaX.Text := Geral.TFT(FloatToStr(ValorX), 2, siPositivo);
      end else begin
        EdParcelaX.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
        Valor1 := Total - ((Parce - 1) * Valor);
        EdParcela1.Text := Geral.TFT(FloatToStr(Valor1), 2, siPositivo);
      end;
      //Duplicata := dmkPF.IncrementaDuplicata(dmkEdDuplicata.Text, -1);
      Duplicata := dmkEdDuplicata.Text;
      Casas := Length(Geral.FF0(Parce));
      fmt := '';
      for i := 1 to Casas do fmt := fmt + '0';
      DuplSep := Trim(EdDuplSep.Text);
      if DuplSep = '' then DuplSep := ' ';
      for i := 1 to Parce do
      begin
        if i= 1 then ValorA := Valor1
        else if i = Parce then ValorA := ValorX
        else ValorA := Valor;
        if TipoValor = tvCred then ValorC := ValorA else ValorC := 0;
        if TipoValor = tvDeb  then ValorD := ValorA else ValorD := 0;
        //
        if RGPeriodo.ItemIndex = 0 then
          Data := dmkPF.IncrementaMeses(dmkEdTPVencto.Date, i-1, True)
        else
          Data := dmkEdTPVencto.Date + (DiasP * (i-1));
        //
        if dmkEdMes.Enabled then
        begin
          EdDescricao := Trim(StringReplace(dmkEdDescricao.Text, dmkEdMes.Text,
                           '', [rfReplaceAll, rfIgnoreCase]));
          //
          if CkIncremTxt.Checked then
          begin
            case RGSepTxt.ItemIndex of
              0: TxtSeq := FormatFloat(fmt, i);
              1: TxtSeq := dmkPF.IntToColTxt(i);
              else TxtSeq := '?';
            end;
            case RGSepTxt.ItemIndex of
              0: Descricao := FormatFloat(fmt, Parce);
              1: Descricao := dmkPF.IntToColTxt(Parce);
              else TxtSeq := '?';
            end;
            TxtSeq := TxtSeq + EdSepTxt.Text;
            Descricao := TxtSeq + Descricao + ' ' + EdDescricao;
          end else
            Descricao := EdDescricao
        end else
          Descricao := Geral.FF0(i) + '�/'+ Geral.FF0(Parce) + ' ' + dmkEdDescricao.Text;
        //Duplicata := dmkPF.IncrementaDuplicata(Duplicata, 1);
        //
        if CkIncremDU.Checked then
        begin
          case RGDuplSeq.ItemIndex of
            0: DuplSeq := FormatFloat(fmt, i);
            1: DuplSeq := dmkPF.IntToColTxt(i);
            else DuplSeq := '?';
          end;
          DuplSeq := DuplSep + DuplSeq;
        end else DuplSeq := '';
        DmodG.QrUpdPID1.SQL.Clear;
        DmodG.QrUpdPID1.SQL.Add('INSERT INTO parcpagtos SET Parcela=:P0, ');
        DmodG.QrUpdPID1.SQL.Add('Data=:P1, Credito=:P2, Debito=:P3, Doc=:P4, ');
        DmodG.QrUpdPID1.SQL.Add('Mora=:P5, Multa=:P6, ICMS_V=:P7, Duplicata=:P8, ');
        DmodG.QrUpdPID1.SQL.Add('Descricao=:P9 ');
        DmodG.QrUpdPID1.Params[0].AsInteger := i;
        DmodG.QrUpdPID1.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Data);
        DmodG.QrUpdPID1.Params[2].AsFloat := ValorC;
        DmodG.QrUpdPID1.Params[3].AsFloat := ValorD;
        DmodG.QrUpdPID1.Params[4].AsFloat := Pagto_Doc;
        DmodG.QrUpdPID1.Params[5].AsFloat := Mora;
        DmodG.QrUpdPID1.Params[6].AsFloat := Multa;
        DmodG.QrUpdPID1.Params[7].AsFloat := Fator * (ValorC+ValorD);
        DmodG.QrUpdPID1.Params[8].AsString := Duplicata + DuplSeq;
        DmodG.QrUpdPID1.Params[9].AsString := Descricao;
        DmodG.QrUpdPID1.ExecSQL;
        if not RGIncremCH.Enabled then Pagto_Doc := Pagto_Doc
        else Pagto_Doc := Pagto_Doc + RGIncremCH.ItemIndex;
        //
      end;
      TbParcpagtos. Open;
      TbParcpagtos.EnableControls;
    end;
    QrSoma.Close;
    QrSoma.Database := DModG.MyPID_DB;
    UnDmkDAC_PF.AbreQuery(QrSoma, DModG.MyPID_DB);
    EdSoma.Text := FormatFloat('#,###,##0.00', QrSomaVALOR.Value);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctEdit.CarteirasReopen;
begin
  QrCarteiras.Close;
  QrCarteiras.SQL.Clear;
  QrCarteiras.SQL.Add('SELECT car.*');
  QrCarteiras.SQL.Add('FROM carteiras car');
  QrCarteiras.SQL.Add('WHERE car.ForneceI IN (' + VAR_LIB_EMPRESAS + ')');
  QrCarteiras.SQL.Add('AND car.Ativo = 1 ');
  QrCarteiras.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
end;

procedure TFmLctEdit.VerificaEdits;
begin
  if (QrContas1Mensal.Value = 'V') or (Dmod.QrControle.FieldByName('MensalSempre').AsInteger = 1) then
  begin
    dmkEdMes.Enabled := True;
    LaMes.Enabled := True;
  end else begin
    dmkEdMes.Enabled := False;
    LaMes.Enabled := False;
  end;
  //
  if (VAR_BAIXADO <> -2) and not VAR_FATURANDO then Exit;
  if QrContas1Credito.Value = 'V' then
  begin
    dmkEdCred.Enabled          := True;
    LaCred.Enabled             := True;
    dmkEdCBCliente.Enabled     := True;
    LaCliente.Enabled          := True;
    dmkCBCliente.Enabled       := True;
    dmkEdCBClienteCNPJ.Enabled := True;
    SBCliente.Enabled          := True;
  end else
  begin
    dmkEdCred.Enabled          := False;
    LaCred.Enabled             := False;
    dmkEdCBCliente.Enabled     := False;
    LaCliente.Enabled          := False;
    dmkCBCliente.Enabled       := False;
    dmkEdCBClienteCNPJ.Enabled := False;
    SBCliente.Enabled          := False;
  end;

  if QrContas1Debito.Value = 'V' then
  begin
    dmkEdDeb.Enabled           := True;
    LaDeb.Enabled              := True;
    dmkEdCBFornece.Enabled     := True;
    LaFornecedor.Enabled       := True;
    dmkCBFornece.Enabled       := True;
    dmkEdCBForneceCNPJ.Enabled := True;
    SBFornece.Enabled          := True;
  end else
  begin
    dmkEdDeb.Enabled           := False;
    LaDeb.Enabled              := False;
    dmkEdCBFornece.Enabled     := False;
    LaFornecedor.Enabled       := False;
    dmkCBFornece.Enabled       := False;
    dmkEdCBForneceCNPJ.Enabled := False;
    SBFornece.Enabled          := False;
  end;
end;

procedure TFmLctEdit.BtDesisteClick(Sender: TObject);
begin
  Close;
  VAR_VALOREMITIRIMP := 0;
end;

procedure TFmLctEdit.BtReabreLctClick(Sender: TObject);
begin
  if VLAN_QRLCT <> nil then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      VLAN_QRLCT.Close;
      UnDmkDAC_PF.AbreQuery(VLAN_QRLCT, VLAN_QRLCT.Database);
      VLAN_QRLCT.Locate('Controle', FLAN_Controle, []);
      //
      BtReabreLct.Enabled := False;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmLctEdit.BtCalculadoraClick(Sender: TObject);
begin
  dmkPF.MostraCalculadoraDoWindows(Sender);
end;

procedure TFmLctEdit.BtConfirmaClick(Sender: TObject);
var
  Cartao, Carteira, Genero, GenCtb, TipoAnt, CartAnt, NF, Sit, Linha, Depto, Fornecedor,
  Cliente, CliInt, ForneceI, Vendedor, Account, CentroCusto, Funci, Parcelas,
  DifMes, Me1, Me2, EventosCad: Integer;
  Controle: Int64;
  Credito, Debito, MoraDia, Multa, ICMS_V, Difer, MultaVal, MoraVal: Double;
  Mes: Integer;
  MesTxt, Vencimento, Compensado: String;
  Inseriu: Boolean;
  MesX, AnoX: Word;
  DataX: TDateTime;
  ValParc: Double;
  SerieCH: String;
  Documen: Double;
begin
  try
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando inclus�o');
  //
  Me1 := 0;
  //
  if CkParcelamento.Checked and (ImgTipo.SQLType = stUpd) then
  begin
    Geral.MB_Aviso('N�o pode haver parcelamento em altera��o!');
    Exit;
  end;
  //  Veriificar integridade do parcelamento
  if CkParcelamento.Checked then
  begin
    ValParc := 0;
    if dmkEdCred.Enabled then
      ValParc := ValParc + dmkEdCred.ValueVariant;
    if dmkEdDeb.Enabled then
      ValParc := ValParc - dmkEdDeb.ValueVariant;
    if ValParc < 0 then ValParc := ValParc * -1;
    if ValParc <> EdSoma.ValueVariant then
    begin
      Geral.MB_Aviso(
      'Valor do parcelamento n�o confere com d�bito/cr�dito!');
      Exit;
    end;
  end;
  // fim integridade parcelamento

  Screen.Cursor := crHourGlass;
  if dmkEdCBFornece.Enabled = False then Fornecedor := 0 else
    if dmkCBFornece.KeyValue <> Null then
      Fornecedor := dmkCBFornece.KeyValue else Fornecedor := 0;
  if dmkEdCBCliente.Enabled = False then Cliente := 0 else
    if dmkCBCliente.KeyValue <> Null then
      Cliente := dmkCBCliente.KeyValue else Cliente := 0;
  //
  Vendedor    := Geral.IMV(dmkEdCBVendedor.Text);
  Account     := Geral.IMV(dmkEdCBAccount.Text);
  CentroCusto := Geral.IMV(dmkEdCBCentroCusto.Text);
  Depto       := Geral.IMV(dmkEdCBDepto.Text);
  ForneceI    := Geral.IMV(dmkEdCBForneceI.Text);
  CliInt      := Geral.IMV(dmkEdCBCliInt.Text);
  //
  if (CliInt = 0) or not DmodG.EmpresaLogada(CliInt)then
  begin
    Geral.MB_Erro('ERRO! Cliente interno inv�lido!');
    LaCliInt.Enabled := True;
    dmkEdCBCliInt.Enabled := True;
    dmkCBCliInt.Enabled := True;
    if dmkEdCBCliInt.Enabled then dmkEdCBCliInt.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (VAR_AVISOSCXAEDIT in ([1,3,5,7]))
  and (Vendedor < 1) and (dmkCBVendedor.Visible) and ((*Panel3*)GBVendas.Visible) then
  begin
    if Geral.MB_Pergunta(
    'Deseja continuar mesmo sem definir o vendedor?') <> ID_YES then
    begin
      if dmkCBVendedor.Enabled then dmkCBVendedor.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  if (VAR_AVISOSCXAEDIT in ([2,3,6,7]))
  and (Account < 1) and (dmkCBAccount.Visible) then
  begin
    if Geral.MB_Pergunta(
    'Deseja continuar mesmo sem definir o Representante?') <> ID_YES then
    begin
      if dmkCBAccount.Enabled then dmkCBAccount.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  VAR_CANCELA := False;
  if not VerificaVencimento then
  begin
   Screen.Cursor := crDefault;
   Exit;
  end;
  Cartao := 0;
  Compensado := CO_VAZIO;
  if VAR_FATURANDO then
  begin
    {
    desabilitei at� ser necess�rio 2009-12-21
    Cartao := FmPrincipal.CartaoDeFatura;
    Compensado := FmPrincipal.CompensacaoDeFatura;
    }
  end else
  if dmkEdTPCompensado.Date > 1 then
    Compensado := Geral.FDT(dmkEdTPCompensado.Date, 1);
  if (dmkEdNF.Enabled = False) then
    NF := 0
  else
    NF := Geral.IMV(dmkEdNF.Text);
  //
  Credito := Geral.DMV(dmkEdCred.Text);
  if VAR_CANCELA then
  begin
    Geral.MB_Aviso('Inclus�o cancelada pelo usu�rio!');
    //
    if dmkEdNF.Enabled then
      dmkEdNF.SetFocus;
    //
    Screen.Cursor := crDefault;
    Exit;
  end;
  Debito  := Geral.DMV(dmkEdDeb.Text);
  MoraDia := Geral.DMV(dmkEdMoraDia.Text);
  Multa   := Geral.DMV(dmkEdMulta.Text);
  ICMS_V  := Geral.DMV(dmkEdICMS_V.Text);
  Difer   := Credito-Debito;
  if ((Difer<0) and (ICMS_V>0)) or ((Difer>0) and (ICMS_V<0)) then
  begin
    Geral.MB_Aviso('ICMS incorreto! Para d�bitos o ICMS deve ser negativo!');
    if dmkEdICMS_P.Enabled then dmkEdICMS_P.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (dmkEdCred.Enabled = False) and (Credito <> 0) and (VAR_BAIXADO = -2) then
  begin
    dmkEdCred.Text := '0';
    Credito := 0;
  end;
  if (dmkEdDeb.Enabled = False) and (Debito <> 0) and (VAR_BAIXADO = -2) then
  begin
    dmkEdDeb.Text := '0';
    Debito := 0;
  end;
  if (dmkCBCarteira.KeyValue <> Null) and (dmkEdCarteira.ValueVariant <> 0) then
    Carteira := dmkCBCarteira.KeyValue
  else
    Carteira := 0;
  //
  if Carteira = 0 then
  begin
    Geral.MB_Erro('ERRO. Defina uma carteira!');
    if dmkEdCarteira.Enabled then
      dmkEdCarteira.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (dmkCBFunci.Visible = True) and
     ((*Panel3*)GBVendas.Visible = True) and (dmkCBFunci.KeyValue = Null)then
  begin
    Geral.MB_Aviso('ERRO. Defina um funcion�rio!');
    if dmkEdCBFunci.Enabled
    and dmkEdCBFunci.Visible then
    begin
      PageControl1.ActivePageIndex := 1;
      dmkEdCBFunci.SetFocus;
    end;
    Screen.Cursor := crDefault;
    Exit;
  end else Funci := Geral.IMV(dmkEdCBFunci.Text);
  if Funci = 0 then Funci := VAR_USUARIO;
  Genero := Geral.IMV(dmkEdCBGenero.Text);
  if Genero = 0 then
  begin
    Geral.MB_Aviso('ERRO. Defina uma conta!');
    if dmkEdCBGenero.Enabled then dmkEdCBGenero.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  GenCtb := EdGenCtb.ValueVariant;
  if DModG.QrCtrlGeralUsaGenCtb.Value = 1 then
    if MyObjects.FIC(GenCtb = 0, EdGenCtb, 'Informe a conta cont�bil!') then
      Exit;
  //
  if (Credito > 0) and (QrContas1Credito.Value = 'F') then
  begin
    Geral.MB_Aviso('ERRO. Valor n�o pode ser cr�dito');
    dmkEdCred.Text;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Debito > 0) and (QrContas1Debito.Value = 'F') then
  begin
    Geral.MB_Aviso('ERRO. Valor n�o pode ser d�bito');
    dmkEdDeb.Text;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (dmkEdCred.Enabled = False) and (dmkEdDeb.Enabled = False) and
  (VAR_BAIXADO = -2) then
  begin
    Geral.MB_Aviso('ERRO. Opera��o imposs�vel. Conta sem permiss�o de d�bito ou cr�dito.');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Credito = 0) and (Debito = 0) and (CkCancelado.Checked = False) then
  begin
    Geral.MB_Aviso('ERRO. Defina um d�bito ou um cr�dito.');
    if dmkEdDeb.Enabled = True then dmkEdDeb.SetFocus
    else if dmkEdCred.Enabled = True then dmkEdCred.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if dmkEdMes.Text = CO_VAZIO then Mes := 0 else
  if (dmkEdMes.Enabled = False) then Mes := 0 else
  begin
    MesTxt := (*dmkEdMes.Text[4]+dmkEdMes.Text[5]+*)dmkEdMes.Text[6]+dmkEdMes.Text[7]+(*'/'+*)
    dmkEdMes.Text[1]+dmkEdMes.Text[2](*+'/01'*);
    Mes := Geral.IMV(MesTxt);
    Me1 := Geral.Periodo2000(dmkEdTPVencto.Date);
    AnoX := Geral.IMV(Copy(MesTxt, 1, 2)) + 2000;
    MesX := Geral.IMV(Copy(MesTxt, 3, 2));
    DataX := EncodeDate(AnoX, MesX, 1);
    Me2 := Geral.Periodo2000(DataX);
    DifMes := Me2 - Me1;
    if Mes = 0 then
    begin
      Geral.MB_Aviso('ERRO. Defina o m�s do vencimento.');
      if dmkEdMes.Enabled then dmkEdMes.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  if (Mes = 0) and (dmkEdMes.Enabled = True) then
  begin
    Geral.MB_Aviso('ERRO. Defina o m�s do vencimento.');
    if dmkEdMes.Enabled then dmkEdMes.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  // n�o mexer no vencimento aqui!
  {if (dmkEdTPVencto.Enabled = False) then Vencimento :=
    FormatDateTime(VAR_FORMATDATE, dmkEdTPdata.Date)
  else}
  Vencimento := FormatDateTime(VAR_FORMATDATE, dmkEdTPVencto.Date);
  //
  if VAR_IMPORTANDO then
    Linha := VAR_IMPLINHA
  else
    Linha := 0;
  //
  if VAR_FATURANDO then
    Sit := 3
  else if VAR_IMPORTANDO then
    Sit := -1
  else if VAR_BAIXADO <> -2 then
    Sit := VAR_BAIXADO
  else if QrCarteirasTipo.Value = 2 then
  begin
    Compensado := '';
    Sit        := 0;
  end else
    Sit := 3;
  //
  TipoAnt  := StrToInt(dmkEdTipoAnt.Text);
  CartAnt  := StrToInt(dmkEdCartAnt.Text);
  MultaVal := Geral.DMV(dmkEdMultaVal.Text);
  MoraVal  := Geral.DMV(dmkEdMoraVal.Text);
  //
  if ImgTipo.SQLType = stIns then
  begin
    Controle        := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', FTabLctA, LAN_CTOS, 'Controle');
    VAR_DATAINSERIR := dmkEdTPdata.Date;
  end else
  begin
    Controle := Geral.IMV(dmkEdControle.Text);
{
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add(EXCLUI_DE + FTabLctA + ' WHERE Controle=:P0');
    Dmod.QrUpdM.Params[0].AsFloat := Controle;
    Dmod.QrUpdM.ExecSQL;
}
    UFinanceiro.ExcluiLct_Unico(FTabLcta, Dmod.MyDB, TPOldData.Date,
      EdOldTipo.ValueVariant, EdOldCarteira.ValueVariant,
      EdOldControle.ValueVariant, EdOldSub.ValueVariant,
      dmkPF.MotivDel_ValidaCodigo(100), False);
    UFinanceiro.RecalcSaldoCarteira(CartAnt, nil, nil, False, False);
    VAR_DATAINSERIR := dmkEdTPdata.Date;
  end;

 if CkCancelado.Checked then
  begin
    Credito    := 0;
    Debito     := 0;
    Compensado := FormatDateTime(VAR_FORMATDATE, dmkEdTPData.Date);
    Sit        := 4;
  end;

  {
  Colocar em baixo de onde obtem o n�mero do cheque para n�o dar erro (Continuar Inserindo!
  if not CkNaoPesquisar.Checked then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando duplica��es');
    if UFinanceiro.NaoDuplicarLancto(ImgTipo.SQLType, dmkEdDoc.ValueVariant,
    CkPesqCH.Checked, dmkEdSerieCH.Text, dmkEdNF.ValueVariant, CkPesqNF.Checked,
    Credito, Debito, Genero, Mes, CkPesqVal.Checked,
    Geral.IMV(dmkEdCBCliInt.Text), False, dmkEdCarteira.ValueVariant,
    dmkEdCBCliente.ValueVariant, dmkEdCBFornece.ValueVariant,
    LaAviso, FtabLctA, '', '') <> 1 then
    begin
      Geral.MB_Aviso('Inclus�o cancelada pelo usu�rio!');
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...';
      Exit;
    end;
  end;
  }

  //

  if (ImgTipo.SQLType = stUpd) then
  begin
    Documen := dmkEdDoc.ValueVariant;
    SerieCH := dmkEdSerieCH.Text;
  end else
  if (QrCarteirasUsaTalao.Value = 1) and (ImgTipo.SQLType = stIns)  and
  (dmkEdDoc.Enabled = False) and CkDuplicando.Checked then
  begin
    Documen := dmkEdDoc.ValueVariant;
    SerieCH := dmkEdSerieCH.Text;
    //
  end else
  {
  Ap�s obter o novo n�mero ele seta o campo e d� erro no Continuar Inserindo.
  if (QrCarteirasUsaTalao.Value = 1) and (ImgTipo.SQLType = stIns) and
  (dmkEdDoc.ValueVariant = 0) then
  }
  if (QrCarteirasUsaTalao.Value = 1) and (ImgTipo.SQLType = stIns) and
  (dmkEdDoc.Enabled = False) then
  begin
    if CkParcelamento.Checked then
    begin
      if UMyMod.ObtemQtdeCHTaloes(QrCarteirasCodigo.Value) < TbParcpagtos.RecordCount then
        if Geral.MB_Pergunta(
        'N�o existem folhas de cheque suficientes para todos lan�amentos.' +
        sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then
          Exit;
    end;
    //  Parcelamento ser� feito adiante
    if CkParcelamento.Checked = False then
    begin
      if not UMyMod.ObtemProximoCHTalao(QrCarteirasCodigo.Value,
      dmkEdSerieCH, dmkEdDoc) then
        Exit
      else begin
        Documen := dmkEdDoc.ValueVariant;
        SerieCH := dmkEdSerieCH.Text;
      end;
    end;
  end else// Doc := dmkEdDoc.ValueVariant;
  if (dmkEdDoc.Enabled = False) and (VAR_BAIXADO = -2) then
  Documen := 0
  else begin
    Documen := Geral.DMV(dmkEdDoc.Text);
    SerieCH := dmkEdSerieCH.Text;
    if (QrCarteirasExigeNumCheque.Value = 1) and (Documen=0) then
    begin
      Geral.MB_Aviso('Esta carteira exige o n�mero do cheque (documento)!');
      PageControl1.ActivePageIndex := 0;
      dmkEdDoc.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;

  if not CkNaoPesquisar.Checked then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando duplica��es');
    if UFinanceiro.NaoDuplicarLancto(ImgTipo.SQLType, dmkEdDoc.ValueVariant,
    CkPesqCH.Checked, dmkEdSerieCH.Text, dmkEdNF.ValueVariant, CkPesqNF.Checked,
    dmkEdSerieNF.Text, Credito, Debito, Genero, Mes, CkPesqVal.Checked,
    Geral.IMV(dmkEdCBCliInt.Text), False, dmkEdCarteira.ValueVariant,
    dmkEdCBCliente.ValueVariant, dmkEdCBFornece.ValueVariant, LaAviso1,
    LaAviso2, FtabLctA, '', '', CkPesqEntValData.Checked, dmkEdTPData.Date) <> 1 then
    begin
      Geral.MB_Aviso('Inclus�o cancelada pelo usu�rio!');
      MyObjects.Informa2(LaAviso1, LaAviso2, True, '...');
      Exit;
    end;
  end;

  VAR_LANCTO2 := Controle;

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo dados');
  UFinanceiro.LancamentoDefaultVARS;
  FLAN_EventosCad := EdEventosCad.ValueVariant;            if FLAN_EventosCad <> 0 then
    FLAN_EventosCad := QrEventosCadCodigo.Value;
  FLAN_Documento  := Trunc(Documen);
  FLAN_Data       := FormatDateTime(VAR_FORMATDATE, dmkEdTPdata.Date);
  FLAN_Tipo       := QrCarteirasTipo.Value;
  FLAN_Carteira   := Carteira;
  FLAN_Credito    := Credito;
  FLAN_Debito     := Debito;
  FLAN_Genero     := Genero;  // OK! para n�o uso no Cont�bil!!!!
  FLAN_GenCtb     := GenCtb;
  FLAN_GenCtbD    := 0; // Para usar ver em LctEdit2
  FLAN_GenCtbC    := 0; // Para usar ver em LctEdit2
  FLAN_SerieNF    := dmkEdSerieNF.ValueVariant;
  FLAN_NotaFiscal := NF;
  FLAN_Vencimento := Vencimento;
  FLAN_Mez        := Mes;
  FLAN_Descricao  := dmkEdDescricao.Text;
  FLAN_Sit        := Sit;
  FLAN_Controle   := Controle;
  FLAN_Cartao     := Cartao;
  //
  if Compensado = '' then
    FLAN_Compensado := '0000-00-00'
  else
    FLAN_Compensado := Compensado;
  //
  FLAN_Linha       := Linha;
  FLAN_Fornecedor  := Fornecedor;
  FLAN_Cliente     := Cliente;
  FLAN_MoraDia     := MoraDia;
  FLAN_Multa       := Multa;
  FLAN_DataCad     := FormatDateTime(VAR_FORMATDATE, Date);
  FLAN_UserCad     := Funci;
  FLAN_DataDoc     := FormatDateTime(VAR_FORMATDATE, dmkEdTPDataDoc.Date);
  FLAN_Vendedor    := Vendedor;
  FLAN_Account     := Account;
  FLAN_CentroCusto := CentroCusto;
  FLAN_FatID       := dmkEdFatID.ValueVariant;
  FLAN_FatID_Sub   := dmkEdFatID_Sub.ValueVariant;
  FLAN_ICMS_P      := Geral.DMV(dmkEdICMS_P.Text);
  FLAN_ICMS_V      := Geral.DMV(dmkEdICMS_V.Text);
  FLAN_Duplicata   := dmkEdDuplicata.Text;
  FLAN_CliInt      := CliInt;
  FLAN_Depto       := Depto;
  FLAN_DescoPor    := dmkEdDescoPor.ValueVariant;
  FLAN_ForneceI    := ForneceI;
  FLAN_DescoVal    := dmkEdDescoVal.ValueVariant;
  FLAN_NFVal       := dmkEdNFVAl.ValueVariant;
  FLAN_Unidade     := dmkEdUnidade.ValueVariant;
  //FLAN_Qtde       := Qtde;  ABAIXO
  FLAN_FatNum      := dmkPF.I64MV(dmkEdFatNum.ValueVariant);
  FLAN_FatParcela  := dmkedFatParcela.ValueVariant;
  FLAN_Doc2        := dmkEdDoc2.ValueVariant;
  FLAN_SerieCH     := dmkEdSerieCh.Text;
  FLAN_MultaVal    := MultaVal;
  FLAN_MoraVal     := MoraVal;
  FLAN_TipoCH      := dmkRGTipoCH.ItemIndex;
  FLAN_IndiPag     := EdIndiPag.ValueVariant;
  FLAN_FisicoSrc   := RGFisicoSrc.ItemIndex;
  FLAN_FisicoCod   := EdFisicoCod.ValueVariant;
  // Evitar erro na duplica��o!
  if ImgTipo.SQLType = stUpd then
  begin
    FLAN_ID_Pgto  := dmkEdID_Pgto.ValueVariant;
    FLAN_CNAB_Sit := dmkEdCNAB_Sit.ValueVariant;
    FLAN_Nivel    := dmkEdNivel.ValueVariant;
    FLAN_CtrlIni  := dmkEdCtrlIni.ValueVariant;
  end;
  if dmkEdQtde.Text <> '' then
    FLAN_Qtde := Geral.DMV(dmkEdQtde.Text);
  if dmkEdQtd2.Text <> '' then
    FLAN_Qtd2 := Geral.DMV(dmkEdQtd2.Text);
  FLAN_VctoOriginal := Geral.FDT(TPVctoOriginal.Date, 1);
  FLAN_ModeloNF := EdModeloNF.Text;

  Parcelas := 0;
  if CkParcelamento.Checked then
  begin
    TbParcpagtos.First;
    while not TbParcpagtos.Eof do
    begin
      if (Me1 > 0) then
      begin
        Me2            := Geral.Periodo2000(TbParcpagtosData.Value) + DifMes;
        FLAN_Mez       := dmkPF.PeriodoToAnoMes(Me2);
        FLAN_Descricao := TbParcPagtosDescricao.Value;
        //
        if (CkIncremTxt.Checked = False) then
          FLAN_Descricao  := FLAN_Descricao + ' ' + dmkPF.MesEAnoDoPeriodo(Me2)
      end else
      begin
        FLAN_Mez       := 0;
        FLAN_Descricao := TbParcPagtosDescricao.Value;
      end;
      FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', FTabLctA, LAN_CTOS, 'Controle');
      FLAN_Duplicata  := TbParcPagtosDuplicata.Value;
      FLAN_Credito    := TbParcpagtosCredito.Value;
      FLAN_Debito     := TbParcpagtosDebito.Value;
      FLAN_MoraDia    := dmkEdPercJuroM.ValueVariant;//FmCondGer.QrCondPercJuros.Value;
      FLAN_Multa      := dmkEdPercMulta.ValueVariant;//FmCondGer.QrCondPercMulta.Value;
      FLAN_ICMS_V     := TbParcpagtosICMS_V.Value;
      FLAN_Vencimento := Geral.FDT(TbParcpagtosData.Value, 1);
      FLAN_Sit        := Sit;
      if (QrCarteirasUsaTalao.Value = 1) and (ImgTipo.SQLType = stIns)  and
      (dmkEdDoc.Enabled = False) and CkDuplicando.Checked then
      begin
        FLAN_Documento := dmkEdDoc.ValueVariant;
        FLAN_SerieCH   := dmkEdSerieCH.Text;
      end else
      if (QrCarteirasUsaTalao.Value = 1) and (ImgTipo.SQLType = stIns)  and
      (CkDuplicando.Checked = False) then
      begin
        if not UMyMod.ObtemProximoCHTalao(QrCarteirasCodigo.Value,
        dmkEdSerieCH, dmkEdDoc) then
          Exit
        else
        begin
          FLAN_Documento := dmkEdDoc.ValueVariant;
          FLAN_SerieCH   := dmkEdSerieCH.Text;
        end;
      end else FLAN_Documento := TbParcpagtosDoc.Value;
      //
      if UFinanceiro.InsereLancamento(FTabLctA) then
      begin
        Inc(Parcelas, 1);
        IncrementaExecucoes;
      end;
      TbParcPagtos.Next;
    end;
    Inseriu := TbParcPagtos.RecordCount = Parcelas;
    if not Inseriu then
      Geral.MB_Aviso('ATEN��O! Foram inseridas ' +
      Geral.FF0(Parcelas) + ' de ' + Geral.FF0(TbParcPagtos.RecordCount) + '!');
  end else Inseriu := UFinanceiro.InsereLancamento(FTabLctA);


  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Incrementando execu��es');
  if Inseriu then
  begin
    IncrementaExecucoes;
    //
    if ImgTipo.SQLType = stIns then
      UFinanceiro.RecalcSaldoCarteira(dmkCBCarteira.KeyValue, nil, nil, False, False)
    else// begin if CBNovo.Checked = False then
    begin
      UFinanceiro.RecalcSaldoCarteira(dmkCBCarteira.KeyValue, nil, nil, False, False);
      if CartAnt <> dmkCBCarteira.KeyValue then
        UFinanceiro.RecalcSaldoCarteira(CartAnt, nil, nil, False, False);
    end;
    //
    VAR_VALOREMITIRIMP := VAR_VALOREMITIRIMP - Credito - Debito;
    {###
    if CkCopiaCH.Checked then
      FmFinForm.ImprimeCopiaDeCh(FLAN_Controle, Geral.IMV(dmkEdCBCliInt.Text));
    }
    if (CkContinuar.Checked) and (ImgTipo.SQLType = stIns) then
    begin
      CkParcelamento.Checked       := False;
      PageControl1.ActivePageIndex := 0;
      Geral.MB_Info('Inclus�o concluida.');
      if Pagecontrol1.ActivePageIndex = 0 then
      if dmkEdTPData.Enabled then dmkEdTPData.SetFocus else dmkEdCBGenero.SetFocus;
      BtReabreLct.Enabled := True;
      //
      LaCarteira.Enabled := True;
      dmkEdCarteira.Enabled := True;
      dmkCBCarteira.Enabled := True;
    end else Close;
  end;
  finally
    Screen.Cursor := crDefault;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  end;
end;

procedure TFmLctEdit.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Contin: Integer;
begin
  if CkContinuar.Checked then
    Contin := 1
  else
    Contin := 0;
  Geral.WriteAppKeyCU('CkContinuar', Application.Title + '\LctEdit', Contin, ktInteger);
end;

procedure TFmLctEdit.FormCreate(Sender: TObject);
var
  CliInt: Integer;
begin
  ImgTipo.SQLType := stIns;
  //
  DBNiveis1.DataField := '';
  DBNiveis2.DataField := '';
  //
  VAVcto.ValueVariant := False;
  //
  MyObjects.ConfiguraRadioGroup(RGFisicoSrc, CO_TAB_INDX_CB4_DESCREVE,
    Length(CO_TAB_INDX_CB4_DESCREVE), 0);
  //
  dmkEdTPData.MinDate := VAR_DATA_MINIMA;
  PnLancto1.BevelOuter   := bvNone;
  PnDepto.BevelOuter     := bvNone;
  PnForneceI.BevelOuter  := bvNone;
  PnAccount.BevelOuter   := bvNone;
  PnCentroCusto.BevelOuter := bvNone;
  //PnEdit05.BevelOuter := bvNone;
  //PnEdit06.BevelOuter := bvNone;
  PnLancto2.BevelOuter  := bvNone;
  //PnLancto3.BevelOuter  := bvNone;
  //Width := FLargMenor;
  FCriandoForm := True;
  // 2012-05-15
  //UnDmkDAC_PF.AbreQuery(QrContas1, Dmod.MyDB);
  QrCliInt.Close;
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  //
  ReopenContas(0, 0, 0);
  // FIM 2012-05-15
  if VAR_CXEDIT_ICM then
  begin
    LaICMS_P.Visible := True;
    LaICMS_V.Visible := True;
    dmkEdICMS_P.Visible := True;
    dmkEdICMS_V.Visible := True;
  end;
  //if Trim(VAR_CLIENTEC) <> '' then
  //begin
    ReopenClientes(0, 0);
    //////
    LaCliente.Visible          := True;
    dmkEdCBCliente.Visible     := True;
    dmkCBCliente.Visible       := True;
    dmkEdCBClienteCNPJ.Visible := True;
    //////
    //dmkEdCBCliInt.Enabled := False;
    //dmkCBCliInt.Enabled := False;
    //if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
    CliInt := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
    dmkEdCBCliInt.Text := Geral.FF0(CliInt);
    dmkCBCliInt.KeyValue := CliInt;
  //end;
  ReopenFornecedores(0, 0);
  //////
  QrCentroCusto.SQL.Clear;
  QrCentroCusto.SQL.Add('SELECT Codigo, Nome');
  QrCentroCusto.SQL.Add('FROM centrocusto');
  QrCentroCusto.SQL.Add('WHERE Ativo = 1');
  QrCentroCusto.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrCentroCusto, Dmod.MyDB);
  //
  if Trim(VAR_FORNECEF) <> '' then
  begin
    //
    LaFornecedor.Visible       := True;
    dmkEdCBFornece.Visible     := True;
    dmkCBFornece.Visible       := True;
    dmkEdCBForneceCNPJ.Visible := True;
  end;
  if Trim(VAR_FORNECEV) <> '' then
  begin
    QrVendedores.SQL.Clear;
    QrVendedores.SQL.Add('SELECT Codigo, ');
    QrVendedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrVendedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrVendedores.SQL.Add('FROM entidades');
    QrVendedores.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrVendedores.SQL.Add('ORDER BY NOMEENTIDADE');
    UnDmkDAC_PF.AbreQuery(QrVendedores, Dmod.MyDB);
    //////
    QrAccounts.SQL.Clear;
    QrAccounts.SQL.Add('SELECT Codigo, ');
    QrAccounts.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrAccounts.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrAccounts.SQL.Add('FROM entidades');
    QrAccounts.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrAccounts.SQL.Add('ORDER BY NOMEENTIDADE');
    UnDmkDAC_PF.AbreQuery(QrAccounts, Dmod.MyDB);
    //////
    LaVendedor.Visible      := True;
    dmkEdCBVendedor.Visible := True;
    dmkCBVendedor.Visible   := True;
    //2021-01-09 ini
    //PnAccount.Visible       := True;
    if PnCentroCusto.Visible then
    begin
      PnCentroCusto.Visible := False;
      PnCentroCusto.Visible := True;
    end;
    if PnAccount.Visible then
    begin
      PnAccount.Visible := False;
      PnAccount.Visible := True;
      PnAccount.Top := PnCentroCusto.Top - 30;
    end;
    //2021-01-09 fim
  end;
  dmkEdTPDataDoc.Date := Date;
  if VAR_LANCTOCONDICIONAL1 then
  begin
    VAR_DATAINSERIR := Date;
    dmkEdTPDataDoc.Enabled := False;
    LaDataDoc.Enabled := False;
    //
    dmkEdCarteira.Enabled := False;
    dmkCBCarteira.Enabled := False;
    dmkEdTPdata.Enabled := False;
  end;
  if VAR_TIPOVARCLI <> '' then ExigeFuncionario;
  PageControl1.ActivePageIndex := 0;
  TabControl1.TabIndex := 0;
  //
  CkContinuar.Checked := Geral.ReadAppKeyCU('CkContinuar', Application.Title +
  '\LctEdit', ktInteger, 0);
  ReopenIndiPag();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrContas2, Dmod.MyDB, [
  'SELECT CONCAT( ',
  '  pl.Codigo, ".", ',
  '  cj.Codigo, ".", ',
  '  gr.Codigo, ".", ',
  '  sg.Codigo, ".", ',
  '  co.Codigo) Niveis, ',
  'CONCAT( ',
  '  pl.OrdemLista, ".", ',
  '  cj.OrdemLista, ".", ',
  '  gr.OrdemLista, ".", ',
  '  sg.OrdemLista, ".", ',
  '  co.OrdemLista) Ordens, ',
  'co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO, ',
  'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO, ',
  'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA ',
  'FROM contas co ',
  'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo ',
  'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo ',
  'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto ',
  'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano ',
  'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa ',
(*
  'WHERE co.Terceiro=0 ',
  'AND co.Codigo>0 ',
  SQL1,
  SQL2,
*)
  'ORDER BY co.Nome ',
  '']);
end;

procedure TFmLctEdit.FormActivate(Sender: TObject);
var
  Carteira: Integer;
begin
  FTabLctA := EdTabLctA.Text;
  if UFinanceiro.TabLctNaoDefinida(FTabLctA, 'TFmLctEdit.FormActivate()') then
    Close;
  CarteirasReopen;
  if ImgTipo.SQLType = stIns then
  begin
    CkContinuar.Visible := True;
    CkParcelamento.Visible := True;
    //
    if VLAN_QrCarteiras <> nil then
    begin
      Carteira := VLAN_QrCarteiras.FieldByName('Codigo').AsInteger;
      if Carteira <> 0 then
      begin
        dmkEdCarteira.ValueVariant := Carteira;
        dmkCBCarteira.KeyValue     := Carteira;
      end;
    end;
  end;
  if dmkEdCarteira.Enabled then dmkEdCarteira.SetFocus;
  VerificaEdits;
  if FCriandoForm then
  begin
    ConfiguracoesIniciais;
  end;
  FCriandoForm := False;
  MyObjects.CorIniComponente();
end;

procedure TFmLctEdit.ConfiguraComponentesCarteira;
begin
  case QrCarteirasTipo.Value of
    0:
    begin
      if QrCarteirasPrazo.Value = 1 then
      begin
        LaDoc.Enabled := True;
        dmkEdSerieCH.Enabled := True;
        dmkEdDoc.Enabled := True;
        dmkEdNF.Enabled := True;
        LaNF.Enabled := True;
      end else begin
        // N�mero bloqueto no cheque
        LaDoc.Enabled := True;
        dmkEdSerieCH.Enabled := True;
        dmkEdDoc.Enabled := True;
        LaDoc.Enabled := True;
        //
        dmkEdNF.Enabled := True;
        LaNF.Enabled := True;
      end;
    end;
    1:
    begin
      LaDoc.Enabled := True;
      dmkEdSerieCH.Enabled := True;
      dmkEdDoc.Enabled := True;
    end;
    2:
    begin
      LaDoc.Enabled := True;
      dmkEdSerieCH.Enabled := True;
      dmkEdDoc.Enabled := True;
      //
      dmkEdNF.Enabled := True;
      LaNF.Enabled := True;
    end;
  end;
  ConfiguraVencimento;
end;

procedure TFmLctEdit.BtContasClick(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := dmkEdCBCliInt.ValueVariant;
  //
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.LocCod(QrContas1Codigo.Value, QrContas1Codigo.Value);
    FmContas.ShowModal;
    FmContas.Destroy;
    // 2012-05-15
    //QrContas1.Close;
    //UnDmkDAC_PF.AbreQuery(QrContas1, Dmod.MyDB);
    ReopenContas(Entidade, 0, 0);
    // FIM 2012-05-15
    if VAR_CADASTRO <> 0 then
    begin
      dmkEdCBGenero.Text   := Geral.FF0(VAR_CONTA);
      dmkCBGenero.KeyValue := VAR_CONTA;
    end;
    //
    if (ImgTipo.SQLType = stIns) and (CkDuplicando.Checked = False) then
      dmkEdDescricao.Text := dmkCBGenero.Text;
    //Dmod.DefParams;
  end;
end;

function TFmLctEdit.VerificaVencimento: Boolean;
var
  Carteira: Integer;
  Data: TDateTime;
begin
  Result := True;
  if ImgTipo.SQLType = stIns then
  begin
    if QrCarteirasFatura.Value = 'V' then
    begin
      if dmkCBCarteira.KeyValue <> Null then Carteira := dmkCBCarteira.KeyValue
      else Carteira := 0;
      QrFatura.Close;
      QrFatura.Params[0].AsInteger := Carteira;
      UnDmkDAC_PF.AbreQuery(QrFatura, Dmod.MyDB);
      if (QrFatura.RecordCount > 0) and (dmkCBCarteira.KeyValue <> Null) then
      begin
        Data := QrFaturaData.Value;
        while (Data - QrCarteirasFechamento.Value) < dmkEdTPdata.Date do
          Data := IncMonth(Data, 1);
        if int(dmkEdTPVencto.Date) <> Int(Data) then
        begin
          case Geral.MB_Pergunta(
          'A configura��o do sistema sugere a data de vencimento '+
          FormatDateTime(VAR_FORMATDATE3, Data)+' ao inv�s de '+
          FormatDateTime(VAR_FORMATDATE3, dmkEdTPVencto.Date)+
          '. Confirma a altera��o?') of
            ID_YES    : dmkEdTPVencto.Date := Data;
            ID_CANCEL : Result := False;
            ID_NO     : ;
          end;
        end;
      end;
      QrFatura.Close;
    end;
  end;
end;

procedure TFmLctEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctEdit.SbCarteiraClick(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := dmkEdCarteira.ValueVariant;
  //
  FinanceiroJan.CadastroDeCarteiras(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrCarteiras.Close;
    UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
    //
    dmkEdCarteira.ValueVariant := VAR_CADASTRO;
    dmkCBCarteira.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmLctEdit.SbEventosCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEventosCad, FmEventosCad, afmoNegarComAviso) then
  begin
    FmEventosCad.ShowModal;
    FmEventosCad.Destroy;
    //
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdEventosCad, CBEventosCad, QrEventosCad,
        VAR_CADASTRO);
    end;
  end;
end;

procedure TFmLctEdit.SbIndiPagClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmIndiPag, FmIndiPag, afmoNegarComAviso) then
  begin
    FmIndiPag.ShowModal;
    FmIndiPag.Destroy;
    //
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodigoPesquisado(EdIndiPag, CBIndiPag, QrIndiPag,
        VAR_CADASTRO);
    end;
  end;
end;

procedure TFmLctEdit.SbPsqGCCtbClick(Sender: TObject);
var
  CtaFin, CtaCtb: Integer;
  UsarAsDuas: Boolean;
begin
  {$IfDef UsaContabil}
    CtaFin := dmkEdCBGenero.ValueVariant;
    CtaCtb := EdGenCtb.ValueVariant;
    Contabil_Jan.MostraFormCtbGruPsq(CtaFin, CtaCtb, UsarAsDuas);
    EdGenCtb.ValueVariant := CtaCtb;
    CBGenCtb.KeyValue     := CtaCtb;
    if UsarAsDuas then
    begin
      dmkEdCBGenero.ValueVariant := CtaFin;
      dmkCBGenero.KeyValue     := CtaFin;
    end;
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappContabil);
  {$EndIf}
end;

procedure TFmLctEdit.SbPsqGCFinClick(Sender: TObject);
var
  CtaFin, CtaCtb: Integer;
  UsarAsDuas: Boolean;
begin
  {$IfDef UsaContabil}
    CtaFin := dmkEdCBGenero.ValueVariant;
    CtaCtb := EdGenCtb.ValueVariant;
    Contabil_Jan.MostraFormCtbGruPsq(CtaFin, CtaCtb, UsarAsDuas);
    dmkEdCBGenero.ValueVariant := CtaFin;
    dmkCBGenero.KeyValue     := CtaFin;
    if UsarAsDuas then
    begin
      EdGenCtb.ValueVariant := CtaCtb;
      CBGenCtb.KeyValue     := CtaCtb;
    end;
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappContabil);
  {$EndIf}
end;

procedure TFmLctEdit.QrContas1AfterOpen(DataSet: TDataSet);
begin
  LaContas.Caption := Geral.FFN(QrContas1.RecordCount, 3);
end;

procedure TFmLctEdit.QrContas1AfterScroll(DataSet: TDataSet);
begin
  VerificaEdits;
end;

procedure TFmLctEdit.QrContas1BeforeClose(DataSet: TDataSet);
begin
  LaContas.Caption := '000';
end;

procedure TFmLctEdit.SetaAccount(var Key: Word; Shift: TShiftState);
begin
  if key=VK_F4 then
  begin
    dmkEdCBAccount.Text   := Geral.FF0(QrClientesAccount.Value);
    dmkCBAccount.KeyValue := QrClientesAccount.Value;
  end;
end;

procedure TFmLctEdit.ExigeFuncionario;
begin
  QrFunci.Close;
  QrFunci.SQL.Clear;
  QrFunci.SQL.Add('SELECT Codigo,');
  QrFunci.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
  QrFunci.SQL.Add('ELSE Nome END NOMEENTIDADE');
  QrFunci.SQL.Add('FROM entidades');
  QrFunci.SQL.Add('WHERE '+VAR_TIPOVARCLI+'="V"');
  QrFunci.SQL.Add('ORDER BY NomeENTIDADE');
  UnDmkDAC_PF.AbreQuery(QrFunci, Dmod.MyDB);
  //
  Label10.Visible      := True;
  dmkEdCBFunci.Visible := True;
  dmkCBFunci.Visible   := True;
end;

procedure TFmLctEdit.CadastraEntidade(EntTipo: TEntiTipo; Docum: String);
var
  CNPJ, CPF: String;
  Cliente1, Cliente2, Cliente3, Cliente4, Fornece1, Fornece2, Fornece3,
  Fornece4, Fornece5, Fornece6, Fornece7, Fornece8, Terceiro: Boolean;
begin
  if Docum = '' then
    Exit;
  //
  if Geral.MB_Pergunta('Entidade n�o localizada!' + sLineBreak + 'Deseja cadastr�-la?') = ID_YES then
  begin
    if Length(CPF) = 11 then
    begin
      CNPJ := '';
      CPF  := Docum;
    end else
    begin
      CNPJ := Docum;
      CPF  := '';
    end;
    //
    Cliente1 := False;
    Cliente2 := False;
    Cliente3 := False;
    Cliente4 := False;
    Fornece1 := False;
    Fornece2 := False;
    Fornece3 := False;
    Fornece4 := False;
    Fornece5 := False;
    Fornece6 := False;
    Fornece7 := False;
    Fornece8 := False;
    Terceiro := False;
    //
    if CO_DMKID_APP = 29 then //Infrajob
    begin
      if EntTipo = tetCliente then
        Cliente1 := True
      else if EntTipo = tetFornece then
        Fornece1 := True;
    end;
    //
    if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
    begin
      FmEntidade2.Incluinovaentidade1Click(Self);
      //
      FmEntidade2.PreCadastro(CNPJ, CPF, Cliente1, Cliente2, Cliente3, Cliente4,
        Fornece1, Fornece2, Fornece3, Fornece4, Fornece5, Fornece6, Fornece7,
        Fornece8, Terceiro);
      //
      FmEntidade2.ShowModal;
      FmEntidade2.Destroy;
    end;
  end;
end;

procedure TFmLctEdit.CalculaICMS;
var
  C, D, B, V, P: Double;
begin
  V := Geral.DMV(dmkEdICMS_V.Text);
  P := Geral.DMV(dmkEdICMS_P.Text);
  C := Geral.DMV(dmkEdCred.Text);
  D := Geral.DMV(dmkEdDeb.Text);
  //if C > D then B := C - D else B := D - C;
  B := C-D;
  if FICMS = 0 then V := B * P / 100
  else if B <> 0 then P := V / B * 100 else P := 0;
  dmkEdICMS_V.Text := Geral.FFT(V, 2, siPositivo);
  dmkEdICMS_P.Text := Geral.FFT(P, 2, siPositivo);
end;

procedure TFmLctEdit.EdDuplicataKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key in ([VK_DOWN, VK_UP]) then
  begin
    if (key=VK_DOWN) then
      dmkEdDuplicata.Text := dmkPF.DuplicataIncrementa(dmkEdDuplicata.Text, -1)
    else
      dmkEdDuplicata.Text := dmkPF.DuplicataIncrementa(dmkEdDuplicata.Text,  1);
  end;
end;

function TFmLctEdit.ReopenLct: Boolean;
var
  Ini, Fim: String;
begin
  Ini := FormatDateTime(VAR_FORMATDATE, 1);
  Fim := FormatDateTime(VAR_FORMATDATE, Date);
  QrLct.Close;
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
  QrLct.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
  QrLct.SQL.Add('la.*, ct.Codigo CONTA,');
  QrLct.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
  QrLct.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
  QrLct.SQL.Add('CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial');
  QrLct.SQL.Add('ELSE em.Nome END NOMEEMPRESA,');
  QrLct.SQL.Add('CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  QrLct.SQL.Add('ELSE cl.Nome END NOMECLIENTE,');
  QrLct.SQL.Add('CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial');
  QrLct.SQL.Add('ELSE fo.Nome END NOMEFORNECEDOR,');
  QrLct.SQL.Add('ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA');
  {#### colocar B e D?}
  QrLct.SQL.Add('FROM ' + FTabLctA + ' la');
  QrLct.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  QrLct.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  QrLct.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  QrLct.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  QrLct.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  QrLct.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrLct.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  QrLct.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  //
  QrLct.SQL.Add('WHERE la.Data BETWEEN "' + Ini + '" AND "' + Fim + '"');
  //
  case TabControl1.TabIndex of
    0: QrLct.SQL.Add('AND la.Carteira='  + Geral.FF0(Geral.IMV(dmkEdCarteira.Text)));
    1: QrLct.SQL.Add('AND ca.ForneceI='  + Geral.FF0(Geral.IMV(dmkEdCBCliInt.Text)));
    2: QrLct.SQL.Add('AND la.Cliente='   + Geral.FF0(Geral.IMV(dmkEdCBCliente.Text)));
    3: QrLct.SQL.Add('AND la.Fornecedor='+ Geral.FF0(Geral.IMV(dmkEdCBFornece.Text)));
    4: QrLct.SQL.Add('AND la.Genero='    + Geral.FF0(Geral.IMV(dmkEdCBGenero.Text)));
  end;
  QrLct.SQL.Add('ORDER BY la.Data DESC, la.Controle DESC');
  UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
  Result := True;
end;

procedure TFmLctEdit.TabSheet2Resize(Sender: TObject);
begin
  (*DBGrid1.Align := alClient;
  DBGrid1.Align := alNone;
  DBGrid1.Top := 24;
  DBGrid1.Height := DBGrid1.Height - 24;*)
end;

procedure TFmLctEdit.TbParcpagtosCreditoSetText(Sender: TField;
  const Text: string);
begin
  Sender.Value := dmkPF.Table_FormataCampo(Sender, Text);
end;

procedure TFmLctEdit.TbParcpagtosDataSetText(Sender: TField;
  const Text: string);
begin
  Sender.Value := dmkPF.Table_FormataCampo(Sender, Text);
end;

procedure TFmLctEdit.TbParcpagtosDebitoSetText(Sender: TField;
  const Text: string);
begin
  Sender.Value := dmkPF.Table_FormataCampo(Sender, Text);
end;

procedure TFmLctEdit.TbParcpagtosICMS_VSetText(Sender: TField;
  const Text: string);
begin
  Sender.Value := dmkPF.Table_FormataCampo(Sender, Text);
end;

procedure TFmLctEdit.PageControl1Change(Sender: TObject);
begin
  ReopenLct;
  if PageControl1.ActivePageIndex in ([2,3]) then
    WindowState := wsMaximized
  else
    WindowState := wsNormal;
end;

procedure TFmLctEdit.TabControl1Change(Sender: TObject);
begin
  ReopenLct;
end;

procedure TFmLctEdit.TPDataChange(Sender: TObject);
begin
  ConfiguraVencimento;
end;

procedure TFmLctEdit.ConfiguraVencimento;
begin
  if (ImgTipo.SQLType = stIns) and (VAVcto.ValueVariant = False) then
    dmkEdTPVencto.Date := dmkEdTPdata.Date;
end;

procedure TFmLctEdit.QrCarteirasAfterScroll(DataSet: TDataSet);
begin
  ConfiguraComponentesCarteira;
end;

procedure TFmLctEdit.ReopenContas(Entidade, Codigo, Genero: Integer);
var
  CtaCfgCab, PlanoPadrao: Integer;
  SQL1, SQL2: String;
begin
  QrContas1.Close;
  //
  if QrCliInt.State <> dsInactive then
  begin
    QrCliInt.Locate('Codigo', Entidade, []);
    //
    CtaCfgCab   := QrCliIntCtaCfgCab.Value;
    PlanoPadrao := DModFin.QrFinOpcoes.FieldByName('PlanoPadrao').asInteger;
    //
    if CtaCfgCab <> 0 then
    begin
      SQL1 :=
      'AND co.Codigo IN ( ' + sLineBreak +
      '     SELECT Genero ' + sLineBreak +
      '     FROM ctacfgits ' + sLineBreak +
      '     WHERE Codigo=' + Geral.FF0(CtaCfgCab) + ') ';
    end else
      SQL1 := '';
    //
    if PlanoPadrao <> 0 then
      SQL2 := 'AND pl.Codigo=' + Geral.FF0(PlanoPadrao)
    else
      SQL2 := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrContas1, Dmod.MyDB, [
    'SELECT CONCAT( ',
    '  pl.Codigo, ".", ',
    '  cj.Codigo, ".", ',
    '  gr.Codigo, ".", ',
    '  sg.Codigo, ".", ',
    '  co.Codigo) Niveis, ',
    'CONCAT( ',
    '  pl.OrdemLista, ".", ',
    '  cj.OrdemLista, ".", ',
    '  gr.OrdemLista, ".", ',
    '  sg.OrdemLista, ".", ',
    '  co.OrdemLista) Ordens, ',
    'co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO, ',
    'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO, ',
    'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA ',
    'FROM contas co ',
    'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo ',
    'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo ',
    'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto ',
    'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano ',
    'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa ',
    'WHERE co.Terceiro=0 ',
    'AND co.Codigo>0 ',
    SQL1,
    SQL2,
    'ORDER BY co.Nome ',
    '']);
    if Genero <> 0 then
    begin
      dmkEdCBGenero.ValueVariant := Genero;
      dmkCBGenero.KeyValue       := Genero;
    end;
    //
    if Codigo <> 0 then
      QrContas1.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmLctEdit.ReopenClientes(Tipo, Entidade: Integer);
var
  SQL: String;
begin
  if Tipo = 0 then
    SQL := 'IF (Tipo=0, RazaoSocial, Nome) NOMEENTIDADE '
  else
    SQL := 'IF (Tipo=0, Fantasia, Apelido) NOMEENTIDADE ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrClientes, Dmod.MyDB, [
    'SELECT Codigo, Account, ',
    'IF(Tipo=0, CNPJ, CPF) Docum, ',
    SQL,
    'FROM entidades',
    'WHERE Cliente1="V"',
    'OR Cliente2="V"',
    'OR Cliente3="V"',
    'OR Cliente4="V"',
    'OR Terceiro="V"',
    'ORDER BY NOMEENTIDADE',
    '']);
  if (Entidade <> 0) and (QrClientes.Locate('Codigo', Entidade, [])) then
  begin
    dmkEdCBCliente.ValueVariant := QrClientesCodigo.Value;
    dmkCBCliente.KeyValue       := QrClientesCodigo.Value;
  end;
end;

procedure TFmLctEdit.ReopenFornecedores(Tipo, Entidade: Integer);
var
  SQL: String;
begin
  if Tipo = 0 then
    SQL := 'IF (Tipo=0, RazaoSocial, Nome) NOMEENTIDADE '
  else
    SQL := 'IF (Tipo=0, Fantasia, Apelido) NOMEENTIDADE ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFornecedores, Dmod.MyDB, [
    'SELECT Codigo, ',
    'IF(Tipo=0, CNPJ, CPF) Docum, ',
    SQL,
    'FROM entidades',
    'WHERE Fornece1="V"',
    'OR Fornece2="V"',
    'OR Fornece3="V"',
    'OR Fornece4="V"',
    'OR Fornece5="V"',
    'OR Fornece6="V"',
    'OR Terceiro="V"',
    'ORDER BY NOMEENTIDADE',
    '']);
  if Entidade <> 0 then
    QrFornecedores.Locate('Codigo', Entidade, []);
end;

procedure TFmLctEdit.ReopenIndiPag();
begin
  QrIndiPag.Close;
  UnDmkDAC_PF.AbreQuery(QrIndiPag, Dmod.MyDB);
end;

procedure TFmLctEdit.QrClientesAfterScroll(DataSet: TDataSet);
begin
  if not dmkEdCBClienteCNPJ.Focused then
  begin
  // 2021-01-09 ini
    if dmkEdCBCliente.ValueVariant <> 0 then
  // 2021-01-09 fim
      dmkEdCBClienteCNPJ.ValueVariant := QrClientesDocum.Value;
  end;
end;

procedure TFmLctEdit.QrClientesBeforeClose(DataSet: TDataSet);
begin
  if not dmkEdCBClienteCNPJ.Focused then
    dmkEdCBClienteCNPJ.ValueVariant := '';
end;

procedure TFmLctEdit.QrCliIntAfterScroll(DataSet: TDataSet);
begin
  if FIDFinalidade = 0 then
    FIDFinalidade := Geral.IMV(Copy(LaFinalidade.Caption, 1, 3));
  case FIDFinalidade of
    1: // Pr�prios
    begin
      //Nada
    end;
    2: // Condom�nios
    begin
      QrForneceI.Close;
      QrForneceI.SQL.Clear;
{
      QrForneceI.SQL.Add('SELECT Codigo,');
      QrForneceI.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
      QrForneceI.SQL.Add('ELSE Nome END NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM entidades');
      QrForneceI.SQL.Add('WHERE Cliente2="V"');
      QrForneceI.SQL.Add('AND Codigo in (');
      QrForneceI.SQL.Add('  SELECT ci2.Propriet');
      QrForneceI.SQL.Add('  FROM condimov ci2');
      QrForneceI.SQL.Add('  LEFT JOIN cond co2 ON co2.Codigo=ci2.Codigo');
      QrForneceI.SQL.Add('  WHERE co2.Cliente IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add(') ORDER BY NOMEENTIDADE');
}
      QrForneceI.SQL.Add('SELECT DISTINCT ent.Codigo,');
      QrForneceI.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
      QrForneceI.SQL.Add('ELSE ent.Nome END NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM condimov ci2');
      QrForneceI.SQL.Add('LEFT JOIN cond con ON con.Codigo = ci2.Codigo');
      QrForneceI.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ci2.Propriet');
      //QrForneceI.SQL.Add('WHERE ci2.Codigo IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add('WHERE con.Cliente IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add('ORDER BY NOMEENTIDADE');
      QrForneceI.SQL.Add('');
      UnDmkDAC_PF.AbreQuery(QrForneceI, Dmod.MyDB);
    end;
    3: // Cursos c�clicos
    begin
      //Nada ?
    end;
  end;
end;

procedure TFmLctEdit.SBClienteClick(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  //
  DModG.CadastroDeEntidade(dmkEdCBCliente.ValueVariant, fmcadEntidade2, fmcadEntidade2, True);
  //
  QrClientes.Close;
  UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
  //
  if dmkEdCBCliente.Enabled then
  begin
    if VAR_ENTIDADE <> 0 then
    begin
      dmkEdCBCliente.Text   := Geral.FF0(VAR_ENTIDADE);
      dmkCBCliente.KeyValue := VAR_ENTIDADE;
      dmkEdCBCliente.SetFocus;
    end;
  end;
end;

procedure TFmLctEdit.SBForneceClick(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  //
  DModG.CadastroDeEntidade(dmkEdCBFornece.ValueVariant, fmcadEntidade2, fmcadEntidade2, True);
  //
  QrFornecedores.Close;
  UnDmkDAC_PF.AbreQuery(QrFornecedores, Dmod.MyDB);
  //
  if dmkEdCBFornece.Enabled then
  begin
    if VAR_ENTIDADE <> 0 then
    begin
      dmkEdCBFornece.Text   := Geral.FF0(VAR_ENTIDADE);
      dmkCBFornece.KeyValue := VAR_ENTIDADE;
      dmkEdCBFornece.SetFocus;
    end;
  end;
end;

procedure TFmLctEdit.BitBtn1Click(Sender: TObject);
{
var
  Valor, Benef, Cidade: String;
}
begin
{$IfNDef NAO_USA_IMP_CHEQUE}
    MyPrinters.EmiteCheque(0, 0, QrCarteirasBanco1.Value,
    dmkEdDeb.ValueVariant, dmkCBFornece.Text,
    dmkEdCred.ValueVariant, dmkCBCliente.Text,
    ''(*Serie*), 0(*Cheque*), 0(*Data*), 0(*Carteira*),
    ''(*CampoNomeFornecedor*), nil (*Grade*), FTabLctA, -1, 0, 0);
{$EndIf}
 {
  if dmkEdDeb.Enabled then
  begin
    Valor := dmkEdDeb.Text;
    Benef := dmkCBFornece.Text;
  end else begin
    Valor := dmkEdCred.Text;
    Benef := dmkCBCliente.Text;
  end;
  //
  Benef := Geral.SemAcento(Geral.Maiusculas(Benef, False));
  Cidade := Geral.SemAcento(Geral.Maiusculas(DmodG.QrDonoCIDADE.Value, False));
  //
  if DBCheck.CriaFm(TFmEmiteCheque, FmEmiteCheque, afmoNegarComAviso) then
  begin
    FmEmiteCheque.TPData.Date   := dmkEdTPdata.Date;
    FmEmiteCheque.EdValor.Text  := Valor;
    FmEmiteCheque.EdBenef.Text  := Benef;
    FmEmiteCheque.EdCidade.Text := Cidade;
    //
    FmEmiteCheque.ShowModal;
    (*if FmEmiteCheque.ST_CMC7.Caption <> '' then
      EdBanda1.Text := ???.SoNumeroECMC7(FmEmiteCheque.ST_CMC7.Caption);*)
    FmEmiteCheque.Destroy;
  end;
  }
end;

procedure TFmLctEdit.EventosCadReopen(Entidade: Integer);
begin
  QrEventosCad.Close;
  QrEventosCad.Params[0].AsInteger := Entidade;
  UnDmkDAC_PF.AbreQuery(QrEventosCad, Dmod.MyDB);
  //
  LaEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  EdEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  CBEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  SbEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  if not QrEventosCad.Locate('CodUsu', QrEventosCadCodUsu.Value, []) then
  begin
    EdEventosCad.ValueVariant := 0;
    CBEventosCad.KeyValue     := 0;
  end;
end;

procedure TFmLctEdit.CkParcelamentoClick(Sender: TObject);
begin
  if CkParcelamento.Checked then
  begin
    GBParcelamento.Visible := True;
    DBGParcelas.Visible := True;
    CalculaParcelas;
  end else begin
    GBParcelamento.Visible := False;
    DBGParcelas.Visible := True;
  end;
end;

procedure TFmLctEdit.RGPeriodoClick(Sender: TObject);
begin
  if RGPeriodo.ItemIndex = 1 then
  begin
    EdDias.Enabled := True;
    EdDias.SetFocus;
  end else begin
    EdDias.Enabled := False;
  end;
  CalculaParcelas;
end;

procedure TFmLctEdit.EdDiasExit(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.RGIncremCHClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.CkArredondarClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.RGArredondarClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.DBGParcelasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then if TbParcPagtos.State in ([dsEdit, dsInsert])
  then TbParcpagtos.Post;
end;

function TFmLctEdit.GetTipoValor: TTipoValor;
var
  D,C: Double;
begin
  if dmkEdDeb.Enabled  then D := Geral.DMV(dmkEdDeb.Text)  else D := 0;
  if dmkEdCred.Enabled then C := Geral.DMV(dmkEdCred.Text) else C := 0;
  FValorAParcelar := D + C;
  if (D>0) and (C>0) then
  begin
    Geral.MB_Aviso('Valor n�o pode ser cr�dito e d�bito ao mesmo tempo!');
    Result := tvNil;
    Exit;
  end else if D>0 then Result := tvDeb
  else if C>0 then Result := tvCred
  else Result := tvNil;
end;

procedure TFmLctEdit.CkIncremDUClick(Sender: TObject);
begin
  RGIncremDupl.Visible := CkIncremDU.Checked;
  CalculaParcelas;
end;

procedure TFmLctEdit.EdDuplSepExit(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.EdGenCtbRedefinido(Sender: TObject);
begin
  if EdGenCtb.ValueVariant = 0 then
    DBNiveis2.DataField := ''
  else
  begin
    if DModG.QrCtrlGeralConcatNivOrdCta.Value = 1 then
      DBNiveis2.DataField := 'Ordens'
    else
      DBNiveis2.DataField := 'Niveis';
  end;
end;

procedure TFmLctEdit.RGDuplSeqClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.BitBtn2Click(Sender: TObject);
var
  ValOrig, ValMult, ValJuro, ValNovo, PerMult, ValDife: Double;
begin
  ValOrig := Geral.DMV(dmkEdCred.Text) - Geral.DMV(dmkEdDeb.Text);
  if ValOrig < 0 then ValOrig := ValOrig * -1;
  ValNovo := Geral.DMV(dmkEdValNovo.Text);
  PerMult := Geral.DMV(dmkEdPerMult.Text);
  //
  ValDife := ValOrig - ValNovo;
  ValMult := Int(ValOrig * PerMult ) / 100;
  if ValMult > ValDife then ValMult := ValDife;
  ValJuro := ValDife - ValMult;
  //
  dmkEdMultaVal.Text := Geral.FFT(ValMult, 2, siNegativo);
  dmkEdMoraVal.Text  := Geral.FFT(ValJuro, 2, siNegativo);
end;

procedure TFmLctEdit.CkIncremTxtClick(Sender: TObject);
begin
  GBIncremTxt.Visible := CkIncremTxt.Checked;
  CalculaParcelas;
end;

procedure TFmLctEdit.QrDeptosAfterScroll(DataSet: TDataSet);
begin
  case FIDFinalidade of
    1: // Pr�prio
    begin
      if CO_DMKID_APP = 5 then //Synker
      begin
        QrForneceI.Close;
        QrForneceI.SQL.Clear;
        QrForneceI.SQL.Add('SELECT ent.Codigo,');
        QrForneceI.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE');
        QrForneceI.SQL.Add('FROM intentosoc soc');
        QrForneceI.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=soc.Socio');
        QrForneceI.SQL.Add('WHERE soc.Codigo=:P0');
        QrForneceI.SQL.Add('ORDER BY NOMEENTIDADE');
        QrForneceI.SQL.Add('');
        QrForneceI.Params[0].AsInteger := QrDeptosCODI_1.Value;
        UnDmkDAC_PF.AbreQuery(QrForneceI, Dmod.MyDB);
        if not QrForneceI.Locate('Codigo', Geral.IMV(dmkEdCBForneceI.Text), []) then
        begin
          dmkEdCBForneceI.Text := '';
          dmkCBForneceI.KeyValue := Null;
        end;
      end;
    end;
    2: // Condom�nios
    begin
      //Nada
    end;
    3: // Cursos c�clicos
    begin
      //Nada
    end;
  end;
end;

procedure TFmLctEdit.QrFornecedoresAfterScroll(DataSet: TDataSet);
begin
  if not dmkEdCBForneceCNPJ.Focused then
  begin
  // 2021-01-09 ini
    if dmkEdCBFornece.ValueVariant <> 0 then
  // 2021-01-09 fim
      dmkEdCBForneceCNPJ.ValueVariant := QrFornecedoresDocum.Value;
  end;
end;

procedure TFmLctEdit.QrFornecedoresBeforeClose(DataSet: TDataSet);
begin
  dmkEdCBForneceCNPJ.ValueVariant := '';
end;

procedure TFmLctEdit.dmkEdCBGeneroChange(Sender: TObject);
begin
  VerificaEdits;
  if (ImgTipo.SQLType = stIns) and (CkDuplicando.Checked = False) then
    if dmkEdCBGenero.ValueVariant <> 0 then
      dmkEdDescricao.Text := QrContas1Nome.Value;
end;

procedure TFmLctEdit.dmkEdCBGeneroRedefinido(Sender: TObject);
begin
  if dmkEdCBGenero.ValueVariant = 0 then
    DBNiveis1.DataField := ''
  else
  begin
    if DModG.QrCtrlGeralConcatNivOrdCta.Value = 1 then
      DBNiveis1.DataField := 'Ordens'
    else
      DBNiveis1.DataField := 'Niveis';
  end;
end;

procedure TFmLctEdit.dmkEdMesExit(Sender: TObject);
var
  Nome: String;
begin
  Nome := QrContas1Nome2.Value;
  //
  if (Nome = '') or (UpperCase(Nome) = 'NULO') then
    Nome := QrContas1Nome.Value;
  //
  if (ImgTipo.SQLType = stIns) and (CkDuplicando.Checked = False)  then
    dmkEdDescricao.Text := Nome + ' ' + dmkEdMes.Text;
end;

procedure TFmLctEdit.dmkEdMesKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Genero, Cliente, Fornece: Integer;
  GenTxt, EntTxt, Qual: String;
begin
  if Key = VK_F4 then
  begin
    if ImgTipo.SQLType = stIns then
    begin
      Genero  := dmkEdCBGenero.ValueVariant;
      if dmkEdCBCliente.Enabled then
        Cliente := dmkEdCBCliente.ValueVariant
      else Cliente := 0;
      if dmkEdCBFornece.Enabled then
        Fornece := dmkEdCBFornece.ValueVariant
      else Fornece := 0;  
      //
      GenTxt := FormatFloat('0', Genero);
      EntTxt := 'qualquer cliente ou fornecedor';
      Qual   := 'De ';
      if MyObjects.FIC(Genero = 0, nil (*dmkEdCBGenero*),
        'Informe a conta do plano de contas!') then Exit;
      if MyObjects.FIC((Cliente = 0) and (Fornece = 0), nil (*dmkEdCBCliente*),
        'Informe um cliente ou um fornecedor!') then Exit;
       //
       QrLocMes.Close;
       QrLocMes.SQL.Clear;
       QrLocMes.SQL.Add('SELECT MAX(Mez) MEZ');
       QrLocMes.SQL.Add('FROM ' + FTabLctA);
       QrLocMes.SQL.Add('WHERE Mez > 0');
       QrLocMes.SQL.Add('AND Genero=' + GenTxt);
       if Cliente <> 0 then
       begin
         Qual := 'Cliente: ';
         EntTxt := FormatFloat('0', Cliente);
         QrLocMes.SQL.Add('AND Cliente=' + EntTxt);
       end else if Fornece <> 0 then
       begin
         Qual := 'Fornecedor: ';
         EntTxt := FormatFloat('0', Fornece);
         QrLocMes.SQL.Add('AND Fornecedor=' + EntTxt);
       end;
       UnDmkDAC_PF.AbreQuery(QrLocMes, Dmod.MyDB);
       if QrLocMes.RecordCount > 0 then
       begin
         dmkEdMes.Texto := dmkPF.MezToFDT(QrLocMesMEZ.Value, 32, 14);
       end else Geral.MB_Aviso('N�o foi localizado nenhum m�s para:' + sLineBreak+
       'Conta do plano de contas: ' + sLineBreak + Qual + EntTxt);
    end else Geral.MB_Aviso(
    'A sugest�o do pr�ximo m�s de compet�ncia � v�lida' + sLineBreak +
    'apenas quando o status da janela for de inclus�o!');
  end;
end;

procedure TFmLctEdit.dmkEdDebExit(Sender: TObject);
begin
  CalculaICMS;
  if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdit.dmkCBGeneroClick(Sender: TObject);
begin
  VerificaEdits;
end;

procedure TFmLctEdit.dmkEdCredExit(Sender: TObject);
begin
  CalculaICMS;
  if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdit.dmkEdDocChange(Sender: TObject);
begin
  if Geral.DMV(dmkEdDoc.Text) = 0 then
  begin
    RGIncremCH.Enabled := False;
    //GBCheque.Visible := False;
  end else begin
    RGIncremCH.Enabled := True;
    //GBCheque.Visible := True;
  end;
end;

procedure TFmLctEdit.dmkEdDocExit(Sender: TObject);
begin
  if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdit.dmkEdDuplicataExit(Sender: TObject);
begin
  if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdit.dmkEdTPDataExit(Sender: TObject);
begin
  ConfiguraVencimento();
end;

procedure TFmLctEdit.dmkEdTPVenctoChange(Sender: TObject);
begin
  if CkParcelamento.Checked then CalculaParcelas;
  if ImgTipo.SQLType = stIns then
    TPVctoOriginal.Date := dmkEdTPVencto.Date;
end;

procedure TFmLctEdit.dmkEdTPVenctoClick(Sender: TObject);
begin
  if CkParcelamento.Checked then CalculaParcelas;
  if ImgTipo.SQLType = stIns then
    TPVctoOriginal.Date := dmkEdTPVencto.Date;
end;

procedure TFmLctEdit.dmkEdTPVenctoExit(Sender: TObject);
begin
  if VAVcto.ValueVariant = False then
    VAVcto.ValueVariant := True;
end;

procedure TFmLctEdit.dmkEdTPVenctoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F6 then
    dmkEdTPVencto.Date := dmkEdTPdata.Date;
end;

procedure TFmLctEdit.ConfiguracoesIniciais();
begin
  (*
  QrCliInt.Close;
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  *)
  // 2012-05-15
  // Precisa do cliInt aberto para abrir as contas restritivas corretamente
  ReopenContas(dmkEdCBCliInt.ValueVariant, 0, dmkEdCBGenero.ValueVariant);
  // FIM 2012-05-15
  //
  MeConfig.Lines.Clear;
  QrDeptos.Close;
  QrDeptos.SQL.Clear;
  FIDFinalidade := Geral.IMV(Copy(LaFinalidade.Caption, 1, 3));
  case FIDFinalidade of
    1: // Pr�prios
    begin
      LaCliInt.Caption := 'Empresa (Filial):';
      if CO_DMKID_APP = 5 then //Synker
      begin
        //
        QrDeptos.SQL.Add('SELECT Codigo CODI_1, Nome NOME_1, FLOOR(0) CODI_2');
        QrDeptos.SQL.Add('FROM intentocad');
        QrDeptos.SQL.Add('WHERE Ativo=1');
        QrDeptos.SQL.Add('ORDER BY NOME_1');
        //
        {
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Add('SELECT Codigo, ');
        QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
        QrCliInt.SQL.Add('FROM entidades');
        QrCliInt.SQL.Add('WHERE Codigo=' + Geral.FF0(Dmod.QrControle.FieldByName('Dono').AsInteger));
        UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
        }
        //////
        //PnDepto.Visible   := True;
        //PnForneceI.Visible := True;
      end else
      if CO_DMKID_APP = 29 then //Infrajob
      begin
        //
        LaDepto.Caption := 'Obra:';
        PnDepto.Visible   := True;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrDeptos, Dmod.MyDB, [
        'SELECT obc.Codigo CODI_1, obc.Nome NOME_1, ',
        'FLOOR(obc.Codigo) CODI_2 ',
        'FROM obrascab obc ',
        'WHERE obc.Ativo=1 ',
        'AND (obc.DtaJobFim<2 ',
        '  OR obc.DtaJobFim > SYSDATE()) ',
        //'AND DtaJobFim < "1900-01-01"',
        'ORDER BY NOME_1 ',
        '']);
      end;
    end;
    2: // Condom�nios
    begin
      LaCliInt.Caption := 'Condom�nio (Cliente interno):';
      //
      {
      QrDeptos.SQL.Add('SELECT Conta CODI_1, Unidade NOME_1, FLOOR(Propriet) CODI_2');
      QrDeptos.SQL.Add('FROM condimov');
      QrDeptos.SQL.Add('WHERE Codigo=:P0');
      QrDeptos.SQL.Add('ORDER BY NOME_1');
      }
      QrDeptos.SQL.Add('SELECT imv.Conta CODI_1, imv.Unidade NOME_1,');
      QrDeptos.SQL.Add('FLOOR(imv.Propriet) CODI_2');
      QrDeptos.SQL.Add('FROM condimov imv');
      QrDeptos.SQL.Add('LEFT JOIN cond con ON con.Codigo = imv.Codigo');
      QrDeptos.SQL.Add('WHERE con.Cliente=:P0');
      QrDeptos.SQL.Add('ORDER BY NOME_1');
      QrDeptos.Params[0].AsInteger := dmkEdOneCliInt.ValueVariant;//FmCondGer.QrCondCodigo.Value;
      //
      if Trim(VAR_CLIENTEI) <> '' then
      begin
        {
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Add('SELECT Codigo, ');
        QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
        QrCliInt.SQL.Add('FROM entidades');
        QrCliInt.SQL.Add('WHERE Cliente'+VAR_CLIENTEI+'="V"');
        QrCliInt.SQL.Add('ORDER BY NOMEENTIDADE');
        UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
        }
        //////
        LaCliInt.Visible := True;
        dmkEdCBCliInt.Visible := True;
        dmkCBCliInt.Visible := True;
      end;
      PnDepto.Visible   := True;
      PnForneceI.Visible := True;
    end;
    3: // Cursos c�clicos
    begin
      {
      QrCliInt.SQL.Clear;
      QrCliInt.SQL.Add('SELECT Codigo, ');
      QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
      QrCliInt.SQL.Add('FROM entidades');
      QrCliInt.SQL.Add('WHERE CliInt>0 OR Codigo=-1');
      QrCliInt.SQL.Add('ORDER BY NOMEENTIDADE');
      UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
      }
      LaCliInt.Caption        := 'Empresa (Filial):';
      LaDepto.Caption         := '';
      LaAccount.Caption       := 'Respons�vel interno pelo movimento:';
      dmkEdCBForneceI.Visible := False;
      dmkCBForneceI.Visible   := False;
      //
      QrAccounts.SQL.Clear;
      QrAccounts.SQL.Add('SELECT Codigo, ');
      QrAccounts.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
      QrAccounts.SQL.Add('FROM entidades');
      QrAccounts.SQL.Add('WHERE Fornece2="V" OR Fornece3="V"');
      UnDmkDAC_PF.AbreQuery(QrAccounts, Dmod.MyDB);
      //
      PnAccount.Visible := True;
      if ImgTipo.SQLType = stIns then
      begin
        dmkEdCBAccount.ValueVariant := dmkEdOneAccount.ValueVariant;
        dmkCBAccount.KeyValue       := dmkEdOneAccount.ValueVariant;
      end;
    end;
    else Geral.MB_Erro('Nenhuma finalidade foi definida para a ' +
    'janela de inclus�o / altera��o de lan�amentos!');
  end;
  if QrDeptos.SQL.Text <> '' then
    UnDmkDAC_PF.AbreQuery(QrDeptos, Dmod.MyDB);
end;

procedure TFmLctEdit.dmkCBForneceKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  ConfiguraNomeRazao(tetFornece, dmkCBFornece.KeyValue, Key);
end;

procedure TFmLctEdit.dmkEdCBDeptoChange(Sender: TObject);
begin
  if not FCriandoForm then
  begin
    if FIDFinalidade = 2 then
    begin
      if dmkEdCBDepto.ValueVariant = 0 then
      begin
        dmkEdCBForneceI.Text := '0';
        dmkCBForneceI.KeyValue := Null;
      end else begin
        dmkEdCBForneceI.Text := FormatFloat('0', QrDeptosCODI_2.Value);
        dmkCBForneceI.KeyValue := Int(QrDeptosCODI_2.Value / 1);
      end;
    end;
  end;
end;

procedure TFmLctEdit.dmkEdCBForneceChange(Sender: TObject);
begin
  if not dmkEdCBForneceCNPJ.Focused then
  begin
    if dmkEdCBFornece.ValueVariant <> 0 then
      dmkEdCBForneceCNPJ.ValueVariant := QrFornecedoresDocum.Value
    else
      dmkEdCBForneceCNPJ.ValueVariant := '';
  end;
end;

procedure TFmLctEdit.dmkEdCBForneceCNPJExit(Sender: TObject);
begin
  ConfiguraDadosDocum(tetFornece, dmkEdCBForneceCNPJ, dmkEdCBFornece,
    dmkCBFornece, QrFornecedores);
end;

procedure TFmLctEdit.dmkEdCBForneceKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ConfiguraNomeRazao(tetFornece, dmkEdCBFornece.ValueVariant, Key);
end;

procedure TFmLctEdit.dmkCBDeptoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_DELETE then
  begin
    dmkEdCBDepto.Text := '';
    dmkCBDepto.KeyValue := Null;
  end;
end;

procedure TFmLctEdit.dmkEdDescricaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if dmkEdMes.Enabled then Mes := dmkEdMes.Text else Mes := '';
    if key=VK_F4 then dmkEdDescricao.Text := QrContas1Nome.Value+' '+Mes;
    if key=VK_F5 then dmkEdDescricao.Text := QrContas1Nome2.Value+' '+Mes;
    if key=VK_F6 then dmkEdDescricao.Text := QrContas1Nome3.Value+' '+Mes;
    if dmkEdDescricao.Enabled then dmkEdDescricao.SetFocus;
    dmkEdDescricao.SelStart := Length(dmkEdDescricao.Text);
    dmkEdDescricao.SelLength := 0;
  end else if key = VK_F8 then
  begin
    if dmkCBFornece.Enabled then
      dmkEdDescricao.Text := dmkEdDescricao.Text + ' - ' + dmkCBFornece.Text
    else if dmkCBCliente.Enabled then
      dmkEdDescricao.Text := dmkEdDescricao.Text + ' - ' + dmkCBCliente.Text;
  end;
end;

procedure TFmLctEdit.dmkCBAccountKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLctEdit.ConfiguraNomeRazao(EntTipo: TEntiTipo; Entidade: Integer;
  Key: Word);
begin
  if Key = VK_F5 then
  begin
    case EntTipo of
      tetCliente:
        ReopenClientes(0, Entidade);
      tetFornece:
        ReopenFornecedores(0, Entidade);
    end;
  end
  else if Key = VK_F6 then
  begin
    case EntTipo of
      tetCliente:
        ReopenClientes(1, Entidade);
      tetFornece:
        ReopenFornecedores(1, Entidade);
    end;
  end;
end;

procedure TFmLctEdit.dmkCBClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ConfiguraNomeRazao(tetCliente, dmkCBCliente.KeyValue, Key);
end;

procedure TFmLctEdit.dmkEdICMS_PExit(Sender: TObject);
begin
  CalculaICMS;
end;

procedure TFmLctEdit.dmkEdICMS_PKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if dmkPF.EhNumeroOuSinalOuSeparador(Key) then FICMS := 0;
end;

procedure TFmLctEdit.dmkEdICMS_VExit(Sender: TObject);
begin
  CalculaICMS;
end;

procedure TFmLctEdit.dmkEdICMS_VKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if dmkPF.EhNumeroOuSinalOuSeparador(Key) then FICMS := 1;
end;

procedure TFmLctEdit.dmkEdCarteiraChange(Sender: TObject);
var
  UsaTalao: Boolean;
begin
  UsaTalao := QrCarteirasUsaTalao.Value = 0;
  dmkEdSerieCH.Enabled := UsaTalao;
  dmkEdDoc.Enabled     := UsaTalao;
  LaDoc.Enabled        := UsaTalao;
end;

procedure TFmLctEdit.dmkEdCarteiraRedefinido(Sender: TObject);
begin
  if (dmkEdCarteira.Focused or FCriandoForm) and dmkEdCarteira.Enabled and
  (ImgTipo.SQLType = stIns) then
  begin
    if QrCarteirasFornecePadrao.Value > 0 then
    begin
      dmkEdCBFornece.ValueVariant := QrCarteirasFornecePadrao.Value;
      dmkCBFornece.KeyValue       := QrCarteirasFornecePadrao.Value;
    end;
    if QrCarteirasClientePadrao.Value > 0 then
    begin
      dmkEdCBCliente.ValueVariant := QrCarteirasClientePadrao.Value;
      dmkCBCliente.KeyValue       := QrCarteirasClientePadrao.Value;
    end;
  end;
end;

procedure TFmLctEdit.dmkEdCBAccountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLctEdit.dmkEdCBClienteChange(Sender: TObject);
begin
  if not dmkEdCBClienteCNPJ.Focused then
  begin
    if dmkEdCBCliente.ValueVariant <> 0 then
      dmkEdCBClienteCNPJ.ValueVariant := QrClientesDocum.Value
    else
      dmkEdCBClienteCNPJ.ValueVariant := '';
  end;
end;

procedure TFmLctEdit.ConfiguraDadosDocum(EntiTipo: TEntiTipo; EdCNPJ: TdmkEdit;
  EdEntidade: TdmkEditCB; CBEntidade: TdmkDBLookupComboBox; Query: TmySQLQuery);
var
  Entidade: Integer;
  Docum: String;
begin
  Docum := Geral.SoNumero_TT(EdCNPJ.ValueVariant);
  //
  if Docum <> '' then
  begin
    if not Query.Locate('Docum', Docum, [loPartialKey, loCaseInsensitive]) then
    begin
      EdEntidade.ValueVariant := 0;
      CBEntidade.KeyValue     := Null;
      //
      if Geral.CNPJ_Valida(TWinControl(EdCNPJ), Docum) then
      begin
        CadastraEntidade(EntiTipo, Docum);
        //
        if VAR_CADASTRO <> 0 then
        begin
          Query.Close;
          UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
          //
          EdEntidade.Text     := Geral.FF0(VAR_ENTIDADE);
          CBEntidade.KeyValue := VAR_ENTIDADE;
          EdEntidade.SetFocus;
        end;
      end;
    end else
    begin
      EdEntidade.ValueVariant := Query.FieldByName('Codigo').AsInteger;
      CBEntidade.KeyValue     := Query.FieldByName('Codigo').AsInteger;
    end;
  end else
  begin
    EdEntidade.ValueVariant := 0;
    CBEntidade.KeyValue     := Null;
  end;
end;

procedure TFmLctEdit.dmkEdCBClienteCNPJExit(Sender: TObject);
begin
  ConfiguraDadosDocum(tetCliente, dmkEdCBClienteCNPJ, dmkEdCBCliente,
    dmkCBCliente, QrClientes);
end;

procedure TFmLctEdit.dmkEdCBClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ConfiguraNomeRazao(tetCliente, dmkEdCBCliente.ValueVariant, Key);
end;

procedure TFmLctEdit.dmkEdCBCliIntChange(Sender: TObject);
var
  Entidade, Genero: Integer;
begin
  Entidade := dmkEdCBCliInt.ValueVariant;
  Genero   := dmkEdCBGenero.ValueVariant;
  //
  EventosCadReopen(Entidade);
  ReopenContas(Entidade, 0, Genero);
end;

procedure TFmLctEdit.dmkEdParcelasExit(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.dmkEdQtd2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Preco_TXT: String;
  Preco, Qtde2, Valor: Double;
begin
  if (Key = VK_F4) then
  begin
    if InputQuery('Pre�o', 'Informe o pre�o', Preco_TXT) then
    begin
      Preco := Geral.DMV(Preco_TXT);
      Qtde2 := dmkEdQtd2.ValueVariant;
      Valor := Preco * Qtde2;
      if dmkEdDeb.Enabled then
        dmkEdDeb.ValueVariant := Valor
      else
        dmkEdCred.ValueVariant := Valor
    end;
  end;
end;

procedure TFmLctEdit.dmkEdQtdeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Preco_TXT: String;
  Preco, Qtde1, Valor: Double;
begin
  if (Key = VK_F4) then
  begin
    if InputQuery('Pre�o', 'Informe o pre�o', Preco_TXT) then
    begin
      Preco := Geral.DMV(Preco_TXT);
      Qtde1 := dmkEdQtde.ValueVariant;
      Valor := Preco * Qtde1;
      if dmkEdDeb.Enabled then
        dmkEdDeb.ValueVariant := Valor
      else
        dmkEdCred.ValueVariant := Valor
    end;
  end;
end;

procedure TFmLctEdit.dmkEdSerieCHExit(Sender: TObject);
begin
  if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdit.IncrementaExecucoes();
begin
  dmkEdExecs.ValueVariant := dmkEdExecs.ValueVariant + 1;
end;

end.




