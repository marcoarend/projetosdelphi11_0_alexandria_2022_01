object FmLctEditC: TFmLctEditC
  Left = 339
  Top = 169
  Caption = 'FIN-LANCT-201 :: Edi'#231#227'o de Lan'#231'amentos'
  ClientHeight = 665
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 1008
    Height = 617
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Edi'#231#227'o'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PainelDados: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 589
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object LaFunci: TLabel
          Left = 496
          Top = 4
          Width = 6
          Height = 13
          Caption = '0'
          Visible = False
        end
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 553
          Height = 589
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object PnLancto1: TPanel
            Left = 0
            Top = 0
            Width = 553
            Height = 69
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object BtContas: TSpeedButton
              Left = 528
              Top = 18
              Width = 21
              Height = 21
              Hint = 'Inclui conta'
              Caption = '...'
              OnClick = BtContasClick
            end
            object Label2: TLabel
              Left = 8
              Top = 2
              Width = 125
              Height = 13
              Caption = 'Conta do plano de contas:'
            end
            object CBGenero: TdmkDBLookupComboBox
              Left = 64
              Top = 18
              Width = 465
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContas
              TabOrder = 1
              OnClick = CBGeneroClick
              OnExit = CBGeneroExit
              dmkEditCB = EdGenero
              QryCampo = 'Genero'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdGenero: TdmkEditCB
              Left = 9
              Top = 18
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Genero'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdGeneroChange
              DBLookupComboBox = CBGenero
              IgnoraDBLookupComboBox = False
            end
            object EdPsqCta: TEdit
              Left = 8
              Top = 44
              Width = 521
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGray
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              Text = 'Digite aqui a descri'#231#227'o parcial da conta para pesquisar'
              OnChange = EdPsqCtaChange
              OnEnter = EdPsqCtaEnter
            end
          end
          object Panel14: TPanel
            Left = 0
            Top = 284
            Width = 553
            Height = 44
            Align = alTop
            BevelOuter = bvLowered
            TabOrder = 2
            object LaCred: TLabel
              Left = 76
              Top = 2
              Width = 36
              Height = 13
              Caption = 'Cr'#233'dito:'
              Enabled = False
            end
            object LaDeb: TLabel
              Left = 4
              Top = 2
              Width = 34
              Height = 13
              Caption = 'D'#233'bito:'
              Enabled = False
            end
            object Label13: TLabel
              Left = 147
              Top = 2
              Width = 144
              Height = 13
              Caption = 'Descri'#231#227'o [F4], [F5], [F6], [F8]:'
            end
            object EdCredito: TdmkEdit
              Left = 76
              Top = 18
              Width = 68
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Credito'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnExit = EdCreditoExit
            end
            object EdDebito: TdmkEdit
              Left = 4
              Top = 18
              Width = 68
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Debito'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnExit = EdDebitoExit
            end
            object EdDescricao: TdmkEdit
              Left = 147
              Top = 17
              Width = 354
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Descricao'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnKeyDown = EdDescricaoKeyDown
            end
            object BitBtn3: TBitBtn
              Left = 504
              Top = 16
              Width = 42
              Height = 21
              Caption = '&Adi'
              TabOrder = 3
              OnClick = BitBtn3Click
            end
          end
          object Panel15: TPanel
            Left = 0
            Top = 69
            Width = 553
            Height = 188
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 5
            object DBGrid2: TDBGrid
              Left = 0
              Top = 0
              Width = 325
              Height = 188
              Align = alClient
              DataSource = DsPsqCta
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = DBGrid2DblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'C'#243'digo'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 229
                  Visible = True
                end>
            end
            object Panel11: TPanel
              Left = 325
              Top = 0
              Width = 228
              Height = 188
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object GroupBox7: TGroupBox
                Left = 0
                Top = 0
                Width = 228
                Height = 185
                Align = alTop
                Caption = ' Informa'#231#245'es da conta selecionada: '
                TabOrder = 0
                object Panel16: TPanel
                  Left = 2
                  Top = 15
                  Width = 224
                  Height = 168
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label51: TLabel
                    Left = 4
                    Top = 4
                    Width = 52
                    Height = 13
                    Caption = 'Sub-grupo:'
                    FocusControl = DBEdit1
                  end
                  object Label52: TLabel
                    Left = 4
                    Top = 44
                    Width = 32
                    Height = 13
                    Caption = 'Grupo:'
                    FocusControl = DBEdit2
                  end
                  object Label53: TLabel
                    Left = 4
                    Top = 84
                    Width = 45
                    Height = 13
                    Caption = 'Conjunto:'
                    FocusControl = DBEdit3
                  end
                  object Label54: TLabel
                    Left = 4
                    Top = 124
                    Width = 30
                    Height = 13
                    Caption = 'Plano:'
                    FocusControl = DBEdit4
                  end
                  object DBEdit1: TDBEdit
                    Left = 4
                    Top = 20
                    Width = 212
                    Height = 21
                    DataField = 'NOMESUBGRUPO'
                    DataSource = DsPsqCta
                    TabOrder = 0
                  end
                  object DBEdit2: TDBEdit
                    Left = 4
                    Top = 60
                    Width = 212
                    Height = 21
                    DataField = 'NOMEGRUPO'
                    DataSource = DsPsqCta
                    TabOrder = 1
                  end
                  object DBEdit3: TDBEdit
                    Left = 4
                    Top = 100
                    Width = 212
                    Height = 21
                    DataField = 'NOMECONJUNTO'
                    DataSource = DsPsqCta
                    TabOrder = 2
                  end
                  object DBEdit4: TDBEdit
                    Left = 4
                    Top = 140
                    Width = 212
                    Height = 21
                    DataField = 'NOMEPLANO'
                    DataSource = DsPsqCta
                    TabOrder = 3
                  end
                end
              end
            end
          end
          object GBRodaPe: TGroupBox
            Left = 0
            Top = 519
            Width = 553
            Height = 70
            Align = alBottom
            TabOrder = 3
            object PnSaiDesis: TPanel
              Left = 407
              Top = 15
              Width = 144
              Height = 53
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BtDesiste: TBitBtn
                Tag = 13
                Left = 8
                Top = 4
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = '&Desiste'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtDesisteClick
              end
            end
            object Panel6: TPanel
              Left = 2
              Top = 15
              Width = 405
              Height = 53
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object BtConfirma: TBitBtn
                Tag = 14
                Left = 148
                Top = 4
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = '&Confirma'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtConfirmaClick
              end
              object CkContinuar: TCheckBox
                Left = 12
                Top = 7
                Width = 113
                Height = 17
                Caption = 'Continuar inserindo.'
                TabOrder = 2
                Visible = False
              end
              object CkCopiaCH: TCheckBox
                Left = 12
                Top = 24
                Width = 129
                Height = 17
                Caption = 'Fazer c'#243'pia de cheque.'
                Enabled = False
                TabOrder = 3
              end
              object BitBtn1: TBitBtn
                Tag = 10042
                Left = 270
                Top = 4
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = 'C&heque'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                Visible = False
                OnClick = BitBtn1Click
              end
            end
          end
          object Panel25: TPanel
            Left = 0
            Top = 257
            Width = 553
            Height = 27
            Align = alTop
            BevelOuter = bvLowered
            TabOrder = 1
            object LaDoc: TLabel
              Left = 4
              Top = 5
              Width = 121
              Height = 13
              Caption = 'S'#233'rie / docum. (CH) [F4] :'
            end
            object LaMes: TLabel
              Left = 444
              Top = 5
              Width = 44
              Height = 13
              Caption = 'M'#234's [F4]:'
              Enabled = False
            end
            object LaVencimento: TLabel
              Left = 244
              Top = 5
              Width = 80
              Height = 13
              Caption = 'Vencimento [F6]:'
            end
            object dmkEdSerieCH: TdmkEdit
              Left = 128
              Top = 3
              Width = 41
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'SerieCH'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnExit = dmkEdSerieCHExit
              OnKeyDown = dmkEdSerieCHKeyDown
            end
            object dmkEdDoc: TdmkEdit
              Left = 168
              Top = 3
              Width = 62
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000000'
              QryCampo = 'Documento'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = dmkEdDocChange
              OnExit = dmkEdDocExit
              OnKeyDown = dmkEdDocKeyDown
            end
            object EdMes: TdmkEdit
              Left = 492
              Top = 3
              Width = 51
              Height = 21
              Alignment = taCenter
              TabOrder = 3
              FormatType = dmktfMesAno
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfLong
              HoraFormat = dmkhfShort
              QryCampo = 'Mez'
              UpdType = utYes
              Obrigatorio = True
              PermiteNulo = False
              ValueVariant = Null
              ValWarn = False
              OnExit = EdMesExit
              OnKeyDown = EdMesKeyDown
            end
            object dmkEdTPVencto: TdmkEditDateTimePicker
              Left = 328
              Top = 3
              Width = 104
              Height = 21
              Date = 39615.672523148100000000
              Time = 39615.672523148100000000
              TabOrder = 2
              OnClick = dmkEdTPVenctoClick
              OnChange = dmkEdTPVenctoChange
              OnKeyDown = dmkEdTPVenctoKeyDown
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'Vencimento'
              UpdType = utYes
            end
          end
          object DBGrid3: TDBGrid
            Left = 0
            Top = 328
            Width = 553
            Height = 191
            Align = alClient
            DataSource = DsMulInsLct
            TabOrder = 4
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnKeyDown = DBGrid3KeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'GeneroCod'
                Title.Caption = 'Conta'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GeneroNom'
                Title.Caption = 'Nome conta'
                Width = 176
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Title.Caption = 'Valor D/C'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Mes'
                Title.Caption = 'Compet.'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Descri'#231#227'o  ([Ctrl + DEL] exclui linha selecionada!)'
                Width = 513
                Visible = True
              end>
          end
        end
        object Panel4: TPanel
          Left = 553
          Top = 0
          Width = 447
          Height = 589
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object PageControl2: TPageControl
            Left = 0
            Top = 0
            Width = 447
            Height = 589
            ActivePage = TabSheet7
            Align = alClient
            TabOrder = 0
            object TabSheet7: TTabSheet
              Caption = ' Pesquisa de lan'#231'amentos'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel18: TPanel
                Left = 0
                Top = 0
                Width = 439
                Height = 561
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Panel21: TPanel
                  Left = 0
                  Top = 37
                  Width = 439
                  Height = 524
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Bevel1: TBevel
                    Left = 4
                    Top = 298
                    Width = 157
                    Height = 57
                  end
                  object Bevel2: TBevel
                    Left = 168
                    Top = 298
                    Width = 157
                    Height = 57
                  end
                  object CkDataIni: TCheckBox
                    Left = 4
                    Top = 4
                    Width = 149
                    Height = 17
                    Caption = 'Data inicial ou '#250'nica'
                    TabOrder = 0
                  end
                  object CkDataFim: TCheckBox
                    Left = 172
                    Top = 4
                    Width = 97
                    Height = 17
                    Caption = 'Data final'
                    TabOrder = 2
                    OnClick = CkDataFimClick
                  end
                  object TPDataIni: TdmkEditDateTimePicker
                    Left = 4
                    Top = 24
                    Width = 152
                    Height = 21
                    CalColors.TextColor = clMenuText
                    Date = 37636.777157974500000000
                    Time = 37636.777157974500000000
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                  end
                  object TPDataFim: TdmkEditDateTimePicker
                    Left = 172
                    Top = 24
                    Width = 152
                    Height = 21
                    Date = 37636.777203761600000000
                    Time = 37636.777203761600000000
                    TabOrder = 3
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                  end
                  object CkConta: TCheckBox
                    Left = 4
                    Top = 104
                    Width = 61
                    Height = 17
                    Caption = 'Conta'
                    TabOrder = 8
                  end
                  object CBConta: TdmkDBLookupComboBox
                    Left = 52
                    Top = 124
                    Width = 272
                    Height = 21
                    DataSource = DsPContas
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsContas
                    TabOrder = 10
                    dmkEditCB = EdConta
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object CkDescricao: TCheckBox
                    Left = 4
                    Top = 152
                    Width = 373
                    Height = 17
                    Caption = 'Descri'#231#227'o parcial ou total (Utilize "%" para m'#225'scara):'
                    TabOrder = 11
                  end
                  object EdPesqDescri: TEdit
                    Left = 4
                    Top = 173
                    Width = 320
                    Height = 21
                    TabOrder = 12
                    Text = '%%'
                  end
                  object CkDebito: TCheckBox
                    Left = 8
                    Top = 302
                    Width = 145
                    Height = 17
                    Caption = 'D'#233'bito (m'#237'nimo e m'#225'ximo)'
                    TabOrder = 19
                  end
                  object CkCredito: TCheckBox
                    Left = 171
                    Top = 302
                    Width = 146
                    Height = 17
                    Caption = 'Cr'#233'dito (m'#237'nimo e m'#225'ximo)'
                    TabOrder = 22
                  end
                  object CkNF: TCheckBox
                    Left = 4
                    Top = 362
                    Width = 49
                    Height = 17
                    Caption = 'N.F.'
                    TabOrder = 25
                  end
                  object CkDoc: TCheckBox
                    Left = 164
                    Top = 362
                    Width = 77
                    Height = 17
                    Caption = 'Doc. (Cheq.)'
                    TabOrder = 29
                  end
                  object CkControle: TCheckBox
                    Left = 245
                    Top = 362
                    Width = 61
                    Height = 17
                    Caption = 'Controle'
                    TabOrder = 31
                  end
                  object EdDebMin: TdmkEdit
                    Left = 8
                    Top = 326
                    Width = 72
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 20
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnExit = EdDebMinExit
                  end
                  object EdCredMin: TdmkEdit
                    Left = 171
                    Top = 326
                    Width = 72
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 23
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnExit = EdCredMinExit
                  end
                  object EdNF: TdmkEdit
                    Left = 4
                    Top = 382
                    Width = 76
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 26
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object EdDoc: TdmkEdit
                    Left = 164
                    Top = 382
                    Width = 76
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 30
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object EdControle: TdmkEdit
                    Left = 245
                    Top = 382
                    Width = 80
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 32
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object RgOrdem: TRadioGroup
                    Left = 334
                    Top = 4
                    Width = 100
                    Height = 185
                    Caption = ' Ordem resultados: '
                    ItemIndex = 0
                    Items.Strings = (
                      'Data, Controle'
                      'Controle'
                      'Nota Fiscal'
                      'Documento'
                      'Descri'#231#227'o'
                      'D'#233'bito, Cr'#233'dito'
                      'Cr'#233'dito, D'#233'bito')
                    TabOrder = 37
                  end
                  object EdDuplicata: TdmkEdit
                    Left = 84
                    Top = 382
                    Width = 76
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 28
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object CkDuplicata: TCheckBox
                    Left = 84
                    Top = 362
                    Width = 73
                    Height = 17
                    Caption = 'Duplicata'
                    TabOrder = 27
                  end
                  object EdDebMax: TdmkEdit
                    Left = 82
                    Top = 325
                    Width = 72
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 21
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdCredMax: TdmkEdit
                    Left = 247
                    Top = 326
                    Width = 68
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 24
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object CkExcelGru: TCheckBox
                    Left = 4
                    Top = 456
                    Width = 317
                    Height = 17
                    Caption = 'Somente lan'#231'amentos sem contas sazonais.'
                    TabOrder = 36
                  end
                  object EdConta: TdmkEditCB
                    Left = 4
                    Top = 124
                    Width = 45
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 9
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBConta
                    IgnoraDBLookupComboBox = False
                  end
                  object CGCarteiras: TdmkCheckGroup
                    Left = 334
                    Top = 195
                    Width = 100
                    Height = 105
                    Caption = ' Carteiras: '
                    Items.Strings = (
                      'Caixa (esp.)'
                      'Banco (c/c)'
                      'Emiss'#227'o banc.')
                    TabOrder = 38
                    UpdType = utYes
                    Value = 0
                    OldValor = 0
                  end
                  object CkCliInt: TCheckBox
                    Left = 4
                    Top = 410
                    Width = 317
                    Height = 17
                    Caption = 'Cliente interno:'
                    TabOrder = 33
                  end
                  object EdCliInt: TdmkEditCB
                    Left = 4
                    Top = 430
                    Width = 45
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 34
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBCliInt
                    IgnoraDBLookupComboBox = False
                  end
                  object CBCliInt: TdmkDBLookupComboBox
                    Left = 52
                    Top = 430
                    Width = 272
                    Height = 21
                    DataSource = DsPCliInt
                    KeyField = 'Codigo'
                    ListField = 'NOMEENTIDADE'
                    ListSource = DsCliInt
                    TabOrder = 35
                    dmkEditCB = EdCliInt
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object CkVctoIni: TCheckBox
                    Left = 4
                    Top = 52
                    Width = 149
                    Height = 17
                    Caption = 'Vencimento inicial ou '#250'nico'
                    TabOrder = 4
                  end
                  object TPVctoIni: TdmkEditDateTimePicker
                    Left = 4
                    Top = 72
                    Width = 152
                    Height = 21
                    CalColors.TextColor = clMenuText
                    Date = 37636.777157974500000000
                    Time = 37636.777157974500000000
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 5
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                  end
                  object CkVctoFim: TCheckBox
                    Left = 172
                    Top = 52
                    Width = 97
                    Height = 17
                    Caption = 'Vencimento final'
                    TabOrder = 6
                    OnClick = CkVctoFimClick
                  end
                  object TPVctoFim: TdmkEditDateTimePicker
                    Left = 172
                    Top = 72
                    Width = 152
                    Height = 21
                    Date = 37636.777203761600000000
                    Time = 37636.777203761600000000
                    TabOrder = 7
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                  end
                  object CBFornece: TdmkDBLookupComboBox
                    Left = 52
                    Top = 220
                    Width = 272
                    Height = 21
                    DataSource = DsPFornece
                    KeyField = 'Codigo'
                    ListField = 'NOMEENT'
                    TabOrder = 15
                    dmkEditCB = EdFornece
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object EdFornece: TdmkEditCB
                    Left = 4
                    Top = 220
                    Width = 45
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 14
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBFornece
                    IgnoraDBLookupComboBox = False
                  end
                  object CkFornece: TCheckBox
                    Left = 4
                    Top = 200
                    Width = 317
                    Height = 17
                    Caption = 'Fornecedor'
                    TabOrder = 13
                  end
                  object CkCliente: TCheckBox
                    Left = 4
                    Top = 249
                    Width = 317
                    Height = 17
                    Caption = 'Cliente'
                    TabOrder = 16
                  end
                  object EdCliente: TdmkEditCB
                    Left = 4
                    Top = 269
                    Width = 45
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 17
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBCliente
                    IgnoraDBLookupComboBox = False
                  end
                  object CBCliente: TdmkDBLookupComboBox
                    Left = 52
                    Top = 269
                    Width = 272
                    Height = 21
                    DataSource = DsPCliente
                    KeyField = 'Codigo'
                    ListField = 'NOMEENT'
                    TabOrder = 18
                    dmkEditCB = EdCliente
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object RGTabela: TRadioGroup
                    Left = 334
                    Top = 300
                    Width = 100
                    Height = 85
                    Caption = ' Arquivo: '
                    ItemIndex = 0
                    Items.Strings = (
                      'A - Ativo'
                      'B - Encerrado'
                      'D - Morto')
                    TabOrder = 39
                  end
                  object BtPsqLct: TBitBtn
                    Tag = 18
                    Left = 340
                    Top = 412
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
                    Caption = '&Pesquisa'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    NumGlyphs = 2
                    ParentFont = False
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 40
                    OnClick = BtPsqLctClick
                  end
                  object GroupBox6: TGroupBox
                    Left = 0
                    Top = 475
                    Width = 439
                    Height = 49
                    Align = alBottom
                    TabOrder = 41
                    object Panel5: TPanel
                      Left = 2
                      Top = 15
                      Width = 435
                      Height = 17
                      Align = alTop
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Label18: TLabel
                        Left = 4
                        Top = 1
                        Width = 165
                        Height = 13
                        Caption = 'Quantidade de contas dispon'#237'veis:'
                      end
                      object LaContas: TLabel
                        Left = 176
                        Top = 1
                        Width = 22
                        Height = 13
                        Caption = '000'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = [fsBold]
                        ParentFont = False
                      end
                    end
                    object Panel17: TPanel
                      Left = 2
                      Top = 32
                      Width = 435
                      Height = 15
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 1
                      object Label61: TLabel
                        Left = 4
                        Top = 1
                        Width = 168
                        Height = 13
                        Caption = 'Quantidade de contas na pesquisa:'
                      end
                      object LaCtaPsq: TLabel
                        Left = 176
                        Top = 1
                        Width = 22
                        Height = 13
                        Caption = '000'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'MS Sans Serif'
                        Font.Style = [fsBold]
                        ParentFont = False
                      end
                    end
                  end
                end
                object GBAvisos1: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 439
                  Height = 37
                  Align = alTop
                  Caption = ' Avisos: '
                  TabOrder = 1
                  object Panel19: TPanel
                    Left = 2
                    Top = 15
                    Width = 435
                    Height = 20
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object LaAviso1: TLabel
                      Left = 13
                      Top = 2
                      Width = 329
                      Height = 16
                      Caption = 'INFORME SOMENTE OS DADOS QUE TEM CERTEZA!'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clSilver
                      Font.Height = -13
                      Font.Name = 'Arial'
                      Font.Style = []
                      ParentFont = False
                      Transparent = True
                    end
                    object LaAviso2: TLabel
                      Left = 12
                      Top = 1
                      Width = 329
                      Height = 16
                      Caption = 'INFORME SOMENTE OS DADOS QUE TEM CERTEZA!'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clRed
                      Font.Height = -13
                      Font.Name = 'Arial'
                      Font.Style = []
                      ParentFont = False
                      Transparent = True
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Outros dados'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel111: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 589
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object GBVendas: TGroupBox
          Left = 0
          Top = 205
          Width = 1000
          Height = 107
          Align = alTop
          TabOrder = 1
          Visible = False
          object PnVendas: TPanel
            Left = 2
            Top = 15
            Width = 996
            Height = 90
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            Visible = False
            object Label10: TLabel
              Left = 8
              Top = 4
              Width = 58
              Height = 13
              Caption = 'Funcion'#225'rio:'
              Visible = False
            end
            object LaVendedor: TLabel
              Left = 8
              Top = 48
              Width = 49
              Height = 13
              Caption = 'Vendedor:'
              Visible = False
            end
            object LaICMS_P: TLabel
              Left = 288
              Top = 4
              Width = 37
              Height = 13
              Caption = '% ICMS'
              Visible = False
            end
            object LaICMS_V: TLabel
              Left = 392
              Top = 4
              Width = 56
              Height = 13
              Caption = 'Valor ICMS:'
              Visible = False
            end
            object dmkCBFunci: TdmkDBLookupComboBox
              Left = 68
              Top = 20
              Width = 217
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsFunci
              TabOrder = 1
              Visible = False
              dmkEditCB = dmkEdCBFunci
              QryCampo = 'UserCad'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkEdCBFunci: TdmkEditCB
              Left = 8
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              Visible = False
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'UserCad'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = dmkCBFunci
              IgnoraDBLookupComboBox = False
            end
            object dmkEdCBVendedor: TdmkEditCB
              Left = 8
              Top = 64
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              Visible = False
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'UserCad'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = dmkCBVendedor
              IgnoraDBLookupComboBox = False
            end
            object dmkCBVendedor: TdmkDBLookupComboBox
              Left = 68
              Top = 64
              Width = 217
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsVendedores
              TabOrder = 5
              Visible = False
              dmkEditCB = dmkEdCBVendedor
              QryCampo = 'Vendedor'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkEdICMS_P: TdmkEdit
              Left = 288
              Top = 20
              Width = 101
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              Visible = False
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'ICMS_P'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnExit = dmkEdICMS_PExit
              OnKeyDown = dmkEdICMS_PKeyDown
            end
            object dmkEdICMS_V: TdmkEdit
              Left = 392
              Top = 20
              Width = 101
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              Visible = False
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'ICMS_V'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnExit = dmkEdICMS_VExit
              OnKeyDown = dmkEdICMS_VKeyDown
            end
          end
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 1000
          Height = 205
          Align = alTop
          TabOrder = 0
          object Panel1: TPanel
            Left = 2
            Top = 15
            Width = 996
            Height = 188
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object LaDataDoc: TLabel
              Left = 4
              Top = 4
              Width = 82
              Height = 13
              Caption = 'Data documento:'
            end
            object Label22: TLabel
              Left = 4
              Top = 44
              Width = 65
              Height = 13
              Caption = 'Compensado:'
              Enabled = False
            end
            object GroupBox2: TGroupBox
              Left = 108
              Top = 4
              Width = 461
              Height = 137
              TabOrder = 2
              object GroupBox3: TGroupBox
                Left = 8
                Top = 8
                Width = 233
                Height = 61
                Caption = ' Multa e juros a cobrar: '
                TabOrder = 0
                object Label8: TLabel
                  Left = 8
                  Top = 16
                  Width = 83
                  Height = 13
                  Caption = 'Juros ao M'#234's (%):'
                end
                object Label7: TLabel
                  Left = 120
                  Top = 16
                  Width = 46
                  Height = 13
                  Caption = 'Multa (%):'
                end
                object dmkEdMoraDia: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 109
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryCampo = 'MoraDia'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object dmkEdMulta: TdmkEdit
                  Left = 120
                  Top = 32
                  Width = 106
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryCampo = 'Multa'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object GroupBox4: TGroupBox
                Left = 8
                Top = 72
                Width = 233
                Height = 61
                Caption = ' Multa e juros pagos: '
                TabOrder = 1
                object Label19: TLabel
                  Left = 120
                  Top = 16
                  Width = 38
                  Height = 13
                  Caption = 'Multa $:'
                end
                object Label21: TLabel
                  Left = 8
                  Top = 16
                  Width = 37
                  Height = 13
                  Caption = 'Juros $:'
                end
                object dmkEdMultaVal: TdmkEdit
                  Left = 120
                  Top = 32
                  Width = 106
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryCampo = 'MultaVal'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object dmkEdMoraVal: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 109
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  QryCampo = 'MoraVal'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
              object GroupBox9: TGroupBox
                Left = 244
                Top = 8
                Width = 209
                Height = 125
                Caption = 'Rec'#225'lculo: '
                TabOrder = 2
                object Label28: TLabel
                  Left = 8
                  Top = 16
                  Width = 63
                  Height = 13
                  Caption = 'Valor original:'
                end
                object Label29: TLabel
                  Left = 116
                  Top = 16
                  Width = 46
                  Height = 13
                  Caption = 'Multa (%):'
                end
                object BitBtn2: TBitBtn
                  Tag = 180
                  Left = 43
                  Top = 68
                  Width = 120
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
                  Caption = '&Recalcular'
                  Enabled = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  NumGlyphs = 2
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                  OnClick = BitBtn2Click
                end
                object dmkEdValNovo: TdmkEdit
                  Left = 8
                  Top = 32
                  Width = 105
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object dmkEdPerMult: TdmkEdit
                  Left = 116
                  Top = 32
                  Width = 85
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
            object dmkEdTPDataDoc: TdmkEditDateTimePicker
              Left = 4
              Top = 20
              Width = 101
              Height = 21
              Date = 39615.655720300900000000
              Time = 39615.655720300900000000
              TabOrder = 0
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DataDoc'
              UpdType = utYes
            end
            object dmkEdTPCompensado: TdmkEditDateTimePicker
              Left = 4
              Top = 60
              Width = 101
              Height = 21
              Date = 39615.887948310200000000
              Time = 39615.887948310200000000
              Enabled = False
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'Compensado'
              UpdType = utYes
            end
            object dmkRGTipoCH: TdmkRadioGroup
              Left = 4
              Top = 84
              Width = 101
              Height = 101
              Caption = ' Tipo de cheque: '
              ItemIndex = 0
              Items.Strings = (
                '?'
                'Visado'
                'Cruzado'
                'Ambos')
              TabOrder = 3
              QryCampo = 'TipoCH'
              UpdType = utYes
              OldValor = 0
            end
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Parcelamento'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 156
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object GBParcelamento: TGroupBox
          Left = 0
          Top = 0
          Width = 1000
          Height = 156
          Align = alClient
          Caption = ' Parcelamento autom'#225'tico: '
          TabOrder = 1
          Visible = False
          object Label23: TLabel
            Left = 8
            Top = 20
            Width = 44
            Height = 13
            Caption = 'Parcelas:'
          end
          object Label24: TLabel
            Left = 508
            Top = 24
            Width = 78
            Height = 13
            Caption = 'Primeira parcela:'
          end
          object Label25: TLabel
            Left = 588
            Top = 24
            Width = 70
            Height = 13
            Caption = #218'ltima parcela:'
          end
          object Label26: TLabel
            Left = 660
            Top = 24
            Width = 97
            Height = 13
            Caption = 'Total parcelamento: '
          end
          object RGArredondar: TRadioGroup
            Left = 308
            Top = 16
            Width = 193
            Height = 45
            Caption = '   Arredondar '
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              '1'#170' parcela'
              #218'ltima parcela')
            TabOrder = 7
            OnClick = RGArredondarClick
          end
          object RGPeriodo: TRadioGroup
            Left = 60
            Top = 16
            Width = 141
            Height = 45
            Caption = ' Periodo: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Mensal'
              '         dias')
            TabOrder = 1
            OnClick = RGPeriodoClick
          end
          object EdDias: TdmkEdit
            Left = 151
            Top = 32
            Width = 24
            Height = 21
            Alignment = taRightJustify
            Color = clBtnFace
            Enabled = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '7'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 7
            ValWarn = False
            OnExit = EdDiasExit
          end
          object RGIncremCH: TRadioGroup
            Left = 204
            Top = 16
            Width = 101
            Height = 45
            Caption = ' Increm. cheque.: '
            Columns = 2
            Enabled = False
            ItemIndex = 1
            Items.Strings = (
              'N'#227'o'
              'Sim')
            TabOrder = 3
            OnClick = RGIncremCHClick
          end
          object EdParcela1: TdmkEdit
            Left = 508
            Top = 40
            Width = 77
            Height = 21
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdParcelaX: TdmkEdit
            Left = 588
            Top = 40
            Width = 69
            Height = 21
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object CkArredondar: TCheckBox
            Left = 320
            Top = 14
            Width = 77
            Height = 17
            Caption = 'Arredondar: '
            TabOrder = 6
            OnClick = CkArredondarClick
          end
          object EdSoma: TdmkEdit
            Left = 660
            Top = 40
            Width = 97
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 8
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object RGIncremDupl: TGroupBox
            Left = 8
            Top = 66
            Width = 381
            Height = 73
            Caption = '   Incremento de duplicata: '
            TabOrder = 10
            Visible = False
            object Label27: TLabel
              Left = 4
              Top = 36
              Width = 52
              Height = 13
              Caption = 'Separador:'
            end
            object EdDuplSep: TEdit
              Left = 60
              Top = 32
              Width = 21
              Height = 21
              TabOrder = 0
              Text = '/'
              OnExit = EdDuplSepExit
            end
            object RGDuplSeq: TRadioGroup
              Left = 84
              Top = 16
              Width = 293
              Height = 53
              Caption = ' Sequenciador: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Num'#233'rico: 1, 2, 3, etc.'
                'Alfab'#233'tico: A, B, C, etc.')
              TabOrder = 1
              OnClick = RGDuplSeqClick
            end
          end
          object GBIncremTxt: TGroupBox
            Left = 392
            Top = 66
            Width = 381
            Height = 73
            Caption = '       '
            TabOrder = 11
            Visible = False
            object Label30: TLabel
              Left = 4
              Top = 36
              Width = 52
              Height = 13
              Caption = 'Separador:'
            end
            object EdSepTxt: TEdit
              Left = 60
              Top = 32
              Width = 21
              Height = 21
              TabOrder = 0
              Text = '/'
              OnExit = EdDuplSepExit
            end
            object RGSepTxt: TRadioGroup
              Left = 84
              Top = 20
              Width = 293
              Height = 49
              Caption = ' Sequenciador: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Num'#233'rico: 1, 2, 3, etc.'
                'Alfab'#233'tico: A, B, C, etc.')
              TabOrder = 1
              OnClick = RGDuplSeqClick
            end
          end
          object CkIncremDU: TCheckBox
            Left = 20
            Top = 63
            Width = 137
            Height = 17
            Caption = 'Incremento da duplicata: '
            TabOrder = 9
            OnClick = CkIncremDUClick
          end
          object CkIncremTxt: TCheckBox
            Left = 408
            Top = 64
            Width = 121
            Height = 17
            Caption = 'Incremento do texto: '
            TabOrder = 12
            OnClick = CkIncremTxtClick
          end
          object dmkEdParcelas: TdmkEdit
            Left = 8
            Top = 36
            Width = 45
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 3
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '2'
            ValMax = '1000'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '002'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 2
            ValWarn = False
            OnExit = dmkEdParcelasExit
          end
        end
        object CkParcelamento: TCheckBox
          Left = 12
          Top = 1
          Width = 153
          Height = 17
          Caption = ' Parcelamento autom'#225'tico: '
          TabOrder = 0
          Visible = False
          OnClick = CkParcelamentoClick
        end
      end
      object DBGParcelas: TDBGrid
        Left = 0
        Top = 156
        Width = 1000
        Height = 433
        TabStop = False
        Align = alClient
        DataSource = DsParcPagtos
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnKeyDown = DBGParcelasKeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'Parcela'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Data'
            Title.Caption = 'Vencimento'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Debito'
            Title.Caption = 'D'#233'bito'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Credito'
            Title.Caption = 'Cr'#233'dito'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Doc'
            Title.Caption = 'Docum.'
            Width = 49
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Mora'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Multa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ICMS_V'
            Title.Caption = '$ ICMS'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Duplicata'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Visible = True
          end>
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Hist'#243'rico'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object TabControl1: TTabControl
        Left = 0
        Top = 0
        Width = 1000
        Height = 589
        Align = alClient
        TabOrder = 0
        Tabs.Strings = (
          'Carteira'
          'Cliente interno'
          'Cliente'
          'Fornecedor'
          'Conta')
        TabIndex = 0
        OnChange = TabControl1Change
        object DBGrid1: TDBGrid
          Left = 4
          Top = 24
          Width = 992
          Height = 561
          Align = alClient
          DataSource = DsLct
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Empresa'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Title.Caption = 'N.F.'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Duplicata'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'amento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Title.Caption = 'Saldo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMS_P'
              Title.Caption = '% ICMS'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMS_V'
              Title.Caption = '$ ICMS'
              Visible = True
            end>
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = ' Dados complementares '
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel8: TPanel
        Left = 0
        Top = 510
        Width = 1000
        Height = 79
        Align = alBottom
        BevelOuter = bvNone
        Caption = 'Panel8'
        ParentBackground = False
        TabOrder = 0
        object MeConfig: TMemo
          Left = 0
          Top = 0
          Width = 1000
          Height = 79
          Align = alClient
          Enabled = False
          TabOrder = 0
        end
      end
      object Panel10: TPanel
        Left = 0
        Top = 62
        Width = 1000
        Height = 448
        Align = alClient
        BevelOuter = bvNone
        Enabled = False
        ParentBackground = False
        TabOrder = 1
        object Label5: TLabel
          Left = 8
          Top = 8
          Width = 47
          Height = 13
          Caption = 'CNAB_Sit'
          Enabled = False
        end
        object Label6: TLabel
          Left = 8
          Top = 32
          Width = 39
          Height = 13
          Caption = 'ID_Pgto'
          Enabled = False
        end
        object Label12: TLabel
          Left = 8
          Top = 80
          Width = 26
          Height = 13
          Caption = 'FatID'
          Enabled = False
        end
        object Label16: TLabel
          Left = 8
          Top = 56
          Width = 36
          Height = 13
          Caption = 'TipoCH'
          Enabled = False
        end
        object Label17: TLabel
          Left = 8
          Top = 128
          Width = 37
          Height = 13
          Caption = 'FatNum'
          Enabled = False
        end
        object Label31: TLabel
          Left = 8
          Top = 104
          Width = 51
          Height = 13
          Caption = 'FatID_Sub'
          Enabled = False
        end
        object Label32: TLabel
          Left = 8
          Top = 176
          Width = 24
          Height = 13
          Caption = 'Nivel'
          Enabled = False
        end
        object Label33: TLabel
          Left = 8
          Top = 152
          Width = 51
          Height = 13
          Caption = 'FatParcela'
          Enabled = False
        end
        object Label34: TLabel
          Left = 8
          Top = 224
          Width = 26
          Height = 13
          Caption = 'CtrlIni'
          Enabled = False
        end
        object Label35: TLabel
          Left = 8
          Top = 200
          Width = 47
          Height = 13
          Caption = 'DescoPor'
          Enabled = False
        end
        object Label36: TLabel
          Left = 8
          Top = 272
          Width = 46
          Height = 13
          Caption = 'DescoVal'
          Enabled = False
        end
        object Label37: TLabel
          Left = 8
          Top = 248
          Width = 40
          Height = 13
          Caption = 'Unidade'
          Enabled = False
        end
        object Label38: TLabel
          Left = 160
          Top = 8
          Width = 26
          Height = 13
          Caption = 'Doc2'
          Enabled = False
        end
        object Label39: TLabel
          Left = 8
          Top = 296
          Width = 29
          Height = 13
          Caption = 'NFVal'
          Enabled = False
        end
        object Label40: TLabel
          Left = 160
          Top = 32
          Width = 35
          Height = 13
          Caption = 'CartAnt'
          Enabled = False
        end
        object Label41: TLabel
          Left = 160
          Top = 56
          Width = 37
          Height = 13
          Caption = 'TipoAnt'
          Enabled = False
        end
        object Label42: TLabel
          Left = 160
          Top = 80
          Width = 47
          Height = 13
          Caption = 'PercJuros'
          Enabled = False
        end
        object Label43: TLabel
          Left = 160
          Top = 104
          Width = 48
          Height = 13
          Caption = 'PercMulta'
          Enabled = False
        end
        object Label44: TLabel
          Left = 160
          Top = 128
          Width = 26
          Height = 13
          Caption = 'ICMS'
          Enabled = False
        end
        object Label45: TLabel
          Left = 160
          Top = 296
          Width = 56
          Height = 13
          Caption = 'Execu'#231#245'es:'
          Enabled = False
        end
        object Label46: TLabel
          Left = 160
          Top = 200
          Width = 70
          Height = 13
          Caption = 'Cliente interno:'
          Enabled = False
        end
        object Label3: TLabel
          Left = 160
          Top = 224
          Width = 68
          Height = 13
          Caption = 'Forneced. int.:'
          Enabled = False
        end
        object Label47: TLabel
          Left = 160
          Top = 248
          Width = 69
          Height = 13
          Caption = 'Vendedor  int.:'
          Enabled = False
        end
        object Label48: TLabel
          Left = 160
          Top = 272
          Width = 60
          Height = 13
          Caption = 'Account int.:'
          Enabled = False
        end
        object Label49: TLabel
          Left = 160
          Top = 152
          Width = 35
          Height = 13
          Caption = 'Cliente:'
          Enabled = False
        end
        object Label50: TLabel
          Left = 160
          Top = 176
          Width = 60
          Height = 13
          Caption = 'Fornecedor.:'
          Enabled = False
        end
        object Label9: TLabel
          Left = 8
          Top = 320
          Width = 36
          Height = 13
          Caption = 'Tabela:'
          Enabled = False
        end
        object Label4: TLabel
          Left = 96
          Top = 347
          Width = 112
          Height = 13
          Caption = 'Configura'#231#227'o da janela:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object LaFinalidade: TLabel
          Left = 212
          Top = 348
          Width = 22
          Height = 13
          Caption = '000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object dmkEdCNAB_Sit: TdmkEdit
          Left = 80
          Top = 4
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CNAB_Sit'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdID_pgto: TdmkEdit
          Left = 80
          Top = 28
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ID_Pgto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdFatID: TdmkEdit
          Left = 80
          Top = 76
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FatID'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdTipoCH: TdmkEdit
          Left = 80
          Top = 52
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'TipoCH'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdFatNum: TdmkEdit
          Left = 80
          Top = 124
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FatNum'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdFatID_Sub: TdmkEdit
          Left = 80
          Top = 100
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FatID_Sub'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdNivel: TdmkEdit
          Left = 80
          Top = 172
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Nivel'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdFatParcela: TdmkEdit
          Left = 80
          Top = 148
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FatParcela'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdCtrlIni: TdmkEdit
          Left = 80
          Top = 220
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CtrlIni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdDescoPor: TdmkEdit
          Left = 80
          Top = 196
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'DescoPor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdDescoVal: TdmkEdit
          Left = 80
          Top = 268
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'DescoVal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object dmkEdUnidade: TdmkEdit
          Left = 80
          Top = 244
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 11
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Unidade'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdDoc2: TdmkEdit
          Left = 232
          Top = 4
          Width = 72
          Height = 21
          Enabled = False
          TabOrder = 12
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Doc2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '0'
          ValWarn = False
        end
        object dmkEdNFVal: TdmkEdit
          Left = 80
          Top = 292
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'NFVal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object dmkEdTipoAnt: TdmkEdit
          Left = 232
          Top = 52
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 14
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Tipo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdCartAnt: TdmkEdit
          Left = 232
          Top = 28
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 15
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Carteira'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdPercJuroM: TdmkEdit
          Left = 232
          Top = 76
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 16
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdPercMulta: TdmkEdit
          Left = 232
          Top = 100
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 17
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEd_: TdmkEdit
          Left = 232
          Top = 124
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 18
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object dmkEdExecs: TdmkEdit
          Left = 232
          Top = 292
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 19
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdOneCliInt: TdmkEdit
          Left = 232
          Top = 196
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 20
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-1000000'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdOneForneceI: TdmkEdit
          Left = 232
          Top = 220
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 21
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-1000000'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdOneVendedor: TdmkEdit
          Left = 232
          Top = 244
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 22
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-1000000'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdOneAccount: TdmkEdit
          Left = 232
          Top = 268
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 23
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-1000000'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdOneCliente: TdmkEdit
          Left = 232
          Top = 148
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 24
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-1000000'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object dmkEdOneFornecedor: TdmkEdit
          Left = 232
          Top = 172
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 25
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-1000000'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CkDuplicando: TCheckBox
          Left = 8
          Top = 344
          Width = 97
          Height = 17
          Caption = 'Duplicando?'
          TabOrder = 26
        end
        object EdTabLctA: TdmkEdit
          Left = 80
          Top = 316
          Width = 224
          Height = 21
          Enabled = False
          TabOrder = 27
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object Panel3: TPanel
          Left = 668
          Top = 0
          Width = 332
          Height = 448
          Align = alRight
          TabOrder = 28
          object PnMaskPesq: TPanel
            Left = 1
            Top = 1
            Width = 330
            Height = 446
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object GBLancto3: TGroupBox
              Left = 0
              Top = 311
              Width = 330
              Height = 135
              Align = alBottom
              TabOrder = 0
              object CkPesqNF: TCheckBox
                Left = 8
                Top = 16
                Width = 300
                Height = 17
                TabStop = False
                Caption = 'Pesquisar duplica'#231#227'o de nota fiscal.'
                Checked = True
                State = cbChecked
                TabOrder = 0
              end
              object CkPesqCH: TCheckBox
                Left = 8
                Top = 44
                Width = 300
                Height = 17
                TabStop = False
                Caption = 'Pesquisar duplica'#231#227'o de cheque.'
                Checked = True
                State = cbChecked
                TabOrder = 1
              end
              object CkPesqVal: TCheckBox
                Left = 8
                Top = 67
                Width = 300
                Height = 26
                TabStop = False
                Caption = 
                  'Pesquisa lan'#231'amentos cruzando o valor com a conta e o m'#234's de com' +
                  'pet'#234'ncia.'
                Checked = True
                State = cbChecked
                TabOrder = 2
                WordWrap = True
              end
              object CkNaoPesquisar: TCheckBox
                Left = 9
                Top = 104
                Width = 169
                Height = 17
                Caption = 'N'#227'o fazer nenhuma pesquisa.'
                TabOrder = 3
              end
            end
          end
        end
      end
      object Panel13: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 62
        Align = alTop
        BevelOuter = bvNone
        Enabled = False
        ParentBackground = False
        TabOrder = 2
        object Label62: TLabel
          Left = 812
          Top = 16
          Width = 62
          Height = 13
          Caption = 'ID doc fisico:'
          Enabled = False
        end
        object GroupBox5: TGroupBox
          Left = 4
          Top = 0
          Width = 393
          Height = 61
          Caption = ' Dados para altera'#231#227'o '
          TabOrder = 0
          object Label55: TLabel
            Left = 140
            Top = 16
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object Label56: TLabel
            Left = 12
            Top = 16
            Width = 42
            Height = 13
            Caption = 'Controle:'
          end
          object Label57: TLabel
            Left = 76
            Top = 16
            Width = 22
            Height = 13
            Caption = 'Sub:'
          end
          object Label58: TLabel
            Left = 256
            Top = 16
            Width = 24
            Height = 13
            Caption = 'Tipo:'
          end
          object Label59: TLabel
            Left = 320
            Top = 16
            Width = 39
            Height = 13
            Caption = 'Carteira:'
          end
          object EdOldControle: TdmkEdit
            Left = 12
            Top = 32
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object TPOldData: TdmkEditDateTimePicker
            Left = 140
            Top = 32
            Width = 112
            Height = 21
            Date = 40568.480364108800000000
            Time = 40568.480364108800000000
            Color = clWhite
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object EdOldSub: TdmkEdit
            Left = 76
            Top = 32
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdOldTipo: TdmkEdit
            Left = 256
            Top = 32
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdOldCarteira: TdmkEdit
            Left = 320
            Top = 32
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object RGFisicoSrc: TdmkRadioGroup
          Left = 400
          Top = 0
          Width = 401
          Height = 61
          Caption = ' Fonte f'#237'sica do documento:  '
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            
              'MyObjects.ConfiguraRadioGroup(RGFisicoSrc, CO_TAB_INDX_CB4_DESCR' +
              'EVE')
          TabOrder = 1
          QryCampo = 'FisicoSrc'
          UpdCampo = 'FisicoSrc'
          UpdType = utYes
          OldValor = 0
        end
        object EdFisicoCod: TdmkEdit
          Left = 812
          Top = 32
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FisicoCod'
          UpdCampo = 'FisicoCod'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Edi'#231#227'o 2'
      ImageIndex = 5
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 589
        Align = alClient
        ParentBackground = False
        TabOrder = 0
        object Panel12: TPanel
          Left = 1
          Top = 1
          Width = 998
          Height = 48
          Align = alTop
          TabOrder = 0
          object LaCliInt: TLabel
            Left = 8
            Top = 6
            Width = 60
            Height = 13
            Caption = 'Condom'#237'nio:'
          end
          object dmkEdCBCliInt: TdmkEditCB
            Left = 8
            Top = 22
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CliInt'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = dmkEdCBCliIntChange
            DBLookupComboBox = dmkCBCliInt
            IgnoraDBLookupComboBox = False
          end
          object dmkCBCliInt: TdmkDBLookupComboBox
            Left = 68
            Top = 22
            Width = 493
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsCliInt
            TabOrder = 1
            dmkEditCB = dmkEdCBCliInt
            QryCampo = 'CliInt'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object BtReabreLct: TBitBtn
            Tag = 20
            Left = 600
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Reabre Lct'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtReabreLctClick
          end
        end
        object PnDepto: TPanel
          Left = 1
          Top = 49
          Width = 998
          Height = 39
          Align = alTop
          TabOrder = 1
          Visible = False
          object LaDepto: TLabel
            Left = 8
            Top = 2
            Width = 177
            Height = 13
            Caption = 'Unidade habitacional do condom'#237'nio:'
          end
          object dmkCBDepto: TdmkDBLookupComboBox
            Left = 68
            Top = 18
            Width = 493
            Height = 21
            KeyField = 'CODI_1'
            ListField = 'NOME_1'
            ListSource = DsDeptos
            TabOrder = 1
            dmkEditCB = dmkEdCBDepto
            QryCampo = 'Depto'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object dmkEdCBDepto: TdmkEditCB
            Left = 8
            Top = 18
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Depto'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = dmkCBDepto
            IgnoraDBLookupComboBox = False
          end
        end
        object PnForneceI: TPanel
          Left = 1
          Top = 88
          Width = 998
          Height = 37
          Align = alTop
          TabOrder = 2
          Visible = False
          object LaForneceI: TLabel
            Left = 8
            Top = 2
            Width = 172
            Height = 13
            Caption = 'Propriet'#225'rio da unidade habitacional:'
          end
          object dmkCBForneceI: TdmkDBLookupComboBox
            Left = 68
            Top = 16
            Width = 493
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsForneceI
            TabOrder = 1
            dmkEditCB = dmkEdCBForneceI
            QryCampo = 'ForneceI'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object dmkEdCBForneceI: TdmkEditCB
            Left = 8
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ForneceI'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = dmkCBForneceI
            IgnoraDBLookupComboBox = False
          end
        end
        object PnAccount: TPanel
          Left = 1
          Top = 125
          Width = 998
          Height = 39
          Align = alTop
          TabOrder = 3
          Visible = False
          object LaAccount: TLabel
            Left = 8
            Top = 2
            Width = 43
            Height = 13
            Caption = 'Account:'
          end
          object dmkEdCBAccount: TdmkEditCB
            Left = 8
            Top = 18
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Depto'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnKeyDown = dmkEdCBAccountKeyDown
            DBLookupComboBox = dmkCBAccount
            IgnoraDBLookupComboBox = False
          end
          object dmkCBAccount: TdmkDBLookupComboBox
            Left = 68
            Top = 18
            Width = 493
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsAccounts
            TabOrder = 1
            OnKeyDown = dmkDBLookupComboBox1KeyDown
            dmkEditCB = dmkEdCBAccount
            QryCampo = 'Depto'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object PnLancto2: TPanel
          Left = 1
          Top = 164
          Width = 998
          Height = 121
          Align = alTop
          TabOrder = 4
          object LaEventosCad: TLabel
            Left = 8
            Top = 41
            Width = 37
            Height = 13
            Caption = 'Evento:'
          end
          object SbEventosCad: TSpeedButton
            Left = 540
            Top = 57
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbEventosCadClick
          end
          object Label60: TLabel
            Left = 8
            Top = 81
            Width = 121
            Height = 13
            Caption = 'Indica'#231#227'o de pagamento:'
          end
          object SbIndiPag: TSpeedButton
            Left = 540
            Top = 97
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbIndiPagClick
          end
          object EdEventosCad: TdmkEditCB
            Left = 8
            Top = 57
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEventosCad
            IgnoraDBLookupComboBox = False
          end
          object CBEventosCad: TdmkDBLookupComboBox
            Left = 68
            Top = 57
            Width = 472
            Height = 21
            Color = clWhite
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsEventosCad
            TabOrder = 1
            dmkEditCB = EdEventosCad
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdIndiPag: TdmkEditCB
            Left = 8
            Top = 97
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'IndiPag'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBIndiPag
            IgnoraDBLookupComboBox = False
          end
          object CBIndiPag: TdmkDBLookupComboBox
            Left = 68
            Top = 97
            Width = 472
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsIndiPag
            TabOrder = 3
            dmkEditCB = EdIndiPag
            QryCampo = 'IndiPag'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object Panel22: TPanel
          Left = 1
          Top = 285
          Width = 998
          Height = 303
          Align = alClient
          TabOrder = 5
          object Panel23: TPanel
            Left = 1
            Top = 1
            Width = 996
            Height = 284
            Align = alTop
            TabOrder = 0
            object Label14: TLabel
              Left = 8
              Top = 2
              Width = 62
              Height = 13
              Caption = 'Lan'#231'amento:'
            end
            object Label15: TLabel
              Left = 88
              Top = 2
              Width = 256
              Height = 13
              Caption = 'Carteira (Extrato banc'#225'rio, emiss'#227'o banc'#225'ria ou caixa):'
            end
            object SbCarteira: TSpeedButton
              Left = 528
              Top = 18
              Width = 21
              Height = 21
              Hint = 'Inclui item de carteira'
              Caption = '...'
              OnClick = SbCarteiraClick
            end
            object Label1: TLabel
              Left = 8
              Top = 42
              Width = 99
              Height = 13
              Caption = 'Data do lan'#231'amento:'
            end
            object LaCliente: TLabel
              Left = 8
              Top = 82
              Width = 35
              Height = 13
              Caption = 'Cliente:'
            end
            object SpeedButton1: TSpeedButton
              Left = 520
              Top = 98
              Width = 22
              Height = 21
              Hint = 'Inclui conta'
              Caption = '...'
              OnClick = SpeedButton1Click
            end
            object LaFornecedor: TLabel
              Left = 8
              Top = 122
              Width = 57
              Height = 13
              Caption = 'Fornecedor:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object LaForneceRN: TLabel
              Left = 64
              Top = 122
              Width = 85
              Height = 13
              Caption = '[F5] Raz'#227'o/Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object LaForneceFA: TLabel
              Left = 156
              Top = 122
              Width = 101
              Height = 13
              Caption = '[F6] Fantasia/Apelido'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object SpeedButton2: TSpeedButton
              Left = 520
              Top = 138
              Width = 22
              Height = 21
              Hint = 'Inclui conta'
              Caption = '...'
              OnClick = SpeedButton2Click
            end
            object Label11: TLabel
              Left = 372
              Top = 162
              Width = 48
              Height = 13
              Caption = 'Duplicata:'
            end
            object Label20: TLabel
              Left = 8
              Top = 162
              Width = 58
              Height = 13
              Caption = 'Quantidade:'
            end
            object LaNF: TLabel
              Left = 308
              Top = 162
              Width = 23
              Height = 13
              Caption = 'N.F.:'
            end
            object dmkEdControle: TdmkEdit
              Left = 8
              Top = 18
              Width = 76
              Height = 21
              Alignment = taRightJustify
              Color = clInactiveCaption
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBackground
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Controle'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object dmkEdCarteira: TdmkEditCB
              Left = 88
              Top = 18
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Carteira'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = dmkEdCarteiraChange
              DBLookupComboBox = dmkCBCarteira
              IgnoraDBLookupComboBox = False
            end
            object dmkCBCarteira: TdmkDBLookupComboBox
              Left = 148
              Top = 18
              Width = 381
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCarteiras
              TabOrder = 2
              dmkEditCB = dmkEdCarteira
              QryCampo = 'Carteira'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkEdTPData: TdmkEditDateTimePicker
              Left = 8
              Top = 58
              Width = 106
              Height = 21
              Date = 39615.655720300900000000
              Time = 39615.655720300900000000
              TabOrder = 3
              OnClick = dmkEdTPDataClick
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'Data'
              UpdType = utYes
            end
            object dmkEdCBCliente: TdmkEditCB
              Left = 8
              Top = 98
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Cliente'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = dmkCBCliente
              IgnoraDBLookupComboBox = False
            end
            object dmkCBCliente: TdmkDBLookupComboBox
              Left = 64
              Top = 98
              Width = 456
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsClientes
              TabOrder = 5
              dmkEditCB = dmkEdCBCliente
              QryCampo = 'Cliente'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkEdCBFornece: TdmkEditCB
              Left = 8
              Top = 138
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Fornecedor'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnKeyDown = dmkCBForneceKeyDown
              DBLookupComboBox = dmkCBFornece
              IgnoraDBLookupComboBox = False
            end
            object dmkCBFornece: TdmkDBLookupComboBox
              Left = 64
              Top = 138
              Width = 456
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsFornecedores
              TabOrder = 7
              OnKeyDown = dmkCBForneceKeyDown
              dmkEditCB = dmkEdCBFornece
              QryCampo = 'Fornecedor'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkEdDuplicata: TdmkEdit
              Left = 372
              Top = 178
              Width = 65
              Height = 21
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Duplicata'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnExit = dmkEdDuplicataExit
            end
            object dmkEdQtde: TdmkEdit
              Left = 8
              Top = 177
              Width = 71
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdNF: TdmkEdit
              Left = 308
              Top = 178
              Width = 60
              Height = 21
              Alignment = taRightJustify
              TabOrder = 10
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000000'
              QryCampo = 'NotaFiscal'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object CkCancelado: TdmkCheckBox
              Left = 8
              Top = 202
              Width = 441
              Height = 17
              TabStop = False
              Caption = 
                'Lan'#231'amento cancelado (cr'#233'dito e d'#233'bito ser'#227'o zerados e status se' +
                'r'#225' igual a cancelado).'
              TabOrder = 11
              WordWrap = True
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 292
        Height = 32
        Caption = 'Edi'#231#227'o de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 292
        Height = 32
        Caption = 'Edi'#231#227'o de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 292
        Height = 32
        Caption = 'Edi'#231#227'o de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 96
    Top = 284
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 96
    Top = 256
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 40
    Top = 256
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 156
    Top = 256
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrContasAfterOpen
    BeforeClose = QrContasBeforeClose
    AfterScroll = QrContasAfterScroll
    SQL.Strings = (
      'SELECT co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 68
    Top = 284
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContasUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContasNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContasNOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrContasNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCarteirasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, '
      'Tipo, ExigeNumCheque, ForneceI, Banco1, UsaTalao'
      'FROM carteiras '
      'WHERE ForneceI=:P0'
      'ORDER BY Nome')
    Left = 68
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCarteirasUsaTalao: TSmallintField
      FieldName = 'UsaTalao'
    end
  end
  object QrFatura: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Data) Data '
      'FROM faturas'
      'WHERE Emissao=:P0')
    Left = 508
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFaturaData: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'Data'
      Required = True
    end
  end
  object QrFornecedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'OR Terceiro="V"'
      'ORDER BY NomeENTIDADE')
    Left = 12
    Top = 256
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFornecedoresNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrVendedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NomeENTIDADE')
    Left = 12
    Top = 312
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 40
    Top = 312
  end
  object DsAccounts: TDataSource
    DataSet = QrAccounts
    Left = 96
    Top = 228
  end
  object QrAccounts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  '
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NomeENTIDADE')
    Left = 68
    Top = 228
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField2: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NomeENTIDADE')
    Left = 12
    Top = 284
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFunciNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFunci: TDataSource
    DataSet = QrFunci
    Left = 40
    Top = 284
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCliIntAfterScroll
    SQL.Strings = (
      'SELECT eci.CtaCfgCab, ent.Codigo, ent.Account,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'LEFT JOIN enticliint eci ON eci.CodEnti=ent.Codigo'
      'WHERE ent.CliInt <> 0'
      'ORDER BY NomeENTIDADE'
      '')
    Left = 128
    Top = 228
    object QrCliIntCtaCfgCab: TIntegerField
      FieldName = 'CtaCfgCab'
    end
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrCliIntNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 156
    Top = 228
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, '
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR,'
      'ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA'
      'FROM VAR LCT la'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'ORDER BY la.Data, la.Controle')
    Left = 548
    Top = 48
    object QrLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 25
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctCONTA: TIntegerField
      FieldName = 'CONTA'
    end
    object QrLctNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      FixedChar = True
      Size = 128
    end
    object QrLctNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 128
    end
    object QrLctNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 4
    end
    object QrLctNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 4
    end
    object QrLctNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 4
    end
    object QrLctNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctcliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLctUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLctUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLctCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrLctSALDOCARTEIRA: TFloatField
      FieldName = 'SALDOCARTEIRA'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 576
    Top = 48
  end
  object DsDeptos: TDataSource
    DataSet = QrDeptos
    Left = 156
    Top = 284
  end
  object QrDeptos: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrDeptosAfterScroll
    SQL.Strings = (
      'SELECT Codigo CODI_1, FLOOR(0) CODI_2, Nome NOME_1'
      'FROM intentocad'
      'ORDER BY NOME_1')
    Left = 128
    Top = 284
    object QrDeptosCODI_1: TIntegerField
      FieldName = 'CODI_1'
      Required = True
    end
    object QrDeptosCODI_2: TLargeintField
      FieldName = 'CODI_2'
      Required = True
    end
    object QrDeptosNOME_1: TWideStringField
      FieldName = 'NOME_1'
      Required = True
      Size = 100
    end
  end
  object QrForneceI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY NomeENTIDADE')
    Left = 12
    Top = 228
    object QrForneceICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceINOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsForneceI: TDataSource
    DataSet = QrForneceI
    Left = 40
    Top = 228
  end
  object TbParcpagtos: TmySQLTable
    Database = DModG.MyPID_DB
    TableName = '_parcpagtos_'
    Left = 192
    Top = 228
    object TbParcpagtosParcela: TIntegerField
      FieldName = 'Parcela'
    end
    object TbParcpagtosData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbParcpagtosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosDoc: TLargeintField
      FieldName = 'Doc'
    end
    object TbParcpagtosMora: TFloatField
      FieldName = 'Mora'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosMulta: TFloatField
      FieldName = 'Multa'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 30
    end
    object TbParcpagtosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object TbParcpagtosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object TbParcpagtosOrdIns: TIntegerField
      FieldName = 'OrdIns'
    end
  end
  object DsParcPagtos: TDataSource
    DataSet = TbParcpagtos
    Left = 220
    Top = 228
  end
  object QrSoma: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT SUM(Valor) VALOR'
      'FROM parcpagtos')
    Left = 192
    Top = 256
    object QrSomaVALOR: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'VALOR'
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'OR Terceiro="V"'
      'ORDER BY NomeENTIDADE')
    Left = 128
    Top = 256
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrEventosCad: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrContasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM eventoscad'
      'WHERE Ativo=1'
      'AND Entidade=:P0'
      'ORDER BY Nome')
    Left = 68
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEventosCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEventosCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEventosCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsEventosCad: TDataSource
    DataSet = QrEventosCad
    Left = 96
    Top = 312
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdEventosCad
    Panel = Panel2
    QryCampo = 'EventosCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 12
    Top = 4
  end
  object QrLocMes: TmySQLQuery
    Database = Dmod.MyDB
    Left = 192
    Top = 284
    object QrLocMesMEZ: TIntegerField
      FieldName = 'MEZ'
    end
  end
  object QrIndiPag: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM indipag'
      'ORDER BY Nome'
      '')
    Left = 128
    Top = 312
    object QrIndiPagCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIndiPagNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsIndiPag: TDataSource
    DataSet = QrIndiPag
    Left = 156
    Top = 312
  end
  object QrPsqCta: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPsqCtaAfterOpen
    BeforeClose = QrPsqCtaBeforeClose
    SQL.Strings = (
      'SELECT co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 192
    Top = 312
    object QrPsqCtaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPsqCtaNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPsqCtaNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrPsqCtaNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 50
    end
    object QrPsqCtaID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrPsqCtaSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
    end
    object QrPsqCtaCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object QrPsqCtaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPsqCtaCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrPsqCtaDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrPsqCtaMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrPsqCtaExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrPsqCtaMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrPsqCtaMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrPsqCtaMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrPsqCtaMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrPsqCtaMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrPsqCtaTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrPsqCtaExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrPsqCtaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPsqCtaRateio: TIntegerField
      FieldName = 'Rateio'
    end
    object QrPsqCtaEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrPsqCtaAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrPsqCtaPendenMesSeg: TSmallintField
      FieldName = 'PendenMesSeg'
    end
    object QrPsqCtaCalculMesSeg: TSmallintField
      FieldName = 'CalculMesSeg'
    end
    object QrPsqCtaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPsqCtaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPsqCtaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPsqCtaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPsqCtaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPsqCtaOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object QrPsqCtaContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object QrPsqCtaContasSum: TIntegerField
      FieldName = 'ContasSum'
    end
    object QrPsqCtaCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
    object QrPsqCtaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPsqCtaNotPrntBal: TIntegerField
      FieldName = 'NotPrntBal'
    end
    object QrPsqCtaSigla: TWideStringField
      FieldName = 'Sigla'
    end
    object QrPsqCtaProvRat: TSmallintField
      FieldName = 'ProvRat'
    end
    object QrPsqCtaNotPrntFin: TIntegerField
      FieldName = 'NotPrntFin'
    end
    object QrPsqCtaNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrPsqCtaNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrPsqCtaNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 50
    end
    object QrPsqCtaNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 50
    end
    object QrPsqCtaNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
  end
  object DsPsqCta: TDataSource
    DataSet = QrPsqCta
    Left = 220
    Top = 312
  end
  object QrPContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas'
      'ORDER BY Nome')
    Left = 256
    Top = 228
  end
  object DsPContas: TDataSource
    DataSet = QrPContas
    Left = 284
    Top = 228
  end
  object QrPCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 256
    Top = 256
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsPCliInt: TDataSource
    DataSet = QrPCliInt
    Left = 284
    Top = 256
  end
  object QrPFornece: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 256
    Top = 284
    object QrPForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPForneceNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsPFornece: TDataSource
    DataSet = QrPFornece
    Left = 284
    Top = 284
  end
  object QrPCliente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 256
    Top = 312
    object QrPClienteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPClienteNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsPCliente: TDataSource
    DataSet = QrPCliente
    Left = 284
    Top = 312
  end
  object QrMulInsLct: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrMulInsLctAfterOpen
    SQL.Strings = (
      'SELECT * '
      'FROM _mulinslct_'
      'ORDER BY OrdIns')
    Left = 208
    Top = 472
    object QrMulInsLctGeneroCod: TIntegerField
      FieldName = 'GeneroCod'
    end
    object QrMulInsLctGeneroNom: TWideStringField
      FieldName = 'GeneroNom'
      Size = 50
    end
    object QrMulInsLctValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrMulInsLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrMulInsLctOrdIns: TIntegerField
      FieldName = 'OrdIns'
    end
    object QrMulInsLctAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMulInsLctMes: TWideStringField
      FieldName = 'Mes'
      Size = 7
    end
  end
  object DsMulInsLct: TDataSource
    DataSet = QrMulInsLct
    Left = 236
    Top = 472
  end
end
