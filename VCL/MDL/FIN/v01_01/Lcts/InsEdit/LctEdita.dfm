object FmLctEdita: TFmLctEdita
  Left = 339
  Top = 185
  Caption = 'FIN-LANCT-005 :: Edi'#231#227'o de Lan'#231'amentos [A]'
  ClientHeight = 748
  ClientWidth = 1187
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 55
    Width = 1187
    Height = 624
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PainelDados: TPanel
      Left = 0
      Top = 0
      Width = 1187
      Height = 517
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object LaFunci: TLabel
        Left = 651
        Top = 5
        Width = 6
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '0'
        Visible = False
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 751
        Height = 517
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object PnLct1: TPanel
          Left = 0
          Top = 0
          Width = 751
          Height = 210
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 0
          object Label14: TLabel
            Left = 11
            Top = 2
            Width = 62
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Lan'#231'amento:'
          end
          object SbCarteira: TSpeedButton
            Left = 445
            Top = 18
            Width = 23
            Height = 23
            Hint = 'Inclui item de carteira'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SbCarteiraClick
          end
          object BtContas: TSpeedButton
            Left = 721
            Top = 58
            Width = 23
            Height = 23
            Hint = 'Inclui conta'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = BtContasClick
          end
          object Label1: TLabel
            Left = 11
            Top = 43
            Width = 99
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data do lan'#231'amento:'
          end
          object Label2: TLabel
            Left = 125
            Top = 43
            Width = 97
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Conta [F7] pesquisa:'
          end
          object LaMes: TLabel
            Left = 11
            Top = 125
            Width = 23
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'M'#234's:'
            Enabled = False
          end
          object LaDeb: TLabel
            Left = 85
            Top = 125
            Width = 34
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'D'#233'bito:'
            Enabled = False
          end
          object LaCred: TLabel
            Left = 186
            Top = 125
            Width = 36
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cr'#233'dito:'
            Enabled = False
          end
          object LaNF: TLabel
            Left = 285
            Top = 125
            Width = 23
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'N.F.:'
          end
          object LaDoc: TLabel
            Left = 359
            Top = 125
            Width = 104
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'S'#233'rie  e docum. (CH) :'
          end
          object Label11: TLabel
            Left = 500
            Top = 125
            Width = 48
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Duplicata:'
          end
          object LaVencimento: TLabel
            Left = 590
            Top = 125
            Width = 80
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vencimento [F6]:'
          end
          object LaCliInt: TLabel
            Left = 476
            Top = 2
            Width = 60
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Condom'#237'nio:'
            Enabled = False
          end
          object LaCliente: TLabel
            Left = 11
            Top = 165
            Width = 35
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cliente:'
          end
          object LaFornecedor: TLabel
            Left = 383
            Top = 165
            Width = 57
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fornecedor:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object SpeedButton2: TSpeedButton
            Left = 719
            Top = 181
            Width = 23
            Height = 23
            Hint = 'Inclui conta'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object LaForneceRN: TLabel
            Left = 462
            Top = 165
            Width = 85
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '[F5] Raz'#227'o/Nome'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object LaForneceFA: TLabel
            Left = 582
            Top = 165
            Width = 101
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '[F6] Fantasia/Apelido'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label15: TLabel
            Left = 126
            Top = 2
            Width = 256
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Carteira (Extrato banc'#225'rio, emiss'#227'o banc'#225'ria ou caixa):'
          end
          object Label5: TLabel
            Left = 125
            Top = 83
            Width = 97
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Conta [F7] pesquisa:'
          end
          object SpeedButton1: TSpeedButton
            Left = 721
            Top = 98
            Width = 23
            Height = 23
            Hint = 'Inclui conta'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = BtContasClick
          end
          object SbPsqGCFin: TSpeedButton
            Left = 696
            Top = 58
            Width = 23
            Height = 23
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'GC'
            OnClick = SbPsqGCFinClick
          end
          object SbPsqGCCtb: TSpeedButton
            Left = 696
            Top = 99
            Width = 23
            Height = 23
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'GC'
            OnClick = SbPsqGCCtbClick
          end
          object EdControle: TdmkEdit
            Left = 11
            Top = 19
            Width = 79
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Color = clInactiveCaption
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBackground
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utIdx
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCarteira: TdmkEditCB
            Left = 126
            Top = 19
            Width = 55
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Carteira'
            UpdCampo = 'Carteira'
            UpdType = utIdx
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCarteiraChange
            DBLookupComboBox = CBCarteira
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCarteira: TdmkDBLookupComboBox
            Left = 185
            Top = 19
            Width = 260
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCarteiras
            TabOrder = 4
            dmkEditCB = EdCarteira
            QryCampo = 'Carteira'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CBGenero: TdmkDBLookupComboBox
            Left = 300
            Top = 59
            Width = 394
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContas1
            TabOrder = 10
            OnClick = CBGeneroClick
            OnKeyDown = CBGeneroKeyDown
            dmkEditCB = EdCBGenero
            QryCampo = 'Genero'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCBGenero: TdmkEditCB
            Left = 245
            Top = 59
            Width = 55
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Genero'
            UpdCampo = 'Genero'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCBGeneroChange
            OnKeyDown = EdCBGeneroKeyDown
            OnRedefinido = EdCBGeneroRedefinido
            DBLookupComboBox = CBGenero
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdTPData: TdmkEditDateTimePicker
            Left = 11
            Top = 59
            Width = 112
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 40569.000000000000000000
            Time = 0.655720300899702100
            TabOrder = 7
            OnClick = EdTPDataClick
            OnChange = EdTPDataChange
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'Data'
            UpdCampo = 'Data'
            UpdType = utIdx
            DatePurpose = dmkdpNone
          end
          object EdMes: TdmkEdit
            Left = 11
            Top = 142
            Width = 70
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taCenter
            Enabled = False
            TabOrder = 14
            FormatType = dmktfMesAno
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfLong
            HoraFormat = dmkhfShort
            QryCampo = 'Mez'
            UpdCampo = 'Mez'
            UpdType = utYes
            Obrigatorio = True
            PermiteNulo = False
            ValueVariant = Null
            ValWarn = False
            OnExit = EdMesExit
          end
          object EdDeb: TdmkEdit
            Left = 85
            Top = 142
            Width = 96
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 15
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'Debito'
            UpdCampo = 'Debito'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdDebExit
          end
          object EdCred: TdmkEdit
            Left = 186
            Top = 142
            Width = 95
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 16
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'Credito'
            UpdCampo = 'Credito'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnExit = EdCredExit
          end
          object EdNF: TdmkEdit
            Left = 285
            Top = 142
            Width = 73
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 17
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            QryCampo = 'NotaFiscal'
            UpdCampo = 'NotaFiscal'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdSerieCH: TdmkEdit
            Left = 363
            Top = 142
            Width = 53
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            CharCase = ecUpperCase
            TabOrder = 18
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SerieCH'
            UpdCampo = 'SerieCH'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdDoc: TdmkEdit
            Left = 415
            Top = 142
            Width = 81
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 19
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            QryCampo = 'Documento'
            UpdCampo = 'Documento'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdDocChange
            OnExit = EdDocExit
          end
          object EdDuplicata: TdmkEdit
            Left = 500
            Top = 142
            Width = 86
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 20
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Duplicata'
            UpdCampo = 'Duplicata'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdTPVencto: TdmkEditDateTimePicker
            Left = 590
            Top = 142
            Width = 152
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 40569.000000000000000000
            Time = 0.672523148103209700
            TabOrder = 21
            OnClick = EdTPVenctoClick
            OnChange = EdTPVenctoChange
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'Vencimento'
            UpdCampo = 'Vencimento'
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object EdCliInt: TdmkEditCB
            Left = 476
            Top = 19
            Width = 55
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CliInt'
            UpdCampo = 'CliInt'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCliIntChange
            DBLookupComboBox = CBCliInt
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCliInt: TdmkDBLookupComboBox
            Left = 533
            Top = 19
            Width = 208
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Enabled = False
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsCliInt
            TabOrder = 6
            dmkEditCB = EdCliInt
            QryCampo = 'CliInt'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCBCliente: TdmkEditCB
            Left = 11
            Top = 182
            Width = 73
            Height = 23
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 22
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cliente'
            UpdCampo = 'Cliente'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 85
            Top = 182
            Width = 290
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsClientes
            TabOrder = 23
            dmkEditCB = EdCBCliente
            QryCampo = 'Cliente'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCBFornece: TdmkEditCB
            Left = 383
            Top = 182
            Width = 74
            Height = 23
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 24
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Fornecedor'
            UpdCampo = 'Fornecedor'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBFornece
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFornece: TdmkDBLookupComboBox
            Left = 462
            Top = 182
            Width = 255
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsFornecedores
            TabOrder = 25
            dmkEditCB = EdCBFornece
            QryCampo = 'Fornecedor'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdSub: TdmkEdit
            Left = 90
            Top = 19
            Width = 17
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Color = clMenu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Sub'
            UpdCampo = 'Sub'
            UpdType = utIdx
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OldValor = '0'
            ValWarn = False
          end
          object EdTipo: TdmkEdit
            Left = 110
            Top = 19
            Width = 17
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Color = clMenu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Tipo'
            UpdCampo = 'Tipo'
            UpdType = utIdx
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OldValor = '0'
            ValWarn = False
          end
          object DBNiveis1: TDBEdit
            Left = 125
            Top = 59
            Width = 120
            Height = 21
            TabStop = False
            DataField = 'Niveis'
            DataSource = DsContas1
            TabOrder = 8
          end
          object DBNiveis2: TDBEdit
            Left = 125
            Top = 99
            Width = 120
            Height = 21
            TabStop = False
            DataField = 'Niveis'
            DataSource = DsContas2
            TabOrder = 11
          end
          object EdGenCtb: TdmkEditCB
            Left = 245
            Top = 99
            Width = 55
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'GenCtb'
            UpdCampo = 'GenCtb'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdGenCtbRedefinido
            DBLookupComboBox = CBGenCtb
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGenCtb: TdmkDBLookupComboBox
            Left = 300
            Top = 99
            Width = 394
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContas1
            TabOrder = 13
            dmkEditCB = EdGenCtb
            QryCampo = 'GenCtb'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object PnDepto: TPanel
          Left = 0
          Top = 210
          Width = 751
          Height = 45
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 1
          Visible = False
          object LaDepto: TLabel
            Left = 11
            Top = 2
            Width = 177
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Unidade habitacional do condom'#237'nio:'
          end
          object CBDepto: TdmkDBLookupComboBox
            Left = 90
            Top = 19
            Width = 650
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'CODI_1'
            ListField = 'NOME_1'
            ListSource = DsDeptos
            TabOrder = 1
            dmkEditCB = EdCBDepto
            QryCampo = 'Depto'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCBDepto: TdmkEditCB
            Left = 11
            Top = 19
            Width = 73
            Height = 23
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Depto'
            UpdCampo = 'Depto'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBDepto
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
        end
        object PnForneceI: TPanel
          Left = 0
          Top = 255
          Width = 751
          Height = 43
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 2
          Visible = False
          object LaForneceI: TLabel
            Left = 11
            Top = 2
            Width = 172
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Propriet'#225'rio da unidade habitacional:'
          end
          object CBForneceI: TdmkDBLookupComboBox
            Left = 90
            Top = 17
            Width = 650
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsForneceI
            TabOrder = 1
            dmkEditCB = EdCBForneceI
            QryCampo = 'ForneceI'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCBForneceI: TdmkEditCB
            Left = 11
            Top = 17
            Width = 73
            Height = 22
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ForneceI'
            UpdCampo = 'ForneceI'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBForneceI
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
        end
        object PnAccount: TPanel
          Left = 0
          Top = 298
          Width = 751
          Height = 44
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 3
          Visible = False
          object LaAccount: TLabel
            Left = 11
            Top = 2
            Width = 43
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Account:'
          end
          object EdCBAccount: TdmkEditCB
            Left = 11
            Top = 19
            Width = 73
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Account'
            UpdCampo = 'Account'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnKeyDown = EdCBAccountKeyDown
            DBLookupComboBox = CBAccount
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBAccount: TdmkDBLookupComboBox
            Left = 90
            Top = 19
            Width = 650
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsAccounts
            TabOrder = 1
            OnKeyDown = CBAccountKeyDown
            dmkEditCB = EdCBAccount
            QryCampo = 'Account'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object PnLct2: TPanel
          Left = 0
          Top = 387
          Width = 751
          Height = 46
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabOrder = 5
          object Label13: TLabel
            Left = 207
            Top = 2
            Width = 144
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o [F4], [F5], [F6], [F8]:'
          end
          object Label20: TLabel
            Left = 11
            Top = 2
            Width = 67
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade 1:'
          end
          object Label3: TLabel
            Left = 109
            Top = 2
            Width = 67
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade 2:'
          end
          object EdDescricao: TdmkEdit
            Left = 207
            Top = 18
            Width = 532
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Descricao'
            UpdCampo = 'Descricao'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnKeyDown = EdDescricaoKeyDown
          end
          object EdQtde: TdmkEdit
            Left = 11
            Top = 18
            Width = 92
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Qtde'
            UpdCampo = 'Qtde'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object dmkEdit1: TdmkEdit
            Left = 109
            Top = 18
            Width = 93
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Qtd2'
            UpdCampo = 'Qtd2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object Panel8: TPanel
          Left = 0
          Top = 433
          Width = 751
          Height = 84
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 6
          object PnLct3: TPanel
            Left = 0
            Top = 0
            Width = 531
            Height = 84
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            TabOrder = 0
            object CkPesqNF: TCheckBox
              Left = 11
              Top = 5
              Width = 263
              Height = 23
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Pesquisar duplica'#231#227'o de nota fiscal.'
              Checked = True
              State = cbChecked
              TabOrder = 0
            end
            object CkPesqCH: TCheckBox
              Left = 290
              Top = 5
              Width = 232
              Height = 23
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Pesquisar duplica'#231#227'o de cheque.'
              Checked = True
              State = cbChecked
              TabOrder = 1
            end
            object CkCancelado: TdmkCheckBox
              Left = 11
              Top = 55
              Width = 484
              Height = 23
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 
                'Lan'#231'amento cancelado (cr'#233'dito e d'#233'bito ficam = 0 e status = canc' +
                'elado).'
              TabOrder = 3
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
            object CkPesqVal: TCheckBox
              Left = 11
              Top = 30
              Width = 516
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 
                'Pesquisa lan'#231'amentos cruzando o valor com a conta e o m'#234's de com' +
                'pet'#234'ncia.'
              Checked = True
              State = cbChecked
              TabOrder = 2
            end
          end
          object Panel9: TPanel
            Left = 531
            Top = 0
            Width = 220
            Height = 84
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object LaDataDoc: TLabel
              Left = 11
              Top = 0
              Width = 82
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data documento:'
            end
            object Label4: TLabel
              Left = 11
              Top = 43
              Width = 95
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Vencimento original:'
            end
            object EdTPDataDoc: TdmkEditDateTimePicker
              Left = 11
              Top = 17
              Width = 147
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 39615.000000000000000000
              Time = 0.655720300899702100
              TabOrder = 0
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DataDoc'
              UpdCampo = 'DataDoc'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdTPVctoOriginal: TdmkEditDateTimePicker
              Left = 11
              Top = 60
              Width = 147
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Date = 39615.000000000000000000
              Time = 0.655720300899702100
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'VctoOriginal'
              UpdCampo = 'VctoOriginal'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
          end
        end
        object PnCentroCusto: TPanel
          Left = 0
          Top = 342
          Width = 751
          Height = 45
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 4
          Visible = False
          object Label63: TLabel
            Left = 11
            Top = 2
            Width = 78
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Centro de custo:'
          end
          object dmkEdCBCentroCusto: TdmkEditCB
            Left = 11
            Top = 19
            Width = 73
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CentroCusto'
            UpdCampo = 'CentroCusto'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = dmkCBCentroCusto
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object dmkCBCentroCusto: TdmkDBLookupComboBox
            Left = 90
            Top = 19
            Width = 650
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCentroCusto
            TabOrder = 1
            dmkEditCB = dmkEdCBCentroCusto
            QryCampo = 'CentroCusto'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
      object GroupBox1: TGroupBox
        Left = 751
        Top = 0
        Width = 436
        Height = 517
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = ' Pesquisa: '
        TabOrder = 1
        object PnMaskPesq: TPanel
          Left = 2
          Top = 15
          Width = 432
          Height = 500
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object PnLink: TPanel
            Left = 0
            Top = 0
            Width = 432
            Height = 27
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvLowered
            Caption = 'Sem link'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            object Panel6: TPanel
              Left = 407
              Top = 1
              Width = 24
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 0
              object SpeedButton3: TSpeedButton
                Left = 0
                Top = 0
                Width = 23
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'x'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Layout = blGlyphBottom
                ParentFont = False
                Visible = False
              end
            end
          end
          object Panel11: TPanel
            Left = 0
            Top = 279
            Width = 432
            Height = 221
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object Label51: TLabel
              Left = 5
              Top = 1
              Width = 52
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Sub-grupo:'
              FocusControl = DBEdit1
            end
            object Label52: TLabel
              Left = 5
              Top = 41
              Width = 32
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Grupo:'
              FocusControl = DBEdit2
            end
            object Label53: TLabel
              Left = 5
              Top = 84
              Width = 45
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Conjunto:'
              FocusControl = DBEdit3
            end
            object Label54: TLabel
              Left = 5
              Top = 129
              Width = 30
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Plano:'
              FocusControl = DBEdit4
            end
            object DBEdit1: TDBEdit
              Left = 5
              Top = 18
              Width = 410
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NOMESUBGRUPO'
              DataSource = DsContas1
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 5
              Top = 58
              Width = 410
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NOMEGRUPO'
              DataSource = DsContas1
              TabOrder = 1
            end
            object DBEdit3: TDBEdit
              Left = 5
              Top = 101
              Width = 410
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NOMECONJUNTO'
              DataSource = DsContas1
              TabOrder = 2
            end
            object DBEdit4: TDBEdit
              Left = 5
              Top = 145
              Width = 410
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NOMEPLANO'
              DataSource = DsContas1
              TabOrder = 3
            end
          end
          object Panel5: TPanel
            Left = 0
            Top = 27
            Width = 432
            Height = 252
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object Panel12: TPanel
              Left = 0
              Top = 0
              Width = 432
              Height = 48
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvLowered
              TabOrder = 0
              object Label18: TLabel
                Left = 5
                Top = 5
                Width = 186
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Digite parte da descri'#231#227'o (min. 3 letras):'
              end
              object EdLinkMask: TEdit
                Left = 5
                Top = 22
                Width = 406
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                OnChange = EdLinkMaskChange
                OnKeyDown = EdLinkMaskKeyDown
              end
            end
            object LLBPesq: TDBLookupListBox
              Left = 0
              Top = 48
              Width = 432
              Height = 199
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsPesq
              TabOrder = 1
              OnDblClick = LLBPesqDblClick
              OnEnter = LLBPesqEnter
              OnExit = LLBPesqExit
              OnKeyDown = LLBPesqKeyDown
            end
          end
        end
      end
    end
    object MeConfig: TMemo
      Left = 0
      Top = 581
      Width = 1187
      Height = 43
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Enabled = False
      TabOrder = 1
    end
    object GBVenda: TGroupBox
      Left = 0
      Top = 517
      Width = 1187
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 2
      Visible = False
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 1183
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object LaICMS_P: TLabel
          Left = 11
          Top = 1
          Width = 37
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% ICMS'
          Visible = False
        end
        object LaICMS_V: TLabel
          Left = 147
          Top = 1
          Width = 56
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Valor ICMS:'
          Visible = False
        end
        object LaVendedor: TLabel
          Left = 656
          Top = 1
          Width = 49
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Vendedor:'
          Visible = False
        end
        object Label10: TLabel
          Left = 284
          Top = 1
          Width = 58
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Funcion'#225'rio:'
          Enabled = False
          Visible = False
        end
        object EdICMS_P: TdmkEdit
          Left = 11
          Top = 18
          Width = 132
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          Visible = False
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMS_P'
          UpdCampo = 'ICMS_P'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdICMS_PExit
          OnKeyDown = EdICMS_PKeyDown
        end
        object EdICMS_V: TdmkEdit
          Left = 147
          Top = 18
          Width = 132
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 1
          Visible = False
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMS_V'
          UpdCampo = 'ICMS_V'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdICMS_VExit
          OnKeyDown = EdICMS_VKeyDown
        end
        object EdCBVendedor: TdmkEditCB
          Left = 656
          Top = 18
          Width = 74
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          Visible = False
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Vendedor'
          UpdCampo = 'Vendedor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBVendedor
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBVendedor: TdmkDBLookupComboBox
          Left = 735
          Top = 18
          Width = 285
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsVendedores
          TabOrder = 3
          Visible = False
          dmkEditCB = EdCBVendedor
          QryCampo = 'Vendedor'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCBFunci: TdmkEditCB
          Left = 284
          Top = 18
          Width = 73
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          Visible = False
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBFunci
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBFunci: TdmkDBLookupComboBox
          Left = 363
          Top = 18
          Width = 284
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Enabled = False
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsFunci
          TabOrder = 5
          Visible = False
          dmkEditCB = EdCBFunci
          QryCampo = 'UserCad'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1187
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1124
      Top = 0
      Width = 63
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 11
        Top = 15
        Width = 34
        Height = 34
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 63
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 63
      Top = 0
      Width = 1061
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 10
        Top = 12
        Width = 355
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Edi'#231#227'o de Lan'#231'amentos [A]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 12
        Top = 15
        Width = 355
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Edi'#231#227'o de Lan'#231'amentos [A]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 11
        Top = 13
        Width = 355
        Height = 33
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Edi'#231#227'o de Lan'#231'amentos [A]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 679
    Width = 1187
    Height = 69
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 996
      Top = 15
      Width = 189
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 13
        Left = 16
        Top = 4
        Width = 157
        Height = 43
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 994
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 16
        Top = 4
        Width = 157
        Height = 43
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object GBAvisos1: TGroupBox
        Left = 213
        Top = 0
        Width = 781
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        Caption = ' Avisos: '
        TabOrder = 1
        object Panel2: TPanel
          Left = 2
          Top = 15
          Width = 777
          Height = 35
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1: TLabel
            Left = 17
            Top = 2
            Width = 150
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -18
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 16
            Top = 1
            Width = 150
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -18
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
      end
    end
  end
  object QrCliInt: TMySQLQuery
    BeforeScroll = QrCliIntBeforeScroll
    SQL.Strings = (
      'SELECT eci.CtaCfgCab, ent.Codigo, ent.Account,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'LEFT JOIN enticliint eci ON eci.CodEnti=ent.Codigo'
      'WHERE ent.CliInt <> 0'
      'ORDER BY NomeENTIDADE'
      '')
    Left = 624
    Top = 172
    object QrCliIntCtaCfgCab: TIntegerField
      FieldName = 'CtaCfgCab'
    end
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrCliIntNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 652
    Top = 172
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 652
    Top = 200
  end
  object QrClientes: TMySQLQuery
    AfterScroll = QrClientesAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'OR Terceiro="V"'
      'ORDER BY NomeENTIDADE')
    Left = 624
    Top = 200
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrDeptos: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo CODI_1, FLOOR(0) CODI_2, Nome NOME_1'
      'FROM intentocad'
      'ORDER BY NOME_1')
    Left = 624
    Top = 228
    object QrDeptosCODI_1: TIntegerField
      FieldName = 'CODI_1'
      Required = True
    end
    object QrDeptosCODI_2: TLargeintField
      FieldName = 'CODI_2'
      Required = True
    end
    object QrDeptosNOME_1: TWideStringField
      FieldName = 'NOME_1'
      Required = True
      Size = 100
    end
  end
  object DsDeptos: TDataSource
    DataSet = QrDeptos
    Left = 652
    Top = 228
  end
  object QrForneceI: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 272
    object QrForneceICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceINOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsForneceI: TDataSource
    DataSet = QrForneceI
    Left = 504
    Top = 272
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 504
    Top = 300
  end
  object QrFornecedores: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'OR Terceiro="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 300
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFornecedoresNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrFunci: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 328
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFunciNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFunci: TDataSource
    DataSet = QrFunci
    Left = 504
    Top = 328
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 504
    Top = 356
  end
  object QrVendedores: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 356
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrAccounts: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo,  '
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 384
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField2: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsAccounts: TDataSource
    DataSet = QrAccounts
    Left = 504
    Top = 384
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 504
    Top = 412
  end
  object QrCarteiras: TMySQLQuery
    AfterScroll = QrCarteirasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, '
      'Tipo, ExigeNumCheque, ForneceI, Banco1, UsaTalao'
      'FROM carteiras '
      'WHERE ForneceI=:P0'
      'AND Ativo = 1'
      'ORDER BY Nome')
    Left = 476
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCarteirasUsaTalao: TSmallintField
      FieldName = 'UsaTalao'
    end
  end
  object QrContas1: TMySQLQuery
    AfterScroll = QrContas1AfterScroll
    SQL.Strings = (
      'SELECT co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 476
    Top = 440
    object QrContas1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContas1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContas1Nome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContas1Nome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContas1ID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContas1Subgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContas1Empresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContas1Credito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContas1Debito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContas1Mensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContas1Exclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContas1Mensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContas1Mensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContas1Mensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContas1Menscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContas1Mensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContas1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContas1Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContas1Excel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContas1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContas1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContas1UserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContas1UserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContas1NOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContas1NOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContas1NOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContas1NOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrContas1NOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrContas1Niveis: TWideStringField
      FieldName = 'Niveis'
      Size = 60
    end
    object QrContas1Ordens: TWideStringField
      FieldName = 'Ordens'
      Size = 100
    end
  end
  object DsContas1: TDataSource
    DataSet = QrContas1
    Left = 504
    Top = 440
  end
  object QrPesq: TMySQLQuery
    SQL.Strings = (
      'SELECT co.Codigo, co.Nome, '
      'pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Nome LIKE :P0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 624
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrPesqNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrPesqNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrPesqNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 50
    end
    object QrPesqNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 50
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 652
    Top = 144
  end
  object QrCentroCusto: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM centrocusto'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 636
    Top = 576
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCentroCustoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCentroCusto: TDataSource
    DataSet = QrCentroCusto
    Left = 664
    Top = 576
  end
  object QrContas2: TMySQLQuery
    SQL.Strings = (
      'SELECT CONCAT('
      '  pl.Codigo, ".", '
      '  cj.Codigo, ".", '
      '  gr.Codigo, ".",'
      '  sg.Codigo, ".", '
      '  co.Codigo) Niveis, '
      'co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 292
    Top = 312
    object QrContas2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContas2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContas2Nome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContas2Nome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContas2ID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContas2Subgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContas2Empresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContas2Credito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContas2Debito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContas2Mensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContas2Exclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContas2Mensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContas2Mensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContas2Mensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContas2Menscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContas2Mensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContas2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContas2Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContas2Excel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContas2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContas2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContas2UserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContas2UserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContas2NOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContas2NOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContas2NOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContas2NOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrContas2NOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrContas2Niveis: TWideStringField
      FieldName = 'Niveis'
      Size = 100
    end
    object QrContas2Ordens: TWideStringField
      FieldName = 'Ordens'
      Size = 100
    end
  end
  object DsContas2: TDataSource
    DataSet = QrContas2
    Left = 292
    Top = 360
  end
end
