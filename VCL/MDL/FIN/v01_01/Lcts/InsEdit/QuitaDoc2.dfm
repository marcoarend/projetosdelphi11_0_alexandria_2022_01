object FmQuitaDoc2: TFmQuitaDoc2
  Left = 472
  Top = 219
  Caption = 'FIN-PGTOS-001 :: Quita'#231#227'o de Documento 2'
  ClientHeight = 355
  ClientWidth = 752
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 47
    Width = 752
    Height = 133
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label2: TLabel
      Left = 560
      Top = 51
      Width = 58
      Height = 13
      Caption = 'Documento:'
    end
    object Label3: TLabel
      Left = 16
      Top = 12
      Width = 62
      Height = 13
      Caption = 'Lan'#231'amento:'
    end
    object Label4: TLabel
      Left = 327
      Top = 12
      Width = 39
      Height = 13
      Caption = 'Carteira:'
    end
    object Label5: TLabel
      Left = 134
      Top = 12
      Width = 36
      Height = 13
      Caption = 'Cr'#233'dito:'
    end
    object Label6: TLabel
      Left = 232
      Top = 12
      Width = 34
      Height = 13
      Caption = 'D'#233'bito:'
    end
    object Label7: TLabel
      Left = 85
      Top = 51
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label8: TLabel
      Left = 499
      Top = 51
      Width = 27
      Height = 13
      Caption = 'S'#233'rie:'
    end
    object Label10: TLabel
      Left = 492
      Top = 90
      Width = 36
      Height = 13
      Caption = '$ Valor:'
    end
    object Label14: TLabel
      Left = 98
      Top = 90
      Width = 40
      Height = 13
      Caption = '% Multa:'
    end
    object Label16: TLabel
      Left = 389
      Top = 90
      Width = 58
      Height = 13
      Caption = '$ Desconto:'
    end
    object Label1: TLabel
      Left = 645
      Top = 51
      Width = 85
      Height = 13
      Caption = 'Data da quita'#231#227'o:'
    end
    object Label11: TLabel
      Left = 286
      Top = 90
      Width = 38
      Height = 13
      Caption = '$ Multa:'
    end
    object Label12: TLabel
      Left = 182
      Top = 90
      Width = 37
      Height = 13
      Caption = '$ Juros:'
    end
    object Label15: TLabel
      Left = 16
      Top = 90
      Width = 39
      Height = 13
      Caption = '% Juros:'
    end
    object Label9: TLabel
      Left = 16
      Top = 51
      Width = 53
      Height = 13
      Caption = 'Nota fiscal:'
    end
    object EdDocumento: TdmkEdit
      Left = 560
      Top = 67
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 6
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdDescricao: TdmkEdit
      Left = 85
      Top = 67
      Width = 409
      Height = 21
      TabOrder = 7
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdSerieCH: TdmkEdit
      Left = 499
      Top = 67
      Width = 56
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdValor: TdmkEdit
      Left = 492
      Top = 106
      Width = 104
      Height = 21
      Alignment = taRightJustify
      TabOrder = 16
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdMulta: TdmkEdit
      Left = 98
      Top = 106
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 12
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdMultaExit
    end
    object DBEdit1: TDBEdit
      Left = 134
      Top = 27
      Width = 95
      Height = 21
      Color = clInactiveCaption
      DataField = 'Credito'
      Enabled = False
      TabOrder = 2
    end
    object DBEdit2: TDBEdit
      Left = 232
      Top = 27
      Width = 91
      Height = 21
      Color = clInactiveCaption
      DataField = 'Debito'
      Enabled = False
      TabOrder = 3
    end
    object EdLancto: TdmkEdit
      Left = 16
      Top = 27
      Width = 86
      Height = 21
      Alignment = taRightJustify
      Color = clInactiveCaption
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSub: TdmkEdit
      Left = 106
      Top = 27
      Width = 28
      Height = 21
      Alignment = taRightJustify
      Color = clInactiveCaption
      Enabled = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object DBEdit3: TDBEdit
      Left = 327
      Top = 27
      Width = 59
      Height = 21
      Color = clInactiveCaption
      DataField = 'Carteira'
      Enabled = False
      TabOrder = 4
    end
    object DBEdit4: TDBEdit
      Left = 386
      Top = 27
      Width = 351
      Height = 21
      Color = clInactiveCaption
      DataField = 'NOMECARTEIRA'
      Enabled = False
      TabOrder = 5
    end
    object EdDescoVal: TdmkEdit
      Left = 389
      Top = 106
      Width = 98
      Height = 21
      Alignment = taRightJustify
      TabOrder = 15
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdDescoValExit
    end
    object TPData1: TdmkEditDateTimePicker
      Left = 645
      Top = 67
      Width = 92
      Height = 21
      Date = 37619.000000000000000000
      Time = 0.114129247696837400
      TabOrder = 10
      OnClick = TPData1Click
      OnChange = TPData1Change
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdMultaVal: TdmkEdit
      Left = 286
      Top = 106
      Width = 98
      Height = 21
      Alignment = taRightJustify
      TabOrder = 14
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdMultaValExit
    end
    object EdMoraVal: TdmkEdit
      Left = 182
      Top = 106
      Width = 99
      Height = 21
      Alignment = taRightJustify
      TabOrder = 13
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdMoraValExit
    end
    object EdMoraDia: TdmkEdit
      Left = 16
      Top = 106
      Width = 78
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdMoraDiaExit
    end
    object DBEdit5: TDBEdit
      Left = 16
      Top = 67
      Width = 64
      Height = 21
      Color = clInactiveCaption
      DataField = 'NotaFiscal'
      Enabled = False
      TabOrder = 6
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 752
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 705
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 658
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 297
        Height = 31
        Caption = 'Quita'#231#227'o de Documento 2'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 297
        Height = 31
        Caption = 'Quita'#231#227'o de Documento 2'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 297
        Height = 31
        Caption = 'Quita'#231#227'o de Documento 2'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 243
    Width = 752
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 748
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 286
    Width = 752
    Height = 69
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 609
      Top = 15
      Width = 141
      Height = 52
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 10
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 607
      Height = 52
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 14
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object GBCarteira: TGroupBox
    Left = 0
    Top = 180
    Width = 752
    Height = 63
    Align = alBottom
    Caption = 'Carteira onde o lan'#231'amento ser'#225' quitado'
    TabOrder = 4
    object Label13: TLabel
      Left = 12
      Top = 16
      Width = 39
      Height = 13
      Caption = 'Carteira:'
    end
    object CBCarteira: TdmkDBLookupComboBox
      Left = 67
      Top = 31
      Width = 670
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCarteiras
      TabOrder = 1
      dmkEditCB = EdCarteira
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCarteira: TdmkEditCB
      Left = 12
      Top = 31
      Width = 55
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCarteira
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
  end
  object QrCarteiras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo'
      'FROM carteiras'
      'WHERE Tipo IN(0,1)'
      'AND ForneceI=:P0'
      'AND Codigo > 0'
      'AND Ativo = 1'
      'ORDER BY Nome')
    Left = 480
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 508
    Top = 200
  end
end
