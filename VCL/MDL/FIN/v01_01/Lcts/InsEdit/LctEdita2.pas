unit LctEdita2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, Variants,
  mySQLDbTables, Mask, dmkCheckBox, ComCtrls, dmkEditDateTimePicker,
  UnFinanceiro, dmkImage, DmkDAC_PF, UnDmkEnums, unDmkProcFunc;

type
  TFmLctEdita2 = class(TForm)
    Panel1: TPanel;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    DsClientes: TDataSource;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesAccount: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrDeptos: TmySQLQuery;
    QrDeptosCODI_1: TIntegerField;
    QrDeptosCODI_2: TLargeintField;
    QrDeptosNOME_1: TWideStringField;
    DsDeptos: TDataSource;
    QrForneceI: TmySQLQuery;
    QrForneceICodigo: TIntegerField;
    QrForneceINOMEENTIDADE: TWideStringField;
    DsForneceI: TDataSource;
    DsFornecedores: TDataSource;
    QrFornecedores: TmySQLQuery;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOMEENTIDADE: TWideStringField;
    QrFunci: TmySQLQuery;
    QrFunciCodigo: TIntegerField;
    QrFunciNOMEENTIDADE: TWideStringField;
    DsFunci: TDataSource;
    DsVendedores: TDataSource;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    QrAccounts: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsAccounts: TDataSource;
    DsCarteiras: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasUsaTalao: TSmallintField;
    QrContas1: TMySQLQuery;
    QrContas1Codigo: TIntegerField;
    QrContas1Nome: TWideStringField;
    QrContas1Nome2: TWideStringField;
    QrContas1Nome3: TWideStringField;
    QrContas1ID: TWideStringField;
    QrContas1Subgrupo: TIntegerField;
    QrContas1Empresa: TIntegerField;
    QrContas1Credito: TWideStringField;
    QrContas1Debito: TWideStringField;
    QrContas1Mensal: TWideStringField;
    QrContas1Exclusivo: TWideStringField;
    QrContas1Mensdia: TSmallintField;
    QrContas1Mensdeb: TFloatField;
    QrContas1Mensmind: TFloatField;
    QrContas1Menscred: TFloatField;
    QrContas1Mensminc: TFloatField;
    QrContas1Lk: TIntegerField;
    QrContas1Terceiro: TIntegerField;
    QrContas1Excel: TWideStringField;
    QrContas1DataCad: TDateField;
    QrContas1DataAlt: TDateField;
    QrContas1UserCad: TSmallintField;
    QrContas1UserAlt: TSmallintField;
    QrContas1NOMESUBGRUPO: TWideStringField;
    QrContas1NOMEGRUPO: TWideStringField;
    QrContas1NOMECONJUNTO: TWideStringField;
    QrContas1NOMEEMPRESA: TWideStringField;
    QrContas1NOMEPLANO: TWideStringField;
    DsContas1: TDataSource;
    PainelDados: TPanel;
    LaFunci: TLabel;
    Panel3: TPanel;
    PnLct1: TPanel;
    Label14: TLabel;
    SbCarteira: TSpeedButton;
    BtContas: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    LaMes: TLabel;
    LaDeb: TLabel;
    LaCred: TLabel;
    LaNF: TLabel;
    LaDoc: TLabel;
    Label11: TLabel;
    LaVencimento: TLabel;
    LaCliInt: TLabel;
    LaCliente: TLabel;
    LaFornecedor: TLabel;
    SpeedButton2: TSpeedButton;
    LaForneceRN: TLabel;
    LaForneceFA: TLabel;
    Label15: TLabel;
    EdControle: TdmkEdit;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    CBGenero: TdmkDBLookupComboBox;
    EdCBGenero: TdmkEditCB;
    EdTPData: TdmkEditDateTimePicker;
    EdMes: TdmkEdit;
    EdDeb: TdmkEdit;
    EdCred: TdmkEdit;
    EdNF: TdmkEdit;
    EdSerieCH: TdmkEdit;
    EdDoc: TdmkEdit;
    EdDuplicata: TdmkEdit;
    EdTPVencto: TdmkEditDateTimePicker;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    EdCBCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdCBFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    PnDepto: TPanel;
    LaDepto: TLabel;
    CBDepto: TdmkDBLookupComboBox;
    EdCBDepto: TdmkEditCB;
    PnForneceI: TPanel;
    LaForneceI: TLabel;
    CBForneceI: TdmkDBLookupComboBox;
    EdCBForneceI: TdmkEditCB;
    PnAccount: TPanel;
    LaAccount: TLabel;
    EdCBAccount: TdmkEditCB;
    CBAccount: TdmkDBLookupComboBox;
    PnLct2: TPanel;
    Label13: TLabel;
    Label20: TLabel;
    EdDescricao: TdmkEdit;
    EdQtde: TdmkEdit;
    MeConfig: TMemo;
    QrPesq: TmySQLQuery;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    QrPesqNOMEPLANO: TWideStringField;
    QrPesqNOMESUBGRUPO: TWideStringField;
    QrPesqNOMEGRUPO: TWideStringField;
    QrPesqNOMECONJUNTO: TWideStringField;
    DsPesq: TDataSource;
    EdSub: TdmkEdit;
    EdTipo: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel7: TPanel;
    BtOK: TBitBtn;
    GroupBox1: TGroupBox;
    PnMaskPesq: TPanel;
    PnLink: TPanel;
    Panel6: TPanel;
    SpeedButton3: TSpeedButton;
    Panel11: TPanel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Panel5: TPanel;
    Panel12: TPanel;
    Label18: TLabel;
    EdLinkMask: TEdit;
    LLBPesq: TDBLookupListBox;
    GBVenda: TGroupBox;
    Panel4: TPanel;
    LaICMS_P: TLabel;
    LaICMS_V: TLabel;
    LaVendedor: TLabel;
    Label10: TLabel;
    EdICMS_P: TdmkEdit;
    EdICMS_V: TdmkEdit;
    EdCBVendedor: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    EdCBFunci: TdmkEditCB;
    CBFunci: TdmkDBLookupComboBox;
    Panel8: TPanel;
    PnLct3: TPanel;
    CkPesqNF: TCheckBox;
    CkPesqCH: TCheckBox;
    CkCancelado: TdmkCheckBox;
    CkPesqVal: TCheckBox;
    Panel9: TPanel;
    LaDataDoc: TLabel;
    EdTPDataDoc: TdmkEditDateTimePicker;
    QrCliIntCtaCfgCab: TIntegerField;
    QrCliIntCodigo: TIntegerField;
    QrCliIntAccount: TIntegerField;
    QrCliIntNOMEENTIDADE: TWideStringField;
    PnCentroCusto: TPanel;
    Label63: TLabel;
    dmkEdCBCentroCusto: TdmkEditCB;
    dmkCBCentroCusto: TdmkDBLookupComboBox;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCentroCusto: TmySQLQuery;
    IntegerField3: TIntegerField;
    QrCentroCustoNome: TWideStringField;
    DsCentroCusto: TDataSource;
    dmkEdit1: TdmkEdit;
    Label3: TLabel;
    EdTPVctoOriginal: TdmkEditDateTimePicker;
    Label4: TLabel;
    QrContas2: TMySQLQuery;
    QrContas2Codigo: TIntegerField;
    QrContas2Nome: TWideStringField;
    QrContas2Nome2: TWideStringField;
    QrContas2Nome3: TWideStringField;
    QrContas2ID: TWideStringField;
    QrContas2Subgrupo: TIntegerField;
    QrContas2Empresa: TIntegerField;
    QrContas2Credito: TWideStringField;
    QrContas2Debito: TWideStringField;
    QrContas2Mensal: TWideStringField;
    QrContas2Exclusivo: TWideStringField;
    QrContas2Mensdia: TSmallintField;
    QrContas2Mensdeb: TFloatField;
    QrContas2Mensmind: TFloatField;
    QrContas2Menscred: TFloatField;
    QrContas2Mensminc: TFloatField;
    QrContas2Lk: TIntegerField;
    QrContas2Terceiro: TIntegerField;
    QrContas2Excel: TWideStringField;
    QrContas2DataCad: TDateField;
    QrContas2DataAlt: TDateField;
    QrContas2UserCad: TSmallintField;
    QrContas2UserAlt: TSmallintField;
    QrContas2NOMESUBGRUPO: TWideStringField;
    QrContas2NOMEGRUPO: TWideStringField;
    QrContas2NOMECONJUNTO: TWideStringField;
    QrContas2NOMEEMPRESA: TWideStringField;
    QrContas2NOMEPLANO: TWideStringField;
    QrContas2Niveis: TWideStringField;
    DsContas2: TDataSource;
    DBNiveis1: TDBEdit;
    DBNiveis2: TDBEdit;
    EdGenCtb: TdmkEditCB;
    Label5: TLabel;
    CBGenCtb: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    QrContas1Niveis: TWideStringField;
    QrContas1Ordens: TWideStringField;
    QrContas2Ordens: TWideStringField;
    SbPsqGCFin: TSpeedButton;
    SbPsqGCCtb: TSpeedButton;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
    procedure SbCarteiraClick(Sender: TObject);
    procedure EdTPDataClick(Sender: TObject);
    procedure EdCBGeneroChange(Sender: TObject);
    procedure EdCBGeneroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGeneroClick(Sender: TObject);
    procedure CBGeneroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtContasClick(Sender: TObject);
    procedure EdMesExit(Sender: TObject);
    procedure EdDebExit(Sender: TObject);
    procedure EdCredExit(Sender: TObject);
    procedure EdDocChange(Sender: TObject);
    procedure EdDocExit(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure EdCBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdICMS_PExit(Sender: TObject);
    procedure EdICMS_PKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdICMS_VKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdICMS_VExit(Sender: TObject);
    procedure EdLinkMaskChange(Sender: TObject);
    procedure EdLinkMaskKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LLBPesqDblClick(Sender: TObject);
    procedure LLBPesqEnter(Sender: TObject);
    procedure LLBPesqExit(Sender: TObject);
    procedure LLBPesqKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrCliIntBeforeScroll(DataSet: TDataSet);
    procedure QrClientesAfterScroll(DataSet: TDataSet);
    procedure QrCarteirasAfterScroll(DataSet: TDataSet);
    procedure EdTPDataChange(Sender: TObject);
    procedure QrContas1AfterScroll(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure EdCliIntChange(Sender: TObject);
    procedure EdTPVenctoChange(Sender: TObject);
    procedure EdTPVenctoClick(Sender: TObject);
    procedure EdGenCtbRedefinido(Sender: TObject);
    procedure EdCBGeneroRedefinido(Sender: TObject);
    procedure SbPsqGCFinClick(Sender: TObject);
    procedure SbPsqGCCtbClick(Sender: TObject);
  private
    { Private declarations }
    FICMS: Double;
    FCriandoForm: Boolean;
    procedure CalculaICMS();
    procedure CarteirasReopen();
    procedure ConfiguracoesIniciais();
    procedure ConfiguraComponentesCarteira();
    procedure ConfiguraVencimento();
    procedure ExigeFuncionario();
    procedure MostraPnMaskPesq(Link: String);
    procedure ReopenFornecedores(Tipo: Integer);
    procedure SelecionaItemDePesquisa();
    procedure SetaAccount(var Key: Word; Shift: TShiftState);
    procedure VerificaEdits();
    procedure ReopenContas(Entidade, Codigo: Integer);
  public
    { Public declarations }
    FTabLctA: String;
    FOneAccount, FOneCliInt: Integer;
    FFinalidade: TLanctoFinalidade;
    FQrCrt, FQrLct: TmySQLQuery;
  end;

  var
  FmLctEdita2: TFmLctEdita2;

implementation

uses UnMyObjects, ModuleGeral, Module, UnInternalConsts, ModuleFin,
  MyDBCheck, Contas, Entidades, UMySQLModule, UnFinanceiroJan, UnContabil_Jan;

{$R *.DFM}

procedure TFmLctEdita2.BtContasClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.LocCod(QrContas1Codigo.Value, QrContas1Codigo.Value);
    FmContas.ShowModal;
    FmContas.Destroy;
    // 2012-05-15
    //QrContas.Close;
    //UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
    ReopenContas(0, 0);
    // FIM 2012-05-15
    EdCBGenero.Text := IntToStr(VAR_CONTA);
    CBGenero.KeyValue := VAR_CONTA;
    //
    if (ImgTipo.SQLType = stIns) (*and (CkDuplicando.Checked = False)*) then
      EdDescricao.Text := CBGenero.Text;
  end;
end;

procedure TFmLctEdita2.BtDesisteClick(Sender: TObject);
begin
  Close;
  VAR_VALOREMITIRIMP := 0;
end;

procedure TFmLctEdita2.BtOKClick(Sender: TObject);
var
  Carteira, Controle, Tipo, Sub: Integer;
begin
  if MyObjects.FIC(EdCliInt.ValueVariant = 0, nil, 'Informe o ' +
  Copy(LaCliInt.Caption, 1, Length(LaCliInt.Caption)-1) + '!') then
    Exit;
  //
  if (QrContas1Debito.Value <> 'V') or (CkCancelado.Checked) then
    EdDeb.ValueVariant := 0;
  if (QrContas1Credito.Value <> 'V') or (CkCancelado.Checked) then
    EdCred.ValueVariant := 0;
  //
  if (CBCarteira.KeyValue <> Null) and (EdCarteira.ValueVariant <> 0) then
    Carteira := CBCarteira.KeyValue
  else
    Carteira := 0;
  if MyObjects.FIC(Carteira = 0, nil, 'Informe a carteira!') then Exit;
  //
  if MyObjects.FIC((EdCred.ValueVariant + EdDeb.ValueVariant = 0) and
  (CkCancelado.Checked = False), nil, 'Informe o valor!') then
    Exit;
  //Salva hist�rico de lan�amento
  if ImgTipo.SQLType = stUpd then
  begin
    Tipo     := EdTipo.ValueVariant;
    Controle := EdControle.ValueVariant;
    Sub      := EdSub.ValueVariant;
    //
    (*
    UFinanceiro.ExcluiLct_EnviaArquivoMorto('lanctoz', FTabLctA,
      dmkPF.MotivDel_ValidaCodigo(101), Tipo, Carteira, Sub, Controle,
      DModFin.QrDelLct, Dmod.MyDB);
    *)
    UMyMod.ExcluiRegistro_EnviaArquivoMorto('lanctoz', FTabLctA,
      dmkPF.MotivDel_ValidaCodigo(101), ['Tipo', 'Carteira', 'Sub', 'Controle'],
      [Tipo, Carteira, Sub, Controle], Dmod.MyDB);
  end;
  if UMyMod.ExecSQLInsUpdFm(FmLctEdita2, ImgTipo.SQLType, FTabLctA, 0, Dmod.QrUpd) then
  begin
    if EdCarteira.OldValor <> EdCarteira.ValueVariant then
      UFinanceiro.RecalcSaldoCarteira(EdCarteira.ValueVariant, nil, nil, False, False);
    UFinanceiro.RecalcSaldoCarteira(EdCarteira.OldValor, FQrCrt, FQrLct, True, True);
    if FQrLct <> nil then
      FQrLct.Locate('Controle', EdControle.ValueVariant, []);
    Close;
  end;
end;

procedure TFmLctEdita2.CalculaICMS();
var
  C, D, B, V, P: Double;
begin
  V := Geral.DMV(EdICMS_V.Text);
  P := Geral.DMV(EdICMS_P.Text);
  C := Geral.DMV(EdCred.Text);
  D := Geral.DMV(EdDeb.Text);
  //if C > D then B := C - D else B := D - C;
  B := C-D;
  if FICMS = 0 then V := B * P / 100
  else if B <> 0 then P := V / B * 100 else P := 0;
  EdICMS_V.Text := Geral.FFT(V, 2, siPositivo);
  EdICMS_P.Text := Geral.FFT(P, 2, siPositivo);
end;

procedure TFmLctEdita2.CarteirasReopen;
begin
  QrCarteiras.Close;
  QrCarteiras.SQL.Clear;
  QrCarteiras.SQL.Add('SELECT car.*');
  QrCarteiras.SQL.Add('FROM carteiras car');
  QrCarteiras.SQL.Add('WHERE car.ForneceI IN (' + VAR_LIB_EMPRESAS + ')');
  QrCarteiras.SQL.Add('AND car.Ativo = 1 ');
  QrCarteiras.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
end;

procedure TFmLctEdita2.CBAccountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLctEdita2.CBGeneroClick(Sender: TObject);
begin
  VerificaEdits();
end;

procedure TFmLctEdita2.CBGeneroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
    MostraPnMaskPesq('Conta (Plano de contas)');
end;

procedure TFmLctEdita2.ConfiguracoesIniciais();
begin
  QrCliInt.Close;
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCentroCusto, Dmod.MyDB);
  // 2012-05-15
  // Precisa do cliInt aberto para abrir as contas restritivas corretamente
  ReopenContas(EdCliInt.ValueVariant, 0);
  // FIM 2012-05-15
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrContas2, Dmod.MyDB, [
  'SELECT CONCAT( ',
  '  pl.Codigo, ".", ',
  '  cj.Codigo, ".", ',
  '  gr.Codigo, ".", ',
  '  sg.Codigo, ".", ',
  '  co.Codigo) Niveis, ',
  'CONCAT( ',
  '  pl.OrdemLista, ".", ',
  '  cj.OrdemLista, ".", ',
  '  gr.OrdemLista, ".", ',
  '  sg.OrdemLista, ".", ',
  '  co.OrdemLista) Ordens, ',
  'co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO, ',
  'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO, ',
  'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA ',
  'FROM contas co ',
  'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo ',
  'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo ',
  'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto ',
  'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano ',
  'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa ',
(*
  'WHERE co.Terceiro=0 ',
  'AND co.Codigo>0 ',
  SQL1,
  SQL2,
*)
  'ORDER BY co.Nome ',
  '']);

  MeConfig.Lines.Clear;
  QrDeptos.Close;
  QrDeptos.SQL.Clear;
  //FIDFinalidade := Geral.IMV(Copy(LaFinalidade.Caption, 1, 3));
  case FFinalidade of
    lfProprio:
    begin
      LaCliInt.Caption := 'Empresa (Filial):';
      if Uppercase(Application.Title) = 'SYNKER' then
      begin
        //
        QrDeptos.SQL.Add('SELECT Codigo CODI_1, Nome NOME_1, FLOOR(0) CODI_2');
        QrDeptos.SQL.Add('FROM intentocad');
        QrDeptos.SQL.Add('WHERE Ativo=1');
        QrDeptos.SQL.Add('ORDER BY NOME_1');
        //
        {
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Add('SELECT Codigo, ');
        QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
        QrCliInt.SQL.Add('FROM entidades');
        QrCliInt.SQL.Add('WHERE Codigo=' + Geral.FF0(Dmod.QrControleFieldByName('Dono').AsInteger));
        UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
        }
        //////
        //PnDepto.Visible   := True;
        //PnForneceI.Visible := True;
      end;
    end;
    lfCondominio:
    begin
      LaCliInt.Caption := 'Condom�nio (Cliente interno):';
      //
      QrDeptos.SQL.Add('SELECT Conta CODI_1, Unidade NOME_1, FLOOR(Propriet) CODI_2');
      QrDeptos.SQL.Add('FROM condimov');
      QrDeptos.SQL.Add('WHERE Codigo=:P0');
      QrDeptos.SQL.Add('ORDER BY NOME_1');
      QrDeptos.Params[0].AsInteger := FOneCliInt;//EdOneCliInt.ValueVariant;//FmCondGer.QrCondCodigo.Value;
      //
      if Trim(VAR_CLIENTEI) <> '' then
      begin
        {
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Add('SELECT Codigo, ');
        QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
        QrCliInt.SQL.Add('FROM entidades');
        QrCliInt.SQL.Add('WHERE Cliente'+VAR_CLIENTEI+'="V"');
        QrCliInt.SQL.Add('ORDER BY NOMEENTIDADE');
        UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
        }
        //////
        LaCliInt.Visible := True;
        EdCliInt.Visible := True;
        CBCliInt.Visible := True;
      end;
      PnDepto.Visible   := True;
      PnForneceI.Visible := True;
    end;
    lfCicloCurso:
    begin
      {
      QrCliInt.SQL.Clear;
      QrCliInt.SQL.Add('SELECT Codigo, ');
      QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
      QrCliInt.SQL.Add('FROM entidades');
      QrCliInt.SQL.Add('WHERE CliInt>0 OR Codigo=-1');
      QrCliInt.SQL.Add('ORDER BY NOMEENTIDADE');
      UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
      }
      LaCliInt.Caption        := 'Empresa (Filial):';
      LaDepto.Caption         := '';
      LaAccount.Caption       := 'Respons�vel interno pelo movimento:';
      EdCBForneceI.Visible := False;
      CBForneceI.Visible   := False;
      //
      QrAccounts.SQL.Clear;
      QrAccounts.SQL.Add('SELECT Codigo, ');
      QrAccounts.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
      QrAccounts.SQL.Add('FROM entidades');
      QrAccounts.SQL.Add('WHERE Fornece2="V" OR Fornece3="V"');
      UnDmkDAC_PF.AbreQuery(QrAccounts, Dmod.MyDB);
      //
      PnAccount.Visible           := True;
      if ImgTipo.SQLType = stIns then
      begin
        EdCBAccount.ValueVariant := FOneAccount;
        CBAccount.KeyValue       := FOneAccount;
      end;
    end;
    else Geral.MB_Erro('Nenhuma finalidade foi definida para a ' +
    'janela de inclus�o / altera��o de lan�amentos!');
  end;
  if QrDeptos.SQL.Text <> '' then
    UnDmkDAC_PF.AbreQuery(QrDeptos, Dmod.MyDB);
end;

procedure TFmLctEdita2.ConfiguraComponentesCarteira;
begin
  case QrCarteirasTipo.Value of
    0:
    begin
      if (QrCarteirasPrazo.Value = 1) then
      begin
        if (QrCarteirasUsaTalao.Value = 0) then
        begin
          LaDoc.Enabled := True;
          EdSerieCH.Enabled := True;
          EdDoc.Enabled := True;
        end;
        EdNF.Enabled := True;
        LaNF.Enabled := True;
      end else begin
        // N�mero bloqueto no cheque
        if (QrCarteirasUsaTalao.Value = 0) then
        begin
          LaDoc.Enabled := True;
          EdSerieCH.Enabled := True;
          EdDoc.Enabled := True;
        end;
        //
        EdNF.Enabled := True;
        LaNF.Enabled := True;
      end;
    end;
    1:
    begin
      if (QrCarteirasUsaTalao.Value = 0) then
      begin
        LaDoc.Enabled := True;
        EdSerieCH.Enabled := True;
        EdDoc.Enabled := True;
      end;
    end;
    2:
    begin
      if (QrCarteirasUsaTalao.Value = 0) then
      begin
        LaDoc.Enabled := True;
        EdSerieCH.Enabled := True;
        EdDoc.Enabled := True;
      end;
      //
      EdNF.Enabled := True;
      LaNF.Enabled := True;
    end;
  end;
  ConfiguraVencimento();
end;

procedure TFmLctEdita2.ConfiguraVencimento();
begin
  if ImgTipo.SQLType = stIns then
    EdTPVencto.Date := EdTPdata.Date;
end;

procedure TFmLctEdita2.EdCarteiraChange(Sender: TObject);
var
  UsaTalao: Boolean;
begin
  UsaTalao := QrCarteirasUsaTalao.Value = 0;
  EdSerieCH.Enabled := UsaTalao;
  EdDoc.Enabled     := UsaTalao;
  LaDoc.Enabled        := UsaTalao;
end;

procedure TFmLctEdita2.EdCBAccountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLctEdita2.EdCBGeneroChange(Sender: TObject);
begin
  VerificaEdits();
  if (ImgTipo.SQLType = stIns) (*and (CkDuplicando.Checked = False)*) then
    if EdCBGenero.ValueVariant <> 0 then
      EdDescricao.Text := QrContas1Nome.Value;
end;

procedure TFmLctEdita2.EdCBGeneroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
    MostraPnMaskPesq('Conta (Plano de contas)');
end;

procedure TFmLctEdita2.EdCBGeneroRedefinido(Sender: TObject);
begin
  if EdCBGenero.ValueVariant = 0 then
    DBNiveis1.DataField := ''
  else
  begin
    if DModG.QrCtrlGeralConcatNivOrdCta.Value = 1 then
      DBNiveis1.DataField := 'Ordens'
    else
      DBNiveis1.DataField := 'Niveis';
  end;
end;

procedure TFmLctEdita2.EdCliIntChange(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := EdCliInt.ValueVariant;
  //EventosCadReopen(Entidade);  Fazer??
  ReopenContas(Entidade, 0);
end;

procedure TFmLctEdita2.EdCredExit(Sender: TObject);
begin
  CalculaICMS;
  //if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdita2.EdDebExit(Sender: TObject);
begin
  CalculaICMS();
  //if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdita2.EdDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if EdMes.Enabled then Mes := EdMes.Text else Mes := '';
    if key=VK_F4 then EdDescricao.Text := QrContas1Nome.Value+' '+Mes;
    if key=VK_F5 then EdDescricao.Text := QrContas1Nome2.Value+' '+Mes;
    if key=VK_F6 then EdDescricao.Text := QrContas1Nome3.Value+' '+Mes;
    if EdDescricao.Enabled then EdDescricao.SetFocus;
    EdDescricao.SelStart := Length(EdDescricao.Text);
    EdDescricao.SelLength := 0;
  end else if key = VK_F8 then
  begin
    if CBFornece.Enabled then
      EdDescricao.Text := EdDescricao.Text + ' - ' + CBFornece.Text
    else if CBCliente.Enabled then
      EdDescricao.Text := EdDescricao.Text + ' - ' + CBCliente.Text;
  end;
end;

procedure TFmLctEdita2.EdDocChange(Sender: TObject);
begin
{
  if Geral.DMV(EdDoc.Text) = 0 then
    RGIncremCH.Enabled := False
  else
    RGIncremCH.Enabled := True;
}
end;

procedure TFmLctEdita2.EdDocExit(Sender: TObject);
begin
  //if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdita2.EdGenCtbRedefinido(Sender: TObject);
begin
  if EdGenCtb.ValueVariant = 0 then
    DBNiveis2.DataField := ''
  else
  begin
    if DModG.QrCtrlGeralConcatNivOrdCta.Value = 1 then
      DBNiveis2.DataField := 'Ordens'
    else
      DBNiveis2.DataField := 'Niveis';
  end;
end;

procedure TFmLctEdita2.EdICMS_PExit(Sender: TObject);
begin
  CalculaICMS();
end;

procedure TFmLctEdita2.EdICMS_PKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if dmkPF.EhNumeroOuSinalOuSeparador(Key) then FICMS := 0;
end;

procedure TFmLctEdita2.EdICMS_VExit(Sender: TObject);
begin
  CalculaICMS();
end;

procedure TFmLctEdita2.EdICMS_VKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if dmkPF.EhNumeroOuSinalOuSeparador(Key) then FICMS := 0;
end;

procedure TFmLctEdita2.EdLinkMaskChange(Sender: TObject);
begin
  QrPesq.Close;
  if Length(EdLinkMask.Text) >= 3 then
  begin
    if PnLink.Caption = 'Conta (Plano de contas)' then
    begin
      QrPesq.Close;
      QrPesq.Params[0].AsString := '%' + EdLinkMask.Text + '%';
      UnDmkDAC_PF.AbreQuery(QrPesq, Dmod.MyDB);
      LLBPesq.ListSource := DsPesq;
    end
  end;
end;

procedure TFmLctEdita2.EdLinkMaskKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DOWN then LLBPesq.SetFocus;
end;

procedure TFmLctEdita2.EdMesExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) (*and (CkDuplicando.Checked = False)*)  then
    EdDescricao.Text := QrContas1Nome2.Value + ' ' + EdMes.Text;
end;

procedure TFmLctEdita2.EdTPDataChange(Sender: TObject);
begin
  ConfiguraVencimento();
end;

procedure TFmLctEdita2.EdTPDataClick(Sender: TObject);
begin
  ConfiguraVencimento();
end;

procedure TFmLctEdita2.EdTPVenctoChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    EdTPVctoOriginal.Date := EdTPVencto.Date;
end;

procedure TFmLctEdita2.EdTPVenctoClick(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    EdTPVctoOriginal.Date := EdTPVencto.Date;
end;

procedure TFmLctEdita2.ExigeFuncionario();
begin
  QrFunci.Close;
  QrFunci.SQL.Clear;
  QrFunci.SQL.Add('SELECT Codigo,');
  QrFunci.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
  QrFunci.SQL.Add('ELSE Nome END NOMEENTIDADE');
  QrFunci.SQL.Add('FROM entidades');
  QrFunci.SQL.Add('WHERE '+VAR_TIPOVARCLI+'="V"');
  QrFunci.SQL.Add('ORDER BY NomeENTIDADE');
  UnDmkDAC_PF.AbreQuery(QrFunci, Dmod.MyDB);
  Label10.Visible      := True;
  EdCBFunci.Visible := True;
  CBFunci.Visible   := True;
end;

procedure TFmLctEdita2.FormActivate(Sender: TObject);
var
  Carteira: Integer;
begin
  if FFinalidade = lfUnknow then
  begin
    Geral.MB_Erro(
    'ID da Finalidade n�o definida! Avise a DERMATEK!');
    Close;
    Exit;
  end;
  CarteirasReopen;
  if ImgTipo.SQLType = stIns then
  begin
    //CkContinuar.Visible := True;
    //CkParcelamento.Visible := True;
    //
    if VLAN_QrCarteiras <> nil then
    begin
      Carteira := VLAN_QrCarteiras.FieldByName('Codigo').AsInteger;
      if Carteira <> 0 then
      begin
        EdCarteira.ValueVariant := Carteira;
        CBCarteira.KeyValue     := Carteira;
      end;
    end;
  end;
  if EdCarteira.Enabled then EdCarteira.SetFocus;
  VerificaEdits();
  if FCriandoForm then
  begin
    ConfiguracoesIniciais();
  end;
  FCriandoForm := False;
  MyObjects.CorIniComponente();
  EdCarteiraChange(Self);
end;

procedure TFmLctEdita2.FormCreate(Sender: TObject);
var
  CliInt: Integer;
begin
  ImgTipo.SQLType := stLok;
  //
  DBNiveis1.DataField := '';
  DBNiveis2.DataField := '';
  //
  EdTPData.MinDate := VAR_DATA_MINIMA;
  PnLct1.BevelOuter  := bvNone;
  PnDepto.BevelOuter    := bvNone;
  PnForneceI.BevelOuter := bvNone;
  PnAccount.BevelOuter  := bvNone;
  PnCentroCusto.BevelOuter := bvNone;
  //PnEdit05.BevelOuter := bvNone;
  //PnEdit06.BevelOuter := bvNone;
  PnLct2.BevelOuter  := bvNone;
  PnLct3.BevelOuter  := bvNone;
  //Width := FLargMenor;
  FCriandoForm := True;
  // 2012-05-15
  //UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  ReopenContas(0, 0);
  // FIM 2012-05-15
  if VAR_CXEDIT_ICM then
  begin
    LaICMS_P.Visible := True;
    LaICMS_V.Visible := True;
    EdICMS_P.Visible := True;
    EdICMS_V.Visible := True;
  end;
  //if Trim(VAR_CLIENTEC) <> '' then
  //begin
    QrClientes.SQL.Clear;
    QrClientes.SQL.Add('SELECT Codigo, Account, ');
    QrClientes.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrClientes.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrClientes.SQL.Add('FROM entidades');
    QrClientes.SQL.Add('WHERE Cliente1="V"');
    QrClientes.SQL.Add('OR    Cliente2="V"');
    QrClientes.SQL.Add('OR    Cliente3="V"');
    QrClientes.SQL.Add('OR    Cliente4="V"');
    QrClientes.SQL.Add('OR    Terceiro="V"');
    QrClientes.SQL.Add('ORDER BY NOMEENTIDADE');
    UnDmkDAC_PF.AbreQuery(QrClientes, Dmod.MyDB);
    //////
    LaCliente.Visible := True;
    EdCBCliente.Visible := True;
    CBCliente.Visible := True;
    //////
    //EdCliInt.Enabled := False;
    //CBCliInt.Enabled := False;
    if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
    EdCliInt.Text := IntToStr(CliInt);
    CBCliInt.KeyValue := CliInt;
  //end;
  ReopenFornecedores(0);
  if Trim(VAR_FORNECEF) <> '' then
  begin
    //
    LaFornecedor.Visible := True;
    EdCBFornece.Visible := True;
    CBFornece.Visible := True;
  end;
  if Trim(VAR_FORNECEV) <> '' then
  begin
    QrVendedores.SQL.Clear;
    QrVendedores.SQL.Add('SELECT Codigo, ');
    QrVendedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrVendedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrVendedores.SQL.Add('FROM entidades');
    QrVendedores.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrVendedores.SQL.Add('ORDER BY NOMEENTIDADE');
    UnDmkDAC_PF.AbreQuery(QrVendedores, Dmod.MyDB);
    //////
    QrAccounts.SQL.Clear;
    QrAccounts.SQL.Add('SELECT Codigo, ');
    QrAccounts.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrAccounts.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrAccounts.SQL.Add('FROM entidades');
    QrAccounts.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrAccounts.SQL.Add('ORDER BY NOMEENTIDADE');
    UnDmkDAC_PF.AbreQuery(QrAccounts, Dmod.MyDB);
    //////
    LaVendedor.Visible := True;
    CBVendedor.Visible := True;
    CBVendedor.Visible   := True;
    PnAccount.Visible := True;
  end;
  EdTPDataDoc.Date := Date;
  if VAR_LANCTOCONDICIONAL1 then
  begin
    VAR_DATAINSERIR := Date;
    EdTPDataDoc.Enabled := False;
    LaDataDoc.Enabled := False;
    //
    EdCarteira.Enabled := False;
    CBCarteira.Enabled := False;
    EdTPdata.Enabled := False;
  end;
  if VAR_TIPOVARCLI <> '' then ExigeFuncionario();
  //PageControl1.ActivePageIndex := 0;
  //TabControl1.TabIndex := 0;
  CBCarteira.ListSource := DsCarteiras;
  CBGenero.ListSource := DsContas1;
  CBGenCtb.ListSource := DsContas2;
  CBCliInt.ListSource := DsCliInt;
  CBCliente.ListSource := DsClientes;
  CBFornece.ListSource := DsFornecedores;
  CBDepto.ListSource := DsDeptos;
  CBForneceI.ListSource := DsForneceI;
  CBAccount.ListSource := DsAccounts;
  dmkCBCentroCusto.ListSource := DsCentroCusto;
  LLBPesq.ListSource := DsPesq;
  CBVendedor.ListSource := DsVendedores;
  CBFunci.ListSource := DsFunci;
end;

procedure TFmLctEdita2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctEdita2.LLBPesqDblClick(Sender: TObject);
begin
  SelecionaItemDePesquisa();
end;

procedure TFmLctEdita2.LLBPesqEnter(Sender: TObject);
begin
  DBEdit1.DataSource := DsPesq;
  DBEdit2.DataSource := DsPesq;
  DBEdit3.DataSource := DsPesq;
  DBEdit4.DataSource := DsPesq;
end;

procedure TFmLctEdita2.LLBPesqExit(Sender: TObject);
begin
  DBEdit1.DataSource := DsContas1;
  DBEdit2.DataSource := DsContas1;
  DBEdit3.DataSource := DsContas1;
  DBEdit4.DataSource := DsContas1;
end;

procedure TFmLctEdita2.LLBPesqKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then SelecionaItemdePesquisa();
end;

procedure TFmLctEdita2.MostraPnMaskPesq(Link: String);
begin
{ Acho que n�o precisa 2011-01-26
  PnLink.Caption     := Link;
  EdLinkMask.Text    := '';
  EdLinkMask.SetFocus;
}
end;

procedure TFmLctEdita2.QrCarteirasAfterScroll(DataSet: TDataSet);
begin
  ConfiguraComponentesCarteira();
end;

procedure TFmLctEdita2.QrClientesAfterScroll(DataSet: TDataSet);
begin
  case FFinalidade of
    lfProprio: // Pr�prio
    begin
      {  Ver como fazer no self da adm de condom�nios!
      QrForneceI.Close;
      QrForneceI.SQL.Clear;
      QrForneceI.SQL.Add('SELECT ent.Codigo,');
      QrForneceI.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM intentosoc soc');
      QrForneceI.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=soc.Socio');
      QrForneceI.SQL.Add('WHERE soc.Codigo=:P0');
      QrForneceI.SQL.Add('ORDER BY NOMEENTIDADE');
      QrForneceI.SQL.Add('');
      QrForneceI.Params[0].AsInteger := QrDeptosCODI_1.Value;
      try
        UnDmkDAC_PF.AbreQuery(QrForneceI, Dmod.MyDB);
        if not QrForneceI.Locate('Codigo', Geral.IMV(EdCBForneceI.Text), []) then
        begin
          EdCBForneceI.Text := '';
          CBForneceI.KeyValue := Null;
        end;
      finally
        ;
      end;
      }
    end;
    lfCondominio: // Condom�nios
    begin
      //Nada
    end;
    lfCicloCurso: // Cursos c�clicos
    begin
      //Nada
    end;
  end;
end;

procedure TFmLctEdita2.QrCliIntBeforeScroll(DataSet: TDataSet);
begin
{
  if FFinalidade = 0 then
    FFinalidade := Geral.IMV(Copy(LaFinalidade.Caption, 1, 3));
}
  case FFinalidade of
    lfProprio: // Pr�prios
    begin
      //Nada
    end;
    lfCondominio: // Condom�nios
    begin
      QrForneceI.Close;
      QrForneceI.SQL.Clear;
{
      QrForneceI.SQL.Add('SELECT Codigo,');
      QrForneceI.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
      QrForneceI.SQL.Add('ELSE Nome END NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM entidades');
      QrForneceI.SQL.Add('WHERE Cliente2="V"');
      QrForneceI.SQL.Add('AND Codigo in (');
      QrForneceI.SQL.Add('  SELECT ci2.Propriet');
      QrForneceI.SQL.Add('  FROM condimov ci2');
      QrForneceI.SQL.Add('  LEFT JOIN cond co2 ON co2.Codigo=ci2.Codigo');
      QrForneceI.SQL.Add('  WHERE co2.Cliente IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add(') ORDER BY NOMEENTIDADE');
}
      QrForneceI.SQL.Add('SELECT DISTINCT ent.Codigo,');
      QrForneceI.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
      QrForneceI.SQL.Add('ELSE ent.Nome END NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM condimov ci2');
      QrForneceI.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ci2.Propriet');
      QrForneceI.SQL.Add('WHERE ci2.Codigo IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add('ORDER BY NOMEENTIDADE');
      QrForneceI.SQL.Add('');
      UnDmkDAC_PF.AbreQuery(QrForneceI, Dmod.MyDB);
    end;
    lfCicloCurso: // Cursos c�clicos
    begin
      //Nada ?
    end;
  end;
end;

procedure TFmLctEdita2.QrContas1AfterScroll(DataSet: TDataSet);
begin
  VerificaEdits();
end;

procedure TFmLctEdita2.ReopenContas(Entidade, Codigo: Integer);
var
  CtaCfgCab, PlanoPadrao: Integer;
  SQL1, SQL2: String;
begin
  QrContas1.Close;
  if QrCliInt.State <> dsInactive then
  begin
    QrCliInt.Locate('Codigo', Entidade, []);
    //
    CtaCfgCab   := QrCliIntCtaCfgCab.Value;
    PlanoPadrao := DModFin.QrFinOpcoes.FieldByName('PlanoPadrao').asInteger;
    //
    if CtaCfgCab <> 0 then
    begin
      SQL1 :=
        'AND co.Codigo IN ( ' + sLineBreak +
        '     SELECT Genero ' + sLineBreak +
        '     FROM ctacfgits ' + sLineBreak +
        '     WHERE Codigo=' + Geral.FF0(CtaCfgCab) + ') ';
    end else
      SQL1 := '';
    //
    if PlanoPadrao <> 0 then
      SQL2 := 'AND pl.Codigo=' + Geral.FF0(PlanoPadrao)
    else
      SQL2 := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrContas1, Dmod.MyDB, [
      'SELECT CONCAT( ',
      '  pl.Codigo, ".", ',
      '  cj.Codigo, ".", ',
      '  gr.Codigo, ".", ',
      '  sg.Codigo, ".", ',
      '  co.Codigo) Niveis, ',
      'CONCAT( ',
      '  pl.OrdemLista, ".", ',
      '  cj.OrdemLista, ".", ',
      '  gr.OrdemLista, ".", ',
      '  sg.OrdemLista, ".", ',
      '  co.OrdemLista) Ordens, ',
      'co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO, ',
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO, ',
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA ',
      'FROM contas co ',
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo ',
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo ',
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto ',
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano ',
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa ',
      'WHERE co.Terceiro=0 ',
      'AND co.Codigo>0 ',
      SQL1,
      SQL2,
      'ORDER BY co.Nome ',
      '']);
    //
    if Codigo <> 0 then
      QrContas1.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmLctEdita2.ReopenFornecedores(Tipo: Integer);
begin
  QrFornecedores.Close;
  QrFornecedores.SQL.Clear;
  QrFornecedores.SQL.Add('SELECT Codigo, ');
  if Tipo = 0 then
  begin
    QrFornecedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrFornecedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    //
    LaFornecedor.Font.Color := LaForneceRN.Font.Color;
  end else begin
    QrFornecedores.SQL.Add('CASE WHEN Tipo=0 THEN Fantasia');
    QrFornecedores.SQL.Add('ELSE Apelido END NOMEENTIDADE');
    //
    LaFornecedor.Font.Color := LaForneceFA.Font.Color;
  end;
  QrFornecedores.SQL.Add('FROM entidades');
  QrFornecedores.SQL.Add('WHERE Fornece1="V"');
  QrFornecedores.SQL.Add('OR    Fornece2="V"');
  QrFornecedores.SQL.Add('OR    Fornece3="V"');
  QrFornecedores.SQL.Add('OR    Fornece4="V"');
  QrFornecedores.SQL.Add('OR    Fornece5="V"');
  QrFornecedores.SQL.Add('OR    Fornece6="V"');
  QrFornecedores.SQL.Add('OR    Terceiro="V"');
  QrFornecedores.SQL.Add('ORDER BY NOMEENTIDADE');
  UnDmkDAC_PF.AbreQuery(QrFornecedores, Dmod.MyDB);
  //
end;

procedure TFmLctEdita2.SbCarteiraClick(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdCarteira.ValueVariant;
  //
  FinanceiroJan.CadastroDeCarteiras(Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrCarteiras.Close;
    UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  end;
end;

procedure TFmLctEdita2.SbPsqGCCtbClick(Sender: TObject);
var
  CtaFin, CtaCtb: Integer;
  UsarAsDuas: Boolean;
begin
  CtaFin := EdCBGenero.ValueVariant;
  CtaCtb := EdGenCtb.ValueVariant;
  Contabil_Jan.MostraFormCtbGruPsq(CtaFin, CtaCtb, UsarAsDuas);
  EdGenCtb.ValueVariant := CtaCtb;
  CBGenCtb.KeyValue     := CtaCtb;
  if UsarAsDuas then
  begin
    EdCBGenero.ValueVariant := CtaFin;
    CBGenero.KeyValue     := CtaFin;
  end;
end;

procedure TFmLctEdita2.SbPsqGCFinClick(Sender: TObject);
var
  CtaFin, CtaCtb: Integer;
  UsarAsDuas: Boolean;
begin
  CtaFin := EdCBGenero.ValueVariant;
  CtaCtb := EdGenCtb.ValueVariant;
  Contabil_Jan.MostraFormCtbGruPsq(CtaFin, CtaCtb, UsarAsDuas);
  EdCBGenero.ValueVariant := CtaFin;
  CBGenero.KeyValue     := CtaFin;
  if UsarAsDuas then
  begin
    EdGenCtb.ValueVariant := CtaCtb;
    CBGenCtb.KeyValue     := CtaCtb;
  end;
end;

procedure TFmLctEdita2.SelecionaItemDePesquisa;
begin
  if QrPesq.RecordCount > 0 then
  begin
    if PnLink.Caption = 'Conta (Plano de contas)' then
    begin
      EdCBGenero.Text     := IntToStr(QrPesqCodigo.Value);
      CBGenero.KeyValue := QrPesqCodigo.Value;
      if CBGenero.Visible then CBGenero.SetFocus;
    end
  end;
end;

procedure TFmLctEdita2.SetaAccount(var Key: Word; Shift: TShiftState);
begin
  if key=VK_F4 then
  begin
    EdCBAccount.Text   := IntToStr(QrClientesAccount.Value);
    CBAccount.KeyValue := QrClientesAccount.Value;
  end;
end;

procedure TFmLctEdita2.SpeedButton2Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  DModG.CadastroDeEntidade(EdCBFornece.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  QrFornecedores.Close;
  UnDmkDAC_PF.AbreQuery(QrFornecedores, Dmod.MyDB);
  if VAR_ENTIDADE <> 0 then
  begin
    EdCBFornece.Text := IntToStr(VAR_ENTIDADE);
    CBFornece.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmLctEdita2.VerificaEdits();
begin
  if (QrContas1Mensal.Value = 'V') or (Dmod.QrControle.FieldByName('MensalSempre').AsInteger = 1) then
  begin
    EdMes.Enabled := True;
    LaMes.Enabled := True;
  end else begin
    EdMes.Enabled := False;
    LaMes.Enabled := False;
  end;
  //
  if (VAR_BAIXADO <> -2) and not VAR_FATURANDO then Exit;
  if QrContas1Credito.Value = 'V' then
  begin
    EdCred.Enabled := True;
    LaCred.Enabled := True;
    EdCBCliente.Enabled := True;
    LaCliente.Enabled := True;
    CBCliente.Enabled := True;
  end else begin
    EdCred.Enabled := False;
    LaCred.Enabled := False;
    EdCBCliente.Enabled := False;
    LaCliente.Enabled := False;
    CBCliente.Enabled := False;
  end;

  if QrContas1Debito.Value = 'V' then
  begin
    EdDeb.Enabled := True;
    LaDeb.Enabled := True;
    EdCBFornece.Enabled := True;
    LaFornecedor.Enabled := True;
    CBFornece.Enabled := True;
  end else begin
    EdDeb.Enabled := False;
    LaDeb.Enabled := False;
    EdCBFornece.Enabled := False;
    LaFornecedor.Enabled := False;
    CBFornece.Enabled := False;
  end;

end;

end.
