unit Transfer2;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  ZCF2, ResIntStrings, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UnGOTOy, UnMySQLCuringa, ComCtrls, (*DBIProcs,*) UMySQLModule,
  mySQLDbTables, dmkEdit, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB,
  dmkGeral, dmkImage, UnDmkEnums;

type
  TFmTransfer2 = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    LaConjunto: TLabel;
    QrOrigem: TmySQLQuery;
    DsOrigem: TDataSource;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DsDestino: TDataSource;
    QrOrigemCodigo: TIntegerField;
    QrOrigemNome: TWideStringField;
    EdCodigo: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    EdValor: TdmkEdit;
    EdDocDeb: TdmkEdit;
    EdDocCred: TdmkEdit;
    EdSerieChDeb: TdmkEdit;
    Label2: TLabel;
    EdSerieCHCred: TdmkEdit;
    Label3: TLabel;
    EdOrigem: TdmkEditCB;
    CBOrigem: TdmkDBLookupComboBox;
    EdDestino: TdmkEditCB;
    CBDestino: TdmkDBLookupComboBox;
    QrDestino: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    QrOrigemTipo: TIntegerField;
    QrDestinoTipo: TIntegerField;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EdOldControle1: TdmkEdit;
    TPOldData1: TdmkEditDateTimePicker;
    EdOldSub1: TdmkEdit;
    EdOldTipo1: TdmkEdit;
    EdOldCarteira1: TdmkEdit;
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    EdOldControle2: TdmkEdit;
    TPOldData2: TdmkEditDateTimePicker;
    EdOldSub2: TdmkEdit;
    EdOldTipo2: TdmkEdit;
    EdOldCarteira2: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    BtConfirma: TBitBtn;
    BtExclui: TBitBtn;
    Label19: TLabel;
    EdDuplicataDeb: TdmkEdit;
    Label20: TLabel;
    EdDuplicataCred: TdmkEdit;
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdDuplicataDebChange(Sender: TObject);
  private
    { Private declarations }
    function  ImpedePelaMigracao(): Boolean;
  public
    { Public declarations }
    FFatID, FFatID_Sub, FFAtNum, F_CliInt: Integer;
    FQrCrt, FQrLct: TmySQLQuery;
    FTabLctA: String;
  end;

var
  FmTransfer2: TFmTransfer2;

implementation

uses UnMyObjects, Module, UnFinanceiro, ModuleFin, UnDmkProcFunc;

{$R *.DFM}

procedure TFmTransfer2.BtConfirmaClick(Sender: TObject);
var
  OldData1, Olddata2: TDate;
  OldTipo1, OldCarteira1, OldControle1, OldSub1,
  OldTipo2, OldCarteira2, OldControle2, OldSub2,
  Carteira, Tipo: Integer;
  Debito, Credito, Documento: Double;
  SerieCH, Vencimento: String;
  //
  Codigo, Origem, Destino, Sit: Integer;
  Localiza: Boolean;
  Controle: Int64;
  Valor: Double;
  Data, Duplicata: String;
begin
  Data := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  UFinanceiro.AtzDataAtzSdoPlaCta(F_CliInt, TPData.Date);
  Codigo := EdCodigo.ValueVariant;
  if (Codigo = 0) and (ImgTipo.SQLType = stUpd) then
  begin
    Geral.MB_Aviso('Altera��o imposs�vel sem o n� do lan�amento!');
    Exit;
  end;
  Origem := EdOrigem.ValueVariant;
  if Origem = 0 then
  begin
    Geral.MB_Aviso('Defina a carteira de origem!');
    EdOrigem.SetFocus;
    Exit;
  end;
  Destino := EdDestino.ValueVariant;
  if Destino = 0 then
  begin
    Geral.MB_Aviso('Defina a carteira de destino!');
    EdDestino.SetFocus;
    Exit;
  end;
  if Origem = Destino then
  begin
    Geral.MB_Aviso('A carteira de origem e destino n�o podem ser a mesma!');
    EdDestino.SetFocus;
    Exit;
  end;
  Valor := Geral.DMV(EdValor.Text);
  if Valor <= 0 then
  begin
    Geral.MB_Aviso('Defina um valor positivo, maior que zero.');
    EdValor.SetFocus;
    Exit;
  end;
  if ImgTipo.SQLType = stUpd then
  begin
    if ImpedePelaMigracao() then Exit;
    OldData1     := TPOldData1.Date;
    OldTipo1     := EdOldTipo1.ValueVariant;
    OldCarteira1 := EdOldCarteira1.ValueVariant;
    OldControle1 := EdOldControle1.ValueVariant;
    OldSub1      := EdOldSub1.ValueVariant;
    Debito       := EdValor.ValueVariant;
    Credito      := 0;
    SerieCH      := EdSerieChDeb.Text;
    Documento    := EdDocDeb.ValueVariant;
    Vencimento   := Data;
    Carteira     := EdOrigem.ValueVariant;
    Tipo         := QrOrigemTipo.Value;
    Duplicata    := EdDuplicataDeb.Text;
    //
    UMyMod.LctUpd(Dmod.QrUpd, FTabLctA, [
    'Carteira', 'Tipo',
    'Debito', 'Credito', 'SerieCH',
    'Documento', 'Vencimento', 'Data',
    'Duplicata'], [
    Carteira, Tipo,
    Debito, Credito, SerieCH,
    Documento, Vencimento, Data,
    Duplicata],
    OldData1, OldTipo1, OldCarteira1, OldControle1, OldSub1);
    //
    OldData2     := TPOldData2.Date;
    OldTipo2     := EdOldTipo2.ValueVariant;
    OldCarteira2 := EdOldCarteira2.ValueVariant;
    OldControle2 := EdOldControle2.ValueVariant;
    OldSub2      := EdOldSub2.ValueVariant;
    Debito       := 0;
    Credito      := EdValor.ValueVariant;
    SerieCH      := EdSerieCHCred.Text;
    Documento    := EdDocCred.ValueVariant;
    Vencimento   := Data;
    Carteira     := EdDestino.ValueVariant;
    Tipo         := QrDestinoTipo.Value;
    Duplicata    := EdDuplicataCred.Text;
    //
    UMyMod.LctUpd(Dmod.QrUpd, FTabLctA, [
    'Carteira', 'Tipo',
    'Debito', 'Credito', 'SerieCH',
    'Documento', 'Vencimento', 'Data',
    'Duplicata'], [
    Carteira, Tipo,
    Debito, Credito, SerieCH,
    Documento, Vencimento, Data,
    Duplicata],
    OldData2, OldTipo2, OldCarteira2, OldControle2, OldSub2);
    //
    DmodFin.QrTransf.First;
    while not DmodFin.QrTransf.Eof do
    begin
      Localiza := False;
      if FQrCrt <> nil then
      begin
        if (DmodFin.QrTransfTipo.Value = FQrCrt.FieldByName('Tipo').AsInteger)
        and (DmodFin.QrTransfCarteira.Value = FQrCrt.FieldByName('Codigo').AsInteger)
        then Localiza := True;
      end;
      UFinanceiro.RecalcSaldoCarteira(DmodFin.QrTransfCarteira.Value,
        FQrCrt, FQrLct, Localiza, Localiza);
      DmodFin.QrTransf.Next;
    end;
    //Controle := Codigo;
  end else
  begin
    Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle', FTabLctA, LAN_CTOS, 'Controle');
    //
    VAR_LANCTO2 := Controle;
    if QrOrigemTipo.Value = 2 then Sit := 0 else Sit := 2;

    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO ' + FTabLctA + ' SET Controle=:P0, Documento=:P1, ');
    Dmod.QrUpdU.SQL.Add('Data=:P2, Carteira=:P3, Tipo=:P4, Credito=:P5, ');
    Dmod.QrUpdU.SQL.Add('Debito=:P6, Genero=:P7, Descricao=:P8, ');
    Dmod.QrUpdU.SQL.Add('Vencimento=:P9, Sit=:P10, Sub=:P11, Compensado=:P12, ');
    Dmod.QrUpdU.SQL.Add('SerieCH=:P13, FatID=:P14, FatNum=:P15, CliInt=:P16, ');
    Dmod.QrUpdU.SQL.Add('FatID_Sub=:P17, Duplicata=:P18, ');
    Dmod.QrUpdU.SQL.Add('DataCad=:Pa, UserCad=:Pb');
    Dmod.QrUpdU.Params[00].AsFloat   := Controle;
    Dmod.QrUpdU.Params[01].AsFloat   := Geral.DMV(EdDocDeb.Text);
    Dmod.QrUpdU.Params[02].AsString  := Data;
    Dmod.QrUpdU.Params[03].AsInteger := Origem;
    Dmod.QrUpdU.Params[04].AsInteger := QrOrigemTipo.Value;
    Dmod.QrUpdU.Params[05].AsFloat   := 0;
    Dmod.QrUpdU.Params[06].AsFloat   := Valor;
    Dmod.QrUpdU.Params[07].AsInteger := -1;
    Dmod.QrUpdU.Params[08].AsString  := FIN_MSG_TRANSFERENCIA+FIN_MSG_PARA+CBDestino.Text;
    Dmod.QrUpdU.Params[09].AsString  := Data;
    Dmod.QrUpdU.Params[10].AsInteger := Sit;
    Dmod.QrUpdU.Params[11].AsInteger := 1;
    Dmod.QrUpdU.Params[12].AsString  := Data;
    Dmod.QrUpdU.Params[13].AsString  := EdSerieChDeb.Text;
    Dmod.QrUpdU.Params[14].AsInteger := FFatID;
    Dmod.QrUpdU.Params[15].AsInteger := FFatNum;
    Dmod.QrUpdU.Params[16].AsInteger := F_CliInt;
    Dmod.QrUpdU.Params[17].AsInteger := FFatID_Sub;
    Dmod.QrUpdU.Params[18].AsString  := EdDuplicataDeb.Text;
    //
    Dmod.QrUpdU.Params[19].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[20].AsInteger := VAR_USUARIO;
    Dmod.QrUpdU.ExecSQL;
    //
    if QrDestinoTipo.Value = 2 then Sit := 0 else Sit := 2;
    Dmod.QrUpdU.Params[00].AsFloat   := Controle;
    Dmod.QrUpdU.Params[01].AsFloat   := Geral.DMV(EdDocCred.Text);
    Dmod.QrUpdU.Params[02].AsString  := Data;
    Dmod.QrUpdU.Params[03].AsInteger := Destino;
    Dmod.QrUpdU.Params[04].AsInteger := QrDestinoTipo.Value;
    Dmod.QrUpdU.Params[05].AsFloat   := Valor;
    Dmod.QrUpdU.Params[06].AsFloat   := 0;
    Dmod.QrUpdU.Params[07].AsInteger := -1;
    Dmod.QrUpdU.Params[08].AsString  := FIN_MSG_TRANSFERENCIA+FIN_MSG_DE+CBOrigem.Text;
    Dmod.QrUpdU.Params[09].AsString  := Data;
    Dmod.QrUpdU.Params[10].AsInteger := Sit;
    Dmod.QrUpdU.Params[11].AsInteger := 2;
    Dmod.QrUpdU.Params[12].AsString  := Data;
    Dmod.QrUpdU.Params[13].AsString  := EdSerieChCred.Text;
    Dmod.QrUpdU.Params[14].AsInteger := FFatID;
    Dmod.QrUpdU.Params[15].AsInteger := FFatNum;
    Dmod.QrUpdU.Params[16].AsInteger := F_CliInt;
    Dmod.QrUpdU.Params[17].AsInteger := FFatID_Sub;
    Dmod.QrUpdU.Params[18].AsString  := EdDuplicataCred.Text;
    //
    Dmod.QrUpdU.Params[19].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[20].AsInteger := VAR_USUARIO;
    Dmod.QrUpdU.ExecSQL;
  end;
  //
  UMyMod.LogDel(Dmod.MyDB, 3, Codigo);
  Localiza := False;
  if FQrCrt <> nil then
  begin
    if (QrOrigemTipo.Value = FQrCrt.FieldByName('Tipo').AsInteger)
    and (CBOrigem.KeyValue = FQrCrt.FieldByName('Codigo').AsInteger)
    then Localiza := True;
  end;
  UFinanceiro.RecalcSaldoCarteira(CBOrigem.KeyValue,
    QrOrigem, FQrLct, Localiza, Localiza);
  //
  Localiza := False;
  if FQrCrt <> nil then
  begin
    if (QrDestinoTipo.Value = FQrCrt.FieldByName('Tipo').AsInteger)
    and (CBDestino.KeyValue = FQrCrt.FieldByName('Codigo').AsInteger)
    then Localiza := True;
  end;
  UFinanceiro.RecalcSaldoCarteira(CBDestino.KeyValue,
    QrDestino, FQrLct, Localiza, Localiza);
  //
  VAR_DATAINSERIR := TPData.Date;
  Close;
end;

procedure TFmTransfer2.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTransfer2.SbNumeroClick(Sender: TObject);
begin
//  LaRegistro.Text := GOTOy.Codigo(QrGruposCodigo.Value, LaRegistro.Text);
end;

procedure TFmTransfer2.SbNomeClick(Sender: TObject);
begin
//  LaRegistro.Text := GOTOy.Nome(LaRegistro.Text);
end;

procedure TFmTransfer2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdValor.SetFocus;
  if UFinanceiro.TabLctNaoDefinida(FTabLctA, 'TFmTransfer2.FormActivate()') then
    Close;
end;

procedure TFmTransfer2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPData.MinDate := VAR_DATA_MINIMA;
end;

procedure TFmTransfer2.SbQueryClick(Sender: TObject);
begin
//  GOTOy.LocalizaCodigo(QrGruposCodigo.Value,
//  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, CO_Grupos, Dmod.MyDB));
end;

procedure TFmTransfer2.BtExcluiClick(Sender: TObject);
var
  Localiza: Boolean;
  //Controle: Double;
begin
  if ImpedePelaMigracao() then
    Exit;
  UFinanceiro.AtzDataAtzSdoPlaCta(F_CliInt, DmodFin.QrTransfData.Value);
  if Geral.MB_Pergunta('Confirma a exclus�o desta transfer�ncia?') = ID_YES then
  begin
    if FQrLct <> nil then
      VAR_LANCTO2 := FQrLct.FieldByName('Controle').AsInteger
    else
      VAR_LANCTO2 := 0;
    //
    UFinanceiro.ExcluiLct_Unico(FTabLctA, QrOrigem.Database, TPOldData1.Date,
      EdOldTipo1.ValueVariant, EdOldCarteira1.ValueVariant,
      EdOldControle1.ValueVariant, EdOldSub1.ValueVariant,
      dmkPF.MotivDel_ValidaCodigo(302), False);
    UFinanceiro.ExcluiLct_Unico(FTabLctA, QrOrigem.Database, TPOldData2.Date,
      EdOldTipo2.ValueVariant, EdOldCarteira2.ValueVariant,
      EdOldControle2.ValueVariant, EdOldSub2.ValueVariant,
      dmkPF.MotivDel_ValidaCodigo(302), False);
    //
    DmodFin.QrTransf.First;
    while not DmodFin.QrTransf.Eof do
    begin
      Localiza := False;
      if FQrCrt <> nil then
      begin
        if (DmodFin.QrTransfTipo.Value = FQrCrt.FieldByName('Tipo').AsInteger)
        and (DmodFin.QrTransfCarteira.Value = FQrCrt.FieldByName('Codigo').AsInteger)
        then Localiza := True;
      end;
      UFinanceiro.RecalcSaldoCarteira(DmodFin.QrTransfCarteira.Value,
        FQrCrt, FQrLct, Localiza, Localiza);
      DmodFin.QrTransf.Next;
    end;
    Close;
  end;
end;

procedure TFmTransfer2.EdDuplicataDebChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    EdDuplicataCred.Text := EdDuplicataDeb.Text;
end;

procedure TFmTransfer2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmTransfer2.ImpedePelaMigracao(): Boolean;
begin
  Result := False;
  // Parei aqui 2010-09-03
  // Falta fazer
  // verificar se um dos dois lan ctos j� n�o foi migrado
  // Fazer o qu� neste caso?
end;

end.

