unit TransferCtas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  ZCF2, ResIntStrings, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  UnGOTOy, UnMySQLCuringa, ComCtrls, (*DBIProcs,*) UMySQLModule, mySQLDbTables,
  dmkEdit, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, dmkPermissoes,
  dmkGeral, dmkImage, UnDmkEnums;

type
  TFmTransferCtas = class(TForm)
    PainelDados: TPanel;
    QrOrigem: TmySQLQuery;
    DsOrigem: TDataSource;
    DsDestino: TDataSource;
    QrOrigemCodigo: TIntegerField;
    QrOrigemNome: TWideStringField;
    QrDestino: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    QrOrigemTipo: TIntegerField;
    QrDestinoTipo: TIntegerField;
    Panel1: TPanel;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    Label8: TLabel;
    Label7: TLabel;
    EdValor: TdmkEdit;
    GBDebt: TGroupBox;
    EdOrigem: TdmkEditCB;
    EdDocDeb: TdmkEdit;
    Label5: TLabel;
    Label2: TLabel;
    EdSerieChDeb: TdmkEdit;
    CBOrigem: TdmkDBLookupComboBox;
    LaConjunto: TLabel;
    GBCred: TGroupBox;
    Label4: TLabel;
    EdDestino: TdmkEditCB;
    CBDestino: TdmkDBLookupComboBox;
    EdSerieCHCred: TdmkEdit;
    Label3: TLabel;
    Label6: TLabel;
    EdDocCred: TdmkEdit;
    Label9: TLabel;
    EdCtaDebt: TdmkEditCB;
    CBCtaDebt: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdCtaCred: TdmkEditCB;
    CBCtaCred: TdmkDBLookupComboBox;
    QrCtaDebt: TmySQLQuery;
    DsCtaDebt: TDataSource;
    QrCtaDebtCodigo: TIntegerField;
    QrCtaDebtNome: TWideStringField;
    QrCtaCred: TmySQLQuery;
    DsCtaCred: TDataSource;
    QrCtaCredCodigo: TIntegerField;
    QrCtaCredNome: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EdOldControle1: TdmkEdit;
    TPOldData1: TdmkEditDateTimePicker;
    EdOldSub1: TdmkEdit;
    EdOldTipo1: TdmkEdit;
    EdOldCarteira1: TdmkEdit;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdOldControle2: TdmkEdit;
    TPOldData2: TdmkEditDateTimePicker;
    EdOldSub2: TdmkEdit;
    EdOldTipo2: TdmkEdit;
    EdOldCarteira2: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel4: TPanel;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    BtConfirma: TBitBtn;
    BtExclui: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTabLctA: String;
    F_CliInt: Integer;
    FQrCrt, FQrLct: TmySQLQuery;
  end;

var
  FmTransferCtas: TFmTransferCtas;

implementation

uses UnMyObjects, Module, Principal, UnFinanceiro, ModuleFin, DmkDAC_PF,
  UnDmkProcFunc;

{$R *.DFM}

procedure TFmTransferCtas.BtConfirmaClick(Sender: TObject);
var
  OldData1, Olddata2: TDate;
  OldTipo1, OldCarteira1, OldControle1, OldSub1,
  OldTipo2, OldCarteira2, OldControle2, OldSub2,
  Carteira, Tipo, Genero: Integer;
  Debito, Credito, Documento: Double;
  SerieCH, Vencimento: String;
  //
  Codigo, Origem, Destino, Sit, CtaDebt, CtaCred: Integer;
  Localiza: Boolean;
  Controle: Int64;
  Valor: Double;
  Data: String;
begin
  Localiza := False;
  Data := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  UFinanceiro.AtzDataAtzSdoPlaCta(F_CliInt, TPData.Date);
  Codigo := EdCodigo.ValueVariant;
  if (Codigo = 0) and (ImgTipo.SQLType = stUpd) then
  begin
    Geral.MB_Aviso('Altera��o imposs�vel sem o n� do lan�amento!');
    Exit;
  end;

  //  CARTEIRAS
  Origem := EdOrigem.ValueVariant;
  if Origem = 0 then
  begin
    Geral.MB_Aviso('Defina a carteira de origem!');
    EdOrigem.SetFocus;
    Exit;
  end;
  Destino := EdDestino.ValueVariant;
  if Destino = 0 then
  begin
    Geral.MB_Aviso('Defina a carteira de destino!');
    EdDestino.SetFocus;
    Exit;
  end;
  {
  if Origem = Destino then
  begin
    Geral.MB_Aviso('As carteiras de origem e destino n�o podem ser as mesmas!');
    EdDestino.SetFocus;
    Exit;
  end;
  }
  //  CONTAS
  CtaDebt := EdCtaDebt.ValueVariant;
  if CtaDebt = 0 then
  begin
    Geral.MB_Aviso('Defina a conta para d�bito!');
    EdCtaDebt.SetFocus;
    Exit;
  end;
  CtaCred := EdCtaCred.ValueVariant;
  if CtaCred = 0 then
  begin
    Geral.MB_Aviso('Defina a conta para cr�dito!');
    EdCtaCred.SetFocus;
    Exit;
  end;
  if CtaDebt = CtaCred then
  begin
    Geral.MB_Aviso('Aa contas de d�bito e cr�dito n�o podem ser as mesmas!');
    EdCtaCred.SetFocus;
    Exit;
  end;


  Valor := Geral.DMV(EdValor.Text);
  if Valor <= 0 then
  begin
    Geral.MB_Aviso('Defina um valor positivo, maior que zero.');
    EdValor.SetFocus;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  if ImgTipo.SQLType = stUpd then
  begin
    OldData1     := TPOldData1.Date;
    OldTipo1     := EdOldTipo1.ValueVariant;
    OldCarteira1 := EdOldCarteira1.ValueVariant;
    OldControle1 := EdOldControle1.ValueVariant;
    OldSub1      := EdOldSub1.ValueVariant;
    Debito       := EdValor.ValueVariant;
    Credito      := 0;
    SerieCH      := EdSerieChDeb.Text;
    Documento    := EdDocDeb.ValueVariant;
    Vencimento   := Data;
    Carteira     := EdOrigem.ValueVariant;
    Tipo         := QrOrigemTipo.Value;
    Genero       := EdCtaDebt.ValueVariant;
    //
    UMyMod.LctUpd(Dmod.QrUpd, FtabLctA, [
    'Genero', 'Carteira', 'Tipo',
    'Debito', 'Credito', 'SerieCH',
    'Documento', 'Vencimento', 'Data'], [
    Genero, Carteira, Tipo,
    Debito, Credito, SerieCH,
    Documento, Vencimento, Data],
    OldData1, OldTipo1, OldCarteira1, OldControle1, OldSub1);
    //
    OldData2     := TPOldData2.Date;
    OldTipo2     := EdOldTipo2.ValueVariant;
    OldCarteira2 := EdOldCarteira2.ValueVariant;
    OldControle2 := EdOldControle2.ValueVariant;
    OldSub2      := EdOldSub2.ValueVariant;
    Debito       := 0;
    Credito      := EdValor.ValueVariant;
    SerieCH      := EdSerieCHCred.Text;
    Documento    := EdDocCred.ValueVariant;
    Vencimento   := Data;
    Carteira     := EdDestino.ValueVariant;
    Tipo         := QrDestinoTipo.Value;
    Genero       := EdCtaCred.ValueVariant;
    //
    UMyMod.LctUpd(Dmod.QrUpd, FTabLctA, [
    'Genero', 'Carteira', 'Tipo',
    'Debito', 'Credito', 'SerieCH',
    'Documento', 'Vencimento', 'Data'], [
    Genero, Carteira, Tipo,
    Debito, Credito, SerieCH,
    Documento, Vencimento, Data],
    OldData2, OldTipo2, OldCarteira2, OldControle2, OldSub2);
    //
    DmodFin.QrTrfCtas.First;
    while not DmodFin.QrTrfCtas.Eof do
    begin
      if FQrCrt <> nil then
      begin
        if (DmodFin.QrTrfCtasTipo.Value = FQrCrt.FieldByName('Tipo').AsInteger)
        and (DmodFin.QrTrfCtasCarteira.Value = FQrCrt.FieldByName('Codigo').AsInteger)
        then Localiza := True;
      end;
      UFinanceiro.RecalcSaldoCarteira(DmodFin.QrTrfCtasCarteira.Value,
        FQrCrt, FQrLct, Localiza, Localiza);
      DmodFin.QrTrfCtas.Next;
    end;
    //Controle := Codigo;
  end else
  begin
    Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle',
      FTabLctA, LAN_CTOS, 'Controle');
    //
    VAR_LANCTO2 := Controle;
    if QrOrigemTipo.Value = 2 then Sit := 0 else Sit := 2;

    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO ' + FTabLctA + ' SET AlterWeb=1, Controle=:P0, ');
    Dmod.QrUpdU.SQL.Add('Documento=:P1, Data=:P2, Carteira=:P3, Tipo=:P4, ');
    Dmod.QrUpdU.SQL.Add('Credito=:P5, Debito=:P6, Genero=:P7, Descricao=:P8, ');
    Dmod.QrUpdU.SQL.Add('Vencimento=:P9, Sit=:P10, Sub=:P11, Compensado=:P12, ');
    Dmod.QrUpdU.SQL.Add('SerieCH=:P13, FatID=:P14, FatNum=:P15, CliInt=:P16, ');
    Dmod.QrUpdU.SQL.Add('FatID_Sub=:P17, DataCad=:Pa, UserCad=:Pb');
    Dmod.QrUpdU.Params[00].AsFloat   := Controle;
    Dmod.QrUpdU.Params[01].AsFloat   := Geral.DMV(EdDocDeb.Text);
    Dmod.QrUpdU.Params[02].AsString  := Data;
    Dmod.QrUpdU.Params[03].AsInteger := Origem;
    Dmod.QrUpdU.Params[04].AsInteger := QrOrigemTipo.Value;
    Dmod.QrUpdU.Params[05].AsFloat   := 0;
    Dmod.QrUpdU.Params[06].AsFloat   := Valor;
    Dmod.QrUpdU.Params[07].AsInteger := CtaDebt;
    Dmod.QrUpdU.Params[08].AsString  := FIN_MSG_TRANSFERENCIA + FIN_MSG_PARA + CBCtaCred.Text;
    Dmod.QrUpdU.Params[09].AsString  := Data;
    Dmod.QrUpdU.Params[10].AsInteger := Sit;
    Dmod.QrUpdU.Params[11].AsInteger := 1;
    Dmod.QrUpdU.Params[12].AsString  := Data;
    Dmod.QrUpdU.Params[13].AsString  := EdSerieChDeb.Text;
    Dmod.QrUpdU.Params[14].AsInteger := -1;
    Dmod.QrUpdU.Params[15].AsInteger := 0;
    Dmod.QrUpdU.Params[16].AsInteger := F_CliInt;
    Dmod.QrUpdU.Params[17].AsInteger := 0;
    //
    Dmod.QrUpdU.Params[18].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[19].AsInteger := VAR_USUARIO;
    Dmod.QrUpdU.ExecSQL;
    //
    if QrDestinoTipo.Value = 2 then Sit := 0 else Sit := 2;
    Dmod.QrUpdU.Params[00].AsFloat   := Controle;
    Dmod.QrUpdU.Params[01].AsFloat   := Geral.DMV(EdDocCred.Text);
    Dmod.QrUpdU.Params[02].AsString  := Data;
    Dmod.QrUpdU.Params[03].AsInteger := Destino;
    Dmod.QrUpdU.Params[04].AsInteger := QrDestinoTipo.Value;
    Dmod.QrUpdU.Params[05].AsFloat   := Valor;
    Dmod.QrUpdU.Params[06].AsFloat   := 0;
    Dmod.QrUpdU.Params[07].AsInteger := CtaCred;
    Dmod.QrUpdU.Params[08].AsString  := FIN_MSG_TRANSFERENCIA + FIN_MSG_DE + CBCtaDebt.Text;
    Dmod.QrUpdU.Params[09].AsString  := Data;
    Dmod.QrUpdU.Params[10].AsInteger := Sit;
    Dmod.QrUpdU.Params[11].AsInteger := 2;
    Dmod.QrUpdU.Params[12].AsString  := Data;
    Dmod.QrUpdU.Params[13].AsString  := EdSerieChCred.Text;
    Dmod.QrUpdU.Params[14].AsInteger := -1;
    Dmod.QrUpdU.Params[15].AsInteger := 0;
    Dmod.QrUpdU.Params[16].AsInteger := F_CliInt;
    Dmod.QrUpdU.Params[17].AsInteger := 0;
    //
    Dmod.QrUpdU.Params[18].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[19].AsInteger := VAR_USUARIO;
    Dmod.QrUpdU.ExecSQL;
  end;
  //
  UMyMod.LogDel(Dmod.MyDB, 3, Codigo);
  if FQrCrt <> nil then
  begin
    if (QrOrigemTipo.Value = FQrCrt.FieldByName('Tipo').AsInteger)
    and (CBOrigem.KeyValue = FQrCrt.FieldByName('Codigo').AsInteger)
    then Localiza := True;
  end;
  UFinanceiro.RecalcSaldoCarteira(CBOrigem.KeyValue,
    QrOrigem, FQrLct, Localiza, Localiza);
  //
  if FQrCrt <> nil then
  begin
    if (QrDestinoTipo.Value = FQrCrt.FieldByName('Tipo').AsInteger)
    and (CBDestino.KeyValue = FQrCrt.FieldByName('Codigo').AsInteger)
    then Localiza := True;
  end;
  UFinanceiro.RecalcSaldoCarteira(CBDestino.KeyValue,
    QrDestino, FQrLct, Localiza, Localiza);
  //
  VAR_DATAINSERIR := TPData.Date;
  Screen.Cursor := crDefault;
  Close;
end;

procedure TFmTransferCtas.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTransferCtas.SbNumeroClick(Sender: TObject);
begin
//  LaRegistro.Text := GOTOy.Codigo(QrGruposCodigo.Value, LaRegistro.Text);
end;

procedure TFmTransferCtas.SbNomeClick(Sender: TObject);
begin
//  LaRegistro.Text := GOTOy.Nome(LaRegistro.Text);
end;

procedure TFmTransferCtas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdValor.SetFocus;
  if UFinanceiro.TabLctNaoDefinida(FTabLctA, 'TFmTransferCtas.FormActivate()') then
    Close;
end;

procedure TFmTransferCtas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrCtaDebt.Close;
  UnDmkDAC_PF.AbreQuery(QrCtaDebt, Dmod.MyDB);
  //
  QrCtaCred.Close;
  UnDmkDAC_PF.AbreQuery(QrCtaCred, Dmod.MyDB);
end;

procedure TFmTransferCtas.SbQueryClick(Sender: TObject);
begin
//  GOTOy.LocalizaCodigo(QrGruposCodigo.Value,
//  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, CO_Grupos, Dmod.MyDB));
end;

procedure TFmTransferCtas.BtExcluiClick(Sender: TObject);
var
  Localiza: Boolean;
begin
  Localiza := False;
  UFinanceiro.AtzDataAtzSdoPlaCta(F_CliInt, DmodFin.QrTrfCtasData.Value);
  if Geral.MB_Pergunta('Confirma a exclus�o desta transfer�ncia?') = ID_YES then
  begin
    if FQrLct <> nil then
      VAR_LANCTO2 := FQrLct.FieldByName('Controle').AsInteger
    else
      VAR_LANCTO2 := 0;
    DmodFin.QrTrfCtas.First;
    while not DmodFin.QrTrfCtas.Eof do
    begin
      UFinanceiro.ExcluiLct_Unico(FTabLctA, QrOrigem.Database, TPOldData1.Date,
        EdOldTipo1.ValueVariant, EdOldCarteira1.ValueVariant,
        EdOldControle1.ValueVariant, EdOldSub1.ValueVAriant,
        dmkPF.MotivDel_ValidaCodigo(303), False);
      UFinanceiro.ExcluiLct_Unico(FTabLctA, QrOrigem.Database, TPOldData2.Date,
        EdOldTipo2.ValueVariant, EdOldCarteira2.ValueVariant,
        EdOldControle2.ValueVariant, EdOldSub2.ValueVAriant,
        dmkPF.MotivDel_ValidaCodigo(303), False);
      //
      if FQrCrt <> nil then
      begin
        if (DmodFin.QrTrfCtasTipo.Value = FQrCrt.FieldByName('Tipo').AsInteger)
        and (DmodFin.QrTrfCtasCarteira.Value = FQrCrt.FieldByName('Codigo').AsInteger)
        then Localiza := True else Localiza := False;
      end;
      UFinanceiro.RecalcSaldoCarteira(DmodFin.QrTrfCtasCarteira.Value,
        nil, nil, Localiza, Localiza);
      DmodFin.QrTrfCtas.Next;
    end;
    Close;
  end;
end;

procedure TFmTransferCtas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

