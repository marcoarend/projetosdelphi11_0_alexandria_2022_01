unit ExpContab1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, Grids, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmExpContab1 = class(TForm)
    PainelDados: TPanel;
    StaticText3: TStaticText;
    EdDiaI: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdDiaF: TdmkEdit;
    Label4: TLabel;
    EdDiaP: TdmkEdit;
    StaticText1: TStaticText;
    EdMesI: TdmkEdit;
    EdMesF: TdmkEdit;
    EdMesP: TdmkEdit;
    StaticText2: TStaticText;
    EdDevI: TdmkEdit;
    EdDevF: TdmkEdit;
    EdDevP: TdmkEdit;
    StaticText4: TStaticText;
    EdCreI: TdmkEdit;
    EdCreF: TdmkEdit;
    EdCreP: TdmkEdit;
    StaticText5: TStaticText;
    EdComI: TdmkEdit;
    EdComF: TdmkEdit;
    EdComP: TdmkEdit;
    StaticText6: TStaticText;
    EdValI: TdmkEdit;
    EdValF: TdmkEdit;
    EdValP: TdmkEdit;
    QrExpContab: TmySQLQuery;
    DsExpContab: TDataSource;
    QrExpContabDiaI: TIntegerField;
    QrExpContabDiaF: TIntegerField;
    QrExpContabDiaP: TWideStringField;
    QrExpContabMesI: TIntegerField;
    QrExpContabMesF: TIntegerField;
    QrExpContabMesP: TWideStringField;
    QrExpContabDevI: TIntegerField;
    QrExpContabDevF: TIntegerField;
    QrExpContabDevP: TWideStringField;
    QrExpContabCreI: TIntegerField;
    QrExpContabCreF: TIntegerField;
    QrExpContabCreP: TWideStringField;
    QrExpContabComI: TIntegerField;
    QrExpContabComF: TIntegerField;
    QrExpContabComP: TWideStringField;
    QrExpContabValI: TIntegerField;
    QrExpContabValF: TIntegerField;
    QrExpContabValP: TWideStringField;
    StaticText7: TStaticText;
    EdDocI: TdmkEdit;
    EdDocF: TdmkEdit;
    EdDocP: TdmkEdit;
    QrExpContabDocI: TIntegerField;
    QrExpContabDocF: TIntegerField;
    QrExpContabDocP: TWideStringField;
    Label5: TLabel;
    EdCartPadr: TdmkEditCB;
    CBCartPadr: TdmkDBLookupComboBox;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrExpContabCartPadr: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    PnOK: TPanel;
    BtOK: TBitBtn;
    PnCores: TPanel;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdDiaIExit(Sender: TObject);
    procedure EdDiaFExit(Sender: TObject);
    procedure EdMesIExit(Sender: TObject);
    procedure EdMesFExit(Sender: TObject);
    procedure EdDevIExit(Sender: TObject);
    procedure EdDevFExit(Sender: TObject);
    procedure EdCreIExit(Sender: TObject);
    procedure EdCreFExit(Sender: TObject);
    procedure EdComIExit(Sender: TObject);
    procedure EdComFExit(Sender: TObject);
    procedure EdValIExit(Sender: TObject);
    procedure EdValFExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdDocIExit(Sender: TObject);
    procedure EdDocFExit(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    FArrPos: array[1..1024] of Integer;
    LShape: array[1..1024] of TShape;
    procedure DesenhaGrid;
  public
    { Public declarations }
  end;

  var
  FmExpContab1: TFmExpContab1;

implementation

{$R *.DFM}

uses UnMyObjects, Module;

procedure TFmExpContab1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmExpContab1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  //
  DesenhaGrid;
end;

procedure TFmExpContab1.EdDiaIExit(Sender: TObject);
begin
  EdDiaI.Text := Geral.TFT_NULL(EdDiaI.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.EdDiaFExit(Sender: TObject);
begin
  EdDiaF.Text := Geral.TFT_NULL(EdDiaF.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.EdMesIExit(Sender: TObject);
begin
  EdMesI.Text := Geral.TFT_NULL(EdMesI.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.EdMesFExit(Sender: TObject);
begin
  EdMesF.Text := Geral.TFT_NULL(EdMesF.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.EdDevIExit(Sender: TObject);
begin
  EdDevI.Text := Geral.TFT_NULL(EdDevI.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.EdDevFExit(Sender: TObject);
begin
  EdDevF.Text := Geral.TFT_NULL(EdDevF.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.EdCreIExit(Sender: TObject);
begin
  EdCreI.Text := Geral.TFT_NULL(EdCreI.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.EdCreFExit(Sender: TObject);
begin
  EdCreF.Text := Geral.TFT_NULL(EdCreF.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.EdComIExit(Sender: TObject);
begin
  EdComI.Text := Geral.TFT_NULL(EdComI.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.EdComFExit(Sender: TObject);
begin
  EdComF.Text := Geral.TFT_NULL(EdComF.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.EdValIExit(Sender: TObject);
begin
  EdValI.Text := Geral.TFT_NULL(EdValI.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.EdValFExit(Sender: TObject);
begin
  EdValF.Text := Geral.TFT_NULL(EdValF.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.EdDocIExit(Sender: TObject);
begin
  EdDocI.Text := Geral.TFT_NULL(EdDocI.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.EdDocFExit(Sender: TObject);
begin
  EdDocF.Text := Geral.TFT_NULL(EdDocF.Text, 0, siPositivo);
  DesenhaGrid;
end;

procedure TFmExpContab1.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  ImgTipo.SQLType := stUpd;
  //
  for i := 1 to 1024 do
  begin
    LShape[i] := TShape.Create(Self);
    LShape[i].Parent  := PnCores;
    LShape[i].Width   := 3;
    LShape[i].Height  := 3;
    LShape[i].Top     := 0;
    LShape[i].Left    := 0;
    LShape[i].Visible := False;
  end;
  QrExpContab.Open;
  QrCarteiras.Open;
  if QrExpContab.RecordCount > 0 then
  begin
    EdDiaI.Text := Geral.TFT(IntToStr(QrExpContabDiaI.Value), 0, siPositivo);
    EdDiaF.Text := Geral.TFT(IntToStr(QrExpContabDiaF.Value), 0, siPositivo);
    EdDiaP.Text := QrExpContabDiaP.Value;
    //
    EdMesI.Text := Geral.TFT(IntToStr(QrExpContabMesI.Value), 0, siPositivo);
    EdMesF.Text := Geral.TFT(IntToStr(QrExpContabMesF.Value), 0, siPositivo);
    EdMesP.Text := QrExpContabMesP.Value;
    //
    EdDevI.Text := Geral.TFT(IntToStr(QrExpContabDevI.Value), 0, siPositivo);
    EdDevF.Text := Geral.TFT(IntToStr(QrExpContabDevF.Value), 0, siPositivo);
    EdDevP.Text := QrExpContabDevP.Value;
    //
    EdCreI.Text := Geral.TFT(IntToStr(QrExpContabCreI.Value), 0, siPositivo);
    EdCreF.Text := Geral.TFT(IntToStr(QrExpContabCreF.Value), 0, siPositivo);
    EdCreP.Text := QrExpContabCreP.Value;
    //
    EdComI.Text := Geral.TFT(IntToStr(QrExpContabComI.Value), 0, siPositivo);
    EdComF.Text := Geral.TFT(IntToStr(QrExpContabComF.Value), 0, siPositivo);
    EdComP.Text := QrExpContabComP.Value;
    //
    EdValI.Text := Geral.TFT(IntToStr(QrExpContabValI.Value), 0, siPositivo);
    EdValF.Text := Geral.TFT(IntToStr(QrExpContabValF.Value), 0, siPositivo);
    EdValP.Text := QrExpContabValP.Value;
    //
    EdDocI.Text := Geral.TFT(IntToStr(QrExpContabDocI.Value), 0, siPositivo);
    EdDocF.Text := Geral.TFT(IntToStr(QrExpContabDocF.Value), 0, siPositivo);
    EdDocP.Text := QrExpContabDocP.Value;
    //
    EdCartPadr.Text := IntToStr(QrExpContabCartPadr.Value);
    CBCartPadr.KeyValue := QrExpContabCartPadr.Value;
  end;
end;

procedure TFmExpContab1.BtOKClick(Sender: TObject);
var
  i, p: Integer;
begin
  Dmod.QrUpd.SQL.Clear;
  p := 0;
  for i := 1 to 1024 do
  begin
    if LShape[i].Visible then
      if LShape[i].Brush.Color = clRed then inc(p, 1);
  end;
  if p > 0 then
  begin
    Application.MessageBox(PChar('Existem '+IntToStr(p)+' posi��es com mais ' +
    'de um item!'), 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if QrExpContab.RecordCount = 0 then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO expcontab SET');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE expcontab SET');
  end;
  Dmod.QrUpd.SQL.Add('DiaI=:P00, DiaF=:P01, DiaP=:P02, ');
  Dmod.QrUpd.SQL.Add('MesI=:P03, MesF=:P04, MesP=:P05, ');
  Dmod.QrUpd.SQL.Add('DevI=:P06, DevF=:P07, DevP=:P08, ');
  Dmod.QrUpd.SQL.Add('CreI=:P09, CreF=:P10, CreP=:P11, ');
  Dmod.QrUpd.SQL.Add('ComI=:P12, ComF=:P13, ComP=:P14, ');
  Dmod.QrUpd.SQL.Add('ValI=:P15, ValF=:P16, ValP=:P17, ');
  Dmod.QrUpd.SQL.Add('DocI=:P18, DocF=:P19, DocP=:P20, ');
  //
  Dmod.QrUpd.SQL.Add('CartPadr=:Px');
  //
  Dmod.QrUpd.Params[00].AsInteger := Geral.IMV(EdDiaI.Text);
  Dmod.QrUpd.Params[01].AsInteger := Geral.IMV(EdDiaF.Text);
  Dmod.QrUpd.Params[02].AsString  := EdDiaP.Text;
  //
  Dmod.QrUpd.Params[03].AsInteger := Geral.IMV(EdMesI.Text);
  Dmod.QrUpd.Params[04].AsInteger := Geral.IMV(EdMesF.Text);
  Dmod.QrUpd.Params[05].AsString  := EdMesP.Text;
  //
  Dmod.QrUpd.Params[06].AsInteger := Geral.IMV(EdDevI.Text);
  Dmod.QrUpd.Params[07].AsInteger := Geral.IMV(EdDevF.Text);
  Dmod.QrUpd.Params[08].AsString  := EdDevP.Text;
  //
  Dmod.QrUpd.Params[09].AsInteger := Geral.IMV(EdCreI.Text);
  Dmod.QrUpd.Params[10].AsInteger := Geral.IMV(EdCreF.Text);
  Dmod.QrUpd.Params[11].AsString  := EdCreP.Text;
  //
  Dmod.QrUpd.Params[12].AsInteger := Geral.IMV(EdComI.Text);
  Dmod.QrUpd.Params[13].AsInteger := Geral.IMV(EdComF.Text);
  Dmod.QrUpd.Params[14].AsString  := EdComP.Text;
  //
  Dmod.QrUpd.Params[15].AsInteger := Geral.IMV(EdValI.Text);
  Dmod.QrUpd.Params[16].AsInteger := Geral.IMV(EdValF.Text);
  Dmod.QrUpd.Params[17].AsString  := EdValP.Text;
  //
  Dmod.QrUpd.Params[18].AsInteger := Geral.IMV(EdDocI.Text);
  Dmod.QrUpd.Params[19].AsInteger := Geral.IMV(EdDocF.Text);
  Dmod.QrUpd.Params[20].AsString  := EdDocP.Text;
  //
  Dmod.QrUpd.Params[21].AsInteger := Geral.IMV(EdCartPadr.Text);
  Dmod.QrUpd.ExecSQL;
  //
  Close;
end;

procedure TFmExpContab1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmExpContab1.DesenhaGrid;
var
  DiaI, DiaF, MesI, MesF, DevI, DevF, CreI, CreF, ComI, ComF, ValI, ValF,
  DocI, DocF, i, t, xy: Integer;
begin
  t := 0;
  for i := 1 to 1024 do FArrPos[i] := 0;
  //
  DiaI := Geral.IMV(EdDiaI.Text);
  DiaF := Geral.IMV(EdDiaF.Text);
  if (DiaI <> 0) and (DiaF <> 0) then
    if DiaI + DiaF - 1 > t then t := DiaI + DiaF -1;
  //
  MesI := Geral.IMV(EdMesI.Text);
  MesF := Geral.IMV(EdMesF.Text);
  if (MesI <> 0) and (MesF <> 0) then
    if MesI + MesF - 1 > t then t := MesI + MesF -1;
  //
  DevI := Geral.IMV(EdDevI.Text);
  DevF := Geral.IMV(EdDevF.Text);
  if (DevI <> 0) and (DevF <> 0) then
    if DevI + DevF - 1 > t then t := DevI + DevF -1;
  //
  CreI := Geral.IMV(EdCreI.Text);
  CreF := Geral.IMV(EdCreF.Text);
  if (CreI <> 0) and (CreF <> 0) then
    if CreI + CreF - 1 > t then t := CreI + CreF -1;
  //
  ComI := Geral.IMV(EdComI.Text);
  ComF := Geral.IMV(EdComF.Text);
  if (ComI <> 0) and (ComF <> 0) then
    if ComI + ComF - 1 > t then t := ComI + ComF -1;
  //
  ValI := Geral.IMV(EdValI.Text);
  ValF := Geral.IMV(EdValF.Text);
  if (ValI <> 0) and (ValF <> 0) then
    if ValI + ValF - 1 > t then t := ValI + ValF -1;
  //
  DocI := Geral.IMV(EdDocI.Text);
  DocF := Geral.IMV(EdDocF.Text);
  if (DocI <> 0) and (DocF <> 0) then
    if DocI + DocF - 1 > t then t := DocI + DocF -1;
  //
  if (DiaI > 0) and (DiaF > 0) and (DiaI + DiaF < 1025) then
    for i := DiaI to DiaI + DiaF -1 do FArrPos[i] := FArrPos[i] + 1;
  //
  if (MesI > 0) and (MesF > 0) and (MesI + MesF < 1025) then
    for i := MesI to MesI + MesF -1 do FArrPos[i] := FArrPos[i] + 1;
  //
  if (DevI > 0) and (DevF > 0) and (DevI + DevF < 1025) then
    for i := DevI to DevI + DevF -1 do FArrPos[i] := FArrPos[i] + 1;
  //
  if (CreI > 0) and (CreF > 0) and (CreI + CreF < 1025) then
    for i := CreI to CreI + CreF -1 do FArrPos[i] := FArrPos[i] + 1;
  //
  if (ComI > 0) and (ComF > 0) and (ComI + ComF < 1025) then
    for i := ComI to ComI + ComF -1 do FArrPos[i] := FArrPos[i] + 1;
  //
  if (ValI > 0) and (ValF > 0) and (ValI + ValF < 1025) then
    for i := ValI to ValI + ValF -1 do FArrPos[i] := FArrPos[i] + 1;
  //
  if (DocI > 0) and (DocF > 0) and (DocI + DocF < 1025) then
    for i := DocI to DocI + DocF -1 do FArrPos[i] := FArrPos[i] + 1;
  //
  if t <= 0 then xy := 1 else xy := PnCores.Width div t;
  if xy > 10 then xy := 10;
  PnCores.Height := xy;
  for i := 1 to 1024 do LShape[i].Visible := False;
  for i := 1 to t do
  begin
    if i <= 1024 then
    begin
     LShape[i].Left := (i - 1) * (xy-1);
     LShape[i].Height := xy;
     LShape[i].Width := xy;
     case FArrPos[i] of
       0: LShape[i].Brush.Color := clWhite;
       1: LShape[i].Brush.Color := clBlue;
       else LShape[i].Brush.Color := clRed;
     end;
     if xy < 4 then LShape[i].Pen.Style := psClear
     else LShape[i].Pen.Style := psSolid;
     LShape[i].Visible := True;
    end;
  end;
end;

end.
