object FmMyPagtos: TFmMyPagtos
  Left = 334
  Top = 173
  Caption = 'FIN-MYPAG-001 :: Pagamentos e Recebimentos'
  ClientHeight = 594
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinWidth = 789
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 55
    Width = 1008
    Height = 539
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 652
      Top = 0
      Width = 356
      Height = 539
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object PainelDados2: TPanel
        Left = 2
        Top = 15
        Width = 352
        Height = 522
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PainelControle: TPanel
          Left = 0
          Top = 470
          Width = 352
          Height = 52
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object BtConfirma: TBitBtn
            Tag = 14
            Left = 14
            Top = 4
            Width = 128
            Height = 43
            Cursor = crHandPoint
            Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Confirma'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtConfirmaClick
          end
          object Panel2: TPanel
            Left = 205
            Top = 0
            Width = 147
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BtDesiste: TBitBtn
              Tag = 15
              Left = 6
              Top = 4
              Width = 128
              Height = 43
              Cursor = crHandPoint
              Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Desiste'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtDesisteClick
            end
          end
        end
        object PageControl1: TPageControl
          Left = 0
          Top = 0
          Width = 352
          Height = 413
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet2
          Align = alClient
          TabOrder = 0
          object TabSheet2: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Parcelamento'
            ImageIndex = 1
            object DBGParcelas: TDBGrid
              Left = 0
              Top = 172
              Width = 344
              Height = 189
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabStop = False
              Align = alClient
              DataSource = DsParcPagtos
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnKeyDown = DBGParcelasKeyDown
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Parcela'
                  Width = 47
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 69
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Caption = 'D'#233'bito'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Caption = 'Cr'#233'dito'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Doc'
                  Title.Caption = 'Docum.'
                  Width = 52
                  Visible = True
                end>
            end
            object Panel4: TPanel
              Left = 0
              Top = 0
              Width = 344
              Height = 172
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object GBParcelamento: TGroupBox
                Left = 0
                Top = 0
                Width = 344
                Height = 172
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Caption = ' Parcelamento autom'#225'tico: '
                TabOrder = 1
                Visible = False
                object Label7: TLabel
                  Left = 11
                  Top = 27
                  Width = 44
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Parcelas:'
                end
                object Label8: TLabel
                  Left = 5
                  Top = 124
                  Width = 78
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Primeira parcela:'
                end
                object Label9: TLabel
                  Left = 109
                  Top = 124
                  Width = 70
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = #218'ltima parcela:'
                end
                object Label11: TLabel
                  Left = 212
                  Top = 124
                  Width = 97
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Total parcelamento: '
                end
                object RGArredondar: TRadioGroup
                  Left = 5
                  Top = 71
                  Width = 205
                  Height = 50
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = ' Increm. do doc.: '
                  Columns = 2
                  ItemIndex = 1
                  Items.Strings = (
                    '1'#170' parcela'
                    #218'ltima parcela')
                  TabOrder = 7
                  OnClick = RGArredondarClick
                end
                object EdParcelas: TdmkEdit
                  Left = 11
                  Top = 43
                  Width = 43
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '2'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 2
                  ValWarn = False
                  OnExit = EdParcelasExit
                end
                object RGPeriodo: TRadioGroup
                  Left = 79
                  Top = 21
                  Width = 157
                  Height = 44
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = ' Periodo: '
                  Columns = 2
                  ItemIndex = 0
                  Items.Strings = (
                    'Mensal'
                    '           dias')
                  TabOrder = 1
                  OnClick = RGPeriodoClick
                end
                object EdDias: TdmkEdit
                  Left = 177
                  Top = 37
                  Width = 29
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  Color = clBtnFace
                  Enabled = False
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '7'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 7
                  ValWarn = False
                  OnExit = EdDiasExit
                end
                object RGIncremento: TRadioGroup
                  Left = 242
                  Top = 21
                  Width = 99
                  Height = 44
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = ' Increm. cheque.: '
                  Columns = 2
                  Enabled = False
                  ItemIndex = 1
                  Items.Strings = (
                    'N'#227'o'
                    'Sim')
                  TabOrder = 3
                  OnClick = RGIncrementoClick
                end
                object EdParcela1: TdmkEdit
                  Left = 5
                  Top = 141
                  Width = 98
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  Color = clBtnFace
                  ReadOnly = True
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdParcelaX: TdmkEdit
                  Left = 109
                  Top = 141
                  Width = 98
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  Color = clBtnFace
                  ReadOnly = True
                  TabOrder = 5
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object CkArredondar: TCheckBox
                  Left = 21
                  Top = 68
                  Width = 101
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Arredondar: '
                  TabOrder = 6
                  OnClick = CkArredondarClick
                end
                object EdSoma: TdmkEdit
                  Left = 212
                  Top = 141
                  Width = 98
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabStop = False
                  Alignment = taRightJustify
                  Color = clBtnFace
                  ReadOnly = True
                  TabOrder = 8
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnExit = EdValorExit
                end
              end
              object CkParcelamento: TCheckBox
                Left = 16
                Top = 2
                Width = 201
                Height = 23
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Parcelamento autom'#225'tico: '
                TabOrder = 0
                Visible = False
                OnClick = CkParcelamentoClick
              end
            end
            object Panel7: TPanel
              Left = 0
              Top = 361
              Width = 344
              Height = 24
              Align = alBottom
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 2
              object CkContinuar: TCheckBox
                Left = 6
                Top = 0
                Width = 122
                Height = 22
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cont. inserindo.'
                TabOrder = 0
                Visible = False
              end
            end
          end
          object TabSheet1: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Outros'
            ImageIndex = 1
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 344
              Height = 385
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label14: TLabel
                Left = 2
                Top = 5
                Width = 26
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Data:'
              end
              object TPData: TdmkEditDateTimePicker
                Left = 2
                Top = 22
                Width = 147
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Date = 37478.000000000000000000
                Time = 0.679422685199824600
                Color = clWhite
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = 0
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                OnExit = TPVencimentoExit
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object GBCheque: TGroupBox
                Left = 1
                Top = 49
                Width = 346
                Height = 110
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Dados do cheque: '
                TabOrder = 1
                Visible = False
                object Label15: TLabel
                  Left = 12
                  Top = 15
                  Width = 167
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Emitente (F4 -> Copia do devedor): '
                end
                object Label16: TLabel
                  Left = 128
                  Top = 59
                  Width = 34
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Banco:'
                end
                object Label17: TLabel
                  Left = 169
                  Top = 59
                  Width = 42
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Ag'#234'ncia:'
                end
                object Label18: TLabel
                  Left = 225
                  Top = 59
                  Width = 31
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Conta:'
                end
                object Label19: TLabel
                  Left = 12
                  Top = 59
                  Width = 61
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'CNPJ / CPF:'
                end
                object EdNome: TdmkEdit
                  Left = 12
                  Top = 32
                  Width = 326
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = 0
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  MaxLength = 30
                  ParentFont = False
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnKeyDown = EdNomeKeyDown
                end
                object EdBanco: TdmkEdit
                  Left = 128
                  Top = 76
                  Width = 37
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = 0
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  MaxLength = 3
                  ParentFont = False
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 3
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnExit = EdBancoExit
                end
                object EdAgencia: TdmkEdit
                  Left = 169
                  Top = 76
                  Width = 54
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = 0
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  MaxLength = 11
                  ParentFont = False
                  TabOrder = 3
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 4
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdConta: TdmkEdit
                  Left = 225
                  Top = 76
                  Width = 114
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taCenter
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = 0
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  MaxLength = 15
                  ParentFont = False
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCNPJCPF: TdmkEdit
                  Left = 12
                  Top = 76
                  Width = 113
                  Height = 22
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taCenter
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = 0
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  MaxLength = 15
                  ParentFont = False
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtCPFJ
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object GroupBox3: TGroupBox
                Left = 1
                Top = 164
                Width = 346
                Height = 67
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Quantidades: '
                TabOrder = 2
                object Label24: TLabel
                  Left = 15
                  Top = 19
                  Width = 35
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Qtde 1:'
                end
                object Label64: TLabel
                  Left = 113
                  Top = 19
                  Width = 35
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Qtde 2:'
                end
                object EdQtde: TdmkEdit
                  Left = 15
                  Top = 35
                  Width = 93
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  QryCampo = 'Qtde'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdQtd2: TdmkEdit
                  Left = 113
                  Top = 35
                  Width = 93
                  Height = 23
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  QryCampo = 'Qtd2'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
        end
        object GBAvisos1: TGroupBox
          Left = 0
          Top = 413
          Width = 352
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Caption = ' Avisos: '
          TabOrder = 2
          object Panel6: TPanel
            Left = 2
            Top = 15
            Width = 348
            Height = 40
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object LaAviso1: TLabel
              Left = 17
              Top = 2
              Width = 150
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -18
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAviso2: TLabel
              Left = 16
              Top = 1
              Width = 150
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -18
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
          end
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 652
      Height = 539
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 1
      object PainelDados: TPanel
        Left = 2
        Top = 15
        Width = 648
        Height = 522
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaParcela: TLabel
          Left = 8
          Top = 0
          Width = 39
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Parcela:'
        end
        object Label1: TLabel
          Left = 55
          Top = 0
          Width = 39
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Carteira:'
        end
        object LaValor: TLabel
          Left = 8
          Top = 128
          Width = 34
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'D'#233'bito:'
        end
        object LaDocumento: TLabel
          Left = 160
          Top = 128
          Width = 40
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cheque:'
        end
        object LaVencimento: TLabel
          Left = 496
          Top = 128
          Width = 59
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Vencimento:'
        end
        object LaCredor: TLabel
          Left = 103
          Top = 171
          Width = 34
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Credor:'
          Enabled = False
        end
        object LaDevedor: TLabel
          Left = 103
          Top = 213
          Width = 44
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Devedor:'
          Enabled = False
        end
        object LaMoraDia: TLabel
          Left = 103
          Top = 256
          Width = 163
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Valor mora/dia ( F4 para  %/30dd):'
        end
        object LaMulta: TLabel
          Left = 382
          Top = 256
          Width = 112
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Valor Multa (F4 para %):'
        end
        object Label10: TLabel
          Left = 8
          Top = 299
          Width = 70
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cliente interno:'
        end
        object Label2: TLabel
          Left = 258
          Top = 128
          Width = 23
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'N.F.:'
        end
        object Label20: TLabel
          Left = 320
          Top = 128
          Width = 48
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Duplicata:'
        end
        object Label21: TLabel
          Left = 8
          Top = 43
          Width = 80
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Conta financeira:'
        end
        object SpeedButton1: TSpeedButton
          Left = 610
          Top = 16
          Width = 23
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object SpeedButton2: TSpeedButton
          Left = 610
          Top = 144
          Width = 23
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton2Click
        end
        object SpeedButton3: TSpeedButton
          Left = 610
          Top = 59
          Width = 23
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton3Click
        end
        object Label22: TLabel
          Left = 116
          Top = 128
          Width = 27
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'S'#233'rie:'
        end
        object Label23: TLabel
          Left = 213
          Top = 128
          Width = 27
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'S'#233'rie:'
        end
        object Label25: TLabel
          Left = 8
          Top = 85
          Width = 71
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Conta cont'#225'bil:'
        end
        object SbGenCtb: TSpeedButton
          Left = 610
          Top = 101
          Width = 23
          Height = 23
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SbGenCtbClick
        end
        object SbPsqGCFin: TSpeedButton
          Left = 586
          Top = 59
          Width = 23
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'GC'
          OnClick = SbPsqGCFinClick
        end
        object SbPsqGCCtb: TSpeedButton
          Left = 586
          Top = 101
          Width = 23
          Height = 23
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'GC'
          OnClick = SbPsqGCCtbClick
        end
        object GBRecibo: TGroupBox
          Left = 8
          Top = 345
          Width = 618
          Height = 125
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Recibos: '
          TabOrder = 24
          Visible = False
          object Label3: TLabel
            Left = 166
            Top = 21
            Width = 49
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '$ Pessoal:'
          end
          object Label4: TLabel
            Left = 247
            Top = 21
            Width = 53
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '$ Empresa:'
          end
          object Label5: TLabel
            Left = 329
            Top = 21
            Width = 55
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '% Empresa:'
          end
          object Label6: TLabel
            Left = 16
            Top = 21
            Width = 15
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'N'#186':'
          end
          object EdPessoal: TdmkEdit
            Left = 166
            Top = 37
            Width = 77
            Height = 23
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnEnter = EdPessoalEnter
            OnExit = EdPessoalExit
          end
          object EdEmpresa: TdmkEdit
            Left = 247
            Top = 37
            Width = 77
            Height = 23
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnEnter = EdEmpresaEnter
            OnExit = EdEmpresaExit
          end
          object EdPorcento: TdmkEdit
            Left = 329
            Top = 37
            Width = 76
            Height = 23
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdPorcentoChange
            OnEnter = EdPorcentoEnter
            OnExit = EdPorcentoExit
          end
          object EdRecibo: TdmkEdit
            Left = 16
            Top = 37
            Width = 146
            Height = 23
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            CharCase = ecLowerCase
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnEnter = EdPessoalEnter
            OnExit = EdPessoalExit
          end
          object EdBaseRecibo: TdmkEdit
            Left = 246
            Top = -5
            Width = 159
            Height = 22
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 6
            Visible = False
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object RGRecibo: TRadioGroup
            Left = 16
            Top = 61
            Width = 463
            Height = 60
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Op'#231#227'o: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o necessita de recibo'
              'Emitir um recibo no total'
              'Emitir um recibo para cada parcela'
              'Emitir recibos mensais. dia')
            TabOrder = 4
            OnClick = RGReciboClick
          end
          object EdDiaRecibo: TdmkEdit
            Left = 403
            Top = 95
            Width = 52
            Height = 22
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            Alignment = taRightJustify
            MaxLength = 2
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnExit = EdDiaReciboExit
          end
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 17
          Width = 40
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Color = clWhite
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '1'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
          OnChange = EdCodigoChange
          OnExit = EdCodigoExit
        end
        object EdCarteira: TdmkEditCB
          Left = 55
          Top = 17
          Width = 44
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCarteira
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCarteira: TdmkDBLookupComboBox
          Left = 101
          Top = 17
          Width = 508
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCarteiras
          TabOrder = 2
          dmkEditCB = EdCarteira
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdValor: TdmkEdit
          Left = 8
          Top = 145
          Width = 101
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValorChange
          OnExit = EdValorExit
        end
        object EdDocumento: TdmkEdit
          Left = 160
          Top = 145
          Width = 50
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdDocumentoChange
          OnExit = EdDocumentoExit
        end
        object TPVencimento: TdmkEditDateTimePicker
          Left = 496
          Top = 145
          Width = 111
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 37478.000000000000000000
          Time = 0.679422685199824600
          Color = clWhite
          TabOrder = 13
          OnExit = TPVencimentoExit
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdCredor: TdmkEditCB
          Left = 103
          Top = 188
          Width = 54
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 15
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCredorChange
          DBLookupComboBox = CBCredor
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCredor: TdmkDBLookupComboBox
          Left = 159
          Top = 188
          Width = 470
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          Enabled = False
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsCredor
          TabOrder = 16
          dmkEditCB = EdCredor
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdDevedor: TdmkEditCB
          Left = 103
          Top = 230
          Width = 54
          Height = 23
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 17
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdDevedorChange
          DBLookupComboBox = CBDevedor
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBDevedor: TdmkDBLookupComboBox
          Left = 159
          Top = 230
          Width = 470
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          Enabled = False
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsDevedor
          TabOrder = 18
          dmkEditCB = EdDevedor
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CkRecibo: TCheckBox
          Left = 27
          Top = 341
          Width = 105
          Height = 23
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Emitir recibo:'
          TabOrder = 23
          OnClick = CkReciboClick
        end
        object Memo1: TdmkMemo
          Left = 226
          Top = 16
          Width = 211
          Height = 43
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 25
          Visible = False
          UpdType = utYes
        end
        object EdMoraDia: TdmkEdit
          Left = 103
          Top = 273
          Width = 192
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 19
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdMoraDiaKeyDown
        end
        object EdMulta: TdmkEdit
          Left = 382
          Top = 273
          Width = 197
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 20
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdMultaKeyDown
        end
        object Panel3: TPanel
          Left = 8
          Top = 172
          Width = 88
          Height = 123
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          BevelOuter = bvLowered
          TabOrder = 14
          object Label12: TLabel
            Left = 5
            Top = 49
            Width = 54
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor troco:'
          end
          object Label13: TLabel
            Left = 5
            Top = 2
            Width = 55
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Troco para:'
          end
          object EdTrocoVal: TdmkEdit
            Left = 5
            Top = 64
            Width = 76
            Height = 23
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdTrocoPara: TdmkEdit
            Left = 5
            Top = 22
            Width = 76
            Height = 2
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdTrocoParaChange
          end
        end
        object EdCliInt: TdmkEditCB
          Left = 8
          Top = 316
          Width = 55
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 21
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCliInt: TdmkDBLookupComboBox
          Left = 69
          Top = 316
          Width = 560
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsCliInt
          TabOrder = 22
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNotaFiscal: TdmkEdit
          Left = 258
          Top = 145
          Width = 63
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdNotaFiscalExit
        end
        object EdDuplicata: TdmkEdit
          Left = 320
          Top = 145
          Width = 173
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdNotaFiscalExit
        end
        object EdGenero: TdmkEditCB
          Left = 128
          Top = 60
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdGeneroRedefinido
          DBLookupComboBox = CBGenero
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGenero: TdmkDBLookupComboBox
          Left = 180
          Top = 60
          Width = 404
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsContas1
          TabOrder = 4
          dmkEditCB = EdGenero
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdSerieCH: TdmkEdit
          Left = 116
          Top = 145
          Width = 40
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          TabOrder = 8
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdDocumentoChange
          OnExit = EdDocumentoExit
        end
        object DBRadioGroup1: TDBRadioGroup
          Left = 8
          Top = 473
          Width = 618
          Height = 37
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Tipo de carteira: '
          Columns = 3
          DataField = 'Tipo'
          DataSource = DsCarteiras
          Items.Strings = (
            'Caixa (Esp'#233'cie)'
            'Banco (Conta corrente)'
            'Emiss'#227'o (Ch.,dupl.,fat.)')
          TabOrder = 26
          Values.Strings = (
            '0'
            '1'
            '2')
        end
        object EdSerieNF: TdmkEdit
          Left = 213
          Top = 145
          Width = 40
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdDocumentoChange
          OnExit = EdDocumentoExit
        end
        object EdGenCtb: TdmkEditCB
          Left = 128
          Top = 102
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdGenCtbRedefinido
          DBLookupComboBox = CBGenCtb
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGenCtb: TdmkDBLookupComboBox
          Left = 180
          Top = 102
          Width = 404
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsContas2
          TabOrder = 6
          dmkEditCB = EdGenCtb
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object DBNiveis1: TDBEdit
          Left = 8
          Top = 60
          Width = 120
          Height = 21
          TabStop = False
          DataField = 'Niveis'
          DataSource = DsContas1
          TabOrder = 27
        end
        object DBNiveis2: TDBEdit
          Left = 8
          Top = 102
          Width = 120
          Height = 21
          TabStop = False
          DataField = 'Niveis'
          DataSource = DsContas2
          TabOrder = 28
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 952
      Top = 0
      Width = 56
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 11
        Top = 11
        Width = 34
        Height = 34
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 55
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 55
      Top = 0
      Width = 897
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 10
        Top = 12
        Width = 388
        Height = 35
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pagamentos e Recebimentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -30
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 12
        Top = 15
        Width = 388
        Height = 35
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pagamentos e Recebimentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -30
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 11
        Top = 13
        Width = 388
        Height = 35
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pagamentos e Recebimentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -30
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 376
    Top = 49
  end
  object DsCredor: TDataSource
    DataSet = QrCredor
    Left = 380
    Top = 268
  end
  object DsDevedor: TDataSource
    DataSet = QrDevedor
    Left = 372
    Top = 308
  end
  object TbParcPagtos: TMySQLTable
    Database = Dmod.MyDB
    BeforeOpen = TbParcPagtosBeforeOpen
    AfterPost = TbParcPagtosAfterPost
    TableName = 'parcpagtos'
    Left = 473
    Top = 269
    object TbParcPagtosParcela: TIntegerField
      FieldName = 'Parcela'
    end
    object TbParcPagtosData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbParcPagtosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcPagtosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcPagtosDoc: TLargeintField
      FieldName = 'Doc'
    end
  end
  object DsParcPagtos: TDataSource
    DataSet = TbParcPagtos
    Left = 501
    Top = 269
  end
  object QrCarteiras: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCarteirasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo, ForneceI'
      'FROM carteiras'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 348
    Top = 49
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
  end
  object QrTerceiro: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.*, '
      'ufp.Nome NOMEUFP,'
      'ufe.Nome NOMEUFE'
      'FROM entidades en, UFs ufe, UFs ufp'
      'WHERE en.Codigo=:P0'
      'AND ufe.Codigo=en.EUF'
      'AND ufp.Codigo=en.PUF'
      '')
    Left = 336
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerceiroCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTerceiroRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrTerceiroFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrTerceiroPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrTerceiroMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrTerceiroCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTerceiroIE: TWideStringField
      FieldName = 'IE'
    end
    object QrTerceiroNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrTerceiroApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrTerceiroCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTerceiroRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrTerceiroERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrTerceiroECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrTerceiroEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrTerceiroECIDADE: TWideStringField
      FieldName = 'ECIDADE'
      Size = 25
    end
    object QrTerceiroEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrTerceiroECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrTerceiroEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrTerceiroETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrTerceiroETe2: TWideStringField
      FieldName = 'ETe2'
    end
    object QrTerceiroETe3: TWideStringField
      FieldName = 'ETe3'
    end
    object QrTerceiroECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrTerceiroEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrTerceiroEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 100
    end
    object QrTerceiroEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrTerceiroENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrTerceiroPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrTerceiroPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrTerceiroPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrTerceiroPCIDADE: TWideStringField
      FieldName = 'PCIDADE'
      Size = 25
    end
    object QrTerceiroPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrTerceiroPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrTerceiroPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrTerceiroPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrTerceiroPTe2: TWideStringField
      FieldName = 'PTe2'
    end
    object QrTerceiroPTe3: TWideStringField
      FieldName = 'PTe3'
    end
    object QrTerceiroPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrTerceiroPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrTerceiroPEMail: TWideStringField
      FieldName = 'PEMail'
      Size = 100
    end
    object QrTerceiroPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrTerceiroPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrTerceiroSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrTerceiroResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrTerceiroProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrTerceiroCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrTerceiroRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrTerceiroDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrTerceiroAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrTerceiroAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrTerceiroCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrTerceiroCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrTerceiroFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrTerceiroFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrTerceiroFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrTerceiroFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrTerceiroTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrTerceiroCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrTerceiroInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrTerceiroLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrTerceiroVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrTerceiroMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrTerceiroObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTerceiroTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTerceiroLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTerceiroGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrTerceiroDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTerceiroDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTerceiroUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrTerceiroUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrTerceiroCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrTerceiroCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrTerceiroCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrTerceiroCCIDADE: TWideStringField
      FieldName = 'CCIDADE'
      Size = 25
    end
    object QrTerceiroCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrTerceiroCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrTerceiroCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrTerceiroCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrTerceiroCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrTerceiroCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrTerceiroCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrTerceiroLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrTerceiroLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrTerceiroLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrTerceiroLCIDADE: TWideStringField
      FieldName = 'LCIDADE'
      Size = 25
    end
    object QrTerceiroLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrTerceiroLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrTerceiroLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrTerceiroLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrTerceiroLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrTerceiroLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrTerceiroLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrTerceiroComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrTerceiroSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrTerceiroNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrTerceiroNOMEUFP: TWideStringField
      FieldName = 'NOMEUFP'
      Required = True
      Size = 2
    end
    object QrTerceiroNOMEUFE: TWideStringField
      FieldName = 'NOMEUFE'
      Required = True
      Size = 2
    end
    object QrTerceiroENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrTerceiroPNumero: TIntegerField
      FieldName = 'PNumero'
    end
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito+Debito) VALOR'
      'FROM parcpagtos')
    Left = 148
    Top = 277
    object QrSomaVALOR: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'VALOR'
    end
  end
  object QrCredor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE 1 '
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 352
    Top = 269
    object QrCredorCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCredorNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrDevedor: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDevedorCalcFields
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE,'
      'CASE WHEN Tipo=0 THEN CNPJ'
      'ELSE CPF END CNPJ_CPF, '
      'Banco, Agencia, ContaCorrente'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 344
    Top = 309
    object QrDevedorCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDevedorNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrDevedorCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrDevedorBanco: TSmallintField
      FieldName = 'Banco'
    end
    object QrDevedorAgencia: TWideStringField
      FieldName = 'Agencia'
      Size = 11
    end
    object QrDevedorContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrDevedorCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 50
      Calculated = True
    end
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE CliInt<>0'
      'ORDER BY NOMEENTIDADE')
    Left = 348
    Top = 349
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 376
    Top = 348
  end
  object QrContas1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT('
      '  pl.Codigo, ".", '
      '  cj.Codigo, ".", '
      '  gr.Codigo, ".",'
      '  sg.Codigo, ".", '
      '  co.Codigo) Niveis, '
      ' co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 576
    Top = 292
    object QrContas1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContas1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContas1Nome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContas1Nome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContas1ID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContas1Subgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContas1Empresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContas1Credito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContas1Debito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContas1Mensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContas1Exclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContas1Mensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContas1Mensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContas1Mensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContas1Menscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContas1Mensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContas1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContas1Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContas1Excel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContas1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContas1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContas1UserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContas1UserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContas1NOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContas1NOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContas1NOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContas1NOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrContas1NOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrContas1Niveis: TWideStringField
      FieldName = 'Niveis'
      Size = 100
    end
    object QrContas1Ordens: TWideStringField
      FieldName = 'Ordens'
      Size = 100
    end
  end
  object DsContas1: TDataSource
    DataSet = QrContas1
    Left = 576
    Top = 340
  end
  object QrContas2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT('
      '  pl.Codigo, ".", '
      '  cj.Codigo, ".", '
      '  gr.Codigo, ".",'
      '  sg.Codigo, ".", '
      '  co.Codigo) Niveis, '
      'co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 664
    Top = 296
    object QrContas2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContas2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContas2Nome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContas2Nome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContas2ID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContas2Subgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContas2Empresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContas2Credito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContas2Debito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContas2Mensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContas2Exclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContas2Mensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContas2Mensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContas2Mensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContas2Menscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContas2Mensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContas2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContas2Terceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContas2Excel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContas2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContas2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContas2UserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContas2UserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContas2NOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContas2NOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContas2NOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContas2NOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrContas2NOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrContas2Niveis: TWideStringField
      FieldName = 'Niveis'
      Size = 100
    end
    object QrContas2Ordens: TWideStringField
      FieldName = 'Ordens'
      Size = 100
    end
  end
  object DsContas2: TDataSource
    DataSet = QrContas2
    Left = 664
    Top = 348
  end
end
