object FmCashPreCta: TFmCashPreCta
  Left = 339
  Top = 185
  Caption = 'FIN-BALAN-003 :: Presta'#231#227'o de Contas'
  ClientHeight = 751
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 550
    Height = 551
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Panel6: TPanel
      Left = 0
      Top = 167
      Width = 550
      Height = 223
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object Label4: TLabel
        Left = 15
        Top = 167
        Width = 121
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Despesas a vencer:'
      end
      object Label5: TLabel
        Left = 15
        Top = 197
        Width = 112
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Receitas a vencer:'
      end
      object CkNaoAgruparNada: TCheckBox
        Left = 15
        Top = 113
        Width = 530
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#227'o agrupar nada (visualiza'#231#227'o de todos lan'#231'amentos).'
        TabOrder = 1
        OnClick = CBMesIniChange
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 550
        Height = 105
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Caption = ' Agrupamentos: '
        TabOrder = 0
        object CkAcordos: TCheckBox
          Left = 10
          Top = 25
          Width = 535
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Individualizar acordos judiciais. (pode aumentar a quantidade de' +
            ' registros)'
          TabOrder = 0
          OnClick = CBMesIniChange
        end
        object CkPeriodos: TCheckBox
          Left = 10
          Top = 49
          Width = 535
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Individualizar per'#237'odos [mensal]. (pode aumentar a quantidade de' +
            ' registros)'
          TabOrder = 1
          OnClick = CBMesIniChange
        end
        object CkTextos: TCheckBox
          Left = 10
          Top = 74
          Width = 535
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Agrupar lan'#231'amentos com hist'#243'ricos iguais. (pode diminuir a quan' +
            'tidade de registros)'
          Checked = True
          State = cbChecked
          TabOrder = 2
          OnClick = CBMesIniChange
        end
      end
      object CkPaginar: TdmkCheckBox
        Left = 15
        Top = 138
        Width = 530
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Paginar as folhas.'
        Checked = True
        State = cbChecked
        TabOrder = 2
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object EdReceVencer: TdmkEdit
        Left = 162
        Top = 192
        Width = 124
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdDebiVencer: TdmkEdit
        Left = 162
        Top = 162
        Width = 124
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 550
      Height = 167
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 15
        Top = 5
        Width = 87
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente interno:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 15
        Top = 25
        Width = 54
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInt64
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        OnExit = EdEmpresaExit
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 69
        Top = 25
        Width = 439
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'FILIAL'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object GroupBox1: TGroupBox
        Left = 15
        Top = 54
        Width = 503
        Height = 105
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Per'#237'odo da pesquisa: '
        TabOrder = 2
        object GroupBox2: TGroupBox
          Left = 10
          Top = 20
          Width = 237
          Height = 75
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Per'#237'odo inicial: '
          TabOrder = 0
          object LaAno: TLabel
            Left = 143
            Top = 17
            Width = 27
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Ano:'
          end
          object LaMes: TLabel
            Left = 10
            Top = 17
            Width = 29
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'M'#234's:'
          end
          object CBMesIni: TComboBox
            Left = 10
            Top = 38
            Width = 129
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Color = clWhite
            DropDownCount = 12
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            Text = 'CBMesIni'
            OnChange = CBMesIniChange
            OnClick = CBMesIniChange
          end
          object CBAnoIni: TComboBox
            Left = 143
            Top = 38
            Width = 85
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Color = clWhite
            DropDownCount = 3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Text = 'CBAnoIni'
            OnChange = CBMesIniChange
            OnClick = CBMesIniChange
          end
        end
        object GroupBox3: TGroupBox
          Left = 256
          Top = 20
          Width = 238
          Height = 75
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Per'#237'odo final: '
          TabOrder = 1
          object Label2: TLabel
            Left = 143
            Top = 17
            Width = 27
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Ano:'
          end
          object Label3: TLabel
            Left = 10
            Top = 17
            Width = 29
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'M'#234's:'
          end
          object CBMesFim: TComboBox
            Left = 10
            Top = 38
            Width = 129
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Color = clWhite
            DropDownCount = 12
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            Text = 'CBMesFim'
            OnChange = CBMesIniChange
            OnClick = CBMesIniChange
          end
          object CBAnoFim: TComboBox
            Left = 143
            Top = 38
            Width = 85
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Color = clWhite
            DropDownCount = 3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Text = 'CBAnoFim'
            OnChange = CBMesIniChange
            OnClick = CBMesIniChange
          end
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 390
      Width = 550
      Height = 161
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
    end
  end
  object PageControl1: TPageControl
    Left = 550
    Top = 59
    Width = 691
    Height = 551
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' G'#234'neros controlados '
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label6: TLabel
        Left = 0
        Top = 0
        Width = 352
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Alignment = taCenter
        Caption = 'Informe aqui os itens futuros a serem pesquisados!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBGContasNiv: TdmkDBGridDAC
        Left = 0
        Top = 16
        Width = 683
        Height = 504
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        SQLFieldsToChange.Strings = (
          'Ativo')
        SQLIndexesOnUpdate.Strings = (
          'Nivel'
          'Genero')
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Ativo'
            Title.Caption = 'ok'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nivel'
            Title.Caption = 'N'#237'vel'
            Width = 51
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMENIVEL'
            Title.Caption = 'Descri'#231#227'o do n'#237'vel'
            Width = 96
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Genero'
            Title.Caption = 'G'#234'nero'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEGENERO'
            Title.Caption = 'Descri'#231#227'o do G'#234'nero'
            Width = 312
            Visible = True
          end>
        Color = clWindow
        DataSource = DsCtasNiv
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        SQLTable = 'ctasniv'
        EditForceNextYear = False
        Columns = <
          item
            Expanded = False
            FieldName = 'Ativo'
            Title.Caption = 'ok'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nivel'
            Title.Caption = 'N'#237'vel'
            Width = 51
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMENIVEL'
            Title.Caption = 'Descri'#231#227'o do n'#237'vel'
            Width = 96
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Genero'
            Title.Caption = 'G'#234'nero'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEGENERO'
            Title.Caption = 'Descri'#231#227'o do G'#234'nero'
            Width = 312
            Visible = True
          end>
      end
    end
    object TabSheet2: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'D'#233'bitos agendados'
      ImageIndex = 1
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 683
        Height = 520
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsFutDeb
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Vencimento'
            Title.Caption = 'Vencto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NotaFiscal'
            Title.Caption = 'N.F.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Hist'#243'rico'
            Width = 346
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Debito'
            Title.Caption = 'D'#233'bito'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Data'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Genero'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Carteira'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Documento'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Fornecedor'
            Width = 56
            Visible = True
          end>
      end
    end
    object TabSheet3: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Cr'#233'ditos agendados '
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DBGrid2: TDBGrid
        Left = 0
        Top = 0
        Width = 683
        Height = 520
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsArreBaI
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 319
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Percent'
            Title.Caption = '%'
            Width = 47
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ParcelasRestantes'
            Title.Caption = 'Parc.'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VAL_RESTANTE'
            Title.Caption = 'Falta (Total)'
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 299
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Presta'#231#227'o de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 299
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Presta'#231#227'o de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 299
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Presta'#231#227'o de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 610
    Width = 1241
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 35
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 665
    Width = 1241
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 4
    object Panel8: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 1059
        Top = 0
        Width = 178
        Height = 66
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 22
        Left = 25
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 177
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtImprimeClick
      end
    end
  end
  object frxDsEmp: TfrxDBDataset
    UserName = 'frxDsEmp'
    CloseDataSource = False
    DataSet = DModG.QrEmpresas
    BCDToCurrency = False
    Left = 64
    Top = 8
  end
  object QrExclusivos: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrExclusivosCalcFields
    SQL.Strings = (
      'SELECT SUM(mov.Movim) Movim'
      'FROM contasmov mov'
      'LEFT JOIN contas cta ON cta.Codigo=mov.Genero'
      'WHERE cta.Exclusivo="V"'
      'AND mov.CliInt=:P0'
      'AND mov.Mez>:P1')
    Left = 416
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrExclusivosMovim: TFloatField
      FieldName = 'Movim'
    end
    object QrExclusivosCREDITO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CREDITO'
      Calculated = True
    end
    object QrExclusivosDEBITO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DEBITO'
      Calculated = True
    end
    object QrExclusivosANTERIOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ANTERIOR'
      Calculated = True
    end
  end
  object frxDsExclusivos: TfrxDBDataset
    UserName = 'frxDsExclusivos'
    CloseDataSource = False
    DataSet = QrExclusivos
    BCDToCurrency = False
    Left = 444
    Top = 4
  end
  object QrOrdinarios: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOrdinariosCalcFields
    SQL.Strings = (
      'SELECT SUM(mov.Movim) Movim'
      'FROM contasmov mov'
      'LEFT JOIN contas cta ON cta.Codigo=mov.Genero'
      'WHERE Exclusivo<>"V"'
      'AND mov.CliInt=:P0'
      'AND mov.Mez<=:P1')
    Left = 472
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOrdinariosMovim: TFloatField
      FieldName = 'Movim'
    end
    object QrOrdinariosCREDITO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CREDITO'
      Calculated = True
    end
    object QrOrdinariosDEBITO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DEBITO'
      Calculated = True
    end
    object QrOrdinariosANTERIOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ANTERIOR'
      Calculated = True
    end
  end
  object frxDsOrdinarios: TfrxDBDataset
    UserName = 'frxDsOrdinarios'
    CloseDataSource = False
    DataSet = QrOrdinarios
    BCDToCurrency = False
    Left = 500
    Top = 4
  end
  object QrOrdiMov: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito'
      'FROM contasmov mov'
      'LEFT JOIN contas cta ON cta.Codigo=mov.Genero'
      'WHERE Exclusivo<>"V"'
      'AND mov.CliInt=:P0'
      'AND mov.Mez BETWEEN :P1 AND :P2')
    Left = 472
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrOrdiMovCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrOrdiMovDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrExclMov: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito'
      'FROM contasmov mov'
      'LEFT JOIN contas cta ON cta.Codigo=mov.Genero'
      'WHERE Exclusivo="V"'
      'AND mov.CliInt=:P0'
      'AND mov.Mez BETWEEN :P1 AND :P2')
    Left = 416
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrExclMovCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrExclMovDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object frxPreCta: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxPreCtaGetValue
    Left = 92
    Top = 8
    Datasets = <
      item
        DataSet = frxDsCreditos
        DataSetName = 'frxDsCreditos'
      end
      item
        DataSet = frxDsDebitos
        DataSetName = 'frxDsDebitos'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsExclusivos
        DataSetName = 'frxDsExclusivos'
      end
      item
        DataSet = frxDsInad
        DataSetName = 'frxDsInad'
      end
      item
        DataSet = frxDsOrdinarios
        DataSetName = 'frxDsOrdinarios'
      end
      item
        DataSet = frxDsResumo
        DataSetName = 'frxDsResumo'
      end
      item
        DataSet = frxDsSaldoA
        DataSetName = 'frxDsSaldoA'
      end
      item
        DataSet = frxDsSdoNivCtas
        DataSetName = 'frxDsSdoNivCtas'
      end
      item
        DataSet = frxDsSTCPa
        DataSetName = 'frxDsSTCPa'
      end
      item
        DataSet = frxDsSTCPd
        DataSetName = 'frxDsSTCPd'
      end
      item
        DataSet = frxDsSTCPf
        DataSetName = 'frxDsSTCPf'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'PRESTA'#199#195'O DE CONTAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716535433080000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NOMEFILIAL"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCreditos."NOMEGRU"'
        object Memo51: TfrxMemoView
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCreditos."NOMEGRU"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valores em R$')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 521.575140000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 257.008040000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCreditos."NOMESGR"'
        object Memo53: TfrxMemoView
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCreditos."NOMESGR"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Left = 521.575140000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCreditos
        DataSetName = 'frxDsCreditos'
        RowCount = 0
        object Memo55: TfrxMemoView
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          DataField = 'NOMECON_2'
          DataSet = frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCreditos."NOMECON_2"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataField = 'Credito'
          DataSet = frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCreditos."Credito"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 521.575140000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DataField = 'MES'
          DataSet = frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCreditos."MES"]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 332.598640000000000000
        Width = 680.315400000000000000
        object Memo58: TfrxMemoView
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCreditos."Credito">)]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 521.575140000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 370.393940000000000000
        Width = 680.315400000000000000
        object Memo60: TfrxMemoView
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de [frxDsCreditos."NOMEGRU"] ')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCreditos."Credito">)]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 510.236550000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDebitos."NOMEGRU"'
        object Memo64: TfrxMemoView
          Width = 366.614410000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."NOMEGRU"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 411.968770000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Baixa')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 476.220780000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 551.811380000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Nota fiscal')
          ParentFont = False
        end
      end
      object GroupHeader5: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 548.031850000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDebitos."NOMESGR"'
        object Memo70: TfrxMemoView
          Width = 366.614410000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDebitos."NOMESGR"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 411.968770000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 476.220780000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 551.811380000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 623.622450000000000000
        Width = 680.315400000000000000
        DataSet = frxDsDebitos
        DataSetName = 'frxDsDebitos'
        RowCount = 0
        object Memo76: TfrxMemoView
          Width = 366.614410000000000000
          Height = 15.118120000000000000
          DataField = 'Descricao'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDebitos."Descricao"]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDebitos."DEBITO"]')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          DataField = 'MES'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."MES"]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 411.968770000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataField = 'COMPENSADO_TXT'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."COMPENSADO_TXT"]')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 476.220780000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataField = 'SERIE_DOC'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."SERIE_DOC"]')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 551.811380000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DataField = 'NF_TXT'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."NF_TXT"]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 699.213050000000000000
        Width = 680.315400000000000000
        object Memo82: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Width = 366.614410000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sub-total de [frxDsDebitos."NOMESGR"]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Left = 411.968770000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Left = 476.220780000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Left = 551.811380000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter5: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.456690470000000000
        Top = 737.008350000000000000
        Width = 680.315400000000000000
        object Memo83: TfrxMemoView
          Left = 589.606680000000000000
          Width = 90.708720000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Width = 589.606680000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de [frxDsDebitos."NOMEGRU"] ')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 1292.599260000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object GroupHeader9: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 585.827150000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDebitos."NOMECON"'
        object Memo4: TfrxMemoView
          Width = 366.614410000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  [frxDsDebitos."NOMECON"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 411.968770000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 476.220780000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 551.811380000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter9: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 661.417750000000000000
        Width = 680.315400000000000000
        object Memo10: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Width = 366.614410000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  Sub-total de [frxDsDebitos."NOMECON"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Left = 411.968770000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo198: TfrxMemoView
          Left = 476.220780000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo199: TfrxMemoView
          Left = 551.811380000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        object Memo200: TfrxMemoView
          Left = 3.779530000000000000
          Top = 11.338590000000000000
          Width = 676.535870000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'RECEITAS')
          ParentFont = False
        end
      end
      object Footer4: TfrxFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Memo201: TfrxMemoView
          Top = 7.559060000000000000
          Width = 521.575140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL RECEITAS:')
          ParentFont = False
        end
        object Memo202: TfrxMemoView
          Left = 521.575140000000000000
          Top = 7.559060000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCreditos."Credito">,MasterData2)]')
          ParentFont = False
        end
      end
      object Header4: TfrxHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 464.882190000000000000
        Width = 680.315400000000000000
        object Memo203: TfrxMemoView
          Left = 3.779530000000000000
          Width = 676.535870000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'DESPESAS')
          ParentFont = False
        end
      end
      object Footer6: TfrxFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 786.142240000000000000
        Width = 680.315400000000000000
        object Memo204: TfrxMemoView
          Width = 521.575140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DESPESAS:')
          ParentFont = False
        end
        object Memo205: TfrxMemoView
          Left = 521.575140000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">,MasterData3)]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 136.063080000000000000
        Top = 831.496600000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Memo85: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'RESULTADO NO PER'#205'ODO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          Top = 18.897650000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo Anterior:')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSaldoA."TOTAL"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Top = 34.015770000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de receitas:')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Left = 566.929500000000000000
          Top = 34.015770000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."Credito"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Top = 49.133890000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de despesas:')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Top = 64.252010000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo do per'#237'odo selecionado:')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Left = 566.929500000000000000
          Top = 49.133890000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."Debito"]')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 566.929500000000000000
          Top = 64.252010000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."SALDO"]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Top = 79.370130000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SALDO A SER TRANSFERIDO:')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 566.929500000000000000
          Top = 79.370130000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."FINAL"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Top = 105.826840000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo das contas exclusivas:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 566.929500000000000000
          Top = 105.826840000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExclusivos."Movim"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 120.944960000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SALDO SEM AS CONTAS EXCLUSIVAS:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 566.929500000000000000
          Top = 120.944960000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsExclusivos."Movim"> + <frxDsResumo."FINAL">]')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        Height = 94.488250000000000000
        Top = 1137.638530000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Memo18: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'RESUMO FUTURO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Top = 18.897650000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SALDO FINAL DO PER'#205'ODO SELECIONADO:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCPf."SDOFIM"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Top = 34.015770000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INADIMPL'#202'NCIA NO FIM DO PER'#205'ODO:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 566.929500000000000000
          Top = 34.015770000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInad."CREDITO"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Top = 49.133890000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL DE D'#201'BITOS AGENDADOS A QUITAR:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Top = 64.252010000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL DE RECEITAS AGENDADAS A QUITAR:')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 566.929500000000000000
          Top = 49.133890000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_DESP_VENCER]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 566.929500000000000000
          Top = 64.252010000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_RECE_VENCER]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Top = 79.370130000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SALDO VIRTUAL DO PER'#205'ODO:')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 566.929500000000000000
          Top = 79.370130000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<frxDsSTCPf."SDOFIM">+<frxDsInad."CREDITO">+<VARF_RECE_VENCER>+' +
              '<VARF_DESP_VENCER>]')
          ParentFont = False
        end
      end
      object GroupHeader15: TfrxGroupHeader
        FillType = ftBrush
        Height = 41.574820240000000000
        Top = 990.236860000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSdoNivCtas."Ativo"'
        object Memo343: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'SALDOS DE CONTAS CONTROLADAS')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo312: TfrxMemoView
          Left = 340.157700000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_DATA_PER_ANT]')
          ParentFont = False
        end
        object Memo313: TfrxMemoView
          Left = 408.189240000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'ditos')
          ParentFont = False
        end
        object Memo314: TfrxMemoView
          Left = 75.590600000000000000
          Top = 26.456710000000000000
          Width = 264.567100000000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo315: TfrxMemoView
          Left = 544.252320000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Res. per'#237'odo')
          ParentFont = False
        end
        object Memo316: TfrxMemoView
          Left = 612.283860000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_DATA_PER_FIM]')
          ParentFont = False
        end
        object Memo317: TfrxMemoView
          Left = 476.220780000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bitos')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Top = 26.456710000000000000
          Width = 34.015770000000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#237'vel')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 34.015770000000000000
          Top = 26.456710000000000000
          Width = 41.574830000000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'G'#234'nero')
          ParentFont = False
        end
      end
      object GroupFooter15: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 1092.284170000000000000
        Width = 680.315400000000000000
        object Memo337: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCPa."SDOANT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo338: TfrxMemoView
          Top = 3.779530000000000000
          Width = 340.157700000000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SOMAS DE VALORES DE TODAS CONTAS:     ')
          ParentFont = False
        end
        object Memo339: TfrxMemoView
          Left = 408.189240000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCPd."SUMCRE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo340: TfrxMemoView
          Left = 476.220780000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCPd."SUMDEB"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo341: TfrxMemoView
          Left = 544.252320000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCPd."SUMMOV"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo342: TfrxMemoView
          Left = 612.283860000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = FmContasHistSdo3.frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCPf."SDOFIM"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData14: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 1054.488870000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSdoNivCtas
        DataSetName = 'frxDsSdoNivCtas'
        KeepTogether = True
        RowCount = 0
        object Memo331: TfrxMemoView
          Left = 340.157700000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsSdoNivCtas
          DataSetName = 'frxDsSdoNivCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoNivCtas."SdoIni"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo332: TfrxMemoView
          Left = 408.189240000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'MovCre'
          DataSet = frxDsSdoNivCtas
          DataSetName = 'frxDsSdoNivCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoNivCtas."MovCre"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo333: TfrxMemoView
          Left = 75.590600000000000000
          Width = 264.567100000000000000
          Height = 15.118110240000000000
          DataField = 'NomeGenero'
          DataSet = frxDsSdoNivCtas
          DataSetName = 'frxDsSdoNivCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSdoNivCtas."NomeGenero"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo334: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'SdoFim'
          DataSet = frxDsSdoNivCtas
          DataSetName = 'frxDsSdoNivCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoNivCtas."SdoFim"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo335: TfrxMemoView
          Left = 544.252320000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsSdoNivCtas
          DataSetName = 'frxDsSdoNivCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsSdoNivCtas."MovCre">-<frxDsSdoNivCtas."MovDeb">]')
          ParentFont = False
          WordWrap = False
        end
        object Memo336: TfrxMemoView
          Left = 476.220780000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'MovDeb'
          DataSet = frxDsSdoNivCtas
          DataSetName = 'frxDsSdoNivCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoNivCtas."MovDeb"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Width = 34.015770000000000000
          Height = 15.118110240000000000
          DataField = 'Nivel'
          DataSet = frxDsSdoNivCtas
          DataSetName = 'frxDsSdoNivCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSdoNivCtas."Nivel"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 34.015770000000000000
          Width = 41.574830000000000000
          Height = 15.118110240000000000
          DataField = 'Genero'
          DataSet = frxDsSdoNivCtas
          DataSetName = 'frxDsSdoNivCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSdoNivCtas."Genero"]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object QrCtasNiv: TMySQLQuery
   
    SQL.Strings = (
      'SELECT *'
      'FROM ctasniv')
    Left = 880
    Top = 124
    object QrCtasNivNOMENIVEL: TWideStringField
      FieldName = 'NOMENIVEL'
      Size = 9
    end
    object QrCtasNivNOMEGENERO: TWideStringField
      FieldName = 'NOMEGENERO'
      Size = 50
    end
    object QrCtasNivNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrCtasNivGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCtasNivAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsCtasNiv: TDataSource
    DataSet = QrCtasNiv
    Left = 908
    Top = 124
  end
  object QrAnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Movim) SDOANT '
      'FROM contasmov'
      'WHERE CliInt=:P0'
      'AND Mez<:P1'
      'GROUP BY Conjunto')
    Left = 880
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAntSDOANT: TFloatField
      FieldName = 'SDOANT'
    end
  end
  object QrMov: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito,'
      'SUM(Debito) Debito'
      'FROM contasmov'
      'WHERE CliInt=1751'
      'AND Mez BETWEEN 1004 AND 1010'
      'AND Conjunto=1'
      'GROUP BY Conjunto, Mez'
      ''
      '')
    Left = 908
    Top = 152
    object QrMovCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrMovDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrSdoNivCtas: TMySQLQuery
   
    SQL.Strings = (
      'SELECT * '
      'FROM sdonivctas')
    Left = 936
    Top = 124
    object QrSdoNivCtasNomeNivel: TWideStringField
      FieldName = 'NomeNivel'
    end
    object QrSdoNivCtasNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrSdoNivCtasNomeGenero: TWideStringField
      FieldName = 'NomeGenero'
      Size = 100
    end
    object QrSdoNivCtasGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrSdoNivCtasSdoIni: TFloatField
      FieldName = 'SdoIni'
    end
    object QrSdoNivCtasMovCre: TFloatField
      FieldName = 'MovCre'
    end
    object QrSdoNivCtasMovDeb: TFloatField
      FieldName = 'MovDeb'
    end
    object QrSdoNivCtasSdoFim: TFloatField
      FieldName = 'SdoFim'
    end
    object QrSdoNivCtasAtivo: TIntegerField
      FieldName = 'Ativo'
    end
  end
  object frxDsSdoNivCtas: TfrxDBDataset
    UserName = 'frxDsSdoNivCtas'
    CloseDataSource = False
    DataSet = QrSdoNivCtas
    BCDToCurrency = False
    Left = 964
    Top = 124
  end
  object QrSTCPa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Movim) SDOANT'
      'FROM contasmov mov'
      'WHERE mov.CliInt=:P0'
      'AND mov.Mez<:P1')
    Left = 880
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSTCPaSDOANT: TFloatField
      FieldName = 'SDOANT'
    end
  end
  object frxDsSTCPa: TfrxDBDataset
    UserName = 'frxDsSTCPa'
    CloseDataSource = False
    DataSet = QrSTCPa
    BCDToCurrency = False
    Left = 908
    Top = 208
  end
  object QrSTCPd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) SUMCRE, SUM(Debito) SUMDEB,'
      'SUM(movim) SUMMOV'
      'FROM contasmov mov'
      'WHERE mov.CliInt=:P0'
      'AND mov.Mez BETWEEN :P1 AND :P2;')
    Left = 880
    Top = 236
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSTCPdSUMCRE: TFloatField
      FieldName = 'SUMCRE'
    end
    object QrSTCPdSUMDEB: TFloatField
      FieldName = 'SUMDEB'
    end
    object QrSTCPdSUMMOV: TFloatField
      FieldName = 'SUMMOV'
    end
  end
  object frxDsSTCPd: TfrxDBDataset
    UserName = 'frxDsSTCPd'
    CloseDataSource = False
    DataSet = QrSTCPd
    BCDToCurrency = False
    Left = 908
    Top = 236
  end
  object QrSTCPf: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  SUM(Movim) SDOFIM'
      'FROM contasmov mov'
      'WHERE mov.CliInt=:P0'
      'AND mov.Mez<=:P1')
    Left = 880
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSTCPfSDOFIM: TFloatField
      FieldName = 'SDOFIM'
    end
  end
  object frxDsSTCPf: TfrxDBDataset
    UserName = 'frxDsSTCPf'
    CloseDataSource = False
    DataSet = QrSTCPf
    BCDToCurrency = False
    Left = 908
    Top = 264
  end
  object QrInad: TMySQLQuery
    Database = Dmod.MyDB
    Left = 880
    Top = 292
    object QrInadCREDITO: TFloatField
      FieldName = 'CREDITO'
    end
  end
  object frxDsInad: TfrxDBDataset
    UserName = 'frxDsInad'
    CloseDataSource = False
    DataSet = QrInad
    BCDToCurrency = False
    Left = 908
    Top = 292
  end
  object QrFutDeb: TMySQLQuery
    Database = Dmod.MyDB
    Left = 936
    Top = 208
    object QrFutDebData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFutDebControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFutDebGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrFutDebDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrFutDebNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrFutDebDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrFutDebVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFutDebCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrFutDebDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrFutDebFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
  end
  object QrCNS: TMySQLQuery
   
    SQL.Strings = (
      'SELECT Nivel, Genero '
      'FROM ctasniv'
      'WHERE Ativo=1')
    Left = 936
    Top = 152
    object QrCNSNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrCNSGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object QrCNA: TMySQLQuery
   
    SQL.Strings = (
      'SELECT Nivel, Genero '
      'FROM ctasniv'
      'WHERE Ativo=1')
    Left = 964
    Top = 152
    object IntegerField1: TIntegerField
      FieldName = 'Nivel'
    end
    object IntegerField2: TIntegerField
      FieldName = 'Genero'
    end
  end
  object DsFutDeb: TDataSource
    DataSet = QrFutDeb
    Left = 964
    Top = 208
  end
  object QrSumDeb: TMySQLQuery
    Database = Dmod.MyDB
    Left = 936
    Top = 236
    object QrSumDebDEBITO: TFloatField
      FieldName = 'DEBITO'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsSumDeb: TDataSource
    DataSet = QrSumDeb
    Left = 964
    Top = 236
  end
  object QrArreBaI: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrArreBaICalcFields
    Left = 936
    Top = 264
    object QrArreBaIParcelasRestantes: TLargeintField
      FieldName = 'ParcelasRestantes'
      Required = True
    end
    object QrArreBaIValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrArreBaIPercent: TFloatField
      FieldName = 'Percent'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrArreBaIFator: TSmallintField
      FieldName = 'Fator'
    end
    object QrArreBaIDoQue: TSmallintField
      FieldName = 'DoQue'
    end
    object QrArreBaIValFracAsk: TSmallintField
      FieldName = 'ValFracAsk'
    end
    object QrArreBaIValFracDef: TFloatField
      FieldName = 'ValFracDef'
    end
    object QrArreBaITexto: TWideStringField
      FieldName = 'Texto'
      Size = 40
    end
    object QrArreBaINome: TWideStringField
      FieldName = 'Nome'
      Size = 40
    end
    object QrArreBaIVAL_RESTANTE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_RESTANTE'
      DisplayFormat = '#,###,###,##0.00'
      Calculated = True
    end
  end
  object DsArreBaI: TDataSource
    DataSet = QrArreBaI
    Left = 964
    Top = 264
  end
  object QrPagantes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(cdi.Conta) ITENS'
      'FROM condimov cdi'
      'WHERE cdi.SitImv=1'
      'AND cdi.Codigo=23')
    Left = 936
    Top = 292
    object QrPagantesITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object QrDebitos: TMySQLQuery
   
    OnCalcFields = QrDebitosCalcFields
    Left = 508
    Top = 128
    object QrDebitosCOMPENSADO_TXT: TWideStringField
      FieldName = 'COMPENSADO_TXT'
      Size = 8
    end
    object QrDebitosDATA: TWideStringField
      FieldName = 'DATA'
      Size = 8
    end
    object QrDebitosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDebitosDEBITO: TFloatField
      FieldName = 'DEBITO'
    end
    object QrDebitosNOTAFISCAL: TLargeintField
      FieldName = 'NOTAFISCAL'
    end
    object QrDebitosSERIECH: TWideStringField
      FieldName = 'SERIECH'
      Size = 10
    end
    object QrDebitosDOCUMENTO: TFloatField
      FieldName = 'DOCUMENTO'
    end
    object QrDebitosMEZ: TLargeintField
      FieldName = 'MEZ'
      Required = True
    end
    object QrDebitosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrDebitosNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrDebitosNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrDebitosNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrDebitosITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrDebitosMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 7
      Calculated = True
    end
    object QrDebitosSERIE_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_DOC'
      Size = 30
      Calculated = True
    end
    object QrDebitosNF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NF_TXT'
      Size = 30
      Calculated = True
    end
    object QrDebitosMES2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES2'
      Size = 5
      Calculated = True
    end
    object QrDebitosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDebitosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrDebitosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDebitosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrDebitosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrDebitosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrDebitosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrDebitosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
  end
  object DsDebitos: TDataSource
    DataSet = QrDebitos
    Left = 536
    Top = 128
  end
  object frxDsDebitos: TfrxDBDataset
    UserName = 'frxDsDebitos'
    CloseDataSource = False
    DataSet = QrDebitos
    BCDToCurrency = False
    Left = 564
    Top = 128
  end
  object frxDsCreditos: TfrxDBDataset
    UserName = 'frxDsCreditos'
    CloseDataSource = False
    DataSet = QrCreditos
    BCDToCurrency = False
    Left = 564
    Top = 156
  end
  object DsCreditos: TDataSource
    DataSet = QrCreditos
    Left = 536
    Top = 156
  end
  object QrCreditos: TMySQLQuery
   
    OnCalcFields = QrCreditosCalcFields
    Left = 508
    Top = 156
    object QrCreditosMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrCreditosCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrCreditosNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrCreditosNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrCreditosNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrCreditosMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 7
      Calculated = True
    end
    object QrCreditosNOMECON_2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECON_2'
      Size = 255
      Calculated = True
    end
    object QrCreditosSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrCreditosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCreditosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrCreditosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrCreditosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrCreditosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrCreditosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrCreditosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrCreditosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCreditosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
  end
  object QrSaldoA: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSaldoACalcFields
    Left = 508
    Top = 212
    object QrSaldoAInicial: TFloatField
      FieldName = 'Inicial'
    end
    object QrSaldoASALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrSaldoATOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL'
      Calculated = True
    end
  end
  object frxDsSaldoA: TfrxDBDataset
    UserName = 'frxDsSaldoA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Inicial=Inicial'
      'SALDO=SALDO'
      'TOTAL=TOTAL')
    DataSet = QrSaldoA
    BCDToCurrency = False
    Left = 536
    Top = 212
  end
  object QrResumo: TMySQLQuery
   
    OnCalcFields = QrResumoCalcFields
    Left = 508
    Top = 184
    object QrResumoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrResumoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrResumoSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      Calculated = True
    end
    object QrResumoFINAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FINAL'
      Calculated = True
    end
  end
  object frxDsResumo: TfrxDBDataset
    UserName = 'frxDsResumo'
    CloseDataSource = False
    DataSet = QrResumo
    BCDToCurrency = False
    Left = 536
    Top = 184
  end
end
