object FmFinOrfao1: TFmFinOrfao1
  Left = 339
  Top = 185
  Caption = 'FIN-ORFAO-001 :: Lan'#231'amentos Sem Cliente Interno'
  ClientHeight = 639
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 439
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGLct: TdmkDBGrid
      Left = 0
      Top = 0
      Width = 1241
      Height = 439
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Carteira'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECARTEIRA'
          Title.Caption = 'Descri'#231#227'o da carteira'
          Width = 216
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Bloqueto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatID'
          Title.Caption = 'Tipo'
          Width = 29
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_Pgto'
          Title.Caption = 'ID Pagto'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Protocolo'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SERIE_CHEQUE'
          Title.Caption = 'S'#233'rie/Docum.'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 164
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vencim.'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'COMPENSADO_TXT'
          Title.Caption = 'Compen.'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MENSAL'
          Title.Caption = 'M'#234's'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESIT'
          Title.Caption = 'Situa'#231#227'o'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Lan'#231'to'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sub'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SALDO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMERELACIONADO'
          Title.Caption = 'Cliente / Fornecedor'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Title.Caption = 'Quantidade'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFORNECEI'
          Title.Caption = 'Propriet'#225'rio'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MultaVal'
          Title.Caption = 'Multa paga'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MoraVal'
          Title.Caption = 'Juros pagos'
          Width = 56
          Visible = True
        end>
      Color = clWindow
      DataSource = DsELSCI
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      FieldsCalcToOrder.Strings = (
        'NOMESIT=Sit,Vencimento'
        'SERIE_CHEQUE=SerieCH,Documento'
        'COMPENSADO_TXT=Compensado'
        'MENSAL=Mez')
      Columns = <
        item
          Expanded = False
          FieldName = 'Carteira'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECARTEIRA'
          Title.Caption = 'Descri'#231#227'o da carteira'
          Width = 216
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Bloqueto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatID'
          Title.Caption = 'Tipo'
          Width = 29
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_Pgto'
          Title.Caption = 'ID Pagto'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Protocolo'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SERIE_CHEQUE'
          Title.Caption = 'S'#233'rie/Docum.'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 164
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vencim.'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'COMPENSADO_TXT'
          Title.Caption = 'Compen.'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MENSAL'
          Title.Caption = 'M'#234's'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESIT'
          Title.Caption = 'Situa'#231#227'o'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Lan'#231'to'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sub'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SALDO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMERELACIONADO'
          Title.Caption = 'Cliente / Fornecedor'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Title.Caption = 'Quantidade'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFORNECEI'
          Title.Caption = 'Propriet'#225'rio'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MultaVal'
          Title.Caption = 'Multa paga'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MoraVal'
          Title.Caption = 'Juros pagos'
          Width = 56
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 483
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lan'#231'amentos Sem Cliente Interno'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 483
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lan'#231'amentos Sem Cliente Interno'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 483
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lan'#231'amentos Sem Cliente Interno'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 498
    Width = 1241
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1236
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 553
    Width = 1241
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 18
      Width = 1236
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 1058
        Top = 0
        Width = 178
        Height = 65
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 15
          Top = 4
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtAcao: TBitBtn
        Tag = 10010
        Left = 25
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&A'#231#227'o'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAcaoClick
      end
    end
  end
  object DsELSCI: TDataSource
    DataSet = DModFin.QrELSCI
    Left = 136
    Top = 124
  end
  object PMAcao: TPopupMenu
    Left = 52
    Top = 416
    object Atribuirlanamentoaodonodacarteira1: TMenuItem
      Caption = 'Atribuir lan'#231'amento(s) ao dono da carteira'
      OnClick = Atribuirlanamentoaodonodacarteira1Click
    end
    object Excluirlanamentoincondicionalmente1: TMenuItem
      Caption = 'Excluir lan'#231'amento(s) incondicionalmente'
      OnClick = Excluirlanamentoincondicionalmente1Click
    end
  end
end
