object FmCentroCustoCtas: TFmCentroCustoCtas
  Left = 339
  Top = 185
  Caption = 'FIN-CCUST-002 :: Adi'#231#227'o de Conta ao Centro de Custo'
  ClientHeight = 449
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 287
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 570
    object PnMaskPesq: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 287
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 570
      object Panel5: TPanel
        Left = 0
        Top = 44
        Width = 784
        Height = 44
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 570
        object Label18: TLabel
          Left = 4
          Top = 4
          Width = 186
          Height = 13
          Caption = 'Digite parte da descri'#231#227'o (min. 3 letras):'
        end
        object EdLinkMask: TEdit
          Left = 4
          Top = 20
          Width = 201
          Height = 21
          TabOrder = 0
          OnChange = EdLinkMaskChange
        end
        object CkSoZerados: TCheckBox
          Left = 216
          Top = 20
          Width = 269
          Height = 17
          Caption = 'Somente contas sem centro de custo informado.'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 44
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitWidth = 570
        object Label1: TLabel
          Left = 4
          Top = 4
          Width = 79
          Height = 13
          Caption = 'Centro de Custo:'
          FocusControl = DBEdit1
        end
        object DBEdit1: TDBEdit
          Left = 4
          Top = 20
          Width = 53
          Height = 21
          DataField = 'Codigo'
          DataSource = FmCentroCusto.DsCentroCusto
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHighlight
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 56
          Top = 20
          Width = 424
          Height = 21
          DataField = 'Nome'
          DataSource = FmCentroCusto.DsCentroCusto
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHighlight
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object DBGContas: TdmkDBGridZTO
        Left = 0
        Top = 88
        Width = 784
        Height = 199
        Align = alClient
        DataSource = DsContas
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit, dgMultiSelect]
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        OnDrawColumnCell = DBGContasDrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            ReadOnly = True
            Title.Caption = 'C'#243'digo'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Width = 217
            Visible = True
          end
          item
            Expanded = False
            FieldName = '_S'
            Title.Caption = 'S*'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = '_M'
            Title.Caption = 'M*'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = '_C'
            Title.Caption = 'C*'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = '_D'
            Title.Caption = 'D*'
            Width = 17
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = '_R'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Title.Caption = '*R'
            Width = 20
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = '_P'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Title.Caption = '*P'
            Width = 20
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOM_SGR'
            Title.Caption = 'Sub grupo'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOM_GRU'
            Title.Caption = 'Grupo'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOM_CJT'
            Title.Caption = 'Conjunto'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOM_PLA'
            Title.Caption = 'Plano'
            Width = 240
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 570
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 522
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 474
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 448
        Height = 32
        Caption = 'Adi'#231#227'o de Conta ao Centro de Custo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 448
        Height = 32
        Caption = 'Adi'#231#227'o de Conta ao Centro de Custo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 448
        Height = 32
        Caption = 'Adi'#231#227'o de Conta ao Centro de Custo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 335
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 570
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 566
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 379
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 570
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 566
      object Label2: TLabel
        Left = 152
        Top = 0
        Width = 87
        Height = 13
        Caption = '*S: Controla saldo.'
      end
      object Label3: TLabel
        Left = 252
        Top = 0
        Width = 56
        Height = 13
        Caption = '*M: Mensal.'
      end
      object Label4: TLabel
        Left = 324
        Top = 0
        Width = 52
        Height = 13
        Caption = '*D: D'#233'bito.'
      end
      object Label5: TLabel
        Left = 384
        Top = 0
        Width = 53
        Height = 13
        Caption = '*C: Cr'#233'dito.'
      end
      object Label6: TLabel
        Left = 152
        Top = 16
        Width = 256
        Height = 13
        Caption = '*R: Centro de Resultado D=Definitivo, A=Acumulativo.'
      end
      object Label8: TLabel
        Left = 152
        Top = 32
        Width = 292
        Height = 13
        Caption = '*P: Caracteristica Principal. P=Pagar, R=Receber e A=Ambos.'
      end
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 422
        object BtSaida: TBitBtn
          Tag = 13
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Aciciona'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 140
    Top = 252
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrContasCalcFields
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Nome LIKE "%%"'
      'ORDER BY Nome')
    Left = 140
    Top = 204
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContasCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
      Required = True
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContasRateio: TIntegerField
      FieldName = 'Rateio'
      Required = True
    end
    object QrContasEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrContasAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrContasPendenMesSeg: TSmallintField
      FieldName = 'PendenMesSeg'
      Required = True
    end
    object QrContasCalculMesSeg: TSmallintField
      FieldName = 'CalculMesSeg'
      Required = True
    end
    object QrContasOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Required = True
    end
    object QrContasContasAgr: TIntegerField
      FieldName = 'ContasAgr'
      Required = True
    end
    object QrContasContasSum: TIntegerField
      FieldName = 'ContasSum'
      Required = True
    end
    object QrContasCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
      Required = True
    end
    object QrContasNotPrntBal: TIntegerField
      FieldName = 'NotPrntBal'
      Required = True
    end
    object QrContasSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
    end
    object QrContasProvRat: TSmallintField
      FieldName = 'ProvRat'
      Required = True
    end
    object QrContasNotPrntFin: TIntegerField
      FieldName = 'NotPrntFin'
      Required = True
    end
    object QrContasTemDocFisi: TSmallintField
      FieldName = 'TemDocFisi'
      Required = True
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrContasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrContasCentroRes: TSmallintField
      FieldName = 'CentroRes'
      Required = True
    end
    object QrContasPagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrContas_S: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_S'
      Calculated = True
    end
    object QrContas_M: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_M'
      Calculated = True
    end
    object QrContas_C: TIntegerField
      FieldKind = fkCalculated
      FieldName = '_C'
      Calculated = True
    end
    object QrContas_D: TIntegerField
      FieldKind = fkCalculated
      FieldName = '_D'
      Calculated = True
    end
    object QrContas_P: TIntegerField
      FieldKind = fkCalculated
      FieldName = '_P'
      Calculated = True
    end
    object QrContas_R: TIntegerField
      FieldKind = fkCalculated
      FieldName = '_R'
      Calculated = True
    end
    object QrContasLetraP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LetraP'
      Size = 1
      Calculated = True
    end
    object QrContasLetraR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LetraR'
      Size = 1
      Calculated = True
    end
    object QrContasCOD_PLA: TIntegerField
      FieldName = 'COD_PLA'
      Required = True
    end
    object QrContasNOM_PLA: TWideStringField
      FieldName = 'NOM_PLA'
      Size = 50
    end
    object QrContasCOD_CJT: TIntegerField
      FieldName = 'COD_CJT'
      Required = True
    end
    object QrContasNOM_CJT: TWideStringField
      FieldName = 'NOM_CJT'
      Size = 50
    end
    object QrContasCOD_GRU: TIntegerField
      FieldName = 'COD_GRU'
      Required = True
    end
    object QrContasNOM_GRU: TWideStringField
      FieldName = 'NOM_GRU'
      Size = 50
    end
    object QrContasCOD_SGR: TIntegerField
      FieldName = 'COD_SGR'
      Required = True
    end
    object QrContasNOM_SGR: TWideStringField
      FieldName = 'NOM_SGR'
      Size = 50
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 260
    Top = 12
  end
end
