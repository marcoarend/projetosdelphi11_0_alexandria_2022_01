unit CentroCustoCtas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, Grids, DBGrids,
  Mask, DBCtrls, dmkPermissoes, dmkGeral, dmkImage, UnDmkEnums, dmkDBGridZTO;

type
  TFmCentroCustoCtas = class(TForm)
    Panel1: TPanel;
    PnMaskPesq: TPanel;
    Panel5: TPanel;
    Label18: TLabel;
    EdLinkMask: TEdit;
    DsContas: TDataSource;
    QrContas: TmySQLQuery;
    Panel3: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    DBGContas: TdmkDBGridZTO;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasCentroCusto: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasRateio: TIntegerField;
    QrContasEntidade: TIntegerField;
    QrContasAntigo: TWideStringField;
    QrContasPendenMesSeg: TSmallintField;
    QrContasCalculMesSeg: TSmallintField;
    QrContasOrdemLista: TIntegerField;
    QrContasContasAgr: TIntegerField;
    QrContasContasSum: TIntegerField;
    QrContasCtrlaSdo: TSmallintField;
    QrContasNotPrntBal: TIntegerField;
    QrContasSigla: TWideStringField;
    QrContasProvRat: TSmallintField;
    QrContasNotPrntFin: TIntegerField;
    QrContasTemDocFisi: TSmallintField;
    QrContasLk: TIntegerField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TIntegerField;
    QrContasUserAlt: TIntegerField;
    QrContasAlterWeb: TSmallintField;
    QrContasAtivo: TSmallintField;
    QrContasCentroRes: TSmallintField;
    QrContasPagRec: TSmallintField;
    QrContas_S: TSmallintField;
    QrContas_M: TSmallintField;
    QrContas_C: TIntegerField;
    QrContas_D: TIntegerField;
    QrContas_P: TIntegerField;
    QrContas_R: TIntegerField;
    QrContasLetraP: TWideStringField;
    QrContasLetraR: TWideStringField;
    QrContasCOD_PLA: TIntegerField;
    QrContasNOM_PLA: TWideStringField;
    QrContasCOD_CJT: TIntegerField;
    QrContasNOM_CJT: TWideStringField;
    QrContasCOD_GRU: TIntegerField;
    QrContasNOM_GRU: TWideStringField;
    QrContasCOD_SGR: TIntegerField;
    QrContasNOM_SGR: TWideStringField;
    CkSoZerados: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdLinkMaskChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrContasCalcFields(DataSet: TDataSet);
    procedure DBGContasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    function Adiciona(): Boolean;
  public
    { Public declarations }
  end;

  var
  FmCentroCustoCtas: TFmCentroCustoCtas;

implementation

{$R *.DFM}

uses UnMyObjects, QuaisItens, Module, CentroCusto, MyVCLSkin, UnDmkProcFunc,
  DmkDAC_PF;

procedure TFmCentroCustoCtas.BtOKClick(Sender: TObject);
begin
  Adiciona;
end;

procedure TFmCentroCustoCtas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCentroCustoCtas.DBGContasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
  Inverte = False;
var
  CorFundo, CorTexto: TColor;
begin
  if Column.FieldName = '_M' then
    MeuVCLSkin.DrawGrid(TDbGrid(DBGContas), Rect, 1, QrContas_M.Value);
  if Column.FieldName = '_S' then
    MeuVCLSkin.DrawGrid(TDbGrid(DBGContas), Rect, 1, QrContas_S.Value);
  if Column.FieldName = '_C' then
    MeuVCLSkin.DrawGrid(TDbGrid(DBGContas), Rect, 1, QrContas_C.Value);
  if Column.FieldName = '_D' then
    MeuVCLSkin.DrawGrid(TDbGrid(DBGContas), Rect, 1, QrContas_D.Value);
  //

  if Column.FieldName = '_P' then
  begin
    dmkPF.CorFundoEFonte(
      TdmkTemaCores(QrContas_P.Value), CorFundo, CorTexto, Inverte);
    MyObjects.DesenhaTextoEmDBGrid(TDbGrid(DBGContas), Rect,
      CorTexto, CorFundo, Column.Alignment, QrContasLetraP.Value);
  end;

  if Column.FieldName = '_R' then
  begin
    dmkPF.CorFundoEFonte(
      TdmkTemaCores(QrContas_R.Value), CorFundo, CorTexto, Inverte);
    MyObjects.DesenhaTextoEmDBGrid(TDbGrid(DBGContas), Rect,
      CorTexto, CorFundo, Column.Alignment, QrContasLetraR.Value);
  end;

end;

procedure TFmCentroCustoCtas.EdLinkMaskChange(Sender: TObject);
var
  Zerados: String;
begin
  QrContas.Close;
  if Length(EdLinkMask.Text) >= 3 then
  begin
    if CkSoZerados.Checked then
      Zerados := 'AND CentroCusto=0'
    else
      Zerados := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrContas, Dmod.MyDB, [
    'SELECT con.*, ',
    'pla.Codigo COD_PLA, pla.Nome NOM_PLA,  ',
    'cjt.Codigo COD_CJT, cjt.Nome NOM_CJT,  ',
    'gru.Codigo COD_GRU, gru.Nome NOM_GRU,  ',
    'sgr.Codigo COD_SGR, sgr.Nome NOM_SGR   ',
    'FROM contas con  ',
    'LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo  ',
    'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo  ',
    'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto  ',
    'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano  ',
    'WHERE con.Codigo > 0 ',
    'AND con.Nome LIKE "%' + EdLinkMask.Text + '%" ',
    Zerados,
    'ORDER BY con.Nome ',
    '']);
  end;
end;

procedure TFmCentroCustoCtas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCentroCustoCtas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmCentroCustoCtas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCentroCustoCtas.QrContasCalcFields(DataSet: TDataSet);
begin
  if QrContasMensal.Value = '' then QrContas_M.Value := 0
  else QrContas_M.Value := dmkPF.V_FToInt(QrContasMensal.Value[1]);

  QrContas_S.Value := QrContasCtrlaSdo.Value;

  if QrContasCredito.Value = '' then QrContas_C.Value := 0
  else QrContas_C.Value := dmkPF.V_FToInt(QrContasCredito.Value[1]);

  if QrContasDebito.Value = '' then QrContas_D.Value := 0
  else QrContas_D.Value := dmkPF.V_FToInt(QrContasDebito.Value[1]);
  //

  case QrContasCentroRes.Value of
    1: QrContas_R.Value := Integer(dmkcorXcel2k7Incorreto); //'D';
    2: QrContas_R.Value := Integer(dmkcorXcel2k7Bom); //'A';
    else QrContas_R.Value := Integer(dmkcorXcel2k7Saida); //'';
  end;
  case QrContasPagRec.Value of
    -1: QrContas_P.Value := Integer(dmkcorXcel2k7Incorreto); //'P';
     0: QrContas_P.Value := Integer(dmkcorXcel2k7Neutra); //'A';
     1: QrContas_P.Value := Integer(dmkcorXcel2k7Bom); //'R';
     else QrContas_P.Value := Integer(dmkcorNotaVermelho); //'?';
  end;
  case QrContasCentroRes.Value of
    1: QrContasLetraR.Value := 'D';
    2: QrContasLetraR.Value := 'A';
    else QrContasLetraR.Value := '?';
  end;
  case QrContasPagRec.Value of
    -1: QrContasLetraP.Value := 'P';
     0: QrContasLetraP.Value := 'A';
     1: QrContasLetraP.Value := 'R';
     else QrContasLetraP.Value := '?';
  end;
end;

function TFmCentroCustoCtas.Adiciona(): Boolean;
  function ExecutaSQL: Boolean;
  begin
    //Result := False;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE contas SET CentroCusto=' +
      FormatFloat('0', FmCentroCusto.QrCentroCustoCodigo.Value) + 
      ' WHERE Codigo=' + FormatFloat('0', QrContasCodigo.Value));
    Dmod.QrUpd.ExecSQL;
    Result := True;
  end;
var
  n, m: integer;
  q: TSelType;
begin
  q := istNenhum;
  Result := False;
  if (QrContas.State <> dsBrowse) or (QrContas.RecordCount = 0) then
  begin
    Geral.MB_Aviso('N�o h� item a ser inclu�do!');
    Exit;
  end;
  if MyObjects.CriaForm_AcessoTotal(TFmQuaisItens, FmQuaisItens) then
  begin
    with FmQuaisItens do
    begin
      ShowModal;
      if not FSelecionou then
      begin
        Geral.MB_Aviso('Inclus�o cancelada pelo usu�rio!');
        q := istDesiste;
      end else q := FEscolha;
      Destroy;
    end;
  end;
  if q = istDesiste then Exit;
  //
  Screen.Cursor := crHourGlass;
  m := 0;
  if (q = istSelecionados) and (DBGContas.SelectedRows.Count < 2) then
    q := istAtual;
  case q of
    istAtual:
    begin
      if Geral.MB_Pergunta('Confirma a inclus�o do item selecionado?') = ID_YES then
        if ExecutaSQL() then m := 1;
    end;
    istSelecionados:
    begin
      if Geral.MB_Pergunta('Confirma a inclus�o dos ' +
      IntToStr(DBGContas.SelectedRows.Count) + ' itens selecionados?') = ID_YES then
      begin
        with DBGContas.DataSource.DataSet do
        for n := 0 to DBGContas.SelectedRows.Count-1 do
        begin
          //GotoBookmark(DBGContas.SelectedRows.Items[n]);
          GotoBookmark(DBGContas.SelectedRows.Items[n]);
          if ExecutaSQL() then inc(m, 1);
        end;
      end;
    end;
    istTodos:
    begin
      if Geral.MB_Pergunta('Confirma a inclus�o de todos os ' +
      IntToStr(QrContas.RecordCount) + ' itens pesquisados?') = ID_YES then
      begin
        QrContas.First;
        while not QrContas.Eof do
        begin
          if ExecutaSQL() then inc(m, 1);
          QrContas.Next;
        end;
      end;
    end;
  end;
  if m > 0 then
  begin
    Result := True;
    FmCentroCusto.ReopenContas(QrContasCodigo.Value);
    UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
    if m = 1 then Geral.MB_Info('Um item foi inclu�do!') else
    Geral.MB_Info(IntToStr(m) + ' itens foram inclu�dos!');
  end;
  Screen.Cursor := crDefault;
end;

end.

