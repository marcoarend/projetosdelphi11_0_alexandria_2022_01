unit CartTalCH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmCartTalCH = class(TForm)
    Panel1: TPanel;
    QrEntiTipCto: TmySQLQuery;
    DsEntiTipCto: TDataSource;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label8: TLabel;
    EdControle: TdmkEdit;
    EdSerie: TdmkEdit;
    QrEntiTipCtoCodigo: TIntegerField;
    QrEntiTipCtoCodUsu: TIntegerField;
    QrEntiTipCtoNome: TWideStringField;
    EdNumIni: TdmkEdit;
    Label10: TLabel;
    EdNumFim: TdmkEdit;
    Label1: TLabel;
    EdNumAtu: TdmkEdit;
    LaNumAtu: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCartTalCH: TFmCartTalCH;

implementation

uses UnMyObjects, Module, Carteiras, UMySQLModule, UnInternalConsts, Principal,
  MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmCartTalCH.BtOKClick(Sender: TObject);
var
  Controle, Ini, Fim, Atu: Integer;
begin
  Ini := EdNumIni.ValueVariant;
  Fim := EdNumFim.ValueVariant;
  Atu := EdNumAtu.ValueVariant;
  //
  if (Ini = 0) or (Fim = 0) or (Ini > Fim) or (Fim < Ini)
  or ((Atu < Ini) and (Atu > 0)) or ((Atu > Fim) and (Atu > 0)) then
  begin
    Geral.MB_Aviso('Erro de numera��o!');
    Exit;
  end;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('carttalch', 'Controle', ImgTipo.SQLType,
  EdControle.ValueVariant);
  //
  if UMyMod.ExecSQLInsUpdFm(FmCartTalCH, ImgTipo.SQLType, 'carttalch',
  Controle, Dmod.QrUpd) then
  begin
    FmCarteiras.ReopenCartTalCH(Controle);
    if CkContinuar.Checked then
    begin
      Geral.MB_Aviso('Tal�o inclu�do / alterado!');
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant   := 0;
      EdSerie.ValueVariant      := 0;
      EdNumIni.ValueVariant     := 0;
      EdNumFim.ValueVariant     := 0;
      EdNumAtu.ValueVariant     := 0;
      EdSerie.SetFocus;
    end else Close;
  end;
end;

procedure TFmCartTalCH.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCartTalCH.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCartTalCH.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrEntiTipCto, Dmod.MyDB);
end;

procedure TFmCartTalCH.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
