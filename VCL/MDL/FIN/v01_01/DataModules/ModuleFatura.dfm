object DmFatura: TDmFatura
  Height = 407
  Width = 451
  PixelsPerInch = 96
  object QrNF_X: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SerieNFTxt, NumeroNF'
      'FROM stqmovnfsa'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1'
      'AND Empresa=:P2')
    Left = 48
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNF_XSerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Size = 5
    end
    object QrNF_XNumeroNF: TIntegerField
      FieldName = 'NumeroNF'
    end
  end
  object QrPrzT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Dias, Percent1, Percent2'
      'FROM pediprzits'
      'WHERE Codigo=:P0'
      'ORDER BY Dias')
    Left = 48
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrzTDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPrzTPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPrzTPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrPrzTControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPrzX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Dias, Percent1 Percent'
      'FROM pediprzits'
      'WHERE Codigo=:P0'
      'AND Percent1 > 0'
      'ORDER BY Dias')
    Left = 48
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrzXDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPrzXPercent: TFloatField
      FieldName = 'Percent'
      Required = True
    end
    object QrPrzXControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSumT: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ppi.Percent1) Percent1, SUM(ppi.Percent2) Percent2 ,'
      'ppc.JurosMes'
      'FROM pediprzits ppi'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=ppi.Codigo'
      'WHERE ppi.Codigo=:P0'
      'GROUP BY ppi.Codigo')
    Left = 48
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumTPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrSumTPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrSumTJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
  end
  object QrSumX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Total) Total '
      'FROM stqmovvala'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1 '
      'AND Empresa=:P2')
    Left = 48
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSumXTotal: TFloatField
      FieldName = 'Total'
    end
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT par.CtaProdVen, par.CtaServico,'
      'par.FaturaSeq, par.FaturaSep, '
      'par.FaturaDta, par.FaturaNum,'
      'par.TxtProdVen, par.DupProdVen,'
      'par.TxtServico, par.DupServico'
      'FROM paramsemp par '
      'WHERE par.Codigo>0')
    Left = 48
    Top = 4
    object QrParamsEmpCtaProdVen: TIntegerField
      FieldName = 'CtaProdVen'
    end
    object QrParamsEmpFaturaSeq: TSmallintField
      FieldName = 'FaturaSeq'
    end
    object QrParamsEmpFaturaSep: TWideStringField
      FieldName = 'FaturaSep'
      Size = 1
    end
    object QrParamsEmpFaturaDta: TSmallintField
      FieldName = 'FaturaDta'
    end
    object QrParamsEmpFaturaNum: TSmallintField
      FieldName = 'FaturaNum'
    end
    object QrParamsEmpTxtProdVen: TWideStringField
      FieldName = 'TxtProdVen'
      Size = 100
    end
    object QrParamsEmpDupProdVen: TWideStringField
      FieldName = 'DupProdVen'
      Size = 3
    end
    object QrParamsEmpCtaServico: TIntegerField
      FieldName = 'CtaServico'
    end
    object QrParamsEmpTxtServico: TWideStringField
      FieldName = 'TxtServico'
      Size = 100
    end
    object QrParamsEmpDupServico: TWideStringField
      FieldName = 'DupServico'
      Size = 3
    end
    object QrParamsEmpDupMultiSV: TWideStringField
      FieldName = 'DupMultiSV'
      Size = 3
    end
    object QrParamsEmpCtaMultiSV: TIntegerField
      FieldName = 'CtaMultiSV'
    end
    object QrParamsEmpTxtMultiSV: TWideStringField
      FieldName = 'TxtMultiSV'
      Size = 100
    end
  end
  object QrPediPrzCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, MaxDesco,'
      'JurosMes, Parcelas, Percent1, Percent2,'
      'MedDDSimpl'
      'FROM pediprzcab'
      'WHERE Aplicacao & :P0 <> 0'
      'ORDER BY Nome')
    Left = 132
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrPediPrzCabMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
      Required = True
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 204
    Top = 4
  end
  object QrCartEmis: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome, car.Tipo'
      'FROM carteiras car'
      'WHERE Tipo=2'
      'ORDER BY Nome')
    Left = 132
    Top = 52
    object QrCartEmisCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCartEmisNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCartEmisTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsCartEmis: TDataSource
    DataSet = QrCartEmis
    Left = 204
    Top = 52
  end
  object QrFisRegCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.ModeloNF,'
      'frc.Nome, frc.Financeiro, imp.Nome NO_MODELO_NF'
      'FROM fisregcad frc'
      'LEFT JOIN imprime imp ON imp.Codigo=frc.ModeloNF'
      'ORDER BY Nome')
    Left = 132
    Top = 100
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFisRegCadModeloNF: TIntegerField
      FieldName = 'ModeloNF'
      Required = True
    end
    object QrFisRegCadNO_MODELO_NF: TWideStringField
      FieldName = 'NO_MODELO_NF'
      Size = 100
    end
    object QrFisRegCadFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 204
    Top = 100
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome, car.Tipo'
      'FROM carteiras car'
      'WHERE Tipo=2'
      'ORDER BY Nome')
    Left = 132
    Top = 156
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 204
    Top = 156
  end
  object QrEntiCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome, car.Tipo'
      'FROM carteiras car'
      'WHERE Tipo=2'
      'ORDER BY Nome')
    Left = 44
    Top = 52
    object QrEntiCliIntCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object QrEntiCliIntCodFilial: TIntegerField
      FieldName = 'CodFilial'
    end
  end
  object QrProd: TMySQLQuery
    Database = Dmod.MyDB
    Left = 128
    Top = 236
    object QrProdUsaSubsTrib: TSmallintField
      FieldName = 'UsaSubsTrib'
    end
  end
end
