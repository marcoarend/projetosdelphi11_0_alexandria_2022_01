unit ModuleLct2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  mySQLDbTables, dmkGeral, frxClass, frxDBSet, dmkEditDateTimePicker,
  Variants, DBGrids, UnDmkProcFunc, DmkDAC_PF, UnDmkEnums, dmkPageControl,
  UnProjGroup_Consts, UnGrl_Vars;

type
  TDmLct2 = class(TDataModule)
    DsCrt: TDataSource;
    DsLct: TDataSource;
    DsCrtSum: TDataSource;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctLk: TIntegerField;
    QrLctFatID: TIntegerField;
    QrLctFatParcela: TIntegerField;
    QrLctCONTA: TIntegerField;
    QrLctNOMECONTA: TWideStringField;
    QrLctNOMEEMPRESA: TWideStringField;
    QrLctNOMESUBGRUPO: TWideStringField;
    QrLctNOMEGRUPO: TWideStringField;
    QrLctNOMECONJUNTO: TWideStringField;
    QrLctNOMESIT: TWideStringField;
    QrLctAno: TFloatField;
    QrLctMENSAL: TWideStringField;
    QrLctMENSAL2: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctLocal: TIntegerField;
    QrLctFatura: TWideStringField;
    QrLctSub: TSmallintField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctPago: TFloatField;
    QrLctSALDO: TFloatField;
    QrLctID_Sub: TSmallintField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctcliente: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctNOMECLIENTE: TWideStringField;
    QrLctNOMEFORNECEDOR: TWideStringField;
    QrLctTIPOEM: TWideStringField;
    QrLctNOMERELACIONADO: TWideStringField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctMulta: TFloatField;
    QrLctATRASO: TFloatField;
    QrLctJUROS: TFloatField;
    QrLctDataDoc: TDateField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctMes2: TLargeintField;
    QrLctProtesto: TDateField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TSmallintField;
    QrLctUserAlt: TSmallintField;
    QrLctControle: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctCtrlIni: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctCOMPENSADO_TXT: TWideStringField;
    QrLctCliInt: TIntegerField;
    QrLctDepto: TIntegerField;
    QrLctDescoPor: TIntegerField;
    QrLctPrazo: TSmallintField;
    QrLctForneceI: TIntegerField;
    QrLctQtde: TFloatField;
    QrLctFatNum: TFloatField;
    QrLctEmitente: TWideStringField;
    QrLctContaCorrente: TWideStringField;
    QrLctCNPJCPF: TWideStringField;
    QrLctDescoVal: TFloatField;
    QrLctDescoControle: TIntegerField;
    QrLctNFVal: TFloatField;
    QrLctAntigo: TWideStringField;
    QrLctUnidade: TIntegerField;
    QrLctExcelGru: TIntegerField;
    QrLctSerieCH: TWideStringField;
    QrLctMoraVal: TFloatField;
    QrLctMultaVal: TFloatField;
    QrLctDoc2: TWideStringField;
    QrLctCNAB_Sit: TSmallintField;
    QrLctTipoCH: TSmallintField;
    QrLctID_Quit: TIntegerField;
    QrLctReparcel: TIntegerField;
    QrLctAtrelado: TIntegerField;
    QrLctPagMul: TFloatField;
    QrLctPagJur: TFloatField;
    QrLctSubPgto1: TIntegerField;
    QrLctMultiPgto: TIntegerField;
    QrLctProtocolo: TIntegerField;
    QrLctCtrlQuitPg: TIntegerField;
    QrLctAlterWeb: TSmallintField;
    QrLctAtivo: TSmallintField;
    QrLctSerieNF: TWideStringField;
    QrLctEndossas: TSmallintField;
    QrLctEndossan: TFloatField;
    QrLctEndossad: TFloatField;
    QrLctCancelado: TSmallintField;
    QrLctEventosCad: TIntegerField;
    QrCrt: TmySQLQuery;
    QrCrtDIFERENCA: TFloatField;
    QrCrtTIPOPRAZO: TWideStringField;
    QrCrtNOMEPAGREC: TWideStringField;
    QrCrtCodigo: TIntegerField;
    QrCrtNome: TWideStringField;
    QrCrtTipo: TIntegerField;
    QrCrtInicial: TFloatField;
    QrCrtBanco: TIntegerField;
    QrCrtID: TWideStringField;
    QrCrtFatura: TWideStringField;
    QrCrtID_Fat: TWideStringField;
    QrCrtSaldo: TFloatField;
    QrCrtLk: TIntegerField;
    QrCrtEmCaixa: TFloatField;
    QrCrtFechamento: TIntegerField;
    QrCrtPrazo: TSmallintField;
    QrCrtDataCad: TDateField;
    QrCrtDataAlt: TDateField;
    QrCrtUserCad: TIntegerField;
    QrCrtUserAlt: TIntegerField;
    QrCrtPagRec: TIntegerField;
    QrCrtDiaMesVence: TSmallintField;
    QrCrtExigeNumCheque: TSmallintField;
    QrCrtForneceI: TIntegerField;
    QrCrtNome2: TWideStringField;
    QrCrtTipoDoc: TSmallintField;
    QrCrtBanco1: TIntegerField;
    QrCrtAgencia1: TIntegerField;
    QrCrtConta1: TWideStringField;
    QrCrtCheque1: TIntegerField;
    QrCrtContato1: TWideStringField;
    QrCrtAntigo: TWideStringField;
    QrCrtContab: TWideStringField;
    QrCrtOrdem: TIntegerField;
    QrCrtFuturoC: TFloatField;
    QrCrtFuturoD: TFloatField;
    QrCrtFuturoS: TFloatField;
    QrCrtForneceN: TSmallintField;
    QrCrtExclusivo: TSmallintField;
    QrCrtRecebeBloq: TSmallintField;
    QrCrtEntiDent: TIntegerField;
    QrCrtCodCedente: TWideStringField;
    QrCrtAlterWeb: TSmallintField;
    QrCrtAtivo: TSmallintField;
    QrCrtValMorto: TFloatField;
    QrCrtNOMEDOBANCO: TWideStringField;
    QrCrtNOMEFORNECEI: TWideStringField;
    QrCrtSum: TmySQLQuery;
    QrCrtSumSALDO: TFloatField;
    QrCrtSumFuturoC: TFloatField;
    QrCrtSumFuturoD: TFloatField;
    QrCrtSumFuturoS: TFloatField;
    QrCrtSumEmCaixa: TFloatField;
    QrCrtSumDifere: TFloatField;
    QrCrtSumSDO_FUT: TFloatField;
    QrCrtCliInt: TIntegerField;
    QrCrtIgnorSerie: TSmallintField;
    QrCrtValIniOld: TFloatField;
    QrCrtSdoFimB: TFloatField;
    QrLctUH: TWideStringField;
    QrLctEncerrado: TIntegerField;
    QrLctErrCtrl: TIntegerField;
    QrLctNO_Carteira: TWideStringField;
    QrLctBanco1: TIntegerField;
    QrLctAgencia1: TIntegerField;
    QrLctConta1: TWideStringField;
    QrLctNOMEFORNECEI: TWideStringField;
    QrLctNO_ENDOSSADO: TWideStringField;
    QrLctSERIE_CHEQUE: TWideStringField;
    QrLctIndiPag: TIntegerField;
    QrLctAgencia: TIntegerField;
    QrLcP: TmySQLQuery;
    DsLcP: TDataSource;
    QrLcI: TmySQLQuery;
    DsQrLcI: TDataSource;
    QrLcPData: TDateField;
    QrLcPTipo: TSmallintField;
    QrLcPCarteira: TIntegerField;
    QrLcPControle: TLargeintField;
    QrLcPSub: TSmallintField;
    QrLcPCliInt: TIntegerField;
    QrLcPCliente: TIntegerField;
    QrLcPFornecedor: TIntegerField;
    QrLcPDocumento: TFloatField;
    QrLcPCredito: TFloatField;
    QrLcPDebito: TFloatField;
    QrLcPSerieCH: TWideStringField;
    QrLcPVencimento: TDateField;
    QrLcPITENS: TLargeintField;
    QrLcILancto: TLargeintField;
    QrLctFisicoSrc: TSmallintField;
    QrLctFisicoCod: TIntegerField;
    QrLctUSERCAD_TXT: TWideStringField;
    QrLctUSERALT_TXT: TWideStringField;
    QrLctDATACAD_TXT: TWideStringField;
    QrLctDATAALT_TXT: TWideStringField;
    QrCarts: TmySQLQuery;
    QrCartsDIFERENCA: TFloatField;
    QrCartsTIPOPRAZO: TWideStringField;
    QrCartsNOMEPAGREC: TWideStringField;
    QrCartsNOMEFORNECEI: TWideStringField;
    QrCartsCodigo: TIntegerField;
    QrCartsTipo: TIntegerField;
    QrCartsNome: TWideStringField;
    QrCartsInicial: TFloatField;
    QrCartsBanco: TIntegerField;
    QrCartsID: TWideStringField;
    QrCartsFatura: TWideStringField;
    QrCartsID_Fat: TWideStringField;
    QrCartsSaldo: TFloatField;
    QrCartsLk: TIntegerField;
    QrCartsEmCaixa: TFloatField;
    QrCartsFechamento: TIntegerField;
    QrCartsPrazo: TSmallintField;
    QrCartsDataCad: TDateField;
    QrCartsDataAlt: TDateField;
    QrCartsUserCad: TSmallintField;
    QrCartsUserAlt: TSmallintField;
    QrCartsPagRec: TSmallintField;
    QrCartsFuturoC: TFloatField;
    QrCartsFuturoD: TFloatField;
    QrCartsFuturoS: TFloatField;
    QrCartsNOMEDOBANCO: TWideStringField;
    QrCartsDiaMesVence: TSmallintField;
    QrCartsExigeNumCheque: TSmallintField;
    QrCartsForneceI: TIntegerField;
    QrCartsNome2: TWideStringField;
    QrCartsContato1: TWideStringField;
    QrCartsTipoDoc: TSmallintField;
    QrCartsBanco1: TIntegerField;
    QrCartsAgencia1: TIntegerField;
    QrCartsConta1: TWideStringField;
    QrCartsCheque1: TIntegerField;
    QrCartsAntigo: TWideStringField;
    QrCartsContab: TWideStringField;
    QrCartsOrdem: TIntegerField;
    QrCartsForneceN: TSmallintField;
    QrCartsExclusivo: TSmallintField;
    QrCartsAlterWeb: TSmallintField;
    QrCartsRecebeBloq: TSmallintField;
    QrCartsEntiDent: TIntegerField;
    QrCartsCodCedente: TWideStringField;
    QrCartsValMorto: TFloatField;
    QrCartsAtivo: TSmallintField;
    QrCartsIgnorSerie: TSmallintField;
    DsCarts: TDataSource;
    QrLctos: TmySQLQuery;
    QrLctosData: TDateField;
    QrLctosTipo: TSmallintField;
    QrLctosCarteira: TIntegerField;
    QrLctosAutorizacao: TIntegerField;
    QrLctosGenero: TIntegerField;
    QrLctosDescricao: TWideStringField;
    QrLctosNotaFiscal: TIntegerField;
    QrLctosCompensado: TDateField;
    QrLctosDocumento: TFloatField;
    QrLctosSit: TIntegerField;
    QrLctosVencimento: TDateField;
    QrLctosLk: TIntegerField;
    QrLctosFatID: TIntegerField;
    QrLctosFatParcela: TIntegerField;
    QrLctosCONTA: TIntegerField;
    QrLctosNOMECONTA: TWideStringField;
    QrLctosNOMEEMPRESA: TWideStringField;
    QrLctosNOMESUBGRUPO: TWideStringField;
    QrLctosNOMEGRUPO: TWideStringField;
    QrLctosNOMECONJUNTO: TWideStringField;
    QrLctosNOMESIT: TWideStringField;
    QrLctosAno: TFloatField;
    QrLctosMENSAL: TWideStringField;
    QrLctosMENSAL2: TWideStringField;
    QrLctosBanco: TIntegerField;
    QrLctosLocal: TIntegerField;
    QrLctosFatura: TWideStringField;
    QrLctosSub: TSmallintField;
    QrLctosCartao: TIntegerField;
    QrLctosLinha: TIntegerField;
    QrLctosPago: TFloatField;
    QrLctosSALDO: TFloatField;
    QrLctosID_Sub: TSmallintField;
    QrLctosMez: TIntegerField;
    QrLctosFornecedor: TIntegerField;
    QrLctoscliente: TIntegerField;
    QrLctosMoraDia: TFloatField;
    QrLctosNOMECLIENTE: TWideStringField;
    QrLctosNOMEFORNECEDOR: TWideStringField;
    QrLctosTIPOEM: TWideStringField;
    QrLctosNOMERELACIONADO: TWideStringField;
    QrLctosOperCount: TIntegerField;
    QrLctosLancto: TIntegerField;
    QrLctosMulta: TFloatField;
    QrLctosATRASO: TFloatField;
    QrLctosJUROS: TFloatField;
    QrLctosDataDoc: TDateField;
    QrLctosNivel: TIntegerField;
    QrLctosVendedor: TIntegerField;
    QrLctosAccount: TIntegerField;
    QrLctosMes2: TLargeintField;
    QrLctosProtesto: TDateField;
    QrLctosDataCad: TDateField;
    QrLctosDataAlt: TDateField;
    QrLctosUserCad: TSmallintField;
    QrLctosUserAlt: TSmallintField;
    QrLctosControle: TIntegerField;
    QrLctosID_Pgto: TIntegerField;
    QrLctosCtrlIni: TIntegerField;
    QrLctosFatID_Sub: TIntegerField;
    QrLctosICMS_P: TFloatField;
    QrLctosICMS_V: TFloatField;
    QrLctosDuplicata: TWideStringField;
    QrLctosCOMPENSADO_TXT: TWideStringField;
    QrLctosCliInt: TIntegerField;
    QrLctosDepto: TIntegerField;
    QrLctosDescoPor: TIntegerField;
    QrLctosPrazo: TSmallintField;
    QrLctosForneceI: TIntegerField;
    QrLctosQtde: TFloatField;
    QrLctosEmitente: TWideStringField;
    QrLctosContaCorrente: TWideStringField;
    QrLctosCNPJCPF: TWideStringField;
    QrLctosDescoVal: TFloatField;
    QrLctosDescoControle: TIntegerField;
    QrLctosUnidade: TIntegerField;
    QrLctosNFVal: TFloatField;
    QrLctosAntigo: TWideStringField;
    QrLctosExcelGru: TIntegerField;
    QrLctosSerieCH: TWideStringField;
    QrLctosSERIE_CHEQUE: TWideStringField;
    QrLctosDoc2: TWideStringField;
    QrLctosNOMEFORNECEI: TWideStringField;
    QrLctosMoraVal: TFloatField;
    QrLctosMultaVal: TFloatField;
    QrLctosCNAB_Sit: TSmallintField;
    QrLctosBanco1: TIntegerField;
    QrLctosAgencia1: TIntegerField;
    QrLctosConta1: TWideStringField;
    QrLctosTipoCH: TSmallintField;
    QrLctosFatNum: TFloatField;
    QrLctosSerieNF: TWideStringField;
    QrLctosNO_ENDOSSADO: TWideStringField;
    QrLctosNO_Carteira: TWideStringField;
    QrLctosUH: TWideStringField;
    QrLctosID_Quit: TIntegerField;
    QrLctosReparcel: TIntegerField;
    QrLctosAtrelado: TIntegerField;
    QrLctosPagMul: TFloatField;
    QrLctosPagJur: TFloatField;
    QrLctosSubPgto1: TIntegerField;
    QrLctosMultiPgto: TIntegerField;
    QrLctosProtocolo: TIntegerField;
    QrLctosCtrlQuitPg: TIntegerField;
    QrLctosEndossas: TSmallintField;
    QrLctosEndossan: TFloatField;
    QrLctosEndossad: TFloatField;
    QrLctosCancelado: TSmallintField;
    QrLctosEventosCad: TIntegerField;
    QrLctosEncerrado: TIntegerField;
    QrLctosErrCtrl: TIntegerField;
    QrLctosAlterWeb: TSmallintField;
    QrLctosAtivo: TSmallintField;
    QrLctosIndiPag: TIntegerField;
    QrLctosAgencia: TIntegerField;
    DsLctos: TDataSource;
    QrCartSum: TmySQLQuery;
    QrCartSumSALDO: TFloatField;
    QrCartSumFuturoC: TFloatField;
    QrCartSumFuturoD: TFloatField;
    QrCartSumFuturoS: TFloatField;
    QrCartSumEmCaixa: TFloatField;
    QrCartSumDifere: TFloatField;
    QrCartSumSDO_FUT: TFloatField;
    DsCartSum: TDataSource;
    QrSomaLinhas: TmySQLQuery;
    QrSomaLinhasCREDITO: TFloatField;
    QrSomaLinhasDEBITO: TFloatField;
    QrSomaLinhasSALDO: TFloatField;
    DsSomaLinhas: TDataSource;
    QrSomaM: TmySQLQuery;
    QrSomaMValor: TFloatField;
    QrLctosRecDes: TFloatField;
    QrLctRecDes: TFloatField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctosCredito: TFloatField;
    QrLctosDebito: TFloatField;
    QrLctosBancoCar: TIntegerField;
    QrLctBancoCar: TIntegerField;
    QrLctCentroCusto: TIntegerField;
    QrLctosCentroCusto: TIntegerField;
    QrLctQtd2: TFloatField;
    QrLctosQtd2: TFloatField;
    QrLctVctoOriginal: TDateField;
    QrLctModeloNF: TWideStringField;
    QrLctosVctoOriginal: TDateField;
    QrLctFatParcRef: TIntegerField;
    QrLctGenCtb: TIntegerField;
    QrLctosGenCtb: TIntegerField;
    QrLctDuplicata: TWideStringField;
    QrLctGenCtbD: TIntegerField;
    QrLctGenCtbC: TIntegerField;
    procedure QrLctBeforeClose(DataSet: TDataSet);
    procedure QrLctCalcFields(DataSet: TDataSet);
    procedure QrCrtAfterClose(DataSet: TDataSet);
    procedure QrCrtAfterScroll(DataSet: TDataSet);
    procedure QrCrtBeforeClose(DataSet: TDataSet);
    procedure QrCrtCalcFields(DataSet: TDataSet);
    procedure QrLctAfterScroll(DataSet: TDataSet);
    procedure QrCartsBeforeClose(DataSet: TDataSet);
    procedure QrCartsAfterScroll(DataSet: TDataSet);
    procedure QrCartsAfterClose(DataSet: TDataSet);
    procedure QrCartsCalcFields(DataSet: TDataSet);
    procedure QrLctosBeforeClose(DataSet: TDataSet);
    procedure QrLctosCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    FFmLctGer2: TForm;
    FFmSelfGer2: TForm;
    FInOwner: TWincontrol;
    FPager: TWinControl;
    FPageControl: TdmkPageControl;
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo
    //
  public
    { Public declarations }
    FTabLctA: String;
    FTipoData: Integer;
    FBtConcilia1: TBitBtn;
    FTPDataIni, FTPDataFim: TdmkEditDateTimePicker;
    //
    FCriandoLctGer2,
    F2021: Boolean;
    F2021_Carteira, F2021_Cliente, F2021_Fornecedor: Integer;
    F2021_CliFrnCondition: TCliFrnCondition;
    F2021_PsqCredito, F2021_PsqDebito: Boolean;
    F2021_Genese, F2021_Suplente,
    F2021_AVencer, F2021_Vencido, F2021_Quitado: Boolean;
    //
    //procedure DefineVarsEmpresa(Empresa: Integer);
    //procedure EncerrraMes();
    //procedure FechaQueries();
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
    //
    procedure GerenciaEmpresa(); overload;
    procedure GerenciaEmpresa(InOwner: TWinControl; Pager:
              TWinControl; Collaps: Boolean = True; Unique: Boolean = True); overload;
    procedure GerenciaEmpresa(InOwner: TWinControl; PageControl:
              TdmkPageControl; Collaps: Boolean = True; Unique: Boolean = True); overload;
    procedure GerenciaEmpresa2(InOwner: TWinControl; Pager:
              TWinControl; Cliente, Fornecedor: Integer); overload;
    procedure GerenciaEmpresa2(InOwner: TWinControl; PageControl:
              TdmkPageControl; Cliente, Fornecedor: Integer); overload;
    function  GerenciaTerceiro(InOwner: TWinControl; Pager:
              TWinControl; Cliente, Fornecedor: Integer): TForm;
    procedure MigraLctsParaTabLct();
    procedure MostraFmLctGer2(Collaps: Boolean = True; Unique: Boolean = True); overload;
    function  MostraSelfGer2(Cliente, Fornecedor: Integer): TForm;
    function  SelecionaEmpresa(SelectLetraLct: TSelectLetraLct;
              FinanceiroNovo: Boolean = True): Boolean;
    // Crt e Lct
    //function  AtualizaPagamentosAVista(TabLct: String; QrLct: TmySQLQuery): Boolean;  J� tem no UnFinanceiro!
    function  DefParams(QrCrt, QrCrtSum: TmySQLQuery;
              AvisaErro: Boolean; Aviso: String): Boolean;
    function  LocCod(Atual, Codigo: Integer; QrCrt: TmySQLQuery;
              (*CliInt: Integer; *)Aviso: String): Boolean;
    function  LocalizaLancamento(Carteira, Lancamento: Integer;
              QrCrt, QrLct: TmySQLQuery; FormDefs, FormLocLct: TForm;
              Avisa: Boolean): Boolean;
    procedure LocalizaUltimoLanctoDia(Data: TDateTime; Carteira: Integer);
    procedure ReabreCarteiras(LocCart: Integer;
              QrCrt, QrCrtSum: TmySQLQuery; Aviso: String;
              Filtro: String = '');
    procedure ReabreSoLct(QrLct: TmySQLQuery; ItemCarteira, Controle, Sub:
              Integer; TPDataIni, TPDataFim: TdmkEditDateTimePicker; Apto:
              Variant);
    procedure ReabreSoLct2021(QrLct: TmySQLQuery; Controle, Sub: Integer;
              TPDataIni, TPDataFim: TdmkEditDateTimePicker; Apto: Variant);
    procedure VeSeReabreLct(TPDataIni, TPDataFim: TdmkEditDateTimePicker;
              Apto: Variant; Carteira, Controle, Sub: Integer; QrCrt,
              QrLct: TmySQLQuery; ForcaAbertura: Boolean = False);
    procedure VeSeReabreLct2021(Form: TForm; TPDataIni, TPDataFim:
              TdmkEditDateTimePicker; Apto: Variant; Carteira, Controle, Sub:
              Integer; QrCrt, QrLct: TmySQLQuery; ForcaAbertura: Boolean = False);
    function  DataUltimoLct(TabLctA: String): TDateTime;
    procedure DefineVarsCliInt(CliInt: Integer);
    // Fim CXtr e Lct
    procedure RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte;
              TabLctA: String);
    // Quita��es + Recibo
    procedure GeraReciboImpCabOriDst(TabLctA: String; DBGLct: TDBGrid;
              ModuleLctX: TDmLct2);
    procedure MostraFormReciboImpCab_M(Codigo: Integer; TabLctA: String; DBGLct:
              TDBGrid; ModuleLctX: TDmLct2);
  end;

var
  DmLct2: TDmLct2;

implementation

uses UnInternalConsts, ModuleGeral, MyDBCheck, EmpresaSel, Principal, LctGer2,
  UMySQLModule, Module, UnFinanceiro, ModuleFin, UnMyObjects, SelfGer2,
  MyListas, GetValor, ReciboImpCab;


{$R *.dfm}

{
procedure TDmLct2.DefineVarsEmpresa(Empresa: Integer);
begin
  DmodG.QrCliIntLog.Close;
  DmodG.QrCliIntLog.Params[0].AsInteger := Empresa;
  DmodG.QrCliIntLog . O p e n;
  //
  DModG.FEntidade := DmodG.QrCliIntUniCodigo.Value;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntLogCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  //
end;
}

{ J� tem no UnFinanceiro
function TDmLct2.AtualizaPagamentosAVista(TabLct: String; QrLct: TmySQLQuery): Boolean;
var
  Controle: Integer;
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + TabLct + ' SET Compensado = Data ');
  Dmod.QrUpd.SQL.Add('WHERE Tipo < 2 AND Compensado < 2');
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + TabLct + ' SET Sit = 3 ');
  Dmod.QrUpd.SQL.Add('WHERE Tipo < 2 AND Sit < 2');
  Dmod.QrUpd.ExecSQL;
  Result := True;
  //
  if (QrLct <> nil) and (QrLct.State <> dsInactive) then
  begin
    Controle := QrLct.FieldByName('Controle').AsInteger;
    QrLct.Close;
    QrLct . O p e n ;
    if Controle > 0 then
      QrLct.Locate('Controle', Controle, []);
  end;
  Screen.Cursor := MyCursor;
end;
}

procedure TDmLct2.DataModuleCreate(Sender: TObject);
begin
  FCriandoLctGer2       := True;
  F2021                 := True;
  F2021_Carteira        := 0;
  F2021_Cliente         := 0;
  F2021_Fornecedor      := 0;
  F2021_CliFrnCondition := TCliFrnCondition.cofcNone;
  F2021_PsqCredito      := True;
  F2021_PsqDebito       := True;
  F2021_Genese          := True;
  F2021_Suplente        := True;
  F2021_AVencer         := True;
  F2021_Vencido         := True;
  F2021_Quitado         := True;
end;

procedure TDmLct2.DataModuleDestroy(Sender: TObject);
begin
  VAR_EvitaErroExterno := True;
end;

function TDmLct2.DataUltimoLct(TabLctA: String): TDateTime;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT MAX(Data) Data FROM ' + TabLctA);
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  //
  Result := Dmod.QrAux.FieldByName('Data').AsDateTime;
  if Result < 2 then
    Result := Int(Date) + 90;  // s� por garantia?!
end;

procedure TDmLct2.DefineProperties(Filer: TFiler);
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

procedure TDmLct2.DefineVarsCliInt(CliInt: Integer);
var
  EntInt: Integer;
begin
  if (VAR_KIND_DEPTO = kdUH) or (CO_DMKID_APP = 7) then //LeSew
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrCliIntLog, Dmod.MyDB, [
      'SELECT DISTINCT eci.CodEnti',
      'FROM enticliint eci',
      'LEFT JOIN senhas snh ON snh.Funcionario=eci.AccManager',
      'LEFT JOIN senhasits sei ON eci.CodEnti=sei.Empresa',
      'WHERE eci.CodCliInt=' + FormatFloat('0', CliInt),
      'AND ',
      '(',
      '  eci.AccManager=0',
      '  OR ',
      '  snh.Numero=' + FormatFloat('0', VAR_USUARIO),
      '  OR',
      '  sei.Numero=' + FormatFloat('0', VAR_USUARIO),
      '  OR',
         FormatFloat('0', VAR_USUARIO) + '<0',
      ')',
      '']);
    //
    if DModG.QrCliIntLogCodEnti.Value <> 0 then
      DModG.EmpresaAtual_SetaCodigos(DModG.QrCliIntLogCodEnti.Value, tecEntidade, True);
    //
    EntInt := DmodG.QrCliIntLogCodEnti.Value;
    //
    VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntLogCodEnti.Value);
    //
    if VAR_LIB_EMPRESAS = '' then
      VAR_LIB_EMPRESAS := '-100000000';
    //
    DModG.DefineDataMinima(EntInt);
    //
    DmLct2.QrCrt.Close;
    DmLct2.QrLct.Close;
  end;
end;

function TDmLct2.DefParams(QrCrt, QrCrtSum: TmySQLQuery; AvisaErro: Boolean;
  Aviso: String): Boolean;
var
  Carteira: Integer;
begin
  if QrCrt <> nil then
  begin
    if QrCrt.State <> dsBrowse then
      Carteira := 0
    else begin
      if QrCrt.RecordCount = 0 then Carteira := 0
      else
        Carteira := QrCrt.FieldByName('Codigo').AsInteger;
    end;
    ReabreCarteiras(Carteira, QrCrt, QrCrtSum, Aviso +
      ' > TDmLct2.DefParams()');
    Result := True;
  end else begin
    Result := False;
    if AvisaErro then
      Geral.MB_Erro(
      '"QrCrt" n�o definido na procedure "DefParams"');
  end;
end;

{
procedure TDmLct2.FechaQueries();
var
  I: Integer;
  Compo: TComponent;
  Query: TmySQLQuery;
begin
  FTabLctA := '';
  for I := 0 to ComponentCount - 1 do
  begin
    Compo := Components[I];
    if Compo is TmySQLQuery then
    begin
      Query := TmySQLQuery(Compo);
      Query.Close;
      Query.SortFieldNames := '';
    end;
  end;
end;
}
procedure TDmLct2.GerenciaEmpresa();
const
  LocCart = 0;
var
  Entidade, CliInt: Integer;
begin
  if SelecionaEmpresa(sllLivre) then
  begin
    Entidade := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
    CliInt   := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
    //
    DModG.DefineDataMinima(Entidade);
    //
    DefineVarsCliInt(CliInt);
    //
    if DBCheck.CriaFm(TFmLctGer2, FmLctGer2, afmoNegarComAviso) then
    begin
      FCriandoLctGer2 := True;
      FFmLctGer2 := TFmLctGer2(FmLctGer2);
      //
      if FFmLctGer2 <> nil then
      begin
        TFmLctGer2(FFmLctGer2).DefineDataModule(TDmLct2(Self));
        //
        DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, TFmLctGer2(FFmLctGer2).FDtEncer,
          TFmLctGer2(FFmLctGer2).FDtMorto, TFmLctGer2(FFmLctGer2).FTabLctA,
          TFmLctGer2(FFmLctGer2).FTabLctB, TFmLctGer2(FFmLctGer2).FTabLctD);
        TFmLctGer2(FFmLctGer2).TPDataFim.Date :=
          DataUltimoLct(TFmLctGer2(FFmLctGer2).FTabLctA);
        FTPDataIni := TFmLctGer2(FFmLctGer2).TPDataIni;
        FTPDataFim := TFmLctGer2(FFmLctGer2).TPDataFim;
        ReabreCarteiras(LocCart, QrCrt, QrCrtSum, 'TDmLct2.MostraFmLctGer2()');
        //
        TFmLctGer2(FFmLctGer2).MyFormCreate(TFmLctGer2(FFmLctGer2));
        TDmLct2(Self).FTabLctA := TFmLctGer2(FFmLctGer2).FTabLctA;
        //
        //FCriandoLctGer2 := False;
        TFmLctGer2(FFmLctGer2).ShowModal;
        TFmLctGer2(FFmLctGer2).Destroy;
      end;
    end;
  end;
end;

procedure TDmLct2.GerenciaEmpresa(InOwner: TWincontrol; Pager:
  TWinControl; Collaps: Boolean = True; Unique: Boolean = True);
begin
  FInOwner := InOwner;
  FPager := Pager;
  //FechaQueries();
  if SelecionaEmpresa(sllLivre) then
    MostraFmLctGer2(Collaps, Unique);
end;

procedure TDmLct2.GeraReciboImpCabOriDst(TabLctA: String; DBGLct: TDBGrid;
  ModuleLctX: TDmLct2);
  //
  function ObtemValorPagto(var Valor: Double): Boolean;
  var
    ResVar: Variant;
    CasasDecimais: Integer;
  begin
    Result        := False;
    CasasDecimais := 4;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    Valor, CasasDecimais, 0, '', '', True, 'Valor Pago', 'Informe o valor: ',
    0, ResVar) then
    begin
      Valor := Geral.DMV(ResVar);
      Result := True;
    end;
  end;
  //
  procedure IncluiReciboImpCab(const Nome: String; const Valor: Double; var NewCod: Integer);
  var
    emit_CNPJ, emit_CPF, emit_IE,
    DtRecibo, DtPrnted, CNPJCPF, DtConfrmad: String;
    Codigo, Emitente, Beneficiario: Integer;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    Codigo         := 0;
    NewCod         := 0;
    DtRecibo       := Geral.FDT(DModG.ObtemAgora(), 109);
    DtPrnted       := '1899-12-31 12:00:00';
    if QrLctCredito.Value > 0 then
    begin
      Emitente     := QrLctCliente.Value;
      Beneficiario := QrLctCliInt.Value;
    end else
    begin
      Emitente     := QrLctCliInt.Value;
      Beneficiario := QrLctCliente.Value;
    end;
    DModG.ObtemCNPJ_CPF_IE_DeEntidade(QrLctCliente.Value, emit_CNPJ, emit_CPF, emit_IE);
    if emit_CNPJ <> EmptyStr then
      CNPJCPF      := emit_CNPJ
    else
      CNPJCPF      := emit_CPF;
    DtConfrmad     := '1899-12-31 12:00:00';
    //
    Codigo := UMyMod.BPGS1I32('reciboimpcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'reciboimpcab', False, [
    'DtRecibo', 'DtPrnted', 'Emitente',
    'Beneficiario', 'CNPJCPF', 'Valor',
    'DtConfrmad', 'Nome'], [
    'Codigo'], [
    DtRecibo, DtPrnted, Emitente,
    Beneficiario, CNPJCPF, Valor,
    DtConfrmad, Nome], [
    Codigo], True) then
    begin
      NewCod := Codigo;
    end;
  end;
  //
  procedure IncluiReciboImpOriAtual(const Codigo: Integer);
  var
    Controle, LctCtrl, LctSub: Integer;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    //Codigo         := ;
    Controle       := 0;
    LctCtrl        := QrLctControle.Value;
    LctSub         := QrLctSub.Value;
    //
    Controle := UMyMod.BPGS1I32('reciboimpori', 'Controle', '', '', tsPos, SQLType, Controle);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'reciboimpori', False, [
    'Codigo', 'LctCtrl', 'LctSub'], [
    'Controle'], [
    Codigo, LctCtrl, LctSub], [
    Controle], True);
  end;
var
  I, Codigo: Integer;
  Continua: Boolean;
  Corda, Nome: String;
  ValorPndt, ValorAPag, ValorPago, TmpVal: Double;
  ItsDesneces: Integer;
begin
  //if DBGLct.SelectedRows.Count < 2 then
  //  Geral.MB_Aviso('Selecione ao menos dois lan�amentos!')
  if DBGLct.SelectedRows.Count < 1 then
    Geral.MB_Aviso('Selecione ao menos um lan�amento!')
  else
  begin
    ValorAPag := 0;
    Nome := EmptyStr;
    with DBGLct.DataSource.DataSet do
    for I := 0 to DBGLct.SelectedRows.Count-1 do
    begin
      GotoBookmark(DBGLct.SelectedRows.Items[i]);
      //
      ValorAPag := ValorAPag + QrLctCredito.Value - QrLctPago.Value;
      if I > 0 then
        Nome := Nome + ' - ';
      Nome := Nome + QrLctDescricao.Value;
    end;
    Nome := Copy(Nome, 1, 510); // Evitar erro
    //
    ValorPago := ValorAPag;
    if not ObtemValorPagto(ValorPago) then Exit;
    if ValorPago < 0.01 then Exit;
    if MyObjects.FIC(ValorPago > ValorAPag, nil,
    'Valor de quita��o n�o pode ser superior ao valor a pagar! ' + sLineBreak +
    'No caso de multa e/ou juros crie lan�amento(s) espec�fico(s) e selecione junto aos lan�amentos a serem quitados!') then
      Exit;
    //
    IncluiReciboImpCab(Nome, ValorPago, Codigo);
    if Codigo <> 0 then
    begin
      with DBGLct.DataSource.DataSet do
      for I := 0 to DBGLct.SelectedRows.Count-1 do
      begin
        GotoBookmark(DBGLct.SelectedRows.Items[i]);
        //
        IncluiReciboImpOriAtual(Codigo);
      end;
      //
      if DBCheck.CriaFm(TFmReciboImpCab, FmReciboImpCab, afmoNegarComAviso) then
      begin
        FmReciboImpCab.FTabLctA := TabLctA;
        FmReciboImpCab.FNewCod := Codigo;
        FmReciboImpCab.FValPag := ValorPago;
        FmReciboImpCab.FModuleLctX := ModuleLctX;

        FmReciboImpCab.LocCod(Codigo, Codigo);
        if FmReciboImpCab.QrReciboImpCabCodigo.Value = Codigo then
          FmReciboImpCab.InserePagamento(ValorPago);
        FmReciboImpCab.ShowModal;
        FmReciboImpCab.Destroy;
      end;
      //
    end;
  end;
end;

procedure TDmLct2.GerenciaEmpresa(InOwner: TWinControl;
  PageControl: TdmkPageControl; Collaps, Unique: Boolean);
begin
  FInOwner := InOwner;
  FPageControl := PageControl;
  //FechaQueries();
  if SelecionaEmpresa(sllLivre) then
    MostraFmLctGer2(Collaps, Unique);
end;

procedure TDmLct2.GerenciaEmpresa2(InOwner: TWinControl;
  PageControl: TdmkPageControl; Cliente, Fornecedor: Integer);
begin
  FInOwner := InOwner;
  FPageControl := PageControl;
  //FechaQueries();
  if SelecionaEmpresa(sllLivre) then
    MostraSelfGer2(Cliente, Fornecedor);
end;

procedure TDmLct2.GerenciaEmpresa2(InOwner: TWincontrol; Pager:
  TWinControl; Cliente, Fornecedor: Integer);
begin
  FInOwner := InOwner;
  FPager := Pager;
  //FechaQueries();
  if SelecionaEmpresa(sllLivre) then
    MostraSelfGer2(Cliente, Fornecedor);
end;

function TDmLct2.GerenciaTerceiro(InOwner: TWinControl;
  Pager: TWinControl; Cliente, Fornecedor: Integer): TForm;
begin
  Result := nil;
  FInOwner := InOwner;
  FPager := Pager;
  //
  if SelecionaEmpresa(sllLivre) then
    Result := MostraSelfGer2(Cliente, Fornecedor);
end;

function TDmLct2.LocalizaLancamento(Carteira, Lancamento: Integer;
  QrCrt, QrLct: TmySQLQuery; FormDefs, FormLocLct: TForm; Avisa: Boolean): Boolean;

  function Localiza(Lancamento: Double; QrLct: TmySQLQuery;
    FormDefs, FormLocLct: TForm): Boolean;
  begin
    if QrLct.State = dsInactive then
      UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
    if QrLct.Locate('Controle', Lancamento, []) then
    begin
      if FormDefs <> nil then
        FormDefs.Close;
      Screen.Cursor := crDefault;
      if FormLocLct <> nil then
        FormLocLct.Close;
      //
      Result := True
    end else Result := False;
  end;
begin
  Result := False;
  if DefParams(QrCrt, nil, True, 'FmLocLancto.BtLocaliza1Click()') then
  begin
    if not LocCod(Carteira, Carteira, QrCrt, 'DmLct2.LocalizaLancamento()') then
      Geral.MB_Aviso('N�o foi poss�vel localizar a carteira ' +
      IntToStr(Carteira) + '!' + sLineBreak +
      'Verifique se esta carteira pertence realmente a empresa ' +
      VAR_LIB_EMPRESAS + '!')
    else begin
      if not Localiza(Lancamento, QrLct, FormDefs, FormLocLct) then
      begin
        if not Localiza(Lancamento, QrLct, FormDefs, FormLocLct) then
        begin
          if Avisa then
          Geral.MB_Aviso('N�o foi poss�vel localizar o ' +
          'lan�amento ' + FormatFloat('0', Lancamento) + '!');
        end;
      end else Result := True;
    end;
  end;
end;

procedure TDmLct2.LocalizaUltimoLanctoDia(Data: TDateTime; Carteira: Integer);
var
  Controle: Integer;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT Max(Controle) Controle');
  Dmod.QrAux.SQL.Add('FROM ' + FTabLctA + '');
  Dmod.QrAux.SQL.Add('WHERE Carteira=:P0');
  Dmod.QrAux.SQL.Add('AND Data=:P1');
  Dmod.QrAux.Params[00].AsInteger := Carteira;
  Dmod.QrAux.Params[01].AsString := Geral.FDT(Data, 1);
  UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Controle := Dmod.QrAux.FieldByName('Controle').AsInteger;
    if not QrLct.Locate('Controle', Controle, []) then
      Geral.MB_Aviso('O lan�amento n�mero ' + IntToStr(Controle) +
      ' n�o pode ser localizado sem pesquisa!');
  end else begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Max(Controle) Controle');
    Dmod.QrAux.SQL.Add('FROM ' + FTabLcta + '');
    Dmod.QrAux.SQL.Add('WHERE Carteira=:P0');
    Dmod.QrAux.SQL.Add('AND Data<:P1');
    Dmod.QrAux.Params[00].AsInteger := Carteira;
    Dmod.QrAux.Params[01].AsString := Geral.FDT(Data, 1);
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Controle := Dmod.QrAux.FieldByName('Controle').AsInteger;
      if not QrLct.Locate('Controle', Controle, []) then
        Geral.MB_Aviso('O lan�amento n�mero ' + IntToStr(Controle) +
        ' n�o pode ser localizado sem pesquisa!');
    end else
      Geral.MB_Aviso('A pesquisa n�o localizou nenhum lan�amento!');
  end;
end;

function TDmLct2.LocCod(Atual, Codigo: Integer; QrCrt: TmySQLQuery;
  Aviso: String): Boolean;
begin
  Result := False;
  DefParams(QrCrt, nil, True, Aviso + ' > TDmodFin.LocCod(');
  if not QrCrt.Locate('Codigo', Codigo, []) then
    QrCrt.Locate('Codigo', Atual, [])
  else Result := True;
end;

procedure TDmLct2.MigraLctsParaTabLct();
const
  Antiga = LAN_CTOS;
var
  A1, A2, B1, B2, A3, B3, Entidade, CliInt, QtdReg1: Integer;
  Campos, TabLctA: String;
  Continua: Boolean;
begin
  if SelecionaEmpresa(sllForcaA, False) then
  begin
    Entidade := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
    CliInt   := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
    TabLctA  := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
    //
    ShowMessage('Entidade: ' + IntToStr(Entidade));
    ShowMessage('CliInt: ' + IntToStr(CliInt));
    //verificar saldos de contas e carteiras!
    if DmodFin.EntidadeHabilitadadaParaFinanceiroNovo(Entidade, True, True) <> sfnFaltaMigrar then Exit;
    //
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT COUNT(Controle) Itens FROM ' + Antiga);
    Dmod.QrAux.SQL.Add('WHERE Controle <> 0 AND CliInt=:P0 ');
    Dmod.QrAux.Params[0].AsInteger := Entidade;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    A1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
    //
    DModG.ReopenEndereco(Entidade);
    if A1 = 0 then
    begin
      Geral.MB_Aviso('N�o h� lan�amentos a serem migrados para o cliente "' +
      DModG.QrEnderecoNOME_ENT.Value + '"!');
      Exit;
    end;
    if Geral.MB_Pergunta('Confirma a migra��o dos ' + IntToStr(A1) +
    ' lan�amentos da tabela "' + Antiga + '" para a tabela "' +
    TabLctA + '"?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      //
      try
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('LOCK TABLES ' + TabLctA + ' WRITE, ' + Antiga + ' WRITE, master WRITE');
        Dmod.QrUpd.ExecSQL;
        //
        try
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Controle) Itens FROM ' + TabLctA);
          Dmod.QrAux.SQL.Add('WHERE Controle <> 0 AND CliInt=:P0 ');
          Dmod.QrAux.Params[0].AsInteger := Entidade;
          UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          B1 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabLctA, '', QtdReg1);
          //
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO ' + TabLctA);
          Dmod.QrUpd.SQL.Add('SELECT ' + Campos);
          Dmod.QrUpd.SQL.Add('FROM ' + Antiga);
          Dmod.QrUpd.SQL.Add('WHERE Controle <> 0 AND CliInt = ' + FormatFloat('0', Entidade));
          Dmod.QrUpd.ExecSQL;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Controle) Itens FROM ' + TabLctA);
          Dmod.QrAux.SQL.Add('WHERE Controle <> 0 AND CliInt=:P0 ');
          Dmod.QrAux.Params[0].AsInteger := Entidade;
          UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          B2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
          B3 := B2 - B1;
          if B3 > 0 then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add(DELETE_FROM + Antiga);
            Dmod.QrUpd.SQL.Add('WHERE Controle <> 0 AND CliInt = ' + FormatFloat('0', Entidade));
            Dmod.QrUpd.ExecSQL;
            //
            // Parei aqui!
            //fazer update na tabela carteira (entidade?) que esta foi migrada
          end;
          //
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SELECT COUNT(Controle) Itens FROM ' + Antiga);
          Dmod.QrAux.SQL.Add('WHERE Controle <> 0 AND CliInt=:P0 ');
          Dmod.QrAux.Params[0].AsInteger := Entidade;
          UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          A2 := Dmod.QrAux.FieldByName('Itens').AsInteger;
          //
        finally
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UNLOCK TABLES');
          Dmod.QrUpd.ExecSQL;
        end;
        A3 := A1 - A2;
        if A3 <> B3 then
        begin
          Geral.MB_Erro('Ocorreu um erro na migra��o:' + sLineBreak +
          'Lan�amentos na tabela "' + Antiga + '" antes da migra��o = ' + IntToStr(A1) + sLineBreak +
          'Lan�amentos na tabela "' + TabLctA + '" antes da migra��o = ' + IntToStr(B1) + sLineBreak +
          'Lan�amentos na tabela "' + Antiga + '" depois da migra��o = ' + IntToStr(A2) + sLineBreak +
          'Lan�amentos na tabela "' + TabLctA + '" Depois da migra��o = ' + IntToStr(B2) + sLineBreak +
          'Diferen�a na migra��o: ' + IntToStr(B3 - A3) + ' lan�amentos!');
        end else
        begin
          {$IFDEF MIGRAOUTROS}
          Continua := FmPrincipal.MigraEspecificos(CliInt);
          {$ELSE}
          Continua := True;
          {$ENDIF}
          if Continua then
          begin
            // Cadastra que a empresa usar� o novo financeiro e n�o mais o antigo
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'enticliint', False, [
            'TipoTabLct'], ['CodEnti'], [1], [Entidade], True);
            //
            Geral.MB_Info('Migra��o realizada com sucesso.' + sLineBreak +
            'Foram migrados ' + IntToStr(B2) + ' lan�amentos!');
          end else
            Geral.MB_Erro('ERRO durante a migra��o!' + sLineBreak +
            'Foram migrados ' + IntToStr(B2) + ' lan�amentos!');
        end;
        Screen.Cursor := crDefault;
     finally
        Screen.Cursor := crDefault;
        //Geral.MB_Erro('ERRO ao migrar lan�amentos!');
      end;
    end;
  end;
end;

procedure TDmLct2.MostraFmLctGer2(Collaps: Boolean = True; Unique: Boolean = True);
const
  LocCart = 0;
var
  Entidade, CliInt: Integer;
begin
  Entidade := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
  CliInt   := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
  //
  DModG.DefineDataMinima(Entidade);
  //
  DefineVarsCliInt(CliInt);
  //
  FCriandoLctGer2 := True;
  FFmLctGer2 := MyObjects.FormTDICria(TFmLctGer2, FInOwner, FPager, Collaps, Unique);
  //
  if FFmLctGer2 <> nil then
  begin
    TFmLctGer2(FFmLctGer2).DefineDataModule(TDmLct2(Self));
    //
    DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, TFmLctGer2(FFmLctGer2).FDtEncer,
      TFmLctGer2(FFmLctGer2).FDtMorto, TFmLctGer2(FFmLctGer2).FTabLctA,
      TFmLctGer2(FFmLctGer2).FTabLctB, TFmLctGer2(FFmLctGer2).FTabLctD);
    TFmLctGer2(FFmLctGer2).TPDataFim.Date :=
      DataUltimoLct(TFmLctGer2(FFmLctGer2).FTabLctA);
    FTPDataIni := TFmLctGer2(FFmLctGer2).TPDataIni;
    FTPDataFim := TFmLctGer2(FFmLctGer2).TPDataFim;
    ReabreCarteiras(LocCart, QrCrt, QrCrtSum, 'TDmLct2.MostraFmLctGer2()');
    //
    TFmLctGer2(FFmLctGer2).MyFormCreate(TFmLctGer2(FFmLctGer2));
    TDmLct2(Self).FTabLctA := TFmLctGer2(FFmLctGer2).FTabLctA;
  end;
  //FCriandoLctGer2 := False;
end;

procedure TDmLct2.MostraFormReciboImpCab_M(Codigo: Integer; TabLctA: String; DBGLct: TDBGrid; ModuleLctX: TDmLct2);
begin
  if DBCheck.CriaFm(TFmReciboImpCab, FmReciboImpCab, afmoNegarComAviso) then
  begin
    FmReciboImpCab.FNewCod := 0; //Codigo;
    FmReciboImpCab.FValPag := 0.00; //ValorPago;
    FmReciboImpCab.FModuleLctX := ModuleLctX;
    //
    if Codigo <> 0 then
      FmReciboImpCab.LocCod(Codigo, Codigo);
    //
(*
    if FmReciboImpCab.QrReciboImpCabCodigo.Value = Codigo then
      FmReciboImpCab.InserePagamento(ValorPago);
*)
    FmReciboImpCab.ShowModal;
    FmReciboImpCab.Destroy;
  end;

end;

function TDmLct2.MostraSelfGer2(Cliente, Fornecedor: Integer): TForm;
const
  LocCart = 0;
var
  Entidade, CliInt: Integer;
begin
  Result := nil;
  Entidade := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
  CliInt   := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
  DModG.DefineDataMinima(Entidade);
  //
  FFmSelfGer2 := MyObjects.FormTDICria(TFmSelfGer2, FInOwner, FPager);
  TFmSelfGer2(FFmSelfGer2).DefineDataModule(TDmLct2(Self));
  //
  DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, TFmSelfGer2(FFmSelfGer2).FDtEncer,
    TFmSelfGer2(FFmSelfGer2).FDtMorto, TFmSelfGer2(FFmSelfGer2).FTabLctA,
    TFmSelfGer2(FFmSelfGer2).FTabLctB, TFmSelfGer2(FFmSelfGer2).FTabLctD);
  TFmSelfGer2(FFmSelfGer2).TPDataF.Date :=
    DataUltimoLct(TFmSelfGer2(FFmSelfGer2).FTabLctA);
  FTPDataIni := TFmSelfGer2(FFmSelfGer2).TPDataI;
  FTPDataFim := TFmSelfGer2(FFmSelfGer2).TPDataF;
  ReabreCarteiras(LocCart, QrCrt, QrCrtSum, 'TDmLct2.MostraFmSelfGer2()');
  //
  TFmSelfGer2(FFmSelfGer2).MyFormCreate(TFmSelfGer2(FFmSelfGer2));
  //
  TFmSelfGer2(FFmSelfGer2).FGeradorTxt := 'Fatura';
  TFmSelfGer2(FFmSelfGer2).FFinalidade := idflProprios;

  TFmSelfGer2(FFmSelfGer2).EdCliente.ValueVariant := Cliente;
  TFmSelfGer2(FFmSelfGer2).EdCliente.ValueVariant := Cliente;

  TFmSelfGer2(FFmSelfGer2).EdFornecedor.ValueVariant := Fornecedor;
  TFmSelfGer2(FFmSelfGer2).EdFornecedor.ValueVariant := Fornecedor;

  //TFmSelfGer2(FFmSelfGer2).ShowModal;
  //TFmSelfGer2(FFmSelfGer2).Destroy;
  //
  //Application.OnHint := FmPrincipal.ShowHint;
  Application.OnHint := FmPrincipal.MyOnHint;
  //
  Result := FFmSelfGer2;
end;

procedure TDmLct2.QrCartsAfterClose(DataSet: TDataSet);
begin
  QrLctos.Close;
end;

procedure TDmLct2.QrCartsAfterScroll(DataSet: TDataSet);
var
  Controle, Sub: Integer;
begin
  Controle := 0;
  Sub := 1;
  if QrLctos.State = dsBrowse then
  begin
    Controle := QrLctosControle.Value;
    Sub := QrLctosSub.Value;
  end;
  ReabreSoLct(QrLctos, (*QrCartsTipo.Value*,*) QrCartsCodigo.Value,
    Controle, Sub, FTPDataIni, FTPDataFim, Null);
end;

procedure TDmLct2.QrCartsBeforeClose(DataSet: TDataSet);
begin
  QrLctos.Close;
end;

procedure TDmLct2.QrCartsCalcFields(DataSet: TDataSet);
begin
  QrCartsDIFERENCA.Value :=
    QrCartsEmCaixa.Value - QrCartsSaldo.Value;
  case QrCartsPrazo.Value of
    0: QrCartsTIPOPRAZO.Value := 'F';
    1: QrCartsTIPOPRAZO.Value := 'V';
  end;
  case QrCartsPagRec.Value of
   -1: QrCartsNOMEPAGREC.Value := 'CONTAS A PAGAR';
    0: QrCartsNOMEPAGREC.Value := 'CONTAS A PAGAR E RECEBER';
    1: QrCartsNOMEPAGREC.Value := 'CONTAS A RECEBER';
  end;
end;

procedure TDmLct2.QrCrtAfterClose(DataSet: TDataSet);
begin
  QrLct.Close;
end;

procedure TDmLct2.QrCrtAfterScroll(DataSet: TDataSet);
var
  Controle, Sub: Integer;
begin
  if F2021 = false then
  begin
    Controle := 0;
    Sub := 1;
    if QrLct.State = dsBrowse then
    begin
      Controle := QrLctControle.Value;
      Sub := QrLctSub.Value;
    end;
    ReabreSoLct(QrLct, QrCrtCodigo.Value, Controle, Sub, FTPDataIni, FTPDataFim, Null);
  end;
  if FindWindow('TFmLctGer2', nil) > 0 then
    TFmLctGer2(FFmLctGer2).DmLct2_QrCrtAfterScroll();
{###
var
  Mostra: Boolean;
begin
  ReabreSoLct(QrCarteirasTipo.Value, QrCarteirasCodigo.Value, -1, 0);

  Mostra := QrCarteirasTipo.Value in ([0,1]);
  if Mostra then EdNome.Width := 181 else EdNome.Width := 593;
  EdSaldo.Visible := Mostra;
  LaSaldo.Visible := Mostra;
  EdDiferenca.Visible := Mostra;
  LaDiferenca.Visible := Mostra;
  EdCaixa.Visible := Mostra;
  LaCaixa.Visible := Mostra;
  BtPagtoDuvida.Visible := Mostra;
  BtContarDinheiro.Visible := Mostra;
  EdSdoAqui.Visible := Mostra;
  //
  BtConcilia.Enabled := QrCarteirasTipo.Value = 1;
}
end;

procedure TDmLct2.QrCrtBeforeClose(DataSet: TDataSet);
begin
  if FBtConcilia1 <> nil then
    FBtConcilia1.Enabled := False;
end;

procedure TDmLct2.QrCrtCalcFields(DataSet: TDataSet);
begin
  if (QrCrtTipo.Value = 0) and (QrCrtEmCaixa.Value <> 0) then
  begin
    if QrCrtSaldo.Value > 0 then
      QrCrtDIFERENCA.Value := QrCrtSaldo.Value - QrCrtEmCaixa.Value
    else
      QrCrtDIFERENCA.Value := QrCrtSaldo.Value + QrCrtEmCaixa.Value;
  end else
    QrCrtDIFERENCA.Value := 0;
  //
  case QrCrtPrazo.Value of
    0: QrCrtTIPOPRAZO.Value := 'V';
    1: QrCrtTIPOPRAZO.Value := 'F';
  end;
  case QrCrtPagRec.Value of
   -1: QrCrtNOMEPAGREC.Value := 'CONTAS A PAGAR';
    0: QrCrtNOMEPAGREC.Value := 'CONTAS A PAGAR E RECEBER';
    1: QrCrtNOMEPAGREC.Value := 'CONTAS A RECEBER';
  end;
end;

procedure TDmLct2.QrLctAfterScroll(DataSet: TDataSet);
begin
  if FindWindow('TFmLctGer2', nil) > 0 then
    TFmLctGer2(FFmLctGer2).DmLct2_QrLctAfterScroll();
end;

procedure TDmLct2.QrLctBeforeClose(DataSet: TDataSet);
begin
  VAR_LANCTO := QrLctControle.Value;
end;

procedure TDmLct2.QrLctCalcFields(DataSet: TDataSet);
begin
  if QrLctMes2.Value > 0 then
    QrLctMENSAL.Value := FormatFloat('00', QrLctMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrLctAno.Value), 3, 2)
   else QrLctMENSAL.Value := CO_VAZIO;

  //

  QrLctMENSAL2.Value := UFinanceiro.Mensal2(QrLctMes2.Value, QrLctAno.Value);

  //

  QrLctNOMESIT.Value := UFinanceiro.NomeSitLancto(QrLctSit.Value,
    QrLctTipo.Value, QrLctPrazo.Value, QrLctVencimento.Value,
    QrLctReparcel.Value);

  //

  case QrLctSit.Value of
    0: QrLctSALDO.Value   := QrLctCredito.Value - QrLctDebito.Value;
    1: QrLctSALDO.Value   := (QrLctCredito.Value - QrLctDebito.Value) -
                               (QrLctPago.Value + QrLctPagMul.Value +
                               QrLctPagJur.Value - QrLctRecDes.Value);
    else QrLctSALDO.Value := 0;
  end;

  QrLctNOMERELACIONADO.Value := UFinanceiro.NomeRelacionado(QrLctcliente.Value,
    QrLctFornecedor.Value, QrLctNOMECLIENTE.Value, QrLctNOMEFORNECEDOR.Value);

  //
  if QrLctVencimento.Value > Date then QrLctATRASO.Value := 0
    else QrLctATRASO.Value := Date - QrLctVencimento.Value;
  //
  QrLctJUROS.Value :=
    Trunc(QrLctATRASO.Value * QrLctMoraDia.Value * 100)/100;
  //
  if QrLctCompensado.Value = 0 then
     QrLctCOMPENSADO_TXT.Value := '' else
     QrLctCOMPENSADO_TXT.Value :=
     FormatDateTime(VAR_FORMATDATE3, QrLctCompensado.Value);
  //
  QrLctSERIE_CHEQUE.Value := QrLctSerieCH.Value +
    FormatFloat('000000;-0; ', QrLctDocumento.Value);
end;

procedure TDmLct2.QrLctosBeforeClose(DataSet: TDataSet);
begin
  VAR_LANCTO := QrLctosControle.Value;
end;

procedure TDmLct2.QrLctosCalcFields(DataSet: TDataSet);
begin
  if QrLctosMes2.Value > 0 then
    QrLctosMENSAL.Value := FormatFloat('00', QrLctosMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrLctosAno.Value), 3, 2)
   else QrLctosMENSAL.Value := CO_VAZIO;
  if QrLctosMes2.Value > 0 then
    QrLctosMENSAL2.Value := FormatFloat('0000', QrLctosAno.Value)+'/'+
    FormatFloat('00', QrLctosMes2.Value)+'/01'
   else QrLctosMENSAL2.Value := CO_VAZIO;
  //
  QrLctosNOMESIT.Value := UFinanceiro.NomeSitLancto(QrLctosSit.Value,
    QrLctosTipo.Value, QrLctosPrazo.Value, QrLctosVencimento.Value,
    QrLctosReparcel.Value);
  //
  case QrLctosSit.Value of
    0: QrLctosSALDO.Value   := QrLctosCredito.Value - QrLctosDebito.Value;
    1: QrLctosSALDO.Value   := (QrLctosCredito.Value - QrLctosDebito.Value) -
                                 (QrLctosPago.Value + QrLctosPagMul.Value +
                                 QrLctosPagJur.Value - QrLctosRecDes.Value);
    else QrLctosSALDO.Value := 0;
  end;

  QrLctosNOMERELACIONADO.Value := UFinanceiro.NomeRelacionado(QrLctosCliente.Value,
    QrLctosFornecedor.Value, QrLctosNOMECLIENTE.Value, QrLctosNOMEFORNECEDOR.Value);

  //
  if QrLctosVencimento.Value > Date then QrLctosATRASO.Value := 0
    else QrLctosATRASO.Value := Date - QrLctosVencimento.Value;
  //
  QrLctosJUROS.Value :=
    Trunc(QrLctosATRASO.Value * QrLctosMoraDia.Value * 100)/100;
  //
  if QrLctosCompensado.Value = 0 then
     QrLctosCOMPENSADO_TXT.Value := '' else
     QrLctosCOMPENSADO_TXT.Value :=
     FormatDateTime(VAR_FORMATDATE3, QrLctosCompensado.Value);
  QrLctosSERIE_CHEQUE.Value := QrLctosSerieCH.Value +
    FormatFloat('000000;-0; ', QrLctosDocumento.Value);
  //
  //QrLctosFATID_TXT.Value := QrLctosFatID.Value
end;

procedure TDmLct2.ReabreCarteiras(LocCart: Integer;
QrCrt, QrCrtSum: TmySQLQuery; Aviso: String; Filtro: String);
var
  Empresa: Integer;
  //Empresas: String;
  SQL_Nome: String;
begin
  Empresa := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
  if Empresa = 0 then
    Empresa := -100000000;
  //
  {
  if VAR_LIB_EMPRESAS = '' then
    Empresas := '-100000000'
  else
    Empresas := VAR_LIB_EMPRESAS;
  }
  if Filtro <> EmptyStr then
    SQL_Nome := 'AND ca.Nome LIKE "%' + Filtro + '%"'
  else
    SQL_Nome := '';
  //
  QrCrt.Close;
  QrCrt.SQL.Clear;
  QrCrt.SQL.Text := Geral.ATS([
    'SELECT DISTINCT ca.*, ba.Nome NOMEDOBANCO, '      ,
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial '         ,
    'ELSE en.Nome END NOMEFORNECEI, en.CliInt '        ,
    'FROM carteiras ca '                               ,
    'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco '    ,
    'LEFT JOIN entidades en ON en.Codigo=ca.ForneceI ' ,
    //'WHERE ca.ForneceI in (' + Empresas + ')'         ,
    'WHERE ca.ForneceI in (' + Geral.FF0(Empresa) + ')',
    //'OR ca.Codigo=0'                                   ,
    // Ini 2020-01-09
    //'AND ca.Codigo > 0'                                ,
    'AND ca.Codigo >= -1000'                                ,  // 2021-01-18
    // Fim 2020-01-09
    'AND ca.Ativo = 1'                                 ,
    SQL_Nome,                                                  // 2021-04-15
    'ORDER BY ca.Nome',
    '']);
  UMyMod.AbreQuery(QrCrt, Dmod.MyDB, Aviso + ' > TDmLct2.ReabreCarteiras() > QrCarteira');
  //Geral.MB_Teste(QrCrt.SQL.Text);
  //
  QrCrt.Locate('Codigo', LocCart, []);
  //
  if QrCrtSum <> nil  then
  begin
    QrCrtSum.Close;
    QrCrtSum.SQL.Text :=
      'SELECT SUM(Saldo) SALDO, SUM(FuturoC) FuturoC,'    + sLineBreak +
      'SUM(FuturoD) FuturoD, SUM(FuturoS) FuturoS,'       + sLineBreak +
      'SUM(EmCaixa) EmCaixa, SUM(EmCaixa-Saldo) Difere,'  + sLineBreak +
      'SUM(Saldo+FuturoS) SDO_FUT'                        + sLineBreak +
      'FROM carteiras ca'                                 + sLineBreak +
      //'WHERE ca.ForneceI in (' + Empresas + ')';
      'WHERE ca.ForneceI in (' + Geral.FF0(Empresa) + ')' + sLineBreak +
      'AND ca.Codigo > 0'                                 + sLineBreak +
      'AND ca.Ativo = 1';
    UMyMod.AbreQuery(QrCrtSum, Dmod.MyDB, Aviso + 'TDmLct2.ReabreCarteiras() > QrCartSum');
  end;
  //
end;

procedure TDmLct2.ReabreSoLct(QrLct: TmySQLQuery; ItemCarteira, Controle,
  Sub: Integer; TPDataIni, TPDataFim: TdmkEditDateTimePicker; Apto: Variant);
var
  Ini, Fim, Campo, Tab_Lct: String;
  MeuC, MeuS: Integer;
begin
  Tab_Lct := DModG.NomeTab(TMeuDB, ntLct, True);
  QrLct.Close;

  // Fazer mais r�pido inclus�es multiplas!
  if VAR_NaoReabrirLct then
    Exit;

  if TPDataIni <> nil then
    VAR_FL_DataIni := TPDataIni.Date;
  if TPDataFim <> nil then
    VAR_FL_DataFim := TPDataFim.Date;
  //
  Ini := FormatDateTime(VAR_FORMATDATE, VAR_FL_DataIni);
  Fim := FormatDateTime(VAR_FORMATDATE, VAR_FL_DataFim);
  //
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
  QrLct.SQL.Add('  ((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
  case VAR_KIND_DEPTO of
    kdUH: QrLct.SQL.Add('ci.Unidade UH, ');
    kdObra: QrLct.SQL.Add('ci.Sigla UH, ');
    else QrLct.SQL.Add('"" UH, ');
  end;
  QrLct.SQL.Add('la.*, ct.Codigo CONTA, ca.Prazo, ca.Nome NO_Carteira, ');
  QrLct.SQL.Add('ca.Banco1, ca.Agencia1, ca.Conta1, ca.Banco BancoCar, ');
  QrLct.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
  QrLct.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
  QrLct.SQL.Add('IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA,');
  QrLct.SQL.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,');
  QrLct.SQL.Add('IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,');
  QrLct.SQL.Add('IF(la.ForneceI=0, "",');
  QrLct.SQL.Add('  IF(fi.Tipo=0, fi.RazaoSocial, fi.Nome)) NOMEFORNECEI,');
  QrLct.SQL.Add('IF(la.Sit<2, la.Credito-la.Debito-((la.Pago+la.PagMul+la.PagJur-la.RecDes)*la.Sit), 0) SALDO,');
  QrLct.SQL.Add('IF(la.Cliente>0,');
  QrLct.SQL.Add('  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),');
  QrLct.SQL.Add('  IF (la.Fornecedor>0,');
  QrLct.SQL.Add('    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO, ');
  QrLct.SQL.Add('ELT(la.Endossas, "Endossado", "Endossante", "Ambos", "? ? ? ?") NO_ENDOSSADO, ');
  QrLct.SQL.Add('uc.Login USERCAD_TXT, ua.Login USERALT_TXT, ');
  QrLct.SQL.Add('IF(la.DataCad < 2, "",DATE_FORMAT(la.DataCad,"%d/%m/%y")) DATACAD_TXT, ');
  QrLct.SQL.Add('IF(la.DataAlt < 2, "", DATE_FORMAT(la.DataAlt, "%d/%m/%y")) DATAALT_TXT ');
  QrLct.SQL.Add('FROM ' + Tab_Lct + ' la');
  QrLct.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  QrLct.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  QrLct.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  QrLct.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  QrLct.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  QrLct.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  QrLct.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrLct.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  QrLct.SQL.Add('LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI');
  QrLct.SQL.Add('LEFT JOIN senhas uc ON uc.Numero=la.UserCad');
  QrLct.SQL.Add('LEFT JOIN senhas ua ON ua.Numero=la.UserAlt');
  case VAR_KIND_DEPTO of
    kdUH: QrLct.SQL.Add('LEFT JOIN condimov  ci ON ci.Conta=la.Depto');
    kdObra: QrLct.SQL.Add('LEFT JOIN obrascab  ci ON ci.Codigo=la.Depto');
  end;

  case FTipoData of
    //0: Campo := 'la.Data';
    1: Campo := 'la.Vencimento';
    2: Campo := 'la.Compensado';
    else Campo := 'la.Data';
  end;
  QrLct.SQL.Add('WHERE ' + Campo + ' BETWEEN "'+Ini+'" AND "'+Fim+'"');
  // est� gerando problemas
  //QrLct.SQL.Add('AND la.Tipo='+IntToStr(TipoCarteira));
  QrLct.SQL.Add('AND la.Carteira='+IntToStr(ItemCarteira));
  //
  if (Apto <> Null) and (Apto <> 0) then
    QrLct.SQL.Add('AND la.Depto='+IntToStr(Apto));
  //
  // Ini 2020-01-01
  QrLct.SQL.Add('AND ca.Ativo=1');
  QrLct.SQL.Add('AND (ca.Ativo=1 OR ca.Codigo=-1)');
  // Fim 2020-01-01
  QrLct.SQL.Add('ORDER BY la.Data, la.Controle');
  UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
  //Geral.MB_Teste(QrLct.SQL.Text);
  //
  if Controle = -1 then QrLct.Last else
  begin
    if Controle > 0 then
    begin
      MeuC := Controle;
      MeuS := Sub;
    end else begin
      MeuC := VAR_CONTROLE;
      MeuS := 0;
    end;
    if not QrLct.Locate('Controle;Sub', VarArrayOf([MeuC, MeuS]),[]) then
      QrLct.Last;
  end;
  //Geral.MB_Teste(QrLct.SQL.Text);
end;

procedure TDmLct2.ReabreSoLct2021(QrLct: TmySQLQuery; Controle, Sub: Integer;
  TPDataIni, TPDataFim: TdmkEditDateTimePicker; Apto: Variant);
const
  sProcName = 'TDmLct2.ReabreSoLct2021()';
var
  Ini, Fim, Tab_Lct: String;
  MeuC, MeuS, Indice: Integer;
  //
  FLD_UH, LJ_UH, SQL_UH,
  FLD_Dt,
  sCli, sFrn, sAVencer, sVencido, SQL_Carteira,
  SQL_Terceiro, SQL_CreDeb, SQL_Filiacao, SQL_ContrlCred, SQL_Periodo: String;
begin
  if FCriandoLctGer2 then Exit;
  //
  Tab_Lct := DModG.NomeTab(TMeuDB, ntLct, True);
  QrLct.Close;

  // Fazer mais r�pido inclus�es multiplas!
  if VAR_NaoReabrirLct then
    Exit;

  if TPDataIni <> nil then
    VAR_FL_DataIni := TPDataIni.Date;
  if TPDataFim <> nil then
    VAR_FL_DataFim := TPDataFim.Date;
  //
  Ini := FormatDateTime(VAR_FORMATDATE, VAR_FL_DataIni);
  Fim := FormatDateTime(VAR_FORMATDATE, VAR_FL_DataFim);
  //
  case VAR_KIND_DEPTO of
    kdUH:   FLD_UH := 'ci.Unidade UH, ';
    kdObra: FLD_UH := 'ci.Sigla UH, ';
    else    FLD_UH := '"" UH, ';
  end;
  case VAR_KIND_DEPTO of
    kdUH:   LJ_UH := 'LEFT JOIN condimov  ci ON ci.Conta=la.Depto';
    kdObra: LJ_UH := 'LEFT JOIN obrascab  ci ON ci.Codigo=la.Depto';
    else    LJ_UH := '';
  end;
  if (Apto <> Null) and (Apto <> 0) then
    SQL_UH := 'AND la.Depto=' + Geral.FF0(Apto)
  else
    SQL_UH := '';
  //
  case FTipoData of
    0: FLD_Dt := 'la.Data';
    1: FLD_Dt := 'la.Vencimento';
    2: FLD_Dt := 'la.Compensado';
    else FLD_Dt := EmptyStr;
  end;
  if FLD_Dt  <> EmptyStr then
    SQL_Periodo := 'AND ' + FLD_Dt + ' BETWEEN "' + Ini + '" AND "' + Fim + '"'
  else
    SQL_Periodo := EmptyStr;
  //
  if F2021_Carteira <> 0 then
    SQL_Carteira := 'AND la.Carteira=' + Geral.FF0(F2021_Carteira)
  else
    SQL_Carteira := EmptyStr;
  //
  SQL_CreDeb := '';
  if (F2021_CLiente <> 0) and (F2021_Fornecedor <> 0) then
  begin
    sCli := ' (la.Cliente=' + Geral.FF0(F2021_CLiente) + ') ';
    sFrn := ' (la.Fornecedor=' + Geral.FF0(F2021_Fornecedor) + ') ';
    case F2021_CliFrnCondition of
      TCliFrnCondition.cofcNone    : SQL_Terceiro := '';
      TCliFrnCondition.cofcBothOr  : SQL_Terceiro := 'AND ( ' + sCli + ' OR ' + sFrn + ')';
      TCliFrnCondition.cofcBothAnd : SQL_Terceiro := 'AND ( ' + sCli + ' AND ' + sFrn + ')';
      TCliFrnCondition.cofcCli     : SQL_Terceiro := 'AND ' + sCli;
      TCliFrnCondition.cofcFrn     : SQL_Terceiro := 'AND ' + sFrn;
      else
      begin
        SQL_Terceiro := '';
        Geral.MB_Erro('"CliFrnCondition" indefinido em ' + sProcName)
      end;
    end;
  end;
  //
  if F2021_PsqCredito <> F2021_PsqDebito then
  begin
    if F2021_PsqCredito then
      SQL_CreDeb := ' AND la.Credito <> 0 '
    else
    if F2021_PsqDebito then
      SQL_CreDeb := ' AND la.Debito <> 0 ';
  end;
  //
  if F2021_Genese <> F2021_Suplente then
  begin
    if F2021_Genese then
      SQL_Filiacao := ' AND ID_Pgto <> 0 '
    else
      SQL_Filiacao := ' AND ID_Pgto = 0 ';
  end else
    SQL_Filiacao := '';
  //
  if (F2021_AVencer = F2021_Vencido) and (F2021_AVencer= F2021_Quitado) then
    SQL_ContrlCred := ''
  else
  begin
    //sAVencer := '((la.Tipo = 2 AND la.Compensado < "1899-12-31 12:00:00" AND la.Vencimento >= SYSDATE())';
    sAVencer := '((la.Tipo = 2) AND (la.Compensado < "1899-12-31 12:00:00") AND (la.Vencimento >= SYSDATE()))';
    sVencido := '((la.Tipo = 2) AND (la.Compensado < "1899-12-31 12:00:00") AND (la.Vencimento <  SYSDATE()))';
    Indice := 0;
    if F2021_AVencer  then Indice := Indice + 1;
    if F2021_Vencido  then Indice := Indice + 2;
    if F2021_Quitado  then Indice := Indice + 4;
    case Indice of
      1: SQL_ContrlCred := 'AND ' + sAVencer;
      2: SQL_ContrlCred := 'AND ' + sVencido;
      3: SQL_ContrlCred := 'AND ((la.Tipo = 2) AND (la.Compensado < "1899-12-31 12:00:00"))';
      4: SQL_ContrlCred := 'AND ((la.Tipo in (0,1)) OR ((la.Tipo = 2) AND (la.Compensado >= "1899-12-31 12:00:00")))';
      5: SQL_ContrlCred := 'AND (NOT ' + sVencido + ')';
      6: SQL_ContrlCred := 'AND (NOT ' + sAVencer + ')';
    end;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLct, Dmod.MyDB, [
  'SELECT MOD(la.Mez, 100) Mes2,',
  '  ((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,',
  FLD_UH,
  'la.*, ct.Codigo CONTA, ca.Prazo, ca.Nome NO_Carteira, ',
  'ca.Banco1, ca.Agencia1, ca.Conta1, ca.Banco BancoCar, ',
  'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,',
  'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,',
  'IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA,',
  'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,',
  'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,',
  'IF(la.ForneceI=0, "",',
  '  IF(fi.Tipo=0, fi.RazaoSocial, fi.Nome)) NOMEFORNECEI,',
  'IF(la.Sit<2, la.Credito-la.Debito-((la.Pago+la.PagMul+la.PagJur-la.RecDes)*la.Sit), 0) SALDO,',
  'IF(la.Cliente>0,',
  '  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),',
  '  IF (la.Fornecedor>0,',
  '    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO, ',
  'ELT(la.Endossas, "Endossado", "Endossante", "Ambos", "? ? ? ?") NO_ENDOSSADO, ',
  'uc.Login USERCAD_TXT, ua.Login USERALT_TXT, ',
  'IF(la.DataCad < 2, "",DATE_FORMAT(la.DataCad,"%d/%m/%y")) DATACAD_TXT, ',
  'IF(la.DataAlt < 2, "", DATE_FORMAT(la.DataAlt, "%d/%m/%y")) DATAALT_TXT ',
  'FROM ' + Tab_Lct + ' la',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
  'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0',
  'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo',
  'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo',
  'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto',
  'LEFT JOIN entidades em ON em.Codigo=ct.Empresa',
  'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente',
  'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor',
  'LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI',
  'LEFT JOIN senhas uc ON uc.Numero=la.UserCad',
  'LEFT JOIN senhas ua ON ua.Numero=la.UserAlt',
  'WHERE ca.Ativo=1',
  'AND (ca.Ativo=1 OR ca.Codigo=-1)',
  LJ_UH,
  //'AND la.Carteira=' + Geral.FF0(ItemCarteira),
  SQL_Carteira,
  SQL_Periodo,
  SQL_UH,
  SQL_Terceiro,
  SQL_CreDeb,
  SQL_Filiacao,
  SQL_ContrlCred,
  'ORDER BY la.Data, la.Controle',
  '']);
  //
  if Controle = -1 then QrLct.Last else
  begin
    if Controle > 0 then
    begin
      MeuC := Controle;
      MeuS := Sub;
    end else begin
      MeuC := VAR_CONTROLE;
      MeuS := 0;
    end;
    if not QrLct.Locate('Controle;Sub', VarArrayOf([MeuC, MeuS]),[]) then
      QrLct.Last;
  end;
  //Geral.MB_Teste(QrLct.SQL.Text);
end;

procedure TDmLct2.ReadPixelsPerInch(Reader: TReader);
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

procedure TDmLct2.RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte;
TabLctA: String);
var
  Saldo: Double;
  SQL_Fld: String;
begin
  if Tipo < 2 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSomaM, Dmod.MyDB, [
    'SELECT Inicial Valor ',
    'FROM carteiras ',
    'WHERE Tipo=' + Geral.FF0(Tipo),
    'AND Codigo=' + Geral.FF0(Carteira),
    '']);
    Saldo := QrSomaMValor.Value;
  end else
    Saldo := 0;
  //
  if Tipo = 2 then
    SQL_Fld :=
    'SELECT SUM(IF(Sit=0, (Credito-Debito), ' + sLineBreak +
    'IF(Sit=1, (Credito-Debito+Pago), 0))) Valor '
  else
    SQL_Fld :=
    'SELECT (SUM(Credito) - SUM(Debito)) Valor ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrSomaM, Dmod.MyDB, [
  SQL_Fld,
  'FROM ' + TabLctA,
  'WHERE Tipo=' + Geral.FF0(Tipo),
  'AND Carteira=' + Geral.FF0(Carteira),
  '']);
  Saldo := Saldo + QrSomaMValor.Value;
  QrSomaM.Close;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'carteiras', False, [
  'Saldo'], ['Tipo', 'Codigo'], [Saldo], [Tipo, Carteira], True);

(*  if Localiza = 3 then
  begin
    if Dmod.QrLct.State in [dsBrowse] then
    if (Dmod.QrLctTipo.Value = Tipo)
    and (Dmod.QrLctCarteira.Value = Carteira) then
    Localiza := 1;
    Dmod.DefParams;
  end;
  if Localiza = 1 then GOTOx.LocalizaCodigo(Carteira, Carteira);*)
end;

function TDmLct2.SelecionaEmpresa(SelectLetraLct: TSelectLetraLct;
  FinanceiroNovo: Boolean): Boolean;
var
  Entidade: Integer;
  FinNovo: Boolean;
begin
  DModG.EmpresaAtual_SetaCodigos(0, tecEntidade, False);
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DModG.QrEmpresas.RecordCount = 1 then
  begin
    DModG.EmpresaAtual_SetaCodigos(DModG.QrEmpresasCodigo.Value, tecEntidade, True);
    VAR_LETRA_LCT := 'a';
    // N�o pode!
    //FmPrincipal.DefineVarsCliInt(FEmpresa);
    DModG.NomeTab(TMeuDB, ntLct, True);
  end else
  begin
    if DBCheck.CriaFm(TFmEmpresaSel, FmEmpresaSel, afmoLiberado) then
    begin
      case SelectLetraLct of
        sllNenhum:
        begin
          FmEmpresaSel.RGLetraLct.Visible := False;
        end;
        sllLivre: ; // Nada
        //
        sllSelecA: FmEmpresaSel.RGLetraLct.ItemIndex := 0;
        sllSelecB: FmEmpresaSel.RGLetraLct.ItemIndex := 1;
        sllSelecC: FmEmpresaSel.RGLetraLct.ItemIndex := 2;
        sllSelecD: FmEmpresaSel.RGLetraLct.ItemIndex := 3;
        sllSelecE: FmEmpresaSel.RGLetraLct.ItemIndex := 4;
        sllSelecF: FmEmpresaSel.RGLetraLct.ItemIndex := 5;
        //
        sllForcaA: FmEmpresaSel.RGLetraLct.ItemIndex := 0;
        sllForcaB: FmEmpresaSel.RGLetraLct.ItemIndex := 1;
        sllForcaC: FmEmpresaSel.RGLetraLct.ItemIndex := 2;
        sllForcaD: FmEmpresaSel.RGLetraLct.ItemIndex := 3;
        sllForcaE: FmEmpresaSel.RGLetraLct.ItemIndex := 4;
        sllForcaF: FmEmpresaSel.RGLetraLct.ItemIndex := 5;
      end;
      FmEmpresaSel.RGLetraLct.Enabled := SelectLetraLct < sllForcaA;
      if DModG.QrEmpresas.RecordCount = 0 then
        Geral.MB_Aviso('N�o h� empresa cadastrada!')
      else begin
        if DModG.QrEmpresas.RecordCount = 1 then
        begin
          {
          Precisa saber a tabela de lan ctos
          FEmpresa  := DModG.QrEmpresasFilial.Value;
          DefineVarsEmpresa(FEmpresa);
          }
          FmEmpresaSel.EdEmpresa.ValueVariant := DModG.QrEmpresasFilial.Value;
          FmEmpresaSel.CBEmpresa.KeyValue     := DModG.QrEmpresasFilial.Value;
        end;
        DmodG.ReopenEmpresas(VAR_USUARIO, 0, nil, nil, '', False);
        FmEmpresaSel.ShowModal;
        // DModG.FEmpresa  := FmEmpresaSel.FEmpresa;
        // DModG.FEntidade := FmEmpresaSel.FEntidade;
        // Erro quando n�o seleciona
        // al�m do mais j� selecionouno FmEmpresaSel
        //DefineVarsEmpresa(FEmpresa);
      end;
      FmEmpresaSel.Destroy;
      Application.ProcessMessages;
    end;
  end;
  Entidade := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
  Result   := Entidade <> 0;
  //
  if Result then
  begin
    FinNovo := DModFin.EntidadeHabilitadadaParaFinanceiroNovo(Entidade,
      FinanceiroNovo, FinanceiroNovo) = sfnNovoFin;
    if FinanceiroNovo then
      Result := FinNovo
    else
      Result := not FinNovo;
  end;
end;

procedure TDmLct2.VeSeReabreLct(TPDataIni, TPDataFim: TdmkEditDateTimePicker;
  Apto: Variant; Carteira, Controle, Sub: Integer; QrCrt, QrLct: TmySQLQuery;
  ForcaAbertura: Boolean);
begin
  if (QrCrt.State = dsBrowse) and ((QrLct.State = dsBrowse) or ForcaAbertura) then
  begin
    ReabreSoLct(QrLct, (*QrCrt.FieldByName('Tipo').AsInteger,*) Carteira,
      Controle, Sub, TPDataIni, TPDataFim, Apto);
  end;
end;

procedure TDmLct2.VeSeReabreLct2021(Form: TForm; TPDataIni, TPDataFim:
  TdmkEditDateTimePicker; Apto: Variant; Carteira, Controle, Sub: Integer;
  QrCrt, QrLct: TmySQLQuery; ForcaAbertura: Boolean);
begin
  if (QrCrt.State = dsBrowse) and ((QrLct.State = dsBrowse) or ForcaAbertura) then
  begin
    ReabreSoLct2021(QrLct, Controle, Sub, TPDataIni, TPDataFim, Apto);
  end;
end;

procedure TDmLct2.WritePixelsPerInch(Writer: TWriter);
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

end.
