object DmLct0: TDmLct0
  OldCreateOrder = False
  Height = 587
  Width = 826
  object DsCrt: TDataSource
    DataSet = QrCrt
    Left = 96
    Top = 52
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 96
    Top = 4
  end
  object DsCrtSum: TDataSource
    DataSet = QrCrtSum
    Left = 96
    Top = 100
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrLctBeforeClose
    AfterScroll = QrLctAfterScroll
    OnCalcFields = QrLctCalcFields
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2,'
      '  ((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,'
      'ci.Unidade UH, '
      'la.*, ct.Codigo CONTA, ca.Prazo, ca.Nome NO_Carteira, '
      'ca.Banco1, ca.Agencia1, ca.Conta1,'
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,'
      'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,'
      'IF(la.ForneceI=0, "",'
      '  IF(fi.Tipo=0, fi.RazaoSocial, fi.Nome)) NOMEFORNECEI,'
      'IF(la.Sit<2, la.Credito-la.Debito-(la.Pago*la.Sit), 0) SALDO,'
      'IF(la.Cliente>0,'
      '  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),'
      '  IF (la.Fornecedor>0,'
      
        '    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO' +
        ', '
      
        'ELT(la.Endossas, "Endossado", "Endossante", "Ambos", "? ? ? ?") ' +
        'NO_ENDOSSADO,'
      'uc.Login USERCAD_TXT, ua.Login USERALT_TXT,'
      
        'IF(la.DataCad < 2, "",DATE_FORMAT(la.DataCad,"%d/%m/%y")) DATACA' +
        'D_TXT,'
      
        'IF(la.DataAlt < 2, "", DATE_FORMAT(la.DataAlt, "%d/%m/%y")) DATA' +
        'ALT_TXT'
      'FROM lct0001a la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI'
      'LEFT JOIN condimov  ci ON ci.Conta=la.Depto'
      'LEFT JOIN senhas uc ON uc.Numero=la.UserCad'
      'LEFT JOIN senhas ua ON ua.Numero=la.UserAlt'
      'WHERE la.Data BETWEEN "2007/05/11" AND "2010/10/04"'
      'AND la.Carteira=3'
      'ORDER BY la.Data, la.Controle'
      ''
      '')
    Left = 28
    Top = 4
    object QrLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 25
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrLctNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 128
    end
    object QrLctNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'contas.Nome'
      Size = 128
    end
    object QrLctNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 128
    end
    object QrLctNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 128
    end
    object QrLctNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 128
    end
    object QrLctNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctcliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctUserCad: TSmallintField
      FieldName = 'UserCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLctCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLctPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrLctForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLctQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLctEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrLctAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrLctContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrLctCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrLctDescoVal: TFloatField
      FieldName = 'DescoVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrLctNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrLctAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrLctUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrLctExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrLctSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLctMoraVal: TFloatField
      FieldName = 'MoraVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctMultaVal: TFloatField
      FieldName = 'MultaVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrLctCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrLctTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrLctID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrLctReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrLctAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrLctPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrLctPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrLctSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrLctMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrLctProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrLctCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
      Required = True
    end
    object QrLctAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLctAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrLctSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 5
    end
    object QrLctEndossas: TSmallintField
      FieldName = 'Endossas'
    end
    object QrLctEndossan: TFloatField
      FieldName = 'Endossan'
    end
    object QrLctEndossad: TFloatField
      FieldName = 'Endossad'
    end
    object QrLctCancelado: TSmallintField
      FieldName = 'Cancelado'
    end
    object QrLctEventosCad: TIntegerField
      FieldName = 'EventosCad'
    end
    object QrLctUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrLctEncerrado: TIntegerField
      FieldName = 'Encerrado'
    end
    object QrLctErrCtrl: TIntegerField
      FieldName = 'ErrCtrl'
    end
    object QrLctNO_Carteira: TWideStringField
      FieldName = 'NO_Carteira'
      Size = 100
    end
    object QrLctBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrLctAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrLctConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrLctNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrLctNO_ENDOSSADO: TWideStringField
      FieldName = 'NO_ENDOSSADO'
      Size = 10
    end
    object QrLctSERIE_CHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_CHEQUE'
      Calculated = True
    end
    object QrLctIndiPag: TIntegerField
      FieldName = 'IndiPag'
      DisplayFormat = '0;-0; '
    end
    object QrLctFisicoSrc: TSmallintField
      FieldName = 'FisicoSrc'
    end
    object QrLctFisicoCod: TIntegerField
      FieldName = 'FisicoCod'
    end
    object QrLctUSERCAD_TXT: TWideStringField
      FieldName = 'USERCAD_TXT'
      Size = 30
    end
    object QrLctUSERALT_TXT: TWideStringField
      FieldName = 'USERALT_TXT'
      Size = 30
    end
    object QrLctDATACAD_TXT: TWideStringField
      FieldName = 'DATACAD_TXT'
      Size = 10
    end
    object QrLctDATAALT_TXT: TWideStringField
      FieldName = 'DATAALT_TXT'
      Size = 10
    end
  end
  object QrCrt: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCrtBeforeClose
    AfterClose = QrCrtAfterClose
    AfterScroll = QrCrtAfterScroll
    OnCalcFields = QrCrtCalcFields
    SQL.Strings = (
      'SELECT DISTINCT ca.*, ba.Nome NOMEDOBANCO,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEFORNECEI'
      'FROM carteiras ca'
      'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco'
      'LEFT JOIN entidades en ON en.Codigo=ca.ForneceI'
      'WHERE ca.ForneceI in (-11)'
      'AND ca.Codigo > 0'
      'ORDER BY ca.Nome')
    Left = 28
    Top = 52
    object QrCrtDIFERENCA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIFERENCA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrCrtTIPOPRAZO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOPRAZO'
      Size = 1
      Calculated = True
    end
    object QrCrtNOMEPAGREC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEPAGREC'
      Size = 50
      Calculated = True
    end
    object QrCrtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCrtNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCrtTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCrtInicial: TFloatField
      FieldName = 'Inicial'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCrtID: TWideStringField
      FieldName = 'ID'
      Size = 100
    end
    object QrCrtFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCrtID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Size = 50
    end
    object QrCrtSaldo: TFloatField
      FieldName = 'Saldo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCrtEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCrtPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCrtDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCrtDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCrtUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCrtUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCrtPagRec: TIntegerField
      FieldName = 'PagRec'
    end
    object QrCrtDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrCrtExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCrtForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCrtNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 100
    end
    object QrCrtTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrCrtBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCrtAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrCrtConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCrtCheque1: TIntegerField
      FieldName = 'Cheque1'
    end
    object QrCrtContato1: TWideStringField
      FieldName = 'Contato1'
      Size = 100
    end
    object QrCrtAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrCrtContab: TWideStringField
      FieldName = 'Contab'
    end
    object QrCrtOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCrtFuturoC: TFloatField
      FieldName = 'FuturoC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtFuturoD: TFloatField
      FieldName = 'FuturoD'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtFuturoS: TFloatField
      FieldName = 'FuturoS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtForneceN: TSmallintField
      FieldName = 'ForneceN'
    end
    object QrCrtExclusivo: TSmallintField
      FieldName = 'Exclusivo'
    end
    object QrCrtRecebeBloq: TSmallintField
      FieldName = 'RecebeBloq'
    end
    object QrCrtEntiDent: TIntegerField
      FieldName = 'EntiDent'
    end
    object QrCrtCodCedente: TWideStringField
      FieldName = 'CodCedente'
    end
    object QrCrtAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCrtAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCrtValMorto: TFloatField
      FieldName = 'ValMorto'
    end
    object QrCrtNOMEDOBANCO: TWideStringField
      FieldName = 'NOMEDOBANCO'
      Size = 100
    end
    object QrCrtNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrCrtCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrCrtIgnorSerie: TSmallintField
      FieldName = 'IgnorSerie'
    end
    object QrCrtValIniOld: TFloatField
      FieldName = 'ValIniOld'
    end
    object QrCrtSdoFimB: TFloatField
      FieldName = 'SdoFimB'
    end
  end
  object QrCrtSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Saldo) SALDO, SUM(FuturoC) FuturoC,'
      'SUM(FuturoD) FuturoD, SUM(FuturoS) FuturoS, '
      'SUM(EmCaixa) EmCaixa, SUM(EmCaixa-Saldo) Difere,'
      'SUM(Saldo+FuturoS) SDO_FUT'
      'FROM carteiras ca'
      'WHERE ca.ForneceI in (1,2,3,4)'
      '')
    Left = 28
    Top = 100
    object QrCrtSumSALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtSumFuturoC: TFloatField
      FieldName = 'FuturoC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtSumFuturoD: TFloatField
      FieldName = 'FuturoD'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtSumFuturoS: TFloatField
      FieldName = 'FuturoS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtSumEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtSumDifere: TFloatField
      FieldName = 'Difere'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtSumSDO_FUT: TFloatField
      FieldName = 'SDO_FUT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object QrLcP: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeClose = QrLctBeforeClose
    AfterScroll = QrLctAfterScroll
    OnCalcFields = QrLctCalcFields
    SQL.Strings = (
      'SELECT Data, Tipo, Carteira, Controle,'
      'Sub, CliInt, Cliente, Fornecedor,'
      'Documento, SUM(Credito) Credito, SUM(Debito)'
      'Debito, SerieCH, Vencimento, COUNT(Ativo) ITENS'
      'FROM _lct_proto_'
      'GROUP BY Data, Tipo, Carteira, CliInt,'
      'SerieCH, Documento')
    Left = 312
    Top = 4
    object QrLcPData: TDateField
      FieldName = 'Data'
    end
    object QrLcPTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLcPCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLcPControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrLcPSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLcPCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLcPCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLcPFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLcPDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLcPCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLcPDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLcPSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLcPVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLcPITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsLcP: TDataSource
    DataSet = QrLcP
    Left = 380
    Top = 4
  end
  object QrLcI: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeClose = QrLctBeforeClose
    AfterScroll = QrLctAfterScroll
    OnCalcFields = QrLctCalcFields
    SQL.Strings = (
      'SELECT Controle Lancto'
      'FROM _lct_proto_'
      'WHERE Data=:P0'
      'AND Tipo=:P1'
      'AND Carteira=:P2'
      'AND CliInt=:P3'
      'AND SerieCH=:P4'
      'AND Documento=:P5')
    Left = 312
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end>
    object QrLcILancto: TLargeintField
      FieldName = 'Lancto'
    end
  end
  object DsQrLcI: TDataSource
    DataSet = QrLcI
    Left = 380
    Top = 52
  end
end
