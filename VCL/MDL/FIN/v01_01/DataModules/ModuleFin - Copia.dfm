object DModFin: TDModFin
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 562
  Width = 1040
  object QrAPL: TMySQLQuery
    Database = Dmod.MyDB
    Left = 456
    Top = 100
    object QrAPLCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrAPLDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrAPLMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrAPLMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrAPLData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrAPLItens: TFloatField
      FieldName = 'Itens'
    end
    object QrAPLDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
  end
  object QrVLA: TMySQLQuery
    Database = Dmod.MyDB
    Left = 456
    Top = 148
    object QrVLACredito: TFloatField
      FieldName = 'Credito'
    end
    object QrVLADebito: TFloatField
      FieldName = 'Debito'
    end
    object QrVLAEndossan: TFloatField
      FieldName = 'Endossan'
    end
    object QrVLAEndossad: TFloatField
      FieldName = 'Endossad'
    end
    object QrVLATipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object QrTransf: TMySQLQuery
    Database = Dmod.MyDB
    Left = 84
    Top = 356
    object QrTransfData: TDateField
      FieldName = 'Data'
    end
    object QrTransfTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTransfCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrTransfControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTransfGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrTransfDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrTransfCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrTransfDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrTransfSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrTransfSub: TIntegerField
      FieldName = 'Sub'
    end
    object QrTransfDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
  end
  object QrUDAPC: TMySQLQuery
    Database = Dmod.MyDB
    Left = 24
    Top = 356
  end
  object QrTrfCtas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 152
    Top = 484
    object QrTrfCtasData: TDateField
      FieldName = 'Data'
    end
    object QrTrfCtasTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTrfCtasCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrTrfCtasControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTrfCtasGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrTrfCtasDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrTrfCtasCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrTrfCtasDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrTrfCtasSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrTrfCtasSub: TSmallintField
      FieldName = 'Sub'
    end
  end
  object QrSaldo: TMySQLQuery
    Database = Dmod.MyDB
    Left = 92
    Top = 148
    object QrSaldoValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object QrLocLct: TMySQLQuery
    Database = Dmod.MyDB
    Left = 32
    Top = 148
    object QrLocLctData: TDateField
      FieldName = 'Data'
    end
    object QrLocLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLocLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLocLctControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocLctSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLocLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrLocLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLocLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLocLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLocLctDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLocLctCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLocLctCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLocLctDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLocLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLocLctVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLocLctLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLocLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLocLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrLocLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLocLctFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrLocLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLocLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLocLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLocLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLocLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLocLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLocLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLocLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLocLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLocLctCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLocLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLocLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLocLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLocLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocLctDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocLctUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLocLctUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLocLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLocLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrLocLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLocLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLocLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLocLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrLocLcto: TMySQLQuery
    Database = Dmod.MyDB
    Left = 456
    Top = 196
    object QrLocLctoData: TDateField
      FieldName = 'Data'
    end
    object QrLocLctoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocLctoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLocLctoCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLocLctoCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLocLctoTab: TWideStringField
      FieldName = 'Tab'
      Size = 8
    end
  end
  object QrLocCta: TMySQLQuery
    Database = Dmod.MyDB
    Left = 152
    Top = 436
    object QrLocCtaMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
  end
  object QrLocUHs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta Depto, Unidade NO_DEPTO'
      'FROM condimov'
      'WHERE Propriet=:P0')
    Left = 696
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocUHsDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLocUHsNO_DEPTO: TWideStringField
      FieldName = 'NO_DEPTO'
      Size = 100
    end
  end
  object QrELSCI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 332
    Top = 4
    object QrELSCIData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrELSCITipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrELSCICarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrELSCIAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrELSCIGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrELSCIDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 25
    end
    object QrELSCINotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrELSCIDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCICredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCICompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrELSCIDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000; ; '
    end
    object QrELSCISit: TIntegerField
      FieldName = 'Sit'
    end
    object QrELSCIVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrELSCILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrELSCIFatID: TIntegerField
      FieldName = 'FatID'
      DisplayFormat = '000;-000; '
    end
    object QrELSCIFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrELSCICONTA: TIntegerField
      FieldName = 'CONTA'
    end
    object QrELSCINOMECONTA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONTA'
      FixedChar = True
      Size = 128
    end
    object QrELSCINOMEEMPRESA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEEMPRESA'
      Size = 128
    end
    object QrELSCINOMESUBGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMESUBGRUPO'
      Size = 128
    end
    object QrELSCINOMEGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEGRUPO'
      Size = 128
    end
    object QrELSCINOMECONJUNTO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONJUNTO'
      Size = 128
    end
    object QrELSCINOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrELSCIAno: TFloatField
      FieldName = 'Ano'
    end
    object QrELSCIMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrELSCIMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrELSCIBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrELSCILocal: TIntegerField
      FieldName = 'Local'
    end
    object QrELSCIFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrELSCISub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrELSCICartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrELSCILinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrELSCIPago: TFloatField
      FieldName = 'Pago'
    end
    object QrELSCISALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCIID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrELSCIMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrELSCIFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrELSCIcliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrELSCIMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrELSCINOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrELSCINOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrELSCITIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrELSCINOMERELACIONADO: TWideStringField
      FieldName = 'NOMERELACIONADO'
      Size = 50
    end
    object QrELSCIOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrELSCILancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrELSCIMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrELSCIATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrELSCIJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrELSCIDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrELSCINivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrELSCIVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrELSCIAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrELSCIMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrELSCIProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrELSCIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrELSCIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrELSCIUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrELSCIUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrELSCIControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrELSCIID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrELSCICtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrELSCIFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrELSCIICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCIICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCIDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrELSCICOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrELSCICliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrELSCIDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrELSCIDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrELSCIPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrELSCIForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrELSCIQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrELSCIEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrELSCIAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrELSCIContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrELSCICNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrELSCIDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrELSCIDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrELSCIUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrELSCINFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrELSCIAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrELSCIExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrELSCISerieCH: TWideStringField
      DisplayWidth = 10
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrELSCISERIE_CHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_CHEQUE'
      Calculated = True
    end
    object QrELSCIDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrELSCINOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrELSCIMoraVal: TFloatField
      FieldName = 'MoraVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCIMultaVal: TFloatField
      FieldName = 'MultaVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCICNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrELSCIBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrELSCIAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrELSCIConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrELSCITipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrELSCIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrELSCIReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrELSCIID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrELSCIAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrELSCIAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrELSCIFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrELSCIProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrELSCIPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrELSCIPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrELSCICART_DONO: TIntegerField
      FieldName = 'CART_DONO'
    end
    object QrELSCINOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT PBB'
      'FROM cond'
      'WHERE Codigo=1')
    Left = 452
    Top = 388
    object QrCliIntPBB: TSmallintField
      FieldName = 'PBB'
    end
  end
  object QrSTCP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1')
    Left = 152
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSTCPSUMMOV: TFloatField
      FieldName = 'SUMMOV'
    end
    object QrSTCPSDOANT: TFloatField
      FieldName = 'SDOANT'
    end
    object QrSTCPSUMCRE: TFloatField
      FieldName = 'SUMCRE'
    end
    object QrSTCPSUMDEB: TFloatField
      FieldName = 'SUMDEB'
    end
    object QrSTCPSDOFIM: TFloatField
      FieldName = 'SDOFIM'
    end
  end
  object frxDsSTCP: TfrxDBDataset
    UserName = 'frxDsSTCP'
    CloseDataSource = False
    DataSet = QrSTCP
    BCDToCurrency = False
    Left = 152
    Top = 340
  end
  object QrExtratos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM extratocc'
      
        'ORDER BY CartO, CartN, CartC, DataM, SdIni, DebCr, Unida,  Docum' +
        ', TipoI')
    Left = 516
    Top = 4
    object QrExtratosLinha: TAutoIncField
      FieldName = 'Linha'
    end
    object QrExtratosDataM: TDateField
      FieldName = 'DataM'
    end
    object QrExtratosTexto: TWideStringField
      DisplayWidth = 255
      FieldName = 'Texto'
      Size = 255
    end
    object QrExtratosDocum: TWideStringField
      FieldName = 'Docum'
      Size = 30
    end
    object QrExtratosNotaF: TIntegerField
      FieldName = 'NotaF'
    end
    object QrExtratosCredi: TFloatField
      FieldName = 'Credi'
    end
    object QrExtratosDebit: TFloatField
      FieldName = 'Debit'
    end
    object QrExtratosSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrExtratosCartO: TIntegerField
      FieldName = 'CartO'
    end
    object QrExtratosCartC: TIntegerField
      FieldName = 'CartC'
    end
    object QrExtratosCartN: TWideStringField
      FieldName = 'CartN'
      Size = 100
    end
    object QrExtratosSdIni: TIntegerField
      FieldName = 'SdIni'
    end
    object QrExtratosTipoI: TIntegerField
      FieldName = 'TipoI'
    end
    object QrExtratosUnida: TWideStringField
      FieldName = 'Unida'
    end
    object QrExtratosCTipN: TWideStringField
      FieldName = 'CTipN'
      Size = 30
    end
    object QrExtratosDebCr: TWideStringField
      FieldName = 'DebCr'
      Size = 1
    end
    object QrExtratosTEXTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXTO_TXT'
      Size = 255
      Calculated = True
    end
  end
  object frxDsExtratos: TfrxDBDataset
    UserName = 'frxDsExtratos'
    CloseDataSource = False
    DataSet = QrExtratos
    BCDToCurrency = False
    Left = 576
    Top = 4
  end
  object DsExtratos: TDataSource
    DataSet = QrExtratos
    Left = 636
    Top = 4
  end
  object QrZer: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CartC, SUM(Credi) Credi, SUM(Debit) Debit'
      'FROM extratocc'
      'GROUP BY CartC')
    Left = 696
    Top = 4
    object QrZerCartC: TIntegerField
      FieldName = 'CartC'
    end
    object QrZerCredi: TFloatField
      FieldName = 'Credi'
    end
    object QrZerDebit: TFloatField
      FieldName = 'Debit'
    end
  end
  object QrPesqCar: TMySQLQuery
    Database = Dmod.MyDB
    Left = 212
    Top = 292
    object QrPesqCarCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPesqCarInicial: TFloatField
      FieldName = 'Inicial'
    end
    object QrPesqCarSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrPesqCarNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrPesqCarNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 100
    end
    object QrPesqCarOrdemCart: TIntegerField
      FieldName = 'OrdemCart'
      Required = True
    end
    object QrPesqCarTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Required = True
    end
  end
  object QrPesqAgr: TMySQLQuery
    Database = Dmod.MyDB
    Left = 212
    Top = 340
    object QrPesqAgrContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object QrPesqAgrNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrPesqAgrNOME2CART: TWideStringField
      FieldName = 'NOME2CART'
      Size = 100
    end
    object QrPesqAgrCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPesqAgrData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPesqAgrDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPesqAgrCREDITO: TFloatField
      FieldName = 'CREDITO'
    end
    object QrPesqAgrDEBITO: TFloatField
      FieldName = 'DEBITO'
    end
    object QrPesqAgrSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrPesqAgrDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPesqAgrDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrPesqAgrMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPesqAgrApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrPesqAgrNOMEAGR: TWideStringField
      FieldName = 'NOMEAGR'
      Size = 50
    end
    object QrPesqAgrInfoDescri: TSmallintField
      FieldName = 'InfoDescri'
    end
    object QrPesqAgrMensal: TSmallintField
      FieldName = 'Mensal'
    end
    object QrPesqAgrNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrPesqAgrOrdemCart: TIntegerField
      FieldName = 'OrdemCart'
    end
    object QrPesqAgrTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Required = True
    end
    object QrPesqAgrNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrPesqAgrSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrPesqAgrUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
  end
  object QrPesqRes: TMySQLQuery
    Database = Dmod.MyDB
    Left = 272
    Top = 292
    object QrPesqResNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrPesqResNOME2CART: TWideStringField
      FieldName = 'NOME2CART'
      Size = 100
    end
    object QrPesqResCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPesqResData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPesqResDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPesqResCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPesqResDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrPesqResMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPesqResSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrPesqResDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPesqResOrdemCart: TIntegerField
      FieldName = 'OrdemCart'
    end
    object QrPesqResDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrPesqResTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Required = True
    end
    object QrPesqResNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrPesqResSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrPesqResNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrPesqResUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
  end
  object QrPesqSum: TMySQLQuery
    Database = Dmod.MyDB
    Left = 272
    Top = 340
    object QrPesqSumContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object QrPesqSumNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrPesqSumNOME2CART: TWideStringField
      FieldName = 'NOME2CART'
      Size = 100
    end
    object QrPesqSumCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPesqSumData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPesqSumDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPesqSumCREDITO: TFloatField
      FieldName = 'CREDITO'
    end
    object QrPesqSumDEBITO: TFloatField
      FieldName = 'DEBITO'
    end
    object QrPesqSumMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPesqSumOrdemCart: TIntegerField
      FieldName = 'OrdemCart'
    end
    object QrPesqSumTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Required = True
    end
    object QrPesqSumNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrPesqSumSit: TIntegerField
      FieldName = 'Sit'
    end
  end
  object QrSdoTrf: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(car.Tipo<>2,lct.Carteira, bco.Codigo) Carteira,'
      'SUM(lct.Credito) SDO_CRE, SUM(lct.Debito) SDO_DEB'
      'FROM syndic.lct0001a lct'
      'LEFT JOIN carteiras car ON car.Codigo=lct.Carteira'
      'LEFT JOIN carteiras bco ON bco.Codigo=car.Banco'
      'WHERE lct.Genero = -1'
      'AND car.ForneceI=1'
      'AND lct.Data BETWEEN "2012-11-01" AND "2012-11-30"'
      'GROUP BY Carteira')
    Left = 516
    Top = 52
    object QrSdoTrfCarteira: TLargeintField
      FieldName = 'Carteira'
    end
    object QrSdoTrfSDO_CRE: TFloatField
      FieldName = 'SDO_CRE'
    end
    object QrSdoTrfSDO_DEB: TFloatField
      FieldName = 'SDO_DEB'
    end
  end
  object frxDsSdoExcl: TfrxDBDataset
    UserName = 'frxDsSdoExcl'
    CloseDataSource = False
    DataSet = QrSdoExcl
    BCDToCurrency = False
    Left = 636
    Top = 52
  end
  object QrSdoExcl: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSdoExclCalcFields
    Left = 576
    Top = 52
    object QrSdoExclCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrSdoExclNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrSdoExclNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 100
    end
    object QrSdoExclOrdemCart: TIntegerField
      FieldName = 'OrdemCart'
      Required = True
    end
    object QrSdoExclSDO_CAD: TFloatField
      FieldName = 'SDO_CAD'
    end
    object QrSdoExclSDO_ANT: TFloatField
      FieldName = 'SDO_ANT'
    end
    object QrSdoExclSDO_INI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SDO_INI'
      Calculated = True
    end
    object QrSdoExclMOV_CRE: TFloatField
      FieldKind = fkLookup
      FieldName = 'MOV_CRE'
      LookupDataSet = QrSdoMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_CRE'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoExclMOV_DEB: TFloatField
      FieldKind = fkLookup
      FieldName = 'MOV_DEB'
      LookupDataSet = QrSdoMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_DEB'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoExclTRF_CRE: TFloatField
      FieldKind = fkLookup
      FieldName = 'TRF_CRE'
      LookupDataSet = QrSdoTrf
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_CRE'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoExclSDO_FIM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SDO_FIM'
      Calculated = True
    end
    object QrSdoExclTRF_DEB: TFloatField
      FieldKind = fkLookup
      FieldName = 'TRF_DEB'
      LookupDataSet = QrSdoTrf
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_DEB'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoExclKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrSdoExclTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrSdoExclNO_TipoCart: TWideStringField
      FieldName = 'NO_TipoCart'
      Size = 7
    end
  end
  object QrSdoMov: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lct.Carteira,'
      'SUM(lct.Credito) SDO_CRE, SUM(lct.Debito) SDO_DEB'
      'FROM syndic.lct0001a lct'
      'LEFT JOIN carteiras car ON car.Codigo=lct.Carteira'
      'WHERE (lct.Genero > 0'
      'OR lct.Genero=-10)'
      'AND car.Tipo <> 2'
      'AND car.ForneceI=1'
      'AND lct.Data BETWEEN "2012-11-01" AND "2012-11-30"'
      'AND lct.Sit IN (0,1,2,3)'
      'GROUP BY lct.Carteira')
    Left = 516
    Top = 100
    object QrSdoMovCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrSdoMovSDO_CRE: TFloatField
      FieldName = 'SDO_CRE'
    end
    object QrSdoMovSDO_DEB: TFloatField
      FieldName = 'SDO_DEB'
    end
  end
  object QrSdoCtas: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSdoCtasCalcFields
    SQL.Strings = (
      'SELECT 0 KGT, car.Codigo Carteira, car.SdoFimB SDO_CAD, '
      'car.Nome, car.Nome2, car.Ordem OrdemCart, car.Tipo,'
      'ELT(car.Tipo+1,"CAIXA","EXTRATO","EMISS'#195'O","?") NO_TipoCart,'
      '('
      '  SELECT SUM(lct.Credito-lct.Debito)'
      '  FROM syndic.lct0001a lct'
      '  WHERE lct.Carteira=car.Codigo'
      '  AND lct.Data < "2012-11-01"'
      ') SDO_ANT'
      'FROM carteiras car'
      'WHERE car.ForneceI=1'
      'AND car.Tipo <> 2'
      'AND'
      '('
      '  car.Ativo=1'
      '  OR'
      '  car.Saldo <> 0'
      ')'
      'AND car.Exclusivo=0')
    Left = 576
    Top = 100
    object QrSdoCtasCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrSdoCtasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrSdoCtasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 100
    end
    object QrSdoCtasOrdemCart: TIntegerField
      FieldName = 'OrdemCart'
      Required = True
    end
    object QrSdoCtasSDO_CAD: TFloatField
      FieldName = 'SDO_CAD'
    end
    object QrSdoCtasSDO_ANT: TFloatField
      FieldName = 'SDO_ANT'
    end
    object QrSdoCtasSDO_INI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SDO_INI'
      Calculated = True
    end
    object QrSdoCtasMOV_CRE: TFloatField
      FieldKind = fkLookup
      FieldName = 'MOV_CRE'
      LookupDataSet = QrSdoMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_CRE'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoCtasMOV_DEB: TFloatField
      FieldKind = fkLookup
      FieldName = 'MOV_DEB'
      LookupDataSet = QrSdoMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_DEB'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoCtasTRF_CRE: TFloatField
      FieldKind = fkLookup
      FieldName = 'TRF_CRE'
      LookupDataSet = QrSdoTrf
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_CRE'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoCtasSDO_FIM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SDO_FIM'
      Calculated = True
    end
    object QrSdoCtasTRF_DEB: TFloatField
      FieldKind = fkLookup
      FieldName = 'TRF_DEB'
      LookupDataSet = QrSdoTrf
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_DEB'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoCtasKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrSdoCtasTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrSdoCtasNO_TipoCart: TWideStringField
      FieldName = 'NO_TipoCart'
      Size = 7
    end
  end
  object frxDsSdoCtas: TfrxDBDataset
    UserName = 'frxDsSdoCtas'
    CloseDataSource = False
    DataSet = QrSdoCtas
    BCDToCurrency = False
    Left = 636
    Top = 100
  end
  object QrCTCN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM contasniv'
      'WHERE Entidade=:P0')
    Left = 152
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrSNC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pla.Codigo, pla.Nome NOME, SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM,'
      'pla.Codigo CodPla, cjt.Codigo CodCjt,'
      'gru.Codigo CodGru, sgr.Codigo CodSgr'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano'
      'LEFT JOIN contasniv niv ON pla.Codigo=niv.Genero'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND niv.Nivel=5 AND niv.Entidade=:P2'
      ''
      'GROUP BY pla.Codigo'
      'ORDER BY pla.OrdemLista, pla.Nome')
    Left = 152
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSNCNOME: TWideStringField
      FieldName = 'NOME'
      Origin = 'plano.Nome'
      Size = 50
    end
    object QrSNCSUMMOV: TFloatField
      FieldName = 'SUMMOV'
    end
    object QrSNCSDOANT: TFloatField
      FieldName = 'SDOANT'
    end
    object QrSNCSUMCRE: TFloatField
      FieldName = 'SUMCRE'
    end
    object QrSNCSUMDEB: TFloatField
      FieldName = 'SUMDEB'
    end
    object QrSNCSDOFIM: TFloatField
      FieldName = 'SDOFIM'
    end
    object QrSNCCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'plano.Codigo'
    end
    object QrSNCCodPla: TIntegerField
      FieldName = 'CodPla'
      Origin = 'plano.Codigo'
      Required = True
    end
    object QrSNCCodCjt: TIntegerField
      FieldName = 'CodCjt'
      Origin = 'conjuntos.Codigo'
      Required = True
    end
    object QrSNCCodGru: TIntegerField
      FieldName = 'CodGru'
      Origin = 'grupos.Codigo'
      Required = True
    end
    object QrSNCCodSgr: TIntegerField
      FieldName = 'CodSgr'
      Origin = 'subgrupos.Codigo'
      Required = True
    end
  end
  object QrSNG: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSNGCalcFields
    SQL.Strings = (
      'SELECT 0 KGT, sn.*'
      'FROM sdoniveis sn'
      'ORDER BY sn.Ordena')
    Left = 152
    Top = 152
    object QrSNGNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'sdoniveis.Nivel'
    end
    object QrSNGGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'sdoniveis.Genero'
    end
    object QrSNGNomeGe: TWideStringField
      FieldName = 'NomeGe'
      Origin = 'sdoniveis.NomeGe'
      Size = 100
    end
    object QrSNGNomeNi: TWideStringField
      FieldName = 'NomeNi'
      Origin = 'sdoniveis.NomeNi'
    end
    object QrSNGSumMov: TFloatField
      FieldName = 'SumMov'
      Origin = 'sdoniveis.SumMov'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGSdoAnt: TFloatField
      FieldName = 'SdoAnt'
      Origin = 'sdoniveis.SdoAnt'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGSumCre: TFloatField
      FieldName = 'SumCre'
      Origin = 'sdoniveis.SumCre'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGSumDeb: TFloatField
      FieldName = 'SumDeb'
      Origin = 'sdoniveis.SumDeb'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGSdoFim: TFloatField
      FieldName = 'SdoFim'
      Origin = 'sdoniveis.SdoFim'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGCtrla: TSmallintField
      FieldName = 'Ctrla'
      Origin = 'sdoniveis.Ctrla'
    end
    object QrSNGSeleci: TSmallintField
      FieldName = 'Seleci'
      Origin = 'sdoniveis.Seleci'
    end
    object QrSNGCodPla: TIntegerField
      FieldName = 'CodPla'
    end
    object QrSNGCodCjt: TIntegerField
      FieldName = 'CodCjt'
    end
    object QrSNGCodGru: TIntegerField
      FieldName = 'CodGru'
    end
    object QrSNGCodSgr: TIntegerField
      FieldName = 'CodSgr'
    end
    object QrSNGCODIGOS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CODIGOS_TXT'
      Size = 50
      Calculated = True
    end
    object QrSNGOrdena: TWideStringField
      FieldName = 'Ordena'
      Size = 100
    end
    object QrSNGKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
  end
  object frxDsSNG: TfrxDBDataset
    UserName = 'frxDsSNG'
    CloseDataSource = False
    DataSet = QrSNG
    BCDToCurrency = False
    Left = 152
    Top = 196
  end
  object QrSaldosNiv: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *, 0 KGT FROM saldosniv'
      'ORDER BY OL_Pla, NO_Pla, OL_Cjt, NO_Cjt, OL_Gru, '
      'NO_Gru, OL_SGr, NO_SGr, OL_Cta, NO_Cta')
    Left = 212
    Top = 244
    object QrSaldosNivNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrSaldosNivCO_Cta: TIntegerField
      FieldName = 'CO_Cta'
    end
    object QrSaldosNivCO_SGr: TIntegerField
      FieldName = 'CO_SGr'
    end
    object QrSaldosNivCO_Gru: TIntegerField
      FieldName = 'CO_Gru'
    end
    object QrSaldosNivCO_Cjt: TIntegerField
      FieldName = 'CO_Cjt'
    end
    object QrSaldosNivCO_Pla: TIntegerField
      FieldName = 'CO_Pla'
    end
    object QrSaldosNivNO_Cta: TWideStringField
      FieldName = 'NO_Cta'
      Size = 100
    end
    object QrSaldosNivNO_SGr: TWideStringField
      FieldName = 'NO_SGr'
      Size = 100
    end
    object QrSaldosNivNO_Gru: TWideStringField
      FieldName = 'NO_Gru'
      Size = 100
    end
    object QrSaldosNivNO_Cjt: TWideStringField
      FieldName = 'NO_Cjt'
      Size = 100
    end
    object QrSaldosNivNO_Pla: TWideStringField
      FieldName = 'NO_Pla'
      Size = 100
    end
    object QrSaldosNivOL_Cta: TIntegerField
      FieldName = 'OL_Cta'
    end
    object QrSaldosNivOL_SGr: TIntegerField
      FieldName = 'OL_SGr'
    end
    object QrSaldosNivOL_Gru: TIntegerField
      FieldName = 'OL_Gru'
    end
    object QrSaldosNivOL_Cjt: TIntegerField
      FieldName = 'OL_Cjt'
    end
    object QrSaldosNivOL_Pla: TIntegerField
      FieldName = 'OL_Pla'
    end
    object QrSaldosNivSdoAnt: TFloatField
      FieldName = 'SdoAnt'
    end
    object QrSaldosNivCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrSaldosNivDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrSaldosNivMovim: TFloatField
      FieldName = 'Movim'
    end
    object QrSaldosNivEX_Cta: TIntegerField
      FieldName = 'EX_Cta'
    end
    object QrSaldosNivEX_SGr: TIntegerField
      FieldName = 'EX_SGr'
    end
    object QrSaldosNivEX_Gru: TIntegerField
      FieldName = 'EX_Gru'
    end
    object QrSaldosNivEX_Cjt: TIntegerField
      FieldName = 'EX_Cjt'
    end
    object QrSaldosNivEX_Pla: TIntegerField
      FieldName = 'EX_Pla'
    end
    object QrSaldosNivID_SEQ: TIntegerField
      FieldName = 'ID_SEQ'
    end
    object QrSaldosNivKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrSaldosNivAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsSaldosNiv: TDataSource
    DataSet = QrSaldosNiv
    Left = 272
    Top = 244
  end
  object frxDsSaldosNiv: TfrxDBDataset
    UserName = 'frxDsSaldosNiv'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nivel=Nivel'
      'CO_Cta=CO_Cta'
      'CO_SGr=CO_SGr'
      'CO_Gru=CO_Gru'
      'CO_Cjt=CO_Cjt'
      'CO_Pla=CO_Pla'
      'NO_Cta=NO_Cta'
      'NO_SGr=NO_SGr'
      'NO_Gru=NO_Gru'
      'NO_Cjt=NO_Cjt'
      'NO_Pla=NO_Pla'
      'OL_Cta=OL_Cta'
      'OL_SGr=OL_SGr'
      'OL_Gru=OL_Gru'
      'OL_Cjt=OL_Cjt'
      'OL_Pla=OL_Pla'
      'SdoAnt=SdoAnt'
      'Credito=Credito'
      'Debito=Debito'
      'Movim=Movim'
      'EX_Cta=EX_Cta'
      'EX_SGr=EX_SGr'
      'EX_Gru=EX_Gru'
      'EX_Cjt=EX_Cjt'
      'EX_Pla=EX_Pla'
      'ID_SEQ=ID_SEQ'
      'KGT=KGT'
      'Ativo=Ativo')
    DataSet = QrSaldosNiv
    BCDToCurrency = False
    Left = 332
    Top = 244
  end
  object QrSdoPar: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, '
      'SUM(Debito) Debito, SUM(Movim) Movim '
      'FROM saldosniv;')
    Left = 392
    Top = 244
    object QrSdoParSdoAnt: TFloatField
      FieldName = 'SdoAnt'
    end
    object QrSdoParCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrSdoParDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrSdoParMovim: TFloatField
      FieldName = 'Movim'
    end
  end
  object DsSdoPar: TDataSource
    DataSet = QrSdoPar
    Left = 452
    Top = 244
  end
  object QrNiv1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mov.Genero, mov.Subgrupo, mov.Grupo, '
      'mov.Conjunto, mov.Plano, SUM(mov.Movim) Movim,'
      'cta.Nome NO_Cta, cta.OrdemLista OR_Cta,'
      'sgr.Nome NO_Sgr, sgr.OrdemLista OR_SGr,'
      'gru.Nome NO_Gru, gru.OrdemLista OR_Gru,'
      'cjt.Nome NO_Cjt, cjt.OrdemLista OR_Cjt,'
      'pla.Nome NO_Pla, pla.OrdemLista OR_Pla'
      'FROM contasmov mov'
      'LEFT JOIN contas cta ON cta.Codigo=mov.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=mov.Subgrupo'
      'LEFT JOIN grupos gru ON gru.Codigo=mov.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=mov.Conjunto'
      'LEFT JOIN plano pla ON pla.Codigo=mov.Plano'
      'WHERE mov.CliInt=:P0'
      'AND mov.Genero IN '
      '('
      '  SELECT Genero   '
      '  FROM contasniv'
      '  WHERE Entidade=:P1'
      '  AND Nivel=1'
      ')'
      'AND mov.Mez <=:P2'
      'GROUP BY mov.Genero')
    Left = 392
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNiv1Genero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrNiv1Subgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrNiv1Grupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrNiv1Conjunto: TIntegerField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrNiv1Plano: TIntegerField
      FieldName = 'Plano'
      Required = True
    end
    object QrNiv1Movim: TFloatField
      FieldName = 'Movim'
    end
    object QrNiv1NO_Cta: TWideStringField
      FieldName = 'NO_Cta'
      Size = 50
    end
    object QrNiv1OR_Cta: TIntegerField
      FieldName = 'OR_Cta'
    end
    object QrNiv1NO_Sgr: TWideStringField
      FieldName = 'NO_Sgr'
      Size = 50
    end
    object QrNiv1OR_SGr: TIntegerField
      FieldName = 'OR_SGr'
    end
    object QrNiv1NO_Gru: TWideStringField
      FieldName = 'NO_Gru'
      Size = 50
    end
    object QrNiv1OR_Gru: TIntegerField
      FieldName = 'OR_Gru'
    end
    object QrNiv1NO_Cjt: TWideStringField
      FieldName = 'NO_Cjt'
      Size = 50
    end
    object QrNiv1OR_Cjt: TIntegerField
      FieldName = 'OR_Cjt'
    end
    object QrNiv1NO_Pla: TWideStringField
      FieldName = 'NO_Pla'
      Size = 50
    end
    object QrNiv1OR_Pla: TIntegerField
      FieldName = 'OR_Pla'
    end
  end
  object QrNiv2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT 0 Genero, mov.Subgrupo, mov.Grupo, '
      'mov.Conjunto, mov.Plano, SUM(mov.Movim) Movim,'
      
        'CONCAT("CONTAS N'#195'O CONTROLADAS DO SUBGRUPO ",sgr.Nome) NO_Cta, 9' +
        '99999999 OR_Cta,'
      'sgr.Nome NO_Sgr, sgr.OrdemLista OR_SGr,'
      'gru.Nome NO_Gru, gru.OrdemLista OR_Gru,'
      'cjt.Nome NO_Cjt, cjt.OrdemLista OR_Cjt,'
      'pla.Nome NO_Pla, pla.OrdemLista OR_Pla'
      'FROM contasmov mov'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=mov.Subgrupo'
      'LEFT JOIN grupos gru ON gru.Codigo=mov.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=mov.Conjunto'
      'LEFT JOIN plano pla ON pla.Codigo=mov.Plano'
      'WHERE mov.CliInt=:P0'
      'AND mov.Subgrupo IN '
      '('
      '  SELECT Genero   '
      '  FROM contasniv'
      '  WHERE Entidade=:P1'
      '  AND Nivel=2'
      ')'
      'AND mov.Mez <=:P2'
      'GROUP BY mov.Subgrupo')
    Left = 392
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNiv2Genero: TLargeintField
      FieldName = 'Genero'
      Required = True
    end
    object QrNiv2Subgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrNiv2Grupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrNiv2Conjunto: TIntegerField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrNiv2Plano: TIntegerField
      FieldName = 'Plano'
      Required = True
    end
    object QrNiv2Movim: TFloatField
      FieldName = 'Movim'
    end
    object QrNiv2NO_Cta: TWideStringField
      FieldName = 'NO_Cta'
      Size = 85
    end
    object QrNiv2OR_Cta: TLargeintField
      FieldName = 'OR_Cta'
      Required = True
    end
    object QrNiv2NO_Sgr: TWideStringField
      FieldName = 'NO_Sgr'
      Size = 50
    end
    object QrNiv2OR_SGr: TIntegerField
      FieldName = 'OR_SGr'
    end
    object QrNiv2NO_Gru: TWideStringField
      FieldName = 'NO_Gru'
      Size = 50
    end
    object QrNiv2OR_Gru: TIntegerField
      FieldName = 'OR_Gru'
    end
    object QrNiv2NO_Cjt: TWideStringField
      FieldName = 'NO_Cjt'
      Size = 50
    end
    object QrNiv2OR_Cjt: TIntegerField
      FieldName = 'OR_Cjt'
    end
    object QrNiv2NO_Pla: TWideStringField
      FieldName = 'NO_Pla'
      Size = 50
    end
    object QrNiv2OR_Pla: TIntegerField
      FieldName = 'OR_Pla'
    end
  end
  object QrNiv3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT 0 Genero, 0 Subgrupo, mov.Grupo, '
      'mov.Conjunto, mov.Plano, SUM(mov.Movim) Movim,'
      
        'CONCAT("CONTAS N'#195'O CONTROLADAS DO GRUPO ", gru.Nome) NO_Cta, 999' +
        '999999 OR_Cta,'
      
        'CONCAT("SUB-GRUPOS N'#195'O CONTROLADOS DO GRUPO ", gru.Nome) NO_Sgr,' +
        ' 999999999 OR_SGr,'
      'gru.Nome NO_Gru, gru.OrdemLista OR_Gru,'
      'cjt.Nome NO_Cjt, cjt.OrdemLista OR_Cjt,'
      'pla.Nome NO_Pla, pla.OrdemLista OR_Pla'
      'FROM contasmov mov'
      'LEFT JOIN grupos gru ON gru.Codigo=mov.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=mov.Conjunto'
      'LEFT JOIN plano pla ON pla.Codigo=mov.Plano'
      'WHERE mov.CliInt=:P0'
      'AND mov.Grupo IN '
      '('
      '  SELECT Genero   '
      '  FROM contasniv'
      '  WHERE Entidade=:P1'
      '  AND Nivel=3'
      ')'
      'AND mov.Mez <=:P2'
      'GROUP BY mov.Grupo')
    Left = 392
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNiv3Genero: TLargeintField
      FieldName = 'Genero'
      Required = True
    end
    object QrNiv3Subgrupo: TLargeintField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrNiv3Grupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrNiv3Conjunto: TIntegerField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrNiv3Plano: TIntegerField
      FieldName = 'Plano'
      Required = True
    end
    object QrNiv3Movim: TFloatField
      FieldName = 'Movim'
    end
    object QrNiv3NO_Cta: TWideStringField
      FieldName = 'NO_Cta'
      Size = 82
    end
    object QrNiv3OR_Cta: TLargeintField
      FieldName = 'OR_Cta'
      Required = True
    end
    object QrNiv3NO_Sgr: TWideStringField
      FieldName = 'NO_Sgr'
      Size = 86
    end
    object QrNiv3OR_SGr: TLargeintField
      FieldName = 'OR_SGr'
      Required = True
    end
    object QrNiv3NO_Gru: TWideStringField
      FieldName = 'NO_Gru'
      Size = 50
    end
    object QrNiv3OR_Gru: TIntegerField
      FieldName = 'OR_Gru'
    end
    object QrNiv3NO_Cjt: TWideStringField
      FieldName = 'NO_Cjt'
      Size = 50
    end
    object QrNiv3OR_Cjt: TIntegerField
      FieldName = 'OR_Cjt'
    end
    object QrNiv3NO_Pla: TWideStringField
      FieldName = 'NO_Pla'
      Size = 50
    end
    object QrNiv3OR_Pla: TIntegerField
      FieldName = 'OR_Pla'
    end
  end
  object QrNiv4: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT 0 Genero, 0 Subgrupo,0 Grupo, '
      'mov.Conjunto, mov.Plano, SUM(mov.Movim) Movim,'
      
        'CONCAT("CONTAS N'#195'O CONTROLADAS DO CONJUTO ", cjt.Nome) NO_Cta, 9' +
        '99999999 OR_Cta,'
      
        'CONCAT("SUB-GRUPOS N'#195'O CONTROLADOS DO CONJUNTO ", cjt.Nome) NO_S' +
        'gr, 999999999 OR_SGr,'
      
        'CONCAT("GRUPOS N'#195'O CONTROLADOS", cjt.Nome) NO_Gru, 999999999 OR_' +
        'Gru,'
      'cjt.Nome NO_Cjt, cjt.OrdemLista OR_Cjt,'
      'pla.Nome NO_Pla, pla.OrdemLista OR_Pla'
      'FROM contasmov mov'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=mov.Conjunto'
      'LEFT JOIN plano pla ON pla.Codigo=mov.Plano'
      'WHERE mov.CliInt=:P0'
      'AND mov.Conjunto IN '
      '('
      '  SELECT Genero   '
      '  FROM contasniv'
      '  WHERE Entidade=:P1'
      '  AND Nivel=4'
      ')'
      'AND mov.Mez <=:P2'
      'GROUP BY mov.Conjunto')
    Left = 392
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNiv4Genero: TLargeintField
      FieldName = 'Genero'
      Required = True
    end
    object QrNiv4Subgrupo: TLargeintField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrNiv4Grupo: TLargeintField
      FieldName = 'Grupo'
      Required = True
    end
    object QrNiv4Conjunto: TIntegerField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrNiv4Plano: TIntegerField
      FieldName = 'Plano'
      Required = True
    end
    object QrNiv4Movim: TFloatField
      FieldName = 'Movim'
    end
    object QrNiv4NO_Cta: TWideStringField
      FieldName = 'NO_Cta'
      Size = 84
    end
    object QrNiv4OR_Cta: TLargeintField
      FieldName = 'OR_Cta'
      Required = True
    end
    object QrNiv4NO_Sgr: TWideStringField
      FieldName = 'NO_Sgr'
      Size = 89
    end
    object QrNiv4OR_SGr: TLargeintField
      FieldName = 'OR_SGr'
      Required = True
    end
    object QrNiv4NO_Gru: TWideStringField
      FieldName = 'NO_Gru'
      Size = 72
    end
    object QrNiv4OR_Gru: TLargeintField
      FieldName = 'OR_Gru'
      Required = True
    end
    object QrNiv4NO_Cjt: TWideStringField
      FieldName = 'NO_Cjt'
      Size = 50
    end
    object QrNiv4OR_Cjt: TIntegerField
      FieldName = 'OR_Cjt'
    end
    object QrNiv4NO_Pla: TWideStringField
      FieldName = 'NO_Pla'
      Size = 50
    end
    object QrNiv4OR_Pla: TIntegerField
      FieldName = 'OR_Pla'
    end
  end
  object QrNiv5: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT 0 Genero, 0 Subgrupo, 0 Grupo, '
      '0 Conjunto, mov.Plano, SUM(mov.Movim) Movim,'
      
        'CONCAT("CONTAS N'#195'O CONTROLADAS DO PLANO", pla.Nome) NO_Cta, 9999' +
        '99999 OR_Cta,'
      
        'CONCAT("SUB-GRUPOS N'#195'O CONTROLADOS DO PLANO", pla.Nome) NO_Sgr, ' +
        '999999999 OR_SGr,'
      
        'CONCAT("GRUPOS N'#195'O CONTROLADOS DO PLANO", pla.Nome) NO_Gru, 9999' +
        '99999 OR_Gru,'
      
        'CONCAT("CONJUNTOS N'#195'O CONTROLADOS DO PLANO", pla.Nome) NO_Cjt, 9' +
        '99999999 OR_Cjt,'
      'pla.Nome NO_Pla, pla.OrdemLista OR_Pla'
      'FROM contasmov mov'
      'LEFT JOIN plano pla ON pla.Codigo=mov.Plano'
      'WHERE mov.CliInt=:P0'
      'AND mov.Plano IN '
      '('
      '  SELECT Genero   '
      '  FROM contasniv'
      '  WHERE Entidade=:P1'
      '  AND Nivel=5'
      ')'
      'AND mov.Mez <=:P2'
      'GROUP BY mov.Plano')
    Left = 392
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNiv5Genero: TLargeintField
      FieldName = 'Genero'
      Required = True
    end
    object QrNiv5Subgrupo: TLargeintField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrNiv5Grupo: TLargeintField
      FieldName = 'Grupo'
      Required = True
    end
    object QrNiv5Conjunto: TLargeintField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrNiv5Plano: TIntegerField
      FieldName = 'Plano'
      Required = True
    end
    object QrNiv5Movim: TFloatField
      FieldName = 'Movim'
    end
    object QrNiv5NO_Cta: TWideStringField
      FieldName = 'NO_Cta'
      Size = 81
    end
    object QrNiv5OR_Cta: TLargeintField
      FieldName = 'OR_Cta'
      Required = True
    end
    object QrNiv5NO_Sgr: TWideStringField
      FieldName = 'NO_Sgr'
      Size = 85
    end
    object QrNiv5OR_SGr: TLargeintField
      FieldName = 'OR_SGr'
      Required = True
    end
    object QrNiv5NO_Gru: TWideStringField
      FieldName = 'NO_Gru'
      Size = 81
    end
    object QrNiv5OR_Gru: TLargeintField
      FieldName = 'OR_Gru'
      Required = True
    end
    object QrNiv5NO_Cjt: TWideStringField
      FieldName = 'NO_Cjt'
      Size = 84
    end
    object QrNiv5OR_Cjt: TLargeintField
      FieldName = 'OR_Cjt'
      Required = True
    end
    object QrNiv5NO_Pla: TWideStringField
      FieldName = 'NO_Pla'
      Size = 50
    end
    object QrNiv5OR_Pla: TIntegerField
      FieldName = 'OR_Pla'
    end
  end
  object QrNivMov: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mov.Credito, mov.Debito'
      'FROM contasmov mov'
      'WHERE mov.CliInt=-11'
      'AND mov.Mez=905')
    Left = 332
    Top = 196
    object QrNivMovCredito: TFloatField
      FieldName = 'Credito'
      Required = True
    end
    object QrNivMovDebito: TFloatField
      FieldName = 'Debito'
      Required = True
    end
  end
  object QrResumo: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrResumoCalcFields
    SQL.Strings = (
      'SELECT SUM(lct.Credito) Credito, -SUM(lct.Debito) Debito'
      'FROM lct0001a lct'
      'LEFT JOIN carteiras car ON car.Codigo=lct.Carteira'
      'WHERE lct.Tipo <> 2'
      'AND lct.Genero > 0'
      'AND lct.Sit IN (:P0)'
      'AND car.ForneceI=:P1'
      'AND lct.Data BETWEEN :P2 AND :P3')
    Left = 212
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrResumoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrResumoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrResumoSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      Calculated = True
    end
    object QrResumoFINAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FINAL'
      Calculated = True
    end
  end
  object frxDsResumo: TfrxDBDataset
    UserName = 'frxDsResumo'
    CloseDataSource = False
    DataSet = QrResumo
    BCDToCurrency = False
    Left = 272
    Top = 388
  end
  object QrSaldoA: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSaldoACalcFields
    SQL.Strings = (
      'SELECT SUM(car.SdoFimB) Inicial,'
      '('
      '  SELECT SUM(lct.Credito-lct.Debito)'
      '  FROM lct0001a lct'
      '  LEFT JOIN carteiras crt ON crt.Codigo=lct.Carteira'
      '  WHERE crt.Tipo <> 2'
      '  AND crt.ForneceI=:P0'
      '  AND lct.Data < :P1'
      ') SALDO'
      'FROM carteiras car'
      'WHERE car.ForneceI=:P3'
      'AND car.Tipo <> 2')
    Left = 332
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSaldoAInicial: TFloatField
      FieldName = 'Inicial'
    end
    object QrSaldoASALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrSaldoATOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL'
      Calculated = True
    end
  end
  object frxDsSaldoA: TfrxDBDataset
    UserName = 'frxDsSaldoA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Inicial=Inicial'
      'SALDO=SALDO'
      'TOTAL=TOTAL')
    DataSet = QrSaldoA
    BCDToCurrency = False
    Left = 392
    Top = 388
  end
  object QrDebitos: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDebitosCalcFields
    Left = 332
    Top = 292
    object QrDebitosCOMPENSADO_TXT: TWideStringField
      FieldName = 'COMPENSADO_TXT'
      Size = 8
    end
    object QrDebitosDATA: TWideStringField
      FieldName = 'DATA'
      Size = 8
    end
    object QrDebitosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDebitosDEBITO: TFloatField
      FieldName = 'DEBITO'
    end
    object QrDebitosNOTAFISCAL: TLargeintField
      FieldName = 'NOTAFISCAL'
    end
    object QrDebitosSERIECH: TWideStringField
      FieldName = 'SERIECH'
      Size = 10
    end
    object QrDebitosDOCUMENTO: TFloatField
      FieldName = 'DOCUMENTO'
    end
    object QrDebitosMEZ: TLargeintField
      FieldName = 'MEZ'
      Required = True
    end
    object QrDebitosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrDebitosNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrDebitosNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrDebitosNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrDebitosITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrDebitosMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 7
      Calculated = True
    end
    object QrDebitosSERIE_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_DOC'
      Size = 30
      Calculated = True
    end
    object QrDebitosNF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NF_TXT'
      Size = 30
      Calculated = True
    end
    object QrDebitosMES2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES2'
      Size = 5
      Calculated = True
    end
    object QrDebitosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDebitosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrDebitosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDebitosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrDebitosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrDebitosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrDebitosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrDebitosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
  end
  object DsDebitos: TDataSource
    DataSet = QrDebitos
    Left = 392
    Top = 292
  end
  object frxDsDebitos: TfrxDBDataset
    UserName = 'frxDsDebitos'
    CloseDataSource = False
    DataSet = QrDebitos
    BCDToCurrency = False
    Left = 452
    Top = 292
  end
  object frxDsCreditos: TfrxDBDataset
    UserName = 'frxDsCreditos'
    CloseDataSource = False
    DataSet = QrCreditos
    BCDToCurrency = False
    Left = 452
    Top = 340
  end
  object DsCreditos: TDataSource
    DataSet = QrCreditos
    Left = 392
    Top = 340
  end
  object QrCreditos: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCreditosCalcFields
    SQL.Strings = (
      'SELECT lct.Mez, SUM(lct.Credito) Credito, '
      'lct.Controle, lct.Sub, lct.Carteira, lct.Cartao, lct.Tipo,'
      'lct.Vencimento, lct.Compensado, lct.Sit, lct.Genero, '
      'lct.SubPgto1, '
      'con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU'
      'FROM syndic.lct0001a lct'
      'LEFT JOIN carteiras car ON car.Codigo=lct.Carteira'
      'LEFT JOIN contas    con ON con.Codigo=lct.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'WHERE lct.Tipo <> 2'
      'AND lct.Credito > 0'
      'AND lct.Genero>0'
      'AND lct.Sit IN (0,1,2,3)'
      'AND car.ForneceI = :P0'
      'AND lct.Data BETWEEN :P1 AND :P2'
      'GROUP BY lct.Genero '
      ', lct.Mez'
      ', lct.SubPgto1'
      'ORDER BY gru.OrdemLista, NOMEGRU, sgr.OrdemLista,'
      'NOMESGR, con.OrdemLista, NOMECON, Mez, Data')
    Left = 332
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCreditosMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrCreditosCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrCreditosNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrCreditosNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrCreditosNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrCreditosMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 7
      Calculated = True
    end
    object QrCreditosNOMECON_2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECON_2'
      Size = 255
      Calculated = True
    end
    object QrCreditosSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrCreditosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCreditosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrCreditosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrCreditosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrCreditosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrCreditosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrCreditosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrCreditosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCreditosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
  end
  object QrRealizado: TMySQLQuery
    Database = Dmod.MyDB
    Left = 836
    Top = 124
    object QrRealizadoValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCIni: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.Codigo, MIN(ci.Periodo) Minimo, co.Nome,'
      'MAX(ci.Periodo) Maximo, co.PendenMesSeg, co.CalculMesSeg  '
      'FROM contas co'
      'LEFT JOIN contasits ci ON ci.Codigo=co.Codigo'
      'WHERE ci.Codigo IS NOT NULL'
      'GROUP BY co.Codigo')
    Left = 808
    Top = 124
    object QrCIniCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCIniMinimo: TIntegerField
      FieldName = 'Minimo'
    end
    object QrCIniMaximo: TIntegerField
      FieldName = 'Maximo'
    end
    object QrCIniPendenMesSeg: TSmallintField
      FieldName = 'PendenMesSeg'
      Required = True
    end
    object QrCIniCalculMesSeg: TSmallintField
      FieldName = 'CalculMesSeg'
      Required = True
    end
    object QrCIniNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object QrCIts1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT co.Codigo, co.Nome,'
      'ci.Periodo, ci.Tipo, ci.Fator'
      'FROM contas co'
      'LEFT JOIN contasits ci ON ci.Codigo=co.Codigo'
      'WHERE co.Codigo = :P0'
      'AND ci.Periodo = :P1')
    Left = 808
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCIts1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCIts1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrCIts1Periodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrCIts1Tipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCIts1Fator: TFloatField
      FieldName = 'Fator'
      Required = True
    end
  end
  object QrCIts2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT co.Codigo, co.Nome,'
      'ci.Periodo, ci.Tipo, ci.Fator '
      'FROM contas co'
      'LEFT JOIN contasits ci ON ci.Codigo=co.Codigo'
      'WHERE co.Codigo = :P0'
      'AND ci.Periodo = :P1')
    Left = 836
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCIts2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCIts2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrCIts2Periodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrCIts2Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCIts2Fator: TFloatField
      FieldName = 'Fator'
    end
  end
  object QrItsCtas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Conta '
      'FROM contasitsctas'
      'WHERE Codigo=:P0'
      'AND Periodo=:P1')
    Left = 516
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrItsCtasConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object QrValFator: TMySQLQuery
    Database = Dmod.MyDB
    Left = 576
    Top = 148
    object QrValFatorValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCtasResMes2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ctasresmes')
    Left = 756
    Top = 4
    object QrCtasResMes2SeqImp: TIntegerField
      FieldName = 'SeqImp'
    end
    object QrCtasResMes2Conta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCtasResMes2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCtasResMes2Periodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrCtasResMes2Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCtasResMes2Fator: TFloatField
      FieldName = 'Fator'
    end
    object QrCtasResMes2ValFator: TFloatField
      FieldName = 'ValFator'
    end
    object QrCtasResMes2Devido: TFloatField
      FieldName = 'Devido'
    end
    object QrCtasResMes2Pago: TFloatField
      FieldName = 'Pago'
    end
    object QrCtasResMes2Diferenca: TFloatField
      FieldName = 'Diferenca'
    end
    object QrCtasResMes2Acumulado: TFloatField
      FieldName = 'Acumulado'
    end
  end
  object QrCtasAnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta, Nome, SUM(Devido+Pago) Acumulado'
      'FROM ctasresmes'
      'WHERE Periodo<:P0'
      'GROUP BY Conta')
    Left = 816
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCtasAntConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCtasAntAcumulado: TFloatField
      FieldName = 'Acumulado'
    end
    object QrCtasAntNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrPrevItO: TMySQLQuery
    Database = Dmod.MyDB
    Left = 272
    Top = 436
    object QrPrevItOCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrevItOControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPrevItOConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrPrevItOValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrPrevItOPrevBaI: TIntegerField
      FieldName = 'PrevBaI'
      Required = True
    end
    object QrPrevItOTexto: TWideStringField
      FieldName = 'Texto'
      Required = True
      Size = 50
    end
    object QrPrevItOLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPrevItODataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPrevItODataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPrevItOUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPrevItOUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPrevItOPrevBaC: TIntegerField
      FieldName = 'PrevBaC'
      Required = True
    end
    object QrPrevItOEmitVal: TFloatField
      FieldName = 'EmitVal'
      Required = True
    end
    object QrPrevItOEmitSit: TIntegerField
      FieldName = 'EmitSit'
      Required = True
    end
    object QrPrevItOSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
    end
    object QrPrevItONOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrPrevItONOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
  end
  object QrLocPerX: TMySQLQuery
    Database = Dmod.MyDB
    Left = 212
    Top = 436
    object QrLocPerXCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSuBloq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 332
    Top = 484
    object QrSuBloqMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrSuBloqMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrSuBloqNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrSuBloqORIGINAL: TFloatField
      FieldName = 'ORIGINAL'
    end
    object QrSuBloqKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrSuBloqPAGO: TFloatField
      FieldName = 'PAGO'
    end
  end
  object QrPgBloq: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPgBloqCalcFields
    Left = 212
    Top = 484
    object QrPgBloqCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPgBloqMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrPgBloqMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrPgBloqNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrPgBloqUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrPgBloqNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrPgBloqMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPgBloqVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPgBloqDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPgBloqORIGINAL: TFloatField
      FieldName = 'ORIGINAL'
    end
    object QrPgBloqData: TDateField
      FieldName = 'Data'
    end
    object QrPgBloqMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
    object QrPgBloqDATA_TXT: TWideStringField
      FieldName = 'DATA_TXT'
      Size = 10
    end
  end
  object frxDsTotalSaldo: TfrxDBDataset
    UserName = 'frxDsTotalSaldo'
    CloseDataSource = False
    DataSet = QrTotalSaldo
    BCDToCurrency = False
    Left = 708
    Top = 452
  end
  object frxSaldos: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.439335567100000000
    ReportOptions.LastChange = 39720.439335567100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxSaldosGetValue
    Left = 708
    Top = 404
    Datasets = <
      item
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsTotalSaldo
        DataSetName = 'frxDsTotalSaldo'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 219.212740000000000000
        Width = 702.614627000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsTotalSaldo
        DataSetName = 'frxDsTotalSaldo'
        RowCount = 0
        object Memo14: TfrxMemoView
          Left = 120.000000000000000000
          Width = 416.756030000000000000
          Height = 18.000000000000000000
          DataField = 'Nome'
          DataSet = frxDsTotalSaldo
          DataSetName = 'frxDsTotalSaldo'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTotalSaldo."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 536.692918270000000000
          Width = 105.826771650000000000
          Height = 18.000000000000000000
          DataSet = frxDsTotalSaldo
          DataSetName = 'frxDsTotalSaldo'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTotalSaldo."Saldo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 98.267716540000000000
        Top = 18.897650000000000000
        Width = 702.614627000000000000
        object Memo28: TfrxMemoView
          Left = 120.000000000000000000
          Top = 79.370078740157480000
          Width = 416.756030000000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 536.692913390000000000
          Top = 79.370078740157480000
          Width = 105.826771650000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Top = 79.370078740157480000
          Width = 116.000000000000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Status')
          ParentFont = False
        end
        object Memo211: TfrxMemoView
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'SALDOS FUTUROS DAS CARTEIRAS EXTRATO E CAIXA SEM AS EMISS'#213'ES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 38.000000000000000000
        Top = 355.275820000000000000
        Width = 702.614627000000000000
        object Memo11: TfrxMemoView
          Left = 423.307086610000000000
          Top = 7.559057559999985000
          Width = 104.000000000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Total Geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 536.692913389999900000
          Top = 7.559057559999985000
          Width = 105.826771650000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsTotalSaldo."Saldo">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 5.385589999999980000
          Width = 716.440940000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 177.637910000000000000
        Width = 702.614627000000000000
        Condition = 'frxDsTotalSaldo."Tipo"'
        object Memo13: TfrxMemoView
          Left = 4.000000000000000000
          Width = 332.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsTotalSaldo."NOMETIPO"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 34.000000000000000000
        Top = 260.787570000000000000
        Width = 702.614627000000000000
        object Memo1: TfrxMemoView
          Left = 536.692913389999900000
          Top = 3.779527560000020000
          Width = 105.826771650000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsTotalSaldo."Saldo">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 423.307086614173300000
          Top = 3.779527560000020000
          Width = 104.000000000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 415.748300000000000000
        Width = 702.614627000000000000
        object Memo120: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object QrSaldos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Saldo, Tipo'
      'FROM Carteiras'
      'WHERE Tipo <> 2'
      'AND ForneceI=:P0')
    Left = 704
    Top = 356
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSaldosNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 8
    end
    object QrSaldosSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'DBMMONEY.carteiras.Saldo'
    end
    object QrSaldosTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSaldosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrConsigna: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Descricao Nome, (Debito-Credito) Saldo'
      'FROM Consignacoes'
      'WHERE CliInt=:P0')
    Left = 704
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConsignaNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.consignacoes.Descricao'
      Size = 128
    end
    object QrConsignaSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'DBMMONEY.consignacoes.Credito'
    end
  end
  object QrTotalSaldo: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTotalSaldoCalcFields
    SQL.Strings = (
      'SELECT * FROM Saldos')
    Left = 704
    Top = 260
    object QrTotalSaldoNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMLOCAL.saldos.Nome'
      Size = 128
    end
    object QrTotalSaldoSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'DBMLOCAL.saldos.Saldo'
    end
    object QrTotalSaldoTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'DBMLOCAL.saldos.Tipo'
    end
    object QrTotalSaldoNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 30
      Calculated = True
    end
  end
  object QrDuplCH: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDuplCHCalcFields
    Left = 32
    Top = 196
    object QrDuplCHData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplCHControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplCHDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDuplCHCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplCHDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplCHNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplCHCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplCHMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrDuplCHFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrDuplCHCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDuplCHNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrDuplCHNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrDuplCHNOMEFNC: TWideStringField
      FieldName = 'NOMEFNC'
      Size = 100
    end
    object QrDuplCHDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrDuplCHSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrDuplCHCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDuplCHTERCEIRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TERCEIRO'
      Size = 255
      Calculated = True
    end
    object QrDuplCHMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
    object QrDuplCHCHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CHEQUE'
      Calculated = True
    end
    object QrDuplCHCOMP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMP_TXT'
      Size = 10
      Calculated = True
    end
  end
  object DsDuplCH: TDataSource
    DataSet = QrDuplCH
    Left = 92
    Top = 196
  end
  object QrDuplNF: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDuplNFCalcFields
    Left = 32
    Top = 244
    object QrDuplNFData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplNFControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplNFDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDuplNFCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplNFDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplNFNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplNFCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplNFMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrDuplNFFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrDuplNFCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDuplNFNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrDuplNFNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrDuplNFNOMEFNC: TWideStringField
      FieldName = 'NOMEFNC'
      Size = 100
    end
    object QrDuplNFDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrDuplNFSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrDuplNFCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDuplNFTERCEIRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TERCEIRO'
      Size = 255
      Calculated = True
    end
    object QrDuplNFMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
    object QrDuplNFCHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CHEQUE'
      Calculated = True
    end
    object QrDuplNFCOMP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMP_TXT'
      Size = 10
      Calculated = True
    end
  end
  object DsDuplNF: TDataSource
    DataSet = QrDuplNF
    Left = 92
    Top = 244
  end
  object QrDuplVal: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDuplValCalcFields
    Left = 212
    Top = 196
    object QrDuplValData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplValControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplValDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDuplValCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplValDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplValNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplValCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplValMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrDuplValFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrDuplValCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDuplValNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrDuplValNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrDuplValNOMEFNC: TWideStringField
      FieldName = 'NOMEFNC'
      Size = 100
    end
    object QrDuplValDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrDuplValSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrDuplValCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDuplValTERCEIRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TERCEIRO'
      Size = 255
      Calculated = True
    end
    object QrDuplValMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
    object QrDuplValCHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CHEQUE'
      Calculated = True
    end
    object QrDuplValCOMP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMP_TXT'
      Size = 10
      Calculated = True
    end
  end
  object DsDuplVal: TDataSource
    DataSet = QrDuplVal
    Left = 272
    Top = 196
  end
  object QrSdoCtSdo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(SdoIni) SdoIni'
      'FROM contassdo'
      'WHERE Entidade=:P0'
      '')
    Left = 332
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSdoCtSdoSdoIni: TFloatField
      FieldName = 'SdoIni'
    end
  end
  object QrSdoCarts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Inicial) Inicial '
      'FROM carteiras'
      'WHERE ForneceI=:P0')
    Left = 332
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSdoCartsInicial: TFloatField
      FieldName = 'Inicial'
    end
  end
  object QrSdo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.Codigo,  cta.Nome NOMECTA, sdo.SdoIni'
      'FROM contas cta'
      'LEFT JOIN contassdo sdo ON cta.Codigo=sdo.Codigo '
      'WHERE cta.Codigo<>0'
      'AND sdo.Entidade=:P0'
      'AND sdo.SdoIni <> 0'
      'ORDER BY cta.Nome')
    Left = 88
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSdoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSdoNOMECTA: TWideStringField
      FieldName = 'NOMECTA'
      Size = 50
    end
    object QrSdoSdoIni: TFloatField
      FieldName = 'SdoIni'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object QrCrt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Inicial '
      'FROM carteiras'
      'WHERE ForneceI=:P0'
      'AND Inicial <> 0')
    Left = 32
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCrtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCrtNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCrtInicial: TFloatField
      FieldName = 'Inicial'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrCrtDIFERENCA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIFERENCA'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Calculated = True
    end
  end
  object QrECI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodCliInt, TipoTabLct '
      'FROM enticliint'
      'WHERE CodEnti=:P0')
    Left = 156
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrECITipoTabLct: TSmallintField
      FieldName = 'TipoTabLct'
    end
    object QrECICodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
  end
  object QrPendG: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPendGCalcFields
    Left = 272
    Top = 100
    object QrPendGCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPendGFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrPendGMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPendGCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPendGNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrPendGUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrPendGNOMEMEZ: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMEZ'
      Size = 30
      Calculated = True
    end
    object QrPendGVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPendGFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrRep: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrRepAfterScroll
    SQL.Strings = (
      'SELECT bpa.*, cim.Unidade,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP '
      'FROM bloqparc bpa'
      'LEFT JOIN entidades ent ON ent.Codigo=bpa.CodigoEnt'
      'LEFT JOIN condimov  cim ON cim.Conta=bpa.CodigoEsp'
      'WHERE bpa.CodCliEnt=:P0'
      'AND bpa.DataP BETWEEN :P1 AND :P2 '
      'ORDER BY bpa.DataP, cim.Unidade')
    Left = 512
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrRepCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrReploginID: TWideStringField
      FieldName = 'loginID'
      Required = True
      Size = 32
    end
    object QrRepautentica: TWideMemoField
      FieldName = 'autentica'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrRepCodCliEsp: TIntegerField
      FieldName = 'CodCliEsp'
      Required = True
    end
    object QrRepCodCliEnt: TIntegerField
      FieldName = 'CodCliEnt'
      Required = True
    end
    object QrRepCodigoEnt: TIntegerField
      FieldName = 'CodigoEnt'
      Required = True
    end
    object QrRepCodigoEsp: TIntegerField
      FieldName = 'CodigoEsp'
      Required = True
    end
    object QrRepVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
      Required = True
    end
    object QrRepVlrMulta: TFloatField
      FieldName = 'VlrMulta'
      Required = True
    end
    object QrRepVlrJuros: TFloatField
      FieldName = 'VlrJuros'
      Required = True
    end
    object QrRepVlrTotal: TFloatField
      FieldName = 'VlrTotal'
      Required = True
    end
    object QrRepDataP: TDateTimeField
      FieldName = 'DataP'
      Required = True
    end
    object QrRepNovo: TSmallintField
      FieldName = 'Novo'
      Required = True
    end
    object QrRepTxaJur: TFloatField
      FieldName = 'TxaJur'
      Required = True
    end
    object QrRepTxaMul: TFloatField
      FieldName = 'TxaMul'
      Required = True
    end
    object QrRepLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrRepDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrRepDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrRepUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrRepUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrRepAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrRepAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrRepUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrRepNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
  end
  object QrRepori: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT BloqOrigi, SUM(VlrOrigi) VlrOrigi, '
      'SUM(VlrMulta) VlrMulta, SUM(VlrJuros+VlrAjust) VlrJuros,'
      'SUM(VlrTotal) VlrTotal'
      'FROM bloqparcits'
      'WHERE Codigo=:P0'
      ''
      'GROUP BY BloqOrigi')
    Left = 452
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrReporiBloqOrigi: TIntegerField
      FieldName = 'BloqOrigi'
    end
    object QrReporiVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
    end
    object QrReporiVlrMulta: TFloatField
      FieldName = 'VlrMulta'
    end
    object QrReporiVlrJuros: TFloatField
      FieldName = 'VlrJuros'
    end
    object QrReporiVlrTotal: TFloatField
      FieldName = 'VlrTotal'
    end
  end
  object QrRepBPP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM bloqparcpar'
      'WHERE Codigo=:P0'
      'ORDER BY Vencimento')
    Left = 512
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRepBPPCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'bloqparcpar.Codigo'
    end
    object QrRepBPPControle: TAutoIncField
      FieldName = 'Controle'
      Origin = 'bloqparcpar.Controle'
    end
    object QrRepBPPParcela: TIntegerField
      FieldName = 'Parcela'
      Origin = 'bloqparcpar.Parcela'
    end
    object QrRepBPPVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
      Origin = 'bloqparcpar.VlrOrigi'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepBPPVlrMulta: TFloatField
      FieldName = 'VlrMulta'
      Origin = 'bloqparcpar.VlrMulta'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepBPPVlrJuros: TFloatField
      FieldName = 'VlrJuros'
      Origin = 'bloqparcpar.VlrJuros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepBPPLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'bloqparcpar.Lk'
    end
    object QrRepBPPDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'bloqparcpar.DataCad'
    end
    object QrRepBPPDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'bloqparcpar.DataAlt'
    end
    object QrRepBPPUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'bloqparcpar.UserCad'
    end
    object QrRepBPPUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'bloqparcpar.UserAlt'
    end
    object QrRepBPPAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'bloqparcpar.AlterWeb'
    end
    object QrRepBPPLancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'bloqparcpar.Lancto'
    end
    object QrRepBPPValBol: TFloatField
      FieldName = 'ValBol'
      Origin = 'bloqparcpar.ValBol'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepBPPVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'bloqparcpar.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRepBPPVlrTotal: TFloatField
      FieldName = 'VlrTotal'
      Origin = 'bloqparcpar.VlrTotal'
    end
    object QrRepBPPAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'bloqparcpar.Ativo'
    end
    object QrRepBPPFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'bloqparcpar.FatNum'
    end
  end
  object QrReplan: TMySQLQuery
    Database = Dmod.MyDB
    Left = 512
    Top = 436
    object QrReplanCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrReplanVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrReplanVLRORIG: TFloatField
      FieldKind = fkLookup
      FieldName = 'VLRORIG'
      LookupDataSet = QrRepori
      LookupKeyFields = 'BloqOrigi'
      LookupResultField = 'VlrOrigi'
      KeyFields = 'FatNum'
      Lookup = True
    end
    object QrReplanVLRMULTA: TFloatField
      FieldKind = fkLookup
      FieldName = 'VLRMULTA'
      LookupDataSet = QrRepori
      LookupKeyFields = 'BloqOrigi'
      LookupResultField = 'VlrMulta'
      KeyFields = 'FatNum'
      Lookup = True
    end
    object QrReplanVLRJUROS: TFloatField
      FieldKind = fkLookup
      FieldName = 'VLRJUROS'
      LookupDataSet = QrRepori
      LookupKeyFields = 'BloqOrigi'
      LookupResultField = 'VlrJuros'
      KeyFields = 'FatNum'
      Lookup = True
    end
    object QrReplanVLRTOTAL: TFloatField
      FieldKind = fkLookup
      FieldName = 'VLRTOTAL'
      LookupDataSet = QrRepori
      LookupKeyFields = 'BloqOrigi'
      LookupResultField = 'VlrTotal'
      KeyFields = 'FatNum'
      Lookup = True
    end
    object QrReplanReparcel: TIntegerField
      FieldName = 'Reparcel'
    end
    object QrReplanFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrRepAbe: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRepAbeCalcFields
    Left = 512
    Top = 484
    object QrRepAbeUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrRepAbeData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrRepAbeVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrRepAbeControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRepAbeDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrRepAbeCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrRepAbeCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrRepAbeMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrRepAbeMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrRepAbeDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrRepAbeSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrRepAbePago: TFloatField
      FieldName = 'Pago'
    end
    object QrRepAbePagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrRepAbePagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrRepAbeNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrRepAbeAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrRepAbeCompensado_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Compensado_TXT'
      Size = 8
      Calculated = True
    end
    object QrRepAbeFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrPendAll: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lan.Credito) Credito,'
      'imv.Unidade, "Quota condominial" TIPO, 0 KGT,'
      'COUNT(DISTINCT lan.FatNum) BLOQUETOS'
      'FROM ?.lct0001a lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN condimov  imv ON imv.Conta=lan.Depto'
      'WHERE car.Tipo=2'
      'AND lan.Reparcel=0'
      'AND car.ForneceI=1'
      'AND imv.Codigo=1'
      'AND lan.Depto>0'
      'AND lan.Sit<2'
      'AND lan.Vencimento <= "2012-11-30"'
      'GROUP BY imv.Unidade'
      ''
      'UNION'
      ''
      'SELECT SUM(lan.Credito) Credito,'
      'imv.Unidade, "Reparcelamento" TIPO, 0 KGT,'
      'COUNT(DISTINCT lan.FatNum) BLOQUETOS'
      'FROM syndic.lct0001a lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN condimov  imv ON imv.Conta=lan.Depto'
      'WHERE lan.Atrelado>0'
      'AND lan.Genero=-10'
      'AND car.Tipo=2'
      'AND lan.Sit < 2'
      'AND car.ForneceI=1'
      'AND lan.Vencimento <= "2012-11-30"'
      'GROUP BY imv.Unidade'
      ''
      'ORDER BY Unidade')
    Left = 512
    Top = 244
    object QrPendAllUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrPendAllCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPendAllTIPO: TWideStringField
      FieldName = 'TIPO'
      Required = True
      Size = 17
    end
    object QrPendAllKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrPendAllBLOQUETOS: TLargeintField
      FieldName = 'BLOQUETOS'
      Required = True
    end
  end
  object QrPendSum: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lan.Credito) Credito'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      '/*LEFT JOIN condimov  imv ON imv.Conta=lan.Depto*/'
      'WHERE (car.Tipo=2'
      'AND lan.Reparcel=0'
      'AND car.ForneceI=:P0'
      '/*AND imv.Codigo=:P1*/'
      'AND lan.Depto>0'
      'AND lan.Sit<2'
      'AND lan.Vencimento <=:P2)'
      ''
      'OR'
      ''
      '(lan.Atrelado>0'
      'AND lan.Genero=-10'
      'AND car.Tipo=2'
      'AND car.ForneceI=:P3'
      'AND lan.Vencimento <=:P4'
      'AND lan.Sit < 2'#13
      ')'#10)
    Left = 512
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1*/'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrPendSumCredito: TFloatField
      FieldName = 'Credito'
    end
  end
  object frxDsPendAll: TfrxDBDataset
    UserName = 'frxDsPendAll'
    CloseDataSource = False
    DataSet = QrPendAll
    BCDToCurrency = False
    Left = 572
    Top = 244
  end
  object frxDsPendSum: TfrxDBDataset
    UserName = 'frxDsPendSum'
    CloseDataSource = False
    DataSet = QrPendSum
    BCDToCurrency = False
    Left = 572
    Top = 292
  end
  object QrLcts: TMySQLQuery
    Database = Dmod.MyDB
    Left = 840
    Top = 308
    object QrLctsData: TDateField
      FieldName = 'Data'
    end
    object QrLctsTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctsCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctsSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLctsFatID: TIntegerField
      FieldName = 'FatID'
    end
  end
  object QrDelLct: TMySQLQuery
    Database = Dmod.MyDB
    Left = 780
    Top = 308
  end
  object Tb_Pla_Ctas: TMySQLTable
    Database = Dmod.MyDB
    TableName = '_pla_ctas_'
    Left = 960
    Top = 4
    object Tb_Pla_CtasNivel: TSmallintField
      FieldName = 'Nivel'
    end
    object Tb_Pla_CtascN5: TIntegerField
      FieldName = 'cN5'
    end
    object Tb_Pla_CtasnN5: TWideStringField
      FieldName = 'nN5'
      Size = 50
    end
    object Tb_Pla_CtascN4: TIntegerField
      FieldName = 'cN4'
    end
    object Tb_Pla_CtasnN4: TWideStringField
      FieldName = 'nN4'
      Size = 50
    end
    object Tb_Pla_CtascN3: TIntegerField
      FieldName = 'cN3'
    end
    object Tb_Pla_CtasnN3: TWideStringField
      FieldName = 'nN3'
      Size = 50
    end
    object Tb_Pla_CtascN2: TIntegerField
      FieldName = 'cN2'
    end
    object Tb_Pla_CtasnN2: TWideStringField
      FieldName = 'nN2'
      Size = 50
    end
    object Tb_Pla_CtascN1: TIntegerField
      FieldName = 'cN1'
    end
    object Tb_Pla_CtasnN1: TWideStringField
      FieldName = 'nN1'
      Size = 50
    end
    object Tb_Pla_CtasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object Ds_Pla_Ctas: TDataSource
    DataSet = Tb_Pla_Ctas
    Left = 960
    Top = 56
  end
  object QrFats3: TMySQLQuery
    Database = Dmod.MyDB
    Left = 780
    Top = 356
    object QrFats3Data: TDateField
      FieldName = 'Data'
    end
    object QrFats3Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrFats3Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrFats3Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFats3Sub: TSmallintField
      FieldName = 'Sub'
    end
  end
  object QrFats2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 780
    Top = 404
    object QrFats2Data: TDateField
      FieldName = 'Data'
    end
    object QrFats2Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrFats2Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrFats2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFats2Sub: TSmallintField
      FieldName = 'Sub'
    end
  end
  object QrFats1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Tipo, Carteira, Controle, Sub'
      'FROM lanctos'
      'WHERE FatID=:P0')
    Left = 780
    Top = 452
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFats1Data: TDateField
      FieldName = 'Data'
    end
    object QrFats1Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrFats1Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrFats1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFats1Sub: TSmallintField
      FieldName = 'Sub'
    end
  end
  object QrContaFat: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM contasfat')
    Left = 964
    Top = 104
    object QrContaFatPlaGen: TIntegerField
      FieldName = 'PlaGen'
    end
  end
  object QrCtaSemFat: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM contasfat')
    Left = 964
    Top = 152
    object IntegerField1: TIntegerField
      FieldName = 'PlaGen'
    end
  end
  object QrSemConta: TMySQLQuery
    Database = Dmod.MyDB
    Left = 32
    Top = 52
  end
  object DsSemConta: TDataSource
    DataSet = QrSemConta
    Left = 60
    Top = 52
  end
  object QrLancto1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM lanctos')
    Left = 24
    Top = 404
    object QrLancto1Sub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLancto1Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 128
    end
    object QrLancto1Credito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00  '
    end
    object QrLancto1Debito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00  '
    end
    object QrLancto1NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLancto1Documento: TFloatField
      FieldName = 'Documento'
    end
    object QrLancto1Vencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLancto1Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLancto1Data: TDateField
      FieldName = 'Data'
    end
    object QrLancto1NOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 128
    end
    object QrLancto1TIPOCARTEIRA: TIntegerField
      FieldName = 'TIPOCARTEIRA'
    end
    object QrLancto1CARTEIRABANCO: TIntegerField
      FieldName = 'CARTEIRABANCO'
    end
    object QrLancto1Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLancto1Fornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLancto1DataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLancto1Nivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLancto1Vendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLancto1Account: TIntegerField
      FieldName = 'Account'
    end
    object QrLancto1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLancto1CtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLancto1Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLancto1Mez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrLancto1Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrLancto1Doc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrLancto1SerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLancto1MoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLancto1Multa: TFloatField
      FieldName = 'Multa'
    end
    object QrLancto1ICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrLancto1ICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrLancto1CliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLancto1Depto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLancto1DescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLancto1ForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLancto1Unidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrLancto1Qtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrLancto1Qtd2: TFloatField
      FieldName = 'Qtd2'
    end
    object QrLancto1FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLancto1FatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLancto1DescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrLancto1NFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrLancto1FatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLancto1FatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLancto1CentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object QrLancto1VctoOriginal: TDateField
      FieldName = 'VctoOriginal'
    end
    object QrLancto1ModeloNF: TWideStringField
      FieldName = 'ModeloNF'
    end
  end
  object DsLancto1: TDataSource
    DataSet = QrLancto1
    Left = 52
    Top = 404
  end
  object DsLancto2: TDataSource
    DataSet = QrLancto2
    Left = 52
    Top = 432
  end
  object QrLancto2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 24
    Top = 432
    object QrLancto2Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLancto2Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrLancto2Debito: TFloatField
      FieldName = 'Debito'
    end
    object QrLancto2Sub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLancto2Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLancto2Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPsqLct1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(lct.Controle) Pagamentos'
      'FROM lct0001a lct'
      'WHERE lct.ID_Pgto=999999999')
    Left = 32
    Top = 100
    object QrPsqLct1Pagamentos: TLargeintField
      FieldName = 'Pagamentos'
      Required = True
    end
  end
  object QrPsqCrt1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo '
      'FROM Carteiras'
      'WHERE Codigo = ('
      '     SELECT Banco'
      '     FROM carteiras '
      '     WHERE Codigo=:P0)')
    Left = 92
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPsqCrt1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPsqCrt1Tipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrUmLct: TMySQLQuery
    Database = Dmod.MyDB
    Left = 968
    Top = 200
  end
  object QrLE: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(Controle) Itens '
      'FROM lct0001a'
      'WHERE FatID=3001'
      'AND FatNum=8'
      'AND (Sit = 0'
      '  OR Compensado < 2)')
    Left = 968
    Top = 252
    object QrLEItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrCtaNSdo: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT con.Codigo CodCta, csd.Codigo CodSdo'
      'FROM contas con'
      'LEFT JOIN contassdo csd ON con.Codigo=csd.Codigo'
      'WHERE (con.Codigo IS NULL) OR (csd.Codigo IS NULL)'
      'AND con.Codigo > 0')
    Left = 56
    Top = 500
    object QrCtaNSdoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrCarteira: TMySQLQuery
    Database = Dmod.MyDB
    Left = 904
    Top = 412
    object QrCarteiraTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrContasEnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM '
      'contasent'
      'WHERE Codigo=:P0'
      'AND Entidade=:P1')
    Left = 652
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrContasEntCntrDebCta: TWideStringField
      FieldName = 'CntrDebCta'
      Size = 50
    end
    object QrContasEntCodExporta: TWideStringField
      FieldName = 'CodExporta'
      Size = 60
    end
  end
  object QrDuplEntValData: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDuplEntValDataCalcFields
    Left = 24
    Top = 300
    object QrDuplEntValDataData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplEntValDataControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplEntValDataDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDuplEntValDataCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplEntValDataDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplEntValDataNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplEntValDataCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplEntValDataMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrDuplEntValDataFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrDuplEntValDataCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDuplEntValDataNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrDuplEntValDataNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrDuplEntValDataNOMEFNC: TWideStringField
      FieldName = 'NOMEFNC'
      Size = 100
    end
    object QrDuplEntValDataDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrDuplEntValDataSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrDuplEntValDataCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDuplEntValDataTERCEIRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TERCEIRO'
      Size = 255
      Calculated = True
    end
    object QrDuplEntValDataMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
    object QrDuplEntValDataCHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CHEQUE'
      Calculated = True
    end
    object QrDuplEntValDataCOMP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMP_TXT'
      Size = 10
      Calculated = True
    end
  end
  object DsDuplEntValData: TDataSource
    DataSet = QrDuplEntValData
    Left = 84
    Top = 300
  end
  object QrFinOpcoes: TMySQLQuery
    Database = Dmod.MyDB
    Left = 228
    Top = 36
  end
  object QrLocFIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lfi.*'
      'FROM locfits lfi'
      'WHERE lfi.Controle>0'
      '')
    Left = 574
    Top = 352
    object QrLocFItsPARCELA: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'PARCELA'
      Calculated = True
    end
    object QrLocFItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocFItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocFItsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrLocFItsLancto: TLargeintField
      FieldName = 'Lancto'
    end
    object QrLocFItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocFItsVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
  end
end
