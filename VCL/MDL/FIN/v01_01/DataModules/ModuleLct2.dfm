object DmLct2: TDmLct2
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 587
  Width = 826
  PixelsPerInch = 96
  object DsCrt: TDataSource
    DataSet = QrCrt
    Left = 96
    Top = 52
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 96
    Top = 4
  end
  object DsCrtSum: TDataSource
    DataSet = QrCrtSum
    Left = 96
    Top = 100
  end
  object QrLct: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrLctBeforeClose
    AfterScroll = QrLctAfterScroll
    OnCalcFields = QrLctCalcFields
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2,'
      '  ((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,'
      'ci.Unidade UH, '
      'la.*, ct.Codigo CONTA, ca.Prazo, ca.Nome NO_Carteira, '
      'ca.Banco1, ca.Agencia1, ca.Conta1,'
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,'
      'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,'
      'IF(la.ForneceI=0, "",'
      '  IF(fi.Tipo=0, fi.RazaoSocial, fi.Nome)) NOMEFORNECEI,'
      'IF(la.Sit<2, la.Credito-la.Debito-(la.Pago*la.Sit), 0) SALDO,'
      'IF(la.Cliente>0,'
      '  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),'
      '  IF (la.Fornecedor>0,'
      
        '    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO' +
        ', '
      
        'ELT(la.Endossas, "Endossado", "Endossante", "Ambos", "? ? ? ?") ' +
        'NO_ENDOSSADO,'
      'uc.Login USERCAD_TXT, ua.Login USERALT_TXT,'
      
        'IF(la.DataCad < 2, "",DATE_FORMAT(la.DataCad,"%d/%m/%y")) DATACA' +
        'D_TXT,'
      
        'IF(la.DataAlt < 2, "", DATE_FORMAT(la.DataAlt, "%d/%m/%y")) DATA' +
        'ALT_TXT'
      'FROM lct0001a la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI'
      'LEFT JOIN condimov  ci ON ci.Conta=la.Depto'
      'LEFT JOIN senhas uc ON uc.Numero=la.UserCad'
      'LEFT JOIN senhas ua ON ua.Numero=la.UserAlt'
      'WHERE la.Data BETWEEN "2007/05/11" AND "2010/10/04"'
      'AND la.Carteira=3'
      'ORDER BY la.Data, la.Controle'
      ''
      '')
    Left = 28
    Top = 4
    object QrLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctDescricao: TWideStringField
      DisplayWidth = 100
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrLctNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 128
    end
    object QrLctNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'contas.Nome'
      Size = 128
    end
    object QrLctNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 128
    end
    object QrLctNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 128
    end
    object QrLctNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 128
    end
    object QrLctNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctcliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctUserCad: TSmallintField
      FieldName = 'UserCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDuplicata: TWideStringField
      DisplayWidth = 15
      FieldName = 'Duplicata'
      Size = 15
    end
    object QrLctCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLctPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrLctForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLctQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrLctQtd2: TFloatField
      FieldName = 'Qtd2'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLctEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrLctAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrLctContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrLctCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrLctDescoVal: TFloatField
      FieldName = 'DescoVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrLctNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrLctAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrLctUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrLctExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrLctSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLctMoraVal: TFloatField
      FieldName = 'MoraVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctMultaVal: TFloatField
      FieldName = 'MultaVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrLctCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrLctTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrLctID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrLctReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrLctAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrLctPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrLctPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrLctSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrLctMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrLctProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrLctCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
      Required = True
    end
    object QrLctAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLctAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrLctSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 5
    end
    object QrLctEndossas: TSmallintField
      FieldName = 'Endossas'
    end
    object QrLctEndossan: TFloatField
      FieldName = 'Endossan'
    end
    object QrLctEndossad: TFloatField
      FieldName = 'Endossad'
    end
    object QrLctCancelado: TSmallintField
      FieldName = 'Cancelado'
    end
    object QrLctEventosCad: TIntegerField
      FieldName = 'EventosCad'
    end
    object QrLctUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrLctEncerrado: TIntegerField
      FieldName = 'Encerrado'
    end
    object QrLctErrCtrl: TIntegerField
      FieldName = 'ErrCtrl'
    end
    object QrLctNO_Carteira: TWideStringField
      FieldName = 'NO_Carteira'
      Size = 100
    end
    object QrLctBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrLctAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrLctConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrLctNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrLctNO_ENDOSSADO: TWideStringField
      FieldName = 'NO_ENDOSSADO'
      Size = 10
    end
    object QrLctSERIE_CHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_CHEQUE'
      Calculated = True
    end
    object QrLctIndiPag: TIntegerField
      FieldName = 'IndiPag'
      DisplayFormat = '0;-0; '
    end
    object QrLctFisicoSrc: TSmallintField
      FieldName = 'FisicoSrc'
    end
    object QrLctFisicoCod: TIntegerField
      FieldName = 'FisicoCod'
    end
    object QrLctUSERCAD_TXT: TWideStringField
      FieldName = 'USERCAD_TXT'
      Size = 30
    end
    object QrLctUSERALT_TXT: TWideStringField
      FieldName = 'USERALT_TXT'
      Size = 30
    end
    object QrLctDATACAD_TXT: TWideStringField
      FieldName = 'DATACAD_TXT'
      Size = 10
    end
    object QrLctDATAALT_TXT: TWideStringField
      FieldName = 'DATAALT_TXT'
      Size = 10
    end
    object QrLctRecDes: TFloatField
      FieldName = 'RecDes'
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00;'
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00;'
    end
    object QrLctBancoCar: TIntegerField
      FieldName = 'BancoCar'
    end
    object QrLctCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object QrLctVctoOriginal: TDateField
      FieldName = 'VctoOriginal'
    end
    object QrLctModeloNF: TWideStringField
      FieldName = 'ModeloNF'
    end
    object QrLctFatParcRef: TIntegerField
      FieldName = 'FatParcRef'
    end
    object QrLctGenCtb: TIntegerField
      FieldName = 'GenCtb'
    end
    object QrLctGenCtbD: TIntegerField
      FieldName = 'GenCtbD'
    end
    object QrLctGenCtbC: TIntegerField
      FieldName = 'GenCtbC'
    end
  end
  object QrCrt: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCrtBeforeClose
    AfterClose = QrCrtAfterClose
    AfterScroll = QrCrtAfterScroll
    OnCalcFields = QrCrtCalcFields
    SQL.Strings = (
      'SELECT DISTINCT ca.*, ba.Nome NOMEDOBANCO,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEFORNECEI'
      'FROM carteiras ca'
      'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco'
      'LEFT JOIN entidades en ON en.Codigo=ca.ForneceI'
      'WHERE ca.ForneceI in (-11)'
      'AND ca.Codigo > 0'
      'ORDER BY ca.Nome')
    Left = 28
    Top = 52
    object QrCrtDIFERENCA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIFERENCA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrCrtTIPOPRAZO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOPRAZO'
      Size = 1
      Calculated = True
    end
    object QrCrtNOMEPAGREC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEPAGREC'
      Size = 50
      Calculated = True
    end
    object QrCrtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCrtNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCrtTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCrtInicial: TFloatField
      FieldName = 'Inicial'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCrtID: TWideStringField
      FieldName = 'ID'
      Size = 100
    end
    object QrCrtFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCrtID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Size = 50
    end
    object QrCrtSaldo: TFloatField
      FieldName = 'Saldo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCrtEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCrtPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCrtDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCrtDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCrtUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCrtUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCrtPagRec: TIntegerField
      FieldName = 'PagRec'
    end
    object QrCrtDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrCrtExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCrtForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCrtNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 100
    end
    object QrCrtTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrCrtBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCrtAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrCrtConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCrtCheque1: TIntegerField
      FieldName = 'Cheque1'
    end
    object QrCrtContato1: TWideStringField
      FieldName = 'Contato1'
      Size = 100
    end
    object QrCrtAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrCrtContab: TWideStringField
      FieldName = 'Contab'
    end
    object QrCrtOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCrtFuturoC: TFloatField
      FieldName = 'FuturoC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtFuturoD: TFloatField
      FieldName = 'FuturoD'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtFuturoS: TFloatField
      FieldName = 'FuturoS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtForneceN: TSmallintField
      FieldName = 'ForneceN'
    end
    object QrCrtExclusivo: TSmallintField
      FieldName = 'Exclusivo'
    end
    object QrCrtRecebeBloq: TSmallintField
      FieldName = 'RecebeBloq'
    end
    object QrCrtEntiDent: TIntegerField
      FieldName = 'EntiDent'
    end
    object QrCrtCodCedente: TWideStringField
      FieldName = 'CodCedente'
    end
    object QrCrtAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCrtAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCrtValMorto: TFloatField
      FieldName = 'ValMorto'
    end
    object QrCrtNOMEDOBANCO: TWideStringField
      FieldName = 'NOMEDOBANCO'
      Size = 100
    end
    object QrCrtNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrCrtCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrCrtIgnorSerie: TSmallintField
      FieldName = 'IgnorSerie'
    end
    object QrCrtValIniOld: TFloatField
      FieldName = 'ValIniOld'
    end
    object QrCrtSdoFimB: TFloatField
      FieldName = 'SdoFimB'
    end
  end
  object QrCrtSum: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Saldo) SALDO, SUM(FuturoC) FuturoC,'
      'SUM(FuturoD) FuturoD, SUM(FuturoS) FuturoS, '
      'SUM(EmCaixa) EmCaixa, SUM(EmCaixa-Saldo) Difere,'
      'SUM(Saldo+FuturoS) SDO_FUT'
      'FROM carteiras ca'
      'WHERE ca.ForneceI in (1,2,3,4)'
      '')
    Left = 28
    Top = 100
    object QrCrtSumSALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtSumFuturoC: TFloatField
      FieldName = 'FuturoC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtSumFuturoD: TFloatField
      FieldName = 'FuturoD'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtSumFuturoS: TFloatField
      FieldName = 'FuturoS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtSumEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtSumDifere: TFloatField
      FieldName = 'Difere'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCrtSumSDO_FUT: TFloatField
      FieldName = 'SDO_FUT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object QrLcP: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrLctBeforeClose
    AfterScroll = QrLctAfterScroll
    OnCalcFields = QrLctCalcFields
    SQL.Strings = (
      'SELECT Data, Tipo, Carteira, Controle,'
      'Sub, CliInt, Cliente, Fornecedor,'
      'Documento, SUM(Credito) Credito, SUM(Debito)'
      'Debito, SerieCH, Vencimento, COUNT(Ativo) ITENS'
      'FROM _lct_proto_'
      'GROUP BY Data, Tipo, Carteira, CliInt,'
      'SerieCH, Documento')
    Left = 312
    Top = 4
    object QrLcPData: TDateField
      FieldName = 'Data'
    end
    object QrLcPTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLcPCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLcPControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrLcPSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLcPCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLcPCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLcPFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLcPDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLcPCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLcPDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLcPSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLcPVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLcPITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsLcP: TDataSource
    DataSet = QrLcP
    Left = 380
    Top = 4
  end
  object QrLcI: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrLctBeforeClose
    AfterScroll = QrLctAfterScroll
    OnCalcFields = QrLctCalcFields
    SQL.Strings = (
      'SELECT Controle Lancto'
      'FROM _lct_proto_'
      'WHERE Data=:P0'
      'AND Tipo=:P1'
      'AND Carteira=:P2'
      'AND CliInt=:P3'
      'AND SerieCH=:P4'
      'AND Documento=:P5')
    Left = 312
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end>
    object QrLcILancto: TLargeintField
      FieldName = 'Lancto'
    end
  end
  object DsQrLcI: TDataSource
    DataSet = QrLcI
    Left = 380
    Top = 52
  end
  object QrCarts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCartsBeforeClose
    AfterClose = QrCartsAfterClose
    AfterScroll = QrCartsAfterScroll
    OnCalcFields = QrCartsCalcFields
    SQL.Strings = (
      'SELECT DISTINCT ca.*, ba.Nome NOMEDOBANCO, '
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial '
      'ELSE en.Nome END NOMEFORNECEI '
      'FROM carteiras ca '
      'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco '
      'LEFT JOIN entidades en ON en.Codigo=ca.ForneceI '
      'WHERE ca.ForneceI in (-11)'
      'ORDER BY ca.Nome')
    Left = 32
    Top = 340
    object QrCartsDIFERENCA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIFERENCA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrCartsTIPOPRAZO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOPRAZO'
      Size = 1
      Calculated = True
    end
    object QrCartsNOMEPAGREC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEPAGREC'
      Size = 50
      Calculated = True
    end
    object QrCartsNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrCartsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'carteiras.Codigo'
      DisplayFormat = '000'
    end
    object QrCartsTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'carteiras.Tipo'
    end
    object QrCartsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrCartsInicial: TFloatField
      FieldName = 'Inicial'
      Origin = 'carteiras.Inicial'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartsBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'carteiras.Banco'
    end
    object QrCartsID: TWideStringField
      FieldName = 'ID'
      Origin = 'carteiras.ID'
      Size = 100
    end
    object QrCartsFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'carteiras.Fatura'
      FixedChar = True
      Size = 1
    end
    object QrCartsID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Origin = 'carteiras.ID_Fat'
      Size = 50
    end
    object QrCartsSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'carteiras.Saldo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'carteiras.Lk'
    end
    object QrCartsEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      Origin = 'carteiras.EmCaixa'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartsFechamento: TIntegerField
      FieldName = 'Fechamento'
      Origin = 'carteiras.Fechamento'
    end
    object QrCartsPrazo: TSmallintField
      FieldName = 'Prazo'
      Origin = 'carteiras.Prazo'
    end
    object QrCartsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'carteiras.DataCad'
    end
    object QrCartsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'carteiras.DataAlt'
    end
    object QrCartsUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'carteiras.UserCad'
    end
    object QrCartsUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'carteiras.UserAlt'
    end
    object QrCartsPagRec: TSmallintField
      FieldName = 'PagRec'
      Origin = 'carteiras.PagRec'
    end
    object QrCartsFuturoC: TFloatField
      FieldName = 'FuturoC'
      Origin = 'carteiras.FuturoC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartsFuturoD: TFloatField
      FieldName = 'FuturoD'
      Origin = 'carteiras.FuturoD'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartsFuturoS: TFloatField
      FieldName = 'FuturoS'
      Origin = 'carteiras.FuturoS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartsNOMEDOBANCO: TWideStringField
      FieldName = 'NOMEDOBANCO'
      Size = 100
    end
    object QrCartsDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrCartsExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCartsForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCartsNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 100
    end
    object QrCartsContato1: TWideStringField
      FieldName = 'Contato1'
      Size = 100
    end
    object QrCartsTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrCartsBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCartsAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrCartsConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCartsCheque1: TIntegerField
      FieldName = 'Cheque1'
    end
    object QrCartsAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrCartsContab: TWideStringField
      FieldName = 'Contab'
    end
    object QrCartsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCartsForneceN: TSmallintField
      FieldName = 'ForneceN'
    end
    object QrCartsExclusivo: TSmallintField
      FieldName = 'Exclusivo'
    end
    object QrCartsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCartsRecebeBloq: TSmallintField
      FieldName = 'RecebeBloq'
    end
    object QrCartsEntiDent: TIntegerField
      FieldName = 'EntiDent'
    end
    object QrCartsCodCedente: TWideStringField
      FieldName = 'CodCedente'
    end
    object QrCartsValMorto: TFloatField
      FieldName = 'ValMorto'
    end
    object QrCartsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCartsIgnorSerie: TSmallintField
      FieldName = 'IgnorSerie'
    end
  end
  object DsCarts: TDataSource
    DataSet = QrCarts
    Left = 92
    Top = 340
  end
  object QrLctos: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrLctosBeforeClose
    OnCalcFields = QrLctosCalcFields
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, ca.Prazo,'
      'ca.Banco1, ca.Agencia1, ca.Conta1, '
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'ca.Nome NO_Carteira,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR,'
      'CASE 1 WHEN fi.Tipo=0 THEN fi.RazaoSocial '
      'ELSE fi.Nome END NOMEFORNECEI,'
      
        'ELT(la.Endossas, "Endossado", "Endossante", "Ambos", "? ? ? ?") ' +
        'NO_ENDOSSADO '
      'FROM lanctos la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI'
      'ORDER BY la.Data, la.Controle')
    Left = 32
    Top = 436
    object QrLctosData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctosTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrLctosCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrLctosAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctosGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrLctosDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrLctosNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctosCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctosDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctosSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrLctosVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctosLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrLctosFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrLctosFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrLctosCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrLctosNOMECONTA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 50
    end
    object QrLctosNOMEEMPRESA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrLctosNOMESUBGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 50
    end
    object QrLctosNOMEGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 50
    end
    object QrLctosNOMECONJUNTO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 50
    end
    object QrLctosNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctosAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctosMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctosMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctosBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lanctos.Banco'
    end
    object QrLctosLocal: TIntegerField
      FieldName = 'Local'
      Origin = 'lanctos.Local'
    end
    object QrLctosFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'lanctos.Fatura'
      FixedChar = True
      Size = 1
    end
    object QrLctosSub: TSmallintField
      FieldName = 'Sub'
      Origin = 'lanctos.Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctosCartao: TIntegerField
      FieldName = 'Cartao'
      Origin = 'lanctos.Cartao'
    end
    object QrLctosLinha: TIntegerField
      FieldName = 'Linha'
      Origin = 'lanctos.Linha'
    end
    object QrLctosPago: TFloatField
      FieldName = 'Pago'
      Origin = 'lanctos.Pago'
    end
    object QrLctosSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctosID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Origin = 'lanctos.ID_Sub'
    end
    object QrLctosMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'lanctos.Mez'
    end
    object QrLctosFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'lanctos.Fornecedor'
    end
    object QrLctoscliente: TIntegerField
      FieldName = 'cliente'
      Origin = 'lanctos.Cliente'
    end
    object QrLctosMoraDia: TFloatField
      FieldName = 'MoraDia'
      Origin = 'lanctos.MoraDia'
    end
    object QrLctosNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctosNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctosTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctosNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctosOperCount: TIntegerField
      FieldName = 'OperCount'
      Origin = 'lanctos.OperCount'
    end
    object QrLctosLancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'lanctos.Lancto'
    end
    object QrLctosMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'lanctos.Multa'
    end
    object QrLctosATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctosJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctosDataDoc: TDateField
      FieldName = 'DataDoc'
      Origin = 'lanctos.DataDoc'
    end
    object QrLctosNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'lanctos.Nivel'
    end
    object QrLctosVendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'lanctos.Vendedor'
    end
    object QrLctosAccount: TIntegerField
      FieldName = 'Account'
      Origin = 'lanctos.Account'
    end
    object QrLctosMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLctosProtesto: TDateField
      FieldName = 'Protesto'
      Origin = 'lanctos.Protesto'
    end
    object QrLctosDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'lanctos.DataCad'
    end
    object QrLctosDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'lanctos.DataAlt'
    end
    object QrLctosUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'lanctos.UserCad'
    end
    object QrLctosUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'lanctos.UserAlt'
    end
    object QrLctosControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lanctos.Controle'
      Required = True
    end
    object QrLctosID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Origin = 'lanctos.ID_Pgto'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrLctosCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Origin = 'lanctos.CtrlIni'
      Required = True
    end
    object QrLctosFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
      Origin = 'lanctos.FatID_Sub'
    end
    object QrLctosICMS_P: TFloatField
      FieldName = 'ICMS_P'
      Origin = 'lanctos.ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctosICMS_V: TFloatField
      FieldName = 'ICMS_V'
      Origin = 'lanctos.ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'lanctos.Duplicata'
      Size = 10
    end
    object QrLctosCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctosCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'lanctos.CliInt'
    end
    object QrLctosDepto: TIntegerField
      FieldName = 'Depto'
      Origin = 'lanctos.Depto'
    end
    object QrLctosDescoPor: TIntegerField
      FieldName = 'DescoPor'
      Origin = 'lanctos.DescoPor'
    end
    object QrLctosPrazo: TSmallintField
      FieldName = 'Prazo'
      Origin = 'carteiras.Prazo'
    end
    object QrLctosForneceI: TIntegerField
      FieldName = 'ForneceI'
      Origin = 'lanctos.ForneceI'
    end
    object QrLctosQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'lanctos.Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrLctosQtd2: TFloatField
      FieldName = 'Qtd2'
      Origin = 'lanctos.Qtd2'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrLctosEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lanctos.Emitente'
      Size = 30
    end
    object QrLctosContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Origin = 'lanctos.ContaCorrente'
      Size = 15
    end
    object QrLctosCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Origin = 'lanctos.CNPJCPF'
      Size = 15
    end
    object QrLctosDescoVal: TFloatField
      FieldName = 'DescoVal'
      Origin = 'lanctos.DescoVal'
    end
    object QrLctosDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Origin = 'lanctos.DescoControle'
      Required = True
    end
    object QrLctosUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'lanctos.Unidade'
      Required = True
    end
    object QrLctosNFVal: TFloatField
      FieldName = 'NFVal'
      Origin = 'lanctos.NFVal'
      Required = True
    end
    object QrLctosAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'lanctos.Antigo'
    end
    object QrLctosExcelGru: TIntegerField
      FieldName = 'ExcelGru'
      Origin = 'lanctos.ExcelGru'
    end
    object QrLctosSerieCH: TWideStringField
      DisplayWidth = 10
      FieldName = 'SerieCH'
      Origin = 'lanctos.SerieCH'
      Size = 10
    end
    object QrLctosSERIE_CHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_CHEQUE'
      Calculated = True
    end
    object QrLctosDoc2: TWideStringField
      FieldName = 'Doc2'
      Origin = 'lanctos.Doc2'
    end
    object QrLctosNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrLctosMoraVal: TFloatField
      FieldName = 'MoraVal'
      Origin = 'lanctos.MoraVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctosMultaVal: TFloatField
      FieldName = 'MultaVal'
      Origin = 'lanctos.MultaVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctosCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
      Origin = 'lanctos.CNAB_Sit'
    end
    object QrLctosBanco1: TIntegerField
      FieldName = 'Banco1'
      Origin = 'carteiras.Banco1'
    end
    object QrLctosAgencia1: TIntegerField
      FieldName = 'Agencia1'
      Origin = 'carteiras.Agencia1'
    end
    object QrLctosConta1: TWideStringField
      FieldName = 'Conta1'
      Origin = 'carteiras.Conta1'
      Size = 15
    end
    object QrLctosTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Origin = 'lanctos.TipoCH'
      Required = True
    end
    object QrLctosFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'lanctos.FatNum'
    end
    object QrLctosSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Origin = 'lanctos.SerieNF'
      Size = 5
    end
    object QrLctosNO_ENDOSSADO: TWideStringField
      FieldName = 'NO_ENDOSSADO'
      Size = 10
    end
    object QrLctosNO_Carteira: TWideStringField
      FieldName = 'NO_Carteira'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrLctosUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrLctosID_Quit: TIntegerField
      FieldName = 'ID_Quit'
    end
    object QrLctosReparcel: TIntegerField
      FieldName = 'Reparcel'
    end
    object QrLctosAtrelado: TIntegerField
      FieldName = 'Atrelado'
    end
    object QrLctosPagMul: TFloatField
      FieldName = 'PagMul'
    end
    object QrLctosPagJur: TFloatField
      FieldName = 'PagJur'
    end
    object QrLctosSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
    end
    object QrLctosMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrLctosProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrLctosCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrLctosEndossas: TSmallintField
      FieldName = 'Endossas'
    end
    object QrLctosEndossan: TFloatField
      FieldName = 'Endossan'
    end
    object QrLctosEndossad: TFloatField
      FieldName = 'Endossad'
    end
    object QrLctosCancelado: TSmallintField
      FieldName = 'Cancelado'
    end
    object QrLctosEventosCad: TIntegerField
      FieldName = 'EventosCad'
    end
    object QrLctosEncerrado: TIntegerField
      FieldName = 'Encerrado'
    end
    object QrLctosErrCtrl: TIntegerField
      FieldName = 'ErrCtrl'
    end
    object QrLctosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrLctosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLctosIndiPag: TIntegerField
      FieldName = 'IndiPag'
    end
    object QrLctosAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrLctosRecDes: TFloatField
      FieldName = 'RecDes'
    end
    object QrLctosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00;'
    end
    object QrLctosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00;'
    end
    object QrLctosBancoCar: TIntegerField
      FieldName = 'BancoCar'
    end
    object QrLctosCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object QrLctosVctoOriginal: TDateField
      FieldName = 'VctoOriginal'
    end
    object QrLctosGenCtb: TIntegerField
      FieldName = 'GenCtb'
    end
  end
  object DsLctos: TDataSource
    DataSet = QrLctos
    Left = 92
    Top = 436
  end
  object QrCartSum: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Saldo) SALDO, SUM(FuturoC) FuturoC,'
      'SUM(FuturoD) FuturoD, SUM(FuturoS) FuturoS, '
      'SUM(EmCaixa) EmCaixa, SUM(EmCaixa-Saldo) Difere,'
      'SUM(Saldo+FuturoS) SDO_FUT'
      'FROM carteiras ca'
      'WHERE ca.ForneceI in (1,2,3,4)'
      '')
    Left = 32
    Top = 388
    object QrCartSumSALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartSumFuturoC: TFloatField
      FieldName = 'FuturoC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartSumFuturoD: TFloatField
      FieldName = 'FuturoD'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartSumFuturoS: TFloatField
      FieldName = 'FuturoS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartSumEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartSumDifere: TFloatField
      FieldName = 'Difere'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartSumSDO_FUT: TFloatField
      FieldName = 'SDO_FUT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsCartSum: TDataSource
    DataSet = QrCartSum
    Left = 92
    Top = 388
  end
  object QrSomaLinhas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) CREDITO, SUM(Debito) DEBITO'
      'FROM lanctos la')
    Left = 32
    Top = 484
    object QrSomaLinhasCREDITO: TFloatField
      FieldName = 'CREDITO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSomaLinhasDEBITO: TFloatField
      FieldName = 'DEBITO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSomaLinhasSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsSomaLinhas: TDataSource
    DataSet = QrSomaLinhas
    Left = 92
    Top = 484
  end
  object QrSomaM: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT (SUM(Credito) - SUM(Debito)) Valor'
      'FROM lctxxxxx')
    Left = 572
    Top = 51
    object QrSomaMValor: TFloatField
      FieldName = 'Valor'
    end
  end
end
