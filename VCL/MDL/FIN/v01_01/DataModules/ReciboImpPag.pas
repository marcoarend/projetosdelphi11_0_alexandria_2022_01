unit ReciboImpPag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker;

type
  TFmReciboImpPag = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label6: TLabel;
    EdCodigo: TdmkEdit;
    QrCarteiras: TMySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    DsCarteiras: TDataSource;
    RGCarteira: TRadioGroup;
    Label15: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    EdValor: TdmkEdit;
    LaDeb: TLabel;
    LaDoc: TLabel;
    EdSerieCH: TdmkEdit;
    EdDoc: TdmkEdit;
    TPVencimento: TdmkEditDateTimePicker;
    LaVencimento: TLabel;
    TPData: TdmkEditDateTimePicker;
    Label1: TLabel;
    dmkEdNF: TdmkEdit;
    LaNF: TLabel;
    Label13: TLabel;
    EdDescricao: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGCarteiraClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenReciboImpDst(Controle: Integer);
  public
    { Public declarations }
    FTabLctA: String;
    FCliInt: Integer;
    FModuleLctX: TDataModule;
    FCodigo: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    procedure CarteirasReopen(Tipo: Integer);
  end;

  var
  FmReciboImpPag: TFmReciboImpPag;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModuleLct2, UnFinanceiro, ModuleFin, ReciboImpCab, ModuleGeral;

{$R *.DFM}

procedure TFmReciboImpPag.BtOKClick(Sender: TObject);
var
  Carteira: Integer;
  //

  function InsereReciboImpDst(): Boolean;
  var
    Codigo, Controle, LctCtrl, LctSub: Integer;
    SQLType: TSQLType;
  begin
    Result         := False;
    SQLType        := ImgTipo.SQLType;
    Codigo         := FCodigo;
    Controle       := 0;
    //VAR_LANCTO2     := Controle;
    //VAR_DATAINSERIR := DataPag;
    LctCtrl        := FLAN_Controle;
    LctSub         := 0;
    //
    Controle := UMyMod.BPGS1I32('reciboimpdst', 'Controle', '', '', tsPos, SQLType, Controle);
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'reciboimpdst', False, [
    'Codigo', 'LctCtrl', 'LctSub'], [
    'Controle'], [
    Codigo, LctCtrl, LctSub], [
    Controle], True);
  end;
  //
  function InserePagamento(var ValorResidual: Double): Boolean;
  const
    QrCrt = nil;
    QrLct = nil;
    RecalcSdoCart = False;
    UsaOutrosFatDfs = False;
    FatGrupo = 0;
  var
    OldTipo, OldCarteira, OldControle, OldSub, Carteira: Integer;
    Controle: Int64;
    Valor, Credito, Debito: Double;
    OldData: TDate;
    //
    Sit, Nivel, CtrlOri, Tipo, Genero, NotaFiscal, Cliente, Fornecedor, ID_Pgto,
    Vendedor, Account, CentroCusto, DescoPor, CliInt, ForneceI, Depto: Integer;
    Descricao, SerieCH, Duplicata, Mes, Msg: String;
    Compensado, Vencimento, DataPag, DataDoc: TDateTime;
    MoraDia, Multa, DescoVal, MultaVal, MoraVal, TaxasVal: Double;
    FatID, FatID_Sub, FatParcela, FatParcRef, Documento, CtrlIni: Integer;
    FatNum: Double;
    //
    CreToQuit: Double;
    //
    _Carteira, _GenCtbD, _GenCtbC, GenCtbD, GenCtbC, GenCtb: Integer;
    //
  begin
    Result := False;
    if ValorResidual < 0.01 then Exit;
    //
    if TDmLct2(FModuleLctX).QrLct.Locate('Controle;Sub', VarArrayOf([
    FmReciboImpCab.QrReciboImpOriLctCtrl.Value,
    FmReciboImpCab.QrReciboImpOriLctSub.Value]), []) then
    begin
      Nivel := TDmLct2(FModuleLctX).QrLctNivel.Value + 1;
      //
      if TDmLct2(FModuleLctX).QrLctCtrlIni.Value > 0 then
        CtrlIni := TDmLct2(FModuleLctX).QrLctCtrlIni.Value
      else
        CtrlIni := TDmLct2(FModuleLctX).QrLctControle.Value;
      //
      //Valor := Geral.DMV(EdValor.Text);
      CreToQuit := TDmLct2(FModuleLctX).QrLctCredito.Value;
      if TDmLct2(FModuleLctX).QrLctCredito.Value > 0 then Credito := Valor else Credito := 0;
      if TDmLct2(FModuleLctX).QrLctDebito.Value > 0 then Debito := Valor else Debito := 0;
      if CreToQuit > 0 then
      begin
        if ValorResidual > CreToQuit then
        begin
          ValorResidual := ValorResidual - CreToQuit;
          Credito := CreToQuit;
        end else
        begin
          Credito := ValorResidual;
          ValorResidual := 0;
        end;
      end else
        // Fazer de d�bito?
        Exit;
  (*   Movido para antes do loop!
      if MyObjects.FIC(RGCarteira.ItemIndex < 0, RGCarteira, 'Defina um tipo de carteira!') then
        Exit;
      Carteira := EdCarteira.ValueVariant;
      if MyObjects.FIC(Carteira < 1, EdCarteira, 'Defina um item de carteira maior que zero!') then
        Exit;
      if MyObjects.FIC(Valor = 0, nil, 'Defina um valor diferente de zero!') then
        Exit;
  *)
      //Verifica FatID
      if UFinanceiro.FatIDProibidoEmLct(TDmLct2(FModuleLctX).QrLctFatID.Value, Msg) then
      begin
        Geral.MB_Aviso(Msg);
        Exit;
      end;
      Mes := TDmLct2(FModuleLctX).QrLctMENSAL2.Value;
      if (EdDoc.Enabled = False) then Documento := 0
      else Documento := Geral.IMV(EdDoc.Text);
      //
      if (TPVencimento.Visible = False) then
        Vencimento := TDmLct2(FModuleLctX).QrLctData.Value
      else
        Vencimento := TPVencimento.Date;
      //
      if RGCarteira.ItemIndex = 2 then
      begin
        Sit := 0;
        Compensado := 0;
      end else begin
        Sit := 3;
        Compensado := TPData.Date;
      end;
      //TipoAnt := StrToInt(EdTipoAnt.Caption);
      OldCarteira := TDmLct2(FModuleLctX).QrLctCarteira.Value;
      //
      /////////// 2012-01-05
      Valor := Credito - Debito;
      //
      CtrlOri := 0;
      Duplicata := '';

      SerieCH        := EdSerieCH.Text;
      DataPag        := TPData.Date;
      Tipo           := RGCarteira.ItemIndex;
      _Carteira      := EdCarteira.ValueVariant;
      //Credito        := Credito;
      //Debito         := Debito;
      Genero         := TDmLct2(FModuleLctX).QrLctGenero.Value;
      GenCtb         := TDmLct2(FModuleLctX).QrLctGenCtb.Value;
      _GenCtbD       := TDmLct2(FModuleLctX).QrLctGenCtbD.Value;
      _GenCtbC       := TDmLct2(FModuleLctX).QrLctGenCtbC.Value;
      NotaFiscal     := dmkEdNF.ValueVariant;
      //Vencimento     := Vencimento;
      Descricao      := EdDescricao.Text;
      Sit            := Sit;
      Cliente        := TDmLct2(FModuleLctX).QrLctCliente.Value;
      Fornecedor     := TDmLct2(FModuleLctX).QrLctFornecedor.Value;
      ID_Pgto        := TDmLct2(FModuleLctX).QrLctControle.Value;
      DataDoc        := TDmLct2(FModuleLctX).QrLctDataDoc.Value;
      MoraDia        := 0;
      Multa          := 0;
      TaxasVal       := 0;
      MultaVal       := 0.00; // EdMultaVal.ValueVariant;
      MoraVal        := 0.00; // EdMoraVal.ValueVariant;
      CtrlIni        := Trunc(CtrlIni + 0.01);
      Nivel          := Nivel;
      Vendedor       := TDmLct2(FModuleLctX).QrLctVendedor.Value;
      Account        := TDmLct2(FModuleLctX).QrLctAccount.Value;
      CentroCusto    := TDmLct2(FModuleLctX).QrLctCentroCusto.Value;
      DescoPor       := 0; // Geral.IMV(EdDescoPor.Text);
      DescoVal       := 0; // Geral.DMV(EdDescoVal.Text);
      //Synker
      CliInt         := TDmLct2(FModuleLctX).QrLctCliInt.Value;
      ForneceI       := TDmLct2(FModuleLctX).QrLctForneceI.Value;
      Depto          := TDmLct2(FModuleLctX).QrLctDepto.Value;
      FatID          := TDmLct2(FModuleLctX).QrLctFatID.Value;
      FatID_Sub      := TDmLct2(FModuleLctX).QrLctFatID_Sub.Value;
      FatNum         := TDmLct2(FModuleLctX).QrLctFatNum.Value;
      FatParcela     := TDmLct2(FModuleLctX).QrLctFatParcela.Value;
      FatParcRef     := 0; // Cuidado ao usar! Usa no Creditor!
      //
      // ini 2022-03-24
      GenCtbD := 0;
      GenCtbC := 0;
      if not UFinanceiro.PagamentoEmBanco_DefineGenerosECarteira(
        TDmLct2(FModuleLctX).QrLctControle.Value,
        TDmLct2(FModuleLctX).QrLctSub.Value,
        Credito, Debito, (*OutraCartCompensa*)0, _Carteira, _GenCtbD, _GenCtbC,
        Carteira, GenCtbD, GenCtbC) then Exit;
      // fim 2022-03-24

      if ImgTipo.SQLType = stIns then
      begin
        if DModFin.PagamtParcialDeDocumento(Valor, Credito, Debito,
        CtrlIni, CtrlOri, Documento,
        SerieCH, Duplicata, DataPag, Vencimento, DataDoc, Compensado, Descricao, QrCrt,
        QrLct, RecalcSdoCart, FTabLctA, UsaOutrosFatDfs, FatID, FatID_Sub, FatNum,
        FatParcela, FatParcRef, FatGrupo,   // Novo
        Nivel, Carteira, Sit, Tipo, Genero, NotaFiscal, Cliente, Fornecedor, ID_Pgto,
        Vendedor, Account, DescoPor, CliInt, ForneceI, Depto, CentroCusto,
        Mes, MoraDia, Multa, DescoVal, TaxasVal, MultaVal, MoraVal, OldCarteira,
        GenCtb, GenCtbD, GenCtbC,
        '', False) then
        //if U n F i n a n c e i r o . I n s e r e L a n c a m e n t o (FTabLctA) then
        begin
          Result := InsereReciboImpDst();
          //
          UFinanceiro.RecalcSaldoCarteira(Carteira, nil, nil, False, False);
          UFinanceiro.RecalcSaldoCarteira(OldCarteira,  nil, nil, False, False);
        end;
      end else begin
        // 2013-07-25 N�o altera mais nesta janela!
        Geral.MB_Aviso('Altera��o desativada. Solicite implementa��o � Dermatek!');
      end;
    end;
  end;
var
  N: Integer;
  Valor, ValorRestante: Double;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Incluindo itens');
    Valor := EdValor.ValueVariant;
    if MyObjects.FIC(RGCarteira.ItemIndex < 0, RGCarteira, 'Defina um tipo de carteira!') then
      Exit;
    Carteira := EdCarteira.ValueVariant;
    if MyObjects.FIC(Carteira < 1, EdCarteira, 'Defina um item de carteira maior que zero!') then
      Exit;
    if MyObjects.FIC(Valor = 0, nil, 'Defina um valor diferente de zero!') then
      Exit;
    //
    ValorRestante := Valor;
    Continua      := True;
    N := 1;
    FmReciboImpCab.ReopenReciboImpOri(0);
    FmReciboImpCab.QrReciboImpOri.First;
    while not FmReciboImpCab.QrReciboImpOri.Eof do
    begin
      if (ValorRestante > 0) then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'Incluindo pagamento ' + Geral.FF0(N) + '.');
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Incluindo itens (' +
        Geral.FF0(N) + ')');
        if InserePagamento(ValorRestante) then
          N := N + 1;
      end;
      //
      FmReciboImpCab.QrReciboImpOri.Next;
    end;
    Screen.Cursor := crDefault;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Itens inclu�dos!');
    Geral.MB_Info('Inclus�o finalizada!');
  finally
    Screen.Cursor := crDefault;
  end;
  Close;
end;

procedure TFmReciboImpPag.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReciboImpPag.CarteirasReopen(Tipo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCarteiras, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM carteiras ',
    'WHERE Codigo > 0 ',
    'AND Ativo = 1 ',
    'AND Tipo=' + Geral.FF0(Tipo),
    'AND ForneceI=' + Geral.FF0(FCliInt),
    '']);
end;

procedure TFmReciboImpPag.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmReciboImpPag.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  TPData.Date := DModG.ObtemAgora();
end;

procedure TFmReciboImpPag.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmReciboImpPag.FormShow(Sender: TObject);
const
  sProcName = 'TFmReciboImpPag.FormShow()';
begin
  CarteirasReopen(RGCarteira.ItemIndex);
  if UFinanceiro.TabLctNaoDefinida(FTabLctA, sProcName) then
    Close;
end;

procedure TFmReciboImpPag.ReopenReciboImpDst(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmReciboImpPag.RGCarteiraClick(Sender: TObject);
begin
  EdCarteira.Text := '0';
  CBCarteira.KeyValue := 0;
  CarteirasReopen(RGCarteira.ItemIndex);
  case RGCarteira.ItemIndex of
    0:
    begin
      EdDoc.Enabled := False;
      LaDoc.Enabled := False;
      TPVencimento.Visible := False;
      LaVencimento.Visible := False;
    end;
    1:
    begin
      EdDoc.Enabled := True;
      LaDoc.Enabled := True;
      TPVencimento.Visible := False;
      LaVencimento.Visible := False;
    end;
    2:
    begin
      EdDoc.Enabled := True;
      LaDoc.Enabled := True;
      TPVencimento.Visible := True;
      LaVencimento.Visible := True;
    end;
  end;
end;

end.
