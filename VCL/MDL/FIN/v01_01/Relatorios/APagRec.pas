unit APagRec;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGridDAC, Tabs, DockTabSet, dmkPermissoes, frxClass, frxDBSet,
  UnDmkProcFunc, DmkDAC_PF, UnDmkEnums (*dmkDockTabSet*)(*, dmkDockTabSet*);

type
  //TTipoSaidaRelatorio = (tsrImpressao, tsrExportRTF);
  TFmAPagRec = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Timer1: TTimer;
    QrIndiPags: TmySQLQuery;
    DsIndiPags: TDataSource;
    QrIndiPagsCodigo: TIntegerField;
    QrIndiPagsNome: TWideStringField;
    QrIndiPagsAtivo: TSmallintField;
    Splitter1: TSplitter;
    Panel5: TPanel;
    LaEmpresa: TLabel;
    BtConfirma: TBitBtn;
    BitBtn1: TBitBtn;
    BtReexibe: TBitBtn;
    DockTabSet1: TTabSet;
    pDockLeft: TPanel;
    QrAccounts: TmySQLQuery;
    QrAccountsCodigo: TIntegerField;
    QrAccountsNOMECONTATO: TWideStringField;
    DsAccounts: TDataSource;
    QrTerceiros: TmySQLQuery;
    QrTerceirosCodigo: TIntegerField;
    QrTerceirosNOMECONTATO: TWideStringField;
    QrTerceirosTel1: TWideStringField;
    QrTerceirosTel2: TWideStringField;
    QrTerceirosTel3: TWideStringField;
    QrTerceirosTEL1_TXT: TWideStringField;
    QrTerceirosTEL2_TXT: TWideStringField;
    QrTerceirosTEL3_TXT: TWideStringField;
    QrTerceirosTELEFONES: TWideStringField;
    DsTerceiros: TDataSource;
    QrVendedores: TmySQLQuery;
    QrVendedoresCodigo: TIntegerField;
    QrVendedoresNOMECONTATO: TWideStringField;
    DsVendedores: TDataSource;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    PainelD: TPanel;
    LaAccount: TLabel;
    LaTerceiro: TLabel;
    LaVendedor: TLabel;
    CBAccount: TdmkDBLookupComboBox;
    EdAccount: TdmkEditCB;
    CBTerceiro: TdmkDBLookupComboBox;
    EdTerceiro: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    EdVendedor: TdmkEditCB;
    Panel6: TPanel;
    RGNivelSel: TRadioGroup;
    QrNivelSel: TmySQLQuery;
    QrNivelSelCodigo: TIntegerField;
    QrNivelSelNome: TWideStringField;
    DsNivelSel: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasForneceI: TIntegerField;
    DsCarteiras: TDataSource;
    LaNivelSel: TLabel;
    EdNivelSel: TdmkEditCB;
    CBNivelSel: TdmkDBLookupComboBox;
    PainelB: TPanel;
    RGTipo: TRadioGroup;
    PainelC: TPanel;
    PnDatas2: TPanel;
    GBVencto: TGroupBox;
    LaVenctI: TLabel;
    LaVenctF: TLabel;
    TPVctoIni: TdmkEditDateTimePicker;
    TPVctoFim: TdmkEditDateTimePicker;
    CkVencto: TCheckBox;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    TPDocIni: TdmkEditDateTimePicker;
    TPDocFim: TdmkEditDateTimePicker;
    CkDataDoc: TCheckBox;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    TPCompIni: TdmkEditDateTimePicker;
    TPCompFim: TdmkEditDateTimePicker;
    CkDataComp: TCheckBox;
    PnDatas1: TPanel;
    GroupBox4: TGroupBox;
    Label101: TLabel;
    Label102: TLabel;
    TPEmissIni: TdmkEditDateTimePicker;
    TPEmissFim: TdmkEditDateTimePicker;
    CkEmissao: TCheckBox;
    PnFluxo: TPanel;
    Label4: TLabel;
    Label7: TLabel;
    GroupBox5: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    TPCre: TDateTimePicker;
    TPDeb: TDateTimePicker;
    Label8: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    PainelE: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    CkOrdem1: TCheckBox;
    Panel7: TPanel;
    CkOrdem2: TCheckBox;
    RGHistorico: TRadioGroup;
    RGFonte: TRadioGroup;
    PainelDados: TPanel;
    CkGrade: TCheckBox;
    EdA: TdmkEdit;
    EdZ: TdmkEdit;
    CkNiveis: TCheckBox;
    GBOmiss: TGroupBox;
    CkAnos: TCheckBox;
    CkMeses: TCheckBox;
    CkDias: TCheckBox;
    EdAnos: TdmkEdit;
    EdMeses: TdmkEdit;
    EdDias: TdmkEdit;
    BtSalvar: TBitBtn;
    CkOmiss: TCheckBox;
    GBPerdido: TGroupBox;
    CkAnos2: TCheckBox;
    CkMeses2: TCheckBox;
    CkDias2: TCheckBox;
    EdAnos2: TdmkEdit;
    EdMeses2: TdmkEdit;
    EdDias2: TdmkEdit;
    BtGravar: TBitBtn;
    CkSubstituir: TCheckBox;
    QrExtrato: TmySQLQuery;
    QrExtratoNOMECONTA: TWideStringField;
    QrExtratoNOMECARTEIRA: TWideStringField;
    QrExtratoData: TDateField;
    QrExtratoTipo: TSmallintField;
    QrExtratoCarteira: TIntegerField;
    QrExtratoControle: TIntegerField;
    QrExtratoSub: TSmallintField;
    QrExtratoAutorizacao: TIntegerField;
    QrExtratoGenero: TIntegerField;
    QrExtratoDescricao: TWideStringField;
    QrExtratoNotaFiscal: TIntegerField;
    QrExtratoDebito: TFloatField;
    QrExtratoCredito: TFloatField;
    QrExtratoCompensado: TDateField;
    QrExtratoDocumento: TFloatField;
    QrExtratoSit: TIntegerField;
    QrExtratoVencimento: TDateField;
    QrExtratoLk: TIntegerField;
    QrExtratoFatID: TIntegerField;
    QrExtratoFatParcela: TIntegerField;
    QrExtratoID_Pgto: TIntegerField;
    QrExtratoID_Sub: TSmallintField;
    QrExtratoFatura: TWideStringField;
    QrExtratoBanco: TIntegerField;
    QrExtratoLocal: TIntegerField;
    QrExtratoCartao: TIntegerField;
    QrExtratoLinha: TIntegerField;
    QrExtratoOperCount: TIntegerField;
    QrExtratoLancto: TIntegerField;
    QrExtratoPago: TFloatField;
    QrExtratoMez: TIntegerField;
    QrExtratoFornecedor: TIntegerField;
    QrExtratoCliente: TIntegerField;
    QrExtratoMoraDia: TFloatField;
    QrExtratoMulta: TFloatField;
    QrExtratoProtesto: TDateField;
    QrExtratoDataCad: TDateField;
    QrExtratoDataAlt: TDateField;
    QrExtratoUserCad: TSmallintField;
    QrExtratoUserAlt: TSmallintField;
    QrExtratoDataDoc: TDateField;
    QrExtratoCtrlIni: TIntegerField;
    QrExtratoNivel: TIntegerField;
    QrExtratoVendedor: TIntegerField;
    QrExtratoAccount: TIntegerField;
    QrExtratoQtde: TFloatField;
    QrFlxo: TmySQLQuery;
    QrFlxoData: TDateField;
    QrFlxoCarteira: TIntegerField;
    QrFlxoControle: TIntegerField;
    QrFlxoSub: TSmallintField;
    QrFlxoQtde: TFloatField;
    QrFlxoDescricao: TWideStringField;
    QrFlxoNotaFiscal: TIntegerField;
    QrFlxoDebito: TFloatField;
    QrFlxoCredito: TFloatField;
    QrFlxoCompensado: TDateField;
    QrFlxoSerieCH: TWideStringField;
    QrFlxoDocumento: TFloatField;
    QrFlxoVencimento: TDateField;
    QrFlxoID_Pgto: TIntegerField;
    QrFlxoNOMECART: TWideStringField;
    QrFlxoNOMERELACIONADO: TWideStringField;
    QrFlxoQUITACAO: TDateField;
    DsFlxo: TDataSource;
    QrInicial: TmySQLQuery;
    QrInicialSdoFimB: TFloatField;
    QrAnterior: TmySQLQuery;
    QrAnteriorMovimento: TFloatField;
    frxFin_Relat_005_00: TfrxReport;
    frxDsExtrato: TfrxDBDataset;
    PB1: TProgressBar;
    frxDsExtr: TfrxDBDataset;
    QrExtr: TmySQLQuery;
    QrExtratoDataE: TDateField;
    QrExtratoDataV: TDateField;
    QrExtratoDataQ: TDateField;
    QrExtratoDataX: TDateField;
    QrExtratoTexto: TWideStringField;
    QrExtratoDocum: TWideStringField;
    QrExtratoNotaF: TWideStringField;
    QrExtratoSaldo: TFloatField;
    QrExtrCredi: TFloatField;
    QrExtrDebit: TFloatField;
    QrExtratoCartC: TIntegerField;
    QrExtratoCartN: TWideStringField;
    QrExtratoCodig: TIntegerField;
    QrExtratoCtrle: TIntegerField;
    QrExtratoCtSub: TIntegerField;
    QrExtratoCART_ORIG: TWideStringField;
    QrExtratoID_Pg: TIntegerField;
    QrExtratoTipoI: TIntegerField;
    QrExtrVALOR: TFloatField;
    DsExtr: TDataSource;
    frxFin_Relat_005_01_A06: TfrxReport;
    frxFin_Relat_005_01_A07: TfrxReport;
    frxFin_Relat_005_01_A08: TfrxReport;
    frxDsFluxo: TfrxDBDataset;
    frxDsPagRec: TfrxDBDataset;
    QrPagRec1: TmySQLQuery;
    QrPagRec1PAGO_ROLADO: TWideStringField;
    QrPagRec1DEVIDO: TFloatField;
    QrPagRec1PEND_TOTAL: TFloatField;
    QrPagRec1Data: TDateField;
    QrPagRec1DataDoc: TDateField;
    QrPagRec1Vencimento: TDateField;
    QrPagRec1TERCEIRO: TFloatField;
    QrPagRec1NOME_TERCEIRO: TWideStringField;
    QrPagRec1NOMEVENCIDO: TWideStringField;
    QrPagRec1NOMECONTA: TWideStringField;
    QrPagRec1Descricao: TWideStringField;
    QrPagRec1CtrlPai: TFloatField;
    QrPagRec1Tipo: TFloatField;
    QrPagRec1Documento: TFloatField;
    QrPagRec1NotaFiscal: TFloatField;
    QrPagRec1Valor: TFloatField;
    QrPagRec1MoraDia: TFloatField;
    QrPagRec1PAGO_REAL: TFloatField;
    QrPagRec1PENDENTE: TFloatField;
    QrPagRec1ATUALIZADO: TFloatField;
    QrPagRec1MULTA_REAL: TFloatField;
    QrPagRec1ATRAZODD: TFloatField;
    QrPagRec1ID_Pgto: TFloatField;
    QrPagRec1CtrlIni: TFloatField;
    QrPagRec1Controle: TFloatField;
    QrPagRec1Duplicata: TWideStringField;
    QrPagRec1Qtde: TFloatField;
    QrPagRec1NO_UH: TWideStringField;
    QrPagRecX: TmySQLQuery;
    QrPagRecXVALOR: TFloatField;
    QrPagRecXPAGO_REAL: TFloatField;
    QrPagRecXNOMEVENCIDO: TWideStringField;
    StringField7: TWideStringField;
    StringField8: TWideStringField;
    QrPagRecXATRAZODD: TFloatField;
    QrPagRecXATUALIZADO: TFloatField;
    QrPagRecXMULTA_REAL: TFloatField;
    QrPagRecXVENCER: TDateField;
    QrPagRecXVENCIDO: TDateField;
    QrPagRecXPENDENTE: TFloatField;
    QrPagRecXData: TDateField;
    QrPagRecXTipo: TSmallintField;
    QrPagRecXCarteira: TIntegerField;
    QrPagRecXSub: TSmallintField;
    QrPagRecXAutorizacao: TIntegerField;
    QrPagRecXGenero: TIntegerField;
    QrPagRecXDescricao: TWideStringField;
    QrPagRecXNotaFiscal: TIntegerField;
    QrPagRecXDebito: TFloatField;
    QrPagRecXCredito: TFloatField;
    QrPagRecXCompensado: TDateField;
    QrPagRecXDocumento: TFloatField;
    QrPagRecXSit: TIntegerField;
    QrPagRecXVencimento: TDateField;
    QrPagRecXLk: TIntegerField;
    QrPagRecXFatID: TIntegerField;
    QrPagRecXFatParcela: TIntegerField;
    QrPagRecXID_Sub: TSmallintField;
    QrPagRecXFatura: TWideStringField;
    QrPagRecXBanco: TIntegerField;
    QrPagRecXLocal: TIntegerField;
    QrPagRecXCartao: TIntegerField;
    QrPagRecXLinha: TIntegerField;
    QrPagRecXOperCount: TIntegerField;
    QrPagRecXLancto: TIntegerField;
    QrPagRecXPago: TFloatField;
    QrPagRecXFornecedor: TIntegerField;
    QrPagRecXCliente: TIntegerField;
    QrPagRecXMoraDia: TFloatField;
    QrPagRecXMulta: TFloatField;
    QrPagRecXProtesto: TDateField;
    QrPagRecXDataCad: TDateField;
    QrPagRecXDataAlt: TDateField;
    QrPagRecXUserCad: TSmallintField;
    QrPagRecXUserAlt: TSmallintField;
    QrPagRecXDataDoc: TDateField;
    QrPagRecXNivel: TIntegerField;
    QrPagRecXVendedor: TIntegerField;
    QrPagRecXAccount: TIntegerField;
    QrPagRecXNOMECONTA: TWideStringField;
    QrPagRecXNOMECARTEIRA: TWideStringField;
    QrPagRecXSALDO: TFloatField;
    QrPagRecXEhCRED: TWideStringField;
    QrPagRecXEhDEB: TWideStringField;
    QrPagRecXNOME_TERCEIRO: TWideStringField;
    QrPagRecXControle: TIntegerField;
    QrPagRecXID_Pgto: TIntegerField;
    QrPagRecXMez: TIntegerField;
    QrPagRecXCtrlIni: TIntegerField;
    QrPagRecXDuplicata: TWideStringField;
    QrPagRecXFatID_Sub: TIntegerField;
    QrPagRecXICMS_P: TFloatField;
    QrPagRecXICMS_V: TFloatField;
    QrPagRecXQtde: TFloatField;
    QrPagRecXNO_UH: TWideStringField;
    QrPagRecXTERCEIRO: TIntegerField;
    QrPagRecIts: TmySQLQuery;
    StringField5: TWideStringField;
    QrPagRecItsNOMEVENCIDO: TWideStringField;
    QrPagRecItsVALOR: TFloatField;
    QrPagRecItsPAGO_REAL: TFloatField;
    QrPagRecItsATRAZODD: TFloatField;
    QrPagRecItsATUALIZADO: TFloatField;
    QrPagRecItsMULTA_REAL: TFloatField;
    QrPagRecItsVENCER: TDateField;
    QrPagRecItsVENCIDO: TDateField;
    QrPagRecItsData: TDateField;
    QrPagRecItsTipo: TSmallintField;
    QrPagRecItsCarteira: TIntegerField;
    QrPagRecItsSub: TSmallintField;
    QrPagRecItsAutorizacao: TIntegerField;
    QrPagRecItsGenero: TIntegerField;
    QrPagRecItsDescricao: TWideStringField;
    QrPagRecItsNotaFiscal: TIntegerField;
    QrPagRecItsDebito: TFloatField;
    QrPagRecItsCredito: TFloatField;
    QrPagRecItsCompensado: TDateField;
    QrPagRecItsDocumento: TFloatField;
    QrPagRecItsSit: TIntegerField;
    QrPagRecItsVencimento: TDateField;
    QrPagRecItsLk: TIntegerField;
    QrPagRecItsFatID: TIntegerField;
    QrPagRecItsFatParcela: TIntegerField;
    QrPagRecItsID_Sub: TSmallintField;
    QrPagRecItsFatura: TWideStringField;
    QrPagRecItsBanco: TIntegerField;
    QrPagRecItsLocal: TIntegerField;
    QrPagRecItsCartao: TIntegerField;
    QrPagRecItsLinha: TIntegerField;
    QrPagRecItsOperCount: TIntegerField;
    QrPagRecItsLancto: TIntegerField;
    QrPagRecItsPago: TFloatField;
    QrPagRecItsFornecedor: TIntegerField;
    QrPagRecItsCliente: TIntegerField;
    QrPagRecItsMoraDia: TFloatField;
    QrPagRecItsMulta: TFloatField;
    QrPagRecItsProtesto: TDateField;
    QrPagRecItsDataCad: TDateField;
    QrPagRecItsDataAlt: TDateField;
    QrPagRecItsUserCad: TSmallintField;
    QrPagRecItsUserAlt: TSmallintField;
    QrPagRecItsDataDoc: TDateField;
    QrPagRecItsNivel: TIntegerField;
    QrPagRecItsVendedor: TIntegerField;
    QrPagRecItsAccount: TIntegerField;
    QrPagRecItsNOMECONTA: TWideStringField;
    QrPagRecItsNOMECARTEIRA: TWideStringField;
    QrPagRecItsSALDO: TFloatField;
    QrPagRecItsEhCRED: TWideStringField;
    QrPagRecItsEhDEB: TWideStringField;
    QrPagRecItsNOME_TERCEIRO: TWideStringField;
    QrPagRecItsControle: TIntegerField;
    QrPagRecItsID_Pgto: TIntegerField;
    QrPagRecItsMez: TIntegerField;
    QrPagRecItsCtrlIni: TIntegerField;
    QrPagRecItsNO_UH: TWideStringField;
    QrPagRecItsTERCEIRO: TLargeintField;
    QrPagRec: TmySQLQuery;
    QrPagRecData: TDateField;
    QrPagRecTipo: TSmallintField;
    QrPagRecCarteira: TIntegerField;
    QrPagRecControle: TIntegerField;
    QrPagRecSub: TSmallintField;
    QrPagRecAutorizacao: TIntegerField;
    QrPagRecGenero: TIntegerField;
    QrPagRecDescricao: TWideStringField;
    QrPagRecNotaFiscal: TIntegerField;
    QrPagRecDebito: TFloatField;
    QrPagRecCredito: TFloatField;
    QrPagRecCompensado: TDateField;
    QrPagRecDocumento: TFloatField;
    QrPagRecSit: TIntegerField;
    QrPagRecVencimento: TDateField;
    QrPagRecLk: TIntegerField;
    QrPagRecFatID: TIntegerField;
    QrPagRecFatParcela: TIntegerField;
    QrPagRecID_Pgto: TIntegerField;
    QrPagRecID_Sub: TSmallintField;
    QrPagRecFatura: TWideStringField;
    QrPagRecBanco: TIntegerField;
    QrPagRecLocal: TIntegerField;
    QrPagRecCartao: TIntegerField;
    QrPagRecLinha: TIntegerField;
    QrPagRecOperCount: TIntegerField;
    QrPagRecLancto: TIntegerField;
    QrPagRecPago: TFloatField;
    QrPagRecMez: TIntegerField;
    QrPagRecFornecedor: TIntegerField;
    QrPagRecCliente: TIntegerField;
    QrPagRecMoraDia: TFloatField;
    QrPagRecMulta: TFloatField;
    QrPagRecProtesto: TDateField;
    QrPagRecDataCad: TDateField;
    QrPagRecDataAlt: TDateField;
    QrPagRecUserCad: TSmallintField;
    QrPagRecUserAlt: TSmallintField;
    QrPagRecDataDoc: TDateField;
    QrPagRecCtrlIni: TIntegerField;
    QrPagRecNivel: TIntegerField;
    QrPagRecVendedor: TIntegerField;
    QrPagRecAccount: TIntegerField;
    QrPagRecNOMECONTA: TWideStringField;
    QrPagRecNOMECARTEIRA: TWideStringField;
    QrPagRecSALDO: TFloatField;
    QrPagRecEhCRED: TWideStringField;
    QrPagRecEhDEB: TWideStringField;
    QrPagRecNOME_TERCEIRO: TWideStringField;
    QrPagRecVENCER: TDateField;
    QrPagRecVALOR: TFloatField;
    QrPagRecVENCIDO: TDateField;
    QrPagRecATRAZODD: TFloatField;
    QrPagRecMULTA_REAL: TFloatField;
    QrPagRecATUALIZADO: TFloatField;
    QrPagRecPAGO_REAL: TFloatField;
    QrPagRecNOMEVENCIDO: TWideStringField;
    QrPagRecFatID_Sub: TIntegerField;
    QrPagRecICMS_P: TFloatField;
    QrPagRecICMS_V: TFloatField;
    QrPagRecDuplicata: TWideStringField;
    QrPagRecQtde: TFloatField;
    QrPagRecNO_UH: TWideStringField;
    QrPagRecSERIEDOC: TWideStringField;
    QrPagRecSerieCH: TWideStringField;
    QrPagRecTERCEIRO: TLargeintField;
    frxDsPagRec1: TfrxDBDataset;
    frxFin_Relat_005_02_A06: TfrxReport;
    frxFin_Relat_005_02_A07: TfrxReport;
    frxFin_Relat_005_02_B1: TfrxReport;
    frxFin_Relat_005_02_B2: TfrxReport;
    frxFin_Relat_005_02_A08: TfrxReport;
    LaIndiPag: TLabel;
    EdIndiPag: TdmkEditCB;
    CBIndiPag: TdmkDBLookupComboBox;
    QrEmpresas: TmySQLQuery;
    QrEmpresasCodCliInt: TIntegerField;
    QrEmpresasCodEnti: TIntegerField;
    QrEmpresasCodFilial: TIntegerField;
    QrEmpresasNome: TWideStringField;
    TabSheet3: TTabSheet;
    Memo1: TMemo;
    Panel8: TPanel;
    GroupBox6: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    CBMesIni: TComboBox;
    CBAnoIni: TComboBox;
    CBMesFim: TComboBox;
    CBAnoFim: TComboBox;
    CkCompetencia: TCheckBox;
    CkExclusivo: TCheckBox;
    Panel9: TPanel;
    TabSheet4: TTabSheet;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    RecIts: TTabSheet;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    DsPagRec: TDataSource;
    DsPagRecIts: TDataSource;
    DsPagRecX: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure DockTabSet1DockDrop(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer);
    procedure DockTabSet1TabAdded(Sender: TObject);
    procedure DockTabSet1TabRemoved(Sender: TObject);
    procedure pDockLeftDockDrop(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer);
    procedure pDockLeftDockOver(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure pDockLeftUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtReexibeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure pDockLeftDblClick(Sender: TObject);
    procedure RGNivelSelClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure CkCompetenciaClick(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure CkOmissClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure BtGravarClick(Sender: TObject);
    procedure frxFin_Relat_005_00GetValue(const VarName: string;
      var Value: Variant);
    procedure frxFin_Relat_005_02_B1GetValue(const VarName: string;
      var Value: Variant);
    procedure QrTerceirosCheckSQLField(Sender: TObject; const FieldName: string;
      var Allow: Boolean);
    procedure QrPagRec1CalcFields(DataSet: TDataSet);
    procedure QrPagRecXCalcFields(DataSet: TDataSet);
    procedure QrPagRecItsCalcFields(DataSet: TDataSet);
    procedure QrPagRecCalcFields(DataSet: TDataSet);
    procedure QrExtrCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FCliInt, FEntidade: Integer;
    FTabLctA, FTabLctB, FTabLctD, FEntidade_TXT: String;
    FDtEncer, FDtMorto: TDateTime;
    FPreparou: Boolean;
    // Dock Forms
    FInCreation: Boolean;
    procedure RecriaDockForms();
    procedure CreateDockableWindows();
    // Fim Dock Forms
    procedure ReopenNivelSel();
    function NomeDoNivelSelecionado(Tipo: Integer): String;

  public
    { Public declarations }
    procedure AtivaBtConfirma();
    procedure R00_ImprimeExtratoConsolidado();
    procedure R01_ImprimeFluxoDeCaixa();
    procedure R02_ImprimeContasAPagar(Sender: TObject(*; Saida: TTipoSaidaRelatorio*));
    procedure SQLContasAPagar(Query: TmySQLQuery; Tipo: Integer);
    function SQL_Ordenar(): String;
  end;

  var
  FmAPagRec: TFmAPagRec;
  const
  FDockPanelWidth = 411;

implementation

uses UnMyObjects, Module, UCreate, CreateGeral, UCreateFin, ModuleGeral,
// DockForms
_Empresas, _Indi_Pags, _Pla_Ctas, DockForm,
// fim DockForms
UMySQLModule, UnFinanceiro, ModuleFin;
{$R *.DFM}

var
  Ext_EmisI, Ext_EmisF, Ext_VctoI, Ext_VctoF, Ext_DataH, Ext_DataA,
  Ext_DocI,  Ext_DocF,  Ext_CompI, Ext_CompF: String;
  Ext_Saldo, Ext_SdIni,
  Ext_VALORA,     Ext_VALORB,     Ext_VALORC,
  Ext_PENDENTEA,  Ext_PENDENTEB,  Ext_PENDENTEC,
  Ext_ATUALIZADOA, Ext_ATUALIZADOB, Ext_ATUALIZADOC,
  Ext_DEVIDOA, Ext_DEVIDOB, Ext_DEVIDOC,
  Ext_PAGO_REALA, Ext_PAGO_REALB, Ext_PAGO_REALC: Double;
  Ext_Vencto: TDate;
  DockWindows: array[0..0] of TFmDockForm;

const
  MaxJan = 3;
  Janelas: array [0..MaxJan-1] of String = ('Empresas', 'Indica��es',
  'Plano de Contas');

procedure TFmAPagRec.AtivaBtConfirma();
begin
  // Quando fecha as filhas fecham depois e geram um erro silencioso
  try
{
  BtConfirma.Enabled :=
    (RGTipoSaldo.ItemIndex > -1) and
    //(RGOrdemAgr.ItemIndex > -1) and
    (RGOrdemIts.ItemIndex > -1) and
    (RGAgrupa.ItemIndex > -1);
}
  finally
    ;
  end;
end;

procedure TFmAPagRec.BtConfirmaClick(Sender: TObject);
begin
  Memo1.Lines.Clear;
  Memo1.Lines.Add('/*' + Geral.FDT(Now(), 0) + '*/');
  QrEmpresas.Close;
  if EdEmpresa.Enabled and (EdEmpresa.ValueVariant <> 0) then
  begin
    //QrEmpresas.Database := DMod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresas, Dmod.MyDB, [
    'SELECT eci.CodCliInt, eci.CodEnti, eci.CodFilial, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, Nome) Nome',
    'FROM enticliint eci',
    'LEFT JOIN entidades ent ON eci.CodEnti=ent.Codigo',
    'WHERE eci.CodCliInt=' + FormatFloat('0', EdEmpresa.ValueVariant)]);
  end else begin
    //QrEmpresas.Database := DModG.MyPID_DB;
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresas, DModG.MyPID_DB, [
    'SELECT CodCliInt, CodEnti, CodFilial, Nome',
    'FROM ' + TAB_TMP_EMPRESAS,
    'WHERE Ativo=1']);
  end;
  if MyObjects.FIC(QrEmpresas.RecordCount = 0, nil,
    'Nenhuma empresa foi definida!') then Exit;
  //

  //
  VAR_PARAR := False;
  Ext_VALORA := 0;
  Ext_VALORB := 0;
  Ext_VALORC := 0;
  Ext_PENDENTEA := 0;
  Ext_PENDENTEB := 0;
  Ext_PENDENTEC := 0;
  Ext_ATUALIZADOA := 0;
  Ext_ATUALIZADOB := 0;
  Ext_ATUALIZADOC := 0;
  Ext_DEVIDOA := 0;
  Ext_DEVIDOB := 0;
  Ext_DEVIDOC := 0;
  Ext_PAGO_REALA := 0;
  Ext_PAGO_REALB := 0;
  Ext_PAGO_REALC := 0;
  if (RGOrdem1.Enabled) and (RGOrdem1.ItemIndex = -1) then
  begin
    Geral.MB_Aviso('Defina a "Ordem 1"!');
    Exit;
  end;
  if (RGOrdem2.Enabled) and (RGOrdem2.ItemIndex = -1) then
  begin
    Geral.MB_Aviso('Defina uma "Ordem 2"!');
    Exit;
  end;
  dmkPF.ExcluiSQLsDermatek();

  if MyObjects.FIC(CkEmissao.Checked and (TPEmissFim.Date < TPEmissIni.Date), nil,
  '"Data de emiss�o" final menor que inicial!') then
    Exit;
  if MyObjects.FIC(CkVencto.Checked and (TPVctoFim.Date < TPVctoIni.Date), nil,
    '"Data de vencimento" final menor que inicial!') then
    Exit;
  if MyObjects.FIC(CkDataDoc.Checked and (TPDocFim.Date < TPDocIni.Date), nil,
    '"Data do documento" final menor que inicial!') then
    Exit;
  Ext_Saldo := 0;
  Ext_EmisI := FormatDateTime(VAR_FORMATDATE, TPEmissIni.Date);
  Ext_EmisF := FormatDateTime(VAR_FORMATDATE, TPEmissFim.Date);
  Ext_VctoI := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date);
  Ext_VctoF := FormatDateTime(VAR_FORMATDATE, TPVctoFim.Date);
  Ext_CompI := FormatDateTime(VAR_FORMATDATE, TPCompIni.Date);
  Ext_CompF := FormatDateTime(VAR_FORMATDATE, TPCompFim.Date);
  Ext_DocI  := FormatDateTime(VAR_FORMATDATE, TPDocIni.Date);
  Ext_DocF  := FormatDateTime(VAR_FORMATDATE, TPDocFim.Date);
  Ext_DataH := FormatDateTime(VAR_FORMATDATE, Date);
  Ext_DataA := FormatDateTime(VAR_FORMATDATE, TPEmissIni.Date-1);
  //
{ N�o precisa, � definido antes!
  DModG.ReopenDtEncerMorto(DModG.QrEmpresasFilial.Value);
  FDtEncer := DModG.QrLastEncerData.Value;
  FDtMorto := DModG.QrLastMortoData.Value;
  FTabLctA := DModG.NomeTab(ntLct, False, ttA, FEntidade);
  FTabLctB := DModG.NomeTab(ntLct, False, ttB, FEntidade);
  FTabLctD := DModG.NomeTab(ntLct, False, ttD, FEntidade);
}  //
  case RGTipo.ItemIndex of
{
    0: R00_ImprimeExtratoConsolidado();
    1: R01_ImprimeFluxoDeCaixa();
}
    2: R02_ImprimeContasAPagar(Self);
    3: R02_ImprimeContasAPagar(Self);
    4: R02_ImprimeContasAPagar(Self);
    5: R02_ImprimeContasAPagar(Self);
    6: R02_ImprimeContasAPagar(Self);
    7: R02_ImprimeContasAPagar(Self);
    8: R02_ImprimeContasAPagar(Self);
    9: R02_ImprimeContasAPagar(Self);
    else Geral.MB_Erro('Relat�rio n�o implementado! Solicite � Dermatek');
  end;
end;

procedure TFmAPagRec.BtGravarClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Anos',  'Dermatek\APagRec\Perdidos', EdAnos2.Text,  ktString);
  Geral.WriteAppKeyCU('Meses', 'Dermatek\APagRec\Perdidos', EdMeses2.Text, ktString);
  Geral.WriteAppKeyCU('Dias',  'Dermatek\APagRec\Perdidos', EdDias2.Text,  ktString);
  /////
  Geral.WriteAppKeyCU('Ck_Anos',  'Dermatek\APagRec\Perdidos', CkAnos2.Checked,  ktBoolean);
  Geral.WriteAppKeyCU('Ck_Meses', 'Dermatek\APagRec\Perdidos', CkMeses2.Checked, ktBoolean);
  Geral.WriteAppKeyCU('Ck_Dias',  'Dermatek\APagRec\Perdidos', CkDias2.Checked,  ktBoolean);
end;

procedure TFmAPagRec.BtReexibeClick(Sender: TObject);
begin
  RecriaDockForms();
  //
end;

procedure TFmAPagRec.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAPagRec.BtSalvarClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Anos',  'Dermatek\APagRec\Venctos', EdAnos.Text,  ktString);
  Geral.WriteAppKeyCU('Meses', 'Dermatek\APagRec\Venctos', EdMeses.Text, ktString);
  Geral.WriteAppKeyCU('Dias',  'Dermatek\APagRec\Venctos', EdDias.Text,  ktString);
  /////
  Geral.WriteAppKeyCU('Ck_Anos',  'Dermatek\APagRec\Venctos', CkAnos.Checked,  ktBoolean);
  Geral.WriteAppKeyCU('Ck_Meses', 'Dermatek\APagRec\Venctos', CkMeses.Checked, ktBoolean);
  Geral.WriteAppKeyCU('Ck_Dias',  'Dermatek\APagRec\Venctos', CkDias.Checked,  ktBoolean);
end;

procedure TFmAPagRec.CkCompetenciaClick(Sender: TObject);
begin
  if CkCompetencia.Checked then
  begin
    Label4.Enabled := True;
    Label5.Enabled := True;
    Label6.Enabled := True;
    Label7.Enabled := True;
    CBMesIni.Enabled := True;
    CBMesFim.Enabled := True;
    CBAnoIni.Enabled := True;
    CBAnoFim.Enabled := True;
  end else begin
    Label4.Enabled := False;
    Label5.Enabled := False;
    Label6.Enabled := False;
    Label7.Enabled := False;
    CBMesIni.Enabled := False;
    CBMesFim.Enabled := False;
    CBAnoIni.Enabled := False;
    CBAnoFim.Enabled := False;
  end;
end;

procedure TFmAPagRec.CkOmissClick(Sender: TObject);
begin
  GBOmiss.Visible := CkOmiss.Checked;
end;

procedure TFmAPagRec.CreateDockableWindows;
var
  I: Integer;
begin
  for I := 0 to High(DockWindows) do
  begin
    DockWindows[I]         := TFmDockForm.Create(Application);
    DockWindows[I].Caption := 'C�lculos em Progresso';
    //
//    DockWindows[I].Memo1.Color := Colors[I];
//    DockWindows[I].Memo1.Font.Color := Colors[I] xor $00FFFFFF;
//    DockWindows[I].Memo1.Text := ColStr[I] + ' window ';
  end;
end;

procedure TFmAPagRec.DockTabSet1DockDrop(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer);
begin
  DockTabSet1.Visible := True;
end;

procedure TFmAPagRec.DockTabSet1TabAdded(Sender: TObject);
begin
  DockTabSet1.Visible := DockTabSet1.Tabs.Count > 0;
end;

procedure TFmAPagRec.DockTabSet1TabRemoved(Sender: TObject);
begin
  DockTabSet1.Visible := DockTabSet1.Tabs.Count > 0;
  // N�o fazer!!!
  // RecriaDockForms();
end;

procedure TFmAPagRec.EdEmpresaChange(Sender: TObject);
begin
  FTabLctA := '?';
  FTabLctB := '?';
  FTabLctD := '?';
end;

procedure TFmAPagRec.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := True;
end;

procedure TFmAPagRec.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  PageControl1.ActivePageIndex := 0;
  //
  FInCreation := False;
  FPreparou   := False;
  //
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrIndiPags, DModG.MyPID_DB);
  (* do FmPesquisas. Ver se usa!
  QrLct1.Close;
  QrLct1.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  TPIni.Date := Date -30;
  TPFim.Date := Date;
  *)
  MyObjects.PreencheCBAnoECBMes(CBAnoIni, CBMesIni);
  MyObjects.PreencheCBAnoECBMes(CBAnoFim, CBMesFim);

  //

  TPCre.Date := Int(Date);
  TPDeb.Date := Int(Date);
  //
  RGOrdem1.Items[7] := dmkPF.TxtUH();
  RGOrdem2.Items[7] := dmkPF.TxtUH();
  //
  CreateDockableWindows;
  //
  TPEmissIni.Date  := Date - 60;
  TPEmissFim.Date  := Date;
  TPVctoIni.Date   := Date - 60;
  TPVctoFim.Date   := Date;
  TPDocIni.Date    := Date - 90;
  TPDocFim.Date    := Date + 60;
  TPCompIni.Date   := Date;
  TPCompFim.Date   := Date;
  RGTipo.ItemIndex := 2;
  //
  UnDmkDAC_PF.AbreQuery(QrTerceiros, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrAccounts, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVendedores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  //
  EdAnos.Text := Geral.ReadAppKeyCU('Anos', 'Dermatek\APagRec\Venctos',
                ktString,'0');
  EdMeses.Text := Geral.ReadAppKeyCU('Meses', 'Dermatek\APagRec\Venctos',
                ktString,'0');
  EdDias.Text := Geral.ReadAppKeyCU('Dias', 'Dermatek\APagRec\Venctos',
                ktString,'0');
  CkAnos.Checked := Geral.ReadAppKeyCU('Ck_Anos', 'Dermatek\APagRec\Venctos',
                ktBoolean, False);
  CkMeses.Checked := Geral.ReadAppKeyCU('Ck_Meses', 'Dermatek\APagRec\Venctos',
                ktBoolean, False);
  CkDias.Checked := Geral.ReadAppKeyCU('Ck_Dias', 'Dermatek\APagRec\Venctos',
                ktBoolean, False);
  ///////
  EdAnos2.Text := Geral.ReadAppKeyCU('Anos', 'Dermatek\APagRec\Perdidos',
                ktString,'0');
  EdMeses2.Text := Geral.ReadAppKeyCU('Meses', 'Dermatek\APagRec\Perdidos',
                ktString,'0');
  EdDias2.Text := Geral.ReadAppKeyCU('Dias', 'Dermatek\APagRec\Perdidos',
                ktString,'0');
  CkAnos2.Checked := Geral.ReadAppKeyCU('Ck_Anos', 'Dermatek\APagRec\Perdidos',
                ktBoolean, False);
  CkMeses2.Checked := Geral.ReadAppKeyCU('Ck_Meses', 'Dermatek\APagRec\Perdidos',
                ktBoolean, False);
  CkDias2.Checked := Geral.ReadAppKeyCU('Ck_Dias', 'Dermatek\APagRec\Perdidos',
                ktBoolean, False);
  //
  QrExtrato.Close;
  QrExtrato.Database := DModG.MyPID_DB;
  //
  QrFlxo.Close;
  QrFlxo.Database := DModG.MyPID_DB;
  //
  QrPagRec.Close;
  QrPagRec.Database := DModG.MyPID_DB;
  //
  QrPagRecX.Close;
  QrPagRecX.Database := DModG.MyPID_DB;
  //
  QrPagRecIts.Close;
  QrPagRecIts.Database := DModG.MyPID_DB;
  //
  (* Definido ao usar
  QrEmpresas.Close;
  QrEmpresas.Database := DModG.MyPID_DB;
  *)
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmAPagRec.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAPagRec.frxFin_Relat_005_00GetValue(const VarName: string;
  var Value: Variant);
var
  Liga: String;
begin
  if VarName = 'SUBST' then
  begin
    if CkSubstituir.Checked then
      Value := 'Cliente / Fornecedor'
    else
      Value := 'Descri��o';
  end;
  if VarName = 'REPRESENTANTE' then
  begin
    if EdAccount.ValueVariant = 0 then Value := 'TODOS'
    else Value := CBAccount.Text;
  end else if VarName = 'CLI_INT' then
    Value := CBEmpresa.Text
  else if VarName = 'VENDEDOR' then
  begin
    if EdVendedor.ValueVariant = 0 then Value := 'TODOS'
    else Value := CBVendedor.Text;
  end else if VarName = 'TITU_NIVEL' then
  begin
    if RGNivelSel.ItemIndex = 0 then Value := ' '
    else Value := RGNivelSel.Items[RGNivelSel.ItemIndex];
  end else if VarName = 'NOME_NIVEL' then
  begin
    if EdNivelSel.ValueVariant = 0 then Value := 'TUDO'
    else Value := Geral.FF0(EdNivelSel.ValueVariant) + ' - ' + CBNivelSel.Text;
  end;
  ////////////////////////////////////////
  if VarName = 'VALOR_A' then Value := Ext_VALORA;
  if VarName = 'VALOR_B' then Value := Ext_VALORB;
  if VarName = 'VALOR_C' then Value := Ext_VALORC / 2; // two pass
  ///////////////////////////////////////
  if VarName = 'PAGO_REAL_A' then Value := Ext_PAGO_REALA;
  if VarName = 'PAGO_REAL_B' then Value := Ext_PAGO_REALB;
  if VarName = 'PAGO_REAL_C' then Value := Ext_PAGO_REALC / 2;
  ///////////////////////////////////////
  if VarName = 'ATUALIZADO_A' then Value := Ext_ATUALIZADOA;
  if VarName = 'ATUALIZADO_B' then Value := Ext_ATUALIZADOB;
  if VarName = 'ATUALIZADO_C' then Value := Ext_ATUALIZADOC / 2;
  ///////////////////////////////////////
  if VarName = 'DEVIDO_A' then Value := Ext_DEVIDOA;
  if VarName = 'DEVIDO_B' then Value := Ext_DEVIDOB;
  if VarName = 'DEVIDO_C' then Value := Ext_DEVIDOC / 2;
  ///////////////////////////////////////
  if VarName = 'PENDENTE_A' then Value := Ext_PENDENTEA;
  if VarName = 'PENDENTE_B' then Value := Ext_PENDENTEB;
  if VarName = 'PENDENTE_C' then Value := Ext_PENDENTEC / 2;
  ///////////////////////////////////////

  if VarName = 'GRUPO1' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRecNO_UH.Value;
    end;
  end;
  if VarName = 'GRUPO2' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRecNO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOA' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOB' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOC' then
  begin
    Value := 'geral';
  end;
  if VarName = 'OMITIDOS' then
  begin
    if CkOmiss.Checked then Value := 'Omitidos valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else if RGTipo.ItemIndex in ([8,9]) then Value :=
      'Somente valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else Value := '  ';
  end else
  if VarName = 'NIVEIS' then
  begin
    if CkNiveis.Checked then Value := 'N�veis '+EdA.Text+' a '+EdZ.Text
    else Value := '  ';
  end else
  if VarName = 'TERCEIRO' then
  begin
    if RGTipo.ItemIndex = 2 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 3 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 4 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 5 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 6 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 7 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 8 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 9 then Value := 'Cliente: ';
  end else
  if VarName = 'INICIAL' then
  begin
    Value     := Ext_SdIni;
    Ext_Saldo := Ext_SdIni;
  end else
  if VarName = 'NOMEREL' then
  begin
    case RGTipo.ItemIndex of
      1: Value := 'RELAT�RIO FLUXO DE CAIXA';
      2: Value := 'RELAT�RIO DE CONTAS A PAGAR';
      3: Value := 'RELAT�RIO DE CONTAS A RECEBER';
      4: Value := 'RELAT�RIO DE CONTAS PAGAS';
      5: Value := 'RELAT�RIO DE CONTAS RECEBIDAS';
      6: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A PAGAR';
      7: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A RECEBER';
      8: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A PAGAR';
      9: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A RECEBER';
    end;
  end;

  if VarName = 'SALDO' then
  begin
    Value := QrExtratoCredito.Value - QrExtratoDebito.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'SALDO2' then
  begin
    Value := 0(*QrFluxoSALDO.Value*);
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'SALDO4' then
  begin
    Value := QrPagRecSALDO.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'FORNECEDOR' then
    if EdTerceiro.ValueVariant = 0 then Value := 'TODOS' else
    Value := EdTerceiro.Text + ' - ' + CBTerceiro.Text + ' ' +
    QrTerceirosTELEFONES.Text;
  if VarName = 'SALDODIA' then
    Value := Ext_Saldo;
  if VarName = 'PERIODO' then
  begin
    Value := '';
    if not CkEmissao.Checked and not CkVencto.Checked
    and not CkDataDoc.Checked then Value := 'N�o definido';
    if CkEmissao.Checked and CkVencto.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkEmissao.Checked then
      Value := 'Emiss�o de '+
        FormatDateTime(VAR_FORMATDATE3, TPEmissIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPEmissFim.Date) + Liga;
    if (CkEmissao.Checked or CkVencto.Checked) and CkDataDoc.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkVencto.Checked then
      Value := Value + 'Vencimento de '+
        FormatDateTime(VAR_FORMATDATE3, TPVctoIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPVctoFim.Date) + Liga;
    if CkDataDoc.Checked then
      Value := Value + 'Datadoc de '+
        FormatDateTime(VAR_FORMATDATE3, TPDocIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPDocFim.Date);
  end else
  if VarName = 'VAR_NOMEEMPRESA' then
    Value := CBEmpresa.Text;
end;

procedure TFmAPagRec.frxFin_Relat_005_02_B1GetValue(const VarName: string;
  var Value: Variant);
var
  Liga: String;
begin
  if VarName = 'REPRESENTANTE' then
  begin
    if EdAccount.ValueVariant = 0 then Value := 'TODOS'
    else Value := CBAccount.Text;
  end else if VarName = 'CLI_INT' then
    Value := CBEmpresa.Text
  else if VarName = 'VENDEDOR' then
  begin
    if EdVendedor.ValueVariant = 0 then Value := 'TODOS'
    else Value := CBVendedor.Text;
  end else if VarName = 'TITU_NIVEL' then
  begin
    if RGNivelSel.ItemIndex = 0 then Value := ' '
    else Value := RGNivelSel.Items[RGNivelSel.ItemIndex];
  end else if VarName = 'NOME_NIVEL' then
  begin
    if EdNivelSel.ValueVariant = 0 then Value := 'TUDO'
    else Value := Geral.FF0(EdNivelSel.ValueVariant) + ' - ' + CBNivelSel.Text;
  end;
  ////////////////////////////////////////
  if VarName = 'VALOR_A' then Value := Ext_VALORA;
  if VarName = 'VALOR_B' then Value := Ext_VALORB;
  if VarName = 'VALOR_C' then Value := Ext_VALORC / 2; // two pass
  ///////////////////////////////////////
  if VarName = 'PAGO_REAL_A' then Value := Ext_PAGO_REALA;
  if VarName = 'PAGO_REAL_B' then Value := Ext_PAGO_REALB;
  if VarName = 'PAGO_REAL_C' then Value := Ext_PAGO_REALC / 2;
  ///////////////////////////////////////
  if VarName = 'ATUALIZADO_A' then Value := Ext_ATUALIZADOA;
  if VarName = 'ATUALIZADO_B' then Value := Ext_ATUALIZADOB;
  if VarName = 'ATUALIZADO_C' then Value := Ext_ATUALIZADOC / 2;
  ///////////////////////////////////////
  if VarName = 'DEVIDO_A' then Value := Ext_DEVIDOA;
  if VarName = 'DEVIDO_B' then Value := Ext_DEVIDOB;
  if VarName = 'DEVIDO_C' then Value := Ext_DEVIDOC / 2;
  ///////////////////////////////////////
  if VarName = 'PENDENTE_A' then Value := Ext_PENDENTEA;
  if VarName = 'PENDENTE_B' then Value := Ext_PENDENTEB;
  if VarName = 'PENDENTE_C' then Value := Ext_PENDENTEC / 2;
  ///////////////////////////////////////

  if VarName = 'GRUPO1' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRecNO_UH.Value;
    end;
  end;
  if VarName = 'GRUPO2' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRecNO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOA' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOB' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := dmkPF.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOC' then
  begin
    Value := 'geral';
  end;
  if VarName = 'OMITIDOS' then
  begin
    if CkOmiss.Checked then Value := 'Omitidos valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else if RGTipo.ItemIndex in ([8,9]) then Value :=
      'Somente valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else Value := '  ';
  end;
  if VarName = 'NIVEIS' then
  begin
    if CkNiveis.Checked then Value := 'N�veis '+EdA.Text+' a '+EdZ.Text
    else Value := '  ';
  end;
  if VarName = 'TERCEIRO' then
  begin
    if RGTipo.ItemIndex = 2 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 3 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 4 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 5 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 6 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 7 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 8 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 9 then Value := 'Cliente: ';
  end;
{
  if VarName = 'INICIAL' then
  begin
    Value := Ext_SdIni;
    Ext_Saldo := Ext_SdIni
  end else
}
  if VarName = 'NOMEREL' then
  begin
    case RGTipo.ItemIndex of
      2: Value := 'RELAT�RIO DE CONTAS A PAGAR';
      3: Value := 'RELAT�RIO DE CONTAS A RECEBER';
      4: Value := 'RELAT�RIO DE CONTAS PAGAS';
      5: Value := 'RELAT�RIO DE CONTAS RECEBIDAS';
      6: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A PAGAR';
      7: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A RECEBER';
      8: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A PAGAR';
      9: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A RECEBER';
    end;
  end;

  if VarName = 'SALDO' then
  begin
    Value := QrExtratoCredito.Value - QrExtratoDebito.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'SALDO2' then
  begin
    Value := 0(*QrFluxoSALDO.Value*);
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'SALDO4' then
  begin
    Value := QrPagRecSALDO.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'FORNECEDOR' then
    if EdTerceiro.ValueVariant = 0 then Value := 'TODOS' else
    Value := EdTerceiro.Text + ' - ' + CBTerceiro.Text + ' ' +
      QrTerceirosTELEFONES.Text;
  if VarName = 'SALDODIA' then
    Value := Ext_Saldo;
  if VarName = 'PERIODO' then
  begin
    Value := '';
    if not CkEmissao.Checked and not CkVencto.Checked
    and not CkDataDoc.Checked then Value := 'N�o definido';
    if CkEmissao.Checked and CkVencto.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkEmissao.Checked then
      Value := 'Emiss�o de '+
        FormatDateTime(VAR_FORMATDATE3, TPEmissIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPEmissFim.Date) + Liga;
    if (CkEmissao.Checked or CkVencto.Checked) and CkDataDoc.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkVencto.Checked then
      Value := Value + 'Vencimento de '+
        FormatDateTime(VAR_FORMATDATE3, TPVctoIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPVctoFim.Date) + Liga;
    if CkDataDoc.Checked then
      Value := Value + 'Datadoc de '+
        FormatDateTime(VAR_FORMATDATE3, TPDocIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPDocFim.Date);
  end

  // user function

  else if VarName = 'VFR_CODITION_A' then
  begin
    case RGOrdem1.ItemIndex of
      00: Value := 'frxDsPagRec1."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec1."DataDoc"';
      02: Value := 'frxDsPagRec1."Data"';
      03: Value := 'frxDsPagRec1."Vencimento"';
      04: Value := 'frxDsPagRec1."Documento"';
      05: Value := 'frxDsPagRec1."NotaFiscal"';
      06: Value := 'frxDsPagRec1."Duplicata"';
      07: Value := 'frxDsPagRec1."NO_UH"';
    end;
  end
  else
  if VarName = 'VFR_CODITION_B' then
  begin
    case RGOrdem2.ItemIndex of
      00: Value := 'frxDsPagRec1."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec1."DataDoc"';
      02: Value := 'frxDsPagRec1."Data"';
      03: Value := 'frxDsPagRec1."Vencimento"';
      04: Value := 'frxDsPagRec1."Documento"';
      05: Value := 'frxDsPagRec1."NotaFiscal"';
      06: Value := 'frxDsPagRec1."Duplicata"';
      07: Value := 'frxDsPagRec1."NO_UH"';
    end;
  end else if VarName = 'VFR_CODITION_A1' then
  begin
    case RGOrdem1.ItemIndex of
      00: Value := 'frxDsPagRec."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec."DataDoc"';
      02: Value := 'frxDsPagRec."Data"';
      03: Value := 'frxDsPagRec."Vencimento"';
      04: Value := 'frxDsPagRec."Documento"';
      05: Value := 'frxDsPagRec."NotaFiscal"';
      06: Value := 'frxDsPagRec."Duplicata"';
      07: Value := 'frxDsPagRec."NO_UH"';
    end;
  end
  else
  if VarName = 'VFR_CODITION_B1' then
  begin
    case RGOrdem2.ItemIndex of
      00: Value := 'frxDsPagRec."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec."DataDoc"';
      02: Value := 'frxDsPagRec."Data"';
      03: Value := 'frxDsPagRec."Vencimento"';
      04: Value := 'frxDsPagRec."Documento"';
      05: Value := 'frxDsPagRec."NotaFiscal"';
      06: Value := 'frxDsPagRec."Duplicata"';
      07: Value := 'frxDsPagRec."NO_UH"';
    end;
  end;
  if VarName = 'VFR_ORD' then if EdTerceiro.ValueVariant = 0 then
    Value := False else Value := True;
  if VarName = 'VARF_GRADE' then Value := CkGrade.Checked;
  if VarName = 'VFR_GRUPO1' then
  begin
    if CkOrdem1.Checked then Value := RGOrdem1.ItemIndex else Value := -1;
  end;
  if VarName = 'VFR_GRUPO2' then
  begin
    if CkOrdem2.Checked then Value := RGOrdem2.ItemIndex else Value := -1;
  end;
  if VarName = 'VFR_GRUPOA' then
  begin
    if CkOrdem1.Checked then Value := RGOrdem1.ItemIndex else Value := -1;
    Ext_VALORA := 0;
    Ext_DEVIDOA := 0;
    Ext_PENDENTEA := 0;
    Ext_PAGO_REALA := 0;
    Ext_ATUALIZADOA := 0;
  end;
  if VarName = 'VFR_GRUPOB' then
  begin
    if CkOrdem2.Checked then Value := RGOrdem2.ItemIndex else Value := -1;
    Ext_VALORB := 0;
    Ext_DEVIDOB := 0;
    Ext_PENDENTEB := 0;
    Ext_PAGO_REALB := 0;
    Ext_ATUALIZADOB := 0;
  end;
  (*if VarName = 'VFR_GRUPOC' then
  begin
    Value := 0;
    Ext_ValueORC := 0;
    Ext_DEVIDOC := 0;
    Ext_PENDENTEC := 0;
    Ext_PAGO_REALC := 0;
    Ext_ATUALIZADOC := 0;
  end; *)
  if VarName = 'VFR_ID_PAGTO' then
  begin
    if QrPagRec1CtrlPai.Value = QrPagRec1Controle.Value then
    begin
      Value := 0;
      Ext_VALORA := Ext_VALORA + QrPagRec1VALOR.Value;
      Ext_VALORB := Ext_VALORB + QrPagRec1VALOR.Value;
      Ext_VALORC := Ext_VALORC + QrPagRec1VALOR.Value;
      /////
      Ext_PAGO_REALA := Ext_PAGO_REALA + QrPagRec1PAGO_REAL.Value;
      Ext_PAGO_REALB := Ext_PAGO_REALB + QrPagRec1PAGO_REAL.Value;
      Ext_PAGO_REALC := Ext_PAGO_REALC + QrPagRec1PAGO_REAL.Value;
      /////
      Ext_ATUALIZADOA := Ext_ATUALIZADOA + QrPagRec1ATUALIZADO.Value;
      Ext_ATUALIZADOB := Ext_ATUALIZADOB + QrPagRec1ATUALIZADO.Value;
      Ext_ATUALIZADOC := Ext_ATUALIZADOC + QrPagRec1ATUALIZADO.Value;
      /////
      Ext_DEVIDOA := Ext_DEVIDOA + QrPagRec1DEVIDO.Value;
      Ext_DEVIDOB := Ext_DEVIDOB + QrPagRec1DEVIDO.Value;
      Ext_DEVIDOC := Ext_DEVIDOC + QrPagRec1DEVIDO.Value;
      /////
    end else Value := 1;
    Ext_PENDENTEA := Ext_PENDENTEA + QrPagRec1PEND_TOTAL.Value;
    Ext_PENDENTEB := Ext_PENDENTEB + QrPagRec1PEND_TOTAL.Value;
    Ext_PENDENTEC := Ext_PENDENTEC + QrPagRec1PEND_TOTAL.Value;
  end else
  if VarName = 'VAR_NOMEEMPRESA' then
    Value := CBEmpresa.Text
  else
  if VarName = 'VAR_UH' then
      Value := dmkPF.TxtUH()
  else
end;

function TFmAPagRec.NomeDoNivelSelecionado(Tipo: Integer): String;
begin
  case Tipo of
    1:
    case RGNivelSel.ItemIndex of
      1: Result := 'contas';
      2: Result := 'subgrupos';
      3: Result := 'grupos';
      4: Result := 'conjuntos';
      5: Result := 'plano';
      else Result := '?';
    end;
    2:
    case RGNivelSel.ItemIndex of
      1: Result := 'lct.Genero';
      2: Result := 'co.Subgrupo';
      3: Result := 'sg.Grupo';
      4: Result := 'gr.Conjunto';
      5: Result := 'cj.Plano';
      else Result := '?';
    end;
    3:
    case RGNivelSel.ItemIndex of
      1: Result := 'da Conta';
      2: Result := 'do Sub-grupo';
      3: Result := 'do Grupo';
      4: Result := 'do Conjunto';
      5: Result := 'do Plano';
      else Result := ' do Nivel ? ';
    end;
    else Result := '(Tipo)=?';
  end;
end;

procedure TFmAPagRec.pDockLeftDblClick(Sender: TObject);
begin
  ShowMessage('pDockLeft');
end;

procedure TFmAPagRec.pDockLeftDockDrop(Sender: TObject; Source: TDragDockObject;
  X, Y: Integer);
begin
  if pDockLeft.Width = 0 then
    pDockLeft.Width := FDockPanelWidth;
  Splitter1.Visible := True;
  Splitter1.Left := pDockLeft.Width;
end;

procedure TFmAPagRec.pDockLeftDockOver(Sender: TObject; Source: TDragDockObject;
  X, Y: Integer; State: TDragState; var Accept: Boolean);
var
  lRect: TRect;
begin
  Accept := Source.Control is TFm_Empresas;
  if Accept then
  begin
    lRect.TopLeft := pDockLeft.ClientToScreen(Point(0, 0));
    lRect.BottomRight := pDockLeft.ClientToScreen(Point(FDockPanelWidth, pDockLeft.Height));
    Source.DockRect := lRect;
  end;
end;

procedure TFmAPagRec.pDockLeftUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
  if pDockLeft.DockClientCount = 1 then
  begin
    pDockLeft.Width := 0;
    Splitter1.Visible := False;
  end;
end;

procedure TFmAPagRec.QrExtrCalcFields(DataSet: TDataSet);
begin
  QrExtrVALOR.Value := QrExtrCredi.Value - QrExtrDebit.Value;
end;

procedure TFmAPagRec.QrPagRec1CalcFields(DataSet: TDataSet);
var
  PagRec: String;
begin
  case Trunc(QrPagRec1Tipo.Value) of
    0: PagRec := ' Pago';
    1: PagRec := ' Depositado';
    2: PagRec := ' Rolado';
  end;
  PagRec := PagRec + ' ['+FormatFloat('000000', QrPagRec1ID_Pgto.Value)+ '] ';
  QrPagRec1PAGO_ROLADO.Value := PagRec;
  /////////////////////////////////////
  QrPagRec1DEVIDO.Value     := QrPagRec1ATUALIZADO.Value;
  QrPagRec1PEND_TOTAL.Value := QrPagRec1ATUALIZADO.Value;
end;

procedure TFmAPagRec.QrPagRecCalcFields(DataSet: TDataSet);
var
  MoraDia: Double;
  SerDoc: String;
begin
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  if QrPagRecFornecedor.Value > 0 then
    QrPagRecTERCEIRO.Value := QrPagRecFornecedor.Value
  else
    QrPagRecTERCEIRO.Value := QrPagRecCliente.Value;
    // FIM 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  //
  QrPagRecVENCER.Value := QrPagRecVencimento.Value;
  QrPagRecVALOR.Value := QrPagRecCredito.Value - QrPagRecDebito.Value;
  ///////////////////////////////
  QrPagRecVENCIDO.Value := 0;
  if (QrPagRecVencimento.Value < Date) and (QrPagRecTipo.Value <> 1) then
    if QrPagRecSit.Value < 2 then
      QrPagRecVENCIDO.Value := QrPagRecVencimento.Value;
  ///////////////////////////////
  if QrPagRecVENCIDO.Value > 0 then
//  begin
  begin
     QrPagRecATRAZODD.Value := Trunc(Date) - QrPagRecVENCIDO.Value;
     QrPagRecMULTA_REAL.Value := QrPagRecMulta.Value / 100 * QrPagRecVALOR.Value;
  end else begin
     QrPagRecATRAZODD.Value := 0;
     QrPagRecMULTA_REAL.Value := 0;
  end;
  if (QrPagRecEhCRED.Value = 'V') and (QrPagRecEhDEB.Value = 'F') then
    MoraDia := QrPagRecMoraDia.Value
  else if (QrPagRecEhCRED.Value = 'F') and (QrPagRecEhDEB.Value = 'V') then
    MoraDia := QrPagRecMoraDia.Value else MoraDia := 0;
  QrPagRecATUALIZADO.Value := QrPagRecSALDO.Value + (QrPagRecSALDO.Value *
  (QrPagRecATRAZODD.Value / 30) * MoraDia / 100) + QrPagRecMULTA_REAL.Value;
  ///////////////////////////////
  if QrPagRecTipo.Value = 1 then
  begin
    QrPagRecPAGO_REAL.Value := QrPagRecVALOR.Value;
  end else if QrPagRecTipo.Value = 0 then
  begin
    if QrPagRecVENCIDO.Value > 0 then
    QrPagRecPAGO_REAL.Value := QrPagRecVALOR.Value
    else QrPagRecPAGO_REAL.Value := 0;
  end else
  begin
    QrPagRecPAGO_REAL.Value := QrPagRecPago.Value;
  end;
  ///////////////////////////////
  if QrPagRecVencimento.Value < Date then
    QrPagRecNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagRecVencimento.Value)
  else QrPagRecNOMEVENCIDO.Value := CO_VAZIO;
  //////////////////////////
  if QrPagRecDocumento.Value > 0 then
    SerDoc := FormatFloat('000000', QrPagRecDocumento.Value) else SerDoc := '';
  if Length(QrPagRecSerieCH.Value) > 0 then SerDoc := QrPagRecSerieCH.Value + SerDoc;
  QrPagRecSERIEDOC.Value := SerDoc;
end;

procedure TFmAPagRec.QrPagRecItsCalcFields(DataSet: TDataSet);
var
  MoraDia: Double;
begin
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  if QrPagRecItsFornecedor.Value > 0 then
    QrPagRecItsTERCEIRO.Value := QrPagRecItsFornecedor.Value
  else
    QrPagRecItsTERCEIRO.Value := QrPagRecItsCliente.Value;
    // fim 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  QrPagRecItsVENCER.Value := QrPagRecItsVencimento.Value;
  QrPagRecItsVALOR.Value := QrPagRecItsCredito.Value - QrPagRecItsDebito.Value;
  ///////////////////////////////
  QrPagRecItsVENCIDO.Value := 0;
  if (QrPagRecItsVencimento.Value < Date) and (QrPagRecItsTipo.Value <> 1) then
    if QrPagRecItsSit.Value < 2 then
      QrPagRecItsVENCIDO.Value := QrPagRecItsVencimento.Value;
  ///////////////////////////////
  if QrPagRecItsVENCIDO.Value > 0 then
//  begin
  begin
     QrPagRecItsATRAZODD.Value := Trunc(Date) - QrPagRecItsVENCIDO.Value;
     QrPagRecItsMULTA_REAL.Value := QrPagRecItsMulta.Value / 100 * QrPagRecItsVALOR.Value;
  end else begin
     QrPagRecItsATRAZODD.Value := 0;
     QrPagRecItsMULTA_REAL.Value := 0;
  end;
  if (QrPagRecItsEhCRED.Value = 'V') and (QrPagRecItsEhDEB.Value = 'F') then
    MoraDia := QrPagRecItsMoraDia.Value
  else if (QrPagRecItsEhCRED.Value = 'F') and (QrPagRecItsEhDEB.Value = 'V') then
    MoraDia := QrPagRecItsMoraDia.Value else MoraDia := 0;
  QrPagRecItsATUALIZADO.Value := QrPagRecItsSALDO.Value + (QrPagRecItsSALDO.Value *
  (QrPagRecItsATRAZODD.Value / 30) * MoraDia / 100) + QrPagRecItsMULTA_REAL.Value;
  ///////////////////////////////
  if QrPagRecItsTipo.Value = 1 then
  begin
    QrPagRecItsPAGO_REAL.Value := QrPagRecItsVALOR.Value;
  end else if QrPagRecItsTipo.Value = 0 then
  begin
    if QrPagRecItsVENCIDO.Value > 0 then
    QrPagRecItsPAGO_REAL.Value := QrPagRecItsVALOR.Value
    else QrPagRecItsPAGO_REAL.Value := 0;
  end else
  begin
    QrPagRecItsPAGO_REAL.Value := QrPagRecItsPago.Value;
  end;
  ///////////////////////////////
  if QrPagRecItsVencimento.Value < Date then
    QrPagRecItsNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagRecItsVencimento.Value)
  else QrPagRecItsNOMEVENCIDO.Value := CO_VAZIO;
  //////////////////////////
end;

procedure TFmAPagRec.QrPagRecXCalcFields(DataSet: TDataSet);
var
  MoraDia: Double;
begin
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  if QrPagRecXFornecedor.Value > 0 then
    QrPagRecXTERCEIRO.Value := QrPagRecXFornecedor.Value
  else
    QrPagRecXTERCEIRO.Value := QrPagRecXCliente.Value;
    // fim 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
  QrPagRecXVENCER.Value := QrPagRecXVencimento.Value;
  QrPagRecXVALOR.Value := QrPagRecXCredito.Value - QrPagRecXDebito.Value;
  ///////////////////////////////
  QrPagRecXVENCIDO.Value := 0;
  if (QrPagRecXVencimento.Value < Date) and (QrPagRecXTipo.Value <> 1) then
    if QrPagRecXSit.Value < 2 then
      QrPagRecXVENCIDO.Value := QrPagRecXVencimento.Value;
  ///////////////////////////////
  if QrPagRecXVENCIDO.Value > 0 then
//  begin
  begin
     QrPagRecXATRAZODD.Value := Trunc(Date) - QrPagRecXVENCIDO.Value;
     QrPagRecXMULTA_REAL.Value := QrPagRecXMulta.Value / 100 * QrPagRecXVALOR.Value;
  end else begin
     QrPagRecXATRAZODD.Value := 0;
     QrPagRecXMULTA_REAL.Value := 0;
  end;
  if (QrPagRecXEhCRED.Value = 'V') and (QrPagRecXEhDEB.Value = 'F') then
    MoraDia := QrPagRecXMoraDia.Value
  else if (QrPagRecXEhCRED.Value = 'F') and (QrPagRecXEhDEB.Value = 'V') then
    MoraDia := QrPagRecXMoraDia.Value
  else if (QrPagRecXID_Pgto.Value <> QrPagRecXControle.Value) then
    MoraDia := QrPagRecXMoraDia.Value
  else MoraDia := 0;
  QrPagRecXATUALIZADO.Value := QrPagRecXSALDO.Value + (QrPagRecXSALDO.Value *
  (QrPagRecXATRAZODD.Value / 30) * MoraDia / 100) + QrPagRecXMULTA_REAL.Value;
  ///////////////////////////////
  if QrPagRecXTipo.Value = 1 then
  begin
    QrPagRecXPAGO_REAL.Value := QrPagRecXVALOR.Value;
  end else if QrPagRecXTipo.Value = 0 then
  begin
    if QrPagRecXVENCIDO.Value > 0 then
    QrPagRecXPAGO_REAL.Value := QrPagRecXVALOR.Value
    else QrPagRecXPAGO_REAL.Value := 0;
  end else
  begin
    if QrPagRecXCompensado.Value > 0 then
      QrPagRecXPAGO_REAL.Value := QrPagRecXVALOR.Value
    else QrPagRecXPAGO_REAL.Value := QrPagRecXPago.Value;
  end;
  QrPagRecXPENDENTE.Value := QrPagRecXVALOR.Value - QrPagRecXPAGO_REAL.Value;
  ///////////////////////////////
  if QrPagRecXVencimento.Value < Date then
    QrPagRecXNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagRecXVencimento.Value)
  else QrPagRecXNOMEVENCIDO.Value := CO_VAZIO;
  //////////////////////////
end;

procedure TFmAPagRec.QrTerceirosCheckSQLField(Sender: TObject;
  const FieldName: string; var Allow: Boolean);
begin
  if QrTerceirosTel1.Value = '' then QrTerceirosTEL1_TXT.Value := '' else
     QrTerceirosTEL1_TXT.Value := '[ ' + Geral.FormataTelefone_TT(
     QrTerceirosTel1.Value) + ' ]';
  //
  if QrTerceirosTel2.Value = '' then QrTerceirosTEL2_TXT.Value := '' else
     QrTerceirosTEL2_TXT.Value := '[ ' + Geral.FormataTelefone_TT(
     QrTerceirosTel2.Value) + ' ]';
  //
  if QrTerceirosTel3.Value = '' then QrTerceirosTEL3_TXT.Value := '' else
     QrTerceirosTEL3_TXT.Value := '[ ' + Geral.FormataTelefone_TT(
     QrTerceirosTel3.Value) + ' ]';
  //
  QrTerceirosTELEFONES.Value := QrTerceirosTEL1_TXT.Value +
     QrTerceirosTEL2_TXT.Value + QrTerceirosTEL3_TXT.Value;
end;

procedure TFmAPagRec.R00_ImprimeExtratoConsolidado;
  procedure GeraParteSQL(TabLct: String);
  begin
    QrExtrato.SQL.Add('SELECT lct.*, cta.Nome NOMECONTA, car.Nome NOMECARTEIRA');
    QrExtrato.SQL.Add('FROM ' + TabLct + ' lct, ' + TMeuDB +
                      '.Contas cta, ' + TMeuDB + '.Carteiras car');
    QrExtrato.SQL.Add('WHERE lct.Data BETWEEN "' + Ext_EmisI + '" AND "' + Ext_EmisF + '"');
    QrExtrato.SQL.Add('AND (lct.Tipo=1 OR (lct.Tipo=0 AND Vencimento<="' + Ext_EmisF + '"))');
    QrExtrato.SQL.Add('AND cta.Codigo=lct.Genero');
    QrExtrato.SQL.Add('AND car.Codigo=lct.Carteira');
    QrExtrato.SQL.Add('AND car.ForneceI=' + FEntidade_TXT);
    QrExtrato.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);
    if CkExclusivo.Checked then
      QrExtrato.SQL.Add('AND car.Exclusivo = 0');

  end;
var
  FldIni, TabIni: String;
  Inicial: Double;
begin
  FldIni := UFinanceiro.DefLctFldSdoIni(TPEmissIni.Date, FDtEncer, FDtMorto);
  TabIni := UFinanceiro.DefLctTab(TPEmissIni.Date, FDtEncer, FDtMorto,
            FTabLcta, FTabLctB, FTabLctD);
  //
  if Uppercase(TabIni) = Uppercase(FTabLctA)then
  begin
    QrInicial.Close;
    QrInicial.SQL.Clear;
    QrInicial.SQL.Add('SELECT SUM(SdoFimB) SdoFimB');
    QrInicial.SQL.Add('FROM carteiras');
    QrInicial.SQL.Add('WHERE Tipo <> 2');
    QrInicial.SQL.Add('AND ForneceI=' + FEntidade_Txt);
    if CkExclusivo.Checked then
      QrInicial.SQL.Add('AND Exclusivo = 0');
    UnDmkDAC_PF.AbreQuery(QrInicial, Dmod.MyDB);
    Inicial := QrInicialSdoFimB.Value;
  end else Inicial := 0;
  //
  QrAnterior.Close;
  QrAnterior.SQL.Clear;
  QrAnterior.SQL.Add('SELECT SUM(lct.Credito - lct.Debito)  Movimento');
  QrAnterior.SQL.Add('FROM ' + TabIni + ' lct');
  QrAnterior.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrAnterior.SQL.Add('WHERE lct.Data<"' + Ext_EmisI + '"');
  QrAnterior.SQL.Add('AND (lct.Tipo=1 OR (lct.Tipo=0 AND lct.Vencimento<"' + Ext_EmisI + '"))');
  QrAnterior.SQL.Add('AND car.ForneceI=' + FEntidade_TXT);
  if CkExclusivo.Checked then
    QrAnterior.SQL.Add('AND car.Exclusivo = 0');
  UnDmkDAC_PF.AbreQuery(QrAnterior, Dmod.MyDB);
  Ext_SdIni := Inicial + QrAnteriorMovimento.Value;
  QrAnterior.Close;
  //
  QrExtrato.Close;
  QrExtrato.SQL.Clear;
  QrExtrato.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_0;');
  QrExtrato.SQL.Add('CREATE TABLE _FIN_RELAT_005_0');
  QrExtrato.SQL.Add('');
  GeraParteSQL(FTabLctA);
  QrExtrato.SQL.Add('UNION');
  GeraParteSQL(FTabLctB);
  QrExtrato.SQL.Add('UNION');
  GeraParteSQL(FTabLctD);
  QrExtrato.SQL.Add(';');
  QrExtrato.SQL.Add('');
  QrExtrato.SQL.Add('SELECT * FROM _FIN_RELAT_005_0');
  QrExtrato.SQL.Add('ORDER BY Data, Carteira, Controle;');
  QrExtrato.SQL.Add('');
  QrExtrato.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_0;');
  QrExtrato.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrExtrato, DModG.MyPID_DB);
  //
  MyObjects.frxMostra(frxFin_Relat_005_00, 'Extrato');
  QrExtrato.Close;
end;

procedure TFmAPagRec.R01_ImprimeFluxoDeCaixa;
  function Fmt(Data: TDateTime): String;
  begin
    Result := '"' + FormatDateTime('YYYY-MM-DD', Data) + '"';
  end;
  procedure GeraParteSQL(TabLct, Quitacao: String);
  begin
    QrFlxo.SQL.Add('SELECT ' + Quitacao + ' QUITACAO, ');
    QrFlxo.SQL.Add('lct.Credito, lct.Debito, lct.Data, lct.Vencimento, ');
    QrFlxo.SQL.Add('lct.Compensado, lct.Descricao, lct.Qtde, lct.Documento, ');
    QrFlxo.SQL.Add('lct.SerieCH, lct.NotaFiscal, lct.Carteira, lct.Controle, ');
    QrFlxo.SQL.Add('lct.ID_Pgto, lct.Sub, ');
    QrFlxo.SQL.Add('car.Nome NOMECART, ');
    QrFlxo.SQL.Add('IF(lct.Cliente>0,');
    QrFlxo.SQL.Add('  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),');
    QrFlxo.SQL.Add('  IF (lct.Fornecedor>0,');
    QrFlxo.SQL.Add('    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO');
    QrFlxo.SQL.Add('FROM ' + TabLct + ' lct');
    QrFlxo.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrFlxo.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades cl ON cl.Codigo=lct.Cliente');
    QrFlxo.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades fo ON fo.Codigo=lct.Fornecedor');
    //
    QrFlxo.SQL.Add('WHERE car.Fornecei=' + IntToStr(FEntidade));
    QrFlxo.SQL.Add(dmkPF.SQL_Periodo('AND ' + Quitacao,
      TPEmissIni.Date, TPEmissFim.Date, CkEmissao.Checked, CkEmissao.Checked));
    QrFlxo.SQL.Add('AND lct.Genero > 0');
    QrFlxo.SQL.Add('AND ((car.Tipo<>2) OR (lct.Sit < 2))');
    if CkExclusivo.Checked then
      QrFlxo.SQL.Add('AND car.Exclusivo = 0');
  end;
var
  Quitacao, Texto, Docum, NotaF: String;
  DataE, DataV, DataQ, DataX: Variant;
  Codig: Integer;
  Saldo, Inicial: Double;
  DataI: TDateTime;
  FldIni, TabIni, TbExtratoCC2: String;
begin
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  PB1.Visible  := True;
  FldIni := UFinanceiro.DefLctFldSdoIni(TPEmissIni.Date, FDtEncer, FDtMorto);
  TabIni := UFinanceiro.DefLctTab(TPEmissIni.Date, FDtEncer, FDtMorto,
            FTabLcta, FTabLctB, FTabLctD);
  //   S A L D O    I N I C I A L
  if CkEmissao.Checked then DataI := TPEmissIni.Date else DataI := 0;
  //
  if Uppercase(TabIni) = Uppercase(FTabLctA)then
  begin
    QrInicial.Close;
    QrInicial.SQL.Clear;
    QrInicial.SQL.Add('SELECT SUM(SdoFimB) SdoFimB');
    QrInicial.SQL.Add('FROM carteiras');
    QrInicial.SQL.Add('WHERE Tipo <> 2');
    QrInicial.SQL.Add('AND ForneceI=' + FEntidade_Txt);
    if CkExclusivo.Checked then
      QrInicial.SQL.Add('AND Exclusivo = 0');
    UnDmkDAC_PF.AbreQuery(QrInicial, Dmod.MyDB);
    Inicial := QrInicialSdoFimB.Value;
  end else Inicial := 0;
  QrAnterior.Close;
  QrAnterior.SQL.Clear;
  QrAnterior.SQL.Add('SELECT ');
  QrAnterior.SQL.Add('SUM(lct.Credito - lct.Debito) Movimento');
  QrAnterior.SQL.Add('FROM ' + TabIni + ' lct');
  QrAnterior.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  QrAnterior.SQL.Add('WHERE car.Tipo <> 2');
  QrAnterior.SQL.Add('AND car.ForneceI=' + FEntidade_TXT);
  QrAnterior.SQL.Add('AND lct.Data<"' + Geral.FDT(DataI, 1) + '"');
  if CkExclusivo.Checked then
    QrAnterior.SQL.Add('AND car.Exclusivo = 0');
  UnDmkDAC_PF.AbreQuery(QrAnterior, Dmod.MyDB);

  //   L A N � A M E N T O S
   Quitacao := 'Date(IF(car.Tipo <> 2, lct.Data,IF(lct.Sit > 1, lct.Compensado, ' +
  'IF(lct.Vencimento<SYSDATE(), IF(lct.Debito>0, ' + Fmt(TPDeb.Date) +
  ', ' + Fmt(TPCre.Date) + '), lct.Vencimento))))';
  //
  QrFlxo.Close;
  QrFlxo.SQL.Clear;
  QrFlxo.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_1;');
  QrFlxo.SQL.Add('CREATE TABLE _FIN_RELAT_005_1');
  QrFlxo.SQL.Add('');
  GeraParteSQL(FTabLctA, Quitacao);
  QrFlxo.SQL.Add('UNION');
  GeraParteSQL(FTabLctB, Quitacao);
  QrFlxo.SQL.Add('UNION');
  GeraParteSQL(FTabLctD, Quitacao);
  QrFlxo.SQL.Add(';');
  QrFlxo.SQL.Add('');
  QrFlxo.SQL.Add('SELECT * FROM _FIN_RELAT_005_1');
  QrFlxo.SQL.Add('ORDER BY QUITACAO, Credito DESC, Debito;');
  QrFlxo.SQL.Add('');
  QrFlxo.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_1;');
  QrFlxo.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrFlxo, DModG.MyPID_DB);
  //
  PB1.Max := QrFlxo.RecordCount + 1;

  // INSERE DADOS TABELA LOCAL
  TbExtratoCC2 := UCriar.RecriaTempTableNovo(ntrttExtratoCC2, DmodG.QrUpdPID1, False, 0, 'extratocc2');
  //
  Codig := 1;
  PB1.Position := Codig;
  PB1.Update;
  Application.ProcessMessages;
  //
  Saldo := Inicial + QrAnteriorMovimento.Value;
  //
  UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, TbExtratoCC2, false,
  [
    'DataE', 'DataV', 'DataQ',
    'DataX', 'Texto', 'Docum',
    'NotaF', 'Credi', 'Debit',
    'Saldo', 'CartC', 'CartN',
    'TipoI', 'Ctrle', 'ID_Pg'
  ], ['Codig'], [
    Null, Null, Null,
    dmkPF.FDT_NULL(DataI, 1), 'SALDO ANTERIOR', '',
    '', 0, 0,
    Saldo, 0, '*** T O D A S ***', 1, 0, 0
  ], [Codig], False);
  //
  QrFlxo.First;
  while not QrFlxo.Eof do
  begin
    Codig := Codig + 1;

    PB1.Position := Codig;
    PB1.Update;
    Application.ProcessMessages;

    Saldo := Saldo + QrFlxoCredito.Value - QrFlxoDebito.Value;
    DataE := dmkPF.FDT_NULL(QrFlxoData.Value, 1);
    DataV := dmkPF.FDT_NULL(QrFlxoVencimento.Value, 1);
    DataQ := dmkPF.FDT_NULL(QrFlxoCompensado.Value, 1);
    DataX := QrFlxoQUITACAO.Value;
    //
    if CkSubstituir.Checked then
      Texto := QrFlxoNOMERELACIONADO.Value
    else
      Texto := QrFlxoDescricao.Value;
    //
    if QrFlxoQtde.Value > 0 then
      Texto := FloatToStr(QrFlxoQtde.Value) + ' ' + Texto;
    if QrFlxoDocumento.Value > 0 then
      Docum := FormatFloat('000000', QrFlxoDocumento.Value) else Docum := '';
    if QrFlxoSerieCH.Value <> '' then Docum := QrFlxoSerieCH.Value + Docum;
    if QrFlxoNotaFiscal.Value > 0 then
      NotaF := FormatFloat('000000', QrFlxoNotaFiscal.Value) else NotaF := '';
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, TbExtratoCC2, false,
    [
      'DataE', 'DataV', 'DataQ',
      'DataX', 'Texto', 'Docum',
      'NotaF', 'Credi', 'Debit',
      'Saldo', 'CartC', 'CartN',
      'TipoI', 'Ctrle', 'ID_Pg', 'CtSub'
    ], ['Codig'], [
      DataE, DataV, DataQ,
      DataX, Texto, Docum,
      NotaF, QrFlxoCredito.Value, QrFlxoDebito.Value,
      Saldo, QrFlxoCarteira.Value, QrFlxoNOMECART.Value,
      2, QrFlxoControle.Value, QrFlxoID_Pgto.Value, QrFlxoSub.Value
    ], [Codig], False);
    //
    QrFlxo.Next;
  end;
  PB1.Visible := False;

  QrExtr.Close;
  UnDmkDAC_PF.AbreQuery(QrExtr, DModG.MyPID_DB);
  Screen.Cursor := crDefault;

  case RGFonte.ItemIndex of
    0: MyObjects.frxMostra(frxFin_Relat_005_01_A06, 'Relat�rio de Fluxo de Caixa');
    1: MyObjects.frxMostra(frxFin_Relat_005_01_A07, 'Relat�rio de Fluxo de Caixa');
    2: MyObjects.frxMostra(frxFin_Relat_005_01_A08, 'Relat�rio de Fluxo de Caixa');
  end;
end;

procedure TFmAPagRec.R02_ImprimeContasAPagar(Sender: TObject(*;
  Saida: TTipoSaidaRelatorio*));
var
  CtrlPai: Double;
  DockWindow: TFmDockForm;
  Maximo, Conta: Integer;
  RelFrx: TfrxReport;
  TbPagRec1, TbPagRec2: String;
begin
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  VAR_ORDENAR := RGOrdem1.ItemIndex * 10 + RGOrdem2.ItemIndex;
  ////
  SQLContasAPagar(QrPagRecX,   0);
  if RGTipo.ItemIndex in ([4,5]) then
    SQLContasAPagar(QrPagRec,    3)
  else
    SQLContasAPagar(QrPagRec,    1);
  SQLContasAPagar(QrPagRecIts, 2);
  if RGTipo.ItemIndex in ([6,7]) then
  begin
    DockWindow := DockWindows[(Sender as TComponent).Tag];
    DockWindow.Show;
    DockWindow.Refresh;
    //
    TbPagRec1 := UCriar.RecriaTempTableNovo(ntrttPagRec1, DmodG.QrUpdPID1, False, 0, 'PagRec1');
    TbPagRec2 := UCriar.RecriaTempTableNovo(ntrttPagRec2, DmodG.QrUpdPID1, False, 0, 'PagRec2');
    //
    Maximo := QrPagRecX.RecordCount;
    QrPagRecX.First;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + TbPagRec1 + ' SET ');
    DModG.QrUpdPID1.SQL.Add('Data=:P0, Documento=:P1, NotaFiscal=:P2, ');
    DModG.QrUpdPID1.SQL.Add('NOMEVENCIDO=:P3, NOMECONTA=:P4, Descricao=:P5, ');
    DModG.QrUpdPID1.SQL.Add('Controle=:P6, Valor=:P7, MoraDia=:P8, ');
    DModG.QrUpdPID1.SQL.Add('PAGO_REAL=:P9, ATRAZODD=:P10, ATUALIZADO=:P11, ');
    DModG.QrUpdPID1.SQL.Add('MULTA_REAL=:P12, DataDoc=:P13, Vencimento=:P14, ');
    DModG.QrUpdPID1.SQL.Add('TERCEIRO=:P15, NOME_TERCEIRO=:P16, CtrlPai=:P17, ');
    DModG.QrUpdPID1.SQL.Add('Tipo=:P18, Pendente=:P19, ID_Pgto=:P20, ');
    DModG.QrUpdPID1.SQL.Add('CtrlIni=:P21, Duplicata=:P22, Qtde=:P23, ');
    DModG.QrUpdPID1.SQL.Add('NO_UH=:P24 ');
    Screen.Cursor := VAR_CURSOR;
    Conta := 0;
    DockWindow.AtualizaDados(Conta, Maximo);
    while not QrPagRecX.Eof do
    begin
      Conta := Conta + 1;
      DockWindow.AtualizaDados(Conta, Maximo);
      Update;
      Application.ProcessMessages;
      if VAR_PARAR then
      begin
        TbPagRec1 := UCriar.RecriaTempTableNovo(ntrttPagRec1, DmodG.QrUpdPID1, False, 0, 'PagRec1');
        TbPagRec2 := UCriar.RecriaTempTableNovo(ntrttPagRec2, DmodG.QrUpdPID1, False, 0, 'PagRec2');
        //
        Exit;
      end;
      if QrPagRecXCtrlIni.Value > 0 then
        CtrlPai := QrPagRecXCtrlIni.Value
      else CtrlPai := QrPagRecXControle.Value;
      DModG.QrUpdPID1.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, QrPagRecXData.Value);
      DModG.QrUpdPID1.Params[01].AsFloat   := QrPagRecXDocumento.Value;
      DModG.QrUpdPID1.Params[02].AsFloat   := QrPagRecXNotaFiscal.Value;
      DModG.QrUpdPID1.Params[03].AsString  := QrPagRecXNOMEVENCIDO.Value;
      DModG.QrUpdPID1.Params[04].AsString  := QrPagRecXNOMECONTA.Value;
      DModG.QrUpdPID1.Params[05].AsString  := QrPagRecXDescricao.Value;
      DModG.QrUpdPID1.Params[06].AsFloat   := QrPagRecXControle.Value;
      DModG.QrUpdPID1.Params[07].AsFloat   := QrPagRecXValor.Value;
      DModG.QrUpdPID1.Params[08].AsFloat   := QrPagRecXMoraDia.Value;
      DModG.QrUpdPID1.Params[09].AsFloat   := QrPagRecXPAGO_REAL.Value;
      DModG.QrUpdPID1.Params[10].AsFloat   := QrPagRecXATRAZODD.Value;
      DModG.QrUpdPID1.Params[11].AsFloat   := QrPagRecXATUALIZADO.Value;
      DModG.QrUpdPID1.Params[12].AsFloat   := QrPagRecXMULTA_REAL.Value;
      //
      DModG.QrUpdPID1.Params[13].AsString  := FormatDateTime(VAR_FORMATDATE, QrPagRecXDataDoc.Value);
      DModG.QrUpdPID1.Params[14].AsString  := FormatDateTime(VAR_FORMATDATE, QrPagRecXVencimento.Value);
      DModG.QrUpdPID1.Params[15].AsFloat   := QrPagRecXTerceiro.Value;
      DModG.QrUpdPID1.Params[16].AsString  := QrPagRecXNOME_TERCEIRO.Value;
      DModG.QrUpdPID1.Params[17].AsFloat   := CtrlPai;
      DModG.QrUpdPID1.Params[18].AsFloat   := QrPagRecXTipo.Value;
      DModG.QrUpdPID1.Params[19].AsFloat   := QrPagRecXPENDENTE.Value;
      DModG.QrUpdPID1.Params[20].AsFloat   := QrPagRecXID_Pgto.Value;
      DModG.QrUpdPID1.Params[21].AsFloat   := QrPagRecXCtrlIni.Value;
      DModG.QrUpdPID1.Params[22].AsString  := QrPagRecXDuplicata.Value;
      DModG.QrUpdPID1.Params[23].AsFloat   := QrPagRecXQtde.Value;
      DModG.QrUpdPID1.Params[24].AsString  := QrPagRecXNO_UH.Value;

      DModG.QrUpdPID1.ExecSQL;
      QrPagRecX.Next;
    end;
    DockWindow.Hide;
    with QrPagRec1.SQL do
    begin
      Clear;
      Add('SELECT * FROM ' + TbPagRec1);
      Add(SQL_Ordenar());
    end;
    QrPagRec1.Database := DModG.MyPID_DB;
    UnDmkDAC_PF.AbreQuery(QrPagRec1, DModG.MyPID_DB);
    case RGHistorico.ItemIndex of
      0: Relfrx := frxFin_Relat_005_02_B1;
      1: Relfrx := frxFin_Relat_005_02_B2;
      else Relfrx := nil;
    end;
  end else begin
    case RGFonte.ItemIndex of
      0: RelFrx := frxFin_Relat_005_02_A06;
      1: RelFrx := frxFin_Relat_005_02_A07;
      2: RelFrx := frxFin_Relat_005_02_A08;
      else RelFrx := nil;
    end;
  end;
  if Relfrx <> nil then
    MyObjects.frxMostra(Relfrx, 'Hist�rico de lan�amentos')
  else
    Geral.MB_Erro('Relat�rio n�o definido!');
  QrPagRec1.Close;
  QrPagRec.Close;
  QrPagRecIts.Close;
  Screen.Cursor := VAR_CURSOR;
end;

procedure TFmAPagRec.RecriaDockForms();
var
  i: Integer;
begin
  if FInCreation then
    Exit;
  FInCreation := True;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando tabelas');
  Screen.Cursor := crHourGlass;
  try
    for i := 0 to Screen.FormCount - MaxJan - 1 do
    begin
      if Screen.Forms[i] is TFm_Empresas then
        Screen.Forms[i].Destroy;
      if Screen.Forms[i] is TFm_Indi_Pags then
        Screen.Forms[i].Destroy;
      if Screen.Forms[i] is TFm_Pla_Ctas then
        Screen.Forms[i].Destroy;
    end;
    TFm_Empresas.CreateDockForm().ManualDock(DockTabSet1);
    TFm_Indi_Pags.CreateDockForm().ManualDock(DockTabSet1);
    TFm_Pla_Ctas.CreateDockForm().ManualDock(DockTabSet1);

    //
    FInCreation := False;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmAPagRec.ReopenNivelSel;
var
  Nivel: String;
begin
  Nivel := NomeDoNivelSelecionado(1);
  //
  QrNivelSel.Close;
  QrNivelSel.SQL.Clear;
  QrNivelSel.SQL.Add('SELECT Codigo, Nome');
  QrNivelSel.SQL.Add('FROM ' + Nivel);
  //QrNivelSel.SQL.Add('WHERE Codigo > 0');
  QrNivelSel.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrNivelSel, Dmod.MyDB);
  //
  EdNivelSel.ValueVariant := 0;
  CBNivelSel.KeyValue     := Null;
end;

procedure TFmAPagRec.RGNivelSelClick(Sender: TObject);
begin
  ReopenNivelSel();
end;

procedure TFmAPagRec.RGTipoClick(Sender: TObject);
begin
  CkExclusivo.Visible := RGTipo.ItemIndex in ([0,1]);
  PnDatas2.Visible    := RGTipo.ItemIndex in ([2,3,4,5,6,7,8,9]);
  PainelD.Visible     := RGTipo.ItemIndex in ([2,3,4,5,6,7,8,9]);
  PnFluxo.Visible     := RGTipo.ItemIndex = 1;
  CkOrdem1.Visible    := RGTipo.ItemIndex <> 1;
  RGOrdem1.Visible    := RGTipo.ItemIndex <> 1;
  CkOrdem2.Visible    := RGTipo.ItemIndex <> 1;
  RGOrdem2.Visible    := RGTipo.ItemIndex <> 1;
  RGHistorico.Visible := RGTipo.ItemIndex in ([6,7]);
  RGFonte.Visible     := RGTipo.ItemIndex in ([1,2,3,4,5,8,9]);
  //
  EdA.Visible        := True;
  EdZ.Visible        := True;
  CkNiveis.Visible   := True;
  //CkNiveis.Checked   := False;
  CkOmiss.Visible    := False;
  GBOmiss.Visible    := False;
  GBPerdido.Visible  := False;
  LaAccount.Visible  := False;
  EdAccount.Visible  := False;
  CBAccount.Visible  := False;
  LaVendedor.Visible := False;
  EdVendedor.Visible := False;
  CBVendedor.Visible := False;
  //
  //CkVencto.Visible     := True;
  GBVencto.Visible     := True;
  CkSubstituir.Visible := False;
  CkEmissao.Caption    := 'Data da emiss�o:';
  case RGTipo.ItemIndex of
    0:
    begin
      GBVencto.Visible := True;
      //
      EdA.Visible := False;
      EdZ.Visible := False;
      CkNiveis.Visible := False;
      //
      if TPEmissIni.Date > date then TPEmissIni.Date := date;
      if TPEmissFim.Date < date then TPEmissFim.Date := date;
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date;
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      if TPVctoIni.Date > date then TPVctoIni.Date := date;
      if TPVctoFim.Date < date then TPVctoFim.Date := date;
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date;
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CBTerceiro.Enabled := False;
      EdTerceiro.Enabled := False;
      LaTerceiro.Enabled := False;
    end;
    1:
    begin
      EdA.Visible          := False;
      EdZ.Visible          := False;
      CkNiveis.Visible     := False;
      CkGrade.Visible      := False;
      CkSubstituir.Visible := True;
      CkEmissao.Caption    := 'Per�odo:';
      //
      if TPEmissFim.Date < date then TPEmissFim.Date := date;
      TPEmissIni.MinDate := 1;
      TPEmissIni.MaxDate := Date+7305;//Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := Date;
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      if TPVctoFim.Date < date then TPVctoFim.Date := date;
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := Date;
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      CBTerceiro.Enabled := False;
      EdTerceiro.Enabled := False;
      LaTerceiro.Enabled := False;
    end;
    2:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Fornecedor';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    3:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
      ////
      LaAccount.Visible := True;
      EdAccount.Visible := True;
      CBAccount.Visible := True;
      LaVendedor.Visible := True;
      EdVendedor.Visible := True;
      CBVendedor.Visible := True;
    end;
    4:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    5:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    6:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Fornecedor';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    7:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
      ////
      LaAccount.Visible := True;
      EdAccount.Visible := True;
      CBAccount.Visible := True;
      LaVendedor.Visible := True;
      EdVendedor.Visible := True;
      CBVendedor.Visible := True;
    end;
    8:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      GBPerdido.Visible := True;
    end;
    9:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      GBPerdido.Visible := True;
    end;
  end;
  if not RGOrdem1.Enabled then RGOrdem1.ItemIndex := 0;
  if not RGOrdem2.Enabled then RGOrdem2.ItemIndex := 1;
  if RGTipo.ItemIndex in ([0,1]) then
  begin
    CkVencto.Enabled  := False;
    GBVencto.Enabled  := False;
    LaVenctI.Enabled  := False;
    LaVenctF.Enabled  := False;
    TPVctoIni.Enabled := False;
    TPVctoFim.Enabled := False;
  end else begin
    CkVencto.Enabled  := True;
    GBVencto.Enabled  := True;
    LaVenctI.Enabled  := True;
    LaVenctF.Enabled  := True;
    TPVctoIni.Enabled := True;
    TPVctoFim.Enabled := True;
  end;
end;

procedure TFmAPagRec.SQLContasAPagar(Query: TmySQLQuery; Tipo: Integer);
  procedure GeraParteSQL(TabLct, TextoEmpresa: String);
  var
    Anos, Meses, Dias: Word;
    Nivel, NivelSel: String;
  begin
    Query.SQL.Add('SELECT ' + TextoEmpresa);
    Query.SQL.Add('lct.*, co.Nome NOMECONTA, ca.Nome NOMECARTEIRA,');
    Query.SQL.Add('IF(lct.Sit=0, (lct.Credito-lct.Debito),');
    Query.SQL.Add('IF(lct.Sit=1, (lct.Credito-lct.Debito-lct.Pago), 0)) SALDO,');
    Query.SQL.Add('co.Credito EhCRED, co.Debito EhDEB, ');
    Query.SQL.Add('IF ((lct.Fornecedor>0) AND (fo.Tipo=0), fo.RazaoSocial, ');
    Query.SQL.Add('IF ((lct.Fornecedor>0) AND (fo.Tipo=1), fo.Nome, ');
    Query.SQL.Add('IF ((lct.Cliente>0) AND (cl.Tipo=0), cl.RazaoSocial, ');
    Query.SQL.Add('IF ((lct.Cliente>0) AND (cl.Tipo=1), cl.Nome, "???")))) NOME_TERCEIRO, ');
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
    //Query.SQL.Add('CASE WHEN lct.Fornecedor>0 then lct.Fornecedor ELSE lct.Cliente END TERCEIRO,');
    // 2011-02-13 Migra��o MySQL 5.x - ERRO TLargeIntField
    Query.SQL.Add('0 CtrlPai, ');
    if VAR_KIND_DEPTO = kdUH then
      Query.SQL.Add('imv.Unidade NO_UH')
    else
      Query.SQL.Add('"" NO_UH');
    Query.SQL.Add('FROM ' + TabLct + ' lct, ' + TMeuDB + '.contas co, ' +
    TMeuDB + '.subgrupos sg, ' + TMeuDB + '.grupos gr, ' + TMeuDB +
    '.conjuntos cj, ' + TMeuDB + '.plano pl, ' + TMeuDB + '.carteiras ca, ' +
    TMeuDB + '.entidades cl, ' + TMeuDB + '.entidades fo');
    if VAR_KIND_DEPTO = kdUH then
      Query.SQL.Add(', ' + TMeuDB + '.condimov imv');

    Query.SQL.Add('WHERE fo.Codigo=lct.Fornecedor AND cl.Codigo=lct.Cliente');
    Query.SQL.Add('AND sg.Codigo=co.SubGrupo');
    Query.SQL.Add('AND gr.Codigo=sg.Grupo');
    Query.SQL.Add('AND cj.Codigo=gr.Conjunto');
    Query.SQL.Add('AND pl.Codigo=cj.Plano');
    Query.SQL.Add('AND lct.Genero<>-1');

    //  2010-08-02 - N�o usa mais Genero=-5 > Outras carteiras!
    Query.SQL.Add('AND ( lct.Tipo<>2 OR (lct.Tipo=2 AND Sit < 2) )');
    // fim 2010-08-02
    Query.SQL.Add('AND ca.ForneceI =' + FormatFloat('0', FEntidade));

    //if RGTipo.ItemIndex in ([6,7]) then
    if CkEmissao.Checked then
      Query.SQL.Add('AND lct.Data BETWEEN "'+Ext_EmisI+'" AND "'+Ext_EmisF+'"');
    //else
    if CkVencto.Checked then
      Query.SQL.Add('AND lct.Vencimento BETWEEN "'+Ext_VctoI+'" AND "'+Ext_VctoF+'"');
    if CkDataDoc.Checked then
      Query.SQL.Add('AND lct.DataDoc BETWEEN "'+Ext_DocI+'" AND "'+Ext_DocF+'"');
    if CkDataComp.Checked then
    begin
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE IGNORE ' + FTabLctA + ' SET Compensado=Vencimento');
      Dmod.QrUpdU.SQL.Add('WHERE Tipo<>2');
      Dmod.QrUpdU.ExecSQL;
      Query.SQL.Add('AND lct.Compensado BETWEEN "'+Ext_CompI+'" AND "'+Ext_CompF+'"');
    end;
    if RGTipo.ItemIndex in ([2,3]) then
    begin
      Query.SQL.Add('AND lct.Tipo=2');
      Query.SQL.Add('AND lct.Sit<2');
    end else
    if RGTipo.ItemIndex in ([5]) then
    begin
      //Query.SQL.Add('AND lct.Tipo=2');
      Query.SQL.Add('AND lct.Sit>1');
    end;
    Query.SQL.Add('AND co.Codigo=lct.Genero');
    Query.SQL.Add('AND ca.Codigo=lct.Carteira');
    if VAR_KIND_DEPTO = kdUH then
      Query.SQL.Add('AND lct.Depto=imv.Conta');

    if RGTipo.ItemIndex in ([2,4,6]) then
    begin
      Query.SQL.Add('AND lct.Debito>0');
      if EdTerceiro.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Fornecedor ='+IntToStr(EdTerceiro.ValueVariant));
    end else if RGTipo.ItemIndex in ([3,5,7]) then
    begin
      Query.SQL.Add('AND lct.Credito>0');
      if EdTerceiro.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Cliente='+IntToStr(EdTerceiro.ValueVariant));
    end;
    if RGTipo.ItemIndex in ([3,7]) then
    begin
      if EdAccount.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Account ='+IntToStr(CBAccount.KeyValue));
      if EdVendedor.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Vendedor ='+IntToStr(CBVendedor.KeyValue));
    end;
    if CkNiveis.Checked then
    begin
      if RGTipo.ItemIndex in ([2,4,6]) then
      Query.SQL.Add('AND fo.Nivel BETWEEN "'+EdA.Text+'" AND "'+EdZ.Text+'"');
      if RGTipo.ItemIndex in ([3,5,7]) then
      Query.SQL.Add('AND cl.Nivel BETWEEN "'+EdA.Text+'" AND "'+EdZ.Text+'"');
    end;
    if CkOmiss.Checked then
    begin
      if CkAnos.Checked  then Anos :=  Geral.IMV(EdAnos.Text)  else Anos := 0;
      if CkMeses.Checked then Meses := Geral.IMV(EdMeses.Text) else Meses := 0;
      if CkDias.Checked  then Dias :=  Geral.IMV(EdDias.Text)  else Dias := 0;
      Ext_Vencto := IncMonth(Date, -1 * ((Anos*12)+Meses)) - Dias;
      Query.SQL.Add('AND lct.Vencimento >= "'+
      FormatDateTime(VAR_FORMATDATE, Ext_Vencto)+'"');
    end;
    if RGTipo.ItemIndex in ([8,9]) then
    begin
      if CkAnos2.Checked  then Anos :=  Geral.IMV(EdAnos2.Text)  else Anos := 0;
      if CkMeses2.Checked then Meses := Geral.IMV(EdMeses2.Text) else Meses := 0;
      if CkDias2.Checked  then Dias :=  Geral.IMV(EdDias2.Text)  else Dias := 0;
      Ext_Vencto := IncMonth(Date, -1 * ((Anos*12)+Meses)) - Dias;
      Query.SQL.Add('AND lct.Vencimento < "'+
      FormatDateTime(VAR_FORMATDATE, Ext_Vencto)+'"');
    end;
    //
    if RGTipo.ItemIndex in ([2,3,4,5,6,7,8,9]) then
    begin
      if RGNivelSel.Enabled then
      begin
        Nivel       := NomeDoNivelSelecionado(2);
        NivelSel    := FormatFloat('0', EdNivelSel.ValueVariant);
        Query.SQL.Add('AND ' + Nivel + '=' + NivelSel);
      end else
      begin
        Query.SQL.Add('AND lct.Genero IN (');
        Query.SQL.Add('      SELECT cN1');
        Query.SQL.Add('      FROM ' + TAB_TMP_PLA_CTAS);
        Query.SQL.Add('      WHERE Ativo=1');
        Query.SQL.Add(')');
      end;
    {
      if EdConta.ValueVariant <> 0 then
        Query.SQL.Add('AND lct.Genero ='+IntToStr(CBConta.KeyValue));
    }
    end;
    case Tipo of
      0: Query.SQL.Add('');
      1: Query.SQL.Add('AND lct.ID_Pgto=0');
      2: Query.SQL.Add('AND lct.ID_Pgto<>0');
      // Novo 2008.07.22
      3: Query.SQL.Add('AND lct.Compensado>0');
    end;
    if EdIndiPag.Enabled and (EdIndiPag.ValueVariant > 0) then
      Query.SQL.Add('AND lct.IndiPag=' + FormatFloat('0', EdIndiPag.ValueVariant))
    else begin
      Query.SQL.Add('AND lct.IndiPag IN (');
      Query.SQL.Add('  SELECT Codigo FROM ' + TAB_TMP_INDIPAGS);
      Query.SQL.Add('  WHERE Ativo=1');
      Query.SQL.Add(')');
    end;
  end;
var
  TxtEmp: String;
begin
  PB1.Position := 0;
  PB1.Max := QrEmpresas.RecordCount;
  QrEmpresas.First;
  while not QrEmpresas.Eof do
  begin
    FEntidade := QrEmpresasCodEnti.Value;
    FCliInt   := QrEmpresasCodCliInt.Value;
    // Precisa?? ou gera erro?
    if FCliInt = 0 then
      FCliInt   := QrEmpresasCodFilial.Value;
    //  
    FEntidade_TXT := FormatFloat('0', FEntidade);
    DModG.Def_EM_ABD(TMeuDB, FEntidade, FCliInt,
      FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
    TxtEmp := FormatFloat('0', QrEmpresasCodCliInt.Value) + ' CodCliInt, ''' +
    QrEmpresasNome.Value + ''' NO_CodCliInt, ';
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      'Pesquisando empresa ' + FormatFloat('0', FCliInt) + ' - ' +
      QrEmpresasNome.Value);
    Query.Close;
    Query.SQL.Clear;
    if QrEmpresas.RecNo = 1 then
    begin
      Query.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_2;');
      Query.SQL.Add('CREATE TABLE _FIN_RELAT_005_2');
      Query.SQL.Add('');
    end else begin
      Query.SQL.Add('INSERT INTO _FIN_RELAT_005_2');
      Query.SQL.Add('');
    end;
    GeraParteSQL(FTabLctA, TxtEmp);
    Query.SQL.Add('UNION');
    GeraParteSQL(FTabLctB, TxtEmp);
    Query.SQL.Add('UNION');
    GeraParteSQL(FTabLctD, TxtEmp);
    Query.SQL.Add(';');
    Memo1.Lines.Add(Query.SQL.Text);
    UMyMod.ExecutaQuery(Query);
    //
    QrEmpresas.Next;
  end;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM _FIN_RELAT_005_2');
  Query.SQL.Add(SQL_Ordenar() + ';');
  Query.SQL.Add('');
  Query.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_005_2;');
  Query.SQL.Add('');
  Memo1.Lines.Add(Query.SQL.Text);
  Memo1.Lines.Add('/* Fim Query */');
  Memo1.Lines.Add('');
  UnDmkDAC_PF.AbreQuery(Query, DModG.MyPID_DB);
end;

function TFmAPagRec.SQL_Ordenar(): String;
begin
  case VAR_ORDENAR of
    00: Result := 'ORDER BY NOME_TERCEIRO, DataDoc, CtrlPai, Data, Vencimento, Controle';
    01: Result := 'ORDER BY NOME_TERCEIRO, DataDoc, CtrlPai, Data, Vencimento, Controle';
    02: Result := 'ORDER BY NOME_TERCEIRO, Data, CtrlPai, DataDoc, Vencimento, Controle';
    03: Result := 'ORDER BY NOME_TERCEIRO, Vencimento, CtrlPai, DataDoc, Data, Controle';
    04: Result := 'ORDER BY NOME_TERCEIRO, Documento, Vencimento, CtrlPai, DataDoc, Data, Controle';
    05: Result := 'ORDER BY NOME_TERCEIRO, NotaFiscal, Vencimento, CtrlPai, DataDoc, Data, Controle';
    06: Result := 'ORDER BY NOME_TERCEIRO, Duplicata, NotaFiscal, Vencimento, CtrlPai, DataDoc, Data, Controle';
    07: Result := 'ORDER BY NOME_TERCEIRO, NO_UH, Vencimento, CtrlPai, DataDoc, Data, Controle';
    //
    10: Result := 'ORDER BY DataDoc, NOME_TERCEIRO, CtrlPai, Data, Vencimento, Controle';
    11: Result := 'ORDER BY DataDoc, NOME_TERCEIRO, CtrlPai, Data, Vencimento, Controle';
    12: Result := 'ORDER BY DataDoc, Data, CtrlPai, NOME_TERCEIRO, Vencimento, Controle';
    13: Result := 'ORDER BY DataDoc, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    14: Result := 'ORDER BY DataDoc, Documento, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    15: Result := 'ORDER BY DataDoc, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    16: Result := 'ORDER BY DataDoc, Duplicata, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    17: Result := 'ORDER BY DataDoc, NO_UH, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    //
    20: Result := 'ORDER BY Data, NOME_TERCEIRO, CtrlPai, DataDoc, Vencimento, Controle';
    21: Result := 'ORDER BY Data, DataDoc, CtrlPai, NOME_TERCEIRO, Vencimento, Controle';
    22: Result := 'ORDER BY Data, NOME_TERCEIRO, CtrlPai, DataDoc, Vencimento, Controle';
    23: Result := 'ORDER BY Data, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    24: Result := 'ORDER BY Data, Documento, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    25: Result := 'ORDER BY Data, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    26: Result := 'ORDER BY Data, Duplicata, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    27: Result := 'ORDER BY Data, NO_UH, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    //
    30: Result := 'ORDER BY Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    31: Result := 'ORDER BY Vencimento, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    32: Result := 'ORDER BY Vencimento, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    33: Result := 'ORDER BY Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    34: Result := 'ORDER BY Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    35: Result := 'ORDER BY Vencimento, NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    36: Result := 'ORDER BY Vencimento, Duplicata,NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    37: Result := 'ORDER BY Vencimento, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    //
    40: Result := 'ORDER BY Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    41: Result := 'ORDER BY Documento, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    42: Result := 'ORDER BY Documento, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    43: Result := 'ORDER BY Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    44: Result := 'ORDER BY Documento, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    45: Result := 'ORDER BY Documento, NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    46: Result := 'ORDER BY Documento, Duplicata, NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    47: Result := 'ORDER BY Documento, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    //
    50: Result := 'ORDER BY NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    51: Result := 'ORDER BY NotaFiscal, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    52: Result := 'ORDER BY NotaFiscal, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    53: Result := 'ORDER BY NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    54: Result := 'ORDER BY NotaFiscal, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    55: Result := 'ORDER BY NotaFiscal, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    56: Result := 'ORDER BY NotaFiscal, Duplicata, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    57: Result := 'ORDER BY NotaFiscal, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    //
    60: Result := 'ORDER BY Duplicata, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    61: Result := 'ORDER BY Duplicata, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    62: Result := 'ORDER BY Duplicata, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    63: Result := 'ORDER BY Duplicata, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    64: Result := 'ORDER BY Duplicata, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    65: Result := 'ORDER BY Duplicata, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    66: Result := 'ORDER BY Duplicata, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    67: Result := 'ORDER BY Duplicata, NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    //
    70: Result := 'ORDER BY NO_UH, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    71: Result := 'ORDER BY NO_UH, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    72: Result := 'ORDER BY NO_UH, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    73: Result := 'ORDER BY NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    74: Result := 'ORDER BY NO_UH, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    75: Result := 'ORDER BY NO_UH, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    76: Result := 'ORDER BY NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    77: Result := 'ORDER BY NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    //
    else Result := '';
  end;
end;

procedure TFmAPagRec.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  if FPreparou = False then
  begin
    Screen.Cursor := crHourGlass;
    try
      FPreparou := True;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas tempor�rias');
      //
      DModG.Tb_Empresas.Close;
      DModG.Tb_Empresas.Database := DModG.MyPID_DB;
      //
      UnCreateGeral.RecriaTempTableNovo(ntrtt_Empresas, DModG.QrUpdPID1,
        False, 1, TAB_TMP_EMPRESAS);
      //
      UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
        'DELETE FROM ' + TAB_TMP_EMPRESAS + ';',
        'INSERT INTO '  + TAB_TMP_EMPRESAS,
        'SELECT eci.CodCliInt, eci.CodEnti, eci.CodFilial, ',
        'IF(ent.Tipo=0, ent.RazaoSocial, Nome) Nome, ',
        '0 Ativo',
        'FROM ' + TMeuDB + '.enticliint eci',
        'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=eci.CodEnti'
        , 'WHERE eci.TipoTabLct=1'
        ]);
      DModG.Tb_Empresas. Open;
      //
      DModG.Tb_Indi_Pags.Close;
      DModG.Tb_Indi_Pags.Database := DModG.MyPID_DB;
      //
      UCriar.RecriaTempTableNovo(ntrttPesqESel, DModG.QrUpdPID1, False, 1, TAB_TMP_INDIPAGS);
      //
      UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
        'DELETE FROM ' + TAB_TMP_INDIPAGS + ';',
        'INSERT INTO ' + TAB_TMP_INDIPAGS,
        'SELECT Codigo, Nome, 1 Ativo',
        'FROM ' + TMeuDB + '.indipag']);
      //
      DModG.Tb_Indi_Pags. Open;
      //
      DModFin.Tb_Pla_Ctas.Close;
      DModFin.Tb_Pla_Ctas.Database := DModG.MyPID_DB;
      //
      UCriarFin.RecriaTempTableNovo(ntrtt_PlaCta, DModG.QrUpdPID1, False, 1, TAB_TMP_PLA_CTAS);
      //
      UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
        'DELETE FROM ' + TAB_TMP_PLA_CTAS + ';',

        'INSERT INTO ' + TAB_TMP_PLA_CTAS,
        'SELECT 5 Nivel,',
        'pla.OrdemLista, pla.Codigo, pla.Nome, ',
        '0, 0, "",',
        '0, 0, "",',
        '0, 0, "",',
        '0, 0, "",',
        //'0, 0, 0, 0, 0, 0, ',
        '1 Ativo',
        'FROM ' + TMeuDB + '.plano pla;',

        'INSERT INTO ' + TAB_TMP_PLA_CTAS,
        'SELECT 4 Nivel,',
        'pla.OrdemLista, pla.Codigo, pla.Nome, ',
        'cjt.OrdemLista, cjt.Codigo, cjt.Nome,',
        '0, 0, "",',
        '0, 0, "",',
        '0, 0, "",',
        //'0, 0, 0, 0, 0, 0, ',
        '1 Ativo',
        'FROM ' + TMeuDB + '.conjuntos cjt',
        'LEFT JOIN ' + TMeuDB + '.plano pla ON pla.Codigo=cjt.Plano;',

        'INSERT INTO ' + TAB_TMP_PLA_CTAS,
        'SELECT 3 Nivel,',
        'pla.OrdemLista, pla.Codigo, pla.Nome, ',
        'cjt.OrdemLista, cjt.Codigo, cjt.Nome,',
        'gru.OrdemLista, gru.Codigo, gru.Nome,',
        '0, 0, "",',
        '0, 0, "",',
        //'0, 0, 0, 0, 0, 0, ',
        '1 Ativo',
        'FROM ' + TMeuDB + '.grupos gru',
        'LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto',
        'LEFT JOIN ' + TMeuDB + '.plano pla ON pla.Codigo=cjt.Plano;',

        'INSERT INTO ' + TAB_TMP_PLA_CTAS,
        'SELECT 2 Nivel,',
        'pla.OrdemLista, pla.Codigo, pla.Nome, ',
        'cjt.OrdemLista, cjt.Codigo, cjt.Nome,',
        'gru.OrdemLista, gru.Codigo, gru.Nome,',
        'sgr.OrdemLista, sgr.Codigo, sgr.Nome,',
        '0, 0, "", ',
        //'0, 0, 0, 0, 0, 0, ',
        '1 Ativo',
        'FROM ' + TMeuDB + '.subgrupos sgr',
        'LEFT JOIN ' + TMeuDB + '.grupos gru ON gru.Codigo=sgr.grupo',
        'LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto',
        'LEFT JOIN ' + TMeuDB + '.plano pla ON pla.Codigo=cjt.Plano;',

        'INSERT INTO ' + TAB_TMP_PLA_CTAS,
        'SELECT 1 Nivel, ',
        'pla.OrdemLista, pla.Codigo, pla.Nome, ',
        'cjt.OrdemLista, cjt.Codigo, cjt.Nome,',
        'gru.OrdemLista, gru.Codigo, gru.Nome,',
        'sgr.OrdemLista, sgr.Codigo, sgr.Nome,',
        'cta.OrdemLista, cta.Codigo, cta.Nome,',
        //'0, 0, 0, 0, 0, 0, ',
        '1 Ativo',
        'FROM ' + TMeuDB + '.contas cta',
        'LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=cta.Subgrupo',
        'LEFT JOIN ' + TMeuDB + '.grupos gru ON gru.Codigo=sgr.grupo',
        'LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto',
        'LEFT JOIN ' + TMeuDB + '.plano pla ON pla.Codigo=cjt.Plano']);
      //
      DModFin.Tb_Pla_Ctas. Open;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      RecriaDockForms();
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.
