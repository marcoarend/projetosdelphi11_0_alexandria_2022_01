object FmFluxCxaDia2: TFmFluxCxaDia2
  Left = 339
  Top = 185
  Caption = 'FIN-RELAT-015 :: Fechamento de Caixa Di'#225'rio'
  ClientHeight = 308
  ClientWidth = 592
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 592
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 544
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 496
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 344
        Height = 32
        Caption = 'Fechamento de Caixa Di'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 344
        Height = 32
        Caption = 'Fechamento de Caixa Di'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 344
        Height = 32
        Caption = 'Fechamento de Caixa Di'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 592
    Height = 134
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 592
      Height = 134
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 592
        Height = 134
        Align = alClient
        TabOrder = 0
        object Panel12: TPanel
          Left = 2
          Top = 15
          Width = 588
          Height = 117
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 588
            Height = 201
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel15: TPanel
              Left = 0
              Top = 0
              Width = 588
              Height = 45
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label1: TLabel
                Left = 8
                Top = 2
                Width = 44
                Height = 13
                Caption = 'Empresa:'
              end
              object EdEmpresa: TdmkEditCB
                Left = 8
                Top = 18
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdEmpresaChange
                DBLookupComboBox = CBEmpresa
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBEmpresa: TdmkDBLookupComboBox
                Left = 64
                Top = 18
                Width = 517
                Height = 21
                KeyField = 'Filial'
                ListField = 'NOMEFILIAL'
                TabOrder = 1
                dmkEditCB = EdEmpresa
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
            end
            object BtHoje: TBitBtn
              Tag = 10131
              Left = 8
              Top = 44
              Width = 120
              Height = 40
              Caption = '&Hoje'
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtHojeClick
            end
            object BitBtn2: TBitBtn
              Tag = 191
              Left = 128
              Top = 44
              Width = 120
              Height = 40
              Caption = '&Este m'#234's'
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BitBtn2Click
            end
            object GroupBox7: TGroupBox
              Left = 264
              Top = 44
              Width = 317
              Height = 49
              Caption = ' Per'#237'odo da pesquisa: '
              TabOrder = 3
              object LaEmissIni: TLabel
                Left = 12
                Top = 24
                Width = 30
                Height = 13
                Caption = 'In'#237'cio:'
              end
              object LaEmissFim: TLabel
                Left = 168
                Top = 24
                Width = 19
                Height = 13
                Caption = 'Fim:'
              end
              object TPEmissIni: TdmkEditDateTimePicker
                Left = 48
                Top = 20
                Width = 112
                Height = 21
                Date = 41671.516375590300000000
                Time = 41671.516375590300000000
                TabOrder = 0
                OnClick = TPEmissIniClick
                OnChange = TPEmissIniChange
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPEmissFim: TdmkEditDateTimePicker
                Left = 192
                Top = 20
                Width = 112
                Height = 21
                Date = 41673.516439456000000000
                Time = 41673.516439456000000000
                TabOrder = 1
                OnClick = TPEmissFimClick
                OnChange = TPEmissFimChange
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 182
    Width = 592
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 588
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 588
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 238
    Width = 592
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 446
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 444
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAbertos: TBitBtn
        Tag = 5
        Left = 140
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Abertos'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAbertosClick
      end
      object BtOK: TBitBtn
        Tag = 5
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
  object QrSdoCartAcu: TMySQLQuery
    Database = Dmod.MyDB
    Left = 168
    Top = 48
  end
  object QrLcts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM _fluxcxadia_ '
      'ORDER BY Data, Controle ')
    Left = 80
    Top = 152
    object QrLctsData: TDateField
      FieldName = 'Data'
    end
    object QrLctsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrLctsCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctsSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLctsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctsSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrLctsNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLctsDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLctsCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctsCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLctsSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLctsDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLctsSit: TSmallintField
      FieldName = 'Sit'
    end
    object QrLctsVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLctsPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctsMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctsFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLctsCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctsForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLctsDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrLctsDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctsCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrLctsID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrLctsValAPag: TFloatField
      FieldName = 'ValAPag'
    end
    object QrLctsValARec: TFloatField
      FieldName = 'ValARec'
    end
    object QrLctsVTransf: TFloatField
      FieldName = 'VTransf'
    end
    object QrLctsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLctsPagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrLctsSeqPag: TIntegerField
      FieldName = 'SeqPag'
    end
    object QrLctsErrCtrl: TIntegerField
      FieldName = 'ErrCtrl'
    end
  end
  object frxFIN_RELAT_015_001: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Chart003AOnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  OK: Boolean;                                      '
      'begin'
      '  OK := <VAR_LINE_TEE_003>;                    '
      'end;'
      ''
      'begin'
      'end.')
    Left = 404
    Top = 16
    Datasets = <
      item
        DataSet = frxDsFCA
        DataSetName = 'frxDsFCA'
      end
      item
        DataSet = frxDsFCD
        DataSetName = 'frxDsFCD'
      end
      item
        DataSet = frxDsFCS
        DataSetName = 'frxDsFCS'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 45.354360000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 26.456692913385830000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'CAIXA DI'#193'RIO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 26.456692913385830000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        DataSet = frxDsFCD
        DataSetName = 'frxDsFCD'
        KeepTogether = True
        RowCount = 0
        object Memo24: TfrxMemoView
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data: [frxDsFCD."Data"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 593.386210000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Top = 166.299320000000000000
        Width = 680.315400000000000000
      end
      object GH_FCA: TfrxGroupHeader
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        Height = 45.354360000000000000
        ParentFont = False
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsFCA."NO_CARTEIRA"'
        object Memo5: TfrxMemoView
          Top = 26.456709999999930000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 98.267780000000000000
          Top = 26.456709999999990000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 498.897960000000000000
          Top = 26.456709999999930000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 45.354360000000000000
          Top = 26.456709999999930000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 438.425480000000000000
          Top = 26.456709999999930000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 559.370440000000000000
          Top = 26.456709999999930000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'A receber')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 619.842920000000000000
          Top = 26.456709999999930000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'A Pagar')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 268.346630000000000000
          Top = 26.456709999999930000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente / Fornecedor')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -17
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Carteira: [frxDsFCA."NO_CARTEIRA"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo32: TfrxMemoView
          Left = 192.756030000000000000
          Top = 26.456709999999990000
          Width = 37.795275590000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 230.551330000000000000
          Top = 26.456709999999990000
          Width = 37.795275590000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fornec.')
          ParentFont = False
        end
      end
      object GF_FCA: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 393.071120000000000000
        Width = 680.315400000000000000
        object Memo19: TfrxMemoView
          Left = 498.897960000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCA."Debito">)]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 438.425480000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCA."Credito">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 559.370440000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCA."VALMCRE">)]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCA."VALMDEB">)]')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 355.275820000000000000
        Width = 680.315400000000000000
        DataSet = frxDsFCA
        DataSetName = 'frxDsFCA'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 45.354330710000000000
          Height = 15.118110236220500000
          DataField = 'Data'
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFCA."Data"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 98.267780000000000000
          Width = 94.488188976377960000
          Height = 15.118110240000000000
          DataField = 'Descricao'
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFCA."Descricao"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 498.897960000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Debito'
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFCA."Debito"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 45.354360000000000000
          Width = 52.913385830000000000
          Height = 15.118110236220500000
          DataField = 'Controle'
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFCA."Controle"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 438.425480000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Credito'
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFCA."Credito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 559.370440000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFCA."VALMCRE"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'VALMDEB'
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFCA."VALMDEB"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 268.346630000000000000
          Width = 170.078740160000000000
          Height = 15.118110236220500000
          DataField = 'NO_TERCEIRO'
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFCA."NO_TERCEIRO"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 192.756030000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'Cliente'
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFCA."Cliente"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 230.551330000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'Fornecedor'
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFCA."Fornecedor"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 438.425480000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsFCS."TIPEPAGREC"'
        object Memo26: TfrxMemoView
          Width = 196.535450160000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo de carteira: [frxDsFCS."NO_TIPO_CART"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Left = 268.346630000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vendido')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 336.378170000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Adquirido')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo final')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 540.472790000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Movim. total')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 196.535560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo inicial')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 404.409710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Recebido')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 472.441250000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
      end
      object DetailData2: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 476.220780000000000000
        Width = 680.315400000000000000
        DataSet = frxDsFCS
        DataSetName = 'frxDsFCS'
        RowCount = 0
        object Memo25: TfrxMemoView
          Width = 196.535450160000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFCS."Carteira"] - [frxDsFCS."NO_CARTEIRA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 268.346630000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFCS."ImpCreMov"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 336.378170000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFCS."ImpDebMov"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(   <frxDsFCS."IMP_SDO_INI">   +   <frxDsFCS."ImpCreMov"> - <fr' +
              'xDsFCS."ImpDebMov">   -    <frxDsFCS."ValEmiPagC"> + <frxDsFCS."' +
              'ValEmiPagD">   )]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 540.472790000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(   <frxDsFCS."ImpCreMov"> - <frxDsFCS."ImpDebMov">   -    <frx' +
              'DsFCS."ValEmiPagC"> + <frxDsFCS."ValEmiPagD">   )]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 196.535560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFCS."IMP_SDO_INI"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 404.409710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFCS."ValEmiPagC"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 472.441250000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'ValEmiPagD'
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFCS."ValEmiPagD"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.897640240000000000
        Top = 514.016080000000000000
        Width = 680.315400000000000000
        object Memo40: TfrxMemoView
          Left = 268.346630000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCS."ImpCreMov">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 336.378170000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCS."ImpDebMov">)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Width = 196.535450160000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Somas [frxDsFCS."NO_TIPO_CART"]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 196.535560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCS."IMP_SDO_INI">)]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 540.472790000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(   <frxDsFCS."ImpCreMov"> - <frxDsFCS."ImpDebMov">   -    <' +
              'frxDsFCS."ValEmiPagC"> + <frxDsFCS."ValEmiPagD">   )]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(   <frxDsFCS."IMP_SDO_INI">   +   <frxDsFCS."ImpCreMov"> - ' +
              '<frxDsFCS."ImpDebMov">   -    <frxDsFCS."ValEmiPagC"> + <frxDsFC' +
              'S."ValEmiPagD">   )]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 404.409710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCS."ValEmiPagC">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 472.441250000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCS."ValEmiPagD">)]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Top = 143.622140000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsFCA."Data"'
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        object Memo51: TfrxMemoView
          Left = 268.346630000000000000
          Top = 3.779529999999994000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCS."ImpCreMov">,DetailData2)]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 336.378170000000000000
          Top = 3.779529999999994000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCS."ImpDebMov">,DetailData2)]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Top = 3.779529999999994000
          Width = 196.535523390000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCA
          DataSetName = 'frxDsFCA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Somas [frxDsFCD."Data"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 196.535560000000000000
          Top = 3.779529999999994000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCS."IMP_SDO_INI">,DetailData2)]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 540.472790000000000000
          Top = 3.779529999999994000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(   <frxDsFCS."ImpCreMov"> - <frxDsFCS."ImpDebMov">   -    <' +
              'frxDsFCS."ValEmiPagC"> + <frxDsFCS."ValEmiPagD">, DetailData2)]')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 612.283860000000000000
          Top = 3.779529999999994000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(   <frxDsFCS."IMP_SDO_INI">   +   <frxDsFCS."ImpCreMov"> - ' +
              '<frxDsFCS."ImpDebMov">   -    <frxDsFCS."ValEmiPagC"> + <frxDsFC' +
              'S."ValEmiPagD">,   DetailData2)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 404.409710000000000000
          Top = 3.779529999999994000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCS."ValEmiPagC">, DetailData2)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 472.441250000000000000
          Top = 3.779529999999994000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsFCS
          DataSetName = 'frxDsFCS'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsFCS."ValEmiPagD">, DetailData2)]')
          ParentFont = False
        end
      end
    end
  end
  object QrFCA: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFCACalcFields
    SQL.Strings = (
      'SELECT * FROM _fluxcxadia_')
    Left = 136
    Top = 152
    object QrFCAData: TDateField
      FieldName = 'Data'
    end
    object QrFCATipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrFCACarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrFCAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFCASub: TSmallintField
      FieldName = 'Sub'
    end
    object QrFCAGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrFCADescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrFCASerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrFCANotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrFCADebito: TFloatField
      FieldName = 'Debito'
    end
    object QrFCACredito: TFloatField
      FieldName = 'Credito'
    end
    object QrFCACompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrFCASerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrFCADocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrFCASit: TSmallintField
      FieldName = 'Sit'
    end
    object QrFCAVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrFCAPago: TFloatField
      FieldName = 'Pago'
    end
    object QrFCAMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrFCAFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrFCACliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrFCACliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrFCAForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrFCADataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrFCADuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrFCADepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrFCACtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrFCAID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrFCAValAPag: TFloatField
      FieldName = 'ValAPag'
    end
    object QrFCAValARec: TFloatField
      FieldName = 'ValARec'
    end
    object QrFCAVTransf: TFloatField
      FieldName = 'VTransf'
    end
    object QrFCAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFCANO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrFCANO_CARTEIRA: TWideStringField
      FieldName = 'NO_CARTEIRA'
      Size = 100
    end
    object QrFCAPagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrFCAVALMCRE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALMCRE'
      Calculated = True
    end
    object QrFCAVALMDEB: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALMDEB'
      Calculated = True
    end
    object QrFCAValPgCre: TFloatField
      FieldName = 'ValPgCre'
    end
    object QrFCAValPgDeb: TFloatField
      FieldName = 'ValPgDeb'
    end
    object QrFCAValPgTrf: TFloatField
      FieldName = 'ValPgTrf'
    end
  end
  object frxDsFCA: TfrxDBDataset
    UserName = 'frxDsFCA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'Tipo=Tipo'
      'Carteira=Carteira'
      'Controle=Controle'
      'Sub=Sub'
      'Genero=Genero'
      'Descricao=Descricao'
      'SerieNF=SerieNF'
      'NotaFiscal=NotaFiscal'
      'Debito=Debito'
      'Credito=Credito'
      'Compensado=Compensado'
      'SerieCH=SerieCH'
      'Documento=Documento'
      'Sit=Sit'
      'Vencimento=Vencimento'
      'Pago=Pago'
      'Mez=Mez'
      'Fornecedor=Fornecedor'
      'Cliente=Cliente'
      'CliInt=CliInt'
      'ForneceI=ForneceI'
      'DataDoc=DataDoc'
      'Duplicata=Duplicata'
      'Depto=Depto'
      'CtrlQuitPg=CtrlQuitPg'
      'ID_Pgto=ID_Pgto'
      'ValAPag=ValAPag'
      'ValARec=ValARec'
      'VTransf=VTransf'
      'Ativo=Ativo'
      'NO_TERCEIRO=NO_TERCEIRO'
      'NO_CARTEIRA=NO_CARTEIRA'
      'PagRec=PagRec'
      'VALMCRE=VALMCRE'
      'VALMDEB=VALMDEB'
      'ValPgCre=ValPgCre'
      'ValPgDeb=ValPgDeb'
      'ValPgTrf=ValPgTrf')
    DataSet = QrFCA
    BCDToCurrency = False
    Left = 136
    Top = 200
  end
  object QrPago: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito - Debito) Valor,'
      'SUM(Credito) Credito, SUM(Debito) Debito,'
      'COUNT(Controle) ITENS'
      'FROM lct0001a'
      'WHERE ID_Pgto=0'
      'AND Data<"2014-01-01"'
      '')
    Left = 196
    Top = 152
    object QrPagoValor: TFloatField
      FieldName = 'Valor'
    end
    object QrPagoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPagoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrPagoITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object QrFCD: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFCDBeforeClose
    AfterScroll = QrFCDAfterScroll
    Left = 256
    Top = 152
    object QrFCDData: TDateField
      FieldName = 'Data'
    end
  end
  object QrSumAnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _fluxcxadis_')
    Left = 196
    Top = 200
    object QrSumAntTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSumAntCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrSumAntDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrSumAntCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrSumAntValAPag: TFloatField
      FieldName = 'ValAPag'
    end
    object QrSumAntValARec: TFloatField
      FieldName = 'ValARec'
    end
    object QrSumAntVTransf: TFloatField
      FieldName = 'VTransf'
    end
    object QrSumAntPagRec: TSmallintField
      FieldName = 'PagRec'
    end
  end
  object QrFCS: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFCSCalcFields
    Left = 316
    Top = 152
    object QrFCSTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrFCSCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrFCSValAPagAnt: TFloatField
      FieldName = 'ValAPagAnt'
    end
    object QrFCSValARecAnt: TFloatField
      FieldName = 'ValARecAnt'
    end
    object QrFCSValAPagMov: TFloatField
      FieldName = 'ValAPagMov'
    end
    object QrFCSValARecMov: TFloatField
      FieldName = 'ValARecMov'
    end
    object QrFCSValAPagAcu: TFloatField
      FieldName = 'ValAPagAcu'
    end
    object QrFCSValARecAcu: TFloatField
      FieldName = 'ValARecAcu'
    end
    object QrFCSVTransfAcu: TFloatField
      FieldName = 'VTransfAcu'
    end
    object QrFCSNO_CARTEIRA: TWideStringField
      FieldName = 'NO_CARTEIRA'
      Size = 100
    end
    object QrFCSNO_TIPO_CART: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO_CART'
      Size = 255
      Calculated = True
    end
    object QrFCSImpCreAnt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ImpCreAnt'
      Calculated = True
    end
    object QrFCSImpDebAnt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ImpDebAnt'
      Calculated = True
    end
    object QrFCSImpCreMov: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ImpCreMov'
      Calculated = True
    end
    object QrFCSImpDebMov: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ImpDebMov'
      Calculated = True
    end
    object QrFCSImpCreAcu: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ImpCreAcu'
      Calculated = True
    end
    object QrFCSImpDebAcu: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ImpDebAcu'
      Calculated = True
    end
    object QrFCSEfetSdoAnt: TFloatField
      FieldName = 'EfetSdoAnt'
    end
    object QrFCSEfetMovCre: TFloatField
      FieldName = 'EfetMovCre'
    end
    object QrFCSEfetMovDeb: TFloatField
      FieldName = 'EfetMovDeb'
    end
    object QrFCSPagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrFCSTIPEPAGREC: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'TIPEPAGREC'
      Calculated = True
    end
    object QrFCSValEmiPagC: TFloatField
      FieldName = 'ValEmiPagC'
    end
    object QrFCSValEmiPagD: TFloatField
      FieldName = 'ValEmiPagD'
    end
    object QrFCSIMP_SDO_INI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'IMP_SDO_INI'
      Calculated = True
    end
  end
  object QrSumMov: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito, '
      'SUM(Debito) Debito, SUM(ValAPag) ValAPag, '
      'SUM(ValARec) ValARec, SUM(VTransf) VTransf, '
      'SUM(ValPgCre) ValPgCre, SUM(ValPgDeb) ValPgDeb, '
      'car.Tipo, car.PagRec, fcd.Carteira,'
      'IF(car.PagRec=0, 0, 1) TIPOMOV '
      'FROM _fluxcxadia_  fcd '
      'LEFT JOIN bugstrol.carteiras car ON car.Codigo=fcd.Carteira '
      'WHERE Data="2014-01-02" '
      'GROUP BY car.Tipo, car.Codigo, TIPOMOV ')
    Left = 196
    Top = 248
    object QrSumMovTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSumMovCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrSumMovDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrSumMovCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrSumMovValAPag: TFloatField
      FieldName = 'ValAPag'
    end
    object QrSumMovValARec: TFloatField
      FieldName = 'ValARec'
    end
    object QrSumMovVTransf: TFloatField
      FieldName = 'VTransf'
    end
    object QrSumMovPagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrSumMovValPgCre: TFloatField
      FieldName = 'ValPgCre'
    end
    object QrSumMovValPgDeb: TFloatField
      FieldName = 'ValPgDeb'
    end
    object QrSumMovTIPOMOV: TIntegerField
      FieldName = 'TIPOMOV'
      Required = True
    end
  end
  object frxDsFCD: TfrxDBDataset
    UserName = 'frxDsFCD'
    CloseDataSource = False
    DataSet = QrFCD
    BCDToCurrency = False
    Left = 256
    Top = 200
  end
  object QrDias: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO,'
      'car.Nome NO_CARTEIRA, fcd.* '
      'FROM _fluxcxadia_  fcd'
      
        'LEFT JOIN bugstrol_ddfog.carteiras car ON car.Codigo=fcd.Carteir' +
        'a'
      'LEFT JOIN bugstrol_ddfog.entidades ent ON ent.Codigo='
      '  IF(Fornecedor<>0, fcd.Fornecedor, fcd.Cliente)'
      'ORDER BY car.Ordem, NO_CART, Data, Controle ')
    Left = 80
    Top = 200
    object QrDiasData: TDateField
      FieldName = 'Data'
    end
  end
  object frxDsFCS: TfrxDBDataset
    UserName = 'frxDsFCS'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Tipo=Tipo'
      'Carteira=Carteira'
      'ValAPagAnt=ValAPagAnt'
      'ValARecAnt=ValARecAnt'
      'ValAPagMov=ValAPagMov'
      'ValARecMov=ValARecMov'
      'ValAPagAcu=ValAPagAcu'
      'ValARecAcu=ValARecAcu'
      'VTransfAcu=VTransfAcu'
      'NO_CARTEIRA=NO_CARTEIRA'
      'NO_TIPO_CART=NO_TIPO_CART'
      'ImpCreAnt=ImpCreAnt'
      'ImpDebAnt=ImpDebAnt'
      'ImpCreMov=ImpCreMov'
      'ImpDebMov=ImpDebMov'
      'ImpCreAcu=ImpCreAcu'
      'ImpDebAcu=ImpDebAcu'
      'EfetSdoAnt=EfetSdoAnt'
      'EfetMovCre=EfetMovCre'
      'EfetMovDeb=EfetMovDeb'
      'PagRec=PagRec'
      'TIPEPAGREC=TIPEPAGREC'
      'ValEmiPagC=ValEmiPagC'
      'ValEmiPagD=ValEmiPagD'
      'IMP_SDO_INI=IMP_SDO_INI')
    DataSet = QrFCS
    BCDToCurrency = False
    Left = 316
    Top = 200
  end
  object QrSaldos: TMySQLQuery
    Database = Dmod.MyDB
    Left = 80
    Top = 248
    object QrSaldosCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrSaldosTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrSaldosCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrSaldosDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrSaldosPagRec: TSmallintField
      FieldName = 'PagRec'
    end
  end
  object QrACA: TMySQLQuery
    Database = Dmod.MyDB
    Left = 376
    Top = 152
    object QrACAData: TDateField
      FieldName = 'Data'
    end
    object QrACATipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrACACarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrACAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrACASub: TSmallintField
      FieldName = 'Sub'
    end
    object QrACAGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrACADescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrACASerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrACANotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrACADebito: TFloatField
      FieldName = 'Debito'
    end
    object QrACACredito: TFloatField
      FieldName = 'Credito'
    end
    object QrACACompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrACASerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrACADocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrACASit: TSmallintField
      FieldName = 'Sit'
    end
    object QrACAVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrACAPago: TFloatField
      FieldName = 'Pago'
    end
    object QrACAMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrACAFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrACACliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrACACliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrACAForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrACADataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrACADuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrACADepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrACACtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrACAID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrACAValAPag: TFloatField
      FieldName = 'ValAPag'
    end
    object QrACAValARec: TFloatField
      FieldName = 'ValARec'
    end
    object QrACAVTransf: TFloatField
      FieldName = 'VTransf'
    end
    object QrACAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrACANO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object QrACANO_CARTEIRA: TWideStringField
      FieldName = 'NO_CARTEIRA'
      Size = 100
    end
    object QrACAPagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrACAIMP_ValAPag: TFloatField
      FieldName = 'IMP_ValAPag'
    end
    object QrACAIMP_ValARec: TFloatField
      FieldName = 'IMP_ValARec'
    end
  end
  object frxDsACA: TfrxDBDataset
    UserName = 'frxDsACA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'Tipo=Tipo'
      'Carteira=Carteira'
      'Controle=Controle'
      'Sub=Sub'
      'Genero=Genero'
      'Descricao=Descricao'
      'SerieNF=SerieNF'
      'NotaFiscal=NotaFiscal'
      'Debito=Debito'
      'Credito=Credito'
      'Compensado=Compensado'
      'SerieCH=SerieCH'
      'Documento=Documento'
      'Sit=Sit'
      'Vencimento=Vencimento'
      'Pago=Pago'
      'Mez=Mez'
      'Fornecedor=Fornecedor'
      'Cliente=Cliente'
      'CliInt=CliInt'
      'ForneceI=ForneceI'
      'DataDoc=DataDoc'
      'Duplicata=Duplicata'
      'Depto=Depto'
      'CtrlQuitPg=CtrlQuitPg'
      'ID_Pgto=ID_Pgto'
      'ValAPag=ValAPag'
      'ValARec=ValARec'
      'VTransf=VTransf'
      'Ativo=Ativo'
      'NO_TERCEIRO=NO_TERCEIRO'
      'NO_CARTEIRA=NO_CARTEIRA'
      'PagRec=PagRec'
      'IMP_ValAPag=IMP_ValAPag'
      'IMP_ValARec=IMP_ValARec')
    DataSet = QrACA
    BCDToCurrency = False
    Left = 376
    Top = 200
  end
  object QrACS: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrACSCalcFields
    Left = 440
    Top = 152
    object QrACSTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrACSCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrACSValAPagAnt: TFloatField
      FieldName = 'ValAPagAnt'
    end
    object QrACSValARecAnt: TFloatField
      FieldName = 'ValARecAnt'
    end
    object QrACSValAPagMov: TFloatField
      FieldName = 'ValAPagMov'
    end
    object QrACSValARecMov: TFloatField
      FieldName = 'ValARecMov'
    end
    object QrACSValAPagAcu: TFloatField
      FieldName = 'ValAPagAcu'
    end
    object QrACSValARecAcu: TFloatField
      FieldName = 'ValARecAcu'
    end
    object QrACSVTransfAcu: TFloatField
      FieldName = 'VTransfAcu'
    end
    object QrACSNO_CARTEIRA: TWideStringField
      FieldName = 'NO_CARTEIRA'
      Size = 100
    end
    object QrACSNO_TIPO_CART: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO_CART'
      Size = 255
      Calculated = True
    end
    object QrACSImpCreAnt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ImpCreAnt'
      Calculated = True
    end
    object QrACSImpDebAnt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ImpDebAnt'
      Calculated = True
    end
    object QrACSImpCreMov: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ImpCreMov'
      Calculated = True
    end
    object QrACSImpDebMov: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ImpDebMov'
      Calculated = True
    end
    object QrACSImpCreAcu: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ImpCreAcu'
      Calculated = True
    end
    object QrACSImpDebAcu: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ImpDebAcu'
      Calculated = True
    end
    object QrACSEfetSdoAnt: TFloatField
      FieldName = 'EfetSdoAnt'
    end
    object QrACSEfetMovCre: TFloatField
      FieldName = 'EfetMovCre'
    end
    object QrACSEfetMovDeb: TFloatField
      FieldName = 'EfetMovDeb'
    end
    object QrACSPagRec: TSmallintField
      FieldName = 'PagRec'
    end
  end
  object frxDsACS: TfrxDBDataset
    UserName = 'frxDsACS'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Tipo=Tipo'
      'Carteira=Carteira'
      'ValAPagAnt=ValAPagAnt'
      'ValARecAnt=ValARecAnt'
      'ValAPagMov=ValAPagMov'
      'ValARecMov=ValARecMov'
      'ValAPagAcu=ValAPagAcu'
      'ValARecAcu=ValARecAcu'
      'VTransfAcu=VTransfAcu'
      'NO_CARTEIRA=NO_CARTEIRA'
      'NO_TIPO_CART=NO_TIPO_CART'
      'ImpCreAnt=ImpCreAnt'
      'ImpDebAnt=ImpDebAnt'
      'ImpCreMov=ImpCreMov'
      'ImpDebMov=ImpDebMov'
      'ImpCreAcu=ImpCreAcu'
      'ImpDebAcu=ImpDebAcu'
      'EfetSdoAnt=EfetSdoAnt'
      'EfetMovCre=EfetMovCre'
      'EfetMovDeb=EfetMovDeb'
      'PagRec=PagRec'
      'TIPEPAGREC=TIPEPAGREC')
    DataSet = QrACS
    BCDToCurrency = False
    Left = 440
    Top = 200
  end
  object frxFIN_RELAT_015_002: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Chart003AOnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  OK: Boolean;                                      '
      'begin'
      '  OK := <VAR_LINE_TEE_003>;                    '
      'end;'
      ''
      'begin'
      'end.')
    Left = 404
    Top = 64
    Datasets = <
      item
        DataSet = frxDsACA
        DataSetName = 'frxDsACA'
      end
      item
        DataSet = frxDsACS
        DataSetName = 'frxDsACS'
      end
      item
        DataSet = frxDsTotAnt
        DataSetName = 'frxDsTotAnt'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 45.354360000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#245'es Abertas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 26.456692913385800000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 26.456692913385800000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERI_ANTES]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 415.748300000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH_ACA: TfrxGroupHeader
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsACA."NO_CARTEIRA"'
        object Memo5: TfrxMemoView
          Top = 18.897650000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 98.267780000000000000
          Top = 18.897650000000000000
          Width = 94.488164570000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 498.897960000000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 45.354360000000000000
          Top = 18.897650000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 438.425480000000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 559.370440000000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'A receber')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 619.842920000000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'A Pagar')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 268.346630000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente / Fornecedor')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Carteira: [frxDsACA."NO_CARTEIRA"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 192.756030000000000000
          Top = 18.897650000000000000
          Width = 37.795275590000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 230.551330000000000000
          Top = 18.897650000000000000
          Width = 37.795275590000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fornec.')
          ParentFont = False
        end
      end
      object GF_ACA: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        object Memo19: TfrxMemoView
          Left = 498.897960000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsACA."Debito">)]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 438.425480000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsACA."Credito">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 559.370440000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsACA."IMP_ValARec">)]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsACA."IMP_ValAPag">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Width = 438.425480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Soma da carteira: [frxDsACA."NO_CARTEIRA"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 143.622140000000000000
        Width = 680.315400000000000000
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 332.598640000000000000
        Width = 680.315400000000000000
        object Memo3: TfrxMemoView
          Left = 438.425480000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsACA."Credito">, MasterData1)]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 498.897960000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsACA."Debito">, MasterData1)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 559.370440000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsACA."IMP_ValARec">, MasterData1)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 619.842920000000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsACA."IMP_ValAPag">, MasterData1)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 343.937230000000000000
          Top = 3.779530000000022000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Soma geral')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        DataSet = frxDsACA
        DataSetName = 'frxDsACA'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsACA."Data"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 98.267780000000000000
          Width = 94.488188976377960000
          Height = 15.118110240000000000
          DataField = 'Descricao'
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsACA."Descricao"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 498.897960000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Debito'
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsACA."Debito"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 45.354360000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsACA."Controle"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 438.425480000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Credito'
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsACA."Credito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 559.370440000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'IMP_ValARec'
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsACA."IMP_ValARec"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'IMP_ValAPag'
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsACA."IMP_ValAPag"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 268.346630000000000000
          Width = 170.078740160000000000
          Height = 15.118110240000000000
          DataField = 'NO_TERCEIRO'
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsACA."NO_TERCEIRO"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 192.756030000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'Cliente'
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsACA."Cliente"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 230.551330000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'Fornecedor'
          DataSet = frxDsACA
          DataSetName = 'frxDsACA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsACA."Fornecedor"]')
          ParentFont = False
        end
      end
    end
  end
  object QrTotAnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(Credito) Credito, '
      'SUM(Debito) Debito,'
      ''
      'SUM(Credito) -'
      'SUM(IF(Compensado >= "2014-02-01", 0, '
      'IF(Credito > 0, Pago, 0))) AbertoCred,'
      ''
      'SUM(Debito) +'
      'SUM(IF(Compensado >= "2014-02-01", 0, '
      'IF(Debito > 0, Pago, 0))) AbertoDebi'
      ''
      'FROM _fluxcxadia_'
      'WHERE Data <"2014-02-01" ')
    Left = 504
    Top = 152
    object QrTotAntCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrTotAntDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrTotAntAbertoCred: TFloatField
      FieldName = 'AbertoCred'
    end
    object QrTotAntAbertoDebi: TFloatField
      FieldName = 'AbertoDebi'
    end
  end
  object frxDsTotAnt: TfrxDBDataset
    UserName = 'frxDsTotAnt'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Credito=Credito'
      'Debito=Debito'
      'AbertoCred=AbertoCred'
      'AbertoDebi=AbertoDebi')
    DataSet = QrTotAnt
    BCDToCurrency = False
    Left = 504
    Top = 200
  end
  object QrLct2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 20
    Top = 152
    object QrLct2Data: TDateField
      FieldName = 'Data'
    end
    object QrLct2Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrLct2Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLct2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLct2Sub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLct2Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLct2Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLct2SerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrLct2NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLct2Debito: TFloatField
      FieldName = 'Debito'
    end
    object QrLct2Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrLct2Compensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLct2SerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLct2Documento: TFloatField
      FieldName = 'Documento'
    end
    object QrLct2Sit: TSmallintField
      FieldName = 'Sit'
    end
    object QrLct2Vencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLct2Pago: TFloatField
      FieldName = 'Pago'
    end
    object QrLct2Mez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLct2Fornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLct2Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLct2CliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLct2ForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLct2DataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLct2Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrLct2Depto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLct2CtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrLct2ID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrLct2ValAPag: TFloatField
      FieldName = 'ValAPag'
    end
    object QrLct2ValARec: TFloatField
      FieldName = 'ValARec'
    end
    object QrLct2VTransf: TFloatField
      FieldName = 'VTransf'
    end
    object QrLct2PagRec: TSmallintField
      FieldName = 'PagRec'
    end
  end
  object QrLct0: TMySQLQuery
    Database = Dmod.MyDB
    Left = 20
    Top = 200
  end
  object QrSit2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 256
    Top = 248
    object QrSit2Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPago1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 316
    Top = 248
    object QrPago1Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrPago1Debito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrSumMv2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 376
    Top = 248
    object QrSumMv2Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSumMv2PagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrSumMv2Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrSumMv2Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrSumMv2Debito: TFloatField
      FieldName = 'Debito'
    end
  end
  object PMOK: TPopupMenu
    Left = 492
    Top = 60
    object odascarteiras1: TMenuItem
      Caption = '&Todas carteiras'
      OnClick = odascarteiras1Click
    end
    object Carteirasobrigatrias1: TMenuItem
      Caption = '&Carteiras obrigat'#243'rias'
      OnClick = Carteirasobrigatrias1Click
    end
  end
  object QrSumMv1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 440
    Top = 248
    object QrSumMv1Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSumMv1PagRec: TSmallintField
      FieldName = 'PagRec'
    end
    object QrSumMv1Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrSumMv1Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrSumMv1Debito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrPagoO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito - Debito) Valor,'
      'SUM(Credito) Credito, SUM(Debito) Debito,'
      'COUNT(Controle) ITENS'
      'FROM lct0001a'
      'WHERE ID_Pgto=0'
      'AND Data<"2014-01-01"'
      '')
    Left = 560
    Top = 152
    object QrPagoOValor: TFloatField
      FieldName = 'Valor'
    end
    object QrPagoOCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPagoODebito: TFloatField
      FieldName = 'Debito'
    end
    object QrPagoOITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrPagoOID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
  end
end
