unit FluxCxaDia1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, mySQLDbTables, frxClass, frxDBSet;

type
  TFmFluxCxaDia1 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel12: TPanel;
    Panel11: TPanel;
    Panel15: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrSdoCartAcu: TmySQLQuery;
    QrLcts: TmySQLQuery;
    QrLctsData: TDateField;
    QrLctsTipo: TIntegerField;
    QrLctsCarteira: TIntegerField;
    QrLctsControle: TIntegerField;
    QrLctsSub: TSmallintField;
    QrLctsGenero: TIntegerField;
    QrLctsDescricao: TWideStringField;
    QrLctsSerieNF: TWideStringField;
    QrLctsNotaFiscal: TIntegerField;
    QrLctsDebito: TFloatField;
    QrLctsCredito: TFloatField;
    QrLctsCompensado: TDateField;
    QrLctsSerieCH: TWideStringField;
    QrLctsDocumento: TFloatField;
    QrLctsSit: TSmallintField;
    QrLctsVencimento: TDateField;
    QrLctsPago: TFloatField;
    QrLctsMez: TIntegerField;
    QrLctsFornecedor: TIntegerField;
    QrLctsCliente: TIntegerField;
    QrLctsCliInt: TIntegerField;
    QrLctsForneceI: TIntegerField;
    QrLctsDataDoc: TDateField;
    QrLctsDuplicata: TWideStringField;
    QrLctsDepto: TIntegerField;
    QrLctsCtrlQuitPg: TIntegerField;
    QrLctsID_Pgto: TIntegerField;
    QrLctsValAPag: TFloatField;
    QrLctsValARec: TFloatField;
    QrLctsVTransf: TFloatField;
    QrLctsAtivo: TSmallintField;
    PB1: TProgressBar;
    frxFIN_RELAT_015_001: TfrxReport;
    QrFCA: TmySQLQuery;
    QrFCAData: TDateField;
    QrFCATipo: TIntegerField;
    QrFCACarteira: TIntegerField;
    QrFCAControle: TIntegerField;
    QrFCASub: TSmallintField;
    QrFCAGenero: TIntegerField;
    QrFCADescricao: TWideStringField;
    QrFCASerieNF: TWideStringField;
    QrFCANotaFiscal: TIntegerField;
    QrFCADebito: TFloatField;
    QrFCACredito: TFloatField;
    QrFCACompensado: TDateField;
    QrFCASerieCH: TWideStringField;
    QrFCADocumento: TFloatField;
    QrFCASit: TSmallintField;
    QrFCAVencimento: TDateField;
    QrFCAPago: TFloatField;
    QrFCAMez: TIntegerField;
    QrFCAFornecedor: TIntegerField;
    QrFCACliente: TIntegerField;
    QrFCACliInt: TIntegerField;
    QrFCAForneceI: TIntegerField;
    QrFCADataDoc: TDateField;
    QrFCADuplicata: TWideStringField;
    QrFCADepto: TIntegerField;
    QrFCACtrlQuitPg: TIntegerField;
    QrFCAID_Pgto: TIntegerField;
    QrFCAValAPag: TFloatField;
    QrFCAValARec: TFloatField;
    QrFCAVTransf: TFloatField;
    QrFCAAtivo: TSmallintField;
    frxDsFCA: TfrxDBDataset;
    QrPago: TmySQLQuery;
    QrPagoValor: TFloatField;
    QrFCANO_TERCEIRO: TWideStringField;
    QrFCANO_CARTEIRA: TWideStringField;
    QrFCD: TmySQLQuery;
    QrFCDData: TDateField;
    QrSumAnt: TmySQLQuery;
    QrFCS: TmySQLQuery;
    QrSumAntTipo: TIntegerField;
    QrSumAntCarteira: TIntegerField;
    QrSumAntDebito: TFloatField;
    QrSumAntCredito: TFloatField;
    QrSumAntValAPag: TFloatField;
    QrSumAntValARec: TFloatField;
    QrSumAntVTransf: TFloatField;
    QrSumMov: TmySQLQuery;
    QrSumMovTipo: TIntegerField;
    QrSumMovCarteira: TIntegerField;
    QrSumMovDebito: TFloatField;
    QrSumMovCredito: TFloatField;
    QrSumMovValAPag: TFloatField;
    QrSumMovValARec: TFloatField;
    QrSumMovVTransf: TFloatField;
    frxDsFCD: TfrxDBDataset;
    QrDias: TmySQLQuery;
    QrDiasData: TDateField;
    frxDsFCS: TfrxDBDataset;
    QrSaldos: TmySQLQuery;
    QrSaldosCarteira: TIntegerField;
    QrSaldosTipo: TSmallintField;
    QrSaldosCredito: TFloatField;
    QrSaldosDebito: TFloatField;
    QrFCSTipo: TIntegerField;
    QrFCSCarteira: TIntegerField;
    QrFCSValAPagAnt: TFloatField;
    QrFCSValARecAnt: TFloatField;
    QrFCSValAPagMov: TFloatField;
    QrFCSValARecMov: TFloatField;
    QrFCSValAPagAcu: TFloatField;
    QrFCSValARecAcu: TFloatField;
    QrFCSVTransfAcu: TFloatField;
    QrFCSNO_CARTEIRA: TWideStringField;
    QrFCSNO_TIPO_CART: TWideStringField;
    QrFCSImpCreAnt: TFloatField;
    QrFCSImpDebAnt: TFloatField;
    QrFCSImpCreMov: TFloatField;
    QrFCSImpDebMov: TFloatField;
    QrFCSImpCreAcu: TFloatField;
    QrFCSImpDebAcu: TFloatField;
    QrFCSEfetSdoAnt: TFloatField;
    QrFCSEfetMovCre: TFloatField;
    QrFCSEfetMovDeb: TFloatField;
    BtAbertos: TBitBtn;
    QrLctsPagRec: TSmallintField;
    QrSumAntPagRec: TSmallintField;
    QrSaldosPagRec: TSmallintField;
    QrSumMovPagRec: TSmallintField;
    QrFCSPagRec: TSmallintField;
    QrACA: TmySQLQuery;
    frxDsACA: TfrxDBDataset;
    QrACAData: TDateField;
    QrACATipo: TIntegerField;
    QrACACarteira: TIntegerField;
    QrACAControle: TIntegerField;
    QrACASub: TSmallintField;
    QrACAGenero: TIntegerField;
    QrACADescricao: TWideStringField;
    QrACASerieNF: TWideStringField;
    QrACANotaFiscal: TIntegerField;
    QrACADebito: TFloatField;
    QrACACredito: TFloatField;
    QrACACompensado: TDateField;
    QrACASerieCH: TWideStringField;
    QrACADocumento: TFloatField;
    QrACASit: TSmallintField;
    QrACAVencimento: TDateField;
    QrACAPago: TFloatField;
    QrACAMez: TIntegerField;
    QrACAFornecedor: TIntegerField;
    QrACACliente: TIntegerField;
    QrACACliInt: TIntegerField;
    QrACAForneceI: TIntegerField;
    QrACADataDoc: TDateField;
    QrACADuplicata: TWideStringField;
    QrACADepto: TIntegerField;
    QrACACtrlQuitPg: TIntegerField;
    QrACAID_Pgto: TIntegerField;
    QrACAValAPag: TFloatField;
    QrACAValARec: TFloatField;
    QrACAVTransf: TFloatField;
    QrACAAtivo: TSmallintField;
    QrACANO_TERCEIRO: TWideStringField;
    QrACANO_CARTEIRA: TWideStringField;
    QrACS: TmySQLQuery;
    frxDsACS: TfrxDBDataset;
    frxFIN_RELAT_015_002: TfrxReport;
    QrACSTipo: TIntegerField;
    QrACSCarteira: TIntegerField;
    QrACSValAPagAnt: TFloatField;
    QrACSValARecAnt: TFloatField;
    QrACSValAPagMov: TFloatField;
    QrACSValARecMov: TFloatField;
    QrACSValAPagAcu: TFloatField;
    QrACSValARecAcu: TFloatField;
    QrACSVTransfAcu: TFloatField;
    QrACSNO_CARTEIRA: TWideStringField;
    QrACSNO_TIPO_CART: TWideStringField;
    QrACSImpCreAnt: TFloatField;
    QrACSImpDebAnt: TFloatField;
    QrACSImpCreMov: TFloatField;
    QrACSImpDebMov: TFloatField;
    QrACSImpCreAcu: TFloatField;
    QrACSImpDebAcu: TFloatField;
    QrACSEfetSdoAnt: TFloatField;
    QrACSEfetMovCre: TFloatField;
    QrACSEfetMovDeb: TFloatField;
    QrACSPagRec: TSmallintField;
    QrTotAnt: TmySQLQuery;
    QrTotAntCredito: TFloatField;
    QrTotAntDebito: TFloatField;
    QrTotAntAbertoCred: TFloatField;
    QrTotAntAbertoDebi: TFloatField;
    frxDsTotAnt: TfrxDBDataset;
    QrLct2: TmySQLQuery;
    QrLct2Data: TDateField;
    QrLct2Tipo: TIntegerField;
    QrLct2Carteira: TIntegerField;
    QrLct2Controle: TIntegerField;
    QrLct2Sub: TSmallintField;
    QrLct2Genero: TIntegerField;
    QrLct2Descricao: TWideStringField;
    QrLct2SerieNF: TWideStringField;
    QrLct2NotaFiscal: TIntegerField;
    QrLct2Debito: TFloatField;
    QrLct2Credito: TFloatField;
    QrLct2Compensado: TDateField;
    QrLct2SerieCH: TWideStringField;
    QrLct2Documento: TFloatField;
    QrLct2Sit: TSmallintField;
    QrLct2Vencimento: TDateField;
    QrLct2Pago: TFloatField;
    QrLct2Mez: TIntegerField;
    QrLct2Fornecedor: TIntegerField;
    QrLct2Cliente: TIntegerField;
    QrLct2CliInt: TIntegerField;
    QrLct2ForneceI: TIntegerField;
    QrLct2DataDoc: TDateField;
    QrLct2Duplicata: TWideStringField;
    QrLct2Depto: TIntegerField;
    QrLct2CtrlQuitPg: TIntegerField;
    QrLct2ID_Pgto: TIntegerField;
    QrLct2ValAPag: TFloatField;
    QrLct2ValARec: TFloatField;
    QrLct2VTransf: TFloatField;
    QrLct2Ativo: TSmallintField;
    QrLct2PagRec: TSmallintField;
    QrFCAPagRec: TSmallintField;
    QrFCAValPgCre: TFloatField;
    QrFCAValPgDeb: TFloatField;
    QrFCAValPgTrf: TFloatField;
    BtHoje: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox7: TGroupBox;
    LaEmissIni: TLabel;
    LaEmissFim: TLabel;
    TPEmissIni: TdmkEditDateTimePicker;
    TPEmissFim: TdmkEditDateTimePicker;
    QrLctsSeqPag: TIntegerField;
    QrLct0: TmySQLQuery;
    QrFCSTIPEPAGREC: TIntegerField;
    QrSumMovValPgCre: TFloatField;
    QrSumMovValPgDeb: TFloatField;
    QrSumMovTIPOMOV: TIntegerField;
    QrFCAVALMCRE: TFloatField;
    QrFCAVALMDEB: TFloatField;
    QrFCSValEmiPagC: TFloatField;
    QrFCSValEmiPagD: TFloatField;
    QrSit2: TmySQLQuery;
    QrSit2Controle: TIntegerField;
    QrPago1: TmySQLQuery;
    QrPago1Credito: TFloatField;
    QrPago1Debito: TFloatField;
    QrFCSIMP_SDO_INI: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure QrFCDAfterScroll(DataSet: TDataSet);
    procedure QrFCDBeforeClose(DataSet: TDataSet);
    procedure QrFCSCalcFields(DataSet: TDataSet);
    procedure BtAbertosClick(Sender: TObject);
    procedure QrACSCalcFields(DataSet: TDataSet);
    procedure QrFCACalcFields(DataSet: TDataSet);
    procedure BtHojeClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure TPEmissIniChange(Sender: TObject);
    procedure TPEmissIniClick(Sender: TObject);
    procedure TPEmissFimChange(Sender: TObject);
    procedure TPEmissFimClick(Sender: TObject);
  private
    { Private declarations }
    FTabLctA,
    FEmissIni, FEmissFim, FNO_Empresa, FPeriodo,
    FAntesDe,
    FFluxCxaDia, FFluxCxaDiD, FFluxCxaDiS: String;
    //FDtaImp: TDateTime;
    //
    procedure ImprimeFluxo_001();
    function  Mais(Val: Double): Double;
    function  Menos(Val: Double): Double;
    procedure FechaPesquisa();
    procedure ReopenFCD();
    procedure ReopenDias();
  public
    { Public declarations }
  end;

  var
  FmFluxCxaDia1: TFmFluxCxaDia1;

implementation

uses UnMyObjects, Module, ModuleGeral, UCreateFin, DmkDAC_PF, UMySQLModule,
UnDmkProcFunc;

{$R *.DFM}

procedure TFmFluxCxaDia1.BitBtn2Click(Sender: TObject);
var
  DataIni: TDateTime;
begin
  DataIni := Geral.PrimeiroDiaDoMes(Date);
  TPEmissIni.Date := Int(DataIni);
  TPEmissFim.Date := Int(Date());
end;

procedure TFmFluxCxaDia1.BtAbertosClick(Sender: TObject);
const
  Cliente = 0;
var
  AteData: String;
begin
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrACA, DModG.MyPID_DB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, ',
  'car.Nome NO_CARTEIRA, fcd.* ',
  'FROM _fluxcxadia_  fcd ',
  'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=fcd.Carteira ',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo',
  '  = IF(Fornecedor<>0, fcd.Fornecedor, fcd.Cliente) ',
  'WHERE Data <"' + FEmissIni + '" ',
  'ORDER BY car.Ordem, NO_CARTEIRA, Data, Controle ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrACS, DModG.MyPID_DB, [
  'SELECT fcs.Tipo, fcs.PagRec, fcs.Carteira, ',
  'SUM(fcs.ValAPagAnt) ValAPagAnt, ',
  'SUM(fcs.ValARecAnt) ValARecAnt, ',
  'SUM(fcs.VTransfAnt) VTransfAnt, ',
  'SUM(fcs.ValAPagMov) ValAPagMov, ',
  'SUM(fcs.ValARecMov) ValARecMov, ',
  'SUM(fcs.VTransfMov) VTransfMov, ',

  'SUM(fcs.ValAPagAnt + fcs.ValAPagMov) ValAPagAcu, ',
  'SUM(fcs.ValARecAnt + fcs.ValARecMov) ValARecAcu, ',
  'SUM(fcs.VTransfAnt + fcs.VTransfMov) VTransfAcu, ',

  'SUM(fcs.EfetSdoAnt) EfetSdoAnt, ',
  'SUM(fcs.EfetMovCre) EfetMovCre, ',
  'SUM(fcs.EfetMovDeb) EfetMovDeb, ',

  'car.Nome NO_CARTEIRA ',
  'FROM _fluxcxadis_ fcs ',
  'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=fcs.Carteira ',
  'WHERE Data<"' + FEmissIni + '" ',
  'GROUP BY car.Tipo, car.Codigo ',
  'ORDER BY car.Ordem, car.Nome ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTotAnt, DModG.MyPID_DB, [
  'SELECT ',
  'SUM(Credito) Credito, ',
  'SUM(Debito) Debito, ',
  'SUM(Credito) - ',
  'SUM(IF(Compensado >= "' + FEmissIni + '", 0, ',
  'IF(Credito <> 0, Pago, 0))) AbertoCred, ',
  'SUM(Debito) + ',
  'SUM(IF(Compensado >= "' + FEmissIni + '", 0, ',
  'IF(Debito <> 0, Pago, 0))) AbertoDebi ',
  'FROM _fluxcxadia_ ',
  'WHERE Data <"' + FEmissIni + '" ',
  '']);
  //
  AteData := 'Antes de ' + FAntesDe;
  frxFIN_RELAT_015_002.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxFIN_RELAT_015_002.Variables['VARF_DATA']    := Now();
  frxFIN_RELAT_015_002.Variables['VARF_PERI_ANTES'] := QuotedStr(AteData);
  MyObjects.frxDefineDataSets(frxFIN_RELAT_015_002, [
    frxDsACA,
    frxDsACS,
    frxDsTotAnt
  ]);
  MyObjects.frxMostra(frxFIN_RELAT_015_002, 'Emiss�es Abertas');
end;

procedure TFmFluxCxaDia1.BtHojeClick(Sender: TObject);
begin
  TPEmissIni.Date := Int(Date());
  TPEmissFim.Date := Int(Date());
end;

procedure TFmFluxCxaDia1.BtOKClick(Sender: TObject);
var
  DataI, DataF: TDateTime;
  Data, SQL: String;
  ValAPag, ValARec, VTransf: Double;
  Entidade, Erros: Integer;
  Tipo, PagRec, Carteira, Sub: Integer;
  ValAPagAnt, ValARecAnt, VTransfAnt, ValAPagMov, ValARecMov, VTransfMov,
  EfetSdoAnt, EfetMovCre, EfetMovDeb, EfetSdoAcu, ValPgCre, ValPgDeb, ValPgTrf,
  Controle: Double;
  //
  Descricao, SerieNF, Compensado, SerieCH, Vencimento, DataDoc, Duplicata: String;
  Genero, NotaFiscal, Sit, Mez, Fornecedor, Cliente, CliInt, ForneceI, Depto,
  CtrlQuitPg, ID_Pgto, SeqPag: Integer;
  Debito, Credito, Documento, Pago, ValEmiPagC, ValEmiPagD,
  PagoParciC, PagoParciD: Double;
  DataA, DataB, SQLDtaX: String;
begin
  if not UMyMod.ObtemCodigoDeCodUsu(EdEmpresa, Entidade, 'Informe a empresa',
  'Codigo', 'FILIAL') then
    Exit;
  Erros := 0;
  //
  DataI := Trunc(TPEmissIni.Date);
  DataF := Trunc(TPEmissFim.Date);
  FEmissIni := Geral.FDT(DataI, 1);
  FAntesDe  := Geral.FDT(DataI, 3);
  FEmissFim := Geral.FDT(TPEmissFim.Date, 1);
  //
  // Lancamentos em emissoes lancados antes da data inicial da pesquisa e que
  // continuam abertas ou foram quitadas a partir do dia inicial da pesquisa.
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DELETE FROM _fluxcxadia_; ',
  'INSERT INTO _fluxcxadia_ ',
  'SELECT lct.Data, car.Tipo, car.PagRec, lct.Carteira, lct.Controle, lct.Sub, 0 SeqPag, ',
  'lct.Genero, lct.Descricao, lct.SerieNF, lct.NotaFiscal, ',
  'lct.Debito, lct.Credito, lct.Compensado, lct.SerieCH, ',
  'lct.Documento, lct.Sit, lct.Vencimento, lct.Pago, lct.Mez, ',
  'lct.Fornecedor, lct.Cliente, lct.CliInt, lct.ForneceI, ',
  'lct.DataDoc, lct.Duplicata, lct.Depto, lct.CtrlQuitPg, ',
  'lct.ID_Pgto, 0 ValAPag, 0 ValARec, 0 VTransf, ',
  '0 ValPgCre, 0 ValPgDeb, 0 ValPgTrf, ',
  '1 Ativo ',
  'FROM ' + FTabLctA + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira ',
  'WHERE Data < "' + FEmissIni + '" ',
  'AND car.Tipo=2 ',
  'AND (lct.Sit<2 ',
  'OR lct.Compensado >= "' + FEmissIni + '") ',
  '']));
  //
  // Lancamentos em emissoes durante o periodo da pesquisa.
  SQL := Geral.ATS([
  'INSERT INTO _fluxcxadia_ ',
  'SELECT lct.Data, car.Tipo, car.PagRec, lct.Carteira, lct.Controle, lct.Sub, 0 SeqPag, ',
  'lct.Genero, lct.Descricao, lct.SerieNF, lct.NotaFiscal, ',
  'lct.Debito, lct.Credito, lct.Compensado, lct.SerieCH, ',
  'lct.Documento, lct.Sit, lct.Vencimento, lct.Pago, lct.Mez, ',
  'lct.Fornecedor, lct.Cliente, lct.CliInt, lct.ForneceI, ',
  'lct.DataDoc, lct.Duplicata, lct.Depto, lct.CtrlQuitPg, ',
  'lct.ID_Pgto, 0 ValAPag, 0 ValARec, 0 VTransf, ',
  '0 ValPgCre, 0 ValPgDeb, 0 ValPgTrf, ',
  '1 Ativo ',
  'FROM ' + FTabLctA + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira ',
  'WHERE Data BETWEEN "' + FEmissIni + '" AND "' + FEmissFim + '" ',
  'AND car.Tipo=2 ',
  '']);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);
  //
  // Excluir o controle zero pois veio junto e nao interessa.
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DELETE FROM _fluxcxadia_ WHERE Controle=0',
  '']));
  //
  // Abrir os lancamentos de emissoes e trata-los conforme sua quitacao ou nao.
  UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM _fluxcxadia_ ',
  'ORDER BY Data, Controle ',
  '']);
  PB1.Position := 0;
  PB1.Max := QrLcts.RecordCount;
  QrLcts.First;
  while not QrLcts.Eof do
  begin
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    if (QrLctsTipo.Value = 2) then
    begin
      if ((QrLctsSit.Value < 2) and (QrLctsCompensado.Value > 2))
      or ((QrLctsSit.Value > 1) and (QrLctsCompensado.Value < 2)) then
      begin
        // Erro que pode estragar os saldos e somas.
        Geral.MB_ERRO('O lan�amento controle n�mero ' +
        Geral.FFI(QrLctsControle.Value) + ' est� quitado incorretamente!' +
        sLineBreak + '"Sit" n�o combina com "Compensado"' + sLineBreak +
        'Sit: ' + Geral.FF0(QrLctsSit.Value) + sLineBreak +
        'Compensado: ' + Geral.FDT(QrLctsCompensado.Value, 2) + sLineBreak +
        'Valor: ' +
        Geral.FFT(QrLctsCredito.Value - QrLctsDebito.Value, 2, siNegativo) +
        sLineBreak + 'Pago: ' + Geral.FFT(QrLctsPago.Value, 2, siNegativo));
        //
        Erros := Erros + 1;
      end;
      if (QrLctsSit.Value < 2)
      or ((QrLctsSit.Value > 1) and (QrLctsCompensado.Value > QrLctsData.Value)) then
      begin
        // Emissoes com pagamento parcial.
        // Somar apenas valores pagos antes do dia inicial da pesquisa.
        if ((QrLctsSit.Value = 1) and (QrLctsCompensado.Value < 2)) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrPago, Dmod.MyDB, [
          'SELECT SUM(Credito - Debito) Valor ',
          'FROM ' + FTabLctA,
          'WHERE ID_Pgto=' + Geral.FFI(QrLctsControle.Value),
          'AND Data<"' + FEmissIni + '"',
          '']);
          ValAPag  := 0;
          ValARec  := 0;
          // Nao pode!! Erro quando QrPagoValor.Value = 0
          //if QrPagoValor.Value > 0 then
          if (QrLctsCredito.Value - QrLctsDebito.Value > 0) then
            ValARec := QrLctsCredito.Value - QrLctsDebito.Value - QrPagoValor.Value
          else
            ValAPag := QrLctsCredito.Value + QrLctsDebito.Value + QrPagoValor.Value
        end else
        begin
          ValAPag  := QrLctsDebito.Value;
          ValARec  := QrLctsCredito.Value;
          //
        end;
        //VTransf ???
        Data     := Geral.FDT(QrLctsData.Value, 1);
        Tipo     := QrLctsTipo.Value;
        Pagrec   := QrLctsPagRec.Value;
        Carteira := QrLctsCarteira.Value;
        Controle := QrLctsControle.Value;
        Sub      := QrLctsSub.Value;
        SeqPag   := QrLctsSeqPag.Value;
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_fluxcxadia_', False, [
        'ValAPag', 'ValARec', 'VTransf'], [
        'Data', 'Tipo', 'Pagrec',
        'Carteira', 'Controle', 'Sub', 'SeqPag'], [
        ValAPag, ValARec, VTransf], [
        Data, Tipo, PagRec,
        Carteira, Controle, Sub, SeqPag], False);
      end;
    end;
    //
    QrLcts.Next;
  end;
  if Erros > 0 then
    Exit;
  //
  //  Lancamentos Direto em caixa!!!
  SQL := Geral.ATS([
  'INSERT INTO _fluxcxadia_ ',
  'SELECT lct.Data, car.Tipo, car.PagRec, lct.Carteira, lct.Controle, lct.Sub, 0 SeqPag, ',
  'lct.Genero, lct.Descricao, lct.SerieNF, lct.NotaFiscal, ',
  'lct.Debito, lct.Credito, lct.Compensado, lct.SerieCH, ',
  'lct.Documento, lct.Sit, lct.Vencimento, lct.Pago, lct.Mez, ',
  'lct.Fornecedor, lct.Cliente, lct.CliInt, lct.ForneceI, ',
  'lct.DataDoc, lct.Duplicata, lct.Depto, lct.CtrlQuitPg, ',
  'lct.ID_Pgto, 0 ValAPag, 0 ValARec, 0 VTransf, ',
  '0 ValPgCre, 0 ValPgDeb, 0 ValPgTrf, ',
  '1 Ativo ',
  'FROM ' + FTabLctA + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira ',
  'WHERE Data BETWEEN "' + FEmissIni + '" AND "' + FEmissFim + '" ',
  'AND car.Tipo=0 ',
  '']);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);

  // Lancamentos diretos em Banco ou quitacoes / pagamentos de emissoes!!!
  SQL := Geral.ATS([
  'INSERT INTO _fluxcxadia_ ',
  'SELECT lct.Data, car.Tipo, car.PagRec, lct.Carteira, lct.Controle, lct.Sub, 0 SeqPag, ',
  'lct.Genero, lct.Descricao, lct.SerieNF, lct.NotaFiscal, ',
  'lct.Debito, lct.Credito, lct.Compensado, lct.SerieCH, ',
  'lct.Documento, lct.Sit, lct.Vencimento, lct.Pago, lct.Mez, ',
  'lct.Fornecedor, lct.Cliente, lct.CliInt, lct.ForneceI, ',
  'lct.DataDoc, lct.Duplicata, lct.Depto, lct.CtrlQuitPg, ',
  'lct.ID_Pgto, 0 ValAPag, 0 ValARec, 0 VTransf, ',
  '0 ValPgCre, 0 ValPgDeb, 0 ValPgTrf, ',
  '1 Ativo ',
  'FROM ' + FTabLctA + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira ',
  'WHERE Data BETWEEN "' + FEmissIni + '" AND "' + FEmissFim + '" ',
  'AND car.Tipo=1 ',
  '']);
  //Geral.MB_Aviso(SQL);
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, SQL);

{ SQL que agrupa e explica quitacoes
SELECT Tipo, IF(ID_Pgto=0, 0, 1) _ID_Pgto,
IF(CtrlQuitPg=0, 0, 1) _CtrlQuitPg,
COUNT(Controle) ITENS
FROM lct0001a
GROUP BY Tipo, _ID_Pgto, _CtrlQuitPg
}

  // Abrir os lancamentos de caixas e bancos que quitaram emissoes para gerar
  // lancamentos que neutralizem estas emissoes (quando houver) que estao
  // atreladas a estes lancamentos em caixa e banco.
  // Usar campos especificos para isto! >>
  UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM _fluxcxadia_ ',
  'WHERE Tipo < 2',
  'AND ID_Pgto <> 0 ',
  'ORDER BY Data, Controle ',
  '']);
  PB1.Position := 0;
  PB1.Max := QrLcts.RecordCount;
  QrLcts.First;
  while not QrLcts.Eof do
  begin
    {
    if (QrLctsID_Pgto.Value = 4178)
    or (QrLctsID_Pgto.Value = 4641)
    or (QrLctsControle.Value = 4178)
    or (QrLctsControle.Value = 4641) then
      Geral.MB_Aviso('ID_Pgto: ' + Geral.FF0(QrLctsID_Pgto.Value) +
      sLineBreak + 'Controle: ' + Geral.FF0(QrLctsControle.Value));
    //
    }
    MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
    if (QrLctsTipo.Value < 2) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLct2, DModG.MyPID_DB, [
      'SELECT * ',
      'FROM _fluxcxadia_ ',
      'WHERE Controle =' + Geral.FF0(QrLctsID_Pgto.Value),
      '']);
      if QrLct2.RecordCount > 0 then
      begin
        Pagrec     := QrLct2PagRec.Value;
        Genero     := QrLct2Genero.Value;
        Descricao  := QrLct2Descricao.Value;
        SerieNF    := QrLct2SerieNF.Value;
        NotaFiscal := QrLct2NotaFiscal.Value;
        // 2014-04-11
        Debito     := -QrLctsCredito.Value;
        Credito    := -QrLctsDebito.Value;
        Compensado := Geral.FDT(QrLctsCompensado.Value, 1);
        SerieCH    := QrLct2SerieCH.Value;
        Documento  := QrLct2Documento.Value;
        Sit        := 2;
        Vencimento := Geral.FDT(QrLct2Vencimento.Value, 1);
        Pago       := QrLctsCredito.Value - QrLctsDebito.Value;
        Mez        := QrLct2Mez.Value;
        Fornecedor := QrLct2Fornecedor.Value;
        Cliente    := QrLct2Cliente.Value;
        CliInt     := QrLct2CliInt.Value;
        ForneceI   := QrLct2ForneceI.Value;
        DataDoc    := Geral.FDT(QrLct2DataDoc.Value, 1);
        Duplicata  := QrLct2Duplicata.Value;
        Depto      := QrLct2Depto.Value;
        CtrlQuitPg := QrLct2CtrlQuitPg.Value;
        ID_Pgto    := QrLctsControle.Value;
        ValAPag    := 0;
        ValARec    := 0;
        VTransf    := 0;
        //
        Data       := Geral.FDT(QrLctsCompensado.Value, 1);
        Tipo       := QrLct2Tipo.Value;
        Carteira   := QrLct2Carteira.Value;
        Controle   := QrLct2Controle.Value;
        Sub        := QrLct2Sub.Value;
        //Criado para evitar erro de primary key!!!
        SeqPag     := SeqPag + 1;
        //
        ValPgCre   := QrLctsCredito.Value;
        ValPgDeb   := QrLctsDebito.Value;
        ValPgTrf   := 0;
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_fluxcxadia_', False, [
        'PagRec', 'Genero', 'Descricao',
        'SerieNF', 'NotaFiscal', 'Debito',
        'Credito', 'Compensado', 'SerieCH',
        'Documento', 'Sit', 'Vencimento',
        'Pago', 'Mez', 'Fornecedor',
        'Cliente', 'CliInt', 'ForneceI',
        'DataDoc', 'Duplicata', 'Depto',
        'CtrlQuitPg', 'ID_Pgto', 'ValAPag',
        'ValARec', 'VTransf',
        'ValPgCre', 'ValPgDeb', 'ValPgTrf',
        'SeqPag'
        ], [
        'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
        PagRec, Genero, Descricao,
        SerieNF, NotaFiscal, Debito,
        Credito, Compensado, SerieCH,
        Documento, Sit, Vencimento,
        Pago, Mez, Fornecedor,
        Cliente, CliInt, ForneceI,
        DataDoc, Duplicata, Depto,
        CtrlQuitPg, ID_Pgto, ValAPag,
        ValARec, VTransf,
        ValPgCre, ValPgDeb, ValPgTrf,
        SeqPag
        ], [
        Data, Tipo, Carteira, Controle, Sub], False);
      end else
      begin
        (*
        UnDmkDAC_PF.AbreMySQLQuery0(QrLct0, DMod.MyDB, [
        'SELECT * ',
        'FROM ' + FTabLctA,
        'WHERE Controle =' + Geral.FF0(QrLctsID_Pgto.Value),
        'AND (Data < "' + FEmissIni + '" ',
        'OR Data > "' + FEmissFim + '") ',
        '']);
        if QrLct0.RecordCount = 0 then
        begin
        *)
          Erros := Erros + 1;
          //
          Geral.MB_Aviso(
          'Origem de quita��o de lan�amento n�o localizada:'
          + sLineBreak + 'Controle Origem: ' + Geral.FF0(QrLctsID_Pgto.Value) +
          sLineBreak + 'Controle do pagemento: ' + Geral.FF0(QrLctsControle.Value));
        //end;
      end;
    end;
    //
    QrLcts.Next;
  end;
  if Erros > 0 then
    Exit;
  //
   //  Dados base prontos.
  //  Preparar aorupamentos e suas somas.
  //
  ReopenDias();
  if QrDias.RecordCount = 0 then
  begin
    Credito  := 0;
    Data     := FEmissFim;
    Tipo     := 0;
    Carteira := 0;
    Controle := 0;
    Sub      := 127;
    SeqPag   := 0;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_fluxcxadia_', False, [
    'Credito'], [
    'Data', 'Tipo', 'Carteira', 'Controle', 'Sub', 'SeqPag'], [
    Credito], [
    Data, Tipo, Carteira, Controle, Sub, SeqPag], False);
    //
    ReopenDias();
  end;
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DELETE FROM _fluxcxadid_; ',
  '']));
  //
  UnDmkDAC_PF.ExecutaDB(DModG.MyPID_DB, Geral.ATS([
  'DELETE FROM _fluxcxadis_; ',
  '']));
  //
  DataA := '';
  QrDias.First;
  while not QrDias.Eof do
  begin
    Data := Geral.FDT(QrDiasData.Value, 1);
    DataB := Geral.FDT(QrDiasData.Value-1, 1);
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_fluxcxadid_', False, [
    'Data'], [], [
    Data], [], False);
    //
    // "Saldos" anteriores das carteiras tipo=2 emissoes.
    // Eh feito separado porque pode haver pagamento parcial
    // e porque na verdade sao os lancamentos em aberto
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumAnt, DModG.MyPID_DB, [
    'SELECT SUM(Credito) Credito, SUM(Debito) Debito, ',
    'SUM(VTransf) VTransf, ',
    'car.Tipo, car.PagRec, fcd.Carteira, ',
    'SUM(IF(Compensado<"1900-01-01" or Compensado>="' + Data + '", ',
    'ValAPag, 0)) ValAPag, ',
    'SUM(IF(Compensado<"1900-01-01" or Compensado>="' + Data + '", ',
    'ValARec, 0)) ValARec ',
    'FROM _fluxcxadia_  fcd ',
    'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=fcd.Carteira ',
    'WHERE Data<"' + Data + '" ',
    'GROUP BY car.Tipo, car.Codigo ',
    //'ORDER BY car.Ordem, car.Nome',
    '']);
    //dmkPF.LeMeuTexto(QrSumAnt.SQL.Text);
    QrSumAnt.First;
    while not QrSumAnt.Eof do
    begin
      PagoParciC := 0;
      PagoParciD := 0;
      if QrSumAntTipo.Value = 2 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrSit2, DModG.MyPID_DB, [
        'SELECT Controle ',
        'FROM _fluxcxadia_  fcd ',
        'WHERE Sit=1 ',
        'AND Carteira=' + Geral.FF0(QrSumAntCarteira.Value),
        '']);
        if QrSit2.RecordCount > 0 then
        begin
          QrSit2.First;
          while not QrSit2.Eof do
          begin
            // Somente se o for o dia anterior para nao somar mais de uma vez
            {
            if DataA = '' then
              SQLDtaX := 'AND Data < "' + Data + '" '
            else
              SQLDtaX := 'AND Data BETWEEN "' + DataA + '" AND "' + DataB + '"'
            ;
            }
            SQLDtaX := 'AND Data BETWEEN "' + FEmissIni + '" AND "' + DataB + '"';
            /////////////
            UnDmkDAC_PF.AbreMySQLQuery0(QrPago1, Dmod.MyDB, [
            'SELECT SUM(Credito) Credito, SUM(Debito) Debito ',
            'FROM ' + FTabLctA,
            'WHERE ID_Pgto=' + Geral.FF0(QrSit2Controle.Value),
            SQLDtaX,
            '']);
            //
            PagoParciC := PagoParciC - QrPago1Credito.Value;
            PagoParciD := PagoParciD - QrPago1Debito.Value;
            //
            QrSit2.Next;
          end;
        end;
      end;
      Tipo       := QrSumAntTipo.Value;
      PagRec     := QrSumAntPagRec.Value;
      Carteira   := QrSumAntCarteira.Value;
      ValAPagAnt := QrSumAntValAPag.Value - PagoParciC;
      ValARecAnt := QrSumAntValARec.Value - PagoParciD;
      VTransfAnt := QrSumAntVTransf.Value;
      ValAPagMov := 0;
      ValARecMov := 0;
      VTransfMov := 0;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_fluxcxadis_', False, [
      'Tipo', 'PagRec', 'Carteira', 'ValAPagAnt',
      'ValARecAnt', 'VTransfAnt', 'ValAPagMov',
      'ValARecMov', 'VTransfMov'], [
      'Data'], [
      Tipo, PagRec, Carteira, ValAPagAnt,
      ValARecAnt, VTransfAnt, ValAPagMov,
      ValARecMov, VTransfMov], [
      Data], False);
      //
      QrSumAnt.Next;
    end;
    //
    // Saldos anteriores das carteiras tipo 0=caixa e 1=banco
    UnDmkDAC_PF.AbreMySQLQuery0(QrSaldos, Dmod.MyDB, [
    'SELECT car.Tipo, car.PagRec, lct.Carteira, SUM(lct.Credito) Credito, ',
    'SUM(lct.Debito) Debito ',
    'FROM ' + FTabLctA + ' lct ',
    'LEFT JOIN carteiras car ON car.Codigo=lct.Carteira ',
    'WHERE car.Tipo <> 2 ',
    'AND lct.Data < "' + Data + '" ',
    'GROUP BY car.Tipo, car.Codigo ',
    '']);
    QrSaldos.First;
    while not QrSaldos.Eof do
    begin
      Tipo       := QrSaldosTipo.Value;
      PagRec     := QrSaldosPagRec.Value;
      Carteira   := QrSaldosCarteira.Value;
      ValAPagAnt := 0; //QrSaldosDebito.Value;
      ValARecAnt := 0; //QrSaldosCredito.Value;
      VTransfAnt := 0;//QrSaldosVTransf.Value;
      ValAPagMov := 0;
      ValARecMov := 0;
      VTransfMov := 0;
      EfetSdoAnt := QrSaldosCredito.Value - QrSaldosDebito.Value;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_fluxcxadis_', False, [
      'Tipo', 'PagRec', 'Carteira', 'ValAPagAnt',
      'ValARecAnt', 'VTransfAnt', 'ValAPagMov',
      'ValARecMov', 'VTransfMov',
      'EfetSdoAnt'], [
      'Data'], [
      Tipo, PagRec, Carteira, ValAPagAnt,
      ValARecAnt, VTransfAnt, ValAPagMov,
      ValARecMov, VTransfMov,
      EfetSdoAnt], [
      Data], False);
      //
      QrSaldos.Next;
    end;
    //
    // Movimento de todos tipos de carteira!!
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumMov, DModG.MyPID_DB, [
(*
    'SELECT SUM(Credito) Credito, SUM(Debito) Debito, SUM(ValAPag) ValAPag, ',
    'SUM(ValARec) ValARec, SUM(VTransf) VTransf, ',
    'car.Tipo, car.PagRec, fcd.Carteira ',
    'FROM _fluxcxadia_  fcd ',
    'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=fcd.Carteira ',
    'WHERE Data="' + Data + '" ',
    'GROUP BY car.Tipo, car.Codigo ',
*)
    'SELECT SUM(Credito) Credito, ',
    'SUM(Debito) Debito, SUM(ValAPag) ValAPag, ',
    'SUM(ValARec) ValARec, SUM(VTransf) VTransf, ',
    'SUM(ValPgCre) ValPgCre, SUM(ValPgDeb) ValPgDeb, ',
    'car.Tipo, car.PagRec, fcd.Carteira,',
    'IF(fcd.SeqPag=0, 0, 1) TIPOMOV ',
    'FROM _fluxcxadia_  fcd ',
    'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=fcd.Carteira ',
    'WHERE Data="' + Data + '" ',
    'GROUP BY car.Tipo, car.Codigo, TIPOMOV ',
    '']);
    QrSumMov.First;
    while not QrSumMov.Eof do
    begin
      Tipo       := QrSumMovTipo.Value;
      PagRec     := QrSumMovPagRec.Value;
      Carteira   := QrSumMovCarteira.Value;
      ValAPagAnt := 0;
      ValARecAnt := 0;
      VTransfAnt := 0;
      VTransfMov := 0;
      ValEmiPagC := 0;
      ValEmiPagD := 0;
      ValAPagMov := 0;
      ValARecMov := 0;
      EfetMovCre := 0;
      EfetMovDeb := 0;
      if QrSumMovTipo.Value = 2 then
      begin
        if QrSumMovTIPOMOV.Value = 0 then
        begin
          ValAPagMov := QrSumMovDebito.Value;
          ValARecMov := QrSumMovCredito.Value;
        end
        else
        begin
          ValEmiPagC := QrSumMovValPgCre.Value;
          ValEmiPagD := QrSumMovValPgDeb.Value;
        end;
      end else
      begin
        EfetMovCre := QrSumMovCredito.Value;
        EfetMovDeb := QrSumMovDebito.Value;
      end;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, '_fluxcxadis_', False, [
      'Tipo', 'PagRec', 'Carteira', 'ValAPagAnt',
      'ValARecAnt', 'VTransfAnt', 'ValAPagMov',
      'ValARecMov', 'VTransfMov',
      'EfetMovCre', 'EfetMovDeb',
      'ValEmiPagC', 'ValEmiPagD'], [
      'Data'], [
      Tipo, PagRec, Carteira, ValAPagAnt,
      ValARecAnt, VTransfAnt, ValAPagMov,
      ValARecMov, VTransfMov,
      EfetMovCre, EfetMovDeb,
      ValEmiPagC, ValEmiPagD], [
      Data], False);
      //
      QrSumMov.Next;
    end;
    //
    DataA := Data;
    QrDias.Next;
  end;
  //
  ReopenFCD();
  //
  //
  //FDtaImp := Now(); //Geral.FDT(Now(), 107);
  FNO_Empresa := CBEmpresa.Text;
  if DataI <> DataF then
    FPeriodo := dmkPF.PeriodoImp2(DataI, DataF, True, True, 'Per�odo: ', '', ' ')
  else
    FPeriodo := dmkPF.PeriodoImp2(DataI, DataF, True, False, 'Data: ', '', ' ');
  BtAbertos.Enabled := True;
  ImprimeFluxo_001();
end;

procedure TFmFluxCxaDia1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFluxCxaDia1.EdEmpresaChange(Sender: TObject);
begin
  FTabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, EdEmpresa.ValueVariant);
end;

procedure TFmFluxCxaDia1.FechaPesquisa();
begin
  BtAbertos.Enabled := False;
end;

procedure TFmFluxCxaDia1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFluxCxaDia1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FFluxCxaDia :=
    UCriarFin.RecriaTempTableNovo(ntrtt_FluxCxaDia, DModG.QrUpdPID1, False);
  FFluxCxaDiD :=
    UCriarFin.RecriaTempTableNovo(ntrtt_FluxCxaDiD, DModG.QrUpdPID1, False);
  FFluxCxaDiS :=
    UCriarFin.RecriaTempTableNovo(ntrtt_FluxCxaDiS, DModG.QrUpdPID1, False);
end;

procedure TFmFluxCxaDia1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFluxCxaDia1.ImprimeFluxo_001();
(*
const
  Cliente = 0;
var
  Aviso: String;
  //
  FEmpresa, FCliente, FItemRel: Integer;
  FDtaIni, FDtaFim, FDtaImp: TDateTime;
  FNO_Empresa, FNO_CLiente, FNO_REL: String;
*)
begin
//  Aviso := ' ';
  //
  frxFIN_RELAT_015_001.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxFIN_RELAT_015_001.Variables['VARF_DATA']    := Now();//FDtaImp;
  frxFIN_RELAT_015_001.Variables['VARF_PERIODO'] := QuotedStr(FPeriodo);
  MyObjects.frxDefineDataSets(frxFIN_RELAT_015_001, [
    frxDsFCA,
    frxDsFCD,
    frxDsFCS
  ]);
  MyObjects.frxMostra(frxFIN_RELAT_015_001, 'Fechamento de Caixa');
end;

function TFmFluxCxaDia1.Mais(Val: Double): Double;
begin
   if Val > 0 then
     Result := Val
   else
     Result := 0;
end;

function TFmFluxCxaDia1.Menos(Val: Double): Double;
begin
   if Val < 0 then
     Result := Val
   else
     Result := 0;
end;

procedure TFmFluxCxaDia1.QrACSCalcFields(DataSet: TDataSet);
var
  Cred, Deb, Soma: Double;
begin
  QrACSNO_TIPO_CART.Value := dmkPF.TipoDeCarteiraCashierSub(QrACSTipo.Value, QrACSPagRec.Value, True);
  //
  Soma := QrACSEfetSdoAnt.Value + QrACSEfetMovCre.Value - QrACSEfetMovDeb.Value;
  case QrACSTipo.Value of
    0,1:
    begin
      QrACSImpCreAnt.Value := Mais(QrACSEfetSdoAnt.Value);
      QrACSImpDebAnt.Value := Menos(QrACSEfetSdoAnt.Value);
      QrACSImpCreMov.Value := QrACSEfetMovCre.Value;
      QrACSImpDebMov.Value := QrACSEfetMovDeb.Value;
      QrACSImpCreAcu.Value := Mais(Soma);
      QrACSImpDebAcu.Value := Menos(Soma);
    end;
    2:
    begin
      QrACSImpCreAnt.Value := QrACSValARecAnt.Value;
      QrACSImpDebAnt.Value := QrACSValAPagAnt.Value;
      QrACSImpCreMov.Value := QrACSValARecMov.Value;
      QrACSImpDebMov.Value := QrACSValAPagMov.Value;
      QrACSImpCreAcu.Value := QrACSValARecAcu.Value;
      QrACSImpDebAcu.Value := QrACSValAPagAcu.Value;
    end;
  end;
end;

procedure TFmFluxCxaDia1.QrFCACalcFields(DataSet: TDataSet);
begin
  QrFCAVALMCRE.Value := QrFCAValARec.Value - QrFCAValPgCre.Value;
  QrFCAVALMDEB.Value := QrFCAValAPag.Value - QrFCAValPgDeb.Value;
end;

procedure TFmFluxCxaDia1.QrFCDAfterScroll(DataSet: TDataSet);
var
  Data: String;
begin
  Data := Geral.FDT(QrFCDData.Value, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFCA, DModG.MyPID_DB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_TERCEIRO, ',
  'car.Nome NO_CARTEIRA, fcd.* ',
  'FROM _fluxcxadia_  fcd ',
  'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=fcd.Carteira ',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo',
  '  = IF(Fornecedor<>0, fcd.Fornecedor, fcd.Cliente) ',
  'WHERE Data ="' + Data + '" ',
  'ORDER BY car.Ordem, NO_CARTEIRA, Data, Controle ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFCS, DModG.MyPID_DB, [
  'SELECT fcs.Tipo, fcs.PagRec, fcs.Carteira, ',
  'SUM(fcs.ValAPagAnt) ValAPagAnt, ',
  'SUM(fcs.ValARecAnt) ValARecAnt, ',
  'SUM(fcs.VTransfAnt) VTransfAnt, ',
  'SUM(fcs.ValAPagMov) ValAPagMov, ',
  'SUM(fcs.ValARecMov) ValARecMov, ',
  'SUM(fcs.VTransfMov) VTransfMov, ',

  'SUM(fcs.ValAPagAnt + fcs.ValAPagMov) ValAPagAcu, ',
  'SUM(fcs.ValARecAnt + fcs.ValARecMov) ValARecAcu, ',
  'SUM(fcs.VTransfAnt + fcs.VTransfMov) VTransfAcu, ',

  'SUM(fcs.EfetSdoAnt) EfetSdoAnt, ',
  'SUM(fcs.EfetMovCre) EfetMovCre, ',
  'SUM(fcs.EfetMovDeb) EfetMovDeb, ',

  'SUM(fcs.ValEmiPagC) ValEmiPagC, ',
  'SUM(fcs.ValEmiPagD) ValEmiPagD, ',

  'car.Nome NO_CARTEIRA ',
  'FROM _fluxcxadis_ fcs ',
  'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=fcs.Carteira ',
  'WHERE Data="' + Data + '" ',
  'GROUP BY car.Tipo, car.Codigo ',
  'ORDER BY car.Ordem, car.Nome ',
  '']);
end;

procedure TFmFluxCxaDia1.QrFCDBeforeClose(DataSet: TDataSet);
begin
  QrFCA.Close;
  QrFCS.Close;
end;

procedure TFmFluxCxaDia1.QrFCSCalcFields(DataSet: TDataSet);
var
  Cred, Deb, Soma: Double;
begin
  QrFCSTIPEPAGREC.Value := dmkPF.TipoEPagRecDeCarteira(QrFCSTipo.Value, QrFCSPagRec.Value);
  QrFCSNO_TIPO_CART.Value := dmkPF.TipoDeCarteiraCashierSub(QrFCSTipo.Value, QrFCSPagRec.Value, True);
  //
  Soma := QrFCSEfetSdoAnt.Value + QrFCSEfetMovCre.Value - QrFCSEfetMovDeb.Value;
  case QrFCSTipo.Value of
    0,1:
    begin
      QrFCSImpCreAnt.Value := Mais(QrFCSEfetSdoAnt.Value);
      QrFCSImpDebAnt.Value := Menos(QrFCSEfetSdoAnt.Value);
      QrFCSImpCreMov.Value := QrFCSEfetMovCre.Value;
      QrFCSImpDebMov.Value := QrFCSEfetMovDeb.Value;
      QrFCSImpCreAcu.Value := Mais(Soma);
      QrFCSImpDebAcu.Value := Menos(Soma);
      //
      QrFCSIMP_SDO_INI.Value := QrFCSImpCreAnt.Value + QrFCSImpDebAnt.Value;
    end;
    2:
    begin
      QrFCSImpCreAnt.Value := QrFCSValARecAnt.Value;
      QrFCSImpDebAnt.Value := QrFCSValAPagAnt.Value;
      QrFCSImpCreMov.Value := QrFCSValARecMov.Value;
      QrFCSImpDebMov.Value := QrFCSValAPagMov.Value;
      QrFCSImpCreAcu.Value := QrFCSValARecAcu.Value;
      QrFCSImpDebAcu.Value := QrFCSValAPagAcu.Value;
      //
      QrFCSIMP_SDO_INI.Value := QrFCSImpCreAnt.Value - QrFCSImpDebAnt.Value;
    end;
  end;
end;

procedure TFmFluxCxaDia1.ReopenDias();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDias, DModG.MyPID_DB, [
  'SELECT DISTINCT Data FROM _fluxcxadia_ ',
  'WHERE Data >="' + FEmissIni + '" ',
  'ORDER BY Data ',
  '']);
end;

procedure TFmFluxCxaDia1.ReopenFCD();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFCD, DModG.MyPID_DB, [
  'SELECT DISTINCT Data FROM _fluxcxadia_ ',
  'WHERE Data >="' + FEmissIni + '" ',
  'ORDER BY Data ',
  '']);
end;

procedure TFmFluxCxaDia1.TPEmissFimChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmFluxCxaDia1.TPEmissFimClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmFluxCxaDia1.TPEmissIniChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmFluxCxaDia1.TPEmissIniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

{
SELECT DISTINCT Carteira
FROM _fluxcxadis_
}


end.
