unit Saldos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, ExtCtrls, Db, (*DBTables,*) mySQLDbTables,
  UMySQLModule, UnMyLinguas, frxClass, frxDBSet, dmkGeral, Mask, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage, UnDmkEnums;

type
  TFmSaldos = class(TForm)
    Panel1: TPanel;
    TPData: TDateTimePicker;
    Label1: TLabel;
    QrTotalSaldo: TmySQLQuery;
    QrConsigna: TmySQLQuery;
    QrTotalSaldoNome: TWideStringField;
    QrTotalSaldoSaldo: TFloatField;
    QrTotalSaldoTipo: TIntegerField;
    QrTotalSaldoNOMETIPO: TWideStringField;
    QrConsignaNome: TWideStringField;
    QrConsignaSaldo: TFloatField;
    QrHist: TmySQLQuery;
    RGTipo: TRadioGroup;
    QrHistSALDO: TFloatField;
    QrHistACUMULADO: TFloatField;
    QrAVencer: TmySQLQuery;
    QrAVencerNome: TWideStringField;
    QrAVencerSALDO: TFloatField;
    CkNaoZero: TCheckBox;
    CkExclusivo: TCheckBox;
    frxDsTotalSaldo: TfrxDBDataset;
    frxHist: TfrxReport;
    frxDsHist: TfrxDBDataset;
    QrHistData: TDateField;
    QrHistNOMECARTEIRA: TWideStringField;
    QrHistControle: TIntegerField;
    QrHistsub: TSmallintField;
    QrHistNome: TWideStringField;
    QrHistHISTORICO: TWideStringField;
    QrHistNotaFiscal: TIntegerField;
    QrHistDebito: TFloatField;
    QrHistCredito: TFloatField;
    QrHistCompensado: TDateField;
    QrHistDocumento: TFloatField;
    QrHistVencimento: TDateField;
    Qr1: TmySQLQuery;
    Qr1Data: TDateField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Ds1: TDataSource;
    frxSaldos: TfrxReport;
    Label3: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrCarts: TmySQLQuery;
    QrCartsNome: TWideStringField;
    QrCartsINICIAL: TFloatField;
    QrLcts: TmySQLQuery;
    QrLctsCarteira: TIntegerField;
    QrLctsMovim: TFloatField;
    QrCartsCodigo: TIntegerField;
    QrCartsMOVIM: TFloatField;
    QrCartsSALDO: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtSair: TBitBtn;
    BtImprime: TBitBtn;
    procedure BtSairClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrTotalSaldoCalcFields(DataSet: TDataSet);
    procedure QrHistCalcFields(DataSet: TDataSet);
    procedure frxSaldosGetValue(const VarName: String; var Value: Variant);
    procedure QrCartsCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FSaldos: String;
    procedure ImprimeSaldoEm();
    procedure AtualizaPagos(TabLctA: String);

  public
    { Public declarations }
  end;

var
  FmSaldos: TFmSaldos;
  Acum: Double;

implementation

uses UnMyObjects, UnInternalConsts, Module, UCreateFin, ModuleGeral,
  UnFinanceiro, DmkDAC_PF;

{$R *.DFM}

procedure TFmSaldos.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSaldos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSaldos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSaldos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPData.Date := Date;
  //Qr1. O p e n;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmSaldos.BtImprimeClick(Sender: TObject);
begin
  ImprimeSaldoEm;
end;

procedure TFmSaldos.ImprimeSaldoEm();
var
  Entidade, CliInt: Integer;
  Data, FldIni, TabLctA, TabLctB, TabLctD, TabLctX: String;
  DtIni, DtEncer, DtMorto: TDateTime;
begin
  Entidade := DModG.QrEmpresasCodigo.Value;
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  CliInt   := DModG.QrEmpresasFilial.Value;
  //
  DtIni := Int(TPData.Date);
  Data := FormatDateTime(VAR_FORMATDATE, DtIni);
  //
  DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, DtEncer, DtMorto, TabLctA, TabLctB, TabLctD);
  TabLctX := UFinanceiro.DefLctTab(DtIni, DtEncer, DtMorto, TabLctA, TabLctB, TabLctD);
  FldIni := UFinanceiro.DefLctFldSdoIni(DtIni, DtEncer, DtMorto);
  //
  AtualizaPagos(TabLctA);
  //
  if RGTipo.ItemIndex = 0 then
  begin
    FSaldos := UCriarFin.RecriaTempTableNovo(ntrtt_Saldos, DModG.QrUpdPID1, False);
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT IGNORE INTO ' + FSaldos + ' SET ');
    DModG.QrUpdPID1.SQL.Add('Nome=:P0, Saldo=:P1, Tipo=:P2');
    // Deve ser antes!
    QrLcts.Close;
    QrLcts.SQL.Clear;
    QrLcts.SQL.Add('SELECT lct.Carteira,');
    QrLcts.SQL.Add('SUM(lct.Credito-lct.Debito) Movim');
    QrLcts.SQL.Add('FROM ' + TabLctX + ' lct');
    QrLcts.SQL.Add('WHERE lct.Tipo < 2');
    QrLcts.SQL.Add('AND lct.Data <= "' + Data + '"');
    QrLcts.SQL.Add('GROUP BY lct.Carteira');
    QrLcts.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(QrLcts, Dmod.MyDB);
    //
    QrCarts.Close;
    QrCarts.SQL.Clear;
    QrCarts.SQL.Add('SELECT car.Codigo, car.Nome,');
    QrCarts.SQL.Add(FldIni + ' INICIAL');
    QrCarts.SQL.Add('FROM carteiras car');
    QrCarts.SQL.Add('WHERE car.Tipo < 2');
    QrCarts.SQL.Add('AND car.Exclusivo <= 0');
    QrCarts.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(QrCarts, Dmod.MyDB);
    while not QrCarts.Eof do
    begin
      if (CkNaoZero.Checked = False) or (QrCartsSALDO.Value <> 0) then
      begin
        DModG.QrUpdPID1.Params[0].AsString  := QrCartsNome.Value;
        DModG.QrUpdPID1.Params[1].AsFloat   := QrCartsSALDO.Value;
        DModG.QrUpdPID1.Params[2].AsInteger := 1;
        DModG.QrUpdPID1.ExecSQL;
      end;
      QrCarts.Next;
    end;
    //
    (* A tabela consignacoesits n�o existe mais
    QrConsigna.Close;
    QrConsigna.Params[0].AsString := Data;
    UnDmkDAC_PF.AbreQuery(QrConsigna, Dmod.MyDB);
    while not QrConsigna.Eof do
    begin
      if (CkNaoZero.Checked = False) or (QrConsignaSaldo.Value <> 0) then
      begin
        DModG.QrUpdPID1.Params[0].AsString  := QrConsignaNome.Value;
        DModG.QrUpdPID1.Params[1].AsFloat   := QrConsignaSaldo.Value;
        DModG.QrUpdPID1.Params[2].AsInteger := 2;
        DModG.QrUpdPID1.ExecSQL;
      end;
      QrConsigna.Next;
    end;
    *)
    //
    QrAVencer.Close;
    QrAVencer.SQL.Clear;
    QrAVencer.SQL.Add('SELECT car.Nome,');
    QrAVencer.SQL.Add(' SUM(lct.Credito - lct.Debito) SALDO');
    QrAVencer.SQL.Add('FROM ' + TabLctA + ' lct');
    QrAVencer.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.carteira');
    QrAVencer.SQL.Add('WHERE lct.Tipo=2');
    QrAVencer.SQL.Add('AND lct.Data<="' + Data + '"');
    QrAVencer.SQL.Add('AND (lct.Compensado="1899/12/30" ');
    QrAVencer.SQL.Add(' OR Compensado="" OR lct.Compensado<2 ');
    QrAVencer.SQL.Add('  OR lct.Compensado>"' + Data + '")');
    QrAVencer.SQL.Add('GROUP BY lct.carteira');
    {
    QrAVencer.Params[00].AsString := Data;
    QrAVencer.Params[01].AsString := Data;
    }
    UnDmkDAC_PF.AbreQuery(QrAVencer, Dmod.MyDB);
    while not QrAVencer.Eof do
    begin
      DModG.QrUpdPID1.Params[0].AsString  := QrAVencerNome.Value;
      DModG.QrUpdPID1.Params[1].AsFloat   := QrAVencerSaldo.Value;
      DModG.QrUpdPID1.Params[2].AsInteger := 3;
      DModG.QrUpdPID1.ExecSQL;
      QrAVencer.Next;
    end;
    //
    //QrSaldos.Close;
    (* A tabela consignacoesits n�o existe mais
    QrConsigna.Close;
    *)
(*
    QrTotalSaldo.Close;
    QrTotalSaldo.Database := DModG.MyPID_DB;
    QrTotalSaldo. O p e n;
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrTotalSaldo, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + FSaldos,
    '']);
    //
    MyObjects.frxDefineDataSets(frxSaldos, [
      DModG.frxDsDono,
      frxDsTotalSaldo
    ]);
    MyObjects.frxMostra(frxSaldos, 'Saldos de carteiras');
    QrTotalSaldo.Close;
  end else
  begin
    QrHist.Close;
    QrHist.SQL.Clear;
{
SELECT lct.Data, car.Nome NOMECARTEIRA, lct.Controle,
lct.sub, cta.Nome, lct.Descricao HISTORICO, lct.NotaFiscal,
lct.Debito, lct.Credito, lct.Compensado, lct.Documento,
lct.Vencimento
FROM VAR LCT lct, Carteiras car, Contas cta
WHERE lct.Data= :P0
AND car.Codigo=lct.Carteira
AND cta.Codigo=lct.Genero
ORDER BY NOMECARTEIRA, HISTORICO, Controle
}
    QrHist.SQL.Add('SELECT lct.Data, car.Nome NOMECARTEIRA, lct.Controle,');
    QrHist.SQL.Add('lct.sub, cta.Nome, lct.Descricao HISTORICO, lct.NotaFiscal,');
    QrHist.SQL.Add('lct.Debito, lct.Credito, lct.Compensado, lct.Documento,');
    QrHist.SQL.Add('lct.Vencimento');
    QrHist.SQL.Add('FROM ' + TabLctX + ' lct, Carteiras car, Contas cta');
    QrHist.SQL.Add('WHERE lct.Data= :P0');
    QrHist.SQL.Add('AND car.Codigo=lct.Carteira');
    QrHist.SQL.Add('AND cta.Codigo=lct.Genero');
    QrHist.SQL.Add('ORDER BY NOMECARTEIRA, HISTORICO, Controle');
    QrHist.Params[0].AsString := Data;
    UnDmkDAC_PF.AbreQuery(QrHist, Dmod.MyDB);
    MyObjects.frxDefineDataSets(frxHist, [
      DModG.frxDsDono,
      frxDsHist
    ]);
    MyObjects.frxMostra(frxHist, 'Saldos de contas');
    QrHist.Close;
  end;
end;

procedure TFmSaldos.QrTotalSaldoCalcFields(DataSet: TDataSet);
begin
  case QrTotalSaldoTipo.Value of
    1: QrTotalSaldoNOMETIPO.Value := 'Em carteira';
    2: QrTotalSaldoNOMETIPO.Value := 'Consignado';
    3: QrTotalSaldoNOMETIPO.Value := 'A vencer';
  else QrTotalSaldoNOMETIPO.Value := 'Desconhecido';
  end;
end;

procedure TFmSaldos.QrCartsCalcFields(DataSet: TDataSet);
begin
  QrCartsSALDO.Value := QrCartsMOVIM.Value + QrCartsINICIAL.Value;
end;

procedure TFmSaldos.QrHistCalcFields(DataSet: TDataSet);
begin
  QrHistSALDO.Value := QrHistCredito.Value - QrHistDebito.Value;
  if QrHist.BOF then
    Acum := 0;
  Acum := Acum + QrHistSALDO.Value;
  QrHistACUMULADO.Value := Acum;
end;

procedure TFmSaldos.frxSaldosGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'SALDO_DE_CARTEIRAS' then
     Value := 'Saldo das carteiras em '+
     FormatDateTime(VAR_FORMATDATE3, TPData.Date)
  else
  if VarName = 'VARF_EMPRESA' then
    Value := DModG.QrEmpresasNOMEFILIAL.Value
  else
  if VarName = 'VARF_DATA' then
    Value := Geral.FDT(TPData.Date, 2)
end;

procedure TFmSaldos.AtualizaPagos(TabLctA: String);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE IGNORE ' + TabLctA + ' SET Compensado=Vencimento');
  Dmod.QrUpd.SQL.Add('WHERE Tipo=2 AND Compensado<2 AND Sit > 1');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE IGNORE ' + TabLctA + ' SET Sit=2');
  Dmod.QrUpd.SQL.Add('WHERE Tipo=2 AND Compensado>2');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.ExecSQL;
end;

end.

