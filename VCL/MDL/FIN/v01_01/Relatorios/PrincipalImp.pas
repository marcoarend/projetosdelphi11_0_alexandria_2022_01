unit PrincipalImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, Buttons, ExtCtrls, (*DBTables,*)   frxClass, frxDBSet,
  UnInternalConsts, ComCtrls, mySQLDbTables, DBCtrls, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Variants, dmkEditDateTimePicker,
  dmkPermissoes, Grids, DBGrids, dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  TFmPrincipalImp = class(TForm)
    Panel1: TPanel;
    QrAnterior: TmySQLQuery;
    QrExtrato: TmySQLQuery;
    QrAnteriorCredito: TFloatField;
    QrAnteriorDebito: TFloatField;
    QrExtratoData: TDateField;
    QrExtratoDescricao: TWideStringField;
    QrExtratoNotaFiscal: TIntegerField;
    QrExtratoDebito: TFloatField;
    QrExtratoCredito: TFloatField;
    QrExtratoDocumento: TFloatField;
    QrExtratoFornecedor: TIntegerField;
    QrExtratoCliente: TIntegerField;
    QrExtratoNOMECONTA: TWideStringField;
    QrExtratoNOMECARTEIRA: TWideStringField;
    QrExtratoControle: TIntegerField;
    Panel3: TPanel;
    Label1: TLabel;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    Label2: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrMovimento: TmySQLQuery;
    QrMovimentoNOMECONTA: TWideStringField;
    QrMovimentoNOMECLIENTE: TWideStringField;
    QrMovimentoNOMECLIINT: TWideStringField;
    QrMovimentoNOMEFORNECEDOR: TWideStringField;
    QrMovimentoData: TDateField;
    QrMovimentoCarteira: TIntegerField;
    QrMovimentoControle: TIntegerField;
    QrMovimentoGenero: TIntegerField;
    QrMovimentoDescricao: TWideStringField;
    QrMovimentoNotaFiscal: TIntegerField;
    QrMovimentoDebito: TFloatField;
    QrMovimentoCredito: TFloatField;
    QrMovimentoDocumento: TFloatField;
    QrMovimentoVencimento: TDateField;
    QrMovimentoFornecedor: TIntegerField;
    QrMovimentoCliente: TIntegerField;
    QrMovimentoDuplicata: TWideStringField;
    QrMovimentoTERCEIRO: TIntegerField;
    QrMovimentoNOMETERCEIRO: TWideStringField;
    QrMovimentoCLContato: TWideStringField;
    QrMovimentoCIContato: TWideStringField;
    QrMovimentoFOContato: TWideStringField;
    QrMovimentoCLTel1: TWideStringField;
    QrMovimentoCITel1: TWideStringField;
    QrMovimentoFOTel1: TWideStringField;
    QrMovimentoTETel1: TWideStringField;
    QrMovimentoTEContato: TWideStringField;
    QrMovimentoTXTCITel1: TWideStringField;
    QrMovimentoTXTTETel1: TWideStringField;
    QrMovimentoNOMETECONTATO: TWideStringField;
    QrMovimentoVALOR: TFloatField;
    LaCliente: TLabel;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel9: TPanel;
    RG1: TRadioGroup;
    RG2: TRadioGroup;
    Panel8: TPanel;
    Panel10: TPanel;
    Ck1: TCheckBox;
    Panel11: TPanel;
    Ck2: TCheckBox;
    Panel12: TPanel;
    EdCliFor: TdmkEditCB;
    CBCliFor: TdmkDBLookupComboBox;
    Label4: TLabel;
    QrClienteI: TmySQLQuery;
    DsClienteI: TDataSource;
    QrCliente1: TmySQLQuery;
    DsCliente1: TDataSource;
    Label5: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    QrContas: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsContas: TDataSource;
    QrCliente1Codigo: TIntegerField;
    QrCliente1NOMEENTIDADE: TWideStringField;
    QrClienteICodigo: TIntegerField;
    QrClienteINOMEENTIDADE: TWideStringField;
    QrExtratoNOMERELACIONADO: TWideStringField;
    QrExtratoNOMECLIENTE: TWideStringField;
    QrExtratoNOMEFORNECEDOR: TWideStringField;
    Panel4: TPanel;
    QrExtratoQtde: TFloatField;
    frxExtrato: TfrxReport;
    frxDsExtrato: TfrxDBDataset;
    frxExtrato2: TfrxReport;
    frxDsMovimento: TfrxDBDataset;
    QrMovimentoKGT: TIntegerField;
    QrCarteirasForneceI: TIntegerField;
    frxDsCarteiras: TfrxDBDataset;
    QrCarteirasNO_ENT: TWideStringField;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    frxMovimento1: TfrxReport;
    QrExtratoCarteira: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    DsAnteriror: TDataSource;
    QrInicial: TmySQLQuery;
    QrInicialINICIAL: TFloatField;
    QrAnteriorSALDO: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel14: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel15: TPanel;
    PnSaiDesis: TPanel;
    BtImprime: TBitBtn;
    BtSair: TBitBtn;
    Panel2: TPanel;
    Panel13: TPanel;
    DBGrid1: TDBGrid;
    frxExtrato3: TfrxReport;
    QrExtratoQtd2: TFloatField;
    Panel16: TPanel;
    RG00Quitados: TRadioGroup;
    EdTituloQtde: TdmkEdit;
    Label3: TLabel;
    Label7: TLabel;
    EdTituloQtd2: TdmkEdit;
    procedure BtSairClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMovimentoCalcFields(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure RG1Click(Sender: TObject);
    procedure QrExtratoCalcFields(DataSet: TDataSet);
    procedure frxExtratoGetValue(const VarName: String;
      var Value: Variant);
    procedure EdEmpresaChange(Sender: TObject);
    procedure QrAnteriorCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FDataI, FDataF, FDataH, FDataA,
    FTabLcta, FTabLctB, FTabLctD,
    FCart_Txt, FCliFor_Txt, FConta_Txt, (*FEmpresa_Txt,*) FEntidade_Txt: String;
    //
    FDtIni, FDtEncer, FDtMorto: TDateTime;
    FSaldo, FAcum, FAcuB: Double;
    FEntidade, FEmpresa, FCarteira, FCliFor_Cod, FConta_Cod: Integer;
    FSDO_ACUM, FQT2_ACUM: Double;
    procedure ImprimeExtrato;
    procedure ImprimeFaturamentoPorTerceiro;
    function DefineCondicao(RG: TRadioGroup): String;
    function DefineMeuMemo1(RG: TRadioGroup): String;
    function DefineMeuMemo2(RG: TRadioGroup): String;
    function DefineMeuMemo3(RG: TRadioGroup): String;
    function DefineTotal(RG: TRadioGroup): String;
    procedure ReopenCarteiras();

  public
    { Public declarations }
  end;

var
  FmPrincipalImp: TFmPrincipalImp;

implementation

uses UnMyObjects, Module, ModuleGeral, UnFinanceiro, DmkDAC_PF;

{$R *.DFM}

procedure TFmPrincipalImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPIni.Date := Geral.PrimeiroDiaDoMes(Date);
  TPFim.Date := Geral.UltimoDiaDoMes(Date);
  //
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClienteI, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliente1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  //
  PageControl1.ActivePageIndex := 0;
  //
  QrExtrato.Database   := DModG.MyPID_DB;
  QrMovimento.Database := DModG.MyPID_DB;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmPrincipalImp.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipalImp.BtImprimeClick(Sender: TObject);
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa') then Exit;
  if MyObjects.FIC(RG00Quitados.ItemIndex < 1, RG00Quitados, 'Informe a "' +
    RG00Quitados.Caption + '"') then Exit;
  //
  FEmpresa      := DModG.QrEmpresasFilial.Value;
  FEntidade     := DModG.QrEmpresasCodigo.Value;
  FEntidade_Txt := FormatFloat('0', FEntidade);
  //
  FCarteira     := Geral.IMV(EdCarteira.Text);
  FCart_TXT     := FormatFloat('0', FCarteira);
  FCliFor_Cod   := EdCliFor.ValueVariant;
  FCliFor_TXT   := FormatFloat('0', FCliFor_Cod);
  FConta_Cod    := EdConta.ValueVariant;
  FConta_TXT    := FormatFloat('0', FConta_Cod);
  //
  dmkPF.ExcluiSQLsDermatek();
  //
  FSaldo   := 0;
  FAcum    := 0;
  FAcuB    := 0;
  FDtIni   := Int(TPIni.Date);
  //
  DModG.Def_EM_ABD(TMeuDB, 
    FEntidade, FEmpresa, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  //
  FDataI  := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  FDataF  := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  FDataH  := FormatDateTime(VAR_FORMATDATE, Date);
  FDataA  := FormatDateTime(VAR_FORMATDATE, TPIni.Date-1);
  case PageControl1.ActivePageIndex of
    -1: Geral.MB_Aviso('Escolha o relat�rio!');
    0: ImprimeExtrato;
    1: ImprimeFaturamentoPorTerceiro;
    else Geral.MB_Erro('Relat�rio n�o implementado!');
  end;
end;

procedure TFmPrincipalImp.ImprimeExtrato();
const
  sProcName = 'TFmPrincipalImp.ImprimeExtrato()';
(* 2020-01-09 ini
  procedure LctSQL(TabLct: String);
  begin
    QrExtrato.SQL.Add('SELECT la.Data, la.Documento, la.Qtde, la.Descricao, ');
    QrExtrato.SQL.Add('la.NotaFiscal, la.Cliente, la.Fornecedor, la.Controle,');
    QrExtrato.SQL.Add('la.Carteira, la.Credito, la.Debito, ');
    QrExtrato.SQL.Add('co.Nome NOMECONTA, ca.Nome NOMECARTEIRA,');
    QrExtrato.SQL.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,');
    QrExtrato.SQL.Add('IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR');
    QrExtrato.SQL.Add('FROM ' + TabLct + ' la');
    QrExtrato.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
    QrExtrato.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras ca ON ca.Codigo=la.Carteira');
    QrExtrato.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades cl ON cl.Codigo=la.Cliente');
    QrExtrato.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades fo ON fo.Codigo=la.Fornecedor');
    QrExtrato.SQL.Add('WHERE la.Data BETWEEN "' + FDataI + '" AND "' + FDataF + '"');
    QrExtrato.SQL.Add('AND (la.Tipo=1 OR (la.Tipo=0 AND Vencimento<="' + FDataF + '"))');
    QrExtrato.SQL.Add('AND la.Carteira=' + FCart_TXT);
    QrExtrato.SQL.Add('AND la.FatID <> ' + LAN_SDO_INI_FATID);
  end;
*)
  function LctSQL(TabLct: String): String;
  begin
    Result := Geral.ATS([
    'SELECT la.Data, la.Documento, la.Qtde, la.Qtd2, la.Descricao,  ',
    'la.NotaFiscal, la.Cliente, la.Fornecedor, la.Controle, ',
    'la.Carteira, la.Credito, la.Debito,  ',
    'co.Nome NOMECONTA, ca.Nome NOMECARTEIRA, ',
    'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE, ',
    'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR ',
    'FROM ' + TabLct + ' la ',
    'LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero ',
    'LEFT JOIN ' + TMeuDB + '.carteiras ca ON ca.Codigo=la.Carteira ',
    'LEFT JOIN ' + TMeuDB + '.entidades cl ON cl.Codigo=la.Cliente ',
    'LEFT JOIN ' + TMeuDB + '.entidades fo ON fo.Codigo=la.Fornecedor ',
    'WHERE la.Data BETWEEN "' + FDataI + '" AND "' + FDataF + '" ',
    'AND (la.Tipo=1 OR (la.Tipo=0 AND Vencimento<="' + FDataF + '")) ',
    'AND la.Carteira=' + FCart_TXT,
    'AND la.FatID <> ' + LAN_SDO_INI_FATID,
    '']);
  end;
  // 2020-01-09 fim
var
  FldIni, TabIni: String;
  frxReport: TfrxReport;
begin
  //
  if MyObjects.FIC(FCarteira = 0, EdCarteira, 'Informe a carteira') then Exit;
  //
  FldIni := UFinanceiro.DefLctFldSdoIni(FDtIni, FDtEncer, FDtMorto);
  TabIni := UFinanceiro.DefLctTab(FDtIni, FDtEncer, FDtMorto,
            FTabLcta, FTabLctB, FTabLctD);
  // Deve ser antes do Qr Anterior!
(* 2020-01-09 ini
  QrInicial.Close;
  QrInicial.SQL.Clear;
  QrInicial.SQL.Add('SELECT ' + FldIni + ' INICIAL');
  QrInicial.SQL.Add('FROM carteiras car');
  QrInicial.SQL.Add('WHERE car.Codigo=' + FCart_TXT);
  UnDmkDAC_PF.AbreQuery(QrInicial, Dmod.MyDB);
  //Geral.MB_Teste(QrInicial.SQL.Text);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrInicial, Dmod.MyDB, [
  'SELECT car.SdoFimB INICIAL ',
  'FROM carteiras car ',
  'WHERE car.Codigo=' + FCart_TXT,
  '']);
  // 2020-01-09 fim
  //
(* 2020-01-09 ini
  QrAnterior.Close;
  QrAnterior.SQL.Clear;
  //ERRO QUANDO n�o tem lancto!
  //QrAnterior.SQL.Add('SELECT SUM(lct.Credito) Credito,');
  //QrAnterior.SQL.Add('SUM(lct.Debito) Debito, ' + FldIni + ' INICIAL,');
  //QrAnterior.SQL.Add('SUM(lct.Credito - lct.Debito) + car.SdoFimB Saldo');
  //QrAnterior.SQL.Add('FROM ' + TabIni + ' lct');
  //QrAnterior.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lct.Carteira');
  //QrAnterior.SQL.Add('WHERE lct.Data<"' + FDataI + '"');
  //QrAnterior.SQL.Add('AND (lct.Tipo=1 OR (lct.Tipo=0 AND lct.Vencimento<"' + FDataI + '"))');
  //QrAnterior.SQL.Add('AND lct.Carteira=' + FCart_TXT);
  //QrAnterior.SQL.Add('GROUP BY car.Codigo;');

  QrAnterior.SQL.Add('SELECT SUM(lct.Credito) Credito,');
  QrAnterior.SQL.Add('SUM(lct.Debito) Debito');
  QrAnterior.SQL.Add('FROM ' + TabIni + ' lct');
  QrAnterior.SQL.Add('WHERE lct.Data<"' + FDataI + '"');
  QrAnterior.SQL.Add('AND (lct.Tipo=1 OR (lct.Tipo=0 AND lct.Vencimento<"' + FDataI + '"))');
  QrAnterior.SQL.Add('AND lct.Carteira=' + FCart_TXT);
  UnDmkDAC_PF.AbreQuery(QrAnterior, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrAnterior, Dmod.MyDB, [
  'SELECT SUM(lct.Credito) Credito, ',
  'SUM(lct.Debito) Debito ',
  'FROM ' + TabIni + ' lct ',
  'WHERE lct.Data<"' + FDataI + '" ',
  'AND (lct.Tipo=1 OR (lct.Tipo=0 AND lct.Vencimento<"' + FDataI + '")) ',
  'AND lct.Carteira=' + FCart_TXT,
  '']);
  // 2020-01-09 fim
  //Geral.MB_Teste(QrAnterior.SQL.Text);
  FSaldo := QrAnteriorSaldo.Value;
  FACum := FSaldo;
  FAcuB := FSaldo;
  //
(* 2020-01-09 ini
  QrExtrato.Close;
  QrExtrato.SQL.Clear;
  QrExtrato.SQL.Add('DROP TABLE IF EXISTS fin_relat_007_extrato;');
  QrExtrato.SQL.Add('CREATE TABLE fin_relat_007_extrato');
  QrExtrato.SQL.Add('');
  LctSQL(FTabLctA);
  QrExtrato.SQL.Add('UNION');
  LctSQL(FTabLctB);
  QrExtrato.SQL.Add('UNION');
  LctSQL(FTabLctD);
  QrExtrato.SQL.Add(';');
  QrExtrato.SQL.Add('SELECT * FROM fin_relat_007_extrato');
  QrExtrato.SQL.Add('ORDER BY Data, NotaFiscal, Carteira, Controle;');
  QrExtrato.SQL.Add('DROP TABLE IF EXISTS fin_relat_007_extrato;');
  UnDmkDAC_PF.AbreQuery(QrExtrato, DModG.MyPID_DB);
  //Geral.MB_Teste(QrExtrato.SQL.Text);
*)
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrExtrato, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS fin_relat_007_extrato;',
  'CREATE TABLE fin_relat_007_extrato',
  '',
  LctSQL(FTabLctA),
  'UNION',
  LctSQL(FTabLctB),
  'UNION',
  LctSQL(FTabLctD),
  ';',
  'SELECT * FROM fin_relat_007_extrato',
  'ORDER BY Data, NotaFiscal, Carteira, Controle;',
  //'DROP TABLE IF EXISTS fin_relat_007_extrato;',     <!>
  '']);
  //Geral.MB_Teste(QrExtrato.SQL.Text);
  // 2020-01-09 fim
  //
  //if not CkNF.Checked then
  case RG00Quitados.ItemIndex of
    1: frxReport := frxExtrato2;
    2: frxReport := frxExtrato;
    3: frxReport := frxExtrato3;
    else
    begin
      Geral.MB_Erro('"' + RG00Quitados.Caption + '" indefinido em ' + sProcName);
      Exit;
    end;
  end;
  MyObjects.frxDefineDataSets(frxReport, [
    DmodG.frxDsDono,
    frxDsCarteiras,
    frxDsExtrato
    ]);
  MyObjects.frxMostra(frxReport, 'Extrato de Carteira �nica');
  //
  QrExtrato.Close;
  if RG00Quitados.ItemIndex = 3 then
  begin
    Geral.WriteAppKeyCU('Financeiro\TituloQtde', Application.Title, EdTituloQtde.Text, ktString);
    Geral.WriteAppKeyCU('Financeiro\TituloQtd2', Application.Title, EdTituloQtd2.Text, ktString);
  end;
end;

procedure TFmPrincipalImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrincipalImp.FormShow(Sender: TObject);
begin
  try
    EdTituloQtde.Text := Geral.ReadAppKeyCU('Financeiro\TituloQtde', Application.Title, ktString, 'Qtde 1');
    EdTituloQtd2.Text := Geral.ReadAppKeyCU('Financeiro\TituloQtd2', Application.Title, ktString, 'Qtde 2');
  except
    //
  end;
end;

procedure TFmPrincipalImp.ImprimeFaturamentoPorTerceiro;
  procedure LctSQL(TabLct: String);
  begin
    QrMovimento.SQL.Add('SELECT co.Nome NOMECONTA,');
    QrMovimento.SQL.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,');
    QrMovimento.SQL.Add('IF(ci.Tipo=0, ci.RazaoSocial, ci.Nome) NOMECLIINT,');
    QrMovimento.SQL.Add('IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,');
    QrMovimento.SQL.Add('IF(cl.Tipo=0, cl.EContato, cl.PContato) CLContato,');
    QrMovimento.SQL.Add('IF(ci.Tipo=0, ci.EContato, ci.PContato) CIContato,');
    QrMovimento.SQL.Add('IF(fo.Tipo=0, fo.EContato, fo.PContato) FOContato,');
    QrMovimento.SQL.Add('IF(cl.Tipo=0, cl.ETe1, cl.PTe1) CLTel1,');
    QrMovimento.SQL.Add('IF(ci.Tipo=0, ci.ETe1, ci.PTe1) CITel1,');
    QrMovimento.SQL.Add('IF(fo.Tipo=0, fo.ETe1, fo.PTe1) FOTel1,');
    QrMovimento.SQL.Add('la.Data, la.Documento, la.Qtde, la.Descricao, ');
    QrMovimento.SQL.Add('la.NotaFiscal, la.Cliente, la.Fornecedor, ');
    QrMovimento.SQL.Add('la.Controle, la.Carteira, la.Credito, la.Debito, ');
    QrMovimento.SQL.Add('la.Duplicata, la.Vencimento, la.Genero');
    QrMovimento.SQL.Add('FROM ' + TabLct + ' la');
    QrMovimento.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras ca ON ca.Codigo=la.Carteira');
    QrMovimento.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    co ON co.Codigo=la.Genero');
    QrMovimento.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades cl ON cl.Codigo=la.Cliente');
    QrMovimento.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ci ON ci.Codigo=la.CliInt');
    QrMovimento.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades fo ON fo.Codigo=la.Fornecedor');
    QrMovimento.SQL.Add('WHERE la.Data BETWEEN "'+ FDataI + '" AND "' + FDataF + '"');
    QrMovimento.SQL.Add('AND co.Codigo=la.Genero');
    QrMovimento.SQL.Add('AND ca.ForneceI=' + FEntidade_Txt);
    QrMovimento.SQL.Add('AND la.FatID <> ' + LAN_SDO_INI_FATID);

    if FCarteira <> 0 then
      QrMovimento.SQL.Add('AND la.Carteira=' + FCart_TXT);
    //
    //
    if FCliFor_Cod <> 0 then
      QrMovimento.SQL.Add('AND (la.Cliente='  + FormatFloat('0',
      EdCliFor.ValueVariant) + ' OR la.Fornecedor=' + EdCliFor.Text+')');
    //
    if FConta_Cod <> 0 then
    QrMovimento.SQL.Add('AND la.Genero='  + FConta_TXT);
    //
  end;
var
  Ordem: String;
  MyOrd: Integer;
begin
  MyOrd := RG1.ItemIndex*10+RG2.ItemIndex;
  case MyOrd of
    00: Ordem := '';
    01: Ordem := 'ORDER BY Cliente, Fornecedor, Data';
    02: Ordem := 'ORDER BY Genero, Data';
    10: Ordem := 'ORDER BY Cliente, Fornecedor, Data';
    11: Ordem := 'ORDER BY Cliente, Fornecedor, Data';
    12: Ordem := 'ORDER BY Cliente, Fornecedor, Genero, Data';
    20: Ordem := 'ORDER BY Genero, Data';
    21: Ordem := 'ORDER BY Genero, Cliente, Fornecedor, Data';
    22: Ordem := 'ORDER BY Genero, Data';
    else Ordem := '';
  end;
  QrMovimento.Close;
  QrMovimento.SQL.Clear;
  QrMovimento.SQL.Add('DROP TABLE IF EXISTS fin_relat_007_movim;');
  QrMovimento.SQL.Add('CREATE TABLE fin_relat_007_movim');
  QrMovimento.SQL.Add('');
  LctSQL(FTabLctA);
  QrMovimento.SQL.Add('UNION');
  LctSQL(FTabLctB);
  QrMovimento.SQL.Add('UNION');
  LctSQL(FTabLctD);
  QrMovimento.SQL.Add(';');
  QrMovimento.SQL.Add('');
  QrMovimento.SQL.Add('SELECT * FROM fin_relat_007_movim');
  QrMovimento.SQL.Add(ORDEM + ';');
  QrMovimento.SQL.Add('DROP TABLE IF EXISTS fin_relat_007_movim;');
  //
  UnDmkDAC_PF.AbreQuery(QrMovimento, DModG.MyPID_DB);
  //
  MyObjects.frxDefineDataSets(frxMovimento1, [
    DmodG.frxDsDono,
    frxDsMovimento
    ]);
  MyObjects.frxMostra(frxMovimento1, 'Movimento de Carteira �nica');
end;

procedure TFmPrincipalImp.QrMovimentoCalcFields(DataSet: TDataSet);
begin
  QrMovimentoKGT.Value := 0;
  if QrMovimentoCliente.Value = 0 then
  begin
    QrMovimentoTERCEIRO.Value := QrMovimentoFornecedor.Value;
    QrMovimentoNOMETERCEIRO.Value := QrMovimentoNOMEFORNECEDOR.Value;
    QrMovimentoNOMETECONTATO.Value := QrMovimentoFOContato.Value;
    QrMovimentoTXTTETel1.Value :=
      Geral.FormataTelefone_TT_Curto(QrMovimentoFOTel1.Value);
  end else begin
    QrMovimentoTERCEIRO.Value := QrMovimentoCliente.Value;
    QrMovimentoNOMETERCEIRO.Value := QrMovimentoNOMECLIENTE.Value;
    QrMovimentoNOMETECONTATO.Value := QrMovimentoCLContato.Value;
    QrMovimentoTXTTETel1.Value :=
      Geral.FormataTelefone_TT_Curto(QrMovimentoCLTel1.Value);
  end;
  QrMovimentoTXTCITel1.Value :=
    Geral.FormataTelefone_TT_Curto(QrMovimentoCITel1.Value);
  QrMovimentoVALOR.Value := QrMovimentoCredito.Value - QrMovimentoDebito.Value;
end;

procedure TFmPrincipalImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPrincipalImp.ReopenCarteiras();
var
  Carteira: Integer;
begin
  Carteira := EdCarteira.ValueVariant;
  //
  QrCarteiras.Close;
  QrCarteiras.Params[0].AsInteger := DModG.QrEmpresasCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  if QrCarteiras.Locate('Codigo', Carteira, []) then
  begin
    EdCarteira.ValueVariant := Carteira;
    CBCarteira.KeyValue     := Carteira;
  end else begin
    EdCarteira.ValueVariant := 0;
    CBCarteira.KeyValue     := Null;
  end;
end;

procedure TFmPrincipalImp.RG1Click(Sender: TObject);
begin
  RG2.Enabled := True;
end;

function TFmPrincipalImp.DefineCondicao(RG: TRadioGroup): String;
begin
  case RG.ItemIndex of
    0: Result := 'frxDsMovimento."KGT"';
    1: Result := 'frxDsMovimento."NOMETERCEIRO"';
    2: Result := 'frxDsMovimento."NOMECONTA"';
  end;
end;

function TFmPrincipalImp.DefineTotal(RG: TRadioGroup): String;
begin
  case RG.ItemIndex of
    0: Result := '';
    1: Result := 'Total do terceiro [frxDsMovimento."NOMETERCEIRO"]: ';
    2: Result := 'Total da conta [frxDsMovimento."NOMECONTA"]: ';
  end;
end;

function TFmPrincipalImp.DefineMeuMemo1(RG: TRadioGroup): String;
begin
  case RG.ItemIndex of
    0: Result := '';
    1: Result := '[frxDsMovimento."NOMETERCEIRO"]';
    2: Result := '[frxDsMovimento."NOMECONTA"]';
  end;
end;

function TFmPrincipalImp.DefineMeuMemo2(RG: TRadioGroup): String;
begin
  case RG.ItemIndex of
    0: Result := '';
    1: Result := '[frxDsMovimento."TXTTETel1"]';
    2: Result := '';
  end;
end;

function TFmPrincipalImp.DefineMeuMemo3(RG: TRadioGroup): String;
begin
  case RG.ItemIndex of
    0: Result := '';
    1: Result := '[frxDsMovimento."TEContato"]';
    2: Result := '';
  end;
end;

procedure TFmPrincipalImp.EdEmpresaChange(Sender: TObject);
begin
  ReopenCarteiras();
end;

procedure TFmPrincipalImp.QrAnteriorCalcFields(DataSet: TDataSet);
begin
  QrAnteriorSALDO.Value :=
  QrAnteriorCredito.Value -
  QrAnteriorDebito.Value +
  QrInicialINICIAL.Value;
end;

procedure TFmPrincipalImp.QrExtratoCalcFields(DataSet: TDataSet);
begin
  if QrExtratoCliente.Value <> 0 then
     QrExtratoNOMERELACIONADO.Value := QrExtratoNOMECLIENTE.Value else
  if QrExtratoFornecedor.Value <> 0 then
     QrExtratoNOMERELACIONADO.Value := QrExtratoNOMEFORNECEDOR.Value else
  QrExtratoNOMERELACIONADO.Value := '';
end;

procedure TFmPrincipalImp.frxExtratoGetValue(const VarName: String;
  var Value: Variant);
var
  Saldo, Valor: Double;
begin
  if VarName = 'INICIAL' then
  begin
    FSaldo := QrAnteriorSaldo.Value;
    Value  := FSaldo;
    FAcum  := FSaldo;
    FAcuB  := FSaldo;
  end
  else if VarName = 'NOMEREL' then Value := 'EXTRATO DE CARTEIRA �NICA'
  else if VarName = 'VAR_NOMECARTEIRA' then Value := CBCarteira.Text
  else if VarName = 'VAR_NOMECLIINT' then Value := CBEmpresa.Text
  else if VarName = 'VAR_NOMECLIFOR' then Value := CBCliFor.Text
  else if VarName = 'VAR_NOMECONTA' then  Value := CBConta.Text
  else if VarName = 'SALDO' then
  begin
    Value := QrExtratoCredito.Value - QrExtratoDebito.Value;
    FSaldo := FSaldo + Value;
  end
  else if VarName = 'SALDODIA' then Value := FSaldo
  else if VarName = 'PERIODO' then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIni.Date)+ CO_ATE+
    FormatDateTime(VAR_FORMATDATE3, TPFim.Date)
  else if VarName = 'CAPTION1' then
  begin
    case RG1.ItemIndex of
      0: Value := '';
      1: Value := 'Nome';
      2: Value := 'Conta';
    end;
  end else if VarName = 'CAPTION2' then
  begin
    case RG1.ItemIndex of
      0: Value := '';
      1: Value := 'Telefone';
      2: Value := '';
    end;
  end else if VarName = 'CAPTION3' then
  begin
    case RG1.ItemIndex of
      0: Value := '';
      1: Value := 'Contato';
      2: Value := '';
    end;
  end

  // user function

  else if VarName = 'VAR_SHOW1' then
  begin
    if Ck1.Visible and Ck1.Enabled and (RG1.ItemIndex > 0) then
      Value := True else Value := False;
  end else if VarName = 'VAR_SHOW2' then begin
    if Ck2.Visible and Ck2.Enabled and (RG2.ItemIndex > 0) then
      Value := True else Value := False;
  end else if VarName = 'VAR_CONDITION1' then begin
    Value := DefineCondicao(RG1);
  end else if VarName = 'VAR_CONDITION2' then begin
    Value := DefineCondicao(RG2)
  end else if VarName = 'VAR_DEFMEMO1' then begin
    Value := DefineTotal(RG1);
  end else if VarName = 'VAR_DEFMEMO2' then begin
    Value := DefineTotal(RG2)
  {
  end else if VarName = 'VAR_DEFTIT1' then begin
    Value := DefineTitulo(RG1);
  end else if VarName = 'VAR_DEFTIT2' then begin
    Value := DefineTitulo(RG2)
  }
  end
  else if VarName = 'VAR_MEMO11' then
    Value := DefineMeuMemo1(RG1)
  else if VarName = 'VAR_MEMO12' then
    Value := DefineMeuMemo2(RG1)
  else if VarName = 'VAR_MEMO13' then
    Value := DefineMeuMemo3(RG1)
  else if VarName = 'VAR_MEMO21' then
    Value := DefineMeuMemo1(RG2)
  else if VarName = 'VAR_MEMO22' then
    Value := DefineMeuMemo2(RG2)
  else if VarName = 'VAR_MEMO23' then
    Value := DefineMeuMemo3(RG2)
  //
  else if VarName = 'FZEROU_SDO_ACUM' then
  begin
    FSDO_ACUM := 0;
    FQt2_ACUM := 0;
    Value := False; // > Visible := False;
  end
  else if VarName = 'VARF_NO_QTDE' then
    Value := EdTituloQtde.Text
  else if VarName = 'VARF_NO_QTD2' then
    Value := EdTituloQtd2.Text
  else if VarName = 'SDO_ACUM' then
  begin
    FAcum := FAcum + QrExtratoCredito.Value - QrExtratoDebito.Value;
    Value := FAcum;
  end
  else if VarName = 'SDO_ACUB' then
  begin
    FAcub := FAcub + QrExtratoCredito.Value - QrExtratoDebito.Value;
    Value := FAcub;
  end
  else if VarName = 'VARF_VAL_TOTAL' then
  begin
    Value := QrExtratoCredito.Value - QrExtratoDebito.Value;
    if Value < 0 then
    begin
      Value := -Value;
      FSDO_ACUM := FSDO_ACUM + Value;
      FQT2_ACUM := FQT2_ACUM + QrExtratoQtd2.Value;
    end;
  end
  else if VarName = 'VARF_VAL_UNIT' then
  begin
    if QrExtratoQtd2.Value > 0 then
      Value := QrExtratoDebito.Value / QrExtratoQtd2.Value
    else
    if QrExtratoQtde.Value > 0 then
      Value := QrExtratoDebito.Value / QrExtratoQtd2.Value
    else
      Value := 0.00
  end
  else if VarName = 'VARF_D_C' then
  begin
    Valor := QrExtratoCredito.Value - QrExtratoDebito.Value;
    if Valor < 0 then
      Value := 'D'
    else
    if Valor > 0 then
      Value := 'C'
    else
      Value := '';
  end
  else if VarName = 'VARF_SDOACUM' then
    Value := FSDO_ACUM
  else if VarName = 'VARF_QT2ACUM' then
    Value := FQT2_ACUM
  else

end;

end.

