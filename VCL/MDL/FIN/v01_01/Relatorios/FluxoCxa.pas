unit FluxoCxa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, Grids, dmkGeral,
  DBGrids, Db, mySQLDbTables, Mask, DBCtrls, frxClass, frxDBSet, Variants,
  UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmFluxoCxa = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    QrFluxo: TmySQLQuery;
    DsFluxo: TDataSource;
    QrInicial: TmySQLQuery;
    QrInicialValor: TFloatField;
    Panel5: TPanel;
    Panel6: TPanel;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    QrMovant: TmySQLQuery;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    QrMovantMovim: TFloatField;
    QrMovantVALOR: TFloatField;
    Label7: TLabel;
    DBEdit3: TDBEdit;
    QrFluxoData: TDateField;
    QrFluxoTipo: TSmallintField;
    QrFluxoCarteira: TIntegerField;
    QrFluxoControle: TIntegerField;
    QrFluxoSub: TSmallintField;
    QrFluxoAutorizacao: TIntegerField;
    QrFluxoGenero: TIntegerField;
    QrFluxoQtde: TFloatField;
    QrFluxoDescricao: TWideStringField;
    QrFluxoNotaFiscal: TIntegerField;
    QrFluxoDebito: TFloatField;
    QrFluxoCredito: TFloatField;
    QrFluxoCompensado: TDateField;
    QrFluxoSerieCH: TWideStringField;
    QrFluxoDocumento: TFloatField;
    QrFluxoSit: TIntegerField;
    QrFluxoVencimento: TDateField;
    QrFluxoFatID: TIntegerField;
    QrFluxoFatID_Sub: TIntegerField;
    QrFluxoFatParcela: TIntegerField;
    QrFluxoID_Pgto: TIntegerField;
    QrFluxoID_Sub: TSmallintField;
    QrFluxoFatura: TWideStringField;
    QrFluxoEmitente: TWideStringField;
    QrFluxoBanco: TIntegerField;
    QrFluxoContaCorrente: TWideStringField;
    QrFluxoCNPJCPF: TWideStringField;
    QrFluxoLocal: TIntegerField;
    QrFluxoCartao: TIntegerField;
    QrFluxoLinha: TIntegerField;
    QrFluxoOperCount: TIntegerField;
    QrFluxoLancto: TIntegerField;
    QrFluxoPago: TFloatField;
    QrFluxoMez: TIntegerField;
    QrFluxoFornecedor: TIntegerField;
    QrFluxoCliente: TIntegerField;
    QrFluxoCliInt: TIntegerField;
    QrFluxoForneceI: TIntegerField;
    QrFluxoMoraDia: TFloatField;
    QrFluxoMulta: TFloatField;
    QrFluxoMoraVal: TFloatField;
    QrFluxoMultaVal: TFloatField;
    QrFluxoProtesto: TDateField;
    QrFluxoDataDoc: TDateField;
    QrFluxoCtrlIni: TIntegerField;
    QrFluxoNivel: TIntegerField;
    QrFluxoVendedor: TIntegerField;
    QrFluxoAccount: TIntegerField;
    QrFluxoICMS_P: TFloatField;
    QrFluxoICMS_V: TFloatField;
    QrFluxoDuplicata: TWideStringField;
    QrFluxoDepto: TIntegerField;
    QrFluxoDescoPor: TIntegerField;
    QrFluxoDescoVal: TFloatField;
    QrFluxoDescoControle: TIntegerField;
    QrFluxoUnidade: TIntegerField;
    QrFluxoNFVal: TFloatField;
    QrFluxoAntigo: TWideStringField;
    QrFluxoExcelGru: TIntegerField;
    QrFluxoDoc2: TWideStringField;
    QrFluxoCNAB_Sit: TSmallintField;
    QrFluxoTipoCH: TSmallintField;
    QrFluxoLk: TIntegerField;
    QrFluxoDataCad: TDateField;
    QrFluxoDataAlt: TDateField;
    QrFluxoUserCad: TIntegerField;
    QrFluxoUserAlt: TIntegerField;
    PB1: TProgressBar;
    QrFluxoNOMECART: TWideStringField;
    QrLct: TmySQLQuery;
    QrExtrato: TmySQLQuery;
    DsExtrato: TDataSource;
    QrExtratoDataE: TDateField;
    QrExtratoDataV: TDateField;
    QrExtratoDataQ: TDateField;
    QrExtratoTexto: TWideStringField;
    QrExtratoDocum: TWideStringField;
    QrExtratoNotaF: TWideStringField;
    QrExtratoCredi: TFloatField;
    QrExtratoDebit: TFloatField;
    QrExtratoSaldo: TFloatField;
    QrExtratoCartC: TIntegerField;
    QrExtratoCartN: TWideStringField;
    QrExtratoCodig: TIntegerField;
    QrExtratoCtrle: TIntegerField;
    QrExtratoCtSub: TIntegerField;
    QrExtratoDataX: TDateField;
    QrExtratoID_Pg: TIntegerField;
    QrExtratoTipoI: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGPesq: TDBGrid;
    DBGFluxo: TDBGrid;
    frxExtrato: TfrxReport;
    frxDsExtrato: TfrxDBDataset;
    GroupBox1: TGroupBox;
    CkIni: TCheckBox;
    TPIni: TDateTimePicker;
    CkFim: TCheckBox;
    TPFim: TDateTimePicker;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    TPCre: TDateTimePicker;
    TPDeb: TDateTimePicker;
    Label3: TLabel;
    Label4: TLabel;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    QrPRI: TmySQLQuery;
    DsPRI: TDataSource;
    QrPRIPeriodo: TIntegerField;
    QrPRIControle: TIntegerField;
    QrPRIConta: TIntegerField;
    QrPRIValor: TFloatField;
    QrPRIPrevBaI: TIntegerField;
    QrPRITexto: TWideStringField;
    QrPRIPrevBaC: TIntegerField;
    QrPRIEmitVal: TFloatField;
    QrPRIDIFER: TFloatField;
    QrPRIPERIODO_TXT: TWideStringField;
    QrRealiz: TmySQLQuery;
    QrRealizValor: TFloatField;
    QrFluxoQUITACAO: TDateField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtPesquisa: TBitBtn;
    BtPRI: TBitBtn;
    BtGera: TBitBtn;
    BtSaida: TBitBtn;
    QrFluxoAgencia: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure QrMovantCalcFields(DataSet: TDataSet);
    procedure QrExtratoBeforeOpen(DataSet: TDataSet);
    procedure BtPRIClick(Sender: TObject);
    procedure QrPRICalcFields(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPRI();
  public
    { Public declarations }
    FDtIni, FDtEncer, FDtMorto: TDateTime;
    FEmpresa, FEntidade: Integer;
    FEntidade_TXT, FTabLctA, FTabLctB, FTabLctD: String;
  end;

  var
  FmFluxoCxa: TFmFluxoCxa;

implementation

uses UnMyObjects, Module, ModuleGeral, UCreate, UMySQLModule, UnInternalConsts,
  UnFinanceiro, DmkDAC_PF;

{$R *.DFM}

procedure TFmFluxoCxa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFluxoCxa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFluxoCxa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFluxoCxa.PageControl1Change(Sender: TObject);
begin
  BtPRI.Visible := PageControl1.ActivePageIndex = 2;
end;

procedure TFmFluxoCxa.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  if VAR_KIND_DEPTO = kdUH then
    TabSheet3.TabVisible := True
  else
    TabSheet3.TabVisible := False;
  //
  BtPRI.Visible  := False;
  BtGera.Visible := False;
  //
  QrFluxo.Close;
  QrFluxo.Database := DModG.MyPID_DB;
  //
  QrExtrato.Close;
  QrExtrato.Database := DModG.MyPID_DB;
  //
  QrRealiz.Close;
  QrRealiz.Database := DModG.MyPID_DB;
  //
  QrLct.Close;
  QrLct.Database := DModG.MyPID_DB;
  //
  TPIni.Date := Int(Date);
  TPFim.Date := Int(Date);
  TPCre.Date := Int(Date);
  TPDeb.Date := Int(Date);
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmFluxoCxa.BtPesquisaClick(Sender: TObject);
  function Fmt(Data: TDateTime): String;
  begin
    Result := '"' + FormatDateTime('YYYY-MM-DD', Data) + '"';
  end;
  procedure GeraParteSQL(TabLct, Quitacao: String);
  begin
    QrFluxo.SQL.Add('SELECT ' + Quitacao + ' QUITACAO, ');
    QrFluxo.SQL.Add('lan.*, car.Nome NOMECART');
    QrFluxo.SQL.Add('FROM ' + TabLct + ' lan');
    QrFluxo.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QrFluxo.SQL.Add('WHERE car.Fornecei=' + FEntidade_TXT);
    QrFluxo.SQL.Add(dmkPF.SQL_Periodo('AND ' + Quitacao,
      TPIni.Date, TPFim.Date, CkIni.Checked, CkFim.Checked));
    QrFluxo.SQL.Add('AND lan.Genero > 0');
    QrFluxo.SQL.Add('AND ((car.Tipo<>2) OR (lan.Sit < 2))');
  end;
var
  Quitacao, Texto, Docum, NotaF: String;
  DataE, DataV, DataQ, DataX: Variant;
  Codig: Integer;
  Saldo: Double;
  DataI: String;
  FldIni, TabIni: String;
begin
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  FDtIni := Int(TPIni.Date);
  FldIni := UFinanceiro.DefLctFldSdoIni(FDtIni, FDtEncer, FDtMorto);
  TabIni := UFinanceiro.DefLctTab(FDtIni, FDtEncer, FDtMorto,
            FTabLcta, FTabLctB, FTabLctD);
  ///

  //   S A L D O    I N I C I A L
  QrInicial.Close;
{
SELECT SUM(Inicial) Valor
FROM carteiras
WHERE Tipo <> 2
AND ForneceI=:P0
}
  QrInicial.SQL.Clear;
  QrInicial.SQL.Add('SELECT SUM(' + FldIni + ') Valor');
  QrInicial.SQL.Add('FROM carteiras car ');
  QrInicial.SQL.Add('WHERE car.Tipo <> 2');
  QrInicial.SQL.Add('AND car.ForneceI=' + FEntidade_TXT);
  UnDmkDAC_PF.AbreQuery(QrInicial, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 

  //   M O V I M E N T O
  if CkIni.Checked then
    DataI := Geral.FDT(TPIni.Date, 1)
  else
    DataI := '0';
  QrMovant.Close;
  QrMovant.SQL.Clear;
{
SELECT SUM(lan.Credito-lan.Debito) Movim
FROM lan ctos lan
LEFT JOIN carteiras car ON car.Codigo=lan.Carteira
WHERE car.Tipo <> 2
AND car.ForneceI=:P0
AND Data < :P1
}
  QrMovant.SQL.Add('SELECT SUM(lan.Credito-lan.Debito) Movim');
  QrMovant.SQL.Add('FROM ' + TabIni + ' lan');
  QrMovant.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrMovant.SQL.Add('WHERE car.Tipo <> 2');
  QrMovant.SQL.Add('AND car.ForneceI=' + FEntidade_TXT);
  QrMovant.SQL.Add('AND Data < "' + DataI + '"');
  UnDmkDAC_PF.AbreQuery(QrMovant, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 

  //   L A N � A M E N T O S
   Quitacao := 'Date(IF(car.Tipo <> 2, lan.Data,IF(lan.Sit > 1, lan.Compensado, ' +
  'IF(lan.Vencimento<SYSDATE(), IF(lan.Debito>0, ' + Fmt(TPDeb.Date) +
  ', ' + Fmt(TPCre.Date) + '), lan.Vencimento))))';
  //
  QrFluxo.Close;
  QrFluxo.SQL.Clear;


{
  QrFluxo.SQL.Add('SELECT ' + Quitacao + ' QUITACAO, ');
  QrFluxo.SQL.Add('lan.*, car.Nome NOMECART');
  /
  QrFluxo.SQL.Add('FROM ' + VAR LCT + ' lan');
  QrFluxo.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrFluxo.SQL.Add('WHERE car.Fornecei=' + IntToStr(FEntCod));
  QrFluxo.SQL.Add(dmkPF.SQL_Periodo('AND ' + Quitacao,
    TPIni.Date, TPFim.Date, CkIni.Checked, CkFim.Checked));
  QrFluxo.SQL.Add('AND lan.Genero > 0');
  QrFluxo.SQL.Add('AND ((car.Tipo<>2) OR (lan.Sit < 2))');
  //
  QrFluxo.SQL.Add('ORDER BY QUITACAO, Credito DESC, Debito');
}
  QrFluxo.SQL.Add('DROP TABLE IF EXISTS _FIN_FLCXA_001_FLUXO;');
  QrFluxo.SQL.Add('CREATE TABLE _FIN_FLCXA_001_FLUXO');
  GeraParteSQL(FTabLctA, Quitacao);
  QrFluxo.SQL.Add('UNION');
  GeraParteSQL(FTabLctB, Quitacao);
  QrFluxo.SQL.Add('UNION');
  GeraParteSQL(FTabLctD, Quitacao);
  QrFluxo.SQL.Add(';');
  QrFluxo.SQL.Add('SELECT * ');
  QrFluxo.SQL.Add('FROM _FIN_FLCXA_001_FLUXO');
  QrFluxo.SQL.Add('ORDER BY QUITACAO, Credito DESC, Debito;');
  QrFluxo.SQL.Add('');
  QrFluxo.SQL.Add('DROP TABLE IF EXISTS _FIN_FLCXA_001_FLUXO;');
  QrFluxo.SQL.Add('');
  //
  UnDmkDAC_PF.AbreQuery(QrFluxo, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 

  // INSERE DADOS TABELA LOCAL
  UCriar.RecriaTempTable('extratocc2', DModG.QrUpdPID1, False);
  {
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM extratocc2');
  DModG.QrUpdPID1.ExecSQL;
  }
  //
  Codig := 1;
  PB1.Position := Codig;
  PB1.Update;
  Application.ProcessMessages;
  //
  Saldo := QrMovantVALOR.Value;
  UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'ExtratoCC2', false,
  [
    'DataE', 'DataV', 'DataQ',
    'DataX', 'Texto', 'Docum',
    'NotaF', 'Credi', 'Debit',
    'Saldo', 'CartC', 'CartN',
    'TipoI', 'Ctrle', 'ID_Pg'
  ], ['Codig'], [
    //Null, Null, Null,
    '0000-00-00', '0000-00-00', '0000-00-00',
    Geral.FDT(TPIni.Date, 1), 'SALDO ANTERIOR', '',
    '', 0, 0,
    Saldo, 0, '*** T O D A S ***', 1, 0, 0
  ], [Codig], False);
  //
  QrFluxo.First;
  while not QrFluxo.Eof do
  begin
    Codig := Codig + 1;
    PB1.Position := Codig;
    PB1.Update;
    Application.ProcessMessages;
    Saldo := Saldo + QrFluxoCredito.Value - QrFluxoDebito.Value;
    DataE := Geral.FDT(QrFluxoData.Value, 1);
    DataV := Geral.FDT(QrFluxoVencimento.Value, 1);
    DataQ := Geral.FDT(QrFluxoCompensado.Value, 1);
    DataX := QrFluxoQUITACAO.Value;
    //
    Texto := QrFluxoDescricao.Value;
    if QrFluxoQtde.Value > 0 then
      Texto := FloatToStr(QrFluxoQtde.Value) + ' ' + Texto;
    if QrFluxoDocumento.Value > 0 then
      Docum := FormatFloat('000000', QrFluxoDocumento.Value) else Docum := '';
    if QrFluxoSerieCH.Value <> '' then Docum := QrFluxoSerieCH.Value + Docum;
    if QrFluxoNotaFiscal.Value > 0 then
      NotaF := FormatFloat('000000', QrFluxoNotaFiscal.Value) else NotaF := '';
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'ExtratoCC2', false,
    [
      'DataE', 'DataV', 'DataQ',
      'DataX', 'Texto', 'Docum',
      'NotaF', 'Credi', 'Debit',
      'Saldo', 'CartC', 'CartN',
      'TipoI', 'Ctrle', 'ID_Pg', 'CtSub'
    ], ['Codig'], [
      DataE, DataV, DataQ,
      DataX, Texto, Docum,
      NotaF, QrFluxoCredito.Value, QrFluxoDebito.Value,
      Saldo, QrFluxoCarteira.Value, QrFluxoNOMECART.Value,
      2, QrFluxoControle.Value, QrFluxoID_Pgto.Value, QrFluxoSub.Value
    ], [Codig], False);
    //
    QrFluxo.Next;
  end;
  PB1.Max := QrFluxo.RecordCount + 1;
  PageControl1.ActivepageIndex := 1;
  QrExtrato.Close;
  UnDmkDAC_PF.AbreQuery(QrExtrato, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  PB1.Position := 0;
  Screen.Cursor := crDefault;
end;

procedure TFmFluxoCxa.QrMovantCalcFields(DataSet: TDataSet);
begin
  QrMovantVALOR.Value := QrMovantMovim.Value + QrInicialValor.Value;
end;

procedure TFmFluxoCxa.QrExtratoBeforeOpen(DataSet: TDataSet);
  procedure GeraParteSQL(TabLct: String);
  begin
    QrLct.SQL.Add('SELECT lct.Controle, lct.Carteira, IF(lct.Controle=0, "",');
    QrLct.SQL.Add('IF(lct.Carteira=0, "", car.Nome)) NOMECART');
    QrLct.SQL.Add('FROM ' + TabLct + ' lct');
    QrLct.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrLct.SQL.Add('WHERE Controle IN (');
    QrLct.SQL.Add('  SELECT ID_Pg');
    QrLct.SQL.Add('  FROM extratocc2');
    QrLct.SQL.Add('  WHERE ID_Pg <> 0');
    QrLct.SQL.Add(')');
  end;
begin
{ MUITO DEMORADO!!!!
  QrLct.Close;
  QrLct.SQL.Clear;
(*
SELECT lan.Controle, lan.Carteira, IF(lan.Controle=0, "",
IF(lan.Carteira=0, "", car.Nome)) NOMECART
FROM lan ctos lan
LEFT JOIN carteiras car ON car.Codigo=lan.Carteira
WHERE car.ForneceI=:P0
*)
  QrLct.SQL.Add('DROP TABLE IF EXISTS _FIN_FLCXA_001_LCT;');
  QrLct.SQL.Add('CREATE TABLE _FIN_FLCXA_001_LCT');
  GeraParteSQL(FTabLctA);
  QrLct.SQL.Add('UNION');
  GeraParteSQL(FTabLctB);
  QrLct.SQL.Add('UNION');
  GeraParteSQL(FTabLctD);
  QrLct.SQL.Add(';');
  QrLct.SQL.Add('SELECT * ');
  QrLct.SQL.Add('FROM _FIN_FLCXA_001_LCT;');
  QrLct.SQL.Add('');
  QrLct.SQL.Add('DROP TABLE IF EXISTS _FIN_FLCXA_001_LCT;');
  QrLct.SQL.Add('');
  //
  UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
}
end;

procedure TFmFluxoCxa.QrPRICalcFields(DataSet: TDataSet);
begin
  QrPRIPERIODO_TXT.Value := dmkPF.PeriodoToMensal(QrPRIPeriodo.Value);
end;

procedure TFmFluxoCxa.ReopenPRI();
var
  TabPriA, TabPrvA, Empr_TXT: String;
begin
  Empr_TXT := FormatFloat('0', FEmpresa);
  TabPriA := DModG.NomeTab(TMeuDB, ntPri, False, ttA, FEmpresa);
  TabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, FEmpresa);
  //
  QrPRI.Close;
  QrPRI.SQL.Clear;
  QrPRI.SQL.Add('SELECT prv.Periodo, pri.Controle, pri.Conta, pri.Valor,');
  QrPRI.SQL.Add('pri.PrevBaI, pri.Texto, pri.PrevBaC, pri.EmitVal');
  QrPRI.SQL.Add('FROM ' + TabPriA + ' pri');
  QrPRI.SQL.Add('LEFT JOIN ' + TabPrvA + ' prv ON prv.Codigo=pri.Codigo');
  QrPRI.SQL.Add('WHERE prv.Cond=' + Empr_TXT);
  QrPRI.SQL.Add('AND EmitSit=0');
  QrPRI.SQL.Add('OR EmitVal < Valor');
  QrPRI.SQL.Add('ORDER BY prv.Periodo, pri.Texto');
  UnDmkDAC_PF.AbreQuery(QrPRI, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
end;

procedure TFmFluxoCxa.BtPRIClick(Sender: TObject);
  procedure GeraParteSQL(TabLct, Genero_TXT, Mez_TXT, DataI, DataF: String);
  begin
    QrRealiz.SQL.Add('SELECT SUM(Credito-Debito)  Valor');
    QrRealiz.SQL.Add('FROM ' + TabLct + ' lan');
    QrRealiz.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QrRealiz.SQL.Add('WHERE car.ForneceI=' + FEntidade_TXT);
    QrRealiz.SQL.Add('AND lan.Genero=' + Genero_TXT);
    QrRealiz.SQL.Add('AND ((lan.Mez=' + Mez_TXT +
      ') or ((lan.Mez=0) AND (lan.Data BETWEEN "' + DataI + '" AND "'+ DataF +'")))');
    QrRealiz.SQL.Add('AND ((lan.Tipo in (0,1)) or ((lan.Tipo=2) and (lan.Compensado<2)))');
  end;
var
  DataI, DataF: String;
  Sit: Integer;
  Valor: Double;
  Genero_TXT, Mez_TXT, TabPriA: String;
begin
  Screen.Cursor := crHourGlass;
  //
  PageControl1.ActivePageIndex := 2;
  Update;
  Application.ProcessMessages;
  //
  ReopenPRI();
  //
  PB1.Position := 0;
  PB1.Max := QrPRI.RecordCount;
  Update;
  Application.ProcessMessages;
  //
  TabPriA := DModG.NomeTab(TMeuDB, ntPri, False, ttA, FEmpresa);
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + TabPriA + ' SET EmitVal=:P0, EmitSit=:P1 ');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa');
  while not QrPRI.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Update;
    Application.ProcessMessages;
    DataI := Geral.FDT(dmkPF.PrimeiroDiaDoPeriodo_Date(QrPRIPeriodo.Value), 1);
    DataF := Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(QrPRIPeriodo.Value), 1);
    Genero_TXT := FormatFloat('0', QrPRIConta.Value);
    Mez_TXT    := FormatFloat('0', dmkPF.PeriodoToAnoMes(QrPRIPeriodo.Value));
    //
    QrRealiz.Close;
    QrRealiz.SQL.Clear;

    QrRealiz.SQL.Add('DROP TABLE IF EXISTS _FIN_FLCXA_001_REALIZ;');
    QrRealiz.SQL.Add('CREATE TABLE _FIN_FLCXA_001_REALIZ');
    GeraParteSQL(FTabLctA, Genero_TXT, Mez_TXT, DataI, DataF);
    QrRealiz.SQL.Add('UNION');
    GeraParteSQL(FTabLctB, Genero_TXT, Mez_TXT, DataI, DataF);
    QrRealiz.SQL.Add('UNION');
    GeraParteSQL(FTabLctD, Genero_TXT, Mez_TXT, DataI, DataF);
    QrRealiz.SQL.Add(';');
    QrRealiz.SQL.Add('SELECT SUM(Valor) Valor');
    QrRealiz.SQL.Add('FROM _FIN_FLCXA_001_REALIZ;');
    QrRealiz.SQL.Add('');
    QrRealiz.SQL.Add('DROP TABLE IF EXISTS _FIN_FLCXA_001_REALIZ;');
    QrRealiz.SQL.Add('');
  //
{
SELECT SUM(Credito-Debito)  Valor
FROM lan ctos lan
LEFT JOIN carteiras car ON car.Codigo=lan.Carteira
WHERE car.ForneceI=:P0
AND lan.Genero=:P1
AND ((lan.Mez=:P2) or ((lan.Mez=0) AND (lan.Data BETWEEN :P3 AND :P4)))
AND ((lan.Tipo in (0,1)) or ((lan.Tipo=2) and (lan.Compensado<2)))
    QrRealiz.Params[00].AsInteger := FEntidade_TXT;
    QrRealiz.Params[01].AsInteger := QrPRIConta.Value;
    QrRealiz.Params[02].AsInteger := MLAGeral.PeriodoToMez(QrPRIPeriodo.Value);
    QrRealiz.Params[03].AsString  := DataI;
    QrRealiz.Params[04].AsString  := DataF;
}
    UnDmkDAC_PF.AbreQuery(QrRealiz, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
    //
    Valor := QrRealizValor.Value;
    if Valor < 0 then Valor := - Valor;
    if Valor >= QrPRIValor.Value then Sit := 1 else Sit := 0;
    //
    Dmod.QrUpd.Params[00].AsFloat   := Valor;
    Dmod.QrUpd.Params[01].AsInteger := Sit;
    Dmod.QrUpd.Params[02].AsInteger := QrPRIControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrPRI.Next;
  end;
  //
  ReopenPRI();
  //
  PB1.Position := 0;
  Screen.Cursor := crDefault;
end;

end.

