unit MovFin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Tabs, frxClass, frxDBSet, (*dmkDockTabSet,*)DmkDAC_PF, UnDmkEnums,
  UnDmkProcFunc;

type
  TFmMovFin = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Splitter1: TSplitter;
    Timer1: TTimer;
    PainelC: TPanel;
    PnDatas2: TPanel;
    GBVencto: TGroupBox;
    LaVenctI: TLabel;
    LaVenctF: TLabel;
    TPVctoIni: TdmkEditDateTimePicker;
    TPVctoFim: TdmkEditDateTimePicker;
    CkVencto: TCheckBox;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    TPDocIni: TdmkEditDateTimePicker;
    TPDocFim: TdmkEditDateTimePicker;
    CkDataDoc: TCheckBox;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    TPCompIni: TdmkEditDateTimePicker;
    TPCompFim: TdmkEditDateTimePicker;
    CkDataComp: TCheckBox;
    GroupBox4: TGroupBox;
    Label101: TLabel;
    Label102: TLabel;
    TPEmissIni: TdmkEditDateTimePicker;
    TPEmissFim: TdmkEditDateTimePicker;
    CkEmissao: TCheckBox;
    PB1: TProgressBar;
    QrEmpresas: TmySQLQuery;
    QrEmpresasCodCliInt: TIntegerField;
    QrEmpresasCodEnti: TIntegerField;
    QrEmpresasCodFilial: TIntegerField;
    QrEmpresasNome: TWideStringField;
    Panel5: TPanel;
    LaEmpresa: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrLcts: TmySQLQuery;
    RGSit: TRadioGroup;
    frxFIN_RELAT_013_01: TfrxReport;
    Panel6: TPanel;
    CGAgrup: TdmkCheckGroup;
    frxDsLcts: TfrxDBDataset;
    QrLctsCodCliInt: TLargeintField;
    QrLctsNO_CodCliInt: TWideStringField;
    QrLctsData: TDateField;
    QrLctsVencimento: TDateField;
    QrLctsCompensado: TDateField;
    QrLctsDescricao: TWideStringField;
    QrLctsDocumento: TFloatField;
    QrLctsCredito: TFloatField;
    QrLctsDebito: TFloatField;
    QrLctsNO_CLI_FOR: TWideStringField;
    QrLctscN1: TIntegerField;
    QrLctsoN1: TIntegerField;
    QrLctsnN1: TWideStringField;
    QrLctscN2: TIntegerField;
    QrLctsoN2: TIntegerField;
    QrLctsnN2: TWideStringField;
    QrLctscN3: TIntegerField;
    QrLctsoN3: TIntegerField;
    QrLctsnN3: TWideStringField;
    QrLctscN4: TIntegerField;
    QrLctsoN4: TIntegerField;
    QrLctsnN4: TWideStringField;
    QrLctscN5: TIntegerField;
    QrLctsoN5: TIntegerField;
    QrLctsnN5: TWideStringField;
    QrLctsControle: TIntegerField;
    QrLctsCOMPENSADO_TXT: TWideStringField;
    QrSums: TmySQLQuery;
    frxDsSums: TfrxDBDataset;
    frxFIN_RELAT_013_00: TfrxReport;
    QrSumscN5: TIntegerField;
    QrSumsoN5: TIntegerField;
    QrSumsnN5: TWideStringField;
    QrSumscN4: TIntegerField;
    QrSumsoN4: TIntegerField;
    QrSumsnN4: TWideStringField;
    QrSumscN3: TIntegerField;
    QrSumsoN3: TIntegerField;
    QrSumsnN3: TWideStringField;
    QrSumscN2: TIntegerField;
    QrSumsoN2: TIntegerField;
    QrSumsnN2: TWideStringField;
    QrSumscN1: TIntegerField;
    QrSumsoN1: TIntegerField;
    QrSumsnN1: TWideStringField;
    QrSumsCredito: TFloatField;
    QrSumsDebito: TFloatField;
    RGDemonstrativo: TRadioGroup;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    Memo1: TMemo;
    DockTabSet1: TTabSet;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DockTabSet1DockDrop(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer);
    procedure DockTabSet1TabRemoved(Sender: TObject);
    procedure DockTabSet1TabAdded(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrLctsCalcFields(DataSet: TDataSet);
    procedure frxFIN_RELAT_013_01GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    FInicio: TDateTime;
    FCliInt, FEntidade: Integer;
    FTabLctA, FTabLctB, FTabLctD, FEntidade_TXT: String;
    FDtEncer, FDtMorto: TDateTime;
    FInCreation: Boolean;
    FPreparou: Boolean;
    //
    procedure RecriaDockForms();
    procedure Pesquisa(Query: TmySQLQuery);
    function SQL_Agrupar(): String;
    function SQL_Ordenar(): String;
  public
    { Public declarations }
    FIndi_Pags, FPla_Ctas: String;
    //
  end;

  var
  FmMovFin: TFmMovFin;

implementation

uses UnMyObjects, Module, ModuleGeral, ModuleFin,
// DockForms
_Empresas, _Indi_Pags, _Pla_Ctas,
// fim DockForms
UMySQLModule, UCreate, CreateGeral, UCreateFin;


{$R *.DFM}

var
  Ext_EmisI, Ext_EmisF, Ext_VctoI, Ext_VctoF, Ext_DataH, Ext_DataA,
  Ext_DocI,  Ext_DocF,  Ext_CompI, Ext_CompF: String;
  Ext_Saldo, (*Ext_SdIni,*)
  Ext_VALORA,     Ext_VALORB,     Ext_VALORC,
  Ext_PENDENTEA,  Ext_PENDENTEB,  Ext_PENDENTEC,
  Ext_ATUALIZADOA, Ext_ATUALIZADOB, Ext_ATUALIZADOC,
  Ext_DEVIDOA, Ext_DEVIDOB, Ext_DEVIDOC,
  Ext_PAGO_REALA, Ext_PAGO_REALB, Ext_PAGO_REALC: Double;
  //Ext_Vencto: TDate;

const
  MaxJan = 3;
  Janelas: array [0..MaxJan-1] of String = ('Empresas', 'Indica��es',
  'Plano de Contas');

procedure TFmMovFin.Pesquisa(Query: TmySQLQuery);
  procedure GeraParteSQL(TabLct, TextoEmpresa: String);
  begin
    Query.SQL.Add('SELECT ' + TextoEmpresa);
    Query.SQL.Add('lct.Data, lct.Vencimento, lct.Compensado, lct.Descricao, ');
    Query.SQL.Add('lct.Documento, lct.Credito, lct.Debito, lct.Controle, ');
    Query.SQL.Add('IF(lct.Debito>0, IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), ');
    Query.SQL.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome)) NO_CLI_FOR,');
    Query.SQL.Add('');
    Query.SQL.Add('co.cN1, co.oN1, co.nN1, ');
    Query.SQL.Add('co.cN2, co.oN2, co.nN2, ');
    Query.SQL.Add('co.cN3, co.oN3, co.nN3, ');
    Query.SQL.Add('co.cN4, co.oN4, co.nN4, ');
    Query.SQL.Add('co.cN5, co.oN5, co.nN5  ');
    Query.SQL.Add('FROM ' + TabLct + ' lct, ' + TAB_TMP_PLA_CTAS + ' co, ' +
    TMeuDB + '.carteiras ca, ' + TMeuDB + '.entidades cl, ' + TMeuDB + '.entidades fo');
    if VAR_KIND_DEPTO = kdUH then
      Query.SQL.Add(', ' + TMeuDB + '.condimov imv');

    Query.SQL.Add('WHERE fo.Codigo=lct.Fornecedor AND cl.Codigo=lct.Cliente');
{
    Query.SQL.Add('AND sg.Codigo=co.SubGrupo');
    Query.SQL.Add('AND gr.Codigo=sg.Grupo');
    Query.SQL.Add('AND cj.Codigo=gr.Conjunto');
    Query.SQL.Add('AND pl.Codigo=cj.Plano');
}
    Query.SQL.Add('AND lct.Genero<>-1');
    Query.SQL.Add('AND ca.ForneceI =' + FormatFloat('0', FEntidade));
    if CkEmissao.Checked then
      Query.SQL.Add('AND lct.Data BETWEEN "'+Ext_EmisI+'" AND "'+Ext_EmisF+'"');
    if CkVencto.Checked then
      Query.SQL.Add('AND lct.Vencimento BETWEEN "'+Ext_VctoI+'" AND "'+Ext_VctoF+'"');
    if CkDataDoc.Checked then
      Query.SQL.Add('AND lct.DataDoc BETWEEN "'+Ext_DocI+'" AND "'+Ext_DocF+'"');
    if CkDataComp.Checked then
    begin
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE IGNORE ' + FTabLctA + ' SET Compensado=Vencimento');
      Dmod.QrUpdU.SQL.Add('WHERE Tipo<>2');
      Dmod.QrUpdU.ExecSQL;
      Query.SQL.Add('AND lct.Compensado BETWEEN "'+Ext_CompI+'" AND "'+Ext_CompF+'"');
    end;
    case RGSit.ItemIndex of
      0: Query.SQL.Add('AND ca.Tipo<>2');
      1: Query.SQL.Add('AND (ca.Tipo=2 AND lct.Sit <2)');
      2: Query.SQL.Add('AND ((ca.Tipo<>2) OR (ca.Tipo=2 AND lct.Sit <2))');
    end;
    Query.SQL.Add('AND co.cN1=lct.Genero');
    Query.SQL.Add('AND ca.Codigo=lct.Carteira');
    if VAR_KIND_DEPTO = kdUH then
      Query.SQL.Add('AND lct.Depto=imv.Conta');
    //
    // IndiPag
    Query.SQL.Add('AND lct.IndiPag IN (');
    Query.SQL.Add('  SELECT Codigo FROM ' + TAB_TMP_INDIPAGS);
    Query.SQL.Add('  WHERE Ativo=1');
    Query.SQL.Add(')');
    // Fim IndiPag
    //
    // Plano de contas
    Query.SQL.Add('AND lct.Genero=co.cN1');
    Query.SQL.Add('AND co.Ativo=1');
{
    Query.SQL.Add('AND lct.Genero IN (');
    Query.SQL.Add('  SELECT cN1 FROM ' + TAB_TMP_PLA_CTAS);
    Query.SQL.Add('  WHERE Ativo=1');
    Query.SQL.Add(')');
}
    // Fim Plano de contas
  end;
  //
var
  TxtEmp: String;
begin
  FInicio := Now();
  Screen.Cursor := crHourGlass;
  try
    PB1.Position := 0;
    PB1.Max := QrEmpresas.RecordCount;
    QrEmpresas.First;
    while not QrEmpresas.Eof do
    begin
      FEntidade := QrEmpresasCodEnti.Value;
      FCliInt   := QrEmpresasCodCliInt.Value;
      // Precisa?? ou gera erro?
      if FCliInt = 0 then
        FCliInt   := QrEmpresasCodFilial.Value;
      //
      FEntidade_TXT := FormatFloat('0', FEntidade);
      DModG.Def_EM_ABD(TMeuDB, FEntidade, FCliInt,
        FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
      TxtEmp := FormatFloat('0', QrEmpresasCodCliInt.Value) + ' CodCliInt, ''' +
      QrEmpresasNome.Value + ''' NO_CodCliInt, ';
      //
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Pesquisando empresa ' + FormatFloat('0', FCliInt) + ' - ' +
        QrEmpresasNome.Value);
      Query.Close;
      Query.SQL.Clear;
      if QrEmpresas.RecNo = 1 then
      begin
        Query.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_013_1;');
        Query.SQL.Add('CREATE TABLE _FIN_RELAT_013_1');
        Query.SQL.Add('');
      end else begin
        Query.SQL.Add('INSERT INTO _FIN_RELAT_013_1');
        Query.SQL.Add('');
      end;
      GeraParteSQL(FTabLctA, TxtEmp);
      Query.SQL.Add('UNION');
      GeraParteSQL(FTabLctB, TxtEmp);
      Query.SQL.Add('UNION');
      GeraParteSQL(FTabLctD, TxtEmp);
      Query.SQL.Add(';');
      Memo1.Lines.Add(Query.SQL.Text);
      UMyMod.ExecutaQuery(Query);
      //
      QrEmpresas.Next;
    end;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'Tempo: ' + Geral.FDT(Now() - FInicio, 108));
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMovFin.BtOKClick(Sender: TObject);
begin
  if MyObjects.FIC((RGDemonstrativo.ItemIndex = 0) and (CGAgrup.Value >= 16), nil,
    'O demonstrativo sint�tico n�o permite agrupamento pelo n�vel1 (conta)!')
  then
    Exit;
  //
  Memo1.Lines.Clear;
  Memo1.Lines.Add('/*' + Geral.FDT(Now(), 0) + '*/');
  QrEmpresas.Close;
  //
  if EdEmpresa.Enabled and (EdEmpresa.ValueVariant <> 0) then
  begin
    //QrEmpresas.Database := DMod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresas, Dmod.MyDB, [
    'SELECT eci.CodCliInt, eci.CodEnti, eci.CodFilial, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, Nome) Nome',
    'FROM enticliint eci',
    'LEFT JOIN entidades ent ON eci.CodEnti=ent.Codigo',
    'WHERE eci.CodCliInt=' + FormatFloat('0', EdEmpresa.ValueVariant)]);
  end else
  begin
    //QrEmpresas.Database := DModG.MyPID_DB;
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresas, DModG.MyPID_DB, [
    'SELECT CodCliInt, CodEnti, CodFilial, Nome',
    'FROM ' + TAB_TMP_EMPRESAS,
    'WHERE Ativo=1']);
  end;
  //
  if MyObjects.FIC(QrEmpresas.RecordCount = 0, nil,
    'Nenhuma empresa foi definida!') then Exit;
  //

  //
  VAR_PARAR := False;
  Ext_VALORA := 0;
  Ext_VALORB := 0;
  Ext_VALORC := 0;
  Ext_PENDENTEA := 0;
  Ext_PENDENTEB := 0;
  Ext_PENDENTEC := 0;
  Ext_ATUALIZADOA := 0;
  Ext_ATUALIZADOB := 0;
  Ext_ATUALIZADOC := 0;
  Ext_DEVIDOA := 0;
  Ext_DEVIDOB := 0;
  Ext_DEVIDOC := 0;
  Ext_PAGO_REALA := 0;
  Ext_PAGO_REALB := 0;
  Ext_PAGO_REALC := 0;
  dmkPF.ExcluiSQLsDermatek();
  //
  if MyObjects.FIC(CkEmissao.Checked and (TPEmissFim.Date < TPEmissIni.Date), nil,
    '"Data de emiss�o" final menor que inicial!')
  then
    Exit;
  if MyObjects.FIC(CkVencto.Checked and (TPVctoFim.Date < TPVctoIni.Date), nil,
    '"Data de vencimento" final menor que inicial!')
  then
    Exit;
  if MyObjects.FIC(CkDataDoc.Checked and (TPDocFim.Date < TPDocIni.Date), nil,
    '"Data do documento" final menor que inicial!')
  then
    Exit;
  //
  Ext_Saldo := 0;
  Ext_EmisI := FormatDateTime(VAR_FORMATDATE, TPEmissIni.Date);
  Ext_EmisF := FormatDateTime(VAR_FORMATDATE, TPEmissFim.Date);
  Ext_VctoI := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date);
  Ext_VctoF := FormatDateTime(VAR_FORMATDATE, TPVctoFim.Date);
  Ext_CompI := FormatDateTime(VAR_FORMATDATE, TPCompIni.Date);
  Ext_CompF := FormatDateTime(VAR_FORMATDATE, TPCompFim.Date);
  Ext_DocI  := FormatDateTime(VAR_FORMATDATE, TPDocIni.Date);
  Ext_DocF  := FormatDateTime(VAR_FORMATDATE, TPDocFim.Date);
  Ext_DataH := FormatDateTime(VAR_FORMATDATE, Date);
  Ext_DataA := FormatDateTime(VAR_FORMATDATE, TPEmissIni.Date-1);
  //
  Pesquisa(QrLcts);
  //
  case RGDemonstrativo.ItemIndex of
    0:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrSums, DModG.MyPID_DB, [
        'SELECT ',
        'cN5, oN5, nN5,',
        'cN4, oN4, nN4,',
        'cN3, oN3, nN3,',
        'cN2, oN2, nN2,',
        'cN1, oN1, nN1,',
        'SUM(Credito) Credito, SUM(Debito) Debito',
        'FROM _fin_relat_013_1',
        SQL_Agrupar(),
        SQL_Ordenar(),
        '']);
      //
      MyObjects.frxDefineDataSets(frxFIN_RELAT_013_00, [
        DmodG.frxDsDono,
        frxDsSums
        ]);
      //
      MyObjects.frxMostra(frxFIN_RELAT_013_00, 'Movimento do Plano de Contas - Sint�tico');
    end;
    1:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLcts, DModG.MyPID_DB, [
        'SELECT * FROM _FIN_RELAT_013_1',
        SQL_Ordenar(),
        '']);
      //
      MyObjects.frxDefineDataSets(frxFIN_RELAT_013_01, [
        DmodG.frxDsDono,
        frxDsLcts
        ]);
      //
      MyObjects.frxMostra(frxFIN_RELAT_013_01, 'Movimento do Plano de Contas - Anal�tico');
    end;
    else Geral.MB_Erro('Relat�rio n�o implementado! AVISE A DERMATEK!');
  end;
end;

procedure TFmMovFin.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMovFin.DockTabSet1DockDrop(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer);
begin
  DockTabSet1.Visible := True;
end;

procedure TFmMovFin.DockTabSet1TabAdded(Sender: TObject);
begin
  DockTabSet1.Visible := DockTabSet1.Tabs.Count > 0;
end;

procedure TFmMovFin.DockTabSet1TabRemoved(Sender: TObject);
begin
  DockTabSet1.Visible := DockTabSet1.Tabs.Count > 0;
end;

procedure TFmMovFin.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
  Timer1.Enabled := True;
end;

procedure TFmMovFin.FormCreate(Sender: TObject);
var
  Ini, Fim: TDate;
begin
  FInCreation := False;
  FPreparou   := False;
  //
  Ini := Geral.PrimeiroDiaDoMes(Date);
  Fim := Geral.UltimoDiaDoMes(Date);
  //
  TPEmissIni.Date := Ini;
  TPEmissFim.Date := Fim;
  TPVctoIni.Date  := Ini;
  TPVctoFim.Date  := Fim;
  TPDocIni.Date   := Ini;
  TPDocFim.Date   := Fim;
  TPCompIni.Date  := Ini;
  TPCompFim.Date  := Fim;
  //
  QrLcts.Close;
  QrLcts.Database := DModG.MyPID_DB;
  //
  CGAgrup.Value        := 12;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmMovFin.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMovFin.frxFIN_RELAT_013_01GetValue(const VarName: string;
  var Value: Variant);
var
  Liga: String;  
begin
{
  if VarName = 'VARF_SINTETICO' then
    Value := RGDemonstrativo.ItemIndex = 0
  else
}
  if VarName = 'VARF_VISIVEL_1' then
    Value := Geral.IntInConjunto(16, CGAgrup.Value)
  else
  if VarName = 'VARF_VISIVEL_2' then
    Value := Geral.IntInConjunto(8, CGAgrup.Value)
  else
  if VarName = 'VARF_VISIVEL_3' then
    Value := Geral.IntInConjunto(4, CGAgrup.Value)
  else
  if VarName = 'VARF_VISIVEL_4' then
    Value := Geral.IntInConjunto(2, CGAgrup.Value)
  else
  if VarName = 'VARF_VISIVEL_5' then
    Value := Geral.IntInConjunto(1, CGAgrup.Value)
  else
  if VarName = 'VARF_MIN_GRUP' then
  begin
    Value := 0;
    if Geral.IntInConjunto(1, CGAgrup.Value) then
      Value := 5;
    if Geral.IntInConjunto(2, CGAgrup.Value) then
      Value := 4;
    if Geral.IntInConjunto(4, CGAgrup.Value) then
      Value := 3;
    if Geral.IntInConjunto(8, CGAgrup.Value) then
      Value := 2;
    if Geral.IntInConjunto(16, CGAgrup.Value) then
      Value := 1;
  end else
  if VarName = 'PERIODO' then
  begin
    Value := 'Per�odo ';
    if not CkEmissao.Checked and not CkVencto.Checked
    and not CkDataDoc.Checked then Value := 'N�o Definido';
    if CkEmissao.Checked and CkVencto.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkEmissao.Checked then
      Value := 'Emiss�o de '+
        FormatDateTime(VAR_FORMATDATE3, TPEmissIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPEmissFim.Date) + Liga;
    if (CkEmissao.Checked or CkVencto.Checked) and CkDataDoc.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkVencto.Checked then
      Value := Value + 'Vencimento de '+
        FormatDateTime(VAR_FORMATDATE3, TPVctoIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPVctoFim.Date) + Liga;
    if CkDataDoc.Checked then
      Value := Value + 'Docum. de '+
        FormatDateTime(VAR_FORMATDATE3, TPDocIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPDocFim.Date);
  end else
  if VarName = 'VAR_NOMEEMPRESA' then
    Value := dmkPF.EscolhaDe2Str(
      EdEmpresa.ValueVariant <> 0, CBEmpresa.Text, ' ** DIVERSAS **')
  else
end;

procedure TFmMovFin.QrLctsCalcFields(DataSet: TDataSet);
begin
  QrLctsCOMPENSADO_TXT.Value := Geral.FDT(QrLctsCompensado.Value, 3);
end;

procedure TFmMovFin.RecriaDockForms();
var
  i: Integer;
begin
  if FInCreation then
    Exit;
  FInCreation := True;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando tabelas');
  Screen.Cursor := crHourGlass;
  try
    for i := 0 to Screen.FormCount - MaxJan - 1 do
    begin
      if Screen.Forms[i] is TFm_Empresas then
        Screen.Forms[i].Destroy;
      if Screen.Forms[i] is TFm_Indi_Pags then
        Screen.Forms[i].Destroy;
      if Screen.Forms[i] is TFm_Pla_Ctas then
        Screen.Forms[i].Destroy;
    end;
    TFm_Empresas.CreateDockForm().ManualDock(DockTabSet1);
    TFm_Indi_Pags.CreateDockForm().ManualDock(DockTabSet1);
    TFm_Pla_Ctas.CreateDockForm().ManualDock(DockTabSet1);

    //
    FInCreation := False;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmMovFin.SQL_Agrupar(): String;
begin
  Result := '';
  if Geral.IntInConjunto(1, CGAgrup.Value) then
    Result := Result + ', cN5';
  if Geral.IntInConjunto(2, CGAgrup.Value) then
    Result := Result + ', cN4';
  if Geral.IntInConjunto(4, CGAgrup.Value) then
    Result := Result + ', cN3';
  if Geral.IntInConjunto(8, CGAgrup.Value) then
    Result := Result + ', cN2';
  if Geral.IntInConjunto(16, CGAgrup.Value) then
    Result := Result + ', cN1';
  if Length(Result) > 0 then
    Result := 'GROUP BY' + Copy(Result, 2);
end;

function TFmMovFin.SQL_Ordenar(): String;
begin
  Result := '';
  if Geral.IntInConjunto(1, CGAgrup.Value) then
    Result := Result + ', oN5, nN5, cN5';
  if Geral.IntInConjunto(2, CGAgrup.Value) then
    Result := Result + ', oN4, nN4, cN4';
  if Geral.IntInConjunto(4, CGAgrup.Value) then
    Result := Result + ', oN3, nN3, cN3';
  if Geral.IntInConjunto(8, CGAgrup.Value) then
    Result := Result + ', oN2, nN2, cN2';
  if Geral.IntInConjunto(16, CGAgrup.Value) then
    Result := Result + ', oN1, nN1, cN1';
  if Length(Result) > 0 then
    Result := 'ORDER BY' + Copy(Result, 2);
end;

procedure TFmMovFin.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  if FPreparou = False then
  begin
    Screen.Cursor := crHourGlass;
    try
      FPreparou := True;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas tempor�rias');
      //
      DModG.Tb_Empresas.Close;
      DModG.Tb_Empresas.Database := DModG.MyPID_DB;
      UnCreateGeral.RecriaTempTableNovo(ntrtt_Empresas, DModG.QrUpdPID1,
        False, 1, TAB_TMP_EMPRESAS);
      UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
      'DELETE FROM ' + TAB_TMP_EMPRESAS + ';',
      'INSERT INTO '  + TAB_TMP_EMPRESAS,
      'SELECT eci.CodCliInt, eci.CodEnti, eci.CodFilial, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, Nome) Nome, ',
      '0 Ativo',
      'FROM ' + TMeuDB + '.enticliint eci',
      'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=eci.CodEnti'
      , 'WHERE eci.TipoTabLct=1'
      ]);
      DModG.Tb_Empresas. Open;
      //
      DModG.Tb_Indi_Pags.Close;
      DModG.Tb_Indi_Pags.Database := DModG.MyPID_DB;
      UCriar.RecriaTempTableNovo(ntrttPesqESel, DModG.QrUpdPID1,
        False, 1, TAB_TMP_INDIPAGS);
      UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
      'DELETE FROM ' + TAB_TMP_INDIPAGS + ';',
      'INSERT INTO ' + TAB_TMP_INDIPAGS,
      'SELECT Codigo, Nome, 1 Ativo',
      'FROM ' + TMeuDB + '.indipag']);
      DModG.Tb_Indi_Pags. Open;
      //
      DModFin.Tb_Pla_Ctas.Close;
      DModFin.Tb_Pla_Ctas.Database := DModG.MyPID_DB;
      UCriarFin.RecriaTempTableNovo(ntrtt_PlaCta, DModG.QrUpdPID1,
        False, 1, TAB_TMP_PLA_CTAS);
      UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
      'DELETE FROM ' + TAB_TMP_PLA_CTAS + ';',

      'INSERT INTO ' + TAB_TMP_PLA_CTAS,
      'SELECT 5 Nivel,',
      'pla.OrdemLista, pla.Codigo, pla.Nome, ',
      '0, 0, "",',
      '0, 0, "",',
      '0, 0, "",',
      '0, 0, "",',
      //'0, 0, 0, 0, 0, 0, ',
      '1 Ativo',
      'FROM ' + TMeuDB + '.plano pla;',

      'INSERT INTO ' + TAB_TMP_PLA_CTAS,
      'SELECT 4 Nivel,',
      'pla.OrdemLista, pla.Codigo, pla.Nome, ',
      'cjt.OrdemLista, cjt.Codigo, cjt.Nome,',
      '0, 0, "",',
      '0, 0, "",',
      '0, 0, "",',
      //'0, 0, 0, 0, 0, 0, ',
      '1 Ativo',
      'FROM ' + TMeuDB + '.conjuntos cjt',
      'LEFT JOIN ' + TMeuDB + '.plano pla ON pla.Codigo=cjt.Plano;',

      'INSERT INTO ' + TAB_TMP_PLA_CTAS,
      'SELECT 3 Nivel,',
      'pla.OrdemLista, pla.Codigo, pla.Nome, ',
      'cjt.OrdemLista, cjt.Codigo, cjt.Nome,',
      'gru.OrdemLista, gru.Codigo, gru.Nome,',
      '0, 0, "",',
      '0, 0, "",',
      //'0, 0, 0, 0, 0, 0, ',
      '1 Ativo',
      'FROM ' + TMeuDB + '.grupos gru',
      'LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto',
      'LEFT JOIN ' + TMeuDB + '.plano pla ON pla.Codigo=cjt.Plano;',

      'INSERT INTO ' + TAB_TMP_PLA_CTAS,
      'SELECT 2 Nivel,',
      'pla.OrdemLista, pla.Codigo, pla.Nome, ',
      'cjt.OrdemLista, cjt.Codigo, cjt.Nome,',
      'gru.OrdemLista, gru.Codigo, gru.Nome,',
      'sgr.OrdemLista, sgr.Codigo, sgr.Nome,',
      '0, 0, "", ',
      //'0, 0, 0, 0, 0, 0, ',
      '1 Ativo',
      'FROM ' + TMeuDB + '.subgrupos sgr',
      'LEFT JOIN ' + TMeuDB + '.grupos gru ON gru.Codigo=sgr.grupo',
      'LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto',
      'LEFT JOIN ' + TMeuDB + '.plano pla ON pla.Codigo=cjt.Plano;',

      'INSERT INTO ' + TAB_TMP_PLA_CTAS,
      'SELECT 1 Nivel, ',
      'pla.OrdemLista, pla.Codigo, pla.Nome, ',
      'cjt.OrdemLista, cjt.Codigo, cjt.Nome,',
      'gru.OrdemLista, gru.Codigo, gru.Nome,',
      'sgr.OrdemLista, sgr.Codigo, sgr.Nome,',
      'cta.OrdemLista, cta.Codigo, cta.Nome,',
      //'0, 0, 0, 0, 0, 0, ',
      '1 Ativo',
      'FROM ' + TMeuDB + '.contas cta',
      'LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=cta.Subgrupo',
      'LEFT JOIN ' + TMeuDB + '.grupos gru ON gru.Codigo=sgr.grupo',
      'LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto',
      'LEFT JOIN ' + TMeuDB + '.plano pla ON pla.Codigo=cjt.Plano']);
      //
      DModFin.Tb_Pla_Ctas. Open;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      RecriaDockForms();
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.
