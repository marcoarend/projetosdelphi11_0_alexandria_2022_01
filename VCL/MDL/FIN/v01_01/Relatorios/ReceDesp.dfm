object FmReceDesp: TFmReceDesp
  Left = 339
  Top = 185
  Caption = 'FIN-RELAT-010 :: Relat'#243'rio de Receitas e Despesas'
  ClientHeight = 592
  ClientWidth = 686
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 686
    Height = 430
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 686
      Height = 430
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 0
        Top = 112
        Width = 686
        Height = 318
        ActivePage = TabSheet2
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Demostrativo de receitas e despesas'
          object PageControl3: TPageControl
            Left = 0
            Top = 0
            Width = 678
            Height = 290
            ActivePage = TabSheet5
            Align = alClient
            TabOrder = 0
            object TabSheet4: TTabSheet
              Caption = 'Geral'
              object Panel6: TPanel
                Left = 0
                Top = 0
                Width = 670
                Height = 113
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                ExplicitWidth = 673
                object CkNaoAgruparNada: TCheckBox
                  Left = 12
                  Top = 88
                  Width = 289
                  Height = 17
                  Caption = 'N'#227'o agrupar nada (visualiza'#231#227'o de todos lan'#231'amentos).'
                  TabOrder = 0
                  OnClick = CkNaoAgruparNadaClick
                end
                object GroupBox2: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 673
                  Height = 85
                  Align = alTop
                  Caption = ' Agrupamentos: '
                  TabOrder = 1
                  object CkAcordos: TCheckBox
                    Left = 12
                    Top = 20
                    Width = 501
                    Height = 17
                    Caption = 
                      'Individualizar acordos judiciais. (pode aumentar a quantidade de' +
                      ' registros)'
                    Checked = True
                    State = cbChecked
                    TabOrder = 0
                  end
                  object CkPeriodos: TCheckBox
                    Left = 12
                    Top = 40
                    Width = 501
                    Height = 17
                    Caption = 
                      'Individualizar per'#237'odos [mensal]. (pode aumentar a quantidade de' +
                      ' registros)'
                    Checked = True
                    State = cbChecked
                    TabOrder = 1
                  end
                  object CkTextos: TCheckBox
                    Left = 12
                    Top = 60
                    Width = 501
                    Height = 17
                    Caption = 
                      'Agrupar lan'#231'amentos com hist'#243'ricos iguais. (pode diminuir a quan' +
                      'tidade de registros)'
                    Checked = True
                    State = cbChecked
                    TabOrder = 2
                  end
                end
              end
              object PageControl2: TPageControl
                Left = 0
                Top = 113
                Width = 670
                Height = 149
                ActivePage = TabSheet6
                Align = alClient
                TabOrder = 1
                Visible = False
                ExplicitWidth = 673
                ExplicitHeight = 159
                object TabSheet6: TTabSheet
                  Caption = 'Cr'#233'ditos'
                  object DBGCreditos: TdmkDBGrid
                    Left = 0
                    Top = 0
                    Width = 666
                    Height = 81
                    Align = alClient
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'TbLct'
                        Title.Caption = 'Tabela'
                        Width = 16
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Data'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Controle'
                        Title.Caption = 'Lan'#231'to'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Credito'
                        Title.Caption = 'Cr'#233'dito'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMECON'
                        Title.Caption = 'Conta'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'MES'
                        Title.Caption = 'M'#234's'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMESGR'
                        Title.Caption = 'Sub-grupo'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMEGRU'
                        Title.Caption = 'Grupo'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Vencimento'
                        Visible = True
                      end>
                    Color = clWindow
                    DataSource = DsCreditos
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'TbLct'
                        Title.Caption = 'Tabela'
                        Width = 16
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Data'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Controle'
                        Title.Caption = 'Lan'#231'to'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Credito'
                        Title.Caption = 'Cr'#233'dito'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMECON'
                        Title.Caption = 'Conta'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'MES'
                        Title.Caption = 'M'#234's'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMESGR'
                        Title.Caption = 'Sub-grupo'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMEGRU'
                        Title.Caption = 'Grupo'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Vencimento'
                        Visible = True
                      end>
                  end
                  object Panel7: TPanel
                    Left = 0
                    Top = 81
                    Width = 666
                    Height = 48
                    Align = alBottom
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 1
                    object BtCreditos: TBitBtn
                      Left = 256
                      Top = 4
                      Width = 150
                      Height = 40
                      Caption = '&Lan'#231'amentos'
                      NumGlyphs = 2
                      TabOrder = 0
                      OnClick = BtCreditosClick
                    end
                  end
                end
                object TabSheet7: TTabSheet
                  Caption = 'D'#233'bitos'
                  ImageIndex = 1
                  object DBGDebitos: TdmkDBGrid
                    Left = 0
                    Top = 0
                    Width = 666
                    Height = 81
                    Align = alClient
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'TbLct'
                        Title.Caption = 'Tabela'
                        Width = 16
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DATA'
                        Title.Caption = 'Data'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Controle'
                        Title.Caption = 'Lan'#231'to'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Descricao'
                        Title.Caption = 'Hist'#243'rico'
                        Width = 140
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SERIE_DOC'
                        Title.Caption = 'Documento'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DEBITO'
                        Title.Caption = 'D'#233'bito'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMECON'
                        Title.Caption = 'Conta'
                        Width = 140
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMESGR'
                        Title.Caption = 'Sub-grupo'
                        Width = 140
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMEGRU'
                        Title.Caption = 'Grupo'
                        Width = 140
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COMPENSADO_TXT'
                        Title.Caption = 'Compensado'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'MES'
                        Title.Caption = 'M'#234's'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Vencimento'
                        Visible = True
                      end>
                    Color = clWindow
                    DataSource = DsDebitos
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'TbLct'
                        Title.Caption = 'Tabela'
                        Width = 16
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DATA'
                        Title.Caption = 'Data'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Controle'
                        Title.Caption = 'Lan'#231'to'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Descricao'
                        Title.Caption = 'Hist'#243'rico'
                        Width = 140
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'SERIE_DOC'
                        Title.Caption = 'Documento'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DEBITO'
                        Title.Caption = 'D'#233'bito'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMECON'
                        Title.Caption = 'Conta'
                        Width = 140
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMESGR'
                        Title.Caption = 'Sub-grupo'
                        Width = 140
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMEGRU'
                        Title.Caption = 'Grupo'
                        Width = 140
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'COMPENSADO_TXT'
                        Title.Caption = 'Compensado'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'MES'
                        Title.Caption = 'M'#234's'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Vencimento'
                        Visible = True
                      end>
                  end
                  object Panel8: TPanel
                    Left = 0
                    Top = 81
                    Width = 666
                    Height = 48
                    Align = alBottom
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 1
                    object BtDebitos: TBitBtn
                      Left = 256
                      Top = 4
                      Width = 150
                      Height = 40
                      Caption = '&Lan'#231'amentos'
                      NumGlyphs = 2
                      TabOrder = 0
                      OnClick = BtDebitosClick
                    end
                  end
                end
              end
            end
            object TabSheet5: TTabSheet
              Caption = 'Ordena'#231#227'o'
              ImageIndex = 1
              object LaAvisoUsuario: TLabel
                Left = 0
                Top = 0
                Width = 670
                Height = 32
                Align = alTop
                Caption = 
                  'O relat'#243'rio ser'#225' ordenado de acordo com a configura'#231#227'o da ordem ' +
                  'de cima para baixo '#13#10'das respectivas listas.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitWidth = 606
              end
              object LaAvisoDesenvolvedor: TLabel
                Left = 0
                Top = 32
                Width = 670
                Height = 32
                Align = alTop
                Caption = 'Caso mudar os textos do ListBox mudar '#13#10'tamb'#233'm na procedure.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                Visible = False
                ExplicitWidth = 277
              end
              object GBReceitas: TGroupBox
                Left = 0
                Top = 64
                Width = 330
                Height = 145
                Align = alLeft
                Caption = 'Receitas'
                TabOrder = 0
                object LBReceitas: TListBox
                  Left = 58
                  Top = 15
                  Width = 270
                  Height = 128
                  Align = alRight
                  Columns = 1
                  ItemHeight = 13
                  Items.Strings = (
                    'Ordem do grupo (cadastro de grupos)'
                    'Nome do grupo'
                    'Ordem do subgrupo (cadastro de subgrupos)'
                    'Nome do subgrupo'
                    'Ordem da conta (cadastro de contas)'
                    'Nome da conta'
                    'M'#234's de compet'#234'ncia'
                    'Data do lan'#231'amento')
                  TabOrder = 0
                end
                object BtReceUp: TBitBtn
                  Tag = 32
                  Left = 8
                  Top = 15
                  Width = 40
                  Height = 40
                  NumGlyphs = 2
                  TabOrder = 1
                  OnClick = BtReceUpClick
                end
                object BtReceDown: TBitBtn
                  Tag = 31
                  Left = 8
                  Top = 60
                  Width = 40
                  Height = 40
                  NumGlyphs = 2
                  TabOrder = 2
                  OnClick = BtReceDownClick
                end
              end
              object GBDespesas: TGroupBox
                Left = 330
                Top = 64
                Width = 330
                Height = 145
                Align = alLeft
                Caption = 'Despesas'
                TabOrder = 1
                object LBDespesas: TListBox
                  Left = 58
                  Top = 15
                  Width = 270
                  Height = 128
                  Align = alRight
                  Columns = 1
                  ItemHeight = 13
                  Items.Strings = (
                    'Ordem do grupo (cadastro de grupos)'
                    'Nome do grupo'
                    'Ordem do subgrupo (cadastro de subgrupos)'
                    'Nome do subgrupo'
                    'Ordem da conta (cadastro de contas)'
                    'Nome da conta'
                    'M'#234's de compet'#234'ncia'
                    'Data do lan'#231'amento')
                  TabOrder = 0
                end
                object BtDespDown: TBitBtn
                  Tag = 31
                  Left = 8
                  Top = 60
                  Width = 40
                  Height = 40
                  NumGlyphs = 2
                  TabOrder = 1
                  OnClick = BtDespDownClick
                end
                object BtDespUp: TBitBtn
                  Tag = 32
                  Left = 8
                  Top = 15
                  Width = 40
                  Height = 40
                  NumGlyphs = 2
                  TabOrder = 2
                  OnClick = BtDespUpClick
                end
              end
              object Panel11: TPanel
                Left = 0
                Top = 209
                Width = 670
                Height = 53
                Align = alBottom
                ParentBackground = False
                TabOrder = 2
                object BtCancela: TBitBtn
                  Tag = 329
                  Left = 50
                  Top = 6
                  Width = 40
                  Height = 40
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtCancelaClick
                end
                object BtSalvar: TBitBtn
                  Tag = 24
                  Left = 8
                  Top = 6
                  Width = 40
                  Height = 40
                  NumGlyphs = 2
                  TabOrder = 1
                  OnClick = BtSalvarClick
                end
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Hist'#243'rico de Conta (do plano de contas)'
          ImageIndex = 1
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 678
            Height = 290
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label4: TLabel
              Left = 8
              Top = 8
              Width = 131
              Height = 13
              Caption = 'Conta (do plano de contas):'
            end
            object LaUH: TLabel
              Left = 8
              Top = 48
              Width = 25
              Height = 13
              Caption = 'U.H.:'
            end
            object EdConta: TdmkEditCB
              Left = 8
              Top = 24
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBConta
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBConta: TdmkDBLookupComboBox
              Left = 52
              Top = 24
              Width = 481
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContas
              TabOrder = 1
              dmkEditCB = EdConta
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object CBUH: TDBLookupComboBox
              Left = 8
              Top = 65
              Width = 161
              Height = 21
              KeyField = 'Conta'
              ListField = 'Unidade'
              ListSource = DsUHs
              TabOrder = 2
              OnKeyDown = CBUHKeyDown
            end
            object CGValores: TdmkCheckGroup
              Left = 172
              Top = 48
              Width = 173
              Height = 41
              Caption = ' Valores: '
              Columns = 2
              ItemIndex = 1
              Items.Strings = (
                'Cr'#233'ditos'
                'Debitos')
              TabOrder = 3
              UpdType = utYes
              Value = 2
              OldValor = 0
            end
            object RGOrdem: TRadioGroup
              Left = 348
              Top = 48
              Width = 185
              Height = 41
              Caption = ' Ordena'#231#227'o: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Por M'#234's'
                'Por U.H.')
              TabOrder = 4
            end
          end
        end
        object TSCNAB: TTabSheet
          Caption = ' Retornos CNAB'
          ImageIndex = 2
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 678
            Height = 65
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox4: TGroupBox
              Left = 220
              Top = 0
              Width = 220
              Height = 65
              Align = alLeft
              Caption = ' Faixa de valores pagos: '
              TabOrder = 1
              object Label7: TLabel
                Left = 8
                Top = 16
                Width = 38
                Height = 13
                Caption = 'M'#237'nimo:'
              end
              object Label8: TLabel
                Left = 112
                Top = 16
                Width = 36
                Height = 13
                Caption = 'M'#225'ximo'
              end
              object EdValPagMin: TdmkEdit
                Left = 8
                Top = 32
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdValPagMinChange
              end
              object EdValPagMax: TdmkEdit
                Left = 112
                Top = 32
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '999.999.999,99'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 999999999.990000000000000000
                ValWarn = False
              end
            end
            object GroupBox5: TGroupBox
              Left = 0
              Top = 0
              Width = 220
              Height = 65
              Align = alLeft
              Caption = ' Faixa de valores dos t'#237'tulos: '
              TabOrder = 0
              object Label10: TLabel
                Left = 8
                Top = 16
                Width = 38
                Height = 13
                Caption = 'M'#237'nimo:'
              end
              object Label11: TLabel
                Left = 112
                Top = 16
                Width = 36
                Height = 13
                Caption = 'M'#225'ximo'
              end
              object EdValTitMin: TdmkEdit
                Left = 8
                Top = 32
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdValTitMinChange
              end
              object EdValTitMax: TdmkEdit
                Left = 112
                Top = 32
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '999.999.999,99'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 999999999.990000000000000000
                ValWarn = False
              end
            end
            object GroupBox6: TGroupBox
              Left = 440
              Top = 0
              Width = 238
              Height = 65
              Align = alClient
              Caption = ' Faixa de bloquetos: '
              TabOrder = 2
              object Label9: TLabel
                Left = 7
                Top = 16
                Width = 45
                Height = 13
                Caption = 'Bloqueto:'
              end
              object Label12: TLabel
                Left = 111
                Top = 16
                Width = 45
                Height = 13
                Caption = 'Bloqueto:'
              end
              object EdBloqIni: TdmkEdit
                Left = 7
                Top = 32
                Width = 101
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdBloqFim: TdmkEdit
                Left = 111
                Top = 32
                Width = 101
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
          object DBGrid1: TdmkDBGrid
            Left = 0
            Top = 65
            Width = 678
            Height = 225
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Step'
                Title.Caption = 'S'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Title.Caption = 'Bloqueto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Dta quit.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val. titul.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val. pago'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val. juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Arquivo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Carteira'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsRet
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBGrid1CellClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Step'
                Title.Caption = 'S'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Title.Caption = 'Bloqueto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Dta quit.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val. titul.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val. pago'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val. juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Arquivo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Carteira'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end>
          end
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 686
        Height = 112
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label1: TLabel
          Left = 12
          Top = 4
          Width = 101
          Height = 13
          Caption = 'Cliente (Condom'#237'nio):'
        end
        object EdEmpresa: TdmkEditCB
          Left = 12
          Top = 20
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 56
          Top = 20
          Width = 525
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object GroupBox1: TGroupBox
          Left = 12
          Top = 44
          Width = 349
          Height = 61
          Caption = 
            ' Per'#237'odo de pesquisa:  (data da ocorr'#234'ncia no caso de retorno CN' +
            'AB): '
          TabOrder = 2
          object Label2: TLabel
            Left = 12
            Top = 16
            Width = 55
            Height = 13
            Caption = 'Data inicial:'
          end
          object Label3: TLabel
            Left = 112
            Top = 16
            Width = 48
            Height = 13
            Caption = 'Data final:'
          end
          object TPDataIni1: TdmkEditDateTimePicker
            Left = 12
            Top = 32
            Width = 97
            Height = 21
            Date = 39702.000000000000000000
            Time = 0.829181134300597500
            TabOrder = 0
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPDataFim1: TdmkEditDateTimePicker
            Left = 112
            Top = 32
            Width = 97
            Height = 21
            Date = 39702.000000000000000000
            Time = 0.829181134300597500
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object GroupBox3: TGroupBox
          Left = 364
          Top = 44
          Width = 217
          Height = 61
          Caption = ' Data de quita'#231#227'o (Retorno CNAB): '
          TabOrder = 3
          object Label5: TLabel
            Left = 12
            Top = 16
            Width = 55
            Height = 13
            Caption = 'Data inicial:'
          end
          object Label6: TLabel
            Left = 112
            Top = 16
            Width = 48
            Height = 13
            Caption = 'Data final:'
          end
          object TPDataIni2: TdmkEditDateTimePicker
            Left = 12
            Top = 32
            Width = 97
            Height = 21
            Date = 39702.000000000000000000
            Time = 0.829181134300597500
            TabOrder = 0
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPDataFim2: TdmkEditDateTimePicker
            Left = 112
            Top = 32
            Width = 97
            Height = 21
            Date = 39702.000000000000000000
            Time = 0.829181134300597500
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 686
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 638
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 590
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 415
        Height = 32
        Caption = 'Relat'#243'rio de Receitas e Despesas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 415
        Height = 32
        Caption = 'Relat'#243'rio de Receitas e Despesas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 415
        Height = 32
        Caption = 'Relat'#243'rio de Receitas e Despesas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 478
    Width = 686
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 682
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 522
    Width = 686
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 540
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel10: TPanel
      Left = 2
      Top = 15
      Width = 538
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 272
    Top = 388
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 300
    Top = 388
  end
  object DataSource1: TDataSource
    Left = 152
    Top = 388
  end
  object QrDC: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDCCalcFields
    Left = 124
    Top = 388
    object QrDCNOMEMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMES'
      Size = 30
      Calculated = True
    end
    object QrDCGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrDCMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrDCDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrDCValor: TFloatField
      FieldName = 'Valor'
    end
    object QrDCNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrDCUNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 10
    end
    object QrDCControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object frxDC_Mes: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39702.836399733800000000
    ReportOptions.LastChange = 39702.836399733800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxReceDesp_GetValue
    Left = 284
    Top = 52
    Datasets = <
      item
        DataSet = frxDsDC
        DataSetName = 'frxDsDC'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Columns = 4
      ColumnWidth = 45.800000000000000000
      ColumnPositions.Strings = (
        '0'
        '45,80'
        '91,60'
        '137,40')
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 130.393778900000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 37.795300000000000000
          Width = 689.385826771653500000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 56.692949999999990000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Hist'#243'rico de Conta (do plano de contas)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 79.370130000000000000
          Width = 689.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NOMECOND]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 102.047310000000000000
          Width = 689.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]    -    UH: [VARF_UH]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Top = 120.944960000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818899999999000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'UH')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lan'#231'to')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 320.503937007874000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 260.031496062992100000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'UH')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 226.015748031496100000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lan'#231'to')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 493.606299210000000000
          Top = 120.944960000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 433.133858267716500000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'UH')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 399.118110236220500000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lan'#231'to')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 666.708661420000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 606.236220472441000000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'UH')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 572.220472440944900000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lan'#231'to')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007878900000000000
        Top = 238.110390000000000000
        Width = 173.102474000000000000
        Condition = 'frxDsDC."Genero"'
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 7.559059999999988000
          Width = 170.078740157480300000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDC."NOMECON"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 9.448818900000000000
        Top = 279.685220000000000000
        Width = 173.102474000000000000
        Condition = 'frxDsDC."Mez"'
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 170.078740157480300000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDC."NOMEMES"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 9.448818900000000000
        Top = 313.700990000000000000
        Width = 173.102474000000000000
        DataSet = frxDsDC
        DataSetName = 'frxDsDC'
        RowCount = 0
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 75.590551180000000000
          Height = 9.448818900000000000
          DataField = 'Valor'
          DataSet = frxDsDC
          DataSetName = 'frxDsDC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDC."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 60.472440944881890000
          Height = 9.448818900000000000
          DataField = 'UNIDADE'
          DataSet = frxDsDC
          DataSetName = 'frxDsDC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDC."UNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 34.015748030000000000
          Height = 9.448818900000000000
          DataField = 'Controle'
          DataSet = frxDsDC
          DataSetName = 'frxDsDC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDC."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 9.448818897637795000
        Top = 347.716760000000000000
        Width = 173.102474000000000000
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDC."Valor">)]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 60.472440944881890000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Todas UHs')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 34.015748031496060000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 9.448818897637795000
        Top = 381.732530000000000000
        Width = 173.102474000000000000
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 94.488250000000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tot. [frxDsDC."NOMECON"] ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDC."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 49.133858270000000000
        Top = 487.559370000000000000
        Width = 793.701300000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 691.653543307086600000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 1.889771100000000000
        Top = 211.653680000000000000
        Width = 173.102474000000000000
      end
      object Footer4: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338590000000000000
        Top = 415.748300000000000000
        Width = 173.102474000000000000
        object Memo201: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 94.488250000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL PESQUISADO:')
          ParentFont = False
          WordWrap = False
        end
        object Memo202: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDC."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsDC: TfrxDBDataset
    UserName = 'frxDsDC'
    CloseDataSource = False
    DataSet = QrDC
    BCDToCurrency = False
    
    Left = 180
    Top = 388
  end
  object frxDC_Uni: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39702.836399733800000000
    ReportOptions.LastChange = 39702.836399733800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxReceDesp_GetValue
    Left = 312
    Top = 52
    Datasets = <
      item
        DataSet = frxDsDC
        DataSetName = 'frxDsDC'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Columns = 4
      ColumnWidth = 45.800000000000000000
      ColumnPositions.Strings = (
        '0'
        '45,80'
        '91,60'
        '137,40')
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 130.393778900000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 37.795300000000000000
          Width = 689.385826771653500000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 56.692949999999990000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Hist'#243'rico de Conta (do plano de contas)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 79.370130000000000000
          Width = 689.385826771653500000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NOMECOND]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 102.047310000000000000
          Width = 689.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]    -    UH: [VARF_UH]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Top = 120.944960000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lan'#231'to')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 320.503937007874000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 260.031496060000000000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 226.015748031496100000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lan'#231'to')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 493.606299210000000000
          Top = 120.944960000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 433.133858270000000000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 399.118110236220500000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lan'#231'to')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 666.708661420000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 606.236220470000000000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 572.220472440944900000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lan'#231'to')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007878900000000000
        Top = 238.110390000000000000
        Width = 173.102474000000000000
        Condition = 'frxDsDC."Genero"'
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Top = 7.559059999999988000
          Width = 170.078740157480300000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDC."NOMECON"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 9.448818900000000000
        Top = 279.685220000000000000
        Width = 173.102474000000000000
        Condition = 'frxDsDC."Depto"'
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 170.078740160000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDC."UNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 9.448818900000000000
        Top = 313.700990000000000000
        Width = 173.102474000000000000
        DataSet = frxDsDC
        DataSetName = 'frxDsDC'
        RowCount = 0
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 75.590551180000000000
          Height = 9.448818900000000000
          DataField = 'Valor'
          DataSet = frxDsDC
          DataSetName = 'frxDsDC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDC."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 60.472440944881890000
          Height = 9.448818900000000000
          DataField = 'NOMEMES'
          DataSet = frxDsDC
          DataSetName = 'frxDsDC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDC."NOMEMES"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 34.015748031496060000
          Height = 9.448818900000000000
          DataField = 'Controle'
          DataSet = frxDsDC
          DataSetName = 'frxDsDC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDC."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 9.448818897637795000
        Top = 347.716760000000000000
        Width = 173.102474000000000000
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDC."Valor">)]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 60.472440940000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Todos meses')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 34.015748031496060000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 9.448818897637795000
        Top = 381.732530000000000000
        Width = 173.102474000000000000
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 94.488250000000000000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tot. [frxDsDC."NOMECON"] ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDC."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 49.133858270000000000
        Top = 487.559370000000000000
        Width = 793.701300000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 691.653543307086600000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 1.889771100000000000
        Top = 211.653680000000000000
        Width = 173.102474000000000000
      end
      object Footer4: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 11.338590000000000000
        Top = 415.748300000000000000
        Width = 173.102474000000000000
        object Memo201: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 94.488250000000000000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL PESQUISADO:')
          ParentFont = False
          WordWrap = False
        end
        object Memo202: TfrxMemoView
          AllowVectorExport = True
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 11.338590000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDC."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object QrUHs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Andar, Conta, Unidade '
      'FROM condimov'
      'WHERE Codigo=:P0'
      'ORDER BY Andar, Unidade')
    Left = 216
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUHsUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrUHsConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object DsUHs: TDataSource
    DataSet = QrUHs
    Left = 244
    Top = 388
  end
  object QrRet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cnab_lei'
      'WHERE entidade = 822'
      #10
      ''
      'AND QuitaData="2009-02-02"')
    Left = 124
    Top = 420
    object QrRetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRetBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrRetNossoNum: TWideStringField
      FieldName = 'NossoNum'
    end
    object QrRetSeuNum: TIntegerField
      FieldName = 'SeuNum'
    end
    object QrRetIDNum: TIntegerField
      FieldName = 'IDNum'
    end
    object QrRetOcorrCodi: TWideStringField
      FieldName = 'OcorrCodi'
      Size = 10
    end
    object QrRetOcorrData: TDateField
      FieldName = 'OcorrData'
    end
    object QrRetValTitul: TFloatField
      FieldName = 'ValTitul'
    end
    object QrRetValAbati: TFloatField
      FieldName = 'ValAbati'
    end
    object QrRetValDesco: TFloatField
      FieldName = 'ValDesco'
    end
    object QrRetValPago: TFloatField
      FieldName = 'ValPago'
    end
    object QrRetValJuros: TFloatField
      FieldName = 'ValJuros'
    end
    object QrRetValMulta: TFloatField
      FieldName = 'ValMulta'
    end
    object QrRetValJuMul: TFloatField
      FieldName = 'ValJuMul'
    end
    object QrRetMotivo1: TWideStringField
      FieldName = 'Motivo1'
      Size = 2
    end
    object QrRetMotivo2: TWideStringField
      FieldName = 'Motivo2'
      Size = 2
    end
    object QrRetMotivo3: TWideStringField
      FieldName = 'Motivo3'
      Size = 2
    end
    object QrRetMotivo4: TWideStringField
      FieldName = 'Motivo4'
      Size = 2
    end
    object QrRetMotivo5: TWideStringField
      FieldName = 'Motivo5'
      Size = 2
    end
    object QrRetQuitaData: TDateField
      FieldName = 'QuitaData'
    end
    object QrRetDiretorio: TIntegerField
      FieldName = 'Diretorio'
    end
    object QrRetArquivo: TWideStringField
      FieldName = 'Arquivo'
    end
    object QrRetItemArq: TIntegerField
      FieldName = 'ItemArq'
    end
    object QrRetStep: TSmallintField
      FieldName = 'Step'
      MaxValue = 1
    end
    object QrRetEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrRetCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrRetDevJuros: TFloatField
      FieldName = 'DevJuros'
    end
    object QrRetDevMulta: TFloatField
      FieldName = 'DevMulta'
    end
    object QrRetValOutro: TFloatField
      FieldName = 'ValOutro'
    end
    object QrRetValTarif: TFloatField
      FieldName = 'ValTarif'
    end
    object QrRetLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrRetDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrRetDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrRetUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrRetUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrRetDtaTarif: TDateField
      FieldName = 'DtaTarif'
    end
    object QrRetAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrRetAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrRetTamReg: TIntegerField
      FieldName = 'TamReg'
    end
    object QrRetID_Link: TLargeintField
      FieldName = 'ID_Link'
    end
  end
  object DsRet: TDataSource
    DataSet = QrRet
    Left = 152
    Top = 420
  end
  object PMCreditos: TPopupMenu
    Left = 292
    Top = 420
    object Alteracontadoslanamentosselecionados_Cred: TMenuItem
      Caption = 'Altera conta dos lan'#231'amentos selecionados'
      OnClick = Alteracontadoslanamentosselecionados_CredClick
    end
    object Alteralanamentoselecionado1: TMenuItem
      Caption = 'Altera lan'#231'amento selecionado'
      OnClick = Alteralanamentoselecionado1Click
    end
  end
  object PMDebitos: TPopupMenu
    Left = 324
    Top = 420
    object Alteracontadoslanamentosselecionados_Debi: TMenuItem
      Caption = 'Altera conta dos lan'#231'amentos selecionados'
      OnClick = Alteracontadoslanamentosselecionados_DebiClick
    end
    object Alteradescriodolanamentoselecionado1: TMenuItem
      Caption = 'Altera descri'#231#227'o do lan'#231'amento selecionado'
      OnClick = Alteradescriodolanamentoselecionado1Click
    end
    object Alteralanamentoselecionado2: TMenuItem
      Caption = 'Altera lan'#231'amento selecionado'
      OnClick = Alteralanamentoselecionado2Click
    end
  end
  object QrLct: TMySQLQuery
    Database = Dmod.MyDB
    Left = 480
    Top = 28
    object QrLctData: TDateField
      FieldName = 'Data'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrLctEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrLctContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrLctCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLctDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrLctDescoControle: TIntegerField
      FieldName = 'DescoControle'
    end
    object QrLctUnidade: TIntegerField
      FieldName = 'Unidade'
    end
    object QrLctNFVal: TFloatField
      FieldName = 'NFVal'
    end
    object QrLctAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrLctExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLctUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLctUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLctSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLctDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrLctMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrLctMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrLctCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrLctTipoCH: TSmallintField
      FieldName = 'TipoCH'
    end
    object QrLctAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrLctReparcel: TIntegerField
      FieldName = 'Reparcel'
    end
    object QrLctID_Quit: TIntegerField
      FieldName = 'ID_Quit'
    end
    object QrLctAtrelado: TIntegerField
      FieldName = 'Atrelado'
    end
    object QrLctAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLctPagMul: TFloatField
      FieldName = 'PagMul'
    end
    object QrLctPagJur: TFloatField
      FieldName = 'PagJur'
    end
    object QrLctSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
    end
    object QrLctMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrLctProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrLctCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrLctIndiPag: TIntegerField
      FieldName = 'IndiPag'
    end
  end
  object QrCarteiras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM carteiras'
      'ORDER BY Nome')
    Left = 556
    Top = 36
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 100
    end
    object QrCarteirasContato1: TWideStringField
      FieldName = 'Contato1'
      Size = 100
    end
    object QrCarteirasInicial: TFloatField
      FieldName = 'Inicial'
    end
    object QrCarteirasBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCarteirasID: TWideStringField
      FieldName = 'ID'
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteirasID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Size = 50
    end
    object QrCarteirasSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrCarteirasEmCaixa: TFloatField
      FieldName = 'EmCaixa'
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteirasPagRec: TIntegerField
      FieldName = 'PagRec'
    end
    object QrCarteirasDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCarteirasTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCarteirasAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrCarteirasConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCarteirasCheque1: TIntegerField
      FieldName = 'Cheque1'
    end
    object QrCarteirasAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrCarteirasContab: TWideStringField
      FieldName = 'Contab'
    end
    object QrCarteirasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCarteirasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCarteirasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCarteirasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCarteirasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCarteirasOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCarteirasForneceN: TSmallintField
      FieldName = 'ForneceN'
    end
    object QrCarteirasFuturoC: TFloatField
      FieldName = 'FuturoC'
    end
    object QrCarteirasFuturoD: TFloatField
      FieldName = 'FuturoD'
    end
    object QrCarteirasFuturoS: TFloatField
      FieldName = 'FuturoS'
    end
    object QrCarteirasExclusivo: TSmallintField
      FieldName = 'Exclusivo'
    end
    object QrCarteirasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCarteirasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCarteirasRecebeBloq: TSmallintField
      FieldName = 'RecebeBloq'
    end
    object QrCarteirasEntiDent: TIntegerField
      FieldName = 'EntiDent'
    end
  end
  object QrDebitos: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDebitosCalcFields
    SQL.Strings = (
      'SELECT * FROM _mod_cond_debi_1_')
    Left = 560
    Top = 156
    object QrDebitosCOMPENSADO_TXT: TWideStringField
      FieldName = 'COMPENSADO_TXT'
      Size = 8
    end
    object QrDebitosDATA: TWideStringField
      FieldName = 'DATA'
      Size = 8
    end
    object QrDebitosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDebitosDEBITO: TFloatField
      FieldName = 'DEBITO'
    end
    object QrDebitosNOTAFISCAL: TLargeintField
      FieldName = 'NOTAFISCAL'
    end
    object QrDebitosSERIECH: TWideStringField
      FieldName = 'SERIECH'
      Size = 10
    end
    object QrDebitosDOCUMENTO: TFloatField
      FieldName = 'DOCUMENTO'
    end
    object QrDebitosMEZ: TLargeintField
      FieldName = 'MEZ'
      Required = True
    end
    object QrDebitosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrDebitosNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrDebitosNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrDebitosNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrDebitosITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrDebitosMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 7
      Calculated = True
    end
    object QrDebitosSERIE_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_DOC'
      Size = 30
      Calculated = True
    end
    object QrDebitosNF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NF_TXT'
      Size = 30
      Calculated = True
    end
    object QrDebitosMES2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES2'
      Size = 5
      Calculated = True
    end
    object QrDebitosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDebitosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrDebitosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDebitosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrDebitosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrDebitosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrDebitosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrDebitosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrDebitosTbLct: TWideStringField
      FieldName = 'TbLct'
      Size = 1
    end
    object QrDebitosGRU_OL: TIntegerField
      FieldName = 'GRU_OL'
    end
    object QrDebitosCON_OL: TIntegerField
      FieldName = 'CON_OL'
    end
    object QrDebitosSGR_OL: TIntegerField
      FieldName = 'SGR_OL'
    end
    object QrDebitosNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
  end
  object DsDebitos: TDataSource
    DataSet = QrDebitos
    Left = 588
    Top = 156
  end
  object frxDsDebitos: TfrxDBDataset
    UserName = 'frxDsDebitos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'COMPENSADO_TXT=COMPENSADO_TXT'
      'DATA=DATA'
      'Descricao=Descricao'
      'DEBITO=DEBITO'
      'NOTAFISCAL=NOTAFISCAL'
      'SERIECH=SERIECH'
      'DOCUMENTO=DOCUMENTO'
      'MEZ=MEZ'
      'Compensado=Compensado'
      'NOMECON=NOMECON'
      'NOMESGR=NOMESGR'
      'NOMEGRU=NOMEGRU'
      'ITENS=ITENS'
      'MES=MES'
      'SERIE_DOC=SERIE_DOC'
      'NF_TXT=NF_TXT'
      'MES2=MES2'
      'Controle=Controle'
      'Sub=Sub'
      'Carteira=Carteira'
      'Cartao=Cartao'
      'Vencimento=Vencimento'
      'Sit=Sit'
      'Genero=Genero'
      'Tipo=Tipo'
      'TbLct=TbLct'
      'GRU_OL=GRU_OL'
      'CON_OL=CON_OL'
      'SGR_OL=SGR_OL'
      'NO_FORNECE=NO_FORNECE')
    DataSet = QrDebitos
    BCDToCurrency = False
    
    Left = 620
    Top = 156
  end
  object frxDsCreditos: TfrxDBDataset
    UserName = 'frxDsCreditos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Mez=Mez'
      'Credito=Credito'
      'NOMECON=NOMECON'
      'NOMESGR=NOMESGR'
      'NOMEGRU=NOMEGRU'
      'MES=MES'
      'NOMECON_2=NOMECON_2'
      'SubPgto1=SubPgto1'
      'Controle=Controle'
      'Sub=Sub'
      'Carteira=Carteira'
      'Cartao=Cartao'
      'Vencimento=Vencimento'
      'Compensado=Compensado'
      'Sit=Sit'
      'Genero=Genero'
      'Tipo=Tipo'
      'TbLct=TbLct'
      'Data=Data'
      'GRU_OL=GRU_OL'
      'CON_OL=CON_OL'
      'SGR_OL=SGR_OL')
    DataSet = QrCreditos
    BCDToCurrency = False
    
    Left = 620
    Top = 184
  end
  object DsCreditos: TDataSource
    DataSet = QrCreditos
    Left = 588
    Top = 184
  end
  object QrCreditos: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCreditosCalcFields
    SQL.Strings = (
      'SELECT * FROM _mod_cond_cred_1_')
    Left = 560
    Top = 184
    object QrCreditosMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrCreditosCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrCreditosNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrCreditosNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrCreditosNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrCreditosMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 7
      Calculated = True
    end
    object QrCreditosNOMECON_2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECON_2'
      Size = 255
      Calculated = True
    end
    object QrCreditosSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrCreditosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCreditosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrCreditosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrCreditosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrCreditosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrCreditosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrCreditosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrCreditosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCreditosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCreditosTbLct: TWideStringField
      FieldName = 'TbLct'
      Size = 1
    end
    object QrCreditosData: TDateField
      FieldName = 'Data'
    end
    object QrCreditosGRU_OL: TIntegerField
      FieldName = 'GRU_OL'
    end
    object QrCreditosCON_OL: TIntegerField
      FieldName = 'CON_OL'
    end
    object QrCreditosSGR_OL: TIntegerField
      FieldName = 'SGR_OL'
    end
  end
  object QrResumo: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrResumoCalcFields
    SQL.Strings = (
      'SELECT SUM(Credito) Credito, -SUM(Debito) Debito'
      'FROM _MOD_COND_RESUMO_1_;'
      '')
    Left = 560
    Top = 212
    object QrResumoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrResumoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrResumoSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      Calculated = True
    end
    object QrResumoFINAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FINAL'
      Calculated = True
    end
  end
  object frxDsResumo: TfrxDBDataset
    UserName = 'frxDsResumo'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Credito=Credito'
      'Debito=Debito'
      'SALDO=SALDO'
      'FINAL=FINAL')
    DataSet = QrResumo
    BCDToCurrency = False
    
    Left = 620
    Top = 212
  end
  object frxDsSaldoA: TfrxDBDataset
    UserName = 'frxDsSaldoA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Inicial=Inicial'
      'SALDO=SALDO'
      'TOTAL=TOTAL')
    DataSet = QrSaldoA
    BCDToCurrency = False
    
    Left = 620
    Top = 240
  end
  object QrSaldoA: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSaldoACalcFields
    SQL.Strings = (
      'SELECT * FROM _MOD_COND_SALDOA_1_'
      'ORDER BY GRU_OL, NOMEGRU, SGR_OL,'
      'NOMESGR, CON_OL, NOMECON, Mez, Data;')
    Left = 560
    Top = 240
    object QrSaldoAInicial: TFloatField
      FieldName = 'Inicial'
    end
    object QrSaldoASALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrSaldoATOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL'
      Calculated = True
    end
  end
  object frxReceDesp: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39702.836399733800000000
    ReportOptions.LastChange = 39702.836399733800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxReceDesp_GetValue
    Left = 256
    Top = 52
    Datasets = <
      item
        DataSet = frxDsCreditos
        DataSetName = 'frxDsCreditos'
      end
      item
        DataSet = frxDsDebitos
        DataSetName = 'frxDsDebitos'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end
      item
        DataSet = frxDsResumo
        DataSetName = 'frxDsResumo'
      end
      item
        DataSet = frxDsSaldoA
        DataSetName = 'frxDsSaldoA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 120.944960000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 56.692949999999990000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'DEMONSTRATIVO DE RECEITAS E DESPESAS NO PER'#205'ODO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NOMECOND]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 102.047310000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 257.008040000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsCreditos."NOMEGRU"'
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCreditos."NOMEGRU"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valores em R$')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DataSet = frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 294.803340000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsCreditos."NOMESGR"'
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCreditos."NOMESGR"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DataSet = frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 332.598640000000000000
        Width = 793.701300000000000000
        DataSet = frxDsCreditos
        DataSetName = 'frxDsCreditos'
        RowCount = 0
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          DataField = 'NOMECON_2'
          DataSet = frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCreditos."NOMECON_2"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataField = 'Credito'
          DataSet = frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCreditos."Credito"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DataField = 'MES'
          DataSet = frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCreditos."MES"]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 370.393940000000000000
        Width = 793.701300000000000000
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCreditos."Credito">)]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DataSet = frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 408.189240000000000000
        Width = 793.701300000000000000
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de [frxDsCreditos."NOMEGRU"] ')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCreditos."Credito">)]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 548.031850000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsDebitos."NOMEGRU"'
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 253.228339130000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."NOMEGRU"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 687.874460000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 34.015748031496060000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Width = 45.354338030000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Baixa')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 627.401980000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Nota fiscal')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 143.622118030000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor')
          ParentFont = False
        end
      end
      object GroupHeader5: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 585.827150000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsDebitos."NOMESGR"'
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 253.228339130000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDebitos."NOMESGR"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 687.874460000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 34.015748031496060000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Width = 45.354338030000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 627.401980000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 143.622118030000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 661.417750000000000000
        Width = 793.701300000000000000
        DataSet = frxDsDebitos
        DataSetName = 'frxDsDebitos'
        RowCount = 0
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 253.228363540000000000
          Height = 15.118120000000000000
          DataField = 'Descricao'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDebitos."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 687.874460000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataField = 'DEBITO'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDebitos."DEBITO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 143.622115590000000000
          Height = 15.118120000000000000
          DataField = 'NO_FORNECE'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDebitos."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'COMPENSADO_TXT'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."COMPENSADO_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataField = 'SERIE_DOC'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."SERIE_DOC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 627.401980000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DataField = 'NF_TXT'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."NF_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 34.015748031496060000
          Height = 15.118120000000000000
          DataField = 'MES2'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDebitos."MES2"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 737.008350000000000000
        Width = 793.701300000000000000
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 687.874460000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 253.228339130000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sub-total de [frxDsDebitos."NOMESGR"]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 34.015748031496060000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Width = 45.354338030000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 627.401980000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 143.622118030000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter5: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456690470000000000
        Top = 774.803650000000000000
        Width = 793.701300000000000000
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 665.197280000000000000
          Width = 90.708720000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 589.606680000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de [frxDsDebitos."NOMEGRU"] ')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 49.133858270000000000
        Top = 1065.827460000000000000
        Width = 793.701300000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 136.063080000000000000
        Top = 907.087200000000000000
        Width = 793.701300000000000000
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 41.574830000000020000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'RESUMO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 60.472480000000010000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo Anterior:')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 60.472480000000010000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSaldoA."TOTAL"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 75.590600000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de receitas:')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 75.590600000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."Credito"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 90.708719999999970000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de despesas:')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 105.826840000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo do per'#237'odo selecionado:')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 90.708719999999970000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."Debito"]')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 105.826840000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."SALDO"]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 120.944959999999900000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SALDO NO FINAL DO PER'#205'ODO:')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 120.944959999999900000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."FINAL"]')
          ParentFont = False
        end
      end
      object GroupHeader9: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 623.622450000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsDebitos."NOMECON"'
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 253.228339130000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  [frxDsDebitos."NOMECON"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 687.874460000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 34.015748031496060000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Width = 45.354338030000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 627.401980000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 143.622118030000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter9: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 699.213050000000000000
        Width = 793.701300000000000000
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 687.874460000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 253.228339130000000000
          Height = 15.118120000000000000
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  Sub-total de [frxDsDebitos."NOMECON"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 34.015748031496060000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Width = 45.354338030000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo198: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo199: TfrxMemoView
          AllowVectorExport = True
          Left = 627.401980000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 143.622118030000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015770000000000000
        Top = 200.315090000000000000
        Width = 793.701300000000000000
        object Memo200: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Top = 11.338590000000010000
          Width = 676.535870000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'RECEITAS')
          ParentFont = False
        end
      end
      object Footer4: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015770000000000000
        Top = 445.984540000000000000
        Width = 793.701300000000000000
        object Memo201: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 7.559059999999988000
          Width = 521.575140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL RECEITAS:')
          ParentFont = False
        end
        object Memo202: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 7.559059999999988000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCreditos."Credito">,MasterData2)]')
          ParentFont = False
        end
      end
      object Header4: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 502.677490000000000000
        Width = 793.701300000000000000
        object Memo203: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Width = 676.535870000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'DESPESAS')
          ParentFont = False
        end
      end
      object Footer6: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 823.937540000000000000
        Width = 793.701300000000000000
        object Memo204: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 521.575140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DESPESAS:')
          ParentFont = False
        end
        object Memo205: TfrxMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">,MasterData3)]')
          ParentFont = False
        end
      end
    end
  end
end
