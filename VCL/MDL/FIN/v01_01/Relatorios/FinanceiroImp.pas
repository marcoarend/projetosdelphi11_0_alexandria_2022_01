unit FinanceiroImp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.DBGrids, frxClass, frxRich, frxBarcode, Data.DB, mySQLDbTables, frxDBSet,
  UnDmkEnums, frxChBox;

type
  TFmFinanceiroImp = class(TForm)
    frxBarCodeObject1: TfrxBarCodeObject;
    frxRichObject1: TfrxRichObject;
    frxCopiaDC: TfrxReport;
    QrCopiaCH: TmySQLQuery;
    QrCopiaCHDebito: TFloatField;
    QrCopiaCHBanco1: TWideStringField;
    QrCopiaCHAgencia1: TWideStringField;
    QrCopiaCHConta1: TWideStringField;
    QrCopiaCHSerieCH: TWideStringField;
    QrCopiaCHDocumento: TWideStringField;
    QrCopiaCHData: TDateField;
    QrCopiaCHDescricao: TWideStringField;
    QrCopiaCHFornecedor: TIntegerField;
    QrCopiaCHNOMEFORNECE: TWideStringField;
    QrCopiaCHNOMEBANCO: TWideStringField;
    QrCopiaCHControle: TWideStringField;
    QrCopiaCHCRUZADO_SIM: TBooleanField;
    QrCopiaCHCRUZADO_NAO: TBooleanField;
    QrCopiaCHVISADO_SIM: TBooleanField;
    QrCopiaCHVISADO_NAO: TBooleanField;
    QrCopiaCHTipoCH: TSmallintField;
    QrCopiaCHProtocolo: TIntegerField;
    QrCopiaCHEANTem: TLargeintField;
    QrCopiaCHEAN128: TWideStringField;
    frxDsCopiaCH: TfrxDBDataset;
    frxDsCopiaVC: TfrxDBDataset;
    QrCopiaVC: TmySQLQuery;
    QrCopiaVCDebito: TFloatField;
    QrCopiaVCBanco1: TWideStringField;
    QrCopiaVCAgencia1: TWideStringField;
    QrCopiaVCConta1: TWideStringField;
    QrCopiaVCSerieCH: TWideStringField;
    QrCopiaVCDocumento: TWideStringField;
    QrCopiaVCData: TDateField;
    QrCopiaVCFornecedor: TIntegerField;
    QrCopiaVCNOMEFORNECE: TWideStringField;
    QrCopiaVCNOMEBANCO: TWideStringField;
    QrCopiaVCControle: TWideStringField;
    QrCopiaVCCRUZADO_SIM: TBooleanField;
    QrCopiaVCCRUZADO_NAO: TBooleanField;
    QrCopiaVCVISADO_SIM: TBooleanField;
    QrCopiaVCVISADO_NAO: TBooleanField;
    QrCopiaVCTipoCH: TSmallintField;
    QrCopiaVCGenero: TIntegerField;
    QrCopiaVCProtocolo: TIntegerField;
    QrCopiaVCEANTem: TLargeintField;
    QrCopiaVCEAN128: TWideStringField;
    frxCopiaVC: TfrxReport;
    frxCheckBoxObject1: TfrxCheckBoxObject;
    frxCopiaCH: TfrxReport;
    procedure QrCopiaCHCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure QrCopiaVCCalcFields(DataSet: TDataSet);
    procedure frxCopiaVCGetValue(const VarName: string; var Value: Variant);
    procedure frxCopiaDCGetValue(const VarName: string; var Value: Variant);
    procedure frxCopiaCHGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    FTextoCopia: String;
    FGeneroCH, FEntCliInt: Integer;
    function  AbreQrCopiaCheques(Controle: Integer; TabLct: String): Boolean;
    procedure frxGetValue(const VarName: string; var Value: Variant);
  public
    { Public declarations }
    procedure ImprimeCopiaDC(Controle, Genero, EntCliInt: Integer; TabLct: String);
    procedure ImprimeCopiaVC(QrLct: TmySQLQuery; GradeLct: TDBGrid; EntCliInt: Integer; TabLct: String);
    procedure ImprimeCopiaCH(Controle, Genero, EntCliInt: Integer; TabLct: String);
  end;

var
  FmFinanceiroImp: TFmFinanceiroImp;

implementation

uses Module, dmkGeral, UMySQLModule, UnMyObjects, UnDmkProcFunc, ModuleGeral,
  ModuleFin, DmkDAC_PF;

{$R *.dfm}

{ TFmFinanceiroImp }

function TFmFinanceiroImp.AbreQrCopiaCheques(Controle: Integer;
  TabLct: String): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCopiaCH, Dmod.MyDB, [
    'SELECT lan.Debito, LPAD(car.Banco1, 3, "0") Banco1, ',
    'LPAD(car.Agencia1, 4, "0") Agencia1, car.Conta1, ',
    'lan.SerieCH, IF(lan.Documento=0, "", ',
    'LPAD(lan.Documento, 6, "0")) Documento, lan.Data, ',
    'lan.Descricao, lan.Fornecedor, IF(lan.Fornecedor=0, "", ',
    'CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial ',
    'ELSE frn.Nome END) NOMEFORNECE, ',
    'ban.Nome NOMEBANCO, LPAD(lan.Controle, 6, "0") Controle, ',
    'lan.TipoCH, lan.Protocolo,  ',
    'IF(lan.Protocolo <> 0, 1, 0) EANTem  ',
    'FROM ' + TabLct + ' lan ',
    'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ',
    'LEFT JOIN entidades frn ON frn.Codigo=lan.Fornecedor ',
    'LEFT JOIN bancos ban ON ban.Codigo=car.Banco1 ',
    ' ',
    'WHERE lan.Controle=' + Geral.FF0(Controle),
    '']);
  //
  Result := QrCopiaCH.RecordCount > 0;
end;

procedure TFmFinanceiroImp.FormCreate(Sender: TObject);
begin
  frxDsCopiaCH.DataSet := QrCopiaCH;
  frxDsCopiaVC.DataSet := QrCopiaVC;
end;

procedure TFmFinanceiroImp.frxCopiaCHGetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

procedure TFmFinanceiroImp.frxCopiaDCGetValue(const VarName: string;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

procedure TFmFinanceiroImp.frxCopiaVCGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_EXTENSO') = 0 then Value :=
    dmkPF.ExtensoMoney(Geral.FFT(
    QrCopiaVCDebito.Value, 2, siPositivo))
  else if AnsiCompareText(VarName, 'VARF_HOJE') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6, QrCopiaVCData.Value)
  else if AnsiCompareText(VarName, 'VARF_CONTRATO') = 0 then
  begin
    DModFin.ReopenContasEnt(QrCopiaVCGenero.Value, FEntCliInt);
    //
    Value :=  DModFin.QrContasEntCntrDebCta.Value;
  end else if VarName = 'VARF_DESCRICAO' then
    Value := FTextoCopia
  else if AnsiCompareText(VarName, 'VARF_TEM_CODBARRA') = 0 then
    Value := QrCopiaVCEANTem.Value > 0
end;

procedure TFmFinanceiroImp.frxGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_EXTENSO') = 0 then
    Value := dmkPF.ExtensoMoney(Geral.FFT(QrCopiaCHDebito.Value, 2, siPositivo))
  else if AnsiCompareText(VarName, 'VARF_HOJE') = 0 then
    Value := FormatDateTime(VAR_FORMATDATE6, QrCopiaCHData.Value)
  else if AnsiCompareText(VarName, 'VARF_CONTRATO') = 0 then
  begin
    DModFin.ReopenContasEnt(FGeneroCH, FEntCliInt);
    //
    Value := DModFin.QrContasEntCntrDebCta.Value;
  end
  else if AnsiCompareText(VarName, 'VARF_TEM_CODBARRA') = 0 then
    Value := QrCopiaCHEANTem.Value > 0
end;

procedure TFmFinanceiroImp.ImprimeCopiaCH(Controle, Genero, EntCliInt: Integer;
  TabLct: String);
var
  MasterData: TfrxMasterData;
begin
  FEntCliInt := EntCliInt;
  FGeneroCH  := Genero;
  //
  DModG.ReopenEndereco(FEntCliInt);
  //
  if AbreQrCopiaCheques(Controle, TabLct) then
  begin
    MasterData := frxCopiaCH.FindObject('GH1') as TfrxMasterData;
    //
    if MasterData <> nil then
      MasterData.DataSet := frxDsCopiaCH;
    //
    MyObjects.frxDefineDataSets(frxCopiaCH, [
      frxDsCopiaCH,
      DModG.frxDsEndereco
      ]);
    MyObjects.frxMostra(frxCopiaCH, 'C�pia de cheque');
  end;
end;

procedure TFmFinanceiroImp.ImprimeCopiaDC(Controle, Genero, EntCliInt: Integer;
  TabLct: String);
var
  MasterData: TfrxMasterData;
begin
  FEntCliInt := EntCliInt;
  FGeneroCH  := Genero;
  //
  DModG.ReopenEndereco(FEntCliInt);
  //
  if AbreQrCopiaCheques(Controle, TabLct) then
  begin
    MasterData := frxCopiaDC.FindObject('GH1') as TfrxMasterData;
    //
    if MasterData <> nil then
      MasterData.DataSet := frxDsCopiaCH;
    //
    MyObjects.frxDefineDataSets(frxCopiaDC, [
      frxDsCopiaCH,
      DModG.frxDsEndereco
      ]);
    MyObjects.frxMostra(frxCopiaDC, 'C�pia de d�bito em conta');
  end;
end;

procedure TFmFinanceiroImp.ImprimeCopiaVC(QrLct: TmySQLQuery; GradeLct: TDBGrid;
  EntCliInt: Integer; TabLct: String);
var
  MasterData: TfrxMasterData;
  Documento: Double;
  I, Errados: Integer;
  SerieCH, Controles, Sep1: String;
begin
  FEntCliInt := EntCliInt;
  Errados    := 0;
  Documento  := QrLct.FieldByName('Documento').AsFloat;
  SerieCH    := QrLct.FieldByName('SerieCH').AsString;
  //
  if GradeLct.SelectedRows.Count > 0 then
  begin
    FTextoCopia := '';
    Sep1        := '';
    //
    with GradeLct.DataSource.DataSet do
    begin
      for i:= 0 to GradeLct.SelectedRows.Count-1 do
      begin
        //GotoBookmark(GradeLct.SelectedRows.Items[i]);
        GotoBookmark(GradeLct.SelectedRows.Items[i]);
        if (Documento <> QrLct.FieldByName('Documento').AsFloat) or
           (SerieCH <> QrLct.FieldByName('SerieCH').AsString) then
        begin
          Errados := Errados + 1;
        end else
        begin
          Controles   := Controles + Sep1 + dmkPF.FFP(QrLct.FieldByName('Controle').AsFloat, 0);
          FTextoCopia := FTextoCopia + Sep1 + QrLct.FieldByName('Descricao').AsString;
          //
          Sep1 := ', ';
        end;
      end;
    end;
  end else
  begin
    FTextoCopia := QrLct.FieldByName('Descricao').AsString;
    Controles   := Geral.FF0(QrLct.FieldByName('Controle').AsInteger);
  end;
  if Errados > 0 then
  begin
    Geral.MB_Aviso('Foram localizados ' + Geral.FF0(Errados) +
      ' lan�amentos que n�o tem a S�rie ou Documento igual ao primeiro lan�amento' +
      'do itens selecionados! C�pia cancelada!');
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCopiaVC, Dmod.MyDB, [
      'SELECT SUM(lan.Debito) Debito, LPAD(car.Banco1, 3, "0") ',
      'Banco1, LPAD(car.Agencia1, 4, "0") Agencia1, car.Conta1, ',
      'lan.SerieCH, IF(lan.Documento=0, "", ',
      'LPAD(lan.Documento, 6, "0")) Documento, lan.Data, ',
      'lan.Fornecedor, IF(lan.Fornecedor=0, "", ',
      'CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial ',
      'ELSE frn.Nome END) NOMEFORNECE, ',
      'ban.Nome NOMEBANCO, LPAD(lan.Controle, 6, "0") Controle, ',
      'lan.TipoCH, lan.Genero, lan.Protocolo, ',
      'IF(lan.Protocolo <> 0, 1, 0) EANTem ',
      'FROM ' + TabLct + ' lan ',
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ',
      'LEFT JOIN entidades frn ON frn.Codigo=lan.Fornecedor ',
      'LEFT JOIN bancos ban ON ban.Codigo=car.Banco1 ',
      'WHERE lan.Controle in (' + Controles + ') ',
      'GROUP BY lan.SerieCH, lan.Documento ',
      '']);
    //
    DModG.ReopenEndereco(FEntCliInt);
    //
    if MasterData <> nil then
      MasterData.DataSet := frxDsCopiaVC;
    //
    MyObjects.frxDefineDataSets(frxCopiaVC, [
      frxDsCopiaVC,
      DModG.frxDsEndereco
      ]);
    MyObjects.frxMostra(frxCopiaVC, 'C�pia de cheque (M�ltiplos lan�amentos)');
  end;
end;

procedure TFmFinanceiroImp.QrCopiaCHCalcFields(DataSet: TDataSet);
begin
  QrCopiaCHVISADO_SIM.Value  := Geral.IntInConjunto(1, QrCopiaCHTipoCH.Value);
  QrCopiaCHCRUZADO_SIM.Value := Geral.IntInConjunto(2, QrCopiaCHTipoCH.Value);
  QrCopiaCHVISADO_NAO.Value  := not QrCopiaCHVISADO_SIM.Value;
  QrCopiaCHCRUZADO_NAO.Value := not QrCopiaCHCRUZADO_SIM.Value;
  QrCopiaCHEAN128.Value      := '>I<' + Geral.FFN(QrCopiaCHProtocolo.Value, 11);
end;

procedure TFmFinanceiroImp.QrCopiaVCCalcFields(DataSet: TDataSet);
begin
  QrCopiaVCVISADO_SIM.Value  := Geral.IntInConjunto(1, QrCopiaVCTipoCH.Value);
  QrCopiaVCCRUZADO_SIM.Value := Geral.IntInConjunto(2, QrCopiaVCTipoCH.Value);
  QrCopiaVCVISADO_NAO.Value  := not QrCopiaVCVISADO_SIM.Value;
  QrCopiaVCCRUZADO_NAO.Value := not QrCopiaVCCRUZADO_SIM.Value;
  QrCopiaVCEAN128.Value      := '>I<' + Geral.FFN(QrCopiaVCProtocolo.Value, 11);
end;

end.
