object FmFinanceiroImp: TFmFinanceiroImp
  Left = 0
  Top = 0
  Caption = 'FmFinanceiroImp'
  ClientHeight = 300
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 180
    Top = 60
  end
  object frxRichObject1: TfrxRichObject
    Left = 208
    Top = 60
  end
  object frxCopiaDC: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39094.004039166700000000
    ReportOptions.LastChange = 39094.004039166700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VARF_TEM_CODBARRA> then'
      '  begin'
      
        '    BarCodeI.Visible := True;                                   ' +
        '             '
      '  end else begin'
      
        '    BarCodeI.Visible := False;                                  ' +
        '              '
      '  end;            '
      'end.')
    OnGetValue = frxCopiaDCGetValue
    Left = 236
    Top = 60
    Datasets = <
      item
        DataSet = frxDsCopiaCH
        DataSetName = 'frxDsCopiaCH'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 667.086719130000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        DataSet = frxDsCopiaCH
        DataSetName = 'frxDsCopiaCH'
        RowCount = 0
        object frxShapeView1: TfrxShapeView
          Left = 226.771800000000000000
          Top = 71.811070000000000000
          Width = 532.913730000000000000
          Height = 294.803340000000000000
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Left = 230.551330000000000000
          Top = 83.149660000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Banco')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 275.905690000000000000
          Top = 83.149660000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag'#234'ncia')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 355.275820000000000000
          Top = 83.149660000000000000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta corrente')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 480.000310000000000000
          Top = 83.149660000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 230.551330000000000000
          Top = 105.826840000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataField = 'Banco1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaCH."Banco1"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 275.905690000000000000
          Top = 105.826840000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'Agencia1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaCH."Agencia1"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 351.496290000000000000
          Top = 105.826840000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DataField = 'Conta1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaCH."Conta1"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 480.000310000000000000
          Top = 105.826840000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DataField = 'SerieCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaCH."SerieCH"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 627.401980000000000000
          Top = 79.370130000000000000
          Height = 41.574830000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Memo11: TfrxMemoView
          Left = 631.181510000000000000
          Top = 83.149660000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'R$')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 631.181510000000000000
          Top = 102.047310000000000000
          Width = 113.385900000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '$[frxDsCopiaCH."Debito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 230.551330000000000000
          Top = 143.622140000000000000
          Width = 514.016080000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Foi debitado na conta corrente acima o valor de  [VARF_EXTENSO]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 230.551330000000000000
          Top = 181.417440000000000000
          Width = 514.016080000000000000
          Height = 52.913395590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            
              'referente ao pagamento do fornecedor [frxDsCopiaCH."NOMEFORNECE"' +
              '].')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 317.480520000000000000
          Top = 272.126160000000000000
          Width = 427.086890000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."CIDADE"], [VARF_HOJE]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo16: TfrxMemoView
          Left = 230.551330000000000000
          Top = 272.126160000000000000
          Width = 219.212740000000000000
          Height = 56.692950000000000000
          DataField = 'NOMEBANCO'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCopiaCH."NOMEBANCO"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 453.543600000000000000
          Top = 328.819110000000000000
          Width = 291.023810000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          Left = 79.370130000000000000
          Top = 374.173470000000000000
          Width = 680.315400000000000000
          Height = 170.078850000000000000
          Frame.Width = 0.500000000000000000
        end
        object Shape3: TfrxShapeView
          Left = 525.354670000000100000
          Top = 374.173470000000000000
          Width = 234.330860000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape4: TfrxShapeView
          Left = 291.023810000000000000
          Top = 374.173470000000000000
          Width = 234.330860000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape5: TfrxShapeView
          Left = 79.370130000000000000
          Top = 374.173470000000000000
          Width = 211.653680000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo18: TfrxMemoView
          Left = 86.929190000000000000
          Top = 404.409710000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Do banco: [frxDsCopiaCH."NOMEBANCO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 86.929190000000000000
          Top = 427.086890000000000000
          Width = 665.197280000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Utilizado para: [frxDsCopiaCH."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 79.370130000000000000
          Top = 468.661720000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ADMINISTRADORA')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 343.937230000000000000
          Top = 468.661720000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  CAIXA')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 343.937230000000000000
          Top = 487.559370000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  C/C')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 343.937230000000000000
          Top = 506.457020000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  Valor')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 343.937230000000000000
          Top = 525.354670000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 434.645950000000000000
          Top = 468.661720000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 434.645950000000000000
          Top = 487.559370000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 434.645950000000000000
          Top = 506.457020000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DataField = 'Debito'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCopiaCH."Debito"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 434.645950000000000000
          Top = 525.354670000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 525.354670000000000000
          Top = 468.661720000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lan'#231'amento ratificado por')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 525.354670000000000000
          Top = 487.559370000000000000
          Width = 234.330860000000000000
          Height = 56.692950000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 86.929190000000000000
          Top = 377.953000000000000000
          Width = 200.315090000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'LAN'#199'AMENTO N'#186' [frxDsCopiaCH."Controle"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 294.803340000000000000
          Top = 377.953000000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Contrato')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 529.134200000000000000
          Top = 377.953000000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CRUZADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 37.795300000000000000
          Top = 26.456710000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'PIA DE D'#201'BITO EM CONTA')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 230.551330000000000000
          Top = 45.354360000000000000
          Width = 525.354670000000000000
          Height = 26.456685590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Utilizado para: [frxDsCopiaCH."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 343.937230000000000000
          Top = 377.953000000000000000
          Width = 177.637910000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONTRATO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape7: TfrxShapeView
          Left = 30.236240000000000000
          Top = 45.354360000000000000
          Width = 192.755920160000000000
          Height = 321.259854720000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo21: TfrxMemoView
          Left = 71.811070000000000000
          Top = 52.913420000000000000
          Width = 143.622140000000000000
          Height = 287.244280000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object BarCodeI: TfrxBarCodeView
          Left = 37.795300000000000000
          Top = 64.252010000000000000
          Width = 23.039270000000000000
          Height = 178.000000000000000000
          Visible = False
          BarType = bcCodeEAN128B
          DataField = 'EAN128'
          Rotation = 90
          ShowText = False
          TestLine = False
          Text = 'L00012345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object ShNoCoBarra: TfrxShapeView
          Left = 30.236240000000000000
          Top = 374.173470000000000000
          Width = 49.133890000000000000
          Height = 170.078850000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo22: TfrxMemoView
          Left = 52.913420000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 162.519790000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaCH."Protocolo"]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 30.236240000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 162.519790000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo:')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
    end
  end
  object QrCopiaCH: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCopiaCHCalcFields
    SQL.Strings = (
      'SELECT lan.Debito, LPAD(car.Banco1, 3, "0") Banco1,'
      'LPAD(car.Agencia1, 4, "0") Agencia1, car.Conta1,'
      'lan.SerieCH, IF(lan.Documento=0, "",'
      'LPAD(lan.Documento, 6, "0")) Documento, lan.Data,'
      'lan.Descricao, lan.Fornecedor, IF(lan.Fornecedor=0, "",'
      'CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial'
      'ELSE frn.Nome END) NOMEFORNECE,'
      'ban.Nome NOMEBANCO, LPAD(lan.Controle, 6, "0") Controle,'
      'lan.TipoCH, lan.Protocolo, '
      'IF(lan.Protocolo <> 0, 1, 0) EANTem '
      'FROM lct0001a lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades frn ON frn.Codigo=lan.Fornecedor'
      'LEFT JOIN bancos ban ON ban.Codigo=car.Banco1'
      ''
      'WHERE lan.Controle=:P0')
    Left = 44
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCopiaCHDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrCopiaCHBanco1: TWideStringField
      FieldName = 'Banco1'
      Size = 8
    end
    object QrCopiaCHAgencia1: TWideStringField
      FieldName = 'Agencia1'
      Size = 8
    end
    object QrCopiaCHConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCopiaCHSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrCopiaCHDocumento: TWideStringField
      FieldName = 'Documento'
      Size = 53
    end
    object QrCopiaCHData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrCopiaCHDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrCopiaCHFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrCopiaCHNOMEFORNECE: TWideStringField
      FieldName = 'NOMEFORNECE'
      Size = 100
    end
    object QrCopiaCHNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrCopiaCHControle: TWideStringField
      FieldName = 'Controle'
      Required = True
      Size = 11
    end
    object QrCopiaCHCRUZADO_SIM: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'CRUZADO_SIM'
      Calculated = True
    end
    object QrCopiaCHCRUZADO_NAO: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'CRUZADO_NAO'
      Calculated = True
    end
    object QrCopiaCHVISADO_SIM: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'VISADO_SIM'
      Calculated = True
    end
    object QrCopiaCHVISADO_NAO: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'VISADO_NAO'
      Calculated = True
    end
    object QrCopiaCHTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrCopiaCHProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrCopiaCHEANTem: TLargeintField
      FieldName = 'EANTem'
      Required = True
    end
    object QrCopiaCHEAN128: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EAN128'
      Calculated = True
    end
  end
  object frxDsCopiaCH: TfrxDBDataset
    UserName = 'frxDsCopiaCH'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Debito=Debito'
      'Banco1=Banco1'
      'Agencia1=Agencia1'
      'Conta1=Conta1'
      'SerieCH=SerieCH'
      'Documento=Documento'
      'Data=Data'
      'Descricao=Descricao'
      'Fornecedor=Fornecedor'
      'NOMEFORNECE=NOMEFORNECE'
      'NOMEBANCO=NOMEBANCO'
      'Controle=Controle'
      'CRUZADO_SIM=CRUZADO_SIM'
      'CRUZADO_NAO=CRUZADO_NAO'
      'VISADO_SIM=VISADO_SIM'
      'VISADO_NAO=VISADO_NAO'
      'TipoCH=TipoCH'
      'Protocolo=Protocolo'
      'EANTem=EANTem'
      'EAN128=EAN128')
    DataSet = QrCopiaCH
    BCDToCurrency = False
    Left = 72
    Top = 168
  end
  object frxDsCopiaVC: TfrxDBDataset
    UserName = 'frxDsCopiaVC'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Debito=Debito'
      'Banco1=Banco1'
      'Agencia1=Agencia1'
      'Conta1=Conta1'
      'SerieCH=SerieCH'
      'Documento=Documento'
      'Data=Data'
      'Fornecedor=Fornecedor'
      'NOMEFORNECE=NOMEFORNECE'
      'NOMEBANCO=NOMEBANCO'
      'Controle=Controle'
      'CRUZADO_SIM=CRUZADO_SIM'
      'CRUZADO_NAO=CRUZADO_NAO'
      'VISADO_SIM=VISADO_SIM'
      'VISADO_NAO=VISADO_NAO'
      'TipoCH=TipoCH'
      'Genero=Genero'
      'Protocolo=Protocolo'
      'EANTem=EANTem'
      'EAN128=EAN128')
    DataSet = QrCopiaVC
    BCDToCurrency = False
    Left = 128
    Top = 168
  end
  object QrCopiaVC: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCopiaVCCalcFields
    SQL.Strings = (
      'SELECT SUM(lan.Debito) Debito, LPAD(car.Banco1, 3, "0") '
      'Banco1, LPAD(car.Agencia1, 4, "0") Agencia1, car.Conta1,'
      'lan.SerieCH, IF(lan.Documento=0, "",'
      'LPAD(lan.Documento, 6, "0")) Documento, lan.Data,'
      'lan.Fornecedor, IF(lan.Fornecedor=0, "",'
      'CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial'
      'ELSE frn.Nome END) NOMEFORNECE,'
      'ban.Nome NOMEBANCO, LPAD(lan.Controle, 6, "0") Controle,'
      'lan.TipoCH, lan.Genero, lan.Protocolo, '
      'IF(lan.Protocolo <> 0, 1, 0) EANTem '
      'FROM lct0001a lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades frn ON frn.Codigo=lan.Fornecedor'
      'LEFT JOIN bancos ban ON ban.Codigo=car.Banco1'
      'WHERE lan.Controle in (3456)'
      'GROUP BY lan.SerieCH, lan.Documento')
    Left = 100
    Top = 168
    object QrCopiaVCDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrCopiaVCBanco1: TWideStringField
      FieldName = 'Banco1'
      Size = 8
    end
    object QrCopiaVCAgencia1: TWideStringField
      FieldName = 'Agencia1'
      Size = 8
    end
    object QrCopiaVCConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCopiaVCSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrCopiaVCDocumento: TWideStringField
      FieldName = 'Documento'
      Size = 53
    end
    object QrCopiaVCData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrCopiaVCFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrCopiaVCNOMEFORNECE: TWideStringField
      FieldName = 'NOMEFORNECE'
      Size = 100
    end
    object QrCopiaVCNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrCopiaVCControle: TWideStringField
      FieldName = 'Controle'
      Required = True
      Size = 11
    end
    object QrCopiaVCCRUZADO_SIM: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'CRUZADO_SIM'
      Calculated = True
    end
    object QrCopiaVCCRUZADO_NAO: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'CRUZADO_NAO'
      Calculated = True
    end
    object QrCopiaVCVISADO_SIM: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'VISADO_SIM'
      Calculated = True
    end
    object QrCopiaVCVISADO_NAO: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'VISADO_NAO'
      Calculated = True
    end
    object QrCopiaVCTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrCopiaVCGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCopiaVCProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrCopiaVCEANTem: TLargeintField
      FieldName = 'EANTem'
      Required = True
    end
    object QrCopiaVCEAN128: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EAN128'
      Calculated = True
    end
  end
  object frxCopiaVC: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39094.004039166700000000
    ReportOptions.LastChange = 41048.590854305560000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VARF_TEM_CODBARRA> then'
      '  begin'
      
        '    BarCodeI.Visible := True;                                   ' +
        '             '
      '  end else begin'
      
        '    BarCodeI.Visible := False;                                  ' +
        '              '
      '  end;            '
      'end.')
    OnGetValue = frxCopiaVCGetValue
    Left = 264
    Top = 60
    Datasets = <
      item
        DataSet = frxDsCopiaVC
        DataSetName = 'frxDsCopiaVC'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 667.086719130000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        DataSet = frxDsCopiaVC
        DataSetName = 'frxDsCopiaVC'
        RowCount = 0
        object Shape1: TfrxShapeView
          Left = 226.771800000000000000
          Top = 71.811070000000000000
          Width = 532.913730000000000000
          Height = 294.803340000000000000
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Left = 230.551330000000000000
          Top = 83.149660000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Banco')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 275.905690000000000000
          Top = 83.149660000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag'#234'ncia')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 355.275820000000000000
          Top = 83.149660000000000000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta corrente')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 498.897960000000000000
          Top = 83.149660000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 544.252320000000000000
          Top = 83.149660000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Cheque n'#186)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 230.551330000000000000
          Top = 105.826840000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataField = 'Banco1'
          DataSet = frxDsCopiaVC
          DataSetName = 'frxDsCopiaVC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaVC."Banco1"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 275.905690000000000000
          Top = 105.826840000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'Agencia1'
          DataSet = frxDsCopiaVC
          DataSetName = 'frxDsCopiaVC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaVC."Agencia1"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 351.496290000000000000
          Top = 105.826840000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DataField = 'Conta1'
          DataSet = frxDsCopiaVC
          DataSetName = 'frxDsCopiaVC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaVC."Conta1"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 498.897960000000000000
          Top = 105.826840000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataField = 'SerieCH'
          DataSet = frxDsCopiaVC
          DataSetName = 'frxDsCopiaVC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaVC."SerieCH"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 544.252320000000000000
          Top = 105.826840000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'Documento'
          DataSet = frxDsCopiaVC
          DataSetName = 'frxDsCopiaVC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaVC."Documento"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 627.401980000000000000
          Top = 79.370130000000000000
          Height = 41.574830000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Memo11: TfrxMemoView
          Left = 631.181510000000000000
          Top = 83.149660000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'R$')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 631.181510000000000000
          Top = 102.047310000000000000
          Width = 113.385900000000000000
          Height = 22.677180000000000000
          DataSet = frxDsCopiaVC
          DataSetName = 'frxDsCopiaVC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '$[frxDsCopiaVC."Debito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 230.551330000000000000
          Top = 143.622140000000000000
          Width = 514.016080000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Pague por este cheque a quantia de [VARF_EXTENSO]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 230.551330000000000000
          Top = 181.417440000000000000
          Width = 514.016080000000000000
          Height = 52.913395590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'a [frxDsCopiaVC."NOMEFORNECE"] ou a sua ordem.')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 317.480520000000000000
          Top = 272.126160000000000000
          Width = 427.086890000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."CIDADE"], [VARF_HOJE]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo16: TfrxMemoView
          Left = 230.551330000000000000
          Top = 272.126160000000000000
          Width = 219.212740000000000000
          Height = 56.692950000000000000
          DataField = 'NOMEBANCO'
          DataSet = frxDsCopiaVC
          DataSetName = 'frxDsCopiaVC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCopiaVC."NOMEBANCO"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 453.543600000000000000
          Top = 328.819110000000000000
          Width = 291.023810000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          Left = 79.370130000000000000
          Top = 374.173470000000000000
          Width = 680.315400000000000000
          Height = 170.078850000000000000
          Frame.Width = 0.500000000000000000
        end
        object Shape3: TfrxShapeView
          Left = 525.354670000000100000
          Top = 374.173470000000000000
          Width = 234.330860000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape4: TfrxShapeView
          Left = 291.023810000000000000
          Top = 374.173470000000000000
          Width = 234.330860000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape5: TfrxShapeView
          Left = 79.370130000000000000
          Top = 374.173470000000000000
          Width = 211.653680000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo18: TfrxMemoView
          Left = 86.929190000000000000
          Top = 404.409710000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Do banco: [frxDsCopiaVC."NOMEBANCO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 86.929190000000000000
          Top = 423.307360000000000000
          Width = 665.197280000000000000
          Height = 45.354335590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Utilizado para: [VARF_DESCRICAO]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 79.370130000000000000
          Top = 468.661720000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ADMINISTRADORA')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 343.937230000000000000
          Top = 468.661720000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  CAIXA')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 343.937230000000000000
          Top = 487.559370000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  C/C')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 343.937230000000000000
          Top = 506.457020000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  TAL'#195'O')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 343.937230000000000000
          Top = 525.354670000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 434.645950000000000000
          Top = 468.661720000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 434.645950000000000000
          Top = 487.559370000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 434.645950000000000000
          Top = 506.457020000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DataField = 'Debito'
          DataSet = frxDsCopiaVC
          DataSetName = 'frxDsCopiaVC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCopiaVC."Debito"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 434.645950000000000000
          Top = 525.354670000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 525.354670000000000000
          Top = 468.661720000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CHEQUE ASSINADO POR')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 525.354670000000000000
          Top = 487.559370000000000000
          Width = 234.330860000000000000
          Height = 56.692950000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 86.929190000000000000
          Top = 377.953000000000000000
          Width = 200.315090000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'LAN'#199'AMENTO N'#186' [frxDsCopiaVC."Controle"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 294.803340000000000000
          Top = 377.953000000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VISADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox1: TfrxCheckBoxView
          Left = 374.173470000000000000
          Top = 377.953000000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_SIM'
          DataSet = frxDsCopiaVC
          DataSetName = 'frxDsCopiaVC'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo38: TfrxMemoView
          Left = 396.850650000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox2: TfrxCheckBoxView
          Left = 449.764070000000000000
          Top = 377.953000000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_NAO'
          DataSet = frxDsCopiaVC
          DataSetName = 'frxDsCopiaVC'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 472.441250000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 529.134200000000000000
          Top = 377.953000000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CRUZADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox3: TfrxCheckBoxView
          Left = 608.504330000000000000
          Top = 377.953000000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_SIM'
          DataSet = frxDsCopiaVC
          DataSetName = 'frxDsCopiaVC'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo41: TfrxMemoView
          Left = 631.181510000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox4: TfrxCheckBoxView
          Left = 684.094930000000000000
          Top = 377.953000000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_NAO'
          DataSet = frxDsCopiaVC
          DataSetName = 'frxDsCopiaVC'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo42: TfrxMemoView
          Left = 706.772110000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 226.771800000000000000
          Top = 45.354360000000000000
          Width = 532.913730000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'C'#211'PIA DE CHEQUE')
          ParentFont = False
        end
        object Shape7: TfrxShapeView
          Left = 30.236240000000000000
          Top = 45.354360000000000000
          Width = 192.755920160000000000
          Height = 321.259854720000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo21: TfrxMemoView
          Left = 71.811070000000000000
          Top = 52.913420000000000000
          Width = 143.622140000000000000
          Height = 287.244280000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'C A N H O T O')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object BarCodeI: TfrxBarCodeView
          Left = 37.795300000000000000
          Top = 64.252010000000000000
          Width = 23.039270000000000000
          Height = 178.000000000000000000
          Visible = False
          BarType = bcCodeEAN128B
          DataField = 'EAN128'
          DataSet = frxDsCopiaVC
          DataSetName = 'frxDsCopiaVC'
          Rotation = 270
          ShowText = False
          TestLine = False
          Text = 'L00012345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object ShNoCoBarra: TfrxShapeView
          Left = 30.236240000000000000
          Top = 374.173470000000000000
          Width = 49.133890000000000000
          Height = 170.078850000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo22: TfrxMemoView
          Left = 52.913420000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 162.519790000000000000
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaVC."Protocolo"]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 30.236240000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 162.519790000000000000
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo:')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
    end
  end
  object frxCheckBoxObject1: TfrxCheckBoxObject
    Left = 151
    Top = 60
  end
  object frxCopiaCH: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39094.004039166700000000
    ReportOptions.LastChange = 41048.558544189800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VARF_TEM_CODBARRA> then'
      '  begin'
      
        '    BarCodeI.Visible := True;                                   ' +
        '             '
      '  end else begin'
      
        '    BarCodeI.Visible := False;                                  ' +
        '              '
      '  end;            '
      'end.')
    OnGetValue = frxCopiaCHGetValue
    Left = 292
    Top = 60
    Datasets = <
      item
        DataSet = frxDsCopiaCH
        DataSetName = 'frxDsCopiaCH'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 667.086719130000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        DataSet = frxDsCopiaCH
        DataSetName = 'frxDsCopiaCH'
        RowCount = 0
        object Shape1: TfrxShapeView
          Left = 226.771800000000000000
          Top = 71.811070000000000000
          Width = 532.913730000000000000
          Height = 294.803340000000000000
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Left = 230.551330000000000000
          Top = 83.149660000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Banco')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 275.905690000000000000
          Top = 83.149660000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag'#234'ncia')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 355.275820000000000000
          Top = 83.149660000000000000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta corrente')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 498.897960000000000000
          Top = 83.149660000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 544.252320000000000000
          Top = 83.149660000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Cheque n'#186)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 230.551330000000000000
          Top = 105.826840000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataField = 'Banco1'
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaCH."Banco1"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 275.905690000000000000
          Top = 105.826840000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'Agencia1'
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaCH."Agencia1"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 351.496290000000000000
          Top = 105.826840000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DataField = 'Conta1'
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaCH."Conta1"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 498.897960000000000000
          Top = 105.826840000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataField = 'SerieCH'
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaCH."SerieCH"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 544.252320000000000000
          Top = 105.826840000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'Documento'
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaCH."Documento"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 627.401980000000000000
          Top = 79.370130000000000000
          Height = 41.574830000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Memo11: TfrxMemoView
          Left = 631.181510000000000000
          Top = 83.149660000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'R$')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 631.181510000000000000
          Top = 102.047310000000000000
          Width = 113.385900000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '$[frxDsCopiaCH."Debito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 230.551330000000000000
          Top = 143.622140000000000000
          Width = 514.016080000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Pague por este cheque a quantia de [VARF_EXTENSO]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 230.551330000000000000
          Top = 181.417440000000000000
          Width = 514.016080000000000000
          Height = 52.913395590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'a [frxDsCopiaCH."NOMEFORNECE"] ou a sua ordem.')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 317.480520000000000000
          Top = 272.126160000000000000
          Width = 427.086890000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."CIDADE"], [VARF_HOJE]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo16: TfrxMemoView
          Left = 230.551330000000000000
          Top = 272.126160000000000000
          Width = 219.212740000000000000
          Height = 56.692950000000000000
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCopiaCH."NOMEBANCO"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 453.543600000000000000
          Top = 328.819110000000000000
          Width = 291.023810000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          Left = 79.370130000000000000
          Top = 374.173470000000000000
          Width = 680.315400000000000000
          Height = 170.078850000000000000
          Frame.Width = 0.500000000000000000
        end
        object Shape3: TfrxShapeView
          Left = 525.354670000000000000
          Top = 374.173470000000000000
          Width = 234.330860000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape4: TfrxShapeView
          Left = 291.023810000000000000
          Top = 374.173470000000000000
          Width = 234.330860000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
        end
        object Shape5: TfrxShapeView
          Left = 79.370130000000000000
          Top = 374.173470000000000000
          Width = 211.653680000000000000
          Height = 26.456710000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo18: TfrxMemoView
          Left = 86.929190000000000000
          Top = 404.409710000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Do banco: [frxDsCopiaCH."NOMEBANCO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 86.929190000000000000
          Top = 427.086890000000000000
          Width = 665.197280000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Utilizado para: [frxDsCopiaCH."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 79.370130000000000000
          Top = 468.661720000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ADMINISTRADORA')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 343.937230000000000000
          Top = 468.661720000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  CAIXA')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 343.937230000000000000
          Top = 487.559370000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  C/C')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 343.937230000000000000
          Top = 506.457020000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  TAL'#195'O')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 343.937230000000000000
          Top = 525.354670000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 434.645950000000000000
          Top = 468.661720000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 434.645950000000000000
          Top = 487.559370000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 434.645950000000000000
          Top = 506.457020000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DataField = 'Debito'
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCopiaCH."Debito"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 434.645950000000000000
          Top = 525.354670000000100000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 525.354670000000000000
          Top = 468.661720000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CHEQUE ASSINADO POR')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 525.354670000000000000
          Top = 487.559370000000000000
          Width = 234.330860000000000000
          Height = 56.692950000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 86.929190000000000000
          Top = 377.953000000000000000
          Width = 200.315090000000000000
          Height = 18.897650000000000000
          DataField = 'Controle'
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCopiaCH."Controle"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 294.803340000000000000
          Top = 377.953000000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VISADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox1: TfrxCheckBoxView
          Left = 374.173470000000000000
          Top = 377.953000000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_SIM'
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo38: TfrxMemoView
          Left = 396.850650000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox2: TfrxCheckBoxView
          Left = 449.764070000000000000
          Top = 377.953000000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_NAO'
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 472.441250000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 529.134200000000000000
          Top = 377.953000000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CRUZADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox3: TfrxCheckBoxView
          Left = 608.504330000000000000
          Top = 377.953000000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_SIM'
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo41: TfrxMemoView
          Left = 631.181510000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox4: TfrxCheckBoxView
          Left = 684.094930000000000000
          Top = 377.953000000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_NAO'
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo42: TfrxMemoView
          Left = 706.772110000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 37.795300000000000000
          Top = 26.456710000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'PIA DE CHEQUE')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 230.551330000000000000
          Top = 45.354360000000000000
          Width = 525.354670000000000000
          Height = 26.456685590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Utilizado para: [frxDsCopiaCH."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Shape7: TfrxShapeView
          Left = 30.236240000000000000
          Top = 45.354360000000000000
          Width = 192.755920160000000000
          Height = 321.259854720000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo21: TfrxMemoView
          Left = 71.811070000000000000
          Top = 52.913420000000000000
          Width = 143.622140000000000000
          Height = 287.244280000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'C A N H O T O')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object BarCodeI: TfrxBarCodeView
          Left = 37.795300000000000000
          Top = 64.252010000000000000
          Width = 23.039270000000000000
          Height = 178.000000000000000000
          Visible = False
          BarType = bcCodeEAN128B
          DataField = 'EAN128'
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          Rotation = 90
          ShowText = False
          TestLine = False
          Text = 'L00012345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object ShNoCoBarra: TfrxShapeView
          Left = 30.236240000000000000
          Top = 374.173470000000000000
          Width = 49.133890000000000000
          Height = 170.078850000000000000
          Frame.Width = 0.100000000000000000
        end
        object Memo22: TfrxMemoView
          Left = 52.913420000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 162.519790000000000000
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaCH."Protocolo"]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 30.236240000000000000
          Top = 377.953000000000000000
          Width = 26.456710000000000000
          Height = 162.519790000000000000
          DataSet = frxDsCopiaCH
          DataSetName = 'frxDsCopiaCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo:')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
    end
  end
end
