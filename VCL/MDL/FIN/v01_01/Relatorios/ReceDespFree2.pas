unit ReceDespFree2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, dmkEditCB, Db,
  mySQLDbTables, DBCtrls, dmkDBLookupComboBox, ComCtrls, dmkEditDateTimePicker,
  frxClass, frxDBSet, Variants, dmkCheckGroup, Grids, DBGrids, dmkDBGrid, Menus,
  dmkGeral, MyDBCheck, UmySQLModule, UnDmkProcFunc, dmkImage,
{$IfDef cAdvGrid}
  AdvGrid, DBAdvGrid, AdvObj, BaseGrid,
{$EndIf}
  UnDmkEnums, dmkDBGridZTO;

type
  TFmReceDespFree2 = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DataSource1: TDataSource;
    QrDC: TmySQLQuery;
    frxDsDC: TfrxDBDataset;
    QrDCNOMEMES: TWideStringField;
    QrDCGenero: TIntegerField;
    QrDCMez: TIntegerField;
    QrDCDepto: TIntegerField;
    QrDCValor: TFloatField;
    QrDCNOMECON: TWideStringField;
    QrDCUNIDADE: TWideStringField;
    QrDCControle: TIntegerField;
    QrUHs: TmySQLQuery;
    QrUHsUnidade: TWideStringField;
    QrUHsConta: TIntegerField;
    DsUHs: TDataSource;
    QrRet: TmySQLQuery;
    DsRet: TDataSource;
    QrRetCodigo: TIntegerField;
    QrRetBanco: TIntegerField;
    QrRetNossoNum: TWideStringField;
    QrRetSeuNum: TIntegerField;
    QrRetIDNum: TIntegerField;
    QrRetOcorrCodi: TWideStringField;
    QrRetOcorrData: TDateField;
    QrRetValTitul: TFloatField;
    QrRetValAbati: TFloatField;
    QrRetValDesco: TFloatField;
    QrRetValPago: TFloatField;
    QrRetValJuros: TFloatField;
    QrRetValMulta: TFloatField;
    QrRetValJuMul: TFloatField;
    QrRetMotivo1: TWideStringField;
    QrRetMotivo2: TWideStringField;
    QrRetMotivo3: TWideStringField;
    QrRetMotivo4: TWideStringField;
    QrRetMotivo5: TWideStringField;
    QrRetQuitaData: TDateField;
    QrRetDiretorio: TIntegerField;
    QrRetArquivo: TWideStringField;
    QrRetItemArq: TIntegerField;
    QrRetStep: TSmallintField;
    QrRetEntidade: TIntegerField;
    QrRetCarteira: TIntegerField;
    QrRetDevJuros: TFloatField;
    QrRetDevMulta: TFloatField;
    QrRetValOutro: TFloatField;
    QrRetValTarif: TFloatField;
    QrRetLk: TIntegerField;
    QrRetDataCad: TDateField;
    QrRetDataAlt: TDateField;
    QrRetUserCad: TIntegerField;
    QrRetUserAlt: TIntegerField;
    QrRetDtaTarif: TDateField;
    QrRetAlterWeb: TSmallintField;
    QrRetAtivo: TSmallintField;
    QrRetTamReg: TIntegerField;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctControle: TIntegerField;
    QrLctSub: TSmallintField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctQtde: TFloatField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctFatID: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctFatNum: TFloatField;
    QrLctFatParcela: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctID_Sub: TSmallintField;
    QrLctFatura: TWideStringField;
    QrLctEmitente: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctContaCorrente: TWideStringField;
    QrLctCNPJCPF: TWideStringField;
    QrLctLocal: TIntegerField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctPago: TFloatField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctCliente: TIntegerField;
    QrLctCliInt: TIntegerField;
    QrLctForneceI: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctMulta: TFloatField;
    QrLctProtesto: TDateField;
    QrLctDataDoc: TDateField;
    QrLctCtrlIni: TIntegerField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctDepto: TIntegerField;
    QrLctDescoPor: TIntegerField;
    QrLctDescoVal: TFloatField;
    QrLctDescoControle: TIntegerField;
    QrLctUnidade: TIntegerField;
    QrLctNFVal: TFloatField;
    QrLctAntigo: TWideStringField;
    QrLctExcelGru: TIntegerField;
    QrLctLk: TIntegerField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TIntegerField;
    QrLctUserAlt: TIntegerField;
    QrLctSerieCH: TWideStringField;
    QrLctDoc2: TWideStringField;
    QrLctMoraVal: TFloatField;
    QrLctMultaVal: TFloatField;
    QrLctCNAB_Sit: TSmallintField;
    QrLctTipoCH: TSmallintField;
    QrLctAlterWeb: TSmallintField;
    QrLctReparcel: TIntegerField;
    QrLctID_Quit: TIntegerField;
    QrLctAtrelado: TIntegerField;
    QrLctAtivo: TSmallintField;
    QrLctPagMul: TFloatField;
    QrLctPagJur: TFloatField;
    QrLctSubPgto1: TIntegerField;
    QrLctMultiPgto: TIntegerField;
    QrLctProtocolo: TIntegerField;
    QrLctCtrlQuitPg: TIntegerField;
    QrDebitos: TmySQLQuery;
    QrDebitosCOMPENSADO_TXT: TWideStringField;
    QrDebitosDATA: TWideStringField;
    QrDebitosDescricao: TWideStringField;
    QrDebitosDEBITO: TFloatField;
    QrDebitosNOTAFISCAL: TLargeintField;
    QrDebitosSERIECH: TWideStringField;
    QrDebitosDOCUMENTO: TFloatField;
    QrDebitosMEZ: TLargeintField;
    QrDebitosCompensado: TDateField;
    QrDebitosNOMECON: TWideStringField;
    QrDebitosNOMESGR: TWideStringField;
    QrDebitosNOMEGRU: TWideStringField;
    QrDebitosITENS: TLargeintField;
    QrDebitosMES: TWideStringField;
    QrDebitosSERIE_DOC: TWideStringField;
    QrDebitosNF_TXT: TWideStringField;
    QrDebitosMES2: TWideStringField;
    QrDebitosControle: TIntegerField;
    QrDebitosSub: TSmallintField;
    QrDebitosCarteira: TIntegerField;
    QrDebitosCartao: TIntegerField;
    QrDebitosVencimento: TDateField;
    QrDebitosSit: TIntegerField;
    QrDebitosGenero: TIntegerField;
    QrDebitosTipo: TSmallintField;
    DsDebitos: TDataSource;
    frxDsDebitos: TfrxDBDataset;
    frxDsCreditos: TfrxDBDataset;
    DsCreditos: TDataSource;
    QrCreditos: TmySQLQuery;
    QrCreditosMez: TIntegerField;
    QrCreditosCredito: TFloatField;
    QrCreditosNOMECON: TWideStringField;
    QrCreditosNOMESGR: TWideStringField;
    QrCreditosNOMEGRU: TWideStringField;
    QrCreditosMES: TWideStringField;
    QrCreditosNOMECON_2: TWideStringField;
    QrCreditosSubPgto1: TIntegerField;
    QrCreditosControle: TIntegerField;
    QrCreditosSub: TSmallintField;
    QrCreditosCarteira: TIntegerField;
    QrCreditosCartao: TIntegerField;
    QrCreditosVencimento: TDateField;
    QrCreditosCompensado: TDateField;
    QrCreditosSit: TIntegerField;
    QrCreditosGenero: TIntegerField;
    QrCreditosTipo: TSmallintField;
    QrResumo: TmySQLQuery;
    QrResumoCredito: TFloatField;
    QrResumoDebito: TFloatField;
    QrResumoSALDO: TFloatField;
    QrResumoFINAL: TFloatField;
    frxDsResumo: TfrxDBDataset;
    frxDsSaldoA: TfrxDBDataset;
    QrSaldoA: TmySQLQuery;
    QrSaldoAInicial: TFloatField;
    QrSaldoASALDO: TFloatField;
    QrSaldoATOTAL: TFloatField;
    QrDebitosTbLct: TWideStringField;
    QrDebitosGRU_OL: TIntegerField;
    QrDebitosCON_OL: TIntegerField;
    QrDebitosSGR_OL: TIntegerField;
    QrCreditosTbLct: TWideStringField;
    QrCreditosData: TDateField;
    QrCreditosGRU_OL: TIntegerField;
    QrCreditosCON_OL: TIntegerField;
    QrCreditosSGR_OL: TIntegerField;
    frxCredSinteDebiAnali: TfrxReport;
    QrDebitosNO_FORNECE: TWideStringField;
    QrLctIndiPag: TIntegerField;
    QrRetID_Link: TLargeintField;
    QrLctAgencia: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    QrCred: TmySQLQuery;
    DsCred: TDataSource;
    QrCredoN5: TIntegerField;
    QrCredcN5: TIntegerField;
    QrCrednN5: TWideStringField;
    QrCredoN4: TIntegerField;
    QrCredcN4: TIntegerField;
    QrCrednN4: TWideStringField;
    QrCredoN3: TIntegerField;
    QrCredcN3: TIntegerField;
    QrCrednN3: TWideStringField;
    QrCredoN2: TIntegerField;
    QrCredcN2: TIntegerField;
    QrCrednN2: TWideStringField;
    QrCredoN1: TIntegerField;
    QrCredcN1: TIntegerField;
    QrCrednN1: TWideStringField;
    QrCredCreDeb: TSmallintField;
    QrCredValor: TFloatField;
    QrCredAtivo: TSmallintField;
    QrPsq1: TmySQLQuery;
    QrPsq1oN5: TIntegerField;
    QrPsq1cN5: TIntegerField;
    QrPsq1nN5: TWideStringField;
    QrPsq1oN4: TIntegerField;
    QrPsq1cN4: TIntegerField;
    QrPsq1nN4: TWideStringField;
    QrPsq1oN3: TIntegerField;
    QrPsq1cN3: TIntegerField;
    QrPsq1nN3: TWideStringField;
    QrPsq1oN2: TIntegerField;
    QrPsq1cN2: TIntegerField;
    QrPsq1nN2: TWideStringField;
    QrPsq1oN1: TIntegerField;
    QrPsq1cN1: TIntegerField;
    QrPsq1nN1: TWideStringField;
    QrPsq1CreDeb: TSmallintField;
    QrPsq1Valor: TFloatField;
    QrCredcO1: TFloatField;
    QrDebi: TmySQLQuery;
    DsDebi: TDataSource;
    QrDebioN5: TIntegerField;
    QrDebicN5: TIntegerField;
    QrDebinN5: TWideStringField;
    QrDebioN4: TIntegerField;
    QrDebicN4: TIntegerField;
    QrDebinN4: TWideStringField;
    QrDebioN3: TIntegerField;
    QrDebicN3: TIntegerField;
    QrDebinN3: TWideStringField;
    QrDebioN2: TIntegerField;
    QrDebicN2: TIntegerField;
    QrDebinN2: TWideStringField;
    QrDebioN1: TIntegerField;
    QrDebicN1: TIntegerField;
    QrDebinN1: TWideStringField;
    QrDebicO1: TFloatField;
    QrDebiCreDeb: TSmallintField;
    QrDebiValor: TFloatField;
    QrDebiAtivo: TSmallintField;
    TabSheet6: TTabSheet;
    Panel6: TPanel;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    DBGCreditos: TdmkDBGrid;
    TabSheet5: TTabSheet;
    DBGDebitos: TdmkDBGrid;
    Panel12: TPanel;
    Panel11: TPanel;
    GroupBox6: TGroupBox;
    PainelC: TPanel;
    PnDatas2: TPanel;
    GBVencto: TGroupBox;
    LaVctoIni: TLabel;
    LaVctoFim: TLabel;
    TPVctoIni: TdmkEditDateTimePicker;
    TPVctoFim: TdmkEditDateTimePicker;
    CkVencto: TCheckBox;
    GroupBox4: TGroupBox;
    LaDocIni: TLabel;
    LaDocFim: TLabel;
    TPDocIni: TdmkEditDateTimePicker;
    TPDocFim: TdmkEditDateTimePicker;
    CkDataDoc: TCheckBox;
    GroupBox5: TGroupBox;
    LaCompIni: TLabel;
    LaCompFim: TLabel;
    TPCompIni: TdmkEditDateTimePicker;
    TPCompFim: TdmkEditDateTimePicker;
    CkDataComp: TCheckBox;
    PnDatas1: TPanel;
    GroupBox7: TGroupBox;
    LaEmissIni: TLabel;
    LaEmissFim: TLabel;
    TPEmissIni: TdmkEditDateTimePicker;
    TPEmissFim: TdmkEditDateTimePicker;
    CkEmissao: TCheckBox;
    TabSheet7: TTabSheet;
    PageControl3: TPageControl;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    PnCred: TPanel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    EdCredSorcNiv: TdmkEdit;
    EdCredSorcCod: TdmkEdit;
    EdCredDestNiv: TdmkEdit;
    EdCredDestCod: TdmkEdit;
    EdCredACol: TdmkEdit;
    EdCredARow: TdmkEdit;
    EdCredSorcNom: TdmkEdit;
    Panel9: TPanel;
    Label11: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EdDebiSorcNiv: TdmkEdit;
    EdDebiSorcCod: TdmkEdit;
    EdDebiDestNiv: TdmkEdit;
    EdDebiDestCod: TdmkEdit;
    EdDebiACol: TdmkEdit;
    EdDebiARow: TdmkEdit;
    EdDebiSorcNom: TdmkEdit;
    AdvGridDebi: TdmkDBGridZTO;
    Panel5: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel10: TPanel;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCredNivelSel: TmySQLQuery;
    QrCredNivelSelCodigo: TIntegerField;
    QrCredNivelSelNome: TWideStringField;
    DsCredNivelSel: TDataSource;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    DsFornecedores: TDataSource;
    GroupBox1: TGroupBox;
    Panel4: TPanel;
    Label3: TLabel;
    LaCliente: TLabel;
    LaFornece: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    CBFornece: TdmkDBLookupComboBox;
    EdFornece: TdmkEditCB;
    QrCarteiras: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    DsCarteiras: TDataSource;
    QrDebiNivelSel: TmySQLQuery;
    DsDebiNivelSel: TDataSource;
    QrDebiNivelSelCodigo: TIntegerField;
    QrDebiNivelSelNome: TWideStringField;
    GroupBox9: TGroupBox;
    PnPlanoContas: TPanel;
    GroupBox3: TGroupBox;
    RGCredNivel: TRadioGroup;
    Panel7: TPanel;
    Label2: TLabel;
    EdCredNivelSel: TdmkEditCB;
    CBCredNivelSel: TdmkDBLookupComboBox;
    GroupBox8: TGroupBox;
    RGDebiNivel: TRadioGroup;
    Panel8: TPanel;
    Label5: TLabel;
    EdDebiNivelSel: TdmkEditCB;
    CBDebiNivelSel: TdmkDBLookupComboBox;
    Panel13: TPanel;
    CkPlanoUtiliz: TCheckBox;
    CkPlanoContas: TCheckBox;
    Panel14: TPanel;
    RGMostrar: TRadioGroup;
    RGAskMerge: TRadioGroup;
    CkAgruCta: TCheckBox;
    GroupBox10: TGroupBox;
    GBAnaliDebi: TGroupBox;
    CkDebiAcordos: TCheckBox;
    CkDebiPeriodos: TCheckBox;
    CkDebiTextos: TCheckBox;
    GroupBox11: TGroupBox;
    GBAnaliCred: TGroupBox;
    CkCredAcordos: TCheckBox;
    CkCredPeriodos: TCheckBox;
    CkCredTextos: TCheckBox;
    BtImprime: TBitBtn;
    CkAnaliCred: TCheckBox;
    CkAnaliDebi: TCheckBox;
    Panel15: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    RGRelatorio: TRadioGroup;
    frxReceDesp_: TfrxReport;
    frxCredAnaliDebiAnali: TfrxReport;
    QrCreditosDescricao: TWideStringField;
    QrCreditosNO_CLIENTE: TWideStringField;
    QrCreditosMES2: TWideStringField;
    QrCreditosCOMPENSADO_TXT: TWideStringField;
    QrCreditosSERIE_DOC: TWideStringField;
    QrCreditosNOTAFISCAL: TLargeintField;
    QrCreditosSERIECH: TWideStringField;
    QrCreditosDOCUMENTO: TFloatField;
    QrCreditosNF_TXT: TWideStringField;
    frxReceDesp: TfrxReport;
    frxDsCred: TfrxDBDataset;
    frxDsDebi: TfrxDBDataset;
    BtTeste: TButton;
    frxCredSinteDebiSinte: TfrxReport;
    frxCredAnaliDebiSinte: TfrxReport;
    AdvGridCred: TdmkDBGridZTO;
    QrPsq1Ativ1: TSmallintField;
    QrPsq1Ativ2: TSmallintField;
    QrPsq1Ativ3: TSmallintField;
    QrPsq1Ativ4: TSmallintField;
    QrPsq1Ativ5: TSmallintField;
    QrPsq1Ativo: TSmallintField;
    QrCredAtiv1: TSmallintField;
    QrCredAtiv2: TSmallintField;
    QrCredAtiv3: TSmallintField;
    QrCredAtiv4: TSmallintField;
    QrCredAtiv5: TSmallintField;
    QrDebiAtiv1: TSmallintField;
    QrDebiAtiv2: TSmallintField;
    QrDebiAtiv3: TSmallintField;
    QrDebiAtiv4: TSmallintField;
    QrDebiAtiv5: TSmallintField;
    TabSheet2: TTabSheet;
    LaAvisoDesenvolvedor: TLabel;
    LaAvisoUsuario: TLabel;
    Panel16: TPanel;
    BtCancela: TBitBtn;
    BtSalvar: TBitBtn;
    GBReceitas: TGroupBox;
    LBReceitas: TListBox;
    BtReceUp: TBitBtn;
    BtReceDown: TBitBtn;
    GBDespesas: TGroupBox;
    LBDespesas: TListBox;
    BtDespDown: TBitBtn;
    BtDespUp: TBitBtn;
    PnInfo: TPanel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxReceDesp_GetValue(const VarName: String;
      var Value: Variant);
    procedure QrDCCalcFields(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure QrDebitosCalcFields(DataSet: TDataSet);
    procedure QrCreditosCalcFields(DataSet: TDataSet);
    procedure QrResumoCalcFields(DataSet: TDataSet);
    procedure QrSaldoACalcFields(DataSet: TDataSet);
    procedure AdvGridCredOleDrag(Sender: TObject; ARow, ACol: Integer;
      data: string; var Allow: Boolean);
    procedure AdvGridCredOleDragOver(Sender: TObject; ARow, ACol: Integer;
      var Allow: Boolean);
    procedure AdvGridCredOleDrop(Sender: TObject; ARow, ACol: Integer;
      data: string; var Allow: Boolean);
    procedure QrCredAfterOpen(DataSet: TDataSet);
    procedure CkAgruCtaClick(Sender: TObject);
    procedure AdvGridCredClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure RGMostrarClick(Sender: TObject);
    procedure AdvGridDebiOleDrag(Sender: TObject; ARow, ACol: Integer;
      data: string; var Allow: Boolean);
    procedure AdvGridDebiOleDragOver(Sender: TObject; ARow, ACol: Integer;
      var Allow: Boolean);
    procedure AdvGridDebiClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure QrDebiAfterOpen(DataSet: TDataSet);
    procedure AdvGridDebiOleDrop(Sender: TObject; ARow, ACol: Integer;
      data: string; var Allow: Boolean);
    procedure FechaPesquisa(Sender: TObject);
    procedure RGCredNivelClick(Sender: TObject);
    procedure RGDebiNivelClick(Sender: TObject);
    procedure CkPlanoUtilizClick(Sender: TObject);
    procedure CkPlanoContasClick(Sender: TObject);
    procedure CkAnaliCredClick(Sender: TObject);
    procedure CkAnaliDebiClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure RGRelatorioClick(Sender: TObject);
    procedure BtTesteClick(Sender: TObject);
    procedure BtReceUpClick(Sender: TObject);
    procedure BtReceDownClick(Sender: TObject);
    procedure BtDespUpClick(Sender: TObject);
    procedure BtDespDownClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure AdvGridDebiClick(Sender: TObject);
    procedure AdvGridCredClick(Sender: TObject);
  private
    { Private declarations }
    //FGenero, FUH,
    FCredAntCol, FCredAntRow, FDebiAntCol, FDebiAntRow,
    FEntidade, FEmpresa: Integer;
    FReceDesp,
    FEntidTXT, FEmprTXT,
    //**** FDataI1, FDataF1, FDataI2, FDataF2,
    FTabLctA, FTabLctB, FTabLctD: String;
    FDtIni, FDtEncer, FDtMorto: TDateTime;
    procedure DemostrativoDeReceitasEDespesas();
    //**** procedure HistoricoConta();
    //**** procedure RetornosCNAB();
    procedure ReopenCreditosEDebitos(CliInt: String; DataI,
              DataF: TDateTime; SelPeriodoEmissao, CredAcordos, DebiAcordos,
              CredPeriodos, DebiPeriodos, CredTextos, DebiTextos(*,
              NaoAgruparNada*): Boolean; TabLctA, TabLctB, TabLctD: String);
    //
    function  ObtemNivel(const ACol: Integer): Integer;
    function  ObtemNomeNivel(Nivel: Integer): String;
    procedure ReopenCred(Campo: String; Genero: Integer);
    procedure ReopenDebi(Campo: String; Genero: Integer);
    procedure ReopenDbCr(Sinal: TSinal; Campo: String; Genero: Integer);
    //
    procedure AdvGridConfigura(AdvGrid: TdmkDBGridZTO);
    procedure AdvGridClickCell(Sender: TObject; ARow, ACol: Integer; Sinal: TSinal);
    procedure AdvGridOleDrop(Sender: TObject; ARow, ACol: Integer;
              data: string; var Allow: Boolean; Sinal: TSinal;
              EdDestNiv, EdDestCod, EdSorcNiv, EdSorcCod, EdSorcNom: TdmkEdit);
    procedure Pesquisa();
    function  SQL_Where(CreDeb: Integer): String;
    procedure ReopenNivelSel(Sinal: TSinal);
    function  NomeDoNivelSelecionado(Nivel, Tipo: Integer): String;
    //
    procedure MoveItensListBox(ListBox: TListBox; Cima: Boolean);
    procedure CarregaOrdenacao();
  public
    { Public declarations }
  end;

  var
  FmReceDespFree2: TFmReceDespFree2;

implementation

uses UnMyObjects, UnInternalConsts, Module, LctMudaConta, UnFinanceiro,
  LctEdit, Principal, ModuleGeral, UCreateFin, DmkDAC_PF;

{$R *.DFM}

const
  FColsToMerge: array[0..3] of Integer = (3,5,7,9);

procedure TFmReceDespFree2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReceDespFree2.BtSalvarClick(Sender: TObject);
var
  KeyRecDes, Texto: String;
  i: Integer;
begin
  KeyRecDes := Application.Title + '\ReceitasDespesas';
  Texto     := '';
  //
  for i := 0 to LBReceitas.Count - 1 do
  begin
    if i = 0 then
      Texto := LBReceitas.Items[i]
    else
      Texto := Texto + '||' + LBReceitas.Items[i];
  end;
  Geral.WriteAppKeyCU('Receitas', KeyRecDes, Texto, ktString);
  //
  Texto := '';
  //
  for i := 0 to LBDespesas.Count - 1 do
  begin
    if i = 0 then
      Texto := LBDespesas.Items[i]
    else
      Texto := Texto + '||' + LBDespesas.Items[i];
  end;
  Geral.WriteAppKeyCU('Despesas', KeyRecDes, Texto, ktString);
end;

procedure TFmReceDespFree2.CarregaOrdenacao;
var
  Lista: TStringList;
  KeyRecDes, Receitas, Despesas: String;
begin
  Lista := TStringList.Create;
  KeyRecDes := Application.Title + '\ReceitasDespesas';
  //
  Receitas := Geral.ReadAppKeyCU('Receitas', KeyRecDes, ktString, '');
  Despesas := Geral.ReadAppKeyCU('Despesas', KeyRecDes, ktString, '');
  //
  if Length(Receitas) > 0 then
  begin
    Lista := Geral.Explode(Receitas, '||', 3);
    //
    LBReceitas.Items := Lista;
  end;
  if Length(Despesas) > 0 then
  begin
    Lista := Geral.Explode(Despesas, '||', 3);
    //
    LBDespesas.Items := Lista;
  end;
  Lista.Free;
end;

procedure TFmReceDespFree2.CkAgruCtaClick(Sender: TObject);
begin
  if QrCred.State <> dsInactive then
    ReopenCred('cN1', QrCredcN1.Value);
  if QrDebi.State <> dsInactive then
    ReopenDebi('cN1', QrDebicN1.Value);
end;

procedure TFmReceDespFree2.CkAnaliCredClick(Sender: TObject);
begin
  GBAnaliCred.Visible := CkAnaliCred.Checked;
end;

procedure TFmReceDespFree2.CkAnaliDebiClick(Sender: TObject);
begin
  GBAnaliDebi.Visible := CkAnaliDebi.Checked;
end;

procedure TFmReceDespFree2.CkPlanoContasClick(Sender: TObject);
begin
  PnPlanoContas.Visible := CkPlanoContas.Checked;
  if CkPlanoContas.Checked then
  begin
    if EdEmpresa.ValueVariant = 0 then
    begin
      Geral.MB_Aviso('Informe o cliente interno (Empresa)!');
      CkPlanoContas.Checked := False;
    end;
  end;
end;

procedure TFmReceDespFree2.CkPlanoUtilizClick(Sender: TObject);
begin
  ReopenNivelSel(siPositivo);
  ReopenNivelSel(siNegativo);
  FechaPesquisa(Sender);
end;

procedure TFmReceDespFree2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //****CGValores.Value := 3;
end;

procedure TFmReceDespFree2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  //
{$IfDef cAdvGrid}
  MyObjects.UpdMergeDBAvGrid(AdvGridCred, FColsToMerge);
  MyObjects.UpdMergeDBAvGrid(AdvGridDebi, FColsToMerge);
{$EndIf}
end;

procedure TFmReceDespFree2.FormCreate(Sender: TObject);
(*
var
  I: integer;
*)
begin
  ImgTipo.SQLType := stPsq;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FEntidade            := 0;
  BtImprime.Enabled    := False;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  QrCreditos.Database := DModG.MyPID_DB;
  QrDebitos.Database  := DModG.MyPID_DB;
  QrSaldoA.Database   := DModG.MyPID_DB;
  QrResumo.Database   := DModG.MyPID_DB;
  QrDC.Database       := DModG.MyPID_DB;
  //
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  //
  TPEmissIni.Date := Date -30;
  TPEmissFim.Date := Date;
  //
  TPVctoIni.Date := Date;
  TPVctoFim.Date := Date + 30;
  //
  TPDocIni.Date := Date -30;
  TPDocFim.Date := Date;
  //
  TPCompIni.Date := Date -30;
  TPCompFim.Date := Date;
  //
  {
  MyObjects.PreencheCBAnoECBMes(CBAnoIni, CBMesIni);
  MyObjects.PreencheCBAnoECBMes(CBAnoFim, CBMesFim);
  }
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  //
{$IfDef cAdvGrid}
  AdvGridCred.ExcelClipboardFormat := True;
{$EndIf}
  QrCred.DataBase := DModG.MyPID_DB;
  QrDebi.DataBase := DModG.MyPID_DB;
  QrPsq1.DataBase := DModG.MyPID_DB;
  //
  AdvGridConfigura(AdvGridCred);
  AdvGridConfigura(AdvGridDebi);
  //
  CarregaOrdenacao;
end;

procedure TFmReceDespFree2.BtImprimeClick(Sender: TObject);
begin
  DemostrativoDeReceitasEDespesas();
  //
  if CkAnaliCred.Checked and CkAnaliDebi.Checked then
  begin
    MyObjects.frxDefineDataSets(frxCredAnaliDebiAnali, [
      DmodG.frxDsDono,
      frxDsCreditos,
      frxDsDebitos,
      frxDsResumo,
      frxDsSaldoA
      ]);
    MyObjects.frxMostra(frxCredAnaliDebiAnali, 'Demonstrativo anal�tico de receitas e despesas');
  end else
  if CkAnaliDebi.Checked then
  begin
    MyObjects.frxDefineDataSets(frxCredSinteDebiAnali, [
      DmodG.frxDsDono,
      frxDsCred,
      frxDsCreditos,
      frxDsDebi,
      frxDsDebitos,
      frxDsResumo,
      frxDsSaldoA
      ]);
    MyObjects.frxMostra(frxCredSinteDebiAnali, 'Demonstrativo sint�tico de receitas e anal�tico de despesas');
  end else
  if CkAnaliCred.Checked then
  begin
    MyObjects.frxDefineDataSets(frxCredAnaliDebiSinte, [
      DmodG.frxDsDono,
      frxDsCred,
      frxDsCreditos,
      frxDsDebi,
      frxDsDebitos,
      frxDsResumo,
      frxDsSaldoA
      ]);
    MyObjects.frxMostra(frxCredAnaliDebiSinte, 'Demonstrativo anal�tico de receitas e sint�tico de despesas');
  end else
  //if CkAnaliDebi.Checked then
  begin
    Geral.MB_Aviso('Voc� deve marcar o(s) campo(s): Cr�dito ou D�bito!');
    (* 2017-09-21 => D� erro reativar se necess�rio
    MyObjects.frxDefineDataSets(frxCredSinteDebiSinte, [
      DmodG.frxDsDono,
      frxDsCred,
      frxDsCreditos,
      frxDsDebi,
      frxDsDebitos,
      frxDsResumo,
      frxDsSaldoA
      ]);
    MyObjects.frxMostra(frxCredSinteDebiSinte, 'Demonstrativo sint�tico de receitas e despesas');
    *)
  end;
{
  else
    Geral.MB_('Relat�rio n�o implementado! Solicite � DERMATEK');
}
end;

procedure TFmReceDespFree2.BtCancelaClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Receitas', Application.Title + '\ReceitasDespesas', '', ktString);
  Geral.WriteAppKeyCU('Despesas', Application.Title + '\ReceitasDespesas', '', ktString);
  //
  Geral.MB_Info('Reabra a janela para que a configura��o padr�o seja restaurada!');
end;

procedure TFmReceDespFree2.BtDespDownClick(Sender: TObject);
begin
  MoveItensListBox(LBDespesas, False);
end;

procedure TFmReceDespFree2.BtDespUpClick(Sender: TObject);
begin
  MoveItensListBox(LBDespesas, True);
end;

procedure TFmReceDespFree2.BtOKClick(Sender: TObject);
begin
  if EdEmpresa.ValueVariant = 0 then
  begin
    Geral.MB_Aviso('Informe a Empresa!');
    EdEmpresa.SetFocus;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    FEntidade := DModG.QrEmpresasCodigo.Value;
    FEntidTXT := FormatFloat('0', FEntidade);
    FEmpresa  := DModG.QrEmpresasFilial.Value;
    FEmprTXT  := FormatFloat('0', FEmpresa);
{****
    FDataI1 := Geral.FDT(TPDataIni1.Date, 1);
    FDataF1 := Geral.FDT(TPDataFim1.Date, 1);
    FDataI2 := Geral.FDT(TPDataIni2.Date, 1);
    FDataF2 := Geral.FDT(TPDataFim2.Date, 1);
    FGenero := Geral.IMV(EdConta.Text);
    if CBUH.KeyValue <> Null then
      FUH := CBUH.KeyValue
    else FUH := 0;
    //
    case PageControl1.ActivePageIndex of
      0: DemostrativoDeReceitasEDespesas();
      //****1: HistoricoConta();
      //****2: RetornosCNAB();
      else Geral.MB_('Relat�rio (aba) n�o definido');
    end;
}
    Pesquisa();
    PageControl1.ActivePageIndex := 1;
    //
    BtImprime.Enabled := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmReceDespFree2.BtReceDownClick(Sender: TObject);
begin
  MoveItensListBox(LBReceitas, False);
end;

procedure TFmReceDespFree2.BtReceUpClick(Sender: TObject);
begin
  MoveItensListBox(LBReceitas, True);
end;

procedure TFmReceDespFree2.AdvGridClickCell(Sender: TObject; ARow,
  ACol: Integer; Sinal: TSinal);
var
  cN1, Ativo, Nivel, I: Integer;
  Campo, Codigo, AtivS, FldAtiv, CreDeb, SQL: String;
begin
{$IfDef cAdvGrid}
  FldAtiv := TdmkDBGridZTO(AdvGridDebi).Columns[ACol].FieldName;
  if Copy(FldAtiv, 1, 4) = 'Ativ' then
  begin
    if Sinal = siPositivo then
      CreDeb := '1'
    else
      CreDeb := '2';
    //
    Nivel := Geral.IMV(Copy(FldAtiv, 5));
    if Nivel = 0 then
      Exit;
    cN1 := Geral.IMV(TdmkDBGridZTO(Sender).Cells[1, ARow]);
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq1, DModG.MyPID_DB, [
    'SELECT * FROM ' + FReceDesp,
    'WHERE cN1=' + Geral.FF0(cN1),
    'AND CreDeb=' + CreDeb,
    '']);
    Ativo := QrPsq1.FieldByName(FldAtiv).AsInteger;
    Campo := 'cN' + Geral.FF0(Nivel);
    Codigo := Geral.FF0(QrPsq1.FieldByName(Campo).AsInteger);
    if Ativo = 1 then Ativo := 0 else Ativo := 1;
    AtivS := Geral.FF0(Ativo);
    //
    SQL := '';
    for I := 1 to Nivel do
      SQL := SQL + ' Ativ' + Geral.FF0(I) + '=' + AtivS + ', ';
    SQL := SQL + 'Ativo=' + AtivS;
    //
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'UPDATE ' + FReceDesp,
    ' SET ',
    SQL,
    'WHERE CreDeb=' + CreDeb,
    'AND ' + Campo + '=' + Codigo,
    '']);
    //
    if Sinal = siPositivo then
      ReopenCred('cN1', cN1)
    else
      ReopenDebi('cN1', cN1);
  end;
{$Else}
  PnInfo.Caption := 'Implementa��o pendente: ...GridClickCell...';
{$EndIf}
end;

procedure TFmReceDespFree2.AdvGridConfigura(AdvGrid: TdmkDBGridZTO);
begin
{
  AdvGrid.ColCount := 14;
  AdvGrid.Columns[01].Width := 056;
  AdvGrid.Columns[02].Width := 020;
  AdvGrid.Columns[03].Width := 110;
  AdvGrid.Columns[04].Width := 020;
  AdvGrid.Columns[05].Width := 110;
  AdvGrid.Columns[06].Width := 020;
  AdvGrid.Columns[07].Width := 110;
  AdvGrid.Columns[08].Width := 020;
  AdvGrid.Columns[09].Width := 110;
  AdvGrid.Columns[10].Width := 020;
  AdvGrid.Columns[11].Width := 220;
  AdvGrid.Columns[12].Width := 100;
  AdvGrid.Columns[13].Width := 020;
  //
  AdvGrid.Columns[01].FieldName := 'cN1';
  AdvGrid.Columns[02].FieldName := 'Ativ5';
  AdvGrid.Columns[03].FieldName := 'nN5';
  AdvGrid.Columns[04].FieldName := 'Ativ4';
  AdvGrid.Columns[05].FieldName := 'nN4';
  AdvGrid.Columns[06].FieldName := 'Ativ3';
  AdvGrid.Columns[07].FieldName := 'nN3';
  AdvGrid.Columns[08].FieldName := 'Ativ2';
  AdvGrid.Columns[09].FieldName := 'nN2';
  AdvGrid.Columns[10].FieldName := 'Ativ1';
  AdvGrid.Columns[11].FieldName := 'nN1';
  AdvGrid.Columns[12].FieldName := 'Valor';
  AdvGrid.Columns[13].FieldName := 'Ativo';
  //
  AdvGrid.Columns[01].Header := 'ID';
  AdvGrid.Columns[02].Header := 'A5';
  AdvGrid.Columns[03].Header := 'Plano';
  AdvGrid.Columns[04].Header := 'A4';
  AdvGrid.Columns[05].Header := 'Conjunto';
  AdvGrid.Columns[06].Header := 'A3';
  AdvGrid.Columns[07].Header := 'Grupo';
  AdvGrid.Columns[08].Header := 'A2';
  AdvGrid.Columns[09].Header := 'Sub-grupo';
  AdvGrid.Columns[10].Header := 'A1';
  AdvGrid.Columns[11].Header := 'Conta';
  AdvGrid.Columns[12].Header := 'Valor';
  AdvGrid.Columns[13].Header := 'Ativo';
  //
  AdvGrid.Columns[01].Alignment := taRightJustify;
  AdvGrid.Columns[02].Alignment := taLeftJustify;
  AdvGrid.Columns[03].Alignment := taLeftJustify;
  AdvGrid.Columns[04].Alignment := taLeftJustify;
  AdvGrid.Columns[05].Alignment := taLeftJustify;
  AdvGrid.Columns[06].Alignment := taLeftJustify;
  AdvGrid.Columns[07].Alignment := taLeftJustify;
  AdvGrid.Columns[08].Alignment := taLeftJustify;
  AdvGrid.Columns[09].Alignment := taLeftJustify;
  AdvGrid.Columns[10].Alignment := taLeftJustify;
  AdvGrid.Columns[11].Alignment := taLeftJustify;
  AdvGrid.Columns[12].Alignment := taRightJustify;
  AdvGrid.Columns[13].Alignment := taLeftJustify;
  //
  AdvGrid.Columns[12].FloatFormat := '%2.2n';
  //
  AdvGrid.Columns[02].CheckBoxField := True;
  AdvGrid.Columns[02].CheckFalse := '0';
  AdvGrid.Columns[02].CheckTrue := '1';
  AdvGrid.Columns[04].CheckBoxField := True;
  AdvGrid.Columns[04].CheckFalse := '0';
  AdvGrid.Columns[04].CheckTrue := '1';
  AdvGrid.Columns[06].CheckBoxField := True;
  AdvGrid.Columns[06].CheckFalse := '0';
  AdvGrid.Columns[06].CheckTrue := '1';
  AdvGrid.Columns[08].CheckBoxField := True;
  AdvGrid.Columns[08].CheckFalse := '0';
  AdvGrid.Columns[08].CheckTrue := '1';
  AdvGrid.Columns[10].CheckBoxField := True;
  AdvGrid.Columns[10].CheckFalse := '0';
  AdvGrid.Columns[10].CheckTrue := '1';
  AdvGrid.Columns[13].CheckBoxField := True;
  AdvGrid.Columns[13].CheckFalse := '0';
  AdvGrid.Columns[13].CheckTrue := '1';
  //
}
end;

procedure TFmReceDespFree2.AdvGridCredClick(Sender: TObject);
begin
{$IfDef cAdvGrid}
  MyObjects.UpdMergeDBAvGrid(AdvGridCred, FColsToMerge);
{$EndIf}
end;

procedure TFmReceDespFree2.AdvGridCredClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  AdvGridClickCell(Sender, ARow, ACol, siPositivo);
end;

procedure TFmReceDespFree2.AdvGridCredOleDrag(Sender: TObject; ARow,
  ACol: Integer; data: string; var Allow: Boolean);
begin
  Allow := Arow > 0;
  if Allow then
  begin
    EdCredSorcNiv.ValueVariant := ObtemNivel(ACol);
    EdCredSorcCod.ValueVariant := QrCredcN1.Value;
    EdCredSorcNom.Text         := QrCrednN1.Value;
  end;
end;

procedure TFmReceDespFree2.AdvGridCredOleDragOver(Sender: TObject; ARow,
  ACol: Integer; var Allow: Boolean);
begin
{$IfDef cAdvGrid}
  if Arow > 0 then  // Evitar bug
  begin
    EdCredDestNiv.ValueVariant := ObtemNivel(ACol);
    EdCredDestCod.ValueVariant := Geral.IMV(AdvGridCred.Cells[01, ARow]);
    //pintar celula que est� recebendo o foco!
    if (FCredAntCol >= 1) and (FCredAntRow >= 1) then
      AdvGridCred.Colors[FCredAntCol, FCredAntRow] := clWindow;
    if (ACol >= 1) and (ARow >= 1)
    (*and (ACol < AdvGridCred.ColCount) and (ARow < AdvGridCred.RowCount)*) then
    begin
      EdCredACol.ValueVariant := ACol;
      EdCredARow.ValueVariant := ARow;
      try
        FCredAntCol := ACol;
        FCredAntRow := ARow;
        //AdvGridCred.Colors[ACol, ARow] := FmPrincipal.sd1.Colors[csButtonShadow];
        AdvGridCred.Colors[ACol, ARow] := MyObjects.SubstituiCorParaSkin(csButtonShadow);
      except
        //
      end;
    end;
  end;
{$Else}
  PnInfo.Caption := 'Implementa��o pendente: ...Grid..DragOver...';
{$EndIf}
end;

procedure TFmReceDespFree2.AdvGridCredOleDrop(Sender: TObject; ARow,
  ACol: Integer; data: string; var Allow: Boolean);
(*
var
  SorcNiv, SorcCod, DestNiv, DestCod, NivelPai, I: Integer;
  SorcFld, SorcNom, DestFld, DestNom, Campo, x, SQL: String;
  Continua: Integer;
*)
begin
  AdvGridOleDrop(Sender, ARow, ACol, data, Allow, siPositivo,
  EdCredDestNiv, EdCredDestCod, EdCredSorcNiv, EdCredSorcCod, EdCredSorcNom);
end;

procedure TFmReceDespFree2.AdvGridDebiClick(Sender: TObject);
begin
{$IfDef cAdvGrid}
  MyObjects.UpdMergeDBAvGrid(AdvGridDebi, FColsToMerge);
{$EndIf}
end;

procedure TFmReceDespFree2.AdvGridDebiClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  AdvGridClickCell(Sender, ARow, ACol, siNegativo);
end;

procedure TFmReceDespFree2.AdvGridDebiOleDrag(Sender: TObject; ARow,
  ACol: Integer; data: string; var Allow: Boolean);
begin
  Allow := Arow > 0;
  if Allow then
  begin
    EdDebiSorcNiv.ValueVariant := ObtemNivel(ACol);
    EdDebiSorcCod.ValueVariant := QrDebicN1.Value;
    EdDebiSorcNom.Text         := QrDebinN1.Value;
  end;
end;

procedure TFmReceDespFree2.AdvGridDebiOleDragOver(Sender: TObject; ARow,
  ACol: Integer; var Allow: Boolean);
begin
{$IfDef cAdvGrid}
  if Arow > 0 then  // Evitar bug
  begin
    EdDebiDestNiv.ValueVariant := ObtemNivel(ACol);
    EdDebiDestCod.ValueVariant := Geral.IMV(AdvGridDebi.Cells[01, ARow]);
    //pintar celula que est� recebendo o foco!
    if (FDebiAntCol >= 1) and (FDebiAntRow >= 1) then
      AdvGridDebi.Colors[FDebiAntCol, FDebiAntRow] := clWindow;
    if (ACol >= 1) and (ARow >= 1)
    (*and (ACol < AdvGridDebi.ColCount) and (ARow < AdvGridDebi.RowCount)*) then
    begin
      EdDebiACol.ValueVariant := ACol;
      EdDebiARow.ValueVariant := ARow;
      try
        FDebiAntCol := ACol;
        FDebiAntRow := ARow;
        //AdvGridDebi.Colors[ACol, ARow] := FmPrincipal.sd1.Colors[csButtonShadow];
        AdvGridDebi.Colors[ACol, ARow] := MyObjects.SubstituiCorParaSkin(csButtonShadow);
      except
        //
      end;
    end;
  end;
{$Else}
  PnInfo.Caption := 'Implementa��o pendente: ...GridDebiOleDragOver...';
{$EndIf}
end;

procedure TFmReceDespFree2.AdvGridDebiOleDrop(Sender: TObject; ARow,
  ACol: Integer; data: string; var Allow: Boolean);
begin
  AdvGridOleDrop(Sender, ARow, ACol, data, Allow, siNegativo,
  EdDebiDestNiv, EdDebiDestCod, EdDebiSorcNiv, EdDebiSorcCod, EdDebiSorcNom);
end;

procedure TFmReceDespFree2.AdvGridOleDrop(Sender: TObject; ARow, ACol: Integer;
  data: string; var Allow: Boolean; Sinal: TSinal;
  EdDestNiv, EdDestCod, EdSorcNiv, EdSorcCod, EdSorcNom: TdmkEdit);
var
  SorcNiv, SorcCod, DestNiv, DestCod, (*NivelPai,*) I: Integer;
  SorcFld, SorcNom, DestFld, DestNom, (*Campo,*) x, SQL: String;
  Continua: Integer;
begin
{$IfDef cAdvGrid}
  EdDestCod.ValueVariant := Geral.IMV(TdmkDBGridZTO(Sender).Cells[01, ARow]);
  //
  SorcNiv := EdSorcNiv.ValueVariant;
  SorcCod := EdSorcCod.ValueVariant;
  SorcFld := 'cN' + Geral.FF0(SorcNiv);
  SorcNom := EdSorcNom.ValueVariant;
  //
  DestNiv := EdDestNiv.ValueVariant;
  DestCod := EdDestCod.ValueVariant;
  DestFld := 'cN' + Geral.FF0(DestNiv);
  //
  if (SorcCod = DestCod) then
    Exit;
  if (SorcNiv > 0) and (DestNiv > 0) then
  begin
    //Continua := ID_NO;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq1, DModG.MyPID_DB, [
    'SELECT * FROM ' + FReceDesp,
    'WHERE cN1=' + Geral.FF0(DestCod),
    '']);
    //
    DestNom := QrPsq1.FieldByName('nN' + Geral.FF0(DestNiv)).AsString;
    //
    if DestNiv < SorcNiv then
    begin
      Geral.MB_Aviso(
      'N�vel de destino n�o pode ser inferior ao n�vel de origem!');
      Exit;
    end
    else
    if DestNiv = SorcNiv then
    begin
      if RGAskMerge.ItemIndex < 2 then
        Continua :=
          Geral.MB_Pergunta('Tem certeza que deseja mesclar no n�vel ' +
          ObtemNomeNivel(SorcNiv) + ' os itens de:' + sLineBreak + SorcNom +
          sLineBreak + '  e' + sLineBreak + DestNom + ' ?')
      else
        Continua := ID_YES;
    end else
    begin
      if RGAskMerge.ItemIndex = 0 then
        Continua :=
          Geral.MB_Pergunta('Tem certeza que deseja adicionar [' +
          ObtemNomeNivel(SorcNiv) + '] ' + sLineBreak + SorcNom + sLineBreak +
          '  em [' + ObtemNomeNivel(DestNiv) + '] ' + sLineBreak + DestNom)
      else
        Continua := ID_YES;
    end;
    //
    if Continua = ID_YES then
    begin
      SQL := 'UPDATE ' + FReceDesp + ' SET ' +
        'oN5=' + Geral.FF0(QrPsq1oN5.Value) + ', cN5=' + Geral.FF0(
        QrPsq1cN5.Value) + ', nN5="' + QrPsq1nN5.Value + '"' + sLineBreak;
      for I := 4 downto DestNiv do
      begin
        x := Geral.FF0(I);
        SQL := SQL +
        ', oN' + x + '=' + Geral.FF0(QrPsq1.FieldByName('oN' + x).AsInteger) +
        ', cN' + x + '=' + Geral.FF0(QrPsq1.FieldByName('cN' + x).AsInteger) +
        ', nN' + x + '="' + QrPsq1.FieldByName('nN' + x).AsString + '"' + sLineBreak;
      end;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrPsq1, DModG.MyPID_DB, [
      'SELECT * FROM ' + FReceDesp,
      'WHERE cN1=' + Geral.FF0(SorcCod),
      '']);
      SorcCod := QrPsq1.FieldByName('cN' + Geral.FF0(SorcNiv)).AsInteger;
      //
      SQL := SQL + 'WHERE cN' + Geral.FF0(SorcNiv) + '=' + Geral.FF0(SorcCod);
      //Geral.MB_(SQL, 'Mensagem');
      UMyMod.ExecutaDB(DModG.MyPID_DB, SQL);
      if Sinal = siPositivo then
        ReopenCred('cN' + Geral.FF0(SorcNiv), SorcCod)
      else
        ReopenDebi('cN' + Geral.FF0(SorcNiv), SorcCod);
    end;
  end;
  //
  TdmkDBGridZTO(Sender).Invalidate;
{$Else}
  PnInfo.Caption := 'Implementa��o pendente: ...Grid..Drop...';
{$EndIf}
end;

procedure TFmReceDespFree2.DemostrativoDeReceitasEDespesas();
  procedure GeraParteSQL_SaldoA(TabLct, Entidade, FldIni: String);
  begin
    QrSaldoA.SQL.Add('SELECT ' + FldIni + ' Inicial,');
    QrSaldoA.SQL.Add('(');
    QrSaldoA.SQL.Add('  SELECT SUM(lct.Credito-lct.Debito)');
    QrSaldoA.SQL.Add('  FROM ' + TabLct + ' lct');
    QrSaldoA.SQL.Add('  WHERE lct.Carteira=car.Codigo');
    //QrSaldoA.SQL.Add('  AND lct.Data < "' + FDataI1 + '"');
    QrSaldoA.SQL.Add(DmkPF.SQL_DataMenMai('  AND lct.Data < ',
      TPEmissIni.Date, CkEmissao.Checked));
    //
    QrSaldoA.SQL.Add('  AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
    // N�o precisa
    // QrSaldoA.SQL.Add('  AND car.ForneceI=' + Entidade);
    QrSaldoA.SQL.Add(') SALDO');
    QrSaldoA.SQL.Add('FROM ' + TMeuDB + '.carteiras car');
    QrSaldoA.SQL.Add('WHERE car.ForneceI=' + Entidade);
    QrSaldoA.SQL.Add('AND car.Tipo <> 2');
  end;

  procedure GeraParteSQL_Resumo(TabLct: String);
  begin
    QrResumo.SQL.Add('SELECT SUM(lct.Credito) Credito, -SUM(lct.Debito) Debito');
    QrResumo.SQL.Add('FROM ' + TabLct + ' lct');
    QrResumo.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrResumo.SQL.Add('WHERE lct.Tipo <> 2');
    QrResumo.SQL.Add('AND lct.Genero > 0');
    QrResumo.SQL.Add('AND car.ForneceI=' + FEntidTXT);
    //QrResumo.SQL.Add('AND lct.Data BETWEEN "' + FDataI1 + '" AND "' + FDataF1 + '"');
    QrResumo.SQL.Add(DmkPF.SQL_Periodo('AND lct.Data ', TPEmissIni.Date,
      TPEmissFim.Date, CkEmissao.Checked, CkEmissao.Checked));
    //  
    QrResumo.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
  end;

var
  //Imprime: Boolean;
  FldIni, TabIni: String;
  DataIni: TDateTime;
begin
  ReopenCreditosEDebitos(
  FEntidTXT, TPEmissIni.Date, TPEmissFim.Date, CkEmissao.Checked,
  CkCredAcordos.Checked, CkDebiAcordos.Checked,
  CkCredPeriodos.Checked, CkDebiPeriodos.Checked,
  CkCredTextos.Checked, CkDebiTextos.Checked,
  //CkNaoAgruparNada.Checked,
  FTabLctA, FTabLctB, FTabLctD);
  //
  if CkEmissao.Checked then
    DataIni := TPEmissIni.Date
  else
    DataIni := 0;
  FldIni := UFinanceiro.DefLctFldSdoIni(DataIni, FDtEncer, FDtMorto);
  TabIni := UFinanceiro.DefLctTab(FDtIni, FDtEncer, FDtMorto,
            FTabLcta, FTabLctB, FTabLctD);
  //
  QrSaldoA.Close;
  QrSaldoA.SQL.Clear;

  //
  QrSaldoA.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_SALDOA_1_;');
  QrSaldoA.SQL.Add('CREATE TABLE _MOD_COND_SALDOA_1_ IGNORE ');
  QrSaldoA.SQL.Add('');
  GeraParteSQL_SaldoA(FTabLctA, FEntidTXT, FldIni);
  QrSaldoA.SQL.Add(';');
  QrSaldoA.SQL.Add('');
  QrSaldoA.SQL.Add('SELECT SUM(Inicial) Inicial, SUM(SALDO) SALDO');
  QrSaldoA.SQL.Add('FROM _MOD_COND_SALDOA_1_;');
  QrSaldoA.SQL.Add('');
  // N�o pode !!
  //QrSaldoA.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_SALDOA_1_;');
  QrSaldoA.SQL.Add('');
  //
  UMyMod.AbreQuery(QrSaldoA, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
  // Deve ser ap�s SaldoA
  QrResumo.Close;
  QrResumo.SQL.Clear;
  //
  QrResumo.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_RESUMO_1_;');
  QrResumo.SQL.Add('CREATE TABLE _MOD_COND_RESUMO_1_ IGNORE ');
  QrResumo.SQL.Add('');
  GeraParteSQL_Resumo(FTabLctA);
  QrResumo.SQL.Add('UNION');
  GeraParteSQL_Resumo(FTabLctB);
  QrResumo.SQL.Add('UNION');
  GeraParteSQL_Resumo(FTabLctD);
  QrResumo.SQL.Add(';');
  QrResumo.SQL.Add('');
  QrResumo.SQL.Add('SELECT SUM(Credito) Credito, -SUM(Debito) Debito ');
  QrResumo.SQL.Add('FROM _MOD_COND_RESUMO_1_;');
  QrResumo.SQL.Add('');
  // N�o pode !!
  //QrResumo.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_RESUMO_1_;');
  QrResumo.SQL.Add('');
  //
  UMyMod.AbreQuery(QrResumo, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
  //
  Screen.Cursor := crDefault;
  //
end;

procedure TFmReceDespFree2.EdEmpresaChange(Sender: TObject);
begin
  CkPlanoContas.Checked := False;
  FechaPesquisa(Sender);
  QrUHs.Close;
  FEntidade := DModG.QrEmpresasCodigo.Value;
  FEntidTXT := FormatFloat('0', FEntidade);
  FEmpresa  := DModG.QrEmpresasFilial.Value;
  FEmprTXT  := FormatFloat('0', FEmpresa);

  DModG.Def_EM_ABD(TMeuDB,
    FEntidade, FEmpresa, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  //
{****
  CBUH.KeyValue := Null;
  if (VAR_KIND_DEPTO = kdUH) and (FEmpresa > 0) then
  begin
    QrUHs.Params[0].AsInteger := FEmpresa;
    UnDmkDAC_PF.AbreQuery(QrUHs, Dmod.MyDB);
  end;
}
end;

procedure TFmReceDespFree2.frxReceDesp_GetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then
  begin
    if CkEmissao.Checked then
      Value := '[Emiss�o: ' +
      FormatDateTime(VAR_FORMATDATE2, TPEmissIni.Date) + ' at� ' +
      FormatDateTime(VAR_FORMATDATE2, TPEmissFim.Date) +'] '
    else Value := '[Emiss�o indefinida] ';
  end else if AnsiCompareText(VarName, 'VARF_NOMECOND') = 0 then
    Value := CBEmpresa.Text
  else if AnsiCompareText(VarName, 'VARF_CONTA_SEL') = 0 then
    Value := '?'//CBConta.Text
  else if AnsiCompareText(VarName, 'VARF_UH') = 0 then
  begin
    Value := '?'
    {
    if CBUH.KeyValue = Null then
    begin
      if VAR_KIND_DEPTO = kdUH
        Value := 'TODAS UHs'
      else
        Value := '';
    end else
      Value := CBUH.Text;
    }
  end
  else if AnsiCompareText(VarName, 'VARF_MOSTRA_SUMMARY1') = 0 then
    Value := RGRelatorio.ItemIndex = 0
  else  
end;

procedure TFmReceDespFree2.MoveItensListBox(ListBox: TListBox; Cima: Boolean);
var
  NovoIndex: Integer;
  Executa: Boolean;
begin
  if Cima then
    Executa := ListBox.ItemIndex > 0
  else
    Executa := ListBox.ItemIndex < ListBox.Items.Count - 1;
  if Executa then
  begin
    if Cima then
      NovoIndex := ListBox.ItemIndex - 1
    else
      NovoIndex := ListBox.ItemIndex + 1;
    ListBox.Items.Move(ListBox.ItemIndex, NovoIndex);
    ListBox.ItemIndex := NovoIndex;
    ListBox.SetFocus;
  end;
end;

function TFmReceDespFree2.NomeDoNivelSelecionado(Nivel, Tipo: Integer): String;
begin
  case Tipo of
    1:
    case Nivel of
      1: Result := 'contas';
      2: Result := 'subgrupos';
      3: Result := 'grupos';
      4: Result := 'conjuntos';
      5: Result := 'plano';
      else Result := '?';
    end;
    2:
    case Nivel of
      1: Result := 'lct.Genero';
      2: Result := 'cta.Subgrupo';
      3: Result := 'sgr.Grupo';
      4: Result := 'gru.Conjunto';
      5: Result := 'cjt.Plano';
      else Result := '?';
    end;
    3:
    case Nivel of
      1: Result := 'da Conta';
      2: Result := 'do Sub-grupo';
      3: Result := 'do Grupo';
      4: Result := 'do Conjunto';
      5: Result := 'do Plano';
      else Result := ' do Nivel ? ';
    end;
    else Result := '(Tipo)=?';
  end;
end;

function TFmReceDespFree2.ObtemNivel(const ACol: Integer): Integer;
begin
  case Acol of
    2: Result := 5;
    3: Result := 4;
    4: Result := 3;
    5: Result := 2;
    6: Result := 1;
    else Result := 0;
  end;
end;

function TFmReceDespFree2.ObtemNomeNivel(Nivel: Integer): String;
begin
  case Nivel of
    1: Result := 'genero (conta)';
    2: Result := 'sub-grupo';
    3: Result := 'grupo';
    4: Result := 'conjunto';
    5: Result := 'plano';
    else Result := '????';
  end;
end;

procedure TFmReceDespFree2.Pesquisa();
var
  DtEmi, DtVct, DtDoc, DtQit, Cart, Clie, Forn, NivelC, NivelD: String;
  //
  function GeraParteSQL(TabLct, Nivel: String): String;
  begin
    Result := Geral.ATS([
    '',
    'SELECT pla.OrdemLista OL_PLA, cjt.Plano, pla.Nome NO_PLA, ',
    'cjt.OrdemLista OL_CJT, gru.Conjunto, cjt.Nome NO_CJT, ',
    'gru.OrdemLista OL_GRU, sgr.Grupo, gru.Nome NO_GRU, ',
    'sgr.OrdemLista OL_SGR, cta.SubGrupo, sgr.Nome NO_SGR, ',
    'cta.OrdemLista OL_CTA, lct.Genero, cta.Nome NO_CTA, ',
    'lct.Genero, IF(lct.Credito > lct.Debito, 1, 2) CreDeb, ',
    'SUM(lct.Credito - lct.Debito) Valor, ',
    '1 Ativ5, 1 Ativ4, 1 Ativ3, 1 Ativ2, 1 Ativ1, 1 Ativo ',
    'FROM ' + TabLct + ' lct ',  
    'LEFT JOIN ' + TMeuDB + '.contas cta ON cta.Codigo=lct.Genero ',
    'LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=cta.SubGrupo ',
    'LEFT JOIN ' + TMeuDB + '.grupos gru ON gru.Codigo=sgr.Grupo ',
    'LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto ',
    'LEFT JOIN ' + TMeuDB + '.plano pla ON pla.Codigo=cjt.Plano ',
    'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira ',
    'WHERE lct.Genero > 0 ',
    Nivel,
    DtEmi,
    DtVct,
    DtDoc,
    DtQit,
    Cart,
    Clie,
    Forn,
    'AND car.Tipo IN (0,1) ',
    'GROUP BY lct.Genero ',
    '']);
  end;
var
  Ini, Fim, xA, xB: String;
  Carteira: Integer;
begin
  NivelC := 'AND lct.Credito - lct.Debito > 0';
  if RGCredNivel.ItemIndex > 0 then
  begin
    if MyObjects.FIC(EdCredNivelSel.ValueVariant = 0, EdCredNivelSel,
    'Informe o g�nero do n�vel selecionado para receitas') then Exit;
    xA := NomeDoNivelSelecionado(RGCredNivel.ItemIndex, 2);
    xB := FormatFloat('0', EdCredNivelSel.ValueVariant);
    NivelC := NivelC + sLineBreak + 'AND ' + xA + '=' + xB;
  end;
  //
  NivelD := 'AND lct.Credito - lct.Debito < 0';
  if RGDebiNivel.ItemIndex > 0 then
  begin
    if MyObjects.FIC(EdDebiNivelSel.ValueVariant = 0, EdDebiNivelSel,
    'Informe o g�nero do n�vel selecionado para despesas') then Exit;
    xA := NomeDoNivelSelecionado(RGDebiNivel.ItemIndex, 2);
    xB := FormatFloat('0', EdDebiNivelSel.ValueVariant);
    NivelD := NivelD + sLineBreak + 'AND ' + xA + '=' + xB;
  end;
  //
  FReceDesp :=
    UCriarFin.RecriaTempTableNovo(ntrtt_ReceDesp, DModG.QrUpdPID1, False);
  //
  // EMISS�O
  if CkEmissao.Checked then
  begin
    Ini := FormatDateTime(VAR_FORMATDATE, TPEmissIni.Date);
    Fim := FormatDateTime(VAR_FORMATDATE, TPEmissFim.Date);
    DtEmi := 'AND lct.Data BETWEEN '''+Ini+''' AND '''+Fim+'''';
  end else DtEmi := '';

  if PnDatas2.Visible then
  begin
    // VENCIMENTO
    if CkVencto.Checked then
    begin
      Ini := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date);
      Fim := FormatDateTime(VAR_FORMATDATE, TPVctoFim.Date);
      DtVct := 'AND lct.Vencimento BETWEEN '''+Ini+''' AND '''+Fim+'''';
    end else DtVct := '';

    // DATA DO DOCUMENTO
    if CkDataDoc.Checked then
    begin
      Ini := FormatDateTime(VAR_FORMATDATE, TPDocIni.Date);
      Fim := FormatDateTime(VAR_FORMATDATE, TPDocFim.Date);
      DtDoc := 'AND lct.DataDoc BETWEEN '''+Ini+''' AND '''+Fim+'''';
    end else DtDoc := '';

    // COMPENSA��O
    if CkDataComp.Checked then
    begin
      Ini := FormatDateTime(VAR_FORMATDATE, TPCompIni.Date);
      Fim := FormatDateTime(VAR_FORMATDATE, TPCompFim.Date);
      DtQit := 'AND lct.Compensado BETWEEN '''+Ini+''' AND '''+Fim+'''';
    end else DtQit := '';
  end;

  // CARTEIRA
  Carteira := Geral.IMV(EdCarteira.Text);
  if Carteira <> 0 then
    Cart := 'AND lct.Carteira='''+IntToStr(Carteira)+''''
  else
    Cart := '';

  // CLIENTE E FORNECEDOR
  Clie := dmkPF.SQL_CodTxt('AND lct.Cliente',
    EdCliente.Text, False, True, False);
  Forn := dmkPF.SQL_CodTxt('AND lct.Fornecedor',
    EdFornece.Text, False, True, False);

  {
  // COMPET�NCIA
  if CkCompetencia.Checked and CkCompetencia.Enabled then
  begin
    AnoI := Geral.IMV(CBAnoIni.Text);
    AnoF := Geral.IMV(CBAnoFim.Text);
    MesI := CBMesIni.ItemIndex + 1;
    MesF := CBMesFim.ItemIndex + 1;
    CIni := IntToStr(((AnoI-2000)*100)+MesI);
    CFim := IntToStr(((AnoF-2000)*100)+MesF);
    QrLct1.SQL.Add('AND lct.Mez BETWEEN '''+CIni+''' AND '''+CFim+'''');
  end;
  }
  //
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
  'INSERT INTO ' + FReceDesp,
  GeraParteSQL(FtabLctA, NivelC),
  'UNION',
  GeraParteSQL(FtabLctB, NivelC),
  'UNION',
  GeraParteSQL(FtabLctD, NivelC),
  'UNION',
  GeraParteSQL(FtabLctA, NivelD),
  'UNION',
  GeraParteSQL(FtabLctB, NivelD),
  'UNION',
  GeraParteSQL(FtabLctD, NivelD),
  '']);
  //
  ReopenCred('', 0);
  ReopenDebi('', 0);
  //
end;

{****
procedure TFmReceDespFree.HistoricoConta();
  procedure GeraParteSQL(TabLct, CliInt, Genero, UH: String);
  begin
    QrDC.SQL.Add('SELECT lan.Genero, lan.Mez, lan.Depto, ');
    QrDC.SQL.Add('lan.Credito-lan.Debito Valor, con.OrdemLista, ');
    QrDC.SQL.Add('lan.Controle, con.Nome NOMECON, ');
    if VAR_KIND_DEPTO = kdUH
      QrDC.SQL.Add('cim.Unidade UNIDADE')
    else
      QrDC.SQL.Add('"" UNIDADE');
    QrDC.SQL.Add('FROM ' + TabLct + ' lan');
    QrDC.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QrDC.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lan.Genero');
    if VAR_KIND_DEPTO = kdUH
      QrDC.SQL.Add('LEFT JOIN ' + TMeuDB + '.condimov  cim ON cim.Conta=lan.Depto');
    QrDC.SQL.Add('WHERE lan.Tipo <> 2');
    QrDC.SQL.Add('AND lan.Genero>0');
    QrDC.SQL.Add('AND car.ForneceI = ' + CliInt);

    case CGValores.Value of
      1: QrDC.SQL.Add('AND lan.Credito > lan.Debito');
      2: QrDC.SQL.Add('AND lan.Debito > lan.Credito');
    end;
    if FGenero > 0 then
      QrDC.SQL.Add('AND Genero=' + Genero);
    if FUH > 0 then
      QrDC.SQL.Add('AND Depto=' + UH);
    QrDC.SQL.Add('AND lan.Data BETWEEN "' + FDataI1 + '" AND "' + FDataF2 + '"');
  end;
var
  CliInt, Genero, UH: String;
begin
  CliInt := FormatFloat('0', FEntidade);
  Genero := FormatFloat('0', FGenero);
  UH     := FormatFloat('0', FUH);
  //
  QrDC.Close;
  QrDC.SQL.Clear;
  //
  QrDC.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_010_DC_;');
  QrDC.SQL.Add('CREATE TABLE _FIN_RELAT_010_DC_ IGNORE ');
  QrDC.SQL.Add('');
  GeraParteSQL(FTabLctA, CliInt, Genero, UH);
  QrDC.SQL.Add('UNION');
  GeraParteSQL(FTabLctB, CliInt, Genero, UH);
  QrDC.SQL.Add('UNION');
  GeraParteSQL(FTabLctD, CliInt, Genero, UH);
  QrDC.SQL.Add(';');
  QrDC.SQL.Add('');
  QrDC.SQL.Add('SELECT * FROM _FIN_RELAT_010_DC_');
  case RGOrdem.ItemIndex of
    0: QrDC.SQL.Add('ORDER BY OrdemLista, NOMECON, Mez, Depto;');
    1: QrDC.SQL.Add('ORDER BY OrdemLista, NOMECON, Depto, Mez;');
  end;
  QrDC.SQL.Add('');
  QrDC.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_010_DC_;');
  QrDC.SQL.Add('');
  UnDmkDAC_PF.AbreQuery(QrDC, Dmod.MyDB);
  //
  case RGOrdem.ItemIndex of
    0: MyObjects.frxMostra(frxDC_Mes, 'Hist�rico de Conta (Plano de contas) por M�s');
    1: MyObjects.frxMostra(frxDC_Uni, 'Hist�rico de Conta (Plano de contas) por Unidade');
  end;
end;
}

procedure TFmReceDespFree2.QrCredAfterOpen(DataSet: TDataSet);
begin
{$IfDef cAdvGrid}
  MyObjects.UpdMergeDBAvGrid(AdvGridCred, FColsToMerge);
{$EndIf}
end;

procedure TFmReceDespFree2.QrCreditosCalcFields(DataSet: TDataSet);
var
  SubPgto1: String;
begin
  if QrCreditosSubPgto1.Value = 0 then SubPgto1 := ''
    else SubPgto1 := ' RECEB. ACORDO EXTRAJUDICIAL';
  //
  QrCreditosNOMECON_2.Value := QrCreditosNOMECON.Value + SubPgto1;
  //
  QrCreditosMES.Value := dmkPF.MezToMesEAno(QrCreditosMez.Value);
  QrCreditosMES2.Value := dmkPF.MezToMesEAnoCurto(QrCreditosMez.Value);
  //
  QrCreditosSERIE_DOC.Value := Trim(QrCreditosSerieCH.Value);
  if QrCreditosDocumento.Value <> 0 then QrCreditosSERIE_DOC.Value :=
    QrCreditosSERIE_DOC.Value + FormatFloat('000000', QrCreditosDocumento.Value);
  //
  QrCreditosNF_TXT.Value := FormatFloat('000000;-000000; ', QrCreditosNotaFiscal.Value);
  //
end;

procedure TFmReceDespFree2.QrDCCalcFields(DataSet: TDataSet);
begin
   QrDCNOMEMES.Value := dmkPF.MezToFDT(QrDCMez.Value, 0, 14);
end;

procedure TFmReceDespFree2.QrDebiAfterOpen(DataSet: TDataSet);
begin
{$IfDef cAdvGrid}
  MyObjects.UpdMergeDBAvGrid(AdvGridDebi, FColsToMerge);
{$EndIf}
end;

procedure TFmReceDespFree2.QrDebitosCalcFields(DataSet: TDataSet);
begin
  QrDebitosMES.Value := dmkPF.MezToMesEAno(QrDebitosMez.Value);
  QrDebitosMES2.Value := dmkPF.MezToMesEAnoCurto(QrDebitosMez.Value);
  //
  QrDebitosSERIE_DOC.Value := Trim(QrDebitosSerieCH.Value);
  if QrDebitosDocumento.Value <> 0 then QrDebitosSERIE_DOC.Value :=
    QrDebitosSERIE_DOC.Value + FormatFloat('000000', QrDebitosDocumento.Value);
  //
  QrDebitosNF_TXT.Value := FormatFloat('000000;-000000; ', QrDebitosNotaFiscal.Value);
  //
end;

procedure TFmReceDespFree2.QrResumoCalcFields(DataSet: TDataSet);
begin
  QrResumoSALDO.Value := QrResumoCredito.Value - QrResumoDebito.Value;
  QrResumoFINAL.Value := QrResumoSALDO.Value + QrSaldoATOTAL.Value;
end;

procedure TFmReceDespFree2.QrSaldoACalcFields(DataSet: TDataSet);
begin
  QrSaldoATOTAL.Value := QrSaldoAInicial.Value + QrSaldoASALDO.Value;
end;

procedure TFmReceDespFree2.RGRelatorioClick(Sender: TObject);
begin
  FechaPesquisa(Sender);
  PnDatas2.Visible := RGRelatorio.ItemIndex > 1;
{ TODO :          FAZER } // ocultar saldos nos relatorios!
end;

procedure TFmReceDespFree2.ReopenCred(Campo: String; Genero: Integer);
begin
   ReopenDbCr(siPositivo, Campo, Genero);
end;

procedure TFmReceDespFree2.ReopenCreditosEDebitos(CliInt: String; DataI,
              DataF: TDateTime; SelPeriodoEmissao, CredAcordos, DebiAcordos,
              CredPeriodos, DebiPeriodos, CredTextos, DebiTextos(*,
              NaoAgruparNada*): Boolean; TabLctA, TabLctB, TabLctD: String);
  function OrdenaItens(ListBox: TListBox; Receitas: Boolean): String;
    function TraduzCampos(Texto: String; Receitas: Boolean): String;
    begin
      if Receitas then
      begin
        ///////////////////////////////////////////////////////////////////
        //GRU_OL  = Ordem do Grupo (Cadastro de grupos)
        //NOMEGRU = Nome do Grupo
        //SGR_OL  = Ordem do subgrupo (cadastro de subgrupos)
        //NOMESGR = Nome do Subgrupo
        //CON_OL  = Ordem da Conta (Cadastro de contas)
        //NOMECON = Nome da Conta
        //Mez     = M�s de compet�ncia
        //Data    = Data do Lan�amento
        ///////////////////////////////////////////////////////////////////
        if      LowerCase(Texto) = 'ordem do grupo (cadastro de grupos)' then
          Result := 'GRU_OL'
        else if LowerCase(Texto) = 'nome do grupo' then
          Result := 'NOMEGRU'
        else if LowerCase(Texto) = 'ordem do subgrupo (cadastro de subgrupos)' then
          Result := 'SGR_OL'
        else if LowerCase(Texto) = 'nome do subgrupo' then
          Result := 'NOMESGR'
        else if LowerCase(Texto) = 'ordem da conta (cadastro de contas)' then
          Result := 'CON_OL'
        else if LowerCase(Texto) = 'nome da conta' then
          Result := 'NOMECON'
        else if LowerCase(Texto) = 'm�s de compet�ncia' then
          Result := 'Mez'
        else if LowerCase(Texto) = 'data do lan�amento' then
          Result := 'Data'
        else
          Result := '';
      end else
      begin
        ///////////////////////////////////////////////////////////////////
        //GRU_OL  = Ordem do Grupo (Cadastro de grupos)
        //NOMEGRU = Nome do Grupo
        //Ordem do subgrupo (cadastro de subgrupos)
        //NOMESGR = Nome do Subgrupo
        //CON_OL  = Ordem da Conta (Cadastro de contas)
        //NOMECON = Nome da Conta
        //MEZ     = M�s de compet�ncia
        //Data    = Data do Lan�amento      
        ///////////////////////////////////////////////////////////////////
        if      LowerCase(Texto) = 'ordem do grupo (cadastro de grupos)' then
          Result := 'GRU_OL'
        else if LowerCase(Texto) = 'nome do grupo' then
          Result := 'NOMEGRU'
        else if LowerCase(Texto) = 'ordem do subgrupo (cadastro de subgrupos)' then
          Result := 'SGR_OL'
        else if LowerCase(Texto) = 'nome do subgrupo' then
          Result := 'NOMESGR'
        else if LowerCase(Texto) = 'ordem da conta (cadastro de contas)' then
          Result := 'CON_OL'
        else if LowerCase(Texto) = 'nome da conta' then
          Result := 'NOMECON'
        else if LowerCase(Texto) = 'm�s de compet�ncia' then
          Result := 'MEZ'
        else if LowerCase(Texto) = 'data do lan�amento' then
          Result := 'Data'
        else
          Result := '';
      end;
    end;
  var
    Campo, Ordem: String;
    i, TotReg: Integer;
  begin
    Ordem  := '';
    TotReg := ListBox.Count - 1;
    for i := 0 to TotReg do
    begin
      if i = 0 then
      begin
        Campo := TraduzCampos(ListBox.Items[i], Receitas);
        if Length(Campo) > 0 then
          Ordem := Campo + ', ';
      end
      else if i = TotReg then
      begin
        Campo := TraduzCampos(ListBox.Items[i], Receitas);
        if Length(Campo) > 0 then
          Ordem := Ordem + Campo + ';';
      end
      else
      begin
        Campo := TraduzCampos(ListBox.Items[i], Receitas);
        if Length(Campo) > 0 then
          Ordem := Ordem + Campo + ', ';
      end;
    end;
    Result := Ordem;
  end;

  procedure GeraParteSQL_Cred(TabLct: String);
  begin
    if CredTextos then
    begin
      QrCreditos.SQL.Add('SELECT IF(COUNT(lct.Compensado) > 1, "V�rias", IF(lct.Compensado<2,"",');
      QrCreditos.SQL.Add('DATE_FORMAT(lct.Compensado, "%d/%m/%y"))) COMPENSADO_TXT,');
      QrCreditos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.NotaFiscal) NOTAFISCAL,');
      QrCreditos.SQL.Add('IF(COUNT(lct.Data)>1, "", lct.SerieCH) SERIECH,');
      QrCreditos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.Documento) DOCUMENTO,');
    end else
    begin
      QrCreditos.SQL.Add('SELECT IF(lct.Compensado<2,"", DATE_FORMAT(');
      QrCreditos.SQL.Add('lct.Compensado, "%d/%m/%y")) COMPENSADO_TXT,');
      QrCreditos.SQL.Add('IF(lct.NotaFiscal = 0, 0, lct.NotaFiscal) NOTAFISCAL,');
      QrCreditos.SQL.Add('IF(lct.SerieCH = "", "", lct.SerieCH) SERIECH,');
      QrCreditos.SQL.Add('IF(lct.Documento = 0, 0, lct.Documento) DOCUMENTO,');
    end;
    QrCreditos.SQL.Add('lct.Mez, lct.Credito, "' +
                       Uppercase(Copy(TabLct, Length(TabLct))) + '" TbLct, ');
    QrCreditos.SQL.Add('lct.Descricao, ');
    QrCreditos.SQL.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_CLIENTE, ');
    QrCreditos.SQL.Add('lct.Controle, lct.Sub, lct.Carteira, lct.Cartao, lct.Tipo,');
    QrCreditos.SQL.Add('lct.Vencimento, lct.Compensado, lct.Sit, lct.Genero, ');
    QrCreditos.SQL.Add('lct.SubPgto1, lct.Data, gru.OrdemLista GRU_OL, ');
    QrCreditos.SQL.Add('con.OrdemLista CON_OL, sgr.OrdemLista SGR_OL,');
    QrCreditos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU');
    QrCreditos.SQL.Add('FROM ' + TabLct + ' lct');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lct.Genero');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades frn ON frn.Codigo=lct.Cliente');
    QrCreditos.SQL.Add('WHERE lct.Tipo <> 2');
    QrCreditos.SQL.Add('AND lct.Credito > 0');
    //QrCreditos.SQL.Add('AND lct.Genero>0');
    QrCreditos.SQL.Add('AND lct.Genero IN(');
    QrCreditos.SQL.Add('    SELECT cO1');
    QrCreditos.SQL.Add('    FROM ' + FReceDesp);
    QrCreditos.SQL.Add('    WHERE Ativo=1');
    QrCreditos.SQL.Add(')');
    //
    QrCreditos.SQL.Add('AND lct.Sit IN (' + CO_LIST_SITS_OKA + ')');
    QrCreditos.SQL.Add('AND car.ForneceI=' + FEntidTXT);
    //QrCreditos.SQL.Add('AND lct.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
    QrCreditos.SQL.Add(DmkPF.SQL_Periodo('AND lct.Data ', TPEmissIni.Date,
      TPEmissFim.Date, CkEmissao.Checked, CkEmissao.Checked));
    //  
    QrCreditos.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
{
    if NaoAgruparNada = False then
    begin
      QrCreditos.SQL.Add('GROUP BY lct.Genero ');
      if Periodos then
        QrCreditos.SQL.Add(', lct.Mez');
      if Acordos then
        QrCreditos.SQL.Add(', lct.SubPgto1');
    end;
}
    if CredTextos then
      QrCreditos.SQL.Add('GROUP BY lct.Descricao');
  end;

  procedure GeraParteSQL_Debi(TabLct: String);
  begin
    //if Textos and (NaoAgruparNada = False) then
    if DebiTextos (*and (NaoAgruparNada = False)*) then
    begin
      QrDebitos.SQL.Add('SELECT IF(COUNT(lct.Compensado) > 1, "V�rias", IF(lct.Compensado<2,"",');
      QrDebitos.SQL.Add('DATE_FORMAT(lct.Compensado, "%d/%m/%y"))) COMPENSADO_TXT,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, "V�rias", DATE_FORMAT(lct.Data, "%d/%m/%y")) DATA,');
      QrDebitos.SQL.Add('lct.Descricao, SUM(lct.Debito) DEBITO,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.NotaFiscal) NOTAFISCAL,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, "", lct.SerieCH) SERIECH,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.Documento) DOCUMENTO,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.Mez) MEZ, lct.Compensado,');
      QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
      QrDebitos.SQL.Add('COUNT(lct.Data) ITENS, "" TbLct, ');
    end else begin
      QrDebitos.SQL.Add('SELECT IF(lct.Compensado<2,"", DATE_FORMAT(');
      QrDebitos.SQL.Add('lct.Compensado, "%d/%m/%y")) COMPENSADO_TXT,');
      QrDebitos.SQL.Add('DATE_FORMAT(lct.Data, "%d/%m/%y") DATA, ');
      QrDebitos.SQL.Add('lct.Descricao, lct.Debito, ');
      QrDebitos.SQL.Add('IF(lct.NotaFiscal = 0, 0, lct.NotaFiscal) NOTAFISCAL,');
      QrDebitos.SQL.Add('IF(lct.SerieCH = "", "", lct.SerieCH) SERIECH,');
      QrDebitos.SQL.Add('IF(lct.Documento = 0, 0, lct.Documento) DOCUMENTO,');
      QrDebitos.SQL.Add('IF(lct.Mez = 0, 0, lct.Mez) MEZ, lct.Compensado,');
      QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
      QrDebitos.SQL.Add('0 ITENS, "' + Uppercase(Copy(TabLct, Length(TabLct))) + '" TbLct, ')
    end;
    QrDebitos.SQL.Add('lct.Vencimento, lct.Sit, lct.Genero, ');
    QrDebitos.SQL.Add('lct.Tipo, lct.Controle, lct.Sub, lct.Carteira, lct.Cartao,');
    QrDebitos.SQL.Add('gru.OrdemLista GRU_OL, con.OrdemLista CON_OL, ');
    QrDebitos.SQL.Add('sgr.OrdemLista SGR_OL, IF(frn.Codigo=0, "", ');
    QrDebitos.SQL.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)) NO_FORNECE');
    QrDebitos.SQL.Add('FROM ' + TabLct + ' lct');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lct.Genero');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades frn ON frn.Codigo=lct.Fornecedor');
    QrDebitos.SQL.Add('WHERE lct.Tipo <> 2');
    QrDebitos.SQL.Add('AND lct.Debito > 0');
    //QrDebitos.SQL.Add('AND lct.Genero>0');
    QrDebitos.SQL.Add('AND lct.Genero IN(');
    QrDebitos.SQL.Add('    SELECT cO1');
    QrDebitos.SQL.Add('    FROM ' + FReceDesp);
    QrDebitos.SQL.Add('    WHERE Ativo=1');
    QrDebitos.SQL.Add(')');
    QrDebitos.SQL.Add('AND lct.Sit IN (' + CO_LIST_SITS_OKA + ')');
    QrDebitos.SQL.Add('AND car.ForneceI=' + FEntidTXT);
    //QrDebitos.SQL.Add('AND lct.Data BETWEEN "' + FDataI1 + '" AND "' + FDataF1 + '"');
    QrDebitos.SQL.Add(DmkPF.SQL_Periodo('AND lct.Data ', TPEmissIni.Date,
      TPEmissFim.Date, CkEmissao.Checked, CkEmissao.Checked));
    //  
    QrDebitos.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
    //
    {
    if CkNaoAgruparNada.Checked = False then
    begin
    }
      if DebiTextos then
        QrDebitos.SQL.Add('GROUP BY lct.Descricao');
    //end;
  end;
begin
  QrCreditos.Close;
  QrDebitos.Close;
  QrCreditos.Database := DModG.MyPID_DB;
  QrDebitos.Database  := DModG.MyPID_DB;
  //
  //// C R � D I T O S
  if CkAnaliCred.Checked then
  begin
    QrCreditos.SQL.Clear;
    //
    QrCreditos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_CRED_1_;');
    QrCreditos.SQL.Add('CREATE TABLE _MOD_COND_CRED_1_ IGNORE ');
    QrCreditos.SQL.Add('');
    GeraParteSQL_Cred(TabLctA);
    QrCreditos.SQL.Add('UNION');
    GeraParteSQL_Cred(TabLctB);
    QrCreditos.SQL.Add('UNION');
    GeraParteSQL_Cred(TabLctD);
    QrCreditos.SQL.Add(';');
    QrCreditos.SQL.Add('');
    //if NaoAgruparNada = False then
    if (CredPeriodos = False)  or (CredAcordos = False) then
    begin
      //QrCreditos.SQL.Add('SELECT Mez, SUM(Credito) Credito, Controle, Sub, ');
      QrCreditos.SQL.Add('SELECT COMPENSADO_TXT, NOTAFISCAL, SERIECH, ');
      QrCreditos.SQL.Add('NO_CLIENTE, DOCUMENTO,');
      QrCreditos.SQL.Add('Mez, SUM(Credito) Credito, Controle, Sub, Descricao, ');
      //
      QrCreditos.SQL.Add('Carteira, Cartao, Tipo, Vencimento, Compensado, Sit,');
      QrCreditos.SQL.Add('Genero, SubPgto1, Data, GRU_OL, CON_OL, SGR_OL, ');
      QrCreditos.SQL.Add('NOMECON, NOMESGR, NOMEGRU, TbLct');
      QrCreditos.SQL.Add('FROM _MOD_COND_CRED_1_');
      QrCreditos.SQL.Add('GROUP BY Genero ');
      if CredPeriodos = False then
        QrCreditos.SQL.Add(', Mez');
      if CredAcordos = False then
        QrCreditos.SQL.Add(', SubPgto1');
    end else
      QrCreditos.SQL.Add('SELECT * FROM _MOD_COND_CRED_1_');
    {
    QrCreditos.SQL.Add('ORDER BY GRU_OL, NOMEGRU, SGR_OL, ');
    QrCreditos.SQL.Add('NOMESGR, CON_OL, NOMECON, Mez, Data;');
    }
    QrCred.SQL.Add('ORDER BY ' + OrdenaItens(LBReceitas, True));
    QrCreditos.SQL.Add('');
    // N�o pode !!
    //QrCreditos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_CRED_1_;');
    QrCreditos.SQL.Add('');
    //
    UMyMod.AbreQuery(QrCreditos, DModG.MyPID_DB, 'TDCond.ReopenCreditosEDebitos()');
  end;
  //
  //
  //// D � B I T O S
  if CkAnaliDebi.Checked then
  begin
    QrDebitos.SQL.Clear;
    //
    QrDebitos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_DEBI_1_;');
    QrDebitos.SQL.Add('CREATE TABLE _MOD_COND_DEBI_1_ IGNORE ');
    QrDebitos.SQL.Add('');
    GeraParteSQL_Debi(TabLctA);
    QrDebitos.SQL.Add('UNION');
    GeraParteSQL_Debi(TabLctB);
    QrDebitos.SQL.Add('UNION');
    GeraParteSQL_Debi(TabLctD);
    QrDebitos.SQL.Add(';');
    QrDebitos.SQL.Add('');
    //if (NaoAgruparNada = False) and (Textos) then
    if DebiTextos then
    begin
      QrDebitos.SQL.Add('SELECT COMPENSADO_TXT, DATA,Descricao, NO_FORNECE, ');
      QrDebitos.SQL.Add('SUM(DEBITO) DEBITO, NOTAFISCAL, SERIECH, DOCUMENTO, ');
      QrDebitos.SQL.Add('MEZ, Compensado, NOMECON, NOMESGR, NOMEGRU, ');
      QrDebitos.SQL.Add('ITENS, Vencimento, Sit, Genero, Tipo, Controle,');
      QrDebitos.SQL.Add('Sub, Carteira, Cartao, GRU_OL, CON_OL, SGR_OL, TbLct');
      QrDebitos.SQL.Add('FROM _MOD_COND_DEBI_1_');
      QrDebitos.SQL.Add('GROUP BY Descricao')
    end else
      QrDebitos.SQL.Add('SELECT * FROM _MOD_COND_DEBI_1_');
    {
    QrDebitos.SQL.Add('ORDER BY GRU_OL, NOMEGRU, SGR_OL,');
    QrDebitos.SQL.Add('NOMESGR, CON_OL, NOMECON, Data;');
    }
    QrDebitos.SQL.Add('ORDER BY ' + OrdenaItens(LBDespesas, False));
    //
    // N�o pode !!
    //QrDebitos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_DEBI_1_;');
    UMyMod.AbreQuery(QrDebitos, DModG.MyPID_DB, 'procedure TDmCond.ReopenDEBIitosEDebitos()');
  end;
end;

procedure TFmReceDespFree2.ReopenDebi(Campo: String; Genero: Integer);
begin
  ReopenDbCr(siNegativo, Campo, Genero);
end;

procedure TFmReceDespFree2.ReopenNivelSel(Sinal: TSinal);
var
  NivelX, NivelY: String;
  RG: TRadioGroup;
  Qry: TmySQLQuery;
  EdNivelSel: TDmkEditCB;
  CBNivelSel: TdmkDBLookupComboBox;
  //
  procedure GeraParteSQL(Qry: TmySQLQuery; TabLct: String);
  begin
    Qry.SQL.Add('  SELECT DISTINCT ' + NivelY);
    Qry.SQL.Add('  FROM ' + TabLct + ' lct ');
    Qry.SQL.Add('  LEFT JOIN contas cta ON cta.Codigo=lct.Genero');
    Qry.SQL.Add('  LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo');
    Qry.SQL.Add('  LEFT JOIN grupos gru ON gru.Codigo=sgr.Grupo');
    Qry.SQL.Add('  LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    Qry.SQL.Add('  LEFT JOIN plano pla ON pla.Codigo=cjt.Plano');
    Qry.SQL.Add('  WHERE lct.Genero > 0');
  end;
begin
  Screen.Cursor := crHourGlass;
  try
    if Sinal = siPositivo then
    begin
      RG := RGCredNivel;
      Qry := QrCredNivelSel;
      EdNivelSel := EdCredNivelSel;
      CBNivelSel := CBCredNivelSel;
    end else
    begin
      RG := RGDebiNivel;
      Qry := QrDebiNivelSel;
      EdNivelSel := EdDebiNivelSel;
      CBNivelSel := CBDebiNivelSel;
    end;
    NivelX := NomeDoNivelSelecionado(RG.ItemIndex, 1);
    //
    Qry.Close;
    if RG.ItemIndex > 0 then
    begin
      Qry.SQL.Clear;
      Qry.SQL.Add('SELECT Codigo, Nome');
      Qry.SQL.Add('FROM ' + NivelX);
      if CkPlanoUtiliz.Checked then
      begin
        NivelY := NomeDoNivelSelecionado(RG.ItemIndex, 2);
        //
        Qry.SQL.Add('WHERE codigo IN ( ');
        GeraParteSQL(Qry, FTabLctA);
        Qry.SQL.Add('UNION');
        GeraParteSQL(Qry, FTabLctB);
        Qry.SQL.Add('UNION');
        GeraParteSQL(Qry, FTabLctD);
        Qry.SQL.Add(')');
      end;
      //Qry.SQL.Add('WHERE Codigo > 0');
      Qry.SQL.Add('ORDER BY Nome');
      UMyMod.AbreQuery(Qry, Dmod.MyDB);
    end;
    //
    EdNivelSel.ValueVariant := 0;
    CBNivelSel.KeyValue     := Null;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmReceDespFree2.ReopenDbCr(Sinal: TSinal;
  Campo: String; Genero: Integer);
var
  (*Agrupa,*) Ordem, IniSQL: String;
  Qry: TmySQLQuery;
  Num: Integer;
begin
  if Sinal = siPositivo then
  begin
    Qry := QrCred;
    Num := 1;
  end else
  begin
    Qry := QrDebi;
    Num := 2;
  end;
  IniSQL := Geral.ATS([
    'SELECT oN5, cN5, nN5, Ativ5, ',
    'oN4, cN4, nN4, Ativ4, oN3, cN3, nN3, Ativ3, ',
    'oN2, cN2, nN2, Ativ2, oN1, cN1, nN1, Ativ1, '
    ]);
  Ordem := Geral.ATS([
    'ORDER BY oN5, nN5, oN4, nN4, ',
    'oN3, nN3, oN2, nN2, oN1, nN1 '
    ]);
  if CkAgruCta.Checked then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    IniSQL,
    '-99999999 + 0.000 cO1, CreDeb, SUM(Valor) Valor, Ativo ',
    'FROM ' + FReceDesp,
    SQL_Where(Num),
    'GROUP BY cN5, cN4, cN3, cN2, cN1',
    Ordem,
    '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    IniSQL,
    'cO1 + 0.000 cO1, CreDeb, SUM(Valor) Valor, Ativo ',
    'FROM ' + FReceDesp,
    SQL_Where(Num),
    'GROUP BY cN5, cN4, cN3, cN2, cN1, cO1',
    Ordem,
    '']);
  end;
  if Campo <> '' then
    Qry.Locate(Campo, Genero, []);
end;

procedure TFmReceDespFree2.RGCredNivelClick(Sender: TObject);
begin
  ReopenNivelSel(siPositivo);
  FechaPesquisa(Sender);
end;

procedure TFmReceDespFree2.RGDebiNivelClick(Sender: TObject);
begin
  ReopenNivelSel(siNegativo);
  FechaPesquisa(Sender);
end;

procedure TFmReceDespFree2.RGMostrarClick(Sender: TObject);
begin
  if QrCred.State <> dsInactive then
    ReopenCred('cN1', QrCredcN1.Value);
  if QrDebi.State <> dsInactive then
    ReopenDebi('cN1', QrDebicN1.Value);
end;

function TFmReceDespFree2.SQL_Where(CreDeb: Integer): String;
begin
  case RGMostrar.ItemIndex of
    0: Result := 'Ativo=1';
    1: Result := 'Ativo=0';
    else Result := 'Ativo in (0,1)';
  end;
  Result := 'WHERE CreDeb=' + Geral.FF0(CreDeb) + ' AND ' + Result;
end;

procedure TFmReceDespFree2.BtTesteClick(Sender: TObject);
begin
  EdEmpresa.ValueVariant := 46;
  CBEmpresa.KeyValue := 46;
  //
  RGRelatorio.ItemIndex := 0;
  //
  CkEmissao.Checked := True;
  TPEmissIni.Date := EncodeDate(2011, 01, 01);
  TPEmissFIm.Date := EncodeDate(2012, 04, 30);
  CkVencto.Checked := False;
  CkDataDoc.Checked := False;
  CkDataComp.Checked := False;
  //
  EdCarteira.ValueVariant := 0;
  CBCarteira.KeyValue := Null;
  EdFornece.ValueVariant := 0;
  CBFornece.KeyValue := Null;
  EdCliente.ValueVariant := 0;
  CBCliente.KeyValue := Null;
  //
  CkPlanoContas.Checked := True;
  CkPlanoUtiliz.Checked := False;
  RGCredNivel.ItemIndex := 3;
  EdCredNivelSel.ValueVariant := 3;
  CBDebiNivelSel.KeyValue := 3;
  RGDebiNivel.ItemIndex := 3;
  EdDebiNivelSel.ValueVariant := 3;
  CBDebiNivelSel.KeyValue := 3;
  //
  BtOKClick(Self);
end;

procedure TFmReceDespFree2.FechaPesquisa(Sender: TObject);
begin
  QrCred.Close;
  QrDebi.Close;
end;

end.

