unit Pesquisas2;
// ver impress�o em unit RepasImp;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Db, (*DBTables,*) DBCtrls, ComCtrls,
  UnInternalConsts, UnMsgInt, UnGOTOy, Variants, mySQLDbTables, Grids, DBGrids,
  frxClass, frxDBSet, dmkGeral, dmkEdit, dmkDBLookupComboBox, dmkEditCB,
  dmkImage, dmkEditDateTimePicker, UnDmkProcFunc, UnDmkEnums;

type
  TFmPesquisas2 = class(TForm)
    PainelDados: TPanel;
    QrNivelSel: TmySQLQuery;
    DsNivelSel: TDataSource;
    QrLct1: TmySQLQuery;
    QrLct1Data: TDateField;
    QrLct1Tipo: TSmallintField;
    QrLct1Carteira: TIntegerField;
    QrLct1Sub: TSmallintField;
    QrLct1Autorizacao: TIntegerField;
    QrLct1Genero: TIntegerField;
    QrLct1Descricao: TWideStringField;
    QrLct1NotaFiscal: TIntegerField;
    QrLct1Debito: TFloatField;
    QrLct1Credito: TFloatField;
    QrLct1Compensado: TDateField;
    QrLct1Documento: TFloatField;
    QrLct1Sit: TIntegerField;
    QrLct1Vencimento: TDateField;
    QrLct1Lk: TIntegerField;
    QrLct1FatID: TIntegerField;
    QrLct1FatParcela: TIntegerField;
    QrLct1Fatura: TWideStringField;
    QrLct1Banco: TIntegerField;
    QrLct1Local: TIntegerField;
    QrLct1Cartao: TIntegerField;
    QrLct1Linha: TIntegerField;
    QrNivelSelCodigo: TIntegerField;
    QrNivelSelNome: TWideStringField;
    QrLct1NOMETIPO: TWideStringField;
    QrLct1NOMEGENERO: TWideStringField;
    QrLct1NOMECARTEIRA: TWideStringField;
    QrLct1Ano: TFloatField;
    QrLct1MENSAL: TWideStringField;
    QrLct1MENSAL2: TWideStringField;
    QrLct1ID_Sub: TSmallintField;
    QrLct1Pago: TFloatField;
    QrLct1Mez: TIntegerField;
    QrLct1Controle: TIntegerField;
    QrLct1ID_Pgto: TIntegerField;
    QrLct1Mes2: TLargeintField;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    DsLct1: TDataSource;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrEntidades: TmySQLQuery;
    DsClientes: TDataSource;
    DsFornecedores: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrLct1OperCount: TIntegerField;
    QrLct1Lancto: TIntegerField;
    QrLct1Fornecedor: TIntegerField;
    QrLct1Cliente: TIntegerField;
    QrLct1MoraDia: TFloatField;
    QrLct1Multa: TFloatField;
    QrLct1Protesto: TDateField;
    QrLct1DataCad: TDateField;
    QrLct1DataAlt: TDateField;
    QrLct1UserCad: TIntegerField;
    QrLct1UserAlt: TIntegerField;
    QrLct1DataDoc: TDateField;
    QrLct1CtrlIni: TIntegerField;
    QrLct1Nivel: TIntegerField;
    QrLct1Vendedor: TIntegerField;
    QrLct1Account: TIntegerField;
    QrLct1FatID_Sub: TIntegerField;
    QrLct1ICMS_P: TFloatField;
    QrLct1ICMS_V: TFloatField;
    QrLct1Duplicata: TWideStringField;
    QrLct1CliInt: TIntegerField;
    QrLct1Depto: TIntegerField;
    QrLct1DescoPor: TIntegerField;
    QrLct1ForneceI: TIntegerField;
    QrLct1Qtde: TFloatField;
    QrLct1Emitente: TWideStringField;
    QrLct1ContaCorrente: TWideStringField;
    QrLct1CNPJCPF: TWideStringField;
    QrLct1DescoVal: TFloatField;
    QrLct1DescoControle: TIntegerField;
    QrLct1NFVal: TFloatField;
    QrLct1Antigo: TWideStringField;
    QrLct1NOMEENTIDADE: TWideStringField;
    QrLct1CREDEB: TFloatField;
    QrLct1FatNum: TFloatField;
    frxDsLct1: TfrxDBDataset;
    QrCarteirasForneceI: TIntegerField;
    Panel2: TPanel;
    CkCompetencia: TCheckBox;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    CBMesIni: TComboBox;
    CBAnoIni: TComboBox;
    CBMesFim: TComboBox;
    CBAnoFim: TComboBox;
    RGTipo: TRadioGroup;
    RGNivel: TRadioGroup;
    Label10: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrLct1NOME_SG: TWideStringField;
    QrLct1NOME_GR: TWideStringField;
    QrLct1Agencia: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    BitBtn1: TBitBtn;
    Label9: TLabel;
    EdArq: TEdit;
    GroupBox6: TGroupBox;
    PainelC: TPanel;
    PnDatas2: TPanel;
    GBVencto: TGroupBox;
    LaVctoIni: TLabel;
    LaVctoFim: TLabel;
    TPVctoIni: TdmkEditDateTimePicker;
    TPVctoFim: TdmkEditDateTimePicker;
    CkVencto: TCheckBox;
    GroupBox3: TGroupBox;
    LaDocIni: TLabel;
    LaDocFim: TLabel;
    TPDocIni: TdmkEditDateTimePicker;
    TPDocFim: TdmkEditDateTimePicker;
    CkDataDoc: TCheckBox;
    GroupBox4: TGroupBox;
    LaCompIni: TLabel;
    LaCompFim: TLabel;
    TPCompIni: TdmkEditDateTimePicker;
    TPCompFim: TdmkEditDateTimePicker;
    CkDataComp: TCheckBox;
    PnDatas1: TPanel;
    GroupBox5: TGroupBox;
    LaEmissIni: TLabel;
    LaEmissFim: TLabel;
    TPEmissIni: TdmkEditDateTimePicker;
    TPEmissFim: TdmkEditDateTimePicker;
    CkEmissao: TCheckBox;
    Panel5: TPanel;
    GroupBox1: TGroupBox;
    RGOrdem1: TRadioGroup;
    GroupBox7: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    EdNivelSel: TdmkEditCB;
    CBNivelSel: TdmkDBLookupComboBox;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    Label8: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    LaCliente: TLabel;
    CBFornece: TdmkDBLookupComboBox;
    EdFornece: TdmkEditCB;
    LaFornece: TLabel;
    RGAgrupamentos: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    frxContaN: TfrxReport;
    QrLct1EhCred: TFloatField;
    QrLct1EhDebt: TFloatField;
    QrLct1plOL: TIntegerField;
    QrLct1plNome: TWideStringField;
    QrLct1plCodigo: TIntegerField;
    QrLct1cjOL: TIntegerField;
    QrLct1cjNome: TWideStringField;
    QrLct1cjCodigo: TIntegerField;
    QrLct1grOL: TIntegerField;
    QrLct1grNome: TWideStringField;
    QrLct1grCodigo: TIntegerField;
    QrLct1sgOL: TIntegerField;
    QrLct1sgNome: TWideStringField;
    QrLct1sgCodigo: TIntegerField;
    QrLct1coOL: TIntegerField;
    QrLct1coNome: TWideStringField;
    QrLct1coCodigo: TIntegerField;
    CkSuprimirTamanho: TCheckBox;
    QrLct1plNPF: TIntegerField;
    QrLct1cjNPF: TIntegerField;
    QrLct1grNPF: TIntegerField;
    QrLct1sgNPF: TIntegerField;
    QrLct1coNPF: TIntegerField;
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrLct1CalcFields(DataSet: TDataSet);
    procedure CBNivelSelClick(Sender: TObject);
    procedure CkCompetenciaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure frxConta1aGetValue(const VarName: String;
      var Value: Variant);
    procedure RGNivelClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure CkEmissaoClick(Sender: TObject);
    procedure CkVenctoClick(Sender: TObject);
    procedure CkDataDocClick(Sender: TObject);
    procedure CkDataCompClick(Sender: TObject);
  private
    { Private declarations }
    FTabLctA, FTabLctB, FTabLctD: String;
    FOrdPlaCta1, FOrdPlaCta2, FOrdPlaCta3, FOrdPlaCta4: Integer;
    procedure ReopenNivelSel();
    function  NomeDoNivelSelecionado(Tipo: Integer): String;
    procedure HabilitaDatas();
    function  OrdemSetada(): String;
    procedure GetValueOrdem1(const VarName: String; var Value: Variant);
    procedure GetValueOrdem2(const VarName: String; var Value: Variant);
    procedure GetValueOrdem3(const VarName: String; var Value: Variant);
  public
    { Public declarations }
    FCliente_Txt, FFornece_Txt: String;
  end;

var
  FmPesquisas2: TFmPesquisas2;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmPesquisas2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesquisas2.BtConfirmaClick(Sender: TObject);
  procedure GeraParteSQL(TabLct, Nivel, NivelSel, Empresa_Txt: String);
  var
    CIni, CFim, Ini, Fim: String;
    AnoI, AnoF, MesI, MesF: Word;
    Carteira: Integer;
  begin
    QrLct1.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
    QrLct1.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
    QrLct1.SQL.Add('(la.Credito-la.Debito) CREDEB, la.*, co.Nome NOMEGENERO,');
    QrLct1.SQL.Add('ca.Nome NOMECARTEIRA, sg.Nome NOME_SG, gr.Nome NOME_GR,');
    QrLct1.SQL.Add('IF(la.Fornecedor<>0,');
    QrLct1.SQL.Add('CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ELSE fo.Nome END,');
    QrLct1.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END) NOMEENTIDADE,');
    QrLct1.SQL.Add('IF(la.Credito > 0, 1.000, 0.000) EhCred, ');
    QrLct1.SQL.Add('IF(la.Debito > 0, 1.000, 0.000) EhDebt, ');
    QrLct1.SQL.Add('pl.OrdemLista plOL, pl.Nome plNome, pl.Codigo plCodigo, pl.NotPrntFin plNPF, ');
    QrLct1.SQL.Add('cj.OrdemLista cjOL, cj.Nome cjNome, cj.Codigo cjCodigo, cj.NotPrntFin cjNPF, ');
    QrLct1.SQL.Add('gr.OrdemLista grOL, gr.Nome grNome, gr.Codigo grCodigo, gr.NotPrntFin grNPF, ');
    QrLct1.SQL.Add('sg.OrdemLista sgOL, sg.Nome sgNome, sg.Codigo sgCodigo, sg.NotPrntFin sgNPF, ');
    QrLct1.SQL.Add('co.OrdemLista coOL, co.Nome coNome, co.Codigo coCodigo, co.NotPrntFin coNPF  ');
    QrLct1.SQL.Add('');
    QrLct1.SQL.Add('FROM ' + TabLct + ' la');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co    ON co.Codigo=la.Genero');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.Subgrupo');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gr ON gr.Codigo=sg.Grupo');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.conjuntos cj ON cj.Codigo=gr.Conjunto');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.plano     pl ON pl.Codigo=cj.Plano');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras ca ON ca.Codigo=la.Carteira');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades fo ON fo.Codigo=la.Fornecedor');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades cl ON cl.Codigo=la.Cliente');
    QrLct1.SQL.Add('WHERE ' + Nivel + '=' + NivelSel);
    QrLct1.SQL.Add('AND ca.ForneceI=' + Empresa_Txt);

    // EMISS�O
    if CkEmissao.Checked then
    begin
      Ini := FormatDateTime(VAR_FORMATDATE, TPEmissIni.Date);
      Fim := FormatDateTime(VAR_FORMATDATE, TPEmissFim.Date);
      QrLct1.SQL.Add('AND la.Data BETWEEN '''+Ini+''' AND '''+Fim+'''');
    end;

    // VENCIMENTO
    if CkVencto.Checked then
    begin
      Ini := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date);
      Fim := FormatDateTime(VAR_FORMATDATE, TPVctoFim.Date);
      QrLct1.SQL.Add('AND la.Vencimento BETWEEN '''+Ini+''' AND '''+Fim+'''');
    end;

    // DATA DO DOCUMENTO
    if CkDataDoc.Checked then
    begin
      Ini := FormatDateTime(VAR_FORMATDATE, TPDocIni.Date);
      Fim := FormatDateTime(VAR_FORMATDATE, TPDocFim.Date);
      QrLct1.SQL.Add('AND la.DataDoc BETWEEN '''+Ini+''' AND '''+Fim+'''');
    end;

    // COMPENSA��O
    if CkDataComp.Checked then
    begin
      Ini := FormatDateTime(VAR_FORMATDATE, TPCompIni.Date);
      Fim := FormatDateTime(VAR_FORMATDATE, TPCompFim.Date);
      QrLct1.SQL.Add('AND la.Compensado BETWEEN '''+Ini+''' AND '''+Fim+'''');
    end;

    // CARTEIRA
    Carteira := Geral.IMV(EdCarteira.Text);
    if Carteira <> 0 then
    QrLct1.SQL.Add('AND la.Carteira='''+IntToStr(Carteira)+'''');

    // CLIENTE E FORNECEDOR
    QrLct1.SQL.Add(dmkPF.SQL_CodTxt('AND la.Cliente',
      EdCliente.Text, False, True, False));
    QrLct1.SQL.Add(dmkPF.SQL_CodTxt('AND la.Fornecedor',
      EdFornece.Text, False, True, False));

    // COMPET�NCIA
    if CkCompetencia.Checked and CkCompetencia.Enabled then
    begin
      AnoI := Geral.IMV(CBAnoIni.Text);
      AnoF := Geral.IMV(CBAnoFim.Text);
      MesI := CBMesIni.ItemIndex + 1;
      MesF := CBMesFim.ItemIndex + 1;
      CIni := IntToStr(((AnoI-2000)*100)+MesI);
      CFim := IntToStr(((AnoF-2000)*100)+MesF);
      QrLct1.SQL.Add('AND la.Mez BETWEEN '''+CIni+''' AND '''+CFim+'''');
    end;
  end;
var
  Nivel, NivelSel, Empresa_Txt: String;
  CliInt, Empresa: Integer;
begin
  FOrdPlaCta1 := -1;
  FOrdPlaCta2 := -1;
  FOrdPlaCta3 := -1;
  FOrdPlaCta4 := -1;
  //
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  Empresa  := DModG.QrEmpresasCodigo.Value;
  CliInt   := DModG.QrEmpresasFilial.Value;
  FTabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
  FTabLctB := DModG.NomeTab(TMeuDB, ntLct, False, ttB, CliInt);
  FTabLctD := DModG.NomeTab(TMeuDB, ntLct, False, ttD, CliInt);
  //
  //
  if RGTipo.ItemIndex = 0 then
  begin
    if MyObjects.FIC(EdNivelSel.ValueVariant = 0, EdNivelSel,
    'Informe o g�nero do n�vel selecionado') then Exit;
    Nivel       := NomeDoNivelSelecionado(2);
    NivelSel    := FormatFloat('0', EdNivelSel.ValueVariant);
    Empresa_Txt := FormatFloat('0', Empresa);
    //
    QrLct1.Close;
    QrLct1.SQL.Clear;
    QrLct1.SQL.Add('DROP TABLE IF EXISTS _fin_relat_001_lct1_;');
    QrLct1.SQL.Add('CREATE TABLE _fin_relat_001_lct1_');
    QrLct1.SQL.Add('');
    GeraParteSQL(FTabLctA, Nivel, NivelSel, Empresa_Txt);
    QrLct1.SQL.Add('UNION');
    GeraParteSQL(FTabLctB, Nivel, NivelSel, Empresa_Txt);
    QrLct1.SQL.Add('UNION');
    GeraParteSQL(FTabLctD, Nivel, NivelSel, Empresa_Txt);
    QrLct1.SQL.Add(';');
    QrLct1.SQL.Add('');
    QrLct1.SQL.Add('SELECT * FROM _fin_relat_001_lct1_');
    (*
    if CkCompetencia.Checked and CkCompetencia.Enabled then
    begin
      if CkCliente.Checked then
        QrLct1.SQL.Add('ORDER BY NOMEENTIDADE, Ano, Mes2, Data')
      else
        QrLct1.SQL.Add('ORDER BY Ano, Mes2, Data, Mez');
      UnDmkDAC_PF.AbreQuery(QrLct1, Dmod.MyDB);
      if CkCliente.Checked then
        MyObjects.frxMostra(frxConta2b, 'Movimento de conta')
      else
        MyObjects.frxMostra(frxConta2a, 'Movimento de conta');
    end else begin
      if CkCliente.Checked then
      begin
        QrLct1.SQL.Add('ORDER BY NOMEENTIDADE, Data, Mez');
        UnDmkDAC_PF.AbreQuery(QrLct1, Dmod.MyDB);
        MyObjects.frxMostra(frxConta1a, 'Movimento de conta');
      end else begin
        QrLct1.SQL.Add('ORDER BY Data, Mez');
        UnDmkDAC_PF.AbreQuery(QrLct1, Dmod.MyDB);
        MyObjects.frxMostra(frxConta1, 'Movimento de conta');
      end;
    end;
    *)
    QrLct1.SQL.Add(OrdemSetada());
    UMyMod.AbreQuery(QrLct1, Dmod.MyDB);
    //dmkPF.LeMeuTexto(QrLct1.SQL.Text);
    MyObjects.frxDefineDataSets(frxContaN, [
      DmodG.frxDsDono,
      frxDsLct1
      ]);
    MyObjects.frxMostra(frxContaN, 'Movimento de conta');
  end;
end;

procedure TFmPesquisas2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  QrLct1.Close;
  QrLct1.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  //
  TPEmissIni.Date := Date -30;
  TPEmissFim.Date := Date;
  //
  TPVctoIni.Date := Date;
  TPVctoFim.Date := Date + 30;
  //
  TPDocIni.Date := Date -30;
  TPDocFim.Date := Date;
  //
  TPCompIni.Date := Date -30;
  TPCompFim.Date := Date;
  //
  MyObjects.PreencheCBAnoECBMes(CBAnoIni, CBMesIni);
  MyObjects.PreencheCBAnoECBMes(CBAnoFim, CBMesFim);
end;

procedure TFmPesquisas2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
{
  if FCliente_Txt <> '' then
  begin
    if FCliente_Txt = '-' then
    begin
      LaCliente.Caption := ' ';
      LaCliente.Visible := False;
      EdCliente.Visible := False;
      CBCliente.Visible := False;
      CkCliente.Visible := False;
    end else
    begin
      LaCliente.Caption := FCliente_Txt;
      CkCliente.Caption := 'Agrupar por '+FCliente_Txt
    end;
  end;
  if FFornece_Txt <> '' then
  begin
    if FFornece_Txt = '-' then
    begin
      LaFornece.Caption := ' ';
      LaFornece.Visible := False;
      EdFornece.Visible := False;
      CBFornece.Visible := False;
    end else LaFornece.Caption := FFornece_Txt;
  end;
}
end;

procedure TFmPesquisas2.QrLct1CalcFields(DataSet: TDataSet);
begin
  if QrLct1Mes2.Value > 0 then
    QrLct1MENSAL.Value := FormatFloat('00', QrLct1Mes2.Value)+'/'
    +Copy(FormatFloat('0000', QrLct1Ano.Value), 3, 2)
   else QrLct1MENSAL.Value := CO_VAZIO;
  if QrLct1Mes2.Value > 0 then
    QrLct1MENSAL2.Value := FormatFloat('0000', QrLct1Ano.Value)+'/'+
    FormatFloat('00', QrLct1Mes2.Value)+'/01'
   else QrLct1MENSAL2.Value := CO_VAZIO;
  QrLct1NOMETIPO.Value :=
    dmkPF.TipoDeCarteiraCashier(QrLct1Tipo.Value, True);
end;

procedure TFmPesquisas2.ReopenNivelSel();
var
  Nivel: String;
begin
  if RGNivel.ItemIndex = 0 then
  begin
    QrNivelSel.Close;
    Exit;
  end;
  //
  Nivel := NomeDoNivelSelecionado(1);
  //
  QrNivelSel.Close;
  QrNivelSel.SQL.Clear;
  QrNivelSel.SQL.Add('SELECT Codigo, Nome');
  QrNivelSel.SQL.Add('FROM ' + Nivel);
  //QrNivelSel.SQL.Add('WHERE Codigo > 0');
  QrNivelSel.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrNivelSel, Dmod.MyDB);
  //
  EdNivelSel.ValueVariant := 0;
  CBNivelSel.KeyValue     := Null;
end;

procedure TFmPesquisas2.RGNivelClick(Sender: TObject);
begin
  ReopenNivelSel();
end;

procedure TFmPesquisas2.CkCompetenciaClick(Sender: TObject);
begin
  if CkCompetencia.Checked then
  begin
    Label4.Enabled := True;
    Label5.Enabled := True;
    Label6.Enabled := True;
    Label7.Enabled := True;
    CBMesIni.Enabled := True;
    CBMesFim.Enabled := True;
    CBAnoIni.Enabled := True;
    CBAnoFim.Enabled := True;
  end else begin
    Label4.Enabled := False;
    Label5.Enabled := False;
    Label6.Enabled := False;
    Label7.Enabled := False;
    CBMesIni.Enabled := False;
    CBMesFim.Enabled := False;
    CBAnoIni.Enabled := False;
    CBAnoFim.Enabled := False;
  end;
end;

procedure TFmPesquisas2.CkDataCompClick(Sender: TObject);
begin
  HabilitaDatas();
end;

procedure TFmPesquisas2.CkDataDocClick(Sender: TObject);
begin
  HabilitaDatas();
end;

procedure TFmPesquisas2.CkEmissaoClick(Sender: TObject);
begin
  HabilitaDatas();
end;

procedure TFmPesquisas2.CkVenctoClick(Sender: TObject);
begin
  HabilitaDatas();
end;

procedure TFmPesquisas2.EdEmpresaChange(Sender: TObject);
begin
  FTabLctA := '?';
  FTabLctB := '?';
  FTabLctD := '?';
end;

procedure TFmPesquisas2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesquisas2.CBNivelSelClick(Sender: TObject);
begin
  {
  if QrContasMensal.Value = 'F' then
  CkCompetencia.Enabled := False
  else
  }CkCompetencia.Enabled := True;
end;

procedure TFmPesquisas2.BitBtn1Click(Sender: TObject);
begin
  {
  Application.CreateForm(TFmExportExcelDBGrid, FmExportExcelDBGrid);
  //FmExportExcelDBGrid.DBGrid1.DataSource := DBGrid1.DataSource;
  FmExportExcelDBGrid.XLSExportDBGrid1.DBGrid := DBGrid1;
  FmExportExcelDBGrid.FFileName := EdArq.Text;
  FmExportExcelDBGrid.ShowModal;
  FmExportExcelDBGrid.Destroy;
  }
end;

procedure TFmPesquisas2.frxConta1aGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'PERIODO' then
  begin
    Value := '';
    if CkEmissao.Checked then
      Value := Value + '[ Emiss�o: ' +
      FormatDateTime(VAR_FORMATDATE3, TPEmissIni.Date) + CO_ATE +
      FormatDateTime(VAR_FORMATDATE3, TPEmissFim.Date) + ' ] ';
    if CkVencto.Checked then
      Value := Value + '[ Vencimento: ' +
      FormatDateTime(VAR_FORMATDATE3, TPVctoIni.Date) + CO_ATE +
      FormatDateTime(VAR_FORMATDATE3, TPVctoFim.Date) + ' ] ';
    if CkDataDoc.Checked then
      Value := Value + '[ Data do documento:' +
      FormatDateTime(VAR_FORMATDATE3, TPDocIni.Date) + CO_ATE +
      FormatDateTime(VAR_FORMATDATE3, TPDocFim.Date) + ' ] ';
    if CkDataComp.Checked then
      Value := Value + '[ Compensado: ' +
      FormatDateTime(VAR_FORMATDATE3, TPEmissIni.Date) + CO_ATE +
      FormatDateTime(VAR_FORMATDATE3, TPEmissFim.Date) + ' ] ';
    //
    Value := Trim(Value);
    if Value = '' then
      Value := 'INDEFINIDO';
  end;
  if VarName = 'COMPETENCIA' then
  begin
    if CkCompetencia.Checked then
      Value :=
      CBMesIni.Text+' / '+CBAnoIni.Text+ CO_ATE+
      CBMesFim.Text+' / '+CBAnoFim.Text
    else Value := 'Indefinida';
  end
  else if VarName = 'VARF_CLIENTE_TIT' then Value := LaCliente.Caption
  else if VarName = 'VARF_FORNECE_TIT' then Value := LaFornece.Caption
  else if VarName = 'VARF_CLIENTENOME' then
  begin
    if not EdCliente.Visible then Value := ' ' else Value :=
    dmkPF.ParValueCodTxt('', CBCliente.Text, Null)
  end else if VarName = 'VARF_FORNECENOME' then
  begin
    if not EdFornece.Visible then Value := ' ' else Value :=
    dmkPF.ParValueCodTxt('', CBFornece.Text, Null)
  end else if VarName = 'NOMEREL' then
    Value := 'Movimento ' + NomeDoNivelSelecionado(3) + ' ' + CBNivelSel.Text
  else if VarName = 'VAR_NOMEEMPRESA' then
    Value := CBEmpresa.Text
  // 2012-05-17
  else if Copy(VarName, 1, 9) = 'VFR_ORD1_' then
    GetValueOrdem1(VarName, Value)
  else if Copy(VarName, 1, 9) = 'VFR_ORD2_' then
    GetValueOrdem2(VarName, Value)
  else if Copy(VarName, 1, 9) = 'VFR_ORD3_' then
    GetValueOrdem3(VarName, Value)
  // FIM 2012-05-17
end;

procedure TFmPesquisas2.GetValueOrdem1(const VarName: String;
  var Value: Variant);
  procedure VerificaFOrdPlaCta();
  begin
    if FOrdPlaCta1 < 0 then
    begin
      FOrdPlaCta1 := MyObjects.SelRadioGroup('N�vel do plano de contas',
      'Selecione o n�vel a agrupar', ['N�vel 5 - Plano', 'N�vel 4 - Conjunto',
      'N�vel 3 - Grupo', 'N�vel 2 - Sub-grupo', 'N�vel 1 - Conta'], 1);
    end;
  end;
  function Valor(Padrao, Variavel: Variant): Variant;
  var NPF: Integer;
  begin
    if RGOrdem1.ItemIndex = 8 then
      VerificaFOrdPlaCta();
    case FOrdPlaCta1 of
      0: NPF := QrLct1plNPF.Value;
      1: NPF := QrLct1cjNPF.Value;
      2: NPF := QrLct1grNPF.Value;
      3: NPF := QrLct1sgNPF.Value;
      4: NPF := QrLct1coNPF.Value;
      else NPF := 0;
    end;
    if (RGOrdem1.ItemIndex = 8) and (NPF = 1) and CkSuprimirTamanho.Checked then
      Result := Padrao
    else
      Result := Variavel;
  end;
const
  OrdemPor_frx: array[0..8] of String = ('Mez', 'Data', 'Vencimento',
  'DataDoc', 'Compensado', 'NOMEENTIDADE', 'EhCred', 'EhDebt', '?Genero?');
begin
  if VarName = 'VFR_ORD1_SEE' then
    Value := RGAgrupamentos.ItemIndex >= 1
  else if VarName = 'VFR_ORD1_NOME' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Compet�ncia: '+ dmkPF.MezToFDT(QrLct1Mez.Value, 0, 104);
      1: Value := 'Emiss�o: '+ Geral.FDT(QrLct1Data.Value, 3);
      2: Value := 'Vencimento: '+ Geral.FDT(QrLct1Vencimento.Value, 3);
      3: Value := 'Data do Documento: '+ Geral.FDT(QrLct1DataDoc.Value, 3);
      4: Value := 'Compensa��o: '+ Geral.FDT(QrLct1Compensado.Value, 3);
      // Colocar cliente e fornecedor? ou � terceiro (NOMEENTIDADE)
      5: Value := 'Terceiro: '+ QrLct1NOMEENTIDADE.Value;
      6: Value := 'CR�DITOS';
      7: Value := 'D�BITOS';
      8: // Plano de contas
      begin
        VerificaFOrdPlaCta();
        case FOrdPlaCta1 of
          0: Value := Geral.FFN(QrLct1plCodigo.Value, 2) + ' - ' + QrLct1plNome.Value;
          1: Value := Geral.FFN(QrLct1cjCodigo.Value, 3) + ' - ' + QrLct1cjNome.Value;
          2: Value := Geral.FFN(QrLct1grCodigo.Value, 4) + ' - ' + QrLct1grNome.Value;
          3: Value := Geral.FFN(QrLct1sgCodigo.Value, 5) + ' - ' + QrLct1sgNome.Value;
          4: Value := Geral.FFN(QrLct1coCodigo.Value, 6) + ' - ' + QrLct1coNome.Value;
          else Value := '???';
        end;
      end;
    end;
  end
  else if VarName = 'VFR_ORD1_CODITION' then
  begin
    if RGOrdem1.ItemIndex <> 8 then
      Value := 'frxDsLct1."' + OrdemPor_frx[RGOrdem1.ItemIndex] + '"'
    else
    begin
      VerificaFOrdPlaCta();
      case FOrdPlaCta1 of
        0: Value := 'plCodigo';
        1: Value := 'cjCodigo';
        2: Value := 'grCodigo';
        3: Value := 'sgCodigo';
        4: Value := 'coCodigo';
        else Value := '???';
      end;
      Value := 'frxDsLct1."' + Value + '"';
    end;
  end
  else if VarName = 'VFR_ORD1_TAM_GH' then
  // Altura Group Header
    Value := Valor(0, 18.000000000000000000)
  else if VarName = 'VFR_ORD1_TAM_MD' then
  // Altura do Master Data e seus Memos
    Value := Valor(0, 11.338582680000000000)
  else if VarName = 'VFR_ORD1_VISIBLE_GH' then
    Value := Valor(False, True)
  else if VarName = 'VFR_ORD1_VISIBLE_MD' then
    Value := Valor(False, True)
end;

procedure TFmPesquisas2.GetValueOrdem2(const VarName: String;
  var Value: Variant);
  procedure VerificaFOrdPlaCta();
  begin
    if FOrdPlaCta2 < 0 then
    begin
      FOrdPlaCta2 := MyObjects.SelRadioGroup('N�vel do plano de contas',
      'Selecione o n�vel a agrupar', ['N�vel 5 - Plano', 'N�vel 4 - Conjunto',
      'N�vel 3 - Grupo', 'N�vel 2 - Sub-grupo', 'N�vel 1 - Conta'], 1);
    end;
  end;
  function Valor(Padrao, Variavel: Variant): Variant;
  var NPF: Integer;
  begin
    if RGOrdem2.ItemIndex = 8 then
      VerificaFOrdPlaCta();
    case FOrdPlaCta2 of
      0: NPF := QrLct1plNPF.Value;
      1: NPF := QrLct1cjNPF.Value;
      2: NPF := QrLct1grNPF.Value;
      3: NPF := QrLct1sgNPF.Value;
      4: NPF := QrLct1coNPF.Value;
      else NPF := 0;
    end;
    if (RGOrdem2.ItemIndex = 8) and (NPF = 1) and CkSuprimirTamanho.Checked then
      Result := Padrao
    else
      Result := Variavel;
  end;
const
  OrdemPor_frx: array[0..8] of String = ('Mez', 'Data', 'Vencimento',
  'DataDoc', 'Compensado', 'NOMEENTIDADE', 'EhCred', 'EhDebt', '?Genero?');
begin
  if VarName = 'VFR_ORD2_SEE' then
    Value := RGAgrupamentos.ItemIndex >= 2
  else if VarName = 'VFR_ORD2_NOME' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Compet�ncia: '+ dmkPF.MezToFDT(QrLct1Mez.Value, 0, 104);
      1: Value := 'Emiss�o: '+ Geral.FDT(QrLct1Data.Value, 3);
      2: Value := 'Vencimento: '+ Geral.FDT(QrLct1Vencimento.Value, 3);
      3: Value := 'Data do Documento: '+ Geral.FDT(QrLct1DataDoc.Value, 3);
      4: Value := 'Compensa��o: '+ Geral.FDT(QrLct1Compensado.Value, 3);
      // Colocar cliente e fornecedor? ou � terceiro (NOMEENTIDADE)
      5: Value := 'Terceiro: '+ QrLct1NOMEENTIDADE.Value;
      6: Value := 'CR�DITOS';
      7: Value := 'D�BITOS';
      8: // Plano de contas
      begin
        VerificaFOrdPlaCta();
        case FOrdPlaCta2 of
          0: Value := Geral.FFN(QrLct1plCodigo.Value, 2) + ' - ' + QrLct1plNome.Value;
          1: Value := Geral.FFN(QrLct1cjCodigo.Value, 3) + ' - ' + QrLct1cjNome.Value;
          2: Value := Geral.FFN(QrLct1grCodigo.Value, 4) + ' - ' + QrLct1grNome.Value;
          3: Value := Geral.FFN(QrLct1sgCodigo.Value, 5) + ' - ' + QrLct1sgNome.Value;
          4: Value := Geral.FFN(QrLct1coCodigo.Value, 6) + ' - ' + QrLct1coNome.Value;
          else Value := '???';
        end;
      end;
    end;
  end
  else if VarName = 'VFR_ORD2_CODITION' then
  begin
    if RGOrdem2.ItemIndex <> 8 then
      Value := 'frxDsLct1."' + OrdemPor_frx[RGOrdem2.ItemIndex] + '"'
    else
    begin
      VerificaFOrdPlaCta();
      case FOrdPlaCta2 of
        0: Value := 'plCodigo';
        1: Value := 'cjCodigo';
        2: Value := 'grCodigo';
        3: Value := 'sgCodigo';
        4: Value := 'coCodigo';
        else Value := '???';
      end;
      Value := 'frxDsLct1."' + Value + '"';
    end;
  end
  else if VarName = 'VFR_ORD2_TAM_GH' then
  // Altura Group Header
    Value := Valor(0, 18.000000000000000000)
  else if VarName = 'VFR_ORD2_TAM_MD' then
  // Altura do Master Data e seus Memos
    Value := Valor(0, 11.338582680000000000)
  else if VarName = 'VFR_ORD2_VISIBLE_GH' then
    Value := Valor(False, True)
  else if VarName = 'VFR_ORD2_VISIBLE_MD' then
    Value := Valor(False, True)
end;

procedure TFmPesquisas2.GetValueOrdem3(const VarName: String;
  var Value: Variant);
  procedure VerificaFOrdPlaCta();
  begin
    if FOrdPlaCta3 < 0 then
    begin
      FOrdPlaCta3 := MyObjects.SelRadioGroup('N�vel do plano de contas',
      'Selecione o n�vel a agrupar', ['N�vel 5 - Plano', 'N�vel 4 - Conjunto',
      'N�vel 3 - Grupo', 'N�vel 2 - Sub-grupo', 'N�vel 1 - Conta'], 1);
    end;
  end;
  function Valor(Padrao, Variavel: Variant): Variant;
  var NPF: Integer;
  begin
    if RGOrdem3.ItemIndex = 8 then
      VerificaFOrdPlaCta();
    case FOrdPlaCta3 of
      0: NPF := QrLct1plNPF.Value;
      1: NPF := QrLct1cjNPF.Value;
      2: NPF := QrLct1grNPF.Value;
      3: NPF := QrLct1sgNPF.Value;
      4: NPF := QrLct1coNPF.Value;
      else NPF := 0;
    end;
    if (RGOrdem3.ItemIndex = 8) and (NPF = 1) and CkSuprimirTamanho.Checked then
      Result := Padrao
    else
      Result := Variavel;
  end;
const
  OrdemPor_frx: array[0..8] of String = ('Mez', 'Data', 'Vencimento',
  'DataDoc', 'Compensado', 'NOMEENTIDADE', 'EhCred', 'EhDebt', '?Genero?');
begin
  if VarName = 'VFR_ORD3_SEE' then
    Value := RGAgrupamentos.ItemIndex >= 3
  else if VarName = 'VFR_ORD3_NOME' then
  begin
    case RGOrdem3.ItemIndex of
      0: Value := 'Compet�ncia: '+ dmkPF.MezToFDT(QrLct1Mez.Value, 0, 104);
      1: Value := 'Emiss�o: '+ Geral.FDT(QrLct1Data.Value, 3);
      2: Value := 'Vencimento: '+ Geral.FDT(QrLct1Vencimento.Value, 3);
      3: Value := 'Data do Documento: '+ Geral.FDT(QrLct1DataDoc.Value, 3);
      4: Value := 'Compensa��o: '+ Geral.FDT(QrLct1Compensado.Value, 3);
      // Colocar cliente e fornecedor? ou � terceiro (NOMEENTIDADE)
      5: Value := 'Terceiro: '+ QrLct1NOMEENTIDADE.Value;
      6: Value := 'CR�DITOS';
      7: Value := 'D�BITOS';
      8: // Plano de contas
      begin
        VerificaFOrdPlaCta();
        case FOrdPlaCta3 of
          0: Value := Geral.FFN(QrLct1plCodigo.Value, 2) + ' - ' + QrLct1plNome.Value;
          1: Value := Geral.FFN(QrLct1cjCodigo.Value, 3) + ' - ' + QrLct1cjNome.Value;
          2: Value := Geral.FFN(QrLct1grCodigo.Value, 4) + ' - ' + QrLct1grNome.Value;
          3: Value := Geral.FFN(QrLct1sgCodigo.Value, 5) + ' - ' + QrLct1sgNome.Value;
          4: Value := Geral.FFN(QrLct1coCodigo.Value, 6) + ' - ' + QrLct1coNome.Value;
          else Value := '???';
        end;
      end;
    end;
  end
  else if VarName = 'VFR_ORD3_CODITION' then
  begin
    if RGOrdem3.ItemIndex <> 8 then
      Value := 'frxDsLct1."' + OrdemPor_frx[RGOrdem3.ItemIndex] + '"'
    else
    begin
      VerificaFOrdPlaCta();
      case FOrdPlaCta3 of
        0: Value := 'plCodigo';
        1: Value := 'cjCodigo';
        2: Value := 'grCodigo';
        3: Value := 'sgCodigo';
        4: Value := 'coCodigo';
        else Value := '???';
      end;
      Value := 'frxDsLct1."' + Value + '"';
    end;
  end
  else if VarName = 'VFR_ORD3_TAM_GH' then
  // Altura Group Header
    Value := Valor(0, 18.000000000000000000)
  else if VarName = 'VFR_ORD3_TAM_MD' then
  // Altura do Master Data e seus Memos
    Value := Valor(0, 11.338582680000000000)
  else if VarName = 'VFR_ORD3_VISIBLE_GH' then
    Value := Valor(False, True)
  else if VarName = 'VFR_ORD3_VISIBLE_MD' then
    Value := Valor(False, True)
end;

procedure TFmPesquisas2.HabilitaDatas();
begin
  LaEmissIni.Enabled := CkEmissao.Checked;
  TPEmissIni.Enabled := CkEmissao.Checked;
  LaEmissFim.Enabled := CkEmissao.Checked;
  TPEmissFim.Enabled := CkEmissao.Checked;
  //
  LaVctoIni.Enabled := CkVencto.Checked;
  TPVctoIni.Enabled := CkVencto.Checked;
  LaVctoFim.Enabled := CkVencto.Checked;
  TPVctoFim.Enabled := CkVencto.Checked;
  //
  LaDocIni.Enabled := CkVencto.Checked;
  TPDocIni.Enabled := CkVencto.Checked;
  LaDocFim.Enabled := CkVencto.Checked;
  TPDocFim.Enabled := CkVencto.Checked;
  //
  LaCompIni.Enabled := CkVencto.Checked;
  TPCompIni.Enabled := CkVencto.Checked;
  LaCompFim.Enabled := CkVencto.Checked;
  TPCompFim.Enabled := CkVencto.Checked;
  //
end;

function TFmPesquisas2.NomeDoNivelSelecionado(Tipo: Integer): String;
begin
  case Tipo of
    1:
    case RGNivel.ItemIndex of
      1: Result := 'contas';
      2: Result := 'subgrupos';
      3: Result := 'grupos';
      4: Result := 'conjuntos';
      5: Result := 'plano';
      else Result := '?';
    end;
    2:
    case RGNivel.ItemIndex of
      1: Result := 'la.Genero';
      2: Result := 'co.Subgrupo';
      3: Result := 'sg.Grupo';
      4: Result := 'gr.Conjunto';
      5: Result := 'cj.Plano';
      else Result := '?';
    end;
    3:
    case RGNivel.ItemIndex of
      1: Result := 'da Conta';
      2: Result := 'do Sub-grupo';
      3: Result := 'do Grupo';
      4: Result := 'do Conjunto';
      5: Result := 'do Plano';
      else Result := ' do Nivel ? ';
    end;
    else Result := '(Tipo)=?';
  end;
end;

function TFmPesquisas2.OrdemSetada: String;
const
  Itens: array[0..8] of String = ('Ano, Mes2', 'Data', 'Vencimento', 'DataDoc',
  'Compensado', 'NOMEENTIDADE', 'EhCred', 'EhDebt',
  'plOL, plNome, cjOL, cjNome, grOL, grNome, sgOL, sgNome, coOL, coNome');
begin
  Result := 'ORDER BY '
    + Itens[RGOrdem1.ItemIndex] + ', '
    + Itens[RGOrdem2.ItemIndex] + ', '
    + Itens[RGOrdem3.ItemIndex] + ', '
    + Itens[RGOrdem4.ItemIndex];
end;

end.

