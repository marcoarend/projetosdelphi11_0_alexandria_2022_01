unit ReceDesp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, dmkEditCB, Db,
  mySQLDbTables, DBCtrls, dmkDBLookupComboBox, ComCtrls, dmkEditDateTimePicker,
  frxClass, frxDBSet, Variants, dmkCheckGroup, Grids, DBGrids, dmkDBGrid, Menus,
  dmkGeral, MyDBCheck, UmySQLModule, UnDmkProcFunc, dmkImage, UnDmkEnums,
  UnProjGroup_Consts;

type
  TFmReceDesp = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DataSource1: TDataSource;
    QrDC: TmySQLQuery;
    frxDC_Mes: TfrxReport;
    frxDsDC: TfrxDBDataset;
    QrDCNOMEMES: TWideStringField;
    QrDCGenero: TIntegerField;
    QrDCMez: TIntegerField;
    QrDCDepto: TIntegerField;
    QrDCValor: TFloatField;
    QrDCNOMECON: TWideStringField;
    QrDCUNIDADE: TWideStringField;
    QrDCControle: TIntegerField;
    frxDC_Uni: TfrxReport;
    QrUHs: TmySQLQuery;
    QrUHsUnidade: TWideStringField;
    QrUHsConta: TIntegerField;
    DsUHs: TDataSource;
    TSCNAB: TTabSheet;
    QrRet: TmySQLQuery;
    DsRet: TDataSource;
    Panel4: TPanel;
    EdEmpresa: TdmkEditCB;
    Label1: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    TPDataIni1: TdmkEditDateTimePicker;
    TPDataFim1: TdmkEditDateTimePicker;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    TPDataIni2: TdmkEditDateTimePicker;
    TPDataFim2: TdmkEditDateTimePicker;
    Panel5: TPanel;
    GroupBox4: TGroupBox;
    Label7: TLabel;
    EdValPagMin: TdmkEdit;
    EdValPagMax: TdmkEdit;
    Label8: TLabel;
    GroupBox5: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    EdValTitMin: TdmkEdit;
    EdValTitMax: TdmkEdit;
    DBGrid1: TdmkDBGrid;
    QrRetCodigo: TIntegerField;
    QrRetBanco: TIntegerField;
    QrRetNossoNum: TWideStringField;
    QrRetSeuNum: TIntegerField;
    QrRetIDNum: TIntegerField;
    QrRetOcorrCodi: TWideStringField;
    QrRetOcorrData: TDateField;
    QrRetValTitul: TFloatField;
    QrRetValAbati: TFloatField;
    QrRetValDesco: TFloatField;
    QrRetValPago: TFloatField;
    QrRetValJuros: TFloatField;
    QrRetValMulta: TFloatField;
    QrRetValJuMul: TFloatField;
    QrRetMotivo1: TWideStringField;
    QrRetMotivo2: TWideStringField;
    QrRetMotivo3: TWideStringField;
    QrRetMotivo4: TWideStringField;
    QrRetMotivo5: TWideStringField;
    QrRetQuitaData: TDateField;
    QrRetDiretorio: TIntegerField;
    QrRetArquivo: TWideStringField;
    QrRetItemArq: TIntegerField;
    QrRetStep: TSmallintField;
    QrRetEntidade: TIntegerField;
    QrRetCarteira: TIntegerField;
    QrRetDevJuros: TFloatField;
    QrRetDevMulta: TFloatField;
    QrRetValOutro: TFloatField;
    QrRetValTarif: TFloatField;
    QrRetLk: TIntegerField;
    QrRetDataCad: TDateField;
    QrRetDataAlt: TDateField;
    QrRetUserCad: TIntegerField;
    QrRetUserAlt: TIntegerField;
    QrRetDtaTarif: TDateField;
    QrRetAlterWeb: TSmallintField;
    QrRetAtivo: TSmallintField;
    QrRetTamReg: TIntegerField;
    GroupBox6: TGroupBox;
    Label9: TLabel;
    EdBloqIni: TdmkEdit;
    EdBloqFim: TdmkEdit;
    Label12: TLabel;
    PMCreditos: TPopupMenu;
    Alteracontadoslanamentosselecionados_Cred: TMenuItem;
    PMDebitos: TPopupMenu;
    Alteracontadoslanamentosselecionados_Debi: TMenuItem;
    Alteradescriodolanamentoselecionado1: TMenuItem;
    Alteralanamentoselecionado1: TMenuItem;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctControle: TIntegerField;
    QrLctSub: TSmallintField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctQtde: TFloatField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctFatID: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctFatNum: TFloatField;
    QrLctFatParcela: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctID_Sub: TSmallintField;
    QrLctFatura: TWideStringField;
    QrLctEmitente: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctContaCorrente: TWideStringField;
    QrLctCNPJCPF: TWideStringField;
    QrLctLocal: TIntegerField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctPago: TFloatField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctCliente: TIntegerField;
    QrLctCliInt: TIntegerField;
    QrLctForneceI: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctMulta: TFloatField;
    QrLctProtesto: TDateField;
    QrLctDataDoc: TDateField;
    QrLctCtrlIni: TIntegerField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctDepto: TIntegerField;
    QrLctDescoPor: TIntegerField;
    QrLctDescoVal: TFloatField;
    QrLctDescoControle: TIntegerField;
    QrLctUnidade: TIntegerField;
    QrLctNFVal: TFloatField;
    QrLctAntigo: TWideStringField;
    QrLctExcelGru: TIntegerField;
    QrLctLk: TIntegerField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TIntegerField;
    QrLctUserAlt: TIntegerField;
    QrLctSerieCH: TWideStringField;
    QrLctDoc2: TWideStringField;
    QrLctMoraVal: TFloatField;
    QrLctMultaVal: TFloatField;
    QrLctCNAB_Sit: TSmallintField;
    QrLctTipoCH: TSmallintField;
    QrLctAlterWeb: TSmallintField;
    QrLctReparcel: TIntegerField;
    QrLctID_Quit: TIntegerField;
    QrLctAtrelado: TIntegerField;
    QrLctAtivo: TSmallintField;
    QrLctPagMul: TFloatField;
    QrLctPagJur: TFloatField;
    QrLctSubPgto1: TIntegerField;
    QrLctMultiPgto: TIntegerField;
    QrLctProtocolo: TIntegerField;
    QrLctCtrlQuitPg: TIntegerField;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasNome2: TWideStringField;
    QrCarteirasContato1: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasPagRec: TIntegerField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasAntigo: TWideStringField;
    QrCarteirasContab: TWideStringField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TIntegerField;
    QrCarteirasUserAlt: TIntegerField;
    QrCarteirasOrdem: TIntegerField;
    QrCarteirasForneceN: TSmallintField;
    QrCarteirasFuturoC: TFloatField;
    QrCarteirasFuturoD: TFloatField;
    QrCarteirasFuturoS: TFloatField;
    QrCarteirasExclusivo: TSmallintField;
    QrCarteirasAlterWeb: TSmallintField;
    QrCarteirasAtivo: TSmallintField;
    QrCarteirasRecebeBloq: TSmallintField;
    QrCarteirasEntiDent: TIntegerField;
    Alteralanamentoselecionado2: TMenuItem;
    Panel9: TPanel;
    Label4: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    LaUH: TLabel;
    CBUH: TDBLookupComboBox;
    CGValores: TdmkCheckGroup;
    RGOrdem: TRadioGroup;
    QrDebitos: TmySQLQuery;
    QrDebitosCOMPENSADO_TXT: TWideStringField;
    QrDebitosDATA: TWideStringField;
    QrDebitosDescricao: TWideStringField;
    QrDebitosDEBITO: TFloatField;
    QrDebitosNOTAFISCAL: TLargeintField;
    QrDebitosSERIECH: TWideStringField;
    QrDebitosDOCUMENTO: TFloatField;
    QrDebitosMEZ: TLargeintField;
    QrDebitosCompensado: TDateField;
    QrDebitosNOMECON: TWideStringField;
    QrDebitosNOMESGR: TWideStringField;
    QrDebitosNOMEGRU: TWideStringField;
    QrDebitosITENS: TLargeintField;
    QrDebitosMES: TWideStringField;
    QrDebitosSERIE_DOC: TWideStringField;
    QrDebitosNF_TXT: TWideStringField;
    QrDebitosMES2: TWideStringField;
    QrDebitosControle: TIntegerField;
    QrDebitosSub: TSmallintField;
    QrDebitosCarteira: TIntegerField;
    QrDebitosCartao: TIntegerField;
    QrDebitosVencimento: TDateField;
    QrDebitosSit: TIntegerField;
    QrDebitosGenero: TIntegerField;
    QrDebitosTipo: TSmallintField;
    DsDebitos: TDataSource;
    frxDsDebitos: TfrxDBDataset;
    frxDsCreditos: TfrxDBDataset;
    DsCreditos: TDataSource;
    QrCreditos: TmySQLQuery;
    QrCreditosMez: TIntegerField;
    QrCreditosCredito: TFloatField;
    QrCreditosNOMECON: TWideStringField;
    QrCreditosNOMESGR: TWideStringField;
    QrCreditosNOMEGRU: TWideStringField;
    QrCreditosMES: TWideStringField;
    QrCreditosNOMECON_2: TWideStringField;
    QrCreditosSubPgto1: TIntegerField;
    QrCreditosControle: TIntegerField;
    QrCreditosSub: TSmallintField;
    QrCreditosCarteira: TIntegerField;
    QrCreditosCartao: TIntegerField;
    QrCreditosVencimento: TDateField;
    QrCreditosCompensado: TDateField;
    QrCreditosSit: TIntegerField;
    QrCreditosGenero: TIntegerField;
    QrCreditosTipo: TSmallintField;
    QrResumo: TmySQLQuery;
    QrResumoCredito: TFloatField;
    QrResumoDebito: TFloatField;
    QrResumoSALDO: TFloatField;
    QrResumoFINAL: TFloatField;
    frxDsResumo: TfrxDBDataset;
    frxDsSaldoA: TfrxDBDataset;
    QrSaldoA: TmySQLQuery;
    QrSaldoAInicial: TFloatField;
    QrSaldoASALDO: TFloatField;
    QrSaldoATOTAL: TFloatField;
    QrDebitosTbLct: TWideStringField;
    QrDebitosGRU_OL: TIntegerField;
    QrDebitosCON_OL: TIntegerField;
    QrDebitosSGR_OL: TIntegerField;
    QrCreditosTbLct: TWideStringField;
    QrCreditosData: TDateField;
    QrCreditosGRU_OL: TIntegerField;
    QrCreditosCON_OL: TIntegerField;
    QrCreditosSGR_OL: TIntegerField;
    frxReceDesp: TfrxReport;
    QrDebitosNO_FORNECE: TWideStringField;
    QrLctIndiPag: TIntegerField;
    QrRetID_Link: TLargeintField;
    QrLctAgencia: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel10: TPanel;
    BtOK: TBitBtn;
    PageControl3: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Panel6: TPanel;
    CkNaoAgruparNada: TCheckBox;
    GroupBox2: TGroupBox;
    CkAcordos: TCheckBox;
    CkPeriodos: TCheckBox;
    CkTextos: TCheckBox;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    DBGCreditos: TdmkDBGrid;
    Panel7: TPanel;
    BtCreditos: TBitBtn;
    TabSheet7: TTabSheet;
    DBGDebitos: TdmkDBGrid;
    Panel8: TPanel;
    BtDebitos: TBitBtn;
    LaAvisoUsuario: TLabel;
    LaAvisoDesenvolvedor: TLabel;
    GBReceitas: TGroupBox;
    LBReceitas: TListBox;
    BtReceUp: TBitBtn;
    BtReceDown: TBitBtn;
    GBDespesas: TGroupBox;
    LBDespesas: TListBox;
    BtDespDown: TBitBtn;
    BtDespUp: TBitBtn;
    Panel11: TPanel;
    BtCancela: TBitBtn;
    BtSalvar: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxReceDesp_GetValue(const VarName: String;
      var Value: Variant);
    procedure QrDCCalcFields(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure CBUHKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdValPagMinChange(Sender: TObject);
    procedure EdValTitMinChange(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure CkNaoAgruparNadaClick(Sender: TObject);
    procedure Alteracontadoslanamentosselecionados_CredClick(Sender: TObject);
    procedure BtCreditosClick(Sender: TObject);
    procedure Alteracontadoslanamentosselecionados_DebiClick(Sender: TObject);
    procedure BtDebitosClick(Sender: TObject);
    procedure Alteradescriodolanamentoselecionado1Click(Sender: TObject);
    procedure Alteralanamentoselecionado1Click(Sender: TObject);
    procedure Alteralanamentoselecionado2Click(Sender: TObject);
    procedure QrDebitosCalcFields(DataSet: TDataSet);
    procedure QrCreditosCalcFields(DataSet: TDataSet);
    procedure QrResumoCalcFields(DataSet: TDataSet);
    procedure QrSaldoACalcFields(DataSet: TDataSet);
    procedure BtReceUpClick(Sender: TObject);
    procedure BtReceDownClick(Sender: TObject);
    procedure BtDespUpClick(Sender: TObject);
    procedure BtDespDownClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
  private
    { Private declarations }
    FEntidade, FEmpresa, FGenero, FUH: Integer;
    FEntidTXT, FEmprTXT,
    FDataI1, FDataF1, FDataI2, FDataF2,
    FTabLctA, FTabLctB, FTabLctD: String;
    FDtIni, FDtEncer, FDtMorto: TDateTime;
    procedure DemostrativoDeReceitasEDespesas();
    procedure HistoricoConta();
    procedure RetornosCNAB();
    procedure ReopenCreditosEDebitos(CliInt: String; DataI, DataF: String;
              Acordos, Periodos, Textos, NaoAgruparNada: Boolean;
              TabLctA, TabLctB, TabLctD: String);
    procedure MoveItensListBox(ListBox: TListBox; Cima: Boolean);
    procedure CarregaOrdenacao();
    procedure ConfiguraDatas();
  public
    { Public declarations }
  end;

  var
  FmReceDesp: TFmReceDesp;

implementation

uses UnGrl_Geral, UnMyObjects, UnInternalConsts, Module, LctMudaConta,
  UnFinanceiro, LctEdit, Principal, ModuleGeral, DmkDAC_PF, MyListas;

{$R *.DFM}

procedure TFmReceDesp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReceDesp.BtSalvarClick(Sender: TObject);
var
  KeyRecDes, Texto: String;
  i: Integer;
begin
  KeyRecDes := Application.Title + '\ReceitasDespesas';
  Texto     := '';
  //
  for i := 0 to LBReceitas.Count - 1 do
  begin
    if i = 0 then
      Texto := LBReceitas.Items[i]
    else
      Texto := Texto + '||' + LBReceitas.Items[i];
  end;
  Geral.WriteAppKeyCU('Receitas', KeyRecDes, Texto, ktString);
  //
  Texto := '';
  //
  for i := 0 to LBDespesas.Count - 1 do
  begin
    if i = 0 then
      Texto := LBDespesas.Items[i]
    else
      Texto := Texto + '||' + LBDespesas.Items[i];
  end;
  Geral.WriteAppKeyCU('Despesas', KeyRecDes, Texto, ktString);
end;

procedure TFmReceDesp.CarregaOrdenacao;
var
  Lista: TStringList;
  KeyRecDes, Receitas, Despesas: String;
begin
  Lista := TStringList.Create;
  KeyRecDes := Application.Title + '\ReceitasDespesas';
  //
  Receitas := Geral.ReadAppKeyCU('Receitas', KeyRecDes, ktString, '');
  Despesas := Geral.ReadAppKeyCU('Despesas', KeyRecDes, ktString, '');
  //
  if Length(Receitas) > 0 then
  begin
    Lista := Geral.Explode(Receitas, '||', 3);
    //
    LBReceitas.Items := Lista;
  end;
  if Length(Despesas) > 0 then
  begin
    Lista := Geral.Explode(Despesas, '||', 3);
    //
    LBDespesas.Items := Lista;
  end;
  Lista.Free;
end;

procedure TFmReceDesp.CBUHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_DELETE then
    CBUH.KeyValue := Null;
end;

procedure TFmReceDesp.CkNaoAgruparNadaClick(Sender: TObject);
begin
  Pagecontrol2.Visible := CkNaoAgruparNada.Checked;
  QrCreditos.Close;
  QrDebitos.Close;
end;

procedure TFmReceDesp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  CGValores.Value := 3;
end;

procedure TFmReceDesp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmReceDesp.ConfiguraDatas();
var
  Dia, Mes, Ano: Word;
  Hoje, Data: TDate;
begin
  Hoje := Date;
  //
  Decodedate(Hoje, Ano, Mes, Dia);
  //
  if Dia < 16 then
    Data := Hoje - 16
  else
    Data := Hoje;
  //
  TPDataIni1.Date := Geral.PrimeiroDiaDoMes(Data);
  TPDataFim1.Date := Geral.UltimoDiaDoMes(Data);
  TPDataIni2.Date := Geral.PrimeiroDiaDoMes(Data);
  TPDataFim2.Date := Geral.UltimoDiaDoMes(Data);
end;

procedure TFmReceDesp.FormCreate(Sender: TObject);
var
  MostraTabCNAB, MostraUH: Boolean;
begin
  ImgTipo.SQLType := stPsq;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  QrCreditos.Database := DModG.MyPID_DB;
  QrDebitos.Database  := DModG.MyPID_DB;
  QrSaldoA.Database   := DModG.MyPID_DB;
  QrResumo.Database   := DModG.MyPID_DB;
  QrDC.Database       := DModG.MyPID_DB;
  //
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  //
  ConfiguraDatas();
  //
  PageControl1.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  //
  CarregaOrdenacao;
  //
  MostraUH      := VAR_KIND_DEPTO = kdUH;
  MostraTabCNAB := Grl_Geral.LiberaModulo(CO_DMKID_APP,
                     Grl_Geral.ObtemSiglaModulo(mdlappBloquetos),
                     DModG.QrMaster.FieldByName('HabilModulos').AsString);
  //
  TSCNAB.TabVisible := MostraTabCNAB;
  LaUH.Visible      := MostraUH;
  CBUH.Visible      := MostraUH;
  RGOrdem.Visible   := MostraUH;
end;

procedure TFmReceDesp.Alteracontadoslanamentosselecionados_CredClick(
  Sender: TObject);
var
  i, Genero, Controle: Integer;
begin
  if UFinanceiro.ImpedePelaTabelaLct(QrCreditosTbLct.Value,
  QrCreditosControle.Value) then
    Exit;
  //
  Application.CreateForm(TFmLctMudaConta, FmLctMudaConta);
  FmLctMudaConta.ShowModal;
  Genero := FmLctMudaConta.FContaSel;
  FmLctMudaConta.Destroy;
  if Genero = QrCreditosGenero.Value then Exit;
  if Genero <> -1000 then
  begin
    Screen.Cursor := crHourGlass;
    with DBGCreditos.DataSource.DataSet do
    for i:= 0 to DBGCreditos.SelectedRows.Count-1 do
    begin
      if QrCreditosGenero.Value < 1 then
      begin
        Geral.MB_Aviso('O lan�amento ' + Geral.FF0(QrCreditosControle.Value) +
          ' � protegido. Para editar este item selecione seu caminho correto!');
        Exit;
      end;
      if QrCreditosCartao.Value > 0 then
      begin
        Geral.MB_Aviso('O lan�amento ' + Geral.FF0(QrCreditosControle.Value) +
          ' n�o pode ser editado pois pertence a uma fatura!');
        Exit;
      end;
      //GotoBookmark(DBGCreditos.SelectedRows.Items[i]);
      GotoBookmark(DBGCreditos.SelectedRows.Items[i]);
      {
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Genero=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := Genero;
      //
      Dmod.QrUpd.Params[01].AsInteger := QrCreditosControle.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrCreditosSub.Value;
      Dmod.QrUpd.ExecSQL;
      }
      if not UFinanceiro.ImpedePelaTabelaLct(QrCreditosTbLct.Value,
      QrCreditosControle.Value) then
      begin
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Genero'], ['Controle', 'Sub'], [Genero], [QrCreditosControle.Value,
        QrCreditosSub.Value], True, '', FTabLctA);
      end;
    end;
    //
    Controle := QrCreditosControle.Value;
    QrCreditos.Close;
    UnDmkDAC_PF.AbreQuery(QrCreditos, DModG.MyPID_DB);
    QrCreditos.Locate('Controle', Controle, []);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmReceDesp.Alteracontadoslanamentosselecionados_DebiClick(
  Sender: TObject);
var
  i, Genero, Controle: Integer;
begin
  Application.CreateForm(TFmLctMudaConta, FmLctMudaConta);
  FmLctMudaConta.ShowModal;
  Genero := FmLctMudaConta.FContaSel;
  FmLctMudaConta.Destroy;
  if Genero = QrDebitosGenero.Value then Exit;
  if Genero <> -1000 then
  begin
    Screen.Cursor := crHourGlass;
    with DBGDebitos.DataSource.DataSet do
    for i:= 0 to DBGDebitos.SelectedRows.Count-1 do
    begin
      if QrDebitosGenero.Value < 1 then
      begin
        Geral.MB_Aviso('O lan�amento ' + Geral.FF0(QrDebitosControle.Value) +
          ' � protegido. Para editar este item selecione seu caminho correto!');
        Exit;
      end;
      if QrDebitosCartao.Value > 0 then
      begin
        Geral.MB_Aviso('O lan�amento ' + Geral.FF0(QrDebitosControle.Value) +
          ' n�o pode ser editado pois pertence a uma fatura!');
        Exit;
      end;
      //GotoBookmark(DBGDebitos.SelectedRows.Items[i]);
      GotoBookmark(DBGDebitos.SelectedRows.Items[i]);
      {
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Genero=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := Genero;
      //
      Dmod.QrUpd.Params[01].AsInteger := QrDebitosControle.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrDebitosSub.Value;
      Dmod.QrUpd.ExecSQL;
      }
      if not UFinanceiro.ImpedePelaTabelaLct(QrDebitosTbLct.Value,
      QrDebitosControle.Value) then
      begin
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Genero'], ['Controle', 'Sub'], [Genero], [QrDebitosControle.Value,
        QrDebitosSub.Value], True, '', FTabLctA);
      end;
    end;
    //
    Controle := QrDebitosControle.Value;
    QrDebitos.Close;
    UnDmkDAC_PF.AbreQuery(QrDebitos, DModG.MyPID_DB);
    QrDebitos.Locate('Controle', Controle, []);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmReceDesp.Alteradescriodolanamentoselecionado1Click(
  Sender: TObject);
var
  Descricao: String;
  Controle: Integer;
begin
  if UFinanceiro.ImpedePelaTabelaLct(QrDebitosTbLct.Value,
  QrDebitosControle.Value) then
    Exit;
  //
  Descricao := QrDebitosDescricao.Value;
  if InputQuery('Altera��o de Lan�amento', 'Informa o novo hist�rico', Descricao) then
  begin
    Screen.Cursor := crHourGlass;
    {
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lanc tos SET AlterWeb=1, Descricao=:P0 ');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
    //
    Dmod.QrUpd.Params[00].AsString  := Descricao;
    Dmod.QrUpd.Params[01].AsInteger := QrDebitosControle.Value;
    Dmod.QrUpd.Params[02].AsInteger := QrDebitosSub.Value;
    Dmod.QrUpd.ExecSQL;
    }
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'Descricao'], ['Controle', 'Sub'], [Descricao], [
    QrDebitosControle.Value, QrDebitosSub.Value], True, '', FTabLctA);
    //
    Controle := QrDebitosControle.Value;
    QrDebitos.Close;
    UnDmkDAC_PF.AbreQuery(QrDebitos, DModG.MyPID_DB);
    QrDebitos.Locate('Controle', Controle, []);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmReceDesp.Alteralanamentoselecionado1Click(Sender: TObject);
var
  Controle: Integer;
  Ctrl_TXT, Sub_TXT: String;
begin
  if UFinanceiro.ImpedePelaTabelaLct(QrCreditosTbLct.Value,
    QrCreditosControle.Value) then
    Exit;
  //
  Ctrl_TXT := Geral.FF0(QrCreditosControle.Value);
  Sub_TXT  := Geral.FF0(QrCreditosSub.Value);
  //
  QrLct.Close;
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT *');
  QrLct.SQL.Add('FROM ' + FTabLctA);
  QrLct.SQL.Add('WHERE Controle=' + Ctrl_Txt);
  QrLct.SQL.Add('AND Sub=' + Sub_TXT);
(*
  QrLct.SQL.Add('UNION');
  QrLct.SQL.Add('SELECT *');
  QrLct.SQL.Add('FROM ' + FTabLctB);
  QrLct.SQL.Add('WHERE Controle=' + Ctrl_Txt);
  QrLct.SQL.Add('AND Sub=' + Sub_TXT);
  QrLct.SQL.Add('UNION');
  QrLct.SQL.Add('SELECT *');
  QrLct.SQL.Add('FROM ' + FTabLctD);
  QrLct.SQL.Add('WHERE Controle=' + Ctrl_Txt);
  QrLct.SQL.Add('AND Sub=' + Sub_TXT);
*)
  UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
  //
  if QrLct.RecordCount = 1 then
  begin
    {
    if UFinanceiro.InsAltLancamento(TFmLctEdit, FmLctEdit, lfCondominio,
    afmoNegarComAviso, QrLct, QrCarteiras,
    tgrAltera, QrLctControle.Value, QrLctSub.Value,
    0(*Genero.Value*), 2, 1, nil, 0, 0, 0, 0, 0, 0, True,
    0(*Cliente*), 0(*Fornecedor*), FEmpresa(*cliInt*),
    0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
    False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
    0, 0, 0, 2, 0, FTabLctA) > 0}
    //
    if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
    DModG.ReopenEmpresas(VAR_USUARIO, 0);
    //
    if UFinanceiro.AlteracaoLancamento(QrCarteiras, QrLct, lfProprio,
    0(*OneAccount*), QrCarteirasForneceI.Value(*OneCliInt*), FTabLctA, False) then
    begin
      Controle := QrCreditosControle.Value;
      QrCreditos.Close;
      UnDmkDAC_PF.AbreQuery(QrCreditos, DModG.MyPID_DB);
      QrCreditos.Locate('Controle', Controle, []);
    end;
  end else Geral.MB_Aviso('N�o foi poss�vel localizar o lan�amento ' +
  Ctrl_TXT + '. Verifique se ele n�o pertence a um periodo encerrado!');
end;

procedure TFmReceDesp.Alteralanamentoselecionado2Click(Sender: TObject);
var
  Controle: Integer;
  Ctrl_TXT, Sub_TXT: String;
begin
  if UFinanceiro.ImpedePelaTabelaLct(QrDebitosTbLct.Value,
  QrDebitosControle.Value) then
    Exit;
  //
  Ctrl_TXT := Geral.FF0(QrDebitosControle.Value);
  Sub_TXT  := Geral.FF0(QrDebitosSub.Value);
  //
  QrLct.Close;
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT *');
  QrLct.SQL.Add('FROM ' + FTabLctA);
  QrLct.SQL.Add('WHERE Controle=' + Ctrl_Txt);
  QrLct.SQL.Add('AND Sub=' + Sub_TXT);
(*
  QrLct.SQL.Add('UNION');
  QrLct.SQL.Add('SELECT *');
  QrLct.SQL.Add('FROM ' + FTabLctB);
  QrLct.SQL.Add('WHERE Controle=' + Ctrl_Txt);
  QrLct.SQL.Add('AND Sub=' + Sub_TXT);
  QrLct.SQL.Add('UNION');
  QrLct.SQL.Add('SELECT *');
  QrLct.SQL.Add('FROM ' + FTabLctD);
  QrLct.SQL.Add('WHERE Controle=' + Ctrl_Txt);
  QrLct.SQL.Add('AND Sub=' + Sub_TXT);
*)
  UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
  //
  if QrLct.RecordCount = 1 then
  begin
    {
    if UFinanceiro.InsAltLancamento(TFmLctEdit, FmLctEdit, lfCondominio,
    afmoNegarComAviso, QrLct, QrCarteiras,
    tgrAltera, QrLctControle.Value, QrLctSub.Value,
    0(*Genero.Value*), 2, 1, nil, 0, 0, 0, 0, 0, 0, True,
    0(*Cliente*), 0(*Fornecedor*), FEmpresa(*cliInt*),
    0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
    False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
    0, 0, 0, 2, 0, FTabLctA) > 0}
    //
    if VAR_LIB_EMPRESAS = '' then
      VAR_LIB_EMPRESAS := '-100000000';
    DModG.ReopenEmpresas(VAR_USUARIO, 0);
    //
    if UFinanceiro.AlteracaoLancamento(QrCarteiras, QrLct, lfProprio,
    0(*OneAccount*), QrCarteirasForneceI.Value(*OneCliInt*), FTabLctA, False) then
    begin
      Controle := QrDebitosControle.Value;
      QrDebitos.Close;
      UnDmkDAC_PF.AbreQuery(QrDebitos, DModG.MyPID_DB);
      QrDebitos.Locate('Controle', Controle, []);
    end;
  end else Geral.MB_Aviso('N�o foi poss�vel localizar o lan�amento ' +
  Ctrl_TXT + '. Verifique se ele n�o pertence a um periodo encerrado!');
end;

procedure TFmReceDesp.BtDebitosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDebitos, BtDebitos);
end;

procedure TFmReceDesp.BtDespDownClick(Sender: TObject);
begin
  MoveItensListBox(LBDespesas, False);
end;

procedure TFmReceDesp.BtDespUpClick(Sender: TObject);
begin
  MoveItensListBox(LBDespesas, True);
end;

procedure TFmReceDesp.BtCancelaClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Receitas', Application.Title + '\ReceitasDespesas', '', ktString);
  Geral.WriteAppKeyCU('Despesas', Application.Title + '\ReceitasDespesas', '', ktString);
  //
  Geral.MB_Info('Reabra a janela para que a configura��o padr�o seja restaurada!');
end;

procedure TFmReceDesp.BtCreditosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCreditos, BtCreditos);
end;

procedure TFmReceDesp.BtOKClick(Sender: TObject);
var
  Empresa: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a Empresa!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    FEntidade := DModG.QrEmpresasCodigo.Value;
    FEntidTXT := Geral.FF0(FEntidade);
    FEmpresa  := DModG.QrEmpresasFilial.Value;
    FEmprTXT  := Geral.FF0(FEmpresa);
    FGenero   := Geral.IMV(EdConta.Text);
    FDataI1   := Geral.FDT(TPDataIni1.Date, 1);
    FDataF1   := Geral.FDT(TPDataFim1.Date, 1);
    FDataI2   := Geral.FDT(TPDataIni2.Date, 1);
    FDataF2   := Geral.FDT(TPDataFim2.Date, 1);
    //
    if (CBUH.Visible = True) and (CBUH.KeyValue <> Null) then
      FUH := CBUH.KeyValue
    else
      FUH := 0;
    //
    case PageControl1.ActivePageIndex of
      0: DemostrativoDeReceitasEDespesas();
      1: HistoricoConta();
      2: RetornosCNAB();
      else Geral.MB_Erro('Relat�rio (aba) n�o definido');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmReceDesp.BtReceDownClick(Sender: TObject);
begin
  MoveItensListBox(LBReceitas, False);
end;

procedure TFmReceDesp.BtReceUpClick(Sender: TObject);
begin
  MoveItensListBox(LBReceitas, True);
end;

procedure TFmReceDesp.DBGrid1CellClick(Column: TColumn);
var
  Codigo, Status: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  Codigo := QrRetCodigo.Value;
  if QrRetStep.Value = 0 then
    Status := 1
  else
    Status := 0;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET ');
  Dmod.QrUpd.SQL.Add('Step=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
  Dmod.QrUpd.Params[00].AsInteger := Status;
  Dmod.QrUpd.Params[01].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  QrRet.Close;
  UnDmkDAC_PF.AbreQuery(QrRet, Dmod.MyDB);
  QrRet.Locate('Codigo', Codigo, []);
  Screen.Cursor := crDefault;
end;

procedure TFmReceDesp.DemostrativoDeReceitasEDespesas();
  procedure GeraParteSQL_SaldoA(TabLct, Entidade, DataI, DataF, FldIni: String);
  begin
    QrSaldoA.SQL.Add('SELECT ' + FldIni + ' Inicial,');
    QrSaldoA.SQL.Add('(');
    QrSaldoA.SQL.Add('  SELECT SUM(lct.Credito-lct.Debito)');
    QrSaldoA.SQL.Add('  FROM ' + TabLct + ' lct');
    QrSaldoA.SQL.Add('  WHERE lct.Carteira=car.Codigo');
    QrSaldoA.SQL.Add('  AND lct.Data < "' + FDataI1 + '"');
    //Desativado por causa de condom�nios que foram para o morto e voltaram
    //nestes condom�nios duplica e nos demais n�o apresentaram problemas ao desabilitar
    //QrSaldoA.SQL.Add('  AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!

    // N�o precisa
    // QrSaldoA.SQL.Add('  AND car.ForneceI=' + Entidade);
    QrSaldoA.SQL.Add(') SALDO');
    QrSaldoA.SQL.Add('FROM ' + TMeuDB + '.carteiras car');
    QrSaldoA.SQL.Add('WHERE car.ForneceI=' + Entidade);
    QrSaldoA.SQL.Add('AND car.Tipo <> 2');
{
    QrSaldoA.SQL.Add('SELECT SUM(' + FldIni + ') Inicial,');
    QrSaldoA.SQL.Add('(');
    QrSaldoA.SQL.Add('  SELECT SUM(lct.Credito-lct.Debito)');
    QrSaldoA.SQL.Add('  FROM ' + TabLct + ' lct');
    QrSaldoA.SQL.Add('  LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrSaldoA.SQL.Add('  WHERE car.Tipo <> 2');
    QrSaldoA.SQL.Add('  AND car.ForneceI=' + Entidade);
    QrSaldoA.SQL.Add('  AND lct.Data < ' + FDataI1);
    QrSaldoA.SQL.Add(') SALDO');
    QrSaldoA.SQL.Add('FROM ' + TMeuDB + '.carteiras car');
    QrSaldoA.SQL.Add('WHERE car.ForneceI=' + Entidade);
    QrSaldoA.SQL.Add('AND car.Tipo <> 2');
}
  end;

  procedure GeraParteSQL_Resumo(TabLct: String);
  begin
    QrResumo.SQL.Add('SELECT SUM(lct.Credito) Credito, -SUM(lct.Debito) Debito');
    QrResumo.SQL.Add('FROM ' + TabLct + ' lct');
    QrResumo.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrResumo.SQL.Add('WHERE lct.Tipo <> 2');
    QrResumo.SQL.Add('AND lct.Genero > 0');
    QrResumo.SQL.Add('AND car.ForneceI=' + FEntidTXT);
    QrResumo.SQL.Add('AND lct.Data BETWEEN "' + FDataI1 + '" AND "' + FDataF1 + '"');
    //Desativado por causa de condom�nios que foram para o morto e voltaram
    //nestes condom�nios duplica e nos demais n�o apresentaram problemas ao desabilitar
    //QrResumo.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
  end;

var
  Imprime: Boolean;
  FldIni, TabIni: String;
begin
  Screen.Cursor := crHourGlass;
  try
    ReopenCreditosEDebitos(FEntidTXT, FDataI1, FDataF1, CkAcordos.Checked,
    CkPeriodos.Checked, CkTextos.Checked, CkNaoAgruparNada.Checked,
    FTabLctA, FTabLctB, FTabLctD);
    //
    FldIni := UFinanceiro.DefLctFldSdoIni(TPDataIni1.Date, FDtEncer, FDtMorto);
    TabIni := UFinanceiro.DefLctTab(FDtIni, FDtEncer, FDtMorto,
              FTabLcta, FTabLctB, FTabLctD);
    //
    QrSaldoA.Close;
    QrSaldoA.SQL.Clear;

    //
    QrSaldoA.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_SALDOA_1_;');
    QrSaldoA.SQL.Add('CREATE TABLE _MOD_COND_SALDOA_1_ IGNORE ');
    QrSaldoA.SQL.Add('');
    GeraParteSQL_SaldoA(FTabLctA, FEntidTXT, FDataI1, FDataF1, FldIni);
(*
  QrSaldoA.SQL.Add('UNION');
  GeraParteSQL_SaldoA(FTabLctB, FEntidTXT, FDataI1, FDataF1, CkNaoAgruparNada.Checked);
  QrSaldoA.SQL.Add('UNION');
  GeraParteSQL_SaldoA(FTabLctD, FEntidTXT, FDataI1, FDataF1, CkNaoAgruparNada.Checked);
*)
    QrSaldoA.SQL.Add(';');
    QrSaldoA.SQL.Add('');
    QrSaldoA.SQL.Add('SELECT SUM(Inicial) Inicial, SUM(SALDO) SALDO');
    QrSaldoA.SQL.Add('FROM _MOD_COND_SALDOA_1_;');
    QrSaldoA.SQL.Add('');
    // N�o pode !!
    //QrSaldoA.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_SALDOA_1_;');
    QrSaldoA.SQL.Add('');
{
SELECT ' + FldIni + ' Inicial,
(
  SELECT SUM(lct.Credito-lct.Debito)
  FROM ' + TabLctX + ' lct
  WHERE lct.Carteira=car.Codigo
  AND lct.Data < "' + DataI + '"
) SDO_ANT
FROM carteiras car
WHERE car.ForneceI=
AND car.Tipo <> 2
}
    //
    UMyMod.AbreQuery(QrSaldoA, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
    // Deve ser ap�s SaldoA
    QrResumo.Close;
    QrResumo.SQL.Clear;
    //
    QrResumo.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_RESUMO_1_;');
    QrResumo.SQL.Add('CREATE TABLE _MOD_COND_RESUMO_1_ IGNORE ');
    QrResumo.SQL.Add('');
    GeraParteSQL_Resumo(FTabLctA);
    QrResumo.SQL.Add('UNION');
    GeraParteSQL_Resumo(FTabLctB);
    QrResumo.SQL.Add('UNION');
    GeraParteSQL_Resumo(FTabLctD);
    QrResumo.SQL.Add(';');
    QrResumo.SQL.Add('');
    QrResumo.SQL.Add('SELECT SUM(Credito) Credito, -SUM(Debito) Debito ');
    QrResumo.SQL.Add('FROM _MOD_COND_RESUMO_1_;');
    QrResumo.SQL.Add('');
    // N�o pode !!
    //QrResumo.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_RESUMO_1_;');
    QrResumo.SQL.Add('');
    //
    UMyMod.AbreQuery(QrResumo, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
  finally
    Screen.Cursor := crDefault;
  end;
  MyObjects.frxDefineDataSets(frxReceDesp, [
    DmodG.frxDsDono,
    frxDsCreditos,
    frxDsDebitos,
    frxDsSaldoA,
    frxDsResumo
    ]);
  if not CkNaoAgruparNada.Checked then
    Imprime := True
  else
    Imprime := False;
  if Imprime then
    MyObjects.frxMostra(frxReceDesp, 'Demonstrativo de receitas e despesas');
  //
  if Imprime then
    PageControl3.ActivePageIndex := 1;
end;

procedure TFmReceDesp.EdEmpresaChange(Sender: TObject);
begin
  CBUH.KeyValue := Null;
  //
  QrUHs.Close;
  FEntidade := DModG.QrEmpresasCodigo.Value;
  FEntidTXT := Geral.FF0(FEntidade);
  FEmpresa  := DModG.QrEmpresasFilial.Value;
  FEmprTXT  := Geral.FF0(FEmpresa);

  { desmarcado
  FTabLctA  := DModG.NomeTab(ntLct, False, ttA, FEmpresa);
  FTabLctB  := DModG.NomeTab(ntLct, False, ttB, FEmpresa);
  FTabLctD  := DModG.NomeTab(ntLct, False, ttD, FEmpresa);
  }
  DModG.Def_EM_ABD(TMeuDB, 
    FEntidade, FEmpresa, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  //
  if (VAR_KIND_DEPTO = kdUH) and (FEmpresa > 0) then
  begin
    QrUHs.Params[0].AsInteger := FEmpresa;
    UnDmkDAC_PF.AbreQuery(QrUHs, Dmod.MyDB);
  end;
end;

procedure TFmReceDesp.EdValPagMinChange(Sender: TObject);
begin
  EdValPagMax.ValueVariant := EdValPagMin.ValueVariant;
end;

procedure TFmReceDesp.EdValTitMinChange(Sender: TObject);
begin
  EdValTitMax.ValueVariant := EdValTitMin.ValueVariant;
end;

procedure TFmReceDesp.frxReceDesp_GetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2, TPDataIni1.Date) + ' at� ' +
    FormatDateTime(VAR_FORMATDATE2, TPDataFim1.Date)
  else if AnsiCompareText(VarName, 'VARF_NOMECOND') = 0 then
    Value := CBEmpresa.Text
  else if AnsiCompareText(VarName, 'VARF_CONTA_SEL') = 0 then
    Value := CBConta.Text
  else if AnsiCompareText(VarName, 'VARF_UH') = 0 then
  begin
    if CBUH.KeyValue = Null then
    begin
      if VAR_KIND_DEPTO = kdUH then
        Value := 'TODAS UHs'
      else
        Value := '';
    end else
      Value := CBUH.Text;
  end
  else
end;

procedure TFmReceDesp.HistoricoConta();

  procedure GeraParteSQL(TabLct, CliInt, Genero, UH: String);
  begin
    QrDC.SQL.Add('SELECT lan.Genero, lan.Mez, lan.Depto, ');
    QrDC.SQL.Add('lan.Credito-lan.Debito Valor, con.OrdemLista, ');
    QrDC.SQL.Add('lan.Controle, con.Nome NOMECON, ');
    //
    if VAR_KIND_DEPTO = kdUH then
      QrDC.SQL.Add('cim.Unidade UNIDADE')
    else
      QrDC.SQL.Add('"" UNIDADE');
    //
    QrDC.SQL.Add('FROM ' + TabLct + ' lan');
    QrDC.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
    QrDC.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lan.Genero');
    //
    if VAR_KIND_DEPTO = kdUH then
      QrDC.SQL.Add('LEFT JOIN ' + TMeuDB + '.condimov  cim ON cim.Conta=lan.Depto');
    //
    QrDC.SQL.Add('WHERE lan.Tipo <> 2');
    //
    case CGValores.Value of
      1: QrDC.SQL.Add('AND lan.Credito > lan.Debito');
      2: QrDC.SQL.Add('AND lan.Debito > lan.Credito');
    end;
    //
    QrDC.SQL.Add('AND lan.Genero>0');
    QrDC.SQL.Add('AND car.ForneceI = ' + CliInt);
    //
    if FGenero > 0 then
      QrDC.SQL.Add('AND Genero=' + Genero);
    //
    if FUH > 0 then
      QrDC.SQL.Add('AND Depto=' + UH);
    //
    QrDC.SQL.Add('AND lan.Data BETWEEN "' + FDataI1 + '" AND "' + FDataF2 + '"');
  end;

var
  Ordem: Integer;
  CliInt, Genero, UH: String;
begin
  Screen.Cursor := crHourGlass;
  try
    CliInt := Geral.FF0(FEntidade);
    Genero := Geral.FF0(FGenero);
    //
    if RGOrdem.Visible then
      Ordem := RGOrdem.ItemIndex
    else
      Ordem := 0;
    //
    if CBUH.Visible = False then
      FUH := 0;
    //
    UH := Geral.FF0(FUH);
    //
    QrDC.Close;
    QrDC.SQL.Clear;
    //
    QrDC.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_010_DC_;');
    QrDC.SQL.Add('CREATE TABLE _FIN_RELAT_010_DC_ IGNORE ');
    QrDC.SQL.Add('');
    GeraParteSQL(FTabLctA, CliInt, Genero, UH);
    QrDC.SQL.Add('UNION');
    GeraParteSQL(FTabLctB, CliInt, Genero, UH);
    QrDC.SQL.Add('UNION');
    GeraParteSQL(FTabLctD, CliInt, Genero, UH);
    QrDC.SQL.Add(';');
    QrDC.SQL.Add('');
    QrDC.SQL.Add('SELECT * FROM _FIN_RELAT_010_DC_');
    case Ordem of
      0: QrDC.SQL.Add('ORDER BY OrdemLista, NOMECON, Mez, Depto;');
      1: QrDC.SQL.Add('ORDER BY OrdemLista, NOMECON, Depto, Mez;');
    end;
    QrDC.SQL.Add('');
    QrDC.SQL.Add('DROP TABLE IF EXISTS _FIN_RELAT_010_DC_;');
    QrDC.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(QrDC, DModG.MyPID_DB);
  finally
    Screen.Cursor := crDefault;
  end;

  case Ordem of
    0:
    begin
      MyObjects.frxDefineDataSets(frxDC_Mes, [
        DmodG.frxDsDono,
        frxDsDC
        ]);

      MyObjects.frxMostra(frxDC_Mes, 'Hist�rico de Conta (Plano de contas) por M�s');
    end;
    1:
    begin
      MyObjects.frxDefineDataSets(frxDC_Uni, [
        DmodG.frxDsDono,
        frxDsDC
        ]);

      MyObjects.frxMostra(frxDC_Uni, 'Hist�rico de Conta (Plano de contas) por Unidade');
    end;
  end;
end;

procedure TFmReceDesp.MoveItensListBox(ListBox: TListBox; Cima: Boolean);
var
  NovoIndex: Integer;
  Executa: Boolean;
begin
  if Cima then
    Executa := ListBox.ItemIndex > 0
  else
    Executa := ListBox.ItemIndex < ListBox.Items.Count - 1;
  if Executa then
  begin
    if Cima then
      NovoIndex := ListBox.ItemIndex - 1
    else
      NovoIndex := ListBox.ItemIndex + 1;
    ListBox.Items.Move(ListBox.ItemIndex, NovoIndex);
    ListBox.ItemIndex := NovoIndex;
    ListBox.SetFocus;
  end;
end;

procedure TFmReceDesp.QrCreditosCalcFields(DataSet: TDataSet);
var
  SubPgto1: String;
begin
  if QrCreditosSubPgto1.Value = 0 then SubPgto1 := ''
    else SubPgto1 := ' RECEB. ACORDO EXTRAJUDICIAL';
  //
  QrCreditosNOMECON_2.Value := QrCreditosNOMECON.Value + SubPgto1;
  //
  QrCreditosMES.Value := dmkPF.MezToMesEAno(QrCreditosMez.Value);
end;

procedure TFmReceDesp.QrDCCalcFields(DataSet: TDataSet);
begin
   QrDCNOMEMES.Value := dmkPF.MezToFDT(QrDCMez.Value, 0, 14);
end;

procedure TFmReceDesp.QrDebitosCalcFields(DataSet: TDataSet);
begin
  QrDebitosMES.Value := dmkPF.MezToMesEAno(QrDebitosMez.Value);
  QrDebitosMES2.Value := dmkPF.MezToMesEAnoCurto(QrDebitosMez.Value);
  //
  QrDebitosSERIE_DOC.Value := Trim(QrDebitosSerieCH.Value);
  if QrDebitosDocumento.Value <> 0 then QrDebitosSERIE_DOC.Value :=
    QrDebitosSERIE_DOC.Value + FormatFloat('000000', QrDebitosDocumento.Value);
  //
  QrDebitosNF_TXT.Value := FormatFloat('000000;-000000; ', QrDebitosNotaFiscal.Value);
  //
end;

procedure TFmReceDesp.QrResumoCalcFields(DataSet: TDataSet);
begin
  QrResumoSALDO.Value := QrResumoCredito.Value - QrResumoDebito.Value;
  QrResumoFINAL.Value := QrResumoSALDO.Value + QrSaldoATOTAL.Value;
end;

procedure TFmReceDesp.QrSaldoACalcFields(DataSet: TDataSet);
begin
  QrSaldoATOTAL.Value := QrSaldoAInicial.Value + QrSaldoASALDO.Value;
end;

procedure TFmReceDesp.ReopenCreditosEDebitos(CliInt: String; DataI,
  DataF: String; Acordos, Periodos, Textos, NaoAgruparNada: Boolean; TabLctA,
  TabLctB, TabLctD: String);
  function OrdenaItens(ListBox: TListBox; Receitas: Boolean): String;
    function TraduzCampos(Texto: String; Receitas: Boolean): String;
    begin
      if Receitas then
      begin
        ///////////////////////////////////////////////////////////////////
        //GRU_OL  = Ordem do Grupo (Cadastro de grupos)
        //NOMEGRU = Nome do Grupo
        //SGR_OL  = Ordem do subgrupo (cadastro de subgrupos)
        //NOMESGR = Nome do Subgrupo
        //CON_OL  = Ordem da Conta (Cadastro de contas)
        //NOMECON = Nome da Conta
        //Mez     = M�s de compet�ncia
        //Data    = Data do Lan�amento
        ///////////////////////////////////////////////////////////////////
        if      LowerCase(Texto) = 'ordem do grupo (cadastro de grupos)' then
          Result := 'GRU_OL'
        else if LowerCase(Texto) = 'nome do grupo' then
          Result := 'NOMEGRU'
        else if LowerCase(Texto) = 'ordem do subgrupo (cadastro de subgrupos)' then
          Result := 'SGR_OL'
        else if LowerCase(Texto) = 'nome do subgrupo' then
          Result := 'NOMESGR'
        else if LowerCase(Texto) = 'ordem da conta (cadastro de contas)' then
          Result := 'CON_OL'
        else if LowerCase(Texto) = 'nome da conta' then
          Result := 'NOMECON'
        else if LowerCase(Texto) = 'm�s de compet�ncia' then
          Result := 'Mez'
        else if LowerCase(Texto) = 'data do lan�amento' then
          Result := 'Data'
        else
          Result := '';
      end else
      begin
        ///////////////////////////////////////////////////////////////////
        //GRU_OL  = Ordem do Grupo (Cadastro de grupos)
        //NOMEGRU = Nome do Grupo
        //Ordem do subgrupo (cadastro de subgrupos)
        //NOMESGR = Nome do Subgrupo
        //CON_OL  = Ordem da Conta (Cadastro de contas)
        //NOMECON = Nome da Conta
        //MEZ     = M�s de compet�ncia
        //Data    = Data do Lan�amento      
        ///////////////////////////////////////////////////////////////////
        if      LowerCase(Texto) = 'ordem do grupo (cadastro de grupos)' then
          Result := 'GRU_OL'
        else if LowerCase(Texto) = 'nome do grupo' then
          Result := 'NOMEGRU'
        else if LowerCase(Texto) = 'ordem do subgrupo (cadastro de subgrupos)' then
          Result := 'SGR_OL'
        else if LowerCase(Texto) = 'nome do subgrupo' then
          Result := 'NOMESGR'
        else if LowerCase(Texto) = 'ordem da conta (cadastro de contas)' then
          Result := 'CON_OL'
        else if LowerCase(Texto) = 'nome da conta' then
          Result := 'NOMECON'
        else if LowerCase(Texto) = 'm�s de compet�ncia' then
          Result := 'MEZ'
        else if LowerCase(Texto) = 'data do lan�amento' then
          Result := 'Data'
        else
          Result := '';
      end;
    end;
  var
    Campo, Ordem: String;
    i, TotReg: Integer;
  begin
    Ordem  := '';
    TotReg := ListBox.Count - 1;
    for i := 0 to TotReg do
    begin
      if i = 0 then
      begin
        Campo := TraduzCampos(ListBox.Items[i], Receitas);
        if Length(Campo) > 0 then
          Ordem := Campo + ', ';
      end
      else if i = TotReg then
      begin
        Campo := TraduzCampos(ListBox.Items[i], Receitas);
        if Length(Campo) > 0 then
          Ordem := Ordem + Campo + ';';
      end
      else
      begin
        Campo := TraduzCampos(ListBox.Items[i], Receitas);
        if Length(Campo) > 0 then
          Ordem := Ordem + Campo + ', ';
      end;
    end;
    Result := Ordem;
  end;
  procedure GeraParteSQL_Cred(TabLct: String);
  begin
    if NaoAgruparNada = True then
      QrCreditos.SQL.Add('SELECT lct.Mez, lct.Credito, "' +
      Uppercase(Copy(TabLct, Length(TabLct))) + '" TbLct, ')
    else
      QrCreditos.SQL.Add('SELECT lct.Mez, SUM(lct.Credito) Credito, "" TbLct, ');
    QrCreditos.SQL.Add('lct.Controle, lct.Sub, lct.Carteira, lct.Cartao, lct.Tipo,');
    QrCreditos.SQL.Add('lct.Vencimento, lct.Compensado, lct.Sit, lct.Genero, ');
    QrCreditos.SQL.Add('lct.SubPgto1, lct.Data, gru.OrdemLista GRU_OL, ');
    QrCreditos.SQL.Add('con.OrdemLista CON_OL, sgr.OrdemLista SGR_OL,');
    QrCreditos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU');
    QrCreditos.SQL.Add('FROM ' + TabLct + ' lct');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lct.Genero');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrCreditos.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
    QrCreditos.SQL.Add('WHERE lct.Tipo <> 2');
    QrCreditos.SQL.Add('AND lct.Credito > 0');
    QrCreditos.SQL.Add('AND lct.Genero>0');
    // 2011-12-31
    QrCreditos.SQL.Add('AND lct.Sit IN (' + CO_LIST_SITS_OKA + ')');
    // Fim 2011-12-31
    QrCreditos.SQL.Add('AND car.ForneceI=' + FEntidTXT);
    QrCreditos.SQL.Add('AND lct.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
    //Desativado por causa de condom�nios que foram para o morto e voltaram
    //nestes condom�nios duplica e nos demais n�o apresentaram problemas ao desabilitar
    //QrCreditos.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
    if NaoAgruparNada = False then
    begin
      QrCreditos.SQL.Add('GROUP BY lct.Genero ');
      if Periodos then
        QrCreditos.SQL.Add(', lct.Mez');
      if Acordos then
        QrCreditos.SQL.Add(', lct.SubPgto1');
    end;
  end;

  procedure GeraParteSQL_Debi(TabLct: String);
  begin
    if Textos and (NaoAgruparNada = False) then
    begin
      QrDebitos.SQL.Add('SELECT IF(COUNT(lct.Compensado) > 1, "V�rias", IF(lct.Compensado<2,"",');
      QrDebitos.SQL.Add('DATE_FORMAT(lct.Compensado, "%d/%m/%y"))) COMPENSADO_TXT,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, "V�rias", DATE_FORMAT(lct.Data, "%d/%m/%y")) DATA,');
      QrDebitos.SQL.Add('lct.Descricao, SUM(lct.Debito) DEBITO,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.NotaFiscal) NOTAFISCAL,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, "", lct.SerieCH) SERIECH,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.Documento) DOCUMENTO,');
      QrDebitos.SQL.Add('IF(COUNT(lct.Data)>1, 0, lct.Mez) MEZ, lct.Compensado,');
      QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
      QrDebitos.SQL.Add('COUNT(lct.Data) ITENS, "" TbLct, ');
    end else begin
      QrDebitos.SQL.Add('SELECT IF(lct.Compensado<2,"", DATE_FORMAT(');
      QrDebitos.SQL.Add('lct.Compensado, "%d/%m/%y")) COMPENSADO_TXT,');
      QrDebitos.SQL.Add('DATE_FORMAT(lct.Data, "%d/%m/%y") DATA, ');
      QrDebitos.SQL.Add('lct.Descricao, lct.Debito, ');
      QrDebitos.SQL.Add('IF(lct.NotaFiscal = 0, 0, lct.NotaFiscal) NOTAFISCAL,');
      QrDebitos.SQL.Add('IF(lct.SerieCH = "", "", lct.SerieCH) SERIECH,');
      QrDebitos.SQL.Add('IF(lct.Documento = 0, 0, lct.Documento) DOCUMENTO,');
      QrDebitos.SQL.Add('IF(lct.Mez = 0, 0, lct.Mez) MEZ, lct.Compensado,');
      QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
      QrDebitos.SQL.Add('0 ITENS, "' + Uppercase(Copy(TabLct, Length(TabLct))) + '" TbLct, ')
    end;
    QrDebitos.SQL.Add('lct.Vencimento, lct.Sit, lct.Genero, ');
    QrDebitos.SQL.Add('lct.Tipo, lct.Controle, lct.Sub, lct.Carteira, lct.Cartao,');
    QrDebitos.SQL.Add('gru.OrdemLista GRU_OL, con.OrdemLista CON_OL, ');
    QrDebitos.SQL.Add('sgr.OrdemLista SGR_OL, IF(frn.Codigo=0, "", ');
    QrDebitos.SQL.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)) NO_FORNECE');
    QrDebitos.SQL.Add('FROM ' + TabLct + ' lct');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lct.Genero');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
    QrDebitos.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades frn ON frn.Codigo=lct.Fornecedor');
    QrDebitos.SQL.Add('WHERE lct.Tipo <> 2');
    QrDebitos.SQL.Add('AND lct.Debito > 0');
    QrDebitos.SQL.Add('AND lct.Genero>0');
    // 2011-12-31
    QrDebitos.SQL.Add('AND lct.Sit IN (' + CO_LIST_SITS_OKA + ')');
    // Fim 2011-12-31
    QrDebitos.SQL.Add('AND car.ForneceI=' + FEntidTXT);
    QrDebitos.SQL.Add('AND lct.Data BETWEEN "' + FDataI1 + '" AND "' + FDataF1 + '"');
    //Desativado por causa de condom�nios que foram para o morto e voltaram
    //nestes condom�nios duplica e nos demais n�o apresentaram problemas ao desabilitar
    //QrDebitos.SQL.Add('AND lct.FatID <> ' + LAN_SDO_INI_FATID);  // <- CUIDADO!!! N�o tirar!
    //
    if CkNaoAgruparNada.Checked = False then
    begin
      if Textos then
        QrDebitos.SQL.Add('GROUP BY lct.Descricao');
    end;
  end;

begin
  QrCreditos.Database := DModG.MyPID_DB;
  QrDebitos.Database  := DModG.MyPID_DB;
  //
  //// C R � D I T O S
  QrCreditos.Close;
  QrCreditos.SQL.Clear;
  //
  QrCreditos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_CRED_1_;');
  QrCreditos.SQL.Add('CREATE TABLE _MOD_COND_CRED_1_ IGNORE ');
  QrCreditos.SQL.Add('');
  GeraParteSQL_Cred(TabLctA);
  QrCreditos.SQL.Add('UNION');
  GeraParteSQL_Cred(TabLctB);
  QrCreditos.SQL.Add('UNION');
  GeraParteSQL_Cred(TabLctD);
  QrCreditos.SQL.Add(';');
  QrCreditos.SQL.Add('');
  if NaoAgruparNada = False then
  begin
    QrCreditos.SQL.Add('SELECT Mez, SUM(Credito) Credito, Controle, Sub, ');
    QrCreditos.SQL.Add('Carteira, Cartao, Tipo, Vencimento, Compensado, Sit,');
    QrCreditos.SQL.Add('Genero, SubPgto1, Data, GRU_OL, CON_OL, SGR_OL, ');
    QrCreditos.SQL.Add('NOMECON, NOMESGR, NOMEGRU, TbLct');
    QrCreditos.SQL.Add('FROM _MOD_COND_CRED_1_');
    QrCreditos.SQL.Add('GROUP BY Genero ');
    if Periodos then
      QrCreditos.SQL.Add(', Mez');
    if Acordos then
      QrCreditos.SQL.Add(', SubPgto1');
  end else
    QrCreditos.SQL.Add('SELECT * FROM _MOD_COND_CRED_1_');
  //QrCreditos.SQL.Add('ORDER BY GRU_OL, NOMEGRU, SGR_OL, ');
  //QrCreditos.SQL.Add('NOMESGR, CON_OL, NOMECON, Mez, Data;');
  QrCreditos.SQL.Add('ORDER BY ' + OrdenaItens(LBReceitas, True));
  QrCreditos.SQL.Add('');
  // N�o pode !!
  //QrCreditos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_CRED_1_;');
  QrCreditos.SQL.Add('');
  //
  UMyMod.AbreQuery(QrCreditos, DModG.MyPID_DB, 'TDCond.ReopenCreditosEDebitos()');
  //
  //
  //// D � B I T O S
  QrDebitos.Close;
  QrDebitos.SQL.Clear;
  //
  QrDebitos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_DEBI_1_;');
  QrDebitos.SQL.Add('CREATE TABLE _MOD_COND_DEBI_1_ IGNORE ');
  QrDebitos.SQL.Add('');
  GeraParteSQL_Debi(TabLctA);
  QrDebitos.SQL.Add('UNION');
  GeraParteSQL_Debi(TabLctB);
  QrDebitos.SQL.Add('UNION');
  GeraParteSQL_Debi(TabLctD);
  QrDebitos.SQL.Add(';');
  QrDebitos.SQL.Add('');
  if (NaoAgruparNada = False) and (Textos) then
  begin
    QrDebitos.SQL.Add('SELECT COMPENSADO_TXT, DATA,Descricao, NO_FORNECE, ');
    QrDebitos.SQL.Add('SUM(DEBITO) DEBITO, NOTAFISCAL, SERIECH, DOCUMENTO, ');
    QrDebitos.SQL.Add('MEZ, Compensado, NOMECON, NOMESGR, NOMEGRU, ');
    QrDebitos.SQL.Add('ITENS, Vencimento, Sit, Genero, Tipo, Controle,');
    QrDebitos.SQL.Add('Sub, Carteira, Cartao, GRU_OL, CON_OL, SGR_OL, TbLct');
    QrDebitos.SQL.Add('FROM _MOD_COND_DEBI_1_');
    QrDebitos.SQL.Add('GROUP BY Descricao')
  end else
    QrDebitos.SQL.Add('SELECT * FROM _MOD_COND_DEBI_1_');
  //QrDebitos.SQL.Add('ORDER BY GRU_OL, NOMEGRU, SGR_OL,');
  //QrDebitos.SQL.Add('NOMESGR, CON_OL, NOMECON, Data;');
  QrDebitos.SQL.Add('ORDER BY ' + OrdenaItens(LBDespesas, False));
  //
  // N�o pode !!
  //QrDebitos.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_DEBI_1_;');
  UMyMod.AbreQuery(QrDebitos, DModG.MyPID_DB, 'procedure TDmCond.ReopenDEBIitosEDebitos()');
end;

procedure TFmReceDesp.RetornosCNAB();
begin
  if FEmpresa > 0 then
  begin
    QrRet.Close;
    QrRet.SQL.Clear;
    QrRet.SQL.Add('SELECT *');
    QrRet.SQL.Add('FROM cnab_lei');
    QrRet.SQL.Add('WHERE Entidade=' + dmkPF.FFP(FEntidade, 0));
    QrRet.SQL.Add(dmkPF.SQL_Periodo('AND QuitaData ', TPDataIni2.Date,
      TPDataFim2.Date, True, True));
    QrRet.SQL.Add(dmkPF.SQL_Periodo('AND OcorrData ', TPDataIni2.Date,
      TPDataFim2.Date, True, True));
    QrRet.SQL.Add('AND ValTitul BETWEEN ' +
      dmkPF.FFP(EdValTitMin.ValueVariant, 2) + ' AND '  +
      dmkPF.FFP(EdValTitMax.ValueVariant, 2));
    QrRet.SQL.Add('AND ValPago BETWEEN ' +
      dmkPF.FFP(EdValPagMin.ValueVariant, 2) + ' AND '  +
      dmkPF.FFP(EdValPagMax.ValueVariant, 2));
    if EdBloqIni.ValueVariant > 0 then
      QrRet.SQL.Add('AND IDNum >= ' + dmkPF.FFP(EdBloqIni.ValueVariant, 0));
    if EdBloqFim.ValueVariant > 0 then
      QrRet.SQL.Add('AND IDNum <= ' + dmkPF.FFP(EdBloqIni.ValueVariant, 0));
    UnDmkDAC_PF.AbreQuery(QrRet, Dmod.MyDB);
  end else begin
    Geral.MB_Aviso('Informe a empresa!');
    EdEmpresa.SetFocus;
  end;
end;

{
SELECT pla.OrdemLista OL_PLA, cjt.Plano, pla.Nome NO_PLA,
cjt.OrdemLista OL_CJT, gru.Conjunto, cjt.Nome NO_CJT,
gru.OrdemLista OL_GRU, sgr.Grupo, gru.Nome NO_GRU,
sgr.OrdemLista OL_SGR, cta.SubGrupo, sgr.Nome NO_SGR,
cta.OrdemLista OL_CTA, lct.Genero, cta.Nome NO_CTA,
SUM(lct.Credito) Credito
FROM lct0046a lct
LEFT JOIN contas cta ON cta.Codigo=lct.Genero
LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo
LEFT JOIN grupos gru ON gru.Codigo=sgr.Grupo
LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto
LEFT JOIN plano pla ON pla.Codigo=cjt.Plano
LEFT JOIN carteiras car ON car.Codigo=lct.Carteira
WHERE lct.Genero > 0
AND lct.Data BETWEEN "2011-01-01" AND "2012-04-30"
AND lct.Credito > 0
AND car.Tipo IN (0,1)
GROUP BY lct.Genero
ORDER BY OL_PLA, NO_PLA, OL_CJT, NO_CJT, OL_GRU, NO_GRU,
OL_SGR, NO_SGR, OL_CTA, NO_CTA
}

end.

