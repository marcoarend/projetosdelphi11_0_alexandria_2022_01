unit Pesquisas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Db, (*DBTables,*) DBCtrls, ComCtrls,
  UnInternalConsts, UnMsgInt, UnGOTOy, Variants, mySQLDbTables, Grids, DBGrids,
  frxClass, frxDBSet, dmkGeral, dmkEdit, dmkDBLookupComboBox, dmkEditCB,
  dmkImage, UnDmkProcFunc, UnDmkEnums;

type
  TFmPesquisas = class(TForm)
    PainelDados: TPanel;
    QrNivelSel: TmySQLQuery;
    DsNivelSel: TDataSource;
    QrLct1: TmySQLQuery;
    QrLct1Data: TDateField;
    QrLct1Tipo: TSmallintField;
    QrLct1Carteira: TIntegerField;
    QrLct1Sub: TSmallintField;
    QrLct1Autorizacao: TIntegerField;
    QrLct1Genero: TIntegerField;
    QrLct1Descricao: TWideStringField;
    QrLct1NotaFiscal: TIntegerField;
    QrLct1Debito: TFloatField;
    QrLct1Credito: TFloatField;
    QrLct1Compensado: TDateField;
    QrLct1Documento: TFloatField;
    QrLct1Sit: TIntegerField;
    QrLct1Vencimento: TDateField;
    QrLct1Lk: TIntegerField;
    QrLct1FatID: TIntegerField;
    QrLct1FatParcela: TIntegerField;
    QrLct1Fatura: TWideStringField;
    QrLct1Banco: TIntegerField;
    QrLct1Local: TIntegerField;
    QrLct1Cartao: TIntegerField;
    QrLct1Linha: TIntegerField;
    QrNivelSelCodigo: TIntegerField;
    QrNivelSelNome: TWideStringField;
    QrLct1NOMETIPO: TWideStringField;
    QrLct1NOMEGENERO: TWideStringField;
    QrLct1NOMECARTEIRA: TWideStringField;
    QrLct1Ano: TFloatField;
    QrLct1MENSAL: TWideStringField;
    QrLct1MENSAL2: TWideStringField;
    QrLct1ID_Sub: TSmallintField;
    QrLct1Pago: TFloatField;
    QrLct1Mez: TIntegerField;
    QrLct1Controle: TIntegerField;
    QrLct1ID_Pgto: TIntegerField;
    QrLct1Mes2: TLargeintField;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    DsLct1: TDataSource;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrEntidades: TmySQLQuery;
    DsClientes: TDataSource;
    DsFornecedores: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrLct1OperCount: TIntegerField;
    QrLct1Lancto: TIntegerField;
    QrLct1Fornecedor: TIntegerField;
    QrLct1Cliente: TIntegerField;
    QrLct1MoraDia: TFloatField;
    QrLct1Multa: TFloatField;
    QrLct1Protesto: TDateField;
    QrLct1DataCad: TDateField;
    QrLct1DataAlt: TDateField;
    QrLct1UserCad: TIntegerField;
    QrLct1UserAlt: TIntegerField;
    QrLct1DataDoc: TDateField;
    QrLct1CtrlIni: TIntegerField;
    QrLct1Nivel: TIntegerField;
    QrLct1Vendedor: TIntegerField;
    QrLct1Account: TIntegerField;
    QrLct1FatID_Sub: TIntegerField;
    QrLct1ICMS_P: TFloatField;
    QrLct1ICMS_V: TFloatField;
    QrLct1Duplicata: TWideStringField;
    QrLct1CliInt: TIntegerField;
    QrLct1Depto: TIntegerField;
    QrLct1DescoPor: TIntegerField;
    QrLct1ForneceI: TIntegerField;
    QrLct1Qtde: TFloatField;
    QrLct1Emitente: TWideStringField;
    QrLct1ContaCorrente: TWideStringField;
    QrLct1CNPJCPF: TWideStringField;
    QrLct1DescoVal: TFloatField;
    QrLct1DescoControle: TIntegerField;
    QrLct1NFVal: TFloatField;
    QrLct1Antigo: TWideStringField;
    QrLct1NOMEENTIDADE: TWideStringField;
    QrLct1CREDEB: TFloatField;
    QrLct1FatNum: TFloatField;
    frxConta1a: TfrxReport;
    frxDsLct1: TfrxDBDataset;
    frxDsContas: TfrxDBDataset;
    frxConta1: TfrxReport;
    frxConta2a: TfrxReport;
    QrCarteirasForneceI: TIntegerField;
    Panel2: TPanel;
    CkPeriodo: TCheckBox;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    CkCompetencia: TCheckBox;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    CBMesIni: TComboBox;
    CBAnoIni: TComboBox;
    CBMesFim: TComboBox;
    CBAnoFim: TComboBox;
    RGTipo: TRadioGroup;
    RGNivel: TRadioGroup;
    Label1: TLabel;
    EdNivelSel: TdmkEditCB;
    CBNivelSel: TdmkDBLookupComboBox;
    Label8: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    LaCliente: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    LaFornece: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    CkCliente: TCheckBox;
    Label10: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrLct1NOME_SG: TWideStringField;
    QrLct1NOME_GR: TWideStringField;
    frxConta2b: TfrxReport;
    QrLct1Agencia: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    BitBtn1: TBitBtn;
    Label9: TLabel;
    EdArq: TEdit;
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrLct1CalcFields(DataSet: TDataSet);
    procedure CBNivelSelClick(Sender: TObject);
    procedure CkCompetenciaClick(Sender: TObject);
    procedure CkPeriodoClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure frxConta1aGetValue(const VarName: String;
      var Value: Variant);
    procedure RGNivelClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    FTabLctA, FTabLctB, FTabLctD: String;
    procedure ReopenNivelSel();
  public
    { Public declarations }
    FCliente_Txt, FFornece_Txt: String;
  end;

var
  FmPesquisas: TFmPesquisas;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmPesquisas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesquisas.BtConfirmaClick(Sender: TObject);

  procedure GeraParteSQL(TabLct, Nivel, NivelSel, Empresa_Txt: String);
  var
    CIni, CFim, Ini, Fim: String;
    AnoI, AnoF, MesI, MesF: Word;
    Carteira: Integer;
  begin
    QrLct1.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
    QrLct1.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
    QrLct1.SQL.Add('(la.Credito-la.Debito) CREDEB, la.*, co.Nome NOMEGENERO,');
    QrLct1.SQL.Add('ca.Nome NOMECARTEIRA, sg.Nome NOME_SG, gr.Nome NOME_GR,');
    QrLct1.SQL.Add('IF(la.Fornecedor<>0,');
    QrLct1.SQL.Add('CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ELSE fo.Nome END,');
    QrLct1.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END) NOMEENTIDADE');
    QrLct1.SQL.Add('FROM ' + TabLct + ' la');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas co    ON co.Codigo=la.Genero');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sg ON sg.Codigo=co.Subgrupo');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gr ON gr.Codigo=sg.Grupo');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.conjuntos cj ON cj.Codigo=gr.Conjunto');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.plano     pl ON pl.Codigo=cj.Plano');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras ca ON ca.Codigo=la.Carteira');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades fo ON fo.Codigo=la.Fornecedor');
    QrLct1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades cl ON cl.Codigo=la.Cliente');
    QrLct1.SQL.Add('WHERE ' + Nivel + '=' + NivelSel);
    QrLct1.SQL.Add('AND ca.ForneceI=' + Empresa_Txt);
    if CkPeriodo.Checked then
    begin
      Ini := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
      Fim := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
      QrLct1.SQL.Add('AND la.Data BETWEEN '''+Ini+''' AND '''+Fim+'''');
    end;
    //
    Carteira := Geral.IMV(EdCarteira.Text);
    if Carteira <> 0 then
    QrLct1.SQL.Add('AND la.Carteira='''+IntToStr(Carteira)+'''');
    //
    QrLct1.SQL.Add(dmkPF.SQL_CodTxt('AND la.Cliente',
      EdCliente.Text, False, True, False));
    QrLct1.SQL.Add(dmkPF.SQL_CodTxt('AND la.Fornecedor',
      EdFornece.Text, False, True, False));
    //
    if CkCompetencia.Checked and CkCompetencia.Enabled then
    begin
      AnoI := Geral.IMV(CBAnoIni.Text);
      AnoF := Geral.IMV(CBAnoFim.Text);
      MesI := CBMesIni.ItemIndex + 1;
      MesF := CBMesFim.ItemIndex + 1;
      CIni := IntToStr(((AnoI-2000)*100)+MesI);
      CFim := IntToStr(((AnoF-2000)*100)+MesF);
      QrLct1.SQL.Add('AND la.Mez BETWEEN '''+CIni+''' AND '''+CFim+'''');
    end;
  end;

var
  Nivel, NivelSel, Empresa_Txt: String;
  CliInt, Empresa: Integer;
  Relatorio: TfrxReport;
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  Empresa  := DModG.QrEmpresasCodigo.Value;
  CliInt   := DModG.QrEmpresasFilial.Value;
  FTabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
  FTabLctB := DModG.NomeTab(TMeuDB, ntLct, False, ttB, CliInt);
  FTabLctD := DModG.NomeTab(TMeuDB, ntLct, False, ttD, CliInt);
  //
  //
  Relatorio := nil;
  if RGTipo.ItemIndex = 0 then
  begin
    if MyObjects.FIC(EdNivelSel.ValueVariant = 0, EdNivelSel,
    'Informe o g�nero do n�vel selecionado') then Exit;
    Nivel       := dmkPF.NomeDoNivelSelecionado(2, RGNivel.ItemIndex);
    NivelSel    := FormatFloat('0', EdNivelSel.ValueVariant);
    Empresa_Txt := FormatFloat('0', Empresa);
    //
    QrLct1.Close;
    QrLct1.SQL.Clear;
{
SELECT MOD(la.Mez, 100) Mes2,
((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,
(la.Credito-la.Debito) CREDEB, la.*, co.Nome NOMEGENERO,
ca.Nome NOMECARTEIRA, sg.Nome NOME_SG, gr.Nome NOME_GR,
IF(la.Fornecedor<>0,
CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ELSE fo.Nome END,
CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END) NOMEENTIDADE
FROM lct la
LEFT JOIN contas co    ON co.Codigo=la.Genero
LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo
LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo
LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto
LEFT JOIN plano     pl ON pl.Codigo=cj.Plano
LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira
LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor
LEFT JOIN entidades cl ON cl.Codigo=la.Cliente
}
    QrLct1.SQL.Add('DROP TABLE IF EXISTS _fin_relat_001_lct1_;');
    QrLct1.SQL.Add('CREATE TABLE _fin_relat_001_lct1_');
    QrLct1.SQL.Add('');
    GeraParteSQL(FTabLctA, Nivel, NivelSel, Empresa_Txt);
    QrLct1.SQL.Add('UNION');
    GeraParteSQL(FTabLctB, Nivel, NivelSel, Empresa_Txt);
    QrLct1.SQL.Add('UNION');
    GeraParteSQL(FTabLctD, Nivel, NivelSel, Empresa_Txt);
    QrLct1.SQL.Add(';');
    QrLct1.SQL.Add('');
    QrLct1.SQL.Add('SELECT * FROM _fin_relat_001_lct1_');
    if CkCompetencia.Checked and CkCompetencia.Enabled then
    begin
      if CkCliente.Checked then
        QrLct1.SQL.Add('ORDER BY NOMEENTIDADE, Ano, Mes2, Data')
      else
        QrLct1.SQL.Add('ORDER BY Ano, Mes2, Data, Mez');
      //UnDmkDAC_PF.AbreQuery(QrLct1, DModG.MyPID_DB);
      if CkCliente.Checked then
        //MyObjects.frxMostra(frxConta2b, 'Movimento de conta')
        Relatorio := frxConta2b
      else
        //MyObjects.frxMostra(frxConta2a, 'Movimento de conta');
        Relatorio := frxConta2a
    end else begin
      if CkCliente.Checked then
      begin
        QrLct1.SQL.Add('ORDER BY NOMEENTIDADE, Data, Mez');
        //UnDmkDAC_PF.AbreQuery(QrLct1, DModG.MyPID_DB);
        //MyObjects.frxMostra(frxConta1a, 'Movimento de conta');
        Relatorio := frxConta1a;
      end else begin
        QrLct1.SQL.Add('ORDER BY Data, Mez');
        //UnDmkDAC_PF.AbreQuery(QrLct1, DModG.MyPID_DB);
        //MyObjects.frxMostra(frxConta1, 'Movimento de conta');
        Relatorio := frxConta1;
      end;
    end;
    UnDmkDAC_PF.AbreQuery(QrLct1, DModG.MyPID_DB);
    if Relatorio <> nil then
    begin
        MyObjects.frxDefineDataSets(Relatorio, [
          DModG.frxDsDono,
          frxDsContas,
          frxDsLct1
        ]);
        MyObjects.frxMostra(relatorio, 'Movimento de conta');
    end else
      Geral.MB_Aviso('Relat�rio nao definido!');
{

    QrLct1.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
    QrLct1.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
    QrLct1.SQL.Add('(la.Credito-la.Debito) CREDEB, la.*, co.Nome NOMEGENERO,');
    QrLct1.SQL.Add('ca.Nome NOMECARTEIRA, sg.Nome NOME_SG, gr.Nome NOME_GR,');
    QrLct1.SQL.Add('IF(la.Fornecedor<>0,');
    QrLct1.SQL.Add('CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ELSE fo.Nome END,');
    QrLct1.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END) NOMEENTIDADE');
    QrLct1.SQL.Add('FROM ' + VAR LCT + ' la');
    QrLct1.SQL.Add('LEFT JOIN contas    co ON co.Codigo=la.Genero');
    QrLct1.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo');
    QrLct1.SQL.Add('LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo');
    QrLct1.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
    QrLct1.SQL.Add('LEFT JOIN plano     pl ON pl.Codigo=cj.Plano');
    QrLct1.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
    QrLct1.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
    QrLct1.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
    QrLct1.SQL.Add('WHERE ' + Nivel + '=' + FormatFloat('0', EdNivelSel.ValueVariant));
    QrLct1.SQL.Add('AND ca.ForneceI=' + FormatFloat('0', Empresa));
    if CkPeriodo.Checked then
    begin
      Ini := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
      Fim := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
      QrLct1.SQL.Add('AND la.Data BETWEEN '''+Ini+''' AND '''+Fim+'''');
    end;
    //
    Carteira := Geral.IMV(EdCarteira.Text);
    if Carteira <> 0 then
    QrLct1.SQL.Add('AND la.Carteira='''+IntToStr(Carteira)+'''');
    //
    QrLct1.SQL.Add(dmkPF.SQL_CodTxt('AND la.Cliente',
      EdCliente.Text, False, True, False));
    QrLct1.SQL.Add(dmkPF.SQL_CodTxt('AND la.Fornecedor',
      EdFornece.Text, False, True, False));
    //
    if CkCompetencia.Checked and CkCompetencia.Enabled then
    begin
      AnoI := Geral.IMV(CBAnoIni.Text);
      AnoF := Geral.IMV(CBAnoFim.Text);
      MesI := CBMesIni.ItemIndex + 1;
      MesF := CBMesFim.ItemIndex + 1;
      CIni := IntToStr(((AnoI-2000)*100)+MesI);
      CFim := IntToStr(((AnoF-2000)*100)+MesF);
      QrLct1.SQL.Add('AND la.Mez BETWEEN '''+CIni+''' AND '''+CFim+'''');
      //
      if CkCliente.Checked then
        QrLct1.SQL.Add('ORDER BY NOMEENTIDADE, Ano, Mes2, la.Data')
      else
        QrLct1.SQL.Add('ORDER BY Ano, Mes2, la.Data, la.Mez');
      UnDmkDAC_PF.AbreQuery(QrLct1, DModG.MyPID_DB);
      MyObjects.frxMostra(frxConta2, 'Movimento de conta');
    end else begin
      if CkCliente.Checked then
      begin
        QrLct1.SQL.Add('ORDER BY NOMEENTIDADE, la.Data, la.Mez');
        UnDmkDAC_PF.AbreQuery(QrLct1, DModG.MyPID_DB);
        MyObjects.frxMostra(frxConta1a, 'Movimento de conta');
      end else begin
        QrLct1.SQL.Add('ORDER BY la.Data, la.Mez');
        UnDmkDAC_PF.AbreQuery(QrLct1, DModG.MyPID_DB);
        MyObjects.frxMostra(frxConta1, 'Movimento de conta');
      end;
    end;
    }
    //QrLct1.Close;
  end;
end;

procedure TFmPesquisas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  QrLct1.Close;
  QrLct1.Database := DModG.MyPID_DB;
  //
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  //
  TPIni.Date := Geral.PrimeiroDiaDoMes(Date);
  TPFim.Date := Geral.UltimoDiaDoMes(Date);
  //
  MyObjects.PreencheCBAnoECBMes(CBAnoIni, CBMesIni);
  MyObjects.PreencheCBAnoECBMes(CBAnoFim, CBMesFim);
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmPesquisas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if FCliente_Txt <> '' then
  begin
    if FCliente_Txt = '-' then
    begin
      LaCliente.Caption := ' ';
      LaCliente.Visible := False;
      EdCliente.Visible := False;
      CBCliente.Visible := False;
      CkCliente.Visible := False;
    end else
    begin
      LaCliente.Caption := FCliente_Txt;
      CkCliente.Caption := 'Agrupar por '+FCliente_Txt
    end;
  end;
  if FFornece_Txt <> '' then
  begin
    if FFornece_Txt = '-' then
    begin
      LaFornece.Caption := ' ';
      LaFornece.Visible := False;
      EdFornece.Visible := False;
      CBFornece.Visible := False;
    end else LaFornece.Caption := FFornece_Txt;
  end;
end;

procedure TFmPesquisas.QrLct1CalcFields(DataSet: TDataSet);
begin
  if QrLct1Mes2.Value > 0 then
    QrLct1MENSAL.Value := FormatFloat('00', QrLct1Mes2.Value)+'/'
    +Copy(FormatFloat('0000', QrLct1Ano.Value), 3, 2)
   else QrLct1MENSAL.Value := CO_VAZIO;
  if QrLct1Mes2.Value > 0 then
    QrLct1MENSAL2.Value := FormatFloat('0000', QrLct1Ano.Value)+'/'+
    FormatFloat('00', QrLct1Mes2.Value)+'/01'
   else QrLct1MENSAL2.Value := CO_VAZIO;
  QrLct1NOMETIPO.Value :=
    dmkPF.TipoDeCarteiraCashier(QrLct1Tipo.Value, True);
end;

procedure TFmPesquisas.ReopenNivelSel();
var
  Nivel: String;
begin
  if RGNivel.ItemIndex = 0 then
  begin
    QrNivelSel.Close;
    Exit;
  end;
  //
  Nivel := dmkPF.NomeDoNivelSelecionado(1, RGNivel.ItemIndex);
  //
  QrNivelSel.Close;
  QrNivelSel.SQL.Clear;
  QrNivelSel.SQL.Add('SELECT Codigo, Nome');
  QrNivelSel.SQL.Add('FROM ' + Nivel);
  //QrNivelSel.SQL.Add('WHERE Codigo > 0');
  QrNivelSel.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrNivelSel, Dmod.MyDB);
  //
  EdNivelSel.ValueVariant := 0;
  CBNivelSel.KeyValue     := Null;
end;

procedure TFmPesquisas.RGNivelClick(Sender: TObject);
begin
  ReopenNivelSel();
end;

procedure TFmPesquisas.CkCompetenciaClick(Sender: TObject);
begin
  if CkCompetencia.Checked then
  begin
    Label4.Enabled := True;
    Label5.Enabled := True;
    Label6.Enabled := True;
    Label7.Enabled := True;
    CBMesIni.Enabled := True;
    CBMesFim.Enabled := True;
    CBAnoIni.Enabled := True;
    CBAnoFim.Enabled := True;
  end else begin
    Label4.Enabled := False;
    Label5.Enabled := False;
    Label6.Enabled := False;
    Label7.Enabled := False;
    CBMesIni.Enabled := False;
    CBMesFim.Enabled := False;
    CBAnoIni.Enabled := False;
    CBAnoFim.Enabled := False;
  end;
end;

procedure TFmPesquisas.CkPeriodoClick(Sender: TObject);
begin
  if CkPeriodo.Checked then
  begin
    Label2.Enabled   := True;
    Label3.Enabled   := True;
    TPIni.Enabled    := True;
    TPFim.Enabled    := True;
  end else begin
    Label2.Enabled   := False;
    Label3.Enabled   := False;
    TPIni.Enabled    := False;
    TPFim.Enabled    := False;
  end;
end;

procedure TFmPesquisas.EdEmpresaChange(Sender: TObject);
begin
  FTabLctA := '?';
  FTabLctB := '?';
  FTabLctD := '?';
end;

procedure TFmPesquisas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesquisas.CBNivelSelClick(Sender: TObject);
begin
  {
  if QrContasMensal.Value = 'F' then
  CkCompetencia.Enabled := False
  else
  }CkCompetencia.Enabled := True;
end;

procedure TFmPesquisas.BitBtn1Click(Sender: TObject);
begin
  {
  Application.CreateForm(TFmExportExcelDBGrid, FmExportExcelDBGrid);
  //FmExportExcelDBGrid.DBGrid1.DataSource := DBGrid1.DataSource;
  FmExportExcelDBGrid.XLSExportDBGrid1.DBGrid := DBGrid1;
  FmExportExcelDBGrid.FFileName := EdArq.Text;
  FmExportExcelDBGrid.ShowModal;
  FmExportExcelDBGrid.Destroy;
  }
end;

procedure TFmPesquisas.frxConta1aGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'PERIODO' then
  begin
    if CkPeriodo.Checked then
      Value :=
      FormatDateTime(VAR_FORMATDATE3, TPIni.Date)+ CO_ATE+
      FormatDateTime(VAR_FORMATDATE3, TPFim.Date)
    else Value := 'Indefinido';
  end;
  if VarName = 'COMPETENCIA' then
  begin
    if CkCompetencia.Checked then
      Value :=
      CBMesIni.Text+' / '+CBAnoIni.Text+ CO_ATE+
      CBMesFim.Text+' / '+CBAnoFim.Text
    else Value := 'Indefinida';
  end
  else if VarName = 'VARF_CLIENTE_TIT' then Value := LaCliente.Caption
  else if VarName = 'VARF_FORNECE_TIT' then Value := LaFornece.Caption
  else if VarName = 'VARF_CLIENTENOME' then
  begin
    if not EdCliente.Visible then Value := ' ' else Value :=
    dmkPF.ParValueCodTxt('', CBCliente.Text, EdCliente.ValueVariant)
  end else if VarName = 'VARF_FORNECENOME' then
  begin
    if not EdFornece.Visible then Value := ' ' else Value :=
    dmkPF.ParValueCodTxt('', CBFornece.Text, EdFornece.ValueVariant)
  end else if VarName = 'NOMEREL' then
    Value := 'Movimento ' + dmkPF.NomeDoNivelSelecionado(3, RGNivel.ItemIndex) + ' ' + CBNivelSel.Text
  else if VarName = 'VAR_NOMEEMPRESA' then
    Value := CBEmpresa.Text
end;

end.

