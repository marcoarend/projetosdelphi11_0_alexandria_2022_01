object FmFluxoCxa: TFmFluxoCxa
  Left = 227
  Top = 173
  Caption = 'FIN-FLCXA-001 :: Fluxo de caixa'
  ClientHeight = 638
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1016
    Height = 476
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1016
      Height = 72
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label3: TLabel
        Left = 416
        Top = 20
        Width = 534
        Height = 13
        Caption = 
          '* Data em que se estima recebimentos (cr'#233'ditos) e/ou pagamentos ' +
          '(d'#233'bitos) de valores vencidos e n'#227'o quitados. '
      end
      object Label4: TLabel
        Left = 416
        Top = 36
        Width = 432
        Height = 13
        Caption = 
          '** Se a data informada for inferior a data inicial, os valores v' +
          'encidos ser'#227'o desconsiderados.'
      end
      object GroupBox1: TGroupBox
        Left = 5
        Top = 5
        Width = 200
        Height = 64
        Caption = ' Per'#237'odo: '
        TabOrder = 0
        object CkIni: TCheckBox
          Left = 8
          Top = 16
          Width = 90
          Height = 17
          Caption = 'Data inicial:'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object TPIni: TDateTimePicker
          Left = 8
          Top = 36
          Width = 90
          Height = 21
          CalColors.TextColor = clMenuText
          Date = 37636.000000000000000000
          Time = 0.777157974502188200
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object CkFim: TCheckBox
          Left = 100
          Top = 20
          Width = 90
          Height = 17
          Caption = 'Data final:'
          TabOrder = 2
        end
        object TPFim: TDateTimePicker
          Left = 100
          Top = 36
          Width = 90
          Height = 21
          Date = 37636.000000000000000000
          Time = 0.777203761601413100
          TabOrder = 3
        end
      end
      object GroupBox2: TGroupBox
        Left = 208
        Top = 4
        Width = 201
        Height = 65
        Caption = ' Datas futuras de valores pendentes*: '
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 20
          Width = 49
          Height = 13
          Caption = 'Cr'#233'ditos**:'
        end
        object Label2: TLabel
          Left = 100
          Top = 20
          Width = 47
          Height = 13
          Caption = 'D'#233'bitos**:'
        end
        object TPCre: TDateTimePicker
          Left = 8
          Top = 36
          Width = 90
          Height = 21
          CalColors.TextColor = clMenuText
          Date = 37636.000000000000000000
          Time = 0.777157974502188200
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object TPDeb: TDateTimePicker
          Left = 100
          Top = 36
          Width = 90
          Height = 21
          Date = 37636.000000000000000000
          Time = 0.777203761601413100
          TabOrder = 1
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 72
      Width = 1016
      Height = 404
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel6: TPanel
        Left = 0
        Top = 355
        Width = 1016
        Height = 32
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        object Label5: TLabel
          Left = 12
          Top = 8
          Width = 30
          Height = 13
          Caption = 'Inicial:'
          FocusControl = DBEdit1
        end
        object Label6: TLabel
          Left = 156
          Top = 8
          Width = 65
          Height = 13
          Caption = 'Mov. anterior:'
          FocusControl = DBEdit2
        end
        object Label7: TLabel
          Left = 324
          Top = 8
          Width = 39
          Height = 13
          Caption = 'VALOR:'
          FocusControl = DBEdit3
        end
        object DBEdit1: TDBEdit
          Left = 48
          Top = 4
          Width = 81
          Height = 21
          DataField = 'Valor'
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 228
          Top = 4
          Width = 81
          Height = 21
          DataField = 'Movim'
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 368
          Top = 4
          Width = 85
          Height = 21
          DataField = 'VALOR'
          TabOrder = 2
        end
      end
      object PB1: TProgressBar
        Left = 0
        Top = 387
        Width = 1016
        Height = 17
        Align = alBottom
        TabOrder = 1
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 1016
        Height = 355
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 2
        OnChange = PageControl1Change
        object TabSheet1: TTabSheet
          Caption = 'Pesquisa de lan'#231'amentos'
          object DBGPesq: TDBGrid
            Left = 0
            Top = 0
            Width = 1008
            Height = 327
            Align = alClient
            DataSource = DsFluxo
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Fluxo de caixa'
          ImageIndex = 1
          object DBGFluxo: TDBGrid
            Left = 0
            Top = 0
            Width = 1008
            Height = 327
            Align = alClient
            DataSource = DsExtrato
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'DataX'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'Data fluxo'
                Width = 60
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Docum'
                Title.Caption = 'Documento'
                Width = 87
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'NotaF'
                Title.Caption = 'Nota fiscal'
                Width = 66
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Texto'
                Title.Caption = 'Hist'#243'rico'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Credi'
                Title.Caption = 'Cr'#233'dito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debit'
                Title.Caption = 'D'#233'bito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Saldo'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Width = 76
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'DataE'
                Title.Caption = 'Emiss'#227'o'
                Width = 56
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'DataV'
                Title.Caption = 'Vencto'
                Width = 56
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'DataQ'
                Title.Caption = 'Quita'#231#227'o'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CartN'
                Title.Caption = 'Carteira'
                Width = 140
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Ctrle'
                Title.Caption = 'Controle'
                Visible = True
              end>
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Provis'#245'es'
          ImageIndex = 2
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 1008
            Height = 327
            Align = alClient
            DataSource = DsPRI
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'PERIODO_TXT'
                Title.Caption = 'M'#234's'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Texto'
                Title.Caption = 'Hist'#243'rico'
                Width = 500
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Title.Caption = 'Provis'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'EmitVal'
                Title.Caption = 'Realizado'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DIFER'
                Title.Caption = 'Diferen'#231'a'
                Visible = True
              end
              item
                Expanded = False
                Title.Color = clActiveBorder
                Width = 6
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Conta'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrevBaI'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Periodo'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 968
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 920
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 173
        Height = 32
        Caption = 'Fluxo de caixa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 173
        Height = 32
        Caption = 'Fluxo de caixa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 173
        Height = 32
        Caption = 'Fluxo de caixa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 524
    Width = 1016
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1012
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 568
    Width = 1016
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1012
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 868
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtPesquisa: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtPesquisaClick
      end
      object BtPRI: TBitBtn
        Tag = 14
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Prev Its'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtPRIClick
      end
      object BtGera: TBitBtn
        Tag = 14
        Left = 268
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Gera'
        NumGlyphs = 2
        TabOrder = 3
      end
    end
  end
  object QrFluxo: TMySQLQuery
    Left = 12
    Top = 8
    object QrFluxoData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrFluxoTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrFluxoCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrFluxoControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFluxoSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrFluxoAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrFluxoGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrFluxoQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrFluxoDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrFluxoNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrFluxoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrFluxoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrFluxoCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrFluxoSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrFluxoDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrFluxoSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrFluxoVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrFluxoFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrFluxoFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrFluxoFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrFluxoID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrFluxoID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrFluxoFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrFluxoEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrFluxoBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrFluxoContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrFluxoCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrFluxoLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrFluxoCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrFluxoLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrFluxoOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrFluxoLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrFluxoPago: TFloatField
      FieldName = 'Pago'
    end
    object QrFluxoMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrFluxoFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrFluxoCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrFluxoCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrFluxoForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrFluxoMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrFluxoMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrFluxoMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrFluxoMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrFluxoProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrFluxoDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrFluxoCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrFluxoNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrFluxoVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrFluxoAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrFluxoICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrFluxoICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrFluxoDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrFluxoDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrFluxoDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrFluxoDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrFluxoDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrFluxoUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrFluxoNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrFluxoAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrFluxoExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrFluxoDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrFluxoCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrFluxoTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrFluxoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFluxoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFluxoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFluxoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFluxoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFluxoNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrFluxoQUITACAO: TDateField
      FieldName = 'QUITACAO'
    end
    object QrFluxoAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsFluxo: TDataSource
    DataSet = QrFluxo
    Left = 40
    Top = 8
  end
  object QrInicial: TMySQLQuery
    Database = Dmod.MyDB
    Left = 68
    Top = 8
    object QrInicialValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrMovant: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMovantCalcFields
    Left = 96
    Top = 8
    object QrMovantMovim: TFloatField
      FieldName = 'Movim'
    end
    object QrMovantVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR'
      Calculated = True
    end
  end
  object QrLct: TMySQLQuery
    Left = 125
    Top = 9
  end
  object QrExtrato: TMySQLQuery
    BeforeOpen = QrExtratoBeforeOpen
    SQL.Strings = (
      'SELECT * FROM extratocc2'
      'ORDER BY Codig')
    Left = 152
    Top = 8
    object QrExtratoDataE: TDateField
      FieldName = 'DataE'
      Origin = 'extratocc2.DataE'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrExtratoDataV: TDateField
      FieldName = 'DataV'
      Origin = 'extratocc2.DataV'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrExtratoDataQ: TDateField
      FieldName = 'DataQ'
      Origin = 'extratocc2.DataQ'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrExtratoDataX: TDateField
      FieldName = 'DataX'
      Origin = 'extratocc2.DataX'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrExtratoTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'extratocc2.Texto'
      Size = 255
    end
    object QrExtratoDocum: TWideStringField
      FieldName = 'Docum'
      Origin = 'extratocc2.Docum'
      Size = 30
    end
    object QrExtratoNotaF: TWideStringField
      FieldName = 'NotaF'
      Origin = 'extratocc2.NotaF'
      Size = 30
    end
    object QrExtratoCredi: TFloatField
      FieldName = 'Credi'
      Origin = 'extratocc2.Credi'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrExtratoDebit: TFloatField
      FieldName = 'Debit'
      Origin = 'extratocc2.Debit'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrExtratoSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'extratocc2.Saldo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrExtratoCartC: TIntegerField
      FieldName = 'CartC'
      Origin = 'extratocc2.CartC'
    end
    object QrExtratoCartN: TWideStringField
      FieldName = 'CartN'
      Origin = 'extratocc2.CartN'
      Size = 100
    end
    object QrExtratoCodig: TIntegerField
      FieldName = 'Codig'
      Origin = 'extratocc2.Codig'
    end
    object QrExtratoCtrle: TIntegerField
      FieldName = 'Ctrle'
      Origin = 'extratocc2.Ctrle'
    end
    object QrExtratoCtSub: TIntegerField
      FieldName = 'CtSub'
      Origin = 'extratocc2.CtSub'
    end
    object QrExtratoID_Pg: TIntegerField
      FieldName = 'ID_Pg'
      Origin = 'extratocc2.ID_Pg'
    end
    object QrExtratoTipoI: TIntegerField
      FieldName = 'TipoI'
      Origin = 'extratocc2.TipoI'
    end
  end
  object DsExtrato: TDataSource
    DataSet = QrExtrato
    Left = 180
    Top = 8
  end
  object frxExtrato: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39428.854424907400000000
    ReportOptions.LastChange = 39428.854424907400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 298
    Top = 14
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = []
      MirrorMode = []
    end
  end
  object frxDsExtrato: TfrxDBDataset
    UserName = 'frxDsExtrato'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataE=DataE'
      'DataV=DataV'
      'DataQ=DataQ'
      'DataX=DataX'
      'Texto=Texto'
      'Docum=Docum'
      'NotaF=NotaF'
      'Credi=Credi'
      'Debit=Debit'
      'Saldo=Saldo'
      'CartC=CartC'
      'CartN=CartN'
      'Codig=Codig'
      'Ctrle=Ctrle'
      'CtSub=CtSub'
      'ID_Pg=ID_Pg'
      'TipoI=TipoI')
    DataSet = QrExtrato
    BCDToCurrency = False
    DataSetOptions = []
    Left = 326
    Top = 14
  end
  object QrPRI: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPRICalcFields
    Left = 208
    Top = 8
    object QrPRIPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrPRIControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPRIConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrPRIValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPRIPrevBaI: TIntegerField
      FieldName = 'PrevBaI'
      Required = True
    end
    object QrPRITexto: TWideStringField
      FieldName = 'Texto'
      Required = True
      Size = 50
    end
    object QrPRIPrevBaC: TIntegerField
      FieldName = 'PrevBaC'
      Required = True
    end
    object QrPRIEmitVal: TFloatField
      FieldName = 'EmitVal'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPRIDIFER: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIFER'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPRIPERIODO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODO_TXT'
      Calculated = True
    end
  end
  object DsPRI: TDataSource
    DataSet = QrPRI
    Left = 236
    Top = 8
  end
  object QrRealiz: TMySQLQuery
    Left = 38
    Top = 194
    object QrRealizValor: TFloatField
      FieldName = 'Valor'
    end
  end
end
