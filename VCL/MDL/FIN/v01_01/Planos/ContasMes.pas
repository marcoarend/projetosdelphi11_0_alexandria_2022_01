unit ContasMes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, Db, mySQLDbTables, dmkEdit,
  dmkDBLookupComboBox, dmkEditCB, dmkLabel, dmkGeral, dmkValUsu, dmkRadioGroup,
  dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  TFmContasMes = class(TForm)
    Panel1: TPanel;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    Label1: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdDescricao: TdmkEdit;
    Edit1: TdmkEdit;
    EdPeriodoIni: TdmkEdit;
    Label5: TLabel;
    Label16: TLabel;
    Edit2: TdmkEdit;
    EdPeriodoFim: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    EdCodigo: TdmkEditCB;
    CBCodigo: TdmkDBLookupComboBox;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    GroupBox1: TGroupBox;
    EdValorMin: TdmkEdit;
    Label7: TLabel;
    EdValorMax: TdmkEdit;
    Label8: TLabel;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNO_UF: TWideStringField;
    QrCliIntCO_SHOW: TLargeintField;
    QrCliIntNO_EMPRESA: TWideStringField;
    QrCliIntCIDADE: TWideStringField;
    VUCliInt: TdmkValUsu;
    GroupBox2: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    EdQtdeMin: TdmkEdit;
    EdQtdeMax: TdmkEdit;
    GroupBox3: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    EdDiaAlert: TdmkEdit;
    EdDiaVence: TdmkEdit;
    RGTipMesRef: TdmkRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPeriodoIniChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPeriodoFimChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    //procedure MudaPeriodo(Key: Word);
  public
    { Public declarations }
    FControle: Integer;
    FQuery: TmySQLQuery;
    procedure ReopenQueryOutroForm(Controle: Integer);
  end;

  var
  FmContasMes: TFmContasMes;

implementation

uses UnMyObjects, UMySQLModule, Module, Contas, DmkDAC_PF;

{$R *.DFM}

procedure TFmContasMes.BtOKClick(Sender: TObject);
var
  Codigo, CliInt, PeriodoIni, PeriodoFim, Controle, QtdeMin, QtdeMax,
  DiaAlert, DiaVence, TipMesRef: Integer;
  Descricao: String;
  ValorMin, ValorMax: Double;
begin
  Codigo     := EdCodigo.ValueVariant;
  CliInt     := QrCliIntCodigo.Value;//EdCliInt.ValueVariant;
  PeriodoIni := EdPeriodoIni.ValueVariant;
  PeriodoFim := EdPeriodoFim.ValueVariant;
  Descricao  := EdDescricao.ValueVariant;
  ValorMin   := EdValorMin.ValueVariant;
  ValorMax   := EdValorMax.ValueVariant;
  QtdeMin    := EdQtdeMin.ValueVariant;
  QtdeMax    := EdQtdeMax.ValueVariant;
  DiaAlert   := EdDiaAlert.ValueVariant;
  DiaVence   := EdDiaVence.ValueVariant;
  TipMesRef  := RGTipMesRef.ItemIndex;
  //
  Controle   := UMyMod.BuscaEmLivreY_Def('contasmes', 'Controle', ImgTipo.SQLType, FControle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'contasmes', False, [
  'Codigo', 'CliInt', 'Descricao',
  'PeriodoIni', 'PeriodoFim', 'ValorMin',
  'ValorMax', 'QtdeMin', 'QtdeMax',
  'DiaAlert', 'DiaVence', 'TipMesRef'], [
  'Controle'], [
  Codigo, CliInt, Descricao,
  PeriodoIni, PeriodoFim, ValorMin,
  ValorMax, QtdeMin, QtdeMax,
  DiaAlert, DiaVence, TipMesRef], [
  Controle], True) then
  begin
    ReopenQueryOutroForm(Controle);
    Geral.MB_Info(DmkEnums.NomeTipoSQL(ImgTipo.SQLType) +
    ' realizada com sucesso!');
    ImgTipo.SQLType := stIns;
    EdCliInt.SetFocus;
  end;
end;

procedure TFmContasMes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasMes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasMes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FControle := 0;
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
end;

procedure TFmContasMes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasMes.EdDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdDescricao.Text := CBCliInt.Text + ' - ' + CBCodigo.Text;
end;

procedure TFmContasMes.EdPeriodoFimChange(Sender: TObject);
begin
  Edit2.Text := dmkPF.MesEAnoDoPeriodoLongo(EdPeriodoFim.ValueVariant);
end;

procedure TFmContasMes.EdPeriodoIniChange(Sender: TObject);
begin
  Edit1.Text := dmkPF.MesEAnoDoPeriodoLongo(EdPeriodoIni.ValueVariant);
end;

procedure TFmContasMes.ReopenQueryOutroForm(Controle: Integer);
begin
  if FQuery = nil then Exit;
  FQuery.Close;
  UnDmkDAC_PF.AbreQueryApenas(FQuery);
  FQuery.Locate('Controle', Controle, []);
end;

end.
