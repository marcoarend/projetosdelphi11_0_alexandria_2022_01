object FmPlaCtas: TFmPlaCtas
  Left = 234
  Top = 170
  Caption = 'FIN-PLCTA-006 :: Plano de Contas'
  ClientHeight = 558
  ClientWidth = 1000
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 442
    Width = 1000
    Height = 46
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 27
      Width = 87
      Height = 13
      Caption = '*S: Controla saldo.'
    end
    object Label2: TLabel
      Left = 106
      Top = 27
      Width = 56
      Height = 13
      Caption = '*M: Mensal.'
    end
    object Label3: TLabel
      Left = 178
      Top = 27
      Width = 52
      Height = 13
      Caption = '*D: D'#233'bito.'
    end
    object Label4: TLabel
      Left = 236
      Top = 27
      Width = 53
      Height = 13
      Caption = '*C: Cr'#233'dito.'
    end
    object Label5: TLabel
      Left = 297
      Top = 27
      Width = 44
      Height = 13
      Caption = '*A: Ativo.'
    end
    object CkOcultaCadSistema: TCheckBox
      Left = 8
      Top = 8
      Width = 158
      Height = 17
      Caption = 'Ocultar cadastros do sistema.'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = CkOcultaCadSistemaClick
    end
    object CkDragDrop: TCheckBox
      Left = 170
      Top = 8
      Width = 228
      Height = 17
      Caption = 'Ativar arraste entre n'#237'veis (mudar n'#237'vel pai).'
      TabOrder = 1
      OnClick = CkDragDropClick
    end
    object PnSaiDesis: TPanel
      Left = 858
      Top = 0
      Width = 142
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 118
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object GBAvisos1: TGroupBox
      Left = 415
      Top = 0
      Width = 443
      Height = 46
      Align = alRight
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 439
        Height = 29
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 52
    Width = 1000
    Height = 390
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 42
    ExplicitHeight = 400
    object Splitter1: TSplitter
      Left = 0
      Top = 154
      Width = 1000
      Height = 9
      Cursor = crVSplit
      Align = alTop
      Beveled = True
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 1000
      Height = 154
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter2: TSplitter
        Left = 246
        Top = 0
        Width = 10
        Height = 154
        Beveled = True
      end
      object Splitter3: TSplitter
        Left = 502
        Top = 0
        Width = 10
        Height = 154
        Beveled = True
      end
      object Splitter4: TSplitter
        Left = 758
        Top = 0
        Width = 10
        Height = 154
        Beveled = True
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 246
        Height = 154
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object DBGPlano: TDBGrid
          Left = 0
          Top = 17
          Width = 246
          Height = 137
          Align = alClient
          DataSource = DsPlano
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGPlanoCellClick
          OnColEnter = DBGContaColEnter
          OnColExit = DBGContaColExit
          OnDrawColumnCell = DBGPlanoDrawColumnCell
          OnDblClick = DBGPlanoDblClick
          OnDragDrop = DBGPlanoDragDrop
          OnDragOver = DBGPlanoDragOver
          OnEnter = DBGPlanoEnter
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Cod.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Descri'#231#227'o do PLANO'
              Width = 96
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome2'
              Title.Caption = 'Descri'#231#227'o substituta'
              Width = 96
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrdemLista'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'Ordem'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_S'
              Title.Caption = 'S*'
              Width = 14
              Visible = True
            end>
        end
        object StaticText2: TStaticText
          Left = 0
          Top = 0
          Width = 246
          Height = 17
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'Plano'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel7: TPanel
        Left = 256
        Top = 0
        Width = 246
        Height = 154
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object DBGConju: TDBGrid
          Left = 0
          Top = 17
          Width = 246
          Height = 137
          Align = alClient
          DataSource = DsConju
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGConjuCellClick
          OnColEnter = DBGContaColEnter
          OnColExit = DBGContaColExit
          OnDrawColumnCell = DBGConjuDrawColumnCell
          OnDblClick = DBGConjuDblClick
          OnDragDrop = DBGConjuDragDrop
          OnDragOver = DBGConjuDragOver
          OnEnter = DBGConjuEnter
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Cod.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Descri'#231#227'o do CONJUNTO'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome2'
              Title.Caption = 'Descri'#231#227'o substituta'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrdemLista'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'Ordem'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_S'
              Title.Caption = 'S*'
              Width = 14
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_PAI'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGray
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Plano'
              Width = 112
              Visible = True
            end>
        end
        object StaticText3: TStaticText
          Left = 0
          Top = 0
          Width = 246
          Height = 17
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'Conjunto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel8: TPanel
        Left = 512
        Top = 0
        Width = 246
        Height = 154
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        object DBGGrupo: TDBGrid
          Left = 0
          Top = 17
          Width = 246
          Height = 137
          Align = alClient
          DataSource = DsGrupo
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGGrupoCellClick
          OnColEnter = DBGContaColEnter
          OnColExit = DBGContaColExit
          OnDrawColumnCell = DBGGrupoDrawColumnCell
          OnDblClick = DBGGrupoDblClick
          OnDragDrop = DBGGrupoDragDrop
          OnDragOver = DBGGrupoDragOver
          OnEnter = DBGGrupoEnter
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Cod.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Descri'#231#227'o do GRUPO'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome2'
              Title.Caption = 'Descri'#231#227'o substituta'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrdemLista'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'Ordem'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_S'
              Title.Caption = 'S*'
              Width = 14
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_PAI'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGray
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Conjunto'
              Width = 112
              Visible = True
            end>
        end
        object StaticText4: TStaticText
          Left = 0
          Top = 0
          Width = 246
          Height = 17
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'Grupo'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel9: TPanel
        Left = 768
        Top = 0
        Width = 232
        Height = 154
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 3
        object DBGSubgr: TDBGrid
          Left = 0
          Top = 17
          Width = 232
          Height = 137
          Align = alClient
          DataSource = DsSubgr
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGSubgrCellClick
          OnColEnter = DBGContaColEnter
          OnColExit = DBGContaColExit
          OnDrawColumnCell = DBGSubgrDrawColumnCell
          OnDblClick = DBGSubgrDblClick
          OnDragDrop = DBGSubgrDragDrop
          OnDragOver = DBGSubgrDragOver
          OnEnter = DBGSubgrEnter
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Cod.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Descri'#231#227'o do SUBGRUPO'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome2'
              Title.Caption = 'Descri'#231#227'o substituta'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrdemLista'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'Ordem'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = '_S'
              Title.Caption = 'S*'
              Width = 14
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_PAI'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGray
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = 'Grupo'
              Width = 112
              Visible = True
            end>
        end
        object StaticText5: TStaticText
          Left = 0
          Top = 0
          Width = 232
          Height = 17
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'Subgrupo'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
    end
    object PnExtra: TPanel
      Left = 737
      Top = 163
      Width = 263
      Height = 227
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitHeight = 237
      object DBGExtra: TDBGrid
        Left = 0
        Top = 17
        Width = 263
        Height = 210
        Align = alClient
        DataSource = DsExtra
        Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGExtraCellClick
        OnColEnter = DBGContaColEnter
        OnColExit = DBGContaColExit
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Cod.'
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o CONJUNTO'
            Width = 185
            Visible = True
          end>
      end
      object StaticText1: TStaticText
        Left = 0
        Top = 0
        Width = 263
        Height = 17
        Align = alTop
        Alignment = taCenter
        Caption = 'Itens n'#227'o pertencentes '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
    end
    object Panel10: TPanel
      Left = 0
      Top = 163
      Width = 737
      Height = 227
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      ExplicitHeight = 237
      object DBGConta: TDBGrid
        Left = 0
        Top = 17
        Width = 737
        Height = 210
        Align = alClient
        DataSource = DsConta
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGContaCellClick
        OnColEnter = DBGContaColEnter
        OnColExit = DBGContaColExit
        OnDrawColumnCell = DBGContaDrawColumnCell
        OnDblClick = DBGContaDblClick
        OnEnter = DBGContaEnter
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Cod.'
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Caption = 'Descri'#231#227'o da CONTA'
            Width = 224
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OrdemLista'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_PAI'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Caption = 'Subgrupo'
            Width = 112
            Visible = True
          end
          item
            Expanded = False
            FieldName = '_S'
            ReadOnly = True
            Title.Caption = 'S*'
            Width = 14
            Visible = True
          end
          item
            Expanded = False
            FieldName = '_M'
            ReadOnly = True
            Title.Caption = 'M*'
            Width = 14
            Visible = True
          end
          item
            Expanded = False
            FieldName = '_C'
            ReadOnly = True
            Title.Caption = 'C*'
            Width = 14
            Visible = True
          end
          item
            Expanded = False
            FieldName = '_D'
            ReadOnly = True
            Title.Caption = 'D*'
            Width = 14
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ativo'
            Title.Caption = 'A*'
            Width = 14
            Visible = True
          end>
      end
      object StaticText6: TStaticText
        Left = 0
        Top = 0
        Width = 737
        Height = 17
        Align = alTop
        Alignment = taCenter
        BorderStyle = sbsSunken
        Caption = 'Conta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1000
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 948
      Top = 0
      Width = 52
      Height = 52
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 958
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 297
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 3
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbPlano: TBitBtn
        Tag = 350
        Left = 45
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbPlanoClick
      end
      object SbConjuntos: TBitBtn
        Tag = 351
        Left = 86
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbConjuntosClick
      end
      object SbGrupos: TBitBtn
        Tag = 352
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbGruposClick
      end
      object SbSubgrupos: TBitBtn
        Tag = 353
        Left = 170
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbSubgruposClick
      end
      object SbContas: TBitBtn
        Tag = 354
        Left = 211
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 5
        OnClick = SbContasClick
      end
      object BtImpExp: TBitBtn
        Tag = 270
        Left = 252
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtImpExpClick
      end
    end
    object GB_M: TGroupBox
      Left = 297
      Top = 0
      Width = 651
      Height = 52
      Align = alClient
      TabOrder = 2
      ExplicitLeft = 336
      ExplicitWidth = 612
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 206
        Height = 32
        Caption = 'Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 206
        Height = 32
        Caption = 'Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 206
        Height = 32
        Caption = 'Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 488
    Width = 1000
    Height = 70
    Align = alBottom
    TabOrder = 3
    Visible = False
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 996
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 7
        Top = 4
        Width = 118
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
      end
      object BtCancela: TBitBtn
        Tag = 15
        Left = 195
        Top = 4
        Width = 118
        Height = 40
        Caption = '&Cancela'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
    end
  end
  object DsPlano: TDataSource
    DataSet = TbPlano
    Left = 45
    Top = 89
  end
  object DsConju: TDataSource
    DataSet = TbConju
    Left = 313
    Top = 93
  end
  object DsGrupo: TDataSource
    DataSet = TbGrupo
    Left = 541
    Top = 89
  end
  object DsSubgr: TDataSource
    DataSet = TbSubgr
    Left = 821
    Top = 93
  end
  object DsConta: TDataSource
    DataSet = TbConta
    Left = 88
    Top = 256
  end
  object TbPlano: TMySQLTable
    Filter = 'Codigo>0'
    Filtered = True
    BeforeInsert = TbPlanoBeforeInsert
    BeforePost = TbPlanoBeforePost
    AfterPost = TbPlanoAfterPost
    AfterCancel = TbPlanoAfterCancel
    BeforeDelete = TbPlanoBeforeDelete
    AfterScroll = TbPlanoAfterScroll
    OnCalcFields = TbPlanoCalcFields
    TableName = 'plano'
    Left = 17
    Top = 89
    object TbPlanoCodigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TbPlanoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object TbPlanoLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbPlanoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbPlanoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbPlanoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbPlanoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbPlanoOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object TbPlanoCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
    object TbPlano_S: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_S'
      Calculated = True
    end
    object TbPlanoNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 60
    end
  end
  object TbConju: TMySQLTable
    Filter = 'Plano=-1000'
    Filtered = True
    BeforeInsert = TbConjuBeforeInsert
    AfterInsert = TbConjuAfterInsert
    AfterEdit = TbConjuAfterEdit
    BeforePost = TbConjuBeforePost
    AfterPost = TbConjuAfterPost
    AfterCancel = TbConjuAfterCancel
    BeforeDelete = TbConjuBeforeDelete
    AfterScroll = TbConjuAfterScroll
    OnCalcFields = TbConjuCalcFields
    TableName = 'conjuntos'
    Left = 285
    Top = 93
    object TbConjuCodigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TbConjuNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object TbConjuLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbConjuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbConjuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbConjuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbConjuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbConjuPlano: TIntegerField
      FieldName = 'Plano'
    end
    object TbConjuOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object TbConjuCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
    object TbConjuNOME_PAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PAI'
      LookupDataSet = QrTPla
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Plano'
      Size = 50
      Lookup = True
    end
    object TbConju_S: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_S'
      Calculated = True
    end
    object TbConjuNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 60
    end
  end
  object TbGrupo: TMySQLTable
    Filter = 'Conjunto=-1000'
    Filtered = True
    BeforeInsert = TbGrupoBeforeInsert
    BeforePost = TbGrupoBeforePost
    AfterPost = TbGrupoAfterPost
    AfterCancel = TbGrupoAfterCancel
    BeforeDelete = TbGrupoBeforeDelete
    AfterScroll = TbGrupoAfterScroll
    OnCalcFields = TbGrupoCalcFields
    TableName = 'grupos'
    Left = 513
    Top = 89
    object TbGrupoNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 60
    end
    object TbGrupoCodigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TbGrupoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object TbGrupoConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object TbGrupoLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbGrupoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbGrupoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbGrupoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbGrupoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbGrupoOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object TbGrupoCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
    object TbGrupoNOME_PAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PAI'
      LookupDataSet = QrTCjn
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Conjunto'
      Size = 50
      Lookup = True
    end
    object TbGrupo_S: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_S'
      Calculated = True
    end
  end
  object TbSubgr: TMySQLTable
    Filter = 'Grupo=-1000'
    Filtered = True
    BeforeInsert = TbSubgrBeforeInsert
    BeforePost = TbSubgrBeforePost
    AfterPost = TbSubgrAfterPost
    AfterCancel = TbSubgrAfterCancel
    BeforeDelete = TbSubgrBeforeDelete
    AfterScroll = TbSubgrAfterScroll
    OnCalcFields = TbSubgrCalcFields
    TableName = 'subgrupos'
    Left = 793
    Top = 93
    object TbSubgrNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 60
    end
    object TbSubgrCodigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TbSubgrNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object TbSubgrGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object TbSubgrOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object TbSubgrTipoAgrupa: TIntegerField
      FieldName = 'TipoAgrupa'
    end
    object TbSubgrLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbSubgrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbSubgrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbSubgrUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbSubgrUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbSubgrCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
    object TbSubgrNOME_PAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PAI'
      LookupDataSet = QrTGru
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Grupo'
      Size = 50
      Lookup = True
    end
    object TbSubgr_S: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_S'
      Calculated = True
    end
  end
  object TbConta: TMySQLTable
    Filter = 'SubGrupo=-1000'
    Filtered = True
    BeforePost = TbContaBeforePost
    BeforeDelete = TbContaBeforeDelete
    OnCalcFields = TbContaCalcFields
    TableName = 'contas'
    Left = 60
    Top = 256
    object TbContaCodigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TbContaNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object TbContaID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object TbContaSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
    end
    object TbContaCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object TbContaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object TbContaCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object TbContaDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object TbContaMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object TbContaExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object TbContaMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object TbContaMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object TbContaMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object TbContaMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object TbContaMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object TbContaTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object TbContaExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object TbContaRateio: TIntegerField
      FieldName = 'Rateio'
    end
    object TbContaEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object TbContaAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object TbContaLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbContaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbContaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbContaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbContaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbContaPendenMesSeg: TSmallintField
      FieldName = 'PendenMesSeg'
    end
    object TbContaCalculMesSeg: TSmallintField
      FieldName = 'CalculMesSeg'
    end
    object TbContaOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object TbContaContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object TbContaContasSum: TIntegerField
      FieldName = 'ContasSum'
    end
    object TbContaCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
    object TbContaNOME_PAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PAI'
      LookupDataSet = QrTSgr
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Subgrupo'
      Size = 50
      Lookup = True
    end
    object TbContaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object TbConta_S: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_S'
      Calculated = True
    end
    object TbConta_M: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_M'
      Calculated = True
    end
    object TbConta_C: TIntegerField
      FieldKind = fkCalculated
      FieldName = '_C'
      Calculated = True
    end
    object TbConta_D: TIntegerField
      FieldKind = fkCalculated
      FieldName = '_D'
      Calculated = True
    end
  end
  object QrLoc: TMySQLQuery
    Left = 204
    Top = 252
  end
  object QrNCjt: TMySQLQuery
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM conjuntos tb1'
      'LEFT JOIN plano tb2 ON tb2.Codigo=tb1.Plano'
      'WHERE tb1.Codigo>0'
      'AND tb1.Plano NOT IN (0, :P0)'
      'ORDER BY Nome')
    Left = 808
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNCjtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNCjtNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNCjtNOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object DsExtra: TDataSource
    Left = 836
    Top = 292
  end
  object QrNGru: TMySQLQuery
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM grupos tb1'
      'LEFT JOIN conjuntos tb2 ON tb2.Codigo=tb1.Conjunto'
      'WHERE tb1.Codigo>0'
      'AND tb1.Conjunto NOT IN (0, :P0)'
      'ORDER BY Nome')
    Left = 808
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNGruNOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object QrNSgr: TMySQLQuery
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM subgrupos tb1'
      'LEFT JOIN grupos tb2 ON tb2.Codigo=tb1.Grupo'
      'WHERE tb1.Codigo>0'
      'AND tb1.Grupo NOT IN (0, :P0)'
      'ORDER BY Nome')
    Left = 808
    Top = 348
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNSgrNOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object QrNCta: TMySQLQuery
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM contas tb1'
      'LEFT JOIN subgrupos tb2 ON tb2.Codigo=tb1.Subgrupo'
      'WHERE tb1.Codigo>0'
      'AND tb1.Subgrupo NOT IN (0, :P0)'
      'ORDER BY Nome')
    Left = 808
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNCtaNOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object mySQLUpdateSQL1: TMySQLUpdateSQL
    Left = 412
    Top = 260
  end
  object QrTPla: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM plano'
      'ORDER BY Nome'
      '')
    Left = 140
    Top = 92
    object QrTPlaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTPlaNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrTCjn: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM conjuntos'
      'ORDER BY Nome'
      '')
    Left = 168
    Top = 92
    object QrTCjnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTCjnNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrTGru: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM grupos'
      'ORDER BY Nome'
      '')
    Left = 196
    Top = 92
    object QrTGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrTSgr: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM subgrupos'
      'ORDER BY Nome'
      '')
    Left = 224
    Top = 92
    object QrTSgrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTSgrNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 412
    Top = 92
  end
end
