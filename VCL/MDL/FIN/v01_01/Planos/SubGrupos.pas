﻿unit SubGrupos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  Variants, ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, ComCtrls, dmkPermissoes, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkImage, dmkEditCB, UnDmkProcFunc, UnDmkEnums;

type
  TFmSubGrupos = class(TForm)
    PainelDados: TPanel;
    DsSubGrupos: TDataSource;
    QrSubGrupos: TmySQLQuery;
    PainelEdita: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    EdCodigo: TdmkEdit;
    Label5: TLabel;
    EdGrupo: TdmkEditCB;
    CBGrupo: TdmkDBLookupComboBox;
    QrGrupos: TmySQLQuery;
    DsGrupos: TDataSource;
    QrSubGruposCodigo: TIntegerField;
    QrSubGruposGrupo: TIntegerField;
    QrSubGruposNome: TWideStringField;
    QrSubGruposLk: TIntegerField;
    QrSubGruposDataCad: TDateField;
    QrSubGruposDataAlt: TDateField;
    QrSubGruposUserCad: TIntegerField;
    QrSubGruposUserAlt: TIntegerField;
    QrSubGruposNOMEGRUPO: TWideStringField;
    QrGruposCodigo: TIntegerField;
    QrGruposNome: TWideStringField;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    EdOrdemLista: TdmkEdit;
    RGTipoAgrupa: TRadioGroup;
    QrSubGruposOrdemLista: TIntegerField;
    QrSubGruposTipoAgrupa: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrLocate: TmySQLQuery;
    DsLocate: TDataSource;
    QrLocateCodigo: TIntegerField;
    QrLocateNome: TWideStringField;
    QrLocateGrupo: TIntegerField;
    QrLocateLk: TIntegerField;
    QrLocateDataCad: TDateField;
    QrLocateDataAlt: TDateField;
    QrLocateUserCad: TIntegerField;
    QrLocateUserAlt: TIntegerField;
    QrLocateOrdemLista: TIntegerField;
    QrLocateTipoAgrupa: TIntegerField;
    Panel1: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    Label2: TLabel;
    DBEdNome: TDBEdit;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    EdDBOrdem: TDBEdit;
    RadioGroup1: TDBRadioGroup;
    Label3: TLabel;
    DBEdGrupo: TDBEdit;
    Panel13: TPanel;
    Label40: TLabel;
    EdLocate: TdmkEdit;
    DBGrid1: TDBGrid;
    RGOrdem2: TRadioGroup;
    RGOrdem1: TRadioGroup;
    TabSheet3: TTabSheet;
    TbContas: TmySQLTable;
    DBGrid2: TDBGrid;
    TbContasCodigo: TIntegerField;
    TbContasNome: TWideStringField;
    TbContasNome2: TWideStringField;
    TbContasNome3: TWideStringField;
    TbContasOrdemLista: TIntegerField;
    DsContas: TDataSource;
    TbContasID: TWideStringField;
    TbContasSubgrupo: TIntegerField;
    TbContasCentroCusto: TIntegerField;
    TbContasEmpresa: TIntegerField;
    TbContasCredito: TWideStringField;
    TbContasDebito: TWideStringField;
    TbContasMensal: TWideStringField;
    TbContasExclusivo: TWideStringField;
    TbContasMensdia: TSmallintField;
    TbContasMensdeb: TFloatField;
    TbContasMensmind: TFloatField;
    TbContasMenscred: TFloatField;
    TbContasMensminc: TFloatField;
    TbContasTerceiro: TIntegerField;
    TbContasExcel: TWideStringField;
    TbContasRateio: TIntegerField;
    TbContasEntidade: TIntegerField;
    TbContasAntigo: TWideStringField;
    TbContasLk: TIntegerField;
    TbContasDataCad: TDateField;
    TbContasDataAlt: TDateField;
    TbContasUserCad: TIntegerField;
    TbContasUserAlt: TIntegerField;
    TbContasPendenMesSeg: TSmallintField;
    TbContasCalculMesSeg: TSmallintField;
    Panel2: TPanel;
    Label7: TLabel;
    EdDBCodigo: TDBEdit;
    EdDBDescricao: TDBEdit;
    Label8: TLabel;
    Label11: TLabel;
    EdDBGrupo: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    CkCtrlaSdo: TCheckBox;
    QrSubGruposCtrlaSdo: TSmallintField;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel7: TPanel;
    BtDesiste: TBitBtn;
    BtNivelAcima: TBitBtn;
    SBGrupo: TSpeedButton;
    QrSubGruposNome2: TWideStringField;
    EdNome: TdmkEdit;
    EdNome2: TdmkEdit;
    Label12: TLabel;
    DBEdit1: TDBEdit;
    Label13: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSubGruposAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrSubGruposAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSubGruposBeforeOpen(DataSet: TDataSet);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdLocateChange(Sender: TObject);
    procedure EdLocateKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure TbContasDeleting(Sender: TObject; var Allow: Boolean);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure TbContasBeforeInsert(DataSet: TDataSet);
    procedure TbContasBeforeDelete(DataSet: TDataSet);
    procedure BtNivelAcimaClick(Sender: TObject);
    procedure SBGrupoClick(Sender: TObject);
    procedure EdNomeRedefinido(Sender: TObject);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure LocalizaReferencia;
    procedure MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenLocate;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmSubGrupos: TFmSubGrupos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, SubGruposExclui, Grupos, MyDBCheck, DmkDAC_PF,
  UnFinanceiroJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSubGrupos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSubGrupos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSubGruposCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSubGrupos.DefParams;
begin
  VAR_GOTOTABELA := 'SubGrupos';
  VAR_GOTOMYSQLTABLE := QrSubGrupos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT sg.*, gr.Nome NOMEGRUPO');
  VAR_SQLx.Add('FROM subgrupos sg');
  VAR_SQLx.Add('LEFT JOIN grupos gr ON gr.Codigo=sg.Grupo');
  VAR_SQLx.Add('WHERE sg.Codigo > 0');
  //
  VAR_SQL1.Add('AND sg.Codigo=:P0');
  //
  VAR_SQLa.Add('AND sg.Nome Like :P0');
  //
end;

procedure TFmSubGrupos.MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    EdNome.Text := CO_VAZIO;
    EdNome.Visible := True;
    if SQLType = stIns then
    begin
      EdNome.ValueVariant       := EmptyStr;
      EdNome2.ValueVariant      := EmptyStr;
      EdCodigo.ValueVariant     := 0;
      EdGrupo.ValueVariant      := 0;
      CBGrupo.KeyValue          := Null;
      EdOrdemLista.ValueVariant := 0;
      CkCtrlaSdo.Checked        := True;
    end else begin
      EdCodigo.ValueVariant     := QrSubGruposCodigo.Value;
      EdNome.ValueVariant       := QrSubGruposNome.Value;
      EdNome2.ValueVariant      := QrSubGruposNome2.Value;
      EdGrupo.ValueVariant      := QrSubGruposGrupo.Value;
      CBGrupo.KeyValue          := QrSubGruposGrupo.Value;
      EdOrdemLista.ValueVariant := IntToStr(QrSubGruposOrdemLista.Value);
      CkCtrlaSdo.Checked        := Geral.IntToBool_0(QrSubGruposCtrlaSdo.Value);
(*

      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
      EdOrdemLista.Text      := '';
      RGTipoAgrupa.ItemIndex := 2;
      CkCtrlaSdo.Checked := True;
    end else begin
      EdCodigo.Text          := DBEdCodigo.Text;
      EdNome.Text            := DBEdNome.Text;
      EdOrdemLista.Text      := IntToStr(QrSubGruposOrdemLista.Value);
      RGTipoAgrupa.ItemIndex := QrSubGruposTipoAgrupa.Value;
      EdGrupo.Text           := IntToStr(QrSubGruposGrupo.Value);
      CBGrupo.KeyValue       := QrSubGruposGrupo.Value;
      CkCtrlaSdo.Checked     := Geral.IntToBool_0(QrSubGruposCtrlaSdo.Value);
*)
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmSubGrupos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSubGrupos.AlteraRegistro;
var
  SubGrupos : Integer;
begin
  SubGrupos := QrSubGruposCodigo.Value;
  if (QrSubGrupos.State = dsInactive) or (QrSubGrupos.RecordCount = 0) or
    (QrSubGruposCodigo.Value <= 0) then
    Exit;
  //
  if not UMyMod.SelLockY(SubGrupos, Dmod.MyDB, 'SubGrupos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(SubGrupos, Dmod.MyDB, 'SubGrupos', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmSubGrupos.IncluiRegistro;
var
  Cursor : TCursor;
  SubGrupos : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
(*
    SubGrupos := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'SubGrupos', 'SubGrupo', 'Codigo');
    if Length(FormatFloat(FFormatFloat, SubGrupos))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro(
      'Inclusão cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
*)
    MostraEdicao(True, stIns, SubGrupos);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmSubGrupos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSubGrupos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmSubGrupos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSubGrupos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSubGrupos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSubGrupos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSubGrupos.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
  ReopenLocate;
end;

procedure TFmSubGrupos.BtNivelAcimaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGrupos, FmGrupos, afmoNegarComAviso) then
  begin
    FmGrupos.ShowModal;
    FmGrupos.Destroy;
    //
    QrGrupos.Close;
    UnDmkDAC_PF.AbreQuery(QrGrupos, Dmod.MyDB);
    DefParams;
  end;
end;

procedure TFmSubGrupos.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
  ReopenLocate;
end;

procedure TFmSubGrupos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSubGruposCodigo.Value;
  Close;
end;

procedure TFmSubGrupos.BtConfirmaClick(Sender: TObject);
var
  Nome, Nome2: String;
  //_Categoria,
  Codigo, Grupo, OrdemLista, TipoAgrupa, CtrlaSdo(*, NotPrntFin*): Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Nome2          := EdNome2.ValueVariant;
  Grupo          := EdGrupo.ValueVariant;
  OrdemLista     := EdOrdemLista.ValueVariant;
  CtrlaSdo       := Geral.BoolToInt(CkCtrlaSdo.Checked);
  //NotPrntFin     := EdNotPrntFin.ValueVariant;
  TipoAgrupa     := RGTipoAgrupa.ItemIndex;
  //_Categoria     := ;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  if MyObjects.FIC(Grupo < 1, EdGrupo, 'Defina um grupo!') then Exit;
  if MyObjects.FIC(not TipoAgrupa in [0, 1], RGTipoAgrupa, 'Informe o tipo de agrupamento!') then Exit;
  //
  if SQLType = stIns then
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'SubGrupos', 'SubGrupo', 'Codigo');
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'subgrupos', False, [
  'Nome', 'Grupo', 'OrdemLista',
  'TipoAgrupa', 'CtrlaSdo', (*'_Categoria',
  'NotPrntFin',*) 'Nome2'], [
  'Codigo'], [
  Nome, Grupo, OrdemLista,
  TipoAgrupa, CtrlaSdo, (*_Categoria,
  NotPrntFin,*) Nome2], [
  Codigo], True) then
  begin
(*
var
  Grupo, Codigo, TipoAgrupa: Integer;
  Nome : String;
begin
  Nome       := EdNome.Text;
  Grupo      := Geral.IMV(EdGrupo.Text);
  TipoAgrupa := RGTipoAgrupa.ItemIndex;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  if MyObjects.FIC(Grupo < 1, EdGrupo, 'Defina um grupo!') then Exit;
  if MyObjects.FIC(not TipoAgrupa in [0, 1], RGTipoAgrupa, 'Informe o tipo de agrupamento!') then Exit;
  //
  Codigo := Geral.IMV(EdCodigo.Text);
  //
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO subgrupos SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE subgrupos SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Grupo=:P1, OrdemLista=:P2, ');
  Dmod.QrUpdU.SQL.Add('TipoAgrupa=:P3, CtrlaSdo=:P4, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Grupo;
  Dmod.QrUpdU.Params[02].AsInteger := Geral.IMV(EdOrdemLista.Text);
  Dmod.QrUpdU.Params[03].AsInteger := TipoAgrupa;
  Dmod.QrUpdU.Params[04].AsInteger := Geral.BoolToInt(CkCtrlaSdo.Checked);
  //
  Dmod.QrUpdU.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[06].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[07].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'SubGrupos', 'Codigo');
*)
    MostraEdicao(False, stLok, 0);
    LocCod(Codigo,Codigo);
    ReopenLocate;
  end;
end;

procedure TFmSubGrupos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'SubGrupos', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'SubGrupos', 'Codigo');
end;

procedure TFmSubGrupos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreTable(TBContas, Dmod.MyDB);
  PainelEdita.Align  := alClient;
  PainelDados.Align  := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrGrupos, Dmod.MyDB);
  ReopenLocate;
end;

procedure TFmSubGrupos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSubGruposCodigo.Value,LaRegistro.Caption);
end;

procedure TFmSubGrupos.SBGrupoClick(Sender: TObject);
var
  Grupo: Integer;
begin
  Grupo        := EdGrupo.ValueVariant;
  VAR_CADASTRO := 0;
  //
  FinanceiroJan.CadastroDeGrupos(Grupo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdGrupo, CBGrupo, QrGrupos, VAR_CADASTRO);
    //
    EdGrupo.SetFocus;
  end;
end;

procedure TFmSubGrupos.SbImprimeClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

procedure TFmSubGrupos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSubGrupos.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa não disponível para esta janela!');
end;

procedure TFmSubGrupos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSubGrupos.QrSubGruposAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSubGrupos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'SubGrupos', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmSubGrupos.QrSubGruposAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrSubGruposCodigo.Value, False);
  TbContas.Filter := 'SubGrupo='+IntToStr(QrSubGruposCodigo.Value);
  TbContas.Refresh;
end;

procedure TFmSubGrupos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSubGruposCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'SubGrupos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSubGrupos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSubGrupos.QrSubGruposBeforeOpen(DataSet: TDataSet);
begin
  QrSubGruposCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmSubGrupos.DBGrid1DblClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl1Change(Self);
end;

procedure TFmSubGrupos.PageControl1Change(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: LocCod(QrLocateCodigo.Value, QrLocateCodigo.Value);
    1: EdLocate.SetFocus;
  end;
end;

procedure TFmSubGrupos.ReopenLocate;
var
  Ordem: String;
begin
  QrLocate.Close;
  case (RGOrdem1.ItemIndex+RGOrdem2.ItemIndex*10) of
    00: Ordem := 'ORDER BY Nome, TipoAgrupa, OrdemLista';
    01: Ordem := 'ORDER BY Codigo';
    02: Ordem := 'ORDER BY TipoAgrupa, OrdemLista, Nome';
    03: Ordem := 'ORDER BY OrdemLista, Nome';
    //
    10: Ordem := 'ORDER BY Nome DESC, TipoAgrupa, OrdemLista';
    11: Ordem := 'ORDER BY Codigo DESC';
    12: Ordem := 'ORDER BY TipoAgrupa DESC, OrdemLista, Nome';
    13: Ordem := 'ORDER BY OrdemLista DESC, Nome';
  end;
  QrLocate.SQL[2] := Ordem;
  UnDmkDAC_PF.AbreQuery(QrLocate, Dmod.MyDB);
end;

procedure TFmSubGrupos.BtExcluiClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSubGruposExclui, FmSubGruposExclui, afmoNegarComAviso) then
  begin
    FmSubGruposExclui.ShowModal;
    FmSubGruposExclui.Destroy;
  end;
  LocCod(QrSubgruposCodigo.Value, QrSubgruposCodigo.Value);
  ReopenLocate;
end;

procedure TFmSubGrupos.EdLocateChange(Sender: TObject);
begin
  LocalizaReferencia;
end;

procedure TFmSubGrupos.LocalizaReferencia;
begin
  if QrLocate.State = dsBrowse then QrLocate.Locate('Nome', EdLocate.Text,
    [loPartialKey, loCaseInsensitive]);
end;

procedure TFmSubGrupos.EdLocateKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=13 then
  begin
    PageControl1.ActivePageIndex := 0;
    PageControl1Change(Self);
  end;
end;

procedure TFmSubGrupos.EdNomeRedefinido(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    if EdNome2.ValueVariant = EmptyStr then
      EdNome2.ValueVariant := EdNome.ValueVariant;
end;

procedure TFmSubGrupos.RGOrdem1Click(Sender: TObject);
begin
  ReopenLocate;
end;

procedure TFmSubGrupos.RGOrdem2Click(Sender: TObject);
begin
  ReopenLocate;
end;

procedure TFmSubGrupos.TbContasBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmSubGrupos.TbContasBeforeInsert(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmSubGrupos.TbContasDeleting(Sender: TObject;
  var Allow: Boolean);
begin
  Allow := False;
  Geral.MB_Aviso('Para excluir a conta acesse o cadastro de contas');
end;

end.

