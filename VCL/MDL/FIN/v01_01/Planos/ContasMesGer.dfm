object FmContasMesGer: TFmContasMesGer
  Left = 339
  Top = 185
  Caption = 
    'FIN-PLCTA-029 :: Contas Mensais - Cadastro de Confer'#234'ncia de Emi' +
    'ss'#245'es'
  ClientHeight = 656
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 494
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 494
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnNomeCond: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Nome do Cliente Interno'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 4227327
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object GradeContasMes: TdmkDBGrid
        Left = 0
        Top = 41
        Width = 1008
        Height = 453
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Conta'
            Width = 34
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Conta'
            Title.Caption = 'Descri'#231#227'o da conta'
            Width = 259
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descricao do cadastro de emiss'#227'o'
            Width = 345
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DiaAlert'
            Title.Caption = 'dd Alerta'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DiaVence'
            Title.Caption = 'dd venc.'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERIODOINI_TXT'
            Title.Caption = 'In'#237'cio'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERIODOFIM_TXT'
            Title.Caption = 'Final'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorMin'
            Title.Caption = 'Valor m'#237'n.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorMax'
            Title.Caption = 'Valor m'#225'x.'
            Visible = True
          end>
        Color = clWindow
        DataSource = DsContasMes
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Conta'
            Width = 34
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Conta'
            Title.Caption = 'Descri'#231#227'o da conta'
            Width = 259
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descricao do cadastro de emiss'#227'o'
            Width = 345
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DiaAlert'
            Title.Caption = 'dd Alerta'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DiaVence'
            Title.Caption = 'dd venc.'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERIODOINI_TXT'
            Title.Caption = 'In'#237'cio'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERIODOFIM_TXT'
            Title.Caption = 'Final'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorMin'
            Title.Caption = 'Valor m'#237'n.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorMax'
            Title.Caption = 'Valor m'#225'x.'
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 689
        Height = 32
        Caption = 'Contas Mensais - Cadastro de Confer'#234'ncia de Emiss'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 689
        Height = 32
        Caption = 'Contas Mensais - Cadastro de Confer'#234'ncia de Emiss'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 689
        Height = 32
        Caption = 'Contas Mensais - Cadastro de Confer'#234'ncia de Emiss'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 542
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 586
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Inclui'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Altera'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 268
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Exclui'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtExcluiClick
      end
    end
  end
  object QrContasMes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(en.Codigo<-10,en.Filial,en.CliInt) CO_SHOW,'
      'co.Nome NO_Conta, cm.* '
      'FROM contasmes cm'
      'LEFT JOIN contas co ON co.Codigo=cm.Codigo'
      'LEFT JOIN entidades en ON en.Codigo=cm.CliInt'
      'WHERE cm.CliInt=:P0')
    Left = 32
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasMesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasMesControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContasMesCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrContasMesDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrContasMesPeriodoIni: TIntegerField
      FieldName = 'PeriodoIni'
    end
    object QrContasMesPeriodoFim: TIntegerField
      FieldName = 'PeriodoFim'
    end
    object QrContasMesValorMin: TFloatField
      FieldName = 'ValorMin'
    end
    object QrContasMesValorMax: TFloatField
      FieldName = 'ValorMax'
    end
    object QrContasMesPERIODOINI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODOINI_TXT'
      Size = 10
      Calculated = True
    end
    object QrContasMesPERIODOFIM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODOFIM_TXT'
      Size = 10
      Calculated = True
    end
    object QrContasMesQtdeMin: TIntegerField
      FieldName = 'QtdeMin'
    end
    object QrContasMesQtdeMax: TIntegerField
      FieldName = 'QtdeMax'
    end
    object QrContasMesDiaAlert: TSmallintField
      FieldName = 'DiaAlert'
      DisplayFormat = '0;-0; '
    end
    object QrContasMesDiaVence: TSmallintField
      FieldName = 'DiaVence'
      DisplayFormat = '0;-0; '
    end
    object QrContasMesNO_Conta: TWideStringField
      FieldName = 'NO_Conta'
      Size = 50
    end
    object QrContasMesTipMesRef: TSmallintField
      FieldName = 'TipMesRef'
    end
    object QrContasMesCO_SHOW: TFloatField
      FieldName = 'CO_SHOW'
    end
  end
  object DsContasMes: TDataSource
    DataSet = QrContasMes
    Left = 32
    Top = 184
  end
end
