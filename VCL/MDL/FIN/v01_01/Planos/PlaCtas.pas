unit PlaCtas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, Db, mySQLDbTables,
  dmkPermissoes, dmkGeral, dmkImage, UnDMkEnums, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmPlaCtas = class(TForm)
    DsPlano: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    Splitter1: TSplitter;
    DsConju: TDataSource;
    DsGrupo: TDataSource;
    DsSubgr: TDataSource;
    DsConta: TDataSource;
    Panel3: TPanel;
    TbPlano: TmySQLTable;
    TbPlanoCodigo: TIntegerField;
    TbPlanoNome: TWideStringField;
    TbPlanoLk: TIntegerField;
    TbPlanoDataCad: TDateField;
    TbPlanoDataAlt: TDateField;
    TbPlanoUserCad: TIntegerField;
    TbPlanoUserAlt: TIntegerField;
    TbConju: TmySQLTable;
    TbConjuCodigo: TIntegerField;
    TbConjuNome: TWideStringField;
    TbConjuLk: TIntegerField;
    TbConjuDataCad: TDateField;
    TbConjuDataAlt: TDateField;
    TbConjuUserCad: TIntegerField;
    TbConjuUserAlt: TIntegerField;
    TbConjuPlano: TIntegerField;
    TbGrupo: TmySQLTable;
    TbSubgr: TmySQLTable;
    TbConta: TmySQLTable;
    TbGrupoCodigo: TIntegerField;
    TbGrupoNome: TWideStringField;
    TbGrupoConjunto: TIntegerField;
    TbGrupoLk: TIntegerField;
    TbGrupoDataCad: TDateField;
    TbGrupoDataAlt: TDateField;
    TbGrupoUserCad: TIntegerField;
    TbGrupoUserAlt: TIntegerField;
    TbContaCodigo: TIntegerField;
    TbContaNome: TWideStringField;
    TbContaID: TWideStringField;
    TbContaSubgrupo: TIntegerField;
    TbContaCentroCusto: TIntegerField;
    TbContaEmpresa: TIntegerField;
    TbContaCredito: TWideStringField;
    TbContaDebito: TWideStringField;
    TbContaMensal: TWideStringField;
    TbContaExclusivo: TWideStringField;
    TbContaMensdia: TSmallintField;
    TbContaMensdeb: TFloatField;
    TbContaMensmind: TFloatField;
    TbContaMenscred: TFloatField;
    TbContaMensminc: TFloatField;
    TbContaTerceiro: TIntegerField;
    TbContaExcel: TWideStringField;
    TbContaRateio: TIntegerField;
    TbContaEntidade: TIntegerField;
    TbContaAntigo: TWideStringField;
    TbContaLk: TIntegerField;
    TbContaDataCad: TDateField;
    TbContaDataAlt: TDateField;
    TbContaUserCad: TIntegerField;
    TbContaUserAlt: TIntegerField;
    TbSubgrCodigo: TIntegerField;
    TbSubgrNome: TWideStringField;
    TbSubgrGrupo: TIntegerField;
    TbSubgrOrdemLista: TIntegerField;
    TbSubgrTipoAgrupa: TIntegerField;
    TbSubgrLk: TIntegerField;
    TbSubgrDataCad: TDateField;
    TbSubgrDataAlt: TDateField;
    TbSubgrUserCad: TIntegerField;
    TbSubgrUserAlt: TIntegerField;
    TbPlanoOrdemLista: TIntegerField;
    TbPlanoCtrlaSdo: TSmallintField;
    TbConjuOrdemLista: TIntegerField;
    TbConjuCtrlaSdo: TSmallintField;
    TbContaPendenMesSeg: TSmallintField;
    TbContaCalculMesSeg: TSmallintField;
    TbContaOrdemLista: TIntegerField;
    TbContaContasAgr: TIntegerField;
    TbContaContasSum: TIntegerField;
    TbContaCtrlaSdo: TSmallintField;
    TbGrupoOrdemLista: TIntegerField;
    TbGrupoCtrlaSdo: TSmallintField;
    TbSubgrCtrlaSdo: TSmallintField;
    CkOcultaCadSistema: TCheckBox;
    QrLoc: TmySQLQuery;
    QrNCjt: TmySQLQuery;
    DsExtra: TDataSource;
    QrNCjtCodigo: TIntegerField;
    QrNCjtNome: TWideStringField;
    TbConjuNOME_PAI: TWideStringField;
    TbGrupoNOME_PAI: TWideStringField;
    TbSubgrNOME_PAI: TWideStringField;
    TbContaNOME_PAI: TWideStringField;
    QrNGru: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    QrNSgr: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    QrNCta: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    QrNCjtNOME_PAI: TWideStringField;
    QrNGruNOME_PAI: TWideStringField;
    QrNSgrNOME_PAI: TWideStringField;
    QrNCtaNOME_PAI: TWideStringField;
    PnExtra: TPanel;
    DBGExtra: TDBGrid;
    StaticText1: TStaticText;
    CkDragDrop: TCheckBox;
    mySQLUpdateSQL1: TmySQLUpdateSQL;
    QrTPla: TmySQLQuery;
    QrTCjn: TmySQLQuery;
    QrTGru: TmySQLQuery;
    QrTSgr: TmySQLQuery;
    QrTPlaCodigo: TIntegerField;
    QrTPlaNome: TWideStringField;
    QrTCjnCodigo: TIntegerField;
    QrTCjnNome: TWideStringField;
    QrTGruCodigo: TIntegerField;
    QrTGruNome: TWideStringField;
    QrTSgrCodigo: TIntegerField;
    QrTSgrNome: TWideStringField;
    TbConta_S: TSmallintField;
    TbConta_M: TSmallintField;
    Label1: TLabel;
    Label2: TLabel;
    TbPlano_S: TSmallintField;
    TbConju_S: TSmallintField;
    TbSubgr_S: TSmallintField;
    TbGrupo_S: TSmallintField;
    TbContaAtivo: TSmallintField;
    TbConta_C: TIntegerField;
    TbConta_D: TIntegerField;
    Label3: TLabel;
    Label4: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    BtOK: TBitBtn;
    BtCancela: TBitBtn;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    SbPlano: TBitBtn;
    SbConjuntos: TBitBtn;
    SbGrupos: TBitBtn;
    SbSubgrupos: TBitBtn;
    SbContas: TBitBtn;
    Panel6: TPanel;
    DBGPlano: TDBGrid;
    StaticText2: TStaticText;
    Panel7: TPanel;
    DBGConju: TDBGrid;
    StaticText3: TStaticText;
    Panel8: TPanel;
    DBGGrupo: TDBGrid;
    StaticText4: TStaticText;
    Panel9: TPanel;
    DBGSubgr: TDBGrid;
    StaticText5: TStaticText;
    Panel10: TPanel;
    DBGConta: TDBGrid;
    StaticText6: TStaticText;
    BtImpExp: TBitBtn;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Label5: TLabel;
    TbConjuNome2: TWideStringField;
    TbGrupoNome2: TWideStringField;
    TbSubgrNome2: TWideStringField;
    TbPlanoNome2: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrPlanoAfterScroll(DataSet: TDataSet);
    procedure QrConjuAfterScroll(DataSet: TDataSet);
    procedure QrGrupoAfterScroll(DataSet: TDataSet);
    procedure QrSubGrAfterScroll(DataSet: TDataSet);
    procedure TbPlanoAfterScroll(DataSet: TDataSet);
    procedure TbPlanoBeforePost(DataSet: TDataSet);
    procedure TbConjuBeforePost(DataSet: TDataSet);
    procedure TbGrupoBeforePost(DataSet: TDataSet);
    procedure TbSubgrBeforePost(DataSet: TDataSet);
    procedure TbContaBeforePost(DataSet: TDataSet);
    procedure TbConjuAfterScroll(DataSet: TDataSet);
    procedure TbGrupoAfterScroll(DataSet: TDataSet);
    procedure TbSubgrAfterScroll(DataSet: TDataSet);
    procedure CkOcultaCadSistemaClick(Sender: TObject);
    procedure TbConjuAfterEdit(DataSet: TDataSet);
    procedure TbConjuAfterInsert(DataSet: TDataSet);
    procedure DBGExtraCellClick(Column: TColumn);
    procedure DBGPlanoDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DBGPlanoDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DBGConjuCellClick(Column: TColumn);
    procedure DBGGrupoCellClick(Column: TColumn);
    procedure DBGConjuDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DBGConjuDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DBGSubgrCellClick(Column: TColumn);
    procedure DBGGrupoDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DBGGrupoDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DBGContaCellClick(Column: TColumn);
    procedure DBGSubgrDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DBGSubgrDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure TbPlanoBeforeInsert(DataSet: TDataSet);
    procedure TbPlanoAfterPost(DataSet: TDataSet);
    procedure TbPlanoAfterCancel(DataSet: TDataSet);
    procedure TbConjuBeforeInsert(DataSet: TDataSet);
    procedure TbConjuAfterPost(DataSet: TDataSet);
    procedure TbConjuAfterCancel(DataSet: TDataSet);
    procedure TbGrupoBeforeInsert(DataSet: TDataSet);
    procedure TbGrupoAfterPost(DataSet: TDataSet);
    procedure TbGrupoAfterCancel(DataSet: TDataSet);
    procedure TbSubgrBeforeInsert(DataSet: TDataSet);
    procedure TbSubgrAfterPost(DataSet: TDataSet);
    procedure TbSubgrAfterCancel(DataSet: TDataSet);
    procedure DBGConjuEnter(Sender: TObject);
    procedure DBGPlanoEnter(Sender: TObject);
    procedure DBGGrupoEnter(Sender: TObject);
    procedure DBGSubgrEnter(Sender: TObject);
    procedure DBGContaEnter(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure CkDragDropClick(Sender: TObject);
    procedure TbPlanoBeforeDelete(DataSet: TDataSet);
    procedure TbConjuBeforeDelete(DataSet: TDataSet);
    procedure TbGrupoBeforeDelete(DataSet: TDataSet);
    procedure TbSubgrBeforeDelete(DataSet: TDataSet);
    procedure TbContaBeforeDelete(DataSet: TDataSet);
    procedure TbContaCalcFields(DataSet: TDataSet);
    procedure DBGContaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGContaColEnter(Sender: TObject);
    procedure DBGContaColExit(Sender: TObject);
    procedure TbPlanoCalcFields(DataSet: TDataSet);
    procedure TbConjuCalcFields(DataSet: TDataSet);
    procedure TbGrupoCalcFields(DataSet: TDataSet);
    procedure TbSubgrCalcFields(DataSet: TDataSet);
    procedure DBGPlanoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGConjuDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGGrupoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGSubgrDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGPlanoCellClick(Column: TColumn);
    procedure SbPlanoClick(Sender: TObject);
    procedure SbConjuntosClick(Sender: TObject);
    procedure SbGruposClick(Sender: TObject);
    procedure SbSubgruposClick(Sender: TObject);
    procedure SbContasClick(Sender: TObject);
    procedure BtImpExpClick(Sender: TObject);
    procedure DBGPlanoDblClick(Sender: TObject);
    procedure DBGConjuDblClick(Sender: TObject);
    procedure DBGGrupoDblClick(Sender: TObject);
    procedure DBGSubgrDblClick(Sender: TObject);
    procedure DBGContaDblClick(Sender: TObject);
  private
    { Private declarations }
    FInGrade: Boolean;
    FMySrc: TDBGrid;
    procedure RenomeiaTituloExtra(TitSon, TitPai: String);
    function AceitaDragOver(Source: TObject; Grade: String;
             Tabela: TmySQLQuery): Boolean;
    procedure VeSeAceitaDragDrop(Sender, Source: TObject; X, Y: Integer;
              TitPai, TitSon, Tabela, Campo: String);
    procedure MostraPlanos();
    procedure MostraConjuntos();
    procedure MostraGrupos();
    procedure MostraSubgrupos();
    procedure MostraContas();
  public
    { Public declarations }
  end;

  var
  FmPlaCtas: TFmPlaCtas;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, Principal, MyVCLSkin,
  MyDBCheck, UnFinanceiroJan, DmkDAC_PF;

procedure TFmPlaCtas.BtImpExpClick(Sender: TObject);
begin
  FinanceiroJan.MostraPlanoImpExp;
  //
  TbPlano.Close;
  UnDmkDAC_PF.AbreTable(TbPlano, Dmod.MyDB);
  //
  TbConju.Close;
  UnDmkDAC_PF.AbreTable(TbConju, Dmod.MyDB);
  //
  TbGrupo.Close;
  UnDmkDAC_PF.AbreTable(TbGrupo, Dmod.MyDB);
  //
  TbSubgr.Close;
  UnDmkDAC_PF.AbreTable(TbSubgr, Dmod.MyDB);
  //
  TbConta.Close;
  UnDmkDAC_PF.AbreTable(TbConta, Dmod.MyDB);
end;

procedure TFmPlaCtas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPlaCtas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPlaCtas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPlaCtas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FMySrc := nil;
  PnExtra.Visible := False;
  //ReopenPlano(0);
  UnDmkDAC_PF.AbreQuery(QrTPla, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTCjn, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTGru, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTSgr, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreTable(TbPlano, Dmod.MyDB);
  UnDmkDAC_PF.AbreTable(TbConju, Dmod.MyDB);
  UnDmkDAC_PF.AbreTable(TbGrupo, Dmod.MyDB);
  UnDmkDAC_PF.AbreTable(TbSubGr, Dmod.MyDB);
  UnDmkDAC_PF.AbreTable(TbConta, Dmod.MyDB);
end;

(*procedure TFmPlano.ReopenPlano(Codigo: Integer);
begin
  QrPlano.Close;
  //QrPlano.Params[0].AsInteger := Qr?.Value;
  UnDmkDAC_PF.AbreQuery(QrPlano, Dmod.MyDB);
  //
  if Codigo <> 0 then QrPlano.Locate('Codigo', Codigo, []);
end;

procedure TFmPlano.ReopenConju(Codigo: Integer);
begin
  QrConju.Close;
  QrConju.Params[0].AsInteger := QrPlanoCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrConju, Dmod.MyDB);
  //
  if Codigo <> 0 then QrConju.Locate('Codigo', Codigo, []);
end;

procedure TFmPlano.ReopenGrupo(Codigo: Integer);
begin
  QrGrupo.Close;
  QrGrupo.Params[0].AsInteger := QrConjuCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGrupo, Dmod.MyDB);
  //
  if Codigo <> 0 then QrGrupo.Locate('Codigo', Codigo, []);
end;

procedure TFmPlano.ReopenSubGr(Codigo: Integer);
begin
  QrSubGr.Close;
  QrSubGr.Params[0].AsInteger := QrGrupoCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrSubGr, Dmod.MyDB);
  //
  if Codigo <> 0 then QrSubGr.Locate('Codigo', Codigo, []);
end;

procedure TFmPlano.ReopenConta(Codigo: Integer);
begin
  QrConta.Close;
  QrConta.Params[0].AsInteger := QrSubgrCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrConta, Dmod.MyDB);
  //
  if Codigo <> 0 then QrConta.Locate('Codigo', Codigo, []);
end;*)

procedure TFmPlaCtas.QrPlanoAfterScroll(DataSet: TDataSet);
begin
  //ReopenConju(0);
end;

procedure TFmPlaCtas.QrConjuAfterScroll(DataSet: TDataSet);
begin
  //ReopenGrupo(0);
end;

procedure TFmPlaCtas.QrGrupoAfterScroll(DataSet: TDataSet);
begin
  //ReopenSubGr(0);
end;

procedure TFmPlaCtas.QrSubGrAfterScroll(DataSet: TDataSet);
begin
  //ReopenConta(0);
end;

procedure TFmPlaCtas.TbPlanoBeforePost(DataSet: TDataSet);
begin
  if (TbPlano.State = dsInsert) and (TbPlanoCodigo.Value = 0) then
  begin
    TbPlanoCodigo.ReadOnly := False;
    TbPlanoCodigo.Value := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Plano', 'Plano', 'Codigo');
    TbPlanoCodigo.ReadOnly := True;
  end;
end;

procedure TFmPlaCtas.TbConjuBeforePost(DataSet: TDataSet);
begin
  if (TbConju.State = dsInsert) then
  begin
    if (TbConjuCodigo.Value = 0) then
    begin
      TbConjuCodigo.ReadOnly := False;
      TbConjuCodigo.Value := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'Conjuntos', 'Conjunto', 'Codigo');
      TbConjuPlano.Value := TbPlanoCodigo.Value;
      TbConjuCodigo.ReadOnly := True;
    end;
  end else begin
    if TbConjuCodigo.Value = 0 then TbConjuPlano.Value := 0;
    if UMyMod.SQLLoc1(Dmod.QrUpdU, 'Plano', 'Codigo', TbConjuPlano.Value, '',
    'O plano n� ' + IntToStr(TbConjuPlano.Value) + ' n�o existe!') = 0 then
      TbConju.Cancel;
  end;
end;

procedure TFmPlaCtas.TbGrupoBeforePost(DataSet: TDataSet);
begin
  if (TbGrupo.State = dsInsert) and (TbGrupoCodigo.Value = 0) then
  begin
    TbGrupoCodigo.ReadOnly := False;
    TbGrupoCodigo.Value := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Grupos', 'Grupo', 'Codigo');
    TbGrupoConjunto.Value := TbConjuCodigo.Value;
    TbGrupoCodigo.ReadOnly := True;
  end;
end;

procedure TFmPlaCtas.TbSubgrBeforePost(DataSet: TDataSet);
begin
  if (TbSubgr.State = dsInsert) and (TbSubgrCodigo.Value = 0) then
  begin
    TbSubgrCodigo.ReadOnly := False;
    TbSubgrCodigo.Value := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'SubGrupos', 'SubGrupo', 'Codigo');
    TbSubgrGrupo.Value := TbGrupoCodigo.Value;
    TbSubgrCodigo.ReadOnly := True;
  end;
end;

procedure TFmPlaCtas.TbContaBeforePost(DataSet: TDataSet);
begin
  if (TbConta.State = dsInsert) and (TbContaCodigo.Value = 0) then
  begin
    TbContaCodigo.ReadOnly := False;
    TbContaCodigo.Value := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Contas', 'Contas', 'Codigo');
    TbContaSubGrupo.Value := TbSubgrCodigo.Value;
    TbContaCodigo.ReadOnly := True;
  end;
end;

procedure TFmPlaCtas.TbPlanoAfterScroll(DataSet: TDataSet);
begin
  TbConju.Filter := 'Plano='+IntToStr(TbPlanoCodigo.Value);
  QrNCjt.Close;
  if TbPlano.RecordCount > 0 then
  begin
    QrNCjt.Params[0].AsInteger := TbPlanoCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrNCjt, Dmod.MyDB);
  end;
end;

procedure TFmPlaCtas.TbConjuAfterScroll(DataSet: TDataSet);
begin
  QrNGru.Close;
  if TbConju.RecordCount > 0 then
  begin
    TbGrupo.Filter := 'Conjunto='+IntToStr(TbConjuCodigo.Value);
    QrNGru.Params[0].AsInteger := TbConjuCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrNGru, Dmod.MyDB);
  end else
    TbGrupo.Filter := 'Conjunto=-1000';
end;

procedure TFmPlaCtas.TbGrupoAfterScroll(DataSet: TDataSet);
begin
  QrNSgr.Close;
  if TbGrupo.RecordCount > 0 then
  begin
    TbSubgr.Filter := 'Grupo='+IntToStr(TbGrupoCodigo.Value);
    QrNSgr.Params[0].AsInteger := TbGrupoCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrNSgr, Dmod.MyDB);
  end else
    TbSubgr.Filter := 'Grupo=-1000';
end;

procedure TFmPlaCtas.TbSubgrAfterScroll(DataSet: TDataSet);
begin
  QrNCta.Close;
  if TbSubgr.RecordCount > 0 then
  begin
    TbConta.Filter := 'Subgrupo='+IntToStr(TbSubgrCodigo.Value);
    QrNCta.Params[0].AsInteger := TbSubgrCodigo.Value;
    UnDmkDAC_PF.AbreQuery(QrNCta, Dmod.MyDB);
  end else
    TbConta.Filter := 'Subgrupo=-1000';
end;

procedure TFmPlaCtas.CkOcultaCadSistemaClick(Sender: TObject);
begin
  if CkOcultaCadSistema.Checked then
    TbPlano.Filter := 'Codigo>0'
  else
    TbPlano.Filter := '';
end;

procedure TFmPlaCtas.TbConjuAfterEdit(DataSet: TDataSet);
begin
  //if TbConjuCodigo.Value = 0 then
    //TbConju.Cancel;
end;

procedure TFmPlaCtas.TbConjuAfterInsert(DataSet: TDataSet);
begin
  //if TbPlanoCodigo.Value <> 0 then
  //Abort;
end;

////////////////////////////////////////////////////////////////////////////////
//  Mandar do conjunto para o plano:
////////////////////////////////////////////////////////////////////////////////

procedure TFmPlaCtas.DBGConjuCellClick(Column: TColumn);
begin
  if Column.FieldName = '_S' then
  begin
    Screen.Cursor := crHourGlass;
    TbConju.Edit;
    if TbConju_S.Value = 1 then
      TbConjuCtrlaSdo.Value := 0
    else
      TbConjuCtrlaSdo.Value := 1;
    TbConju.Post;
    Screen.Cursor := crDefault;
  end else
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGConju.BeginDrag(True);
end;

procedure TFmPlaCtas.DBGPlanoDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := AceitaDragOver(Source, 'DBGConju', QrNCjt);
end;

procedure TFmPlaCtas.DBGPlanoDblClick(Sender: TObject);
begin
  MostraPlanos();
end;

procedure TFmPlaCtas.DBGPlanoDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  gc : TGridCoord;
  PaiNome: string;
  SonCodi, PaiCodi: Integer;
begin
  VeSeAceitaDragDrop(Sender, Source, X, Y, 'plano', 'Conjunto', 'Conjuntos', 'Plano');
  Exit;
  gc := THackDBGrid(DBGPlano).MouseCoord(X,Y);
  if (gc.X > 0) and (gc.Y > 0) then
  begin
    if TbConju.RecordCount = 0 then Exit;
    if TbConjuCodigo.Value = 0 then Exit;
    PaiNome := TbConjuNOME_PAI.Value;
    SonCodi := TbConjuCodigo.Value;
    with THackDBGrid(DBGPlano) do
    begin
      DataSource.DataSet.MoveBy (gc.Y - Row);
      PaiCodi := TbPlanoCodigo.Value;
      //
      if Geral.MB_Pergunta('Foi solicitada a altera��o conforme abaixo: '
      + sLineBreak + sLineBreak +
      'Conjunto: "' + TbConjuNome.Value + '" ' +
      sLineBreak + sLineBreak +
      'Do plano: "' + PaiNome + '" ' + sLineBreak +
      'Para o plano: "' + TbPlanoNome.Value + '"' +
      sLineBreak + sLineBreak +
      'Confirma a altera��o?') = ID_YES then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'Conjuntos', False,
        ['Plano'], ['Codigo'], [PaiCodi], [SonCodi], True) then
        begin
          TbPlano.Close;
          UnDmkDAC_PF.AbreTable(TbPlano, Dmod.MyDB);
          TbPlano.Locate('Codigo', PaiCodi, []);
          TbConju.Close;
          UnDmkDAC_PF.AbreTable(TbConju, Dmod.MyDB);
          TbConju.Locate('Codigo', SonCodi, []);
        end;
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//  Mandar do Grupo para o Conjunto:
////////////////////////////////////////////////////////////////////////////////

procedure TFmPlaCtas.DBGGrupoCellClick(Column: TColumn);
begin
  if Column.FieldName = '_S' then
  begin
    Screen.Cursor := crHourGlass;
    TbGrupo.Edit;
    if TbGrupo_S.Value = 1 then
      TbGrupoCtrlaSdo.Value := 0
    else
      TbGrupoCtrlaSdo.Value := 1;
    TbGrupo.Post;
    Screen.Cursor := crDefault;
  end else
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGGrupo.BeginDrag(True);
end;

procedure TFmPlaCtas.DBGConjuDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := AceitaDragOver(Source, 'DBGGrupo', QrNGru);
  //Accept := (Source is TDBGrid) and (TDBGrid(Source).Name = 'DBGGrupo');
end;

procedure TFmPlaCtas.DBGConjuDblClick(Sender: TObject);
begin
  MostraConjuntos();
end;

procedure TFmPlaCtas.DBGConjuDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  gc : TGridCoord;
  PaiNome: string;
  SonCodi, PaiCodi: Integer;
begin
  VeSeAceitaDragDrop(Sender, Source, X, Y, 'conjunto', 'Grupo', 'Grupos', 'Conjunto');
  Exit;
  gc := THackDBGrid(DBGConju).MouseCoord(X,Y);
  if (gc.X > 0) and (gc.Y > 0) then
  begin
    if TbGrupo.RecordCount = 0 then Exit;
    if TbGrupoCodigo.Value = 0 then Exit;
    PaiNome := TbGrupoNOME_PAI.Value;
    SonCodi := TbGrupoCodigo.Value;
    with THackDBGrid(DBGConju) do
    begin
      DataSource.DataSet.MoveBy (gc.Y - Row);
      PaiCodi := TbConjuCodigo.Value;
      //
      if Geral.MB_Pergunta('Foi solicitada a altera��o conforme abaixo: '
      + sLineBreak + sLineBreak +
      'Grupo: "' + TbGrupoNome.Value + '" ' +
      sLineBreak + sLineBreak +
      'Do conjunto: "' + PaiNome + '" ' + sLineBreak +
      'Para o conjunto: "' + TbConjuNome.Value + '"' +
      sLineBreak + sLineBreak +
      'Confirma a altera��o?') = ID_YES then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'Grupos', False,
        ['Conjunto'], ['Codigo'], [PaiCodi], [SonCodi], True) then
        begin
          TbConju.Close;
          UnDmkDAC_PF.AbreTable(TbConju, Dmod.MyDB);
          TbConju.Locate('Codigo', PaiCodi, []);
          TbGrupo.Close;
          UnDmkDAC_PF.AbreTable(TbGrupo, Dmod.MyDB);
          TbGrupo.Locate('Codigo', SonCodi, []);
        end;
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//  Mandar do Sub-grupo para o Grupo:
////////////////////////////////////////////////////////////////////////////////

procedure TFmPlaCtas.DBGSubgrCellClick(Column: TColumn);
begin
  if Column.FieldName = '_S' then
  begin
    Screen.Cursor := crHourGlass;
    TbSubgr.Edit;
    if TbSubgr_S.Value = 1 then
      TbSubgrCtrlaSdo.Value := 0
    else
      TbSubgrCtrlaSdo.Value := 1;
    TbSubgr.Post;
    Screen.Cursor := crDefault;
  end else
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGSubgr.BeginDrag(True);
end;

procedure TFmPlaCtas.DBGGrupoDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := AceitaDragOver(Source, 'DBGSubgr', QrNSgr);
  //Accept := (Source is TDBGrid) and (TDBGrid(Source).Name = 'DBGSubgr');
end;

procedure TFmPlaCtas.DBGGrupoDblClick(Sender: TObject);
begin
  MostraGrupos();
end;

procedure TFmPlaCtas.DBGGrupoDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  gc : TGridCoord;
  PaiNome: string;
  SonCodi, PaiCodi: Integer;
begin
  VeSeAceitaDragDrop(Sender, Source, X, Y, 'grupo', 'Sub-grupo', 'SubGrupos', 'Grupo');
  Exit;
  gc := THackDBGrid(DBGGrupo).MouseCoord(X,Y);
  if (gc.X > 0) and (gc.Y > 0) then
  begin
    if TbSubgr.RecordCount = 0 then Exit;
    if TbSubgrCodigo.Value = 0 then Exit;
    PaiNome := TbSubgrNOME_PAI.Value;
    SonCodi := TbSubgrCodigo.Value;
    with THackDBGrid(DBGGrupo) do
    begin
      DataSource.DataSet.MoveBy (gc.Y - Row);
      PaiCodi := TbGrupoCodigo.Value;
      //
      if Geral.MB_Pergunta('Foi solicitada a altera��o conforme abaixo: '
      + sLineBreak + sLineBreak +
      'Sub-grupo: "' + TbSubgrNome.Value + '" ' +
      sLineBreak + sLineBreak +
      'Do grupo: "' + PaiNome + '" ' + sLineBreak +
      'Para o grupo: "' + TbGrupoNome.Value + '"' +
      sLineBreak + sLineBreak +
      'Confirma a altera��o?') = ID_YES then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'SubGrupos', False,
        ['Grupo'], ['Codigo'], [PaiCodi], [SonCodi], True) then
        begin
          TbGrupo.Close;
          UnDmkDAC_PF.AbreTable(TbGrupo, Dmod.MyDB);
          TbGrupo.Locate('Codigo', PaiCodi, []);
          TbSubgr.Close;
          UnDmkDAC_PF.AbreTable(TbSubgr, Dmod.MyDB);
          TbSubgr.Locate('Codigo', SonCodi, []);
        end;
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//  Mandar da Conta para o Sub-grupo:
////////////////////////////////////////////////////////////////////////////////

procedure TFmPlaCtas.DBGContaCellClick(Column: TColumn);
begin
  if Column.FieldName = '_S' then
  begin
    Screen.Cursor := crHourGlass;
    TbConta.Edit;
    if TbConta_S.Value = 1 then
      TbContaCtrlaSdo.Value := 0
    else
      TbContaCtrlaSdo.Value := 1;
    TbConta.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_M' then
  begin
    Screen.Cursor := crHourGlass;
    TbConta.Edit;
    if TbConta_M.Value = 1 then
      TbContaMensal.Value := 'F'
    else
      TbContaMensal.Value := 'V';
    TbConta.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_C' then
  begin
    Screen.Cursor := crHourGlass;
    TbConta.Edit;
    if TbConta_C.Value = 1 then
      TbContaCredito.Value := 'F'
    else
      TbContaCredito.Value := 'V';
    TbConta.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = '_D' then
  begin
    Screen.Cursor := crHourGlass;
    TbConta.Edit;
    if TbConta_D.Value = 1 then
      TbContaDebito.Value := 'F'
    else
      TbContaDebito.Value := 'V';
    TbConta.Post;
    Screen.Cursor := crDefault;
  end else
  if Column.FieldName = 'Ativo' then
  begin
    Screen.Cursor := crHourGlass;
    TbConta.Edit;
    if TbContaAtivo.Value = 1 then
      TbContaAtivo.Value := 0
    else
      TbContaAtivo.Value := 1;
    TbConta.Post;
    Screen.Cursor := crDefault;
  end else
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGConta.BeginDrag(True);
end;

procedure TFmPlaCtas.DBGSubgrDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := AceitaDragOver(Source, 'DBGConta', QrNCta);
  //Accept := (Source is TDBGrid) and (TDBGrid(Source).Name = 'DBGConta');
end;

procedure TFmPlaCtas.DBGSubgrDblClick(Sender: TObject);
begin
  MostraSubgrupos();
end;

procedure TFmPlaCtas.DBGSubgrDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  gc : TGridCoord;
  PaiNome: string;
  SonCodi, PaiCodi: Integer;
begin
  VeSeAceitaDragDrop(Sender, Source, X, Y, 'sub-grupo', 'Conta', 'contas', 'SubGrupo');
  Exit;
  gc := THackDBGrid(DBGSubgr).MouseCoord(X,Y);
  if (gc.X > 0) and (gc.Y > 0) then
  begin
    if TbConta.RecordCount = 0 then Exit;
    if TbContaCodigo.Value = 0 then Exit;
    PaiNome := TbContaNOME_PAI.Value;
    SonCodi := TbContaCodigo.Value;
    with THackDBGrid(DBGSubgr) do
    begin
      DataSource.DataSet.MoveBy (gc.Y - Row);
      PaiCodi := TbSubgrCodigo.Value;
      //
      if Geral.MB_Pergunta('Foi solicitada a altera��o conforme abaixo: '
      + sLineBreak + sLineBreak +
      'Conta: "' + TbContaNome.Value + '" ' +
      sLineBreak + sLineBreak +
      'Do Sub-grupo: "' + PaiNome + '" ' + sLineBreak +
      'Para o Sub-grupo: "' + TbSubgrNome.Value + '"' +
      sLineBreak + sLineBreak +
      'Confirma a altera��o?') = ID_YES then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'Contaupos', False,
        ['Subgrupos'], ['Codigo'], [PaiCodi], [SonCodi], True) then
        begin
          TbSubgr.Close;
          UnDmkDAC_PF.AbreTable(TbSubgr, Dmod.MyDB);
          TbSubgr.Locate('Codigo', PaiCodi, []);
          TbConta.Close;
          UnDmkDAC_PF.AbreTable(TbConta, Dmod.MyDB);
          TbConta.Locate('Codigo', SonCodi, []);
        end;
      end;
    end;
  end;
end;

procedure TFmPlaCtas.TbPlanoBeforeInsert(DataSet: TDataSet);
begin
  // Evitar exception
  //TbConju.Close;
end;

procedure TFmPlaCtas.TbPlanoAfterPost(DataSet: TDataSet);
begin
  QrTPla.Close;
  UnDmkDAC_PF.AbreQuery(QrTPla, Dmod.MyDB);
  TbConju.Refresh;
  // Evitar exception
  //UnDmkDAC_PF.AbreQuery(TbConju, Dmod.MyDB);
end;

procedure TFmPlaCtas.TbPlanoAfterCancel(DataSet: TDataSet);
begin
  // Evitar exception
  //TUnDmkDAC_PF.AbreQuery(bConju, Dmod.MyDB);
end;

procedure TFmPlaCtas.TbConjuBeforeInsert(DataSet: TDataSet);
begin
  // Evitar exception
  //UnDmkDAC_PF.AbreQuery(TbGrupo.Close;
end;

procedure TFmPlaCtas.TbConjuAfterPost(DataSet: TDataSet);
begin
  QrTCjn.Close;
  UnDmkDAC_PF.AbreQuery(QrTCjn, Dmod.MyDB);
  TbGrupo.Refresh;
  // Evitar exception
  //TUnDmkDAC_PF.AbreQuery(bGrupo, Dmod.MyDB);
end;

procedure TFmPlaCtas.TbConjuAfterCancel(DataSet: TDataSet);
begin
  // Evitar exception
  //TUnDmkDAC_PF.AbreQuery(bGrupo, Dmod.MyDB);
end;

procedure TFmPlaCtas.TbGrupoBeforeInsert(DataSet: TDataSet);
begin
  // Evitar exception
  //TbSubgr.Close;
end;

procedure TFmPlaCtas.TbGrupoAfterPost(DataSet: TDataSet);
begin
  QrTGru.Close;
  UnDmkDAC_PF.AbreQuery(QrTGru, Dmod.MyDB);
  TbSubgr.Refresh;
  // Evitar exception
  //UnDmkDAC_PF.AbreQuery(TbSubgr, Dmod.MyDB);
end;

procedure TFmPlaCtas.TbGrupoAfterCancel(DataSet: TDataSet);
begin
  // Evitar exception
  //UnDmkDAC_PF.AbreQuery(TbSubgr, Dmod.MyDB);
end;

procedure TFmPlaCtas.TbSubgrBeforeInsert(DataSet: TDataSet);
begin
  // Evitar exception
  //TbConta.Close;
end;

procedure TFmPlaCtas.TbSubgrAfterPost(DataSet: TDataSet);
begin
  QrTSgr.Close;
  UnDmkDAC_PF.AbreQuery(QrTSgr, Dmod.MyDB);
  TbConta.Refresh;
  // Evitar exception
  //UnDmkDAC_PF.AbreQuery(TbConta, Dmod.MyDB);
end;

procedure TFmPlaCtas.TbSubgrAfterCancel(DataSet: TDataSet);
begin
  // Evitar exception
  //UnDmkDAC_PF.AbreQuery(TbConta, Dmod.MyDB);
end;

procedure TFmPlaCtas.DBGPlanoEnter(Sender: TObject);
begin
  DsExtra.DataSet := QrNCjt;
  RenomeiaTituloExtra('do CONJUNTO', 'do PLANO');
  FInGrade := True;
end;

procedure TFmPlaCtas.DBGConjuEnter(Sender: TObject);
begin
  DsExtra.DataSet := QrNGru;
  RenomeiaTituloExtra('do GRUPO', 'do CONJUNTO');
  FInGrade := True;
end;

procedure TFmPlaCtas.DBGGrupoEnter(Sender: TObject);
begin
  DsExtra.DataSet := QrNSgr;
  RenomeiaTituloExtra('do SUB-GRUPO', 'do GRUPO');
  FInGrade := True;
end;

procedure TFmPlaCtas.DBGSubgrEnter(Sender: TObject);
begin
  DsExtra.DataSet := QrNCta;
  RenomeiaTituloExtra('da CONTA', 'do SUB-GRUPO');
  FInGrade := True;
end;

procedure TFmPlaCtas.DBGContaEnter(Sender: TObject);
begin
  DsExtra.DataSet := nil;
  RenomeiaTituloExtra('Descri��o', '');
  FInGrade := True;
end;

procedure TFmPlaCtas.RenomeiaTituloExtra(TitSon, TitPai: String);
var
  i: Integer;
begin
  for i := 0 to DBGExtra.Columns.Count -1 do
  begin
    if DBGExtra.Columns[i].FieldName = 'Nome' then
      DBGExtra.Columns[i].Title.Caption := 'Descri��o ' + TitSon;
  end;
  StaticText1.Caption := 'Itens n�o pertencentes ' + TitPai + ' atual';
end;

procedure TFmPlaCtas.DBGExtraCellClick(Column: TColumn);
begin
  if FInGrade then
    FInGrade := False
  else
    if CkDragDrop.Checked then DBGExtra.BeginDrag(True);
end;

function TFmPlaCtas.AceitaDragOver(Source: TObject; Grade: String;
  Tabela: TmySQLQuery): Boolean;
begin
  Result := (Source is TDBGrid)  and
  (
    (TDBGrid(Source).Name = Grade) or
    (
      (TDBGrid(Source).Name = 'DBGExtra')
      and
      (DsExtra.DataSet = Tabela)
     )
   );
end;

procedure TFmPlaCtas.VeSeAceitaDragDrop(Sender, Source: TObject; X, Y: Integer;
TitPai, TitSon, Tabela, Campo: String);
var
  gc : TGridCoord;
  PaiNome: string;
  SonCodi, PaiCodi: Integer;
begin
  gc := THackDBGrid(Sender).MouseCoord(X,Y);
  if (gc.X > 0) and (gc.Y > 0) then
  begin
    if THackDBGrid(Sender).DataSource.DataSet.RecordCount = 0 then Exit;
    if THackDBGrid(Sender).DataSource.DataSet.FieldByName('Codigo').AsInteger = 0 then Exit;
    PaiNome := THackDBGrid(Source).DataSource.DataSet.FieldByName('NOME_PAI').AsString;
    SonCodi := THackDBGrid(Source).DataSource.DataSet.FieldByName('Codigo').AsInteger;
    with THackDBGrid(Sender) do
    begin
      DataSource.DataSet.MoveBy(gc.Y - Row);
      PaiCodi := THackDBGrid(Sender).DataSource.DataSet.FieldByName('Codigo').AsInteger;
      //
      if Geral.MB_Pergunta('Foi solicitada a altera��o conforme abaixo: '
      + sLineBreak + sLineBreak + TitSon + ': "' +
      THackDBGrid(Source).DataSource.DataSet.FieldByName('Nome').AsString + '" ' +
      sLineBreak + sLineBreak +
      'Do ' + TitPai + ': "' + PaiNome + '" ' + sLineBreak +
      'Para o ' + TitPai + ': "' +
      THackDBGrid(Sender).DataSource.DataSet.FieldByName('Nome').AsString + '"' +
      sLineBreak + sLineBreak +
      'Confirma a altera��o?') = ID_YES then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False,
        [Campo], ['Codigo'], [PaiCodi], [SonCodi], True) then
        begin
          THackDBGrid(Sender).DataSource.DataSet.Close;
          THackDBGrid(Sender).DataSource.DataSet. Open;
          THackDBGrid(Sender).DataSource.DataSet.Locate('Codigo', PaiCodi, []);
          //
          THackDBGrid(Source).DataSource.DataSet.Close;
          THackDBGrid(Source).DataSource.DataSet. Open;
          THackDBGrid(Source).DataSource.DataSet.Locate('Codigo', SonCodi, []);
          //
          TbConta.Refresh;
          TbSubgr.Refresh;
          TbGrupo.Refresh;
          TbConju.Refresh;
          TbPlano.Refresh;
        end;
      end;
    end;
  end;
end;

procedure TFmPlaCtas.SbConjuntosClick(Sender: TObject);
begin
  MostraConjuntos();
end;

procedure TFmPlaCtas.SbContasClick(Sender: TObject);
begin
  MostraContas();
end;

procedure TFmPlaCtas.SbGruposClick(Sender: TObject);
begin
  MostraGrupos();
end;

procedure TFmPlaCtas.SbImprimeClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

procedure TFmPlaCtas.MostraPlanos();
var
  Codigo: Integer;
begin
  if (TbPlano.State <> dsInactive) and (TbPlano.RecordCount > 0) then
    Codigo := TbPlanoCodigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDePlano(Codigo);
  //
  TbPlano.Close;
  UnDmkDAC_PF.AbreTable(TbPlano, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbPlano.Locate('Codigo', Codigo, []);
end;

procedure TFmPlaCtas.MostraConjuntos();
var
  Codigo: Integer;
begin
  if (TbConju.State <> dsInactive) and (TbConju.RecordCount > 0) then
    Codigo := TbConjuCodigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDeConjutos(Codigo);
  //
  TbConju.Close;
  UnDmkDAC_PF.AbreTable(TbConju, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbConju.Locate('Codigo', Codigo, []);
end;

procedure TFmPlaCtas.MostraGrupos();
var
  Codigo: Integer;
begin
  if (TbGrupo.State <> dsInactive) and (TbGrupo.RecordCount > 0) then
    Codigo := TbGrupoCodigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDeGrupos(Codigo);
  //
  TbGrupo.Close;
  UnDmkDAC_PF.AbreTable(TbGrupo, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbGrupo.Locate('Codigo', Codigo, []);
end;

procedure TFmPlaCtas.MostraSubgrupos();
var
  Codigo: Integer;
begin
  if (TbSubgr.State <> dsInactive) and (TbSubgr.RecordCount > 0) then
    Codigo := TbSubgrCodigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDeSubGrupos(Codigo);
  //
  TbSubgr.Close;
  UnDmkDAC_PF.AbreTable(TbSubgr, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbSubgr.Locate('Codigo', Codigo, []);
end;

procedure TFmPlaCtas.MostraContas();
var
  Codigo: Integer;
begin
  if (TbConta.State <> dsInactive) and (TbConta.RecordCount > 0) then
    Codigo := TbContaCodigo.Value
  else
    Codigo := 0;
  //
  FinanceiroJan.CadastroDeContas(Codigo);
  //
  TbConta.Close;
  UnDmkDAC_PF.AbreTable(TbConta, Dmod.MyDB);
  //
  if Codigo <> 0 then
    TbConta.Locate('Codigo', Codigo, []);
end;

procedure TFmPlaCtas.SbPlanoClick(Sender: TObject);
begin
  MostraPlanos();
end;

procedure TFmPlaCtas.SbSubgruposClick(Sender: TObject);
begin
  MostraSubgrupos();
end;

procedure TFmPlaCtas.CkDragDropClick(Sender: TObject);
begin
  PnExtra.Visible := CkDragDrop.Checked;
end;

procedure TFmPlaCtas.TbPlanoBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmPlaCtas.TbConjuBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmPlaCtas.TbGrupoBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmPlaCtas.TbSubgrBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmPlaCtas.TbContaBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmPlaCtas.TbContaCalcFields(DataSet: TDataSet);
begin
  if TbContaMensal.Value = '' then TbConta_M.Value := 0
  else TbConta_M.Value := dmkPF.V_FToInt(TbContaMensal.Value[1]);

  TbConta_S.Value := TbContaCtrlaSdo.Value;

  if TbContaCredito.Value = '' then TbConta_C.Value := 0
  else TbConta_C.Value := dmkPF.V_FToInt(TbContaCredito.Value[1]);

  if TbContaDebito.Value = '' then TbConta_D.Value := 0
  else TbConta_D.Value := dmkPF.V_FToInt(TbContaDebito.Value[1]);

end;

procedure TFmPlaCtas.DBGContaDblClick(Sender: TObject);
begin
  MostraContas();
end;

procedure TFmPlaCtas.DBGContaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = '_M' then
    MeuVCLSkin.DrawGrid(DBGConta, Rect, 1, TbConta_M.Value);
  if Column.FieldName = '_S' then
    MeuVCLSkin.DrawGrid(DBGConta, Rect, 1, TbConta_S.Value);
  if Column.FieldName = '_C' then
    MeuVCLSkin.DrawGrid(DBGConta, Rect, 1, TbConta_C.Value);
  if Column.FieldName = '_D' then
    MeuVCLSkin.DrawGrid(DBGConta, Rect, 1, TbConta_D.Value);
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGConta, Rect, 1, TbContaAtivo.Value);
end;

procedure TFmPlaCtas.DBGContaColEnter(Sender: TObject);
begin
  if (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_S')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_M')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_C')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = '_D')
  or (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = 'Ativo') then
    TDBGrid(Sender).Options := TDBGrid(Sender).Options - [dgEditing] else
    TDBGrid(Sender).Options := TDBGrid(Sender).Options + [dgEditing];
end;

procedure TFmPlaCtas.DBGContaColExit(Sender: TObject);
begin
  THackDBGrid(Sender).Options := THackDBGrid(Sender).Options - [dgEditing];
end;

procedure TFmPlaCtas.TbPlanoCalcFields(DataSet: TDataSet);
begin
  TbPlano_S.Value := TbPlanoCtrlaSdo.Value;
end;

procedure TFmPlaCtas.TbConjuCalcFields(DataSet: TDataSet);
begin
  TbConju_S.Value := TbConjuCtrlaSdo.Value;
end;

procedure TFmPlaCtas.TbGrupoCalcFields(DataSet: TDataSet);
begin
  TbGrupo_S.Value := TbGrupoCtrlaSdo.Value;
end;

procedure TFmPlaCtas.TbSubgrCalcFields(DataSet: TDataSet);
begin
  TbSubgr_S.Value := TbSubgrCtrlaSdo.Value;
end;

procedure TFmPlaCtas.DBGPlanoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = '_S' then
    MeuVCLSkin.DrawGrid(DBGPlano, Rect, 1, TbPlano_S.Value);
end;

procedure TFmPlaCtas.DBGConjuDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = '_S' then
    MeuVCLSkin.DrawGrid(DBGConju, Rect, 1, TbConju_S.Value);
end;

procedure TFmPlaCtas.DBGGrupoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = '_S' then
    MeuVCLSkin.DrawGrid(DBGGrupo, Rect, 1, TbGrupo_S.Value);
end;

procedure TFmPlaCtas.DBGSubgrDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = '_S' then
    MeuVCLSkin.DrawGrid(DBGSubgr, Rect, 1, TbSubgr_S.Value);
end;

procedure TFmPlaCtas.DBGPlanoCellClick(Column: TColumn);
begin
  if Column.FieldName = '_S' then
  begin
    Screen.Cursor := crHourGlass;
    TbPlano.Edit;
    if TbPlano_S.Value = 1 then
      TbPlanoCtrlaSdo.Value := 0
    else
      TbPlanoCtrlaSdo.Value := 1;
    TbPlano.Post;
    Screen.Cursor := crDefault;
  end;
end;

(*
DELETE FROM cta
USING
  contas cta,
  subgrupos sgr,
  grupos gru,
  conjuntos cjt,
  plano pla
WHERE cta.SubGrupo=sgr.Codigo
AND sgr.Grupo=gru.Codigo
AND gru.Conjunto=cjt.Codigo
AND cjt.Plano=pla.Codigo
AND pla.Codigo IN (0,4,5,6);
/**/
DELETE FROM sgr
USING
  subgrupos sgr,
  grupos gru,
  conjuntos cjt,
  plano pla
WHERE sgr.Grupo=gru.Codigo
AND gru.Conjunto=cjt.Codigo
AND cjt.Plano=pla.Codigo
AND pla.Codigo IN (0,4,5,6);
/**/
DELETE FROM gru
USING
  grupos gru,
  conjuntos cjt,
  plano pla
WHERE gru.Conjunto=cjt.Codigo
AND cjt.Plano=pla.Codigo
AND pla.Codigo IN (0,4,5,6);
/**/
DELETE FROM cjt
USING
  conjuntos cjt,
  plano pla
WHERE cjt.Plano=pla.Codigo
AND pla.Codigo IN (0,4,5,6);
/**/
DELETE FROM plano
WHERE Codigo IN (0,4,5,6);
/**/
SELECT * FROM contas;

*)
end.
