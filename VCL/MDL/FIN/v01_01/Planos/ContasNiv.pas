unit ContasNiv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids,
  Db, mySQLDbTables, dmkDBGrid, DBCtrls, Menus, ComCtrls, Variants, dmkImage,
  UnDMkEnums;

type
  TFmContasNiv = class(TForm)
    Panel1: TPanel;
    QrEmpresas: TmySQLQuery;
    DsEmpresas: TDataSource;
    QrEmpresasCodigo: TIntegerField;
    QrEmpresasCliInt: TIntegerField;
    QrEmpresasNOMECLI: TWideStringField;
    QrContasNiv: TmySQLQuery;
    DsContasNiv: TDataSource;
    dmkDBGrid2: TdmkDBGrid;
    QrContasNivNOMENIVEL: TWideStringField;
    QrContasNivNOMEGENERO: TWideStringField;
    QrContasNivNivel: TIntegerField;
    QrContasNivGenero: TIntegerField;
    Panel3: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    Splitter1: TSplitter;
    PMAcao: TPopupMenu;
    Incluiitemacontrolar1: TMenuItem;
    Excluiitemacontrolar1: TMenuItem;
    Selecionavriositens1: TMenuItem;
    N1: TMenuItem;
    QrNiveis: TmySQLQuery;
    QrNiveisCodigo: TIntegerField;
    QrNiveisNome: TWideStringField;
    QrNiveisCtrlaSdo: TSmallintField;
    QrNiveisNivel: TLargeintField;
    QrContasNivEntidade: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtAcao: TBitBtn;
    BtSaida: TBitBtn;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrEmpresasAfterScroll(DataSet: TDataSet);
    procedure BtAcaoClick(Sender: TObject);
    procedure Incluiitemacontrolar1Click(Sender: TObject);
    procedure Selecionavriositens1Click(Sender: TObject);
    procedure Excluiitemacontrolar1Click(Sender: TObject);
  private
    { Private declarations }
    FCarregou: Boolean;
    FTabela: String;
  public
    { Public declarations }
    procedure ReopenContasNiv(Nivel, Genero: Integer);
  end;

  var
  FmContasNiv: TFmContasNiv;

implementation

uses UnMyObjects, Module, NivelEGenero, MyDBCheck, UMySQLModule, dmkGeral,
  ContasNivSel, UCreate, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmContasNiv.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasNiv.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasNiv.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasNiv.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCarregou       := False;
  FTabela         := '';
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  UnDmkDAC_PF.AbreQuery(QrEmpresas, Dmod.MyDB);
end;

procedure TFmContasNiv.QrEmpresasAfterScroll(DataSet: TDataSet);
begin
  ReopenContasNiv(0,0);
  BtAcao.Enabled := QrEmpresasCodigo.Value <> 0;
end;

procedure TFmContasNiv.ReopenContasNiv(Nivel, Genero: Integer);
begin
  QrContasNiv.Close;
  QrContasNiv.Params[0].AsInteger := QrEmpresasCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrContasNiv, Dmod.MyDB);
  //
  QrContasNiv.Locate('Nivel;Genero', VarArrayOf([Nivel, Genero]), []);
end;

procedure TFmContasNiv.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmContasNiv.Incluiitemacontrolar1Click(Sender: TObject);
var
  Entidade, Nivel, Genero: Integer;
begin
  Nivel    := 0;
  Genero   := 0;
  Entidade := QrEmpresasCodigo.Value;
  //
  if DBCheck.CriaFm(TFmNivelEGenero, FmNivelEGenero, afmoNegarComAviso) then
  begin
    FmNivelEGenero.ShowModal;
    //
    Nivel  := FmNivelEGenero.FNivel;
    Genero := FmNivelEGenero.FGenero;
    //
    FmNivelEGenero.Destroy;
  end;
  //
  if (Nivel <> 0) and (Genero <> 0) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'contasniv', False, [],
      ['Entidade', 'Nivel', 'Genero'], [], [Entidade, Nivel, Genero], True)
    then
      ReopenContasNiv(Nivel, Genero);
  end;
end;

procedure TFmContasNiv.Selecionavriositens1Click(Sender: TObject);
var
  NomeNi: String;
begin
  if not FCarregou then
  begin
    FTabela := UCriar.RecriaTempTableNovo(ntrttSdoNiveis, DmodG.QrUpdPID1, False, 0, 'SdoNiveis');
    //
    Screen.Cursor := crHourGlass;
    try
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Listando itens.');
      //
      PB1.Position := 0;
      //
      Application.ProcessMessages;
      //
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FTabela + ' SET');
      DModG.QrUpdPID1.SQL.Add('Nivel=:P0, Genero=:P1, NomeNi=:P2, ');
      DModG.QrUpdPID1.SQL.Add('NomeGe=:P3, Ctrla=:P4, Seleci=:P5');
      //
      QrNiveis.Close;
      UnDmkDAC_PF.AbreQuery(QrNiveis, Dmod.MyDB);
      //
      PB1.Max := QrNiveis.RecordCount;
      //
      while not QrNiveis.Eof do
      begin
        PB1.Position := PB1.Position + 1;
        //
        case QrNiveisNivel.Value of
            1: NomeNi := 'Conta';
            2: NomeNi := 'Sub-grupo';
            3: NomeNi := 'Grupo';
            4: NomeNi := 'Conjunto';
            5: NomeNi := 'Plano';
          else NomeNi := '? ? ?';
        end;
        //
        DModG.QrUpdPID1.Params[00].AsInteger := QrNiveisNivel.Value;
        DModG.QrUpdPID1.Params[01].AsInteger := QrNiveisCodigo.Value;
        DModG.QrUpdPID1.Params[02].AsString  := NomeNi;
        DModG.QrUpdPID1.Params[03].AsString  := QrNiveisNome.Value;
        DModG.QrUpdPID1.Params[05].AsInteger := QrNiveisCtrlaSdo.Value;
        DModG.QrUpdPID1.Params[04].AsInteger := QrNiveisCtrlaSdo.Value;
        DModG.QrUpdPID1.ExecSQL;
        //
        QrNiveis.Next;
      end;
    finally
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      FCarregou     := True;
      Screen.Cursor := crDefault;
    end;
  end;
  if DBCheck.CriaFm(TFmContasNivSel, FmContasNivSel, afmoNegarComAviso) then
  begin
    FmContasNivSel.FTabela := FTabela;
    FmContasNivSel.ShowModal;
    FmContasNivSel.Destroy;
  end;
end;

procedure TFmContasNiv.Excluiitemacontrolar1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrContasNiv, TDBGrid(dmkDBGrid2),
    'contasniv', ['Entidade', 'Nivel', 'Genero'],
    ['Entidade', 'Nivel', 'Genero'], istPergunta, '');
  ReopenContasNiv(0,0);
end;

end.

