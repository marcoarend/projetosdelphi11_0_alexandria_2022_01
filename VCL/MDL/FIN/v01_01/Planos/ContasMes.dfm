object FmContasMes: TFmContasMes
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-021 :: Emiss'#245'es Mensais'
  ClientHeight = 451
  ClientWidth = 526
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 526
    Height = 289
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 288
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 70
      Height = 13
      Caption = 'Cliente interno:'
    end
    object Label2: TLabel
      Left = 12
      Top = 84
      Width = 72
      Height = 13
      Caption = 'Descri'#231#227'o [F4]:'
    end
    object Label5: TLabel
      Left = 12
      Top = 124
      Width = 94
      Height = 13
      Caption = 'Compet'#234'ncia inicial:'
    end
    object Label16: TLabel
      Left = 112
      Top = 126
      Width = 22
      Height = 12
      Caption = 'pq'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Wingdings 3'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 300
      Top = 124
      Width = 87
      Height = 13
      Caption = 'Compet'#234'ncia final:'
    end
    object Label4: TLabel
      Left = 392
      Top = 126
      Width = 22
      Height = 12
      Caption = 'pq'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Wingdings 3'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 12
      Top = 44
      Width = 316
      Height = 13
      Caption = 'Conta (Plano de contas): (somente contas mensais s'#227'o mostradas!)'
    end
    object EdCliInt: TdmkEditCB
      Left = 12
      Top = 20
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInt64
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCliInt
      IgnoraDBLookupComboBox = False
    end
    object CBCliInt: TdmkDBLookupComboBox
      Left = 60
      Top = 20
      Width = 453
      Height = 21
      KeyField = 'CO_SHOW'
      ListField = 'NO_EMPRESA'
      ListSource = DsCliInt
      TabOrder = 1
      dmkEditCB = EdCliInt
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdDescricao: TdmkEdit
      Left = 12
      Top = 100
      Width = 501
      Height = 21
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Descricao'
      UpdCampo = 'Descricao'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnKeyDown = EdDescricaoKeyDown
    end
    object Edit1: TdmkEdit
      Left = 48
      Top = 140
      Width = 177
      Height = 21
      ReadOnly = True
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdPeriodoIni: TdmkEdit
      Left = 12
      Top = 140
      Width = 36
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'PeriodoIni'
      UpdCampo = 'PeriodoIni'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdPeriodoIniChange
    end
    object Edit2: TdmkEdit
      Left = 336
      Top = 140
      Width = 177
      Height = 21
      ReadOnly = True
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdPeriodoFim: TdmkEdit
      Left = 300
      Top = 140
      Width = 36
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'PeriodoFim'
      UpdCampo = 'PeriodoFim'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdPeriodoFimChange
    end
    object EdCodigo: TdmkEditCB
      Left = 12
      Top = 60
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCodigo
      IgnoraDBLookupComboBox = False
    end
    object CBCodigo: TdmkDBLookupComboBox
      Left = 60
      Top = 60
      Width = 453
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 3
      dmkEditCB = EdCodigo
      QryCampo = 'Codigo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object GroupBox1: TGroupBox
      Left = 12
      Top = 164
      Width = 189
      Height = 65
      Caption = ' Soma dos valores dos: documentos: '
      TabOrder = 9
      object Label7: TLabel
        Left = 8
        Top = 20
        Width = 64
        Height = 13
        Caption = 'Valor m'#237'nimo:'
      end
      object Label8: TLabel
        Left = 92
        Top = 20
        Width = 65
        Height = 13
        Caption = 'Valor m'#225'ximo:'
      end
      object EdValorMin: TdmkEdit
        Left = 8
        Top = 36
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0,01'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,01'
        QryCampo = 'ValorMin'
        UpdCampo = 'ValorMin'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.010000000000000000
        ValWarn = False
      end
      object EdValorMax: TdmkEdit
        Left = 92
        Top = 36
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0,01'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,01'
        QryCampo = 'ValorMax'
        UpdCampo = 'ValorMax'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.010000000000000000
        ValWarn = False
      end
    end
    object GroupBox2: TGroupBox
      Left = 204
      Top = 164
      Width = 153
      Height = 65
      Caption = ' Qtde de documentos: '
      TabOrder = 10
      object Label9: TLabel
        Left = 12
        Top = 20
        Width = 63
        Height = 13
        Caption = 'Qtde m'#237'nima:'
      end
      object Label10: TLabel
        Left = 80
        Top = 20
        Width = 64
        Height = 13
        Caption = 'Qtde m'#225'xima:'
      end
      object EdQtdeMin: TdmkEdit
        Left = 12
        Top = 36
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '1'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        QryCampo = 'QtdeMin'
        UpdCampo = 'QtdeMin'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
      end
      object EdQtdeMax: TdmkEdit
        Left = 80
        Top = 36
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '1'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        QryCampo = 'QtdeMax'
        UpdCampo = 'QtdeMax'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
      end
    end
    object GroupBox3: TGroupBox
      Left = 360
      Top = 164
      Width = 153
      Height = 65
      Caption = ' Dia do m'#234's: '
      TabOrder = 11
      object Label11: TLabel
        Left = 12
        Top = 20
        Width = 59
        Height = 13
        Caption = 'In'#237'cio alerta:'
      end
      object Label12: TLabel
        Left = 80
        Top = 20
        Width = 59
        Height = 13
        Caption = 'Vencimento:'
      end
      object EdDiaAlert: TdmkEdit
        Left = 12
        Top = 36
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '31'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'DiaAlert'
        UpdCampo = 'DiaAlert'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdDiaVence: TdmkEdit
        Left = 80
        Top = 36
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '31'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'DiaVence'
        UpdCampo = 'DiaVence'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object RGTipMesRef: TdmkRadioGroup
      Left = 12
      Top = 232
      Width = 501
      Height = 45
      Caption = ' M'#234's de compet'#234'ncia em rela'#231#227'o ao vencimento: '
      Columns = 5
      ItemIndex = 1
      Items.Strings = (
        '2 meses antes'
        'M'#234's anterior'
        'Mesmo M'#234's'
        'M'#234's posterios'
        '2 meses depois')
      TabOrder = 12
      QryCampo = 'TipMesRef'
      UpdCampo = 'TipMesRef'
      UpdType = utYes
      OldValor = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 526
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = -258
    ExplicitWidth = 784
    object GB_R: TGroupBox
      Left = 478
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 736
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 430
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 688
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 228
        Height = 32
        Caption = 'Emiss'#245'es Mensais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 228
        Height = 32
        Caption = 'Emiss'#245'es Mensais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 228
        Height = 32
        Caption = 'Emiss'#245'es Mensais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 337
    Width = 526
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitLeft = -258
    ExplicitTop = 341
    ExplicitWidth = 784
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 522
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 780
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 381
    Width = 526
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitLeft = -258
    ExplicitTop = 315
    ExplicitWidth = 784
    object PnSaiDesis: TPanel
      Left = 380
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 638
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 378
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 636
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, uf.Nome NO_UF,'
      'FLOOR(IF(en.Codigo<-10,en.Filial,en.CliInt)) CO_SHOW,'
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_EMPRESA,'
      'IF(en.Tipo=0,en.ECidade,en.PCidade) CIDADE'
      'FROM entidades en'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)'
      'WHERE en.Codigo < -10'
      'OR en.Codigo=-1'
      'OR en.CliInt <> 0')
    Left = 356
    Top = 58
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrCliIntNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrCliIntCO_SHOW: TLargeintField
      FieldName = 'CO_SHOW'
      Required = True
    end
    object QrCliIntNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Required = True
      Size = 100
    end
    object QrCliIntCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 384
    Top = 58
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'AND Mensal="V"'
      'ORDER BY Nome')
    Left = 356
    Top = 86
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 384
    Top = 86
  end
  object VUCliInt: TdmkValUsu
    dmkEditCB = EdCliInt
    Panel = Panel1
    QryCampo = 'CliInt'
    UpdCampo = 'CliInt'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 12
    Top = 8
  end
end
