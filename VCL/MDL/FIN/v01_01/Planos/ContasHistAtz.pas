unit ContasHistAtz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, DB, mySQLDbTables, Grids,
  DBGrids, UMySQLModule, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmContasHistAtz = class(TForm)
    Panel1: TPanel;
    QrOrfaos: TmySQLQuery;
    DsOrfaos: TDataSource;
    DBGPesq: TDBGrid;
    QrOrfaosData: TDateField;
    QrOrfaosControle: TIntegerField;
    QrOrfaosSub: TSmallintField;
    QrOrfaosID_Pgto: TIntegerField;
    QrOrfaosID_Sub: TSmallintField;
    QrOrfaosCredito: TFloatField;
    QrOrfaosDebito: TFloatField;
    QrOrfaosDocumento: TFloatField;
    QrOrfaosCarteira: TIntegerField;
    QrOrfaosDescricao: TWideStringField;
    QrLoc: TmySQLQuery;
    QrLocGenero: TIntegerField;
    DBGOrfaos: TDBGrid;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqData: TDateField;
    QrPesqControle: TIntegerField;
    QrPesqSub: TSmallintField;
    QrPesqCredito: TFloatField;
    QrPesqDebito: TFloatField;
    QrPesqDocumento: TFloatField;
    QrPesqCarteira: TIntegerField;
    QrPesqDescricao: TWideStringField;
    QrPesqGenero: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtLocaliza: TBitBtn;
    BtLinkar: TBitBtn;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtLocalizaClick(Sender: TObject);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure BtLinkarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenOrfaos();
  public
    { Public declarations }
    FTabLctA, FTabLctB, FTabLctD: String;
    procedure ConsertaLancamento();
  end;

  var
  FmContasHistAtz: TFmContasHistAtz;

implementation

uses UnMyObjects, Module, UnInternalConsts, UnFinanceiro, DmkDAC_PF;

{$R *.DFM}

procedure TFmContasHistAtz.BtLinkarClick(Sender: TObject);
begin
  BtLinkar.Enabled := False;
  //
  {
  DMod.MyDB.Execute('UPDATE lan ctos SET Genero=' + FormatFloat('0',
  QrPesqGenero.Value) + ' WHERE Controle=' + FormatFloat('0',
  QrOrfaosControle.Value) + ' AND Sub='  + FormatFloat('0',
  QrOrfaosSub.Value));
  }
  UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
  'Genero'], ['Controle', 'Sub'], [QrPesqGenero.Value], [
  QrOrfaosControle.Value, QrOrfaosSub.Value], True, '', FTabLctA);
  //
  QrPesq.Close;
  DBGPesq.Visible := False;
  //
  ReopenOrfaos();
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Geral.FF0(QrOrfaos.RecordCount) +
  ' registros n�o puderam ser corrigidos.');
end;

procedure TFmContasHistAtz.BtLocalizaClick(Sender: TObject);
begin
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT Data, Controle, Sub, Genero, Credito,');
  QrPesq.SQL.Add('Debito, Documento, Carteira, Descricao');
  QrPesq.SQL.Add('FROM ' + FTabLctA);
  QrPesq.SQL.Add('WHERE Credito=:P0');
  QrPesq.SQL.Add('AND Debito=:P1');
  QrPesq.SQL.Add('AND Documento=:P2');
  QrPesq.SQL.Add('AND Controle <> :P3');
  QrPesq.SQL.Add('AND Tipo=2');
  QrPesq.Params[00].AsFloat   := QrOrfaosCredito.Value;
  QrPesq.Params[01].AsFloat   := QrOrfaosDebito.Value;
  QrPesq.Params[02].AsFloat   := QrOrfaosDocumento.Value;
  QrPesq.Params[03].AsInteger := QrOrfaosControle.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrPesq, Dmod.MyDB);
  DBGPesq.Visible := True;
end;

procedure TFmContasHistAtz.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasHistAtz.ConsertaLancamento();
var
  Controle, Sub: String;
begin
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Consertando lan�amentos de g�nero -5 para o do lan�amento origem.');
    PB1.Position := 0;
    PB1.Max := QrOrfaos.RecordCount;
    QrOrfaos.First;
    while not QrOrfaos.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      if QrOrfaosID_Pgto.Value <> 0 then
      begin
        Controle := FormatFloat('0', QrOrfaosID_Pgto.Value);
        Sub      := FormatFloat('0', QrOrfaosID_Sub.Value);
        QrLoc.Close;
        QrLoc.SQL.Clear;
        QrLoc.SQL.Add('SELECT Genero');
        QrLoc.SQL.Add('FROM ' + FTabLctA);
        QrLoc.SQL.Add('WHERE Controle=' + Controle);
        QrLoc.SQL.Add('AND Sub=' + Sub);
        QrLoc.SQL.Add('UNION');
        QrLoc.SQL.Add('SELECT Genero');
        QrLoc.SQL.Add('FROM ' + FTabLctB);
        QrLoc.SQL.Add('WHERE Controle=' + Controle);
        QrLoc.SQL.Add('AND Sub=' + Sub);
        QrLoc.SQL.Add('UNION');
        QrLoc.SQL.Add('SELECT Genero');
        QrLoc.SQL.Add('FROM ' + FTabLctD);
        QrLoc.SQL.Add('WHERE Controle=' + Controle);
        QrLoc.SQL.Add('AND Sub=' + Sub);
        {
        QrLoc.SQL.Add('SELECT Genero');
        QrLoc.SQL.Add('FROM ' + VAR LCT);
        QrLoc.SQL.Add('WHERE Controle=:P0');
        QrLoc.SQL.Add('AND Sub=:P1');
        QrLoc.Params[00].AsInteger := QrOrfaosID_Pgto.Value;
        QrLoc.Params[01].AsInteger := QrOrfaosID_Sub.Value;
        }
        UnDmkDAC_PF.AbreQuery(QrLoc, Dmod.MyDB);
        if QrLocGenero.Value > 0 then
        begin
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Genero'], ['Controle', 'Sub'], [QrLocGenero.Value], [
          QrOrfaosControle.Value, QrOrfaosSub.Value], True, '', FTabLctA);
        end;
        //
      end;  
      QrOrfaos.Next;
    end;
    //PB1.Position := PB1.Max;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Reabrindo tabela.');
    ReopenOrfaos();
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmContasHistAtz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasHistAtz.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmContasHistAtz.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasHistAtz.QrPesqAfterOpen(DataSet: TDataSet);
begin
  BtLinkar.Enabled := QrPesq.RecordCount > 0;
end;

procedure TFmContasHistAtz.QrPesqBeforeClose(DataSet: TDataSet);
begin
  BtLinkar.Enabled := False;
end;

procedure TFmContasHistAtz.ReopenOrfaos();
begin
  QrOrfaos.Close;
  QrOrfaos.SQL.Clear;
  QrOrfaos.SQL.Add('SELECT Data, Controle, Sub,');
  QrOrfaos.SQL.Add('ID_Pgto, ID_Sub, Credito, Debito,');
  QrOrfaos.SQL.Add('Documento, Carteira, Descricao');
  QrOrfaos.SQL.Add('FROM ' + FTabLctA);
  QrOrfaos.SQL.Add('WHERE Genero=-5');
  UnDmkDAC_PF.AbreQuery(QrOrfaos, Dmod.MyDB);
end;

end.
