object FmContasNivSel: TFmContasNivSel
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-026 :: Sele'#231#227'o de G'#234'neros de Diversos N'#237'veis'
  ClientHeight = 610
  ClientWidth = 774
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 774
    Height = 411
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object dmkDBGrid1: TdmkDBGrid
      Left = 0
      Top = 0
      Width = 774
      Height = 411
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Seleci'
          Title.Caption = 'ok'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nivel'
          Title.Caption = 'N'#237'vel'
          Width = 33
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeNi'
          Title.Caption = 'Descri'#231#227'o'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'G'#234'nero'
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeGe'
          Title.Caption = 'Descri'#231#227'o'
          Width = 420
          Visible = True
        end>
      Color = clWindow
      DataSource = DsSdoNiveis
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dmkDBGrid1CellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Seleci'
          Title.Caption = 'ok'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nivel'
          Title.Caption = 'N'#237'vel'
          Width = 33
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeNi'
          Title.Caption = 'Descri'#231#227'o'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'G'#234'nero'
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeGe'
          Title.Caption = 'Descri'#231#227'o'
          Width = 420
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 774
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 715
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 656
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 566
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sele'#231#227'o de G'#234'neros de Diversos N'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 566
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sele'#231#227'o de G'#234'neros de Diversos N'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 566
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sele'#231#227'o de G'#234'neros de Diversos N'#237'veis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 470
    Width = 774
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 770
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 524
    Width = 774
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 18
      Width = 770
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 593
        Top = 0
        Width = 177
        Height = 66
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 25
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkControla: TCheckBox
        Left = 192
        Top = 7
        Width = 257
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Mostrar somente g'#234'neros que controla.'
        Checked = True
        State = cbChecked
        TabOrder = 2
        OnClick = CkControlaClick
      end
      object CkSeleci: TCheckBox
        Left = 192
        Top = 32
        Width = 257
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Mostrar somente g'#234'neros selecionados.'
        TabOrder = 3
        OnClick = CkControlaClick
      end
    end
  end
  object QrSdoNiveis: TmySQLQuery
   
    SQL.Strings = (
      'SELECT * '
      'FROM sdoniveis'
      'WHERE Seleci>:P0'
      'AND Ctrla>:P1')
    Left = 156
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSdoNiveisNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'sdoniveis.Nivel'
    end
    object QrSdoNiveisGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'sdoniveis.Genero'
    end
    object QrSdoNiveisNomeGe: TWideStringField
      FieldName = 'NomeGe'
      Origin = 'sdoniveis.NomeGe'
      Size = 100
    end
    object QrSdoNiveisNomeNi: TWideStringField
      FieldName = 'NomeNi'
      Origin = 'sdoniveis.NomeNi'
    end
    object QrSdoNiveisSumMov: TFloatField
      FieldName = 'SumMov'
      Origin = 'sdoniveis.SumMov'
    end
    object QrSdoNiveisSdoAnt: TFloatField
      FieldName = 'SdoAnt'
      Origin = 'sdoniveis.SdoAnt'
    end
    object QrSdoNiveisSumCre: TFloatField
      FieldName = 'SumCre'
      Origin = 'sdoniveis.SumCre'
    end
    object QrSdoNiveisSumDeb: TFloatField
      FieldName = 'SumDeb'
      Origin = 'sdoniveis.SumDeb'
    end
    object QrSdoNiveisSdoFim: TFloatField
      FieldName = 'SdoFim'
      Origin = 'sdoniveis.SdoFim'
    end
    object QrSdoNiveisSeleci: TSmallintField
      FieldName = 'Seleci'
      Origin = 'sdoniveis.Seleci'
      MaxValue = 1
    end
    object QrSdoNiveisCtrla: TSmallintField
      FieldName = 'Ctrla'
      Origin = 'sdoniveis.Ctrla'
    end
  end
  object DsSdoNiveis: TDataSource
    DataSet = QrSdoNiveis
    Left = 184
    Top = 128
  end
end
