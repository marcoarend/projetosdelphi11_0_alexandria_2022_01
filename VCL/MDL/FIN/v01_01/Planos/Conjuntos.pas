unit Conjuntos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, ComCtrls, dmkDBGridDAC, dmkDBGrid, Variants, dmkPermissoes, dmkGeral,
  dmkEdit, dmkEditCB, dmkDBLookupComboBox, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmConjuntos = class(TForm)
    DsConjuntos: TDataSource;
    QrConjuntos: TmySQLQuery;
    QrPlano: TmySQLQuery;
    QrPlanoCodigo: TIntegerField;
    QrPlanoNome: TWideStringField;
    DsPlano: TDataSource;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdCodigo: TdmkEdit;
    CBPlano: TdmkDBLookupComboBox;
    EdOrdemLista: TdmkEdit;
    PainelDados: TPanel;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    LaConjunto: TLabel;
    Label5: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdConjunto: TDBEdit;
    DBEdOrdem: TDBEdit;
    TbGrupos: TmySQLTable;
    DsGrupos: TDataSource;
    QrConjuntosCodigo: TIntegerField;
    QrConjuntosNome: TWideStringField;
    QrConjuntosPlano: TIntegerField;
    QrConjuntosLk: TIntegerField;
    QrConjuntosDataCad: TDateField;
    QrConjuntosDataAlt: TDateField;
    QrConjuntosUserCad: TIntegerField;
    QrConjuntosUserAlt: TIntegerField;
    QrConjuntosNOMEPLANO: TWideStringField;
    QrConjuntosOrdemLista: TIntegerField;
    TbGruposCodigo: TIntegerField;
    TbGruposNome: TWideStringField;
    TbGruposConjunto: TIntegerField;
    TbGruposOrdemLista: TIntegerField;
    TbGruposLk: TIntegerField;
    TbGruposDataCad: TDateField;
    TbGruposDataAlt: TDateField;
    TbGruposUserCad: TIntegerField;
    TbGruposUserAlt: TIntegerField;
    CkCtrlaSdo: TCheckBox;
    DBCheckBox1: TDBCheckBox;
    QrConjuntosCtrlaSdo: TSmallintField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGGrupos: TDBGrid;
    QrConjunSdo: TmySQLQuery;
    DsConjunSdo: TDataSource;
    QrConjunSdoNOMEENT: TWideStringField;
    QrConjunSdoCodigo: TIntegerField;
    QrConjunSdoEntidade: TIntegerField;
    QrConjunSdoSdoAtu: TFloatField;
    QrConjunSdoSdoFut: TFloatField;
    QrConjunSdoLk: TIntegerField;
    QrConjunSdoDataCad: TDateField;
    QrConjunSdoDataAlt: TDateField;
    QrConjunSdoUserCad: TIntegerField;
    QrConjunSdoUserAlt: TIntegerField;
    QrConjunSdoPerAnt: TFloatField;
    QrConjunSdoPerAtu: TFloatField;
    QrConjunSdoInfo: TSmallintField;
    dmkDBGSdo: TdmkDBGrid;
    dmkPermissoes1: TdmkPermissoes;
    EdPlano: TdmkEditCB;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtNivelAcima: TBitBtn;
    SBGrupo: TSpeedButton;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    QrConjuntosNome2: TWideStringField;
    EdNome: TdmkEdit;
    EdNome2: TdmkEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrConjuntosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrConjuntosAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrConjuntosBeforeOpen(DataSet: TDataSet);
    procedure TbGruposDeleting(Sender: TObject; var Allow: Boolean);
    procedure dmkDBGSdoCellClick(Column: TColumn);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure TbGruposBeforeInsert(DataSet: TDataSet);
    procedure TbGruposBeforeDelete(DataSet: TDataSet);
    procedure BtNivelAcimaClick(Sender: TObject);
    procedure SBGrupoClick(Sender: TObject);
    procedure EdNomeRedefinido(Sender: TObject);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenConjunSdo(CliInt: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmConjuntos: TFmConjuntos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnFinanceiroJan, MyDBCheck, Plano, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmConjuntos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmConjuntos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrConjuntosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmConjuntos.DefParams;
begin
  VAR_GOTOTABELA := 'Conjuntos';
  VAR_GOTOMYSQLTABLE := QrConjuntos;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT cj.*, pl.Nome NOMEPLANO');
  VAR_SQLx.Add('FROM conjuntos cj');
  VAR_SQLx.Add('LEFT JOIN plano pl ON pl.Codigo=cj.Plano');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('WHERE cj.Codigo=:P0');
  //
  VAR_SQLa.Add('WHERE cj.Nome Like :P0');
  //
end;

procedure TFmConjuntos.MostraEdicao(Mostra: Boolean; SQLType: TSQLType);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    if SQLType = stIns then
    begin
      EdNome.ValueVariant       := EmptyStr;
      EdNome2.ValueVariant      := EmptyStr;
      EdCodigo.ValueVariant     := 0;
      EdPlano.ValueVariant      := 0;
      CBPlano.KeyValue          := Null;
      EdOrdemLista.ValueVariant := 0;
      CkCtrlaSdo.Checked        := True;
    end else begin
      EdCodigo.ValueVariant     := QrConjuntosCodigo.Value;
      EdNome.ValueVariant       := QrConjuntosNome.Value;
      EdNome2.ValueVariant      := QrConjuntosNome2.Value;
      EdPlano.ValueVariant      := QrConjuntosPlano.Value;
      CBPlano.KeyValue          := QrConjuntosPlano.Value;
      EdOrdemLista.ValueVariant := IntToStr(QrConjuntosOrdemLista.Value);
      CkCtrlaSdo.Checked        := Geral.IntToBool_0(QrConjuntosCtrlaSdo.Value);
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmConjuntos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmConjuntos.AlteraRegistro;
var
  Conjuntos : Integer;
begin
  Conjuntos := QrConjuntosCodigo.Value;
  if (QrConjuntos.State = dsInactive) or (QrConjuntos.RecordCount = 0) or
    (QrConjuntosCodigo.Value <= 0) then
    Exit;
  //
  if not UMyMod.SelLockY(Conjuntos, Dmod.MyDB, 'Conjuntos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Conjuntos, Dmod.MyDB, 'Conjuntos', 'Codigo');
      MostraEdicao(True, stUpd);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmConjuntos.IncluiRegistro;
var
  Cursor : TCursor;
  //Conjuntos : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    {
    Conjuntos := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Conjuntos', 'Conjunto', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Conjuntos))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro(
      'Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    }
    MostraEdicao(True, stIns);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmConjuntos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmConjuntos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmConjuntos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmConjuntos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmConjuntos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmConjuntos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmConjuntos.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmConjuntos.BtNivelAcimaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPlano, FmPlano, afmoNegarComAviso) then
  begin
    FmPlano.ShowModal;
    FmPlano.Destroy;
    //
    QrPlano.Close;
    UnDmkDAC_PF.AbreQuery(QrPlano, Dmod.MyDB);
    DefParams;
  end;
end;

procedure TFmConjuntos.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmConjuntos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrConjuntosCodigo.Value;
  Close;
end;

procedure TFmConjuntos.BtConfirmaClick(Sender: TObject);
var
  Nome, Nome2: String;
  Codigo, OrdemLista, Plano, CtrlaSdo(*, NotPrntFin*): Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  Nome2          := EdNome2.Text;
  OrdemLista     := EdOrdemLista.ValueVariant;
  Plano          := EdPlano.ValueVariant;
  CtrlaSdo       := Geral.BoolToInt(CkCtrlaSdo.Checked);
  //NotPrntFin     := ;
  //
  if MyObjects.FIC(Trim(Nome) = EmptyStr, EdNome, 'Informe a descri��o!') then Exit;
  if MyObjects.FIC(Trim(Nome2) = EmptyStr, EdNome2, 'Informe a descri��o substituta!') then Exit;
  //
  if SQLType = stIns then
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Conjuntos', 'Conjunto', 'Codigo');
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'conjuntos', False, [
  'Nome', 'Nome2',
  'OrdemLista', 'Plano',
  'CtrlaSdo'(*, 'NotPrntFin'*)], [
  'Codigo'], [
  Nome, Nome2,
  OrdemLista, Plano,
  CtrlaSdo(*, NotPrntFin*)], [
  Codigo], True) then
(*
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO conjuntos SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE conjuntos SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Plano=:P1, OrdemLista=:P2, CtrlaSdo=:P3, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Geral.IMV(EdPlano.Text);
  Dmod.QrUpdU.Params[02].AsInteger := Geral.IMV(EdOrdemLista.Text);
  Dmod.QrUpdU.Params[03].AsInteger := Geral.BoolToInt(CkCtrlaSdo.Checked);
  //
  Dmod.QrUpdU.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[05].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[06].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
*)
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Conjuntos', 'Codigo');
    MostraEdicao(False, stLok);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmConjuntos.BtDesisteClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Conjuntos', 'Codigo');
  MostraEdicao(False, stLok);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Conjuntos', 'Codigo');
end;

procedure TFmConjuntos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 0;
  PainelEdita.Align  := alClient;
  PainelDados.Align  := alClient;
  PainelEdit.Align   := alClient;
  PageControl1.Align := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreTable(TbGrupos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPlano, Dmod.MyDB);
end;

procedure TFmConjuntos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrConjuntosCodigo.Value,LaRegistro.Caption);
end;

procedure TFmConjuntos.SBGrupoClick(Sender: TObject);
var
  Plano: Integer;
begin
  Plano        := EdPlano.ValueVariant;
  VAR_CADASTRO := 0;
  //
  FinanceiroJan.CadastroDePlano(Plano);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPlano, CBPlano, QrPlano, VAR_CADASTRO);
    //
    EdPlano.SetFocus;
  end;
end;

procedure TFmConjuntos.SbImprimeClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

procedure TFmConjuntos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmConjuntos.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmConjuntos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmConjuntos.QrConjuntosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmConjuntos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Conjuntos', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmConjuntos.QrConjuntosAfterScroll(DataSet: TDataSet);
begin
  TbGrupos.Filter := 'Conjunto='+IntToStr(QrConjuntosCodigo.Value);
  ReopenConjunSdo(0);
end;

procedure TFmConjuntos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrConjuntosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Conjuntos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmConjuntos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmConjuntos.QrConjuntosBeforeOpen(DataSet: TDataSet);
begin
  QrConjuntosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmConjuntos.TbGruposBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmConjuntos.TbGruposBeforeInsert(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmConjuntos.TbGruposDeleting(Sender: TObject;
  var Allow: Boolean);
begin
  Allow := False;
  Geral.MB_Aviso(
  'Para excluir grupo de conta acesse o cadastro de grupos!');
end;

procedure TFmConjuntos.ReopenConjunSdo(CliInt: Integer);
begin
  QrConjunSdo.Close;
  QrConjunSdo.Params[0].AsInteger := QrConjuntosCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrConjunSdo, Dmod.MyDB);
  //
  QrConjunSdo.Locate('Entidade', CliInt, []);
end;

procedure TFmConjuntos.dmkDBGSdoCellClick(Column: TColumn);
var
  SQLType, Entidade: Integer;
begin
  if Column.FieldName = 'Info' then
  begin
    SQLType := QrConjunSdoInfo.Value;
    if SQLType = 0 then SQLType := 1 else SQLType := 0;
    Entidade := QrConjunSdoEntidade.Value;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE conjunsdo SET Info=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Entidade=:P1 AND Codigo=:P2');
    Dmod.QrUpd.Params[00].AsInteger := SQLType;
    Dmod.QrUpd.Params[01].AsInteger := Entidade;
    Dmod.QrUpd.Params[02].AsInteger := QrConjuntosCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenConjunSdo(Entidade);
  end;
end;

procedure TFmConjuntos.EdNomeRedefinido(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    if EdNome2.ValueVariant = EmptyStr then
      EdNome2.ValueVariant := EdNome.ValueVariant;
end;

end.

