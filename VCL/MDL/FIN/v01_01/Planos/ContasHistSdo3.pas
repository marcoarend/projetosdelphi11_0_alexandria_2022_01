unit ContasHistSdo3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, Grids, DBGrids, dmkDBGrid,
  Db, mySQLDbTables, dmkEdit, DBCtrls, frxClass, frxDBSet, dmkDBGridDAC, Menus,
  Variants, mySQLDirectQuery, dmkPermissoes, dmkEditCB, dmkDBLookupComboBox,
  dmkGeral, dmkImage, UnDmkEnums;

type
  TFmContasHistSdo3 = class(TForm)
    Panel1: TPanel;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrPerPla: TmySQLQuery;
    DsPerPla: TDataSource;
    Panel4: TPanel;
    EdCliInt: TdmkEditCB;
    LaCliInt: TLabel;
    CBCliInt: TdmkDBLookupComboBox;
    QrCliIntEntidade: TIntegerField;
    QrCliIntNOMECLIINT: TWideStringField;
    QrPerPlaNOME: TWideStringField;
    QrPerPlaSDOANT: TFloatField;
    QrPerPlaSUMCRE: TFloatField;
    QrPerPlaSUMDEB: TFloatField;
    QrPerPlaSDOFIM: TFloatField;
    QrPerPlaSUMMOV: TFloatField;
    BtAtualiza: TBitBtn;
    QrPerCjt: TmySQLQuery;
    DsPerCjt: TDataSource;
    QrPerCjtNOME: TWideStringField;
    QrPerCjtSDOANT: TFloatField;
    QrPerCjtSUMCRE: TFloatField;
    QrPerCjtSUMDEB: TFloatField;
    QrPerCjtSDOFIM: TFloatField;
    QrPerCjtSUMMOV: TFloatField;
    QrPerGru: TmySQLQuery;
    DsPerGru: TDataSource;
    QrPerGruNOME: TWideStringField;
    QrPerGruSDOANT: TFloatField;
    QrPerGruSUMCRE: TFloatField;
    QrPerGruSUMDEB: TFloatField;
    QrPerGruSDOFIM: TFloatField;
    QrPerGruSUMMOV: TFloatField;
    QrPerCta: TmySQLQuery;
    QrPerSgr: TmySQLQuery;
    DsPerSgr: TDataSource;
    DsPerCta: TDataSource;
    QrPerSgrNOME: TWideStringField;
    QrPerSgrSDOANT: TFloatField;
    QrPerSgrSUMCRE: TFloatField;
    QrPerSgrSUMDEB: TFloatField;
    QrPerSgrSDOFIM: TFloatField;
    QrPerSgrSUMMOV: TFloatField;
    QrPerCtaNOME: TWideStringField;
    QrPerCtaSDOANT: TFloatField;
    QrPerCtaSUMCRE: TFloatField;
    QrPerCtaSUMDEB: TFloatField;
    QrPerCtaSDOFIM: TFloatField;
    QrPerCtaSUMMOV: TFloatField;
    QrPlaSdo: TmySQLQuery;
    QrPlaSdoPeriodo: TIntegerField;
    QrPlaSdoMMYYYY: TWideStringField;
    QrPlaSdoSUMMOV: TFloatField;
    QrPlaSdoSDOANT: TFloatField;
    QrPlaSdoSUMCRE: TFloatField;
    QrPlaSdoSUMDEB: TFloatField;
    QrPlaSdoSDOFIM: TFloatField;
    DsPlaSdo: TDataSource;
    QrPlanos: TmySQLQuery;
    DsPlanos: TDataSource;
    QrPlanosCodigo: TIntegerField;
    QrPlanosNome: TWideStringField;
    QrCjtSdo: TmySQLQuery;
    DsCjtSdo: TDataSource;
    QrCjutos: TmySQLQuery;
    DsCjutos: TDataSource;
    QrGruSdo: TmySQLQuery;
    DsGruSdo: TDataSource;
    QrGrupos: TmySQLQuery;
    DsGrupos: TDataSource;
    QrSgrSdo: TmySQLQuery;
    DsSgrSdo: TDataSource;
    QrSubgru: TmySQLQuery;
    DsSubGru: TDataSource;
    QrCtaSdo: TmySQLQuery;
    DsCtaSdo: TDataSource;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrCjutosCodigo: TIntegerField;
    QrCjutosNome: TWideStringField;
    QrGruposCodigo: TIntegerField;
    QrGruposNome: TWideStringField;
    QrSubgruCodigo: TIntegerField;
    QrSubgruNome: TWideStringField;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrCjtSdoPeriodo: TIntegerField;
    QrCjtSdoMMYYYY: TWideStringField;
    QrCjtSdoSUMMOV: TFloatField;
    QrCjtSdoSDOANT: TFloatField;
    QrCjtSdoSUMCRE: TFloatField;
    QrCjtSdoSUMDEB: TFloatField;
    QrCjtSdoSDOFIM: TFloatField;
    QrGruSdoPeriodo: TIntegerField;
    QrGruSdoMMYYYY: TWideStringField;
    QrGruSdoSUMMOV: TFloatField;
    QrGruSdoSDOANT: TFloatField;
    QrGruSdoSUMCRE: TFloatField;
    QrGruSdoSUMDEB: TFloatField;
    QrGruSdoSDOFIM: TFloatField;
    QrSgrSdoPeriodo: TIntegerField;
    QrSgrSdoMMYYYY: TWideStringField;
    QrSgrSdoSUMMOV: TFloatField;
    QrSgrSdoSDOANT: TFloatField;
    QrSgrSdoSUMCRE: TFloatField;
    QrSgrSdoSUMDEB: TFloatField;
    QrSgrSdoSDOFIM: TFloatField;
    QrCtaSdoPeriodo: TIntegerField;
    QrCtaSdoMMYYYY: TWideStringField;
    QrCtaSdoSUMMOV: TFloatField;
    QrCtaSdoSDOANT: TFloatField;
    QrCtaSdoSUMCRE: TFloatField;
    QrCtaSdoSUMDEB: TFloatField;
    QrCtaSdoSDOFIM: TFloatField;
    QrSdoIni: TmySQLQuery;
    DsSdoIni: TDataSource;
    QrSdoIniCodigo: TIntegerField;
    QrSdoIniSdoIni: TFloatField;
    QrSdoIniNOMECTA: TWideStringField;
    QrSdoIniNOMESGR: TWideStringField;
    QrSdoIniNOMEGRU: TWideStringField;
    QrSdoIniNOMECJT: TWideStringField;
    QrSdoIniNOMEPLA: TWideStringField;
    QrCjtCta: TmySQLQuery;
    QrCjtCtaNOME: TWideStringField;
    QrCjtCtaSUMMOV: TFloatField;
    QrCjtCtaSDOANT: TFloatField;
    QrCjtCtaSUMCRE: TFloatField;
    QrCjtCtaSUMDEB: TFloatField;
    QrCjtCtaSDOFIM: TFloatField;
    DsCjtCta: TDataSource;
    QrCjtCtaConta: TIntegerField;
    frxDsPerPla: TfrxDBDataset;
    frxPerPla: TfrxReport;
    PMImpNivel: TPopupMenu;
    Estenvel1: TMenuItem;
    Todosnveis1: TMenuItem;
    Generoscontrolados1: TMenuItem;
    frxSNG: TfrxReport;
    QrCliIntCliInt: TIntegerField;
    QrLCS: TmySQLQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Panel3: TPanel;
    Label16: TLabel;
    Label1: TLabel;
    EdPeriodo: TdmkEdit;
    Edit1: TEdit;
    BtImprimeP: TBitBtn;
    CkControlados: TCheckBox;
    PB1: TProgressBar;
    PB2: TProgressBar;
    PB3: TProgressBar;
    PB4: TProgressBar;
    PB5: TProgressBar;
    TabControl1: TTabControl;
    dmkDBPer: TdmkDBGrid;
    TabSheet4: TTabSheet;
    Panel5: TPanel;
    Label3: TLabel;
    EdPla: TdmkEditCB;
    CBPla: TdmkDBLookupComboBox;
    dmkDBGrid6: TdmkDBGrid;
    TabSheet5: TTabSheet;
    Panel6: TPanel;
    Label2: TLabel;
    EdCjt: TdmkEditCB;
    CBCjt: TdmkDBLookupComboBox;
    dmkDBGrid2: TdmkDBGrid;
    dmkDBGrid7: TdmkDBGrid;
    TabSheet6: TTabSheet;
    Panel7: TPanel;
    Label4: TLabel;
    EdGru: TdmkEditCB;
    CBGru: TdmkDBLookupComboBox;
    dmkDBGrid3: TdmkDBGrid;
    TabSheet7: TTabSheet;
    Panel8: TPanel;
    Label5: TLabel;
    EdSgr: TdmkEditCB;
    CBSgr: TdmkDBLookupComboBox;
    dmkDBGrid4: TdmkDBGrid;
    TabSheet8: TTabSheet;
    Panel9: TPanel;
    Label6: TLabel;
    EdCta: TdmkEditCB;
    CBCta: TdmkDBLookupComboBox;
    dmkDBGrid5: TdmkDBGrid;
    BtImprimeA: TBitBtn;
    frxLCS: TfrxReport;
    frxDsLCS: TfrxDBDataset;
    QrLCSNOMEPLANO: TWideStringField;
    QrLCSPLANO: TIntegerField;
    QrLCSNOMECONJUNTO: TWideStringField;
    QrLCSConjunto: TIntegerField;
    QrLCSNOMEGRUPO: TWideStringField;
    QrLCSGrupo: TIntegerField;
    QrLCSNOMESUBGRUPO: TWideStringField;
    QrLCSSubGrupo: TIntegerField;
    QrLCSNOMECONTA: TWideStringField;
    QrLCSGenero: TIntegerField;
    TabSheet10: TTabSheet;
    Panel10: TPanel;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdPesqCta: TEdit;
    EdPesqSgr: TEdit;
    EdPesqGru: TEdit;
    EdPesqCjt: TEdit;
    EdPesqPla: TEdit;
    GradeSdoIni: TdmkDBGrid;
    BtAltera: TBitBtn;
    QrSdoIniCODSDO: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    QrLCSValor: TFloatField;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel11: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPeriodoChange(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure EdCliIntChange(Sender: TObject);
    procedure BtAtualizaClick(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure EdPesqCtaChange(Sender: TObject);
    procedure QrCjtSdoAfterScroll(DataSet: TDataSet);
    procedure frxPerPlaGetValue(const VarName: String; var Value: Variant);
    procedure CkControladosClick(Sender: TObject);
    procedure Estenvel1Click(Sender: TObject);
    procedure Todosnveis1Click(Sender: TObject);
    procedure Generoscontrolados1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtImprimePClick(Sender: TObject);
    procedure BtImprimeAClick(Sender: TObject);
    procedure PageControl2Change(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrSdoIniBeforeClose(DataSet: TDataSet);
    procedure QrSdoIniAfterOpen(DataSet: TDataSet);
    procedure EdCliIntExit(Sender: TObject);
  private
    { Private declarations }
    FEntidade, FCliInt: Integer;
    FActive: Boolean;
    FDtEncer, FDtMorto: TDateTime;
    FTabLctA, FTabLctB, FTabLctD: String;
    function SemClienteInterno(): Boolean;
    procedure MudaPeriodo(Key: Word);
    procedure ReabrePesquisaAll;
    procedure ReabrePesquisaPer;
    procedure ReabrePesquisaPla;
    procedure ReabrePesquisaCjt;
    procedure ReabrePesquisaGru;
    procedure ReabrePesquisaSgr;
    procedure ReabrePesquisaCta;
    procedure ReabrePesquisaIni;
    procedure AtualizaEntInt();
  public
    { Public declarations }
  end;

  var
  FmContasHistSdo3: TFmContasHistSdo3;

implementation

uses UnMyObjects, Module, MyDBCheck, ModuleFin, Principal, ModuleGeral,
  UnInternalConsts, UnFinanceiroJan, DmkDAC_PF, UnDmkProcFunc;

{$R *.DFM}

procedure TFmContasHistSdo3.EdCliIntChange(Sender: TObject);
begin
  if not EdCliInt.Focused then
    AtualizaEntInt();
end;

procedure TFmContasHistSdo3.EdCliIntExit(Sender: TObject);
begin
  AtualizaEntInt();
end;

procedure TFmContasHistSdo3.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasHistSdo3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if (PageControl2.ActivePageIndex = 0) then EdPeriodo.SetFocus;
  FActive := True;
end;

procedure TFmContasHistSdo3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasHistSdo3.EdPeriodoChange(Sender: TObject);
begin
  Edit1.Text := dmkPF.MesEAnoDoPeriodoLongo(EdPeriodo.ValueVariant);
  ReabrePesquisaAll;
end;

procedure TFmContasHistSdo3.MudaPeriodo(Key: Word);
begin
  case key of
    VK_DOWN: EdPeriodo.ValueVariant := EdPeriodo.ValueVariant - 1;
    VK_UP: EdPeriodo.ValueVariant := EdPeriodo.ValueVariant + 1;
    VK_PRIOR: EdPeriodo.ValueVariant := EdPeriodo.ValueVariant - 12;
    VK_NEXT: EdPeriodo.ValueVariant := EdPeriodo.ValueVariant + 12;
  end;
end;

procedure TFmContasHistSdo3.PageControl2Change(Sender: TObject);
begin
  ReabrePesquisaAll;
end;

procedure TFmContasHistSdo3.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MudaPeriodo(Key);
end;

procedure TFmContasHistSdo3.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FActive         := False;
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  EdPeriodo.ValueVariant       := Geral.Periodo2000(Date);
  //
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPlanos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCjutos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGrupos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSubgru, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
end;

procedure TFmContasHistSdo3.ReabrePesquisaPer;
var
  Periodo, CtrlaSdo: Integer;
begin
  QrPerPla.Close;
  QrPerCjt.Close;
  QrPerGru.Close;
  QrPerSgr.Close;
  QrPerCta.Close;
  //
  if FEntidade = 0 then
    Exit;
  Periodo := EdPeriodo.ValueVariant;
  CtrlaSdo := dmkPF.BoolToInt2(CkControlados.Checked, 0, -1);
  //
  QrPerPla.Close;
  QrPerPla.Params[00].AsInteger := FEntidade;
  QrPerPla.Params[01].AsInteger := Periodo;
  QrPerPla.Params[02].AsInteger := CtrlaSdo;
  UnDmkDAC_PF.AbreQuery(QrPerPla, Dmod.MyDB);
  //
  QrPerCjt.Close;
  QrPerCjt.Params[00].AsInteger := FEntidade;
  QrPerCjt.Params[01].AsInteger := Periodo;
  QrPerCjt.Params[02].AsInteger := CtrlaSdo;
  UnDmkDAC_PF.AbreQuery(QrPerCjt, Dmod.MyDB);
  //
  QrPerGru.Close;
  QrPerGru.Params[00].AsInteger := FEntidade;
  QrPerGru.Params[01].AsInteger := Periodo;
  QrPerGru.Params[02].AsInteger := CtrlaSdo;
  UnDmkDAC_PF.AbreQuery(QrPerGru, Dmod.MyDB);
  //
  QrPerSgr.Close;
  QrPerSgr.Params[00].AsInteger := FEntidade;
  QrPerSgr.Params[01].AsInteger := Periodo;
  QrPerSgr.Params[02].AsInteger := CtrlaSdo;
  UnDmkDAC_PF.AbreQuery(QrPerSgr, Dmod.MyDB);
  //
  QrPerCta.Close;
  QrPerCta.Params[00].AsInteger := FEntidade;
  QrPerCta.Params[01].AsInteger := Periodo;
  QrPerCta.Params[02].AsInteger := CtrlaSdo;
  UnDmkDAC_PF.AbreQuery(QrPerCta, Dmod.MyDB);
end;

procedure TFmContasHistSdo3.BtAlteraClick(Sender: TObject);
var
  NewVal: String;
  Codigo: Integer;
begin
  Codigo := QrSdoIniCodigo.Value;
  NewVal := Geral.FFT(QrSdoIniSdoIni.Value, 2, siNegativo);
  if InputQuery('Novo Saldo Inicial', 'Informe o novo saldo inicial:', NewVal) then
  begin
    Dmod.QrUpd.SQL.Clear;
    if QrSdoIniCODSDO.Value <> 0 then
    begin
     Dmod.QrUpd.SQL.Add('UPDATE contassdo SET SdoIni=:P0 ');
     Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 AND Entidade=:P2 ');
    end else begin
     Dmod.QrUpd.SQL.Add('INSERT INTO contassdo SET SdoIni=:P0, ');
     Dmod.QrUpd.SQL.Add('Codigo=:P1, Entidade=:P2 ');
    end;
    Dmod.QrUpd.Params[00].AsFloat   := Geral.DMV(NewVal);
    Dmod.QrUpd.Params[01].AsInteger := Codigo;
    Dmod.QrUpd.Params[02].AsInteger := FEntidade;
    Dmod.QrUpd.ExecSQL;
    //  2012-04-25
    ReabrePesquisaAll;
    QrSdoIni.Locate('Codigo', Codigo, []);
    //  FIM - 2012-04-25
    //
    //  No novo n�o precisa!!?? Faz na hora??!!
    {
    if DBCheck.CriaFm(TFmContasHistAtz2, FmContasHistAtz2, afmoNegarComAviso) then
    begin
      FmContasHistAtz2.FEntiCod  := QrCliIntEntidade.Value;
      FmContasHistAtz2.FGenero   := QrSdoIniCodigo.Value;
      FmContasHistAtz2.ShowModal;
      FmContasHistAtz2.Destroy;
      //
      ReabrePesquisaAll;
      QrSdoIni.Locate('Codigo', Codigo, []);
    end;
    }
  end;
end;

procedure TFmContasHistSdo3.BtAtualizaClick(Sender: TObject);
begin
  ReabrePesquisaAll;
end;

procedure TFmContasHistSdo3.BtImprimeAClick(Sender: TObject);
begin
  if SemClienteInterno() then Exit;
  //
  //if not DmodFin.ConfereSalddosIniciais_Carteiras_x_Contas(
    //QrCliIntEntidade.Value) then Exit;
  QrLCS.Close;
  QrLCS.Params[00].AsInteger := QrCliIntEntidade.Value;
  QrLCS.Params[01].AsInteger := High(Integer);
  UnDmkDAC_PF.AbreQuery(QrLCS, Dmod.MyDB);
  //
  MyObjects.frxDefineDataSets(frxLCS, [
    DModG.frxDsDono,
    frxDsLCS]);
  //
  MyObjects.frxMostra(frxLCS, 'Saldo atual do plano de contas');
end;

procedure TFmContasHistSdo3.BtImprimePClick(Sender: TObject);
begin
  //if PageControl1.ActivePageIndex = 0 then
  //begin
    if SemClienteInterno() then Exit;
    MyObjects.MostraPopUpDeBotao(PMImpNivel, BtImprimeP);
  //end else begin
    //
  //end;
end;

procedure TFmContasHistSdo3.QrSdoIniAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrSdoIni.RecordCount > 0;
end;

procedure TFmContasHistSdo3.QrSdoIniBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
end;

procedure TFmContasHistSdo3.TabControl1Change(Sender: TObject);
begin
  case TabControl1.TabIndex of
    0: dmkDBPer.DataSource := DsPerPla;
    1: dmkDBPer.DataSource := DsPerCjt;
    2: dmkDBPer.DataSource := DsPerGru;
    3: dmkDBPer.DataSource := DsPerSgr;
    4: dmkDBPer.DataSource := DsPerCta;
  end;
end;

procedure TFmContasHistSdo3.ReabrePesquisaAll;
begin
  QrPerPla.Close;
  QrPerCjt.Close;
  QrPerGru.Close;
  QrPerSgr.Close;
  QrPerCta.Close;
  //
  QrPlaSdo.Close;
  QrCjtSdo.Close;
  QrGruSdo.Close;
  QrSgrSdo.Close;
  QrCtaSdo.Close;
  //
  QrSdoIni.Close;
  //
  if not FActive then Exit;
  //
  case PageControl1.ActivePageIndex of
    0:
    begin
      case PageControl2.ActivePageIndex of
        0: ReabrePesquisaPer;
        1: ReabrePesquisaPla;
        2: ReabrePesquisaCjt;
        3: ReabrePesquisaGru;
        4: ReabrePesquisaSgr;
        5: ReabrePesquisaCta;
      end;
    end;
    2: ReabrePesquisaIni;
  end;
end;

procedure TFmContasHistSdo3.ReabrePesquisaPla;
var
  Pla: Integer;
begin
  QrPlaSdo.Close;
  //
  Pla := EdPla.ValueVariant;
  if (CBPla.KeyValue <> Null) and (FEntidade <> 0) then
  begin
    QrPlaSdo.Params[00].AsInteger := FEntidade;
    QrPlaSdo.Params[01].AsInteger := Pla;
    UnDmkDAC_PF.AbreQuery(QrPlaSdo, Dmod.MyDB);
  end;
end;

procedure TFmContasHistSdo3.ReabrePesquisaCjt;
var
  Cjt: Integer;
begin
  QrCjtSdo.Close;
  //
  Cjt := EdCjt.ValueVariant;
  if (CBCjt.KeyValue <> Null) and (FEntidade <> 0) then
  begin
    QrCjtSdo.Params[00].AsInteger := FEntidade;
    QrCjtSdo.Params[01].AsInteger := Cjt;
    UnDmkDAC_PF.AbreQuery(QrCjtSdo, Dmod.MyDB);
  end;
end;

procedure TFmContasHistSdo3.ReabrePesquisaGru;
var
  Gru: Integer;
begin
  QrGruSdo.Close;
  //
  Gru := EdGru.ValueVariant;
  if (CBGru.KeyValue <> Null) and (FEntidade <> 0) then
  begin
    QrGruSdo.Params[00].AsInteger := FEntidade;
    QrGruSdo.Params[01].AsInteger := Gru;
    UnDmkDAC_PF.AbreQuery(QrGruSdo, Dmod.MyDB);
  end;
end;

procedure TFmContasHistSdo3.ReabrePesquisaSgr;
var
  Sgr: Integer;
begin
  QrSgrSdo.Close;
  //
  Sgr := EdSgr.ValueVariant;
  if (CBSgr.KeyValue <> Null) and (FEntidade <> 0) then
  begin
    QrSgrSdo.Params[00].AsInteger := FEntidade;
    QrSgrSdo.Params[01].AsInteger := Sgr;
    UnDmkDAC_PF.AbreQuery(QrSgrSdo, Dmod.MyDB);
  end;
end;

procedure TFmContasHistSdo3.ReabrePesquisaCta;
var
  Cta: Integer;
begin
  QrCtaSdo.Close;
  //
  Cta := EdCta.ValueVariant;
  if (CBCta.KeyValue <> Null) and (FEntidade <> 0) then
  begin
    QrCtaSdo.Params[00].AsInteger := FEntidade;
    QrCtaSdo.Params[01].AsInteger := Cta;
    UnDmkDAC_PF.AbreQuery(QrCtaSdo, Dmod.MyDB);
  end;
end;

procedure TFmContasHistSdo3.EdPesqCtaChange(Sender: TObject);
begin
  ReabrePesquisaAll;
end;

procedure TFmContasHistSdo3.ReabrePesquisaIni;
begin
  QrSdoIni.Close;
  //
  if (FEntidade <> 0) then
  begin
    QrSdoIni.Close;
    QrSdoIni.SQL.Clear;
    QrSdoIni.SQL.Add('SELECT cta.Codigo, sdo.Codigo CODSDO, ');
    QrSdoIni.SQL.Add('sdo.SdoIni, cta.Nome NOMECTA,');
    QrSdoIni.SQL.Add('sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
    QrSdoIni.SQL.Add('cjt.Nome NOMECJT, pla.Nome NOMEPLA');
    QrSdoIni.SQL.Add('');
    QrSdoIni.SQL.Add('FROM contas cta');
    QrSdoIni.SQL.Add('LEFT JOIN contassdo sdo ON cta.Codigo=sdo.Codigo ');
    QrSdoIni.SQL.Add('  AND sdo.Entidade=' + FormatFloat('0', FEntidade));
    QrSdoIni.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo');
    QrSdoIni.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
    QrSdoIni.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrSdoIni.SQL.Add('LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano');
    QrSdoIni.SQL.Add('WHERE cta.Codigo<>0');
    QrSdoIni.SQL.Add('');
    if Trim(EdPesqCta.Text) <> '' then
      QrSdoIni.SQL.Add('AND cta.Nome LIKE "%' + EdPesqCta.Text + '%"');
    if Trim(EdPesqSgr.Text) <> '' then
      QrSdoIni.SQL.Add('AND Sgr.Nome LIKE "%' + EdPesqSgr.Text + '%"');
    if Trim(EdPesqGru.Text) <> '' then
      QrSdoIni.SQL.Add('AND Gru.Nome LIKE "%' + EdPesqGru.Text + '%"');
    if Trim(EdPesqCjt.Text) <> '' then
      QrSdoIni.SQL.Add('AND Cjt.Nome LIKE "%' + EdPesqCjt.Text + '%"');
    if Trim(EdPesqPla.Text) <> '' then
      QrSdoIni.SQL.Add('AND Pla.Nome LIKE "%' + EdPesqPla.Text + '%"');
    QrSdoIni.SQL.Add('');
    QrSdoIni.SQL.Add('ORDER BY cta.Nome');
    QrSdoIni.SQL.Add('');
    UnDmkDAC_PF.AbreQuery(QrSdoIni, Dmod.MyDB);
  end;
end;

procedure TFmContasHistSdo3.QrCjtSdoAfterScroll(DataSet: TDataSet);
begin
  QrCjtCta.Close;
  QrCjtCta.Params[00].AsInteger := FEntidade;
  QrCjtCta.Params[01].AsInteger := QrCjtSdoPeriodo.Value;
  QrCjtCta.Params[02].AsInteger := Geral.IMV(EdCjt.Text);
  UnDmkDAC_PF.AbreQuery(QrCjtCta, Dmod.MyDB);
end;

procedure TFmContasHistSdo3.frxPerPlaGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then
    Value := Edit1.Text
  else if AnsiCompareText(VarName, 'VARF_CLIINT') = 0 then
    Value := CBCliInt.Text
  else if AnsiCompareText(VarName, 'VARF_NIVEL') = 0 then
    Value := TabControl1.Tabs[TabControl1.TabIndex]
end;

procedure TFmContasHistSdo3.CkControladosClick(Sender: TObject);
begin
  ReabrePesquisaPer;
end;

procedure TFmContasHistSdo3.Estenvel1Click(Sender: TObject);
begin
  frxDsPerPla.DataSource := dmkDBPer.DataSource;
  //
  MyObjects.frxDefineDataSets(frxPerPla, [
    DModG.frxDsDono,
    frxDsPerPla]);
  //
  MyObjects.frxMostra(frxPerPla, 'Saldos de n�vel de contas');
end;

procedure TFmContasHistSdo3.Todosnveis1Click(Sender: TObject);
begin
  if SemClienteInterno() then Exit;
  //
  DmodFin.ConfereSalddosIniciais_Carteiras_x_Contas(FEntidade);
  DModFin.AtualizaContasHistSdo3(FEntidade, LaAviso1, LaAviso2,
    DModG.MezLastEncerr(), FTabLctA);
  //
  QrLCS.Close;
  QrLCS.Params[00].AsInteger := QrCliIntEntidade.Value;
  QrLCS.Params[01].AsInteger := dmkPF.PeriodoToAnoMes(EdPeriodo.ValueVariant);
  UnDmkDAC_PF.AbreQuery(QrLCS, Dmod.MyDB);
  //
  MyObjects.frxDefineDataSets(frxLCS, [
    DModG.frxDsDono,
    frxDsLCS]);
  //
  MyObjects.frxMostra(frxLCS, 'Saldo de per�odo do plano de contas');
end;

procedure TFmContasHistSdo3.Generoscontrolados1Click(Sender: TObject);
begin
  if DmodFin.SaldosNiveisPeriodo_2(QrCliIntEntidade.Value,
    EdPeriodo.ValueVariant, dmkPF.BoolToInt2(CkControlados.Checked, 0, -1),
    False, PB1, PB2, PB3, PB4, PB5, DmodFin.QrSNG) then
  begin
    MyObjects.frxDefineDataSets(frxSNG, [
      DModG.frxDsDono,
      DModFin.frxDsSNG,
      DModFin.frxDsSTCP]);
    //
    MyObjects.frxMostra(frxSNG, 'Saldos dos N�veis do Plano de Contas');
  end;
end;

procedure TFmContasHistSdo3.AtualizaEntInt();
begin
  if Geral.IMV(EdCliInt.Text) = 0 then
  begin
    FEntidade := 0;
    FCliInt   := 0;
  end
  else begin
    FEntidade := QrCliIntEntidade.Value;
    FCliInt   := QrCliIntCliInt.Value;
    DModG.Def_EM_ABD(TMeuDB, FEntidade, FCliInt, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
    ReabrePesquisaAll;
  end;
  BtAtualiza.Enabled   := FEntidade <> 0;
  PageControl1.Visible := FEntidade <> 0;
end;

procedure TFmContasHistSdo3.BitBtn1Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeContasNiv;
end;

function TFmContasHistSdo3.SemClienteInterno(): Boolean;
begin
  AtualizaEntInt();
  if FEntidade = 0 then
  begin
    Geral.MB_Aviso('Informe o cliente interno!');
    Result := True;
  end else Result := False;
end;

end.


