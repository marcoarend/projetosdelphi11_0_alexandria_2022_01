unit Contas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  ZCF2, ResIntStrings, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UnGOTOy, UnMySQLCuringa, UMySQLModule, mySQLDbTables,
  UnMyLinguas, Grids, DBGrids, ComCtrls, dmkDBGridDAC, Variants, dmkCheckGroup,
  dmkPermissoes, dmkDBGrid, dmkGeral, dmkEdit, dmkEditCB, dmkDBLookupComboBox,
  UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmContas = class(TForm)
    PainelDados: TPanel;
    QrContas: TMySQLQuery;
    DsContas: TDataSource;
    QrLoc: TMySQLQuery;
    QrLocGenero: TIntegerField;
    QrSubGrupos: TMySQLQuery;
    DsSubGrupos: TDataSource;
    QrSubGruposCodigo: TIntegerField;
    QrSubGruposNome: TWideStringField;
    QrEmpresas: TMySQLQuery;
    DsEpresas: TDataSource;
    QrEmpresasCodigo: TIntegerField;
    QrEmpresasNOMEENTIDADE: TWideStringField;
    QrTerceiros: TMySQLQuery;
    DsTerceiros: TDataSource;
    QrTerceirosCodigo: TIntegerField;
    QrTerceirosNOMEENTIDADE: TWideStringField;
    PainelEdita: TPanel;
    Panel2: TPanel;
    Label14: TLabel;
    Label15: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label19: TLabel;
    EdNome2: TdmkEdit;
    CBSubgrupo: TdmkDBLookupComboBox;
    Label17: TLabel;
    Panel4: TPanel;
    QrCentroCusto: TmySQLQuery;
    DsCentroCusto: TDataSource;
    QrCentroCustoCodigo: TIntegerField;
    QrCentroCustoNome: TWideStringField;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TIntegerField;
    QrContasUserAlt: TIntegerField;
    QrContasCentroCusto: TIntegerField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMECENTROCUSTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    QrContasNOMETERCEIRO: TWideStringField;
    QrContasRateio: TIntegerField;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    EdSubgrupo: TdmkEditCB;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTIDADE: TWideStringField;
    QrContasEntidade: TIntegerField;
    QrContasNOMEENTIDADE: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    DBCBDebito: TDBCheckBox;
    DBCBCredito: TDBCheckBox;
    DBCBMensal: TDBCheckBox;
    DBCBExclusivo: TDBCheckBox;
    DBCBAtivo: TDBCheckBox;
    Label3: TLabel;
    DBEdNome2: TDBEdit;
    Label13: TLabel;
    DBEdNome3: TDBEdit;
    Label4: TLabel;
    DBEdEmpresa: TDBEdit;
    Label31: TLabel;
    EdDBEntidade: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdSubgrupo: TDBEdit;
    LaSubGrupo: TLabel;
    Label28: TLabel;
    Label9: TLabel;
    DbEdTerceiro: TDBEdit;
    DBEdExcel: TDBEdit;
    Label10: TLabel;
    LaIDFat: TLabel;
    DBRGRateio: TDBRadioGroup;
    DBEdID: TDBEdit;
    TabSheet3: TTabSheet;
    QrContasIts: TmySQLQuery;
    DsContasIts: TDataSource;
    QrContasItsCodigo: TIntegerField;
    QrContasItsPeriodo: TIntegerField;
    QrContasItsTipo: TIntegerField;
    QrContasItsFator: TFloatField;
    QrContasItsLk: TIntegerField;
    QrContasItsDataCad: TDateField;
    QrContasItsDataAlt: TDateField;
    QrContasItsUserCad: TIntegerField;
    QrContasItsUserAlt: TIntegerField;
    Panel6: TPanel;
    DBGrid1: TDBGrid;
    Panel7: TPanel;
    BtInsere: TBitBtn;
    BtExcluiIts: TBitBtn;
    Panel8: TPanel;
    Panel9: TPanel;
    BtConfirma2: TBitBtn;
    BtDesiste2: TBitBtn;
    QrContasItsNOMEPERIODO: TWideStringField;
    QrContasItsESTIPULADO: TWideStringField;
    Panel10: TPanel;
    GroupBox1: TGroupBox;
    Label32: TLabel;
    Label33: TLabel;
    CBMesI: TComboBox;
    CBAnoI: TComboBox;
    GroupBox2: TGroupBox;
    Label34: TLabel;
    Label35: TLabel;
    CBMesF: TComboBox;
    CBAnoF: TComboBox;
    RGTipo: TRadioGroup;
    Label36: TLabel;
    EdFator: TdmkEdit;
    Panel11: TPanel;
    Panel12: TPanel;
    BtIncluiIts: TBitBtn;
    BtExclui2: TBitBtn;
    DBGrid2: TDBGrid;
    QrCtas: TmySQLQuery;
    DsCtas: TDataSource;
    QrLoc2: TmySQLQuery;
    QrLoc2Codigo: TIntegerField;
    QrLoc2Nome: TWideStringField;
    QrCtasConta: TIntegerField;
    QrCtasNome: TWideStringField;
    DBGrid3: TDBGrid;
    QrContasItsCtas: TmySQLQuery;
    DsContasItsCtas: TDataSource;
    QrContasItsCtasCodigo: TIntegerField;
    QrContasItsCtasPeriodo: TIntegerField;
    QrContasItsCtasConta: TIntegerField;
    QrContasItsCtasLk: TIntegerField;
    QrContasItsCtasDataCad: TDateField;
    QrContasItsCtasDataAlt: TDateField;
    QrContasItsCtasUserCad: TIntegerField;
    QrContasItsCtasUserAlt: TIntegerField;
    QrContasItsCtasNOMECONTA: TWideStringField;
    Label37: TLabel;
    Label38: TLabel;
    EdDBCodigo: TDBEdit;
    EdDBDescricao: TDBEdit;
    Panel13: TPanel;
    Label40: TLabel;
    EdLocate: TdmkEdit;
    DBGrid4: TDBGrid;
    QrLocate: TmySQLQuery;
    DsLocate: TDataSource;
    QrLocateCodigo: TIntegerField;
    QrLocateNome: TWideStringField;
    QrLocateNome2: TWideStringField;
    QrLocateNome3: TWideStringField;
    QrLocateID: TWideStringField;
    QrLocateSubgrupo: TIntegerField;
    QrLocateEmpresa: TIntegerField;
    QrLocateCredito: TWideStringField;
    QrLocateDebito: TWideStringField;
    QrLocateMensal: TWideStringField;
    QrLocateExclusivo: TWideStringField;
    QrLocateMensdia: TSmallintField;
    QrLocateMensdeb: TFloatField;
    QrLocateMensmind: TFloatField;
    QrLocateMenscred: TFloatField;
    QrLocateMensminc: TFloatField;
    QrLocateLk: TIntegerField;
    QrLocateTerceiro: TIntegerField;
    QrLocateExcel: TWideStringField;
    QrLocateDataCad: TDateField;
    QrLocateDataAlt: TDateField;
    QrLocateUserCad: TIntegerField;
    QrLocateUserAlt: TIntegerField;
    QrLocateCentroCusto: TIntegerField;
    QrLocateRateio: TIntegerField;
    QrLocateEntidade: TIntegerField;
    QrLocateNOMESUBGRUPO: TWideStringField;
    QrLocateNOMECENTROCUSTO: TWideStringField;
    QrLocateNOMEEMPRESA: TWideStringField;
    QrLocateNOMETERCEIRO: TWideStringField;
    QrLocateNOMEENTIDADE: TWideStringField;
    TabSheet4: TTabSheet;
    Panel14: TPanel;
    BtReciclo: TBitBtn;
    Label39: TLabel;
    EdNova: TdmkEditCB;
    CBNova: TdmkDBLookupComboBox;
    QrNova: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsNova: TDataSource;
    DsLct: TDataSource;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctLk: TIntegerField;
    QrLctFatID: TIntegerField;
    QrLctFatParcela: TIntegerField;
    QrLctCONTA: TIntegerField;
    QrLctNOMECONTA: TWideStringField;
    QrLctNOMEEMPRESA: TWideStringField;
    QrLctNOMESUBGRUPO: TWideStringField;
    QrLctNOMEGRUPO: TWideStringField;
    QrLctNOMECONJUNTO: TWideStringField;
    QrLctNOMESIT: TWideStringField;
    QrLctAno: TFloatField;
    QrLctMENSAL: TWideStringField;
    QrLctMENSAL2: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctLocal: TIntegerField;
    QrLctFatura: TWideStringField;
    QrLctSub: TSmallintField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctPago: TFloatField;
    QrLctSALDO: TFloatField;
    QrLctID_Sub: TSmallintField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctcliente: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctNOMECLIENTE: TWideStringField;
    QrLctNOMEFORNECEDOR: TWideStringField;
    QrLctTIPOEM: TWideStringField;
    QrLctNOMERELACIONADO: TWideStringField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctMulta: TFloatField;
    QrLctATRASO: TFloatField;
    QrLctJUROS: TFloatField;
    QrLctDataDoc: TDateField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctMes2: TLargeintField;
    QrLctProtesto: TDateField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TSmallintField;
    QrLctUserAlt: TSmallintField;
    QrLctControle: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctCtrlIni: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctCOMPENSADO_TXT: TWideStringField;
    QrLctCliInt: TIntegerField;
    QrLctDepto: TIntegerField;
    QrLctDescoPor: TIntegerField;
    QrLctPrazo: TSmallintField;
    DBGLct: TDBGrid;
    BtAlteraIts: TBitBtn;
    DBCkCalculMesSeg: TDBCheckBox;
    QrContasAntigo: TWideStringField;
    QrContasPendenMesSeg: TSmallintField;
    QrContasCalculMesSeg: TSmallintField;
    GroupBox4: TGroupBox;
    LaSaldo: TLabel;
    DBEdMensDia: TDBEdit;
    Label5: TLabel;
    DBEdMensdeb: TDBEdit;
    DBEdmensmind: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    DBEdMenscred: TDBEdit;
    DBEdMensminc: TDBEdit;
    Label8: TLabel;
    DBRadioGroup1: TDBRadioGroup;
    QrContasAgr: TmySQLQuery;
    DsContasAgr: TDataSource;
    QrContasAgrCodigo: TIntegerField;
    QrContasAgrNome: TWideStringField;
    QrContasOrdemLista: TIntegerField;
    QrContasContasAgr: TIntegerField;
    QrContasContasSum: TIntegerField;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    Panel15: TPanel;
    CBMensal: TCheckBox;
    TabSheet6: TTabSheet;
    Panel16: TPanel;
    Label41: TLabel;
    EdContasAgr: TdmkEditCB;
    CBContasAgr: TdmkDBLookupComboBox;
    TabSheet7: TTabSheet;
    Label18: TLabel;
    EdID: TdmkEdit;
    CkContasSum: TCheckBox;
    DBCheckBox1: TDBCheckBox;
    QrContasCtrlaSdo: TSmallintField;
    QrContasAtivo: TSmallintField;
    DsEnti2: TDataSource;
    QrEnti2: TmySQLQuery;
    IntegerField2: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    TabSheet8: TTabSheet;
    Panel18: TPanel;
    Label43: TLabel;
    EdEnti2: TdmkEditCB;
    CBEnti2: TdmkDBLookupComboBox;
    Panel17: TPanel;
    Label44: TLabel;
    Label45: TLabel;
    EdDBCodigo2: TDBEdit;
    EdDBDescricao2: TDBEdit;
    Label42: TLabel;
    EdCntrDebCta: TdmkEdit;
    Panel20: TPanel;
    BitBtn1: TBitBtn;
    BitBtn5: TBitBtn;
    DBGrid5: TDBGrid;
    Panel21: TPanel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn2: TBitBtn;
    QrContasEnt: TmySQLQuery;
    DsContasEnt: TDataSource;
    QrContasEntNOMEENT: TWideStringField;
    QrContasEntCodigo: TIntegerField;
    QrContasEntEntidade: TIntegerField;
    QrContasEntCntrDebCta: TWideStringField;
    QrContasEntLk: TIntegerField;
    QrContasEntDataCad: TDateField;
    QrContasEntDataAlt: TDateField;
    QrContasEntUserCad: TIntegerField;
    QrContasEntUserAlt: TIntegerField;
    QrContasEntAlterWeb: TSmallintField;
    QrContasEntAtivo: TSmallintField;
    TabSheet9: TTabSheet;
    Panel22: TPanel;
    Label46: TLabel;
    Label47: TLabel;
    EdDBCodigo3: TDBEdit;
    EdDBDescricao3: TDBEdit;
    Panel23: TPanel;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    GradeContasMes: TdmkDBGrid;
    QrContasMes: TmySQLQuery;
    DsContasMes: TDataSource;
    QrLctFatNum: TFloatField;
    QrContasNotPrntBal: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    EdSigla: TdmkEdit;
    Label11: TLabel;
    QrContasAlterWeb: TSmallintField;
    QrContasSigla: TWideStringField;
    Panel24: TPanel;
    Label20: TLabel;
    EdEmpresa: TdmkEditCB;
    Label29: TLabel;
    EdCentroCusto: TdmkEditCB;
    CBCentroCusto: TdmkDBLookupComboBox;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    Label30: TLabel;
    CBEntidade: TdmkDBLookupComboBox;
    Label25: TLabel;
    Label26: TLabel;
    EdExcel: TdmkEdit;
    CBTerceiro: TDBLookupComboBox;
    RGRateio: TRadioGroup;
    CGNotPrntBal: TdmkCheckGroup;
    QrContasMesCodigo: TIntegerField;
    QrContasMesControle: TIntegerField;
    QrContasMesCliInt: TIntegerField;
    QrContasMesDescricao: TWideStringField;
    QrContasMesPeriodoIni: TIntegerField;
    QrContasMesPeriodoFim: TIntegerField;
    QrContasMesValorMin: TFloatField;
    QrContasMesValorMax: TFloatField;
    QrContasMesPERIODOINI_TXT: TWideStringField;
    QrContasMesPERIODOFIM_TXT: TWideStringField;
    QrContasMesNO_CliInt: TWideStringField;
    Label12: TLabel;
    EdCodExporta: TdmkEdit;
    QrContasEntCodExporta: TWideStringField;
    QrContasMesQtdeMin: TIntegerField;
    QrContasMesQtdeMax: TIntegerField;
    QrContasMesDiaAlert: TSmallintField;
    QrContasMesDiaVence: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel25: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel19: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel26: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtNivelAcima: TBitBtn;
    CGNotPrntFin: TdmkCheckGroup;
    QrContasNotPrntFin: TIntegerField;
    DBCheckBox2: TDBCheckBox;
    QrContasTemDocFisi: TSmallintField;
    QrContasProvRat: TSmallintField;
    QrContasCentroRes: TIntegerField;
    TabSheet10: TTabSheet;
    Panel27: TPanel;
    DBRadioGroup2: TDBRadioGroup;
    QrContasPagRec: TSmallintField;
    Label48: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label49: TLabel;
    DBRadioGroup3: TDBRadioGroup;
    QrCROCon: TmySQLQuery;
    DsCROCon: TDataSource;
    QrCROConCodigo: TIntegerField;
    QrCROConNome: TWideStringField;
    TabSheet11: TTabSheet;
    Panel28: TPanel;
    GroupBox5: TGroupBox;
    Panel29: TPanel;
    Label50: TLabel;
    EdCROCon: TdmkEditCB;
    CBCroCon: TdmkDBLookupComboBox;
    QrSubGruposTipoAgrupa: TIntegerField;
    SBSubgrupo: TSpeedButton;
    TabSheet12: TTabSheet;
    Panel30: TPanel;
    Label27: TLabel;
    EdNome3: TdmkEdit;
    CBExclusivo: TCheckBox;
    CkTemDocFisi: TCheckBox;
    CBDebito: TCheckBox;
    CBCredito: TCheckBox;
    CBAtivo: TCheckBox;
    CkCtrlaSdo: TCheckBox;
    RGCentroRes: TRadioGroup;
    GroupBox3: TGroupBox;
    Label16: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    EdMensDia: TdmkEdit;
    EdMensdeb: TdmkEdit;
    Edmensmind: TdmkEdit;
    EdMenscred: TdmkEdit;
    EdMensminc: TdmkEdit;
    CkCalculMesSeg: TCheckBox;
    RGPendenMesSeg: TRadioGroup;
    Grade: TStringGrid;
    CkUsoNoERP: TCheckBox;
    QrContasUsoNoERP: TSmallintField;
    DBCheckBox3: TDBCheckBox;
    EdSPED_COD_CTA_REF: TdmkEdit;
    Label51: TLabel;
    QrContasSPED_COD_CTA_REF: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure EdMensDiaExit(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure CBMensalClick(Sender: TObject);
    procedure DBCBMensalClick(Sender: TObject);
    procedure QrContasAfterScroll(DataSet: TDataSet);
    procedure EdNomeExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtNivelAcimaClick(Sender: TObject);
    procedure BtInsereClick(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure BtDesiste2Click(Sender: TObject);
    procedure QrContasItsCalcFields(DataSet: TDataSet);
    procedure BtExcluiItsClick(Sender: TObject);
    procedure BtIncluiItsClick(Sender: TObject);
    procedure BtExclui2Click(Sender: TObject);
    procedure QrContasItsAfterScroll(DataSet: TDataSet);
    procedure QrContasItsBeforeClose(DataSet: TDataSet);
    procedure QrContasBeforeClose(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure DBGrid4DblClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure EdLocateChange(Sender: TObject);
    procedure EdLocateKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtRecicloClick(Sender: TObject);
    procedure EdNovaChange(Sender: TObject);
    procedure BtAlteraItsClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure QrContasMesCalcFields(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure SBSubgrupoClick(Sender: TObject);
    procedure EdSubgrupoChange(Sender: TObject);
  private
    FLocLocate: Boolean;
    procedure ExcluiContaIts;
    procedure ReopenContasIts(Periodo: Integer);
    procedure ReopenContasEnt(Entidade: Integer);
    procedure ReopenContasItsCtas;
    procedure CriaOForm;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
    procedure VerificaMensal;
    procedure DefParams;
    //procedure TransfereLancto;
    //procedure ReopenLct();

   //Procedures do form
    procedure MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
    procedure MostraEdicaoEnt(Mostra: Integer; SQLType: TSQLType; Codigo : Integer);
    procedure MostraContasMes(SQLType: TSQLType);
    procedure ReopenLocate;
    procedure LocalizaReferencia;
  public
    { Public declarations }
    procedure Va(Para: TVaiPara);
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenContasU(Controle: Integer);
    procedure ReopenContasMes(Controle: Integer);
  end;

var
  FmContas: TFmContas;

implementation

uses UnMyObjects, Module, Subgrupos, UCreate, ContasExclui, ContasMes,
  MyDBCheck, ModuleGeral, DmkDAC_PF, UnFinanceiroJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmContas.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmContas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrContasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////
procedure TFmContas.DefParams;
begin
  VAR_GOTOTABELA := CO_CONTAS;
  VAR_GOTOMySQLTABLE := QrContas;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT co.*, sg.Nome NOMESUBGRUPO, cc.Nome NOMECENTROCUSTO,');
  VAR_SQLx.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END NOMEEMPRESA,');
  VAR_SQLx.Add('CASE WHEN te.Tipo=0 THEN te.RazaoSocial ELSE te.Nome END NOMETERCEIRO,');
  VAR_SQLx.Add('CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEENTIDADE');
  VAR_SQLx.Add('FROM contas co');
  VAR_SQLx.Add('LEFT JOIN subgrupos   sg ON sg.Codigo=co.Subgrupo');
  VAR_SQLx.Add('LEFT JOIN centrocusto cc ON cc.Codigo=co.CentroCusto');
  VAR_SQLx.Add('LEFT JOIN entidades   cl ON cl.Codigo=co.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades   te ON te.Codigo=co.Terceiro');
  VAR_SQLx.Add('LEFT JOIN entidades   en ON en.Codigo=co.Entidade');
  //
  VAR_SQL1.Add('WHERE co.Codigo=:P0');
  //
  VAR_SQLa.Add('WHERE co.Nome Like :P0');
  //
end;

procedure TFmContas.VerificaMensal;
begin
  if ((CBMensal.Checked = False) and (PainelEdita.Visible = True))
  or ((DBCBMensal.Checked = False)  and (PainelEdita.Visible = False)) then
  begin
    EdMensDia.Enabled    := False;
    EdMenscred.Enabled   := False;
    EdMensdeb.Enabled    := False;
    EdMensminc.Enabled   := False;
    EdMensmind.Enabled   := False;
    DBEdMensDia.Enabled  := False;
    DBEdMenscred.Enabled := False;
    DBEdMensdeb.Enabled  := False;
    DBEdMensminc.Enabled := False;
    DBEdMensmind.Enabled := False;
  end else begin
    EdMensDia.Enabled    := True;
    EdMenscred.Enabled   := True;
    EdMensdeb.Enabled    := True;
    EdMensminc.Enabled   := True;
    EdMensmind.Enabled   := True;
    DBEdMensDia.Enabled  := True;
    DBEdMenscred.Enabled := True;
    DBEdMensdeb.Enabled  := True;
    DBEdMensminc.Enabled := True;
    DBEdMensmind.Enabled := True;
  end;
  RGPendenMesSeg.Enabled := CBMensal.Checked;
  CkCalculMesSeg.Enabled := CBMensal.Checked;
end;

procedure TFmContas.MostraContasMes(SQLType: TSQLType);
begin
  if UMyMod.FormInsUpd_Cria(TFmContasMes, FmContasMes, afmoNegarComAviso,
    QrContasMes, SQLType) then
  begin
    FmContasMes.FControle := QrContasMesControle.Value;
    if SQLType = stIns then
    begin
      FmContasMes.EdCodigo.ValueVariant := QrContasCodigo.Value;
      FmContasMes.CBCodigo.KeyValue     := QrContasCodigo.Value;
    end;
    FmContasMes.FQuery := QrContasMes;
    FmContasMes.ShowModal;
    FmContasMes.Destroy;
  end;
end;

procedure TFmContas.MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
{
var
  Tabela: String;
  i: Integer;
}
begin
  if Mostra then
  begin
    EdNome.Text  := CO_VAZIO;
    EdNome2.Text := CO_VAZIO;
    EdNome3.Text := CO_VAZIO;
    EdSigla.Text := CO_VAZIO;
    EdID.Text    := CO_VAZIO;

    PainelEdita.Visible := True;
    PainelDados.Visible := False;

    if SQLType = stIns then
    begin
      EdCodigo.Text           := IntToStr(Codigo);
      EdMensdia.Text          := '1';
      EdEmpresa.Text          := Geral.FF0(DmodG.QrDonoCodigo.Value);
      CBEmpresa.KeyValue      := DmodG.QrDonoCodigo.Value;
      EdSubGrupo.ValueVariant := 0;
      CBSubGrupo.KeyValue     := Null;
      CkCtrlaSdo.Checked      := True;
      CGNotPrntBal.Value      := 0;
      CGNotPrntFIn.Value      := 0;
      CkTemDocFisi.Checked    := False;
      RGCentroRes.ItemIndex   := 1;
    end else begin
      EdCodigo.Text          := DBEdCodigo.Text;
      EdNome.Text            := DBEdNome.Text;
      EdNome2.Text           := DBEdNome2.Text;
      EdNome3.Text           := DBEdNome3.Text;
      EdMenscred.Text        := DBEdMenscred.Text;
      EdMensDia.Text         := DBEdMensDia.Text;
      EdMensminc.Text        := DBEdMensminc.Text;
      Edmensmind.Text        := DBEdMensmind.Text;
      EdMensdeb.Text         := DBEdMensdeb.Text;
      EdExcel.Text           := DBEdExcel.Text;
      EdID.Text              := DBEdID.Text;
      //
      EdSubgrupo.Text        := IntToStr(QrContasSubgrupo.Value);
      EdEmpresa.Text         := IntToStr(QrContasEmpresa.Value);
      EdEntidade.Text        := IntToStr(QrContasEntidade.Value);
      EdCentroCusto.Text     := IntToStr(QrContasCentroCusto.Value);
      EdContasAgr.Text       := IntToStr(QrContasContasAgr.Value);
      //
      EdSigla.Text           := QrContasSigla.Value;
      CBSubgrupo.KeyValue    := QrContasSubgrupo.Value;
      CBEmpresa.KeyValue     := QrContasEmpresa.Value;
      CBTerceiro.KeyValue    := QrContasTerceiro.Value;
      CBEntidade.KeyValue    := QrContasEntidade.Value;
      CBCentroCusto.KeyValue := QrContasCentroCusto.Value;
      CBContasAgr.KeyValue   := QrContasContasAgr.Value;
      //
      CGNotPrntBal.Value     := QrContasNotPrntBal.Value;
      CGNotPrntFin.Value     := QrContasNotPrntFin.Value;
      //
      RGRateio.ItemIndex     := QrContasRateio.Value;
      RGPendenMesSeg.ItemIndex := QrContasPendenMesSeg.Value;
      CkCalculMesSeg.Checked := Geral.IntToBool_0(QrContasCalculMesSeg.Value);
      CkContasSum.Checked    := Geral.IntToBool_0(QrContasContasSum.Value);
      CkCtrlaSdo.Checked     := Geral.IntToBool_0(QrContasCtrlaSdo.Value);
      if QrContasDebito.Value    = 'V' then CBDebito.Checked    := True else CBDebito.Checked    := False;
      if QrContasCredito.Value   = 'V' then CBCredito.Checked   := True else CBCredito.Checked   := False;
      if QrContasMensal.Value    = 'V' then CBMensal.Checked    := True else CBMensal.Checked    := False;
      if QrContasExclusivo.Value = 'V' then CBExclusivo.Checked := True else CBExclusivo.Checked := False;
      if QrContasAtivo.Value     = 0   then CBAtivo.Checked     := False else CBAtivo.Checked    := True;
      CkTemDocFisi.Checked := Geral.IntToBool(QrContasTemDocFisi.Value);
      {### ver o que fazer!
      for i := 1 to 2 do
      begin
        Tabela := VAR LCT;//if Tabela = CO_VAZIO then Tabela := VAR LCT else Tabela := CO_AJCUSTOSITS;
        QrLoc.Close;
        QrLoc.SQL.Clear;
        QrLoc.SQL.Add('SELECT Genero FROM '+lowercase(tabela)+' WHERE Genero=:P0');
        QrLoc.Params[0].AsInteger := QrContasCodigo.Value;
        UnDmkDAC_PF.AbreQuery(QrLoc,
        if GOTOy.Registros(QrLoc) > 0 then CBTerceiro.ReadOnly := True;
      end;
      QrLoc.Close;
      }
      RGCentroRes.ItemIndex := QrContasCentroRes.Value;
      CkUsoNoERP.Checked := Geral.IntToBool(QrContasUsoNoERP.Value);
      EdSPED_COD_CTA_REF.ValueVariant := QrContasSPED_COD_CTA_REF.Value;
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible:=True;
    PainelEdita.Visible:=False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmContas.CriaOForm;
begin
  DefParams;
  Va(vpLast);
end;

procedure TFmContas.AlteraRegistro;
var
  Contas : Integer;
begin
  Contas := QrContasCodigo.Value;
  if (QrContas.State = dsInactive) or (QrContas.RecordCount = 0) or
    (QrContasCodigo.Value <= 0) then
    Exit;
  //
  if not UMyMod.SelLockY(Contas, Dmod.MyDB, 'Contas', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Contas, Dmod.MyDB, 'Contas', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmContas.IncluiRegistro;
var
  Cursor : TCursor;
  Contas : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Contas := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Contas', 'Contas', 'Codigo');;
    MostraEdicao(True, stIns, Contas);
  finally
    Screen.Cursor := Cursor;
  end;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmContas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmContas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmContas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmContas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmContas.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
  ReopenLocate;
end;

procedure TFmContas.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
  ReopenLocate;
end;

procedure TFmContas.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrContasCodigo.Value;
  Close;
end;

procedure TFmContas.BtConfirmaClick(Sender: TObject);
var
  i, Terceiro, Codigo, Subgrupo, Empresa, MensDia, CentroCusto, Entidade,
  CentroRes, PagRec, CROCon, UsoNoERP: Integer;
  Nome, Nome2, Nome3, Sigla, Debito, Credito, Mensal, Exclusivo, Ativo,
  SPED_COD_CTA_REF: String;
begin
  Nome  := EdNome.Text;
  Nome2 := EdNome2.Text;
  Nome3 := EdNome3.Text;
  Sigla := EdSigla.Text;
  SPED_COD_CTA_REF := EdSPED_COD_CTA_REF.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(Nome2) = 0, EdNome2, 'Defina uma descri��o substituta!') then Exit;
  if MyObjects.FIC(Length(Sigla) = 0, EdSigla, 'Defina uma sigla!') then Exit;
  //
  if ((CBDebito.Checked = True) and (CBCredito.Checked = True)) or
    ((CBDebito.Checked = False) and (CBCredito.Checked = False)) then
  begin
    Geral.MB_Aviso('Voc� deve informar se a conta � um d�bito ou um cr�dito!');
    CBDebito.SetFocus;
    exit;
  end;
  //
  if CBTerceiro.KeyValue <> Null then
    Terceiro := CBTerceiro.KeyValue else Terceiro := 0;
  //
  Codigo := Geral.IMV(EdCodigo.Text);
  //
  if CBSubgrupo.KeyValue <> Null then
    Subgrupo := CBSubgrupo.KeyValue;
  (*
  else
  begin
   Geral.MB_Aviso('Defina um Sub-grupo.');
   CBSubgrupo.SetFocus;
   Exit;
  end;
  *)
  if Subgrupo <> 0 then
  begin
    if (QrSubGruposTipoAgrupa.Value = 0) and (CBDebito.Checked = False) then
    begin
      Geral.MB_Aviso('Para o subgrupo selecionado a conta deve ser de d�bito!');
      CBDebito.SetFocus;
      Exit;
    end;
    if (QrSubGruposTipoAgrupa.Value = 1) and (CBCredito.Checked = False) then
    begin
      Geral.MB_Aviso('Para o subgrupo selecionado a conta deve ser de cr�dito!');
      CBCredito.SetFocus;
      Exit;
    end;
  end;
  //
  if CBEmpresa.KeyValue <> Null then Empresa := CBEmpresa.KeyValue else
  begin
    PageControl2.ActivePageIndex := 3;
    //
    Geral.MB_Aviso('Defina uma Empresa!');
    //
    CBEmpresa.SetFocus;
    Exit;
  end;
  if (RGRateio.ItemIndex < 0) and (RGRateio.Items.Count > 0) then
  begin
    PageControl2.ActivePageIndex := 3;
    //
    Geral.MB_Aviso('Defina a forma de rateio!');
    //
    RGRateio.SetFocus;
    Exit;
  end;
  Entidade := Geral.IMV(EdEntidade.Text);
  //
  if (Entidade = 0) and (RGRateio.ItemIndex>-1) then
  begin
    for i := 0 to Grade.ColCount-1 do
    begin
      if Grade.Cells[i, RGRateio.ItemIndex] = 'Entidade' then
      begin
        PageControl2.ActivePageIndex := 3;
        //
        Geral.MB_Aviso('O rateio selecionado requer a escolha de uma entidade!');
        //
        EdEntidade.SetFocus;
        Exit;
      end;
    end;
  end;
  CentroRes := RGCentroRes.ItemIndex;
  //
  if (CentroRes = 0) and (Codigo > 0) then
  begin
    PageControl2.ActivePageIndex := 1;
    //
    Geral.MB_Aviso('Informe um "Centro de Resultado" diferente de "' + RGCentroRes.Items[
      RGCentroRes.ItemIndex] + '"');
    //
    RGCentroRes.SetFocus;
    Exit;
  end;
  //
  if CBCentroCusto.KeyValue <> Null then
    CentroCusto := CBCentroCusto.KeyValue
  else
    CentroCusto := 0;
  //
  if (QrCentroCusto.RecordCount > 1) and (CentroCusto = 0) then
  begin
    PageControl2.ActivePageIndex := 3;
    //
    Geral.MB_Aviso('Informe o Centro de Custo!');
    //
    EdCentroCusto.SetFocus;
    Exit;
  end;
  //
  CROCon := 0;
  //
  if VAR_USA_MODULO_CRO then
  begin
    CROCon := EdCROCon.ValueVariant;
    //
    if CROCon = 0 then
    begin
      PageControl2.ActivePageIndex := 4;
      //
      Geral.MB_Aviso('Informe a conta CRO!');
      //
      EdCROCon.SetFocus;
      Exit;
    end;
  end;
  UsoNoERP := Geral.BoolToInt(CkUsoNoERP.Checked);
  //
  MensDia := Geral.IMV(EdMensDia.Text);
  if CBDebito.Checked    = True then Debito    := 'V' else Debito := 'F';
  if CBCredito.Checked   = True then Credito   := 'V' else Credito := 'F';
  if CBMensal.Checked    = True then Mensal    := 'V' else Mensal := 'F';
  if CBExclusivo.Checked = True then Exclusivo := 'V' else Exclusivo := 'F';
  if CBAtivo.Checked     = True then Ativo     := '1' else Ativo := '0';
  if CBAtivo.Checked     = True then Ativo     := '1' else Ativo := '0';
  //
  if CBDebito.Checked then
    PagRec := -1
  else
    PagRec := 0;
  //
  if DModG.QrCtrlGeralUsarFinCtb.Value = 1 then
  begin
    if MyObjects.FIC(SPED_COD_CTA_REF = EmptyStr, EdSPED_COD_CTA_REF,
    'Defina o "C�digo da conta correlacionada no Plano de Contas Referenciado, publicado pela RFB"!')
    then ; //Exit;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'contas', False, [
    'Nome', 'Nome2', 'Nome3', 'ID',
    'Subgrupo', 'Empresa', 'MensDia', 'Credito',
    'Debito', 'Mensal', 'Exclusivo', 'Mensdeb',
    'Menscred', 'Mensmind',
    'MensMinc','Terceiro',
    'Excel', 'Ativo',
    'CentroCusto', 'Rateio', 'Entidade',
    'PendenMesSeg', 'CalculMesSeg',
    'ContasAgr', 'ContasSum',
    'CtrlaSdo', 'NotPrntBal', 'Sigla',
    'NotPrntFin', 'TemDocFisi',
    'CentroRes', 'PagRec', 'CROCon',
    'UsoNoERP', 'SPED_COD_CTA_REF'
  ], ['Codigo'], [
    Nome, Nome2, Nome3, EdID.Text,
    Subgrupo, Empresa, Mensdia, Credito,
    Debito, Mensal, Exclusivo, Geral.DMV(EdMensdeb.Text),
    Geral.DMV(EdMenscred.Text), Geral.DMV(EdMensmind.Text),
    Geral.DMV(EdMensminc.Text), Terceiro,
    EdExcel.Text, Ativo,
    CentroCusto, RGRateio.ItemIndex, Entidade,
    RGPendenMesSeg.ItemIndex, Geral.BoolToInt(CkCalculMesSeg.Checked),
    Geral.IMV(EdContasAgr.Text), Geral.BoolToInt(CkContasSum.Checked),
    Geral.BoolToInt(CkCtrlaSdo.Checked), CGNotPrntBal.Value, Sigla,
    CGNotPrntFin.Value, Geral.BoolToInt(CkTemDocFisi.Checked),
    CentroRes, PagRec, CROCon,
    UsoNoERP, SPED_COD_CTA_REF
  ], [Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Contas', 'Codigo');
    MostraEdicao(False, stLok, 0);
    LocCod(QrContasCodigo.Value, Codigo);
    ReopenLocate;
  end;
end;

procedure TFmContas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Contas', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Contas', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Contas', 'Codigo');
end;

procedure TFmContas.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  ImgTipo.SQLType := stLok;
  MyObjects.PreencheCBAnoECBMes(CBAnoI, CBMesI, -1);
  //////////////////////////////////////////////////////////////////////////
  MyObjects.PreencheCBAnoECBMes(CBAnoF, CBMesF, -1);
  //////////////////////////////////////////////////////////////////////////
  MyObjects.ConfiguracaoRateioContas(TMeuDB, DBRGRateio.Items, nil);
  MyObjects.ConfiguracaoRateioContas(TMeuDB, RGRateio.Items, Grade);
  for i := 0 to DBRGRateio.Items.Count-1 do DBRGRateio.Values.Add(IntToStr(i));
  DBRGRateio.Columns := DBRGRateio.Items.Count;
  RGRateio.Columns := DBRGRateio.Items.Count;
  //
  CriaOForm;
  Panel2.Align := alClient;
  Panel4.Align := alClient;
  Panel6.Align := alClient;
  Panel8.Align := alClient;
  UnDmkDAC_PF.AbreQuery(QrSubgrupos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEmpresas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTerceiros, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCentroCusto, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContasAgr, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEnti2, Dmod.MyDB);
  PageControl1.ActivePageIndex := 0;
  UCriar.RecriaTempTable('CtasItsCtas', DModG.QrUpdPID1, False);
  ReopenLocate;
  PageControl1.ActivePageIndex := 0;
  QrCtas.Database := DModG.MyPID_DB;
  //
  TabSheet4.TabVisible := False;
  if VAR_USA_MODULO_CRO then
  begin
{$IfNDef NAO_CRO}
    UnDmkDAC_PF.AbreQuery(QrCROCon, Dmod.MyDB);
{$Else}
    Grl_Geral.InfoSemModulo(mdlappCRO);
{$EndIf}
  end;
  PageControl2.ActivePageIndex := 0;
end;

procedure TFmContas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrContasCodigo.Value, LaRegistro.Caption);
end;

procedure TFmContas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmContas.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmContas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  try
    UCriar.RecriaTempTable('CtasItsCtas', DModG.QrUpdPID1, False);
  finally
  end;  
  VAR_CONTA      := QrContasCodigo.Value;
  VAR_CONTA_NOME := QrContasNome.Value;
  Action         := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmContas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  VerificaMensal;
end;

procedure TFmContas.EdMensDiaExit(Sender: TObject);
var
  Num: Integer;
begin
  Num := Geral.IMV(EdMensdia.Text);
  if (Num > 0) and  (Num < 32) then
    EdMensdia.Text := IntToStr(Num)
  else begin
    Geral.MB_Aviso('Dia inv�lido');
    EdMensDia.SetFocus;
  end;
end;

procedure TFmContas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrContasCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, CO_CONTAS, VAR_GOTOMySQLDBNAME, CO_VAZIO));
end;

procedure TFmContas.SBSubgrupoClick(Sender: TObject);
var
  SubGrupo: Integer;
begin
  SubGrupo     := EdSubGrupo.ValueVariant;
  VAR_CADASTRO := 0;
  //
  FinanceiroJan.CadastroDeSubGrupos(SubGrupo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdSubGrupo, CBSubGrupo, QrSubGrupos, VAR_CADASTRO);
    //
    EdSubGrupo.SetFocus;
  end;
end;

procedure TFmContas.CBMensalClick(Sender: TObject);
begin
  VerificaMensal;
end;

procedure TFmContas.DBCBMensalClick(Sender: TObject);
begin
  VerificaMensal;
end;

procedure TFmContas.QrContasAfterScroll(DataSet: TDataSet);
begin
  VerificaMensal;
  BtAltera.Enabled := GOTOy.BtEnabled(QrContasCodigo.Value, False);
  ReopenContasIts(0);
  ReopenContasEnt(0);
  ReopenContasMes(0);
  QrNova.Close;
end;

procedure TFmContas.EdNomeExit(Sender: TObject);
begin
  if EdNome2.ValueVariant = '' then
    EdNome2.ValueVariant := EdNome.ValueVariant;
  if EdNome3.ValueVariant = '' then
    EdNome3.ValueVariant := EdNome.ValueVariant;
end;

procedure TFmContas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContas.BtNivelAcimaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSubGrupos, FmSubGrupos, afmoNegarComAviso) then
  begin
    FmSubGrupos.ShowModal;
    FmSubGrupos.Destroy;
    //
    UnDmkDAC_PF.AbreQuery(QrSubGrupos, Dmod.MyDB);
    DefParams;
  end;
end;

procedure TFmContas.BtInsereClick(Sender: TObject);
begin
  Panel8.Visible := True;
  Panel6.Visible := False;
  UnDmkDAC_PF.AbreQuery(QrCtas, DModG.MyPID_DB);
end;

procedure TFmContas.BtConfirma2Click(Sender: TObject);
var
  i, PeriodoI, PeriodoF: Integer;
  Ano, Mes: Word;
  Fator: Double;
begin
  if (RGTipo.ItemIndex = 1) and (QrCtas.RecordCount=0) then
  begin
    Geral.MB_Aviso('O tipo de c�lculo por porcentagem exige '+
    'pelo menos uma conta dependente!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  Panel6.Visible := True;
  Panel8.Visible := False;
  //
  Ano := Geral.IMV(CBAnoI.Text);
  if Ano = 0 then Exit;
  Mes := CBMesI.ItemIndex + 1;
  PeriodoI := ((Ano - 2000) * 12) + Mes;
  //
  Ano := Geral.IMV(CBAnoF.Text);
  if Ano = 0 then Exit;
  Mes := CBMesF.ItemIndex + 1;
  PeriodoF := ((Ano - 2000) * 12) + Mes;
  //
  Fator := Geral.DMV(EdFator.Text);
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO contasits SET ');
  Dmod.QrUpd.SQL.Add('Tipo      =:P00, ');
  Dmod.QrUpd.SQL.Add('Fator     =:P01, ');
  Dmod.QrUpd.SQL.Add('Codigo    =:P02, ');
  Dmod.QrUpd.SQL.Add('Periodo   =:P03');
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO contasitsctas SET ');
  Dmod.QrUpdU.SQL.Add('Conta     =:P00, ');
  Dmod.QrUpdU.SQL.Add('Codigo    =:P01, ');
  Dmod.QrUpdU.SQL.Add('Periodo   =:P02');
  for i := PeriodoI to PeriodoF do
  begin
    Dmod.QrUpd.Params[00].AsInteger := RGTipo.ItemIndex;
    Dmod.QrUpd.Params[01].AsFloat   := Fator;
    Dmod.QrUpd.Params[02].AsInteger := QrContasCodigo.Value;
    Dmod.QrUpd.Params[03].AsInteger := i;
    Dmod.QrUpd.ExecSQL;
    if RGTipo.ItemIndex = 1 then
    begin
      QrCtas.First;
      while not QrCtas.Eof do
      begin
        Dmod.QrUpdU.Params[00].AsInteger := QrCtasConta.Value;
        Dmod.QrUpdU.Params[01].AsInteger := QrContasCodigo.Value;
        Dmod.QrUpdU.Params[02].AsInteger := i;
        Dmod.QrUpdU.ExecSQL;
        QrCtas.Next;
      end;
    end;
  end;
  Screen.Cursor := crDefault;
  ReopenContasIts(0);
end;

procedure TFmContas.BtDesiste2Click(Sender: TObject);
begin
  QrCtas.Close;
  Panel6.Visible := True;
  Panel8.Visible := False;
end;

procedure TFmContas.ReopenContasIts(Periodo: Integer);
begin
  QrContasIts.Close;
  QrContasIts.Params[0].AsInteger := QrContasCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrContasIts, Dmod.MyDB);
  //
  QrContasIts.Locate('Periodo', Periodo, []);
end;

procedure TFmContas.ReopenContasMes(Controle: Integer);
begin
  QrContasMes.Close;
  QrContasMes.Params[0].AsInteger := QrContasCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrContasMes, Dmod.MyDB);
  //
  QrContasMes.Locate('Controle', Controle, []);
end;

procedure TFmContas.QrContasItsCalcFields(DataSet: TDataSet);
var
  Txt: String;
begin
  QrContasItsNOMEPERIODO.Value :=
    dmkPF.MesEAnoDoPeriodo(QrContasItsPeriodo.Value);
  case QrContasItsTipo.Value of
    0: Txt := ' $';
    1: Txt := ' %';
    else Txt := ' ???';
  end;
  QrContasItsESTIPULADO.Value :=
    Geral.FFT(QrContasItsFator.Value, 6, siNegativo) + Txt;
end;

procedure TFmContas.QrContasMesCalcFields(DataSet: TDataSet);
begin
  QrContasMesPERIODOINI_TXT.Value :=
    Geral.FDT(Geral.PeriodoToDate(QrContasMesPeriodoIni.Value, 1, False), 19);
  QrContasMesPERIODOFIM_TXT.Value :=
    Geral.FDT(Geral.PeriodoToDate(QrContasMesPeriodoFim.Value, 1, False), 19);
end;

procedure TFmContas.BtExcluiItsClick(Sender: TObject);
var
  i: integer;
begin
  if DBGrid1.SelectedRows.Count > 0 then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o dos meses selecionados?') = ID_YES then
    begin
      with DBGrid1.DataSource.DataSet do
      for i:= 0 to DBGrid1.SelectedRows.Count-1 do
      begin
        //GotoBookmark(DBGrid1.SelectedRows.Items[i]);
        GotoBookmark(DBGrid1.SelectedRows.Items[i]);
        ExcluiContaIts;
      end;
    end;
  end else begin
    if Geral.MB_Pergunta('Confirma a exclus�o do m�s selecionado?') = ID_YES then
      ExcluiContaIts;
  end;
  ReopenContasIts(0);
end;

procedure TFmContas.ExcluiContaIts;
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM contasitsctas WHERE Codigo=:P0 AND Periodo=:P1');
  Dmod.QrUpd.Params[0].AsInteger := QrContasItsCodigo.Value;
  Dmod.QrUpd.Params[1].AsInteger := QrContasItsPeriodo.Value;
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM contasits WHERE Codigo=:P0 AND Periodo=:P1');
  Dmod.QrUpd.Params[0].AsInteger := QrContasItsCodigo.Value;
  Dmod.QrUpd.Params[1].AsInteger := QrContasItsPeriodo.Value;
  Dmod.QrUpd.ExecSQL;
end;

procedure TFmContas.BtIncluiItsClick(Sender: TObject);
begin
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, CO_CONTAS, VAR_GOTOMySQLDBNAME, CO_VAZIO);
  if VAR_CODIGO > -1000 then
  begin
    QrLoc2.Close;
    QrLoc2.Params[0].AsInteger := VAR_CODIGO;
    UnDmkDAC_PF.AbreQuery(QrLoc2, Dmod.MyDB);
    //
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO ctasitsctas SET Conta='+IntToStr(VAR_CODIGO));
    DModG.QrUpdPID1.SQL.Add(', Nome="'+QrLoc2Nome.Value+'"');
    DModG.QrUpdPID1.ExecSQL;
    QrCtas.Close;
    UnDmkDAC_PF.AbreQuery(QrCtas, DModG.MyPID_DB);
  end;
end;

procedure TFmContas.BtExclui2Click(Sender: TObject);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM ctasitsctas WHERE Conta='+
  IntToStr(QrCtasConta.Value));
  DModG.QrUpdPID1.ExecSQL;
  QrCtas.Close;
  UnDmkDAC_PF.AbreQuery(QrCtas, DModG.MyPID_DB);
end;

procedure TFmContas.QrContasItsAfterScroll(DataSet: TDataSet);
begin
  ReopenContasItsCtas;
end;

procedure TFmContas.ReopenContasItsCtas;
begin
  QrContasItsCtas.Close;
  QrContasItsCtas.Params[0].AsInteger := QrContasItsCodigo.Value;
  QrContasItsCtas.Params[1].AsInteger := QrContasItsPeriodo.Value;
  UnDmkDAC_PF.AbreQuery(QrContasItsCtas, Dmod.MyDB);
end;


procedure TFmContas.ReopenContasU(Controle: Integer);
begin
//
end;

procedure TFmContas.QrContasItsBeforeClose(DataSet: TDataSet);
begin
  QrContasItsCtas.Close;
end;

procedure TFmContas.QrContasBeforeClose(DataSet: TDataSet);
begin
  QrContasIts.Close;
  QrContasMes.Close;
end;

procedure TFmContas.BtExcluiClick(Sender: TObject);
begin
{###
  ver como fazer!
  if DBCheck.CriaFm(TFmContasExclui, FmContasExclui, afmoNegarComAviso) then
  begin
    FmContasExclui.ShowModal;
    FmContasExclui.Destroy;
    LocCod(QrContasCodigo.Value, QrContasCodigo.Value);
    ReopenLocate;
  end;
}
end;

procedure TFmContas.DBGrid4DblClick(Sender: TObject);
begin
  FLocLocate := True;
  PageControl1.ActivePageIndex := 0;
  PageControl1Change(Self);
end;

procedure TFmContas.PageControl1Change(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0:
    begin
     if FLocLocate then
       begin
         FLocLocate := False;
         LocCod(QrLocateCodigo.Value, QrLocateCodigo.Value);
       end;
    end;
    1: EdLocate.SetFocus;
    3:
    begin
{###
      DModG.ReopenEmpresas(VAR_USUARIO, 0, EdFilial, CBFilial);
      ReopenLct();
      EdNova.Text := '';
      CBNova.KeyValue := Null;
      if QrLct.RecordCount > 0 then
      begin
        QrNova.Close;
        QrNova.Params[0].AsInteger := QrContasCodigo.Value;
        UnDmkDAC_PF.AbreQuery(QrNova, Dmod.MyDB);
      end;
}
    end;
  end;
end;

{###
procedure TFmContas.ReopenLct();
var
  TabLct: String;
begin
  QrLct.Close;
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
  QrLct.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
  QrLct.SQL.Add('la.*, ct.Codigo CONTA, ca.Prazo,');
  QrLct.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
  QrLct.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
  QrLct.SQL.Add('CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial');
  QrLct.SQL.Add('ELSE em.Nome END NOMEEMPRESA,');
  QrLct.SQL.Add('CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  QrLct.SQL.Add('ELSE cl.Nome END NOMECLIENTE,');
  QrLct.SQL.Add('CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial');
  QrLct.SQL.Add('ELSE fo.Nome END NOMEFORNECEDOR');
  QrLct.SQL.Add('FROM ' + VAR LCT + ' la');
  QrLct.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  QrLct.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  QrLct.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  QrLct.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  QrLct.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  QrLct.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  QrLct.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrLct.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  QrLct.SQL.Add('WHERE Genero=:P0');
  QrLct.SQL.Add('AND Genero>=0');
  QrLct.SQL.Add('AND Controle>0');
  QrLct.SQL.Add('ORDER BY la.Data, la.Controle');
  QrLct.Params[0].AsInteger := QrContasCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
end;
}

procedure TFmContas.ReopenLocate;
begin
  QrLocate.Close;
  UnDmkDAC_PF.AbreQuery(QrLocate, Dmod.MyDB);
end;

procedure TFmContas.EdLocateChange(Sender: TObject);
begin
  LocalizaReferencia;
end;

procedure TFmContas.LocalizaReferencia;
begin
  if QrLocate.State = dsBrowse then QrLocate.Locate('Nome', EdLocate.Text,
    [loPartialKey, loCaseInsensitive]);
end;

procedure TFmContas.EdLocateKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=13 then
  begin
    PageControl1.ActivePageIndex := 0;
    PageControl1Change(Self);
  end;
end;

procedure TFmContas.BtRecicloClick(Sender: TObject);
{###
var
  i: integer;
  Nova: Integer;
}
begin
{###
  Nova := Geral.IMV(EdNova.Text);
  if Nova <= 0 then
  begin
    Geral.MB_('Conta de transfer�ncia n�o definida');
    Exit;
  end;
  if DBGLct.SelectedRows.Count > 0 then
  begin
    if Geral.MB_Pergunta('Confirma a transfer�ncia dos lan�amentos selecionados?') = ID_YES then
    begin
      with DBGLct.DataSource.DataSet do
      for i:= 0 to DBGLct.SelectedRows.Count-1 do
      begin
        GotoBookmark(DBGLct.SelectedRows.Items[i]);
        TransfereLancto;
      end;
    end;
  end else begin
    if Geral.MB_Pergunta('Confirma a transfer�ncia do lan�amento selecionado?') = ID_YES then TransfereLancto;
  end;
  ReopenLct();
}
end;

{###
procedure TFmContas.TransfereLancto;
var
  Nova: Integer;
begin
  Nova := Geral.IMV(EdNova.Text);
  if Nova > 0 then
  begin
    UmyMod.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'Genero'], ['Controle', 'Sub'], [Nova], [QrLctControle.Value,
    QrLctSub.Value], True, '');
  end else begin
    Geral.MB_Aviso('Conta de transfer�ncia n�o definida');
    Exit;
  end;
end;
}

procedure TFmContas.EdNovaChange(Sender: TObject);
begin
{###
  if Geral.IMV(EdNova.Text) > 0 then BtReciclo.Enabled := True
  else BtReciclo.Enabled := False;
}
end;

procedure TFmContas.EdSubgrupoChange(Sender: TObject);
var
  SubGrupo: Integer;
begin
  SubGrupo := EdSubGrupo.ValueVariant;
  //
  if SubGrupo <> 0 then
  begin
    if QrSubGruposTipoAgrupa.Value = 0 then //D�bito
    begin
      CBDebito.Checked  := True;
      CBCredito.Checked := False;
    end else
    begin
      CBDebito.Checked  := False;
      CBCredito.Checked := True;
    end;
  end;
end;

procedure TFmContas.BtAlteraItsClick(Sender: TObject);
var
  ValorStr: String;
  ValorNum: Double;
begin
  if QrContasIts.RecordCount > 0 then
  begin
    ValorStr := Geral.FFT(QrContasItsFator.Value, 6, siNegativo);
    if InputQuery('Altera ', 'Informe o novo valor para $ ou %:', ValorStr) then
    begin
      Valornum := Geral.DMV(ValorStr);
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE contasits SET ');
      Dmod.QrUpd.SQL.Add('Fator=:P0 WHERE ');
      Dmod.QrUpd.SQL.Add('Tipo      =:P01 AND ');
      Dmod.QrUpd.SQL.Add('Codigo    =:P02 AND ');
      Dmod.QrUpd.SQL.Add('Periodo   =:P03');
      //
      Dmod.QrUpd.Params[00].AsFloat := ValorNum;
      Dmod.QrUpd.Params[01].AsInteger := QrContasItsTipo.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrContasItsCodigo.Value;
      Dmod.QrUpd.Params[03].AsInteger := QrContasItsPeriodo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenContasIts(QrContasItsPeriodo.Value);
    end;
  end;
end;

procedure TFmContas.MostraEdicaoEnt(Mostra: Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      Panel21.Visible := True;
      Panel18.Visible := False;
      Panel20.Visible := False;
    end;
    1:
    begin
      Panel18.Visible := True;
      Panel20.Visible := True;
      Panel21.Visible := False;
      if SQLType = stIns then
      begin
        EdEnti2.Text      := '';
        CBEnti2.KeyValue  := Null;
        EdCntrDebCta.Text := '';
        EdCodExporta.Text := '';
      end else begin
        EdEnti2.Text      := IntToStr(QrContasEntEntidade.Value);
        CBEnti2.KeyValue  := QrContasEntEntidade.Value;
        EdCntrDebCta.Text := QrContasEntCntrDebCta.Value;
        EdCodExporta.Text := QrContasEntCodExporta.Value;   
      end;
      EdEnti2.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  if Codigo > 0 then ReopenContasEnt(Codigo);
end;

procedure TFmContas.ReopenContasEnt(Entidade: Integer);
begin
  QrContasEnt.Close;
  QrContasEnt.Params[0].AsInteger := QrContasCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrContasEnt, Dmod.MyDB);
  //
  QrContasEnt.Locate('Entidade', Entidade, []);
end;

procedure TFmContas.BitBtn2Click(Sender: TObject);
begin
  MostraEdicaoEnt(1, stIns, 0);
end;

procedure TFmContas.BitBtn4Click(Sender: TObject);
begin
  MostraEdicaoEnt(1, stUpd, 0);
end;

procedure TFmContas.BitBtn1Click(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := Geral.IMV(EdEnti2.Text);
  if Entidade = 0 then
    Geral.MB_Aviso('Informe a entidade!') else
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ContasEnt', False, [
    'CntrDebCta', 'CodExporta'], ['Codigo', 'Entidade'], [
    EdCntrDebCta.Text, EdCodExporta.Text], [QrContasCodigo.Value, Entidade], True) then
      MostraEdicaoEnt(0, stLok, Entidade);
end;

procedure TFmContas.BitBtn5Click(Sender: TObject);
begin
  MostraEdicaoEnt(0, stLok, 0);
end;

procedure TFmContas.BitBtn6Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(DMod.QrUpd, QrContasMes, TDBGrid(GradeContasMes),
  'contasmes', ['controle'], ['controle'], istPergunta, '');
end;

procedure TFmContas.BitBtn7Click(Sender: TObject);
begin
  MostraContasMes(stUpd);
end;

procedure TFmContas.BitBtn8Click(Sender: TObject);
begin
  MostraContasMes(stIns);
end;

procedure TFmContas.SbImprimeClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

end.

