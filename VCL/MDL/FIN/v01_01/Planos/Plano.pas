unit Plano;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, dmkPermissoes, dmkGeral, dmkEdit, UnDmkProcFunc, dmkImage, UnDmkEnums,
  dmkRadioGroup;

type
  TFmPlano = class(TForm)
    PainelDados: TPanel;
    DsPlano: TDataSource;
    QrPlano: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label10: TLabel;
    DBGrid1: TDBGrid;
    TbConjuntos: TmySQLTable;
    DsConjuntos: TDataSource;
    Label4: TLabel;
    EdOrdemLista: TdmkEdit;
    QrPlanoCodigo: TIntegerField;
    QrPlanoNome: TWideStringField;
    QrPlanoLk: TIntegerField;
    QrPlanoDataCad: TDateField;
    QrPlanoDataAlt: TDateField;
    QrPlanoUserCad: TIntegerField;
    QrPlanoUserAlt: TIntegerField;
    QrPlanoCtrlaSdo: TSmallintField;
    QrPlanoOrdemLista: TIntegerField;
    TbConjuntosCodigo: TIntegerField;
    TbConjuntosNome: TWideStringField;
    TbConjuntosPlano: TIntegerField;
    TbConjuntosLk: TIntegerField;
    TbConjuntosDataCad: TDateField;
    TbConjuntosDataAlt: TDateField;
    TbConjuntosUserCad: TIntegerField;
    TbConjuntosUserAlt: TIntegerField;
    TbConjuntosOrdemLista: TIntegerField;
    TbConjuntosCtrlaSdo: TSmallintField;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    CkCtrlaSdo: TCheckBox;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    StaticText3: TStaticText;
    QrPlanoFinContab: TSmallintField;
    RGFinContab: TdmkRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    QrPlanoNome2: TWideStringField;
    EdNome: TdmkEdit;
    EdNome2: TdmkEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPlanoAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrPlanoAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPlanoBeforeOpen(DataSet: TDataSet);
    procedure TbConjuntosDeleting(Sender: TObject; var Allow: Boolean);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure TbConjuntosBeforeInsert(DataSet: TDataSet);
    procedure TbConjuntosBeforeDelete(DataSet: TDataSet);
    procedure QrPlanoBeforeClose(DataSet: TDataSet);
    procedure EdNomeRedefinido(Sender: TObject);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmPlano: TFmPlano;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnFinanceiroJan, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPlano.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPlano.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPlanoCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPlano.DefParams;
begin
  VAR_GOTOTABELA := 'Plano';
  VAR_GOTOMYSQLTABLE := QrPlano;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;



  VAR_SQLx.Add('SELECT pl.*');
  VAR_SQLx.Add('FROM plano pl');
  //
  VAR_SQL1.Add('WHERE pl.Codigo=:P0');
  //
  VAR_SQLa.Add('WHERE pl.Nome Like :P0');
  //
end;

procedure TFmPlano.EdNomeRedefinido(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
    if EdNome2.ValueVariant = EmptyStr then
      EdNome2.ValueVariant := EdNome.ValueVariant;
end;

procedure TFmPlano.MostraEdicao(Mostra: Boolean; SQLType: TSQLType);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    if SQLType = stIns then
    begin
      EdCodigo.ValueVariant     := 0;
      EdNome.Text               := EmptyStr;
      EdNome2.Text              := EmptyStr;
      EdOrdemLista.ValueVariant := 0;
      CkCtrlaSdo.Checked        := True;
      RGFinContab.ItemIndex     := 0;
    end else
    begin
      EdCodigo.ValueVariant     := QrPlanoCodigo.Value;
      EdNome.Text               := QrPlanoNome.Value;
      EdNome2.Text              := QrPlanoNome2.Value;
      EdOrdemLista.ValueVariant := IntToStr(QrPlanoOrdemLista.Value);;
      CkCtrlaSdo.Checked        := Geral.IntToBool_0(QrPlanoCtrlaSdo.Value);
      RGFinContab.ItemIndex     := QrPlanoFinContab.Value;
    end;
    EdNome.SetFocus;
  end else
  begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPlano.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPlano.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPlano.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPlano.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPlano.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPlano.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPlano.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPlano.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(True, stIns);
end;

procedure TFmPlano.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(True, stUpd);
end;

procedure TFmPlano.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPlanoCodigo.Value;
  Close;
end;

procedure TFmPlano.BtConfirmaClick(Sender: TObject);
var
  Nome, Nome2: String;
  //NotPrntFin,
  Codigo, OrdemLista, CtrlaSdo, FinContab: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  Nome2          := EdNome2.Text;
  OrdemLista     := EdOrdemLista.ValueVariant;
  CtrlaSdo       := Geral.BoolToInt(CkCtrlaSdo.Checked);
  //NotPrntFin     := ;
  FinContab      := RGFinContab.ItemIndex;
  //
  if MyObjects.FIC(Nome = EmptyStr, EdNome, 'Informe a descri��o.') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('plano', 'Codigo', SQLType, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'plano', False, [
  'Nome', 'Nome2', 'OrdemLista', 'CtrlaSdo',
  (*'NotPrntFin',*) 'FinContab'], [
  'Codigo'], [
  Nome, Nome2, OrdemLista, CtrlaSdo,
  (*NotPrntFin,*) FinContab], [
  Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Plano', 'Codigo');
    MostraEdicao(False, stLok);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmPlano.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Plano', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Plano', 'Codigo');
  MostraEdicao(False, stLok);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Plano', 'Codigo');
end;

procedure TFmPlano.BtExcluiClick(Sender: TObject);
begin
//
end;

procedure TFmPlano.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  DBGrid1.Align     := alClient;
  CriaOForm;
  QrPlano.Database     := Dmod.MyDB;
end;

procedure TFmPlano.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPlanoCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPlano.SbImprimeClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas();
end;

procedure TFmPlano.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPlano.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmPlano.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPlano.QrPlanoAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPlano.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPlano.QrPlanoAfterScroll(DataSet: TDataSet);
begin
  TbConjuntos.Close;
  TbConjuntos.Filter := 'Plano='+IntToStr(QrPlanoCodigo.Value);
  UnDmkDAC_PF.AbreTable(TbConjuntos, Dmod.MyDB);
end;

procedure TFmPlano.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPlanoCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Plano', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPlano.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPlano.QrPlanoBeforeClose(DataSet: TDataSet);
begin
  TbConjuntos.Close;
end;

procedure TFmPlano.QrPlanoBeforeOpen(DataSet: TDataSet);
begin
  QrPlanoCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPlano.TbConjuntosBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmPlano.TbConjuntosBeforeInsert(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmPlano.TbConjuntosDeleting(Sender: TObject;
  var Allow: Boolean);
begin
  Allow := False;
  Geral.MB_Aviso(
  'Para excluir sub-grupo de conta acesse o cadastro de sub-Plano!');
end;

end.

