unit PlanoImpExp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkPermissoes, mySQLDbTables,
  Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc, Vcl.Menus;

type
  TAnaliSinte = (anasinIndef=0, anasinAnalitico=1, anasinSintetico=2);
  TKindAnaSin = (kasIndef=0, kasAouS=1, kas1ou2=2);
  //
  TFmPlanoImpExp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrPlano: TmySQLQuery;
    QrConjunto: TmySQLQuery;
    QrGrupo: TmySQLQuery;
    QrSubgrupo: TmySQLQuery;
    QrConta: TmySQLQuery;
    PMMenu: TPopupMenu;
    Removeatual1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    TreeView1: TTreeView;
    Panel5: TPanel;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    BtImporta: TBitBtn;
    BtSalvar: TBitBtn;
    BtCarrega: TBitBtn;
    BtExclui: TBitBtn;
    BtExporta: TBitBtn;
    TabSheet2: TTabSheet;
    GradeC: TStringGrid;
    PnCarrega: TPanel;
    Label110: TLabel;
    SBArquivoC: TSpeedButton;
    SbCarregaC: TSpeedButton;
    EdLoadCSVArqC: TEdit;
    MeAvisosC: TMemo;
    Panel6: TPanel;
    BtInsereC: TBitBtn;
    PB1: TProgressBar;
    TabSheet3: TTabSheet;
    Panel7: TPanel;
    Label1: TLabel;
    SbArquivoD: TSpeedButton;
    SbCarregaD: TSpeedButton;
    EdLoadCSVArqD: TEdit;
    GradeD: TStringGrid;
    Panel8: TPanel;
    BtInsereD: TBitBtn;
    MeAvisosD: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtExportaClick(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure Removeatual1Click(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure SbCarregaCClick(Sender: TObject);
    procedure SBArquivoCClick(Sender: TObject);
    procedure BtInsereCClick(Sender: TObject);
    procedure BtInsereDClick(Sender: TObject);
    procedure SbArquivoDClick(Sender: TObject);
    procedure SbCarregaDClick(Sender: TObject);
  private
    { Private declarations }
    FArquivo: String;
    // fim 2022-03-19
    FLinha, FCodigo, FPlano, FConjunto, FGrupo, FSubgrupo, FConta: Integer;
    FAnaliSinte: String;
    FLNivs: TStringList;
    FxNiveis: String;
    FNome: String;
    FDeletouTudo: Boolean;
    FKindAnaSin: TKindAnaSin;
    // fim 2022-03-19
    procedure CarregaPlanoDeContasDB;
    procedure CarregaPlanoDeContasXML(SalvaDB: Boolean);
    procedure ExportaPlanoDeContasXML;
    procedure MostraEdicao(Tipo: TSQLType);
    procedure ExcluiAtual(MostraMsg: Boolean);
    // ini 2022-03-19
    function  CadastraCNivel5(): Boolean;
    function  CadastraCNivel4(): Boolean;
    function  CadastraCNivel3(): Boolean;
    function  CadastraCNivel2(): Boolean;
    function  CadastraCNivel1(): Boolean;
    function  DefineSQLType(Tabela: String; Codigo: INteger): TSQLType;
    procedure RedefineLivresTabela(Tabela: String);
    function  SeparaNiveisENomesD(const Texto: String; var Niveis, Nome: String): Boolean;
    function  ImpedePorAnaliSinte(AnaliSinte: TAnaliSinte): Boolean;
  public
    { Public declarations }
  end;

  var
  FmPlanoImpExp: TFmPlanoImpExp;
  const
  CO_XML_Tit = 'dmk_plano_de_contas';

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnTreeView, UMySQLModule, MyDBCheck;

{$R *.DFM}


const
  _Col_C_Nivs = 1;
  _Col_C_Noms = 2;
  _Col_C_Cods = 3;
  _Col_C_AnSi = 4;

  _Col_D_Nivs = 3;
  //_Col_D_Noms = ;
  _Col_D_Cods = 1;
  _Col_D_AnSi = 2;

procedure TFmPlanoImpExp.BtExportaClick(Sender: TObject);
begin
  ExportaPlanoDeContasXML;
  //
  MostraEdicao(stLok);
end;

procedure TFmPlanoImpExp.BtImportaClick(Sender: TObject);
begin
  CarregaPlanoDeContasXML(False);
  //
  BtExclui.Enabled := False;
  BtSalvar.Enabled := True;
  //
  MostraEdicao(stIns);
end;

procedure TFmPlanoImpExp.BtInsereCClick(Sender: TObject);
var
  I, J, L: Integer;
begin
  FKindAnaSin := TKindAnaSin.kasAouS;
  FDeletouTudo := False;
  if Geral.MB_Pergunta('Deseja excluir todo plano de contas?') = ID_YES then
  begin
    if Geral.MB_Pergunta('Tem certeza que deseja excluir todo plano de contas?') = ID_YES then
    begin
       Dmod.MyDB.Execute('DELETE FROM plano WHERE Codigo>0');
       Dmod.MyDB.Execute('DELETE FROM conjuntos WHERE Codigo>0');
       Dmod.MyDB.Execute('DELETE FROM grupos WHERE Codigo>0');
       Dmod.MyDB.Execute('DELETE FROM subgrupos WHERE Codigo>0');
       Dmod.MyDB.Execute('DELETE FROM contas WHERE Codigo>0');
       FDeletouTudo := True;
    end;
  end;
  FLNivs    := TStringList.Create;
  FPlano    := 0;
  FConjunto := 0;
  FGrupo    := 0;
  FSubgrupo := 0;
  FConta    := 0;
  PB1.Max   := GradeC.RowCount;
  PB1.Position := 0;
  try
    for I := 0 to GradeC.RowCount - 1 do
    begin
      FxNiveis    := Trim(GradeC.Cells[_Col_C_Nivs, I]);
      MyObjects.Informa2eUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Inserindo Linha ' + Geral.FF0(I+1) + ' n�vel ' + FxNiveis);
      FLinha      := I;
      FCodigo     := Geral.IMV(GradeC.Cells[_Col_C_Cods, I]);
      FAnaliSinte :=           GradeC.Cells[_Col_C_AnSi, I];
      FNome       :=           GradeC.Cells[_Col_C_Noms, I];
      FLNivs      := Geral.Explode(FxNiveis, '.');
      case FLNivs.Count of
        1: CadastraCNivel5();
        2: CadastraCNivel4();
        3: CadastraCNivel3();
        4: CadastraCNivel2();
        5: CadastraCNivel1();
      end;
{
      //Geral.MB_Teste(xNiveis);
      for L := 0 to LNivs.Count - 1 do
      begin
          //Geral.MB_Teste(LNivs[L]);
      end;
}
    end;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Importa��o finalizada!');
  finally
    FLNivs.Free;
  end;
end;

procedure TFmPlanoImpExp.BtInsereDClick(Sender: TObject);
var
  I, J, L: Integer;
  NiveisENome: String;
begin
  FKindAnaSin := TKindAnaSin.kas1ou2;
  //
  FDeletouTudo := False;
  if Geral.MB_Pergunta('Deseja excluir todo plano de contas?') = ID_YES then
  begin
    if Geral.MB_Pergunta('Tem certeza que deseja excluir todo plano de contas?') = ID_YES then
    begin
       Dmod.MyDB.Execute('DELETE FROM plano WHERE Codigo>0');
       Dmod.MyDB.Execute('DELETE FROM conjuntos WHERE Codigo>0');
       Dmod.MyDB.Execute('DELETE FROM grupos WHERE Codigo>0');
       Dmod.MyDB.Execute('DELETE FROM subgrupos WHERE Codigo>0');
       Dmod.MyDB.Execute('DELETE FROM contas WHERE Codigo>0');
       FDeletouTudo := True;
    end;
  end;
  FLNivs    := TStringList.Create;
  FPlano    := 0;
  FConjunto := 0;
  FGrupo    := 0;
  FSubgrupo := 0;
  FConta    := 0;
  PB1.Max   := GradeD.RowCount;
  PB1.Position := 0;
  try
    for I := 1 to GradeD.RowCount - 1 do
    begin
      NiveisENome := Trim(GradeD.Cells[3, I]);
      if NiveisENome <> EmptyStr then
      begin
        //FxNiveis    := Trim(GradeD.Cells[_Col_D_Nivs, I]);
        if not SeparaNiveisENomesD(NiveisENome, FxNiveis, FNome) then Exit;
        MyObjects.Informa2eUpdPB(PB1, LaAviso1, LaAviso2, True,
          'Inserindo Linha ' + Geral.FF0(I+1) + ' n�vel ' + FxNiveis);
        FLinha      := I;
        FCodigo     := Geral.IMV(GradeD.Cells[_Col_D_Cods, I]);
        FAnaliSinte :=           GradeD.Cells[_Col_D_AnSi, I];
        //FNome       :=           GradeD.Cells[_Col_D_Noms, I];
        FLNivs      := Geral.Explode(FxNiveis, '.');
        case FLNivs.Count of
          1: CadastraCNivel5();
          2: CadastraCNivel4();
          3: CadastraCNivel3();
          4: CadastraCNivel2();
          5: CadastraCNivel1();
        end;
      end;
    end;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Importa��o finalizada!');
  finally
    FLNivs.Free;
  end;
end;

procedure TFmPlanoImpExp.BtExcluiClick(Sender: TObject);
begin
  ExcluiAtual(True);
end;

procedure TFmPlanoImpExp.BtCarregaClick(Sender: TObject);
begin
  BtCarrega.Enabled := False;
  try
    CarregaPlanoDeContasDB;
    //
    BtExclui.Enabled := True;
    BtSalvar.Enabled := False;
    //
    MostraEdicao(stIns);
  finally
    BtCarrega.Enabled := True;
  end;
end;

procedure TFmPlanoImpExp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPlanoImpExp.BtSalvarClick(Sender: TObject);
begin
  CarregaPlanoDeContasXML(True);
  //
  MostraEdicao(stLok);
end;

function TFmPlanoImpExp.CadastraCNivel1(): Boolean;
var
  Nome, Nome2, Nome3, ID, Credito, Debito, Mensal, Exclusivo, Excel, Antigo,
  Sigla: String;
  Codigo, Subgrupo, CentroCusto, Empresa, Mensdia, Terceiro, Rateio, Entidade,
  PendenMesSeg, CalculMesSeg, OrdemLista, ContasAgr, ContasSum, CtrlaSdo,
  NotPrntBal, ProvRat, NotPrntFin, TemDocFisi, CentroRes, PagRec, CroCon,
  UsoNoERP, ImpactEstru: Integer;
  Mensdeb, Mensmind, Menscred, Mensminc: Double;
  SQLType: TSQLType;
  //
  P: Integer;
const
  Tabela = 'subgrupos';
  TipoAgrupaD = 0;
  TipoAgrupaC = 1;
begin
  Result := False;
  if ImpedePorAnaliSinte(TAnaliSinte.anasinSintetico) then
    Exit;
  SQLType := DefineSQLType(Tabela, FCodigo);
  //
  Codigo         := FCodigo;
  Nome           := FNome;
  Nome2          := FNome;
  Nome3          := FNome;
  OrdemLista     := Geral.IMV(FLNivs[FLNivs.Count-1]);
  SubGrupo       := FSubGrupo;

  //ID             := '';
  CentroCusto    := 0;
  Empresa        := -11;
  Credito        := 'F';
  Debito         := 'F';
  P := pos('-', FNome);
  if (P > 0) and (P < 5) then
  begin
    Debito := 'V';
  end else
  begin
    P := pos('+', FNome);
    if (P > 0) and (P < 5) then
    begin
      Credito := 'V';
    end else
    begin
      case FPlano of
        1, 100: Credito := 'V';
        2, 200: Debito := 'V';
        3, 300: Credito := 'V';
        4, 400: Debito := 'V';
        5, 500: Debito := 'V';
        6, 600: Credito := 'V';
      end;
    end;
  end;

  Mensal         := 'F';
  Exclusivo      := 'F';
  Mensdia        := 1;
  Mensdeb        := 0;
  Mensmind       := 0;
  Menscred       := 0;
  Mensminc       := 0;
  Terceiro       := 0;
  Excel          := '';
  Rateio         := 0;
  Entidade       := 0;
  Antigo         := '';
  PendenMesSeg   := 0;
  CalculMesSeg   := 0;
  //OrdemLista     :=  Acima!!!
  ContasAgr      := 0;
  ContasSum      := 0;
  CtrlaSdo       := 0;
  NotPrntBal     := 0;
  Sigla          := Copy(Nome, 1, 20);
  ProvRat        := 1;
  NotPrntFin     := 0;
  TemDocFisi     := 1;
  CentroRes      := 1;
  //
  PagRec         := 0;
  if Debito = 'V' then
    PagRec := -1;
  //
  CroCon         := 0;
  UsoNoERP       := 0;
  //
  ImpactEstru := -1;
  //
  case FPlano of
    1, 100: ImpactEstru := 0; // Patrimonial
    2, 200: ImpactEstru := 0; // Patrimonial
    3, 300: ImpactEstru := 1; // Resultado
    4, 400: ImpactEstru := 1; // Resultado
    5, 500: ImpactEstru := 1; // Resultado
    6, 600: ImpactEstru := 1; // Resultado
  end;

  //
  //Codigo := UMyMod.BuscaEmLivreY_Def('contas', 'Codigo', SQLType, CodAtual?);
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'contas', False, [
  'Nome', 'Nome2', 'Nome3',
  'ID', 'Subgrupo', 'CentroCusto',
  'Empresa', 'Credito', 'Debito',
  'Mensal', 'Exclusivo', 'Mensdia',
  'Mensdeb', 'Mensmind', 'Menscred',
  'Mensminc', 'Terceiro', 'Excel',
  'Rateio', 'Entidade', 'Antigo',
  'PendenMesSeg', 'CalculMesSeg', 'OrdemLista',
  'ContasAgr', 'ContasSum', 'CtrlaSdo',
  'NotPrntBal', 'Sigla', 'ProvRat',
  'NotPrntFin', 'TemDocFisi', 'CentroRes',
  'PagRec', 'CroCon', 'UsoNoERP',
  'ImpactEstru'], [
  'Codigo'], [
  Nome, Nome2, Nome3,
  ID, Subgrupo, CentroCusto,
  Empresa, Credito, Debito,
  Mensal, Exclusivo, Mensdia,
  Mensdeb, Mensmind, Menscred,
  Mensminc, Terceiro, Excel,
  Rateio, Entidade, Antigo,
  PendenMesSeg, CalculMesSeg, OrdemLista,
  ContasAgr, ContasSum, CtrlaSdo,
  NotPrntBal, Sigla, ProvRat,
  NotPrntFin, TemDocFisi, CentroRes,
  PagRec, CroCon, UsoNoERP,
  ImpactEstru], [
  Codigo], True);
  //
  FConta := Codigo;
  //
  RedefineLivresTabela(Tabela);
end;

function TFmPlanoImpExp.CadastraCNivel2(): Boolean;
var
  Nome, Nome2: String;
  Codigo, Grupo, OrdemLista, TipoAgrupa(*, CtrlaSdo, _Categoria, NotPrntFin*): Integer;
  SQLType: TSQLType;
const
  Tabela = 'subgrupos';
  TipoAgrupaD = 0;
  TipoAgrupaC = 1;
begin
  Result := False;
  if ImpedePorAnaliSinte(TAnaliSinte.anasinAnalitico) then
    Exit;
  SQLType := DefineSQLType(Tabela, FCodigo);
  //
  Codigo         := FCodigo;
  Nome           := FNome;
  Nome2          := FNome;
  OrdemLista     := Geral.IMV(FLNivs[FLNivs.Count-1]);
  Grupo          := FGrupo;
  TipoAgrupa     := -1;
  // C o l o s s o /Brasinha!!!
  case FPlano of
    1, 100: TipoAgrupa := TipoAgrupaC;
    2, 200: TipoAgrupa := TipoAgrupaD;
    3, 300: TipoAgrupa := TipoAgrupaC;
    4, 400: TipoAgrupa := TipoAgrupaD;
    5, 500: TipoAgrupa := TipoAgrupaD;
    6, 600: TipoAgrupa := TipoAgrupaC;
  end;
  //CtrlaSdo       := ;
  //_Categoria     := ;
  //NotPrntFin     := ;

  //
  //Codigo := UMyMod.BuscaEmLivreY_Def('subgrupos', 'Codigo', SQLType, CodAtual?);
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'subgrupos', False, [
  'Nome', 'Nome2', 'Grupo',
  'OrdemLista', 'TipoAgrupa'(*, 'CtrlaSdo',
  '_Categoria', 'NotPrntFin'*)], [
  'Codigo'], [
  Nome, Nome2, Grupo,
  OrdemLista, TipoAgrupa(*, CtrlaSdo,
  _Categoria, NotPrntFin*)], [
  Codigo], True);
  //
  FSubGrupo := Codigo;
  //
  RedefineLivresTabela(Tabela);
end;

function TFmPlanoImpExp.CadastraCNivel3(): Boolean;
var
  Nome, Nome2: String;
  Codigo, Conjunto, OrdemLista, CtrlaSdo, NotPrntFin: Integer;
  SQLType: TSQLType;
const
  Tabela = 'Grupos';
begin
  Result := False;
  if ImpedePorAnaliSinte(TAnaliSinte.anasinAnalitico) then
    Exit;
  SQLType := DefineSQLType(Tabela, FCodigo);
  //
  Codigo         := FCodigo;
  Nome           := FNome;
  Nome2          := FNome;
  OrdemLista     := Geral.IMV(FLNivs[FLNivs.Count-1]);
  Conjunto       := FConjunto;
  //CtrlaSdo       := ;
  //NotPrntFin     := ;

  //
  //Codigo := UMyMod.BuscaEmLivreY_Def('grupos', 'Codigo', SQLType, CodAtual?);
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'grupos', False, [
  'Nome', 'Nome2', 'Conjunto',
  'OrdemLista'(*, 'CtrlaSdo', 'NotPrntFin'*)], [
  'Codigo'], [
  Nome, Nome2, Conjunto,
  OrdemLista(*, CtrlaSdo, NotPrntFin*)], [
  Codigo], True);
  //
  FGrupo := Codigo;
  //
  RedefineLivresTabela(Tabela);
end;

function TFmPlanoImpExp.CadastraCNivel4(): Boolean;
var
  Nome, Nome2: String;
  Codigo, OrdemLista, Plano, CtrlaSdo, NotPrntFin: Integer;
  SQLType: TSQLType;
const
  Tabela = 'conjuntos';
begin
  Result := False;
  if ImpedePorAnaliSinte(TAnaliSinte.anasinAnalitico) then
    Exit;
  SQLType := DefineSQLType(Tabela, FCodigo);
  //
  Codigo         := FCodigo;
  Nome           := FNome;
  Nome2          := FNome;
  OrdemLista     := Geral.IMV(FLNivs[FLNivs.Count-1]);
  //
  Plano          := FPlano;
  //CtrlaSdo       := ;
  //NotPrntFin     := ;

  //
  //Codigo := UMyMod.BuscaEmLivreY_Def('conjuntos', 'Codigo', SQLType, CodAtual?);
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'conjuntos', False, [
  'Nome', 'Nome2', 'OrdemLista',
  'Plano'(*, 'CtrlaSdo', 'NotPrntFin'*)], [
  'Codigo'], [
  Nome, Nome2, OrdemLista,
  Plano(*, CtrlaSdo, NotPrntFin*)], [
  Codigo], True);
  //
  FConjunto := Codigo;
  //
  RedefineLivresTabela(Tabela);
end;

function TFmPlanoImpExp.CadastraCNivel5(): Boolean;
var
  Nome, Nome2: String;
  Codigo, OrdemLista(*, CtrlaSdo, NotPrntFin, FinContab*): Integer;
  SQLType: TSQLType;
const
  Tabela = 'plano';
begin
  Result := False;
  if ImpedePorAnaliSinte(TAnaliSinte.anasinAnalitico) then
    Exit;
  SQLType := DefineSQLType(Tabela, FCodigo);
  //
  Codigo         := FCodigo;
  Nome           := FNome;
  Nome2          := FNome;
  OrdemLista     := Geral.IMV(FLNivs[FLNivs.Count-1]);
  //CtrlaSdo       := ;
  //NotPrntFin     := ;
  //FinContab      := ;
  //Codigo := UMyMod.BuscaEmLivreY_Def('plano', 'Codigo', SQLType, Codigo);
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'plano', False, [
  'Nome', 'Nome2', 'OrdemLista'(*,
  'CtrlaSdo', 'NotPrntFin', 'FinContab'*)], [
  'Codigo'], [
  Nome, Nome2, OrdemLista(*,
  CtrlaSdo, NotPrntFin, FinContab*)], [
  Codigo], True);
  //
  FPlano := Codigo;
  //
  RedefineLivresTabela(Tabela);
end;

procedure TFmPlanoImpExp.CarregaPlanoDeContasDB();
var
  Codigo, Plano, Conjunto, Grupo, Subgrupo: Integer;
  Nome: String;
  Node: TTreeNode;
begin
  TreeView1.Items.Clear;
  //
  Screen.Cursor := crHourGlass;
  try
    //Carrega Plano
    UnDmkDAC_PF.AbreMySQLQuery0(QrPlano, Dmod.MyDB, [
      'SELECT Codigo, Nome, CtrlaSdo ',
      'FROM plano ',
      //'WHERE Codigo > 0 ',
      //'AND Ativo = 1 ',
      '']);
    if QrPlano.RecordCount > 0 then
    begin
      QrPlano.First;
      //
      while not QrPlano.EOF do
      begin
        Codigo := QrPlano.FieldByName('Codigo').AsInteger;
        Nome   := QrPlano.FieldByName('Nome').AsString;
        //
        UTreeView.CriaNode(nil, Nome, Codigo, TreeView1);
        //
        QrPlano.Next;
      end;
    end;
    //Carrega Conjunto
    UnDmkDAC_PF.AbreMySQLQuery0(QrConjunto, Dmod.MyDB, [
      'SELECT Codigo, Nome, Plano, CtrlaSdo ',
      'FROM conjuntos ',
      //'WHERE Codigo > 0 ',
      //'AND Ativo = 1 ',
      '']);
    if QrConjunto.RecordCount > 0 then
    begin
      QrConjunto.First;
      //
      while not QrConjunto.EOF do
      begin
        Codigo := QrConjunto.FieldByName('Codigo').AsInteger;
        Nome   := QrConjunto.FieldByName('Nome').AsString;
        Plano  := QrConjunto.FieldByName('Plano').AsInteger;
        //
        Node := UTreeView.LocalizaNodeInArrayNode((*''*)Nome, Plano, 0, TreeView1);
        //
        if Node <> nil then
          UTreeView.CriaNode(Node, Nome, Codigo, TreeView1, -1, True);
        //
        QrConjunto.Next;
      end;
    end;
    //Carrega Grupo
    UnDmkDAC_PF.AbreMySQLQuery0(QrGrupo, Dmod.MyDB, [
      'SELECT Codigo, Nome, Conjunto, CtrlaSdo ',
      'FROM grupos ',
      //'WHERE Codigo > 0 ',
      //'AND Ativo = 1 ',
      '']);
    if QrGrupo.RecordCount > 0 then
    begin
      QrGrupo.First;
      //
      while not QrGrupo.EOF do
      begin
        Codigo   := QrGrupo.FieldByName('Codigo').AsInteger;
        Nome     := QrGrupo.FieldByName('Nome').AsString;
        Conjunto := QrGrupo.FieldByName('Conjunto').AsInteger;
        //
        Node := UTreeView.LocalizaNodeInArrayNode((*''*)Nome, Conjunto, 1, TreeView1);
        //
        if Node <> nil then
          UTreeView.CriaNode(Node, Nome, Codigo, TreeView1, -1, True);
        //
        QrGrupo.Next;
      end;
    end;
    //Carrega Subgrupo
    UnDmkDAC_PF.AbreMySQLQuery0(QrSubgrupo, Dmod.MyDB, [
      'SELECT Codigo, Nome, Grupo, CtrlaSdo, TipoAgrupa ',
      'FROM subgrupos ',
      //'WHERE Codigo > 0 ',
      //'AND Ativo = 1 ',
      '']);
    if QrSubgrupo.RecordCount > 0 then
    begin
      QrSubgrupo.First;
      //
      while not QrSubgrupo.EOF do
      begin
        Codigo := QrSubgrupo.FieldByName('Codigo').AsInteger;
        Nome   := QrSubgrupo.FieldByName('Nome').AsString;
        Grupo  := QrSubgrupo.FieldByName('Grupo').AsInteger;
        //
        Node := UTreeView.LocalizaNodeInArrayNode((*''*)Nome, Grupo, 2, TreeView1);
        //
        if Node <> nil then
          UTreeView.CriaNode(Node, Nome, Codigo, TreeView1, -1, True);
        //
        QrSubgrupo.Next;
      end;
    end;
    //Carrega Conta
    UnDmkDAC_PF.AbreMySQLQuery0(QrConta, Dmod.MyDB, [
      'SELECT Codigo, Nome, Subgrupo, Debito, Credito, Mensal, CtrlaSdo, ',
      'Nome2, Nome3, Sigla, CentroRes, PagRec ',
      'FROM contas ',
      //'WHERE Codigo > 0 ',
      //'AND Ativo = 1 ',
      '']);
    if QrConta.RecordCount > 0 then
    begin
      QrConta.First;
      //
      while not QrConta.EOF do
      begin
        Codigo   := QrConta.FieldByName('Codigo').AsInteger;
        Nome     := QrConta.FieldByName('Nome').AsString;
        Subgrupo := QrConta.FieldByName('Subgrupo').AsInteger;
        //
        Node := UTreeView.LocalizaNodeInArrayNode((*''*)Nome, Subgrupo, 3, TreeView1);
        //
        if Node <> nil then
          UTreeView.CriaNode(Node, Nome, Codigo, TreeView1, -1, True);
        //
        QrConta.Next;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
    TreeView1.FullExpand;
  end;
end;

procedure TFmPlanoImpExp.CarregaPlanoDeContasXML(SalvaDB: Boolean);
var
  XML: IXMLDocument;
  Node, NodePlano, NodeConjunto, NodeGrupo, NodeSubgrupo, NodeConta: IXMLNode;
  TVNode: TTreeNode;
  i, j, k, l, m, Codigo, CtrlaSdo, TipoAgrupa, Plano, Conjunto, Grupo, SubGrupo, 
  Conta, CentroRes, PagRec: Integer;
  NodeNome, Nome, Nome2, Nome3, Sigla, Debito, Credito, Mensal: String;
begin
  if not SalvaDB then
  begin
    FArquivo := MyObjects.DefineArquivo3(FmPlanoImpExp, nil);
  end else
  begin
    if Geral.MB_Pergunta('Deseja salvar no banco de dados o plano de contas carregado?') = ID_YES then
    begin
      if not DBCheck.LiberaPelaSenhaBoss then Exit;
    end;
  end;
  //
  if FArquivo <> '' then
  begin
    if LowerCase(ExtractFileExt(FArquivo)) = '.xml' then
    begin
      Screen.Cursor := crHourGlass;
      try
        TreeView1.Items.Clear;
        //
        XML := TXMLDocument.Create(nil);
        XML.LoadFromFile(FArquivo);
        //
        Node := XML.ChildNodes.FindNode(CO_XML_Tit);
        //
        if Node <> nil then
        begin
          //Carrega Plano
          for i := 0 to Node.ChildNodes.Count - 1 do
          begin
            NodePlano := Node.ChildNodes[i];
            NodeNome  := NodePlano.NodeName;
            //
            //if NodeNome = 'plano' then
            begin
              if (NodePlano.HasAttribute('Codigo')) then
              begin
                Codigo := NodePlano.Attributes['Codigo'];

                if not SalvaDB then
                  Plano := NodePlano.Attributes['Codigo']
                else
                  Plano := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd, 'plano', 'Codigo', [],
                             [], stIns, 0, siPositivo, nil);
              end else
              begin
                Codigo := 0;
                Plano  := 0;
              end;
              if (NodePlano.HasAttribute('CtrlaSdo')) then
                CtrlaSdo := NodePlano.Attributes['CtrlaSdo']
              else
                CtrlaSdo := 0;
              if (NodePlano.HasAttribute('Nome')) then
                Nome := NodePlano.Attributes['Nome']
              else
                Nome := '';
              //
              if (Nome <> '') and (Plano <> 0) then
              begin
                if not SalvaDB then
                begin
                  UTreeView.CriaNode(nil, Nome, Codigo, TreeView1);
                end else
                begin
                  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'plano', False,
                    ['Nome', 'CtrlaSdo'], ['Codigo'],
                    [Nome, CtrlaSdo], [Plano], True);
                end;
              end;
              //
              //Carrega Conjunto
              for j := 0 to NodePlano.ChildNodes.Count - 1 do
              begin
                NodeConjunto := NodePlano.ChildNodes[j];
                NodeNome     := NodeConjunto.NodeName;
                //
                if NodeNome = 'conjuntos' then
                begin
                  if (NodeConjunto.HasAttribute('Codigo')) then
                  begin
                    Codigo := NodeConjunto.Attributes['Codigo'];

                    if not SalvaDB then
                      Conjunto := NodeConjunto.Attributes['Codigo']
                    else
                      Conjunto := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd,
                                    'conjuntos', 'Codigo', [], [], stIns, 0,
                                    siPositivo, nil);
                  end else
                  begin
                    Codigo   := 0;
                    Conjunto := 0;
                  end;
                  if (NodeConjunto.HasAttribute('CtrlaSdo')) then
                    CtrlaSdo := NodeConjunto.Attributes['CtrlaSdo']
                  else
                    CtrlaSdo := 0;
                  if (NodeConjunto.HasAttribute('Nome')) then
                    Nome := NodeConjunto.Attributes['Nome']
                  else
                    Nome := '';
                  //
                  if (Nome <> '') and (Conjunto <> 0) then
                  begin
                    if not SalvaDB then
                    begin
                      TVNode := UTreeView.LocalizaNodeInArrayNode((*''*)Nome, Plano, 0, TreeView1);
                      //
                      if TVNode <> nil then
                        UTreeView.CriaNode(TVNode, Nome, Codigo, TreeView1, -1, True);
                    end else
                    begin
                      if Plano <> 0 then
                      begin
                        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'conjuntos', False,
                          ['Nome', 'CtrlaSdo', 'Plano'], ['Codigo'],
                          [Nome, CtrlaSdo, Plano], [Conjunto], True);
                      end;
                    end;
                  end;
                  //Carrega Grupo
                  for k := 0 to NodeConjunto.ChildNodes.Count - 1 do
                  begin
                    NodeGrupo := NodeConjunto.ChildNodes[k];
                    NodeNome  := NodeGrupo.NodeName;
                    //
                    if NodeNome = 'grupos' then
                    begin
                      if (NodeGrupo.HasAttribute('Codigo')) then
                      begin
                        Codigo := NodeGrupo.Attributes['Codigo'];

                        if not SalvaDB then
                          Grupo := NodeGrupo.Attributes['Codigo']
                        else
                          Grupo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd,
                                    'grupos', 'Codigo', [], [], stIns, 0,
                                    siPositivo, nil);
                      end else
                      begin
                        Codigo := 0;
                        Grupo  := 0;
                      end;
                      if (NodeGrupo.HasAttribute('CtrlaSdo')) then
                        CtrlaSdo := NodeGrupo.Attributes['CtrlaSdo']
                      else
                        CtrlaSdo := 0;
                      if (NodeGrupo.HasAttribute('Nome')) then
                        Nome := NodeGrupo.Attributes['Nome']
                      else
                        Nome := '';
                      //
                      if (Nome <> '') and (Grupo <> 0) then
                      begin
                        if not SalvaDB then
                        begin
                          TVNode := UTreeView.LocalizaNodeInArrayNode((*''*)Nome, Conjunto, 1, TreeView1);
                          //
                          if TVNode <> nil then
                            UTreeView.CriaNode(TVNode, Nome, Codigo, TreeView1, -1, True);
                        end else
                        begin
                          if Conjunto <> 0 then
                          begin
                            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'grupos', False,
                              ['Nome', 'CtrlaSdo', 'Conjunto'], ['Codigo'],
                              [Nome, CtrlaSdo, Conjunto], [Grupo], True);
                          end;
                        end;
                      end;
                      //Carrega Subgrupo
                      for l := 0 to NodeGrupo.ChildNodes.Count - 1 do
                      begin
                        NodeSubgrupo := NodeGrupo.ChildNodes[l];
                        NodeNome     := NodeSubgrupo.NodeName;
                        //
                        if NodeNome = 'subgrupos' then
                        begin
                          if (NodeSubgrupo.HasAttribute('Codigo')) then
                          begin
                            Codigo := NodeSubgrupo.Attributes['Codigo'];

                            if not SalvaDB then
                              SubGrupo := NodeSubgrupo.Attributes['Codigo']
                            else
                              SubGrupo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd,
                                            'subgrupos', 'Codigo', [], [], stIns,
                                            0, siPositivo, nil);
                          end else
                          begin
                            Codigo   := 0;
                            SubGrupo := 0;
                          end;
                          if (NodeSubgrupo.HasAttribute('CtrlaSdo')) then
                            CtrlaSdo := NodeSubgrupo.Attributes['CtrlaSdo']
                          else
                            CtrlaSdo := 0;
                          if (NodeSubgrupo.HasAttribute('Nome')) then
                            Nome := NodeSubgrupo.Attributes['Nome']
                          else
                            Nome := '';
                          if (NodeSubGrupo.HasAttribute('TipoAgrupa')) then
                            TipoAgrupa := NodeSubgrupo.Attributes['TipoAgrupa']
                          else
                            TipoAgrupa := 0;
                          //
                          if (Nome <> '') and (SubGrupo <> 0) then
                          begin
                            if not SalvaDB then
                            begin
                              TVNode := UTreeView.LocalizaNodeInArrayNode((*''*)Nome, Grupo, 2, TreeView1);
                              //
                              if TVNode <> nil then
                                UTreeView.CriaNode(TVNode, Nome, Codigo, TreeView1, -1, True);
                            end else
                            begin
                              if Grupo <> 0 then
                              begin
                                UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'subgrupos', False,
                                  ['Nome', 'CtrlaSdo', 'TipoAgrupa', 'Grupo'], ['Codigo'],
                                  [Nome, CtrlaSdo, TipoAgrupa, Grupo], [SubGrupo], True);
                              end;
                            end;
                          end;
                          //Carrega Conta
                          for m := 0 to NodeSubgrupo.ChildNodes.Count - 1 do
                          begin
                            NodeConta := NodeSubgrupo.ChildNodes[m];
                            NodeNome  := NodeConta.NodeName;
                            //
                            if NodeNome = 'contas' then
                            begin
                              if (NodeConta.HasAttribute('Codigo')) then
                              begin
                                Codigo := NodeConta.Attributes['Codigo'];

                                if not SalvaDB then
                                  Conta := NodeConta.Attributes['Codigo']
                                else
                                  Conta := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd,
                                            'contas', 'Codigo', [], [], stIns,
                                            0, siPositivo, nil);
                              end else
                              begin
                                Codigo := 0;
                                Conta  := 0;
                              end;
                              if (NodeConta.HasAttribute('CtrlaSdo')) then
                                CtrlaSdo := NodeConta.Attributes['CtrlaSdo']
                              else
                                CtrlaSdo := 0;
                              if (NodeConta.HasAttribute('Debito')) then
                                Debito := NodeConta.Attributes['Debito']
                              else
                                Debito := '';
                              if (NodeConta.HasAttribute('Credito')) then
                                Credito := NodeConta.Attributes['Credito']
                              else
                                Credito := '';
                              if (NodeConta.HasAttribute('Mensal')) then
                                Mensal := NodeConta.Attributes['Mensal']
                              else
                                Mensal := '';
                              if (NodeConta.HasAttribute('Nome2')) then
                                Nome2 := NodeConta.Attributes['Nome2']
                              else
                                Nome2 := '';
                              if (NodeConta.HasAttribute('Nome3')) then
                                Nome3 := NodeConta.Attributes['Nome3']
                              else
                                Nome3 := '';
                              if (NodeConta.HasAttribute('Sigla')) then
                                Sigla := NodeConta.Attributes['Sigla']
                              else
                                Sigla := '';
                              if (NodeConta.HasAttribute('CentroRes')) then
                                CentroRes := NodeConta.Attributes['CentroRes']
                              else
                                CentroRes := 0;
                              if (NodeConta.HasAttribute('PagRec')) then
                                PagRec := NodeConta.Attributes['PagRec']
                              else
                                PagRec := 0;
                              //
                              Nome := NodeConta.Text;
                              //
                              if (Nome <> '') and (Conta <> 0) then
                              begin
                                if not SalvaDB then
                                begin
                                  TVNode := UTreeView.LocalizaNodeInArrayNode((*''*)Nome, SubGrupo, 3, TreeView1);
                                  //
                                  if TVNode <> nil then
                                    UTreeView.CriaNode(TVNode, Nome, Codigo, TreeView1, -1, True);
                                end else
                                begin
                                  if SubGrupo <> 0 then
                                  begin
                                    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'contas', False,
                                      ['Nome', 'CtrlaSdo', 'Debito', 'Credito',
                                      'Mensal', 'SubGrupo', 'Nome2', 'Nome3',
                                      'Sigla', 'CentroRes', 'PagRec'], ['Codigo'],
                                      [Nome, CtrlaSdo, Debito, Credito,
                                      Mensal, SubGrupo, Nome2, Nome3,
                                      Sigla, CentroRes, PagRec], [Conta], True);
                                  end;
                                end;
                              end;
                            end;
                          end;
                        end;
                      end;
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
      finally
        Screen.Cursor := crDefault;
        TreeView1.FullExpand;
        //
        if SalvaDB then
          Geral.MB_Aviso('Importa��o finalizada!');
      end;
    end;
  end else
    Geral.MB_Aviso('Nenhum arquivo foi selecionado!');
end;

function TFmPlanoImpExp.DefineSQLType(Tabela: String;
  Codigo: Integer): TSQLType;
begin
  Result := stIns;
  if FDeletouTudo then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Codigo FROM ' + Lowercase(Tabela),
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  if Dmod.QrAux.RecordCount > 0 then
    if Dmod.QrAux.Fields[0].AsInteger = Codigo then
      Result := stUpd;
end;

procedure TFmPlanoImpExp.ExcluiAtual(MostraMsg: Boolean);
var
  Continua: Integer;
begin
  if MostraMsg then
    Continua := Geral.MB_Pergunta('Deseja remover o item atual?' + sLineBreak +
                  'AVISO: O item ser� removido somente desta lista ou seja ' +
                  'n�o ser� removido do banco de dados!')
  else
    Continua := ID_YES;
  //
  if Continua = ID_YES then
  begin
    UTreeView.ExcluiNode(TreeView1, '');
  end;
end;

procedure TFmPlanoImpExp.ExportaPlanoDeContasXML;
var
  XML: IXMLDocument;
  Node: TTreeNode;
  Codigo, CtrlaSdo, TipoAgrupa, CentroRes, PagRec: Integer;
  Nome, Nome2, Nome3, Sigla, Debito, Credito, Mensal, Diretorio, Arquivo: String;
  NodePlano, NodeConjunto, NodeGrupo, NodeSubgrupo, NodeConta: IXMLNode;
begin
  if TreeView1.Items.Count = 0 then Exit;
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  Diretorio := MyObjects.DefineDiretorio(FmPlanoImpExp, nil);
  //
  if Diretorio = '' then
  begin
    Geral.MB_Aviso('Nenhum arquivo foi selecionado!');
    Exit;
  end;
  XML        := TXMLDocument.Create(nil);
  XML.Active := True;
  //
  //Carrega Plano
  if (QrPlano.State <> dsInactive) and (QrPlano.RecordCount > 0) then
  begin
    XML.DocumentElement := XML.CreateNode(CO_XML_Tit, ntElement, '');
    //
    QrPlano.First;
    while not QrPlano.EOF do
    begin
      Codigo   := QrPlano.FieldByName('Codigo').AsInteger;
      Nome     := QrPlano.FieldByName('Nome').AsString;
      CtrlaSdo := QrPlano.FieldByName('CtrlaSdo').AsInteger;
      //
      Node := UTreeView.LocalizaNodeInArrayNode((*''*)Nome, Codigo, 0, TreeView1);
      //
      if Node <> nil then
      begin
        NodePlano := XML.DocumentElement.AddChild('plano', -1);
        NodePlano.Attributes['Codigo']   := Codigo;
        NodePlano.Attributes['Nome']     := Nome;
        NodePlano.Attributes['CtrlaSdo'] := CtrlaSdo;
        //
        //Carrega Conjunto
        if (QrConjunto.State <> dsInactive) and (QrConjunto.RecordCount > 0) then
        begin
          QrConjunto.First;
          QrConjunto.Filter := 'Plano=' + Geral.FF0(Codigo);
          while not QrConjunto.EOF do
          begin
            Codigo   := QrConjunto.FieldByName('Codigo').AsInteger;
            Nome     := QrConjunto.FieldByName('Nome').AsString;
            CtrlaSdo := QrConjunto.FieldByName('CtrlaSdo').AsInteger;
            //
            Node := UTreeView.LocalizaNodeInArrayNode((*''*)Nome, Codigo, 1, TreeView1);
            //
            if Node <> nil then
            begin
              NodeConjunto := NodePlano.AddChild('conjuntos', -1);
              NodeConjunto.Attributes['Codigo']   := Codigo;
              NodeConjunto.Attributes['Nome']     := Nome;
              NodeConjunto.Attributes['CtrlaSdo'] := CtrlaSdo;
              //
              //Carrega Grupo
              if (QrGrupo.State <> dsInactive) and (QrGrupo.RecordCount > 0) then
              begin
                QrGrupo.First;
                QrGrupo.Filter := 'Conjunto=' + Geral.FF0(Codigo);
                while not QrGrupo.EOF do
                begin
                  Codigo   := QrGrupo.FieldByName('Codigo').AsInteger;
                  Nome     := QrGrupo.FieldByName('Nome').AsString;
                  CtrlaSdo :=  QrGrupo.FieldByName('CtrlaSdo').AsInteger;
                  //
                  Node := UTreeView.LocalizaNodeInArrayNode((*''*)Nome, Codigo, 2, TreeView1);
                  //
                  if Node <> nil then
                  begin
                    NodeGrupo := NodeConjunto.AddChild('grupos', -1);
                    NodeGrupo.Attributes['Codigo']   := Codigo;
                    NodeGrupo.Attributes['Nome']     := Nome;
                    NodeGrupo.Attributes['CtrlaSdo'] := CtrlaSdo;
                    //
                    //Carrega Subgrupo
                    if (QrSubgrupo.State <> dsInactive) and (QrSubgrupo.RecordCount > 0) then
                    begin
                      QrSubgrupo.First;
                      QrSubgrupo.Filter := 'Grupo=' + Geral.FF0(Codigo);
                      while not QrSubgrupo.EOF do
                      begin
                        Codigo     := QrSubgrupo.FieldByName('Codigo').AsInteger;
                        Nome       := QrSubgrupo.FieldByName('Nome').AsString;
                        CtrlaSdo   := QrSubgrupo.FieldByName('CtrlaSdo').AsInteger;
                        TipoAgrupa := QrSubgrupo.FieldByName('TipoAgrupa').AsInteger;
                        //
                        Node := UTreeView.LocalizaNodeInArrayNode((*''*)Nome, Codigo, 3, TreeView1);
                        //
                        if Node <> nil then
                        begin
                          NodeSubgrupo := NodeGrupo.AddChild('subgrupos', -1);
                          NodeSubgrupo.Attributes['Codigo']     := Codigo;
                          NodeSubgrupo.Attributes['Nome']       := Nome;
                          NodeSubgrupo.Attributes['CtrlaSdo']   := CtrlaSdo;
                          NodeSubgrupo.Attributes['TipoAgrupa'] := TipoAgrupa;
                          //
                          //Carrega Conta
                          if (QrConta.State <> dsInactive) and (QrConta.RecordCount > 0) then
                          begin
                            QrConta.First;
                            QrConta.Filter := 'Subgrupo=' + Geral.FF0(Codigo);
                            while not QrConta.EOF do
                            begin
                              Codigo    := QrConta.FieldByName('Codigo').AsInteger;
                              Nome      := QrConta.FieldByName('Nome').AsString;
                              CtrlaSdo  := QrConta.FieldByName('CtrlaSdo').AsInteger;
                              Debito    := QrConta.FieldByName('Debito').AsString;
                              Credito   := QrConta.FieldByName('Credito').AsString;
                              Mensal    := QrConta.FieldByName('Mensal').AsString;
                              Nome2     := QrConta.FieldByName('Nome2').AsString;
                              Nome3     := QrConta.FieldByName('Nome3').AsString;
                              Sigla     := QrConta.FieldByName('Sigla').AsString;
                              CentroRes := QrConta.FieldByName('CentroRes').AsInteger;
                              PagRec    := QrConta.FieldByName('PagRec').AsInteger;
                              //
                              Node := UTreeView.LocalizaNodeInArrayNode((*''*)Nome, Codigo, 4, TreeView1);
                              //
                              if Node <> nil then
                              begin
                                NodeConta := NodeSubgrupo.AddChild('contas', -1);
                                NodeConta.Attributes['Codigo']    := Codigo;
                                NodeConta.Attributes['CtrlaSdo']  := CtrlaSdo;
                                NodeConta.Attributes['Debito']    := Debito;
                                NodeConta.Attributes['Credito']   := Credito;
                                NodeConta.Attributes['Mensal']    := Mensal;
                                NodeConta.Attributes['Nome2']     := Nome2;
                                NodeConta.Attributes['Nome3']     := Nome3;
                                NodeConta.Attributes['Sigla']     := Sigla;
                                NodeConta.Attributes['CentroRes'] := CentroRes;
                                NodeConta.Attributes['PagRec']    := PagRec;
                                NodeConta.Text := Nome;
                              end;
                              //
                              QrConta.Next;
                            end;
                          end;
                        end;
                        QrSubgrupo.Next;
                      end;
                    end;
                  end;
                  QrGrupo.Next;
                end;
              end;
            end;
            QrConjunto.Next;
          end;
        end;
      end;
      QrPlano.Next;
    end;
  end;
  if Diretorio <> '' then
  begin
    Arquivo := Diretorio + '\' + CO_XML_Tit + '_' + Geral.FDT(Now(), 26) + '.xml';
    //
    XML.SaveToFile(Arquivo);
    //
    if Geral.MB_Pergunta('Deseja abrir o diret�rio?') = ID_YES then
      Geral.AbreArquivo(Arquivo);
  end;
end;

procedure TFmPlanoImpExp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPlanoImpExp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType            := stLok;
  TreeView1.PopupMenu        := PMMenu;
  TreeView1.RightClickSelect := True;
  //
  MostraEdicao(stLok);
end;

procedure TFmPlanoImpExp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmPlanoImpExp.ImpedePorAnaliSinte(AnaliSinte: TAnaliSinte): Boolean;
var
  OK: Boolean;
begin
  case FKindAnaSin of
    TKindAnaSin.kasAouS:
    begin
      case AnaliSinte of
        TAnaliSinte.anasinAnalitico: OK := FAnaliSinte = 'A';
        TAnaliSinte.anasinSintetico: OK := FAnaliSinte = 'S';
        else OK := False;
      end;
    end;
    TKindAnaSin.kas1ou2:
    begin
      case AnaliSinte of
        TAnaliSinte.anasinAnalitico: OK := FAnaliSinte = '1';
        TAnaliSinte.anasinSintetico: OK := FAnaliSinte = '2';
        else OK := False;
      end;
    end;
  end;
  Result := ok = False;
  if Result then
    Geral.MB_Aviso('Importa��o abortada! Linha ' + Geral.FF0(FLinha) +
    sLineBreak + 'Item: ' + FxNiveis + sLineBreak +
    'Descri��o:"' + FNome + sLineBreak +
    'Anal�tico/Sint�tico n�o confere!');
end;

procedure TFmPlanoImpExp.MostraEdicao(Tipo: TSQLType);
begin
  case Tipo of
    stIns:
    begin
      BtExporta.Enabled := True;
    end;
    stLok:
    begin
      FArquivo := '';
      //
      BtCarrega.Enabled := True;
      BtImporta.Enabled := True;
      BtSalvar.Enabled  := False;
      BtExclui.Enabled  := False;
      BtExporta.Enabled := False;
    end;
  end;
end;

procedure TFmPlanoImpExp.PMMenuPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := TreeView1.Selected <> nil;
end;

procedure TFmPlanoImpExp.RedefineLivresTabela(Tabela: String);
begin
  // precisa?
end;

procedure TFmPlanoImpExp.Removeatual1Click(Sender: TObject);
begin
  ExcluiAtual(False);
end;

procedure TFmPlanoImpExp.SBArquivoCClick(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EdLoadCSVArqC.Text);
  Arquivo := ExtractFileName(EdLoadCSVArqC.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo CSV', '', [], Arquivo) then
  begin
    EdLoadCSVArqC.Text := Arquivo;
  end;
end;

procedure TFmPlanoImpExp.SbArquivoDClick(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EdLoadCSVArqD.Text);
  Arquivo := ExtractFileName(EdLoadCSVArqD.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo CSV', '', [], Arquivo) then
  begin
    EdLoadCSVArqD.Text := Arquivo;
  end;
end;

procedure TFmPlanoImpExp.SbCarregaCClick(Sender: TObject);
var
  Arquivo: String;
begin
  MeAvisosC.Lines.Clear;
  //
  Arquivo := EdLoadCSVArqC.Text;
  if FileExists(Arquivo) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo: ' + Arquivo + '.');
    MyObjects.Xls_To_StringGrid_Faster(GradeC, Arquivo, '', PB1,
    LaAviso1, LaAviso2, MeAvisosC, 1, 0);
    //
    BtInsereC.Enabled := True;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Arquivo carregado!');
  end;
end;

procedure TFmPlanoImpExp.SbCarregaDClick(Sender: TObject);
var
  Arquivo: String;
begin
  MeAvisosD.Lines.Clear;
  //
  Arquivo := EdLoadCSVArqD.Text;
  if FileExists(Arquivo) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo: ' + Arquivo + '.');
    MyObjects.Xls_To_StringGrid_Faster(GradeD, Arquivo, '', PB1,
    LaAviso1, LaAviso2, MeAvisosD, 1, 0);
    //
    BtInsereD.Enabled := True;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Arquivo carregado!');
  end;
end;

function TFmPlanoImpExp.SeparaNiveisENomesD(const Texto: String; var Niveis,
  Nome: String): Boolean;
var
  p: Integer;
begin
  Result := False;
  Niveis := EmptyStr;
  Nome   := EmptyStr;
  p := pos(' ', Texto);
  if p > 0 then
  begin
    Niveis := Trim(Copy(Texto, 1, p));
    Nome := Trim(Copy(Texto, p+1));
    while (Nome[1] in ([' ', '.'])) and (Nome <> EmptyStr) do
      Nome := Trim(Copy(Nome, 2));
    Result := true;
  end;
  //MeAvisosD.Text := 'Niveis: "' + Niveis + '"  - Nome: "' + Nome + '"'+ sLineBreak + MeAvisosD.Text;
  Application.ProcessMessages;
end;

end.
