object FmContasHistAtz: TFmContasHistAtz
  Left = 339
  Top = 185
  BorderStyle = bsDialog
  Caption = 'FIN-PLCTA-041 :: Atualiza'#231#227'o de Saldos de Contas'
  ClientHeight = 569
  ClientWidth = 794
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 794
    Height = 396
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGPesq: TDBGrid
      Left = 0
      Top = 277
      Width = 794
      Height = 119
      Align = alBottom
      DataSource = DsPesq
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Visible = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sub'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Documento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Carteira'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Visible = True
        end>
    end
    object DBGOrfaos: TDBGrid
      Left = 0
      Top = 0
      Width = 794
      Height = 277
      Align = alClient
      DataSource = DsOrfaos
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sub'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_Pgto'
          Title.Caption = 'ID Pgto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_Sub'
          Title.Caption = 'Sub'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Documento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Carteira'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 794
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 746
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 698
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 401
        Height = 32
        Caption = 'Atualiza'#231#227'o de Saldos de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 401
        Height = 32
        Caption = 'Atualiza'#231#227'o de Saldos de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 401
        Height = 32
        Caption = 'Atualiza'#231#227'o de Saldos de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 444
    Width = 794
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 790
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 790
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 499
    Width = 794
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 790
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 646
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtLocaliza: TBitBtn
        Tag = 18
        Left = 20
        Top = 4
        Width = 180
        Height = 40
        Caption = '&Localiza lancto de origem'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtLocalizaClick
      end
      object BtLinkar: TBitBtn
        Tag = 311
        Left = 354
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Linkar'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtLinkarClick
      end
    end
  end
  object QrOrfaos: TmySQLQuery
    Database = Dmod.MyDB
    Left = 316
    Top = 136
    object QrOrfaosData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOrfaosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOrfaosSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrOrfaosID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrOrfaosID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrOrfaosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOrfaosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOrfaosDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrOrfaosCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrOrfaosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object DsOrfaos: TDataSource
    DataSet = QrOrfaos
    Left = 344
    Top = 136
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 152
    Top = 124
    object QrLocGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    Left = 316
    Top = 172
    object QrPesqData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrPesqCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesqDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesqDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPesqCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrPesqDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPesqGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 344
    Top = 172
  end
end
