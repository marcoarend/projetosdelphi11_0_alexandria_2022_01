object FmContasAgr: TFmContasAgr
  Left = 368
  Top = 194
  Caption = 'FIN-PLCTA-035 :: Agrupamentos Extras de Contas'
  ClientHeight = 353
  ClientWidth = 722
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 722
    Height = 257
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 194
      Width = 722
      Height = 63
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 612
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 722
      Height = 169
      Align = alTop
      TabOrder = 1
      object Label9: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 120
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 16
        Top = 56
        Width = 89
        Height = 13
        Caption = 'Texto para cr'#233'dito:'
      end
      object Label5: TLabel
        Left = 16
        Top = 96
        Width = 86
        Height = 13
        Caption = 'Texto para d'#233'bito:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TEdit
        Left = 120
        Top = 32
        Width = 280
        Height = 21
        MaxLength = 255
        TabOrder = 1
      end
      object RGInfoDescri: TRadioGroup
        Left = 408
        Top = 28
        Width = 157
        Height = 105
        Caption = ' Complemento informativo: '
        Items.Strings = (
          'Nenhum'
          'Unidade habitacional'
          'Propriet'#225'rio do im'#243'vel')
        TabOrder = 4
      end
      object CkMensal: TCheckBox
        Left = 15
        Top = 139
        Width = 97
        Height = 17
        Caption = 'Mensal.'
        TabOrder = 5
      end
      object EdTextoCred: TEdit
        Left = 16
        Top = 72
        Width = 385
        Height = 21
        MaxLength = 255
        TabOrder = 2
      end
      object EdTextoDebi: TEdit
        Left = 16
        Top = 112
        Width = 385
        Height = 21
        MaxLength = 255
        TabOrder = 3
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 722
    Height = 257
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object GBCntrl: TGroupBox
      Left = 0
      Top = 193
      Width = 722
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 290
        Top = 15
        Width = 430
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 321
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 722
      Height = 169
      Align = alTop
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 120
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 89
        Height = 13
        Caption = 'Texto para cr'#233'dito:'
      end
      object Label6: TLabel
        Left = 16
        Top = 96
        Width = 86
        Height = 13
        Caption = 'Texto para d'#233'bito:'
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 32
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsContasAgr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        MaxLength = 1
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 120
        Top = 32
        Width = 281
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsContasAgr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 18
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBCheckBox1: TDBCheckBox
        Left = 16
        Top = 140
        Width = 61
        Height = 17
        Caption = 'Mensal.'
        DataField = 'Mensal'
        DataSource = DsContasAgr
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 385
        Height = 21
        DataField = 'TextoCred'
        DataSource = DsContasAgr
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 16
        Top = 112
        Width = 385
        Height = 21
        DataField = 'TextoDebi'
        DataSource = DsContasAgr
        TabOrder = 4
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 408
        Top = 28
        Width = 157
        Height = 105
        Caption = ' Complemento informativo: '
        DataField = 'InfoDescri'
        DataSource = DsContasAgr
        Items.Strings = (
          'Nenhum'
          'Unidade habitacional'
          'Propriet'#225'rio do im'#243'vel')
        ParentBackground = True
        TabOrder = 5
        Values.Strings = (
          '0'
          '1'
          '2')
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 722
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 674
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 458
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 392
        Height = 32
        Caption = 'Agrupamentos Extras de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 392
        Height = 32
        Caption = 'Agrupamentos Extras de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 392
        Height = 32
        Caption = 'Agrupamentos Extras de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 722
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 718
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsContasAgr: TDataSource
    DataSet = QrContasAgr
    Left = 488
    Top = 161
  end
  object QrContasAgr: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrContasAgrBeforeOpen
    AfterOpen = QrContasAgrAfterOpen
    AfterScroll = QrContasAgrAfterScroll
    OnCalcFields = QrContasAgrCalcFields
    SQL.Strings = (
      'SELECT * FROM contasagr'
      'WHERE Codigo > 0')
    Left = 460
    Top = 161
    object QrContasAgrLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'contasagr.Lk'
    end
    object QrContasAgrDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'contasagr.DataCad'
    end
    object QrContasAgrDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'contasagr.DataAlt'
    end
    object QrContasAgrUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'contasagr.UserCad'
    end
    object QrContasAgrUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'contasagr.UserAlt'
    end
    object QrContasAgrCodigo: TSmallintField
      FieldName = 'Codigo'
      Origin = 'contasagr.Codigo'
    end
    object QrContasAgrNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contasagr.Nome'
      Size = 30
    end
    object QrContasAgrInfoDescri: TSmallintField
      FieldName = 'InfoDescri'
      Origin = 'contasagr.InfoDescri'
    end
    object QrContasAgrMensal: TSmallintField
      FieldName = 'Mensal'
      Origin = 'contasagr.Mensal'
    end
    object QrContasAgrNOMEINFODESCRI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEINFODESCRI'
      Size = 30
      Calculated = True
    end
    object QrContasAgrTextoCred: TWideStringField
      FieldName = 'TextoCred'
      Origin = 'contasagr.TextoCred'
      Size = 50
    end
    object QrContasAgrTextoDebi: TWideStringField
      FieldName = 'TextoDebi'
      Origin = 'contasagr.TextoDebi'
      Size = 50
    end
  end
end
