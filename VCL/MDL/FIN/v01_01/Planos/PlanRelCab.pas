unit PlanRelCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, UnDmkProcFunc, dmkImage,
  UnDmkEnums;

type
  TFmPlanRelCab = class(TForm)
    PnDados: TPanel;
    DsPlanRelCab: TDataSource;
    QrPlanRelCab: TmySQLQuery;
    PnEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtSeleciona: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrPlanRelCabCodigo: TIntegerField;
    QrPlanRelCabCodUsu: TIntegerField;
    QrPlanRelCabNome: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPlanRelCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPlanRelCabBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmPlanRelCab: TFmPlanRelCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPlanRelCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPlanRelCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPlanRelCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPlanRelCab.DefParams;
begin
  VAR_GOTOTABELA := 'planrelcab';
  VAR_GOTOMYSQLTABLE := QrPlanRelCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome');
  VAR_SQLx.Add('FROM planrelcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmPlanRelCab.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'planrelcab', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmPlanRelCab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      GBCntrl.Visible := True;
      PnDados.Visible := True;
      PnEdita.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      GBCntrl.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Erro('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPlanRelCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPlanRelCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPlanRelCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPlanRelCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPlanRelCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPlanRelCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPlanRelCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPlanRelCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPlanRelCab.BtAlteraClick(Sender: TObject);
begin
  if (QrPlanRelCab.State <> dsInactive) and (QrPlanRelCab.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrPlanRelCab, [PnDados],
      [PnEdita], EdCodUsu, ImgTipo, 'planrelcab');
  end;
end;

procedure TFmPlanRelCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPlanRelCabCodigo.Value;
  Close;
end;

procedure TFmPlanRelCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, CodUsu: Integer;
begin
  if DModG.VerificaDuplicidadeCodUsu('planrelcab', 'Nome', ['CodUsu'], EdNome,
    [EdCodUsu.ValueVariant], ImgTipo, True)
  then
    Exit;
  //
  CodUsu := EdCodUsu.ValueVariant;
  //
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Defina uma descri��o!') then
    Exit;
  //
  Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'planrelcab', 'Codigo', [], [],
    ImgTipo.SQLType, EdCodigo.ValueVariant, siPositivo, EdCodigo);
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmPlanRelCab, GBEdita,
  'planrelcab', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPlanRelCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'planrelcab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'planrelcab', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'planrelcab', 'Codigo');
end;

procedure TFmPlanRelCab.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, GBEdita, QrPlanRelCab, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'planrelcab');
  //
  UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'planrelcab', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmPlanRelCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmPlanRelCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPlanRelCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPlanRelCab.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmPlanRelCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPlanRelCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPlanRelCabCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmPlanRelCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPlanRelCab.QrPlanRelCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPlanRelCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPlanRelCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPlanRelCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'planrelcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPlanRelCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPlanRelCab.QrPlanRelCabBeforeOpen(DataSet: TDataSet);
begin
  QrPlanRelCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

