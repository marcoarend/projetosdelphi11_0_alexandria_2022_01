object FmContasHistSdo3: TFmContasHistSdo3
  Left = 370
  Top = 177
  Caption = 'FIN-PLCTA-011 :: Hist'#243'rico de Saldos do Plano de Contas'
  ClientHeight = 720
  ClientWidth = 1135
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 1135
    Height = 521
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 1135
      Height = 55
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaCliInt: TLabel
        Left = 10
        Top = 4
        Width = 87
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente interno:'
      end
      object EdCliInt: TdmkEditCB
        Left = 10
        Top = 23
        Width = 70
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfLongint
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCliIntChange
        OnExit = EdCliIntExit
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 84
        Top = 23
        Width = 907
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'CliInt'
        ListField = 'NOMECLIINT'
        ListSource = DsCliInt
        TabOrder = 1
        dmkEditCB = EdCliInt
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object BtAtualiza: TBitBtn
        Tag = 20
        Left = 994
        Top = 5
        Width = 111
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Atuali&za'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtAtualizaClick
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 55
      Width = 1135
      Height = 466
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      Visible = False
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Peri'#243'dico '
        object PageControl2: TPageControl
          Left = 0
          Top = 0
          Width = 1127
          Height = 435
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet3
          Align = alClient
          TabOrder = 0
          OnChange = PageControl2Change
          object TabSheet3: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Por per'#237'odo '
            object Panel3: TPanel
              Left = 0
              Top = 0
              Width = 1119
              Height = 60
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label16: TLabel
                Left = 59
                Top = 4
                Width = 30
                Height = 17
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'pq'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'Wingdings 3'
                Font.Style = []
                ParentFont = False
              end
              object Label1: TLabel
                Left = 5
                Top = 4
                Width = 51
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Per'#237'odo:'
              end
              object EdPeriodo: TdmkEdit
                Left = 5
                Top = 23
                Width = 44
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdPeriodoChange
              end
              object Edit1: TEdit
                Left = 49
                Top = 23
                Width = 208
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ReadOnly = True
                TabOrder = 1
              end
              object BtImprimeP: TBitBtn
                Tag = 5
                Left = 265
                Top = 4
                Width = 110
                Height = 49
                Cursor = crHandPoint
                Hint = 'Sai da janela atual'
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Imprime'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtImprimePClick
              end
              object CkControlados: TCheckBox
                Left = 383
                Top = 20
                Width = 160
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Somente controlados.'
                TabOrder = 3
                OnClick = CkControladosClick
              end
              object PB1: TProgressBar
                Left = 553
                Top = 20
                Width = 307
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 4
              end
              object PB2: TProgressBar
                Left = 860
                Top = 20
                Width = 123
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 5
              end
              object PB3: TProgressBar
                Left = 983
                Top = 20
                Width = 74
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 6
              end
              object PB4: TProgressBar
                Left = 1057
                Top = 20
                Width = 40
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 7
              end
              object PB5: TProgressBar
                Left = 1097
                Top = 20
                Width = 19
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 8
              end
            end
            object TabControl1: TTabControl
              Left = 0
              Top = 60
              Width = 1119
              Height = 344
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              TabOrder = 1
              Tabs.Strings = (
                ' Planos '
                ' Conjuntos '
                ' Grupos '
                ' Sub-grupos '
                ' Contas ')
              TabIndex = 0
              OnChange = TabControl1Change
              object dmkDBPer: TdmkDBGrid
                Left = 4
                Top = 27
                Width = 1111
                Height = 313
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NOME'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 346
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SDOANT'
                    Title.Caption = 'Saldo inicial m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SUMCRE'
                    Title.Caption = 'Cr'#233'ditos m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SUMDEB'
                    Title.Caption = 'D'#233'bitos m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SUMMOV'
                    Title.Caption = 'Resultado m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SDOFIM'
                    Title.Caption = 'Saldo final m'#234's'
                    Width = 100
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsPerPla
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NOME'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 346
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SDOANT'
                    Title.Caption = 'Saldo inicial m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SUMCRE'
                    Title.Caption = 'Cr'#233'ditos m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SUMDEB'
                    Title.Caption = 'D'#233'bitos m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SUMMOV'
                    Title.Caption = 'Resultado m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SDOFIM'
                    Title.Caption = 'Saldo final m'#234's'
                    Width = 100
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet4: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Plano '
            ImageIndex = 1
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 1119
              Height = 60
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label3: TLabel
                Left = 5
                Top = 4
                Width = 100
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Plano de contas:'
              end
              object EdPla: TdmkEditCB
                Left = 5
                Top = 23
                Width = 70
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCliIntChange
                DBLookupComboBox = CBPla
                IgnoraDBLookupComboBox = False
              end
              object CBPla: TdmkDBLookupComboBox
                Left = 78
                Top = 23
                Width = 907
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsPlanos
                TabOrder = 1
                dmkEditCB = EdPla
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object dmkDBGrid6: TdmkDBGrid
              Left = 0
              Top = 60
              Width = 1119
              Height = 344
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsPlaSdo
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
            end
          end
          object TabSheet5: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Conjuntos '
            ImageIndex = 2
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 1119
              Height = 60
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label2: TLabel
                Left = 5
                Top = 4
                Width = 55
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Conjunto:'
              end
              object EdCjt: TdmkEditCB
                Left = 5
                Top = 23
                Width = 70
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCliIntChange
                DBLookupComboBox = CBCjt
                IgnoraDBLookupComboBox = False
              end
              object CBCjt: TdmkDBLookupComboBox
                Left = 79
                Top = 23
                Width = 907
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCjutos
                TabOrder = 1
                dmkEditCB = EdCjt
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object dmkDBGrid2: TdmkDBGrid
              Left = 0
              Top = 60
              Width = 730
              Height = 344
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsCjtSdo
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
            end
            object dmkDBGrid7: TdmkDBGrid
              Left = 730
              Top = 60
              Width = 389
              Height = 344
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME'
                  Title.Caption = 'Descri'#231#227'o da conta'
                  Width = 266
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsCjtCta
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 2
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME'
                  Title.Caption = 'Descri'#231#227'o da conta'
                  Width = 266
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
            end
          end
          object TabSheet6: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Grupo '
            ImageIndex = 3
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 1119
              Height = 60
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label4: TLabel
                Left = 5
                Top = 4
                Width = 40
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Grupo:'
              end
              object EdGru: TdmkEditCB
                Left = 5
                Top = 23
                Width = 70
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCliIntChange
                DBLookupComboBox = CBGru
                IgnoraDBLookupComboBox = False
              end
              object CBGru: TdmkDBLookupComboBox
                Left = 79
                Top = 23
                Width = 907
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsGrupos
                TabOrder = 1
                dmkEditCB = EdGru
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object dmkDBGrid3: TdmkDBGrid
              Left = 0
              Top = 60
              Width = 1119
              Height = 344
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsGruSdo
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
            end
          end
          object TabSheet7: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Sub-grupo '
            ImageIndex = 4
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 1119
              Height = 60
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label5: TLabel
                Left = 5
                Top = 4
                Width = 66
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Sub-grupo:'
              end
              object EdSgr: TdmkEditCB
                Left = 5
                Top = 23
                Width = 70
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCliIntChange
                DBLookupComboBox = CBSgr
                IgnoraDBLookupComboBox = False
              end
              object CBSgr: TdmkDBLookupComboBox
                Left = 79
                Top = 23
                Width = 907
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsSubGru
                TabOrder = 1
                dmkEditCB = EdSgr
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object dmkDBGrid4: TdmkDBGrid
              Left = 0
              Top = 60
              Width = 1119
              Height = 344
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsSgrSdo
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
            end
          end
          object TabSheet8: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Conta '
            ImageIndex = 5
            object Panel9: TPanel
              Left = 0
              Top = 0
              Width = 1119
              Height = 60
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label6: TLabel
                Left = 5
                Top = 4
                Width = 38
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Conta:'
              end
              object EdCta: TdmkEditCB
                Left = 5
                Top = 23
                Width = 70
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCliIntChange
                DBLookupComboBox = CBCta
                IgnoraDBLookupComboBox = False
              end
              object CBCta: TdmkDBLookupComboBox
                Left = 79
                Top = 23
                Width = 907
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsContas
                TabOrder = 1
                dmkEditCB = EdCta
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object dmkDBGrid5: TdmkDBGrid
              Left = 0
              Top = 60
              Width = 1119
              Height = 344
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsCtaSdo
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Atual (Final) '
        ImageIndex = 1
        object BtImprimeA: TBitBtn
          Tag = 5
          Left = 11
          Top = 4
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Imprime'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtImprimeAClick
        end
      end
      object TabSheet10: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Saldos iniciais '
        ImageIndex = 2
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 1127
          Height = 75
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 804
            Height = 75
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Caption = 
              ' Pesquisa (digite parte da descri'#231#227'o dos n'#237'veis de plano desejad' +
              'os):'
            TabOrder = 0
            object Label7: TLabel
              Left = 10
              Top = 18
              Width = 45
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Contas:'
            end
            object Label8: TLabel
              Left = 167
              Top = 18
              Width = 73
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Sub-grupos:'
            end
            object Label9: TLabel
              Left = 325
              Top = 18
              Width = 47
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Grupos:'
            end
            object Label10: TLabel
              Left = 482
              Top = 18
              Width = 62
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Conjuntos:'
            end
            object Label11: TLabel
              Left = 640
              Top = 18
              Width = 45
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Planos:'
            end
            object EdPesqCta: TEdit
              Left = 10
              Top = 38
              Width = 154
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 0
              OnChange = EdPesqCtaChange
            end
            object EdPesqSgr: TEdit
              Left = 167
              Top = 38
              Width = 154
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 1
              OnChange = EdPesqCtaChange
            end
            object EdPesqGru: TEdit
              Left = 325
              Top = 38
              Width = 154
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 2
              OnChange = EdPesqCtaChange
            end
            object EdPesqCjt: TEdit
              Left = 482
              Top = 38
              Width = 154
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 3
              OnChange = EdPesqCtaChange
            end
            object EdPesqPla: TEdit
              Left = 640
              Top = 38
              Width = 154
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 4
              OnChange = EdPesqCtaChange
            end
          end
          object BtAltera: TBitBtn
            Tag = 11
            Left = 822
            Top = 18
            Width = 111
            Height = 50
            Cursor = crHandPoint
            Hint = 'Altera registro atual'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Altera'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAlteraClick
          end
        end
        object GradeSdoIni: TdmkDBGrid
          Left = 0
          Top = 75
          Width = 1127
          Height = 360
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Conta'
              Width = 39
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECTA'
              Title.Caption = 'Descri'#231#227'o da conta'
              Width = 289
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoIni'
              Title.Caption = 'Saldo inicial'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESGR'
              Title.Caption = 'Sub-grupo'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEGRU'
              Title.Caption = 'Grupo'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECJT'
              Title.Caption = 'Conjunto'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPLA'
              Title.Caption = 'Plano'
              Width = 200
              Visible = True
            end>
          Color = clWindow
          DataSource = DsSdoIni
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Conta'
              Width = 39
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECTA'
              Title.Caption = 'Descri'#231#227'o da conta'
              Width = 289
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoIni'
              Title.Caption = 'Saldo inicial'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESGR'
              Title.Caption = 'Sub-grupo'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEGRU'
              Title.Caption = 'Grupo'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECJT'
              Title.Caption = 'Conjunto'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPLA'
              Title.Caption = 'Plano'
              Width = 200
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 580
    Width = 1135
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel2: TPanel
      Left = 2
      Top = 18
      Width = 1131
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 634
    Width = 1135
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 956
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel11: TPanel
      Left = 2
      Top = 18
      Width = 954
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn1: TBitBtn
        Tag = 125
        Left = 17
        Top = 4
        Width = 148
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Config.'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BitBtn1Click
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1135
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 1076
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1017
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 566
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Hist'#243'rico de Saldos do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 566
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Hist'#243'rico de Saldos do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 566
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Hist'#243'rico de Saldos do Plano de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.CliInt, ent.Codigo Entidade,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLIINT'
      'FROM entidades ent '
      'WHERE '
      '('
      '  ent.CliInt <> 0'
      '  OR'
      '  ent.Codigo < -10'
      ')'
      'ORDER BY NOMECLIINT')
    Left = 13
    Top = 13
    object QrCliIntEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrCliIntNOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Size = 100
    end
    object QrCliIntCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 41
    Top = 13
  end
  object QrPerPla: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pla.Nome NOME, SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE, '
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND pla.CtrlaSdo>:P2'
      ''
      'GROUP BY pla.Codigo'
      'ORDER BY pla.OrdemLista, pla.Nome')
    Left = 33
    Top = 294
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPerPlaNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrPerPlaSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerPlaSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerPlaSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerPlaSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerPlaSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsPerPla: TDataSource
    DataSet = QrPerPla
    Left = 61
    Top = 294
  end
  object QrPerCjt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cjt.Nome NOME, SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND cjt.CtrlaSdo>:P2'
      ''
      'GROUP BY cjt.Codigo'
      'ORDER BY cjt.OrdemLista, cjt.Nome')
    Left = 33
    Top = 322
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPerCjtNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrPerCjtSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCjtSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCjtSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCjtSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCjtSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsPerCjt: TDataSource
    DataSet = QrPerCjt
    Left = 61
    Top = 322
  end
  object QrPerGru: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gru.Nome NOME, SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND gru.CtrlaSdo>:P2'
      ''
      'GROUP BY gru.Codigo'
      'ORDER BY gru.OrdemLista, gru.Nome')
    Left = 33
    Top = 350
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPerGruNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrPerGruSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerGruSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerGruSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerGruSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerGruSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsPerGru: TDataSource
    DataSet = QrPerGru
    Left = 61
    Top = 350
  end
  object QrPerCta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.Nome NOME, SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND cta.CtrlaSdo>:P2'
      ''
      'GROUP BY cta.Codigo'
      'ORDER BY cta.OrdemLista, cta.Nome')
    Left = 33
    Top = 406
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPerCtaNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrPerCtaSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCtaSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCtaSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCtaSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCtaSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object QrPerSgr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sgr.Nome NOME, SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND sgr.CtrlaSdo>:P2'
      ''
      'GROUP BY sgr.Codigo'
      'ORDER BY sgr.OrdemLista, sgr.Nome')
    Left = 33
    Top = 378
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPerSgrNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrPerSgrSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerSgrSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerSgrSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerSgrSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerSgrSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsPerSgr: TDataSource
    DataSet = QrPerSgr
    Left = 61
    Top = 378
  end
  object DsPerCta: TDataSource
    DataSet = QrPerCta
    Left = 61
    Top = 406
  end
  object QrPlaSdo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT his.Periodo,'
      'IF(MOD(his.periodo, 12)=0, '
      ' CONCAT('#39'12 / '#39',(2000+FLOOR(his.periodo / 12)-1)),'
      ' CONCAT(LPAD(MOD(his.periodo, 12), 2, "0"), '#39' / '#39','
      '  (2000+FLOOR(his.periodo / 12)))'
      ') MMYYYY,'
      'SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE, '
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano'
      'WHERE his.Entidade=:P0'
      'AND pla.Codigo=:P1'
      ''
      'GROUP BY his.Periodo'
      'ORDER BY his.Periodo DESC')
    Left = 117
    Top = 294
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPlaSdoPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrPlaSdoMMYYYY: TWideStringField
      FieldName = 'MMYYYY'
      Size = 37
    end
    object QrPlaSdoSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPlaSdoSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPlaSdoSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPlaSdoSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPlaSdoSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsPlaSdo: TDataSource
    DataSet = QrPlaSdo
    Left = 145
    Top = 294
  end
  object QrPlanos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM plano'
      'ORDER BY Nome')
    Left = 173
    Top = 294
    object QrPlanosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPlanosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPlanos: TDataSource
    DataSet = QrPlanos
    Left = 201
    Top = 294
  end
  object QrCjtSdo: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCjtSdoAfterScroll
    SQL.Strings = (
      'SELECT his.Periodo,'
      'IF(MOD(his.periodo, 12)=0, '
      ' CONCAT('#39'12 / '#39',(2000+FLOOR(his.periodo / 12)-1)),'
      ' CONCAT(LPAD(MOD(his.periodo, 12), 2, "0"), '#39' / '#39','
      '  (2000+FLOOR(his.periodo / 12)))'
      ') MMYYYY,'
      'SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE, '
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'WHERE his.Entidade=:P0'
      'AND cjt.Codigo=:P1'
      ''
      'GROUP BY his.Periodo'
      'ORDER BY his.Periodo DESC')
    Left = 117
    Top = 322
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCjtSdoPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrCjtSdoMMYYYY: TWideStringField
      FieldName = 'MMYYYY'
      Size = 37
    end
    object QrCjtSdoSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtSdoSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtSdoSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtSdoSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtSdoSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsCjtSdo: TDataSource
    DataSet = QrCjtSdo
    Left = 145
    Top = 322
  end
  object QrCjutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM conjuntos'
      'ORDER BY Nome')
    Left = 173
    Top = 322
    object QrCjutosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCjutosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCjutos: TDataSource
    DataSet = QrCjutos
    Left = 201
    Top = 322
  end
  object QrGruSdo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT his.Periodo,'
      'IF(MOD(his.periodo, 12)=0, '
      ' CONCAT('#39'12 / '#39',(2000+FLOOR(his.periodo / 12)-1)),'
      ' CONCAT(LPAD(MOD(his.periodo, 12), 2, "0"), '#39' / '#39','
      '  (2000+FLOOR(his.periodo / 12)))'
      ') MMYYYY,'
      'SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE, '
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'WHERE his.Entidade=:P0'
      'AND gru.Codigo=:P1'
      ''
      'GROUP BY his.Periodo'
      'ORDER BY his.Periodo DESC')
    Left = 117
    Top = 350
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGruSdoPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrGruSdoMMYYYY: TWideStringField
      FieldName = 'MMYYYY'
      Size = 37
    end
    object QrGruSdoSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrGruSdoSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrGruSdoSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrGruSdoSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrGruSdoSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsGruSdo: TDataSource
    DataSet = QrGruSdo
    Left = 145
    Top = 350
  end
  object QrGrupos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM grupos'
      'ORDER BY Nome')
    Left = 173
    Top = 350
    object QrGruposCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruposNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 201
    Top = 350
  end
  object QrSgrSdo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT his.Periodo,'
      'IF(MOD(his.periodo, 12)=0, '
      ' CONCAT('#39'12 / '#39',(2000+FLOOR(his.periodo / 12)-1)),'
      ' CONCAT(LPAD(MOD(his.periodo, 12), 2, "0"), '#39' / '#39','
      '  (2000+FLOOR(his.periodo / 12)))'
      ') MMYYYY,'
      'SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE, '
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'WHERE his.Entidade=:P0'
      'AND sgr.Codigo=:P1'
      ''
      'GROUP BY his.Periodo'
      'ORDER BY his.Periodo DESC')
    Left = 117
    Top = 378
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSgrSdoPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrSgrSdoMMYYYY: TWideStringField
      FieldName = 'MMYYYY'
      Size = 37
    end
    object QrSgrSdoSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrSgrSdoSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrSgrSdoSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrSgrSdoSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrSgrSdoSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsSgrSdo: TDataSource
    DataSet = QrSgrSdo
    Left = 145
    Top = 378
  end
  object QrSubgru: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM subgrupos'
      'ORDER BY Nome')
    Left = 173
    Top = 378
    object QrSubgruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSubgruNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsSubGru: TDataSource
    DataSet = QrSubgru
    Left = 201
    Top = 378
  end
  object QrCtaSdo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT his.Periodo,'
      'IF(MOD(his.periodo, 12)=0, '
      ' CONCAT('#39'12 / '#39',(2000+FLOOR(his.periodo / 12)-1)),'
      ' CONCAT(LPAD(MOD(his.periodo, 12), 2, "0"), '#39' / '#39','
      '  (2000+FLOOR(his.periodo / 12)))'
      ') MMYYYY,'
      'SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE, '
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'WHERE his.Entidade=:P0'
      'AND cta.Codigo=:P1'
      ''
      'GROUP BY his.Periodo'
      'ORDER BY his.Periodo DESC')
    Left = 117
    Top = 406
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCtaSdoPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrCtaSdoMMYYYY: TWideStringField
      FieldName = 'MMYYYY'
      Size = 37
    end
    object QrCtaSdoSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCtaSdoSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCtaSdoSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCtaSdoSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCtaSdoSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsCtaSdo: TDataSource
    DataSet = QrCtaSdo
    Left = 145
    Top = 406
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome')
    Left = 173
    Top = 406
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 201
    Top = 406
  end
  object QrSdoIni: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSdoIniAfterOpen
    BeforeClose = QrSdoIniBeforeClose
    SQL.Strings = (
      'SELECT cta.Codigo, sdo.Codigo CODSDO, '
      'sdo.SdoIni, cta.Nome NOMECTA, '
      'sgr.Nome NOMESGR, gru.Nome NOMEGRU, '
      'cjt.Nome NOMECJT, pla.Nome NOMEPLA '
      'FROM contassdo sdo'
      'LEFT JOIN contas    cta ON cta.Codigo=sdo.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano'
      'WHERE sdo.Entidade=1'
      'AND cta.Nome LIKE "%%"'
      'AND sgr.Nome LIKE "%%"'
      'AND gru.Nome LIKE "%%"'
      'AND cjt.Nome LIKE "%%"'
      'AND pla.Nome LIKE "%%"'
      'ORDER BY cta.Nome')
    Left = 173
    Top = 265
    object QrSdoIniCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
      DisplayFormat = '000'
    end
    object QrSdoIniCODSDO: TIntegerField
      FieldName = 'CODSDO'
      Required = True
    end
    object QrSdoIniSdoIni: TFloatField
      FieldName = 'SdoIni'
      Required = True
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrSdoIniNOMECTA: TWideStringField
      FieldName = 'NOMECTA'
      Size = 50
    end
    object QrSdoIniNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrSdoIniNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrSdoIniNOMECJT: TWideStringField
      FieldName = 'NOMECJT'
      Size = 50
    end
    object QrSdoIniNOMEPLA: TWideStringField
      FieldName = 'NOMEPLA'
      Size = 50
    end
  end
  object DsSdoIni: TDataSource
    DataSet = QrSdoIni
    Left = 201
    Top = 265
  end
  object QrCjtCta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.Codigo Conta, cta.Nome NOME, '
      'SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND cjt.Codigo=:P2'
      'GROUP BY cta.Codigo'
      'ORDER BY cjt.OrdemLista, cjt.Nome, '
      '         gru.OrdemLista, gru.Nome,'
      '         sgr.OrdemLista, sgr.Nome,'
      '         cta.OrdemLista, cta.Nome')
    Left = 229
    Top = 321
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCjtCtaNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrCjtCtaSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtCtaSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtCtaSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtCtaSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtCtaSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtCtaConta: TIntegerField
      FieldName = 'Conta'
      Required = True
      DisplayFormat = '000'
    end
  end
  object DsCjtCta: TDataSource
    DataSet = QrCjtCta
    Left = 257
    Top = 321
  end
  object frxDsPerPla: TfrxDBDataset
    UserName = 'frxDsPerPla'
    CloseDataSource = False
    DataSource = DsPerPla
    BCDToCurrency = False
    Left = 397
    Top = 262
  end
  object frxPerPla: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39655.525740740700000000
    ReportOptions.LastChange = 39655.525740740700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxPerPlaGetValue
    Left = 369
    Top = 262
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPerPla
        DataSetName = 'frxDsPerPla'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageHeader9: TfrxPageHeader
        FillType = ftBrush
        Height = 139.842610000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape7: TfrxShapeView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo214: TfrxMemoView
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line24: TfrxLineView
          Left = 75.590600000000000000
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          Left = 226.771800000000000000
          Top = 56.692949999999990000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldos de [VARF_NIVEL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          Left = 83.149660000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo292: TfrxMemoView
          Left = 604.724800000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          Left = 75.590600000000000000
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo294: TfrxMemoView
          Left = 75.590600000000000000
          Top = 102.047310000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 377.953000000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo anterior')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 453.543600000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'ditos')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 75.590600000000000000
          Top = 120.944960000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 604.724800000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Result. per'#237'odo')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 680.315400000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo final')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 529.134200000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bitos')
          ParentFont = False
        end
      end
      object MasterData12: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 219.212740000000000000
        Width = 793.701300000000000000
        DataSet = frxDsPerPla
        DataSetName = 'frxDsPerPla'
        RowCount = 0
        object Memo299: TfrxMemoView
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPerPla."SDOANT"]')
          ParentFont = False
        end
        object Memo300: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'SUMCRE'
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPerPla."SUMCRE"]')
          ParentFont = False
        end
        object Memo301: TfrxMemoView
          Left = 75.590600000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          DataField = 'NOME'
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPerPla."NOME"]')
          ParentFont = False
        end
        object Memo302: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPerPla."SDOFIM"]')
          ParentFont = False
        end
        object Memo303: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPerPla."SUMMOV"]')
          ParentFont = False
        end
        object Memo305: TfrxMemoView
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPerPla."SUMDEB"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 49.133890000000000000
        Top = 351.496290000000000000
        Width = 793.701300000000000000
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 298.582870000000000000
        Width = 793.701300000000000000
        object Memo7: TfrxMemoView
          Left = 377.953000000000000000
          Top = 11.338590000000120000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPerPla."SDOANT">,MasterData12)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 75.590600000000000000
          Top = 11.338590000000120000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAIS')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 453.543600000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPerPla."SUMCRE">,MasterData12)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 529.134200000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPerPla."SUMDEB">,MasterData12)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 604.724800000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPerPla."SUMMOV">,MasterData12)]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 680.315400000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPerPla."SDOFIM">,MasterData12)]')
          ParentFont = False
        end
      end
    end
  end
  object PMImpNivel: TPopupMenu
    Left = 217
    Top = 165
    object Estenvel1: TMenuItem
      Caption = '&Este n'#237'vel'
      OnClick = Estenvel1Click
    end
    object Todosnveis1: TMenuItem
      Caption = '&Todos n'#237'veis'
      OnClick = Todosnveis1Click
    end
    object Generoscontrolados1: TMenuItem
      Caption = '&Generos controlados'
      OnClick = Generoscontrolados1Click
    end
  end
  object frxSNG: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39655.525740740700000000
    ReportOptions.LastChange = 39655.525740740700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxPerPlaGetValue
    Left = 425
    Top = 262
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = DModFin.frxDsSNG
        DataSetName = 'frxDsSNG'
      end
      item
        DataSet = DModFin.frxDsSTCP
        DataSetName = 'frxDsSTCP'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageHeader9: TfrxPageHeader
        FillType = ftBrush
        Height = 139.842610000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape7: TfrxShapeView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo214: TfrxMemoView
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line24: TfrxLineView
          Left = 75.590600000000000000
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          Left = 226.771800000000000000
          Top = 56.692949999999990000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldos dos N'#237'veis do Plano de Contas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          Left = 83.149660000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo292: TfrxMemoView
          Left = 604.724800000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          Left = 75.590600000000000000
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo294: TfrxMemoView
          Left = 75.590600000000000000
          Top = 102.047310000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 377.953000000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo anterior')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 453.543600000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'ditos')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 75.590600000000000000
          Top = 120.944960000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 604.724800000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Result. per'#237'odo')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 680.315400000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo final')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 529.134200000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bitos')
          ParentFont = False
        end
      end
      object MasterData12: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 219.212740000000000000
        Width = 793.701300000000000000
        RowCount = 0
        object Memo299: TfrxMemoView
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SdoAnt"]')
          ParentFont = False
        end
        object Memo300: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'SumCre'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SumCre"]')
          ParentFont = False
        end
        object Memo301: TfrxMemoView
          Left = 75.590600000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSNG."Ordena"] - [frxDsSNG."NomeGe"]')
          ParentFont = False
        end
        object Memo302: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'SdoFim'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SdoFim"]')
          ParentFont = False
        end
        object Memo303: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'SumMov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SumMov"]')
          ParentFont = False
        end
        object Memo305: TfrxMemoView
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'SumDeb'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SumDeb"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 49.133890000000000000
        Top = 351.496290000000000000
        Width = 793.701300000000000000
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 298.582870000000000000
        Width = 793.701300000000000000
        object Memo7: TfrxMemoView
          Left = 377.953000000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SDOANT"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 75.590600000000000000
          Top = 11.338590000000010000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SOMAS DE VALORES DE TODAS CONTAS')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 453.543600000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SUMCRE"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 529.134200000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SUMDEB"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 604.724800000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SUMMOV"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 680.315400000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SDOFIM"]')
          ParentFont = False
        end
      end
    end
  end
  object QrLCS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pla.Nome NOMEPLANO, pla.Codigo PLANO, '
      'cjt.Nome NOMECONJUNTO, gru.Conjunto, '
      'gru.Nome NOMEGRUPO, sgr.Grupo, '
      'sgr.Nome NOMESUBGRUPO, con.SubGrupo, '
      'con.Nome NOMECONTA, mov.Genero, '
      'SUM(mov.Movim) Valor'
      'FROM contasmov mov'
      'LEFT JOIN contas    con ON con.Codigo=mov.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=mov.SubGrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=mov.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=mov.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=mov.Plano'
      'WHERE mov.CliInt=:P0'
      'AND mov.Mez <=:P1'
      'GROUP BY mov.Genero'
      'ORDER BY pla.OrdemLista, NOMEPLANO, cjt.OrdemLista, '
      'NOMECONJUNTO, gru.OrdemLista, NOMEGRUPO, sgr.OrdemLista, '
      'NOMESUBGRUPO, con.OrdemLista, NOMECONTA, mov.Mez')
    Left = 524
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLCSNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Origin = 'plano.Nome'
      Size = 50
    end
    object QrLCSPLANO: TIntegerField
      FieldName = 'PLANO'
      Origin = 'plano.Codigo'
    end
    object QrLCSNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 50
    end
    object QrLCSConjunto: TIntegerField
      FieldName = 'Conjunto'
      Origin = 'grupos.Conjunto'
    end
    object QrLCSNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 50
    end
    object QrLCSGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'subgrupos.Grupo'
    end
    object QrLCSNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 50
    end
    object QrLCSSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
      Origin = 'contas.Subgrupo'
    end
    object QrLCSNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrLCSGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'contasmov.Genero'
    end
    object QrLCSValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object frxLCS: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MePlaOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Engine.FinalPass then'
      '    MePla.Text := Format('#39'%2.2n'#39',[Get(<frxDsLCS."NOMEPLANO">)]);'
      'end;'
      ''
      'procedure MeCjtOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Engine.FinalPass then'
      
        '    MeCjt.Text := Format('#39'%2.2n'#39',[Get(<frxDsLCS."NOMECONJUNTO">)' +
        ']);'
      'end;'
      ''
      'procedure MeGruOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Engine.FinalPass then'
      '    MeGru.Text := Format('#39'%2.2n'#39',[Get(<frxDsLCS."NOMEGRUPO">)]);'
      'end;'
      ''
      'procedure MeSGROnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Engine.FinalPass then'
      
        '    MeSGR.Text := Format('#39'%2.2n'#39',[Get(<frxDsLCS."NOMESUBGRUPO">)' +
        ']);'
      'end;'
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure GroupFooter3OnBeforePrint(Sender: TfrxComponent);'
      
        'begin                                                           ' +
        '             '
      '  Set(<frxDsLCS."NOMEPLANO">,    Sum(<frxDsLCS."Valor">));  '
      'end;'
      ''
      'procedure GroupFooter2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Set(<frxDsLCS."NOMECONJUNTO">, Sum(<frxDsLCS."Valor">));  '
      'end;'
      ''
      'procedure GroupFooter1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Set(<frxDsLCS."NOMEGRUPO">,    Sum(<frxDsLCS."Valor">));  '
      'end;'
      ''
      'procedure GroupFooter4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Set(<frxDsLCS."NOMESUBGRUPO">, Sum(<frxDsLCS."Valor">));  '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxPerPlaGetValue
    Left = 468
    Top = 316
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsLCS
        DataSetName = 'frxDsLCS'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 139.842610000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsLCS."PLANO"'
        object Memo4: TfrxMemoView
          Top = 3.779529999999994000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Plano"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779529999999994000
          Width = 529.134200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMEPLANO"]')
          ParentFont = False
          WordWrap = False
        end
        object MePla: TfrxMemoView
          Left = 566.929500000000000000
          Top = 3.779529999999994000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'MePlaOnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 188.976500000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsLCS."Conjunto"'
        object Memo8: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Conjunto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 75.590600000000000000
          Width = 491.338900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMECONJUNTO"]')
          ParentFont = False
          WordWrap = False
        end
        object MeCjt: TfrxMemoView
          Left = 566.929500000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'MeCjtOnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object GH3: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 230.551330000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsLCS."Grupo"'
        object Memo12: TfrxMemoView
          Left = 75.590600000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Grupo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 113.385900000000000000
          Width = 453.543600000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMEGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object MeGru: TfrxMemoView
          Left = 566.929500000000000000
          Width = 151.181200000000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'MeGruOnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object GH4: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsLCS."SubGrupo"'
        object Memo16: TfrxMemoView
          Left = 113.385900000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Subgrupo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 151.181200000000000000
          Width = 415.748300000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMESUBGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object MeSGR: TfrxMemoView
          Left = 566.929500000000000000
          Width = 151.181200000000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'MeSGROnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = frxDsLCS
        DataSetName = 'frxDsLCS'
        RowCount = 0
        object Memo20: TfrxMemoView
          Left = 151.181200000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataField = 'Genero'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Genero"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 188.976500000000000000
          Width = 377.953000000000000000
          Height = 13.228346460000000000
          DataField = 'NOMECONTA'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object MeCta: TfrxMemoView
          Left = 566.929500000000000000
          Width = 151.181200000000000000
          Height = 13.228346460000000000
          DataField = 'Valor'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."Valor"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 582.047620000000000000
        Width = 718.110700000000000000
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 58.582706460000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Top = 3.779530000000001000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'SALDOS DE PLANOS DE CONTAS')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 22.677180000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Entidade:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 60.472480000000000000
          Top = 22.677180000000000000
          Width = 657.638220000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Top = 45.354360000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Plano')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 37.795300000000000000
          Top = 45.354360000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conjunto')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 75.590600000000000000
          Top = 45.354360000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 113.385900000000000000
          Top = 45.354360000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sub-grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 151.181200000000000000
          Top = 45.354360000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 188.976500000000000000
          Top = 45.354360000000000000
          Width = 377.953000000000000000
          Height = 13.228346460000000000
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 566.929500000000000000
          Top = 45.354360000000000000
          Width = 151.181200000000000000
          Height = 13.228346460000000000
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 532.913730000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 476.220780000000000000
          Top = 3.779530000000022000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SALDO TOTAL:     ')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Left = 566.929500000000000000
          Top = 3.779530000000022000
          Width = 151.181200000000000000
          Height = 15.118110240000000000
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 381.732530000000000000
        Visible = False
        Width = 718.110700000000000000
        OnBeforePrint = 'GroupFooter1OnBeforePrint'
        object Memo10: TfrxMemoView
          Width = 445.984540000000000000
          Height = 15.118120000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">)]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 419.527830000000000000
        Visible = False
        Width = 718.110700000000000000
        OnBeforePrint = 'GroupFooter2OnBeforePrint'
        object Memo11: TfrxMemoView
          Width = 445.984540000000000000
          Height = 15.118120000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">)]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 457.323130000000000000
        Visible = False
        Width = 718.110700000000000000
        OnBeforePrint = 'GroupFooter3OnBeforePrint'
        object Memo6: TfrxMemoView
          Width = 445.984540000000000000
          Height = 15.118120000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">)]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 343.937230000000000000
        Visible = False
        Width = 718.110700000000000000
        OnBeforePrint = 'GroupFooter4OnBeforePrint'
        object Memo7: TfrxMemoView
          Width = 445.984540000000000000
          Height = 15.118120000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsLCS: TfrxDBDataset
    UserName = 'frxDsLCS'
    CloseDataSource = False
    DataSet = QrLCS
    BCDToCurrency = False
    Left = 496
    Top = 316
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 260
    Top = 12
  end
end
