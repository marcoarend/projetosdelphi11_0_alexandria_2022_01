object FmSelConta: TFmSelConta
  Left = 396
  Top = 181
  Caption = 'Conta'
  ClientHeight = 410
  ClientWidth = 713
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 713
    Height = 49
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 131
      Height = 13
      Caption = 'Conta (do plano de contas):'
    end
    object Label2: TLabel
      Left = 440
      Top = 4
      Width = 44
      Height = 13
      Caption = 'Hist'#243'rico:'
    end
    object CBConta: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 353
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 1
      dmkEditCB = EdConta
      UpdType = utYes
    end
    object EdConta: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdContaChange
      DBLookupComboBox = CBConta
    end
    object EdDescricao: TdmkEdit
      Left = 440
      Top = 20
      Width = 261
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 362
    Width = 713
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 14
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object Panel1: TPanel
      Left = 604
      Top = 1
      Width = 108
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 713
    Height = 40
    Align = alTop
    Caption = 'Conta'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 630
      Top = 1
      Width = 82
      Height = 38
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 395
      ExplicitTop = 2
      ExplicitHeight = 36
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 629
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 393
      ExplicitHeight = 36
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 89
    Width = 713
    Height = 273
    Align = alClient
    TabOrder = 3
    object dmkLabel1: TdmkLabel
      Left = 1
      Top = 1
      Width = 711
      Height = 16
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = 
        'Duplo clique na grade copia hist'#243'rico para a sua caixa de edi'#231#227'o' +
        '.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      UpdType = utYes
      SQLType = stNil
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 17
      Width = 711
      Height = 255
      Align = alClient
      DataSource = DsLct
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'MesAno'
          Title.Alignment = taCenter
          Title.Caption = 'Compet.'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Lan'#231'amento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Hist'#243'rico'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Title.Caption = 'NF'
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR'
          Title.Caption = 'Valor (D/C)'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_TER'
          Title.Caption = 'Cliente / Fornecedor'
          Width = 180
          Visible = True
        end>
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 200
    Top = 54
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome')
    Left = 172
    Top = 54
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    Left = 204
    Top = 136
    object QrLctData: TDateField
      DisplayWidth = 8
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctMesAno: TWideStringField
      DisplayWidth = 8
      FieldName = 'MesAno'
      Required = True
      Size = 5
    end
    object QrLctControle: TIntegerField
      DisplayWidth = 12
      FieldName = 'Controle'
      DisplayFormat = '000000;-000000; '
    end
    object QrLctDescricao: TWideStringField
      DisplayWidth = 120
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctNotaFiscal: TIntegerField
      DisplayWidth = 12
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrLctDebito: TFloatField
      DisplayWidth = 12
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctCredito: TFloatField
      DisplayWidth = 12
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrLctNO_TER: TWideStringField
      FieldName = 'NO_TER'
      Size = 100
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctCliente: TIntegerField
      FieldName = 'Cliente'
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 232
    Top = 136
  end
end
