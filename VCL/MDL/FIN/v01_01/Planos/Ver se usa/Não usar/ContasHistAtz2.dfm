object FmContasHistAtz2: TFmContasHistAtz2
  Left = 431
  Top = 173
  Caption = 'FIN-PLCTA-012 :: Atualiza'#231#227'o de Saldo de Contas'
  ClientHeight = 390
  ClientWidth = 515
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 342
    Width = 515
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtNao: TBitBtn
      Tag = 14
      Left = 112
      Top = 4
      Width = 165
      Height = 40
      Caption = '&Continuar sem atualizar'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtNaoClick
    end
    object Panel2: TPanel
      Left = 403
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desistir'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtSim: TBitBtn
      Tag = 14
      Left = 16
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Atualizar'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtSimClick
    end
    object BtExclui: TBitBtn
      Tag = 13
      Left = 280
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Excluir'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 3
      OnClick = BtExcluiClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 515
    Height = 48
    Align = alTop
    Caption = 'Atualiza'#231#227'o de Saldo de Contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 513
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 503
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 515
    Height = 294
    Align = alClient
    TabOrder = 0
    object STAvisoContinuar: TStaticText
      Left = 1
      Top = 1
      Width = 513
      Height = 68
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      Caption = 
        #13#10'Para executar a a'#231#227'o solicitada '#233' necess'#225'rio atualizar os sald' +
        'os do plano de contas! Deseja continuar assim mesmo?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clHighlight
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 1
      Top = 69
      Width = 513
      Height = 224
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 11
        Top = 94
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label2: TLabel
        Left = 11
        Top = 134
        Width = 240
        Height = 13
        Caption = 'Conta do plano de contas (deixe vazio para todas):'
      end
      object Label5: TLabel
        Left = 11
        Top = 178
        Width = 73
        Height = 13
        Caption = 'A partir do m'#234's:'
      end
      object Label3: TLabel
        Left = 119
        Top = 178
        Width = 321
        Height = 13
        Caption = 'Fx: x = meses a menos que o atual: Exemplo F4 = 4 meses a menos.'
      end
      object Label16: TLabel
        Left = 91
        Top = 180
        Width = 22
        Height = 12
        Caption = 'pq'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Wingdings 3'
        Font.Style = []
        ParentFont = False
      end
      object PB: TProgressBar
        Left = 7
        Top = 6
        Width = 497
        Height = 17
        TabOrder = 0
      end
      object StaticText2: TStaticText
        Left = 7
        Top = 26
        Width = 125
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'Cliente interno:'
        TabOrder = 1
      end
      object STCliInt_: TStaticText
        Left = 135
        Top = 26
        Width = 369
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        TabOrder = 2
      end
      object STConta_: TStaticText
        Left = 135
        Top = 46
        Width = 369
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        TabOrder = 3
      end
      object StaticText3: TStaticText
        Left = 7
        Top = 46
        Width = 125
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'Conta (plano de contas)'
        TabOrder = 4
      end
      object StaticText4: TStaticText
        Left = 7
        Top = 66
        Width = 125
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = 'Per'#237'odo:'
        TabOrder = 5
      end
      object STPeriodo_: TStaticText
        Left = 135
        Top = 66
        Width = 369
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        TabOrder = 6
        OnClick = BtSimClick
      end
      object EdCliInt: TdmkEditCB
        Left = 11
        Top = 110
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCliIntChange
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
      end
      object EdConta: TdmkEditCB
        Left = 11
        Top = 150
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCliIntChange
        DBLookupComboBox = CBConta
        IgnoraDBLookupComboBox = False
      end
      object dmkEdPeriodo: TdmkEdit
        Left = 11
        Top = 194
        Width = 89
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-23999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = dmkEdPeriodoChange
        OnKeyDown = dmkEdPeriodoKeyDown
      end
      object Edit1: TEdit
        Left = 99
        Top = 194
        Width = 405
        Height = 21
        ReadOnly = True
        TabOrder = 10
        OnKeyDown = Edit1KeyDown
      end
      object CBConta: TdmkDBLookupComboBox
        Left = 79
        Top = 150
        Width = 425
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 11
        dmkEditCB = EdConta
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 79
        Top = 110
        Width = 425
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsCliInt
        TabOrder = 12
        dmkEditCB = EdCliInt
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object QrSdoIni: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, SdoIni'
      'FROM contassdo'
      'WHERE Entidade=:P0'
      'AND SdoIni <> 0'
      'ORDER BY Codigo')
    Left = 316
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSdoIniSdoIni: TFloatField
      FieldName = 'SdoIni'
    end
    object QrSdoIniCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrIns: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'INSERT INTO contashis'
      'SET Entidade=:P0,'
      'Codigo=:P1,'
      'Periodo=:P2,'
      ''
      'SdoAnt=:P3,'
      'SumCre=:P4,'
      'SumDeb=:P5,'
      'SdoFim=:P6')
    Left = 344
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end>
  end
  object QrDel: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DELETE FROM contashis'
      'WHERE Entidade=:P0'
      'AND Codigo=:P1'
      'AND Periodo=:P2')
    Left = 372
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrMax: TmySQLQuery
    Database = Dmod.MyDB
    Left = 400
    Top = 148
    object QrMaxData: TDateField
      FieldName = 'Data'
      Required = True
    end
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE, Codigo'
      'FROM entidades '
      'WHERE CliInt <> 0'
      'ORDER BY NOMEENTIDADE')
    Left = 288
    Top = 178
    object QrCliIntNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 316
    Top = 178
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      '/*WHERE Codigo > 0*/'
      'ORDER BY Nome')
    Left = 344
    Top = 178
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 372
    Top = 178
  end
  object QrCtLan2: TmySQLQuery
    Database = Dmod.MyDB
    Left = 332
    Top = 249
    object QrCtLan2Genero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object QrDel2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DELETE FROM contashis'
      'WHERE Entidade=:P0'
      'AND Codigo=:P1')
    Left = 360
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object QrVal: TmySQLQuery
    Database = Dmod.MyDB
    Left = 388
    Top = 249
    object QrValCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrValDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrMinT: TmySQLQuery
    Database = Dmod.MyDB
    Left = 428
    Top = 148
    object QrMinTData: TDateField
      FieldName = 'Data'
      Required = True
    end
  end
  object QrMinC: TmySQLQuery
    Database = Dmod.MyDB
    Left = 456
    Top = 148
    object QrMinCData: TDateField
      FieldName = 'Data'
      Required = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanDel01 = BtExclui
    Left = 20
    Top = 9
  end
end
