object FmContasHistSdo: TFmContasHistSdo
  Left = 370
  Top = 177
  Caption = 'FIN-PLCTA-011 :: Hist'#243'rico de Saldos do Plano de Contas'
  ClientHeight = 592
  ClientWidth = 907
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 544
    Width = 907
    Height = 48
    Align = alBottom
    TabOrder = 1
    object LaAviso: TLabel
      Left = 125
      Top = 13
      Width = 13
      Height = 13
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Panel2: TPanel
      Left = 795
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BitBtn1: TBitBtn
      Tag = 125
      Left = 14
      Top = 3
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Config.'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BitBtn1Click
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 907
    Height = 48
    Align = alTop
    Caption = 'Hist'#243'rico de Saldos do Plano de Contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 905
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 907
    Height = 496
    Align = alClient
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 905
      Height = 48
      Align = alTop
      TabOrder = 0
      object LaCliInt: TLabel
        Left = 8
        Top = 3
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label12: TLabel
        Left = 740
        Top = 3
        Width = 45
        Height = 13
        Caption = 'Entidade:'
        FocusControl = DBEdit1
      end
      object EdCliInt: TdmkEditCB
        Left = 8
        Top = 19
        Width = 57
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCliIntChange
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 68
        Top = 19
        Width = 669
        Height = 21
        Color = clWhite
        KeyField = 'CliInt'
        ListField = 'NOMECLIINT'
        ListSource = DsCliInt
        TabOrder = 1
        dmkEditCB = EdCliInt
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object BtAtualiza: TBitBtn
        Tag = 20
        Left = 808
        Top = 4
        Width = 90
        Height = 40
        Caption = 'Atuali&za'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtAtualizaClick
      end
      object DBEdit1: TDBEdit
        Left = 740
        Top = 19
        Width = 65
        Height = 21
        DataField = 'Entidade'
        DataSource = DsCliInt
        TabOrder = 3
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 49
      Width = 905
      Height = 446
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Peri'#243'dico '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl2: TPageControl
          Left = 0
          Top = 0
          Width = 897
          Height = 418
          ActivePage = TabSheet3
          Align = alClient
          TabOrder = 0
          OnChange = PageControl2Change
          object TabSheet3: TTabSheet
            Caption = ' Por per'#237'odo '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel3: TPanel
              Left = 0
              Top = 0
              Width = 889
              Height = 49
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label16: TLabel
                Left = 48
                Top = 3
                Width = 22
                Height = 12
                Caption = 'pq'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Wingdings 3'
                Font.Style = []
                ParentFont = False
              end
              object Label1: TLabel
                Left = 4
                Top = 3
                Width = 41
                Height = 13
                Caption = 'Per'#237'odo:'
              end
              object dmkEdPeriodo: TdmkEdit
                Left = 4
                Top = 19
                Width = 36
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = dmkEdPeriodoChange
              end
              object Edit1: TEdit
                Left = 40
                Top = 19
                Width = 169
                Height = 21
                ReadOnly = True
                TabOrder = 1
              end
              object BtImprimeP: TBitBtn
                Tag = 5
                Left = 215
                Top = 3
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Sai da janela atual'
                Caption = '&Imprime'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtImprimePClick
              end
              object CkControlados: TCheckBox
                Left = 311
                Top = 16
                Width = 119
                Height = 17
                Caption = 'Somente controlados.'
                TabOrder = 3
                OnClick = CkControladosClick
              end
              object PB1: TProgressBar
                Left = 436
                Top = 16
                Width = 250
                Height = 17
                TabOrder = 4
              end
              object PB2: TProgressBar
                Left = 686
                Top = 16
                Width = 100
                Height = 17
                TabOrder = 5
              end
              object PB3: TProgressBar
                Left = 786
                Top = 16
                Width = 60
                Height = 17
                TabOrder = 6
              end
              object PB4: TProgressBar
                Left = 846
                Top = 16
                Width = 32
                Height = 17
                TabOrder = 7
              end
              object PB5: TProgressBar
                Left = 878
                Top = 16
                Width = 16
                Height = 17
                TabOrder = 8
              end
            end
            object TabControl1: TTabControl
              Left = 0
              Top = 49
              Width = 889
              Height = 341
              Align = alClient
              TabOrder = 1
              Tabs.Strings = (
                ' Planos '
                ' Conjuntos '
                ' Grupos '
                ' Sub-grupos '
                ' Contas ')
              TabIndex = 0
              OnChange = TabControl1Change
              object dmkDBPer: TdmkDBGrid
                Left = 4
                Top = 24
                Width = 881
                Height = 313
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NOME'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 346
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SDOANT'
                    Title.Caption = 'Saldo inicial m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SUMCRE'
                    Title.Caption = 'Cr'#233'ditos m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SUMDEB'
                    Title.Caption = 'D'#233'bitos m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SUMMOV'
                    Title.Caption = 'Resultado m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SDOFIM'
                    Title.Caption = 'Saldo final m'#234's'
                    Width = 100
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsPerPla
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NOME'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 346
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SDOANT'
                    Title.Caption = 'Saldo inicial m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SUMCRE'
                    Title.Caption = 'Cr'#233'ditos m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SUMDEB'
                    Title.Caption = 'D'#233'bitos m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SUMMOV'
                    Title.Caption = 'Resultado m'#234's'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SDOFIM'
                    Title.Caption = 'Saldo final m'#234's'
                    Width = 100
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet4: TTabSheet
            Caption = ' Plano '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 889
              Height = 49
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label3: TLabel
                Left = 4
                Top = 3
                Width = 80
                Height = 13
                Caption = 'Plano de contas:'
              end
              object EdPla: TdmkEditCB
                Left = 4
                Top = 19
                Width = 57
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCliIntChange
                DBLookupComboBox = CBPla
                IgnoraDBLookupComboBox = False
              end
              object CBPla: TdmkDBLookupComboBox
                Left = 64
                Top = 19
                Width = 737
                Height = 21
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsPlanos
                TabOrder = 1
                dmkEditCB = EdPla
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object dmkDBGrid6: TdmkDBGrid
              Left = 0
              Top = 49
              Width = 889
              Height = 341
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsPlaSdo
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
            end
          end
          object TabSheet5: TTabSheet
            Caption = ' Conjuntos '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 889
              Height = 49
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label2: TLabel
                Left = 4
                Top = 3
                Width = 45
                Height = 13
                Caption = 'Conjunto:'
              end
              object EdCjt: TdmkEditCB
                Left = 4
                Top = 19
                Width = 57
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCliIntChange
                DBLookupComboBox = CBCjt
                IgnoraDBLookupComboBox = False
              end
              object CBCjt: TdmkDBLookupComboBox
                Left = 64
                Top = 19
                Width = 737
                Height = 21
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCjutos
                TabOrder = 1
                dmkEditCB = EdCjt
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object dmkDBGrid2: TdmkDBGrid
              Left = 0
              Top = 49
              Width = 593
              Height = 341
              Align = alLeft
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsCjtSdo
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
            end
            object dmkDBGrid7: TdmkDBGrid
              Left = 593
              Top = 49
              Width = 296
              Height = 341
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME'
                  Title.Caption = 'Descri'#231#227'o da conta'
                  Width = 266
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsCjtCta
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 2
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME'
                  Title.Caption = 'Descri'#231#227'o da conta'
                  Width = 266
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
            end
          end
          object TabSheet6: TTabSheet
            Caption = ' Grupo '
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 889
              Height = 49
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label4: TLabel
                Left = 4
                Top = 3
                Width = 32
                Height = 13
                Caption = 'Grupo:'
              end
              object EdGru: TdmkEditCB
                Left = 4
                Top = 19
                Width = 57
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCliIntChange
                DBLookupComboBox = CBGru
                IgnoraDBLookupComboBox = False
              end
              object CBGru: TdmkDBLookupComboBox
                Left = 64
                Top = 19
                Width = 737
                Height = 21
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsGrupos
                TabOrder = 1
                dmkEditCB = EdGru
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object dmkDBGrid3: TdmkDBGrid
              Left = 0
              Top = 49
              Width = 889
              Height = 341
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsGruSdo
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
            end
          end
          object TabSheet7: TTabSheet
            Caption = ' Sub-grupo '
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 889
              Height = 49
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label5: TLabel
                Left = 4
                Top = 3
                Width = 52
                Height = 13
                Caption = 'Sub-grupo:'
              end
              object EdSgr: TdmkEditCB
                Left = 4
                Top = 19
                Width = 57
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCliIntChange
                DBLookupComboBox = CBSgr
                IgnoraDBLookupComboBox = False
              end
              object CBSgr: TdmkDBLookupComboBox
                Left = 64
                Top = 19
                Width = 737
                Height = 21
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsSubGru
                TabOrder = 1
                dmkEditCB = EdSgr
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object dmkDBGrid4: TdmkDBGrid
              Left = 0
              Top = 49
              Width = 889
              Height = 341
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsSgrSdo
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
            end
          end
          object TabSheet8: TTabSheet
            Caption = ' Conta '
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel9: TPanel
              Left = 0
              Top = 0
              Width = 889
              Height = 49
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label6: TLabel
                Left = 4
                Top = 3
                Width = 31
                Height = 13
                Caption = 'Conta:'
              end
              object EdCta: TdmkEditCB
                Left = 4
                Top = 19
                Width = 57
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCliIntChange
                DBLookupComboBox = CBCta
                IgnoraDBLookupComboBox = False
              end
              object CBCta: TdmkDBLookupComboBox
                Left = 64
                Top = 19
                Width = 737
                Height = 21
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsContas
                TabOrder = 1
                dmkEditCB = EdCta
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object dmkDBGrid5: TdmkDBGrid
              Left = 0
              Top = 49
              Width = 889
              Height = 341
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsCtaSdo
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MMYYYY'
                  Title.Caption = 'M'#234's'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOANT'
                  Title.Caption = 'Saldo inicial m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMCRE'
                  Title.Caption = 'Cr'#233'ditos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMDEB'
                  Title.Caption = 'D'#233'bitos m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SUMMOV'
                  Title.Caption = 'Resultado m'#234's'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SDOFIM'
                  Title.Caption = 'Saldo final m'#234's'
                  Width = 100
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Atual (Final) '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object BtImprimeA: TBitBtn
          Tag = 5
          Left = 9
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Imprime'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtImprimeAClick
        end
      end
      object TabSheet10: TTabSheet
        Caption = ' Saldos iniciais '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 897
          Height = 61
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 653
            Height = 61
            Align = alLeft
            Caption = 
              ' Pesquisa (digite parte da descri'#231#227'o dos n'#237'veis de plano desejad' +
              'os):'
            TabOrder = 0
            object Label7: TLabel
              Left = 8
              Top = 15
              Width = 36
              Height = 13
              Caption = 'Contas:'
            end
            object Label8: TLabel
              Left = 136
              Top = 15
              Width = 57
              Height = 13
              Caption = 'Sub-grupos:'
            end
            object Label9: TLabel
              Left = 264
              Top = 15
              Width = 37
              Height = 13
              Caption = 'Grupos:'
            end
            object Label10: TLabel
              Left = 392
              Top = 15
              Width = 50
              Height = 13
              Caption = 'Conjuntos:'
            end
            object Label11: TLabel
              Left = 520
              Top = 15
              Width = 35
              Height = 13
              Caption = 'Planos:'
            end
            object EdPesqCta: TEdit
              Left = 8
              Top = 31
              Width = 125
              Height = 21
              TabOrder = 0
              OnChange = EdPesqCtaChange
            end
            object EdPesqSgr: TEdit
              Left = 136
              Top = 31
              Width = 125
              Height = 21
              TabOrder = 1
              OnChange = EdPesqCtaChange
            end
            object EdPesqGru: TEdit
              Left = 264
              Top = 31
              Width = 125
              Height = 21
              TabOrder = 2
              OnChange = EdPesqCtaChange
            end
            object EdPesqCjt: TEdit
              Left = 392
              Top = 31
              Width = 125
              Height = 21
              TabOrder = 3
              OnChange = EdPesqCtaChange
            end
            object EdPesqPla: TEdit
              Left = 520
              Top = 31
              Width = 125
              Height = 21
              TabOrder = 4
              OnChange = EdPesqCtaChange
            end
          end
          object CkZero: TCheckBox
            Left = 660
            Top = 40
            Width = 193
            Height = 17
            Caption = 'Somente saldos diferentes de zero.'
            TabOrder = 1
            OnClick = CkZeroClick
          end
        end
        object GradeSdoIni: TdmkDBGrid
          Left = 0
          Top = 61
          Width = 897
          Height = 309
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Conta'
              Width = 39
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECTA'
              Title.Caption = 'Descri'#231#227'o da conta'
              Width = 289
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoIni'
              Title.Caption = 'Saldo inicial'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESGR'
              Title.Caption = 'Sub-grupo'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEGRU'
              Title.Caption = 'Grupo'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECJT'
              Title.Caption = 'Conjunto'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPLA'
              Title.Caption = 'Plano'
              Width = 200
              Visible = True
            end>
          Color = clWindow
          DataSource = DsSdoIni
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Conta'
              Width = 39
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECTA'
              Title.Caption = 'Descri'#231#227'o da conta'
              Width = 289
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoIni'
              Title.Caption = 'Saldo inicial'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESGR'
              Title.Caption = 'Sub-grupo'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEGRU'
              Title.Caption = 'Grupo'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECJT'
              Title.Caption = 'Conjunto'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPLA'
              Title.Caption = 'Plano'
              Width = 200
              Visible = True
            end>
        end
        object Panel11: TPanel
          Left = 0
          Top = 370
          Width = 897
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object BtAltera: TBitBtn
            Tag = 11
            Left = 4
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Altera registro atual'
            Caption = '&Altera'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtAlteraClick
          end
        end
      end
      object TabSheet9: TTabSheet
        Caption = ' Diferen'#231'as de saldos para o plano de contas novo '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter1: TSplitter
          Left = 0
          Top = 222
          Width = 897
          Height = 3
          Cursor = crVSplit
          Align = alTop
          ExplicitTop = 157
          ExplicitWidth = 165
        end
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 65
          Width = 897
          Height = 157
          Align = alTop
          Columns = <
            item
              Expanded = False
              FieldName = 'PLANO'
              Title.Caption = 'Plano'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPLANO'
              Title.Caption = 'Descri'#231#227'o do plano'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conjunto'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECONJUNTO'
              Title.Caption = 'Descri'#231#227'o do conjunto'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Grupo'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEGRUPO'
              Title.Caption = 'Descri'#231#227'o do grupo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SubGrupo'
              Title.Caption = 'Sub-grupo'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESUBGRUPO'
              Title.Caption = 'Descri'#231#227'o do sub-grupo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Genero'
              Title.Caption = 'Conta'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECONTA'
              Title.Caption = 'Descri'#231#227'o da conta'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Movto'
              Title.Caption = 'Atual'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovNew'
              Title.Caption = 'Novo'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Diferenca'
              Title.Caption = 'Diferen'#231'a'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Incluir'
              Title.Caption = 'A incluir'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsSdoOldNew
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'PLANO'
              Title.Caption = 'Plano'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPLANO'
              Title.Caption = 'Descri'#231#227'o do plano'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conjunto'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECONJUNTO'
              Title.Caption = 'Descri'#231#227'o do conjunto'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Grupo'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEGRUPO'
              Title.Caption = 'Descri'#231#227'o do grupo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SubGrupo'
              Title.Caption = 'Sub-grupo'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESUBGRUPO'
              Title.Caption = 'Descri'#231#227'o do sub-grupo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Genero'
              Title.Caption = 'Conta'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECONTA'
              Title.Caption = 'Descri'#231#227'o da conta'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Movto'
              Title.Caption = 'Atual'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovNew'
              Title.Caption = 'Novo'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Diferenca'
              Title.Caption = 'Diferen'#231'a'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Incluir'
              Title.Caption = 'A incluir'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Visible = True
            end>
        end
        object TPanel
          Left = 0
          Top = 0
          Width = 897
          Height = 48
          Align = alTop
          TabOrder = 1
          object Label13: TLabel
            Left = 476
            Top = 8
            Width = 99
            Height = 13
            Caption = 'Total inicial carteiras:'
            FocusControl = DBEdit2
          end
          object Label14: TLabel
            Left = 612
            Top = 8
            Width = 84
            Height = 13
            Caption = 'Diferen'#231'a contas:'
            FocusControl = DBEdit3
          end
          object Label15: TLabel
            Left = 748
            Top = 8
            Width = 129
            Height = 13
            Caption = 'Valor lan'#231'amentos a incluir:'
            FocusControl = DBEdit4
          end
          object BtPesquisa: TBitBtn
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Caption = 'Pesquisa'
            TabOrder = 0
            OnClick = BtPesquisaClick
          end
          object BtInclui: TBitBtn
            Left = 96
            Top = 4
            Width = 90
            Height = 40
            Caption = 'Inclui'
            Enabled = False
            TabOrder = 1
            OnClick = BtIncluiClick
          end
          object BtMuda: TBitBtn
            Left = 188
            Top = 4
            Width = 90
            Height = 40
            Caption = 'Altera'
            Enabled = False
            TabOrder = 2
            OnClick = BtMudaClick
          end
          object BtExclui: TBitBtn
            Left = 280
            Top = 4
            Width = 90
            Height = 40
            Caption = 'Exclui'
            Enabled = False
            TabOrder = 3
            OnClick = BtExcluiClick
          end
          object BtGrava: TBitBtn
            Left = 372
            Top = 4
            Width = 90
            Height = 40
            Caption = 'Grava'
            Enabled = False
            TabOrder = 4
            OnClick = BtGravaClick
          end
          object DBEdit2: TDBEdit
            Left = 476
            Top = 24
            Width = 134
            Height = 21
            DataField = 'Inicial'
            DataSource = DsTotCar
            TabOrder = 5
          end
          object DBEdit3: TDBEdit
            Left = 612
            Top = 24
            Width = 134
            Height = 21
            DataField = 'Diferenca'
            DataSource = DsTotGen
            TabOrder = 6
          end
          object DBEdit4: TDBEdit
            Left = 748
            Top = 24
            Width = 134
            Height = 21
            DataField = 'Valor'
            DataSource = DataSource1
            TabOrder = 7
          end
        end
        object Panel12: TPanel
          Left = 0
          Top = 225
          Width = 897
          Height = 193
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Splitter2: TSplitter
            Left = 606
            Top = 0
            Height = 193
            ExplicitLeft = 632
            ExplicitTop = 44
            ExplicitHeight = 100
          end
          object Panel13: TPanel
            Left = 0
            Top = 0
            Width = 606
            Height = 193
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object StaticText2: TStaticText
              Left = 0
              Top = 0
              Width = 74
              Height = 17
              Align = alTop
              Alignment = taCenter
              Caption = 'CARTEIRAS'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
            object dmkDBGrid8: TdmkDBGrid
              Left = 0
              Top = 17
              Width = 606
              Height = 176
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o da carteira'
                  Width = 310
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Inicial'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Incluir'
                  Title.Caption = 'A incluir'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SALDO'
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsCartSald
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o da carteira'
                  Width = 310
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Inicial'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Incluir'
                  Title.Caption = 'A incluir'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SALDO'
                  Visible = True
                end>
            end
          end
          object Panel14: TPanel
            Left = 609
            Top = 0
            Width = 288
            Height = 193
            Align = alClient
            Caption = 'Panel14'
            TabOrder = 1
            object dmkDBGrid9: TdmkDBGrid
              Left = 1
              Top = 18
              Width = 286
              Height = 174
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Title.Caption = 'Conta'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_CONTA'
                  Title.Caption = 'Descri'#231#227'o da conta'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Carteira'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_Carteira'
                  Title.Caption = 'Descri'#231#227'o da carteira'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Caption = 'Cr'#233'dito'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Caption = 'D'#233'bito'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descri'
                  Title.Caption = 'Hist'#243'rico'
                  Width = 140
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsTrfOldNew
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Title.Caption = 'Conta'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_CONTA'
                  Title.Caption = 'Descri'#231#227'o da conta'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Carteira'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_Carteira'
                  Title.Caption = 'Descri'#231#227'o da carteira'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Caption = 'Cr'#233'dito'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Caption = 'D'#233'bito'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descri'
                  Title.Caption = 'Hist'#243'rico'
                  Width = 140
                  Visible = True
                end>
            end
            object StaticText3: TStaticText
              Left = 1
              Top = 1
              Width = 223
              Height = 17
              Align = alTop
              Alignment = taCenter
              Caption = 'LAN'#199'AMENTOS DE SALDOS INICIAIS'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
        end
        object StaticText1: TStaticText
          Left = 0
          Top = 48
          Width = 54
          Height = 17
          Align = alTop
          Alignment = taCenter
          Caption = 'CONTAS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
      end
    end
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCliIntAfterScroll
    SQL.Strings = (
      'SELECT ent.CliInt, ent.Codigo Entidade,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLIINT'
      'FROM entidades ent '
      'WHERE ent.CliInt <> 0'
      'ORDER BY NOMECLIINT')
    Left = 13
    Top = 13
    object QrCliIntEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrCliIntNOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Size = 100
    end
    object QrCliIntCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 41
    Top = 13
  end
  object QrPerPla: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pla.Nome NOME, SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE, '
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND pla.CtrlaSdo>:P2'
      ''
      'GROUP BY pla.Codigo'
      'ORDER BY pla.OrdemLista, pla.Nome')
    Left = 33
    Top = 294
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPerPlaNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrPerPlaSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerPlaSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerPlaSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerPlaSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerPlaSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsPerPla: TDataSource
    DataSet = QrPerPla
    Left = 61
    Top = 294
  end
  object QrPerCjt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cjt.Nome NOME, SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND cjt.CtrlaSdo>:P2'
      ''
      'GROUP BY cjt.Codigo'
      'ORDER BY cjt.OrdemLista, cjt.Nome')
    Left = 33
    Top = 322
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPerCjtNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrPerCjtSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCjtSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCjtSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCjtSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCjtSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsPerCjt: TDataSource
    DataSet = QrPerCjt
    Left = 61
    Top = 322
  end
  object QrPerGru: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gru.Nome NOME, SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND gru.CtrlaSdo>:P2'
      ''
      'GROUP BY gru.Codigo'
      'ORDER BY gru.OrdemLista, gru.Nome')
    Left = 33
    Top = 350
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPerGruNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrPerGruSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerGruSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerGruSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerGruSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerGruSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsPerGru: TDataSource
    DataSet = QrPerGru
    Left = 61
    Top = 350
  end
  object QrPerCta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.Nome NOME, SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND cta.CtrlaSdo>:P2'
      ''
      'GROUP BY cta.Codigo'
      'ORDER BY cta.OrdemLista, cta.Nome')
    Left = 33
    Top = 406
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPerCtaNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrPerCtaSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCtaSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCtaSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCtaSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerCtaSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object QrPerSgr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sgr.Nome NOME, SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND sgr.CtrlaSdo>:P2'
      ''
      'GROUP BY sgr.Codigo'
      'ORDER BY sgr.OrdemLista, sgr.Nome')
    Left = 33
    Top = 378
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPerSgrNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrPerSgrSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerSgrSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerSgrSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerSgrSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPerSgrSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsPerSgr: TDataSource
    DataSet = QrPerSgr
    Left = 61
    Top = 378
  end
  object DsPerCta: TDataSource
    DataSet = QrPerCta
    Left = 61
    Top = 406
  end
  object QrPlaSdo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT his.Periodo,'
      'IF(MOD(his.periodo, 12)=0, '
      ' CONCAT('#39'12 / '#39',(2000+FLOOR(his.periodo / 12)-1)),'
      ' CONCAT(LPAD(MOD(his.periodo, 12), 2, "0"), '#39' / '#39','
      '  (2000+FLOOR(his.periodo / 12)))'
      ') MMYYYY,'
      'SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE, '
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano'
      'WHERE his.Entidade=:P0'
      'AND pla.Codigo=:P1'
      ''
      'GROUP BY his.Periodo'
      'ORDER BY his.Periodo DESC')
    Left = 117
    Top = 294
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPlaSdoPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrPlaSdoMMYYYY: TWideStringField
      FieldName = 'MMYYYY'
      Size = 37
    end
    object QrPlaSdoSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPlaSdoSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPlaSdoSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPlaSdoSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrPlaSdoSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsPlaSdo: TDataSource
    DataSet = QrPlaSdo
    Left = 145
    Top = 294
  end
  object QrPlanos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM plano'
      'ORDER BY Nome')
    Left = 173
    Top = 294
    object QrPlanosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPlanosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPlanos: TDataSource
    DataSet = QrPlanos
    Left = 201
    Top = 294
  end
  object QrCjtSdo: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCjtSdoAfterScroll
    SQL.Strings = (
      'SELECT his.Periodo,'
      'IF(MOD(his.periodo, 12)=0, '
      ' CONCAT('#39'12 / '#39',(2000+FLOOR(his.periodo / 12)-1)),'
      ' CONCAT(LPAD(MOD(his.periodo, 12), 2, "0"), '#39' / '#39','
      '  (2000+FLOOR(his.periodo / 12)))'
      ') MMYYYY,'
      'SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE, '
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'WHERE his.Entidade=:P0'
      'AND cjt.Codigo=:P1'
      ''
      'GROUP BY his.Periodo'
      'ORDER BY his.Periodo DESC')
    Left = 117
    Top = 322
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCjtSdoPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrCjtSdoMMYYYY: TWideStringField
      FieldName = 'MMYYYY'
      Size = 37
    end
    object QrCjtSdoSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtSdoSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtSdoSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtSdoSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtSdoSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsCjtSdo: TDataSource
    DataSet = QrCjtSdo
    Left = 145
    Top = 322
  end
  object QrCjutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM conjuntos'
      'ORDER BY Nome')
    Left = 173
    Top = 322
    object QrCjutosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCjutosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCjutos: TDataSource
    DataSet = QrCjutos
    Left = 201
    Top = 322
  end
  object QrGruSdo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT his.Periodo,'
      'IF(MOD(his.periodo, 12)=0, '
      ' CONCAT('#39'12 / '#39',(2000+FLOOR(his.periodo / 12)-1)),'
      ' CONCAT(LPAD(MOD(his.periodo, 12), 2, "0"), '#39' / '#39','
      '  (2000+FLOOR(his.periodo / 12)))'
      ') MMYYYY,'
      'SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE, '
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'WHERE his.Entidade=:P0'
      'AND gru.Codigo=:P1'
      ''
      'GROUP BY his.Periodo'
      'ORDER BY his.Periodo DESC')
    Left = 117
    Top = 350
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGruSdoPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrGruSdoMMYYYY: TWideStringField
      FieldName = 'MMYYYY'
      Size = 37
    end
    object QrGruSdoSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrGruSdoSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrGruSdoSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrGruSdoSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrGruSdoSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsGruSdo: TDataSource
    DataSet = QrGruSdo
    Left = 145
    Top = 350
  end
  object QrGrupos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM grupos'
      'ORDER BY Nome')
    Left = 173
    Top = 350
    object QrGruposCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruposNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 201
    Top = 350
  end
  object QrSgrSdo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT his.Periodo,'
      'IF(MOD(his.periodo, 12)=0, '
      ' CONCAT('#39'12 / '#39',(2000+FLOOR(his.periodo / 12)-1)),'
      ' CONCAT(LPAD(MOD(his.periodo, 12), 2, "0"), '#39' / '#39','
      '  (2000+FLOOR(his.periodo / 12)))'
      ') MMYYYY,'
      'SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE, '
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'WHERE his.Entidade=:P0'
      'AND sgr.Codigo=:P1'
      ''
      'GROUP BY his.Periodo'
      'ORDER BY his.Periodo DESC')
    Left = 117
    Top = 378
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSgrSdoPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrSgrSdoMMYYYY: TWideStringField
      FieldName = 'MMYYYY'
      Size = 37
    end
    object QrSgrSdoSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrSgrSdoSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrSgrSdoSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrSgrSdoSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrSgrSdoSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsSgrSdo: TDataSource
    DataSet = QrSgrSdo
    Left = 145
    Top = 378
  end
  object QrSubgru: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM subgrupos'
      'ORDER BY Nome')
    Left = 173
    Top = 378
    object QrSubgruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSubgruNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsSubGru: TDataSource
    DataSet = QrSubgru
    Left = 201
    Top = 378
  end
  object QrCtaSdo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT his.Periodo,'
      'IF(MOD(his.periodo, 12)=0, '
      ' CONCAT('#39'12 / '#39',(2000+FLOOR(his.periodo / 12)-1)),'
      ' CONCAT(LPAD(MOD(his.periodo, 12), 2, "0"), '#39' / '#39','
      '  (2000+FLOOR(his.periodo / 12)))'
      ') MMYYYY,'
      'SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE, '
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'WHERE his.Entidade=:P0'
      'AND cta.Codigo=:P1'
      ''
      'GROUP BY his.Periodo'
      'ORDER BY his.Periodo DESC')
    Left = 117
    Top = 406
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCtaSdoPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrCtaSdoMMYYYY: TWideStringField
      FieldName = 'MMYYYY'
      Size = 37
    end
    object QrCtaSdoSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCtaSdoSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCtaSdoSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCtaSdoSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCtaSdoSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
  end
  object DsCtaSdo: TDataSource
    DataSet = QrCtaSdo
    Left = 145
    Top = 406
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome')
    Left = 173
    Top = 406
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 201
    Top = 406
  end
  object QrSdoIni: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSdoIniAfterOpen
    BeforeClose = QrSdoIniBeforeClose
    SQL.Strings = (
      'SELECT cta.Codigo, sdo.Codigo CODSDO, '
      'sdo.SdoIni, cta.Nome NOMECTA, '
      'sgr.Nome NOMESGR, gru.Nome NOMEGRU, '
      'cjt.Nome NOMECJT, pla.Nome NOMEPLA '
      'FROM contassdo sdo'
      'LEFT JOIN contas    cta ON cta.Codigo=sdo.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano'
      'WHERE sdo.Entidade=1'
      'AND cta.Nome LIKE "%%"'
      'AND sgr.Nome LIKE "%%"'
      'AND gru.Nome LIKE "%%"'
      'AND cjt.Nome LIKE "%%"'
      'AND pla.Nome LIKE "%%"'
      'ORDER BY cta.Nome')
    Left = 173
    Top = 265
    object QrSdoIniCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
      DisplayFormat = '000'
    end
    object QrSdoIniCODSDO: TIntegerField
      FieldName = 'CODSDO'
      Required = True
    end
    object QrSdoIniSdoIni: TFloatField
      FieldName = 'SdoIni'
      Required = True
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrSdoIniNOMECTA: TWideStringField
      FieldName = 'NOMECTA'
      Size = 50
    end
    object QrSdoIniNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrSdoIniNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrSdoIniNOMECJT: TWideStringField
      FieldName = 'NOMECJT'
      Size = 50
    end
    object QrSdoIniNOMEPLA: TWideStringField
      FieldName = 'NOMEPLA'
      Size = 50
    end
  end
  object DsSdoIni: TDataSource
    DataSet = QrSdoIni
    Left = 201
    Top = 265
  end
  object QrCjtCta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.Codigo Conta, cta.Nome NOME, '
      'SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND cjt.Codigo=:P2'
      'GROUP BY cta.Codigo'
      'ORDER BY cjt.OrdemLista, cjt.Nome, '
      '         gru.OrdemLista, gru.Nome,'
      '         sgr.OrdemLista, sgr.Nome,'
      '         cta.OrdemLista, cta.Nome')
    Left = 229
    Top = 321
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCjtCtaNOME: TWideStringField
      FieldName = 'NOME'
      Size = 50
    end
    object QrCjtCtaSUMMOV: TFloatField
      FieldName = 'SUMMOV'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtCtaSDOANT: TFloatField
      FieldName = 'SDOANT'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtCtaSUMCRE: TFloatField
      FieldName = 'SUMCRE'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtCtaSUMDEB: TFloatField
      FieldName = 'SUMDEB'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtCtaSDOFIM: TFloatField
      FieldName = 'SDOFIM'
      DisplayFormat = '#,###,###,###,##0.00'
    end
    object QrCjtCtaConta: TIntegerField
      FieldName = 'Conta'
      Required = True
      DisplayFormat = '000'
    end
  end
  object DsCjtCta: TDataSource
    DataSet = QrCjtCta
    Left = 257
    Top = 321
  end
  object frxDsPerPla: TfrxDBDataset
    UserName = 'frxDsPerPla'
    CloseDataSource = False
    DataSource = DsPerPla
    BCDToCurrency = False
    Left = 397
    Top = 262
  end
  object frxPerPla: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39655.525740740700000000
    ReportOptions.LastChange = 39655.525740740700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxPerPlaGetValue
    Left = 369
    Top = 262
    Datasets = <
      item
      end
      item
        DataSet = frxDsPerPla
        DataSetName = 'frxDsPerPla'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageHeader9: TfrxPageHeader
        Height = 139.842610000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape7: TfrxShapeView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo214: TfrxMemoView
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line24: TfrxLineView
          Left = 75.590600000000000000
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          Left = 226.771800000000000000
          Top = 56.692949999999990000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldos de [VARF_NIVEL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          Left = 83.149660000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo292: TfrxMemoView
          Left = 604.724800000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          Left = 75.590600000000000000
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo294: TfrxMemoView
          Left = 75.590600000000000000
          Top = 102.047310000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 377.953000000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo anterior')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 453.543600000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'ditos')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 75.590600000000000000
          Top = 120.944960000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 604.724800000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Result. per'#237'odo')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 680.315400000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo final')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 529.134200000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bitos')
          ParentFont = False
        end
      end
      object MasterData12: TfrxMasterData
        Height = 18.897650000000000000
        Top = 219.212740000000000000
        Width = 793.701300000000000000
        DataSet = frxDsPerPla
        DataSetName = 'frxDsPerPla'
        RowCount = 0
        object Memo299: TfrxMemoView
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPerPla."SDOANT"]')
          ParentFont = False
        end
        object Memo300: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'SUMCRE'
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPerPla."SUMCRE"]')
          ParentFont = False
        end
        object Memo301: TfrxMemoView
          Left = 75.590600000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOME'
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPerPla."NOME"]')
          ParentFont = False
        end
        object Memo302: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPerPla."SDOFIM"]')
          ParentFont = False
        end
        object Memo303: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPerPla."SUMMOV"]')
          ParentFont = False
        end
        object Memo305: TfrxMemoView
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPerPla."SUMDEB"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 49.133890000000000000
        Top = 351.496290000000000000
        Width = 793.701300000000000000
      end
      object ReportSummary1: TfrxReportSummary
        Height = 30.236240000000000000
        Top = 298.582870000000000000
        Width = 793.701300000000000000
        object Memo7: TfrxMemoView
          Left = 377.953000000000000000
          Top = 11.338590000000120000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPerPla."SDOANT">,MasterData12)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 75.590600000000000000
          Top = 11.338590000000120000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAIS')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 453.543600000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPerPla."SUMCRE">,MasterData12)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 529.134200000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPerPla."SUMDEB">,MasterData12)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 604.724800000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPerPla."SUMMOV">,MasterData12)]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 680.315400000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPerPla."SDOFIM">,MasterData12)]')
          ParentFont = False
        end
      end
    end
  end
  object PMImpNivel: TPopupMenu
    Left = 205
    Top = 445
    object Estenvel1: TMenuItem
      Caption = '&Este n'#237'vel'
      OnClick = Estenvel1Click
    end
    object Todosnveis1: TMenuItem
      Caption = '&Todos n'#237'veis'
      OnClick = Todosnveis1Click
    end
    object Generoscontrolados1: TMenuItem
      Caption = '&Generos controlados'
      OnClick = Generoscontrolados1Click
    end
  end
  object frxSNG: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39655.525740740700000000
    ReportOptions.LastChange = 39655.525740740700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxPerPlaGetValue
    Left = 425
    Top = 262
    Datasets = <
      item
      end
      item
        DataSet = DModFin.frxDsSNG
        DataSetName = 'frxDsSNG'
      end
      item
        DataSet = DModFin.frxDsSTCP
        DataSetName = 'frxDsSTCP'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageHeader9: TfrxPageHeader
        Height = 139.842610000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape7: TfrxShapeView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo214: TfrxMemoView
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line24: TfrxLineView
          Left = 75.590600000000000000
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          Left = 226.771800000000000000
          Top = 56.692949999999990000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldos dos N'#237'veis do Plano de Contas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          Left = 83.149660000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo292: TfrxMemoView
          Left = 604.724800000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          Left = 75.590600000000000000
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo294: TfrxMemoView
          Left = 75.590600000000000000
          Top = 102.047310000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 377.953000000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo anterior')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 453.543600000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'ditos')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 75.590600000000000000
          Top = 120.944960000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 604.724800000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Result. per'#237'odo')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 680.315400000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo final')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 529.134200000000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bitos')
          ParentFont = False
        end
      end
      object MasterData12: TfrxMasterData
        Height = 18.897650000000000000
        Top = 219.212740000000000000
        Width = 793.701300000000000000
        DataSet = DModFin.frxDsSNG
        DataSetName = 'frxDsSNG'
        RowCount = 0
        object Memo299: TfrxMemoView
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SdoAnt"]')
          ParentFont = False
        end
        object Memo300: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SumCre"]')
          ParentFont = False
        end
        object Memo301: TfrxMemoView
          Left = 75.590600000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSNG."Ordena"] - [frxDsSNG."NomeGe"]')
          ParentFont = False
        end
        object Memo302: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SdoFim"]')
          ParentFont = False
        end
        object Memo303: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SumMov"]')
          ParentFont = False
        end
        object Memo305: TfrxMemoView
          Left = 529.134200000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SumDeb"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 49.133890000000000000
        Top = 351.496290000000000000
        Width = 793.701300000000000000
      end
      object ReportSummary1: TfrxReportSummary
        Height = 30.236240000000000000
        Top = 298.582870000000000000
        Width = 793.701300000000000000
        object Memo7: TfrxMemoView
          Left = 377.953000000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SDOANT"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 75.590600000000000000
          Top = 11.338590000000010000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SOMAS DE VALORES DE TODAS CONTAS')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 453.543600000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SUMCRE"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 529.134200000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SUMDEB"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 604.724800000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SUMMOV"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 680.315400000000000000
          Top = 11.338590000000010000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsPerPla
          DataSetName = 'frxDsPerPla'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SDOFIM"]')
          ParentFont = False
        end
      end
    end
  end
  object QrLCS: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLCSCalcFields
    Left = 520
    Top = 316
    object QrLCSNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrLCSPLANO: TIntegerField
      FieldName = 'PLANO'
    end
    object QrLCSNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 50
    end
    object QrLCSConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object QrLCSNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 50
    end
    object QrLCSGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrLCSNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrLCSSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
    end
    object QrLCSNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrLCSGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLCSSdoIni: TFloatField
      FieldName = 'SdoIni'
    end
    object QrLCSMovto: TFloatField
      FieldName = 'Movto'
    end
    object QrLCSVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR'
      Calculated = True
    end
  end
  object frxLCS: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MePlaOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Engine.FinalPass then'
      
        '  MePla.Text := FormatFloat('#39'#,###,##0.00'#39', <VARF_SUM_PLA>);    ' +
        '  '
      'end;'
      ''
      'procedure MeCjtOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Engine.FinalPass then'
      
        '  MeCjt.Text := FormatFloat('#39'#,###,##0.00'#39', <VARF_SUM_CJT>);    ' +
        '  '
      'end;'
      ''
      'procedure MeGruOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Engine.FinalPass then'
      
        '  MeGru.Text := FormatFloat('#39'#,###,##0.00'#39', <VARF_SUM_GRU>);    ' +
        '  '
      'end;'
      ''
      'procedure MeSGROnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Engine.FinalPass then'
      
        '  MeSgr.Text := FormatFloat('#39'#,###,##0.00'#39', <VARF_SUM_SGR>);    ' +
        '  '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxPerPlaGetValue
    Left = 464
    Top = 316
    Datasets = <
      item
        DataSet = frxDsLCS
        DataSetName = 'frxDsLCS'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object GH1: TfrxGroupHeader
        Height = 26.456710000000000000
        Top = 139.842610000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsLCS."PLANO"'
        object Memo4: TfrxMemoView
          Top = 3.779529999999994000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Plano"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779529999999994000
          Width = 529.134199999999900000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMEPLANO"]')
          ParentFont = False
          WordWrap = False
        end
        object MePla: TfrxMemoView
          Left = 566.929499999999900000
          Top = 3.779529999999994000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'MePlaOnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object GH2: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 188.976500000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsLCS."Conjunto"'
        object Memo8: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Conjunto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 75.590600000000000000
          Width = 491.338900000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMECONJUNTO"]')
          ParentFont = False
          WordWrap = False
        end
        object MeCjt: TfrxMemoView
          Left = 566.929499999999900000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'MeCjtOnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object GH3: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 230.551330000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsLCS."Grupo"'
        object Memo12: TfrxMemoView
          Left = 75.590600000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Grupo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 113.385900000000000000
          Width = 453.543600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMEGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object MeGru: TfrxMemoView
          Left = 566.929499999999900000
          Width = 151.181200000000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'MeGruOnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object GH4: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsLCS."SubGrupo"'
        object Memo16: TfrxMemoView
          Left = 113.385900000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Subgrupo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 151.181200000000000000
          Width = 415.748300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMESUBGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object MeSGR: TfrxMemoView
          Left = 566.929499999999900000
          Width = 151.181200000000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'MeSGROnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        DataSet = frxDsLCS
        DataSetName = 'frxDsLCS'
        RowCount = 0
        object Memo20: TfrxMemoView
          Left = 151.181200000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Genero'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Genero"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 188.976500000000000000
          Width = 377.953000000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object MeCta: TfrxMemoView
          Left = 566.929499999999900000
          Width = 151.181200000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Valor'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."Valor"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 34.015770000000010000
        Top = 430.866420000000000000
        Width = 718.110700000000000000
      end
      object PageHeader1: TfrxPageHeader
        Height = 58.582706460000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Top = 3.779530000000001000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'SALDOS DE PLANOS DE CONTAS')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 22.677180000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Entidade:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 60.472480000000000000
          Top = 22.677180000000000000
          Width = 657.638220000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_CLIINT]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Top = 45.354360000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Plano')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 37.795300000000000000
          Top = 45.354360000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conjunto')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 75.590600000000000000
          Top = 45.354360000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 113.385900000000000000
          Top = 45.354360000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sub-grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 151.181200000000000000
          Top = 45.354360000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 188.976500000000000000
          Top = 45.354360000000000000
          Width = 377.953000000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 566.929499999999900000
          Top = 45.354360000000000000
          Width = 151.181200000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 26.456710000000000000
        Top = 381.732530000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 476.220780000000000000
          Top = 3.779530000000022000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SALDO TOTAL:     ')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Left = 566.929499999999900000
          Top = 3.779530000000022000
          Width = 151.181200000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLCS."SdoIni"> + <frxDsLCS."Movto">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsLCS: TfrxDBDataset
    UserName = 'frxDsLCS'
    CloseDataSource = False
    DataSet = QrLCS
    BCDToCurrency = False
    Left = 492
    Top = 316
  end
  object QrSPla: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLCSCalcFields
    Left = 548
    Top = 316
    object QrSPlaSDOINI: TFloatField
      FieldName = 'SDOINI'
    end
    object QrSPlaMOVTO: TFloatField
      FieldName = 'MOVTO'
    end
  end
  object QrSCjt: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLCSCalcFields
    Left = 576
    Top = 316
    object QrSCjtSDOINI: TFloatField
      FieldName = 'SDOINI'
    end
    object QrSCjtMOVTO: TFloatField
      FieldName = 'MOVTO'
    end
  end
  object QrSGru: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLCSCalcFields
    Left = 604
    Top = 316
    object QrSGruSDOINI: TFloatField
      FieldName = 'SDOINI'
    end
    object QrSGruMOVTO: TFloatField
      FieldName = 'MOVTO'
    end
  end
  object QrSSgr: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLCSCalcFields
    Left = 632
    Top = 316
    object QrSSgrSDOINI: TFloatField
      FieldName = 'SDOINI'
    end
    object QrSSgrMOVTO: TFloatField
      FieldName = 'MOVTO'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 260
    Top = 12
  end
  object QrSdoOldNew: TmySQLQuery
    Database = DModG.DBDmk
    OnCalcFields = QrSdoOldNewCalcFields
    SQL.Strings = (
      'SELECT NOMEPLANO, PLANO, NOMECONJUNTO, Conjunto,'
      'NOMEGRUPO, Grupo, NOMESUBGRUPO, SubGrupo,'
      'NOMECONTA, Genero, SdoIni, Movto, MovNew, Diferenca'
      'FROM sdooldnew'
      'WHERE Diferenca <> 0'
      'ORDER BY NOMECONTA')
    Left = 452
    Top = 388
    object QrSdoOldNewNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 100
    end
    object QrSdoOldNewPLANO: TIntegerField
      FieldName = 'PLANO'
    end
    object QrSdoOldNewNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 100
    end
    object QrSdoOldNewConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object QrSdoOldNewNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 100
    end
    object QrSdoOldNewGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrSdoOldNewNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 100
    end
    object QrSdoOldNewSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
    end
    object QrSdoOldNewNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 100
    end
    object QrSdoOldNewGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrSdoOldNewSdoIni: TFloatField
      FieldName = 'SdoIni'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSdoOldNewMovto: TFloatField
      FieldName = 'Movto'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSdoOldNewMovNew: TFloatField
      FieldName = 'MovNew'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSdoOldNewDiferenca: TFloatField
      FieldName = 'Diferenca'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSdoOldNewIncluir: TFloatField
      FieldKind = fkLookup
      FieldName = 'Incluir'
      LookupDataSet = QrSumGen
      LookupKeyFields = 'Genero'
      LookupResultField = 'Valor'
      KeyFields = 'Genero'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Lookup = True
    end
    object QrSdoOldNewSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
  end
  object DsSdoOldNew: TDataSource
    DataSet = QrSdoOldNew
    Left = 480
    Top = 388
  end
  object QrCartSald: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCartSaldCalcFields
    SQL.Strings = (
      'SELECT Codigo, Nome, Inicial'
      'FROM carteiras'
      'WHERE ForneceI=:P0'
      'AND Inicial <> 0'
      '')
    Left = 452
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCartSaldCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartSaldNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCartSaldInicial: TFloatField
      FieldName = 'Inicial'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartSaldIncluir: TFloatField
      FieldKind = fkLookup
      FieldName = 'Incluir'
      LookupDataSet = QrSumCar
      LookupKeyFields = 'Carteira'
      LookupResultField = 'Valor'
      KeyFields = 'Codigo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Lookup = True
    end
    object QrCartSaldSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
  end
  object DsCartSald: TDataSource
    DataSet = QrCartSald
    Left = 480
    Top = 360
  end
  object QrSumGen: TmySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT Genero, SUM(Credito-Debito) Valor'
      'FROM trfoldnew'
      'GROUP BY Genero')
    Left = 508
    Top = 388
    object QrSumGenGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrSumGenValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrSumCar: TmySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT Carteira, SUM(Credito-Debito) Valor'
      'FROM trfoldnew'
      'GROUP BY Carteira')
    Left = 508
    Top = 360
    object QrSumCarCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrSumCarValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object DsTrfOldNew: TDataSource
    DataSet = QrTrfOldNew
    Left = 480
    Top = 416
  end
  object QrTrfOldNew: TmySQLQuery
    Database = DModG.DBDmk
    AfterOpen = QrTrfOldNewAfterOpen
    BeforeClose = QrTrfOldNewBeforeClose
    SQL.Strings = (
      'SELECT con.Nome NO_CONTA, car.Nome NO_Carteira, '
      'car.Tipo TIPO_CART, ton.* '
      'FROM trfoldnew ton'
      'LEFT JOIN syndic.contas con ON con.Codigo=ton.Genero'
      'LEFT JOIN syndic.carteiras car ON car.Codigo=ton.Carteira'
      '')
    Left = 452
    Top = 416
    object QrTrfOldNewNO_CONTA: TWideStringField
      FieldName = 'NO_CONTA'
      Size = 50
    end
    object QrTrfOldNewNO_Carteira: TWideStringField
      FieldName = 'NO_Carteira'
      Size = 100
    end
    object QrTrfOldNewData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTrfOldNewGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrTrfOldNewCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrTrfOldNewCredito: TFloatField
      FieldName = 'Credito'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTrfOldNewDebito: TFloatField
      FieldName = 'Debito'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTrfOldNewDescri: TWideStringField
      FieldName = 'Descri'
      Size = 50
    end
    object QrTrfOldNewAtivo: TIntegerField
      FieldName = 'Ativo'
      Required = True
    end
    object QrTrfOldNewTIPO_CART: TIntegerField
      FieldName = 'TIPO_CART'
      Required = True
    end
  end
  object QrTotCar: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Inicial) Inicial'
      'FROM carteiras'
      'WHERE ForneceI=:P0'
      '')
    Left = 536
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotCarInicial: TFloatField
      FieldName = 'Inicial'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object QrTotTrf: TmySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT SUM(Credito-Debito) Valor'
      'FROM trfoldnew')
    Left = 536
    Top = 416
    object QrTotTrfValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object QrTotGen: TmySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT SUM(Diferenca) Diferenca'
      'FROM sdooldnew')
    Left = 536
    Top = 388
    object QrTotGenDiferenca: TFloatField
      FieldName = 'Diferenca'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsTotCar: TDataSource
    DataSet = QrTotCar
    Left = 564
    Top = 360
  end
  object DsTotGen: TDataSource
    DataSet = QrTotGen
    Left = 564
    Top = 388
  end
  object DataSource1: TDataSource
    DataSet = QrTotTrf
    Left = 448
    Top = 300
  end
end
