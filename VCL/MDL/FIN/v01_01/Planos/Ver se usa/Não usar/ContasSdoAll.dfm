object FmContasSdoAll: TFmContasSdoAll
  Left = 256
  Top = 169
  Caption = 'FIN-PLCTA-008 :: Saldo Inicial de Contas'
  ClientHeight = 516
  ClientWidth = 1007
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 468
    Width = 1007
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 895
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtAtzSdoEnt: TBitBtn
      Tag = 20
      Left = 440
      Top = 4
      Width = 130
      Height = 40
      Caption = '&Atz sdo entidade'
      Enabled = False
      TabOrder = 1
      OnClick = BtAtzSdoEntClick
      NumGlyphs = 2
    end
    object ProgressBar1: TProgressBar
      Left = 576
      Top = 16
      Width = 185
      Height = 17
      TabOrder = 2
    end
    object BtInclui: TBitBtn
      Tag = 10
      Left = 17
      Top = 4
      Width = 96
      Height = 40
      Cursor = crHandPoint
      Hint = 'Inclui nova entidade'
      Caption = '&Inclui'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BtIncluiClick
      NumGlyphs = 2
    end
    object Button1: TButton
      Left = 180
      Top = 16
      Width = 75
      Height = 25
      Caption = 'SQL'
      TabOrder = 4
      OnClick = Button1Click
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1007
    Height = 48
    Align = alTop
    Caption = 'Saldo Inicial de Contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 52
      Top = 1
      Width = 954
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 53
      ExplicitTop = 2
      ExplicitWidth = 952
      ExplicitHeight = 44
    end
    object PainelPesquisa: TPanel
      Left = 1
      Top = 1
      Width = 51
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        OnClick = SbImprimeClick
        NumGlyphs = 2
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1007
    Height = 101
    Align = alTop
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 552
      Top = 1
      Width = 454
      Height = 99
      Align = alClient
      Caption = ' Saldos da entidade selecionada: '
      TabOrder = 1
      object Label9: TLabel
        Left = 12
        Top = 56
        Width = 65
        Height = 13
        Caption = 'Inicial contas:'
        FocusControl = DBEdit6
      end
      object Label2: TLabel
        Left = 12
        Top = 16
        Width = 73
        Height = 13
        Caption = 'Inicial carteiras:'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 92
        Top = 16
        Width = 67
        Height = 13
        Caption = 'Movimentado:'
        FocusControl = DBEdit2
      end
      object Label6: TLabel
        Left = 172
        Top = 16
        Width = 56
        Height = 13
        Caption = 'Saldo atual:'
        FocusControl = DBEdit3
      end
      object Label7: TLabel
        Left = 252
        Top = 16
        Width = 67
        Height = 13
        Caption = 'A movimentar:'
        FocusControl = DBEdit4
      end
      object Label8: TLabel
        Left = 332
        Top = 16
        Width = 60
        Height = 13
        Caption = 'Saldo futuro:'
        FocusControl = DBEdit5
      end
      object Label10: TLabel
        Left = 172
        Top = 56
        Width = 56
        Height = 13
        Caption = 'Saldo atual:'
        FocusControl = DBEdit7
      end
      object Label11: TLabel
        Left = 332
        Top = 56
        Width = 60
        Height = 13
        Caption = 'Saldo futuro:'
        FocusControl = DBEdit8
      end
      object DBEdit6: TDBEdit
        Left = 12
        Top = 72
        Width = 76
        Height = 21
        TabStop = False
        DataField = 'SdoIni'
        DataSource = DsSCtas
        Enabled = False
        TabOrder = 0
      end
      object DBEdit1: TDBEdit
        Left = 12
        Top = 32
        Width = 76
        Height = 21
        TabStop = False
        DataField = 'Inicial'
        DataSource = DsSCart
        Enabled = False
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 92
        Top = 32
        Width = 76
        Height = 21
        TabStop = False
        DataField = 'MOVTO'
        DataSource = DsMCart
        Enabled = False
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 172
        Top = 32
        Width = 76
        Height = 21
        TabStop = False
        DataField = 'ATUAL'
        DataSource = DsMCart
        Enabled = False
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 252
        Top = 32
        Width = 76
        Height = 21
        TabStop = False
        DataField = 'MOVTO'
        DataSource = DsFCart
        Enabled = False
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 332
        Top = 32
        Width = 76
        Height = 21
        TabStop = False
        DataField = 'SALDO'
        DataSource = DsFCart
        Enabled = False
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 172
        Top = 72
        Width = 76
        Height = 21
        TabStop = False
        DataField = 'SdoAtu'
        DataSource = DsSCtas
        Enabled = False
        TabOrder = 6
      end
      object DBEdit8: TDBEdit
        Left = 332
        Top = 72
        Width = 76
        Height = 21
        TabStop = False
        DataField = 'SdoFut'
        DataSource = DsSCtas
        Enabled = False
        TabOrder = 7
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 551
      Height = 99
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Label3: TLabel
        Left = 16
        Top = 8
        Width = 279
        Height = 13
        Caption = 'Entidade (que possui pelo menos uma carteira cadastrada):'
      end
      object Label1: TLabel
        Left = 16
        Top = 48
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object Label4: TLabel
        Left = 372
        Top = 4
        Width = 86
        Height = 13
        Caption = 'Novo saldo inicial:'
        Visible = False
      end
      object EdEntidade: TdmkEditCB
        Left = 16
        Top = 24
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdEntidadeChange
        DBLookupComboBox = CBEntidade
      end
      object CBEntidade: TdmkDBLookupComboBox
        Left = 64
        Top = 24
        Width = 477
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsEntidades
        TabOrder = 1
        dmkEditCB = EdEntidade
        UpdType = utYes
      end
      object EdCodigo: TdmkEditCB
        Left = 16
        Top = 64
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdEntidadeChange
        DBLookupComboBox = CBCodigo
      end
      object EdxxxSdoIni: TdmkEdit
        Left = 372
        Top = 20
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        Visible = False
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object CBCodigo: TdmkDBLookupComboBox
        Left = 64
        Top = 64
        Width = 477
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECTA'
        TabOrder = 4
        dmkEditCB = EdCodigo
        UpdType = utYes
      end
    end
  end
  object dmkDBGridDAC1: TdmkDBGridDAC
    Left = 0
    Top = 149
    Width = 822
    Height = 319
    OnAfterSQLExec = dmkDBGridDAC1AfterSQLExec
    SQLFieldsToChange.Strings = (
      'SdoIni')
    SQLIndexesOnUpdate.Strings = (
      'Codigo'
      'Entidade')
    Align = alClient
    Columns = <
      item
        Expanded = False
        FieldName = 'NOMEENTI'
        Title.Caption = 'Entidade (Condom'#237'nio)'
        Width = 232
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECTA'
        Title.Caption = 'Conta do plano de contas'
        Width = 232
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SdoIni'
        Title.Caption = 'Saldo Inicial'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SdoAtu'
        Title.Caption = 'Saldo atual'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SdoFut'
        Title.Caption = 'Saldo Futuro'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMESGR'
        Title.Caption = 'Subgrupo'
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEGRU'
        Title.Caption = 'Grupo'
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECNJ'
        Title.Caption = 'Conjunto'
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEPLA'
        Title.Caption = 'Plano de contas'
        Width = 130
        Visible = True
      end>
    Color = clWindow
    DataSource = DsContasSdo
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    SQLTable = 'ContasSdo'
    EditForceNextYear = False
    Columns = <
      item
        Expanded = False
        FieldName = 'NOMEENTI'
        Title.Caption = 'Entidade (Condom'#237'nio)'
        Width = 232
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECTA'
        Title.Caption = 'Conta do plano de contas'
        Width = 232
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SdoIni'
        Title.Caption = 'Saldo Inicial'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SdoAtu'
        Title.Caption = 'Saldo atual'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SdoFut'
        Title.Caption = 'Saldo Futuro'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMESGR'
        Title.Caption = 'Subgrupo'
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEGRU'
        Title.Caption = 'Grupo'
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECNJ'
        Title.Caption = 'Conjunto'
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEPLA'
        Title.Caption = 'Plano de contas'
        Width = 130
        Visible = True
      end>
  end
  object Memo1: TMemo
    Left = 822
    Top = 149
    Width = 185
    Height = 319
    Align = alRight
    Lines.Strings = (
      'Memo1')
    TabOrder = 4
    Visible = False
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'WHERE Codigo IN ('
      '  SELECT ForneceI '
      '  FROM carteiras'
      '  WHERE ForneceI <> 0)'
      'ORDER BY NOMEENT')
    Left = 116
    Top = 8
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 144
    Top = 8
  end
  object DsContasSdo: TDataSource
    DataSet = QrContasSdo
    Left = 84
    Top = 8
  end
  object frxLista: TfrxReport
    Version = '4.7.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39440.588984733800000000
    ReportOptions.LastChange = 39440.588984733800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxListaGetValue
    Left = 236
    Top = 8
    Datasets = <
      item
        DataSet = frxDsListaCtaSdo
        DataSetName = 'frxDsListaCtaSdo'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      object MasterData1: TfrxMasterData
        Height = 13.228346456692900000
        Top = 173.858380000000000000
        Width = 1122.520410000000000000
        DataSet = frxDsListaCtaSdo
        DataSetName = 'frxDsListaCtaSdo'
        RowCount = 0
        object Memo6: TfrxMemoView
          Left = 37.795300000000000000
          Width = 188.976377952756000000
          Height = 13.228346456692900000
          ShowHint = False
          DataField = 'NOMEENTI'
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsListaCtaSdo."NOMEENTI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 226.771800000000000000
          Width = 158.740157480315000000
          Height = 13.228346456692900000
          ShowHint = False
          DataField = 'NOMECTA'
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsListaCtaSdo."NOMECTA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 385.512060000000000000
          Width = 60.472440940000000000
          Height = 13.228346456692900000
          ShowHint = False
          DataField = 'SdoIni'
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsListaCtaSdo."SdoIni"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 445.984540000000000000
          Width = 60.472440940000000000
          Height = 13.228346456692900000
          ShowHint = False
          DataField = 'SdoAtu'
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsListaCtaSdo."SdoAtu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 506.457020000000000000
          Width = 60.472440940000000000
          Height = 13.228346456692900000
          ShowHint = False
          DataField = 'SdoFut'
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsListaCtaSdo."SdoFut"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 566.929500000000000000
          Width = 143.622047244094000000
          Height = 13.228346456692900000
          ShowHint = False
          DataField = 'NOMESGR'
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsListaCtaSdo."NOMESGR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 710.551640000000000000
          Width = 143.622047240000000000
          Height = 13.228346456692900000
          ShowHint = False
          DataField = 'NOMEGRU'
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsListaCtaSdo."NOMEGRU"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 854.173780000000000000
          Width = 120.944881890000000000
          Height = 13.228346456692900000
          ShowHint = False
          DataField = 'NOMECNJ'
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsListaCtaSdo."NOMECNJ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 975.118740000000000000
          Width = 120.944881890000000000
          Height = 13.228346456692900000
          ShowHint = False
          DataField = 'NOMEPLA'
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsListaCtaSdo."NOMEPLA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 92.598425196850400000
        Top = 18.897650000000000000
        Width = 1122.520410000000000000
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Top = 34.015770000000000000
          Width = 1039.370750000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'SALDOS DO PLANO DE CONTAS')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 37.795300000000000000
          Top = 60.472480000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Entidade:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 98.267780000000000000
          Top = 60.472480000000000000
          Width = 472.441250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[VARF_ENTIDADE]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 578.268090000000000000
          Top = 60.472480000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Conta:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 623.622450000000000000
          Top = 60.472480000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[VARF_CONTA]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 37.795300000000000000
          Top = 79.370130000000000000
          Width = 188.976377952756000000
          Height = 13.228346456692900000
          ShowHint = False
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Entidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 226.771800000000000000
          Top = 79.370130000000000000
          Width = 158.740157480315000000
          Height = 13.228346456692900000
          ShowHint = False
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Conta do Plano de Contas')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 385.512060000000000000
          Top = 79.370130000000000000
          Width = 60.472440940000000000
          Height = 13.228346456692900000
          ShowHint = False
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Saldo inicial')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 445.984540000000000000
          Top = 79.370130000000000000
          Width = 60.472440940000000000
          Height = 13.228346456692900000
          ShowHint = False
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Saldo atual')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 506.457020000000000000
          Top = 79.370130000000000000
          Width = 60.472440940000000000
          Height = 13.228346456692900000
          ShowHint = False
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Saldo futuro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 566.929500000000000000
          Top = 79.370130000000000000
          Width = 143.622047244094000000
          Height = 13.228346456692900000
          ShowHint = False
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Sub-grupo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 710.551640000000000000
          Top = 79.370130000000000000
          Width = 143.622047240000000000
          Height = 13.228346456692900000
          ShowHint = False
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Grupo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 854.173780000000000000
          Top = 79.370130000000000000
          Width = 120.944881890000000000
          Height = 13.228346456692900000
          ShowHint = False
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Conjunto')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 975.118740000000000000
          Top = 79.370130000000000000
          Width = 120.944881890000000000
          Height = 13.228346456692900000
          ShowHint = False
          DataSet = frxDsListaCtaSdo
          DataSetName = 'frxDsListaCtaSdo'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Conta do Plano de Contas')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 56.692950000000000000
        Top = 249.448980000000000000
        Width = 1122.520410000000000000
        object Memo24: TfrxMemoView
          Left = 786.142240000000000000
          Width = 272.126160000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'P'#195#161'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsListaCtaSdo: TfrxDBDataset
    UserName = 'frxDsListaCtaSdo'
    CloseDataSource = False
    Left = 264
    Top = 8
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SdoIni'
      'FROM contassdo'
      'WHERE Codigo=:P0'
      'AND Entidade=:P1')
    Left = 204
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesqSdoIni: TFloatField
      FieldName = 'SdoIni'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 172
    Top = 8
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object PMImprime: TPopupMenu
    Left = 12
    Top = 12
    object Listadapesquisa1: TMenuItem
      Caption = '&Lista da pesquisa realizada aqui'
      OnClick = Listadapesquisa1Click
    end
    object Outrasimpresses1: TMenuItem
      Caption = '&Outras impress'#245'es do Plano  de Contas'
      OnClick = Outrasimpresses1Click
    end
  end
  object QrSCart: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Inicial) Inicial'
      'FROM carteiras'
      'WHERE ForneceI=:P0')
    Left = 664
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCartInicial: TFloatField
      FieldName = 'Inicial'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSCart: TDataSource
    DataSet = QrSCart
    Left = 692
    Top = 12
  end
  object QrMCart: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMCartCalcFields
    Left = 720
    Top = 12
    object QrMCartMOVTO: TFloatField
      FieldName = 'MOVTO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMCartATUAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsMCart: TDataSource
    DataSet = QrMCart
    Left = 748
    Top = 12
  end
  object QrFCart: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFCartCalcFields
    Left = 776
    Top = 12
    object QrFCartMOVTO: TFloatField
      FieldName = 'MOVTO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFCartSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsFCart: TDataSource
    DataSet = QrFCart
    Left = 804
    Top = 12
  end
  object QrSCtas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(sdo.SdoIni) SdoIni, '
      'SUM(sdo.SdoAtu) SdoAtu, SUM(sdo.SdoFut) SdoFut'
      'FROM contassdo sdo'
      'WHERE sdo.Entidade=:P0')
    Left = 832
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCtasSdoIni: TFloatField
      FieldName = 'SdoIni'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSCtasSdoAtu: TFloatField
      FieldName = 'SdoAtu'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSCtasSdoFut: TFloatField
      FieldName = 'SdoFut'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSCtas: TDataSource
    DataSet = QrSCtas
    Left = 860
    Top = 12
  end
  object QrContasSdo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.Codigo, cta.Nome NOMECTA, pla.Nome NOMEPLA,'
      'cnj.Nome NOMECNJ, gru.Nome NOMEGRU, sgr.Nome NOMESGR,'
      'sdo.SdoIni, sdo.SdoAtu, sdo.SdoFut, sdo.Entidade, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTI'
      'FROM contassdo sdo'
      'LEFT JOIN contas cta ON cta.Codigo=sdo.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cnj ON cnj.Codigo=gru.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=cnj.Plano'
      'LEFT JOIN entidades ent ON ent.Codigo=sdo.Entidade'
      'WHERE cta.Codigo>0')
    Left = 57
    Top = 9
    object QrContasSdoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasSdoNOMECTA: TWideStringField
      FieldName = 'NOMECTA'
      Size = 50
    end
    object QrContasSdoNOMEPLA: TWideStringField
      FieldName = 'NOMEPLA'
      Size = 50
    end
    object QrContasSdoNOMECNJ: TWideStringField
      FieldName = 'NOMECNJ'
      Size = 50
    end
    object QrContasSdoNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrContasSdoNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrContasSdoSdoIni: TFloatField
      FieldName = 'SdoIni'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrContasSdoSdoAtu: TFloatField
      FieldName = 'SdoAtu'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrContasSdoSdoFut: TFloatField
      FieldName = 'SdoFut'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrContasSdoEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrContasSdoNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    Left = 324
    Top = 12
  end
end
