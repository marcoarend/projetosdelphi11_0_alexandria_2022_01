unit ContasHistSdo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, Grids, DBGrids, dmkDBGrid,
  Db, mySQLDbTables, dmkEdit, DBCtrls, frxClass, frxDBSet, dmkDBGridDAC, Menus,
  Variants, mySQLDirectQuery, dmkPermissoes, dmkGeral, Mask,
  dmkDBLookupComboBox, dmkEditCB;

type
  TFmContasHistSdo = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrPerPla: TmySQLQuery;
    DsPerPla: TDataSource;
    Panel4: TPanel;
    EdCliInt: TdmkEditCB;
    LaCliInt: TLabel;
    CBCliInt: TdmkDBLookupComboBox;
    QrCliIntEntidade: TIntegerField;
    QrCliIntNOMECLIINT: TWideStringField;
    QrPerPlaNOME: TWideStringField;
    QrPerPlaSDOANT: TFloatField;
    QrPerPlaSUMCRE: TFloatField;
    QrPerPlaSUMDEB: TFloatField;
    QrPerPlaSDOFIM: TFloatField;
    QrPerPlaSUMMOV: TFloatField;
    BtAtualiza: TBitBtn;
    QrPerCjt: TmySQLQuery;
    DsPerCjt: TDataSource;
    QrPerCjtNOME: TWideStringField;
    QrPerCjtSDOANT: TFloatField;
    QrPerCjtSUMCRE: TFloatField;
    QrPerCjtSUMDEB: TFloatField;
    QrPerCjtSDOFIM: TFloatField;
    QrPerCjtSUMMOV: TFloatField;
    QrPerGru: TmySQLQuery;
    DsPerGru: TDataSource;
    QrPerGruNOME: TWideStringField;
    QrPerGruSDOANT: TFloatField;
    QrPerGruSUMCRE: TFloatField;
    QrPerGruSUMDEB: TFloatField;
    QrPerGruSDOFIM: TFloatField;
    QrPerGruSUMMOV: TFloatField;
    QrPerCta: TmySQLQuery;
    QrPerSgr: TmySQLQuery;
    DsPerSgr: TDataSource;
    DsPerCta: TDataSource;
    QrPerSgrNOME: TWideStringField;
    QrPerSgrSDOANT: TFloatField;
    QrPerSgrSUMCRE: TFloatField;
    QrPerSgrSUMDEB: TFloatField;
    QrPerSgrSDOFIM: TFloatField;
    QrPerSgrSUMMOV: TFloatField;
    QrPerCtaNOME: TWideStringField;
    QrPerCtaSDOANT: TFloatField;
    QrPerCtaSUMCRE: TFloatField;
    QrPerCtaSUMDEB: TFloatField;
    QrPerCtaSDOFIM: TFloatField;
    QrPerCtaSUMMOV: TFloatField;
    QrPlaSdo: TmySQLQuery;
    QrPlaSdoPeriodo: TIntegerField;
    QrPlaSdoMMYYYY: TWideStringField;
    QrPlaSdoSUMMOV: TFloatField;
    QrPlaSdoSDOANT: TFloatField;
    QrPlaSdoSUMCRE: TFloatField;
    QrPlaSdoSUMDEB: TFloatField;
    QrPlaSdoSDOFIM: TFloatField;
    DsPlaSdo: TDataSource;
    QrPlanos: TmySQLQuery;
    DsPlanos: TDataSource;
    QrPlanosCodigo: TIntegerField;
    QrPlanosNome: TWideStringField;
    QrCjtSdo: TmySQLQuery;
    DsCjtSdo: TDataSource;
    QrCjutos: TmySQLQuery;
    DsCjutos: TDataSource;
    QrGruSdo: TmySQLQuery;
    DsGruSdo: TDataSource;
    QrGrupos: TmySQLQuery;
    DsGrupos: TDataSource;
    QrSgrSdo: TmySQLQuery;
    DsSgrSdo: TDataSource;
    QrSubgru: TmySQLQuery;
    DsSubGru: TDataSource;
    QrCtaSdo: TmySQLQuery;
    DsCtaSdo: TDataSource;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrCjutosCodigo: TIntegerField;
    QrCjutosNome: TWideStringField;
    QrGruposCodigo: TIntegerField;
    QrGruposNome: TWideStringField;
    QrSubgruCodigo: TIntegerField;
    QrSubgruNome: TWideStringField;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrCjtSdoPeriodo: TIntegerField;
    QrCjtSdoMMYYYY: TWideStringField;
    QrCjtSdoSUMMOV: TFloatField;
    QrCjtSdoSDOANT: TFloatField;
    QrCjtSdoSUMCRE: TFloatField;
    QrCjtSdoSUMDEB: TFloatField;
    QrCjtSdoSDOFIM: TFloatField;
    QrGruSdoPeriodo: TIntegerField;
    QrGruSdoMMYYYY: TWideStringField;
    QrGruSdoSUMMOV: TFloatField;
    QrGruSdoSDOANT: TFloatField;
    QrGruSdoSUMCRE: TFloatField;
    QrGruSdoSUMDEB: TFloatField;
    QrGruSdoSDOFIM: TFloatField;
    QrSgrSdoPeriodo: TIntegerField;
    QrSgrSdoMMYYYY: TWideStringField;
    QrSgrSdoSUMMOV: TFloatField;
    QrSgrSdoSDOANT: TFloatField;
    QrSgrSdoSUMCRE: TFloatField;
    QrSgrSdoSUMDEB: TFloatField;
    QrSgrSdoSDOFIM: TFloatField;
    QrCtaSdoPeriodo: TIntegerField;
    QrCtaSdoMMYYYY: TWideStringField;
    QrCtaSdoSUMMOV: TFloatField;
    QrCtaSdoSDOANT: TFloatField;
    QrCtaSdoSUMCRE: TFloatField;
    QrCtaSdoSUMDEB: TFloatField;
    QrCtaSdoSDOFIM: TFloatField;
    QrSdoIni: TmySQLQuery;
    DsSdoIni: TDataSource;
    QrSdoIniCodigo: TIntegerField;
    QrSdoIniSdoIni: TFloatField;
    QrSdoIniNOMECTA: TWideStringField;
    QrSdoIniNOMESGR: TWideStringField;
    QrSdoIniNOMEGRU: TWideStringField;
    QrSdoIniNOMECJT: TWideStringField;
    QrSdoIniNOMEPLA: TWideStringField;
    QrCjtCta: TmySQLQuery;
    QrCjtCtaNOME: TWideStringField;
    QrCjtCtaSUMMOV: TFloatField;
    QrCjtCtaSDOANT: TFloatField;
    QrCjtCtaSUMCRE: TFloatField;
    QrCjtCtaSUMDEB: TFloatField;
    QrCjtCtaSDOFIM: TFloatField;
    DsCjtCta: TDataSource;
    QrCjtCtaConta: TIntegerField;
    frxDsPerPla: TfrxDBDataset;
    frxPerPla: TfrxReport;
    PMImpNivel: TPopupMenu;
    Estenvel1: TMenuItem;
    Todosnveis1: TMenuItem;
    Generoscontrolados1: TMenuItem;
    frxSNG: TfrxReport;
    QrCliIntCliInt: TIntegerField;
    BitBtn1: TBitBtn;
    QrLCS: TmySQLQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Panel3: TPanel;
    Label16: TLabel;
    Label1: TLabel;
    dmkEdPeriodo: TdmkEdit;
    Edit1: TEdit;
    BtImprimeP: TBitBtn;
    CkControlados: TCheckBox;
    PB1: TProgressBar;
    PB2: TProgressBar;
    PB3: TProgressBar;
    PB4: TProgressBar;
    PB5: TProgressBar;
    TabControl1: TTabControl;
    dmkDBPer: TdmkDBGrid;
    TabSheet4: TTabSheet;
    Panel5: TPanel;
    Label3: TLabel;
    EdPla: TdmkEditCB;
    CBPla: TdmkDBLookupComboBox;
    dmkDBGrid6: TdmkDBGrid;
    TabSheet5: TTabSheet;
    Panel6: TPanel;
    Label2: TLabel;
    EdCjt: TdmkEditCB;
    CBCjt: TdmkDBLookupComboBox;
    dmkDBGrid2: TdmkDBGrid;
    dmkDBGrid7: TdmkDBGrid;
    TabSheet6: TTabSheet;
    Panel7: TPanel;
    Label4: TLabel;
    EdGru: TdmkEditCB;
    CBGru: TdmkDBLookupComboBox;
    dmkDBGrid3: TdmkDBGrid;
    TabSheet7: TTabSheet;
    Panel8: TPanel;
    Label5: TLabel;
    EdSgr: TdmkEditCB;
    CBSgr: TdmkDBLookupComboBox;
    dmkDBGrid4: TdmkDBGrid;
    TabSheet8: TTabSheet;
    Panel9: TPanel;
    Label6: TLabel;
    EdCta: TdmkEditCB;
    CBCta: TdmkDBLookupComboBox;
    dmkDBGrid5: TdmkDBGrid;
    BtImprimeA: TBitBtn;
    frxLCS: TfrxReport;
    frxDsLCS: TfrxDBDataset;
    QrLCSNOMEPLANO: TWideStringField;
    QrLCSPLANO: TIntegerField;
    QrLCSNOMECONJUNTO: TWideStringField;
    QrLCSConjunto: TIntegerField;
    QrLCSNOMEGRUPO: TWideStringField;
    QrLCSGrupo: TIntegerField;
    QrLCSNOMESUBGRUPO: TWideStringField;
    QrLCSSubGrupo: TIntegerField;
    QrLCSNOMECONTA: TWideStringField;
    QrLCSGenero: TIntegerField;
    QrLCSSdoIni: TFloatField;
    QrLCSMovto: TFloatField;
    QrLCSVALOR: TFloatField;
    QrSPla: TmySQLQuery;
    QrSPlaSDOINI: TFloatField;
    QrSPlaMOVTO: TFloatField;
    QrSCjt: TmySQLQuery;
    QrSGru: TmySQLQuery;
    QrSSgr: TmySQLQuery;
    QrSGruSDOINI: TFloatField;
    QrSGruMOVTO: TFloatField;
    QrSCjtSDOINI: TFloatField;
    QrSCjtMOVTO: TFloatField;
    QrSSgrSDOINI: TFloatField;
    QrSSgrMOVTO: TFloatField;
    TabSheet10: TTabSheet;
    Panel10: TPanel;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdPesqCta: TEdit;
    EdPesqSgr: TEdit;
    EdPesqGru: TEdit;
    EdPesqCjt: TEdit;
    EdPesqPla: TEdit;
    GradeSdoIni: TdmkDBGrid;
    QrSdoIniCODSDO: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    CkZero: TCheckBox;
    Panel11: TPanel;
    BtAltera: TBitBtn;
    QrSdoOldNew: TmySQLQuery;
    QrSdoOldNewNOMEPLANO: TWideStringField;
    QrSdoOldNewPLANO: TIntegerField;
    QrSdoOldNewNOMECONJUNTO: TWideStringField;
    QrSdoOldNewConjunto: TIntegerField;
    QrSdoOldNewNOMEGRUPO: TWideStringField;
    QrSdoOldNewGrupo: TIntegerField;
    QrSdoOldNewNOMESUBGRUPO: TWideStringField;
    QrSdoOldNewSubGrupo: TIntegerField;
    QrSdoOldNewNOMECONTA: TWideStringField;
    QrSdoOldNewGenero: TIntegerField;
    QrSdoOldNewSdoIni: TFloatField;
    QrSdoOldNewMovto: TFloatField;
    QrSdoOldNewMovNew: TFloatField;
    QrSdoOldNewDiferenca: TFloatField;
    TabSheet9: TTabSheet;
    dmkDBGrid1: TdmkDBGrid;
    DsSdoOldNew: TDataSource;
    Splitter1: TSplitter;
    QrCartSald: TmySQLQuery;
    QrCartSaldCodigo: TIntegerField;
    QrCartSaldNome: TWideStringField;
    QrCartSaldInicial: TFloatField;
    DsCartSald: TDataSource;
    BtPesquisa: TBitBtn;
    BtInclui: TBitBtn;
    QrSumGen: TmySQLQuery;
    QrSumGenGenero: TIntegerField;
    QrSumGenValor: TFloatField;
    QrSdoOldNewIncluir: TFloatField;
    QrSdoOldNewSALDO: TFloatField;
    QrSumCar: TmySQLQuery;
    QrSumCarCarteira: TIntegerField;
    QrSumCarValor: TFloatField;
    QrCartSaldIncluir: TFloatField;
    QrCartSaldSALDO: TFloatField;
    Panel12: TPanel;
    DsTrfOldNew: TDataSource;
    BtMuda: TBitBtn;
    BtExclui: TBitBtn;
    BtGrava: TBitBtn;
    QrTrfOldNew: TmySQLQuery;
    QrTrfOldNewNO_CONTA: TWideStringField;
    QrTrfOldNewNO_Carteira: TWideStringField;
    QrTrfOldNewData: TDateField;
    QrTrfOldNewGenero: TIntegerField;
    QrTrfOldNewCarteira: TIntegerField;
    QrTrfOldNewCredito: TFloatField;
    QrTrfOldNewDebito: TFloatField;
    QrTrfOldNewDescri: TWideStringField;
    QrTrfOldNewAtivo: TIntegerField;
    QrTrfOldNewTIPO_CART: TIntegerField;
    QrTotCar: TmySQLQuery;
    QrTotCarInicial: TFloatField;
    QrTotTrf: TmySQLQuery;
    QrTotTrfValor: TFloatField;
    QrTotGen: TmySQLQuery;
    QrTotGenDiferenca: TFloatField;
    StaticText1: TStaticText;
    Panel13: TPanel;
    StaticText2: TStaticText;
    dmkDBGrid8: TdmkDBGrid;
    Panel14: TPanel;
    dmkDBGrid9: TdmkDBGrid;
    StaticText3: TStaticText;
    Splitter2: TSplitter;
    Label12: TLabel;
    DBEdit1: TDBEdit;
    Label13: TLabel;
    DBEdit2: TDBEdit;
    DsTotCar: TDataSource;
    Label14: TLabel;
    DBEdit3: TDBEdit;
    DsTotGen: TDataSource;
    Label15: TLabel;
    DBEdit4: TDBEdit;
    DataSource1: TDataSource;
    LaAviso: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dmkEdPeriodoChange(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure EdCliIntChange(Sender: TObject);
    procedure BtAtualizaClick(Sender: TObject);
    procedure QrCliIntAfterScroll(DataSet: TDataSet);
    procedure TabControl1Change(Sender: TObject);
    procedure EdPesqCtaChange(Sender: TObject);
    procedure QrCjtSdoAfterScroll(DataSet: TDataSet);
    procedure frxPerPlaGetValue(const VarName: String; var Value: Variant);
    procedure CkControladosClick(Sender: TObject);
    procedure Estenvel1Click(Sender: TObject);
    procedure Todosnveis1Click(Sender: TObject);
    procedure Generoscontrolados1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtImprimePClick(Sender: TObject);
    procedure BtImprimeAClick(Sender: TObject);
    procedure PageControl2Change(Sender: TObject);
    procedure QrLCSCalcFields(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrSdoIniBeforeClose(DataSet: TDataSet);
    procedure QrSdoIniAfterOpen(DataSet: TDataSet);
    procedure CkZeroClick(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure QrSdoOldNewCalcFields(DataSet: TDataSet);
    procedure QrCartSaldCalcFields(DataSet: TDataSet);
    procedure QrTrfOldNewBeforeClose(DataSet: TDataSet);
    procedure QrTrfOldNewAfterOpen(DataSet: TDataSet);
    procedure BtMudaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtGravaClick(Sender: TObject);
  private
    { Private declarations }
    FCliEnt: Integer;
    FActive: Boolean;
    FBD, FSdoOldNew, FTrfOldNew: String;
    //
    function SemClienteInterno(): Boolean;
    procedure MudaPeriodo(Key: Word);
    procedure ReabrePesquisaAll;
    procedure ReabrePesquisaPer;
    procedure ReabrePesquisaPla;
    procedure ReabrePesquisaCjt;
    procedure ReabrePesquisaGru;
    procedure ReabrePesquisaSgr;
    procedure ReabrePesquisaCta;
    procedure ReabrePesquisaIni;
    //
    procedure LctSdoIni(Acao: TSQLType);
  public
    { Public declarations }
    procedure ReopenSdosInis();
  end;

  var
  FmContasHistSdo: TFmContasHistSdo;

implementation

uses UnMyObjects, Module, ContasHistAtz2, MyDBCheck, ModuleFin, Principal, ModuleGeral,
UCreate, SdosIni, UnFinanceiro, UnInternalConsts, UMySQLModule;

{$R *.DFM}

procedure TFmContasHistSdo.EdCliIntChange(Sender: TObject);
begin
  FCliEnt := QrCliIntEntidade.Value;
  ReabrePesquisaAll;
end;

procedure TFmContasHistSdo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasHistSdo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if (PageControl2.ActivePageIndex = 0) then dmkedPeriodo.SetFocus;
  FActive := True;
end;

procedure TFmContasHistSdo.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasHistSdo.dmkEdPeriodoChange(Sender: TObject);
begin
  Edit1.Text := MLAGeral.MesEAnoDoPeriodoLongo(dmkEdPeriodo.ValueVariant);
  ReabrePesquisaAll;
end;

procedure TFmContasHistSdo.MudaPeriodo(Key: Word);
begin
  case key of
    VK_DOWN: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant - 1;
    VK_UP: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant + 1;
    VK_PRIOR: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant - 12;
    VK_NEXT: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant + 12;
  end;
end;

procedure TFmContasHistSdo.PageControl2Change(Sender: TObject);
begin
  ReabrePesquisaAll;
end;

procedure TFmContasHistSdo.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MudaPeriodo(Key);
end;

procedure TFmContasHistSdo.FormCreate(Sender: TObject);
begin
  FBD := Dmod.MyDB.DatabaseName;
  FActive := False;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  dmkedPeriodo.ValueVariant := Geral.Periodo2000(Date);
  QrCliInt.Open;
  QrPlanos.Open;
  QrCjutos.Open;
  QrGrupos.Open;
  QrSubgru.Open;
  QrContas.Open;
end;

procedure TFmContasHistSdo.ReabrePesquisaPer;
var
  CliInt, Periodo, CtrlaSdo: Integer;
begin
  QrPerPla.Close;
  QrPerCjt.Close;
  QrPerGru.Close;
  QrPerSgr.Close;
  QrPerCta.Close;
  if CBCliInt.KeyValue = Null then
  begin
    Application.MessageBox('Informe o cliente interno!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  CliInt  := QrCliIntEntidade.Value;
  Periodo := dmkEdPeriodo.ValueVariant;
  CtrlaSdo := MLAGeral.BoolToInt2(CkControlados.Checked, 0, -1);
  //
  QrPerPla.Close;
  QrPerPla.Params[00].AsInteger := CliInt;
  QrPerPla.Params[01].AsInteger := Periodo;
  QrPerPla.Params[02].AsInteger := CtrlaSdo;
  QrPerPla.Open;
  //
  QrPerCjt.Close;
  QrPerCjt.Params[00].AsInteger := CliInt;
  QrPerCjt.Params[01].AsInteger := Periodo;
  QrPerCjt.Params[02].AsInteger := CtrlaSdo;
  QrPerCjt.Open;
  //
  QrPerGru.Close;
  QrPerGru.Params[00].AsInteger := CliInt;
  QrPerGru.Params[01].AsInteger := Periodo;
  QrPerGru.Params[02].AsInteger := CtrlaSdo;
  QrPerGru.Open;
  //
  QrPerSgr.Close;
  QrPerSgr.Params[00].AsInteger := CliInt;
  QrPerSgr.Params[01].AsInteger := Periodo;
  QrPerSgr.Params[02].AsInteger := CtrlaSdo;
  QrPerSgr.Open;
  //
  QrPerCta.Close;
  QrPerCta.Params[00].AsInteger := CliInt;
  QrPerCta.Params[01].AsInteger := Periodo;
  QrPerCta.Params[02].AsInteger := CtrlaSdo;
  QrPerCta.Open;
end;

procedure TFmContasHistSdo.BtPesquisaClick(Sender: TObject);
var
  Mez: Integer;
begin
  EdCliInt.Enabled := False;
  CBCliInt.Enabled := False;
  LaCliInt.Enabled := False;
  //
  DmodFin.AtualizaContasHistSdo3(QrCliIntEntidade.Value, LaAviso);
  FSdoOldNew := UCriar.RecriaTempTable('SdoOldNew', DmodG.QrUpdPID1, False);
  FTrfOldNew := UCriar.RecriaTempTable('TrfOldNew', DmodG.QrUpdPID1, False);
  Mez := MLAGeral.PeriodoToMez(dmkEdPeriodo.ValueVariant);
  //
  DModG.QrUpdPID1.Close;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO sdooldnew');
  DModG.QrUpdPID1.SQL.Add('SELECT pla.Nome NOMEPLANO, pla.Codigo PLANO,');
  DModG.QrUpdPID1.SQL.Add('cjt.Nome NOMECONJUNTO, gru.Conjunto,');
  DModG.QrUpdPID1.SQL.Add('gru.Nome NOMEGRUPO, sgr.Grupo,');
  DModG.QrUpdPID1.SQL.Add('sgr.Nome NOMESUBGRUPO, cta.SubGrupo,');
  DModG.QrUpdPID1.SQL.Add('cta.Nome NOMECONTA, cta.Codigo Genero,');
  DModG.QrUpdPID1.SQL.Add('0 SdoIni, SUM(his.SdoFim) Movto,');
  DModG.QrUpdPID1.SQL.Add('0 MovNew, 0 Diferenca, 0 Ativo');
  DModG.QrUpdPID1.SQL.Add('FROM '+fbd+'.contashis his');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+fbd+'.contas    cta ON cta.Codigo=his.Codigo');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+fbd+'.subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+fbd+'.grupos    gru ON gru.Codigo=sgr.Grupo');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+fbd+'.conjuntos cjt ON cjt.Codigo=gru.Conjunto');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+fbd+'.plano     pla ON pla.Codigo=cjt.Plano');
  DModG.QrUpdPID1.SQL.Add('WHERE his.Entidade=:P0');
  DModG.QrUpdPID1.SQL.Add('AND his.Periodo=:P1');
  DModG.QrUpdPID1.SQL.Add('GROUP BY cta.Codigo');
  DModG.QrUpdPID1.SQL.Add('ORDER BY cta.OrdemLista, cta.Nome');
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.Params[00].AsInteger := QrCliIntEntidade.Value;
  DModG.QrUpdPID1.Params[01].AsInteger := dmkEdPeriodo.ValueVariant;
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  //
  DModG.QrUpdPID1.Close;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO sdooldnew');
  DModG.QrUpdPID1.SQL.Add('SELECT pla.Nome NOMEPLANO, pla.Codigo PLANO,');
  DModG.QrUpdPID1.SQL.Add('cjt.Nome NOMECONJUNTO, gru.Conjunto,');
  DModG.QrUpdPID1.SQL.Add('gru.Nome NOMEGRUPO, sgr.Grupo,');
  DModG.QrUpdPID1.SQL.Add('sgr.Nome NOMESUBGRUPO, con.SubGrupo,');
  DModG.QrUpdPID1.SQL.Add('con.Nome NOMECONTA, mov.Genero,');
  DModG.QrUpdPID1.SQL.Add('0 SdoIni, 0 Movto,');
  DModG.QrUpdPID1.SQL.Add('SUM(mov.Movim) MovNew, 0 Diferenca, 0 Ativo');
  DModG.QrUpdPID1.SQL.Add('FROM '+fbd+'.contasmov mov');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+fbd+'.contas    con ON con.Codigo=mov.Genero');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+fbd+'.subgrupos sgr ON sgr.Codigo=mov.SubGrupo');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+fbd+'.grupos    gru ON gru.Codigo=mov.Grupo');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+fbd+'.conjuntos cjt ON cjt.Codigo=mov.Conjunto');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+fbd+'.plano     pla ON pla.Codigo=mov.Plano');
  DModG.QrUpdPID1.SQL.Add('WHERE mov.CliInt=:P0');
  DModG.QrUpdPID1.SQL.Add('AND mov.Mez <=:P1');
  DModG.QrUpdPID1.SQL.Add('GROUP BY mov.Genero');
  DModG.QrUpdPID1.SQL.Add('ORDER BY pla.OrdemLista, NOMEPLANO, cjt.OrdemLista,');
  DModG.QrUpdPID1.SQL.Add('NOMECONJUNTO, gru.OrdemLista, NOMEGRUPO, sgr.OrdemLista,');
  DModG.QrUpdPID1.SQL.Add('NOMESUBGRUPO, con.OrdemLista, NOMECONTA, mov.Mez');
  DModG.QrUpdPID1.Params[00].AsInteger := QrCliIntEntidade.Value;
  DModG.QrUpdPID1.Params[01].AsInteger := Mez;
  DModG.QrUpdPID1.SQL.Add('');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  //
  DModG.QrUpdPID1.Close;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO sdooldnew');
  DModG.QrUpdPID1.SQL.Add('SELECT NOMEPLANO, PLANO, NOMECONJUNTO, Conjunto,');
  DModG.QrUpdPID1.SQL.Add('NOMEGRUPO, Grupo, NOMESUBGRUPO, SubGrupo,');
  DModG.QrUpdPID1.SQL.Add('NOMECONTA, Genero, SUM(SdoIni) SdoIni, SUM(Movto) Movto,');
  DModG.QrUpdPID1.SQL.Add('SUM(MovNew) MovNew, SUM(Movto-MovNew) Diferenca, 1 Ativo');
  DModG.QrUpdPID1.SQL.Add('FROM sdooldnew');
  DModG.QrUpdPID1.SQL.Add('GROUP BY Genero');
  DModG.QrUpdPID1.SQL.Add('');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  //
  DModG.QrUpdPID1.Close;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM sdooldnew');
  DModG.QrUpdPID1.SQL.Add('WHERE Ativo=0');
  DModG.QrUpdPID1.SQL.Add('');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  ReopenSdosInis();
  //
  if QrTotCarInicial.Value = QrTotGenDiferenca.Value then
  begin
    BtPesquisa.Enabled := False;
    BtInclui.Enabled := True;
  end else begin
    Application.MessageBox(PChar('ERRO!! Saldos iniciais n�o conferem com ' +
      'necessidade de saldos iniciais do plano de contas!'sLineBreak +
      'Diferen�a = ' + Geral.FFT(QrTotCarInicial.Value -
      QrTotGenDiferenca.Value, 2, siNegativo)), 'Aviso',
      MB_OK+MB_ICONERROR);
    EdCliInt.Enabled := True;
    CBCliInt.Enabled := True;
    LaCliInt.Enabled := True;
    //
  end;
end;

procedure TFmContasHistSdo.BtIncluiClick(Sender: TObject);
begin
  LctSdoIni(stIns);
end;

procedure TFmContasHistSdo.BtMudaClick(Sender: TObject);
begin
  LctSdoIni(stUpd);
end;

procedure TFmContasHistSdo.LctSdoIni(Acao: TSQLType);
begin
  if DBCheck.CriaFm(TFmSdosIni, FmSdosIni, afmoNegarComAviso) then
  begin
    FmSdosIni.LaTipo.SQLType := Acao;
    if Acao = stUpd then
    begin
      // Parei Aqui
    end;
    FmSdosIni.ShowModal;
    FmSdosIni.Destroy;
  end;
end;

procedure TFmContasHistSdo.BtAlteraClick(Sender: TObject);
var
  NewVal: String;
  Codigo: Integer;
begin
  Codigo := QrSdoIniCodigo.Value;
  NewVal := Geral.FFT(QrSdoIniSdoIni.Value, 2, siNegativo);
  if InputQuery('Novo Saldo Inicial', 'Informe o novo saldo inicial:', NewVal) then
  begin
    Dmod.QrUpd.SQL.Clear;
    if QrSdoIniCODSDO.Value <> 0 then
    begin
     Dmod.QrUpd.SQL.Add('UPDATE contassdo SET SdoIni=:P0 ');
     Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 AND Entidade=:P2 ');
    end else begin
     Dmod.QrUpd.SQL.Add('INSERT INTO contassdo SET SdoIni=:P0, ');
     Dmod.QrUpd.SQL.Add('Codigo=:P1, Entidade=:P2 ');
    end;
    Dmod.QrUpd.Params[00].AsFloat   := Geral.DMV(NewVal);
    Dmod.QrUpd.Params[01].AsInteger := Codigo;
    Dmod.QrUpd.Params[02].AsInteger := FCliEnt;
    Dmod.QrUpd.ExecSQL;
    //
    if DBCheck.CriaFm(TFmContasHistAtz2, FmContasHistAtz2, afmoNegarComAviso) then
    begin
      FmContasHistAtz2.FEntiCod  := QrCliIntEntidade.Value;
      FmContasHistAtz2.FGenero   := QrSdoIniCodigo.Value;
      FmContasHistAtz2.ShowModal;
      FmContasHistAtz2.Destroy;
      //
      ReabrePesquisaAll;
      QrSdoIni.Locate('Codigo', Codigo, []);
    end;
  end;
end;

procedure TFmContasHistSdo.BtAtualizaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasHistAtz2, FmContasHistAtz2, afmoNegarComAviso) then
  begin
    FmContasHistAtz2.FEntiCod := QrCliIntEntidade.Value;
    FmContasHistAtz2.ShowModal;
    FmContasHistAtz2.Destroy;
    //
    ReabrePesquisaAll;
  end;
end;

procedure TFmContasHistSdo.BtExcluiClick(Sender: TObject);
var
  Genero, Carteira: Integer;
begin
  if Application.MessageBox('Confirma a retirada do lan�amento?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Genero   := QrTrfOldNewGenero.Value;
    Carteira := QrTrfOldNewCarteira.Value;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('DELETE FROM trfoldnew WHERE ');
    DmodG.QrUpdPID1.SQL.Add('Genero=:P0 AND Carteira=:P1');
    DmodG.QrUpdPID1.Params[00].AsInteger := Genero;
    DmodG.QrUpdPID1.Params[01].AsInteger := Carteira;
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
    //
    ReopenSdosInis();
  end;
end;

procedure TFmContasHistSdo.BtGravaClick(Sender: TObject);
var
  QtdFalt: Integer;
begin
  QtdFalt := 0;
  //  Verificar se dados incluidos de contas est�o ok!
  QrSdoOldNew.DisableControls;
  QrSdoOldNew.First;
  while not QrSdoOldNew.Eof do
  begin
    if QrSdoOldNewSALDO.Value <> 0 then
      QtdFalt := QtdFalt + 1;
    //
    QrSdoOldNew.Next;
  end;
  QrSdoOldNew.EnableControls;
  if QtdFalt > 0 then
  begin
    Application.MessageBox(PChar('Grava��o cancelada! Existem contas com ' +
    'lan�amento(s) a definir!'sLineBreak'Verifique se foram usadas apenas as ' +
    'contas e carteiras listadas nas suas grades!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  //  Verificar se dados incluidos de carteiras est�o ok!
  QrCartSald.DisableControls;
  QrCartSald.First;
  while not QrCartSald.Eof do
  begin
    if QrCartSaldSALDO.Value <> 0 then
      QtdFalt := QtdFalt + 1;
    //
    QrCartSald.Next;
  end;
  QrCartSald.EnableControls;
  if QtdFalt > 0 then
  begin
    Application.MessageBox(PChar('Grava��o cancelada! Existem carteiras com ' +
    'lan�amento(s) a definir!'sLineBreak'Verifique se foram usadas apenas as ' +
    'carteiras e contas listadas nas suas grades!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;

  // Caso tudo ok, inclui lan�amentos
  if Application.MessageBox('Confirma a efetiva��o dos lan�amentos?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    QrTrfOldNew.First;
    while not QrTrfOldNew.Eof do
    begin
      UFinanceiro.LancamentoDefaultVARS;
      //      QrTrfOldNew.Value;
      FLAN_Data          := Geral.FDT(QrTrfOldNewData.Value, 1);
      FLAN_Vencimento    := Geral.FDT(QrTrfOldNewData.Value, 1);
      FLAN_DataCad       := Geral.FDT(Date, 1);
      FLAN_Descricao     := QrTrfOldNewDescri.Value;
      FLAN_Compensado    := Geral.FDT(QrTrfOldNewData.Value, 1);
      FLAN_Tipo          := QrTrfOldNewTIPO_CART.Value;
      FLAN_Carteira      := QrTrfOldNewCarteira.Value;
      FLAN_Credito       := QrTrfOldNewCredito.Value;
      FLAN_Debito        := QrTrfOldNewDebito.Value;
      FLAN_Genero        := QrTrfOldNewGenero.Value;
      FLAN_Sit           := 3;
      FLAN_UserCad       := VAR_USUARIO;
      FLAN_DataDoc       := Geral.FDT(Date, 1);
      FLAN_CliInt        := QrCliIntEntidade.Value;
      //
      FLAN_FatID         := -1;
      FLAN_FatID_Sub     := 0;
      FLAN_FatNum        := 0;
      FLAN_FatParcela    := 0;
      //
      {
      FLAN_Mez           := '';
      FLAN_Duplicata     := '';
      FLAN_Doc2          := '';
      FLAN_SerieCH       := '';
      FLAN_Documento     := 0;
      FLAN_SerieNF       := '';
      FLAN_NotaFiscal    := 0;
      FLAN_Sub           := 0;
      FLAN_ID_Pgto       := 0;
      FLAN_Cartao        := 0;
      FLAN_Linha         := 0;
      FLAN_Fornecedor    := 0;
      FLAN_Cliente       := 0;
      FLAN_MoraDia       := 0;
      FLAN_Multa         := 0;
      FLAN_Vendedor      := 0;
      FLAN_Account       := 0;
      FLAN_ICMS_P        := 0;
      FLAN_ICMS_V        := 0;
      FLAN_Depto         := 0;
      FLAN_DescoPor      := 0;
      FLAN_ForneceI      := 0;
      FLAN_DescoVal      := 0;
      FLAN_NFVal         := 0;
      FLAN_Unidade       := 0;
      FLAN_Qtde          := 0;
      FLAN_MultaVal      := 0;
      FLAN_MoraVal       := 0;
      FLAN_CtrlIni       := 0;
      FLAN_Nivel         := 0;
      FLAN_CNAB_Sit      := 0;
      FLAN_TipoCH        := 0;
      FLAN_Atrelado      := 0;
      FLAN_SubPgto1      := 0;
      FLAN_MultiPgto     := 0;
      //
      FLAN_Emitente      := '';
      FLAN_CNPJCPF       := '';
      FLAN_Banco         := 0;
      FLAN_Agencia       := '';
      FLAN_ContaCorrente := '';
      }
      FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', VAR_LCT, VAR_LCT, 'Controle');
      UFinanceiro.InsereLancamento();
      //
      QrTrfOldNew.Next;
    end;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE carteiras SET Inicial=0 WHERE ForneceI=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrCliIntEntidade.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE contassdo SET SdoIni=0 WHERE Entidade=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrCliIntEntidade.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Screen.Cursor := crDefault;
    Close;
  end;
end;

procedure TFmContasHistSdo.BtImprimeAClick(Sender: TObject);
begin
  if SemClienteInterno() then Exit;
  QrLCS.Close;
  QrLCS.SQL.Clear;
  QrLCS.SQL.Add('SELECT pla.Nome NOMEPLANO, pla.Codigo PLANO,');
  QrLCS.SQL.Add('cjt.Nome NOMECONJUNTO, gru.Conjunto,');
  QrLCS.SQL.Add('gru.Nome NOMEGRUPO, sgr.Grupo,');
  QrLCS.SQL.Add('sgr.Nome NOMESUBGRUPO, con.SubGrupo,');
  QrLCS.SQL.Add('con.Nome NOMECONTA, lan.Genero,');
  QrLCS.SQL.Add('sdo.SdoIni, SUM(lan.Credito - lan.Debito) Movto');
  QrLCS.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrLCS.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrLCS.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lan.Genero');
  QrLCS.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo');
  QrLCS.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
  QrLCS.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
  QrLCS.SQL.Add('LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano');
  QrLCS.SQL.Add('LEFT JOIN contassdo sdo ON sdo.Codigo=con.Codigo AND sdo.Entidade =:P0');
  QrLCS.SQL.Add('WHERE car.ForneceI=:P1');
  QrLCS.SQL.Add('AND car.Tipo < 2');
  QrLCS.SQL.Add('AND lan.Genero>0');
  QrLCS.SQL.Add('GROUP BY lan.Genero');
  QrLCS.SQL.Add('ORDER BY pla.OrdemLista, NOMEPLANO, cjt.OrdemLista,');
  QrLCS.SQL.Add('NOMECONJUNTO, gru.OrdemLista, NOMEGRUPO, sgr.OrdemLista,');
  QrLCS.SQL.Add('NOMESUBGRUPO, con.OrdemLista, NOMECONTA, lan.Mez');
  QrLCS.Params[00].AsInteger := QrCliIntEntidade.Value;
  QrLCS.Params[01].AsInteger := QrCliIntEntidade.Value;
  QrLCS.Open;
  //
  MyObjects.frxMostra(frxLCS, 'Saldo atual do plano de contas');
end;

procedure TFmContasHistSdo.BtImprimePClick(Sender: TObject);
begin
  //if PageControl1.ActivePageIndex = 0 then
  //begin
    if SemClienteInterno() then Exit;
    MyObjects.MostraPopUpDeBotao(PMImpNivel, BtImprimeP);
  //end else begin
    //
  //end;
end;

procedure TFmContasHistSdo.QrCliIntAfterScroll(DataSet: TDataSet);
begin
  BtAtualiza.Enabled := True;
end;

procedure TFmContasHistSdo.QrLCSCalcFields(DataSet: TDataSet);
begin
  QrLCSVALOR.Value := QrLCSSdoIni.Value + QrLCSMovto.Value;
end;

procedure TFmContasHistSdo.QrSdoIniAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrSdoIni.RecordCount > 0;
end;

procedure TFmContasHistSdo.QrSdoIniBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
end;

procedure TFmContasHistSdo.QrSdoOldNewCalcFields(DataSet: TDataSet);
begin
  QrSdoOldNewSALDO.Value := QrSdoOldNewDiferenca.Value - QrSdoOldNewIncluir.Value;
end;

procedure TFmContasHistSdo.QrTrfOldNewAfterOpen(DataSet: TDataSet);
begin
  BtMuda.Enabled   := QrTrfOldNew.RecordCount > 0;
  BtExclui.Enabled := QrTrfOldNew.RecordCount > 0;
end;

procedure TFmContasHistSdo.QrTrfOldNewBeforeClose(DataSet: TDataSet);
begin
  BtMuda.Enabled := False;
  BtExclui.Enabled := False;
end;

procedure TFmContasHistSdo.TabControl1Change(Sender: TObject);
begin
  case TabControl1.TabIndex of
    0: dmkDBPer.DataSource := DsPerPla;
    1: dmkDBPer.DataSource := DsPerCjt;
    2: dmkDBPer.DataSource := DsPerGru;
    3: dmkDBPer.DataSource := DsPerSgr;
    4: dmkDBPer.DataSource := DsPerCta;
  end;
end;

procedure TFmContasHistSdo.ReabrePesquisaAll;
begin
  QrPerPla.Close;
  QrPerCjt.Close;
  QrPerGru.Close;
  QrPerSgr.Close;
  QrPerCta.Close;
  //
  QrPlaSdo.Close;
  QrCjtSdo.Close;
  QrGruSdo.Close;
  QrSgrSdo.Close;
  QrCtaSdo.Close;
  //
  QrSdoIni.Close;
  //
  if not FActive then Exit;
  //
  case PageControl1.ActivePageIndex of
    0:
    begin
      case PageControl2.ActivePageIndex of
        0: ReabrePesquisaPer;
        1: ReabrePesquisaPla;
        2: ReabrePesquisaCjt;
        3: ReabrePesquisaGru;
        4: ReabrePesquisaSgr;
        5: ReabrePesquisaCta;
      end;
    end;
    2: ReabrePesquisaIni;
  end;
end;

procedure TFmContasHistSdo.ReabrePesquisaPla;
var
  Pla: Integer;
begin
  QrPlaSdo.Close;
  //
  Pla := Geral.IMV(EdPla.Text);
  if (CBPla.KeyValue <> Null) and (FCliEnt <> 0) then
  begin
    QrPlaSdo.Params[00].AsInteger := FCliEnt;
    QrPlaSdo.Params[01].AsInteger := Pla;
    QrPlaSdo.Open;
  end;
end;

procedure TFmContasHistSdo.ReabrePesquisaCjt;
var
  Cjt: Integer;
begin
  QrCjtSdo.Close;
  //
  Cjt := Geral.IMV(EdCjt.Text);
  if (CBCjt.KeyValue <> Null) and (FCliEnt <> 0) then
  begin
    QrCjtSdo.Params[00].AsInteger := FCliEnt;
    QrCjtSdo.Params[01].AsInteger := Cjt;
    QrCjtSdo.Open;
  end;
end;

procedure TFmContasHistSdo.ReabrePesquisaGru;
var
  Gru: Integer;
begin
  QrGruSdo.Close;
  //
  Gru := Geral.IMV(EdGru.Text);
  if (CBGru.KeyValue <> Null) and (FCliEnt <> 0) then
  begin
    QrGruSdo.Params[00].AsInteger := FCliEnt;
    QrGruSdo.Params[01].AsInteger := Gru;
    QrGruSdo.Open;
  end;
end;

procedure TFmContasHistSdo.ReabrePesquisaSgr;
var
  Sgr: Integer;
begin
  QrSgrSdo.Close;
  //
  Sgr := Geral.IMV(EdSgr.Text);
  if (CBSgr.KeyValue <> Null) and (FCliEnt <> 0) then
  begin
    QrSgrSdo.Params[00].AsInteger := FCliEnt;
    QrSgrSdo.Params[01].AsInteger := Sgr;
    QrSgrSdo.Open;
  end;
end;

procedure TFmContasHistSdo.ReopenSdosInis;
begin
  QrSumCar.Close;
  QrSumCar.Open;
  //
  QrSumGen.Close;
  QrSumGen.Open;
  //
  QrCartSald.Close;
  QrCartSald.Params[00].AsInteger := QrCliIntEntidade.Value;
  QrCartSald.Open;
  //
  QrSdoOldNew.Close;
  QrSdoOldNew.Open;
  //
  QrTrfOldNew.Close;
  QrTrfOldNew.SQL.Clear;
  QrTrfOldNew.SQL.Add('SELECT con.Nome NO_CONTA, car.Nome NO_Carteira, ');
  QrTrfOldNew.SQL.Add('car.Tipo TIPO_CART, ton.*');
  QrTrfOldNew.SQL.Add('FROM trfoldnew ton');
  QrTrfOldNew.SQL.Add('LEFT JOIN '+fbd+'.contas con ON con.Codigo=ton.Genero');
  QrTrfOldNew.SQL.Add('LEFT JOIN '+fbd+'.carteiras car ON car.Codigo=ton.Carteira');
  QrTrfOldNew.Open;
  //
  QrTotCar.Close;
  QrTotCar.Params[00].AsInteger := QrCliIntEntidade.Value;
  QrTotCar.Open;
  //
  QrTotGen.Close;
  QrTotGen.Open;
  //
  QrTotTrf.Close;
  QrTotTrf.Open;
  //
  BtGrava.Enabled := QrTotCarInicial.Value = QrTotTrfValor.Value;
end;

procedure TFmContasHistSdo.ReabrePesquisaCta;
var
  Cta: Integer;
begin
  QrCtaSdo.Close;
  //
  Cta := Geral.IMV(EdCta.Text);
  if (CBCta.KeyValue <> Null) and (FCliEnt <> 0) then
  begin
    QrCtaSdo.Params[00].AsInteger := FCliEnt;
    QrCtaSdo.Params[01].AsInteger := Cta;
    QrCtaSdo.Open;
  end;
end;

procedure TFmContasHistSdo.EdPesqCtaChange(Sender: TObject);
begin
  ReabrePesquisaAll;
end;

procedure TFmContasHistSdo.ReabrePesquisaIni;
begin
  QrSdoIni.Close;
  //
  if (FCliEnt <> 0) then
  begin
    QrSdoIni.Close;
    QrSdoIni.SQL.Clear;
    QrSdoIni.SQL.Add('SELECT cta.Codigo, sdo.Codigo CODSDO, ');
    QrSdoIni.SQL.Add('sdo.SdoIni, cta.Nome NOMECTA,');
    QrSdoIni.SQL.Add('sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
    QrSdoIni.SQL.Add('cjt.Nome NOMECJT, pla.Nome NOMEPLA');
    QrSdoIni.SQL.Add('');
    QrSdoIni.SQL.Add('FROM contas cta');
    QrSdoIni.SQL.Add('LEFT JOIN contassdo sdo ON cta.Codigo=sdo.Codigo ');
    QrSdoIni.SQL.Add('  AND sdo.Entidade=' + FormatFloat('0', FCliEnt));
    QrSdoIni.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.SubGrupo');
    QrSdoIni.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
    QrSdoIni.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrSdoIni.SQL.Add('LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano');
    QrSdoIni.SQL.Add('WHERE cta.Codigo<>0');
    QrSdoIni.SQL.Add('');
    if Trim(EdPesqCta.Text) <> '' then
      QrSdoIni.SQL.Add('AND cta.Nome LIKE "%' + EdPesqCta.Text + '%"');
    if Trim(EdPesqSgr.Text) <> '' then
      QrSdoIni.SQL.Add('AND sgr.Nome LIKE "%' + EdPesqSgr.Text + '%"');
    if Trim(EdPesqGru.Text) <> '' then
      QrSdoIni.SQL.Add('AND gru.Nome LIKE "%' + EdPesqGru.Text + '%"');
    if Trim(EdPesqCjt.Text) <> '' then
      QrSdoIni.SQL.Add('AND cjt.Nome LIKE "%' + EdPesqCjt.Text + '%"');
    if Trim(EdPesqPla.Text) <> '' then
      QrSdoIni.SQL.Add('AND pla.Nome LIKE "%' + EdPesqPla.Text + '%"');
    if CkZero.Checked then
      QrSdoIni.SQL.Add('AND sdo.SdoIni <> 0');
    QrSdoIni.SQL.Add('');
    QrSdoIni.SQL.Add('ORDER BY cta.Nome');
    QrSdoIni.SQL.Add('');
    QrSdoIni.Open;
  end;
end;

procedure TFmContasHistSdo.QrCartSaldCalcFields(DataSet: TDataSet);
begin
  QrCartSaldSALDO.Value := QrCartSaldInicial.Value - QrCartSaldIncluir.Value;
end;

procedure TFmContasHistSdo.QrCjtSdoAfterScroll(DataSet: TDataSet);
begin
  QrCjtCta.Close;
  QrCjtCta.Params[00].AsInteger := FCliEnt;
  QrCjtCta.Params[01].AsInteger := QrCjtSdoPeriodo.Value;
  QrCjtCta.Params[02].AsInteger := Geral.IMV(EdCjt.Text);
  QrCjtCta.Open;
end;

procedure TFmContasHistSdo.frxPerPlaGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then
    Value := Edit1.Text
  else if AnsiCompareText(VarName, 'VARF_CLIINT') = 0 then
    Value := CBCliInt.Text
  else if AnsiCompareText(VarName, 'VARF_NIVEL') = 0 then
    Value := TabControl1.Tabs[TabControl1.TabIndex]
  else if AnsiCompareText(VarName, 'VARF_SUM_PLA') = 0 then
  begin
    QrSPla.Close;
    QrSPla.SQL.Clear;
    QrSPla.SQL.Add('SELECT  ');
    QrSPla.SQL.Add('SUM(sdo.SdoIni) SDOINI, SUM(lan.Credito - lan.Debito) MOVTO');
    QrSPla.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrSPla.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrSPla.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lan.Genero');
    QrSPla.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrSPla.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
    QrSPla.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrSPla.SQL.Add('LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano');
    QrSPla.SQL.Add('LEFT JOIN contassdo sdo ON sdo.Codigo=con.Codigo AND sdo.Entidade =:P0');
    QrSPla.SQL.Add('WHERE car.ForneceI=:P1');
    QrSPla.SQL.Add('AND pla.Codigo=:P2');
    QrSPla.SQL.Add('AND car.Tipo < 2');
    QrSPla.SQL.Add('AND lan.Genero>0');
    QrSPla.Params[00].AsInteger := FCliEnt;
    QrSPla.Params[01].AsInteger := FCliEnt;
    QrSPla.Params[02].AsInteger := QrLCSPLANO.Value;
    QrSPla.Open;
    Value := QrSPlaMOVTO.Value + QrSPlaSDOINI.Value;
  end
  else if AnsiCompareText(VarName, 'VARF_SUM_CJT') = 0 then
  begin
    QrSCjt.Close;
    QrSCjt.SQL.Clear;
    QrSCjt.SQL.Add('SELECT');
    QrSCjt.SQL.Add('SUM(sdo.SdoIni) SDOINI, SUM(lan.Credito - lan.Debito) MOVTO');
    QrSCjt.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrSCjt.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrSCjt.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lan.Genero');
    QrSCjt.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrSCjt.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
    QrSCjt.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrSCjt.SQL.Add('LEFT JOIN contassdo sdo ON sdo.Codigo=con.Codigo AND sdo.Entidade =:P0');
    QrSCjt.SQL.Add('WHERE car.ForneceI=:P1');
    QrSCjt.SQL.Add('AND cjt.Codigo=:P2');
    QrSCjt.SQL.Add('AND car.Tipo < 2');
    QrSCjt.SQL.Add('AND lan.Genero>0');
    QrSCjt.Params[00].AsInteger := FCliEnt;
    QrSCjt.Params[01].AsInteger := FCliEnt;
    QrSCjt.Params[02].AsInteger := QrLCSConjunto.Value;
    QrSCjt.Open;
    Value := QrSCjtMOVTO.Value + QrSCjtSDOINI.Value;
  end
  else if AnsiCompareText(VarName, 'VARF_SUM_GRU') = 0 then
  begin
    QrSGru.Close;
    QrSGru.SQL.Clear;
    QrSGru.SQL.Add('SELECT');
    QrSGru.SQL.Add('SUM(sdo.SdoIni) SDOINI, SUM(lan.Credito - lan.Debito) MOVTO');
    QrSGru.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrSGru.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrSGru.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lan.Genero');
    QrSGru.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrSGru.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
    QrSGru.SQL.Add('LEFT JOIN contassdo sdo ON sdo.Codigo=con.Codigo AND sdo.Entidade =:P0');
    QrSGru.SQL.Add('WHERE car.ForneceI=:P1');
    QrSGru.SQL.Add('AND gru.Codigo=:P2');
    QrSGru.SQL.Add('AND car.Tipo < 2');
    QrSGru.SQL.Add('AND lan.Genero>0');
    QrSGru.Params[00].AsInteger := FCliEnt;
    QrSGru.Params[01].AsInteger := FCliEnt;
    QrSGru.Params[02].AsInteger := QrLCSGrupo.Value;
    QrSGru.Open;
    Value := QrSGruMOVTO.Value + QrSGruSDOINI.Value;
  end
  else if AnsiCompareText(VarName, 'VARF_SUM_SGR') = 0 then
  begin
    QrSSgr.Close;
    QrSSgr.SQL.Clear;
    QrSSgr.SQL.Add('SELECT');
    QrSSgr.SQL.Add('SUM(sdo.SdoIni) SDOINI, SUM(lan.Credito - lan.Debito) MOVTO');
    QrSSgr.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrSSgr.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrSSgr.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lan.Genero');
    QrSSgr.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrSSgr.SQL.Add('LEFT JOIN contassdo sdo ON sdo.Codigo=con.Codigo AND sdo.Entidade =:P0');
    QrSSgr.SQL.Add('WHERE car.ForneceI=:P1');
    QrSSgr.SQL.Add('AND sgr.Codigo=:P2');
    QrSSgr.SQL.Add('AND car.Tipo < 2');
    QrSSgr.SQL.Add('AND lan.Genero>0');
    QrSSgr.Params[00].AsInteger := FCliEnt;
    QrSSgr.Params[01].AsInteger := FCliEnt;
    QrSSgr.Params[02].AsInteger := QrLCSSubGrupo.Value;
    QrSSgr.Open;
    Value := QrSSgrMOVTO.Value + QrSSgrSDOINI.Value;
  end;
end;

procedure TFmContasHistSdo.CkControladosClick(Sender: TObject);
begin
  ReabrePesquisaPer;
end;

procedure TFmContasHistSdo.CkZeroClick(Sender: TObject);
begin
  ReabrePesquisaAll;
end;

procedure TFmContasHistSdo.Estenvel1Click(Sender: TObject);
begin
  frxDsPerPla.DataSource := dmkDBPer.DataSource;
  MyObjects.frxMostra(frxPerPla, 'Saldos de n�vel de contas');
end;

procedure TFmContasHistSdo.Todosnveis1Click(Sender: TObject);
begin
  DmodFin.SaldosNiveisPeriodo_2(QrCliIntEntidade.Value,
    dmkEdPeriodo.ValueVariant, MLAGeral.BoolToInt2(CkControlados.Checked, 0, -1),
    True, PB1, PB2, PB3, PB4, PB5, DmodFin.QrSNG);
  MyObjects.frxMostra(frxSNG, 'Saldos dos N�veis do Plano de Contas');
end;

procedure TFmContasHistSdo.Generoscontrolados1Click(Sender: TObject);
begin
  if DmodFin.SaldosNiveisPeriodo_2(QrCliIntEntidade.Value,
  dmkEdPeriodo.ValueVariant, MLAGeral.BoolToInt2(CkControlados.Checked, 0, -1),
  False, PB1, PB2, PB3, PB4, PB5, DmodFin.QrSNG) then
    MyObjects.frxMostra(frxSNG, 'Saldos dos N�veis do Plano de Contas');
end;

procedure TFmContasHistSdo.BitBtn1Click(Sender: TObject);
begin
  FmPrincipal.CadastroDeContasNiv;
end;

function TFmContasHistSdo.SemClienteInterno(): Boolean;
begin
  if CBCliInt.KeyValue = Null then
  begin
    Application.MessageBox('Informe o cliente interno!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    FCliEnt := Geral.IMV(EdCliInt.Text);
    Result := True;
  end else Result := False;
end;

end.

