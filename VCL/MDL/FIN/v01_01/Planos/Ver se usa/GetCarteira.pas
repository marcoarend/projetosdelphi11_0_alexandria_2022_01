unit GetCarteira;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmGetCarteira = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Label1: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrSelCarteiras: TmySQLQuery;
    DsSelCareteiras: TDataSource;
    QrSelCarteirasCodigo: TIntegerField;
    QrSelCarteirasNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCarteira: Integer;
  end;

  var
  FmGetCarteira: TFmGetCarteira;

implementation

uses UnMyObjects, Module, Principal;

{$R *.DFM}

procedure TFmGetCarteira.BtSaidaClick(Sender: TObject);
begin
  FCarteira := -1000;
  Close;
end;

procedure TFmGetCarteira.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGetCarteira.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmGetCarteira.BtOKClick(Sender: TObject);
begin
  FCarteira := EdCarteira.ValueVariant;
  Close;
end;

procedure TFmGetCarteira.FormCreate(Sender: TObject);
begin
  QrSelCarteiras.Open;
end;

end.
