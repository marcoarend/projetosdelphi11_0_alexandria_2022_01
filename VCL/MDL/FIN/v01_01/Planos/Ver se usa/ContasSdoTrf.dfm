object FmContasSdoTrf: TFmContasSdoTrf
  Left = 375
  Top = 212
  Caption = 'FIN-PLCTA-018 :: Transferencia Entre Contas'
  ClientHeight = 281
  ClientWidth = 453
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 233
    Width = 453
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 341
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 453
    Height = 48
    Align = alTop
    Caption = 'Transfer'#234'ncia entre Contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 373
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 379
      ExplicitHeight = 44
    end
    object LaTipo: TLabel
      Left = 374
      Top = 1
      Width = 78
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 381
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 453
    Height = 185
    Align = alClient
    TabOrder = 0
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 279
      Height = 13
      Caption = 'Entidade (que possui pelo menos uma carteira cadastrada):'
    end
    object Label1: TLabel
      Left = 16
      Top = 52
      Width = 65
      Height = 13
      Caption = 'Conta origem:'
    end
    object Label4: TLabel
      Left = 116
      Top = 140
      Width = 79
      Height = 13
      Caption = 'Valor a transferir:'
    end
    object Label5: TLabel
      Left = 16
      Top = 96
      Width = 68
      Height = 13
      Caption = 'Conta destino:'
    end
    object Label6: TLabel
      Left = 16
      Top = 140
      Width = 90
      Height = 13
      Caption = 'Data transfer'#234'ncia:'
    end
    object EdEntidade: TdmkEditCB
      Left = 16
      Top = 24
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBEntidade
    end
    object CBEntidade: TdmkDBLookupComboBox
      Left = 64
      Top = 24
      Width = 381
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENT'
      ListSource = DsEntidades
      TabOrder = 1
      dmkEditCB = EdEntidade
      UpdType = utYes
    end
    object EdCtaOrig: TdmkEditCB
      Left = 16
      Top = 68
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCtaOrig
    end
    object CBCtaOrig: TdmkDBLookupComboBox
      Left = 64
      Top = 68
      Width = 381
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 3
      dmkEditCB = EdCtaOrig
      UpdType = utYes
    end
    object EdValor: TdmkEdit
      Left = 116
      Top = 156
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object CkContinuar: TCheckBox
      Left = 212
      Top = 156
      Width = 121
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 8
    end
    object EdCtaDest: TdmkEditCB
      Left = 16
      Top = 112
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCtaDest
    end
    object CBCtaDest: TdmkDBLookupComboBox
      Left = 64
      Top = 112
      Width = 381
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 5
      dmkEditCB = EdCtaDest
      UpdType = utYes
    end
    object TPDataT: TDateTimePicker
      Left = 16
      Top = 156
      Width = 97
      Height = 21
      Date = 39441.490882152800000000
      Time = 39441.490882152800000000
      TabOrder = 6
    end
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'WHERE Codigo IN ('
      '  SELECT ForneceI '
      '  FROM carteiras'
      '  WHERE ForneceI <> 0)'
      'ORDER BY NOMEENT')
    Left = 184
    Top = 76
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 212
    Top = 76
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 248
    Top = 76
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 276
    Top = 76
  end
end
