unit ContasAgr;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkEdit, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmContasAgr = class(TForm)
    PainelDados: TPanel;
    DsContasAgr: TDataSource;
    QrContasAgr: TmySQLQuery;
    QrContasAgrLk: TIntegerField;
    QrContasAgrDataCad: TDateField;
    QrContasAgrDataAlt: TDateField;
    QrContasAgrUserCad: TIntegerField;
    QrContasAgrUserAlt: TIntegerField;
    QrContasAgrCodigo: TSmallintField;
    QrContasAgrNome: TWideStringField;
    PainelEdita: TPanel;
    QrContasAgrInfoDescri: TSmallintField;
    QrContasAgrMensal: TSmallintField;
    QrContasAgrNOMEINFODESCRI: TWideStringField;
    QrContasAgrTextoCred: TWideStringField;
    QrContasAgrTextoDebi: TWideStringField;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBEdita: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    RGInfoDescri: TRadioGroup;
    CkMensal: TCheckBox;
    EdTextoCred: TEdit;
    EdTextoDebi: TEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBDados: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrContasAgrAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrContasAgrAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrContasAgrBeforeOpen(DataSet: TDataSet);
    procedure QrContasAgrCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmContasAgr: TFmContasAgr;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmContasAgr.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmContasAgr.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrContasAgrCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmContasAgr.DefParams;
begin
  VAR_GOTOTABELA := 'ContasAgr';
  VAR_GOTOMYSQLTABLE := QrContasAgr;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM contasagr');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmContasAgr.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    EdNome.Text := CO_VAZIO;
    EdNome.Visible := True;
    if SQLType = stIns then
    begin
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
      RGInfoDescri.ItemIndex := 1;
      CkMensal.Checked := True;
    end else begin
      EdCodigo.Text := DBEdCodigo.Text;
      EdNome.Text := DBEdNome.Text;
      RGInfoDescri.ItemIndex := QrContasAgrInfoDescri.Value;
      CkMensal.Checked := Geral.IntToBool_0(QrContasAgrMensal.Value);
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmContasAgr.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmContasAgr.AlteraRegistro;
var
  ContasAgr : Integer;
begin
  ContasAgr := QrContasAgrCodigo.Value;
  if QrContasAgrCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(ContasAgr, Dmod.MyDB, 'ContasAgr', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(ContasAgr, Dmod.MyDB, 'ContasAgr', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmContasAgr.IncluiRegistro;
var
  Cursor : TCursor;
  ContasAgr : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    ContasAgr := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'ContasAgr', 'ContasAgr', 'Codigo');
    if Length(FormatFloat(FFormatFloat, ContasAgr))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro(
      'Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, stIns, ContasAgr);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmContasAgr.QueryPrincipalAfterOpen;
begin
end;

procedure TFmContasAgr.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmContasAgr.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmContasAgr.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmContasAgr.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmContasAgr.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmContasAgr.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmContasAgr.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmContasAgr.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrContasAgrCodigo.Value;
  Close;
end;

procedure TFmContasAgr.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    Exit;
  end;
  Codigo := EdCodigo.ValueVariant;
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO contasagr SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE contasagr SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, InfoDescri=:P1, Mensal=:P2, ');
  Dmod.QrUpdU.SQL.Add('TextoCred=:P3, TextoDebi=:P4, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := RGInfoDescri.ItemIndex;
  Dmod.QrUpdU.Params[02].AsInteger := Geral.BoolToInt(CkMensal.Checked);
  Dmod.QrUpdU.Params[03].AsString  := EdTextoCred.Text;
  Dmod.QrUpdU.Params[04].AsString  := EdTextoDebi.Text;
  //
  Dmod.QrUpdU.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[06].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[07].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ContasAgr', 'Codigo');
  MostraEdicao(False, stLok, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmContasAgr.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ContasAgr', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ContasAgr', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ContasAgr', 'Codigo');
end;

procedure TFmContasAgr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  GBEdita.Align  := alClient;
  GBDados.Align  := alClient;
  CriaOForm;
end;

procedure TFmContasAgr.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrContasAgrCodigo.Value,LaRegistro.Caption);
end;

procedure TFmContasAgr.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmContasAgr.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmContasAgr.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmContasAgr.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmContasAgr.QrContasAgrAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmContasAgr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'ContasAgr', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmContasAgr.QrContasAgrAfterScroll(DataSet: TDataSet);
begin
  //BtAltera.Enabled := GOTOy.BtEnabled(QrContasAgrCodigo.Value, False);
end;

procedure TFmContasAgr.SbQueryClick(Sender: TObject);
begin
  LocCod(QrContasAgrCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ContasAgr', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmContasAgr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasAgr.QrContasAgrBeforeOpen(DataSet: TDataSet);
begin
  QrContasAgrCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmContasAgr.QrContasAgrCalcFields(DataSet: TDataSet);
begin
  QrContasAgrNOMEINFODESCRI.Value :=
    RGInfoDescri.Items[QrContasAgrInfoDescri.Value];
end;

end.

