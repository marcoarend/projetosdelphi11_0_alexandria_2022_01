object FmSubGruposExclui: TFmSubGruposExclui
  Left = 406
  Top = 221
  Caption = 'FIN-PLCTA-042 :: Exclus'#227'o de Subgrupos'
  ClientHeight = 393
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 231
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 143
        Height = 13
        Caption = 'Sub-grupo que ser'#225'  exclu'#237'do:'
      end
      object Label2: TLabel
        Left = 400
        Top = 4
        Width = 306
        Height = 13
        Caption = 
          'Sub-grupo para a qual ser'#227'o transferidos as contas (se existirem' +
          ').'
      end
      object Label3: TLabel
        Left = 16
        Top = 44
        Width = 168
        Height = 13
        Caption = 'Contas do sub-grupo a ser exclu'#237'da'
      end
      object EdSubgrupo: TdmkEditCB
        Left = 12
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdSubgrupoChange
        DBLookupComboBox = CBSubGrupo
        IgnoraDBLookupComboBox = False
      end
      object CBSubGrupo: TdmkDBLookupComboBox
        Left = 80
        Top = 20
        Width = 313
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSubgrupos
        TabOrder = 1
        dmkEditCB = EdSubgrupo
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdNova: TdmkEditCB
        Left = 400
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBNova
        IgnoraDBLookupComboBox = False
      end
      object CBNova: TdmkDBLookupComboBox
        Left = 468
        Top = 20
        Width = 313
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsNova
        TabOrder = 3
        dmkEditCB = EdNova
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object DBGLct: TDBGrid
      Left = 0
      Top = 60
      Width = 792
      Height = 171
      Align = alClient
      DataSource = DsContas
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 317
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 286
        Height = 32
        Caption = 'Exclus'#227'o de Subgrupos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 286
        Height = 32
        Caption = 'Exclus'#227'o de Subgrupos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 286
        Height = 32
        Caption = 'Exclus'#227'o de Subgrupos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 279
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 323
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 644
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 10
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Sair'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 14
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
    end
  end
  object DsSubgrupos: TDataSource
    DataSet = QrSubGrupos
    Left = 320
    Top = 50
  end
  object QrSubGrupos: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrSubGruposAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM subgrupos'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 292
    Top = 50
    object QrSubGruposCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSubGruposNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsNova: TDataSource
    DataSet = QrNova
    Left = 688
    Top = 62
  end
  object QrNova: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM subgrupos'
      'WHERE Codigo>0'
      'AND Codigo<>:P0'
      'ORDER BY Nome')
    Left = 660
    Top = 62
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 12
    Top = 144
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas'
      'WHERE SubGrupo=:P0')
    Left = 40
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 36
    Top = 4
  end
end
