object FmSubGrupos: TFmSubGrupos
  Left = 386
  Top = 245
  Caption = 'FIN-PLCTA-004 :: Cadastro de Subgrupos'
  ClientHeight = 535
  ClientWidth = 917
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 88
    Width = 917
    Height = 447
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitWidth = 916
    object Label9: TLabel
      Left = 20
      Top = 4
      Width = 36
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'C'#243'digo:'
    end
    object Label10: TLabel
      Left = 81
      Top = 4
      Width = 51
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Descri'#231#227'o:'
    end
    object Label5: TLabel
      Left = 551
      Top = 4
      Width = 32
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Grupo:'
    end
    object SBGrupo: TSpeedButton
      Left = 887
      Top = 20
      Width = 21
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SBGrupoClick
    end
    object Label12: TLabel
      Left = 315
      Top = 4
      Width = 99
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Descri'#231#227'o substituta:'
    end
    object EdCodigo: TdmkEdit
      Left = 20
      Top = 20
      Width = 53
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdGrupo: TdmkEditCB
      Left = 551
      Top = 20
      Width = 55
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGrupo
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBGrupo: TdmkDBLookupComboBox
      Left = 610
      Top = 20
      Width = 271
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsGrupos
      TabOrder = 4
      dmkEditCB = EdGrupo
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object GroupBox1: TGroupBox
      Left = 20
      Top = 44
      Width = 400
      Height = 65
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Configura'#231#227'o de Resultados mensais: '
      TabOrder = 5
      object Label4: TLabel
        Left = 16
        Top = 17
        Width = 74
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem na Lista:'
      end
      object EdOrdemLista: TdmkEdit
        Left = 15
        Top = 32
        Width = 95
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object RGTipoAgrupa: TRadioGroup
        Left = 118
        Top = 16
        Width = 277
        Height = 41
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Tipo de Agrupamento: '
        Columns = 2
        Items.Strings = (
          'D'#233'bito'
          'Cr'#233'dito')
        TabOrder = 1
      end
    end
    object CkCtrlaSdo: TCheckBox
      Left = 20
      Top = 109
      Width = 119
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Controla saldo.'
      TabOrder = 6
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 377
      Width = 917
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 7
      ExplicitWidth = 916
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 111
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel7: TPanel
        Left = 782
        Top = 15
        Width = 133
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 781
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object EdNome: TdmkEdit
      Left = 80
      Top = 20
      Width = 232
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnRedefinido = EdNomeRedefinido
    end
    object EdNome2: TdmkEdit
      Left = 316
      Top = 20
      Width = 232
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 88
    Width = 917
    Height = 447
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 916
    object PageControl1: TPageControl
      Left = 20
      Top = 69
      Width = 891
      Height = 266
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Dados'
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 883
          Height = 238
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          Enabled = False
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 20
            Top = 4
            Width = 36
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'C'#243'digo:'
          end
          object Label2: TLabel
            Left = 76
            Top = 4
            Width = 51
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o:'
          end
          object Label3: TLabel
            Left = 612
            Top = 4
            Width = 32
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Grupo:'
          end
          object Label13: TLabel
            Left = 344
            Top = 4
            Width = 99
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o substituta:'
          end
          object DBEdCodigo: TDBEdit
            Left = 20
            Top = 20
            Width = 52
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            BiDiMode = bdLeftToRight
            DataField = 'Codigo'
            DataSource = DsSubGrupos
            ParentBiDiMode = False
            TabOrder = 0
          end
          object DBEdNome: TDBEdit
            Left = 76
            Top = 20
            Width = 265
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            BiDiMode = bdLeftToRight
            DataField = 'Nome'
            DataSource = DsSubGrupos
            ParentBiDiMode = False
            TabOrder = 1
          end
          object GroupBox2: TGroupBox
            Left = 20
            Top = 44
            Width = 400
            Height = 65
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Configura'#231#227'o de Resultados mensais: '
            TabOrder = 2
            object Label6: TLabel
              Left = 16
              Top = 17
              Width = 74
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Ordem na Lista:'
            end
            object EdDBOrdem: TDBEdit
              Left = 15
              Top = 36
              Width = 95
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'OrdemLista'
              DataSource = DsSubGrupos
              TabOrder = 0
            end
            object RadioGroup1: TDBRadioGroup
              Left = 118
              Top = 16
              Width = 277
              Height = 45
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Tipo de Agrupamento: '
              Columns = 2
              DataField = 'TipoAgrupa'
              DataSource = DsSubGrupos
              Items.Strings = (
                'D'#233'bito'
                'Cr'#233'dito')
              TabOrder = 1
              Values.Strings = (
                '0'
                '1'
                '2')
            end
          end
          object DBEdGrupo: TDBEdit
            Left = 612
            Top = 20
            Width = 261
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            BiDiMode = bdLeftToRight
            DataField = 'NOMEGRUPO'
            DataSource = DsSubGrupos
            ParentBiDiMode = False
            TabOrder = 3
          end
          object DBCheckBox1: TDBCheckBox
            Left = 24
            Top = 114
            Width = 119
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Controla saldo.'
            DataField = 'CtrlaSdo'
            DataSource = DsSubGrupos
            TabOrder = 4
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit1: TDBEdit
            Left = 344
            Top = 20
            Width = 265
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            BiDiMode = bdLeftToRight
            DataField = 'Nome2'
            DataSource = DsSubGrupos
            ParentBiDiMode = False
            TabOrder = 5
          end
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Localiza subgrupo'
        ImageIndex = 1
        object Panel13: TPanel
          Left = 0
          Top = 0
          Width = 883
          Height = 60
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label40: TLabel
            Left = 5
            Top = 5
            Width = 51
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o:'
          end
          object EdLocate: TdmkEdit
            Left = 9
            Top = 25
            Width = 381
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            MaxLength = 30
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdLocateChange
            OnKeyDown = EdLocateKeyDown
          end
          object RGOrdem2: TRadioGroup
            Left = 728
            Top = 0
            Width = 155
            Height = 60
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            Caption = ' Ordem 2: '
            ItemIndex = 0
            Items.Strings = (
              'Crescente'
              'Decrescente')
            TabOrder = 1
            OnClick = RGOrdem2Click
          end
          object RGOrdem1: TRadioGroup
            Left = 400
            Top = 0
            Width = 328
            Height = 60
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            Caption = ' Ordem 1: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Descri'#231#227'o'
              'C'#243'digo'
              'Tipo agrupamento'
              'Ordem lista')
            TabOrder = 2
            OnClick = RGOrdem1Click
          end
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 60
          Width = 883
          Height = 178
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsLocate
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Nome'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TipoAgrupa'
              Title.Caption = 'Tipo Agrupam.'
              Width = 77
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrdemLista'
              Title.Caption = 'Ordem Lista'
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Contas deste subgrupo'
        ImageIndex = 2
        object DBGrid2: TDBGrid
          Left = 0
          Top = 60
          Width = 883
          Height = 178
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsContas
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              ReadOnly = True
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrdemLista'
              Title.Caption = 'Ordem lista'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome2'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome3'
              Width = 177
              Visible = True
            end>
        end
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 883
          Height = 60
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label7: TLabel
            Left = 5
            Top = 5
            Width = 36
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'C'#243'digo:'
          end
          object Label8: TLabel
            Left = 133
            Top = 5
            Width = 51
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o:'
          end
          object Label11: TLabel
            Left = 497
            Top = 5
            Width = 32
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Grupo:'
          end
          object EdDBCodigo: TDBEdit
            Left = 5
            Top = 23
            Width = 123
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            BiDiMode = bdLeftToRight
            DataField = 'Codigo'
            DataSource = DsSubGrupos
            ParentBiDiMode = False
            TabOrder = 0
          end
          object EdDBDescricao: TDBEdit
            Left = 133
            Top = 23
            Width = 361
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            BiDiMode = bdLeftToRight
            DataField = 'Nome'
            DataSource = DsSubGrupos
            ParentBiDiMode = False
            TabOrder = 1
          end
          object EdDBGrupo: TDBEdit
            Left = 497
            Top = 23
            Width = 361
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            BiDiMode = bdLeftToRight
            DataField = 'NOMEGRUPO'
            DataSource = DsSubGrupos
            ParentBiDiMode = False
            TabOrder = 2
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 377
      Width = 917
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      ExplicitWidth = 916
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 175
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 130
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 46
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 4
          Top = 5
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 177
        Top = 18
        Width = 123
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
        ExplicitWidth = 122
      end
      object Panel3: TPanel
        Left = 300
        Top = 18
        Width = 615
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        ExplicitLeft = 299
        object BtExclui: TBitBtn
          Tag = 12
          Left = 231
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 118
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 111
          Height = 40
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel6: TPanel
          Left = 481
          Top = 0
          Width = 134
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 40
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtNivelAcima: TBitBtn
          Tag = 352
          Left = 345
          Top = 5
          Width = 123
          Height = 40
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Grupos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          TabOrder = 4
          OnClick = BtNivelAcimaClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 917
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    ExplicitWidth = 916
    object GB_R: TGroupBox
      Left = 865
      Top = 0
      Width = 52
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 864
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 220
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 5
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 220
      Top = 0
      Width = 645
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 644
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 296
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Subgrupos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 296
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Subgrupos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 296
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Subgrupos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 917
    Height = 36
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    ExplicitWidth = 916
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 913
      Height = 19
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 912
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 12
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 12
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsSubGrupos: TDataSource
    DataSet = QrSubGrupos
    Left = 488
    Top = 161
  end
  object QrSubGrupos: TMySQLQuery
    AfterOpen = QrSubGruposAfterOpen
    AfterScroll = QrSubGruposAfterScroll
    SQL.Strings = (
      'SELECT sg.*, gr.Nome NOMEGRUPO'
      'FROM subgrupos sg'
      'LEFT JOIN grupos gr ON gr.Codigo=sg.Grupo'
      'WHERE sg.Codigo > 0')
    Left = 460
    Top = 161
    object QrSubGruposCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'subgrupos.Codigo'
      Required = True
    end
    object QrSubGruposGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'subgrupos.Grupo'
      Required = True
    end
    object QrSubGruposNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'subgrupos.Nome'
      Required = True
      Size = 50
    end
    object QrSubGruposLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'subgrupos.Lk'
    end
    object QrSubGruposDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'subgrupos.DataCad'
    end
    object QrSubGruposDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'subgrupos.DataAlt'
    end
    object QrSubGruposUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'subgrupos.UserCad'
    end
    object QrSubGruposUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'subgrupos.UserAlt'
    end
    object QrSubGruposNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Required = True
      Size = 50
    end
    object QrSubGruposOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'subgrupos.OrdemLista'
      Required = True
    end
    object QrSubGruposTipoAgrupa: TIntegerField
      FieldName = 'TipoAgrupa'
      Origin = 'subgrupos.TipoAgrupa'
      Required = True
    end
    object QrSubGruposCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
      Origin = 'subgrupos.CtrlaSdo'
      Required = True
    end
    object QrSubGruposNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 60
    end
  end
  object QrGrupos: TMySQLQuery
    BeforeOpen = QrSubGruposBeforeOpen
    AfterOpen = QrSubGruposAfterOpen
    AfterScroll = QrSubGruposAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM grupos'
      'ORDER BY Nome')
    Left = 628
    Top = 17
    object QrGruposCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'grupos.Codigo'
    end
    object QrGruposNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'grupos.Nome'
      Size = 50
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 656
    Top = 17
  end
  object QrLocate: TMySQLQuery
    SQL.Strings = (
      'SELECT *'
      'FROM subgrupos'
      'ORDER BY TipoAgrupa, OrdemLista, Nome')
    Left = 240
    Top = 113
    object QrLocateCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'subgrupos.Codigo'
    end
    object QrLocateNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'subgrupos.Nome'
      Size = 50
    end
    object QrLocateGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'subgrupos.Grupo'
    end
    object QrLocateLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'subgrupos.Lk'
    end
    object QrLocateDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'subgrupos.DataCad'
    end
    object QrLocateDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'subgrupos.DataAlt'
    end
    object QrLocateUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'subgrupos.UserCad'
    end
    object QrLocateUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'subgrupos.UserAlt'
    end
    object QrLocateOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'subgrupos.OrdemLista'
    end
    object QrLocateTipoAgrupa: TIntegerField
      FieldName = 'TipoAgrupa'
      Origin = 'subgrupos.TipoAgrupa'
    end
  end
  object DsLocate: TDataSource
    DataSet = QrLocate
    Left = 268
    Top = 113
  end
  object TbContas: TMySQLTable
    Filtered = True
    BeforeInsert = TbContasBeforeInsert
    BeforeDelete = TbContasBeforeDelete
    SortFieldNames = 'OrdemLista,Nome'
    TableName = 'contas'
    Left = 169
    Top = 117
    object TbContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object TbContasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
    object TbContasNome2: TWideStringField
      FieldName = 'Nome2'
      Origin = 'contas.Nome2'
      Size = 50
    end
    object TbContasNome3: TWideStringField
      FieldName = 'Nome3'
      Origin = 'contas.Nome3'
      Size = 50
    end
    object TbContasOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'contas.OrdemLista'
    end
    object TbContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object TbContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
    end
    object TbContasCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object TbContasEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object TbContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object TbContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object TbContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object TbContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object TbContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object TbContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object TbContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object TbContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object TbContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object TbContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object TbContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object TbContasRateio: TIntegerField
      FieldName = 'Rateio'
    end
    object TbContasEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object TbContasAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object TbContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbContasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbContasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbContasPendenMesSeg: TSmallintField
      FieldName = 'PendenMesSeg'
    end
    object TbContasCalculMesSeg: TSmallintField
      FieldName = 'CalculMesSeg'
    end
  end
  object DsContas: TDataSource
    DataSet = TbContas
    Left = 197
    Top = 117
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Panel7
    CanDel01 = BtDesiste
    Left = 260
    Top = 12
  end
end
