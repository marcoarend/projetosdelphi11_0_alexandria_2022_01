/Form duplicado Usar C:\_Compilers\Delphi_XE2\VCL\MDL\FIN\v01_01\Units\UCreateFin.pas

unit UCreateFin;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnMLAGeral,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral,
  UnProjGroup_Vars;

type
  TNomeTabRecriaTempTableFin = (ntrtt_LctBoleto, ntrtt_LctProto, ntrtt_PlaCta, ntrtt_ReceDesp,
    ntrtt_MulInsLct, ntrtt_ParcPagtos,
    ntrtt_FluxCxaDia, ntrtt_FluxCxaDiD, ntrtt_FluxCxaDiS,
    ntrtt_FluxCxaOpn,
    ntrtt_PagRecSem, ntrtt_FlxCxaMes,
    ntrtt_ResMes, ntrtt_CtasResMes, ntrtt_ConsMes, ntrtt_ResPenM, ntrtt_Saldos);
  TAcaoCreateFin = (acDrop, acCreate, acFind);
  TUCreateFin = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrtt_FluxCxaDia(Qry: TmySQLQuery);
    procedure Cria_ntrtt_FluxCxaDiD(Qry: TmySQLQuery);
    procedure Cria_ntrtt_FluxCxaDiS(Qry: TmySQLQuery);
    procedure Cria_ntrtt_FluxCxaOpn(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Lct_Boleto(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Lct_Proto(Qry: TmySQLQuery);
    procedure Cria_ntrtt_ReceDesp(Qry: TmySQLQuery);
    procedure Cria_ntrtt_MulInsLct(Qry: TmySQLQuery);
    procedure Cria_ntrtt_ParcPagtos(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Pla_Cta(Qry: TmySQLQuery);
    //
    procedure Cria_ntrtt_PagRecSem(Qry: TmySQLQuery);
    procedure Cria_ntrtt_FlxCxaMes(Qry: TmySQLQuery);
    //
    procedure Cria_ntrtt_ResMes(Qry: TmySQLQuery);
    procedure Cria_ntrtt_CtasResMes(Qry: TmySQLQuery);
    procedure Cria_ntrtt_ConsMes(Qry: TmySQLQuery);
    procedure Cria_ntrtt_ResPenM(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Saldos(Qry: TmySQLQuery);

  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTableFin;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  UCriarFin: TUCreateFin;

implementation

uses UnMyObjects, Module, ModuleGeral;

procedure TUCreateFin.Cria_ntrtt_ConsMes(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo       int(11)  DEFAULT 0,');
  Qry.SQL.Add('  Nome         varchar(100)  DEFAULT NULL,');
  Qry.SQL.Add('  Mes01        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes02        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes03        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes04        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes05        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes06        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes07        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes08        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes09        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes10        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes11        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes12        double(15,2) NOT NULL DEFAULT "0.00"');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_CtasResMes(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqImp               int(11)                DEFAULT 1           ,');
  Qry.SQL.Add('  Conta                int(11)                DEFAULT 0           ,');
  Qry.SQL.Add('  Nome                 varchar(100)                               ,');
  Qry.SQL.Add('  Periodo              int(11)                DEFAULT 0           ,');
  Qry.SQL.Add('  Tipo                 int(11)                DEFAULT 0           ,');
  Qry.SQL.Add('  Fator                double(15,6)           DEFAULT 0.000000    ,');
  Qry.SQL.Add('  ValFator             double(15,6)           DEFAULT 0.00        ,');
  Qry.SQL.Add('  Devido               double(15,2)           DEFAULT 0.00        ,');
  Qry.SQL.Add('  Pago                 double(15,2)           DEFAULT 0.00        ,');
  Qry.SQL.Add('  Diferenca            double(15,2)           DEFAULT 0.00        ,');
  Qry.SQL.Add('  Acumulado            double(15,2)           DEFAULT 0.00         ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_FluxCxaDia(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Data                 date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  Tipo                 int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  PagRec               tinyint(1)             DEFAULT "0"         ,');
  Qry.SQL.Add('  Carteira             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Sub                  tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SeqPag               int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Genero               int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Descricao            varchar(100)                               ,');
  Qry.SQL.Add('  SerieNF              char(3)                                    ,');
  Qry.SQL.Add('  NotaFiscal           int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Debito               double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Credito              double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Compensado           date                   DEFAULT "0000-00-00",');
  Qry.SQL.Add('  SerieCH              varchar(10)                                ,');
  Qry.SQL.Add('  Documento            double(20,0)           DEFAULT "0"         ,');
  Qry.SQL.Add('  Sit                  tinyint(1)             DEFAULT "0"         ,');
  Qry.SQL.Add('  Vencimento           date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  Pago                 double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Mez                  int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Fornecedor           int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Cliente              int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  CliInt               int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  ForneceI             int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  DataDoc              date                                       ,');
  Qry.SQL.Add('  Duplicata            varchar(13)                                ,');
  Qry.SQL.Add('  Depto                int(11)                                    ,');
  Qry.SQL.Add('  CtrlQuitPg           int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ID_Pgto              int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  ValAPag              double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValARec              double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  VTransf              double(15,2)           DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  ValPgCre             double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValPgDeb             double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValPgTrf             double(15,2)           DEFAULT "0.00"      ,');
  //
  if VAR_FLUX_CXA_DIA > 1 then
  begin
    Qry.SQL.Add('  PagoAntC             double(15,2)         DEFAULT "0.00"      ,');
    Qry.SQL.Add('  PagoAntD             double(15,2)         DEFAULT "0.00"      ,');
    Qry.SQL.Add('  PagoAntQ             int(11)              DEFAULT "0"         ,');
    Qry.SQL.Add('  SitPgIni             tinyint(1) NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  SitPgFim             tinyint(1) NOT NULL  DEFAULT "0"         ,');
    Qry.SQL.Add('  ID_Cart              int(11)      NOT NULL  DEFAULT "0"       ,');
  end;
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PRIMARY KEY (Data,Tipo,Carteira,Controle,Sub,SeqPag)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_FluxCxaDiD(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Data                 date         NOT NULL  DEFAULT "0000-00-00",');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PRIMARY KEY (Data)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_FluxCxaDiS(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Data                 date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  Tipo                 int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  PagRec               tinyint(1)             DEFAULT "0"         ,');
  Qry.SQL.Add('  Carteira             int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Credito              double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Debito               double(15,2)           DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  ValAPagAnt           double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValARecAnt           double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  VTransfAnt           double(15,2)           DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  ValAPagMov           double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValARecMov           double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  VTransfMov           double(15,2)           DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  EfetSdoAnt           double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  EfetMovCre           double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  EfetMovDeb           double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  EfetTrfCre           double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  EfetTrfDeb           double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  EfetSdoAcu           double(15,2)           DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  ValEmiPagC           double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValEmiPagD           double(15,2)           DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  //Qry.SQL.Add('  PRIMARY KEY (Data,Tipo,Carteira)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_FluxCxaOpn(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Data                 date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  Tipo                 int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  PagRec               tinyint(1)             DEFAULT "0"         ,');
  Qry.SQL.Add('  Carteira             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Sub                  tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SeqPag               int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Genero               int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Descricao            varchar(100)                               ,');
  Qry.SQL.Add('  SerieNF              char(3)                                    ,');
  Qry.SQL.Add('  NotaFiscal           int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Debito               double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Credito              double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Compensado           date                   DEFAULT "0000-00-00",');
  Qry.SQL.Add('  SerieCH              varchar(10)                                ,');
  Qry.SQL.Add('  Documento            double(20,0)           DEFAULT "0"         ,');
  Qry.SQL.Add('  Sit                  tinyint(1)             DEFAULT "0"         ,');
  Qry.SQL.Add('  Vencimento           date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  Pago                 double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Mez                  int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Fornecedor           int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Cliente              int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  CliInt               int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  ForneceI             int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  DataDoc              date                                       ,');
  Qry.SQL.Add('  Duplicata            varchar(13)                                ,');
  Qry.SQL.Add('  Depto                int(11)                                    ,');
  Qry.SQL.Add('  CtrlQuitPg           int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ID_Pgto              int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  ValAPag              double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValARec              double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  VTransf              double(15,2)           DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  ValPgCre             double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValPgDeb             double(15,2)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValPgTrf             double(15,2)           DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PRIMARY KEY (Data,Tipo,Carteira,Controle,Sub,SeqPag)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_FlxCxaMes(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Data                 date         NOT NULL                      ,');
  Qry.SQL.Add('  SaldoAnt             double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Receber              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Disponiv             double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Pagar                double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Acumulou             double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PRIMARY KEY (Data)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_Lct_Boleto(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Data                 date         NOT NULL                      ,');
  Qry.SQL.Add('  Tipo                 tinyint(3)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Carteira             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             bigint(20)   NOT NULL DEFAULT "0"          ,'); // j� testar como ser� quando precisar elevar de int para bigint no lct!
  Qry.SQL.Add('  Sub                  tinyint(1)   NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  CliInt               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cliente              int(11)      NOT NULL DEFAULT "0"          ,');
  //Qry.SQL.Add('  NOMECLI              varchar(100)                               ,');
  Qry.SQL.Add('  Emitente             varchar(30)                                ,');
  Qry.SQL.Add('  CNPJCPF              varchar(13)                                ,');
  Qry.SQL.Add('  Duplicata            varchar(13)                                ,');
  Qry.SQL.Add('  SerieCH              varchar(10)                                ,');
  Qry.SQL.Add('  Documento            double(20,0) NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Credito              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  //
  Qry.SQL.Add('  FatID                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  FatNum               double(20,0) NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  FatParcela           int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  Vencimento           date         NOT NULL                      ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PRIMARY KEY (Data,Tipo,Carteira,Controle,Sub)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_Lct_Proto(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Data                 date         NOT NULL                      ,');
  Qry.SQL.Add('  Tipo                 tinyint(3)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Carteira             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             bigint(20)   NOT NULL DEFAULT "0"          ,'); // j� testar como ser� quando precisar elevar de int para bigint no lct!
  Qry.SQL.Add('  Sub                  tinyint(1)   NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  CliInt               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cliente              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Fornecedor           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Documento            double(20,0) NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Credito              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Debito               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  SerieCH              varchar(10)  NOT NULL                      ,');
  Qry.SQL.Add('  Vencimento           date         NOT NULL                      ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PRIMARY KEY (Data,Tipo,Carteira,Controle,Sub)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_MulInsLct(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  GeneroCod            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  GeneroNom            varchar(50)  NOT NULL                      ,');
  Qry.SQL.Add('  Valor                double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Descricao            varchar(100) NOT NULL                      ,');
  Qry.SQL.Add('  Mes                  varchar(7)                                 ,');
  Qry.SQL.Add('  OrdIns               int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PRIMARY KEY (GeneroCod)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_PagRecSem(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Tipo                 tinyint(1)   NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Vencimento           date                                       ,');
  Qry.SQL.Add('  Genero               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  NO_CTA               varchar(50)                                ,');
  //
  Qry.SQL.Add('  Valor01              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Valor02              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Valor03              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Valor04              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Valor05              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Valor06              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Valor07              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Valor08              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "1"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_ParcPagtos(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Parcela              int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  SerieCH              varchar(10)                                ,');
  Qry.SQL.Add('  Doc                  bigint(20)   NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Data                 date                                       ,');
  Qry.SQL.Add('  Credito              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Debito               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Mora                 double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Multa                double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ICMS_V               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Duplicata            varchar(30)                                ,');
  Qry.SQL.Add('  Descricao            varchar(100)                               ,');
  Qry.SQL.Add('  Genero               int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  OrdIns               int(11)      NOT NULL DEFAULT 0            ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  //Qry.SQL.Add('  PRIMARY KEY (Parcela)) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.SQL.Add('  PRIMARY KEY (Parcela, OrdIns)) ');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_Pla_Cta(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Nivel                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  oN5                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cN5                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nN5                  varchar(50)  NOT NULL                      ,');
  Qry.SQL.Add('  oN4                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cN4                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nN4                  varchar(50)  NOT NULL                      ,');
  Qry.SQL.Add('  oN3                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cN3                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nN3                  varchar(50)  NOT NULL                      ,');
  Qry.SQL.Add('  oN2                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cN2                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nN2                  varchar(50)  NOT NULL                      ,');
  Qry.SQL.Add('  oN1                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cN1                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nN1                  varchar(50)  NOT NULL                      ,');
(*
  Qry.SQL.Add('  Idx1                 int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Idx2                 int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Idx3                 int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Idx4                 int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Idx5                 int(11)      NOT NULL DEFAULT "0"          ,');
*)
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ');
{
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PRIMARY KEY (cN5,cN4,cN3,cN2,cN1)');
}
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_ReceDesp(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  oN5                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cN5                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nN5                  varchar(50)  NOT NULL                      ,');
  Qry.SQL.Add('  oN4                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cN4                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nN4                  varchar(50)  NOT NULL                      ,');
  Qry.SQL.Add('  oN3                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cN3                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nN3                  varchar(50)  NOT NULL                      ,');
  Qry.SQL.Add('  oN2                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cN2                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nN2                  varchar(50)  NOT NULL                      ,');
  Qry.SQL.Add('  oN1                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  cN1                  int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  nN1                  varchar(50)  NOT NULL                      ,');
  Qry.SQL.Add('  cO1                  int(11)      NOT NULL DEFAULT "0"          ,'); // C�digo original da conta
  Qry.SQL.Add('  CreDeb               tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Valor                double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Ativ5                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativ4                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativ3                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativ2                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativ1                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_ResMes(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SeqID        int(11) NOT NULL DEFAULT 0,');
  Qry.SQL.Add('  Exclusivo    char(1) NOT NULL DEFAULT "F",');
  Qry.SQL.Add('  TipoAgrupa   int(11) NOT NULL DEFAULT 0,');
  Qry.SQL.Add('  OrdemLista   int(11) NOT NULL DEFAULT 0,');
  Qry.SQL.Add('  SubOrdem     int(11) NOT NULL DEFAULT 0,');
  Qry.SQL.Add('  SubGrupo     int(11) NOT NULL DEFAULT 0,');
  Qry.SQL.Add('  NomeSubGrupo varchar(255) NOT NULL DEFAULT "?",');
  Qry.SQL.Add('  Genero       int(11) NOT NULL DEFAULT 0,');
  Qry.SQL.Add('  NomeConta    varchar(255) NOT NULL DEFAULT "?",');
  Qry.SQL.Add('  AnoAn        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes01        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes02        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes03        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes04        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes05        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes06        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes07        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes08        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes09        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes10        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes11        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes12        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Pendencias   double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  AnoAt        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  LimChrtLin   int(11) NOT NULL DEFAULT 0,');
  Qry.SQL.Add('  Grupo        int(11) NOT NULL DEFAULT 0,');
  Qry.SQL.Add('  NomeGrupo    varchar(255) NOT NULL DEFAULT "?",');
  //
  Qry.SQL.Add('  Ativo        int(11) NOT NULL DEFAULT 0,');
  Qry.SQL.Add('  PRIMARY KEY (SeqID)');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_ResPenM(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Grupo        int(11) NOT NULL DEFAULT 0,');
  Qry.SQL.Add('  Genero       int(11) NOT NULL DEFAULT 0,');
  Qry.SQL.Add('  NomeConta    varchar(255) NOT NULL DEFAULT "?",');
  Qry.SQL.Add('  AnoAn        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes01        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes02        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes03        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes04        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes05        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes06        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes07        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes08        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes09        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes10        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes11        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Mes12        double(15,2) NOT NULL DEFAULT "0.00",');
  Qry.SQL.Add('  Pendencias   double(15,2) NOT NULL DEFAULT "0.00"');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TUCreateFin.Cria_ntrtt_Saldos(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  CartCodi             int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  CartTipo             int(11)      NOT NULL DEFAULT -1           ,');
  Qry.SQL.Add('  CartNiv2             int(11)      NOT NULL DEFAULT 0            ,');
  Qry.SQL.Add('  Nome                 varchar(50)  NOT NULL DEFAULT "???"        ,');
  Qry.SQL.Add('  Saldo                double(15,2) NOT NULL DEFAULT 0.00         ,');
  Qry.SQL.Add('  Tipo                 int(11)      NOT NULL DEFAULT 0             ');
  Qry.SQL.Add('  ) CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

function TUCreateFin.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTableFin; Qry: TmySQLQuery;
UniqueTableName: Boolean; Repeticoes: Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrtt_FluxCxaDia:     Nome := Lowercase('_FluxCxaDia_');
      ntrtt_FluxCxaDiD:     Nome := Lowercase('_FluxCxaDiD_');
      ntrtt_FluxCxaDiS:     Nome := Lowercase('_FluxCxaDiS_');
      ntrtt_FluxCxaOpn:     Nome := Lowercase('_FluxCxaOpn_');
      ntrtt_LctBoleto:      Nome := Lowercase('_Lct_Boleto_');
      ntrtt_LctProto:       Nome := Lowercase('_Lct_Proto_');
      ntrtt_PlaCta:         Nome := Lowercase('_Pla_Cta_');
      ntrtt_ReceDesp:       Nome := Lowercase('_Rece_Desp_');
      ntrtt_MulInsLct:      Nome := Lowercase('_MulInsLct_');
      ntrtt_ParcPagtos:     Nome := Lowercase('_ParcPagtos_');
      //
      ntrtt_PagRecSem:      Nome := Lowercase('_PagRecSem_');
      ntrtt_FlxCxaMes:      Nome := Lowercase('_FlxCxaMes_');
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
      // 2015-01-02
      // Marcelo, mudar aqui colocando UNDERLINE antes edepois do nome da
      // tabela e testar em todos os aplicativos!!!
      ntrtt_ResMes:         Nome := Lowercase('ResMes');     // '_ResMes_'
      ntrtt_CtasResMes:     Nome := Lowercase('CtasResMes'); // '_CtasResMes_'
      ntrtt_ConsMes:        Nome := Lowercase('ConsMes');    // '_ConsMes_'
      ntrtt_ResPenM:        Nome := Lowercase('ResPenM');    // '_ResPenM_'
      ntrtt_Saldos:         Nome := Lowercase('Saldos');     // '_Saldos_'
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
      // FIM 2015-01-02
      //
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MensagemBox(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)',
    'Erro', MB_OK+MB_ICONERROR);
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrtt_FluxCxaDia:   Cria_ntrtt_FluxCxaDia(Qry);
    ntrtt_FluxCxaDiD:   Cria_ntrtt_FluxCxaDiD(Qry);
    ntrtt_FluxCxaDiS:   Cria_ntrtt_FluxCxaDiS(Qry);
    ntrtt_FluxCxaOpn:   Cria_ntrtt_FluxCxaOpn(Qry);
    ntrtt_LctBoleto:    Cria_ntrtt_Lct_Boleto(Qry);
    ntrtt_LctProto:     Cria_ntrtt_Lct_Proto(Qry);
    ntrtt_PlaCta:       Cria_ntrtt_Pla_Cta(Qry);
    ntrtt_ReceDesp:     Cria_ntrtt_ReceDesp(Qry);
    ntrtt_MulInsLct:    Cria_ntrtt_MulInsLct(Qry);
    ntrtt_ParcPagtos:   Cria_ntrtt_ParcPagtos(Qry);
    //
    ntrtt_PagRecSem:    Cria_ntrtt_PagRecSem(Qry);
    ntrtt_FlxCxaMes:    Cria_ntrtt_FlxCxaMes(Qry);
    //
    ntrtt_ResMes:       Cria_ntrtt_ResMes(Qry);
    ntrtt_CtasResMes:   Cria_ntrtt_CtasResMes(Qry);
    ntrtt_ConsMes:      Cria_ntrtt_ConsMes(Qry);
    ntrtt_ResPenM:      Cria_ntrtt_ResPenM(Qry);
    ntrtt_Saldos:       Cria_ntrtt_Saldos(Qry);
    //
    else Geral.MB_Erro('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!');
  end;
  Result := TabNo;
end;

end.

