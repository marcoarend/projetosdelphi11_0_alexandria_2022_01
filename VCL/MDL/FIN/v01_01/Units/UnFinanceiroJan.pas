unit UnFinanceiroJan;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, DBGrids,
  UnInternalConsts2, ComCtrls, dmkEdit, dmkDBLookupComboBox, dmkGeral,
  dmkEditCB, mySQLDBTables, UnDmkEnums, DmkDAC_PF, UnGrl_Geral;

type
  TUnFinanceiroJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    // J A N E L A S - F I N A N C E I R O
    procedure InsereEDefineCarteira(Codigo: Integer; EdCarteira: TdmkEditCB;
              CBCarteira: TdmkDBLookupComboBox; QrCarteiras: TmySQLQuery);
    procedure InsereEDefineConta(Codigo: Integer; EdConta: TdmkEditCB;
              CBConta: TdmkDBLookupComboBox; QrContas: TmySQLQuery);
    //
    procedure MostraFinancas(InOwner: TWincontrol;
              Pager: TWinControl; Collaps: Boolean = True;
              Unique: Boolean = True);
    procedure SaldoDeContas(Empresa: Integer);
    procedure ImpressaoDoPlanoDeContas();
    procedure CadastroDePlano(Codigo: Integer);
    procedure CadastroDeConjutos(Codigo: Integer);
    procedure CadastroDeGrupos(Codigo: Integer);
    procedure CadastroDeSubGrupos(Codigo: Integer);
    procedure CadastroDeContas(Codigo: Integer);
    procedure CadastroDeNiveisPlano();
    procedure CadastroContasAgr();
    procedure CadastroContasLnk();
    procedure CadastroDeCarteiras(Codigo: Integer);
    procedure CadastroIndiPag();
    procedure MostraPlanRelCab;
    procedure MostraLctEncerraMes;
    procedure MostraRelChAbertos(Empresa: Integer);
    procedure MostraSaldos(Empresa: Integer);
    procedure ExtratosFinanceirosEmpresaUnica();
    procedure ExtratosFinanceirosEmpresaUnica2();
    procedure ExtratosFinanceirosEmpresaUnica3();
    procedure ExtratosFinanceirosEmpresaUnica3_CLiente(Cliente: Integer);
    procedure ExtratosFinanceirosMultiplasEmpresas();
    procedure MostraFormContas(Codigo: Integer);
    procedure MostraMovimento(CliInt: Integer);
    procedure MostraResultadosMensais;
    procedure MostraPesquisaPorNveldoPLanodeContas(Empresa: Integer);
    procedure MostraPesquisaPorNveldoPLanodeContas2(Empresa: Integer);
    procedure MostraPesquisaContasControladas(Empresa: Integer);
    procedure MostraContasSazonais;
    procedure MostraMovimentoPlanodeContas(CliInt: Integer);
    procedure MostraCashBal(CliInt: Integer);
    procedure MostraReceDesp(Empresa: Integer);
    procedure MostraReceDesp2(Empresa: Integer);
    procedure MostraCashPreCta(Empresa: Integer);
    procedure CriaImpressaoDiversos(Empresa, Indice: Integer);
    procedure MostraEventosCad();
    procedure CadastroDeContasNiv();
    procedure MostraRestricaoDeContas();
    procedure DemonstrativoDeReceitasEDespesasFree(Empresa: Integer);
    procedure CadastroDeCartNiv2();
    procedure MostraPlanoImpExp();
    procedure CadastroESelecaoDeCarteira(QrCarteiras: TmySQLQuery; EdCarteira:
              TdmkEditCB; CBCarteira: TdmkDBLookupCombobox);
    procedure MostraLctDelLanctoz();
    procedure MostraCopiaDoc(QueryLct: TMySQLQuery; GradeLct: TDBGrid; TabLct: String);
    procedure ImprimeCopiaDC(Controle, Genero, EntCliInt: Integer; TabLct: String);
    procedure ImprimeCopiaVC(QrLct: TmySQLQuery; GradeLct: TDBGrid;
              EntCliInt: Integer; TabLct: String);
    procedure ImprimeCopiaCH(Controle, Genero, EntCliInt: Integer; TabLct: String);
    procedure MostraFluxoCxa(EntCliInt, CliInt: Integer);
    procedure MostraFormReciboImpCab(Codigo: Integer);
    procedure MostraFormCtbCadGru(Codigo: Integer);
    procedure MostraFormCtbCadMoF(Codigo: Integer);
    //procedure MostraFormLctAtrelaFat(TabLct: String; FFatID, FFatNum, FFatParcela: Integer);
    {$IfDef TEM_UH}
    procedure MostraPrevBaCLctos(Form: String);
    procedure MostraImportSal1(TabLctA: String; CliInt, Carteira: Integer);
    procedure MostraImportSal2(TabLctA: String; CliInt, Carteira: Integer);
    {$EndIf}
    // O U T R O S
    //procedure CorrigeLancamentos(); Ver como fazer!
    function  LancamentosComProblemas(TabLctA: String): Boolean;
  end;

var
  FinanceiroJan: TUnFinanceiroJan;

implementation

uses
  {$IfNDef NAO_CMEB} ContasLnk, {$EndIf}
  {$IfDef TEM_UH}PrevBaCLctos, ImportSal1, ImportSal2, {$EndIf}
  MyDBCheck, ModuleLct2, Principal, ModuleGeral, UnFinanceiro, RelChAbertos,
  ContasHistSdo3, ContasSdoImp, Plano, Conjuntos, Grupos, SubGrupos, Contas,
  PlaCtas, ContasAgr, Carteiras, IndiPag, PlanRelCab, LctEncerraMes, CopiaDoc,
  Saldos, Extratos, APagRec, PrincipalImp, Resmes, Pesquisas, PesqContaCtrl,
  CtaGruImp, MovFin, CashBal, ReceDesp, CashPreCta, ReceDesp2, Formularios,
  EventosCad, Extratos2, Extratos3, ContasNiv, CtaCfgCab, Pesquisas2,
  {$IfDef cAdvGrid} //Berlin
  ReceDespFree,
  {$Else}
  ReceDespFree2,
  {$EndIf}
  UnDmkProcFunc, CartNiv2, UMySQLModule, PlanoImpExp, LctDelLanctoz,
  {$IfDef UsaContabil}
    CtbCadGru, CtbCadMoF,
  {$EndIf}
  FinanceiroImp, FluxoCxa, LctAjustes, ReciboImpCab
  (*,LctAtrelaFat*);

function TUnFinanceiroJan.LancamentosComProblemas(TabLctA: String): Boolean;
var
  Erros: Integer;
begin
  Result := True;
  if DBCheck.CriaFm(TFmLctAjustes, FmLctAjustes, afmoLiberado) then
  begin
    FmLctAjustes.DefineTabLctA(TabLctA);
    //
    FmLctAjustes.ShowModal;
    //
    Erros := FmLctAjustes.ErrosEncontrados;
    //
    FmLctAjustes.Destroy;
    //
    Result := Erros > 0;
    //
    if Result then
      Geral.MB_Aviso('Foram encontrados ' + Geral.FF0(Erros) +
        ' que devem ser corrigidos!');
  end;
end;

procedure TUnFinanceiroJan.CadastroIndiPag;
begin
  if DBCheck.CriaFm(TFmIndiPag, FmIndiPag, afmoNegarComAviso) then
  begin
    FmIndiPag.ShowModal;
    FmIndiPag.Destroy;
  end;
end;

procedure TUnFinanceiroJan.CadastroContasAgr;
begin
  if DBCheck.CriaFm(TFmContasAgr, FmContasAgr, afmoNegarComAviso) then
  begin
    FmContasAgr.ShowModal;
    FmContasAgr.Destroy;
  end;
end;

procedure TUnFinanceiroJan.CadastroContasLnk;
begin
  {$IfNDef NAO_CMEB}
  if DBCheck.CriaFm(TFmContasLnk, FmContasLnk, afmoNegarComAviso) then
  begin
    FmContasLnk.ShowModal;
    FmContasLnk.Destroy;
  end;
  {$Else}
  Grl_Geral.InfoSemModulo(mdlappConciBco);
  {$EndIf}
end;

procedure TUnFinanceiroJan.CadastroDeCarteiras(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCarteiras.LocCod(Codigo, Codigo);
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
  end;
end;

procedure TUnFinanceiroJan.CadastroDeCartNiv2;
begin
  if DBCheck.CriaFm(TFmCartNiv2, FmCartNiv2, afmoNegarComAviso) then
  begin
    FmCartNiv2.ShowModal;
    FmCartNiv2.Destroy;
  end;
end;

procedure TUnFinanceiroJan.CadastroDeConjutos(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmConjuntos, FmConjuntos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmConjuntos.LocCod(Codigo, Codigo);
    FmConjuntos.ShowModal;
    FmConjuntos.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraFormContas(Codigo: Integer);
begin
  CadastroDeContas(Codigo);
end;

procedure TUnFinanceiroJan.CadastroDeContas(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmContas.LocCod(Codigo, Codigo);
    FmContas.ShowModal;
    FmContas.Destroy;
  end;
end;

procedure TUnFinanceiroJan.CadastroDeContasNiv;
begin
  if DBCheck.CriaFm(TFmContasNiv, FmContasNiv, afmoNegarComAviso) then
  begin
    FmContasNiv.ShowModal;
    FmContasNiv.Destroy;
  end;
end;

procedure TUnFinanceiroJan.CadastroDeGrupos(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGrupos, FmGrupos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGrupos.LocCod(Codigo, Codigo);
    FmGrupos.ShowModal;
    FmGrupos.Destroy;
  end;
end;

procedure TUnFinanceiroJan.CadastroDeNiveisPlano;
begin
  if DBCheck.CriaFm(TFmPlaCtas, FmPlaCtas, afmoNegarComAviso) then
  begin
    FmPlaCtas.ShowModal;
    FmPlaCtas.Destroy;
  end;
end;

procedure TUnFinanceiroJan.CadastroDePlano(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPlano, FmPlano, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPlano.LocCod(Codigo, Codigo);
    FmPlano.ShowModal;
    FmPlano.Destroy;
  end;
end;

procedure TUnFinanceiroJan.CadastroDeSubGrupos(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmSubGrupos, FmSubGrupos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmSubGrupos.LocCod(Codigo, Codigo);
    FmSubGrupos.ShowModal;
    FmSubGrupos.Destroy;
  end;
end;

procedure TUnFinanceiroJan.CadastroESelecaoDeCarteira(QrCarteiras: TmySQLQuery;
  EdCarteira: TdmkEditCB; CBCarteira: TdmkDBLookupCombobox);
var
  Carteira: Integer;
begin
  VAR_CADASTRO := 0;
  Carteira := EdCarteira.ValueVariant;
  //
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    if Carteira <> 0 then
      FmCarteiras.LocCod(Carteira, Carteira);
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      if QrCarteiras <> nil then
        UnDmkDAC_PF.AbreQuery(QrCarteiras, QrCarteiras.Database);
      //
      EdCarteira.ValueVariant := VAR_CADASTRO;
      CBCarteira.KeyValue     := VAR_CADASTRO;
      EdCarteira.SetFocus;
    end;
  end;
end;

(* Ver como fazer!
procedure TUnFinanceiroJan.CorrigeLancamentos();
var
  CliInt: Integer;
  TabLctA: String;
begin
  if DModG.SelecionaEmpresa(sllForcaA, True) then
  begin
    CliInt   := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
    TabLctA  := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
    UFinanceiro.LancamentosComProblemas(TabLctA);
  end;
end;
*)

procedure TUnFinanceiroJan.CriaImpressaoDiversos(Empresa, Indice: Integer);
begin
  if DBCheck.CriaFm(TFmFormularios, FmFormularios, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmFormularios.EdEmpresa.ValueVariant := Empresa;
      FmFormularios.CBEmpresa.KeyValue     := Empresa;
    end;
    FmFormularios.RGRelatorio.ItemIndex := Indice;
    FmFormularios.ShowModal;
    FmFormularios.Destroy;
  end;
end;

procedure TUnFinanceiroJan.DemonstrativoDeReceitasEDespesasFree(
  Empresa: Integer);
begin
{$IfDef cAdvGrid} //Berlin
  if DBCheck.CriaFm(TFmReceDespFree, FmReceDespFree, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmReceDespFree.EdEmpresa.ValueVariant := Empresa;
      FmReceDespFree.CBEmpresa.KeyValue     := Empresa;
    end;
    FmReceDespFree.ShowModal;
    FmReceDespFree.Destroy;
  end;
{$Else}
  if DBCheck.CriaFm(TFmReceDespFree2, FmReceDespFree2, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmReceDespFree2.EdEmpresa.ValueVariant := Empresa;
      FmReceDespFree2.CBEmpresa.KeyValue     := Empresa;
    end;
    FmReceDespFree2.ShowModal;
    FmReceDespFree2.Destroy;
  end;
{$EndIf}
end;

procedure TUnFinanceiroJan.ExtratosFinanceirosEmpresaUnica();
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmExtratos, FmExtratos, afmoNegarComAviso) then
  begin
    FmExtratos.ShowModal;
    FmExtratos.Destroy;
  end;
end;

procedure TUnFinanceiroJan.ExtratosFinanceirosEmpresaUnica2;
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmExtratos2, FmExtratos2, afmoNegarComAviso) then
  begin
    FmExtratos2.ShowModal;
    FmExtratos2.Destroy;
  end;
end;

procedure TUnFinanceiroJan.ExtratosFinanceirosEmpresaUnica3_CLiente(
  Cliente: Integer);
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmExtratos3, FmExtratos3, afmoNegarComAviso) then
  begin
    FmExtratos3.RGTipo.ItemIndex        := 3; // Contas a receber
    FmExtratos3.CkEmissao.Checked       := False;
    FmExtratos3.CkVencto.Checked        := False;
    FmExtratos3.CkDataDoc.Checked       := False;
    FmExtratos3.EdTerceiro.ValueVariant := Cliente;
    FmExtratos3.CBTerceiro.KeyValue     := Cliente;
    FmExtratos3.CkOrdem1.Checked        := True;
    FmExtratos3.CkOrdem1.Checked        := False;
    FmExtratos3.RGOrdem1.ItemIndex      := 0;
    FmExtratos3.RGOrdem2.ItemIndex      := 1;
    //
    FmExtratos3.ShowModal;
    FmExtratos3.Destroy;
  end;
end;

procedure TUnFinanceiroJan.ExtratosFinanceirosEmpresaUnica3;
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmExtratos3, FmExtratos3, afmoNegarComAviso) then
  begin
    FmExtratos3.ShowModal;
    FmExtratos3.Destroy;
  end;
end;

procedure TUnFinanceiroJan.ExtratosFinanceirosMultiplasEmpresas();
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmAPagRec, FmAPagRec, afmoNegarComAviso) then
  begin
    FmAPagRec.ShowModal;
    FmAPagRec.Destroy;
  end;
end;

procedure TUnFinanceiroJan.ImpressaoDoPlanoDeContas();
begin
  if DBCheck.CriaFm(TFmContasSdoImp, FmContasSdoImp, afmoNegarComAviso) then
  begin
    FmContasSdoImp.ShowModal;
    FmContasSdoImp.Destroy;
  end;
end;

procedure TUnFinanceiroJan.ImprimeCopiaCH(Controle, Genero, EntCliInt: Integer;
  TabLct: String);
begin
  Application.CreateForm(TFmFinanceiroImp, FmFinanceiroImp);
  FmFinanceiroImp.ImprimeCopiaCH(Controle, Genero, EntCliInt, TabLct);
  FmFinanceiroImp.Destroy;
end;

procedure TUnFinanceiroJan.ImprimeCopiaDC(Controle, Genero, EntCliInt: Integer;
  TabLct: String);
begin
  Application.CreateForm(TFmFinanceiroImp, FmFinanceiroImp);
  FmFinanceiroImp.ImprimeCopiaDC(Controle, Genero, EntCliInt, TabLct);
  FmFinanceiroImp.Destroy;
end;

procedure TUnFinanceiroJan.ImprimeCopiaVC(QrLct: TmySQLQuery; GradeLct: TDBGrid;
  EntCliInt: Integer; TabLct: String);
begin
  Application.CreateForm(TFmFinanceiroImp, FmFinanceiroImp);
  FmFinanceiroImp.ImprimeCopiaVC(QrLct, GradeLct, EntCliInt, TabLct);
  FmFinanceiroImp.Destroy;
end;

procedure TUnFinanceiroJan.InsereEDefineCarteira(Codigo: Integer;
  EdCarteira: TdmkEditCB; CBCarteira: TdmkDBLookupComboBox;
  QrCarteiras: TmySQLQuery);
begin
  VAR_CADASTRO := 0;
  CadastroDeCarteiras(EdCarteira.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdCarteira, CBCarteira, QrCarteiras,
    VAR_CADASTRO, 'Codigo');
  EdCarteira.SetFocus;
end;

procedure TUnFinanceiroJan.InsereEDefineConta(Codigo: Integer;
  EdConta: TdmkEditCB; CBConta: TdmkDBLookupComboBox; QrContas: TmySQLQuery);
begin
  VAR_CADASTRO := 0;
  CadastroDeContas(EdConta.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdConta, CBConta, QrContas,
    VAR_CADASTRO, 'Codigo');
  EdConta.SetFocus;
end;

procedure TUnFinanceiroJan.MostraCashBal(CliInt: Integer);
begin
  if DBCheck.CriaFm(TFmCashBal, FmCashBal, afmoNegarComAviso) then
  begin
    if CliInt <> 0 then
    begin
      FmCashBal.EdEmpresa.ValueVariant := CliInt;
      FmCashBal.CBEmpresa.KeyValue     := CliInt;
    end;
    FmCashBal.ShowModal;
    FmCashBal.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraCashPreCta(Empresa: Integer);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if DBCheck.CriaFm(TFmCashPreCta, FmCashPreCta, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmCashPreCta.EdEmpresa.ValueVariant := Empresa;
      FmCashPreCta.CBEmpresa.KeyValue     := Empresa;
    end;
    FmCashPreCta.ShowModal;
    FmCashPreCta.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraContasSazonais;
begin
  if DBCheck.CriaFm(TFmCtaGruImp, FmCtaGruImp, afmoNegarComAviso) then
  begin
    FmCtaGruImp.ShowModal;
    FmCtaGruImp.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraCopiaDoc(QueryLct: TMySQLQuery;
  GradeLct: TDBGrid; TabLct: String);
begin
  if DBCheck.CriaFm(TFmCopiaDoc, FmCopiaDoc, afmoNegarComAviso) then
  begin
    FmCopiaDoc.FDBG    := GradeLct;
    FmCopiaDoc.FQrLct  := QueryLct;
    FmCopiaDoc.FTabLct := TabLct;
    FmCopiaDoc.ShowModal;
    FmCopiaDoc.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraEventosCad;
begin
  if DBCheck.CriaFm(TFmEventosCad, FmEventosCad, afmoNegarComAviso) then
  begin
    FmEventosCad.ShowModal;
    FmEventosCad.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraFinancas(InOwner: TWincontrol;
  Pager: TWinControl; Collaps: Boolean = True;
  Unique: Boolean = True);
var
  DmLctX: TDataModule;
begin
  if UnDmkDAC_PF.CriaDataModule(FmPrincipal, TDmLct2, DmLctX) then
    TDmLct2(DmLctX).GerenciaEmpresa(InOwner, Pager, Collaps, Unique);
end;

procedure TUnFinanceiroJan.MostraFluxoCxa(EntCliInt, CliInt: Integer);
begin
  if DBCheck.CriaFm(TFmFluxoCxa, FmFluxoCxa, afmoNegarComAviso) then
  begin
    FmFluxoCxa.FEntidade     := EntCliInt;
    FmFluxoCxa.FEntidade_TXT := Geral.FF0(EntCliInt);
    FmFluxoCxa.FEmpresa      := CliInt;
    //
    DModG.Def_EM_ABD(TMeuDB, FmFluxoCxa.FEntidade, FmFluxoCxa.FEmpresa,
      FmFluxoCxa.FDtEncer, FmFluxoCxa.FDtMorto,
      FmFluxoCxa.FTabLctA, FmFluxoCxa.FTabLctB, FmFluxoCxa.FTabLctD);
    //
    FmFluxoCxa.ShowModal;
    FmFluxoCxa.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraFormCtbCadGru(Codigo: Integer);
begin
  {$IfDef UsaContabil}
    if DBCheck.CriaFm(TFmCtbCadGru, FmCtbCadGru, afmoNegarComAviso) then
    begin
      FmCtbCadGru.LocCod(Codigo, Codigo);
      //
      FmCtbCadGru.ShowModal;
      FmCtbCadGru.Destroy;
    end;
  {$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappContabil);
  {$EndIf}
end;

procedure TUnFinanceiroJan.MostraFormCtbCadMoF(Codigo: Integer);
begin
  {$IfDef UsaContabil}
    if DBCheck.CriaFm(TFmCtbCadMoF, FmCtbCadMoF, afmoNegarComAviso) then
    begin
      FmCtbCadMoF.LocCod(Codigo, Codigo);
      //
      FmCtbCadMoF.ShowModal;
      FmCtbCadMoF.Destroy;
    end;
  {$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappContabil);
  {$EndIf}
end;

(*
procedure TUnFinanceiroJan.MostraFormLctAtrelaFat(TabLct: String; FFatID,
  FFatNum, FFatParcela: Integer);
begin
  if DBCheck.CriaFm(TFmLctAtrelaFat, FmLctAtrelaFat, afmoNegarComAviso) then
  begin
    FmLctAtrelaFat.FTabLct     := TabLct;
    FmLctAtrelaFat.FFatID      := FFatID;
    FmLctAtrelaFat.FFatNum     := FFatNum;
    FmLctAtrelaFat.FFatParcela := FFatParcela;
    //
    FmLctAtrelaFat.ShowModal;
    FmLctAtrelaFat.Destroy;
  end;
end;
*)

procedure TUnFinanceiroJan.MostraFormReciboImpCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmReciboImpCab, FmReciboImpCab, afmoNegarComAviso) then
  begin
    FmReciboImpCab.FCodigo     := Codigo;
    FmReciboImpCab.LocCod(Codigo, Codigo);
    //
    FmReciboImpCab.ShowModal;
    FmReciboImpCab.Destroy;
  end;
end;

{$IfDef TEM_UH}
procedure TUnFinanceiroJan.MostraImportSal1(TabLctA: String; CliInt,
  Carteira: Integer);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if DBCheck.CriaFm(TFmImportSal1, FmImportSal1, afmoNegarComAviso) then
  begin
    FmImportSal1.FTabLctA                := TabLctA;
    FmImportSal1.EdEmpresa.ValueVariant  := CliInt;
    FmImportSal1.CBEmpresa.KeyValue      := CliInt;
    FmImportSal1.EdCarteira.ValueVariant := Carteira;
    FmImportSal1.CBCarteira.KeyValue     := Carteira;
    FmImportSal1.ShowModal;
    FmImportSal1.Destroy;
  end;
end;
{$EndIf}

{$IfDef TEM_UH}
procedure TUnFinanceiroJan.MostraImportSal2(TabLctA: String; CliInt,
  Carteira: Integer);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if DBCheck.CriaFm(TFmImportSal2, FmImportSal2, afmoNegarComAviso) then
  begin
    FmImportSal2.FTabLctA                := TabLctA;
    FmImportSal2.EdEmpresa.ValueVariant  := CliInt;
    FmImportSal2.CBEmpresa.KeyValue      := CliInt;
    FmImportSal2.EdCarteira.ValueVariant := Carteira;
    FmImportSal2.CBCarteira.KeyValue     := Carteira;
    FmImportSal2.ShowModal;
    FmImportSal2.Destroy;
  end;
end;
{$EndIf}

procedure TUnFinanceiroJan.MostraLctDelLanctoz;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  if DBCheck.CriaFm(TFmLctDelLanctoz, FmLctDelLanctoz, afmoSoBoss) then
  begin
    FmLctDelLanctoz.ShowModal;
    FmLctDelLanctoz.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraLctEncerraMes;
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if DBCheck.CriaFm(TFmLctEncerraMes, FmLctEncerraMes, afmoNegarComAviso) then
  begin
    FmLctEncerraMes.ShowModal;
    FmLctEncerraMes.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraMovimento(CliInt: Integer);
begin
  if DBCheck.CriaFm(TFmPrincipalImp, FmPrincipalImp, afmoNegarComAviso) then
  begin
    if CliInt <> 0 then
    begin
      FmPrincipalImp.EdEmpresa.ValueVariant := CliInt;
      FmPrincipalImp.CBEmpresa.KeyValue     := CliInt;
    end;
    FmPrincipalImp.ShowModal;
    FmPrincipalImp.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraMovimentoPlanodeContas(CliInt: Integer);
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmMovFin, FmMovFin, afmoNegarComAviso) then
  begin
    if CliInt <> 0 then
    begin
      FmMovFin.EdEmpresa.ValueVariant := CliInt;
      FmMovFin.CBEmpresa.KeyValue     := CliInt;
    end;
    FmMovFin.ShowModal;
    FmMovFin.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraPesquisaContasControladas(Empresa: Integer);
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmPesqContaCtrl, FmPesqContaCtrl, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmPesqContaCtrl.EdEmpresa.ValueVariant := Empresa;
      FmPesqContaCtrl.CBEmpresa.KeyValue     := Empresa;
    end;
    FmPesqContaCtrl.ShowModal;
    FmPesqContaCtrl.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraPesquisaPorNveldoPLanodeContas(Empresa: Integer);
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmPesquisas, FmPesquisas, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmPesquisas.EdEmpresa.ValueVariant := Empresa;
      FmPesquisas.CBEmpresa.KeyValue     := Empresa;
    end;
    FmPesquisas.ShowModal;
    FmPesquisas.FCliente_Txt := VAR_FIN_SELFG_000_CLI + ':';
    FmPesquisas.FFornece_Txt := VAR_FIN_SELFG_000_FRN + ':';
    FmPesquisas.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraPesquisaPorNveldoPLanodeContas2(Empresa: Integer);
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmPesquisas2, FmPesquisas2, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmPesquisas2.EdEmpresa.ValueVariant := Empresa;
      FmPesquisas2.CBEmpresa.KeyValue     := Empresa;
    end;
    FmPesquisas2.ShowModal;
    FmPesquisas2.FCliente_Txt := VAR_FIN_SELFG_000_CLI + ':';
    FmPesquisas2.FFornece_Txt := VAR_FIN_SELFG_000_FRN + ':';
    FmPesquisas2.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraPlanoImpExp;
begin
  if DBCheck.CriaFm(TFmPlanoImpExp, FmPlanoImpExp, afmoNegarComAviso) then
  begin
    FmPlanoImpExp.ShowModal;
    FmPlanoImpExp.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraPlanRelCab;
begin
  if DBCheck.CriaFm(TFmPlanRelCab, FmPlanRelCab, afmoNegarComAviso) then
  begin
    FmPlanRelCab.ShowModal;
    FmPlanRelCab.Destroy;
  end;
end;

{$IfDef TEM_UH}
procedure TUnFinanceiroJan.MostraPrevBaCLctos(Form: String);
begin
  if DBCheck.CriaFm(TFmPrevBaCLctos, FmPrevBaCLctos, afmoNegarComAviso) then
  begin
    FmPrevBaCLctos.FForm := Form;
    FmPrevBaCLctos.ShowModal;
    FmPrevBaCLctos.Destroy;
  end;
end;
{$EndIf}

procedure TUnFinanceiroJan.MostraReceDesp(Empresa: Integer);
begin
  if DBCheck.CriaFm(TFmReceDesp, FmReceDesp, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmReceDesp.EdEmpresa.ValueVariant := Empresa;
      FmReceDesp.CBEmpresa.KeyValue     := Empresa;
    end;
    FmReceDesp.ShowModal;
    FmReceDesp.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraReceDesp2(Empresa: Integer);
begin
  if DBCheck.CriaFm(TFmReceDesp2, FmReceDesp2, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmReceDesp2.EdEmpresa.ValueVariant := Empresa;
      FmReceDesp2.CBEmpresa.KeyValue     := Empresa;
    end;
    FmReceDesp2.ShowModal;
    FmReceDesp2.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraRelChAbertos(Empresa: Integer);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if DBCheck.CriaFm(TFmRelChAbertos, FmRelChAbertos, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmRelChAbertos.EdEmpresa.ValueVariant := Empresa;
      FmRelChAbertos.CBEmpresa.KeyValue     := Empresa;
    end;
    FmRelChAbertos.ShowModal;
    FmRelChAbertos.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraRestricaoDeContas;
begin
  if DBCheck.CriaFm(TFmCtaCfgCab, FmCtaCfgCab, afmoNegarComAviso) then
  begin
    FmCtaCfgCab.ShowModal;
    FmCtaCfgCab.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraResultadosMensais;
var
  Entidade, CliInt: Integer;
begin
  //N�o separa por cliente interno
  Geral.MB_Aviso('N�o implementado!');
  Exit;
  //
(* Ver como fazer o SelecionaEmpresa(...
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DmodG.SelecionaEmpresa(sllNenhuma) then
  begin
    Entidade := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
    CliInt   := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
    //
    if DBCheck.CriaFm(TFmResMes, FmResMes, afmoNegarComAviso) then
    begin
      DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, FmResMes.FDtEncer, FmResMes.FDtMorto,
        FmResMes.FTabLctA, FmResMes.FTabLctB, FmResMes.FTabLctD);
      FmResMes.ShowModal;
      FmResMes.Destroy;
    end;
  end;
*)
end;

procedure TUnFinanceiroJan.MostraSaldos(Empresa: Integer);
begin
  if DBCheck.CriaFm(TFmSaldos, FmSaldos, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmSaldos.EdEmpresa.ValueVariant := Empresa;
      FmSaldos.CBEmpresa.KeyValue     := Empresa;
    end;
    FmSaldos.ShowModal;
    FmSaldos.Destroy;
  end;
end;

procedure TUnFinanceiroJan.SaldoDeContas(Empresa: Integer);
begin
  if DBCheck.CriaFm(TFmContasHistSdo3, FmContasHistSdo3, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      FmContasHistSdo3.EdCliInt.ValueVariant := Empresa;
      FmContasHistSdo3.CBCliInt.KeyValue     := Empresa;
    end;
    FmContasHistSdo3.ShowModal;
    FmContasHistSdo3.Destroy;
  end;
end;

end.
