unit PlanoCtas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkCompoStore, Vcl.Menus, Vcl.ImgList;

type
  TFmPlanoCtas = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    Panel8: TPanel;
    ImageList1: TImageList;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtVoltar: TBitBtn;
    TVItens: TTreeView;
    SbImprime: TBitBtn;
    PMMenu: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    N1: TMenuItem;
    Voltar1: TMenuItem;
    LVItens: TListView;
    BtMover: TBitBtn;
    N2: TMenuItem;
    Mover1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LVItensDblClick(Sender: TObject);
    procedure BtVoltarClick(Sender: TObject);
    procedure TVItensClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure Voltar1Click(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure Mover1Click(Sender: TObject);
    procedure BtMoverClick(Sender: TObject);
  private
    { Private declarations }
    FNivel, FNivel1_Id, FNivel2_Id, FNivel3_Id, FNivel4_Id: Integer;
    FNivel1_Str, FNivel2_Str, FNivel3_Str, FNivel4_Str: String;
    procedure MostraEdicao(Nivel, Codigo: Integer; Nome: String);
    procedure Voltar();
    procedure InsUpd(SQLType: TSQLType);
    procedure Exclui();
    procedure Mover();
  public
    { Public declarations }
  end;

  var
  FmPlanoCtas: TFmPlanoCtas;

implementation

uses MyGlyfs, Principal, UnMyObjects, Module, UnFinanceiro2Cad, UnFinanceiro2Jan,
  UnFinanceiroJan;

{$R *.DFM}

procedure TFmPlanoCtas.Altera1Click(Sender: TObject);
begin
  InsUpd(stUpd);
end;

procedure TFmPlanoCtas.BtAlteraClick(Sender: TObject);
begin
  InsUpd(stUpd);
end;

procedure TFmPlanoCtas.BtExcluiClick(Sender: TObject);
begin
  Exclui();
end;

procedure TFmPlanoCtas.BtIncluiClick(Sender: TObject);
begin
  InsUpd(stIns);
end;

procedure TFmPlanoCtas.BtMoverClick(Sender: TObject);
begin
  Mover();
end;

procedure TFmPlanoCtas.BtSaidaClick(Sender: TObject);
begin
  if TFmPlanoCtas(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmPlanoCtas.BtVoltarClick(Sender: TObject);
begin
  Voltar();
end;

procedure TFmPlanoCtas.Exclui;
var
  Codigo: Integer;
  Item: TListItem;
  Plano: TPlano;
  Conjunto: TConjunto;
  Grupo: TGrupo;
  SubGrupo: TSubGrupo;
  Conta: TConta;
begin
  Item := LVItens.Selected;
  //
  if Item <> nil then
    Codigo := Item.StateIndex
  else
    Codigo := 0;
  //
  if Codigo = 0 then
  begin
    Geral.MB_Aviso('Nenhum item foi selecionado!');
    Exit;
  end;
  //
  case FNivel of
    1: //Plano
    begin
      Plano := TPlano.Create(Dmod.MyDB);
      try
        Plano.Exclui(Codigo);
      finally
        Plano.Free;
      end;
    end;
    2: //Conjunto
    begin
      Conjunto := TConjunto.Create(Dmod.MyDB);
      try
        Conjunto.Exclui(Codigo);
      finally
        Conjunto.Free;
      end;
    end;
    3: //Grupo
    begin
      Grupo := TGrupo.Create(Dmod.MyDB);
      try
        Grupo.Exclui(Codigo);
      finally
        Grupo.Free;
      end;
    end;
    4: //Subgrupo
    begin
      SubGrupo := TSubGrupo.Create(Dmod.MyDB);
      try
        SubGrupo.Exclui(Codigo);
      finally
        SubGrupo.Free;
      end;
    end;
    5: //Conta
    begin
      Conta := TConta.Create(Dmod.MyDB);
      try
        Conta.Exclui(Codigo);
      finally
        Conta.Free;
      end;
    end;
  end;
end;

procedure TFmPlanoCtas.Exclui1Click(Sender: TObject);
begin
  Exclui();
end;

procedure TFmPlanoCtas.FormActivate(Sender: TObject);
begin
  if TFmPlanoCtas(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmPlanoCtas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MostraEdicao(1, 0, '');
end;

procedure TFmPlanoCtas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPlanoCtas.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmPlanoCtas.InsUpd(SQLType: TSQLType);
var
  Codigo: Integer;
  Item: TListItem;
begin
  if SQLType = stUpd then
  begin
    Item := LVItens.Selected;
    //
    if Item = nil then
    begin
      Geral.MB_Aviso('Nenhum item foi selecionado!');
      exit;
    end else
      Codigo := Item.StateIndex;
  end else
    Codigo := 0;
  //
  case FNivel of
    1: //Plano
    begin
      Financeiro2Jan.MostraFormCadastroDePlano(Codigo);
      MostraEdicao(1, 0, '');
    end;
    2: //Conjunto
    begin
      Financeiro2Jan.MostraFormCadastroDeConjunto(Codigo, FNivel1_Id);
      MostraEdicao(2, FNivel1_Id, FNivel1_Str);
    end;
    3: //Grupo
    begin
      Financeiro2Jan.MostraFormCadastroDeGrupo(Codigo, FNivel2_Id);
      MostraEdicao(3, FNivel2_Id, FNivel2_Str);
    end;
    4: //Subgrupo
    begin
      Financeiro2Jan.MostraFormCadastroDeSubGrupo(Codigo, FNivel3_Id);
      MostraEdicao(4, FNivel3_Id, FNivel3_Str);
    end;
    5: //Conta
    begin
      Financeiro2Jan.MostraFormCadastroDeConta(Codigo, FNivel4_Id);
      MostraEdicao(5, FNivel4_Id, FNivel4_Str);
    end;
  end;
end;

procedure TFmPlanoCtas.Inclui1Click(Sender: TObject);
begin
  InsUpd(stIns);
end;

procedure TFmPlanoCtas.LVItensDblClick(Sender: TObject);
var
  Item: TListItem;
begin
  Item := LVItens.Selected;
  //
  if (Item <> nil) and (FNivel < 5) then
    MostraEdicao(FNivel + 1, Item.StateIndex, Item.Caption);
end;

procedure TFmPlanoCtas.MostraEdicao(Nivel, Codigo: Integer; Nome: String);

  procedure ConfiguraTreeView(Niv1, Niv2, Niv3, Niv4: Integer;
    Niv1_Str, Niv2_Str, Niv3_Str, Niv4_Str: String);
  var
    Node: TTreeNode;
  begin
    if (Niv1 <> 0) and (Niv1_Str <> '') then
    begin
      Node := TVItens.Items.Add(nil, Niv1_Str);
      Node.SelectedIndex := Niv1;
      Node.StateIndex    := 2;
    end;
    //
    if (Niv2 <> 0) and (Niv2_Str <> '') and (Node <> nil) then
    begin
      Node := TVItens.Items.AddChild(Node, Niv2_Str);
      Node.SelectedIndex := Niv2;
      Node.StateIndex    := 3;
    end;
    //
    if (Niv3 <> 0) and (Niv3_Str <> '') and (Node <> nil) then
    begin
      Node := TVItens.Items.AddChild(Node, Niv3_Str);
      Node.SelectedIndex := Niv3;
      Node.StateIndex    := 4;
    end;
    //
    if (Niv4 <> 0) and (Niv4_Str <> '') and (Node <> nil) then
    begin
      Node := TVItens.Items.AddChild(Node, Niv4_Str);
      Node.SelectedIndex := Niv4;
      Node.StateIndex    := 5;
    end;
    //
    if TVItens.Items.Count > 0 then
      TVItens.FullExpand;
  end;

var
  Plano: TPlano;
  Conjunto: TConjunto;
  Grupo: TGrupo;
  Subgrupo: TSubgrupo;
  Conta: TConta;
  I: Integer;
  Item: TListItem;
begin
  LVItens.PopupMenu   := PMMenu;
  LVItens.ViewStyle   := vsIcon;
  LVItens.LargeImages := ImageList1;
  //
  TVItens.RightClickSelect := True;
  //
  LVItens.Items.Clear;
  TVItens.Items.Clear;
  //
  if Nivel = 1 then //Plano
  begin
    Plano := TPlano.Create(Dmod.MyDB);
    try
      Plano.ReabreQuery;
      //
      if Length(Plano.Itens) > 0 then
      begin
        for I := Low(Plano.Itens) to High(Plano.Itens) do
        begin
          Item := LVItens.Items.Add;
          //
          Item.StateIndex := Plano.Itens[I].Codigo;
          Item.Caption    := Plano.Itens[I].Nome;
          Item.ImageIndex := 0;
          //
          if I = 0 then
          begin
            ConfiguraTreeView(
              Plano.Itens[I].Niv1_Int,
              Plano.Itens[I].Niv2_Int,
              Plano.Itens[I].Niv3_Int,
              Plano.Itens[I].Niv4_Int,
              Plano.Itens[I].Niv1_Str,
              Plano.Itens[I].Niv2_Str,
              Plano.Itens[I].Niv3_Str,
              Plano.Itens[I].Niv4_Str);
            //
            FNivel      := Plano.Itens[I].Niv_Atual;
            FNivel1_Id  := Plano.Itens[I].Niv1_Int;
            FNivel1_Str := Plano.Itens[I].Niv1_Str;
            FNivel2_Id  := Plano.Itens[I].Niv2_Int;
            FNivel2_Str := Plano.Itens[I].Niv2_Str;
            FNivel3_Id  := Plano.Itens[I].Niv3_Int;
            FNivel3_Str := Plano.Itens[I].Niv3_Str;
            FNivel4_Id  := Plano.Itens[I].Niv4_Int;
            FNivel4_Str := Plano.Itens[I].Niv4_Str;
          end;
        end;
      end else
        FNivel := Nivel;
    finally
      Plano.Free;
    end;
  end else
  if Nivel = 2 then //Conjunto
  begin
    Conjunto := TConjunto.Create(Dmod.MyDB);
    try
      if Codigo = 0 then
        Codigo := FNivel1_Id;
      //
      Conjunto.ReabreQuery(Codigo);
      //
      if Length(Conjunto.Itens) > 0 then
      begin
        for I := Low(Conjunto.Itens) to High(Conjunto.Itens) do
        begin
          Item := LVItens.Items.Add;
          //
          Item.StateIndex := Conjunto.Itens[I].Codigo;
          Item.Caption    := Conjunto.Itens[I].Nome;
          Item.ImageIndex := 0;
          //
          if I = 0 then
          begin
            ConfiguraTreeView(
              Conjunto.Itens[I].Niv1_Int,
              Conjunto.Itens[I].Niv2_Int,
              Conjunto.Itens[I].Niv3_Int,
              Conjunto.Itens[I].Niv4_Int,
              Conjunto.Itens[I].Niv1_Str,
              Conjunto.Itens[I].Niv2_Str,
              Conjunto.Itens[I].Niv3_Str,
              Conjunto.Itens[I].Niv4_Str);
            //
            FNivel      := Conjunto.Itens[I].Niv_Atual;
            FNivel1_Id  := Conjunto.Itens[I].Niv1_Int;
            FNivel1_Str := Conjunto.Itens[I].Niv1_Str;
            FNivel2_Id  := Conjunto.Itens[I].Niv2_Int;
            FNivel2_Str := Conjunto.Itens[I].Niv2_Str;
            FNivel3_Id  := Conjunto.Itens[I].Niv3_Int;
            FNivel3_Str := Conjunto.Itens[I].Niv3_Str;
            FNivel4_Id  := Conjunto.Itens[I].Niv4_Int;
            FNivel4_Str := Conjunto.Itens[I].Niv4_Str;
          end;
        end;
      end else
      begin
        FNivel      := Nivel;
        FNivel1_Id  := Codigo;
        FNivel1_Str := Nome;
        //
        ConfiguraTreeView(FNivel1_Id, 0, 0, 0, FNivel1_Str, '', '', '');
      end;
    finally
      Conjunto.Free;
    end;
  end else
  if Nivel = 3 then //Grupo
  begin
    Grupo := TGrupo.Create(Dmod.MyDB);
    try
      if Codigo = 0 then
        Codigo := FNivel2_Id;
      //
      Grupo.ReabreQuery(Codigo);
      //
      if Length(Grupo.Itens) > 0 then
      begin
        for I := Low(Grupo.Itens) to High(Grupo.Itens) do
        begin
          Item := LVItens.Items.Add;
          //
          Item.StateIndex := Grupo.Itens[I].Codigo;
          Item.Caption    := Grupo.Itens[I].Nome;
          Item.ImageIndex := 0;
          //
          if I = 0 then
          begin
            ConfiguraTreeView(
              Grupo.Itens[I].Niv1_Int,
              Grupo.Itens[I].Niv2_Int,
              Grupo.Itens[I].Niv3_Int,
              Grupo.Itens[I].Niv4_Int,
              Grupo.Itens[I].Niv1_Str,
              Grupo.Itens[I].Niv2_Str,
              Grupo.Itens[I].Niv3_Str,
              Grupo.Itens[I].Niv4_Str);
            //
            FNivel      := Grupo.Itens[I].Niv_Atual;
            FNivel1_Id  := Grupo.Itens[I].Niv1_Int;
            FNivel1_Str := Grupo.Itens[I].Niv1_Str;
            FNivel2_Id  := Grupo.Itens[I].Niv2_Int;
            FNivel2_Str := Grupo.Itens[I].Niv2_Str;
            FNivel3_Id  := Grupo.Itens[I].Niv3_Int;
            FNivel3_Str := Grupo.Itens[I].Niv3_Str;
            FNivel4_Id  := Grupo.Itens[I].Niv4_Int;
            FNivel4_Str := Grupo.Itens[I].Niv4_Str;
          end;
        end;
      end else
      begin
        FNivel      := Nivel;
        FNivel2_Id  := Codigo;
        FNivel2_Str := Nome;
        //
        ConfiguraTreeView(FNivel1_Id, FNivel2_Id, 0, 0, FNivel1_Str,
          FNivel2_Str, '', '');
      end;
    finally
      Grupo.Free;
    end;
  end else
  if Nivel = 4 then //Subgrupo
  begin
    Subgrupo := TSubGrupo.Create(Dmod.MyDB);
    try
      if Codigo = 0 then
        Codigo := FNivel3_Id;
      //
      Subgrupo.ReabreQuery(Codigo);
      //
      if Length(Subgrupo.Itens) > 0 then
      begin
        for I := Low(Subgrupo.Itens) to High(Subgrupo.Itens) do
        begin
          Item := LVItens.Items.Add;
          //
          Item.StateIndex := Subgrupo.Itens[I].Codigo;
          Item.Caption    := Subgrupo.Itens[I].Nome;
          Item.ImageIndex := 0;
          //
          if I = 0 then
          begin
            ConfiguraTreeView(
              Subgrupo.Itens[I].Niv1_Int,
              Subgrupo.Itens[I].Niv2_Int,
              Subgrupo.Itens[I].Niv3_Int,
              Subgrupo.Itens[I].Niv4_Int,
              Subgrupo.Itens[I].Niv1_Str,
              Subgrupo.Itens[I].Niv2_Str,
              Subgrupo.Itens[I].Niv3_Str,
              Subgrupo.Itens[I].Niv4_Str);
            //
            FNivel      := Subgrupo.Itens[I].Niv_Atual;
            FNivel1_Id  := Subgrupo.Itens[I].Niv1_Int;
            FNivel1_Str := Subgrupo.Itens[I].Niv1_Str;
            FNivel2_Id  := Subgrupo.Itens[I].Niv2_Int;
            FNivel2_Str := Subgrupo.Itens[I].Niv2_Str;
            FNivel3_Id  := Subgrupo.Itens[I].Niv3_Int;
            FNivel3_Str := Subgrupo.Itens[I].Niv3_Str;
            FNivel4_Id  := Subgrupo.Itens[I].Niv4_Int;
            FNivel4_Str := Subgrupo.Itens[I].Niv4_Str;
          end;
        end;
      end else
      begin
        FNivel      := Nivel;
        FNivel3_Id  := Codigo;
        FNivel3_Str := Nome;
        //
        ConfiguraTreeView(FNivel1_Id, FNivel2_Id, FNivel3_Id, 0, FNivel1_Str,
          FNivel2_Str, FNivel3_Str, '');
      end;
    finally
      Subgrupo.Free;
    end;
  end else
  if Nivel = 5 then //Conta
  begin
    Conta := TConta.Create(Dmod.MyDB);
    try
      if Codigo = 0 then
        Codigo := FNivel4_Id;
      //
      Conta.ReabreQuery(Codigo);
      //
      if Length(Conta.Itens) > 0 then
      begin
        for I := Low(Conta.Itens) to High(Conta.Itens) do
        begin
          Item := LVItens.Items.Add;
          //
          Item.StateIndex := Conta.Itens[I].Codigo;
          Item.Caption    := Conta.Itens[I].Nome;
          Item.ImageIndex := 1;
          //
          if I = 0 then
          begin
            ConfiguraTreeView(
              Conta.Itens[I].Niv1_Int,
              Conta.Itens[I].Niv2_Int,
              Conta.Itens[I].Niv3_Int,
              Conta.Itens[I].Niv4_Int,
              Conta.Itens[I].Niv1_Str,
              Conta.Itens[I].Niv2_Str,
              Conta.Itens[I].Niv3_Str,
              Conta.Itens[I].Niv4_Str);
            //
            FNivel     := Conta.Itens[I].Niv_Atual;
            FNivel1_Id := Conta.Itens[I].Niv1_Int;
            FNivel2_Id := Conta.Itens[I].Niv2_Int;
            FNivel3_Id := Conta.Itens[I].Niv3_Int;
            FNivel4_Id := Conta.Itens[I].Niv4_Int;
          end;
        end;
      end else
      begin
        FNivel      := Nivel;
        FNivel4_Id  := Codigo;
        FNivel4_Str := Nome;
        //
        ConfiguraTreeView(FNivel1_Id, FNivel2_Id, FNivel3_Id, FNivel4_Id,
          FNivel1_Str, FNivel2_Str, FNivel3_Str, FNivel4_Str);
      end;
    finally
      Conta.Free;
    end;
  end;
end;

procedure TFmPlanoCtas.Mover();
var
  Codigo, CodGrupo: Integer;
  Nome: String;
  Item: TListItem;
  Conjunto: TConjunto;
  Grupo: TGrupo;
  SubGrupo: TSubGrupo;
  Conta: TConta;
begin
  CodGrupo := 0;
  Item     := LVItens.Selected;
  //
  if Item <> nil then
  begin
    Codigo := Item.StateIndex;
    Nome   := Item.Caption;
  end else
    Codigo := 0;
  //
  if Codigo = 0 then
  begin
    Geral.MB_Aviso('Nenhum item foi selecionado!');
    Exit;
  end;
  //
  if FNivel = 1 then //Plano
  begin
    Geral.MB_Aviso('Este item n�o pode ser movido!' + sLineBreak +
      'Voc� pode mover apenas itens contidos dentro deste item!');
    Exit;
  end;
  //
  case FNivel of
    2: //Conjunto
    begin
      Conjunto := TConjunto.Create(Dmod.MyDB);
      try
        CodGrupo := Conjunto.Mover(Codigo);
      finally
        Conjunto.Free;
      end;
      MostraEdicao(2, FNivel1_Id, FNivel1_Str);
    end;
    3: //Grupo
    begin
      Grupo := TGrupo.Create(Dmod.MyDB);
      try
        CodGrupo := Grupo.Mover(Codigo);
      finally
        Grupo.Free;
      end;
      MostraEdicao(3, FNivel2_Id, FNivel2_Str);
    end;
    4: //Subgrupo
    begin
      SubGrupo := TSubGrupo.Create(Dmod.MyDB);
      try
        CodGrupo := SubGrupo.Mover(Codigo);
      finally
        SubGrupo.Free;
      end;
      MostraEdicao(4, FNivel3_Id, FNivel3_Str);
    end;
    5: //Conta
    begin
      Conta := TConta.Create(Dmod.MyDB);
      try
        CodGrupo := Conta.Mover(Codigo);
      finally
        Conta.Free;
      end;
      MostraEdicao(5, FNivel4_Id, FNivel4_Str);
    end;
  end;
end;

procedure TFmPlanoCtas.Mover1Click(Sender: TObject);
begin
  Mover();
end;

procedure TFmPlanoCtas.PMMenuPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := LVItens.Selected <> nil;
  Enab2 := FNivel > 1;
  //
  Altera1.Enabled := Enab;
  Exclui1.Enabled := Enab;
  Mover1.Enabled  := Enab;
  //
  Voltar1.Enabled := Enab2;
end;

procedure TFmPlanoCtas.SbImprimeClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas;
end;

procedure TFmPlanoCtas.TVItensClick(Sender: TObject);
var
  Node: TTreeNode;
begin
  Node := TVItens.Selected;
  //
  if (Node <> nil) and (Node.StateIndex < 5) then
    MostraEdicao(Node.StateIndex, Node.SelectedIndex, Node.Text);
end;

procedure TFmPlanoCtas.Voltar;
begin
  if FNivel > 1 then
    MostraEdicao(FNivel - 1, 0, '');
end;

procedure TFmPlanoCtas.Voltar1Click(Sender: TObject);
begin
  Voltar();
end;

end.
