unit CentroCusto2;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, UnFinanceiro2Cad;

type
  TFmCentroCusto2 = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCentroCusto: TmySQLQuery;
    DsCentroCusto: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrCentroCustoNome: TWideStringField;
    QrCentroCustoCodigo: TIntegerField;
    CkAtivo: TCheckBox;
    DBCheckBox1: TDBCheckBox;
    QrCentroCustoAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCentroCustoAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCentroCustoBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FCentroCustos: TCentroCustos;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(SQLType: TSQLType; Codigo: Integer = 0);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCentroCusto2: TFmCentroCusto2;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCentroCusto2.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCentroCusto2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCentroCustoCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCentroCusto2.DefParams;
begin
  VAR_GOTOTABELA := 'centrocusto';
  VAR_GOTOMYSQLTABLE := QrCentroCusto;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM centrocusto');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCentroCusto2.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCentroCusto2.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCentroCusto2.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmCentroCusto2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCentroCusto2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCentroCusto2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCentroCusto2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCentroCusto2.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCentroCusto2.BtAlteraClick(Sender: TObject);
begin
  if (QrCentroCusto.State <> dsInactive) and (QrCentroCusto.RecordCount > 0) then
    MostraEdicao(stUpd, QrCentroCustoCodigo.Value);
end;

procedure TFmCentroCusto2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCentroCustoCodigo.Value;
  Close;
end;

procedure TFmCentroCusto2.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
  Ativo: Boolean;
begin
  Codigo := EdCodigo.ValueVariant;
  Nome   := EdNome.ValueVariant;
  Ativo  := CkAtivo.Checked;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  Codigo := FCentroCustos.InsUpd(Nome, Ativo, Codigo);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    MostraEdicao(stLok);
  end;
end;

procedure TFmCentroCusto2.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(stLok);
end;

procedure TFmCentroCusto2.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(stIns);
end;

procedure TFmCentroCusto2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBDados.Align   := alClient;
  CriaOForm;
  //
  FCentroCustos := TCentroCustos.Create(Dmod.MyDB);
end;

procedure TFmCentroCusto2.FormDestroy(Sender: TObject);
begin
  FCentroCustos.Free;
end;

procedure TFmCentroCusto2.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCentroCustoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCentroCusto2.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCentroCusto2.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCentroCustoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCentroCusto2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCentroCusto2.QrCentroCustoAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCentroCusto2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCentroCusto2.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCentroCustoCodigo.Value, CuringaLoc.CriaForm(CO_CODIGO, CO_NOME,
    'centrocusto', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCentroCusto2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCentroCusto2.QrCentroCustoBeforeOpen(DataSet: TDataSet);
begin
  QrCentroCustoCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCentroCusto2.MostraEdicao(SQLType: TSQLType; Codigo: Integer = 0);
begin
  if (SQLType = stIns) or (SQLType = stUpd) then
  begin
    PnEdita.Visible := True;
    PnDados.Visible := False;
    //
    FCentroCustos.MostraEdicao(Codigo);
    //
    EdCodigo.ValueVariant := FCentroCustos.Codigo;
    EdNome.ValueVariant   := FCentroCustos.Nome;
    CkAtivo.Checked       := FCentroCustos.Ativo;
    //
    EdNome.SetFocus;
  end else
  begin
    PnDados.Visible := True;
    PnEdita.Visible := False;
  end;
  //
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

end.

