unit PlanoCtas_Conjunto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkCompoStore, dmkDBLookupComboBox, dmkEditCB, UnFinanceiro2Cad;

type
  TFmPlanoCtas_Conjunto = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    CkAtivo: TCheckBox;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FConjunto: TConjunto;
    procedure MostraEdicao();
  public
    { Public declarations }
    FCodigo, FPlano: Integer;
  end;

  var
  FmPlanoCtas_Conjunto: TFmPlanoCtas_Conjunto;

implementation

uses MyGlyfs, Principal, Module, UnMyObjects;

{$R *.DFM}

procedure TFmPlanoCtas_Conjunto.BtOKClick(Sender: TObject);
var
  Codigo, Plano: Integer;
  Nome: String;
  Ativo: Boolean;
begin
  Codigo := EdCodigo.ValueVariant;
  Nome   := EdNome.ValueVariant;
  Plano  := FPlano;
  Ativo  := CkAtivo.Checked;
  //
  VAR_CADASTRO := FConjunto.InsUpd(Nome, Ativo, Plano, Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    if (CkContinuar.Visible = True) and (CkContinuar.Checked = True) then
      MostraEdicao()
    else
      Close;
  end;
end;

procedure TFmPlanoCtas_Conjunto.BtSaidaClick(Sender: TObject);
begin
  if TFmPlanoCtas_Conjunto(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmPlanoCtas_Conjunto.FormActivate(Sender: TObject);
begin
  if TFmPlanoCtas_Conjunto(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmPlanoCtas_Conjunto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType     := stLok;
  FCodigo             := 0;
  VAR_CADASTRO        := 0;
  CkContinuar.Checked := False;
  //
  FConjunto := TConjunto.Create(Dmod.MyDB);
end;

procedure TFmPlanoCtas_Conjunto.FormDestroy(Sender: TObject);
begin
  FConjunto.Free;
end;

procedure TFmPlanoCtas_Conjunto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPlanoCtas_Conjunto.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  MostraEdicao();
end;

procedure TFmPlanoCtas_Conjunto.MostraEdicao;
begin
  FConjunto.MostraEdicao(FCodigo, FPlano);
  //
  if FCodigo <> 0 then
  begin
    ImgTipo.SQLType     := stUpd;
    CkContinuar.Checked := False;
    CkContinuar.Visible := False;
  end else
  begin
    ImgTipo.SQLType     := stIns;
    CkContinuar.Visible := True;
  end;
  //
  EdCodigo.ValueVariant := FConjunto.Codigo;
  EdNome.ValueVariant   := FConjunto.Nome;
  FPlano                := FConjunto.Plano;
  CkAtivo.Checked       := FConjunto.Ativo;
  //
  EdNome.SetFocus;
end;

end.
