unit FinOpcoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkValUsu, DB, mySQLDbTables,
  dmkGeral, ComCtrls, dmkRadioGroup, dmkImage, dmkPermissoes, UnDmkEnums,
  dmkCheckBox, UnDmkProcFunc, UnFinanceiro2Cad;

type
  TFmFinOpcoes = class(TForm)
    QrPlano: TmySQLQuery;
    QrPlanoCodigo: TIntegerField;
    DsPlano: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Label98: TLabel;
    EdPlanoPadrao: TdmkEditCB;
    CBPlanoPadrao: TdmkDBLookupComboBox;
    SBPlanoPadrao: TSpeedButton;
    QrPlanoNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBPlanoPadraoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FOpcoesFinanceiras: TOpcoesFinanceiras;
    procedure MostraEdicao();
  public
    { Public declarations }
  end;

  var
  FmFinOpcoes: TFmFinOpcoes;

implementation

uses
  UMySQLModule, Module, ModuleGeral, UnInternalConsts, MyDBCheck, UnMyObjects,
  DmkDAC_PF, UnFinanceiro2Jan;

{$R *.DFM}

procedure TFmFinOpcoes.BtOKClick(Sender: TObject);
var
  Empresa, PlanoPadrao: Integer;
begin
  Empresa     := DmodG.QrFiliLogCodigo.Value;
  PlanoPadrao := EdPlanoPadrao.ValueVariant;
  //
  FOpcoesFinanceiras.InsUpd(Empresa, PlanoPadrao);
  //
  Close;
end;

procedure TFmFinOpcoes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFinOpcoes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmFinOpcoes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  //
  UMyMod.AbreQuery(QrPlano, DMod.MyDB);
  //
  FOpcoesFinanceiras := TOpcoesFinanceiras.Create(Dmod.MyDB);
end;

procedure TFmFinOpcoes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFinOpcoes.FormShow(Sender: TObject);
begin
  MostraEdicao();
end;

procedure TFmFinOpcoes.MostraEdicao;
var
  Empresa: Integer;
begin
  Empresa := DmodG.QrFiliLogCodigo.Value;
  //
  FOpcoesFinanceiras.MostraEdicao(Empresa);
  //
  EdPlanoPadrao.ValueVariant := FOpcoesFinanceiras.PlanoPadrao;
  CBPlanoPadrao.KeyValue     := FOpcoesFinanceiras.PlanoPadrao;
  EdPlanoPadrao.SetFocus;
end;

procedure TFmFinOpcoes.SBPlanoPadraoClick(Sender: TObject);
begin
  Financeiro2Jan.MostraFormCadastroPlanoCtas();
end;

end.
