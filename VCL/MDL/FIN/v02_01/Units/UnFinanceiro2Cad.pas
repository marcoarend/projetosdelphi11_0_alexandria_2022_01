unit UnFinanceiro2Cad;

interface

uses System.Classes, System.Variants, Data.DB, UnDmkEnums, mySQLDbTables,
  dmkEditCB, dmkDBLookupComboBox;

type
  TFinanceiroItem = record
    Codigo: Integer;
    Nome: String;
    Niv_Atual: Integer;
    Niv1_Int: Integer;
    Niv1_Str: String;
    Niv2_Int: Integer;
    Niv2_Str: String;
    Niv3_Int: Integer;
    Niv3_Str: String;
    Niv4_Int: Integer;
    Niv4_Str: String;
  end;
  TItens = array of TFinanceiroItem;

  TFinanceiroCad = class
  private
    FDB: TmySQLDatabase;
    FQuery: TmySQLQuery;
    FItens: TItens;
    FCodigo: Integer;
    FNome: String;
    FOrdem: Integer;
    FAtivo: Boolean;
    function Move(Nivel, Codigo: Integer; TabelaUpd, Tabela, CampoTab: String): Integer;
  published
    constructor Create(DB: TmySQLDatabase);
    destructor Destroy;
    property Codigo: Integer read FCodigo default 0;
    property Nome: String read FNome;
    property Ordem: Integer read FOrdem default 0;
    property Ativo: Boolean read FAtivo default True;
    property Itens: TItens read FItens;
  end;

  TPlano = class(TFinanceiroCad)
  public
    function  InsUpd(Nome: String; Ativo: Boolean; Codigo: Integer = 0): Integer;
    procedure Exclui(Codigo: Integer);
    procedure MostraEdicao(Codigo: Integer = 0);
    procedure ReabreQuery();
  end;

  TConjunto = class(TFinanceiroCad)
  protected
    FPlano: Integer;
  public
    function  InsUpd(Nome: String; Ativo: Boolean; Plano: Integer; Codigo: Integer = 0): Integer;
    function  Mover(Codigo: Integer): Integer;
    procedure Exclui(Codigo: Integer);
    procedure MostraEdicao(Codigo, Plano: Integer);
    procedure ReabreQuery(Codigo: Integer);
  published
    property Plano: Integer read FPlano default 0;
  end;

  TGrupo = class(TFinanceiroCad)
  protected
    FConjunto: Integer;
  public
    function  InsUpd(Nome: String; Ativo: Boolean; Conjunto: Integer; Codigo: Integer = 0): Integer;
    function  Mover(Codigo: Integer): Integer;
    procedure Exclui(Codigo: Integer);
    procedure MostraEdicao(Codigo, Conjunto: Integer);
    procedure ReabreQuery(Codigo: Integer);
  published
    property Conjunto: Integer read FConjunto default 0;
  end;

  TSubGrupo = class(TFinanceiroCad)
  protected
    FGrupo: Integer;
  public
    function  InsUpd(Nome: String; Ativo: Boolean; Grupo: Integer; Codigo: Integer = 0): Integer;
    function  Mover(Codigo: Integer): Integer;
    procedure Exclui(Codigo: Integer);
    procedure MostraEdicao(Codigo, Grupo: Integer);
    procedure ReabreQuery(Codigo: Integer);
  published
    property Grupo: Integer read FGrupo default 0;
  end;

  TFinContaTipo = (istNenhum = 0, istCredito = 1, istDebito = 2, istAmbos = 3);
  TConta = class(TFinanceiroCad)
  private
    FValorPrevisto: Double;
    function  ObtemTipoDeLancamento(Debito, Credito: Boolean): TFinContaTipo;
    procedure ConfiguraTipoDeLancamento(ContaTipo: TFinContaTipo; var Debito, Credito: String);
  protected
    FSubGrupo: Integer;
    FContaTipo: TFinContaTipo;
    FMensal: Boolean;
    FMensDebi: Double;
    FMensCred: Double;
  public
    function  InsUpd(Nome: String; Ativo: Boolean; SubGrupo: Integer;
              ContaTipo: TFinContaTipo; Mensal: Boolean; Codigo: Integer = 0): Integer;
    function  Mover(Codigo: Integer): Integer;
    procedure Exclui(Codigo: Integer);
    procedure MostraEdicao(Codigo, SubGrupo: Integer);
    procedure ReabreQuery(Codigo: Integer);
  published
    property SubGrupo: Integer read FSubGrupo default 0;
    property ContaTipo: TFinContaTipo read FContaTipo;
    property Mensal: Boolean read FMensal default False;
    property ValorPrevisto: Double read FValorPrevisto;
  end;

  TOpcoesFinanceiras = class(TFinanceiroCad)
  protected
    FPlanoPadrao: Integer;
  public
    procedure MostraEdicao(Empresa: Integer);
    procedure InsUpd(Empresa, PlanoPadrao: Integer);
  published
    property PlanoPadrao: Integer read FPlanoPadrao default 0;
  end;

  TCentroCustos = class(TFinanceiroCad)
  public
    procedure MostraEdicao(Codigo: Integer = 0);
    function  InsUpd(Nome: String; Ativo: Boolean; Codigo: Integer = 0): Integer;
  end;

  TCrtTip = (carCaixa = 0, carBanco = 1, carCC = 2, carDuplicata = 3,
             carCheque = 4, carBoleto = 5, carAPagar = 6, carAReceber = 7,
             carEmisOutros = 8);
  TCarteiras = class(TFinanceiroCad)
  protected
    FTipo: Integer;
    FEmpresa: Integer;
    FPagRec: Integer;
    FCarteira_Receb: Integer;
    FExigeCheque: Boolean;
    FBanco: Integer;
    FAgencia: Integer;
    FConta: String;
    FTipoCarteira: Integer;
  public
    procedure ReopenForneceI(DB: TmySQLDatabase; Query: TMySQLQuery;
              DataSource: TDataSource; EditForneceI: TdmkEditCB;
              ComboForneceI: TdmkDBLookupComboBox);
    procedure ReopenCarteira_Receb(DB: TmySQLDatabase; Query: TMySQLQuery;
              DataSource: TDataSource; EditBanco: TdmkEditCB;
              ComboBanco: TdmkDBLookupComboBox; Empresa: Integer);
    procedure MostraEdicao(TipoCarteira: TCrtTip; Empresa: Integer; Codigo: Integer = 0);
    function  InsUpd(Nome: String; Banco, Agencia: Integer; Conta: String;
              Carteira_Receb, PagRec: Integer; Ativo, ExigeCheque: Boolean;
              Tipo, Empresa: Integer; TipoCarteira: TCrtTip; Codigo: Integer = 0): Integer;
  published
    property Tipo: Integer read FTipo default 0;
    property Empresa: Integer read FEmpresa default 0;
    property PagRec: Integer read FPagRec default 1;
    property Carteira_Receb: Integer read FCarteira_Receb default 0;
    property ExigeCheque: Boolean read FExigeCheque default False;
    property Banco: Integer read FBanco default 0;
    property Agencia: Integer read FAgencia default 0;
    property Conta: String read FConta;
    property TipoCarteira: Integer read FTipoCarteira default 0;
  end;

implementation

uses UMySQLModule, dmkGeral, UnMyObjects, DmkDAC_PF, MyDBCheck, UMySQLDB;

{ TPlano }

procedure TPlano.Exclui(Codigo: Integer);
begin
  Geral.MB_Aviso('N�o implementado!');
end;

function TPlano.InsUpd(Nome: String; Ativo: Boolean; Codigo: Integer = 0): Integer;
var
  SQLType: TSQLType;
  Cod: Integer;
begin
  Result := 0;
  //
  if MyObjects.FIC(Nome = '', nil, 'O campo "Nome" n�o foi informado!') then Exit;
  //
  if Codigo <> 0 then
  begin
    SQLType := stUpd;
    Cod     := Codigo;
  end else
  begin
    SQLType := stIns;
    Cod     := UMyMod.BuscaEmLivreY(FDB, 'Livres', 'Controle', 'Plano', 'Plano', 'Codigo');
  end;
  //
  if Cod <> 0 then
  begin
    if UMyMod.SQLInsUpd(FQuery, SQLType, 'plano', False,
      ['Nome', 'CtrlaSdo', 'Ativo'], ['Codigo'],
      [Nome, 1, Ativo], [Cod], True)
    then
      Result := Cod;
  end else
    Geral.MB_Erro('Falha ao obter "C�digo"!' + sLineBreak + 'Fun��o: "TPlano.InsUpd"');
end;

procedure TPlano.MostraEdicao(Codigo: Integer = 0);
begin
  FCodigo := 0;
  FNome   := '';
  FOrdem  := 0;
  FAtivo  := True;
  //
  if Codigo <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(FQuery, FDB, [
      'SELECT * ',
      'FROM plano ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    if FQuery.RecordCount > 0 then
    begin
      FCodigo := FQuery.FieldByName('Codigo').AsInteger;
      FNome   := FQuery.FieldByName('Nome').AsString;
      FOrdem  := FQuery.FieldByName('OrdemLista').AsInteger;
      FAtivo  := Geral.IntToBool(FQuery.FieldByName('Ativo').AsInteger);
    end;
  end;
end;

procedure TPlano.ReabreQuery();
var
  Idx: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(FQuery, FDB, [
    'SELECT Codigo, Nome ',
    'FROM plano ',
    'WHERE Codigo > 0',
    'ORDER BY Nome',
    '']);
  if FQuery.RecordCount > 0 then
  begin
    SetLength(FItens, FQuery.RecordCount);
    //
    while not FQuery.EOF do
    begin
      Idx := FQuery.RecNo - 1;
      //
      FItens[Idx].Codigo    := FQuery.FieldByName('Codigo').AsInteger;
      FItens[Idx].Nome      := FQuery.FieldByName('Nome').AsString;
      FItens[Idx].Niv_Atual := 1;
      FItens[Idx].Niv1_Int  := 0;
      FItens[Idx].Niv1_Str  := '';
      FItens[Idx].Niv2_Int  := 0;
      FItens[Idx].Niv2_Str  := '';
      FItens[Idx].Niv3_Int  := 0;
      FItens[Idx].Niv3_Str  := '';
      FItens[Idx].Niv4_Int  := 0;
      FItens[Idx].Niv4_Str  := '';
      //
      FQuery.Next;
    end;
  end;
end;

{ TConjunto }

procedure TConjunto.Exclui(Codigo: Integer);
begin
  Geral.MB_Aviso('N�o implementado!');
end;

function TConjunto.InsUpd(Nome: String; Ativo: Boolean; Plano, Codigo: Integer): Integer;
var
  SQLType: TSQLType;
  Cod: Integer;
begin
  Result := 0;
  //
  if MyObjects.FIC(Nome = '', nil, 'O campo "Nome" n�o foi informado!') then Exit;
  if MyObjects.FIC(Plano = 0, nil, 'O campo "Plano" n�o foi informado!') then Exit;
  //
  if Codigo <> 0 then
  begin
    SQLType := stUpd;
    Cod     := Codigo;
  end else
  begin
    SQLType := stIns;
    Cod     := UMyMod.BuscaEmLivreY(FDB, 'Livres', 'Controle', 'Conjuntos', 'Conjunto', 'Codigo');
  end;
  //
  if Cod <> 0 then
  begin
    if UMyMod.SQLInsUpd(FQuery, SQLType, 'conjuntos', False,
      ['Nome', 'Plano', 'CtrlaSdo', 'Ativo'], ['Codigo'],
      [Nome, Plano, 1, Ativo], [Cod], True)
    then
      Result := Cod;
  end else
    Geral.MB_Erro('Falha ao obter "C�digo"!' + sLineBreak + 'Fun��o: "TConjunto.InsUpd"');
end;

function TConjunto.Mover(Codigo: Integer): Integer;
begin
  Result := Move(2, Codigo, 'conjuntos', 'plano', 'Plano');
end;

procedure TConjunto.MostraEdicao(Codigo, Plano: Integer);
begin
  FCodigo := 0;
  FNome   := '';
  FOrdem  := 0;
  FAtivo  := True;
  FPlano  := Plano;
  //
  if Codigo <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(FQuery, FDB, [
      'SELECT * ',
      'FROM conjuntos ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    if FQuery.RecordCount > 0 then
    begin
      FCodigo := FQuery.FieldByName('Codigo').AsInteger;
      FNome   := FQuery.FieldByName('Nome').AsString;
      FOrdem  := FQuery.FieldByName('OrdemLista').AsInteger;
      FAtivo  := Geral.IntToBool(FQuery.FieldByName('Ativo').AsInteger);
      FPlano  := FQuery.FieldByName('Plano').AsInteger;
    end;
  end;
end;

procedure TConjunto.ReabreQuery(Codigo: Integer);
var
  Idx: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(FQuery, FDB, [
    'SELECT con.Codigo, con.Nome, ',
    'pla.Codigo Plano_Int, pla.Nome Plano_Str ',
    'FROM conjuntos con ',
    'LEFT JOIN plano pla ON pla.Codigo = con.Plano ',
    'WHERE con.Codigo > 0 ',
    'AND con.Plano=' + Geral.FF0(Codigo),
    '']);
  if FQuery.RecordCount > 0 then
  begin
    SetLength(FItens, FQuery.RecordCount);
    //
    while not FQuery.EOF do
    begin
      Idx := FQuery.RecNo - 1;
      //
      FItens[Idx].Codigo    := FQuery.FieldByName('Codigo').AsInteger;
      FItens[Idx].Nome      := FQuery.FieldByName('Nome').AsString;
      FItens[Idx].Niv_Atual := 2;
      FItens[Idx].Niv1_Int  := FQuery.FieldByName('Plano_Int').AsInteger;
      FItens[Idx].Niv1_Str  := FQuery.FieldByName('Plano_Str').AsString;
      FItens[Idx].Niv2_Int  := 0;
      FItens[Idx].Niv2_Str  := '';
      FItens[Idx].Niv3_Int  := 0;
      FItens[Idx].Niv3_Str  := '';
      FItens[Idx].Niv4_Int  := 0;
      FItens[Idx].Niv4_Str  := '';
      //
      FQuery.Next;
    end;
  end;
end;

{ TGrupo }

procedure TGrupo.Exclui(Codigo: Integer);
begin
  Geral.MB_Aviso('N�o implementado!');
end;

function TGrupo.InsUpd(Nome: String; Ativo: Boolean; Conjunto, Codigo: Integer): Integer;
var
  SQLType: TSQLType;
  Cod: Integer;
begin
  Result := 0;
  //
  if MyObjects.FIC(Nome = '', nil, 'O campo "Nome" n�o foi informado!') then Exit;
  if MyObjects.FIC(Conjunto = 0, nil, 'O campo "Conjunto" n�o foi informado!') then Exit;
  //
  if Codigo <> 0 then
  begin
    SQLType := stUpd;
    Cod     := Codigo;
  end else
  begin
    SQLType := stIns;
    Cod     := UMyMod.BuscaEmLivreY(FDB, 'Livres', 'Controle', 'Grupos', 'Grupo', 'Codigo');
  end;
  //
  if Cod <> 0 then
  begin
    if UMyMod.SQLInsUpd(FQuery, SQLType, 'grupos', False,
      ['Nome', 'Conjunto', 'CtrlaSdo', 'Ativo'], ['Codigo'],
      [Nome, Conjunto, 1, Ativo], [Cod], True)
    then
      Result := Cod;
  end else
    Geral.MB_Erro('Falha ao obter "C�digo"!' + sLineBreak + 'Fun��o: "TGrupo.InsUpd"');
end;

procedure TGrupo.MostraEdicao(Codigo, Conjunto: Integer);
begin
  FCodigo   := 0;
  FNome     := '';
  FOrdem    := 0;
  FAtivo    := True;
  FConjunto := Conjunto;
  //
  if Codigo <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(FQuery, FDB, [
      'SELECT * ',
      'FROM grupos ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    if FQuery.RecordCount > 0 then
    begin
      FCodigo   := FQuery.FieldByName('Codigo').AsInteger;
      FNome     := FQuery.FieldByName('Nome').AsString;
      FOrdem    := FQuery.FieldByName('OrdemLista').AsInteger;
      FAtivo    := Geral.IntToBool(FQuery.FieldByName('Ativo').AsInteger);
      FConjunto := FQuery.FieldByName('Conjunto').AsInteger;
    end;
  end;
end;

function TGrupo.Mover(Codigo: Integer): Integer;
begin
  Result := Move(3, Codigo, 'grupos', 'conjuntos', 'Conjunto');
end;

procedure TGrupo.ReabreQuery(Codigo: Integer);
var
  Idx: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(FQuery, FDB, [
    'SELECT gru.Codigo, gru.Nome, ',
    'con.Codigo Conjunto_Int, con.Nome Conjunto_Str, ',
    'pla.Codigo Plano_Int, pla.Nome Plano_Str ',
    'FROM grupos gru ',
    'LEFT JOIN conjuntos con ON con.Codigo = gru.Conjunto ',
    'LEFT JOIN plano pla ON pla.Codigo = con.Plano ',
    'WHERE con.Codigo > 0 ',
    'AND gru.Conjunto=' + Geral.FF0(Codigo),
    '']);
  if FQuery.RecordCount > 0 then
  begin
    SetLength(FItens, FQuery.RecordCount);
    //
    while not FQuery.EOF do
    begin
      Idx := FQuery.RecNo - 1;
      //
      FItens[Idx].Codigo    := FQuery.FieldByName('Codigo').AsInteger;
      FItens[Idx].Nome      := FQuery.FieldByName('Nome').AsString;
      FItens[Idx].Niv_Atual := 3;
      FItens[Idx].Niv1_Int  := FQuery.FieldByName('Plano_Int').AsInteger;
      FItens[Idx].Niv1_Str  := FQuery.FieldByName('Plano_Str').AsString;
      FItens[Idx].Niv2_Int  := FQuery.FieldByName('Conjunto_Int').AsInteger;
      FItens[Idx].Niv2_Str  := FQuery.FieldByName('Conjunto_Str').AsString;
      FItens[Idx].Niv3_Int  := 0;
      FItens[Idx].Niv3_Str  := '';
      FItens[Idx].Niv4_Int  := 0;
      FItens[Idx].Niv4_Str  := '';
      //
      FQuery.Next;
    end;
  end;
end;

{ TSubGrupo }

procedure TSubGrupo.Exclui(Codigo: Integer);
begin
  Geral.MB_Aviso('N�o implementado!');
end;

function TSubGrupo.InsUpd(Nome: String; Ativo: Boolean; Grupo, Codigo: Integer): Integer;
var
  SQLType: TSQLType;
  Cod: Integer;
begin
  Result := 0;
  //
  if MyObjects.FIC(Nome = '', nil, 'O campo "Nome" n�o foi informado!') then Exit;
  if MyObjects.FIC(Grupo = 0, nil, 'O campo "Grupo" n�o foi informado!') then Exit;
  //
  if Codigo <> 0 then
  begin
    SQLType := stUpd;
    Cod     := Codigo;
  end else
  begin
    SQLType := stIns;
    Cod     := UMyMod.BuscaEmLivreY(FDB, 'Livres', 'Controle', 'SubGrupos', 'SubGrupo', 'Codigo');
  end;
  //
  if Cod <> 0 then
  begin
    if UMyMod.SQLInsUpd(FQuery, SQLType, 'subgrupos', False,
      ['Nome', 'Grupo', 'CtrlaSdo', 'Ativo'], ['Codigo'],
      [Nome, Grupo, 1, Ativo], [Cod], True)
    then
      Result := Cod;
  end else
    Geral.MB_Erro('Falha ao obter "C�digo"!' + sLineBreak + 'Fun��o: "TSubGrupo.InsUpd"');
end;

procedure TSubGrupo.MostraEdicao(Codigo, Grupo: Integer);
begin
  FCodigo := 0;
  FNome   := '';
  FOrdem  := 0;
  FAtivo  := True;
  FGrupo  := Grupo;
  //
  if Codigo <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(FQuery, FDB, [
      'SELECT * ',
      'FROM subgrupos ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    if FQuery.RecordCount > 0 then
    begin
      FCodigo := FQuery.FieldByName('Codigo').AsInteger;
      FNome   := FQuery.FieldByName('Nome').AsString;
      FOrdem  := FQuery.FieldByName('OrdemLista').AsInteger;
      FAtivo  := Geral.IntToBool(FQuery.FieldByName('Ativo').AsInteger);
      FGrupo  := FQuery.FieldByName('Grupo').AsInteger;
    end;
  end;
end;

function TSubGrupo.Mover(Codigo: Integer): Integer;
begin
  Result := Move(4, Codigo, 'subgrupos', 'grupos', 'Grupo');
end;

procedure TSubGrupo.ReabreQuery(Codigo: Integer);
var
  Idx: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(FQuery, FDB, [
    'SELECT sub.Codigo, sub.Nome, ',
    'gru.Codigo Grupo_Int, gru.Nome Grupo_Str, ',
    'con.Codigo Conjunto_Int, con.Nome Conjunto_Str, ',
    'pla.Codigo Plano_Int, pla.Nome Plano_Str ',
    'FROM subgrupos sub ',
    'LEFT JOIN grupos gru ON gru.Codigo = sub.Grupo ',
    'LEFT JOIN conjuntos con ON con.Codigo = gru.Conjunto ',
    'LEFT JOIN plano pla ON pla.Codigo = con.Plano ',
    'WHERE con.Codigo > 0 ',
    'AND sub.Grupo=' + Geral.FF0(Codigo),
    '']);
  if FQuery.RecordCount > 0 then
  begin
    SetLength(FItens, FQuery.RecordCount);
    //
    while not FQuery.EOF do
    begin
      Idx := FQuery.RecNo - 1;
      //
      FItens[Idx].Codigo    := FQuery.FieldByName('Codigo').AsInteger;
      FItens[Idx].Nome      := FQuery.FieldByName('Nome').AsString;
      FItens[Idx].Niv_Atual := 4;
      FItens[Idx].Niv1_Int  := FQuery.FieldByName('Plano_Int').AsInteger;
      FItens[Idx].Niv1_Str  := FQuery.FieldByName('Plano_Str').AsString;
      FItens[Idx].Niv2_Int  := FQuery.FieldByName('Conjunto_Int').AsInteger;
      FItens[Idx].Niv2_Str  := FQuery.FieldByName('Conjunto_Str').AsString;
      FItens[Idx].Niv3_Int  := FQuery.FieldByName('Grupo_Int').AsInteger;
      FItens[Idx].Niv3_Str  := FQuery.FieldByName('Grupo_Str').AsString;
      FItens[Idx].Niv4_Int  := 0;
      FItens[Idx].Niv4_Str  := '';
      //
      FQuery.Next;
    end;
  end;
end;

{ TConta }

procedure TConta.Exclui(Codigo: Integer);
begin
  Geral.MB_Aviso('N�o implementado!');
end;

function TConta.InsUpd(Nome: String; Ativo: Boolean; SubGrupo: Integer;
  ContaTipo: TFinContaTipo; Mensal: Boolean; Codigo: Integer): Integer;
var
  SQLType: TSQLType;
  Cod: Integer;
  Debi, Cred, Mens: String;
begin
  Result := 0;
  //
  if MyObjects.FIC(Nome = '', nil, 'O campo "Nome" n�o foi informado!') then Exit;
  if MyObjects.FIC(SubGrupo = 0, nil, 'O campo "SubGrupo" n�o foi informado!') then Exit;
  //
  if Codigo <> 0 then
  begin
    SQLType := stUpd;
    Cod     := Codigo;
  end else
  begin
    SQLType := stIns;
    Cod     := UMyMod.BuscaEmLivreY(FDB, 'Livres', 'Controle', 'Contas', 'Contas', 'Codigo');
  end;
  //
  if Cod <> 0 then
  begin
    Mens := Geral.BoolToStr(Mensal);
    //
    ConfiguraTipoDeLancamento(ContaTipo, Debi, Cred);
    //
    if UMyMod.SQLInsUpd(FQuery, SQLType, 'contas', False,
      ['Nome', 'SubGrupo', 'Debito', 'Credito', 'Mensal', 'CtrlaSdo', 'Ativo'], ['Codigo'],
      [Nome, SubGrupo, Debi, Cred, Mens, 1, Ativo], [Cod], True)
    then
      Result := Cod;
  end else
    Geral.MB_Erro('Falha ao obter "C�digo"!' + sLineBreak + 'Fun��o: "TConta.InsUpd"');
end;

procedure TConta.MostraEdicao(Codigo, SubGrupo: Integer);
var
  Debito, Credito: Boolean;
begin
  FCodigo     := 0;
  FNome       := '';
  FOrdem      := 0;
  FAtivo      := True;
  FSubGrupo   := SubGrupo;
  FContaTipo  := istNenhum;
  FMensal     := False;
  FMensDebi   := 0;
  FMensCred   := 0;
  //
  if Codigo <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(FQuery, FDB, [
      'SELECT * ',
      'FROM contas ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    if FQuery.RecordCount > 0 then
    begin
      Debito  := Geral.StrToBool(FQuery.FieldByName('Debito').AsString);
      Credito := Geral.StrToBool(FQuery.FieldByName('Credito').AsString);
      //
      FCodigo    := FQuery.FieldByName('Codigo').AsInteger;
      FNome      := FQuery.FieldByName('Nome').AsString;
      FOrdem     := FQuery.FieldByName('OrdemLista').AsInteger;
      FAtivo     := Geral.IntToBool(FQuery.FieldByName('Ativo').AsInteger);
      FSubGrupo  := FQuery.FieldByName('SubGrupo').AsInteger;
      FContaTipo := ObtemTipoDeLancamento(Debito, Credito);
      FMensal    := Geral.StrToBool(FQuery.FieldByName('Mensal').AsString);
      FMensDebi  := FQuery.FieldByName('Mensdeb').AsFloat;
      FMensCred  := FQuery.FieldByName('Menscred').AsFloat;
    end;
  end;
end;

procedure TConta.ConfiguraTipoDeLancamento(ContaTipo: TFinContaTipo; var Debito,
  Credito: String);
begin
  case ContaTipo of
    istNenhum:
    begin
      Debito  := Geral.BoolToStr(False);
      Credito := Geral.BoolToStr(False);
    end;
    istCredito:
    begin
      Debito  := Geral.BoolToStr(False);
      Credito := Geral.BoolToStr(True);
    end;
    istDebito:
    begin
      Debito  := Geral.BoolToStr(True);
      Credito := Geral.BoolToStr(False);
    end;
    istAmbos:
    begin
      Debito  := Geral.BoolToStr(True);
      Credito := Geral.BoolToStr(True);
    end;
  end;
end;

function TConta.ObtemTipoDeLancamento(Debito, Credito: Boolean): TFinContaTipo;
begin
  if Debito and Credito then
    Result := istAmbos
  else if Debito then
    Result := istDebito
  else if Credito then
    Result := istCredito
  else
    Result := istNenhum;
end;

function TConta.Mover(Codigo: Integer): Integer;
begin
  Result := Move(5, Codigo, 'contas', 'subgrupos', 'Subgrupo');
end;

procedure TConta.ReabreQuery(Codigo: Integer);
var
  Idx: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(FQuery, FDB, [
    'SELECT cta.Codigo, cta.Nome, ',
    'sub.Codigo Subgrupo_Int, sub.Nome Subgrupo_Str, ',
    'gru.Codigo Grupo_Int, gru.Nome Grupo_Str, ',
    'con.Codigo Conjunto_Int, con.Nome Conjunto_Str, ',
    'pla.Codigo Plano_Int, pla.Nome Plano_Str ',
    'FROM contas cta ',
    'LEFT JOIN subgrupos sub ON sub.Codigo = cta.Subgrupo ',
    'LEFT JOIN grupos gru ON gru.Codigo = sub.Grupo ',
    'LEFT JOIN conjuntos con ON con.Codigo = gru.Conjunto ',
    'LEFT JOIN plano pla ON pla.Codigo = con.Plano ',
    'WHERE con.Codigo > 0 ',
    'AND cta.Subgrupo=' + Geral.FF0(Codigo),
    '']);
  if FQuery.RecordCount > 0 then
  begin
    SetLength(FItens, FQuery.RecordCount);
    //
    while not FQuery.EOF do
    begin
      Idx := FQuery.RecNo - 1;
      //
      FItens[Idx].Codigo    := FQuery.FieldByName('Codigo').AsInteger;
      FItens[Idx].Nome      := FQuery.FieldByName('Nome').AsString;
      FItens[Idx].Niv_Atual := 5;
      FItens[Idx].Niv1_Int  := FQuery.FieldByName('Plano_Int').AsInteger;
      FItens[Idx].Niv1_Str  := FQuery.FieldByName('Plano_Str').AsString;
      FItens[Idx].Niv2_Int  := FQuery.FieldByName('Conjunto_Int').AsInteger;
      FItens[Idx].Niv2_Str  := FQuery.FieldByName('Conjunto_Str').AsString;
      FItens[Idx].Niv3_Int  := FQuery.FieldByName('Grupo_Int').AsInteger;
      FItens[Idx].Niv3_Str  := FQuery.FieldByName('Grupo_Str').AsString;
      FItens[Idx].Niv4_Int  := FQuery.FieldByName('Subgrupo_Int').AsInteger;
      FItens[Idx].Niv4_Str  := FQuery.FieldByName('Subgrupo_Str').AsString;
      //
      FQuery.Next;
    end;
  end;
end;

{ TOpcoesFinanceiras }

procedure TOpcoesFinanceiras.MostraEdicao(Empresa: Integer);
var
  Qry: TmySQLQuery;
begin
  FPlanoPadrao := 0;
  //
  Qry := TmySQLQuery.Create(TDataModule(FDB.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, FDB, [
      'SELECT * ',
      'FROM paramsemp ',
      'WHERE Codigo=' + Geral.FF0(Empresa),
      '']);
    if Qry.RecordCount > 0 then
      FPlanoPadrao := Qry.FieldByName('PlanoPadrao').AsInteger;
  finally
    Qry.Free;
  end;
end;

procedure TOpcoesFinanceiras.InsUpd(Empresa, PlanoPadrao: Integer);
begin
  UMyMod.SQLInsUpd(FQuery, stUpd, 'paramsemp', False,
    ['PlanoPadrao'], ['Codigo'],
    [PlanoPadrao], [Empresa], True);
end;

{ TCentroCustos }

function TCentroCustos.InsUpd(Nome: String; Ativo: Boolean; Codigo: Integer): Integer;
var
  SQLType: TSQLType;
  Cod: Integer;
begin
  Result := 0;
  //
  if MyObjects.FIC(Nome = '', nil, 'O campo "Nome" n�o foi informado!') then Exit;
  //
  if Codigo <> 0 then
  begin
    SQLType := stUpd;
    Cod     := Codigo;
  end else
  begin
    SQLType := stIns;
    Cod     := UMyMod.BuscaEmLivreY_Def('centrocusto', 'Codigo', stIns, 0);
  end;
  //
  if Cod <> 0 then
  begin
    if UMyMod.SQLInsUpd(FQuery, SQLType, 'centrocusto', False,
      ['Nome', 'Ativo'], ['Codigo'],
      [Nome, Ativo], [Cod], True)
    then
      Result := Cod;
  end else
    Geral.MB_Erro('Falha ao obter "C�digo"!' + sLineBreak + 'Fun��o: "TCentroCustos.InsUpd"');
end;

procedure TCentroCustos.MostraEdicao(Codigo: Integer);
var
  Qry: TmySQLQuery;
begin
  FCodigo := 0;
  FNome   := '';
  FAtivo  := True;
  //
  if Codigo <> 0 then
  begin
    Qry := TmySQLQuery.Create(TDataModule(FDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, FDB, [
        'SELECT * ',
        'FROM centrocusto ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
      if Qry.RecordCount > 0 then
      begin
        FCodigo := Qry.FieldByName('Codigo').AsInteger;
        FNome   := Qry.FieldByName('Nome').AsString;
        FAtivo  := Geral.IntToBool(Qry.FieldByName('Ativo').AsInteger);
      end;
    finally
      Qry.Free;
    end;
  end;
end;

{ TCarteiras }

function TCarteiras.InsUpd(Nome: String; Banco, Agencia: Integer; Conta: String;
  Carteira_Receb, PagRec: Integer; Ativo, ExigeCheque: Boolean; Tipo,
  Empresa: Integer; TipoCarteira: TCrtTip; Codigo: Integer = 0): Integer;
var
  SQLType: TSQLType;
  Cod: Integer;
begin
  Result := 0;
  //
  if MyObjects.FIC(Nome = '', nil, 'O campo "Descri��o" deve ser informado!') then Exit;
  //
  case TipoCarteira of
    carBanco:
    begin
      (* N�o precisa ser obrigat�rio
      if MyObjects.FIC(Banco = 0, nil, 'O campo "Banco" deve ser informado!') then Exit;
      if MyObjects.FIC(Agencia = 0, nil, 'O campo "Ag�ncia" deve ser informado!') then Exit;
      if MyObjects.FIC(Conta = '', nil, 'O campo "Conta" deve ser informado!') then Exit;
      *)
    end;
    carDuplicata, carCheque, carAPagar, carBoleto, carAReceber:
    begin
      if MyObjects.FIC(Carteira_Receb = 0, nil, 'O campo "Carteira padr�o onde ser�o quitas as faturas" deve ser preenchido!') then Exit;
    end;
  end;

  if MyObjects.FIC(Nome = '', nil, 'O campo "Nome" n�o foi informado!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'O campo "Empresa" n�o foi informado!') then Exit;
  //
  if Codigo <> 0 then
  begin
    SQLType := stUpd;
    Cod     := Codigo;
  end else
  begin
    SQLType := stIns;
    Cod     := UMyMod.BuscaEmLivreY_Def('carteiras', 'Codigo', stIns, 0);
  end;
  //
  if Cod <> 0 then
  begin
    if UMyMod.SQLInsUpd(FQuery, SQLType, 'carteiras', False,
      ['Nome', 'Banco1', 'Agencia1', 'Conta1', 'Banco', 'PagRec', 'Ativo',
      'ExigeNumCheque', 'Tipo', 'ForneceI', 'TipCart'], ['Codigo'],
      [Nome, Banco, Agencia, Conta, Carteira_Receb, PagRec - 1, Ativo,
      Geral.BoolToInt(ExigeCheque), Tipo, Empresa, TipoCarteira], [Cod], True)
    then
      Result := Cod;
  end else
    Geral.MB_Erro('Falha ao obter "C�digo"!' + sLineBreak + 'Fun��o: "TCentroCustos.InsUpd"');
end;

procedure TCarteiras.ReopenForneceI(DB: TmySQLDatabase; Query: TMySQLQuery;
  DataSource: TDataSource; EditForneceI: TdmkEditCB;
  ComboForneceI: TdmkDBLookupComboBox);
begin
  EditForneceI.DBLookupComboBox := ComboForneceI;
  ComboForneceI.dmkEditCB       := EditForneceI;
  ComboForneceI.ListSource      := DataSource;
  ComboForneceI.KeyField        := 'Codigo';
  ComboForneceI.ListField       := 'NOMEENTIDADE';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DB, [
    'SELECT Codigo, ',
    'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE ',
    'FROM entidades ',
    'ORDER BY NomeENTIDADE ',
    '']);
end;

procedure TCarteiras.ReopenCarteira_Receb(DB: TmySQLDatabase; Query: TMySQLQuery;
  DataSource: TDataSource; EditBanco: TdmkEditCB; ComboBanco: TdmkDBLookupComboBox;
  Empresa: Integer);
begin
  EditBanco.DBLookupComboBox := ComboBanco;
  ComboBanco.dmkEditCB       := EditBanco;
  ComboBanco.ListSource      := DataSource;
  ComboBanco.KeyField        := 'Codigo';
  ComboBanco.ListField       := 'Nome';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DB, [
    'SELECT Codigo, Nome ',
    'FROM carteiras ',
    'WHERE Tipo=1 ',
    'AND Ativo=1 ',
    'AND ForneceI=' + Geral.FF0(Empresa),
    'ORDER BY Nome ',
    '']);
end;

procedure TCarteiras.MostraEdicao(TipoCarteira: TCrtTip; Empresa: Integer;
  Codigo: Integer = 0);
var
  Qry: TmySQLQuery;
begin
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa n�o definida!') then Exit;
  //
  case TipoCarteira of
    carCaixa:
    begin
      FCodigo         := 0;
      FNome           := '';
      FBanco          := 0;
      FAgencia        := 0;
      FConta          := '';
      FAtivo          := True;
      FExigeCheque    := False;
      FCarteira_Receb := 0;
      FPagRec         := 1; //Ambos
      FTipo           := 0; //Caixa
      FEmpresa        := Empresa;
      FTipoCarteira   := Integer(TipoCarteira);
    end;
    carBanco, carCC:
    begin
      FCodigo         := 0;
      FNome           := '';
      FBanco          := 0;
      FAgencia        := 0;
      FConta          := '';
      FAtivo          := True;
      FExigeCheque    := False;
      FCarteira_Receb := 0;
      FPagRec         := 1; //Ambos
      FTipo           := 1; //Banco
      FEmpresa        := Empresa;
      FTipoCarteira   := Integer(TipoCarteira);
    end;
    carDuplicata, carCheque, carAPagar:
    begin
      FCodigo         := 0;
      FNome           := '';
      FBanco          := 0;
      FAgencia        := 0;
      FConta          := '';
      FAtivo          := True;
      FExigeCheque    := TipoCarteira = carCheque;
      FCarteira_Receb := 0;
      FPagRec         := 0; //Pagar
      FTipo           := 2; //Emiss�o
      FEmpresa        := Empresa;
      FTipoCarteira   := Integer(TipoCarteira);
    end;
    carBoleto, carAReceber:
    begin
      FCodigo         := 0;
      FNome           := '';
      FBanco          := 0;
      FAgencia        := 0;
      FConta          := '';
      FAtivo          := True;
      FExigeCheque    := False;
      FCarteira_Receb := 0;
      FPagRec         := 2; //Receber
      FTipo           := 2; //Emiss�o
      FEmpresa        := Empresa;
      FTipoCarteira   := Integer(TipoCarteira);
    end;
    carEmisOutros:
    begin
      FCodigo         := 0;
      FNome           := '';
      FBanco          := 0;
      FAgencia        := 0;
      FConta          := '';
      FAtivo          := True;
      FExigeCheque    := False;
      FCarteira_Receb := 0;
      FPagRec         := 1; //Outros
      FTipo           := 2; //Emiss�o
      FEmpresa        := Empresa;
      FTipoCarteira   := Integer(TipoCarteira);
    end
    else
      Geral.MB_Erro('Tipo de carteira n�o implementada!' + sLineBreak +
        'Fun��o: TCarteiras.MostraEdicao');
  end;

  if Codigo <> 0 then
  begin
    Qry := TmySQLQuery.Create(TDataModule(FDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, FDB, [
        'SELECT * ',
        'FROM carteiras ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
      if Qry.RecordCount > 0 then
      begin
        FCodigo         := Qry.FieldByName('Codigo').AsInteger;;
        FNome           := Qry.FieldByName('Nome').AsString;
        FBanco          := Qry.FieldByName('Banco1').AsInteger;
        FAgencia        := Qry.FieldByName('Agencia1').AsInteger;
        FConta          := Qry.FieldByName('Conta1').AsString;
        FAtivo          := Geral.IntToBool(Qry.FieldByName('Ativo').AsInteger);
        FExigeCheque    := Geral.IntToBool(Qry.FieldByName('ExigeNumCheque').AsInteger);
        FCarteira_Receb := Qry.FieldByName('Banco').AsInteger;
        FPagRec         := Qry.FieldByName('PagRec').AsInteger + 1;
        FTipo           := Qry.FieldByName('Tipo').AsInteger;
        FEmpresa        := Qry.FieldByName('ForneceI').AsInteger;
        FTipoCarteira   := Qry.FieldByName('TipCart').AsInteger;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

{ TFinanceiroCad }

constructor TFinanceiroCad.Create(DB: TmySQLDatabase);
begin
  FDB    := DB;
  FQuery := TmySQLQuery.Create(TDataModule(DB.Owner));
  FQuery.Database := DB;
end;

destructor TFinanceiroCad.Destroy;
begin
  FDB.Free;
  FQuery.Free;
end;

function TFinanceiroCad.Move(Nivel, Codigo: Integer; TabelaUpd, Tabela, CampoTab: String): Integer;
const
  Aviso  = '...';
  Titulo = 'Sele��o de item';
  Prompt = 'Mover para:';
  Campo  = 'Descricao';
var
  CampoSel: Variant;
  Cod: Integer;
  SQL: String;
begin
  Result := 0;
  SQL    := '';
  //
  if Codigo = 0 then
  begin
    Geral.MB_Aviso('C�digo n�o informado!');
    Exit;
  end;
  //
  case Nivel of
    2: //Conjunto
    begin
      Cod := Geral.IMV(USQLDB.ObtemValorCampo(FDB, Codigo, 'Codigo', 'Plano', 'conjuntos'));
      //
      if Cod <> 0 then
        SQL := 'WHERE Codigo <> ' + Geral.FF0(Cod);
    end;
    3: //Grupo
    begin
      Cod := Geral.IMV(USQLDB.ObtemValorCampo(FDB, Codigo, 'Codigo', 'Conjunto', 'grupos'));
      //
      if Cod <> 0 then
        SQL := 'WHERE Codigo <> ' + Geral.FF0(Cod);
    end;
    4: //SubGrupo
    begin
      Cod := Geral.IMV(USQLDB.ObtemValorCampo(FDB, Codigo, 'Codigo', 'Grupo', 'subgrupos'));
      //
      if Cod <> 0 then
        SQL := 'WHERE Codigo <> ' + Geral.FF0(Cod);
    end;
    5: //Conta
    begin
      Cod := Geral.IMV(USQLDB.ObtemValorCampo(FDB, Codigo, 'Codigo', 'Subgrupo', 'contas'));
      //
      if Cod <> 0 then
        SQL := 'WHERE Codigo <> ' + Geral.FF0(Cod);
    end;
  end;
  //
  CampoSel := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
                'SELECT Codigo, Nome ' + Campo,
                'FROM ' + Tabela,
                SQL,
                'ORDER BY ' + Campo,
                ''], FDB, True);
  //
  if (CampoSel <> Null) and (CampoSel <> 0) then
  begin
    if not UMyMod.SQLInsUpd(FQuery, stUpd, TabelaUpd, False,
      [CampoTab], ['Codigo'], [CampoSel], [Codigo], True)
    then
      Geral.MB_Erro('Falha ao mover diret�rio!')
    else
      Result := CampoSel;
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

end.
