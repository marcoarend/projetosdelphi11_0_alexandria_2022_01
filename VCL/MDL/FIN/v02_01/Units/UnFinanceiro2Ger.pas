unit UnFinanceiro2Ger;

interface

uses System.Classes, System.Variants, Data.DB, UnDmkEnums, mySQLDbTables,
  dmkEditCB, dmkDBLookupComboBox;

type
  TCarteiras = class
  protected
    FTipo: Integer;
    FEmpresa: Integer;
    FPagRec: Integer;
    FCarteira_Receb: Integer;
    FExigeCheque: Boolean;
    FBanco: Integer;
    FAgencia: Integer;
    FConta: String;
    FTipoCarteira: Integer;
  public
    procedure ReopenForneceI(DB: TmySQLDatabase; Query: TMySQLQuery;
              DataSource: TDataSource; EditForneceI: TdmkEditCB;
              ComboForneceI: TdmkDBLookupComboBox);
    procedure ReopenCarteira_Receb(DB: TmySQLDatabase; Query: TMySQLQuery;
              DataSource: TDataSource; EditBanco: TdmkEditCB;
              ComboBanco: TdmkDBLookupComboBox; Empresa: Integer);
    procedure MostraEdicao(TipoCarteira: TCrtTip; Empresa: Integer; Codigo: Integer = 0);
    function  InsUpd(Nome: String; Banco, Agencia: Integer; Conta: String;
              Carteira_Receb, PagRec: Integer; Ativo, ExigeCheque: Boolean;
              Tipo, Empresa: Integer; TipoCarteira: TCrtTip; Codigo: Integer = 0): Integer;
  published
    property Tipo: Integer read FTipo default 0;
    property Empresa: Integer read FEmpresa default 0;
    property PagRec: Integer read FPagRec default 1;
    property Carteira_Receb: Integer read FCarteira_Receb default 0;
    property ExigeCheque: Boolean read FExigeCheque default False;
    property Banco: Integer read FBanco default 0;
    property Agencia: Integer read FAgencia default 0;
    property Conta: String read FConta;
    property TipoCarteira: Integer read FTipoCarteira default 0;
  end;

implementation

uses UMySQLModule, dmkGeral, UnMyObjects, DmkDAC_PF, MyDBCheck, UMySQLDB;

{ TCarteiras }

function TCarteiras.InsUpd(Nome: String; Banco, Agencia: Integer; Conta: String;
  Carteira_Receb, PagRec: Integer; Ativo, ExigeCheque: Boolean; Tipo,
  Empresa: Integer; TipoCarteira: TCrtTip; Codigo: Integer = 0): Integer;
var
  SQLType: TSQLType;
  Cod: Integer;
begin
  Result := 0;
  //
  if MyObjects.FIC(Nome = '', nil, 'O campo "Descri��o" deve ser informado!') then Exit;
  //
  case TipoCarteira of
    carBanco:
    begin
      if MyObjects.FIC(Banco = 0, nil, 'O campo "Banco" deve ser informado!') then Exit;
      if MyObjects.FIC(Agencia = 0, nil, 'O campo "Ag�ncia" deve ser informado!') then Exit;
      if MyObjects.FIC(Conta = '', nil, 'O campo "Conta" deve ser informado!') then Exit;
    end;
    carDuplicata, carCheque, carAPagar, carBoleto, carAReceber:
    begin
      if MyObjects.FIC(Carteira_Receb = 0, nil, 'O campo "Carteira padr�o onde ser�o quitas as faturas" deve ser preenchido!') then Exit;
    end;
  end;

  if MyObjects.FIC(Nome = '', nil, 'O campo "Nome" n�o foi informado!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'O campo "Empresa" n�o foi informado!') then Exit;
  //
  if Codigo <> 0 then
  begin
    SQLType := stUpd;
    Cod     := Codigo;
  end else
  begin
    SQLType := stIns;
    Cod     := UMyMod.BuscaEmLivreY_Def('carteiras', 'Codigo', stIns, 0);
  end;
  //
  if Cod <> 0 then
  begin
    if UMyMod.SQLInsUpd(FQuery, SQLType, 'carteiras', False,
      ['Nome', 'Banco1', 'Agencia1', 'Conta1', 'Banco', 'PagRec', 'Ativo',
      'ExigeNumCheque', 'Tipo', 'ForneceI', 'TipCart'], ['Codigo'],
      [Nome, Banco, Agencia, Conta, Carteira_Receb, PagRec, Ativo,
      Geral.BoolToInt(ExigeCheque), Tipo, Empresa, TipoCarteira], [Cod], True)
    then
      Result := Cod;
  end else
    Geral.MB_Erro('Falha ao obter "C�digo"!' + sLineBreak + 'Fun��o: "TCentroCustos.InsUpd"');
end;

procedure TCarteiras.ReopenForneceI(DB: TmySQLDatabase; Query: TMySQLQuery;
  DataSource: TDataSource; EditForneceI: TdmkEditCB;
  ComboForneceI: TdmkDBLookupComboBox);
begin
  EditForneceI.DBLookupComboBox := ComboForneceI;
  ComboForneceI.dmkEditCB       := EditForneceI;
  ComboForneceI.ListSource      := DataSource;
  ComboForneceI.KeyField        := 'Codigo';
  ComboForneceI.ListField       := 'NOMEENTIDADE';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DB, [
    'SELECT Codigo, ',
    'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE ',
    'FROM entidades ',
    'ORDER BY NomeENTIDADE ',
    '']);
end;

procedure TCarteiras.ReopenCarteira_Receb(DB: TmySQLDatabase; Query: TMySQLQuery;
  DataSource: TDataSource; EditBanco: TdmkEditCB; ComboBanco: TdmkDBLookupComboBox;
  Empresa: Integer);
begin
  EditBanco.DBLookupComboBox := ComboBanco;
  ComboBanco.dmkEditCB       := EditBanco;
  ComboBanco.ListSource      := DataSource;
  ComboBanco.KeyField        := 'Codigo';
  ComboBanco.ListField       := 'Nome';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DB, [
    'SELECT Codigo, Nome ',
    'FROM carteiras ',
    'WHERE Tipo=1 ',
    'AND Ativo=1 ',
    'AND ForneceI=' + Geral.FF0(Empresa),
    'ORDER BY Nome ',
    '']);
end;

procedure TCarteiras.MostraEdicao(TipoCarteira: TCrtTip; Empresa: Integer;
  Codigo: Integer = 0);
var
  Qry: TmySQLQuery;
begin
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa n�o definida!') then Exit;
  //
  case TipoCarteira of
    carCaixa:
    begin
      FCodigo         := 0;
      FNome           := '';
      FBanco          := 0;
      FAgencia        := 0;
      FConta          := '';
      FAtivo          := True;
      FExigeCheque    := False;
      FCarteira_Receb := 0;
      FPagRec         := 1; //Ambos
      FTipo           := 0; //Caixa
      FEmpresa        := Empresa;
      FTipoCarteira   := Integer(TipoCarteira);
    end;
    carBanco, carCC:
    begin
      FCodigo         := 0;
      FNome           := '';
      FBanco          := 0;
      FAgencia        := 0;
      FConta          := '';
      FAtivo          := True;
      FExigeCheque    := False;
      FCarteira_Receb := 0;
      FPagRec         := 1; //Ambos
      FTipo           := 1; //Banco
      FEmpresa        := Empresa;
      FTipoCarteira   := Integer(TipoCarteira);
    end;
    carDuplicata, carCheque, carAPagar:
    begin
      FCodigo         := 0;
      FNome           := '';
      FBanco          := 0;
      FAgencia        := 0;
      FConta          := '';
      FAtivo          := True;
      FExigeCheque    := TipoCarteira = carCheque;
      FCarteira_Receb := 0;
      FPagRec         := 0; //Pagar
      FTipo           := 2; //Emiss�o
      FEmpresa        := Empresa;
      FTipoCarteira   := Integer(TipoCarteira);
    end;
    carBoleto, carAReceber:
    begin
      FCodigo         := 0;
      FNome           := '';
      FBanco          := 0;
      FAgencia        := 0;
      FConta          := '';
      FAtivo          := True;
      FExigeCheque    := False;
      FCarteira_Receb := 0;
      FPagRec         := 2; //Receber
      FTipo           := 2; //Emiss�o
      FEmpresa        := Empresa;
      FTipoCarteira   := Integer(TipoCarteira);
    end;
    else
      Geral.MB_Erro('Tipo de carteira n�o implementada!' + sLineBreak +
        'Fun��o: TCarteiras.MostraEdicao');
  end;

  if Codigo <> 0 then
  begin
    Qry := TmySQLQuery.Create(TDataModule(FDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, FDB, [
        'SELECT * ',
        'FROM carteiras ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
      if Qry.RecordCount > 0 then
      begin
        FCodigo         := Qry.FieldByName('Codigo').AsInteger;;
        FNome           := Qry.FieldByName('Nome').AsString;
        FBanco          := Qry.FieldByName('Banco1').AsInteger;
        FAgencia        := Qry.FieldByName('Agencia1').AsInteger;
        FConta          := Qry.FieldByName('Conta1').AsString;
        FAtivo          := Geral.IntToBool(Qry.FieldByName('Ativo').AsInteger);
        FExigeCheque    := Geral.IntToBool(Qry.FieldByName('ExigeNumCheque').AsInteger);
        FCarteira_Receb := Qry.FieldByName('Banco').AsInteger;
        FPagRec         := Qry.FieldByName('PagRec').AsInteger;
        FTipo           := Qry.FieldByName('Tipo').AsInteger;
        FEmpresa        := Qry.FieldByName('ForneceI').AsInteger;
        FTipoCarteira   := Qry.FieldByName('TipCart').AsInteger;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

end.
