unit UnFinanceiro2Jan;

interface

uses System.Classes, Vcl.Controls, Vcl.Forms, AdvToolBar;

type
  TUnFinanceiro2Jan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    // J A N E L A S  -  O P � � E S
    procedure MostraFormOpcoes();
    // J A N E L A S  -  P L A N O   D E   C O N T A S
    procedure MostraFormCadastroPlanoCtas();
    procedure MostraFormCadastroDePlano(Codigo: Integer);
    procedure MostraFormCadastroDeConjunto(Codigo, Plano: Integer);
    procedure MostraFormCadastrodeGrupo(Codigo, Conjunto: Integer);
    procedure MostraFormCadastrodeSubGrupo(Codigo, Grupo: Integer);
    procedure MostraFormCadastrodeConta(Codigo, SubGrupo: Integer);
    // J A N E L A S  -  C A D A S T R O S
    procedure MostraFormCadastroCentroCusto(Codigo: Integer);
    procedure MostraFormCadastroCarteiras(Codigo: Integer);
    // J A N E L A S   -   G  E R E N C I A M E N  T O
    procedure MostraFormLctGer3(InOwner: TWincontrol; AdvToolBarPager: TAdvToolBarPager);
  end;

var
  Financeiro2Jan: TUnFinanceiro2Jan;

implementation

uses ModuleGeral, UnMyObjects, MyDBCheck, UnDmkEnums, PlanoCtas, PlanoCtas_Plano,
  PlanoCtas_Conjunto, PlanoCtas_Grupo, PlanoCtas_SubGrupo, PlanoCtas_Conta,
  CentroCusto2, Carteiras2, LctGer3, FinOpcoes, ModuleFin;

procedure TUnFinanceiro2Jan.MostraFormLctGer3(InOwner: TWincontrol;
  AdvToolBarPager: TAdvToolBarPager);
var
  Form: TForm;
begin
  Form := MyObjects.FormTDICria(TFmLctGer3, InOwner, AdvToolBarPager, True, True);
  (*
  if Codigo <> 0 then
    TFmClientes(Form).LocCod(Codigo, Codigo);
  *)
end;

procedure TUnFinanceiro2Jan.MostraFormCadastroCentroCusto(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCentroCusto2, FmCentroCusto2, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCentroCusto2.LocCod(Codigo, Codigo);
    FmCentroCusto2.ShowModal;
    FmCentroCusto2.Destroy;
  end;
end;

procedure TUnFinanceiro2Jan.MostraFormCadastroDeConjunto(Codigo, Plano: Integer);
begin
  if DBCheck.CriaFm(TFmPlanoCtas_Conjunto, FmPlanoCtas_Conjunto, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPlanoCtas_Conjunto.FCodigo := Codigo;
    FmPlanoCtas_Conjunto.FPlano := Plano;
    FmPlanoCtas_Conjunto.ShowModal;
    FmPlanoCtas_Conjunto.Destroy;
  end;
end;

procedure TUnFinanceiro2Jan.MostraFormCadastrodeConta(Codigo, SubGrupo: Integer);
begin
  if DBCheck.CriaFm(TFmPlanoCtas_Conta, FmPlanoCtas_Conta, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPlanoCtas_Conta.FCodigo := Codigo;
    FmPlanoCtas_Conta.FSubGrupo := SubGrupo;
    FmPlanoCtas_Conta.ShowModal;
    FmPlanoCtas_Conta.Destroy;
  end;
end;

procedure TUnFinanceiro2Jan.MostraFormCadastrodeGrupo(Codigo, Conjunto: Integer);
begin
  if DBCheck.CriaFm(TFmPlanoCtas_Grupo, FmPlanoCtas_Grupo, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPlanoCtas_Grupo.FCodigo := Codigo;
    FmPlanoCtas_Grupo.FConjunto := Conjunto;
    FmPlanoCtas_Grupo.ShowModal;
    FmPlanoCtas_Grupo.Destroy;
  end;
end;

procedure TUnFinanceiro2Jan.MostraFormCadastroDePlano(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPlanoCtas_Plano, FmPlanoCtas_Plano, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPlanoCtas_Plano.FCodigo := Codigo;
    FmPlanoCtas_Plano.ShowModal;
    FmPlanoCtas_Plano.Destroy;
  end;
end;

procedure TUnFinanceiro2Jan.MostraFormCadastrodeSubGrupo(Codigo, Grupo: Integer);
begin
  if DBCheck.CriaFm(TFmPlanoCtas_SubGrupo, FmPlanoCtas_SubGrupo, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPlanoCtas_SubGrupo.FCodigo := Codigo;
    FmPlanoCtas_SubGrupo.FGrupo := Grupo;
    FmPlanoCtas_SubGrupo.ShowModal;
    FmPlanoCtas_SubGrupo.Destroy;
  end;
end;

procedure TUnFinanceiro2Jan.MostraFormOpcoes();
begin
  if DmodG.SoUmaEmpresaLogada(True) then
  begin
    if DBCheck.CriaFm(TFmFinOpcoes, FmFinOpcoes, afmoNegarComAviso) then
    begin
      FmFinOpcoes.ShowModal;
      FmFinOpcoes.Destroy;
      //
      DModFin.ReopenFinOpcoes();
    end;
  end;
end;

procedure TUnFinanceiro2Jan.MostraFormCadastroPlanoCtas;
begin
  if DBCheck.CriaFm(TFmPlanoCtas, FmPlanoCtas, afmoNegarComAviso) then
  begin
    FmPlanoCtas.ShowModal;
    FmPlanoCtas.Destroy;
  end;
end;

procedure TUnFinanceiro2Jan.MostraFormCadastroCarteiras(Codigo: Integer);
begin
  if DmodG.SoUmaEmpresaLogada(True) then
  begin
    if DBCheck.CriaFm(TFmCarteiras2, FmCarteiras2, afmoNegarComAviso) then
    begin
      FmCarteiras2.ShowModal;
      FmCarteiras2.Destroy;
    end;
  end;
end;

end.
