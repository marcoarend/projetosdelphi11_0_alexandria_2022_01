unit Carteiras2;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, UnFinanceiro2Cad, Vcl.Menus, dmkDBLookupComboBox,
  dmkEditCB;

type
  TFmCarteiras2 = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    DBCheckBox1: TDBCheckBox;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasAtivo: TSmallintField;
    PMInclui: TPopupMenu;
    Caixacofre1: TMenuItem;
    Contabancria1: TMenuItem;
    Cartodecrdito1: TMenuItem;
    Duplicatasfaturas1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    Contaspagar1: TMenuItem;
    Contasreceber1: TMenuItem;
    Cheques1: TMenuItem;
    RGTipo: TRadioGroup;
    CBBanco: TdmkDBLookupComboBox;
    EdBanco: TdmkEditCB;
    LaBanco: TLabel;
    LaBanco1_1: TLabel;
    EdBanco1_1: TdmkEdit;
    EdAgencia1_1: TdmkEdit;
    LaAgencia1_1: TLabel;
    LaConta1_1: TLabel;
    EdConta1_1: TdmkEdit;
    Boletos1: TMenuItem;
    RGTipoCart: TRadioGroup;
    SBBanco: TSpeedButton;
    QrCarteirasTipCart: TSmallintField;
    CkAtivo: TCheckBox;
    CkExigeNumCheque: TCheckBox;
    QrBancos: TmySQLQuery;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    DsBancos: TDataSource;
    RGPagRec: TRadioGroup;
    N3: TMenuItem;
    LaForneceI: TLabel;
    EdForneceI: TdmkEditCB;
    CBForneceI: TdmkDBLookupComboBox;
    QrForneceI: TmySQLQuery;
    QrForneceICodigo: TIntegerField;
    QrForneceINOMEENTIDADE: TWideStringField;
    DsForneceI: TDataSource;
    N4: TMenuItem;
    Outrasemisses1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCarteirasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCarteirasBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Caixacofre1Click(Sender: TObject);
    procedure Contabancria1Click(Sender: TObject);
    procedure Cartodecrdito1Click(Sender: TObject);
    procedure Duplicatasfaturas1Click(Sender: TObject);
    procedure Cheques1Click(Sender: TObject);
    procedure Boletos1Click(Sender: TObject);
    procedure Contaspagar1Click(Sender: TObject);
    procedure Contasreceber1Click(Sender: TObject);
    procedure SBBancoClick(Sender: TObject);
    procedure Outrasemisses1Click(Sender: TObject);
  private
    FCarteira: TCarteiras;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(SQLType: TSQLType; TipoCarteira: TCrtTip = carCaixa;
              Codigo: Integer = 0);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCarteiras2: TFmCarteiras2;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCarteiras2.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCarteiras2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCarteirasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCarteiras2.DefParams;
begin
  VAR_GOTOTABELA := 'carteiras';
  VAR_GOTOMYSQLTABLE := QrCarteiras;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM carteiras');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQLx.Add('AND ForneceI=' + Geral.FF0(VAR_LIB_EMPRESA_SEL));
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'ForneceI='+ Geral.FF0(VAR_LIB_EMPRESA_SEL);
end;

procedure TFmCarteiras2.Duplicatasfaturas1Click(Sender: TObject);
begin
  MostraEdicao(stIns, carDuplicata);
end;

procedure TFmCarteiras2.Caixacofre1Click(Sender: TObject);
begin
  MostraEdicao(stIns, carCaixa);
end;

procedure TFmCarteiras2.Cartodecrdito1Click(Sender: TObject);
begin
  MostraEdicao(stIns, carCC);
end;

procedure TFmCarteiras2.Cheques1Click(Sender: TObject);
begin
  MostraEdicao(stIns, carCheque);
end;

procedure TFmCarteiras2.Contabancria1Click(Sender: TObject);
begin
  MostraEdicao(stIns, carBanco);
end;

procedure TFmCarteiras2.Contaspagar1Click(Sender: TObject);
begin
  MostraEdicao(stIns, carAPagar);
end;

procedure TFmCarteiras2.Contasreceber1Click(Sender: TObject);
begin
  MostraEdicao(stIns, carAReceber);
end;

procedure TFmCarteiras2.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCarteiras2.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCarteiras2.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCarteiras2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCarteiras2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCarteiras2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCarteiras2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCarteiras2.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCarteiras2.Boletos1Click(Sender: TObject);
begin
  MostraEdicao(stIns, carBoleto);
end;

procedure TFmCarteiras2.BtAlteraClick(Sender: TObject);
begin
  if (QrCarteiras.State <> dsInactive) and (QrCarteiras.RecordCount > 0) then
    MostraEdicao(stUpd, TCrtTip(QrCarteirasTipCart.Value), QrCarteirasCodigo.Value);
end;

procedure TFmCarteiras2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCarteirasCodigo.Value;
  Close;
end;

procedure TFmCarteiras2.BtConfirmaClick(Sender: TObject);
var
  Codigo, Banco, Agencia, Carteira_Receb, PagRec, Tipo, Empresa,
  TipoCarteira: Integer;
  Nome, Conta: String;
  Ativo, ExigeCheque: Boolean;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Banco          := EdBanco1_1.ValueVariant;
  Agencia        := EdAgencia1_1.ValueVariant;
  Conta          := EdConta1_1.ValueVariant;
  Ativo          := CkAtivo.Checked;
  ExigeCheque    := CkExigeNumCheque.Checked;
  PagRec         := RGPagRec.ItemIndex;
  Carteira_Receb := EdBanco.ValueVariant;
  Tipo           := RGTipo.ItemIndex;
  Empresa        := EdForneceI.ValueVariant;
  TipoCarteira   := RGTipoCart.ItemIndex;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := FCarteira.InsUpd(Nome, Banco, Agencia, Conta, Carteira_Receb, PagRec,
              Ativo, ExigeCheque, Tipo, Empresa, TCrtTip(TipoCarteira), Codigo);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    MostraEdicao(stLok);
  end;
end;

procedure TFmCarteiras2.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(stLok);
end;

procedure TFmCarteiras2.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmCarteiras2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBDados.Align   := alClient;
  CriaOForm;
  //
  FCarteira := TCarteiras.Create(Dmod.MyDB);
end;

procedure TFmCarteiras2.FormDestroy(Sender: TObject);
begin
  FCarteira.Free;
end;

procedure TFmCarteiras2.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCarteirasCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCarteiras2.SBBancoClick(Sender: TObject);
begin
  Geral.MB_Info('Est� cateira serve para informar onde a fatura ' +
    'ir� compensar por padr�o!' + sLineBreak + 'Exemplo: Para a carteira: ' +
    '"Cheque do Banco X" a carteira padr�o seria: "Banco X".');
end;

procedure TFmCarteiras2.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCarteiras2.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCarteirasCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCarteiras2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCarteiras2.QrCarteirasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCarteiras2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCarteiras2.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCarteirasCodigo.Value, CuringaLoc.CriaForm(CO_CODIGO, CO_NOME,
    'carteiras', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCarteiras2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCarteiras2.QrCarteirasBeforeOpen(DataSet: TDataSet);
begin
  QrCarteirasCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCarteiras2.MostraEdicao(SQLType: TSQLType;
  TipoCarteira: TCrtTip = carCaixa; Codigo: Integer = 0);
var
  Empresa: Integer;
begin
  if (SQLType = stIns) or (SQLType = stUpd) then
  begin
    Empresa := DModG.QrFiliLogCodigo.Value;
    //
    FCarteira.ReopenCarteira_Receb(Dmod.MyDB, QrBancos, DsBancos, EdBanco, CBBanco, Empresa);
    FCarteira.ReopenForneceI(Dmod.MyDB, QrForneceI, DsForneceI, EdForneceI, CBForneceI);
    FCarteira.MostraEdicao(TipoCarteira, Empresa, Codigo);
    //
    EdCodigo.ValueVariant     := FCarteira.Codigo;
    EdNome.ValueVariant       := FCarteira.Nome;
    EdBanco1_1.ValueVariant   := FCarteira.Banco;
    EdAgencia1_1.ValueVariant := FCarteira.Agencia;
    EdConta1_1.ValueVariant   := FCarteira.Conta;
    EdBanco.ValueVariant      := FCarteira.Carteira_Receb;
    CBBanco.KeyValue          := FCarteira.Carteira_Receb;
    RGPagRec.ItemIndex        := FCarteira.PagRec;
    CkAtivo.Checked           := FCarteira.Ativo;
    CkExigeNumCheque.Checked  := FCarteira.ExigeCheque;
    RGTipo.ItemIndex          := FCarteira.Tipo;
    EdForneceI.ValueVariant   := FCarteira.Empresa;
    CBForneceI.KeyValue       := FCarteira.Empresa;
    RGTipoCart.ItemIndex      := FCarteira.TipoCarteira;
    //
    GBEdita.Visible := True;
    //
    case TipoCarteira of
      carCaixa:
      begin
        LaBanco1_1.Visible       := False;
        EdBanco1_1.Visible       := False;
        LaAgencia1_1.Visible     := False;
        EdAgencia1_1.Visible     := False;
        LaConta1_1.Visible       := False;
        EdConta1_1.Visible       := False;
        CkExigeNumCheque.Visible := False;
        LaBanco.Visible          := False;
        EdBanco.Visible          := False;
        CBBanco.Visible          := False;
        SBBanco.Visible          := False;
        RGPagRec.Enabled         := False;
        LaForneceI.Enabled       := False;
        EdForneceI.Enabled       := False;
        CBForneceI.Enabled       := False;
        RGTipo.Visible           := False;
        RGTipoCart.Visible       := False;
      end;
      carBanco, carCC:
      begin
        LaBanco1_1.Visible       := TipoCarteira = carBanco;
        EdBanco1_1.Visible       := TipoCarteira = carBanco;
        LaAgencia1_1.Visible     := TipoCarteira = carBanco;
        EdAgencia1_1.Visible     := TipoCarteira = carBanco;
        LaConta1_1.Visible       := TipoCarteira = carBanco;
        EdConta1_1.Visible       := TipoCarteira = carBanco;
        CkExigeNumCheque.Visible := False;
        LaBanco.Visible          := False;
        EdBanco.Visible          := False;
        CBBanco.Visible          := False;
        SBBanco.Visible          := False;
        RGPagRec.Enabled         := False;
        LaForneceI.Enabled       := False;
        EdForneceI.Enabled       := False;
        CBForneceI.Enabled       := False;
        RGTipo.Visible           := False;
        RGTipoCart.Visible       := False;
      end;
      carDuplicata, carCheque, carAPagar:
      begin
        LaBanco1_1.Visible       := False;
        EdBanco1_1.Visible       := False;
        LaAgencia1_1.Visible     := False;
        EdAgencia1_1.Visible     := False;
        LaConta1_1.Visible       := False;
        EdConta1_1.Visible       := False;
        CkExigeNumCheque.Visible := TipoCarteira = carCheque;
        LaBanco.Visible          := True;
        EdBanco.Visible          := True;
        CBBanco.Visible          := True;
        SBBanco.Visible          := True;
        RGPagRec.Enabled         := False;
        LaForneceI.Enabled       := False;
        EdForneceI.Enabled       := False;
        CBForneceI.Enabled       := False;
        RGTipo.Visible           := False;
        RGTipoCart.Visible       := False;
      end;
      carBoleto, carAReceber:
      begin
        LaBanco1_1.Visible       := False;
        EdBanco1_1.Visible       := False;
        LaAgencia1_1.Visible     := False;
        EdAgencia1_1.Visible     := False;
        LaConta1_1.Visible       := False;
        EdConta1_1.Visible       := False;
        CkExigeNumCheque.Visible := False;
        LaBanco.Visible          := True;
        EdBanco.Visible          := True;
        CBBanco.Visible          := True;
        SBBanco.Visible          := True;
        RGPagRec.Enabled         := False;
        LaForneceI.Enabled       := False;
        EdForneceI.Enabled       := False;
        CBForneceI.Enabled       := False;
        RGTipo.Visible           := False;
        RGTipoCart.Visible       := False;
      end;
      carEmisOutros:
      begin
        LaBanco1_1.Visible       := False;
        EdBanco1_1.Visible       := False;
        LaAgencia1_1.Visible     := False;
        EdAgencia1_1.Visible     := False;
        LaConta1_1.Visible       := False;
        EdConta1_1.Visible       := False;
        CkExigeNumCheque.Visible := False;
        LaBanco.Visible          := True;
        EdBanco.Visible          := True;
        CBBanco.Visible          := True;
        SBBanco.Visible          := True;
        RGPagRec.Enabled         := True;
        LaForneceI.Enabled       := False;
        EdForneceI.Enabled       := False;
        CBForneceI.Enabled       := False;
        RGTipo.Visible           := False;
        RGTipoCart.Visible       := False;
      end
      else
      begin
        GBEdita.Visible := False;
        //
        Geral.MB_Erro('Tipo de carteira n�o implementada!' + sLineBreak +
          'Fun��o: TFmCarteiras2.MostraEdicao');
      end;
    end;
    //
    PnEdita.Visible := True;
    PnDados.Visible := False;
    //
    EdNome.SetFocus;
  end else
  begin
    PnDados.Visible := True;
    PnEdita.Visible := False;
  end;
  //
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCarteiras2.Outrasemisses1Click(Sender: TObject);
begin
  MostraEdicao(stIns, carEmisOutros);
end;

end.

