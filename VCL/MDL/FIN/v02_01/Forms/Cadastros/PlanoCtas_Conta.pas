unit PlanoCtas_Conta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkCompoStore, dmkDBLookupComboBox, dmkEditCB, UnFinanceiro2Cad, dmkRadioGroup;

type
  TFmPlanoCtas_Conta = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    CkAtivo: TCheckBox;
    CkMensal: TCheckBox;
    SpeedButton1: TSpeedButton;
    RGTipo: TdmkRadioGroup;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    FConta: TConta;
    procedure MostraEdicao();
  public
    { Public declarations }
    FCodigo, FSubGrupo: Integer;
  end;

  var
  FmPlanoCtas_Conta: TFmPlanoCtas_Conta;

implementation

uses MyGlyfs, Principal, Module, UnMyObjects;

{$R *.DFM}

procedure TFmPlanoCtas_Conta.BtOKClick(Sender: TObject);
var
  ContaTipo: TFinContaTipo;
  Codigo, SubGrupo: Integer;
  Nome: String;
  Ativo, Mensal: Boolean;
begin
  Codigo    := EdCodigo.ValueVariant;
  Nome      := EdNome.ValueVariant;
  SubGrupo  := FSubGrupo;
  Mensal    := CkMensal.Checked;
  Ativo     := CkAtivo.Checked;
  ContaTipo := TFinContaTipo(RGTipo.ItemIndex);
  //
  if RGTipo.ItemIndex <= 0 then //Nenhum
  begin
    Geral.MB_Aviso('Informe o tipo de conta!');
    RGTipo.SetFocus;
    Exit;
  end;
  //
  VAR_CADASTRO := FConta.InsUpd(Nome, Ativo, SubGrupo, ContaTipo, Mensal, Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    if (CkContinuar.Visible = True) and (CkContinuar.Checked = True) then
      MostraEdicao()
    else
      Close;
  end;
end;

procedure TFmPlanoCtas_Conta.BtSaidaClick(Sender: TObject);
begin
  if TFmPlanoCtas_Conta(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmPlanoCtas_Conta.FormActivate(Sender: TObject);
begin
  if TFmPlanoCtas_Conta(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmPlanoCtas_Conta.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType     := stLok;
  FCodigo             := 0;
  VAR_CADASTRO        := 0;
  CkContinuar.Checked := False;
  //
  FConta := TConta.Create(Dmod.MyDB);
end;

procedure TFmPlanoCtas_Conta.FormDestroy(Sender: TObject);
begin
  FConta.Free;
end;

procedure TFmPlanoCtas_Conta.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPlanoCtas_Conta.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  MostraEdicao();
end;

procedure TFmPlanoCtas_Conta.MostraEdicao;
begin
  FConta.MostraEdicao(FCodigo, FSubGrupo);
  //
  if FCodigo <> 0 then
  begin
    ImgTipo.SQLType     := stUpd;
    CkContinuar.Checked := False;
    CkContinuar.Visible := False;
  end else
  begin
    ImgTipo.SQLType     := stIns;
    CkContinuar.Visible := True;
  end;
  //
  RGTipo.ItemIndex      := Integer(FConta.ContaTipo);
  EdCodigo.ValueVariant := FConta.Codigo;
  EdNome.ValueVariant   := FConta.Nome;
  FSubGrupo             := FConta.SubGrupo;
  CkMensal.Checked      := FConta.Mensal;
  CkAtivo.Checked       := FConta.Ativo;
  //
  EdNome.SetFocus;
end;

procedure TFmPlanoCtas_Conta.SpeedButton1Click(Sender: TObject);
begin
  Geral.MB_Info('Ao marcar o campo "Mensal" voc� dever� informar o m�s de compet�ncia!'
    + sLineBreak + 'Este campo dever� ser marcado para contas que sejam mensais.'
    + sLineBreak + sLineBreak + 'Exemplo: �gua, Luz, Telefone.');
end;

end.
