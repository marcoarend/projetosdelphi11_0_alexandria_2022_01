unit PlanoCtas_Grupo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkCompoStore, dmkDBLookupComboBox, dmkEditCB, UnFinanceiro2Cad;

type
  TFmPlanoCtas_Grupo = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    CkAtivo: TCheckBox;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FGrupo: TGrupo;
    procedure MostraEdicao();
  public
    { Public declarations }
    FCodigo, FConjunto: Integer;
  end;

  var
  FmPlanoCtas_Grupo: TFmPlanoCtas_Grupo;

implementation

uses MyGlyfs, Principal, Module, UnMyObjects;

{$R *.DFM}

procedure TFmPlanoCtas_Grupo.BtOKClick(Sender: TObject);
var
  Codigo, Conjunto: Integer;
  Nome: String;
  Ativo: Boolean;
begin
  Codigo   := EdCodigo.ValueVariant;
  Nome     := EdNome.ValueVariant;
  Conjunto := FConjunto;
  Ativo    := CkAtivo.Checked;
  //
  VAR_CADASTRO := FGrupo.InsUpd(Nome, Ativo, Conjunto, Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    if (CkContinuar.Visible = True) and (CkContinuar.Checked = True) then
      MostraEdicao()
    else
      Close;
  end;
end;

procedure TFmPlanoCtas_Grupo.BtSaidaClick(Sender: TObject);
begin
  if TFmPlanoCtas_Grupo(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmPlanoCtas_Grupo.FormActivate(Sender: TObject);
begin
  if TFmPlanoCtas_Grupo(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmPlanoCtas_Grupo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType     := stLok;
  FCodigo             := 0;
  VAR_CADASTRO        := 0;
  CkContinuar.Checked := False;
  //
  FGrupo := TGrupo.Create(Dmod.MyDB);
end;

procedure TFmPlanoCtas_Grupo.FormDestroy(Sender: TObject);
begin
  FGrupo.Free;
end;

procedure TFmPlanoCtas_Grupo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPlanoCtas_Grupo.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  MostraEdicao();
end;

procedure TFmPlanoCtas_Grupo.MostraEdicao;
begin
  FGrupo.MostraEdicao(FCodigo, FConjunto);
  //
  if FCodigo <> 0 then
  begin
    ImgTipo.SQLType     := stUpd;
    CkContinuar.Checked := False;
    CkContinuar.Visible := False;
  end else
  begin
    ImgTipo.SQLType     := stIns;
    CkContinuar.Visible := True;
  end;
  //
  EdCodigo.ValueVariant := FGrupo.Codigo;
  EdNome.ValueVariant   := FGrupo.Nome;
  FConjunto             := FGrupo.Conjunto;
  CkAtivo.Checked       := FGrupo.Ativo;
  //
  EdNome.SetFocus;
end;

end.
