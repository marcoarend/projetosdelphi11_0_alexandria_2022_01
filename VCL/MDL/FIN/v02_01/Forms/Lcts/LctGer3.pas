unit LctGer3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkCompoStore, dmkEditCB, dmkDBLookupComboBox;

type
  TFmLctGer3 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel5: TPanel;
    BGPeriodo: TGroupBox;
    Label32: TLabel;
    LaAnoI: TLabel;
    CBMesI: TComboBox;
    CBAnoI: TComboBox;
    CBCarteira: TdmkDBLookupComboBox;
    EdCarteira: TdmkEditCB;
    LaCarteira: TLabel;
    Label60: TLabel;
    EdCentroCusto: TdmkEditCB;
    CBCentroCusto: TdmkDBLookupComboBox;
    GBAcoes: TGroupBox;
    BtInclui: TBitBtn;
    BtDuplica: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtQuita: TBitBtn;
    BtConcilia: TBitBtn;
    BtImprime: TBitBtn;
    LWItem: TListView;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmLctGer3: TFmLctGer3;

implementation

uses MyGlyfs, Principal, UnMyObjects;

{$R *.DFM}

procedure TFmLctGer3.BtSaidaClick(Sender: TObject);
begin
  if TFmLctGer3(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmLctGer3.FormActivate(Sender: TObject);
begin
  if TFmLctGer3(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmLctGer3.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmLctGer3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctGer3.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

end.
