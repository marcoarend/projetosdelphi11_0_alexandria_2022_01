unit PlanoCtas_SubGrupo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkCompoStore, dmkDBLookupComboBox, dmkEditCB, UnFinanceiro2Cad;

type
  TFmPlanoCtas_SubGrupo = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    CkAtivo: TCheckBox;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FSubGrupo: TSubGrupo;
    procedure MostraEdicao();
  public
    { Public declarations }
    FCodigo, FGrupo: Integer;
  end;

  var
  FmPlanoCtas_SubGrupo: TFmPlanoCtas_SubGrupo;

implementation

uses MyGlyfs, Principal, Module, UnMyObjects;

{$R *.DFM}

procedure TFmPlanoCtas_SubGrupo.BtOKClick(Sender: TObject);
var
  Codigo, Grupo: Integer;
  Nome: String;
  Ativo: Boolean;
begin
  Codigo := EdCodigo.ValueVariant;
  Nome   := EdNome.ValueVariant;
  Grupo  := FGrupo;
  Ativo  := CkAtivo.Checked;
  //
  VAR_CADASTRO := FSubGrupo.InsUpd(Nome, Ativo, Grupo, Codigo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    if (CkContinuar.Visible = True) and (CkContinuar.Checked = True) then
      MostraEdicao()
    else
      Close;
  end;
end;

procedure TFmPlanoCtas_SubGrupo.BtSaidaClick(Sender: TObject);
begin
  if TFmPlanoCtas_SubGrupo(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmPlanoCtas_SubGrupo.FormActivate(Sender: TObject);
begin
  if TFmPlanoCtas_SubGrupo(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmPlanoCtas_SubGrupo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType     := stLok;
  FCodigo             := 0;
  VAR_CADASTRO        := 0;
  CkContinuar.Checked := False;
  //
  FSubGrupo := TSubGrupo.Create(Dmod.MyDB);
end;

procedure TFmPlanoCtas_SubGrupo.FormDestroy(Sender: TObject);
begin
  FSubGrupo.Free;
end;

procedure TFmPlanoCtas_SubGrupo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPlanoCtas_SubGrupo.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  MostraEdicao();
end;

procedure TFmPlanoCtas_SubGrupo.MostraEdicao;
begin
  FSubGrupo.MostraEdicao(FCodigo, FGrupo);
  //
  if FCodigo <> 0 then
  begin
    ImgTipo.SQLType     := stUpd;
    CkContinuar.Checked := False;
    CkContinuar.Visible := False;
  end else
  begin
    ImgTipo.SQLType     := stIns;
    CkContinuar.Visible := True;
  end;
  //
  EdCodigo.ValueVariant := FSubGrupo.Codigo;
  EdNome.ValueVariant   := FSubGrupo.Nome;
  FGrupo                := FSubGrupo.Grupo;
  CkAtivo.Checked       := FSubGrupo.Ativo;
  //
  EdNome.SetFocus;
end;

end.
