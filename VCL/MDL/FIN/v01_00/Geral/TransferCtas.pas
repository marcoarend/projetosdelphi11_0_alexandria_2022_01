unit TransferCtas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs, UnGOTOy,
  ZCF2, ResIntStrings, UnMLAGeral, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UnMySQLCuringa, ComCtrls, (*DBIProcs,*) UMySQLModule,
  mySQLDbTables, dmkEdit, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB,
  dmkPermissoes, dmkGeral;

type
  TFmTransferCtas = class(TForm)
    PainelControle: TPanel;
    PainelDados: TPanel;
    BtExclui: TBitBtn;
    QrOrigem: TmySQLQuery;
    DsOrigem: TDataSource;
    DsDestino: TDataSource;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    QrOrigemCodigo: TIntegerField;
    QrOrigemNome: TWideStringField;
    QrDestino: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    QrOrigemTipo: TIntegerField;
    QrDestinoTipo: TIntegerField;
    Panel1: TPanel;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    Label8: TLabel;
    Label7: TLabel;
    EdValor: TdmkEdit;
    GBDebt: TGroupBox;
    EdOrigem: TdmkEditCB;
    EdDocDeb: TdmkEdit;
    Label5: TLabel;
    Label2: TLabel;
    EdSerieChDeb: TdmkEdit;
    CBOrigem: TdmkDBLookupComboBox;
    LaConjunto: TLabel;
    GBCred: TGroupBox;
    Label4: TLabel;
    EdDestino: TdmkEditCB;
    CBDestino: TdmkDBLookupComboBox;
    EdSerieCHCred: TdmkEdit;
    Label3: TLabel;
    Label6: TLabel;
    EdDocCred: TdmkEdit;
    Label9: TLabel;
    EdCtaDebt: TdmkEditCB;
    CBCtaDebt: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdCtaCred: TdmkEditCB;
    CBCtaCred: TdmkDBLookupComboBox;
    QrCtaDebt: TmySQLQuery;
    DsCtaDebt: TDataSource;
    QrCtaDebtCodigo: TIntegerField;
    QrCtaDebtNome: TWideStringField;
    QrCtaCred: TmySQLQuery;
    DsCtaCred: TDataSource;
    QrCtaCredCodigo: TIntegerField;
    QrCtaCredNome: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EdOldControle1: TdmkEdit;
    TPOldData1: TdmkEditDateTimePicker;
    EdOldSub1: TdmkEdit;
    EdOldTipo1: TdmkEdit;
    EdOldCarteira1: TdmkEdit;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdOldControle2: TdmkEdit;
    TPOldData2: TdmkEditDateTimePicker;
    EdOldSub2: TdmkEdit;
    EdOldTipo2: TdmkEdit;
    EdOldCarteira2: TdmkEdit;
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    F_CliInt: Integer;
  end;

var
  FmTransferCtas: TFmTransferCtas;

implementation
  uses UnMyObjects, Module, Principal, UnFinanceiro, ModuleFin;

{$R *.DFM}

procedure TFmTransferCtas.BtConfirmaClick(Sender: TObject);
var
  OldData1, Olddata2: TDate;
  OldTipo1, OldCarteira1, OldControle1, OldSub1,
  OldTipo2, OldCarteira2, OldControle2, OldSub2,
  Carteira, Tipo, Genero: Integer;
  Debito, Credito, Documento: Double;
  SerieCH, Vencimento: String;
  //
  Codigo, Origem, Destino, Sit, CtaDebt, CtaCred: Integer;
  Localiza: Boolean;
  Controle: Int64;
  Valor: Double;
  Data: String;
begin
  Data := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  UFinanceiro.AtzDataAtzSdoPlaCta(F_CliInt, TPData.Date);
  Codigo := Geral.IMV(EdCodigo.Text);
  if (Codigo = 0) and (LaTipo.Caption = CO_ALTERACAO) then
  begin
    Application.MessageBox('Altera��o imposs�vel sem o n� do lan�amento!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;

  //  CARTEIRAS
  Origem := EdOrigem.ValueVariant;
  if Origem = 0 then
  begin
    Application.MessageBox('Defina a carteira de origem!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    EdOrigem.SetFocus;
    Exit;
  end;
  Destino := EdDestino.ValueVariant;
  if Destino = 0 then
  begin
    Application.MessageBox('Defina a carteira de destino!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    EdDestino.SetFocus;
    Exit;
  end;
  {
  if Origem = Destino then
  begin
    Application.MessageBox('As carteiras de origem e destino n�o podem ser as mesmas!',
      'Aviso', MB_OK+MB_ICONWARNING);
    EdDestino.SetFocus;
    Exit;
  end;
  }
  //  CONTAS
  CtaDebt := EdCtaDebt.ValueVariant;
  if CtaDebt = 0 then
  begin
    Application.MessageBox('Defina a conta para d�bito!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    EdCtaDebt.SetFocus;
    Exit;
  end;
  CtaCred := EdCtaCred.ValueVariant;
  if CtaCred = 0 then
  begin
    Application.MessageBox('Defina a conta para cr�dito!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    EdCtaCred.SetFocus;
    Exit;
  end;
  if CtaDebt = CtaCred then
  begin
    Application.MessageBox('Aa contas de d�bito e cr�dito n�o podem ser as mesmas!',
      'Aviso', MB_OK+MB_ICONWARNING);
    EdCtaCred.SetFocus;
    Exit;
  end;


  Valor := Geral.DMV(EdValor.Text);
  if Valor <= 0 then
  begin
    Application.MessageBox('Defina um valor positivo, maior que zero.',
    'Aviso', MB_OK+MB_ICONWARNING);
    EdValor.SetFocus;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  if LaTipo.Caption = CO_ALTERACAO then
  begin
    OldData1     := TPOldData1.Date;
    OldTipo1     := EdOldTipo1.ValueVariant;
    OldCarteira1 := EdOldCarteira1.ValueVariant;
    OldControle1 := EdOldControle1.ValueVariant;
    OldSub1      := EdOldSub1.ValueVariant;
    Debito       := EdValor.ValueVariant;
    Credito      := 0;
    SerieCH      := EdSerieChDeb.Text;
    Documento    := EdDocDeb.ValueVariant;
    Vencimento   := Data;
    Carteira     := EdOrigem.ValueVariant;
    Tipo         := QrOrigemTipo.Value;
    Genero       := EdCtaDebt.ValueVariant;
    //
    UMyMod.LctUpd(Dmod.QrUpd, VAR_LCT, [
    'Genero', 'Carteira', 'Tipo',
    'Debito', 'Credito', 'SerieCH',
    'Documento', 'Vencimento', 'Data'], [
    Genero, Carteira, Tipo,
    Debito, Credito, SerieCH,
    Documento, Vencimento, Data],
    OldData1, OldTipo1, OldCarteira1, OldControle1, OldSub1);
    //
    OldData2     := TPOldData2.Date;
    OldTipo2     := EdOldTipo2.ValueVariant;
    OldCarteira2 := EdOldCarteira2.ValueVariant;
    OldControle2 := EdOldControle2.ValueVariant;
    OldSub2      := EdOldSub2.ValueVariant;
    Debito       := 0;
    Credito      := EdValor.ValueVariant;
    SerieCH      := EdSerieCHCred.Text;
    Documento    := EdDocCred.ValueVariant;
    Vencimento   := Data;
    Carteira     := EdDestino.ValueVariant;
    Tipo         := QrDestinoTipo.Value;
    Genero       := EdCtaCred.ValueVariant;
    //
    UMyMod.LctUpd(Dmod.QrUpd, VAR_LCT, [
    'Genero', 'Carteira', 'Tipo',
    'Debito', 'Credito', 'SerieCH',
    'Documento', 'Vencimento', 'Data'], [
    Genero, Carteira, Tipo,
    Debito, Credito, SerieCH,
    Documento, Vencimento, Data],
    OldData2, OldTipo2, OldCarteira2, OldControle2, OldSub2);
    //
    DmodFin.QrTrfCtas.First;
    while not DmodFin.QrTrfCtas.Eof do
    begin
      if (DmodFin.QrTrfCtasTipo.Value = Dmod.QrCarteirasTipo.Value)
      and (DmodFin.QrTrfCtasCarteira.Value = Dmod.QrCarteirasCodigo.Value)
      then Localiza := True else Localiza := False;
      UFinanceiro.RecalcSaldoCarteira(DmodFin.QrTrfCtasCarteira.Value,
        Dmod.QrCarteiras, Localiza, Localiza);
      DmodFin.QrTrfCtas.Next;
      //dbiSaveChanges(Dmod.QrUpdU.Handle);
    end;
    //Controle := Codigo;
  end else
  begin
    Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle', VAR_LCT, VAR_LCT, 'Controle');
    //
    VAR_LANCTO2 := Controle;
    if QrOrigemTipo.Value = 2 then Sit := 0 else Sit := 3;

    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO ' + VAR_LCT + ' SET AlterWeb=1, Controle=:P0, ');
    Dmod.QrUpdU.SQL.Add('Documento=:P1, Data=:P2, Carteira=:P3, Tipo=:P4, ');
    Dmod.QrUpdU.SQL.Add('Credito=:P5, Debito=:P6, Genero=:P7, Descricao=:P8, ');
    Dmod.QrUpdU.SQL.Add('Vencimento=:P9, Sit=:P10, Sub=:P11, Compensado=:P12, ');
    Dmod.QrUpdU.SQL.Add('SerieCH=:P13, FatID=:P14, FatNum=:P15, CliInt=:P16, ');
    Dmod.QrUpdU.SQL.Add('FatID_Sub=:P17, DataCad=:Pa, UserCad=:Pb');
    Dmod.QrUpdU.Params[00].AsFloat   := Controle;
    Dmod.QrUpdU.Params[01].AsFloat   := Geral.DMV(EdDocDeb.Text);
    Dmod.QrUpdU.Params[02].AsString  := Data;
    Dmod.QrUpdU.Params[03].AsInteger := Origem;
    Dmod.QrUpdU.Params[04].AsInteger := QrOrigemTipo.Value;
    Dmod.QrUpdU.Params[05].AsFloat   := 0;
    Dmod.QrUpdU.Params[06].AsFloat   := Valor;
    Dmod.QrUpdU.Params[07].AsInteger := CtaDebt;
    Dmod.QrUpdU.Params[08].AsString  := FIN_MSG_TRANSFERENCIA + FIN_MSG_PARA + CBCtaCred.Text;
    Dmod.QrUpdU.Params[09].AsString  := Data;
    Dmod.QrUpdU.Params[10].AsInteger := Sit;
    Dmod.QrUpdU.Params[11].AsInteger := 1;
    Dmod.QrUpdU.Params[12].AsString  := Data;
    Dmod.QrUpdU.Params[13].AsString  := EdSerieChDeb.Text;
    Dmod.QrUpdU.Params[14].AsInteger := -1;
    Dmod.QrUpdU.Params[15].AsInteger := 0;
    Dmod.QrUpdU.Params[16].AsInteger := F_CliInt;
    Dmod.QrUpdU.Params[17].AsInteger := 0;
    //
    Dmod.QrUpdU.Params[18].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[19].AsInteger := VAR_USUARIO;
    Dmod.QrUpdU.ExecSQL;
    //
    if QrDestinoTipo.Value = 2 then Sit := 0 else Sit := 3;
    Dmod.QrUpdU.Params[00].AsFloat   := Controle;
    Dmod.QrUpdU.Params[01].AsFloat   := Geral.DMV(EdDocCred.Text);
    Dmod.QrUpdU.Params[02].AsString  := Data;
    Dmod.QrUpdU.Params[03].AsInteger := Destino;
    Dmod.QrUpdU.Params[04].AsInteger := QrDestinoTipo.Value;
    Dmod.QrUpdU.Params[05].AsFloat   := Valor;
    Dmod.QrUpdU.Params[06].AsFloat   := 0;
    Dmod.QrUpdU.Params[07].AsInteger := CtaCred;
    Dmod.QrUpdU.Params[08].AsString  := FIN_MSG_TRANSFERENCIA + FIN_MSG_DE + CBCtaDebt.Text;
    Dmod.QrUpdU.Params[09].AsString  := Data;
    Dmod.QrUpdU.Params[10].AsInteger := Sit;
    Dmod.QrUpdU.Params[11].AsInteger := 2;
    Dmod.QrUpdU.Params[12].AsString  := Data;
    Dmod.QrUpdU.Params[13].AsString  := EdSerieChCred.Text;
    Dmod.QrUpdU.Params[14].AsInteger := -1;
    Dmod.QrUpdU.Params[15].AsInteger := 0;
    Dmod.QrUpdU.Params[16].AsInteger := F_CliInt;
    Dmod.QrUpdU.Params[17].AsInteger := 0;
    //
    Dmod.QrUpdU.Params[18].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[19].AsInteger := VAR_USUARIO;
    Dmod.QrUpdU.ExecSQL;
  end;
  //
  UMyMod.LogDel(Dmod.MyDB, 3, Codigo);
  if (QrOrigemTipo.Value = Dmod.QrCarteirasTipo.Value)
  and (CBOrigem.KeyValue = Dmod.QrCarteirasCodigo.Value)
  then Localiza := True else Localiza := True;
  UFinanceiro.RecalcSaldoCarteira(CBOrigem.KeyValue,
    QrOrigem, Localiza, Localiza);
  //
  if (QrDestinoTipo.Value = Dmod.QrCarteirasTipo.Value)
  and (CBDestino.KeyValue = Dmod.QrCarteirasCodigo.Value)
  then Localiza := True else Localiza := True;
  UFinanceiro.RecalcSaldoCarteira(CBDestino.KeyValue,
    QrDestino, Localiza, Localiza);
  //
  VAR_DATAINSERIR := TPData.Date;
  Screen.Cursor := crDefault;
  Close;
end;

procedure TFmTransferCtas.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTransferCtas.SbNumeroClick(Sender: TObject);
begin
//  LaRegistro.Text := GOTOy.Codigo(QrGruposCodigo.Value, LaRegistro.Text);
end;

procedure TFmTransferCtas.SbNomeClick(Sender: TObject);
begin
//  LaRegistro.Text := GOTOy.Nome(LaRegistro.Text);
end;

procedure TFmTransferCtas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdValor.SetFocus;
end;

procedure TFmTransferCtas.FormCreate(Sender: TObject);
begin
  QrCtaDebt.Close;
  QrCtaDebt.Open;
  //
  QrCtaCred.Close;
  QrCtaCred.Open;
end;

procedure TFmTransferCtas.SbQueryClick(Sender: TObject);
begin
//  GOTOy.LocalizaCodigo(QrGruposCodigo.Value,
//  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, CO_Grupos, Dmod.MyDB));
end;

procedure TFmTransferCtas.BtExcluiClick(Sender: TObject);
var
  Localiza: Boolean;
begin
  UFinanceiro.AtzDataAtzSdoPlaCta(F_CliInt, DmodFin.QrTrfCtasData.Value);
  if Application.MessageBox('Confirma a exclus�o desta transfer�ncia?',
  PChar(VAR_APPNAME), MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
  MB_APPLMODAL) = ID_YES then
  begin
    VAR_LANCTO2 := Dmod.QrLctControle.Value;
    with FmPrincipal do
    begin
      UFinanceiro.ExcluiLct_Unico(True, QrOrigem.Database, TPOldData1.Date,
        EdOldTipo1.ValueVariant, EdOldCarteira1.ValueVariant,
        EdOldControle1.ValueVariant, EdOldSub1.ValueVAriant, False);
      UFinanceiro.ExcluiLct_Unico(True, QrOrigem.Database, TPOldData2.Date,
        EdOldTipo2.ValueVariant, EdOldCarteira2.ValueVariant,
        EdOldControle2.ValueVariant, EdOldSub2.ValueVAriant, False);
      //
      DmodFin.QrTrfCtas.First;
      while not DmodFin.QrTrfCtas.Eof do
      begin
        if (DmodFin.QrTrfCtasTipo.Value = Dmod.QrCarteirasTipo.Value)
        and (DmodFin.QrTrfCtasCarteira.Value = Dmod.QrCarteirasCodigo.Value)
        then Localiza := True else Localiza := False;
        UFinanceiro.RecalcSaldoCarteira(DmodFin.QrTrfCtasCarteira.Value,
          nil, Localiza, Localiza);
        DmodFin.QrTrfCtas.Next;
      end;
    end;
    Close;
  end;
end;

procedure TFmTransferCtas.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.

