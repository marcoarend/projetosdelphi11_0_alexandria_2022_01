unit SelfGer2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkDBGrid, ComCtrls, DBCtrls, Grids,
  DBGrids, Menus, Db, mySQLDbTables, dmkPermissoes, dmkGeral, dmkEdit,
  dmkEditCB, Variants, dmkDBLookupComboBox, Mask, UMySQLModule,
  dmkEditDateTimePicker, UnDmkProcFunc, UnDmkEnums;

type
  TFmSelfGer2 = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    PainelLct: TPanel;
    PainelDados2: TPanel;
    Panel5: TPanel;
    Panel7: TPanel;
    Label20: TLabel;
    BtSaldoAqui: TBitBtn;
    EdSdoAqui: TdmkEdit;
    BtRefresh: TBitBtn;
    EdSoma2: TdmkEdit;
    Panel6: TPanel;
    Panel8: TPanel;
    StatusBar: TStatusBar;
    dmkDBGLct: TdmkDBGrid;
    Timer2: TTimer;
    PMAtzCarts: TPopupMenu;
    Acarteiraselecionada1: TMenuItem;
    Todascarteiras1: TMenuItem;
    PMMenu: TPopupMenu;
    Transferir1: TMenuItem;
    Nova1: TMenuItem;
    Editar1: TMenuItem;
    Excluir1: TMenuItem;
    Localizar1: TMenuItem;
    Quitar1: TMenuItem;
    Compensar1: TMenuItem;
    Reverter1: TMenuItem;
    N10: TMenuItem;
    Pagar1: TMenuItem;
    N11: TMenuItem;
    PagarAVista1: TMenuItem;
    N2: TMenuItem;
    Localizarpagamentosdestaemisso1: TMenuItem;
    Lanamento1: TMenuItem;
    Localizar2: TMenuItem;
    Copiar1: TMenuItem;
    Mudacarteiradelanamentosselecionados1: TMenuItem;
    Carteiras1: TMenuItem;
    Data1: TMenuItem;
    Compensao1: TMenuItem;
    Msdecompetncia1: TMenuItem;
    TextoParcial1: TMenuItem;
    Recibo1: TMenuItem;
    RetornoCNAB1: TMenuItem;
    Colocarmsdecompetnciaondenotem1: TMenuItem;
    Datalancto1: TMenuItem;
    Vencimento1: TMenuItem;
    PMQuita: TPopupMenu;
    Compensar2: TMenuItem;
    Reverter2: TMenuItem;
    MenuItem3: TMenuItem;
    Pagar2: TMenuItem;
    MenuItem5: TMenuItem;
    PagarAVista2: TMenuItem;
    PMRefresh: TPopupMenu;
    Carteira1: TMenuItem;
    Atual5: TMenuItem;
    Todas1: TMenuItem;
    ContaPlanodecontas1: TMenuItem;
    ProgressBar1: TProgressBar;
    PMSaldoAqui: TPopupMenu;
    Calcula1: TMenuItem;
    Limpa1: TMenuItem;
    Diferena1: TMenuItem;
    PMEspecificos: TPopupMenu;
    N1: TMenuItem;
    Exclusoincondicional1: TMenuItem;
    ransfernciaentrecontas1: TMenuItem;
    Inclui1: TMenuItem;
    PMTrfCta: TPopupMenu;
    Novatransferncia1: TMenuItem;
    Alteratransferncia1: TMenuItem;
    Excluitransferncia1: TMenuItem;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    Label2: TLabel;
    DBText1: TDBText;
    Label3: TLabel;
    DBText2: TDBText;
    Label4: TLabel;
    DBText3: TDBText;
    Label7: TLabel;
    DBText4: TDBText;
    Label5: TLabel;
    DBEdit13: TDBEdit;
    Label19: TLabel;
    DBEdit14: TDBEdit;
    Label21: TLabel;
    DBEdit15: TDBEdit;
    QrDonosCart: TmySQLQuery;
    DsDonosCart: TDataSource;
    QrDonosCartForneceI: TIntegerField;
    QrDonosCartNO_ENT: TWideStringField;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    DsDonosLcto: TDataSource;
    QrDonosLcto: TmySQLQuery;
    QrDonosLctoFilial: TIntegerField;
    QrDonosLctoNO_ENT: TWideStringField;
    QrDonosLctoCodigo: TIntegerField;
    QrDonosCartFilial: TIntegerField;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_ENT: TWideStringField;
    QrFornecedores: TmySQLQuery;
    DsFornecedores: TDataSource;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNO_ENT: TWideStringField;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    DsCarteiras: TDataSource;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    Panel3: TPanel;
    BtMenu: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtEspecificos: TBitBtn;
    BtConcilia: TBitBtn;
    BtQuita: TBitBtn;
    BtCNAB: TBitBtn;
    BtCopiaCH: TBitBtn;
    BtContarDinheiro: TBitBtn;
    BtFluxoCxa: TBitBtn;
    BtAutom: TBitBtn;
    BtTrfCta: TBitBtn;
    BtLctoEndoss: TBitBtn;
    Panel4: TPanel;
    CkDataI: TCheckBox;
    TPDataI: TdmkEditDateTimePicker;
    CkDataF: TCheckBox;
    TPDataF: TdmkEditDateTimePicker;
    CkVctoI: TCheckBox;
    TPVctoI: TdmkEditDateTimePicker;
    CkVctoF: TCheckBox;
    TPVctoF: TdmkEditDateTimePicker;
    Label14: TLabel;
    EdCliIntCart: TdmkEditCB;
    CBCliIntCart: TdmkDBLookupComboBox;
    Label8: TLabel;
    EdCliIntLancto: TdmkEditCB;
    CBCliIntLancto: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label17: TLabel;
    EdFornecedor: TdmkEditCB;
    CBFornecedor: TdmkDBLookupComboBox;
    Label16: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    Label18: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    EdMezIni: TdmkEdit;
    CkMezIni: TCheckBox;
    EdMezFim: TdmkEdit;
    CkMezFim: TCheckBox;
    BitBtn1: TBitBtn;
    RGValores: TRadioGroup;
    Label1: TLabel;
    CBUH: TDBLookupComboBox;
    QrCarteira: TmySQLQuery;
    QrCarteiraCodigo: TIntegerField;
    QrCarteiraNome: TWideStringField;
    Label6: TLabel;
    EdCodigo: TDBEdit;
    EdNome: TDBEdit;
    DsCarteira: TDataSource;
    BtRelat: TBitBtn;
    PMRelat: TPopupMenu;
    Pesquisas1: TMenuItem;
    Result1: TMenuItem;
    porPerodo1: TMenuItem;
    Mensais1: TMenuItem;
    Saldos1: TMenuItem;
    SaldosEm1: TMenuItem;
    Descontodeduplicatas1: TMenuItem;
    Extrato1: TMenuItem;
    PagarReceber1: TMenuItem;
    Movimento1: TMenuItem;
    QrCarteirasForneceI: TIntegerField;
    Emitidos1: TMenuItem;
    Quitados1: TMenuItem;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGCarts1DblClick(Sender: TObject);
    procedure dmkDBGLctCellClick(Column: TColumn);
    procedure dmkDBGLctDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dmkDBGLctKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PageControl1Change(Sender: TObject);
    procedure PMQuitaPopup(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Acarteiraselecionada1Click(Sender: TObject);
    procedure Todascarteiras1Click(Sender: TObject);
    procedure Nova1Click(Sender: TObject);
    procedure Editar1Click(Sender: TObject);
    procedure Excluir1Click(Sender: TObject);
    procedure Localizar1Click(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure Compensar1Click(Sender: TObject);
    procedure Reverter1Click(Sender: TObject);
    procedure Pagar1Click(Sender: TObject);
    procedure PagarAVista1Click(Sender: TObject);
    procedure Localizarpagamentosdestaemisso1Click(Sender: TObject);
    procedure Localizar2Click(Sender: TObject);
    procedure Copiar1Click(Sender: TObject);
    procedure Carteiras1Click(Sender: TObject);
    procedure Data1Click(Sender: TObject);
    procedure Compensao1Click(Sender: TObject);
    procedure Msdecompetncia1Click(Sender: TObject);
    procedure TextoParcial1Click(Sender: TObject);
    procedure Recibo1Click(Sender: TObject);
    procedure RetornoCNAB1Click(Sender: TObject);
    procedure Datalancto1Click(Sender: TObject);
    procedure Vencimento1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtConciliaClick(Sender: TObject);
    procedure BtQuitaClick(Sender: TObject);
    procedure Compensar2Click(Sender: TObject);
    procedure Reverter2Click(Sender: TObject);
    procedure Pagar2Click(Sender: TObject);
    procedure PagarAVista2Click(Sender: TObject);
    procedure BtCNABClick(Sender: TObject);
    procedure BtCopiaCHClick(Sender: TObject);
    procedure TPDataIChange(Sender: TObject);
    procedure CBUHClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure BtEspecificosClick(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure Atual5Click(Sender: TObject);
    procedure Todas1Click(Sender: TObject);
    procedure BtSaldoAquiClick(Sender: TObject);
    procedure Calcula1Click(Sender: TObject);
    procedure Limpa1Click(Sender: TObject);
    procedure Diferena1Click(Sender: TObject);
    procedure BtContarDinheiroClick(Sender: TObject);
    procedure BtFluxoCxaClick(Sender: TObject);
    procedure BtAutomClick(Sender: TObject);
    procedure TPDataFChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Exclusoincondicional1Click(Sender: TObject);
    procedure BtTrfCtaClick(Sender: TObject);
    procedure Novatransferncia1Click(Sender: TObject);
    procedure Alteratransferncia1Click(Sender: TObject);
    procedure Excluitransferncia1Click(Sender: TObject);
    procedure PMTrfCtaPopup(Sender: TObject);
    procedure BtLctoEndossClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure CkDataIClick(Sender: TObject);
    procedure CkDataFClick(Sender: TObject);
    procedure CkVctoIClick(Sender: TObject);
    procedure CkVctoFClick(Sender: TObject);
    procedure EdCliIntCartChange(Sender: TObject);
    procedure EdCliIntLanctoChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdFornecedorChange(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
    procedure EdContaChange(Sender: TObject);
    procedure RGValoresClick(Sender: TObject);
    procedure BtRelatClick(Sender: TObject);
    procedure Pesquisas1Click(Sender: TObject);
    procedure Mensais1Click(Sender: TObject);
    procedure Saldos1Click(Sender: TObject);
    procedure SaldosEm1Click(Sender: TObject);
    procedure Descontodeduplicatas1Click(Sender: TObject);
    procedure PagarReceber1Click(Sender: TObject);
    procedure Movimento1Click(Sender: TObject);
    procedure Emitidos1Click(Sender: TObject);
    procedure Quitados1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ShowHint(Sender: TObject);
    procedure SomaLinhas_2;
    procedure VerificaBotoes();
    procedure VerificaCarteirasCliente();
    procedure MudaDataLctSel(CampoData: String);
    procedure ReverterCompensacao;
    procedure CompensarContaCorrenteBanco;
    procedure PagarAVistaEmCaixa;
    procedure Colocarmsdecompetnciaondenotem(Tipo: Integer);
    procedure ReopenSomaLinhas();
    procedure FechaLcto();
    procedure CompletaSQL(Query: TmySQLQuery);

    function ReopenLct(Controle, Sub: Integer): Boolean;
  public
    { Public declarations }
    FFinalidade: TIDFinalidadeLct;
    FGeradorTxt: String;
    procedure AtualizaCarteiraAtual();
    procedure AtualizaTodasCarteiras();
    //procedure CompensarContaCorrenteBanco;
  end;

  var
  FmSelfGer2: TFmSelfGer2;

implementation

uses UnMyObjects, Principal, UnInternalConsts, Module, LctMudaCart,
  GetData, LctMudaText, UnFinanceiro, LctPgEmCxa, UnGOTOy,
  Concilia, MyDBCheck, ModuleFin, FinForm, ModuleGeral,
  FluxoCxa, LctEdit, LctoEndoss, DescDupli, Pesquisas,
  Resultados1, Resmes, PrincipalImp, Extratos, Saldos, Resultados2;

{$R *.DFM}

procedure TFmSelfGer2.FechaLcto;
begin
  DmodFin.QrLctos.Close;
  dmkDBGLct.Visible := False;
end;

procedure TFmSelfGer2.FormActivate(Sender: TObject);
begin
  MyObjects.DefineTituloDBGrid(TDBGrid(dmkDBGLct), 'FatNum', VAR_TITULO_FATNUM);
  MyObjects.CorIniComponente();
end;

procedure TFmSelfGer2.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmSelfGer2.DBGCarts1DblClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  PageControl1Change(PageControl1);
end;

procedure TFmSelfGer2.Descontodeduplicatas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmDescDupli, FmDescDupli, afmoNegarComAviso) then
  begin
    FmDescDupli.ShowModal;
    FmDescDupli.Destroy;
  end;
end;

procedure TFmSelfGer2.dmkDBGLctCellClick(Column: TColumn);
begin
  Somalinhas_2;
end;

procedure TFmSelfGer2.dmkDBGLctDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  MyObjects.DefineCorTextoSitLancto(TDBGrid(Sender), Rect, 'NOMESIT',
    DmodFin.QrLctosNOMESIT.Value);
end;

procedure TFmSelfGer2.dmkDBGLctKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key=VK_UP) and (Shift=[ssShift]) then Somalinhas_2
  else if (key=VK_DOWN) and (Shift=[ssShift]) then Somalinhas_2
  //
  else if key=13 then

{
  UFinanceiro.InsAltLancamento(TFmLctEdit, FmLctEdit,
    lfProprio, afmoNegarComAviso, DmodFin.QrLctos, DmodFin.QrCarts,
    tgrAltera, DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value,
    DmodFin.QrLctosGenero.Value, Dmod.QrControleMyPerJuros.Value,
    Dmod.QrControleMyPerMulta.Value, nil, 0, 0, 0, 0, 0, 0, False,
    0(*Cliente*), 0(*Fornecedor*), DmodFin.QrCartsForneceI.Value(*cliInt*),
    0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
    False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
    0, 0, DmodFin.QrLctosCompensado.Value, 0, 1, 0)
}
  UFinanceiro.AlteracaoLancamento(DmodFin.QrCarts, DmodFin.QrLctos, lfProprio,
    0(*OneAccount*), DmodFin.QrCartsForneceI.Value(*OneCliInt*), True)

  else if (key=VK_DELETE) and (Shift=[ssCtrl]) then
  UFinanceiro.ExcluiItemCarteira(DmodFin.QrLctosControle.Value,
    DmodFin.QrLctosData.Value,
    DmodFin.QrLctosSub.Value, DmodFin.QrLctosGenero.Value,
    DmodFin.QrLctosCartao.Value, DmodFin.QrLctosSit.Value,
    DmodFin.QrLctosTipo.Value, 0, DmodFin.QrLctosID_Pgto.Value,
    DmodFin.QrLctos, DmodFin.QrCarts, True,
    DmodFin.QrLctosCarteira.Value, DmodFin.QrLctosCarteira.Value)
  else if (key=VK_F4) and (Shift=[ssCtrl]) then
    MyObjects.MostraPopUpDeBotaoObject(PMQuita, dmkDBGLct, 0, 0)
  //else if (key=VK_F5) and (Shift=[ssCtrl]) then ExibirLista1Click(Self)
  //else if (key=VK_F6) and (Shift=[ssCtrl]) then Promissria1Click(Self)
end;

procedure TFmSelfGer2.PageControl1Change(Sender: TObject);
begin
  //!BtSaida.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    1: VerificaCarteirasCliente();
    2: VerificaBotoes;
  end;
  //!PainelConfirma.Visible := PageControl1.ActivePageIndex in ([0,1,2]);
end;

procedure TFmSelfGer2.Pesquisas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPesquisas, FmPesquisas, afmoNegarComAviso) then
  begin
    FmPesquisas.ShowModal;
    FmPesquisas.Destroy;
  end;
end;

procedure TFmSelfGer2.SomaLinhas_2;
begin
  Timer2.Enabled := False;
  Timer2.Enabled := True;
end;

procedure TFmSelfGer2.PMQuitaPopup(Sender: TObject);
const
  k = 3;
var
  i, n: Integer;
  c: array [0..k] of Integer;
begin
  for i := 0 to k do c[i] := 0;
  if dmkDBGLct.SelectedRows.Count > 1 then
  begin
    with dmkDBGLct.DataSource.DataSet do
    for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
      Inc(c[DmodFin.QrLctosSit.Value], 1);
    end;
    n := dmkDBGLct.SelectedRows.Count;
  end else
  begin
    c[DmodFin.QrLctosSit.Value] := 1;
    n := 1;
  end;
  Compensar2.Enabled   := False;
  Pagar2.Enabled       := False;
  Reverter2.Enabled    := False;
  PagarAVista2.Enabled := False;
  if DmodFin.QrCartsTipo.Value = 2 then
  begin
    //if DmodFin.QrLctosSit.Value in [0] then   Compensar2.Enabled := True;
    Compensar2.Enabled := c[0] = n;
    //if DmodFin.QrLctosSit.Value in [0,1,2] then   Pagar2.Enabled := True;
    Pagar2.Enabled := (n=1) and (c[3] = 0);
    //if DmodFin.QrLctosSit.Value in [3] then    Reverter2.Enabled := True;
    Reverter2.Enabled := c[3] = n;
    //if DmodFin.QrLctosSit.Value in [0] then PagarAvista2.Enabled := True;
    PagarAvista2.Enabled := c[0] = n;
  end;
end;

procedure TFmSelfGer2.PMTrfCtaPopup(Sender: TObject);
begin
  Alteratransferncia1.Enabled := DmodFin.QrLctosFatID.Value = -1;
  Excluitransferncia1.Enabled := DmodFin.QrLctosFatID.Value = -1;
end;

procedure TFmSelfGer2.Quitados1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmResultados2, FmResultados2, afmoNegarComAviso) then
  begin
    FmResultados2.ShowModal;
    FmResultados2.Destroy;
  end;
end;

procedure TFmSelfGer2.VerificaCarteirasCliente();
var
  Carteira: Integer;
begin
  if DmodFin.QrCarts.State <> dsBrowse then Carteira := 0
  else Carteira := DmodFin.QrCartsCodigo.Value;
  //
  if (DmodFin.QrCarts.State = dsInactive)
  or not DmodG.EmpresaLogada(DmodFin.QrCartsForneceI.Value) then
    DmodFin.ReabreCarteiras(Carteira, DmodFin.QrCarts, DmodFin.QrCartSum,
    'TFmSelfGer2.VerificaCarteirasCliente()');
end;

procedure TFmSelfGer2.VerificaBotoes();
var
  Ativo: Boolean;
begin
  if DmodFin.QrCarts.State = dsBrowse then
  begin
    if DmodFin.QrCarts.RecordCount > 0 then Ativo := True
    else Ativo := False;
  end else Ativo := False;
  //BtMenu.Enabled := Ativo;
  //BtInclui.Enabled := Ativo;
  BtSaldoAqui.Enabled := Ativo;
  BtContarDinheiro.Enabled := Ativo;
  BtRefresh.Enabled := Ativo;
  //
  Ativo := (DmodFin.QrLctos.State = dsBrowse) and (DmodFin.QrLctos.RecordCount > 0);
  BtAltera.Enabled := Ativo;
  BtExclui.Enabled := Ativo;
  //
  (*if DmodFin.QrLctos.State = dsBrowse then
  begin
    if DmodFin.QrLctos.RecordCount > 0 then Ativo := True
    else Ativo := False;
  end else Ativo := False;*)
end;

procedure TFmSelfGer2.Timer2Timer(Sender: TObject);
var
  Debito, Credito: Double;
  i: Integer;
begin
  Timer2.Enabled := False;
  Debito := 0;
  Credito := 0;
  with dmkDBGLct.DataSource.DataSet do
  for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
  begin
    GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
    Debito := Debito + DmodFin.QrLctosDebito.Value;
    Credito := Credito + DmodFin.QrLctosCredito.Value;
  end;
  EdSoma2.Text := Geral.FFT(Credito-Debito, 2, siNegativo);
end;

procedure TFmSelfGer2.Acarteiraselecionada1Click(Sender: TObject);
begin
  AtualizaCarteiraAtual();
end;

procedure TFmSelfGer2.Todascarteiras1Click(Sender: TObject);
begin
  AtualizaTodasCarteiras();
end;

procedure TFmSelfGer2.AtualizaCarteiraAtual();
begin
  UFinanceiro.AtualizaVencimentos;
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer2.AtualizaTodasCarteiras();
var
  Carteira: Integer;
begin
  // parei aqui. ver
  Screen.Cursor := crHourGlass;
  Carteira := DmodFin.QrCartsCodigo.Value;
  UFinanceiro.AtualizaVencimentos;
  DmodFin.QrCarts.DisableControls;
  DmodFin.QrCarts.First;
  while not DmodFin.QrCarts.Eof do
  begin
    UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
      DmodFin.QrCarts, True, True);
    //
    DmodFin.QrCarts.Next;
  end;
  DmodFin.LocCod(Carteira, Carteira, DmodFin.QrCarts,
    'TFmSelfGer2.AtualizaTodasCarteiras()');
  DmodFin.QrCarts.EnableControls;
  Screen.Cursor := crDefault;
end;

procedure TFmSelfGer2.Nova1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCart(0, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
   DmodFin.QrCarts, True, True);
  //if QrCarts.State = dsBrowse then ReopenResumo;
end;

procedure TFmSelfGer2.Novatransferncia1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCtas(0, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  Dmod.RecalcSaldoCarteira(DmodFin.QrCartsTipo.Value, DmodFin.QrCartsCodigo.Value, 1);
end;

procedure TFmSelfGer2.EdCarteiraChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.EdClienteChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.EdCliIntCartChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.EdCliIntLanctoChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.EdContaChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.EdFornecedorChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.Editar1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCart(1, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer2.Emitidos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmResultados1, FmResultados1, afmoNegarComAviso) then
  begin
    FmResultados1.FTabLctA := VAR_LCT;
    FmResultados1.ShowModal;
    FmResultados1.Destroy;
  end;
end;

procedure TFmSelfGer2.Excluir1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCart(2, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer2.Excluitransferncia1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCtas(2, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  Dmod.RecalcSaldoCarteira(DmodFin.QrCartsTipo.Value, DmodFin.QrCartsCodigo.Value, 1);
end;

procedure TFmSelfGer2.Exclusoincondicional1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if Application.MessageBox('Confirma a exclus�o INCONDICIONAL deste lan�amento?',
  PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL) = ID_YES then
  begin
    UFinanceiro.ExcluiLct_Unico(True, DmodFin.QrLctos.Database,
            DmodFin.QrLctosData.Value, DmodFin.QrLctosTipo.Value,
            DmodFin.QrLctosCarteira.Value, DmodFin.QrLctosControle.Value,
            DmodFin.QrLctosSub.Value, True);
    //
    DmodFin.QrLctos.Next;
    Controle := DmodFin.QrLctosControle.Value;
    {
    Parei Aqui
    n�o esta recalculando!
    Dmod.RecalcSaldoCarteira(DmodFin.QrLctosTipo.Value,
      DmodFin.QrLctosCarteira.Value, 1);
    }  
    DmodFin.QrLctos.Close;
    DmodFin.QrLctos.Open;
    DmodFin.QrLctos.Locate('Controle', Controle, []);
  end;
end;

procedure TFmSelfGer2.Localizar1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCart(3, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer2.BtMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMenu, BtMenu);
end;

procedure TFmSelfGer2.Compensar1Click(Sender: TObject);
begin
  CompensarContaCorrenteBanco;
end;

procedure TFmSelfGer2.CompensarContaCorrenteBanco();
var
  Codigo, Controle: Integer;
begin
  Codigo   := DmodFin.QrLctosCarteira.Value;
  Controle := DmodFin.QrLctosControle.Value;
  UFinanceiro.CriaFmQuitaDoc2(DmodFin.QrLctosData.Value,
    DmodFin.QrLctosSub.Value, DmodFin.QrLctosControle.Value,
    DmodFin.QrLctosCtrlIni.Value, DmodFin.QrLctosSub.Value,
    DmodFin.QrCarts);
  DmodFin.LocCod(Codigo, Codigo, DmodFin.QrCarts,
    'TFmSelfGer2.CompensarContaCorrenteBanco()');
  DmodFin.QrLctos.Locate('Controle', Controle, []);
end;

procedure TFmSelfGer2.Reverter1Click(Sender: TObject);
begin
  ReverterCompensacao;
end;

procedure TFmSelfGer2.ReverterCompensacao;
var
  i, k: Integer;
begin
  if dmkDBGLct.SelectedRows.Count > 1 then
  begin
    if Application.MessageBox(PChar('Confirma a revers�o da compensa��o '+
    'dos itens selecionados?'), 'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
    ID_YES then
    begin
      with dmkDBGLct.DataSource.DataSet do
      for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
        if DmodFin.QrLctosSit.Value = 3 then
          UFinanceiro.ReverterPagtoEmissao(DmodFin.QrLctos,
            DmodFin.QrCarts, False, False, False);
      end;
      k := DmodFin.QrLctosControle.Value;
      UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
        DmodFin.QrCarts, True, True);
      DmodFin.QrLctos.Locate('Controle', k, []);
    end;
  end else UFinanceiro.ReverterPagtoEmissao(DmodFin.QrLctos,
    DmodFin.QrCarts, True, True, True);
end;

procedure TFmSelfGer2.RGValoresClick(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.Pagar1Click(Sender: TObject);
begin
  DmodFin.PagarRolarEmissao(DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer2.PagarAVista1Click(Sender: TObject);
begin
  PagarAVistaEmCaixa;
end;

procedure TFmSelfGer2.Localizarpagamentosdestaemisso1Click(Sender: TObject);
begin
  UFinanceiro.LocalizaPagamentosDeEmissao(DmodFin.QrLctosControle.Value,
    TPDataI, TPDataF, DmodFin.QrLctos);
end;

procedure TFmSelfGer2.Localizar2Click(Sender: TObject);
begin
  UFinanceiro.LocalizarLancamento(TPDataI,
  DmodFin.QrCarts, DmodFin.QrLctos, True, 0);
end;

procedure TFmSelfGer2.Copiar1Click(Sender: TObject);
begin
  DmodFin.AlteraLanctoEsp(DmodFin.QrLctos, 3,
    Dmod.QrControleMyPerJuros.Value, Dmod.QrControleMyPerMulta.Value,
    TPDataI, TPDataF, CBUH.KeyValue, FFinalidade);
end;

procedure TFmSelfGer2.Carteiras1Click(Sender: TObject);
var
  i, Carteira, Tipo, Sit: Integer;
  Compensado: String;
begin
  if DBCheck.CriaFm(TFmLctMudaCart, FmLctMudaCart, afmoNegarComAviso) then
  begin
    FmLctMudaCart.ShowModal;
    Carteira := FmLctMudaCart.FCarteiraSel;
    Tipo     := FmLctMudaCart.FCarteiraTip;
    FmLctMudaCart.Destroy;
    if Carteira = DmodFin.QrLctosCarteira.Value then Exit;
    if Carteira <> -1000 then
    begin
      Screen.Cursor := crHourGlass;
      with dmkDBGLct.DataSource.DataSet do
      for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
      begin
        if Tipo <> 2 then
        begin
          Sit := 3;
          Compensado := Geral.FDT(DmodFin.QrLctosVencimento.Value, 1)
        end else begin
          Sit := DmodFin.QrLctosControle.Value;
          Compensado := Geral.FDT(DmodFin.QrLctosCompensado.Value, 1)
        end;
        if DmodFin.QrLctosGenero.Value < 1 then
        begin
          Application.MessageBox(PChar('O lan�amento '+IntToStr(
          DmodFin.QrLctosControle.Value)+' � protegido. Para editar '+
          'este item selecione seu caminho correto!'), 'Erro', MB_OK+MB_ICONERROR);
          Exit;
        end;
        if DmodFin.QrLctosCartao.Value > 0 then
        begin
          Application.MessageBox(PChar('O lan�amento '+IntToStr(
          DmodFin.QrLctosControle.Value)+' n�o pode ser editado pois '+
          'pertence a uma fatura!'), 'Erro', MB_OK+MB_ICONERROR);
          Exit;
        end;
        GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
        {
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Carteira=:P0, Tipo=:P1, ');
        Dmod.QrUpd.SQL.Add('Sit=:P2, Compensado=:P3 ');
        Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
        Dmod.QrUpd.SQL.Add('');
        Dmod.QrUpd.Params[00].AsInteger := Carteira;
        Dmod.QrUpd.Params[01].AsInteger := Tipo;
        Dmod.QrUpd.Params[02].AsInteger := Sit;
        Dmod.QrUpd.Params[03].AsString  := Compensado;
        //
        Dmod.QrUpd.Params[04].AsInteger := DmodFin.QrLctosControle.Value;
        Dmod.QrUpd.Params[05].AsInteger := DmodFin.QrLctosSub.Value;
        Dmod.QrUpd.ExecSQL;
        }
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Carteira', 'Tipo', 'Sit', 'Compensado'], ['Controle', 'Sub'], [
        Carteira, Tipo, Sit, Compensado], [
        DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value], True, '');
      end;
      UFinanceiro.RecalcSaldoCarteira(Carteira, nil, False, False);
      UFinanceiro.RecalcSaldoCarteira(DmodFin.QrLctosCarteira.Value,
        DmodFin.QrLctos, True, True);
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelfGer2.MudaDataLctSel(CampoData: String);
var
  //, Tipo
  i, Carteira, Controle, Sub: Integer;
  Data: String;
begin
  //Tipo     := -1000;
  Carteira := -1000;
  MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
  FmGetData.TPData.Date := Date;
  FmGetData.ShowModal;
  FmGetData.Destroy;
  if VAR_GETDATA = 0 then
    Exit
  else
  begin
    with dmkDBGLct.DataSource.DataSet do
    for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
      //
      if (DmodFin.QrLctosSit.Value < 2) and
      (Uppercase(CampoData) = Uppercase('Compensado')) then
      begin
        Application.MessageBox(PChar('O lan�amento '+IntToStr(
        DmodFin.QrLctosControle.Value)+' n�o foi compensado ainda!'),
        'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
      if (Uppercase(CampoData) = Uppercase('Mez')) then
      begin
        if VAR_GETDATA = 0 then
          Data := ''
        else
          Data := Geral.FDT(VAR_GETDATA, 13);
      end else Data := Geral.FDT(VAR_GETDATA, 1);
      {
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET '+CampoData+'=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P1 AND Sub=:P2');
      Dmod.QrUpd.SQL.Add('');
      //
      Dmod.QrUpd.Params[00].AsString  := Data;
      Dmod.QrUpd.Params[01].AsInteger := DmodFin.QrLctosControle.Value;
      Dmod.QrUpd.Params[02].AsInteger := DmodFin.QrLctosSub.Value;
      Dmod.QrUpd.ExecSQL;
      }
      Controle := DmodFin.QrLctosControle.Value;
      Sub      := DmodFin.QrLctosSub.Value;
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      CampoData], ['Controle', 'Sub'], [Data], [Controle, Sub], True, '');
      if (Uppercase(CampoData) = Uppercase('Compensado')) then
      begin
        {
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET Data=:P0');
        Dmod.QrUpdM.SQL.Add('WHERE ID_Pgto=:P1 AND ID_Pgto>0');
        Dmod.QrUpdM.SQL.Add('');
        Dmod.QrUpdM.Params[00].AsString  := Data;
        Dmod.QrUpdM.Params[01].AsInteger := DmodFin.QrLctosControle.Value;
        Dmod.QrUpdM.ExecSQL;
        // Igualar  Vencimento e compensado dos pagamentos � vista
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET Compensado=:P0, Vencimento=:P1');
        Dmod.QrUpdM.SQL.Add('WHERE ID_Pgto=:P2 AND Tipo<>2 AND ID_Pgto > 0');
        Dmod.QrUpdM.Params[00].AsString  := Data;
        Dmod.QrUpdM.Params[01].AsString  := Data;
        Dmod.QrUpdM.Params[02].AsInteger := DmodFin.QrLctosControle.Value;
        Dmod.QrUpdM.ExecSQL;
        }
        if Controle > 0 then
        begin
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Data'], ['ID_Pgto'], [Data], [Controle], True, '');
          // Igualar  Vencimento e compensado dos pagamentos � vista
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Compensado', 'Vencimento'], ['ID_Pgto', 'Tipo'], [
          Data, Data], [Controle], True, '');
        end;
      end;
    end;
    UFinanceiro.RecalcSaldoCarteira(Carteira, nil, False, False);
    UFinanceiro.RecalcSaldoCarteira(DmodFin.QrLctosCarteira.Value,
      DmodFin.QrCarts, True, True);
  end;
end;

procedure TFmSelfGer2.Data1Click(Sender: TObject);
begin
  MudaDataLctSel('Data');
end;

procedure TFmSelfGer2.Compensao1Click(Sender: TObject);
begin
  MudaDataLctSel('Compensado');
end;

procedure TFmSelfGer2.Mensais1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmResMes, FmResMes, afmoNegarComAviso) then
  begin
    FmResMes.ShowModal;
    FmResMes.Destroy;
  end;
end;

procedure TFmSelfGer2.Movimento1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPrincipalImp, FmPrincipalImp, afmoNegarComAviso) then
  begin
    FmPrincipalImp.ShowModal;
    FmPrincipalImp.Destroy;
  end;
end;

procedure TFmSelfGer2.Msdecompetncia1Click(Sender: TObject);
begin
  MudaDataLctSel('Mez');
end;

procedure TFmSelfGer2.TextoParcial1Click(Sender: TObject);
var
  i: Integer;
  Atual, Novo, TodoTexto: String;
  Muda: Boolean;
begin
  if DBCheck.CriaFm(TFmLctMudaText, FmLctMudaText, afmoNegarComAviso) then
  begin
    FmLctMudaText.ShowModal;
    Muda  := FmLctMudaText.FMuda;
    Atual := FmLctMudaText.FAtual;
    Novo  := FmLctMudaText.FNovo;
    FmLctMudaText.Destroy;
    if Muda then
    begin
      Screen.Cursor := crHourGlass;
      with dmkDBGLct.DataSource.DataSet do
      for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
        TodoTexto := DmodFin.QrLctosDescricao.Value;
        if pos(Atual, TodoTexto) > 0 then
        begin
          TodoTexto := Geral.Substitui(TodoTexto, Atual, Novo);
          {
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Descricao=:P0 ');
          Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
          Dmod.QrUpd.SQL.Add('');
          Dmod.QrUpd.Params[00].AsString  := TodoTexto;
          //
          Dmod.QrUpd.Params[01].AsInteger := DmodFin.QrLctosControle.Value;
          Dmod.QrUpd.Params[02].AsInteger := DmodFin.QrLctosSub.Value;
          Dmod.QrUpd.ExecSQL;
          }
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Descricao'], ['Controle', 'Sub'], [TodoTexto], [
          DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value], True, '');
        end;
      end;
    end;
    ReopenLct(DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelfGer2.PagarAVistaEmCaixa;
  procedure InsereEmLctoEdit();
  begin
    Dmod.QrUpdL.Params[00].AsInteger := DmodFin.QrLctosControle.Value;
    Dmod.QrUpdL.Params[01].AsInteger := DmodFin.QrLctosSub.Value;
    Dmod.QrUpdL.Params[02].AsString  := DmodFin.QrLctosDescricao.Value;
    Dmod.QrUpdL.Params[03].AsString  := Geral.FDT(DmodFin.QrLctosData.Value, 1);
    Dmod.QrUpdL.Params[04].AsString  := Geral.FDT(DmodFin.QrLctosVencimento.Value, 1);
    Dmod.QrUpdL.Params[05].AsFloat   := DmodFin.QrLctosCredito.Value - DmodFin.QrLctosDebito.Value;
    //
    Dmod.QrUpdL.ExecSQL;
  end;
  var
    i: Integer;
begin
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('DELETE FROM lctoedit ');
  Dmod.QrUpdL.ExecSQL;
  //
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO lctoedit SET ');
  Dmod.QrUpdL.SQL.Add('Controle=:P0, Sub=:P1, Descricao=:P2, Data=:P3, ');
  Dmod.QrUpdL.SQL.Add('Vencimento=:P4, ValorOri=:P5');
  Dmod.QrUpdL.SQL.Add('');
  if dmkDBGLct.SelectedRows.Count > 1 then
  begin
    with dmkDBGLct.DataSource.DataSet do
    for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
      InsereEmLctoEdit();
    end;

  end else InsereEmLctoEdit();
  if DBCheck.CriaFm(TFmLctPgEmCxa, FmLctPgEmCxa, afmoNegarComAviso) then
  begin
    FmLctPgEmCxa.EdMulta.Text := Geral.FFT(DmodFin.QrLctosMulta.Value, 6, siPositivo);
    FmLctPgEmCxa.EdTaxaM.Text := Geral.FFT(DmodFin.QrLctosMoraDia.Value, 6, siPositivo);
    FmLctPgEmCxa.ShowModal;
    FmLctPgEmCxa.Destroy;
  end;
end;

procedure TFmSelfGer2.PagarReceber1Click(Sender: TObject);
begin
  if UFinanceiro.AtualizaPagamentosAVista(DmodFin.QrLctos) then
  begin
    if DBCheck.CriaFm(TFmExtratos, FmExtratos, afmoNegarComAviso) then
    begin
      FmExtratos.ShowModal;
      FmExtratos.Destroy;
    end;
  end;
end;

procedure TFmSelfGer2.Recibo1Click(Sender: TObject);
begin
  if DmodFin.QrLctosCredito.Value > 0 then
    GOTOy.EmiteRecibo(DmodFin.QrLctosCliente.Value,
    DmodFin.QrLctosCliInt.Value,
    DmodFin.QrLctosCredito.Value, 0, 0,
    IntToStr(DmodFin.QrLctosControle.Value) + '-' +
    IntToStr(DmodFin.QrLctosSub.Value),
    DmodFin.QrLctosDescricao.Value, '', '',
    DmodFin.QrLctosData.Value, DmodFin.QrLctosSit.Value)
  else
    GOTOy.EmiteRecibo(DmodFin.QrLctosCliInt.Value,
    DmodFin.QrLctosFornecedor.Value,
    DmodFin.QrLctosDebito.Value, 0, 0,
    IntToStr(DmodFin.QrLctosControle.Value) + '-' +
    IntToStr(DmodFin.QrLctosSub.Value),
    DmodFin.QrLctosDescricao.Value, '', '',
    DmodFin.QrLctosData.Value, DmodFin.QrLctosSit.Value);
end;

procedure TFmSelfGer2.RetornoCNAB1Click(Sender: TObject);
begin
  FmPrincipal.RetornoCNAB;
end;

procedure TFmSelfGer2.Datalancto1Click(Sender: TObject);
begin
  Colocarmsdecompetnciaondenotem(0);
end;

procedure TFmSelfGer2.Vencimento1Click(Sender: TObject);
begin
  Colocarmsdecompetnciaondenotem(1);
end;

procedure TFmSelfGer2.Colocarmsdecompetnciaondenotem(Tipo: Integer);
var
  Data: TDateTime;
  Ano, Mes, Dia: Word;
begin
  if Application.MessageBox(PChar('Somente os lan�amentos presentes na grade ' +
  ' (conforme sele��a de carteira e per�odo) ser�o analisados! Deseja ' +
  'continuar assim mesmo?'), 'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    DmodFin.QrLctos.First;
    while not DmodFin.QrLctos.Eof do
    begin
      Application.ProcessMessages;
      DmodFin.QrLocCta.Close;
      DmodFin.QrLocCta.Params[0].AsInteger := DmodFin.QrLctosGenero.Value;
      DmodFin.QrLocCta.Open;
      if (Uppercase(DmodFin.QrLocCtaMensal.Value) = 'V')
      and (DmodFin.QrLctosMez.Value = 0) then
      begin
        case Tipo of
          1: {Vencimento} Data := DmodFin.QrLctosVencimento.Value;
          else {Data}     Data := DmodFin.QrLctosData.Value;
        end;
        DecodeDate(Data, Ano, Mes, Dia);
        while ano > 100 do Ano := Ano - 100;
        {
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Mez=:P0 ');
        Dmod.QrUpd.SQL.Add('WHERE Controle=:P1 ');
        Dmod.QrUpd.SQL.Add('');
        Dmod.QrUpd.Params[00].AsInteger := Ano * 100 + Mes;
        Dmod.QrUpd.Params[01].AsInteger := DmodFin.QrLctosControle.Value;
        Dmod.QrUpd.ExecSQL;
        }
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['Mez'], ['Controle'], [
        Ano * 100 + Mes], [DmodFin.QrLctosControle.Value], True, '');
      end;
      DmodFin.QrLctos.Next;
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelfGer2.BtIncluiClick(Sender: TObject);
begin
  VerificaCarteirasCliente();
  VerificaBotoes();
  // Ver juros e multa da construtora
  if UFinanceiro.InclusaoLancamento(TFmLctEdit, FmLctEdit,
  lfProprio, afmoNegarComAviso, DmodFin.QrLctos, DmodFin.QrCarts,
  tgrInclui, 0, 0, DmodFin.QrLctosGenero.Value,
  Dmod.QrControleMyPerJuros.Value, Dmod.QrControleMyPerMulta.Value, nil,
  0, 0, 0, 0, 0, 0, False,
  0(*Cliente*), 0(*Fornecedor*), DmodFin.QrCartsForneceI.Value(*cliInt*),
  0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
  False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
  0, 0, DmodFin.QrLctosCompensado.Value, 0, 1, 0) > 0 then
    ReopenLct(DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value);
end;

procedure TFmSelfGer2.BtLctoEndossClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmLctoEndoss, FmLctoEndoss, afmoNegarComAviso) then
  begin
    FmLctoEndoss.FControle  := DmodFin.QrLctosControle.Value;
    FmLctoEndoss.FSub       := DmodFin.QrLctosSub.Value;
    FmLctoEndoss.FTPDataIni := TPDataI;
    FmLctoEndoss.FCarteiras := DmodFin.QrCarts;
    FmLctoEndoss.FLct       := DmodFin.QrLctos;
    //
    FmLctoEndoss.ReopenLancto(0, 0, True);
    //
    FmLctoEndoss.ShowModal;
    FmLctoEndoss.Destroy;
  end;
end;

procedure TFmSelfGer2.BitBtn1Click(Sender: TObject);
begin
  ReopenLct(DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value);
end;

procedure TFmSelfGer2.BtAlteraClick(Sender: TObject);
begin
{
  if UFinanceiro.InsAltLancamento(TFmLctEdit, FmLctEdit,
  lfProprio, afmoNegarComAviso, DmodFin.QrLctos, DmodFin.QrCarts,
  tgrAltera, DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value,
  DmodFin.QrLctosGenero.Value, Dmod.QrControleMyPerJuros.Value,
  Dmod.QrControleMyPerMulta.Value, nil,
  0, 0, 0, 0, 0, 0, False,
  0(*Cliente*), 0(*Fornecedor*), DmodFin.QrCartsForneceI.Value(*cliInt*),
  0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
  False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
  0, 0, DmodFin.QrLctosCompensado.Value, 0, 1, 0) > 0 then
    ReopenLct(DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value);
}
  UFinanceiro.AlteracaoLancamento(DmodFin.QrCarts, DmodFin.QrLctos, lfProprio,
    0(*OneAccount*), DmodFin.QrCartsForneceI.Value(*OneCliInt*), True)
end;

procedure TFmSelfGer2.BtExcluiClick(Sender: TObject);
begin
  UFinanceiro.ExcluiLanctoGrade(TDBGrid(dmkDBGLct),
    DmodFin.QrLctos, DmodFin.QrCarts, False);
end;

procedure TFmSelfGer2.BtConciliaClick(Sender: TObject);
begin
  // Criar carteira de concilia��o
  if DBCheck.CriaFm(TFmConcilia, FmConcilia, afmoNegarComAviso) then
  begin
    FmConcilia.FQrCarteiras  := DmodFin.QrCarts;
    FmConcilia.FQrLct        := DmodFin.QrLctos;
    FmConcilia.ReopenConcilia(0);
    //FmConcilia.FCartConcil := QrCondCartConcil.Value;
    FmConcilia.FCartConcilia := 0;
    FmConcilia.FMostra       := FmConcilia.QrConcilia0.RecordCount > 0;
    FmConcilia.F_CliInt      := DmodFin.QrCartsForneceI.Value;
    FmConcilia.FDTPicker     := TPDataI;
    FmConcilia.ShowModal;
    FmConcilia.Destroy;
  end;
end;

procedure TFmSelfGer2.BtQuitaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMQuita, BtQuita);
end;

procedure TFmSelfGer2.Compensar2Click(Sender: TObject);
begin
  CompensarContaCorrenteBanco;
end;

procedure TFmSelfGer2.Reverter2Click(Sender: TObject);
begin
  ReverterCompensacao;
end;

procedure TFmSelfGer2.Pagar2Click(Sender: TObject);
begin
  DmodFin.PagarRolarEmissao(DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer2.PagarAVista2Click(Sender: TObject);
begin
  PagarAVistaEmCaixa;
end;

procedure TFmSelfGer2.BtCNABClick(Sender: TObject);
begin
  FmPrincipal.RetornoCNAB;
end;

procedure TFmSelfGer2.BtCopiaCHClick(Sender: TObject);
begin
  FmFinForm.ImprimeCopiaDeCh(DmodFin.QrLctosControle.Value,
    DmodFin.QrLctosCliInt.Value);
end;

procedure TFmSelfGer2.TPDataIChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.CBUHClick(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.CkDataFClick(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.CkDataIClick(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.CkVctoFClick(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.CkVctoIClick(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Geral.WriteAppKey('Dias', Application.Title, Date - TPDataI.Date,
    ktInteger, HKEY_LOCAL_MACHINE);
  DmodFin.FTPDataIni           := nil;
  DmodFin.FTPDataFim           := nil;
end;

procedure TFmSelfGer2.FormCreate(Sender: TObject);
begin
  QrDonosCart.Open;
  QrDonosLcto.Open;
  QrClientes.Open;
  QrFornecedores.Open;
  QrCarteiras.Open;
  QrContas.Open;
  //
  DmodFin.FTPDataIni           := TPDataI;
  DmodFin.FTPDataFim           := TPDataF;
  FFinalidade                  := idflIndefinido;
  Application.OnHint           := ShowHint;
  PageControl1.ActivePageIndex := 0;
  //DBGCarteiras.DataSource      := DmodFin.DsCarts;
  //DBGCarts1.DataSource         := DmodFin.DsCarts;
  //DBGCarts2.DataSource         := DmodFin.DsCarts;
  EdCodigo.DataSource            := DmodFin.DsLctos;
  EdNome.DataSource              := DmodFin.DsLctos;
  //EdSaldo.DataSource           := DmodFin.DsCarts;
  //EdDiferenca.DataSource       := DmodFin.DsCarts;
  //EdCaixa.DataSource           := DmodFin.DsCarts;
  //
  dmkDBGLct.DataSource         := DmodFin.DsLctos;
  DBText1.DataSource           := DmodFin.DsLctos;
  DBText2.DataSource           := DmodFin.DsLctos;
  DBText3.DataSource           := DmodFin.DsLctos;
  DBText4.DataSource           := DmodFin.DsLctos;
  //
  DBEdit13.DataSource          := DmodFin.DsSomaLinhas;
  DBEdit14.DataSource          := DmodFin.DsSomaLinhas;
  DBEdit15.DataSource          := DmodFin.DsSomaLinhas;
  //////////////////////////////////////////////////////////////////////////////
  TPDataI.Date := Date - Geral.ReadAppKey('Dias', Application.Title,
    ktInteger, 60, HKEY_LOCAL_MACHINE);
  TPDataF.Date := Date;
  //////////////////////////////////////////////////////////////////////////////
  //
  {
  Imagem := Geral.ReadAppKey(
    'ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg', HKEY_LOCAL_MACHINE);
  if FileExists(Imagem) then
    ImgSelfGer.Bitmap.LoadFromFile(Imagem);
  }
  if DmodG.SoUmaEmpresaLogada(False) then
  begin
    EdCliIntCart.ValueVariant := DmodG.QrFiliLogFilial.Value;
    CBCliIntCart.KeyValue     := DmodG.QrFiliLogFilial.Value;
    //
    EdCliIntLancto.ValueVariant := DmodG.QrFiliLogFilial.Value;
    CBCliIntLancto.KeyValue     := DmodG.QrFiliLogFilial.Value;
  end;
  //
end;

procedure TFmSelfGer2.Saldos1Click(Sender: TObject);
begin
  DmodFin.ImprimeSaldos(DModFin.QrCartsForneceI.Value);
end;

procedure TFmSelfGer2.SaldosEm1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSaldos, FmSaldos, afmoNegarComAviso) then
  begin
    FmSaldos.ShowModal;
    FmSaldos.Destroy;
  end;
end;

procedure TFmSelfGer2.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmSelfGer2.BtEspecificosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEspecificos, BtEspecificos);
end;

procedure TFmSelfGer2.PMMenuPopup(Sender: TObject);
begin
  if DmodFin.QrLctosGenero.Value = -1 then begin
    Editar1.Enabled := True;
    Excluir1.Enabled := True;
  end else begin
    Editar1.Enabled  := False;
    Excluir1.Enabled := False;
  end;
  Compensar1.Enabled := False;
  Pagar1.Enabled     := False;
  Reverter1.Enabled  := False;
  if DmodFin.QrLctosTipo.Value = 2 then
  begin
    if DmodFin.QrLctosSit.Value in [0] then
      Compensar1.Enabled := True;
    if DmodFin.QrLctosSit.Value in [0,1,2] then
      Pagar1.Enabled := True;
    if DmodFin.QrLctosSit.Value in [3] then
      Reverter1.Enabled := True;
  end;
  if dmkDBGLct.SelectedRows.Count > 0 then
  begin
    Mudacarteiradelanamentosselecionados1.Enabled := True;
    PagarAVista1.Enabled := True;
  end else begin
    Mudacarteiradelanamentosselecionados1.Enabled := False;
    PagarAVista1.Enabled := False;
  end;
end;

procedure TFmSelfGer2.BtRefreshClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRefresh, BtRefresh);
end;

procedure TFmSelfGer2.BtRelatClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRelat, BtRelat);
end;

procedure TFmSelfGer2.Alteratransferncia1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCtas(1, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  Dmod.RecalcSaldoCarteira(DmodFin.QrCartsTipo.Value, DmodFin.QrCartsCodigo.Value, 1);
end;

procedure TFmSelfGer2.Atual5Click(Sender: TObject);
begin
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer2.Todas1Click(Sender: TObject);
begin
  AtualizaTodasCarteiras();
end;

procedure TFmSelfGer2.BtSaldoAquiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSaldoAqui, BtSaldoAqui);
end;

procedure TFmSelfGer2.BtTrfCtaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTrfCta, BtTrfCta);
end;

procedure TFmSelfGer2.Calcula1Click(Sender: TObject);
begin
  EdSdoAqui.Text := UFinanceiro.SaldoAqui(1, EdSdoAqui.Text,
    DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer2.Limpa1Click(Sender: TObject);
begin
  EdSdoAqui.Text := UFinanceiro.SaldoAqui(0, EdSdoAqui.Text,
    DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer2.Diferena1Click(Sender: TObject);
begin
  EdSdoAqui.Text := UFinanceiro.SaldoAqui(2, EdSdoAqui.Text,
    DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer2.BtContarDinheiroClick(Sender: TObject);
begin
  UFinanceiro.MudaValorEmCaixa(
    DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer2.BtFluxoCxaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFluxoCxa, FmFluxoCxa, afmoNegarComAviso) then
  begin
    FmFluxoCxa.FEntCod := DmodFin.QrCartsForneceI.Value;
    FmFluxoCxa.FConCod := 0;
    FmFluxoCxa.ShowModal;
    FmFluxoCxa.Destroy;
  end;
end;

procedure TFmSelfGer2.BtAutomClick(Sender: TObject);
begin
  UFinanceiro.QuitacaoAutomaticaDmk(dmkDBGLct,
    DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer2.TPDataFChange(Sender: TObject);
begin
  FechaLcto();
end;

procedure TFmSelfGer2.FormDestroy(Sender: TObject);
begin
  Application.OnHint := FmPrincipal.ShowHint;
end;

function TFmSelfGer2.ReopenLct(Controle, Sub: Integer): Boolean;
var
  LIni, LFim: String;
begin
  Result := False;
  if Geral.IMV(EdCliIntCart.Text) = 0 then
  begin
    Application.MessageBox('� necess�rio informar o dono da carteira!',
      'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    {
    if not FALiberar then
    begin
      Result := False;
      Exit;
    end;
    //
    }
    LIni := FormatDateTime(VAR_FORMATDATE, TPDataI.Date);
    LFim := FormatDateTime(VAR_FORMATDATE, TPDataF.Date);
    DmodFin.QrLctos.Close;
    DmodFin.QrLctos.SQL.Clear;
    DmodFin.QrLctos.SQL.Add('SELECT MOD(la.Mez, 100) Mes2, ');
    DmodFin.QrLctos.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, ');
    DmodFin.QrLctos.SQL.Add('la.*, ct.Codigo CONTA, ca.Prazo, ca.Nome NO_Carteira, ');
    DmodFin.QrLctos.SQL.Add('ca.Banco1, ca.Agencia1, ca.Conta1, ');
    DmodFin.QrLctos.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
    DmodFin.QrLctos.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
    DmodFin.QrLctos.SQL.Add('CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial ');
    DmodFin.QrLctos.SQL.Add('ELSE em.Nome END NOMEEMPRESA, ');
    DmodFin.QrLctos.SQL.Add('CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial ');
    DmodFin.QrLctos.SQL.Add('ELSE cl.Nome END NOMECLIENTE,');
    DmodFin.QrLctos.SQL.Add('CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial ');
    DmodFin.QrLctos.SQL.Add('ELSE fo.Nome END NOMEFORNECEDOR,');
    DmodFin.QrLctos.SQL.Add('CASE 1 WHEN fi.Tipo=0 THEN fi.RazaoSocial ');
    DmodFin.QrLctos.SQL.Add('ELSE fi.Nome END NOMEFORNECEI,');
    DmodFin.QrLctos.SQL.Add('ELT(la.Endossas, "Endossado", "Endossante", ');
    DmodFin.QrLctos.SQL.Add('"Ambos", "? ? ? ?") NO_ENDOSSADO, ');
    if VAR_KIND_DEPTO = kdUH then
      DmodFin.QrLctos.SQL.Add('cim.Unidade UH')
    else
      DmodFin.QrLctos.SQL.Add('"" UH');
    DmodFin.QrLctos.SQL.Add('FROM ' + VAR_LCT + ' la');
    DmodFin.QrLctos.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
    DmodFin.QrLctos.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
    DmodFin.QrLctos.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
    DmodFin.QrLctos.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
    DmodFin.QrLctos.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
    DmodFin.QrLctos.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
    DmodFin.QrLctos.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
    DmodFin.QrLctos.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
    DmodFin.QrLctos.SQL.Add('LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI');
    if VAR_KIND_DEPTO = kdUH then
      DmodFin.QrLctos.SQL.Add('LEFT JOIN condimov cim ON cim.Conta=la.Depto');
    //
    CompletaSQL(DmodFin.QrLctos);
    //
    DmodFin.QrLctos.Open;
    //
    if Controle > -1000 then Result := DmodFin.QrLctos.Locate('Controle;Sub', VarArrayOf(
    [Controle, Sub]), []) else Result := False;
    if (DmodFin.QrLctosControle.Value <> Controle) and
       (DmodFin.QrLctosSub.Value <> Sub) then DmodFin.QrLctos.Last;
    //////////////////////////////////////////////////////////////////////////////
    dmkDBGLct.Visible := True;
    //
    ReopenSomaLinhas;
    //
    VerificaBotoes();
    //
    Memo1.Lines.Clear;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelfGer2.ReopenSomaLinhas();
var
  LIni, LFim: String;
begin
  //if not FALiberar then Exit;
  //
  LIni := FormatDateTime(VAR_FORMATDATE, TPDataI.Date);
  LFim := FormatDateTime(VAR_FORMATDATE, TPDataF.Date);
  DModFin.QrSomaLinhas.Close;
  DModFin.QrSomaLinhas.SQL.Clear;
  DModFin.QrSomaLinhas.SQL.Add('SELECT SUM(la.Credito) CREDITO, SUM(la.Debito) DEBITO');
  DModFin.QrSomaLinhas.SQL.Add('FROM ' + VAR_LCT + ' la');
  DmodFin.QrSomaLinhas.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  DmodFin.QrSomaLinhas.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  DmodFin.QrSomaLinhas.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  DmodFin.QrSomaLinhas.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  DmodFin.QrSomaLinhas.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  DmodFin.QrSomaLinhas.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  DmodFin.QrSomaLinhas.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  DmodFin.QrSomaLinhas.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  DmodFin.QrSomaLinhas.SQL.Add('LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI');
  //
  CompletaSQL(DModFin.QrSomaLinhas);
  //
  DModFin.QrSomaLinhas.Open;
end;

procedure TFmSelfGer2.CompletaSQL(Query: TmySQLQuery);
begin
  Query.SQL.Add('WHERE ');
  case RGValores.ItemIndex of
    0: Query.SQL.Add('la.Credito>0');
    1: Query.SQL.Add('la.Debito>0');
    2: Query.SQL.Add('la.Controle>-1000000');
  end;
  //
  //if EdCliIntCart.ValueVariant <> 0 then
    Query.SQL.Add('AND ca.ForneceI=' +
    dmkPF.FFP(QrDonosCartForneceI.Value, 0));
  //
  if EdCliIntLancto.ValueVariant <> 0 then
    Query.SQL.Add('AND la.CliInt=' +
    dmkPF.FFP(QrDonosLctoCodigo.Value, 0));
  //
  Query.SQL.Add(dmkPF.SQL_Periodo(' AND la.Data ',
    TPDataI.Date, TPDataF.Date, CkDataI.Checked, CkDataF.Checked));
  //
  Query.SQL.Add(dmkPF.SQL_Periodo(' AND la.Vencimento ',
    TPVctoI.Date, TPVctoF.Date, CkVctoI.Checked, CkVctoF.Checked));
  //
  Query.SQL.Add(MLAGeral.SQL_Competencia_Mensal(' AND la.Mez ', EdMezIni.ValueVariant,
    EdMezFim.ValueVariant, CkMezIni.Checked, CkMezFim.Checked));
  //
  if EdCarteira.ValueVariant <> 0 then
    Query.SQL.Add('AND la.Carteira=' +
    dmkPF.FFP(EdCarteira.ValueVariant, 0));
  //
  if EdCliente.ValueVariant <> 0 then
    Query.SQL.Add('AND la.Cliente=' +
    dmkPF.FFP(EdCliente.ValueVariant, 0));
  //
  if EdFornecedor.ValueVariant <> 0 then
    Query.SQL.Add('AND la.Fornecedor=' +
    dmkPF.FFP(EdFornecedor.ValueVariant, 0));
  //
  if EdConta.ValueVariant <> 0 then
    Query.SQL.Add('AND la.Genero=' +
    dmkPF.FFP(EdConta.ValueVariant, 0));
  //
  {
  case RGOrdem.ItemIndex of
    1: Query.SQL.Add('ORDER BY la.Controle, la.Sub');
    2: Query.SQL.Add('ORDER BY la.Vencimento, la.Data, la.Controle, la.Sub');
    3: Query.SQL.Add('ORDER BY la.Duplicata, la.Vencimento, la.Data, la.Controle, la.Sub');
    else Query.SQL.Add('ORDER BY la.Data, la.Controle');
  end;
  }
end;

end.

