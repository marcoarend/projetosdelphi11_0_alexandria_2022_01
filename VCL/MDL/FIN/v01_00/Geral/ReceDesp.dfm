object FmReceDesp: TFmReceDesp
  Left = 339
  Top = 185
  Caption = 'FIN-RELAT-010 :: Pesquisa de Receitas e Despesas'
  ClientHeight = 555
  ClientWidth = 686
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 507
    Width = 686
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 574
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 686
    Height = 48
    Align = alTop
    Caption = 'Pesquisa de Receitas e Despesas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 684
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 548
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 686
    Height = 459
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 684
      Height = 457
      Align = alClient
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 1
        Top = 113
        Width = 682
        Height = 343
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Demostrativo de receitas e despesas'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PageControl2: TPageControl
            Left = 0
            Top = 165
            Width = 674
            Height = 150
            ActivePage = TabSheet4
            Align = alClient
            TabOrder = 0
            Visible = False
            object TabSheet4: TTabSheet
              Caption = 'Cr'#233'ditos'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGCreditos: TdmkDBGrid
                Left = 0
                Top = 0
                Width = 666
                Height = 74
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'Lan'#231'to'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Credito'
                    Title.Caption = 'Cr'#233'dito'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMECON'
                    Title.Caption = 'Conta'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MES'
                    Title.Caption = 'M'#234's'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESGR'
                    Title.Caption = 'Sub-grupo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEGRU'
                    Title.Caption = 'Grupo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Vencimento'
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DmodFin.DsCreditos
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'Lan'#231'to'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Credito'
                    Title.Caption = 'Cr'#233'dito'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMECON'
                    Title.Caption = 'Conta'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MES'
                    Title.Caption = 'M'#234's'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESGR'
                    Title.Caption = 'Sub-grupo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEGRU'
                    Title.Caption = 'Grupo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Vencimento'
                    Visible = True
                  end>
              end
              object Panel7: TPanel
                Left = 0
                Top = 74
                Width = 666
                Height = 48
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 1
                object BtCreditos: TBitBtn
                  Left = 256
                  Top = 4
                  Width = 150
                  Height = 40
                  Caption = '&Lan'#231'amentos'
                  TabOrder = 0
                  OnClick = BtCreditosClick
                  NumGlyphs = 2
                end
              end
            end
            object TabSheet5: TTabSheet
              Caption = 'D'#233'bitos'
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 174
              object DBGDebitos: TdmkDBGrid
                Left = 0
                Top = 0
                Width = 666
                Height = 74
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'DATA'
                    Title.Caption = 'Data'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'Lan'#231'to'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Descricao'
                    Title.Caption = 'Hist'#243'rico'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SERIE_DOC'
                    Title.Caption = 'Documento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DEBITO'
                    Title.Caption = 'D'#233'bito'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMECON'
                    Title.Caption = 'Conta'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESGR'
                    Title.Caption = 'Sub-grupo'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEGRU'
                    Title.Caption = 'Grupo'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'COMPENSADO_TXT'
                    Title.Caption = 'Compensado'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MES'
                    Title.Caption = 'M'#234's'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Vencimento'
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DmodFin.DsDebitos
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'DATA'
                    Title.Caption = 'Data'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'Lan'#231'to'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Descricao'
                    Title.Caption = 'Hist'#243'rico'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SERIE_DOC'
                    Title.Caption = 'Documento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DEBITO'
                    Title.Caption = 'D'#233'bito'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMECON'
                    Title.Caption = 'Conta'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESGR'
                    Title.Caption = 'Sub-grupo'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEGRU'
                    Title.Caption = 'Grupo'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'COMPENSADO_TXT'
                    Title.Caption = 'Compensado'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MES'
                    Title.Caption = 'M'#234's'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Vencimento'
                    Visible = True
                  end>
              end
              object Panel8: TPanel
                Left = 0
                Top = 74
                Width = 666
                Height = 48
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 1
                ExplicitTop = 126
                object BtDebitos: TBitBtn
                  Left = 256
                  Top = 4
                  Width = 150
                  Height = 40
                  Caption = '&Lan'#231'amentos'
                  TabOrder = 0
                  OnClick = BtDebitosClick
                  NumGlyphs = 2
                end
              end
            end
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 674
            Height = 165
            Align = alTop
            ParentBackground = False
            TabOrder = 1
            object Label13: TLabel
              Left = 4
              Top = 108
              Width = 68
              Height = 13
              Caption = 'Carteira '#250'nica:'
            end
            object CkNaoAgruparNada: TCheckBox
              Left = 12
              Top = 88
              Width = 289
              Height = 17
              Caption = 'N'#227'o agrupar nada (visualiza'#231#227'o de todos lan'#231'amentos).'
              TabOrder = 0
              OnClick = CkNaoAgruparNadaClick
            end
            object GroupBox2: TGroupBox
              Left = 1
              Top = 1
              Width = 672
              Height = 85
              Align = alTop
              Caption = ' Agrupamentos: '
              TabOrder = 1
              object CkAcordos: TCheckBox
                Left = 12
                Top = 20
                Width = 501
                Height = 17
                Caption = 
                  'Individualizar acordos judiciais. (pode aumentar a quantidade de' +
                  ' registros)'
                Checked = True
                State = cbChecked
                TabOrder = 0
              end
              object CkPeriodos: TCheckBox
                Left = 12
                Top = 40
                Width = 501
                Height = 17
                Caption = 
                  'Individualizar per'#237'odos [mensal]. (pode aumentar a quantidade de' +
                  ' registros)'
                Checked = True
                State = cbChecked
                TabOrder = 1
              end
              object CkTextos: TCheckBox
                Left = 12
                Top = 60
                Width = 501
                Height = 17
                Caption = 
                  'Agrupar lan'#231'amentos com hist'#243'ricos iguais. (pode diminuir a quan' +
                  'tidade de registros)'
                Checked = True
                State = cbChecked
                TabOrder = 2
              end
            end
            object EdCarteira: TdmkEditCB
              Left = 4
              Top = 124
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBCarteira
            end
            object CBCarteira: TdmkDBLookupComboBox
              Left = 48
              Top = 124
              Width = 525
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCarts
              TabOrder = 3
              dmkEditCB = EdCarteira
              UpdType = utYes
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Hist'#243'rico de Conta (do plano de contas)'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 674
            Height = 315
            Align = alClient
            ParentBackground = False
            TabOrder = 0
            object Label4: TLabel
              Left = 8
              Top = 8
              Width = 131
              Height = 13
              Caption = 'Conta (do plano de contas):'
            end
            object Label41: TLabel
              Left = 8
              Top = 52
              Width = 25
              Height = 13
              Caption = 'U.H.:'
            end
            object EdConta: TdmkEditCB
              Left = 8
              Top = 24
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBConta
            end
            object CBConta: TdmkDBLookupComboBox
              Left = 52
              Top = 24
              Width = 481
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContas
              TabOrder = 1
              dmkEditCB = EdConta
              UpdType = utYes
            end
            object CBUH: TDBLookupComboBox
              Left = 8
              Top = 66
              Width = 161
              Height = 21
              KeyField = 'Conta'
              ListField = 'Unidade'
              ListSource = DsUHs
              TabOrder = 2
              OnKeyDown = CBUHKeyDown
            end
            object CGValores: TdmkCheckGroup
              Left = 172
              Top = 48
              Width = 173
              Height = 41
              Caption = ' Valores: '
              Columns = 2
              ItemIndex = 1
              Items.Strings = (
                'Cr'#233'ditos'
                'Debitos')
              TabOrder = 3
              UpdType = utYes
              Value = 2
              OldValor = 0
            end
            object RGOrdem: TRadioGroup
              Left = 348
              Top = 48
              Width = 185
              Height = 41
              Caption = ' Ordena'#231#227'o: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Por Mes'
                'Por Unidade')
              TabOrder = 4
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Retornos CNAB'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 674
            Height = 65
            Align = alTop
            ParentBackground = False
            TabOrder = 0
            object GroupBox4: TGroupBox
              Left = 221
              Top = 1
              Width = 220
              Height = 63
              Align = alLeft
              Caption = ' Faixa de valores pagos: '
              TabOrder = 1
              object Label7: TLabel
                Left = 8
                Top = 16
                Width = 38
                Height = 13
                Caption = 'M'#237'nimo:'
              end
              object Label8: TLabel
                Left = 112
                Top = 16
                Width = 36
                Height = 13
                Caption = 'M'#225'ximo'
              end
              object EdValPagMin: TdmkEdit
                Left = 8
                Top = 32
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                OnChange = EdValPagMinChange
              end
              object EdValPagMax: TdmkEdit
                Left = 112
                Top = 32
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '999.999.999,99'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 999999999.990000000000000000
              end
            end
            object GroupBox5: TGroupBox
              Left = 1
              Top = 1
              Width = 220
              Height = 63
              Align = alLeft
              Caption = ' Faixa de valores dos t'#237'tulos: '
              TabOrder = 0
              object Label10: TLabel
                Left = 8
                Top = 16
                Width = 38
                Height = 13
                Caption = 'M'#237'nimo:'
              end
              object Label11: TLabel
                Left = 112
                Top = 16
                Width = 36
                Height = 13
                Caption = 'M'#225'ximo'
              end
              object EdValTitMin: TdmkEdit
                Left = 8
                Top = 32
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                OnChange = EdValTitMinChange
              end
              object EdValTitMax: TdmkEdit
                Left = 112
                Top = 32
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '999.999.999,99'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 999999999.990000000000000000
              end
            end
            object GroupBox6: TGroupBox
              Left = 441
              Top = 1
              Width = 232
              Height = 63
              Align = alClient
              Caption = ' Faixa de bloquetos: '
              TabOrder = 2
              object Label9: TLabel
                Left = 7
                Top = 16
                Width = 45
                Height = 13
                Caption = 'Bloqueto:'
              end
              object Label12: TLabel
                Left = 111
                Top = 16
                Width = 45
                Height = 13
                Caption = 'Bloqueto:'
              end
              object EdBloqIni: TdmkEdit
                Left = 7
                Top = 32
                Width = 101
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
              object EdBloqFim: TdmkEdit
                Left = 111
                Top = 32
                Width = 101
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
            end
          end
          object DBGrid1: TdmkDBGrid
            Left = 0
            Top = 65
            Width = 674
            Height = 250
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Step'
                Title.Caption = 'S'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Title.Caption = 'Bloqueto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Dta quit.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val. titul.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val. pago'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val. juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Arquivo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Carteira'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsRet
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBGrid1CellClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Step'
                Title.Caption = 'S'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Title.Caption = 'Bloqueto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Dta quit.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val. titul.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val. pago'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val. juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Arquivo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Carteira'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end>
          end
        end
      end
      object Panel4: TPanel
        Left = 1
        Top = 1
        Width = 682
        Height = 112
        Align = alTop
        TabOrder = 1
        object Label1: TLabel
          Left = 12
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object EdEmpresa: TdmkEditCB
          Left = 12
          Top = 20
          Width = 44
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 56
          Top = 20
          Width = 525
          Height = 21
          KeyField = 'CodCliInt'
          ListField = 'NOMECLI'
          ListSource = DsEmpresas
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
        end
        object GroupBox1: TGroupBox
          Left = 12
          Top = 44
          Width = 349
          Height = 61
          Caption = 
            ' Per'#237'odo de pesquisa:  (data da ocorr'#234'ncia no caso de retorno CN' +
            'AB): '
          TabOrder = 2
          object Label2: TLabel
            Left = 12
            Top = 16
            Width = 55
            Height = 13
            Caption = 'Data inicial:'
          end
          object Label3: TLabel
            Left = 112
            Top = 16
            Width = 48
            Height = 13
            Caption = 'Data final:'
          end
          object TPDataIni1: TdmkEditDateTimePicker
            Left = 12
            Top = 32
            Width = 97
            Height = 21
            Date = 39702.829181134300000000
            Time = 39702.829181134300000000
            TabOrder = 0
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object TPDataFim1: TdmkEditDateTimePicker
            Left = 112
            Top = 32
            Width = 97
            Height = 21
            Date = 39702.829181134300000000
            Time = 39702.829181134300000000
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object BitBtn1: TBitBtn
            Left = 212
            Top = 28
            Width = 75
            Height = 25
            Caption = 'Ontem'
            TabOrder = 2
            OnClick = BitBtn1Click
          end
        end
        object GroupBox3: TGroupBox
          Left = 364
          Top = 44
          Width = 217
          Height = 61
          Caption = ' Data de quita'#231#227'o (Retorno CNAB): '
          TabOrder = 3
          object Label5: TLabel
            Left = 12
            Top = 16
            Width = 55
            Height = 13
            Caption = 'Data inicial:'
          end
          object Label6: TLabel
            Left = 112
            Top = 16
            Width = 48
            Height = 13
            Caption = 'Data final:'
          end
          object TPDataIni2: TdmkEditDateTimePicker
            Left = 12
            Top = 32
            Width = 97
            Height = 21
            Date = 39702.829181134300000000
            Time = 39702.829181134300000000
            TabOrder = 0
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object TPDataFim2: TdmkEditDateTimePicker
            Left = 112
            Top = 32
            Width = 97
            Height = 21
            Date = 39702.829181134300000000
            Time = 39702.829181134300000000
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
        end
      end
    end
  end
  object QrEmpresas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodCliInt, CodEnti,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI'
      'FROM enticliint eci'
      'LEFT JOIN entidades ent ON ent.Codigo=eci.CodEnti'
      'ORDER BY NOMECLI')
    Left = 200
    Top = 52
    object QrEmpresasCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object QrEmpresasCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object QrEmpresasNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 228
    Top = 52
  end
  object frxReceDesp: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39702.836399733800000000
    ReportOptions.LastChange = 39702.836399733800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxReceDespGetValue
    Left = 256
    Top = 52
    Datasets = <
      item
        DataSet = DmodFin.frxDsCreditos
        DataSetName = 'frxDsCreditos'
      end
      item
        DataSet = DmodFin.frxDsDebitos
        DataSetName = 'frxDsDebitos'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = DmodFin.frxDsResumo
        DataSetName = 'frxDsResumo'
      end
      item
        DataSet = DmodFin.frxDsSaldoA
        DataSetName = 'frxDsSaldoA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageHeader4: TfrxPageHeader
        Height = 120.944960000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape3: TfrxShapeView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Left = 75.590600000000000000
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 226.771800000000000000
          Top = 56.692950000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'DEMONSTRATIVO DE RECEITAS E DESPESAS NO PER'#195#141'ODO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 83.149660000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 604.724800000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'P'#195#161'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 75.590600000000000000
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[VARF_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 75.590600000000000000
          Top = 102.047310000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Per'#195#173'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 257.008040000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsCreditos."NOMEGRU"'
        object Memo51: TfrxMemoView
          Left = 75.590600000000000000
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsCreditos."NOMEGRU"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 642.520100000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Valores em R$')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 597.165740000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'M'#195#170's')
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 294.803340000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsCreditos."NOMESGR"'
        object Memo53: TfrxMemoView
          Left = 75.590600000000000000
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsCreditos."NOMESGR"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 642.520100000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Left = 597.165740000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        Height = 15.118120000000000000
        Top = 332.598640000000000000
        Width = 793.701300000000000000
        DataSet = DmodFin.frxDsCreditos
        DataSetName = 'frxDsCreditos'
        RowCount = 0
        object Memo55: TfrxMemoView
          Left = 75.590600000000000000
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'NOMECON_2'
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsCreditos."NOMECON_2"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 642.520100000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsCreditos."Credito"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 597.165740000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'MES'
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsCreditos."MES"]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 370.393940000000000000
        Width = 793.701300000000000000
        object Memo58: TfrxMemoView
          Left = 75.590600000000000000
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 642.520100000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsCreditos."Credito">)]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 597.165740000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 408.189240000000000000
        Width = 793.701300000000000000
        object Memo60: TfrxMemoView
          Left = 75.590600000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Total de [frxDsCreditos."NOMEGRU"] ')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 642.520100000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsCreditos."Credito">)]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 548.031850000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsDebitos."NOMEGRU"'
        object Memo64: TfrxMemoView
          Left = 75.590600000000000000
          Width = 264.566929133858300000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDebitos."NOMEGRU"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 687.874460000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Valor')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 483.779840000000000000
          Width = 34.015748030000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'M'#195#170's')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 517.795610000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Baixa')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 551.811380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Documento')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 627.401980000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Nota fiscal')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 340.157700000000000000
          Width = 143.622118030000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Fornecedor')
          ParentFont = False
        end
      end
      object GroupHeader5: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 585.827150000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsDebitos."NOMESGR"'
        object Memo70: TfrxMemoView
          Left = 75.590600000000000000
          Width = 264.566929130000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsDebitos."NOMESGR"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 687.874460000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 483.779840000000000000
          Width = 34.015748030000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 517.795610000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 551.811380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 627.401980000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 340.157700000000000000
          Width = 143.622118030000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        Height = 15.118120000000000000
        Top = 661.417750000000000000
        Width = 793.701300000000000000
        DataSet = DmodFin.frxDsDebitos
        DataSetName = 'frxDsDebitos'
        RowCount = 0
        object Memo76: TfrxMemoView
          Left = 75.590600000000000000
          Width = 264.566953540000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '    [frxDsDebitos."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          Left = 687.874460000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsDebitos."Debito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          Left = 340.157700000000000000
          Width = 143.622115590000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'NO_FORNECE'
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsDebitos."NO_FORNECE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          Left = 517.795610000000000000
          Width = 34.015770000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDebitos."COMPENSADO_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo80: TfrxMemoView
          Left = 551.811380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'SERIE_DOC'
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDebitos."SERIE_DOC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          Left = 627.401980000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NF_TXT'
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDebitos."NF_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 483.779840000000000000
          Width = 34.015745590000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'MES2'
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsDebitos."MES2"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 737.008350000000000000
        Width = 793.701300000000000000
        object Memo82: TfrxMemoView
          Left = 687.874460000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Left = 75.590600000000000000
          Width = 264.566929133858300000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Sub-total de [frxDsDebitos."NOMESGR"]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Left = 483.779840000000000000
          Width = 34.015748030000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Left = 517.795610000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Left = 551.811380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Left = 627.401980000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 340.157700000000000000
          Width = 143.622118030000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter5: TfrxGroupFooter
        Height = 26.456690470000000000
        Top = 774.803650000000000000
        Width = 793.701300000000000000
        object Memo83: TfrxMemoView
          Left = 665.197280000000000000
          Width = 90.708720000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 75.590600000000000000
          Width = 589.606680000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Total de [frxDsDebitos."NOMEGRU"] ')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 49.133858270000000000
        Top = 1065.827460000000000000
        Width = 793.701300000000000000
        object Memo93: TfrxMemoView
          Left = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 136.063080000000000000
        Top = 907.087200000000000000
        Width = 793.701300000000000000
        object Memo85: TfrxMemoView
          Left = 75.590600000000000000
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'RESUMO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          Left = 75.590600000000000000
          Top = 60.472480000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Saldo Anterior:')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Left = 642.520100000000000000
          Top = 60.472480000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsSaldoA."TOTAL"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Left = 75.590600000000000000
          Top = 75.590600000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Total de receitas:')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Left = 642.520100000000000000
          Top = 75.590600000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsResumo."Credito"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Left = 75.590600000000000000
          Top = 90.708720000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Total de despesas:')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Left = 75.590600000000000000
          Top = 105.826840000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Saldo do per'#195#173'odo selecionado:')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Left = 642.520100000000000000
          Top = 90.708720000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsResumo."Debito"]')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 642.520100000000000000
          Top = 105.826840000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsResumo."SALDO"]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Left = 75.590600000000000000
          Top = 120.944960000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'SALDO NO FINAL DO PER'#195#141'ODO:')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 642.520100000000000000
          Top = 120.944960000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsResumo."FINAL"]')
          ParentFont = False
        end
      end
      object GroupHeader9: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 623.622450000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsDebitos."NOMECON"'
        object Memo4: TfrxMemoView
          Left = 75.590600000000000000
          Width = 264.566929133858300000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '  [frxDsDebitos."NOMECON"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 687.874460000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 483.779840000000000000
          Width = 34.015748030000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 517.795610000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 551.811380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 627.401980000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 340.157700000000000000
          Width = 143.622118030000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter9: TfrxGroupFooter
        Height = 15.118110240000000000
        Top = 699.213050000000000000
        Width = 793.701300000000000000
        object Memo10: TfrxMemoView
          Left = 687.874460000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 75.590600000000000000
          Width = 264.566929133858300000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '  Sub-total de [frxDsDebitos."NOMECON"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 483.779840000000000000
          Width = 34.015748030000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Left = 517.795610000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo198: TfrxMemoView
          Left = 551.811380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo199: TfrxMemoView
          Left = 627.401980000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 340.157700000000000000
          Width = 143.622118030000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 34.015770000000000000
        Top = 200.315090000000000000
        Width = 793.701300000000000000
        object Memo200: TfrxMemoView
          Left = 79.370130000000000000
          Top = 11.338590000000000000
          Width = 676.535870000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'RECEITAS')
          ParentFont = False
        end
      end
      object Footer4: TfrxFooter
        Height = 34.015770000000000000
        Top = 445.984540000000000000
        Width = 793.701300000000000000
        object Memo201: TfrxMemoView
          Left = 75.590600000000000000
          Top = 7.559059999999990000
          Width = 521.575140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'TOTAL RECEITAS:')
          ParentFont = False
        end
        object Memo202: TfrxMemoView
          Left = 597.165740000000000000
          Top = 7.559059999999990000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsCreditos."Credito">,MasterData2)]')
          ParentFont = False
        end
      end
      object Header4: TfrxHeader
        Height = 22.677180000000000000
        Top = 502.677490000000000000
        Width = 793.701300000000000000
        object Memo203: TfrxMemoView
          Left = 79.370130000000000000
          Width = 676.535870000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'DESPESAS')
          ParentFont = False
        end
      end
      object Footer6: TfrxFooter
        Height = 22.677180000000000000
        Top = 823.937540000000000000
        Width = 793.701300000000000000
        object Memo204: TfrxMemoView
          Left = 75.590600000000000000
          Width = 521.575140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'TOTAL DESPESAS:')
          ParentFont = False
        end
        object Memo205: TfrxMemoView
          Left = 597.165740000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsDebitos."Debito">,MasterData3)]')
          ParentFont = False
        end
      end
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 288
    Top = 312
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 316
    Top = 312
  end
  object DataSource1: TDataSource
    Left = 168
    Top = 312
  end
  object QrDC: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDCCalcFields
    SQL.Strings = (
      
        'SELECT lan.Genero, lan.Mez, lan.Depto, lan.Credito-lan.Debito Va' +
        'lor,'
      'lan.Controle, con.Nome NOMECON, cim.Unidade UNIDADE'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas    con ON con.Codigo=lan.Genero'#13
      'LEFT JOIN condimov  cim ON cim.Conta=lan.Depto'
      'WHERE lan.Tipo <> 2'
      'AND lan.Genero>0'
      'AND car.ForneceI = :P0'
      'AND Genero=:P1'
      'AND lan.Data BETWEEN :P2 AND :P3'
      'ORDER BY con.OrdemLista, NOMECON, Mez, Depto'
      '')
    Left = 140
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrDCNOMEMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMES'
      Size = 30
      Calculated = True
    end
    object QrDCGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrDCMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrDCDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrDCValor: TFloatField
      FieldName = 'Valor'
    end
    object QrDCNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrDCUNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 10
    end
    object QrDCControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object frxDC_Mes: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39702.836399733800000000
    ReportOptions.LastChange = 39702.836399733800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxReceDespGetValue
    Left = 284
    Top = 52
    Datasets = <
      item
        DataSet = frxDsDC
        DataSetName = 'frxDsDC'
      end
      item
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Columns = 4
      ColumnWidth = 45.800000000000000000
      ColumnPositions.Strings = (
        '0'
        '45,80'
        '91,60'
        '137,40')
      object PageHeader4: TfrxPageHeader
        Height = 130.393778900000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape3: TfrxShapeView
          Left = 52.913420000000000000
          Top = 37.795300000000000000
          Width = 689.385826771653500000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 60.472480000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Left = 52.913420000000000000
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 204.094620000000000000
          Top = 56.692950000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'Hist'#195#179'rico de Conta (do plano de contas)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 60.472480000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 582.047620000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'P'#195#161'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 52.913420000000000000
          Top = 79.370130000000000000
          Width = 689.385826770000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[VARF_NOMECOND]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 52.913420000000000000
          Top = 102.047310000000000000
          Width = 689.385826770000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Per'#195#173'odo: [VARF_PERIODO]    -    UH: [VARF_UH]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 147.401670000000000000
          Top = 120.944960000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 86.929190000000000000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'UH')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 52.913420000000000000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Lan'#195#167'to')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 320.503937007874000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 260.031496062992100000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'UH')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 226.015748031496100000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Lan'#195#167'to')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 493.606299210000000000
          Top = 120.944960000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 433.133858267716500000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'UH')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 399.118110236220500000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Lan'#195#167'to')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 666.708661420000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 606.236220472440900000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'UH')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 572.220472440944900000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Lan'#195#167'to')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 17.007878900000000000
        Top = 238.110390000000000000
        Width = 173.102474000000000000
        Condition = 'frxDsDC."Genero"'
        object Memo51: TfrxMemoView
          Left = 52.913420000000000000
          Top = 7.559060000000000000
          Width = 170.078740157480300000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsDC."NOMECON"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader3: TfrxGroupHeader
        Height = 9.448818900000000000
        Top = 279.685220000000000000
        Width = 173.102474000000000000
        Condition = 'frxDsDC."Mez"'
        object Memo53: TfrxMemoView
          Left = 52.913420000000000000
          Width = 170.078740157480300000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDC."NOMEMES"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        Height = 9.448818900000000000
        Top = 313.700990000000000000
        Width = 173.102474000000000000
        DataSet = frxDsDC
        DataSetName = 'frxDsDC'
        RowCount = 0
        object Memo56: TfrxMemoView
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsDC."Valor"]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 86.929190000000000000
          Width = 60.472440944881890000
          Height = 9.448818900000000000
          ShowHint = False
          DataField = 'UNIDADE'
          DataSet = frxDsDC
          DataSetName = 'frxDsDC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDC."UNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 52.913385830000000000
          Width = 34.015748031496060000
          Height = 9.448818900000000000
          ShowHint = False
          DataField = 'Controle'
          DataSet = frxDsDC
          DataSetName = 'frxDsDC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDC."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 9.448818897637795000
        Top = 347.716760000000000000
        Width = 173.102474000000000000
        object Memo59: TfrxMemoView
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsDC."Valor">)]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 86.929190000000000000
          Width = 60.472440944881890000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Todas UHs')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 52.913420000000000000
          Width = 34.015748031496060000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 9.448818897637795000
        Top = 381.732530000000000000
        Width = 173.102474000000000000
        object Memo60: TfrxMemoView
          Left = 52.913420000000000000
          Width = 94.488250000000000000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Tot. [frxDsDC."NOMECON"] ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsDC."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 49.133858270000000000
        Top = 487.559370000000000000
        Width = 793.701300000000000000
        object Memo93: TfrxMemoView
          Left = 52.913420000000000000
          Width = 691.653543307086600000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 1.889771100000000000
        Top = 211.653680000000000000
        Width = 173.102474000000000000
      end
      object Footer4: TfrxFooter
        Height = 11.338590000000000000
        Top = 415.748300000000000000
        Width = 173.102474000000000000
        object Memo201: TfrxMemoView
          Left = 52.913420000000000000
          Width = 94.488250000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'TOTAL PESQUISADO:')
          ParentFont = False
          WordWrap = False
        end
        object Memo202: TfrxMemoView
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsDC."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsDC: TfrxDBDataset
    UserName = 'frxDsDC'
    CloseDataSource = False
    DataSet = QrDC
    BCDToCurrency = False
    Left = 196
    Top = 312
  end
  object frxDC_Uni: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39702.836399733800000000
    ReportOptions.LastChange = 39702.836399733800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxReceDespGetValue
    Left = 312
    Top = 52
    Datasets = <
      item
        DataSet = frxDsDC
        DataSetName = 'frxDsDC'
      end
      item
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Columns = 4
      ColumnWidth = 45.800000000000000000
      ColumnPositions.Strings = (
        '0'
        '45,80'
        '91,60'
        '137,40')
      object PageHeader4: TfrxPageHeader
        Height = 130.393778900000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape3: TfrxShapeView
          Left = 52.913420000000000000
          Top = 37.795300000000000000
          Width = 689.385826771653500000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 60.472480000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Left = 52.913420000000000000
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 204.094620000000000000
          Top = 56.692950000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'Hist'#195#179'rico de Conta (do plano de contas)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 60.472480000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 582.047620000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'P'#195#161'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 52.913420000000000000
          Top = 79.370130000000000000
          Width = 689.385826771653500000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[VARF_NOMECOND]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 52.913420000000000000
          Top = 102.047310000000000000
          Width = 689.385826770000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Per'#195#173'odo: [VARF_PERIODO]    -    UH: [VARF_UH]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 147.401670000000000000
          Top = 120.944960000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 86.929190000000000000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'M'#195#170's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 52.913420000000000000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Lan'#195#167'to')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 320.503937007874000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 260.031496060000000000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'M'#195#170's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 226.015748031496100000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Lan'#195#167'to')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 493.606299210000000000
          Top = 120.944960000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 433.133858270000000000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'M'#195#170's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 399.118110236220500000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Lan'#195#167'to')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 666.708661420000000000
          Top = 120.944960000000000000
          Width = 75.590551180000000000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            'Valores em R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 606.236220470000000000
          Top = 120.944960000000000000
          Width = 60.472426300000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'M'#195#170's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 572.220472440944900000
          Top = 120.944960000000000000
          Width = 34.015730940000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Lan'#195#167'to')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 17.007878900000000000
        Top = 238.110390000000000000
        Width = 173.102474000000000000
        Condition = 'frxDsDC."Genero"'
        object Memo51: TfrxMemoView
          Left = 52.913420000000000000
          Top = 7.559060000000000000
          Width = 170.078740157480300000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsDC."NOMECON"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader3: TfrxGroupHeader
        Height = 9.448818900000000000
        Top = 279.685220000000000000
        Width = 173.102474000000000000
        Condition = 'frxDsDC."Depto"'
        object Memo53: TfrxMemoView
          Left = 52.913420000000000000
          Width = 170.078740160000000000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDC."UNIDADE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        Height = 9.448818900000000000
        Top = 313.700990000000000000
        Width = 173.102474000000000000
        DataSet = frxDsDC
        DataSetName = 'frxDsDC'
        RowCount = 0
        object Memo56: TfrxMemoView
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsDC."Valor"]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 86.929190000000000000
          Width = 60.472440944881890000
          Height = 9.448818900000000000
          ShowHint = False
          DataField = 'NOMEMES'
          DataSet = frxDsDC
          DataSetName = 'frxDsDC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDC."NOMEMES"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 52.913385830000000000
          Width = 34.015748031496060000
          Height = 9.448818900000000000
          ShowHint = False
          DataField = 'Controle'
          DataSet = frxDsDC
          DataSetName = 'frxDsDC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDC."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 9.448818897637795000
        Top = 347.716760000000000000
        Width = 173.102474000000000000
        object Memo59: TfrxMemoView
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsDC."Valor">)]'
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 86.929190000000000000
          Width = 60.472440940000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Todos meses')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 52.913420000000000000
          Width = 34.015748031496060000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 9.448818897637795000
        Top = 381.732530000000000000
        Width = 173.102474000000000000
        object Memo60: TfrxMemoView
          Left = 52.913420000000000000
          Width = 94.488250000000000000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Tot. [frxDsDC."NOMECON"] ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsDC."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 49.133858270000000000
        Top = 487.559370000000000000
        Width = 793.701300000000000000
        object Memo93: TfrxMemoView
          Left = 52.913420000000000000
          Width = 691.653543307086600000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 1.889771100000000000
        Top = 211.653680000000000000
        Width = 173.102474000000000000
      end
      object Footer4: TfrxFooter
        Height = 11.338590000000000000
        Top = 415.748300000000000000
        Width = 173.102474000000000000
        object Memo201: TfrxMemoView
          Left = 52.913420000000000000
          Width = 94.488250000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'TOTAL PESQUISADO:')
          ParentFont = False
          WordWrap = False
        end
        object Memo202: TfrxMemoView
          Left = 147.401670000000000000
          Width = 75.590551181102360000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsDC."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object QrUHs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Andar, Conta, Unidade '
      'FROM condimov'
      'WHERE Codigo=:P0'
      'ORDER BY Andar, Unidade')
    Left = 232
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUHsUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrUHsConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object DsUHs: TDataSource
    DataSet = QrUHs
    Left = 260
    Top = 312
  end
  object QrRet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cnab_lei'
      'WHERE entidade = 822'
      #10
      ''
      'AND QuitaData="2009-02-02"')
    Left = 140
    Top = 344
    object QrRetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRetBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrRetID_Link: TIntegerField
      FieldName = 'ID_Link'
    end
    object QrRetNossoNum: TWideStringField
      FieldName = 'NossoNum'
    end
    object QrRetSeuNum: TIntegerField
      FieldName = 'SeuNum'
    end
    object QrRetIDNum: TIntegerField
      FieldName = 'IDNum'
    end
    object QrRetOcorrCodi: TWideStringField
      FieldName = 'OcorrCodi'
      Size = 10
    end
    object QrRetOcorrData: TDateField
      FieldName = 'OcorrData'
    end
    object QrRetValTitul: TFloatField
      FieldName = 'ValTitul'
    end
    object QrRetValAbati: TFloatField
      FieldName = 'ValAbati'
    end
    object QrRetValDesco: TFloatField
      FieldName = 'ValDesco'
    end
    object QrRetValPago: TFloatField
      FieldName = 'ValPago'
    end
    object QrRetValJuros: TFloatField
      FieldName = 'ValJuros'
    end
    object QrRetValMulta: TFloatField
      FieldName = 'ValMulta'
    end
    object QrRetValJuMul: TFloatField
      FieldName = 'ValJuMul'
    end
    object QrRetMotivo1: TWideStringField
      FieldName = 'Motivo1'
      Size = 2
    end
    object QrRetMotivo2: TWideStringField
      FieldName = 'Motivo2'
      Size = 2
    end
    object QrRetMotivo3: TWideStringField
      FieldName = 'Motivo3'
      Size = 2
    end
    object QrRetMotivo4: TWideStringField
      FieldName = 'Motivo4'
      Size = 2
    end
    object QrRetMotivo5: TWideStringField
      FieldName = 'Motivo5'
      Size = 2
    end
    object QrRetQuitaData: TDateField
      FieldName = 'QuitaData'
    end
    object QrRetDiretorio: TIntegerField
      FieldName = 'Diretorio'
    end
    object QrRetArquivo: TWideStringField
      FieldName = 'Arquivo'
    end
    object QrRetItemArq: TIntegerField
      FieldName = 'ItemArq'
    end
    object QrRetStep: TSmallintField
      FieldName = 'Step'
      MaxValue = 1
    end
    object QrRetEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrRetCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrRetDevJuros: TFloatField
      FieldName = 'DevJuros'
    end
    object QrRetDevMulta: TFloatField
      FieldName = 'DevMulta'
    end
    object QrRetValOutro: TFloatField
      FieldName = 'ValOutro'
    end
    object QrRetValTarif: TFloatField
      FieldName = 'ValTarif'
    end
    object QrRetLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrRetDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrRetDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrRetUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrRetUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrRetDtaTarif: TDateField
      FieldName = 'DtaTarif'
    end
    object QrRetAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrRetAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrRetTamReg: TIntegerField
      FieldName = 'TamReg'
    end
  end
  object DsRet: TDataSource
    DataSet = QrRet
    Left = 168
    Top = 344
  end
  object PMCreditos: TPopupMenu
    Left = 292
    Top = 420
    object Alteracontadoslanamentosselecionados_Cred: TMenuItem
      Caption = 'Altera conta dos lan'#231'amentos selecionados'
      OnClick = Alteracontadoslanamentosselecionados_CredClick
    end
    object Alteralanamentoselecionado1: TMenuItem
      Caption = 'Altera lan'#231'amento selecionado'
      OnClick = Alteralanamentoselecionado1Click
    end
  end
  object PMDebitos: TPopupMenu
    Left = 324
    Top = 420
    object Alteracontadoslanamentosselecionados_Debi: TMenuItem
      Caption = 'Altera conta dos lan'#231'amentos selecionados'
      OnClick = Alteracontadoslanamentosselecionados_DebiClick
    end
    object Alteradescriodolanamentoselecionado1: TMenuItem
      Caption = 'Altera descri'#231#227'o do lan'#231'amento selecionado'
      OnClick = Alteradescriodolanamentoselecionado1Click
    end
    object Alteralanamentoselecionado2: TMenuItem
      Caption = 'Altera lan'#231'amento selecionado'
      OnClick = Alteralanamentoselecionado2Click
    end
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM lanctos'
      'WHERE Controle=:P0'
      'AND Sub=:P1')
    Left = 480
    Top = 28
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLctData: TDateField
      FieldName = 'Data'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrLctEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrLctCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLctDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrLctDescoControle: TIntegerField
      FieldName = 'DescoControle'
    end
    object QrLctUnidade: TIntegerField
      FieldName = 'Unidade'
    end
    object QrLctNFVal: TFloatField
      FieldName = 'NFVal'
    end
    object QrLctAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrLctExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLctUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLctUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLctSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLctDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrLctMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrLctMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrLctCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrLctTipoCH: TSmallintField
      FieldName = 'TipoCH'
    end
    object QrLctAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrLctReparcel: TIntegerField
      FieldName = 'Reparcel'
    end
    object QrLctID_Quit: TIntegerField
      FieldName = 'ID_Quit'
    end
    object QrLctAtrelado: TIntegerField
      FieldName = 'Atrelado'
    end
    object QrLctAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLctPagMul: TFloatField
      FieldName = 'PagMul'
    end
    object QrLctPagJur: TFloatField
      FieldName = 'PagJur'
    end
    object QrLctSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
    end
    object QrLctMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrLctProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrLctCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrLctAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM carteiras'
      'ORDER BY Nome')
    Left = 556
    Top = 36
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 100
    end
    object QrCarteirasContato1: TWideStringField
      FieldName = 'Contato1'
      Size = 100
    end
    object QrCarteirasInicial: TFloatField
      FieldName = 'Inicial'
    end
    object QrCarteirasBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCarteirasID: TWideStringField
      FieldName = 'ID'
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteirasID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Size = 50
    end
    object QrCarteirasSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrCarteirasEmCaixa: TFloatField
      FieldName = 'EmCaixa'
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteirasPagRec: TIntegerField
      FieldName = 'PagRec'
    end
    object QrCarteirasDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCarteirasTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCarteirasAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrCarteirasConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCarteirasCheque1: TIntegerField
      FieldName = 'Cheque1'
    end
    object QrCarteirasAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrCarteirasContab: TWideStringField
      FieldName = 'Contab'
    end
    object QrCarteirasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCarteirasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCarteirasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCarteirasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCarteirasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCarteirasOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCarteirasForneceN: TSmallintField
      FieldName = 'ForneceN'
    end
    object QrCarteirasFuturoC: TFloatField
      FieldName = 'FuturoC'
    end
    object QrCarteirasFuturoD: TFloatField
      FieldName = 'FuturoD'
    end
    object QrCarteirasFuturoS: TFloatField
      FieldName = 'FuturoS'
    end
    object QrCarteirasExclusivo: TSmallintField
      FieldName = 'Exclusivo'
    end
    object QrCarteirasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCarteirasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCarteirasRecebeBloq: TSmallintField
      FieldName = 'RecebeBloq'
    end
    object QrCarteirasEntiDent: TIntegerField
      FieldName = 'EntiDent'
    end
  end
  object QrCarts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, '
      'Tipo, ExigeNumCheque, ForneceI, Banco1, UsaTalao'
      'FROM carteiras '
      'WHERE ForneceI=:P0'
      'ORDER BY Nome')
    Left = 484
    Top = 304
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object StringField2: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object IntegerField2: TIntegerField
      FieldName = 'Fechamento'
    end
    object SmallintField1: TSmallintField
      FieldName = 'Prazo'
    end
    object IntegerField3: TIntegerField
      FieldName = 'Tipo'
    end
    object SmallintField2: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object IntegerField4: TIntegerField
      FieldName = 'ForneceI'
    end
    object IntegerField5: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCarteirasUsaTalao: TSmallintField
      FieldName = 'UsaTalao'
    end
  end
  object DsCarts: TDataSource
    DataSet = QrCarts
    Left = 512
    Top = 304
  end
end
