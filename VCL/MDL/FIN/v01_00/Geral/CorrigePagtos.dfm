object FmCorrigePagtos: TFmCorrigePagtos
  Left = 339
  Top = 185
  Caption = 'Corrige Pagamentos'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Label1: TLabel
      Left = 316
      Top = 4
      Width = 82
      Height = 13
      Caption = 'Tempo Restante:'
    end
    object Label2: TLabel
      Left = 440
      Top = 4
      Width = 26
      Height = 13
      Caption = 'Itens:'
    end
    object BtOK: TBitBtn
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Linkar pagtos'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Edit1: TEdit
      Left = 316
      Top = 20
      Width = 121
      Height = 21
      ReadOnly = True
      TabOrder = 2
    end
    object Edit2: TEdit
      Left = 440
      Top = 20
      Width = 221
      Height = 21
      ReadOnly = True
      TabOrder = 3
    end
    object BitBtn1: TBitBtn
      Left = 116
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Compensado'
      NumGlyphs = 2
      TabOrder = 4
      OnClick = BitBtn1Click
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Corrige Pagamentos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 780
      Height = 44
      Align = alClient
      Transparent = True
      ExplicitWidth = 788
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 398
    Align = alClient
    TabOrder = 0
    object PB1: TProgressBar
      Left = 1
      Top = 380
      Width = 782
      Height = 17
      Align = alBottom
      TabOrder = 0
    end
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 304
      Height = 379
      Align = alLeft
      ReadOnly = True
      TabOrder = 1
    end
    object Memo2: TMemo
      Left = 305
      Top = 1
      Width = 478
      Height = 379
      Align = alClient
      ReadOnly = True
      TabOrder = 2
    end
  end
  object QrPagtos1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan1.Controle, lan1.ID_Pgto'
      'FROM lanctos lan1'
      'WHERE lan1.Tipo=1'
      'AND lan1.ID_Pgto > 0')
    Left = 20
    Top = 16
    object QrPagtos1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagtos1ID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
  end
  object QrUpd: TmySQLDirectQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'UPDATE lanctos SET CtrlQuitPg=:P0'
      'WHERE Controle=:P1')
    Left = 340
    Top = 188
  end
  object OpenDialog1: TOpenDialog
    Left = 164
    Top = 140
  end
end
