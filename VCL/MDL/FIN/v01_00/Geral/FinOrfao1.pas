unit FinOrfao1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DB, Menus, dmkDBGrid;

type
  TFmFinOrfao1 = class(TForm)
    PainelConfirma: TPanel;
    BtAcao: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DsELSCI: TDataSource;
    PMAcao: TPopupMenu;
    Atribuirlanamentoaodonodacarteira1: TMenuItem;
    Excluirlanamentoincondicionalmente1: TMenuItem;
    DBGLct: TdmkDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Atribuirlanamentoaodonodacarteira1Click(Sender: TObject);
    procedure Excluirlanamentoincondicionalmente1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
  private
    { Private declarations }
    procedure ExecuteAcao(Acao: Integer);
  public
    { Public declarations }
  end;

  var
  FmFinOrfao1: TFmFinOrfao1;

implementation

uses UnMyObjects, ModuleFin, Module, UnInternalConsts, UnFinanceiro;

{$R *.DFM}

procedure TFmFinOrfao1.Atribuirlanamentoaodonodacarteira1Click(Sender: TObject);
begin
  ExecuteAcao(01);
end;

procedure TFmFinOrfao1.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmFinOrfao1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFinOrfao1.Excluirlanamentoincondicionalmente1Click(
  Sender: TObject);
begin
  ExecuteAcao(02);
end;

procedure TFmFinOrfao1.ExecuteAcao(Acao: Integer);
  procedure DefineAcao(Acao, Linha: Integer);
    procedure DefineClienteInternoPelaCarteira(Controle, Sub: Integer);
    begin
      Dmod.QrUpd.SQL.Clear;
      {  Demora muito!
      Dmod.QrUpd.SQL.Add('UPDATE ' + VAR_LCT + ' lan, carteiras car');
      Dmod.QrUpd.SQL.Add('SET lan.CliInt=car.ForneceI');
      Dmod.QrUpd.SQL.Add('WHERE lan.Carteira=car.Codigo');
      Dmod.QrUpd.SQL.Add('AND lan.Controle=:P0 AND lan.Sub=:P1');
      Dmod.QrUpd.SQL.Add('AND lan.CliInt=0');
      }
      Dmod.QrUpd.SQL.Add('UPDATE ' + VAR_LCT + ' lan SET lan.CliInt=:P0');
      Dmod.QrUpd.SQL.Add('WHERE lan.Controle=:P0 AND lan.Sub=:P1');
      Dmod.QrUpd.SQL.Add('AND lan.CliInt=0');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := DmodFin.QrELSCICART_DONO.Value;
      Dmod.QrUpd.Params[01].AsInteger := Controle;
      Dmod.QrUpd.Params[02].AsInteger := Sub;
      Dmod.QrUpd.ExecSQL;
    end;
  begin
    case Acao of
      01: DefineClienteInternoPelaCarteira(DModFin.QrELSCIControle.Value, DModFin.QrELSCISub.Value);
      02: UFinanceiro.ExcluiLct_Unico(True,  DModFin.QrELSCI.Database,
            DModFin.QrELSCIData.Value, DModFin.QrELSCITipo.Value,
            DModFin.QrELSCICarteira.Value, DModFin.QrELSCIControle.Value,
            DModFin.QrELSCISub.Value, False);
    end;
  end;
var
  i: Integer;
begin
  Screen.Cursor := crHourGlass;
  if Acao = 02 then
  begin
    if DBGLct.SelectedRows.Count < 2 then
    begin
      if Application.MessageBox(PChar('Confirma a exclus�o dos '+
      IntToStr(DBGLct.SelectedRows.Count) + ' itens selecionados?'),
      'Exclus�o de Linhas', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
    end else begin
      if Application.MessageBox('Confirma a exclus�o do item selecionado?',
      'Exclus�o de Linha', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
    end;
  end;
  if DBGLct.SelectedRows.Count > 0 then
  begin
    with DBGLct.DataSource.DataSet do
    for i:= 0 to DBGLct.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(DBGLct.SelectedRows.Items[i]));
      DefineAcao(Acao, DmodFin.QrELSCIControle.Value);
    end;
  end else DefineAcao(Acao, DmodFin.QrELSCIControle.Value);
  //
  DmodFin.QrELSCI.Close;
  DmodFin.QrELSCI.Open;
  Screen.Cursor := crDefault;
end;

procedure TFmFinOrfao1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFinOrfao1.FormCreate(Sender: TObject);
begin
  DsELSCI.Dataset := DmodFin.QrELSCI;
end;

procedure TFmFinOrfao1.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
