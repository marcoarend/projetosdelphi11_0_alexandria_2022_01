unit FinForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  frxClass;

type
  TFmFinForm = class(TForm)
    frxCopyCH: TfrxReport;
    procedure frxCopyCHGetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ImprimeCopiadeCH(Controle, CliInt: Integer);
  end;

var
  FmFinForm: TFmFinForm;

implementation

uses UnMyObjects, ModuleFin, UnMLAGeral, UnInternalConsts;

{$R *.DFM}

procedure TFmFinForm.frxCopyCHGetValue(const VarName: String;
  var Value: Variant);
begin
  {
  if AnsiCompareText(VarName, 'VARF_EXTENSO') = 0 then Value :=
    dmkPF.ExtensoMoney(Geral.FFT(
    DModFin.QrCopyCHDebito.Value, 2, siPositivo))
  else if AnsiCompareText(VarName, 'VARF_HOJE') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6, DmodFin.QrCopyCHData.Value)
  }  
end;

procedure TFmFinForm.ImprimeCopiadeCH(Controle, CliInt: Integer);
begin
  if DModFin.AbreQrCopyCHeques(Controle) then
  begin
    DmodFin.Adress1DeEntidade(CliInt, 0);
    MyObjects.frxMostra(frxCopyCH, 'C�pia de cheque');
  end;
end;

end.
