object FmCashBal: TFmCashBal
  Left = 339
  Top = 185
  Caption = 'FIN-BALAN-001 :: Impress'#227'o de Balancete'
  ClientHeight = 592
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 544
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Label3: TLabel
      Left = 480
      Top = 4
      Width = 36
      Height = 13
      Caption = 'Tempo:'
    end
    object BtOK: TBitBtn
      Tag = 14
      Left = 12
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BitBtn1: TBitBtn
      Tag = 14
      Left = 680
      Top = 4
      Width = 213
      Height = 40
      Caption = '&Enviar para o arquivo morto'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BitBtn1Click
    end
    object EdTempo: TdmkEdit
      Left = 480
      Top = 20
      Width = 53
      Height = 21
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfLong
      HoraFormat = dmkhfLong
      Texto = '00:00:00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object BtSdoIni: TBitBtn
      Left = 116
      Top = 4
      Width = 213
      Height = 40
      Caption = '&Definir saldos iniciais'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 4
      OnClick = BtSdoIniClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Impress'#227'o de Balancete'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 548
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 496
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 494
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 1006
        Height = 405
        Align = alTop
        TabOrder = 0
        object Panel7: TPanel
          Left = 1
          Top = 1
          Width = 1004
          Height = 403
          Align = alClient
          TabOrder = 0
          object Panel5: TPanel
            Left = 1
            Top = 1
            Width = 556
            Height = 401
            Align = alLeft
            TabOrder = 0
            object LaAviso: TLabel
              Left = 1
              Top = 293
              Width = 554
              Height = 13
              Align = alTop
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ExplicitWidth = 13
            end
            object Panel8: TPanel
              Left = 1
              Top = 1
              Width = 554
              Height = 292
              Align = alTop
              BevelOuter = bvLowered
              TabOrder = 0
              object Label1: TLabel
                Left = 12
                Top = 4
                Width = 70
                Height = 13
                Caption = 'Cliente interno:'
              end
              object Label2: TLabel
                Left = 12
                Top = 196
                Width = 91
                Height = 13
                Caption = 'Data de impress'#227'o:'
              end
              object EdEmpresa: TdmkEditCB
                Left = 12
                Top = 20
                Width = 44
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInt64
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                OnChange = EdEmpresaChange
                OnExit = EdEmpresaExit
                DBLookupComboBox = CBEmpresa
                IgnoraDBLookupComboBox = False
              end
              object CBEmpresa: TdmkDBLookupComboBox
                Left = 56
                Top = 20
                Width = 493
                Height = 21
                KeyField = 'CO_SHOW'
                ListField = 'NO_EMPRESA'
                ListSource = DsEmp
                TabOrder = 1
                dmkEditCB = EdEmpresa
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object RGFormato: TRadioGroup
                Left = 340
                Top = 44
                Width = 209
                Height = 61
                Caption = ' Formato: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'Completo '
                  'Reduzido')
                TabOrder = 2
                OnClick = RGFormatoClick
              end
              object GroupBox2: TGroupBox
                Left = 12
                Top = 108
                Width = 441
                Height = 85
                Caption = ' Agrupamentos: '
                TabOrder = 3
                object CkAcordos: TCheckBox
                  Left = 12
                  Top = 20
                  Width = 393
                  Height = 17
                  Caption = 
                    'Individualizar acordos judiciais. (pode aumentar a quantidade de' +
                    ' registros)'
                  Checked = True
                  State = cbChecked
                  TabOrder = 0
                end
                object CkPeriodos: TCheckBox
                  Left = 12
                  Top = 40
                  Width = 421
                  Height = 17
                  Caption = 
                    'Individualizar per'#237'odos [mensal]. (pode aumentar a quantidade de' +
                    ' registros)'
                  Checked = True
                  State = cbChecked
                  TabOrder = 1
                end
                object CkTextos: TCheckBox
                  Left = 12
                  Top = 60
                  Width = 421
                  Height = 17
                  Caption = 
                    'Agrupar lan'#231'amentos com hist'#243'ricos iguais. (pode diminuir a quan' +
                    'tidade de registros)'
                  TabOrder = 2
                end
              end
              object GroupBox1: TGroupBox
                Left = 12
                Top = 44
                Width = 193
                Height = 61
                Caption = ' Per'#237'odo de pesquisa: '
                TabOrder = 4
                object LaAno: TLabel
                  Left = 116
                  Top = 14
                  Width = 22
                  Height = 13
                  Caption = 'Ano:'
                end
                object LaMes: TLabel
                  Left = 8
                  Top = 14
                  Width = 23
                  Height = 13
                  Caption = 'M'#234's:'
                end
                object CBMes: TComboBox
                  Left = 8
                  Top = 31
                  Width = 105
                  Height = 21
                  Color = clWhite
                  DropDownCount = 12
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 7622183
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 0
                  Text = 'CBMes'
                end
                object CBAno: TComboBox
                  Left = 116
                  Top = 31
                  Width = 69
                  Height = 21
                  Color = clWhite
                  DropDownCount = 3
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 7622183
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 1
                  Text = 'CBAno'
                end
              end
              object BtTudo: TBitBtn
                Tag = 127
                Left = 458
                Top = 108
                Width = 90
                Height = 40
                Hint = 'Marca todos itens'
                Caption = '&Tudo'
                NumGlyphs = 2
                TabOrder = 5
                OnClick = BtTudoClick
              end
              object BtNenhum: TBitBtn
                Tag = 128
                Left = 458
                Top = 152
                Width = 90
                Height = 40
                Hint = 'Desmarca todos itens'
                Caption = '&Nenhum'
                NumGlyphs = 2
                TabOrder = 6
                OnClick = BtNenhumClick
              end
              object CkPaginar: TdmkCheckBox
                Left = 12
                Top = 240
                Width = 105
                Height = 17
                Caption = 'Paginar as folhas.'
                Checked = True
                State = cbChecked
                TabOrder = 7
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
              object TPData: TdmkEditDateTimePicker
                Left = 12
                Top = 212
                Width = 112
                Height = 21
                Date = 40375.706008634260000000
                Time = 40375.706008634260000000
                TabOrder = 8
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
              end
              object CGTipoDoc: TdmkCheckGroup
                Left = 128
                Top = 196
                Width = 417
                Height = 93
                Caption = 
                  ' Tipos de documentos a serem pesquisados (Documentos compensados' +
                  ' no per'#237'odo): '
                Columns = 5
                Items.Strings = (
                  'DmodFin.CarregaItensTipoDoc')
                TabOrder = 9
                UpdType = utYes
                Value = 0
                OldValor = 0
              end
            end
            object Panel6: TPanel
              Left = 1
              Top = 306
              Width = 554
              Height = 94
              Align = alClient
              BevelOuter = bvLowered
              TabOrder = 1
              object LaSub1: TLabel
                Left = 1
                Top = 1
                Width = 552
                Height = 13
                Align = alTop
                Caption = '...'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitWidth = 13
              end
              object LaSub2: TLabel
                Left = 1
                Top = 31
                Width = 552
                Height = 13
                Align = alTop
                Caption = '...'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitWidth = 13
              end
              object LaSub3: TLabel
                Left = 1
                Top = 61
                Width = 552
                Height = 13
                Align = alTop
                Caption = '...'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ExplicitWidth = 13
              end
              object PB1: TProgressBar
                Left = 1
                Top = 14
                Width = 552
                Height = 17
                Align = alTop
                TabOrder = 0
              end
              object PB2: TProgressBar
                Left = 1
                Top = 44
                Width = 552
                Height = 17
                Align = alTop
                TabOrder = 1
              end
              object PB3: TProgressBar
                Left = 1
                Top = 74
                Width = 552
                Height = 17
                Align = alTop
                TabOrder = 2
              end
            end
          end
          object Panel9: TPanel
            Left = 557
            Top = 1
            Width = 446
            Height = 401
            Align = alClient
            TabOrder = 1
            object DBGLista: TdmkDBGridDAC
              Left = 1
              Top = 1
              Width = 444
              Height = 239
              SQLFieldsToChange.Strings = (
                'Ativo')
              SQLIndexesOnUpdate.Strings = (
                'Controle')
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Width = 17
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ordem'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RelTitu'
                  Title.Caption = 'T'#237'tulo'
                  Width = 351
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsLista
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              SQLTable = 'cfgrel'
              EditForceNextYear = False
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Width = 17
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ordem'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RelTitu'
                  Title.Caption = 'T'#237'tulo'
                  Width = 351
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end>
            end
            object CGResumido: TdmkCheckGroup
              Left = 1
              Top = 240
              Width = 444
              Height = 160
              Align = alBottom
              Caption = ' Itens a serem impressos: '
              Items.Strings = (
                'Demonstrativo de receitas no per'#237'odo'
                'Demonstrativo de despesas no per'#237'odo'
                'Inadimpl'#234'ncia de unidades'
                'Resumo do movimento nas contas correntes'
                'Saldos das contas controladas'
                'Resumo geral de saldos')
              TabOrder = 1
              Visible = False
              UpdType = utYes
              Value = 0
              OldValor = 0
            end
          end
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 405
        Width = 1006
        Height = 89
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 1
        object TabSheet1: TTabSheet
          Caption = ' Arrecada'#231#227'o anual '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 120
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 998
            Height = 69
            Align = alClient
            DataSource = DsArre1
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Arrecada'#231#227'o Anual por Compet'#234'ncia '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 92
          object DBGrid2: TDBGrid
            Left = 0
            Top = 0
            Width = 998
            Height = 69
            Align = alClient
            DataSource = DsArre2
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' Extratos '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 92
          object DBGrid3: TDBGrid
            Left = 0
            Top = 0
            Width = 998
            Height = 69
            Align = alClient
            DataSource = DmodFin.DsExtratos
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet5: TTabSheet
          Caption = ' Saldos de Contas '
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 92
          object DBGrid4: TDBGrid
            Left = 0
            Top = 0
            Width = 998
            Height = 69
            Align = alClient
            DataSource = DmodFin.DsSaldosNiv
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet6: TTabSheet
          Caption = ' Saldos de contas controladas '
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 69
          object DBGrid5: TDBGrid
            Left = 0
            Top = 0
            Width = 998
            Height = 61
            Align = alClient
            DataSource = DmodFin.DsSdoPar
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Documentos compensados no per'#237'odo'
          ImageIndex = 5
          object DBGrid6: TDBGrid
            Left = 0
            Top = 0
            Width = 998
            Height = 61
            Align = alClient
            DataSource = Ds16
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
    end
  end
  object QrEmp: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmpCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, uf.Nome NO_UF, en.CliInt, '
      'FLOOR(IF(en.Codigo<-10,en.Filial,en.CliInt)) CO_SHOW,'
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_EMPRESA,'
      'IF(en.Tipo=0,en.ECidade,en.PCidade) CIDADE'
      'FROM entidades en'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)'
      'WHERE en.Codigo < -10'
      'OR en.Codigo=-1'
      'OR en.CliInt <> 0')
    Left = 8
    Top = 8
    object QrEmpCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEmpNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEmpNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Required = True
      Size = 100
    end
    object QrEmpCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEmpCO_SHOW: TLargeintField
      FieldName = 'CO_SHOW'
      Required = True
    end
    object QrEmpCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object DsEmp: TDataSource
    DataSet = QrEmp
    Left = 36
    Top = 8
  end
  object frxDsEmp: TfrxDBDataset
    UserName = 'frxDsEmp'
    CloseDataSource = False
    DataSet = QrEmp
    BCDToCurrency = False
    Left = 64
    Top = 8
  end
  object frxBal_A_01: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39947.621174710650000000
    ReportOptions.LastChange = 39947.621174710650000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <LogoBalanceteExiste> = True then'
      '      Picture1.LoadFromFile(<LogoBalancetePath>);'
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 104
    Top = 56
    Datasets = <
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object Memo1: TfrxMemoView
        Left = 18.897650000000000000
        Top = 37.795300000000000000
        Width = 755.906000000000000000
        Height = 151.181200000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -32
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsEmp."NO_EMPRESA"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo2: TfrxMemoView
        Left = 18.897650000000000000
        Top = 944.882500000000000000
        Width = 755.906000000000000000
        Height = 132.283550000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -32
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[VARF_PERIODO_TXT]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo3: TfrxMemoView
        Left = 18.897650000000000000
        Top = 188.976500000000000000
        Width = 755.906000000000000000
        Height = 75.590600000000000000
        ShowHint = False
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsEntiCfgRel_01."RelTitu"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Picture1: TfrxPictureView
        Left = 56.692950000000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        Height = 680.315400000000000000
        ShowHint = False
        HightQuality = True
        Transparent = False
        TransparentColor = clWhite
      end
    end
  end
  object frxBal_A_02: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39947.656532893520000000
    ReportOptions.LastChange = 39947.656532893520000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 132
    Top = 56
    Datasets = <
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end
      item
        DataSet = frxDsEntiRespon
        DataSetName = 'frxDsEntiRespon'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageHeader8: TfrxPageHeader
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo128: TfrxMemoView
          Left = 60.472480000000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
        end
      end
      object MasterData8: TfrxMasterData
        Height = 22.677180000000000000
        Top = 143.622140000000000000
        Width = 793.701300000000000000
        RowCount = 1
        Stretched = True
        object Rich1: TfrxRichView
          Left = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataField = 'RelPrcr'
          DataSet = frxDsEntiCfgRel_01
          DataSetName = 'frxDsEntiCfgRel_01'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C204D532053616E732053657269663B7D7D0D0A7B5C2A5C67656E
            657261746F7220526963686564323020362E322E393230307D5C766965776B69
            6E64345C756331200D0A5C706172645C66305C667331365C7061720D0A7D0D0A
            00}
        end
      end
      object MasterData9: TfrxMasterData
        Height = 79.370130000000000000
        Top = 188.976500000000000000
        Width = 793.701300000000000000
        DataSet = frxDsEntiRespon
        DataSetName = 'frxDsEntiRespon'
        RowCount = 0
        object Memo136: TfrxMemoView
          Left = 56.692950000000000000
          Top = 49.133890000000010000
          Width = 37.795300000000000000
          Height = 30.236220472440940000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo137: TfrxMemoView
          Left = 94.488250000000000000
          Top = 49.133890000000010000
          Width = 253.228510000000000000
          Height = 30.236220470000000000
          ShowHint = False
          DataSet = frxDsEntiRespon
          DataSetName = 'frxDsEntiRespon'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[frxDsEntiRespon."NO_RESPON"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo142: TfrxMemoView
          Left = 347.716760000000000000
          Top = 49.133890000000010000
          Width = 37.795300000000000000
          Height = 30.236220470000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'Cargo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo147: TfrxMemoView
          Left = 385.512060000000000000
          Top = 49.133890000000010000
          Width = 207.874150000000000000
          Height = 30.236220470000000000
          ShowHint = False
          DataField = 'NO_CARGO'
          DataSet = frxDsEntiRespon
          DataSetName = 'frxDsEntiRespon'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[frxDsEntiRespon."NO_CARGO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo150: TfrxMemoView
          Left = 502.677490000000000000
          Top = 30.236240000000010000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Ciente em:                 /                 /               ')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          Left = 593.386210000000000000
          Top = 49.133890000000010000
          Width = 143.622140000000000000
          Height = 30.236220470000000000
          ShowHint = False
          DataField = 'Observ'
          DataSet = frxDsEntiRespon
          DataSetName = 'frxDsEntiRespon'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[frxDsEntiRespon."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrEntiRespon: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT er.Nome NO_RESPON, ec.Nome NO_CARGO, '
      'er.Observ, er.MandatoIni, er.MandatoFim '
      'FROM entirespon er'
      'LEFT JOIN enticargos ec ON ec.Codigo=er.Cargo'
      'WHERE er.Codigo=:P0'
      'AND :P1 BETWEEN MandatoIni AND MandatoFim'
      'ORDER BY er.OrdemLista, er.Nome')
    Left = 256
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEntiResponNO_RESPON: TWideStringField
      FieldName = 'NO_RESPON'
      Required = True
      Size = 30
    end
    object QrEntiResponNO_CARGO: TWideStringField
      FieldName = 'NO_CARGO'
      Size = 30
    end
    object QrEntiResponObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
  end
  object frxDsEntiRespon: TfrxDBDataset
    UserName = 'frxDsEntiRespon'
    CloseDataSource = False
    DataSet = QrEntiRespon
    BCDToCurrency = False
    Left = 284
    Top = 312
  end
  object frxBal: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39947.906630057870000000
    ReportOptions.LastChange = 39947.906630057870000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 92
    Top = 8
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
  object QrEntiCfgRel_01: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM enticfgrel'
      'WHERE Codigo=:P0'
      'AND RelTipo=1'
      'ORDER BY Ordem')
    Left = 256
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiCfgRel_01Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'enticfgrel.Codigo'
    end
    object QrEntiCfgRel_01Controle: TIntegerField
      FieldName = 'Controle'
      Origin = 'enticfgrel.Controle'
    end
    object QrEntiCfgRel_01RelTipo: TIntegerField
      FieldName = 'RelTipo'
      Origin = 'enticfgrel.RelTipo'
    end
    object QrEntiCfgRel_01RelTitu: TWideStringField
      FieldName = 'RelTitu'
      Origin = 'enticfgrel.RelTitu'
      Size = 100
    end
    object QrEntiCfgRel_01Ordem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'enticfgrel.Ordem'
    end
    object QrEntiCfgRel_01RelPrcr: TWideMemoField
      FieldName = 'RelPrcr'
      Origin = 'enticfgrel.RelPrcr'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrEntiCfgRel_01RelItem: TIntegerField
      FieldName = 'RelItem'
      Origin = 'enticfgrel.RelItem'
    end
    object QrEntiCfgRel_01Genero: TIntegerField
      FieldName = 'Genero'
      Origin = 'enticfgrel.Genero'
    end
    object QrEntiCfgRel_01LogoPath: TWideStringField
      FieldName = 'LogoPath'
      Origin = 'enticfgrel.LogoPath'
      Size = 255
    end
    object QrEntiCfgRel_01FonteTam: TSmallintField
      FieldName = 'FonteTam'
    end
  end
  object frxDsEntiCfgRel_01: TfrxDBDataset
    UserName = 'frxDsEntiCfgRel_01'
    CloseDataSource = False
    DataSet = QrEntiCfgRel_01
    BCDToCurrency = False
    Left = 284
    Top = 340
  end
  object frxBal_A_03: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.407328206020000000
    ReportOptions.LastChange = 39949.407328206020000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MePagina.Visible := <VARF_PAGINAR>;                           ' +
        '                            '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 160
    Top = 56
    Datasets = <
      item
        DataSet = frxDsArre1
        DataSetName = 'frxDsArre1'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader5: TfrxPageHeader
        Height = 71.811062680000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Shape4: TfrxShapeView
          Width = 1046.929810000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo104: TfrxMemoView
          Left = 7.559060000000000000
          Width = 1031.811690000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo105: TfrxMemoView
          Left = 170.078850000000000000
          Top = 18.897650000000000000
          Width = 725.669760000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 895.748610000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          Left = 7.559060000000000000
          Top = 37.795300000000000000
          Width = 1009.134510000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 245.669450000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_JAN]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 294.803340000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_FEV]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 343.937230000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_MAR]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 393.071120000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_ABR]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 442.205010000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_MAI]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 491.338900000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_JUN]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 540.472790000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_JUL]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 589.606680000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_AGO]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 638.740570000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_SET]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 687.874460000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_OUT]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 737.008350000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_NOV]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 786.142240000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_DEZ]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 835.276130000000000000
          Top = 60.472479999999990000
          Width = 56.692918270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ANO1]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 891.969080000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MID1]'
            '')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 941.102970000000000000
          Top = 60.472479999999990000
          Width = 56.692918270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ANO0]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 997.795920000000000000
          Top = 60.472479999999990000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MID0]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 18.897650000000000000
          Top = 60.472479999999990000
          Width = 226.771653540000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Top = 60.472479999999990000
          Width = 18.897650000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        Height = 10.204724410000000000
        Top = 151.181200000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsArre1
        DataSetName = 'frxDsArre1'
        RowCount = 0
        object Memo111: TfrxMemoView
          Left = 18.897650000000000000
          Width = 226.771653543307100000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'NO_ENT'
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsArre1."NO_ENT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 245.669450000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."Valr01"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 294.803340000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."Valr02"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 343.937230000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."Valr03"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 393.071120000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."Valr04"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 442.205010000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."Valr05"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 491.338900000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."Valr06"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 540.472790000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."Valr07"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 589.606680000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."Valr08"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 638.740570000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."Valr09"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 687.874460000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."Valr10"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 737.008350000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."Valr11"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 786.142240000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."Valr12"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 835.276130000000000000
          Width = 56.692918270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."ValrA1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 891.969080000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."ValrM1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 941.102970000000000000
          Width = 56.692918270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."ValrA0"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 997.795920000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre1."ValrM0"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Width = 18.897650000000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'00;-00; '#39', <frxDsArre1."SEQ">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        Height = 22.677170240000000000
        Top = 185.196970000000000000
        Width = 1046.929810000000000000
        object Memo119: TfrxMemoView
          Left = 18.897650000000000000
          Top = 7.559059999999988000
          Width = 226.771653543307100000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  ')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 245.669450000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."Valr01">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 294.803340000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."Valr02">)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 343.937230000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."Valr03">)]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 393.071120000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."Valr04">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 442.205010000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."Valr05">)]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 491.338900000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."Valr06">)]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 540.472790000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."Valr07">)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 589.606680000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."Valr08">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 638.740570000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."Valr09">)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 687.874460000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."Valr10">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 737.008350000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."Valr11">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 786.142240000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."Valr12">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 835.276130000000000000
          Top = 7.559059999999988000
          Width = 56.692918270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."ValrA1">)]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 891.969080000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."ValrM1">)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 941.102970000000000000
          Top = 7.559059999999988000
          Width = 56.692918270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."ValrA0">)]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 997.795920000000000000
          Top = 7.559059999999988000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre1."ValrM0">)]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Top = 7.559059999999988000
          Width = 18.897650000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre1
          DataSetName = 'frxDsArre1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object PageFooter3: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 268.346630000000000000
        Width = 1046.929810000000000000
        object Memo120: TfrxMemoView
          Width = 1046.929810000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object QrArr1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) '
      'NO_ENT, lan.Cliente, '
      'YEAR(lan.Data) ANO, MONTH(lan.Data) MES,'
      'SUM(lan.Credito) Valor'
      'FROM lanctos lan'
      'LEFT JOIN entidades ent ON ent.Codigo=lan.Cliente'
      'WHERE lan.Genero=:P0'
      'AND lan.Data BETWEEN :P1 AND :P2'
      'AND lan.CliInt=:P3'
      'GROUP BY lan.Cliente, ANO, MES'
      'ORDER BY lan.Cliente, ANO, MES')
    Left = 92
    Top = 404
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrArr1NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrArr1Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrArr1ANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrArr1MES: TLargeintField
      FieldName = 'MES'
    end
    object QrArr1Valor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrAnt1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_ENT, '
      'lan.Cliente, SUM(lan.Credito) Valor'
      'FROM lanctos lan'
      'LEFT JOIN entidades ent ON ent.Codigo=lan.Cliente'
      'WHERE lan.Genero=:P0'
      'AND lan.Data >= :P1'
      'AND lan.Data < :P2'
      'AND lan.CliInt=:P3'
      'GROUP BY lan.Cliente'
      '')
    Left = 204
    Top = 404
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrAnt1Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrAnt1Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrAnt1NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object frxDsArre1: TfrxDBDataset
    UserName = 'frxDsArre1'
    CloseDataSource = False
    DataSet = QrDat1
    BCDToCurrency = False
    Left = 176
    Top = 404
  end
  object DsArre1: TDataSource
    DataSet = QrDat1
    Left = 120
    Top = 404
  end
  object QrArr2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) '
      'NO_ENT, lan.Cliente, Mez, SUM(lan.Credito) Valor'
      'FROM lanctos lan'
      'LEFT JOIN entidades ent ON ent.Codigo=lan.Cliente'
      'WHERE lan.Genero=:P0'
      'AND lan.Mez BETWEEN :P1 AND :P2'
      'AND lan.Data <=:P3'
      'AND lan.CliInt=:P4'
      'GROUP BY lan.Cliente, Mez'
      'ORDER BY lan.Cliente, Mez'
      '')
    Left = 92
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrArr2NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrArr2Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrArr2Mez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrArr2Valor: TFloatField
      FieldName = 'Valor'
    end
  end
  object frxDsArre2: TfrxDBDataset
    UserName = 'frxDsArre2'
    CloseDataSource = False
    DataSet = QrDat2
    BCDToCurrency = False
    Left = 176
    Top = 432
  end
  object DsArre2: TDataSource
    DataSet = QrDat2
    Left = 120
    Top = 432
  end
  object frxBal_A_04: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.407328206020000000
    ReportOptions.LastChange = 39949.407328206020000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MePagina.Visible := <VARF_PAGINAR>;                           ' +
        '                            '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 188
    Top = 56
    Datasets = <
      item
        DataSet = frxDsArre2
        DataSetName = 'frxDsArre2'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader5: TfrxPageHeader
        Height = 71.811062680000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Shape4: TfrxShapeView
          Width = 1046.929810000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo104: TfrxMemoView
          Left = 7.559060000000000000
          Width = 1031.811690000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo105: TfrxMemoView
          Left = 170.078850000000000000
          Top = 18.897650000000000000
          Width = 725.669760000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 895.748610000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          Top = 41.574830000000000000
          Width = 1046.929810000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 245.669450000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_JAN]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 294.803340000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_FEV]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 343.937230000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_MAR]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 393.071120000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_ABR]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 442.205010000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_MAI]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 491.338900000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_JUN]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 540.472790000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_JUL]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 589.606680000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_AGO]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 638.740570000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_SET]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 687.874460000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_OUT]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 737.008350000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_NOV]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 786.142240000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VMES_DEZ]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 835.276130000000000000
          Top = 60.472480000000000000
          Width = 56.692918270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ANO1]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 891.969080000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MID1]'
            '')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 941.102970000000000000
          Top = 60.472480000000000000
          Width = 56.692918270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ANO0]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 997.795920000000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MID0]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 18.897650000000000000
          Top = 60.472480000000000000
          Width = 226.771653543307100000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Top = 60.472480000000000000
          Width = 18.897650000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        Height = 10.204724410000000000
        Top = 151.181200000000000000
        Width = 1046.929810000000000000
        DataSet = frxDsArre2
        DataSetName = 'frxDsArre2'
        RowCount = 0
        object Memo111: TfrxMemoView
          Left = 18.897650000000000000
          Width = 226.771653543307100000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'NO_ENT'
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsArre2."NO_ENT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 245.669450000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."Valr01"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 294.803340000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."Valr02"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 343.937230000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."Valr03"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 393.071120000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."Valr04"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 442.205010000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."Valr05"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 491.338900000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."Valr06"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 540.472790000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."Valr07"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 589.606680000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."Valr08"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 638.740570000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."Valr09"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 687.874460000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."Valr10"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 737.008350000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."Valr11"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 786.142240000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."Valr12"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 835.276130000000000000
          Width = 56.692918270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."ValrA1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 891.969080000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."ValrM1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 941.102970000000000000
          Width = 56.692918270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."ValrA0"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 997.795920000000000000
          Width = 49.133858270000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsArre2."ValrM0"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Width = 18.897650000000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'00;-00; '#39', <frxDsArre2."SEQ">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        Height = 22.677170240000000000
        Top = 185.196970000000000000
        Width = 1046.929810000000000000
        object Memo119: TfrxMemoView
          Left = 18.897650000000000000
          Top = 7.559060000000000000
          Width = 226.771653543307100000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  ')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 245.669450000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."Valr01">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 294.803340000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."Valr02">)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 343.937230000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."Valr03">)]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 393.071120000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."Valr04">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 442.205010000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."Valr05">)]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 491.338900000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."Valr06">)]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 540.472790000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."Valr07">)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 589.606680000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."Valr08">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 638.740570000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."Valr09">)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 687.874460000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."Valr10">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 737.008350000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."Valr11">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 786.142240000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."Valr12">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 835.276130000000000000
          Top = 7.559060000000000000
          Width = 56.692918270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."ValrA1">)]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 891.969080000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."ValrM1">)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 941.102970000000000000
          Top = 7.559060000000000000
          Width = 56.692918270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."ValrA0">)]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 997.795920000000000000
          Top = 7.559060000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsArre2."ValrM0">)]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Top = 7.559060000000000000
          Width = 18.897650000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = frxDsArre2
          DataSetName = 'frxDsArre2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object PageFooter3: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 268.346630000000000000
        Width = 1046.929810000000000000
        object Memo120: TfrxMemoView
          Width = 1046.929810000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object QrAnt2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_ENT, '
      'lan.Cliente,  SUM(lan.Credito) Valor'
      'FROM lanctos lan'
      'LEFT JOIN entidades ent ON ent.Codigo=lan.Cliente'
      'WHERE lan.Genero=:P0'
      'AND lan.Mez BETWEEN :P1 AND :P2'
      'AND lan.Data <=:P3'
      'AND lan.CliInt=:P4'
      'GROUP BY lan.Cliente')
    Left = 204
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrAnt2NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrAnt2Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrAnt2Valor: TFloatField
      FieldName = 'Valor'
    end
  end
  object frxBal_A_05: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.647167257000000000
    ReportOptions.LastChange = 39949.647167257000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  nCC, nLI: Integer;'
      '  SaldoCC_C, SaldoCC_D: Extended;'
      ''
      'procedure ReportTitle1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  nCC := 0;'
      '  SaldoCC_C := 0;'
      '  SaldoCC_D := 0;  '
      'end;'
      ''
      'procedure PageHeader3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  nLI := 0;'
      
        '  if nCC = 0 then PageHeader3.Visible := False else PageHeader3.' +
        'Visible := True;'
      'end;'
      ''
      'procedure GroupHeader1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  // ???                 '
      'end;'
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  nLI := nLI + 1;'
      '  if  <frxDsExtratos."SdIni"> = 0 then'
      '  begin'
      '    SaldoCC_C := SaldoCC_C + <frxDsExtratos."Credi">;'
      '    SaldoCC_D := SaldoCC_D + <frxDsExtratos."Debit">;'
      '  end;  '
      'end;'
      ''
      'procedure Memo32OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Memo32.Text := FormatFloat('#39'#,###,##0.00'#39', SaldoCC_C);'
      '  SaldoCC_C := 0;'
      'end;'
      ''
      'procedure Memo33OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Memo33.Text := FormatFloat('#39'#,###,##0.00'#39', SaldoCC_D);'
      '  SaldoCC_D := 0;'
      'end;'
      ''
      'procedure PageFooter1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  nCC := nCC + 1;'
      '  nLI := 0;'
      'end;'
      ''
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      '  MePagin1.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 216
    Top = 56
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end
      item
        DataSet = DmodFin.frxDsExtratos
        DataSetName = 'frxDsExtratos'
      end
      item
        DataSet = DmodFin.frxDsSdoCtas
        DataSetName = 'frxDsSdoCtas'
      end
      item
        DataSet = DmodFin.frxDsSdoExcl
        DataSetName = 'frxDsSdoExcl'
      end
      item
        DataSet = DmodFin.frxDsSNG
        DataSetName = 'frxDsSNG'
      end
      item
        DataSet = DmodFin.frxDsSTCP
        DataSetName = 'frxDsSTCP'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader3: TfrxPageHeader
        Height = 109.606360240000000000
        Top = 105.826840000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'PageHeader3OnBeforePrint'
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo34: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo36: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagin1: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Top = 94.488250000000000000
          Width = 294.803340000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 347.716760000000000000
          Top = 94.488250000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 400.630180000000000000
          Top = 94.488250000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 476.220780000000000000
          Top = 94.488250000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 544.252320000000000000
          Top = 94.488250000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Top = 68.031540000000000000
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Extrato [frxDsExtratos."CTipN"]: [frxDsExtratos."CartN"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo306: TfrxMemoView
          Left = 612.283860000000000000
          Top = 94.488250000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 294.803340000000000000
          Top = 94.488250000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 45.354330710000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'GroupHeader1OnAfterPrint'
        OnBeforePrint = 'GroupHeader1OnBeforePrint'
        Condition = 'frxDsExtratos."CartC"'
        object Memo21: TfrxMemoView
          Top = 30.236240000000000000
          Width = 294.803340000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 347.716760000000000000
          Top = 30.236240000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 400.630180000000000000
          Top = 30.236240000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 476.220780000000000000
          Top = 30.236240000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 544.252320000000000000
          Top = 30.236240000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Extrato [frxDsExtratos."CTipN"]: [frxDsExtratos."CartN"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo307: TfrxMemoView
          Left = 612.283860000000000000
          Top = 30.236240000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 294.803340000000000000
          Top = 30.236240000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 15.118110240000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = DmodFin.frxDsExtratos
        DataSetName = 'frxDsExtratos'
        RowCount = 0
        object Memo26: TfrxMemoView
          Width = 294.803340000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtratos."Texto"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 347.716760000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsExtratos."NotaF">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 400.630180000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Docum'
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsExtratos."Docum"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 476.220780000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtratos."Credi"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 544.252320000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtratos."Debit"]')
          ParentFont = False
        end
        object Memo308: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtratos."Saldo"]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 294.803340000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsExtratos."DataM"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 37.795280470000000000
        Top = 381.732530000000000000
        Width = 680.315400000000000000
        object Memo31: TfrxMemoView
          Left = 476.220780000000000000
          Top = 15.118120000000000000
          Width = 204.094620000000000000
          Height = 22.677170240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsExtratos."Credi">)-SUM(<frxDsExtratos."Debit">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Top = 15.118120000000000000
          Width = 476.220780000000000000
          Height = 22.677170240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo no final do per'#237'odo:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Width = 544.252320000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 476.220780000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'Memo32OnBeforePrint'
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'ditos')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 544.252320000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'Memo33OnBeforePrint'
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bitos')
          ParentFont = False
        end
        object Memo310: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031500940000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 963.780150000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'PageFooter1OnBeforePrint'
        object Memo39: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'ReportTitle1OnBeforePrint'
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
      end
      object MasterData11: TfrxMasterData
        Height = 15.118120000000000000
        Top = 536.693260000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsSdoCtas
        DataSetName = 'frxDsSdoCtas'
        KeepTogether = True
        RowCount = 0
        object Memo227: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'Nome2'
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSdoCtas."Nome2"]')
          ParentFont = False
        end
        object Memo228: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'SDO_FIM'
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoCtas."SDO_FIM"]')
          ParentFont = False
        end
        object Memo229: TfrxMemoView
          Left = 544.252320000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'MOV_DEB'
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoCtas."MOV_DEB"]')
          ParentFont = False
        end
        object Memo230: TfrxMemoView
          Left = 340.157700000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'MOV_CRE'
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoCtas."MOV_CRE"]')
          ParentFont = False
        end
        object Memo231: TfrxMemoView
          Left = 272.126160000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoCtas."SDO_INI"]')
          ParentFont = False
        end
        object Memo232: TfrxMemoView
          Left = 476.220780000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoCtas."TRF_DEB"]')
          ParentFont = False
        end
        object Memo233: TfrxMemoView
          Left = 408.189240000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'TRF_CRE'
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoCtas."TRF_CRE"]')
          ParentFont = False
        end
      end
      object GroupHeader14: TfrxGroupHeader
        Height = 71.811070000000000000
        Top = 442.205010000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSdoCtas."KGT"'
        object Memo234: TfrxMemoView
          Top = 22.677180000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RESUMO DO MOVIMENTO NAS CONTAS CORRENTES E CAIXAS ORDINARIAS')
          ParentFont = False
        end
        object Memo235: TfrxMemoView
          Top = 56.692950000000000000
          Width = 272.126160000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta corrente')
          ParentFont = False
        end
        object Memo236: TfrxMemoView
          Left = 612.283860000000000000
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo237: TfrxMemoView
          Left = 544.252320000000000000
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo238: TfrxMemoView
          Left = 408.189240000000000000
          Top = 41.574830000000000000
          Width = 136.063036060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Transfer'#234'ncias')
          ParentFont = False
        end
        object Memo239: TfrxMemoView
          Left = 340.157700000000000000
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo240: TfrxMemoView
          Left = 272.126160000000000000
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo241: TfrxMemoView
          Left = 612.283860000000000000
          Top = 56.692950000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Final')
          ParentFont = False
        end
        object Memo242: TfrxMemoView
          Left = 544.252320000000000000
          Top = 56.692950000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'D'#233'bitos')
          ParentFont = False
        end
        object Memo243: TfrxMemoView
          Left = 340.157700000000000000
          Top = 56.692950000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cr'#233'ditos')
          ParentFont = False
        end
        object Memo244: TfrxMemoView
          Left = 272.126160000000000000
          Top = 56.692950000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Inicial')
          ParentFont = False
        end
        object Memo245: TfrxMemoView
          Left = 476.220780000000000000
          Top = 56.692950000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'D'#233'bitos')
          ParentFont = False
        end
        object Memo246: TfrxMemoView
          Left = 408.189240000000000000
          Top = 56.692950000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cr'#233'ditos')
          ParentFont = False
        end
      end
      object GroupFooter14: TfrxGroupFooter
        Height = 18.897650000000000000
        Top = 574.488560000000000000
        Width = 680.315400000000000000
        object Memo247: TfrxMemoView
          Left = 612.283860000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSdoCtas."SDO_FIM">)]')
          ParentFont = False
        end
        object Memo248: TfrxMemoView
          Left = 544.252320000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSdoCtas."MOV_DEB">)]')
          ParentFont = False
        end
        object Memo249: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSdoCtas."MOV_CRE">)]')
          ParentFont = False
        end
        object Memo250: TfrxMemoView
          Left = 272.126160000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSdoCtas."SDO_INI">)]')
          ParentFont = False
        end
        object Memo251: TfrxMemoView
          Left = 476.220780000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSdoCtas."TRF_DEB">)]')
          ParentFont = False
        end
        object Memo252: TfrxMemoView
          Left = 408.189240000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSdoCtas."TRF_CRE">)]')
          ParentFont = False
        end
        object Memo253: TfrxMemoView
          Top = 3.779530000000000000
          Width = 272.126160000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SALDO GERAL  ')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        Height = 15.118120000000000000
        Top = 695.433520000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsSdoExcl
        DataSetName = 'frxDsSdoExcl'
        KeepTogether = True
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 272.126160000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSdoExcl."Nome2"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoExcl."SDO_FIM"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 544.252320000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoExcl."MOV_DEB"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 340.157700000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoExcl."MOV_CRE"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 272.126160000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoExcl."SDO_INI"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 476.220780000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoExcl."TRF_DEB"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 408.189240000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoExcl."TRF_CRE"]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 56.692950000000000000
        Top = 616.063390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSdoExcl."KGT"'
        object Memo8: TfrxMemoView
          Top = 7.559060000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RESUMO DO MOVIMENTO NAS CONTAS CORRENTES E CAIXAS EXCLUSIVAS')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Top = 41.574830000000000000
          Width = 272.126160000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta corrente')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 612.283860000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 544.252320000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 408.189240000000000000
          Top = 26.456710000000000000
          Width = 136.063036060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Transfer'#234'ncias')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 340.157700000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 272.126160000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 612.283860000000000000
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Final')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 544.252320000000000000
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'D'#233'bitos')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 340.157700000000000000
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cr'#233'ditos')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 272.126160000000000000
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Inicial')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 476.220780000000000000
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'D'#233'bitos')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 408.189240000000000000
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cr'#233'ditos')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 733.228820000000000000
        Width = 680.315400000000000000
        object Memo54: TfrxMemoView
          Left = 612.283860000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSdoExcl."SDO_FIM">)]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 544.252320000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSdoExcl."MOV_DEB">)]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSdoExcl."MOV_CRE">)]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 272.126160000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSdoExcl."SDO_INI">)]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 476.220780000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSdoExcl."TRF_DEB">)]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 408.189240000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSdoExcl."TRF_CRE">)]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Top = 3.779530000000000000
          Width = 272.126160000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SALDO GERAL  ')
          ParentFont = False
        end
      end
      object GroupHeader15: TfrxGroupHeader
        Height = 41.574820240000000000
        Top = 778.583180000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSNG."KGT"'
        object Memo343: TfrxMemoView
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'SALDOS DE CONTAS CONTROLADAS')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo312: TfrxMemoView
          Left = 340.157700000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo anter.')
          ParentFont = False
        end
        object Memo313: TfrxMemoView
          Left = 408.189240000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'ditos')
          ParentFont = False
        end
        object Memo314: TfrxMemoView
          Top = 26.456710000000000000
          Width = 340.157700000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo315: TfrxMemoView
          Left = 544.252320000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Res. per'#237'odo')
          ParentFont = False
        end
        object Memo316: TfrxMemoView
          Left = 612.283860000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo final')
          ParentFont = False
        end
        object Memo317: TfrxMemoView
          Left = 476.220780000000000000
          Top = 26.456710000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bitos')
          ParentFont = False
        end
      end
      object GroupFooter15: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 880.630490000000000000
        Width = 680.315400000000000000
        object Memo337: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SDOANT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo338: TfrxMemoView
          Top = 3.779530000000000000
          Width = 340.157700000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Somas de valores de todas contas - controladas ou n'#227'o:  ')
          ParentFont = False
        end
        object Memo339: TfrxMemoView
          Left = 408.189240000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SUMCRE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo340: TfrxMemoView
          Left = 476.220780000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SUMDEB"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo341: TfrxMemoView
          Left = 544.252320000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SUMMOV"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo342: TfrxMemoView
          Left = 612.283860000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SDOFIM"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData14: TfrxMasterData
        Height = 15.118110240000000000
        Top = 842.835190000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsSNG
        DataSetName = 'frxDsSNG'
        KeepTogether = True
        RowCount = 0
        object Memo331: TfrxMemoView
          Left = 340.157700000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SdoAnt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo332: TfrxMemoView
          Left = 408.189240000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'SumCre'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SumCre"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo333: TfrxMemoView
          Width = 340.157700000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSNG."Ordena"] - [frxDsSNG."NomeGe"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo334: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'SdoFim'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SdoFim"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo335: TfrxMemoView
          Left = 544.252320000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'SumMov'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SumMov"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo336: TfrxMemoView
          Left = 476.220780000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SumDeb"]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxBal_A_06: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 244
    Top = 56
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end
      item
        DataSet = frxDsExclusivos
        DataSetName = 'frxDsExclusivos'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsOrdinarios
        DataSetName = 'frxDsOrdinarios'
      end
      item
        DataSet = DmodFin.frxDsSaldosNiv
        DataSetName = 'frxDsSaldosNiv'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object GH1: TfrxGroupHeader
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSaldosNiv."CO_Pla"'
      end
      object GH2: TfrxGroupHeader
        Top = 185.196970000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSaldosNiv."CO_Cjt"'
      end
      object GH3: TfrxGroupHeader
        Top = 207.874150000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSaldosNiv."CO_Gru"'
      end
      object GH4: TfrxGroupHeader
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSaldosNiv."CO_SGr"'
      end
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsSaldosNiv
        DataSetName = 'frxDsSaldosNiv'
        RowCount = 0
        object Memo20: TfrxMemoView
          Left = 151.181200000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSaldosNiv
          DataSetName = 'frxDsSaldosNiv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsSaldosNiv."CO_Cta">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 188.976500000000000000
          Width = 287.244280000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSaldosNiv
          DataSetName = 'frxDsSaldosNiv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSaldosNiv."NO_Cta"]')
          ParentFont = False
          WordWrap = False
        end
        object MeCta: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSaldosNiv
          DataSetName = 'frxDsSaldosNiv'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSaldosNiv."Movim"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 544.252320000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSaldosNiv
          DataSetName = 'frxDsSaldosNiv'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsSaldosNiv."Credito"> - <frxDsSaldosNiv."Debito">]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 476.220780000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSaldosNiv
          DataSetName = 'frxDsSaldosNiv'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSaldosNiv."SdoAnt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Width = 151.181200000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSaldosNiv
          DataSetName = 'frxDsSaldosNiv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 15.118110240000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          Left = 75.590600000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsSaldosNiv."CO_Gru">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 113.385900000000000000
          Width = 362.834880000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSaldosNiv."NO_Gru"]')
          ParentFont = False
          WordWrap = False
        end
        object MeGruFim: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSaldosNiv."Movim">)]')
          ParentFont = False
          WordWrap = False
        end
        object MeGruIni: TfrxMemoView
          Left = 476.220780000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSaldosNiv."SdoAnt">)]')
          ParentFont = False
          WordWrap = False
        end
        object MeGruDeb: TfrxMemoView
          Left = 544.252320000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxDsSaldosNiv."Credito">) - SUM(<frxDsSaldosNiv."Debito">' +
              ')]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 18.897637800000000000
        Top = 366.614410000000000000
        Width = 680.315400000000000000
        object Memo8: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsSaldosNiv."CO_Cjt">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 75.590600000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSaldosNiv."NO_Cjt"]')
          ParentFont = False
          WordWrap = False
        end
        object MeCjtFim: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSaldosNiv."Movim">)]')
          ParentFont = False
          WordWrap = False
        end
        object MeCjtIni: TfrxMemoView
          Left = 476.220780000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSaldosNiv."SdoAnt">)]')
          ParentFont = False
          WordWrap = False
        end
        object MeCjtDeb: TfrxMemoView
          Left = 544.252320000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxDsSaldosNiv."Credito">) - SUM(<frxDsSaldosNiv."Debito">' +
              ')]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 18.897650000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Memo4: TfrxMemoView
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsSaldosNiv."CO_Pla">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 37.795300000000000000
          Width = 442.205010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSaldosNiv."NO_Pla"]')
          ParentFont = False
          WordWrap = False
        end
        object MePlaFim: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSaldosNiv."Movim">)]')
          ParentFont = False
          WordWrap = False
        end
        object MePlaIni: TfrxMemoView
          Left = 476.220780000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSaldosNiv."SdoAnt">)]')
          ParentFont = False
          WordWrap = False
        end
        object MePlaDeb: TfrxMemoView
          Left = 544.252320000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxDsSaldosNiv."Credito">) - SUM(<frxDsSaldosNiv."Debito">' +
              ')]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        Height = 15.118110240000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        object Memo16: TfrxMemoView
          Left = 113.385900000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsSaldosNiv."CO_SGr">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 151.181200000000000000
          Width = 325.039580000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSaldosNiv."NO_SGr"]')
          ParentFont = False
          WordWrap = False
        end
        object MeSGRFim: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSaldosNiv."Movim">)]')
          ParentFont = False
          WordWrap = False
        end
        object MeSGRIni: TfrxMemoView
          Left = 476.220780000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSaldosNiv."SdoAnt">)]')
          ParentFont = False
          WordWrap = False
        end
        object MeSGRDeb: TfrxMemoView
          Left = 544.252320000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxDsSaldosNiv."Credito">) - SUM(<frxDsSaldosNiv."Debito">' +
              ')]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader3: TfrxPageHeader
        Height = 81.259886460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'PageHeader3OnBeforePrint'
        object Memo24: TfrxMemoView
          Top = 68.031540000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Plano')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 37.795300000000000000
          Top = 68.031540000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conjunto')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 75.590600000000000000
          Top = 68.031540000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 113.385900000000000000
          Top = 68.031540000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sub-grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 151.181200000000000000
          Top = 68.031540000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 188.976500000000000000
          Top = 68.031540000000000000
          Width = 287.244280000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 612.283860000000000000
          Top = 68.031540000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo final')
          ParentFont = False
          WordWrap = False
        end
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo18: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo36: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 544.252320000000000000
          Top = 68.031540000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Resultado m'#234's')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 476.220780000000000000
          Top = 68.031540000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo inicial')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118120000000000000
        Top = 551.811380000000000000
        Width = 680.315400000000000000
        object Memo39: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 41.574830000000000000
        Top = 487.559370000000000000
        Width = 680.315400000000000000
        object Memo22: TfrxMemoView
          Top = 3.779530000000000000
          Width = 476.220780000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Movimento de contas exclusivas')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 612.283860000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExclusivos."Movim"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 476.220780000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExclusivos."ANTERIOR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Left = 544.252320000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsExclusivos."CREDITO"> - <frxDsExclusivos."DEBITO">]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Top = 22.677180000000000000
          Width = 476.220780000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo sem movimento de contas exclusivas')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          Left = 612.283860000000000000
          Top = 22.677180000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSaldosNiv."Movim">) + <frxDsExclusivos."Movim">]'
            '')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          Left = 476.220780000000000000
          Top = 22.677180000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSaldosNiv."SdoAnt">) + <frxDsExclusivos."ANTERIOR">]'
            '')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          Left = 544.252320000000000000
          Top = 22.677180000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsSaldosNiv."Movim">) + <frxDsExclusivos."Movim">) - (' +
              'SUM(<frxDsSaldosNiv."SdoAnt">) + <frxDsExclusivos."ANTERIOR">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object QrLCS: TmySQLQuery
    Database = Dmod.MyDB
    Filter = 'Valor <> 0'
    Filtered = True
    SQL.Strings = (
      'SELECT pla.Nome NOMEPLANO, pla.Codigo PLANO, '
      'cjt.Nome NOMECONJUNTO, gru.Conjunto, '
      'gru.Nome NOMEGRUPO, sgr.Grupo, '
      'sgr.Nome NOMESUBGRUPO, con.SubGrupo, '
      'con.Nome NOMECONTA, mov.Genero, '
      'SUM(mov.Movim) Valor'
      'FROM contasmov mov'
      'LEFT JOIN contas    con ON con.Codigo=mov.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=mov.SubGrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=mov.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=mov.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=mov.Plano'
      'WHERE mov.CliInt=:P0'
      'AND mov.Mez <=:P1'
      'AND mov.Movim <> 0'
      'GROUP BY mov.Genero'
      'ORDER BY pla.OrdemLista, NOMEPLANO, cjt.OrdemLista, '
      'NOMECONJUNTO, gru.OrdemLista, NOMEGRUPO, sgr.OrdemLista, '
      'NOMESUBGRUPO, con.OrdemLista, NOMECONTA, mov.Mez')
    Left = 256
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLCSNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Origin = 'plano.Nome'
      Size = 50
    end
    object QrLCSPLANO: TIntegerField
      FieldName = 'PLANO'
      Origin = 'plano.Codigo'
    end
    object QrLCSNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 50
    end
    object QrLCSConjunto: TIntegerField
      FieldName = 'Conjunto'
      Origin = 'grupos.Conjunto'
    end
    object QrLCSNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 50
    end
    object QrLCSGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'subgrupos.Grupo'
    end
    object QrLCSNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 50
    end
    object QrLCSSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
      Origin = 'contas.Subgrupo'
    end
    object QrLCSNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrLCSGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'contasmov.Genero'
    end
    object QrLCSValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object frxDsLCS: TfrxDBDataset
    UserName = 'frxDsLCS'
    CloseDataSource = False
    DataSet = QrLC2
    BCDToCurrency = False
    Left = 284
    Top = 368
  end
  object frxBal_A_07: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 272
    Top = 56
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end
      item
        DataSet = frxDsExclusivos
        DataSetName = 'frxDsExclusivos'
      end
      item
        DataSet = frxDsLCS
        DataSetName = 'frxDsLCS'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsOrdinarios
        DataSetName = 'frxDsOrdinarios'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object GH1: TfrxGroupHeader
        Height = 26.456710000000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLCS."PLANO"'
        object Memo4: TfrxMemoView
          Top = 3.779529999999994000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Plano"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779529999999994000
          Width = 491.338900000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMEPLANO"]')
          ParentFont = False
          WordWrap = False
        end
        object MePla: TfrxMemoView
          Left = 529.134200000000000000
          Top = 3.779529999999994000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."vPLA"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH2: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLCS."Conjunto"'
        object Memo8: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Conjunto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 75.590600000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMECONJUNTO"]')
          ParentFont = False
          WordWrap = False
        end
        object MeCjt: TfrxMemoView
          Left = 529.134200000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."vCJT"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH3: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLCS."Grupo"'
        object Memo12: TfrxMemoView
          Left = 75.590600000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Grupo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 113.385900000000000000
          Width = 415.748300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMEGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object MeGru: TfrxMemoView
          Left = 529.134200000000000000
          Width = 151.181200000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."vGRU"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH4: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLCS."SubGrupo"'
        object Memo16: TfrxMemoView
          Left = 113.385900000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Subgrupo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 151.181200000000000000
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMESUBGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object MeSGR: TfrxMemoView
          Left = 529.134200000000000000
          Width = 151.181200000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."vSGR"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        DataSet = frxDsLCS
        DataSetName = 'frxDsLCS'
        RowCount = 0
        object Memo20: TfrxMemoView
          Left = 151.181200000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Genero'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Genero"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 188.976500000000000000
          Width = 340.157700000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object MeCta: TfrxMemoView
          Left = 529.134200000000000000
          Width = 151.181200000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Valor'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."Valor"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 41.574830000000000000
        Top = 555.590910000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          Top = 3.779530000000022000
          Width = 529.134200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Movimento de contas exclusivas')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 529.134200000000000000
          Top = 3.779530000000022000
          Width = 151.181156060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExclusivos."Movim"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Top = 22.677180000000020000
          Width = 529.134200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo sem movimento de contas exclusivas')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          Left = 529.134200000000000000
          Top = 22.677180000000020000
          Width = 151.181156060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">) + <frxDsExclusivos."Movim">]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 15.118110240000000000
        Top = 404.409710000000000000
        Visible = False
        Width = 680.315400000000000000
        object Memo10: TfrxMemoView
          Width = 445.984540000000000000
          Height = 15.118120000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">)]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 15.118110240000000000
        Top = 442.205010000000000000
        Visible = False
        Width = 680.315400000000000000
        object Memo11: TfrxMemoView
          Width = 445.984540000000000000
          Height = 15.118120000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">)]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 480.000310000000000000
        Visible = False
        Width = 680.315400000000000000
        object Memo6: TfrxMemoView
          Width = 445.984540000000000000
          Height = 15.118120000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">)]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        Height = 15.118110240000000000
        Top = 366.614410000000000000
        Visible = False
        Width = 680.315400000000000000
        object Memo7: TfrxMemoView
          Width = 445.984540000000000000
          Height = 15.118120000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">)]')
          ParentFont = False
        end
      end
      object PageHeader3: TfrxPageHeader
        Height = 81.259886460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo24: TfrxMemoView
          Top = 68.031540000000010000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Plano')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 37.795300000000000000
          Top = 68.031540000000010000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conjunto')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 75.590600000000000000
          Top = 68.031540000000010000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 113.385900000000000000
          Top = 68.031540000000010000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sub-grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 151.181200000000000000
          Top = 68.031540000000010000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 188.976500000000000000
          Top = 68.031540000000010000
          Width = 340.157700000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 529.134200000000000000
          Top = 68.031540000000010000
          Width = 151.181200000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
          WordWrap = False
        end
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo22: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo36: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118120000000000000
        Top = 619.842920000000000000
        Width = 680.315400000000000000
        object Memo39: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxBal_A_08: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 300
    Top = 56
    Datasets = <
      item
        DataSet = DmodFin.frxDsCreditos
        DataSetName = 'frxDsCreditos'
      end
      item
        DataSet = DmodFin.frxDsDebitos
        DataSetName = 'frxDsDebitos'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end
      item
        DataSet = frxDsExclusivos
        DataSetName = 'frxDsExclusivos'
      end
      item
        DataSet = frxDsOrdinarios
        DataSetName = 'frxDsOrdinarios'
      end
      item
        DataSet = DmodFin.frxDsResumo
        DataSetName = 'frxDsResumo'
      end
      item
        DataSet = DmodFin.frxDsSaldoA
        DataSetName = 'frxDsSaldoA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCreditos."NOMEGRU"'
        object Memo51: TfrxMemoView
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCreditos."NOMEGRU"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valores em R$')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 521.575140000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 257.008040000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCreditos."NOMESGR"'
        object Memo53: TfrxMemoView
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCreditos."NOMESGR"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Left = 521.575140000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        Height = 15.118120000000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsCreditos
        DataSetName = 'frxDsCreditos'
        RowCount = 0
        object Memo55: TfrxMemoView
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'NOMECON_2'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCreditos."NOMECON_2"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCreditos."Credito"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 521.575140000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'MES'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCreditos."MES"]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 332.598640000000000000
        Width = 680.315400000000000000
        object Memo58: TfrxMemoView
          Width = 521.575140000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCreditos."Credito">)]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 521.575140000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 370.393940000000000000
        Width = 680.315400000000000000
        object Memo60: TfrxMemoView
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de [frxDsCreditos."NOMEGRU"] ')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCreditos."Credito">)]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 510.236550000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDebitos."NOMEGRU"'
        object Memo64: TfrxMemoView
          Width = 366.614410000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."NOMEGRU"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 411.968770000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Baixa')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 476.220780000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 551.811380000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Nota fiscal')
          ParentFont = False
        end
      end
      object GroupHeader5: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 548.031850000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDebitos."NOMESGR"'
        object Memo70: TfrxMemoView
          Width = 366.614410000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDebitos."NOMESGR"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 411.968770000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 476.220780000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 551.811380000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        Height = 15.118120000000000000
        Top = 623.622450000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsDebitos
        DataSetName = 'frxDsDebitos'
        RowCount = 0
        object Memo76: TfrxMemoView
          Width = 366.614410000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '    [frxDsDebitos."Descricao"]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDebitos."Debito"]')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'MES'
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."MES"]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 411.968770000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'COMPENSADO_TXT'
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."COMPENSADO_TXT"]')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 476.220780000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'SERIE_DOC'
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."SERIE_DOC"]')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 551.811380000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NF_TXT'
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."NF_TXT"]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 699.213050000000000000
        Width = 680.315400000000000000
        object Memo82: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Width = 366.614410000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sub-total de [frxDsDebitos."NOMESGR"]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Left = 411.968770000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Left = 476.220780000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Left = 551.811380000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter5: TfrxGroupFooter
        Height = 26.456690470000000000
        Top = 737.008350000000000000
        Width = 680.315400000000000000
        object Memo83: TfrxMemoView
          Left = 589.606680000000000000
          Width = 90.708720000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Width = 589.606680000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de [frxDsDebitos."NOMEGRU"] ')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 1050.709340000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 158.740260000000000000
        Top = 869.291900000000000000
        Width = 680.315400000000000000
        object Memo85: TfrxMemoView
          Top = 22.677180000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'RESUMO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          Top = 41.574830000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo Anterior:')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Left = 566.929500000000000000
          Top = 41.574830000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSaldoA."TOTAL"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Top = 56.692950000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de receitas:')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Left = 566.929500000000000000
          Top = 56.692950000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."Credito"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Top = 71.811070000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de despesas:')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Top = 86.929190000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo do m'#234's:')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Left = 566.929500000000000000
          Top = 71.811070000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."Debito"]')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 566.929500000000000000
          Top = 86.929190000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."SALDO"]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Top = 102.047310000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SALDO A SER TRANSFERIDO:')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 566.929500000000000000
          Top = 102.047310000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."FINAL"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Top = 128.504020000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo das contas exclusivas:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 566.929500000000000000
          Top = 128.504020000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExclusivos."Movim"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 143.622140000000000000
          Width = 566.929500000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SALDO SEM AS CONTAS EXCLUSIVAS:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 566.929500000000000000
          Top = 143.622140000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDsExclusivos."Movim"> + <frxDsResumo."FINAL">]')
          ParentFont = False
        end
      end
      object GroupHeader9: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 585.827150000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDebitos."NOMECON"'
        object Memo4: TfrxMemoView
          Width = 366.614410000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  [frxDsDebitos."NOMECON"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 411.968770000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 476.220780000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 551.811380000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter9: TfrxGroupFooter
        Height = 15.118110240000000000
        Top = 661.417750000000000000
        Width = 680.315400000000000000
        object Memo10: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Width = 366.614410000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  Sub-total de [frxDsDebitos."NOMECON"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 366.614410000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Left = 411.968770000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo198: TfrxMemoView
          Left = 476.220780000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo199: TfrxMemoView
          Left = 551.811380000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 34.015770000000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        object Memo200: TfrxMemoView
          Left = 3.779530000000000000
          Top = 11.338590000000000000
          Width = 676.535870000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'RECEITAS')
          ParentFont = False
        end
      end
      object Footer4: TfrxFooter
        Height = 34.015770000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Memo201: TfrxMemoView
          Top = 7.559060000000000000
          Width = 521.575140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL RECEITAS:')
          ParentFont = False
        end
        object Memo202: TfrxMemoView
          Left = 521.575140000000000000
          Top = 7.559060000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCreditos."Credito">,MasterData2)]')
          ParentFont = False
        end
      end
      object Header4: TfrxHeader
        Height = 22.677180000000000000
        Top = 464.882190000000000000
        Width = 680.315400000000000000
        object Memo203: TfrxMemoView
          Left = 3.779530000000000000
          Width = 676.535870000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'DESPESAS')
          ParentFont = False
        end
      end
      object Footer6: TfrxFooter
        Height = 22.677180000000000000
        Top = 786.142240000000000000
        Width = 680.315400000000000000
        object Memo204: TfrxMemoView
          Width = 521.575140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DESPESAS:')
          ParentFont = False
        end
        object Memo205: TfrxMemoView
          Left = 521.575140000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">,MasterData3)]')
          ParentFont = False
        end
      end
    end
  end
  object QrCtasResMes: TmySQLQuery
    Database = DModG.MyLocDB
    OnCalcFields = QrCtasResMesCalcFields
    SQL.Strings = (
      'SELECT * FROM ctasresmes'
      'WHERE SeqImp=-1 OR Periodo BETWEEN :P0 AND :P1'
      'ORDER BY Nome, Conta, SeqImp, Periodo')
    Left = 256
    Top = 396
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCtasResMesConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCtasResMesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCtasResMesPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrCtasResMesTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCtasResMesFator: TFloatField
      FieldName = 'Fator'
    end
    object QrCtasResMesValFator: TFloatField
      FieldName = 'ValFator'
    end
    object QrCtasResMesDevido: TFloatField
      FieldName = 'Devido'
    end
    object QrCtasResMesPago: TFloatField
      FieldName = 'Pago'
    end
    object QrCtasResMesDiferenca: TFloatField
      FieldName = 'Diferenca'
    end
    object QrCtasResMesAcumulado: TFloatField
      FieldName = 'Acumulado'
    end
    object QrCtasResMesNOME_PERIODO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_PERIODO'
      Size = 100
      Calculated = True
    end
  end
  object frxDsCtasResMes: TfrxDBDataset
    UserName = 'frxDsCtasResMes'
    CloseDataSource = False
    DataSet = QrCtasResMes
    BCDToCurrency = False
    Left = 284
    Top = 396
  end
  object frxBal_A_09: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38101.979964398100000000
    ReportOptions.LastChange = 39722.438154294000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 328
    Top = 56
    Datasets = <
      item
        DataSet = frxDsCtasResMes
        DataSetName = 'frxDsCtasResMes'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 190.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object DadosMestre1: TfrxMasterData
        Height = 17.007874015748030000
        Top = 222.992270000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCtasResMes
        DataSetName = 'frxDsCtasResMes'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = -0.000048820000000000
          Width = 113.385851180000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCtasResMes."NOME_PERIODO"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 113.385802360000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtasResMes."Fator"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 207.874003540000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtasResMes."ValFator"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 302.362204720000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtasResMes."Devido"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 396.960730000000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtasResMes."Pago"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 491.338607090000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtasResMes."Diferenca"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 585.826808270000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCtasResMes."Acumulado"]')
          ParentFont = False
        end
      end
      object TfrxGroupHeader
        Height = 37.228344020000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsCtasResMes."Nome"'
        object Memo10: TfrxMemoView
          Left = -0.000048820000000000
          Top = 20.220470000000000000
          Width = 113.385851180000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 113.385802360000000000
          Top = 20.220470000000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Fator')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 207.874003540000000000
          Top = 20.220470000000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor refer'#234'ncia')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 302.362204720000000000
          Top = 20.220470000000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor a pagar')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 396.960730000000000000
          Top = 20.220470000000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor pago')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 491.338607090000000000
          Top = 20.220470000000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Diferen'#231'a no m'#234's')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 585.826808270000000000
          Top = 20.220470000000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor acumulado')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = -0.000048820000000000
          Width = 680.314960630000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCtasResMes."Conta"] - [frxDsCtasResMes."Nome"]')
          ParentFont = False
        end
      end
      object TfrxGroupFooter
        Height = 21.354194020000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        object Memo8: TfrxMemoView
          Left = 208.929190000000000000
          Top = 4.346320000000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCtasResMes."ValFator">)]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 302.944960000000000000
          Top = 4.346320000000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCtasResMes."Devido">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 396.960730000000000000
          Top = 4.346320000000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCtasResMes."Pago">)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 1.527520000000000000
          Top = 4.346320000000000000
          Width = 207.401670000000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Total per'#237'odo:')
          ParentFont = False
        end
      end
      object PageHeader4: TfrxPageHeader
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 347.716760000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object QrExclusivos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrExclusivosCalcFields
    SQL.Strings = (
      'SELECT SUM(mov.Movim) Movim'
      'FROM contasmov mov'
      'LEFT JOIN contas cta ON cta.Codigo=mov.Genero'
      'WHERE cta.Exclusivo="V"'
      'AND mov.CliInt=:P0'
      'AND mov.Mez>:P1')
    Left = 256
    Top = 424
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrExclusivosMovim: TFloatField
      FieldName = 'Movim'
    end
    object QrExclusivosCREDITO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CREDITO'
      Calculated = True
    end
    object QrExclusivosDEBITO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DEBITO'
      Calculated = True
    end
    object QrExclusivosANTERIOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ANTERIOR'
      Calculated = True
    end
  end
  object frxDsExclusivos: TfrxDBDataset
    UserName = 'frxDsExclusivos'
    CloseDataSource = False
    DataSet = QrExclusivos
    BCDToCurrency = False
    Left = 284
    Top = 424
  end
  object QrExclMov: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito'
      'FROM contasmov mov'
      'LEFT JOIN contas cta ON cta.Codigo=mov.Genero'
      'WHERE Exclusivo="V"'
      'AND mov.CliInt=:P0'
      'AND mov.Mez=:P1')
    Left = 256
    Top = 452
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrExclMovCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrExclMovDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrOrdinarios: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOrdinariosCalcFields
    SQL.Strings = (
      'SELECT SUM(mov.Movim) Movim'
      'FROM contasmov mov'
      'LEFT JOIN contas cta ON cta.Codigo=mov.Genero'
      'WHERE Exclusivo<>"V"'
      'AND mov.CliInt=:P0'
      'AND mov.Mez<=:P1')
    Left = 312
    Top = 424
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOrdinariosMovim: TFloatField
      FieldName = 'Movim'
    end
    object QrOrdinariosCREDITO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CREDITO'
      Calculated = True
    end
    object QrOrdinariosDEBITO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DEBITO'
      Calculated = True
    end
    object QrOrdinariosANTERIOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ANTERIOR'
      Calculated = True
    end
  end
  object QrOrdiMov: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito'
      'FROM contasmov mov'
      'LEFT JOIN contas cta ON cta.Codigo=mov.Genero'
      'WHERE Exclusivo<>"V"'
      'AND mov.CliInt=:P0'
      'AND mov.Mez=:P1')
    Left = 312
    Top = 452
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOrdiMovCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrOrdiMovDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object frxDsOrdinarios: TfrxDBDataset
    UserName = 'frxDsOrdinarios'
    CloseDataSource = False
    DataSet = QrOrdinarios
    BCDToCurrency = False
    Left = 340
    Top = 424
  end
  object QrDat1: TmySQLQuery
    Database = DModG.MyLocDB
    OnCalcFields = QrDat1CalcFields
    Left = 148
    Top = 404
    object QrDat1Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDat1NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrDat1Valr01: TFloatField
      FieldName = 'Valr01'
    end
    object QrDat1Valr02: TFloatField
      FieldName = 'Valr02'
    end
    object QrDat1Valr03: TFloatField
      FieldName = 'Valr03'
    end
    object QrDat1Valr04: TFloatField
      FieldName = 'Valr04'
    end
    object QrDat1Valr05: TFloatField
      FieldName = 'Valr05'
    end
    object QrDat1Valr06: TFloatField
      FieldName = 'Valr06'
    end
    object QrDat1Valr07: TFloatField
      FieldName = 'Valr07'
    end
    object QrDat1Valr08: TFloatField
      FieldName = 'Valr08'
    end
    object QrDat1Valr09: TFloatField
      FieldName = 'Valr09'
    end
    object QrDat1Valr10: TFloatField
      FieldName = 'Valr10'
    end
    object QrDat1Valr11: TFloatField
      FieldName = 'Valr11'
    end
    object QrDat1Valr12: TFloatField
      FieldName = 'Valr12'
    end
    object QrDat1ValrA0: TFloatField
      FieldName = 'ValrA0'
    end
    object QrDat1ValrM0: TFloatField
      FieldName = 'ValrM0'
    end
    object QrDat1ValrA1: TFloatField
      FieldName = 'ValrA1'
    end
    object QrDat1ValrM1: TFloatField
      FieldName = 'ValrM1'
    end
    object QrDat1SEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object QrDat2: TmySQLQuery
    Database = DModG.MyLocDB
    OnCalcFields = QrDat2CalcFields
    Left = 148
    Top = 432
    object QrDat2Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDat2NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrDat2Valr01: TFloatField
      FieldName = 'Valr01'
    end
    object QrDat2Valr02: TFloatField
      FieldName = 'Valr02'
    end
    object QrDat2Valr03: TFloatField
      FieldName = 'Valr03'
    end
    object QrDat2Valr04: TFloatField
      FieldName = 'Valr04'
    end
    object QrDat2Valr05: TFloatField
      FieldName = 'Valr05'
    end
    object QrDat2Valr06: TFloatField
      FieldName = 'Valr06'
    end
    object QrDat2Valr07: TFloatField
      FieldName = 'Valr07'
    end
    object QrDat2Valr08: TFloatField
      FieldName = 'Valr08'
    end
    object QrDat2Valr09: TFloatField
      FieldName = 'Valr09'
    end
    object QrDat2Valr10: TFloatField
      FieldName = 'Valr10'
    end
    object QrDat2Valr11: TFloatField
      FieldName = 'Valr11'
    end
    object QrDat2Valr12: TFloatField
      FieldName = 'Valr12'
    end
    object QrDat2ValrA0: TFloatField
      FieldName = 'ValrA0'
    end
    object QrDat2ValrM0: TFloatField
      FieldName = 'ValrM0'
    end
    object QrDat2ValrA1: TFloatField
      FieldName = 'ValrA1'
    end
    object QrDat2ValrM1: TFloatField
      FieldName = 'ValrM1'
    end
    object QrDat2SEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object QrLC2: TmySQLQuery
    Database = DModG.MyPID_DB
    Filter = 'Valor <> 0'
    Filtered = True
    SQL.Strings = (
      'SELECT * FROM _LCS_'
      'ORDER BY OL_PLA, NOMEPLANO, OL_CJT,'
      'NOMECONJUNTO, OL_GRU, NOMEGRUPO, OL_SGR,'
      'NOMESUBGRUPO, OL_CON, NOMECONTA')
    Left = 228
    Top = 368
    object QrLC2NOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Origin = '_lcs_.NOMEPLANO'
      Size = 50
    end
    object QrLC2PLANO: TIntegerField
      FieldName = 'PLANO'
      Origin = '_lcs_.PLANO'
    end
    object QrLC2NOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = '_lcs_.NOMECONJUNTO'
      Size = 50
    end
    object QrLC2Conjunto: TIntegerField
      FieldName = 'Conjunto'
      Origin = '_lcs_.Conjunto'
    end
    object QrLC2NOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = '_lcs_.NOMEGRUPO'
      Size = 50
    end
    object QrLC2Grupo: TIntegerField
      FieldName = 'Grupo'
      Origin = '_lcs_.Grupo'
    end
    object QrLC2NOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = '_lcs_.NOMESUBGRUPO'
      Size = 50
    end
    object QrLC2SubGrupo: TIntegerField
      FieldName = 'SubGrupo'
      Origin = '_lcs_.SubGrupo'
    end
    object QrLC2NOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = '_lcs_.NOMECONTA'
      Size = 50
    end
    object QrLC2Genero: TIntegerField
      FieldName = 'Genero'
      Origin = '_lcs_.Genero'
    end
    object QrLC2Valor: TFloatField
      FieldName = 'Valor'
      Origin = '_lcs_.Valor'
    end
    object QrLC2OL_PLA: TIntegerField
      FieldName = 'OL_PLA'
      Origin = '_lcs_.OL_PLA'
    end
    object QrLC2OL_CJT: TIntegerField
      FieldName = 'OL_CJT'
      Origin = '_lcs_.OL_CJT'
    end
    object QrLC2OL_GRU: TIntegerField
      FieldName = 'OL_GRU'
      Origin = '_lcs_.OL_GRU'
    end
    object QrLC2OL_SGR: TIntegerField
      FieldName = 'OL_SGR'
      Origin = '_lcs_.OL_SGR'
    end
    object QrLC2OL_CON: TIntegerField
      FieldName = 'OL_CON'
      Origin = '_lcs_.OL_CON'
    end
    object QrLC2vSGR: TFloatField
      FieldName = 'vSGR'
      Origin = '_lcs_.vSGR'
    end
    object QrLC2vGRU: TFloatField
      FieldName = 'vGRU'
      Origin = '_lcs_.vGRU'
    end
    object QrLC2vCJT: TFloatField
      FieldName = 'vCJT'
      Origin = '_lcs_.vCJT'
    end
    object QrLC2vPLA: TFloatField
      FieldName = 'vPLA'
      Origin = '_lcs_.vPLA'
    end
  end
  object QrSSdo: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT PLANO Codigo, SUM(Valor) Valor'
      'FROM _lcs_'
      'GROUP BY PLANO')
    Left = 452
    Top = 416
    object QrSSdoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSSdoValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object frxBal_A_07_: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MePlaOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Engine.FinalPass then'
      '    MePla.Text := Format('#39'%2.2n'#39',[Get(<frxDsLCS."NOMEPLANO">)]);'
      'end;'
      ''
      'procedure MeCjtOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Engine.FinalPass then'
      
        '    MeCjt.Text := Format('#39'%2.2n'#39',[Get(<frxDsLCS."NOMECONJUNTO">)' +
        ']);'
      'end;'
      ''
      'procedure MeGruOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Engine.FinalPass then'
      '    MeGru.Text := Format('#39'%2.2n'#39',[Get(<frxDsLCS."NOMEGRUPO">)]);'
      'end;'
      ''
      'procedure MeSGROnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if Engine.FinalPass then'
      
        '    MeSGR.Text := Format('#39'%2.2n'#39',[Get(<frxDsLCS."NOMESUBGRUPO">)' +
        ']);'
      'end;'
      ''
      'procedure GroupFooter3OnBeforePrint(Sender: TfrxComponent);'
      
        'begin                                                           ' +
        '             '
      '  Set(<frxDsLCS."NOMEPLANO">,    Sum(<frxDsLCS."Valor">));  '
      'end;'
      ''
      'procedure GroupFooter2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Set(<frxDsLCS."NOMECONJUNTO">, Sum(<frxDsLCS."Valor">));'
      'end;'
      ''
      'procedure GroupFooter1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Set(<frxDsLCS."NOMEGRUPO">,    Sum(<frxDsLCS."Valor">));  '
      'end;'
      ''
      'procedure GroupFooter4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Set(<frxDsLCS."NOMESUBGRUPO">, Sum(<frxDsLCS."Valor">));  '
      'end;'
      ''
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 272
    Top = 84
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end
      item
        DataSet = frxDsExclusivos
        DataSetName = 'frxDsExclusivos'
      end
      item
        DataSet = frxDsLCS
        DataSetName = 'frxDsLCS'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsOrdinarios
        DataSetName = 'frxDsOrdinarios'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object GH1: TfrxGroupHeader
        Height = 26.456710000000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLCS."PLANO"'
        object Memo4: TfrxMemoView
          Top = 3.779529999999994000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Plano"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779529999999994000
          Width = 491.338900000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMEPLANO"]')
          ParentFont = False
          WordWrap = False
        end
        object MePla: TfrxMemoView
          Left = 529.134200000000000000
          Top = 3.779529999999994000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'MePlaOnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object GH2: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLCS."Conjunto"'
        object Memo8: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Conjunto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 75.590600000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMECONJUNTO"]')
          ParentFont = False
          WordWrap = False
        end
        object MeCjt: TfrxMemoView
          Left = 529.134200000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          OnBeforePrint = 'MeCjtOnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object GH3: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLCS."Grupo"'
        object Memo12: TfrxMemoView
          Left = 75.590600000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Grupo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 113.385900000000000000
          Width = 415.748300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMEGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object MeGru: TfrxMemoView
          Left = 529.134200000000000000
          Width = 151.181200000000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'MeGruOnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object GH4: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsLCS."SubGrupo"'
        object Memo16: TfrxMemoView
          Left = 113.385900000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Subgrupo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 151.181200000000000000
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMESUBGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object MeSGR: TfrxMemoView
          Left = 529.134200000000000000
          Width = 151.181200000000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'MeSGROnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        DataSet = frxDsLCS
        DataSetName = 'frxDsLCS'
        RowCount = 0
        object Memo20: TfrxMemoView
          Left = 151.181200000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Genero'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."Genero"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 188.976500000000000000
          Width = 340.157700000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object MeCta: TfrxMemoView
          Left = 529.134200000000000000
          Width = 151.181200000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Valor'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."Valor"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 41.574830000000000000
        Top = 555.590910000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          Top = 3.779530000000022000
          Width = 529.134200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Movimento de contas exclusivas')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 529.134200000000000000
          Top = 3.779530000000022000
          Width = 151.181156060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExclusivos."Movim"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Top = 22.677180000000020000
          Width = 529.134200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo sem movimento de contas exclusivas')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          Left = 529.134200000000000000
          Top = 22.677180000000020000
          Width = 151.181156060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">) + <frxDsExclusivos."Movim">]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 15.118110240000000000
        Top = 404.409710000000000000
        Visible = False
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupFooter1OnBeforePrint'
        object Memo10: TfrxMemoView
          Width = 445.984540000000000000
          Height = 15.118120000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">)]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 15.118110240000000000
        Top = 442.205010000000000000
        Visible = False
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupFooter2OnBeforePrint'
        object Memo11: TfrxMemoView
          Width = 445.984540000000000000
          Height = 15.118120000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">)]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 480.000310000000000000
        Visible = False
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupFooter3OnBeforePrint'
        object Memo6: TfrxMemoView
          Width = 445.984540000000000000
          Height = 15.118120000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">)]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        Height = 15.118110240000000000
        Top = 366.614410000000000000
        Visible = False
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupFooter4OnBeforePrint'
        object Memo7: TfrxMemoView
          Width = 445.984540000000000000
          Height = 15.118120000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Sum(<frxDsLCS."Valor">)]')
          ParentFont = False
        end
      end
      object PageHeader3: TfrxPageHeader
        Height = 81.259886460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo24: TfrxMemoView
          Top = 68.031540000000010000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Plano')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 37.795300000000000000
          Top = 68.031540000000010000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conjunto')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 75.590600000000000000
          Top = 68.031540000000010000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 113.385900000000000000
          Top = 68.031540000000010000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sub-grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 151.181200000000000000
          Top = 68.031540000000010000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 188.976500000000000000
          Top = 68.031540000000010000
          Width = 340.157700000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 529.134200000000000000
          Top = 68.031540000000010000
          Width = 151.181200000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
          WordWrap = False
        end
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo22: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo36: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118120000000000000
        Top = 619.842920000000000000
        Width = 680.315400000000000000
        object Memo39: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object QrSN1: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT SUM(Movim) Movim'
      'FROM saldosniv')
    Left = 596
    Top = 428
    object QrSN1Movim: TFloatField
      FieldName = 'Movim'
    end
  end
  object QrSN2: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor'
      'FROM _lcs_')
    Left = 624
    Top = 428
    object QrSN2Valor: TFloatField
      FieldName = 'Valor'
    end
  end
  object frxBal_A_11: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40318.348259641210000000
    ReportOptions.LastChange = 40318.348259641210000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 356
    Top = 56
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end
      item
        DataSet = DmodFin.frxDsPrevItO
        DataSetName = 'frxDsPrevItO'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader5: TfrxPageHeader
        Height = 98.267770240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo114: TfrxMemoView
          Top = 83.149660000000000000
          Width = 566.929500000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sub-grupo / Conta (do plano de contas)')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Left = 566.929500000000000000
          Top = 83.149660000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object GroupHeader6: TfrxGroupHeader
        Height = 15.118110240000000000
        Top = 177.637910000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPrevItO."NOMESUBGRUPO"'
        object Memo110: TfrxMemoView
          Width = 566.929500000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrevItO."NOMESUBGRUPO"]')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        Height = 15.118110240000000000
        Top = 215.433210000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsPrevItO
        DataSetName = 'frxDsPrevItO'
        RowCount = 0
        object Memo111: TfrxMemoView
          Width = 566.929500000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = DmodFin.frxDsPrevItO
          DataSetName = 'frxDsPrevItO'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPrevItO."NOMECONTA"]')
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Valor'
          DataSet = DmodFin.frxDsPrevItO
          DataSetName = 'frxDsPrevItO'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPrevItO."Valor"]')
          ParentFont = False
        end
      end
      object GroupFooter6: TfrxGroupFooter
        Height = 15.118110240000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        object Memo116: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrevItO."Valor">)]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Width = 566.929500000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 22.677180000000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        object Memo118: TfrxMemoView
          Left = 566.929500000000000000
          Top = 7.559060000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPrevItO."Valor">)]')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Top = 7.559060000000000000
          Width = 566.929500000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  ')
          ParentFont = False
        end
      end
      object PageFooter3: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 374.173470000000000000
        Width = 680.315400000000000000
        object Memo120: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxBal_A_12B: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40318.458037951390000000
    ReportOptions.LastChange = 40318.458037951390000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 384
    Top = 84
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end
      item
        DataSet = DmodFin.frxDsPgBloq
        DataSetName = 'frxDsPgBloq'
      end
      item
        DataSet = DmodFin.frxDsSuBloq
        DataSetName = 'frxDsSuBloq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader6: TfrxPageHeader
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object GroupHeader7: TfrxGroupHeader
        Height = 41.574812910000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPgBloq."Documento"'
        KeepTogether = True
        object Memo157: TfrxMemoView
          Top = 26.456710000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade/Locat'#225'rio/Conta')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Left = 325.039580000000000000
          Top = 26.456710000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Titulo')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Left = 151.181200000000000000
          Top = 26.456710000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Left = 196.535560000000000000
          Top = 26.456710000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Left = 260.787570000000000000
          Top = 26.456710000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pagamento')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Left = 419.527830000000000000
          Top = 26.456710000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Left = 487.559370000000000000
          Top = 26.456710000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Left = 555.590910000000000000
          Top = 26.456710000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Left = 616.063390000000000000
          Top = 26.456710000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade habitacional: [frxDsPgBloq."UH"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo167: TfrxMemoView
          Top = 13.228346460000000000
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPgBloq."NOMEPROPRIET"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData6: TfrxMasterData
        Height = 13.228346460000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsPgBloq
        DataSetName = 'frxDsPgBloq'
        KeepTogether = True
        RowCount = 0
        object Memo168: TfrxMemoView
          Width = 151.181200000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPgBloq."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo169: TfrxMemoView
          Left = 325.039580000000000000
          Width = 94.488250000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Documento'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."Documento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo170: TfrxMemoView
          Left = 151.181200000000000000
          Width = 45.354360000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'MES'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."MES"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo171: TfrxMemoView
          Left = 196.535560000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Vencimento'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."Vencimento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo172: TfrxMemoView
          Left = 260.787570000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DATA_TXT'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."DATA_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo173: TfrxMemoView
          Left = 419.527830000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'ORIGINAL'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."ORIGINAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo174: TfrxMemoView
          Left = 487.559370000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Credito'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."Credito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo175: TfrxMemoView
          Left = 555.590910000000000000
          Width = 60.472480000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'MultaVal'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."MultaVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo176: TfrxMemoView
          Left = 616.063390000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'MoraVal'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."MoraVal"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter7: TfrxGroupFooter
        Height = 13.228346460000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        object Memo177: TfrxMemoView
          Width = 419.527830000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              'Total do t'#237'tulo [frxDsPgBloq."Documento"] da unidade habitaciona' +
              'l [frxDsPgBloq."UH"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo178: TfrxMemoView
          Left = 419.527830000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."ORIGINAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo179: TfrxMemoView
          Left = 487.559370000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo180: TfrxMemoView
          Left = 555.590910000000000000
          Width = 60.472480000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."MultaVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo181: TfrxMemoView
          Left = 616.063390000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."MoraVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter4: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 495.118430000000000000
        Width = 680.315400000000000000
        object Memo182: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData7: TfrxMasterData
        Height = 13.228346460000000000
        Top = 374.173470000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsSuBloq
        DataSetName = 'frxDsSuBloq'
        KeepTogether = True
        RowCount = 0
        object Memo188: TfrxMemoView
          Width = 347.716760000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = DmodFin.frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSuBloq."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo189: TfrxMemoView
          Left = 347.716760000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'ORIGINAL'
          DataSet = DmodFin.frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."ORIGINAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo190: TfrxMemoView
          Left = 430.866420000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'PAGO'
          DataSet = DmodFin.frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."PAGO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo191: TfrxMemoView
          Left = 514.016080000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'MultaVal'
          DataSet = DmodFin.frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."MultaVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo192: TfrxMemoView
          Left = 597.165740000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'MoraVal'
          DataSet = DmodFin.frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."MoraVal"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader13: TfrxGroupHeader
        Height = 49.133890000000000000
        Top = 302.362400000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSuBloq."KGT"'
        KeepTogether = True
        object Memo183: TfrxMemoView
          Top = 34.015770000000000000
          Width = 347.716760000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Left = 347.716760000000000000
          Top = 34.015770000000000000
          Width = 83.149606300000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Left = 430.866420000000000000
          Top = 34.015770000000000000
          Width = 83.149606300000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          Left = 514.016080000000000000
          Top = 34.015770000000000000
          Width = 83.149606300000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Left = 597.165740000000000000
          Top = 34.015770000000000000
          Width = 83.149606300000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo330: TfrxMemoView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAIS DAS CONTAS NO PROCESSAMENTO DE BLOQUETOS DE COBRAN'#199'A')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter13: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 411.968770000000000000
        Width = 680.315400000000000000
        object Memo193: TfrxMemoView
          Left = 347.716760000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."ORIGINAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo194: TfrxMemoView
          Left = 430.866420000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."PAGO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo195: TfrxMemoView
          Left = 514.016080000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."MULTAVAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo196: TfrxMemoView
          Left = 597.165740000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."MORAVAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo197: TfrxMemoView
          Width = 347.716760000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  ')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxBal_A_12A: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40318.458037951390000000
    ReportOptions.LastChange = 40318.458037951390000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 384
    Top = 56
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end
      item
        DataSet = DmodFin.frxDsPgBloq
        DataSetName = 'frxDsPgBloq'
      end
      item
        DataSet = DmodFin.frxDsSuBloq
        DataSetName = 'frxDsSuBloq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader6: TfrxPageHeader
        Height = 41.574803150000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 28.346456692913390000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 13.228346456692910000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 13.228346456692910000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 13.228346460000000000
          Width = 468.661720000000000000
          Height = 15.118110236220470000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 13.228346460000000000
          Width = 98.267716540000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 13.228346460000000000
          Width = 98.267716540000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 28.346456690000000000
          Width = 453.543600000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 453.543600000000000000
          Top = 28.346456690000000000
          Width = 226.771800000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object GroupHeader7: TfrxGroupHeader
        Height = 24.566929130000000000
        Top = 120.944960000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPgBloq."Documento"'
        KeepTogether = True
        object Memo157: TfrxMemoView
          Top = 13.228346456692910000
          Width = 253.228510000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade/Locat'#225'rio/Conta')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Left = 385.512060000000000000
          Top = 13.228346456692910000
          Width = 83.149606300000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Titulo')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Left = 253.228510000000000000
          Top = 13.228346456692910000
          Width = 34.015748030000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Left = 287.244280000000000000
          Top = 13.228346456692910000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Left = 336.378170000000000000
          Top = 13.228346456692910000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pagamento')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Left = 468.661720000000000000
          Top = 13.228346456692910000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Left = 525.354670000000000000
          Top = 13.228346456692910000
          Width = 56.692913390000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Left = 582.047620000000000000
          Top = 13.228346456692910000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Left = 631.181510000000000000
          Top = 13.228346456692910000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Left = 491.338900000000000000
          Top = 1.889763779527559000
          Width = 188.976463390000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade habitacional: [frxDsPgBloq."UH"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo167: TfrxMemoView
          Top = 1.889763779527559000
          Width = 491.338863390000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPgBloq."NOMEPROPRIET"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData6: TfrxMasterData
        Height = 11.338582680000000000
        Top = 170.078850000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsPgBloq
        DataSetName = 'frxDsPgBloq'
        KeepTogether = True
        RowCount = 0
        object Memo168: TfrxMemoView
          Width = 253.228510000000000000
          Height = 11.338582677165350000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPgBloq."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo169: TfrxMemoView
          Left = 385.512060000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          ShowHint = False
          DataField = 'Documento'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."Documento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo170: TfrxMemoView
          Left = 253.228510000000000000
          Width = 34.015748030000000000
          Height = 11.338582677165350000
          ShowHint = False
          DataField = 'MES'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."MES"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo171: TfrxMemoView
          Left = 287.244280000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataField = 'Vencimento'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."Vencimento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo172: TfrxMemoView
          Left = 336.378170000000000000
          Width = 49.133858270000000000
          Height = 11.338582677165350000
          ShowHint = False
          DataField = 'DATA_TXT'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgBloq."DATA_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo173: TfrxMemoView
          Left = 468.661720000000000000
          Width = 56.692913390000000000
          Height = 11.338582677165350000
          ShowHint = False
          DataField = 'ORIGINAL'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."ORIGINAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo174: TfrxMemoView
          Left = 525.354670000000000000
          Width = 56.692913390000000000
          Height = 11.338582677165350000
          ShowHint = False
          DataField = 'Credito'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."Credito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo175: TfrxMemoView
          Left = 582.047620000000000000
          Width = 49.133858270000000000
          Height = 11.338582677165350000
          ShowHint = False
          DataField = 'MultaVal'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."MultaVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo176: TfrxMemoView
          Left = 631.181510000000000000
          Width = 49.133858270000000000
          Height = 11.338582677165350000
          ShowHint = False
          DataField = 'MoraVal'
          DataSet = DmodFin.frxDsPgBloq
          DataSetName = 'frxDsPgBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgBloq."MoraVal"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter7: TfrxGroupFooter
        Height = 11.338582680000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        object Memo177: TfrxMemoView
          Width = 468.661720000000000000
          Height = 11.338582677165350000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              'Total do t'#237'tulo [frxDsPgBloq."Documento"] da unidade habitaciona' +
              'l [frxDsPgBloq."UH"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo178: TfrxMemoView
          Left = 468.661720000000000000
          Width = 56.692913390000000000
          Height = 11.338582677165350000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."ORIGINAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo179: TfrxMemoView
          Left = 525.354670000000000000
          Width = 56.692913390000000000
          Height = 11.338582677165350000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo180: TfrxMemoView
          Left = 582.047620000000000000
          Width = 49.133858270000000000
          Height = 11.338582677165350000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."MultaVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo181: TfrxMemoView
          Left = 631.181510000000000000
          Width = 49.133858270000000000
          Height = 11.338582677165350000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPgBloq."MoraVal">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter4: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 400.630180000000000000
        Width = 680.315400000000000000
        object Memo182: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData7: TfrxMasterData
        Height = 11.338582680000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsSuBloq
        DataSetName = 'frxDsSuBloq'
        KeepTogether = True
        RowCount = 0
        object Memo188: TfrxMemoView
          Width = 347.716760000000000000
          Height = 11.338582677165350000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = DmodFin.frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSuBloq."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo189: TfrxMemoView
          Left = 347.716760000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          ShowHint = False
          DataField = 'ORIGINAL'
          DataSet = DmodFin.frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."ORIGINAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo190: TfrxMemoView
          Left = 430.866420000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          ShowHint = False
          DataField = 'PAGO'
          DataSet = DmodFin.frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."PAGO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo191: TfrxMemoView
          Left = 514.016080000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          ShowHint = False
          DataField = 'MultaVal'
          DataSet = DmodFin.frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."MultaVal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo192: TfrxMemoView
          Left = 597.165740000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          ShowHint = False
          DataField = 'MoraVal'
          DataSet = DmodFin.frxDsSuBloq
          DataSetName = 'frxDsSuBloq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSuBloq."MoraVal"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader13: TfrxGroupHeader
        Height = 32.125976930000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSuBloq."KGT"'
        KeepTogether = True
        object Memo183: TfrxMemoView
          Top = 20.787394251968500000
          Width = 347.716760000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Left = 347.716760000000000000
          Top = 20.787394251968500000
          Width = 83.149606300000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Left = 430.866420000000000000
          Top = 20.787394251968500000
          Width = 83.149606300000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          Left = 514.016080000000000000
          Top = 20.787394251968500000
          Width = 83.149606300000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Left = 597.165740000000000000
          Top = 20.787394251968500000
          Width = 83.149606300000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo330: TfrxMemoView
          Top = 7.559059999999999000
          Width = 680.315400000000000000
          Height = 13.228346456692910000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAIS DAS CONTAS NO PROCESSAMENTO DE BLOQUETOS DE COBRAN'#199'A')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter13: TfrxGroupFooter
        Height = 11.338582680000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        object Memo193: TfrxMemoView
          Left = 347.716760000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."ORIGINAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo194: TfrxMemoView
          Left = 430.866420000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."PAGO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo195: TfrxMemoView
          Left = 514.016080000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."MULTAVAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo196: TfrxMemoView
          Left = 597.165740000000000000
          Width = 83.149606300000000000
          Height = 11.338582677165350000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSuBloq."MORAVAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo197: TfrxMemoView
          Width = 347.716760000000000000
          Height = 11.338582677165350000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  ')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxBal_A_13: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40319.801356423610000000
    ReportOptions.LastChange = 40319.801356423610000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 412
    Top = 56
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end
      item
        DataSet = DmodFin.frxDsPendG
        DataSetName = 'frxDsPendG'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader7: TfrxPageHeader
        Height = 94.488250000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo126: TfrxMemoView
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data: [Date]')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Top = 79.370130000000000000
          Width = 408.189240000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade/Locat'#225'rio/Conta')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Left = 517.795610000000000000
          Top = 79.370130000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Titulo')
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          Left = 408.189240000000000000
          Top = 79.370130000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Left = 453.543600000000000000
          Top = 79.370130000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Left = 612.283860000000000000
          Top = 79.370130000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
      end
      object GroupHeader8: TfrxGroupHeader
        Height = 13.228346460000000000
        Top = 173.858380000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPendG."Unidade"'
        KeepTogether = True
        object Memo127: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade habitacional: [frxDsPendG."Unidade"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData5: TfrxMasterData
        Height = 13.228346460000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsPendG
        DataSetName = 'frxDsPendG'
        RowCount = 0
        object Memo138: TfrxMemoView
          Width = 408.189240000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMEPROPRIET'
          DataSet = DmodFin.frxDsPendG
          DataSetName = 'frxDsPendG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPendG."NOMEPROPRIET"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo139: TfrxMemoView
          Left = 517.795610000000000000
          Width = 94.488250000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'FatNum'
          DataSet = DmodFin.frxDsPendG
          DataSetName = 'frxDsPendG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPendG."FatNum"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo140: TfrxMemoView
          Left = 408.189240000000000000
          Width = 45.354360000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMEMEZ'
          DataSet = DmodFin.frxDsPendG
          DataSetName = 'frxDsPendG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPendG."NOMEMEZ"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo141: TfrxMemoView
          Left = 453.543600000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Vencimento'
          DataSet = DmodFin.frxDsPendG
          DataSetName = 'frxDsPendG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPendG."Vencimento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo143: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Credito'
          DataSet = DmodFin.frxDsPendG
          DataSetName = 'frxDsPendG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPendG."Credito"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter8: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        object Memo133: TfrxMemoView
          Width = 612.283860000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total da unidade habitacional [frxDsPendG."Unidade"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo135: TfrxMemoView
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPendG."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer3: TfrxFooter
        Height = 22.677180000000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        object Memo144: TfrxMemoView
          Left = 597.165740000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPendG."Credito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo146: TfrxMemoView
          Width = 597.165740000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL GERAL:  ')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter3: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
        object Memo120: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxBal_A_14: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40319.822980474540000000
    ReportOptions.LastChange = 40319.822980474540000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 440
    Top = 56
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end
      item
        DataSet = DmodFin.frxDsRep
        DataSetName = 'frxDsRep'
      end
      item
        DataSet = DmodFin.frxDsRepBPP
        DataSetName = 'frxDsRepBPP'
      end
      item
        DataSet = DmodFin.frxDsRepLan
        DataSetName = 'frxDsRepLan'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader2: TfrxPageHeader
        Height = 79.370130000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo211: TfrxMemoView
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
      end
      object MasterData10: TfrxMasterData
        Height = 117.165430000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsRep
        DataSetName = 'frxDsRep'
        KeepTogether = True
        RowCount = 0
        object Memo217: TfrxMemoView
          Left = 204.094620000000000000
          Top = 18.897650000000000000
          Width = 457.323130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsRep."NOMEPROP"]')
          ParentFont = False
        end
        object Memo218: TfrxMemoView
          Left = 147.401670000000000000
          Top = 18.897650000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Propriet'#225'rio:')
          ParentFont = False
        end
        object Memo277: TfrxMemoView
          Left = 18.897650000000000000
          Top = 18.897650000000000000
          Width = 30.236240000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'U.H.:')
          ParentFont = False
        end
        object Memo278: TfrxMemoView
          Left = 49.133890000000000000
          Top = 18.897650000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Unidade'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsRep."Unidade"]')
          ParentFont = False
        end
        object Memo279: TfrxMemoView
          Left = 124.724490000000000000
          Top = 37.795300000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Original')
          ParentFont = False
        end
        object Memo280: TfrxMemoView
          Left = 124.724490000000000000
          Top = 56.692950000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'VlrOrigi'
          DataSet = DmodFin.frxDsRep
          DataSetName = 'frxDsRep'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRep."VlrOrigi"]')
          ParentFont = False
        end
        object Memo281: TfrxMemoView
          Left = 200.315090000000000000
          Top = 37.795300000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Multa')
          ParentFont = False
        end
        object Memo282: TfrxMemoView
          Left = 200.315090000000000000
          Top = 56.692950000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'VlrMulta'
          DataSet = DmodFin.frxDsRep
          DataSetName = 'frxDsRep'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRep."VlrMulta"]')
          ParentFont = False
        end
        object Memo283: TfrxMemoView
          Left = 275.905690000000000000
          Top = 37.795300000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Juros')
          ParentFont = False
        end
        object Memo284: TfrxMemoView
          Left = 275.905690000000000000
          Top = 56.692950000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'VlrJuros'
          DataSet = DmodFin.frxDsRep
          DataSetName = 'frxDsRep'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRep."VlrJuros"]')
          ParentFont = False
        end
        object Memo285: TfrxMemoView
          Left = 351.496290000000000000
          Top = 37.795300000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Total')
          ParentFont = False
        end
        object Memo286: TfrxMemoView
          Left = 351.496290000000000000
          Top = 56.692950000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'VlrTotal'
          DataSet = DmodFin.frxDsRep
          DataSetName = 'frxDsRep'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRep."VlrTotal"]')
          ParentFont = False
        end
        object Memo287: TfrxMemoView
          Left = 68.031540000000000000
          Top = 37.795300000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo288: TfrxMemoView
          Left = 68.031540000000000000
          Top = 56.692950000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DataP'
          DataSet = DmodFin.frxDsRep
          DataSetName = 'frxDsRep'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRep."DataP"]')
          ParentFont = False
        end
        object Memo289: TfrxMemoView
          Left = 18.897650000000000000
          Top = 75.590600000000000000
          Width = 642.520100000000000000
          Height = 41.574830000000000000
          ShowHint = False
          DataField = 'autentica'
          DataSet = DmodFin.frxDsRep
          DataSetName = 'frxDsRep'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRep."autentica"]')
          ParentFont = False
        end
        object Memo290: TfrxMemoView
          Left = 427.086890000000000000
          Top = 37.795300000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Login ID')
          ParentFont = False
        end
        object Memo291: TfrxMemoView
          Left = 427.086890000000000000
          Top = 56.692950000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'loginID'
          DataSet = DmodFin.frxDsRep
          DataSetName = 'frxDsRep'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRep."loginID"]')
          ParentFont = False
        end
        object Memo212: TfrxMemoView
          Left = 18.897650000000000000
          Top = 37.795300000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Memo213: TfrxMemoView
          Left = 18.897650000000000000
          Top = 56.692950000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Codigo'
          DataSet = DmodFin.frxDsRep
          DataSetName = 'frxDsRep'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000;-000000; '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRep."Codigo"]')
          ParentFont = False
        end
        object Line8: TfrxLineView
          Height = 117.165430000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
        object Line9: TfrxLineView
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Line10: TfrxLineView
          Left = 680.315400000000000000
          Height = 117.165430000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
      end
      object DetailData1: TfrxDetailData
        Height = 18.000000000000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsRepBPP
        DataSetName = 'frxDsRepBPP'
        RowCount = 0
        object Memo219: TfrxMemoView
          Left = 170.078752360000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsRepBPP
          DataSetName = 'frxDsRepBPP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000;-000000; '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39',<frxDsRepBPP."FatNum">)]')
          ParentFont = False
        end
        object Memo268: TfrxMemoView
          Left = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataSet = DmodFin.frxDsRepBPP
          DataSetName = 'frxDsRepBPP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000;-000000; '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39',<frxDsRepBPP."Parcela">)]')
          ParentFont = False
        end
        object Memo269: TfrxMemoView
          Left = 245.669303540000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'ValBol'
          DataSet = DmodFin.frxDsRepBPP
          DataSetName = 'frxDsRepBPP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepBPP."ValBol"]')
          ParentFont = False
        end
        object Memo270: TfrxMemoView
          Left = 94.488201180000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'Vencimento'
          DataSet = DmodFin.frxDsRepBPP
          DataSetName = 'frxDsRepBPP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepBPP."Vencimento"]')
          ParentFont = False
        end
        object Line12: TfrxLineView
          Left = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
        object Line22: TfrxLineView
          Height = 18.897650000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
      end
      object DetailData2: TfrxDetailData
        Height = 15.118110240000000000
        Top = 517.795610000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsRepLan
        DataSetName = 'frxDsRepLan'
        RowCount = 0
        object Memo261: TfrxMemoView
          Left = 94.488298820000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'FatNum'
          DataSet = DmodFin.frxDsRepLan
          DataSetName = 'frxDsRepLan'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepLan."FatNum"]')
          ParentFont = False
        end
        object Memo262: TfrxMemoView
          Left = 170.078752360000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Credito'
          DataSet = DmodFin.frxDsRepLan
          DataSetName = 'frxDsRepLan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepLan."Credito"]')
          ParentFont = False
        end
        object Memo263: TfrxMemoView
          Left = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Vencimento'
          DataSet = DmodFin.frxDsRepLan
          DataSetName = 'frxDsRepLan'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepLan."Vencimento"]')
          ParentFont = False
        end
        object Memo264: TfrxMemoView
          Left = 245.669450000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'VLRORIG'
          DataSet = DmodFin.frxDsRepLan
          DataSetName = 'frxDsRepLan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepLan."VLRORIG"]')
          ParentFont = False
        end
        object Memo265: TfrxMemoView
          Left = 321.260050000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'VLRMULTA'
          DataSet = DmodFin.frxDsRepLan
          DataSetName = 'frxDsRepLan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepLan."VLRMULTA"]')
          ParentFont = False
        end
        object Memo266: TfrxMemoView
          Left = 396.850650000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'VLRJUROS'
          DataSet = DmodFin.frxDsRepLan
          DataSetName = 'frxDsRepLan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepLan."VLRJUROS"]')
          ParentFont = False
        end
        object Memo267: TfrxMemoView
          Left = 472.441250000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'VLRTOTAL'
          DataSet = DmodFin.frxDsRepLan
          DataSetName = 'frxDsRepLan'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepLan."VLRTOTAL"]')
          ParentFont = False
        end
        object Line15: TfrxLineView
          Left = 680.315400000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
        object Line19: TfrxLineView
          Height = 15.118120000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
      end
      object GroupHeader10: TfrxGroupHeader
        Height = 49.133890000000000000
        Top = 445.984540000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsRepLan."Reparcel"'
        object Memo220: TfrxMemoView
          Left = 94.488298820000000000
          Top = 34.015770000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Bloqueto')
          ParentFont = False
        end
        object Memo221: TfrxMemoView
          Left = 170.078752360000000000
          Top = 34.015770000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Valor original')
          ParentFont = False
        end
        object Memo222: TfrxMemoView
          Left = 18.897650000000000000
          Top = 34.015770000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo223: TfrxMemoView
          Left = 18.897650000000000000
          Top = 15.118120000000000000
          Width = 529.134151180000000000
          Height = 18.897640240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Bloquetos que foram parcelados')
          ParentFont = False
        end
        object Memo224: TfrxMemoView
          Left = 245.669450000000000000
          Top = 34.015770000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Base c'#225'lculo*')
          ParentFont = False
        end
        object Memo225: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '$ Multa*')
          ParentFont = False
        end
        object Memo226: TfrxMemoView
          Left = 396.850650000000000000
          Top = 34.015770000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '$ Juros*')
          ParentFont = False
        end
        object Memo254: TfrxMemoView
          Left = 472.441250000000000000
          Top = 34.015770000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '$ Atualizado*')
          ParentFont = False
        end
        object Line14: TfrxLineView
          Left = 680.315400000000000000
          Height = 49.133890000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
        object Line20: TfrxLineView
          Height = 49.133890000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
      end
      object GroupFooter10: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 555.590910000000000000
        Width = 680.315400000000000000
        object Memo255: TfrxMemoView
          Left = 18.897650000000000000
          Width = 151.181151180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo256: TfrxMemoView
          Left = 170.078850000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReplan."Credito">)]')
          ParentFont = False
        end
        object Memo257: TfrxMemoView
          Left = 245.669547640000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReplan."VLRORIG">)]*')
          ParentFont = False
        end
        object Memo258: TfrxMemoView
          Left = 321.260147640000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReplan."VLRMULTA">)]*')
          ParentFont = False
        end
        object Memo259: TfrxMemoView
          Left = 396.850747640000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReplan."VLRJUROS">)]*')
          ParentFont = False
        end
        object Memo260: TfrxMemoView
          Left = 472.441347640000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsReplan."VLRTOTAL">)]*')
          ParentFont = False
        end
        object Line16: TfrxLineView
          Left = 680.315400000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
        object Line17: TfrxLineView
          Top = 22.677180000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Line18: TfrxLineView
          Height = 22.677180000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
      end
      object GroupHeader11: TfrxGroupHeader
        Height = 36.897650000000000000
        Top = 298.582870000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsRepBPP."Codigo"'
        object Memo271: TfrxMemoView
          Left = 170.078752360000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Bloqueto')
          ParentFont = False
        end
        object Memo272: TfrxMemoView
          Left = 18.897650000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Parcela')
          ParentFont = False
        end
        object Memo273: TfrxMemoView
          Left = 245.669303540000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo274: TfrxMemoView
          Left = 94.488201180000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Line11: TfrxLineView
          Left = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
        object Line23: TfrxLineView
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
      end
      object GroupFooter11: TfrxGroupFooter
        Height = 21.779530000000000000
        Top = 400.630180000000000000
        Width = 680.315400000000000000
        object Memo275: TfrxMemoView
          Left = 245.669450000000000000
          Top = 3.779530000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepBPP."ValBol">)]')
          ParentFont = False
        end
        object Memo276: TfrxMemoView
          Left = 170.078850000000000000
          Top = 3.779530000000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[COUNT(DetailData1)] parcelas')
          ParentFont = False
        end
        object Line13: TfrxLineView
          Left = 680.315400000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
        object Line21: TfrxLineView
          Height = 22.677180000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
        end
      end
      object PageFooter3: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 638.740570000000000000
        Width = 680.315400000000000000
        object Memo120: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxBal_A_15: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40320.401049953700000000
    ReportOptions.LastChange = 40320.401049953700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  MePagina.Visible := <VARF_PAGINAR>;  '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 468
    Top = 56
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end
      item
        DataSet = DmodFin.frxDsRepAbe
        DataSetName = 'frxDsRepAbe'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader2: TfrxPageHeader
        Height = 79.370130000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo211: TfrxMemoView
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader12: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsRepAbe."NOMEPROP"'
        KeepTogether = True
        object Memo295: TfrxMemoView
          Left = 185.196970000000000000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMEPROP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsRepAbe."NOMEPROP"]')
          ParentFont = False
        end
        object Memo296: TfrxMemoView
          Left = 128.504020000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Propriet'#225'rio:')
          ParentFont = False
        end
        object Memo297: TfrxMemoView
          Width = 30.236240000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'U.H.:')
          ParentFont = False
        end
        object Memo298: TfrxMemoView
          Left = 30.236240000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Unidade'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsRepAbe."Unidade"]')
          ParentFont = False
        end
      end
      object MasterData12: TfrxMasterData
        Height = 18.897650000000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        DataSet = DmodFin.frxDsRepAbe
        DataSetName = 'frxDsRepAbe'
        RowCount = 0
        object Memo299: TfrxMemoView
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Data'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRepAbe."Data"]')
          ParentFont = False
        end
        object Memo300: TfrxMemoView
          Left = 45.354360000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Vencimento'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRepAbe."Vencimento"]')
          ParentFont = False
        end
        object Memo301: TfrxMemoView
          Left = 136.063080000000000000
          Width = 362.834880000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Descricao'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRepAbe."Descricao"]')
          ParentFont = False
        end
        object Memo302: TfrxMemoView
          Left = 498.897960000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'FatNum'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRepAbe."FatNum"]')
          ParentFont = False
        end
        object Memo303: TfrxMemoView
          Left = 559.370440000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Credito'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRepAbe."Credito"]')
          ParentFont = False
        end
        object Memo304: TfrxMemoView
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Pago'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRepAbe."Pago"]')
          ParentFont = False
        end
        object Memo305: TfrxMemoView
          Left = 90.708720000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Compensado_TXT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRepAbe."Compensado_TXT"]')
          ParentFont = False
        end
      end
      object PageFooter3: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        object Memo120: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object QrLista: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT * '
      'FROM cfgrel'
      'ORDER BY Ordem, Controle')
    Left = 680
    Top = 8
    object QrListaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrListaRelTitu: TWideStringField
      FieldName = 'RelTitu'
      Size = 100
    end
    object QrListaOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrListaAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsLista: TDataSource
    DataSet = QrLista
    Left = 708
    Top = 8
  end
  object QrSels: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT COUNT(*) Itens'
      'FROM cfgrel'
      'WHERE Ativo=1')
    Left = 736
    Top = 8
    object QrSelsItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object frxBalancete2: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39100.337475578700000000
    ReportOptions.LastChange = 40321.531897615700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  nCC, nLI: Integer;'
      '  SaldoCC_C, SaldoCC_D: Extended;'
      '  Resumido_01, Resumido_02,'
      '  Resumido_03, Resumido_04,'
      '  Resumido_05, Resumido_06: Boolean;'
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  nLI := nLI + 1;'
      '  if  <frxDsExtratos."SdIni"> = 0 then'
      '  begin'
      '    SaldoCC_C := SaldoCC_C + <frxDsExtratos."Credi">;'
      '    SaldoCC_D := SaldoCC_D + <frxDsExtratos."Debit">;'
      '  end;'
      'end;'
      ''
      'procedure ReportTitle1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  nCC := 0;'
      '  SaldoCC_C := 0;'
      '  SaldoCC_D := 0;'
      'end;'
      ''
      'begin'
      '  Resumido_01 := <VARF_RESUMIDO_01>;          '
      '  Resumido_02 := <VARF_RESUMIDO_02>;          '
      '  Resumido_03 := <VARF_RESUMIDO_03>;          '
      '  Resumido_04 := <VARF_RESUMIDO_04>;          '
      '  Resumido_05 := <VARF_RESUMIDO_05>;          '
      '  Resumido_06 := <VARF_RESUMIDO_06>;          '
      '  //                '
      '  MeCredDebi.Visible := Resumido_01 and Resumido_02;'
      '  //        '
      
        '  HeCred1.Visible := Resumido_01;                               ' +
        '                 '
      '  GHCred1.Visible := Resumido_01;'
      '  GHCred2.Visible := Resumido_01;'
      '  MDCred1.Visible := Resumido_01;'
      '  GFCred2.Visible := Resumido_01;'
      '  GFCred1.Visible := Resumido_01;'
      '  FoCred1.Visible := Resumido_01;'
      '  //        '
      
        '  HeDebi1.Visible := Resumido_02;                               ' +
        '                 '
      '  GHDebi1.Visible := Resumido_02;'
      '  GHDebi2.Visible := Resumido_02;'
      '  MDDebi1.Visible := Resumido_02;'
      '  GFDebi2.Visible := Resumido_02;'
      '  GFDebi1.Visible := Resumido_02;'
      '  FoDebi1.Visible := Resumido_02;'
      '  //        '
      '  GHInad1.Visible := Resumido_03;'
      '  MDInad1.Visible := Resumido_03;'
      '  GFInad1.Visible := Resumido_03;'
      '  //        '
      '  GHMovi1.Visible := Resumido_04;'
      '  MDMovi1.Visible := Resumido_04;'
      '  GFMovi1.Visible := Resumido_04;'
      '  //        '
      '  GHSCCs1.Visible := Resumido_05;'
      '  MDSCCs1.Visible := Resumido_05;'
      '  GFSCCs1.Visible := Resumido_05;'
      '  //        '
      '  MDResu1.Visible := Resumido_06;'
      '  DDResu1.Visible := Resumido_06;'
      '  //        '
      'end.')
    OnGetValue = frxBal_A_01GetValue
    OnStartReport = 'frxBalancete3OnStartReport'
    Left = 120
    Top = 8
    Datasets = <
      item
        DataSet = DmodFin.frxDsCreditos
        DataSetName = 'frxDsCreditos'
      end
      item
        DataSet = frxDsDebitos
        DataSetName = 'frxDsDebitos'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsExclusivos
        DataSetName = 'frxDsExclusivos'
      end
      item
        DataSet = DmodFin.frxDsPendAll
        DataSetName = 'frxDsPendAll'
      end
      item
        DataSet = DmodFin.frxDsPendG
        DataSetName = 'frxDsPendG'
      end
      item
        DataSet = DmodFin.frxDsPendSum
        DataSetName = 'frxDsPendSum'
      end
      item
        DataSet = DmodFin.frxDsResumo
        DataSetName = 'frxDsResumo'
      end
      item
        DataSet = DmodFin.frxDsSaldoA
        DataSetName = 'frxDsSaldoA'
      end
      item
        DataSet = DmodFin.frxDsSdoCtas
        DataSetName = 'frxDsSdoCtas'
      end
      item
        DataSet = DmodFin.frxDsSNG
        DataSetName = 'frxDsSNG'
      end
      item
        DataSet = DmodFin.frxDsSTCP
        DataSetName = 'frxDsSTCP'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Columns = 2
      ColumnWidth = 91.500000000000000000
      ColumnPositions.Strings = (
        '0'
        '93,50')
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 75.590600000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape3: TfrxShapeView
          Left = 52.913385830000000000
          Top = 37.795300000000000000
          Width = 699.212598430000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 56.692950000000000000
          Top = 37.795300000000000000
          Width = 555.590910000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Left = 52.913385830000000000
          Top = 56.692950000000000000
          Width = 697.323061650000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 612.283860000000000000
          Top = 37.795300000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'BALANCETE RESUMIDO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 56.692915830000000000
          Top = 56.692950000000000000
          Width = 555.590910000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 404.409675830000000000
          Top = 56.692950000000000000
          Width = 343.937230000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
      end
      object GHCred1: TfrxGroupHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 10.204724410000000000
        ParentFont = False
        Top = 211.653680000000000000
        Width = 345.826995000000000000
        Condition = 'frxDsCreditos."NOMEGRU"'
        object Memo51: TfrxMemoView
          Left = 52.913385830000000000
          Width = 221.102362200000000000
          Height = 10.204724409448800000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCreditos."NOMEGRU"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 319.480520000000000000
          Width = 79.370130000000000000
          Height = 10.204724409448800000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valores em R$')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 274.126160000000000000
          Width = 45.354360000000000000
          Height = 10.204724409448800000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
        end
      end
      object GHCred2: TfrxGroupHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 10.204724410000000000
        ParentFont = False
        Top = 245.669450000000000000
        Width = 345.826995000000000000
        Condition = 'frxDsCreditos."NOMESGR"'
        object Memo53: TfrxMemoView
          Left = 52.913385830000000000
          Width = 221.102362200000000000
          Height = 10.204724409448800000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCreditos."NOMESGR"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 319.480520000000000000
          Width = 79.370130000000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Left = 274.126160000000000000
          Width = 45.354360000000000000
          Height = 10.204724409448800000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MDCred1: TfrxMasterData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 9.448818900000000000
        ParentFont = False
        Top = 279.685220000000000000
        Width = 345.826995000000000000
        DataSet = DmodFin.frxDsCreditos
        DataSetName = 'frxDsCreditos'
        RowCount = 0
        object Memo55: TfrxMemoView
          Left = 52.913385830000000000
          Width = 221.102362200000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataField = 'NOMECON_2'
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCreditos."NOMECON_2"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          Left = 319.480520000000000000
          Width = 79.370130000000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCreditos."Credito"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 274.126160000000000000
          Width = 45.354360000000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataField = 'MES'
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCreditos."MES"]')
          ParentFont = False
        end
      end
      object GFCred2: TfrxGroupFooter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 10.204724410000000000
        ParentFont = False
        Top = 313.700990000000000000
        Width = 345.826995000000000000
        object Memo58: TfrxMemoView
          Left = 52.913385830000000000
          Width = 221.102362200000000000
          Height = 10.204724409448800000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 319.480520000000000000
          Width = 79.370130000000000000
          Height = 10.204724409448800000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCreditos."Credito">)]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 274.126160000000000000
          Width = 45.354360000000000000
          Height = 10.204724409448800000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GFCred1: TfrxGroupFooter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 10.204724410000000000
        ParentFont = False
        Top = 347.716760000000000000
        Width = 345.826995000000000000
        object Memo60: TfrxMemoView
          Left = 52.913385830000000000
          Width = 221.102362200000000000
          Height = 10.204724409448800000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de [frxDsCreditos."NOMEGRU"] ')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 274.015748030000000000
          Width = 124.724409450000000000
          Height = 10.204724409448800000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCreditos."Credito">)]')
          ParentFont = False
        end
      end
      object GHDebi1: TfrxGroupHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 10.204724410000000000
        ParentFont = False
        Top = 453.543600000000000000
        Width = 345.826995000000000000
        Condition = 'frxDsDebitos."NOMEGRU"'
        object Memo64: TfrxMemoView
          Left = 52.913385830000000000
          Width = 266.456773460000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDebitos."NOMEGRU"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 319.370056770000000000
          Width = 79.370100710000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
      end
      object GHDebi2: TfrxGroupHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 10.204724410000000000
        ParentFont = False
        Top = 487.559370000000000000
        Width = 345.826995000000000000
        Condition = 'frxDsDebitos."NOMESGR"'
        object Memo70: TfrxMemoView
          Left = 52.913385830000000000
          Width = 266.456692910000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDebitos."NOMESGR"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 319.370078740000000000
          Width = 79.370130000000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object MDDebi1: TfrxMasterData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 10.204724410000000000
        ParentFont = False
        Top = 521.575140000000000000
        Width = 345.826995000000000000
        DataSet = frxDsDebitos
        DataSetName = 'frxDsDebitos'
        RowCount = 0
        object Memo77: TfrxMemoView
          Left = 319.370056770000000000
          Width = 79.370100710000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'Debito'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDebitos."Debito"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 52.913385830000000000
          Width = 266.456641650000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataField = 'NOMECON'
          DataSet = frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDebitos."NOMECON"]')
          ParentFont = False
        end
      end
      object GFDebi2: TfrxGroupFooter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 10.204724410000000000
        ParentFont = False
        Top = 555.590910000000000000
        Width = 345.826995000000000000
        object Memo82: TfrxMemoView
          Left = 319.370056770000000000
          Width = 79.370100710000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Left = 52.913385830000000000
          Width = 266.456641650000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sub-total de [frxDsDebitos."NOMESGR"]')
          ParentFont = False
        end
      end
      object GFDebi1: TfrxGroupFooter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 10.204724410000000000
        ParentFont = False
        Top = 589.606680000000000000
        Width = 345.826995000000000000
        object Memo83: TfrxMemoView
          Left = 319.370078740000000000
          Width = 79.370078740000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">)]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 52.913385830000000000
          Width = 266.456692913386000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total de [frxDsDebitos."NOMEGRU"] ')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 52.913410240000000000
        ParentFont = False
        Top = 1190.551950000000000000
        Width = 793.701300000000000000
        object Memo93: TfrxMemoView
          Left = 52.913385830000000000
          Width = 438.425028430000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 491.338900000000000000
          Width = 260.787118430000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object HeCred1: TfrxHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 34.015770000000000000
        ParentFont = False
        Top = 154.960730000000000000
        Width = 345.826995000000000000
        object Memo200: TfrxMemoView
          Left = 52.913385830000000000
          Top = 18.897650000000000000
          Width = 345.826771650000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'RECEITAS')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeCredDebi: TfrxMemoView
          Left = 52.913420000000000000
          Width = 345.826771650000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'DEMOSTRATIVO DE RECEITAS E DESPESAS NO PER'#205'ODO')
          ParentFont = False
          VAlign = vaBottom
        end
      end
      object FoCred1: TfrxFooter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 11.338580240000000000
        ParentFont = False
        Top = 381.732530000000000000
        Width = 345.826995000000000000
        object Memo201: TfrxMemoView
          Left = 52.913385830000000000
          Width = 221.102362200000000000
          Height = 10.204724409448800000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL RECEITAS:')
          ParentFont = False
        end
        object Memo202: TfrxMemoView
          Left = 274.015748030000000000
          Width = 124.724409450000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCreditos."Credito">,MDCred1)]')
          ParentFont = False
        end
      end
      object HeDebi1: TfrxHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 15.118120000000000000
        ParentFont = False
        Top = 415.748300000000000000
        Width = 345.826995000000000000
        object Memo203: TfrxMemoView
          Left = 52.913385830000000000
          Width = 345.826771650000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'DESPESAS')
          ParentFont = False
          VAlign = vaBottom
        end
      end
      object FoDebi1: TfrxFooter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 11.338590000000000000
        ParentFont = False
        Top = 623.622450000000000000
        Width = 345.826995000000000000
        object Memo204: TfrxMemoView
          Left = 52.913385830000000000
          Width = 221.102362200000000000
          Height = 10.204724409448800000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DESPESAS:')
          ParentFont = False
        end
        object Memo205: TfrxMemoView
          Left = 274.015748030000000000
          Width = 124.724409450000000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDebitos."Debito">,MDDebi1)]')
          ParentFont = False
        end
      end
      object MDMovi1: TfrxMasterData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 9.448818900000000000
        ParentFont = False
        Top = 823.937540000000000000
        Width = 345.826995000000000000
        DataSet = DmodFin.frxDsSdoCtas
        DataSetName = 'frxDsSdoCtas'
        KeepTogether = True
        RowCount = 0
        object Memo227: TfrxMemoView
          Left = 52.913385830000000000
          Width = 266.456692913386000000
          Height = 9.448818900000000000
          ShowHint = False
          DataField = 'Nome2'
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSdoCtas."Nome2"]')
          ParentFont = False
        end
        object Memo228: TfrxMemoView
          Left = 319.370078740158000000
          Width = 79.370078740000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataField = 'SDO_FIM'
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSdoCtas."SDO_FIM"]')
          ParentFont = False
        end
      end
      object MDSCCs1: TfrxMasterData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 9.448818900000000000
        ParentFont = False
        Top = 929.764380000000000000
        Width = 345.826995000000000000
        DataSet = DmodFin.frxDsSNG
        DataSetName = 'frxDsSNG'
        KeepTogether = True
        RowCount = 0
        object Memo333: TfrxMemoView
          Left = 52.913385830000000000
          Width = 266.456692910000000000
          Height = 9.448818900000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSNG."Ordena"] - [frxDsSNG."NomeGe"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo334: TfrxMemoView
          Left = 319.370078740158000000
          Width = 79.370078740157500000
          Height = 9.448818900000000000
          ShowHint = False
          DataField = 'SdoFim'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSNG."SdoFim"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GHMovi1: TfrxGroupHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 26.456702680000000000
        ParentFont = False
        Top = 774.803650000000000000
        Width = 345.826995000000000000
        Condition = 'frxDsSdoCtas."KGT"'
        object Memo234: TfrxMemoView
          Left = 52.913385830000000000
          Width = 345.826771650000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RESUMO DO MOVIMENTO NAS CONTAS CORRENTES')
          ParentFont = False
        end
        object Memo236: TfrxMemoView
          Left = 319.370078740000000000
          Top = 15.118120000000000000
          Width = 79.370078740000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo240: TfrxMemoView
          Left = 52.913385830000000000
          Top = 15.118120000000000000
          Width = 266.456692913386000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta (Plano de contas)')
          ParentFont = False
        end
      end
      object GFMovi1: TfrxGroupFooter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 10.204724410000000000
        ParentFont = False
        Top = 857.953310000000000000
        Width = 345.826995000000000000
        object Memo247: TfrxMemoView
          Left = 319.370078740158000000
          Width = 79.370078740000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSdoCtas."SDO_FIM">)]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 52.913385830000000000
          Width = 266.456692913386000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = DmodFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo Geral')
          ParentFont = False
        end
      end
      object GHSCCs1: TfrxGroupHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 15.118112680000000000
        ParentFont = False
        Top = 891.969080000000000000
        Width = 345.826995000000000000
        Condition = 'frxDsSNG."KGT"'
        object Memo312: TfrxMemoView
          Left = 52.913385830000000000
          Top = 3.779530000000000000
          Width = 266.456692913386000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SALDOS DE CONTAS CONTROLADAS')
          ParentFont = False
        end
        object Memo316: TfrxMemoView
          Left = 319.370078740158000000
          Top = 3.779530000000000000
          Width = 79.370078740157500000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo final')
          ParentFont = False
        end
      end
      object GFSCCs1: TfrxGroupFooter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 15.118120000000000000
        ParentFont = False
        Top = 963.780150000000000000
        Width = 345.826995000000000000
        object Memo338: TfrxMemoView
          Left = 52.913385830000000000
          Width = 266.456692913386000000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Somas de valores de todas contas - controladas ou n'#227'o:  ')
          ParentFont = False
        end
        object Memo342: TfrxMemoView
          Left = 319.370078740158000000
          Width = 79.370078740157500000
          Height = 10.204724410000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSTCP."SDOFIM"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MDResu1: TfrxMasterData
        Height = 68.031540000000000000
        Top = 1001.575450000000000000
        Width = 345.826995000000000000
        RowCount = 1
        object Memo98: TfrxMemoView
          Left = 52.913385830000000000
          Top = 34.015770000000000000
          Width = 232.440944880000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de despesas:')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Left = 52.913385830000000000
          Top = 45.354360000000000000
          Width = 232.440944880000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo do m'#234's:')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Left = 52.913385830000000000
          Top = 56.692950000000000000
          Width = 232.440944880000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SALDO A SER TRANSFERIDO')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Left = 52.913385830000000000
          Width = 345.826771650000000000
          Height = 11.338582677165400000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'RESUMO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          Left = 52.913385830000000000
          Top = 11.338590000000000000
          Width = 232.440944880000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo Anterior:')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Left = 285.354364880000000000
          Top = 11.338590000000000000
          Width = 113.385900000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSaldoA."TOTAL"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Left = 52.913385830000000000
          Top = 22.677180000000000000
          Width = 232.440944880000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total de receitas:')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Left = 285.354364880000000000
          Top = 22.677180000000000000
          Width = 113.385900000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."Credito"]')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Left = 285.354364880000000000
          Top = 34.015770000000000000
          Width = 113.385900000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."Debito"]')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 285.354364880000000000
          Top = 45.354360000000000000
          Width = 113.385900000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."SALDO"]')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 285.354364880000000000
          Top = 56.692950000000000000
          Width = 113.385900000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResumo."FINAL"]')
          ParentFont = False
        end
      end
      object GHInad1: TfrxGroupHeader
        Height = 26.456702680000000000
        Top = 657.638220000000000000
        Visible = False
        Width = 345.826995000000000000
        Condition = 'frxDsPendAll."KGT"'
        object Memo5: TfrxMemoView
          Left = 52.913420000000000000
          Width = 345.826771650000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'UNIDADES INADIMPLENTES')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 52.913420000000000000
          Top = 15.118120000000000000
          Width = 160.629882200000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 319.480554170000000000
          Top = 15.118120000000000000
          Width = 79.370130000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valores em R$')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 213.653714170000000000
          Top = 15.118120000000000000
          Width = 105.826840000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsCreditos
          DataSetName = 'frxDsCreditos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Quantidade de bloquetos')
          ParentFont = False
        end
      end
      object MDInad1: TfrxMasterData
        Height = 9.448818900000000000
        Top = 706.772110000000000000
        Visible = False
        Width = 345.826995000000000000
        DataSet = DmodFin.frxDsPendAll
        DataSetName = 'frxDsPendAll'
        RowCount = 0
        object Memo9: TfrxMemoView
          Left = 52.913420000000000000
          Width = 160.629882200000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataField = 'Unidade'
          DataSet = DmodFin.frxDsPendAll
          DataSetName = 'frxDsPendAll'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPendAll."Unidade"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 319.480554170000000000
          Width = 79.370130000000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataField = 'Credito'
          DataSet = DmodFin.frxDsPendAll
          DataSetName = 'frxDsPendAll'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPendAll."Credito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 213.653714170000000000
          Width = 105.826840000000000000
          Height = 9.448818900000000000
          ShowHint = False
          DataField = 'BLOQUETOS'
          DataSet = DmodFin.frxDsPendAll
          DataSetName = 'frxDsPendAll'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPendAll."BLOQUETOS"]')
          ParentFont = False
        end
      end
      object GFInad1: TfrxGroupFooter
        Height = 11.338590000000000000
        Top = 740.787880000000000000
        Visible = False
        Width = 345.826995000000000000
        object Memo14: TfrxMemoView
          Left = 52.913420000000000000
          Width = 107.716462200000000000
          Height = 10.204724409448800000
          ShowHint = False
          DataSet = DmodFin.frxDsPendAll
          DataSetName = 'frxDsPendAll'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 319.480554170000000000
          Width = 79.370130000000000000
          Height = 10.204724410000000000
          ShowHint = False
          DataSet = DmodFin.frxDsPendAll
          DataSetName = 'frxDsPendAll'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPendAll."Credito">)]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 160.740294170000000000
          Width = 158.740260000000000000
          Height = 10.204724409448800000
          ShowHint = False
          DataSet = DmodFin.frxDsPendAll
          DataSetName = 'frxDsPendAll'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Total Receitas a Receber')
          ParentFont = False
        end
      end
      object DDResu1: TfrxDetailData
        Height = 37.795292680000000000
        Top = 1092.284170000000000000
        Width = 345.826995000000000000
        OnBeforePrint = 'DetailData1OnBeforePrint'
        RowCount = 1
        object Memo17: TfrxMemoView
          Left = 52.913420000000000000
          Top = 3.779530000000000000
          Width = 232.440944880000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SALDO A RECEBER')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 285.354399050000000000
          Top = 3.779530000000000000
          Width = 113.385900000000000000
          Height = 11.338582677165400000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPendSum."Credito"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 52.913420000000000000
          Top = 26.456710000000000000
          Width = 232.440944880000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SALDO FINAL')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 285.354399050000000000
          Top = 26.456710000000000000
          Width = 113.385900000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(<frxDsExclusivos."Movim"> + <frxDsPendSum."Credito"> + <frxDsR' +
              'esumo."FINAL">)]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 52.913420000000000000
          Top = 15.118120000000000000
          Width = 232.440944880000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CONTAS EXCLUSIVAS:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 285.354399050000000000
          Top = 15.118120000000000000
          Width = 113.385900000000000000
          Height = 11.338582680000000000
          ShowHint = False
          DataSet = DmodFin.frxDsDebitos
          DataSetName = 'frxDsDebitos'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExclusivos."Movim"]')
          ParentFont = False
        end
      end
    end
  end
  object QrDebitos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lan.Debito) Debito,'
      'con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas    con ON con.Codigo=lan.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'WHERE lan.Tipo <> 2'
      'AND lan.Debito > 0'
      'AND lan.Genero>0'
      'AND car.ForneceI = :P0'
      'AND lan.Data BETWEEN :P1 AND :P2'
      'GROUP BY lan.Genero'
      'ORDER BY gru.OrdemLista, NOMEGRU, sgr.OrdemLista,'
      'NOMESGR, con.OrdemLista, NOMECON')
    Left = 256
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrDebitosDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrDebitosNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrDebitosNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrDebitosNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
  end
  object frxDsDebitos: TfrxDBDataset
    UserName = 'frxDsDebitos'
    CloseDataSource = False
    DataSet = QrDebitos
    BCDToCurrency = False
    Left = 284
    Top = 284
  end
  object frxBal_A_16: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40377.425845717590000000
    ReportOptions.LastChange = 40377.425845717590000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxBal_A_01GetValue
    Left = 496
    Top = 56
    Datasets = <
      item
        DataSet = frxDs16
        DataSetName = 'frxDs16'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmp
        DataSetName = 'frxDsEmp'
      end
      item
        DataSet = frxDsEntiCfgRel_01
        DataSetName = 'frxDsEntiCfgRel_01'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader2: TfrxPageHeader
        Height = 98.267780000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo211: TfrxMemoView
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEntiCfgRel_01."RelTitu"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEmp."NO_EMPRESA"]')
          ParentFont = False
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Top = 79.370130000000000000
          Width = 41.574803150000000000
          Height = 15.118110236220470000
          ShowHint = False
          DataSet = frxDs16
          DataSetName = 'frxDs16'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo2: TfrxMemoView
          Left = 41.574830000000000000
          Top = 79.370130000000000000
          Width = 45.354330710000000000
          Height = 15.118110236220470000
          ShowHint = False
          DataSet = frxDs16
          DataSetName = 'frxDs16'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 86.929190000000000000
          Top = 79.370130000000000000
          Width = 45.354330708661420000
          Height = 13.984251970000000000
          ShowHint = False
          DataSet = frxDs16
          DataSetName = 'frxDs16'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 132.283550000000000000
          Top = 79.370130000000000000
          Width = 226.771653540000000000
          Height = 15.118110236220470000
          ShowHint = False
          DataSet = DmodFin.frxDsRepAbe
          DataSetName = 'frxDsRepAbe'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 544.252320000000000000
          Top = 79.370130000000000000
          Width = 52.913385830000000000
          Height = 15.118110236220470000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 597.165740000000000000
          Top = 79.370130000000000000
          Width = 41.574803150000000000
          Height = 15.118110236220470000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencim.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Left = 638.740570000000000000
          Top = 79.370130000000000000
          Width = 41.574803150000000000
          Height = 15.118110236220470000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Compens.')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Left = 359.055350000000000000
          Top = 79.370130000000000000
          Width = 185.196850390000000000
          Height = 15.118110236220470000
          ShowHint = False
          DataSet = DmodFin.frxDsRepAbe
          DataSetName = 'frxDsRepAbe'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta do plano de contas')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object GroupHeader12: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 177.637910000000000000
        Width = 680.315400000000000000
        Condition = 'frxDs16."NO_CART"'
        KeepTogether = True
        object Memo297: TfrxMemoView
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Carteira:')
          ParentFont = False
        end
        object Memo298: TfrxMemoView
          Left = 41.574830000000000000
          Width = 638.740570000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDs16."Carteira"] - [frxDs16."NO_CART"]')
          ParentFont = False
        end
      end
      object MasterData12: TfrxMasterData
        Height = 15.118110240000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        DataSet = frxDs16
        DataSetName = 'frxDs16'
        RowCount = 0
        object Memo299: TfrxMemoView
          Width = 41.574803149606300000
          Height = 15.118110236220470000
          ShowHint = False
          DataField = 'Data'
          DataSet = frxDs16
          DataSetName = 'frxDs16'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs16."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo300: TfrxMemoView
          Left = 41.574830000000000000
          Width = 45.354330710000000000
          Height = 15.118110236220470000
          ShowHint = False
          DataField = 'SerieCH'
          DataSet = frxDs16
          DataSetName = 'frxDs16'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs16."SerieCH"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo301: TfrxMemoView
          Left = 132.283550000000000000
          Width = 226.771653540000000000
          Height = 15.118110236220470000
          ShowHint = False
          DataField = 'Descricao'
          DataSet = frxDs16
          DataSetName = 'frxDs16'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs16."Descricao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo302: TfrxMemoView
          Left = 544.252320000000000000
          Width = 52.913385830000000000
          Height = 15.118110236220470000
          ShowHint = False
          DataField = 'Debito'
          DataSet = frxDs16
          DataSetName = 'frxDs16'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs16."Debito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo303: TfrxMemoView
          Left = 597.165740000000000000
          Width = 41.574803150000000000
          Height = 15.118110236220470000
          ShowHint = False
          DataField = 'Vencimento'
          DataSet = frxDs16
          DataSetName = 'frxDs16'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs16."Vencimento"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo304: TfrxMemoView
          Left = 638.740570000000000000
          Width = 41.574803150000000000
          Height = 15.118110236220470000
          ShowHint = False
          DataField = 'Compensado'
          DataSet = frxDs16
          DataSetName = 'frxDs16'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs16."Compensado"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo305: TfrxMemoView
          Left = 86.929190000000000000
          Width = 45.354330708661420000
          Height = 15.118110236220470000
          ShowHint = False
          DataSet = frxDs16
          DataSetName = 'frxDs16'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDs16."Documento">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Left = 359.055350000000000000
          Width = 185.196850393700800000
          Height = 15.118110236220470000
          ShowHint = False
          DataField = 'NO_GENERO'
          DataSet = frxDs16
          DataSetName = 'frxDs16'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs16."NO_GENERO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object PageFooter3: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        object Memo120: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object Qr16: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT car.Nome NO_CART, lan.Carteira, lan.SerieCH, lan.Document' +
        'o,'
      'lan.Debito, lan.Qtde, lan.Descricao, lan.Data, lan.Genero,'
      'cta.Nome NO_GENERO, lan.Controle, lan.NotaFiscal,'
      'lan.Vencimento, lan.Compensado'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas cta ON cta.Codigo=lan.Genero'
      'WHERE car.Tipo=2'
      'AND lan.Debito > 0'
      'AND car.ForneceI=:P0'
      'AND lan.Compensado BETWEEN :P1 AND :P2'
      'ORDER BY car.TipoDoc, car.Nome, car.Codigo, SerieCH, Documento')
    Left = 340
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object Qr16NO_CART: TWideStringField
      FieldName = 'NO_CART'
      Size = 100
    end
    object Qr16Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object Qr16SerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object Qr16Documento: TFloatField
      FieldName = 'Documento'
    end
    object Qr16Debito: TFloatField
      FieldName = 'Debito'
    end
    object Qr16Qtde: TFloatField
      FieldName = 'Qtde'
    end
    object Qr16Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object Qr16Data: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object Qr16Genero: TIntegerField
      FieldName = 'Genero'
    end
    object Qr16NO_GENERO: TWideStringField
      FieldName = 'NO_GENERO'
      Size = 50
    end
    object Qr16Controle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr16NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object Qr16Vencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object Qr16Compensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object Ds16: TDataSource
    DataSet = Qr16
    Left = 396
    Top = 284
  end
  object frxDs16: TfrxDBDataset
    UserName = 'frxDs16'
    CloseDataSource = False
    DataSet = Qr16
    BCDToCurrency = False
    Left = 368
    Top = 284
  end
end
