unit CopiaDoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DB, mySQLDbTables,
  dmkGeral, frxRich, frxChBox, frxClass, frxDBSet, UnDMKProcFunc, UnDmkEnums;

type
  TFmCopiaDoc = class(TForm)
    PainelConfirma: TPanel;
    BtPesquisa: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrCorrige: TmySQLQuery;
    Panel3: TPanel;
    LaAviso: TLabel;
    QrCorrigeTipoDoc: TSmallintField;
    QrCorrigeID_Pgto: TIntegerField;
    QrCopiaCH1: TmySQLQuery;
    QrCopiaCH1Itens: TLargeintField;
    QrCopiaCH1Banco1: TIntegerField;
    QrCopiaCH1Conta1: TWideStringField;
    QrCopiaCH1Agencia1: TIntegerField;
    QrCopiaCH1SerieCH: TWideStringField;
    QrCopiaCH1Data: TDateField;
    QrCopiaCH1Documento: TFloatField;
    QrCopiaCH1NOMEBANCO: TWideStringField;
    QrCopiaCH1TipoDoc: TIntegerField;
    QrCopiaCH1Tipo: TIntegerField;
    QrCopiaCH1ID_Copia: TIntegerField;
    QrCopiaCH1DEBITO: TFloatField;
    QrItens: TmySQLQuery;
    QrItensNOMEFORNECE: TWideStringField;
    QrItensDescricao: TWideStringField;
    QrItensControle: TIntegerField;
    QrCopiaCH2: TmySQLQuery;
    DsCopiaCH2: TDataSource;
    QrCopiaCH2Banco1: TIntegerField;
    QrCopiaCH2Conta1: TWideStringField;
    QrCopiaCH2Agencia1: TIntegerField;
    QrCopiaCH2SerieCH: TWideStringField;
    QrCopiaCH2Data: TDateField;
    QrCopiaCH2Documento: TFloatField;
    QrCopiaCH2Debito: TFloatField;
    QrCopiaCH2NOMEFORNECE: TWideMemoField;
    QrCopiaCH2Descricao: TWideMemoField;
    QrCopiaCH2Controles: TWideMemoField;
    QrCopiaCH2ID_Pgto: TIntegerField;
    QrCopiaCH2NOMEBANCO: TWideStringField;
    QrCopiaCH2TipoDoc: TIntegerField;
    QrCopiaCH2Tipo: TIntegerField;
    QrCopiaCH2ID_Copia: TIntegerField;
    QrCopiaCH2QtdDocs: TIntegerField;
    DBGrid2: TDBGrid;
    frxCopiaDOC: TfrxReport;
    frxCheckBoxObject1: TfrxCheckBoxObject;
    frxRichObject1: TfrxRichObject;
    frxDsCopiaDOC: TfrxDBDataset;
    QrCopiaCH1NOMEEMPRESA: TWideStringField;
    QrCopiaCH1NotaFiscal: TIntegerField;
    QrCopiaCH2NOMEEMPRESA: TWideStringField;
    QrCopiaCH2NotaFiscal: TIntegerField;
    QrCopiaCH1Duplicata: TWideStringField;
    QrCopiaCH2Duplicata: TWideStringField;
    QrContasEnt: TmySQLQuery;
    QrContasEntCntrDebCta: TWideStringField;
    QrCopiaCH2Genero: TIntegerField;
    QrCopiaCH1Genero: TIntegerField;
    BtImprime: TBitBtn;
    QrCopiaCH1NOMEFORNECE: TWideStringField;
    QrCopiaCH1Controle: TIntegerField;
    QrCopiaCH1Descricao: TWideStringField;
    frxCopiaDOC2: TfrxReport;
    Panel4: TPanel;
    CkDono: TCheckBox;
    RGDataTipo: TRadioGroup;
    RGItensPagina: TRadioGroup;
    CkNaoAgrupar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure frxCopiaDOCGetValue(const VarName: string; var Value: Variant);
    procedure QrCopiaCH2BeforeClose(DataSet: TDataSet);
    procedure QrCopiaCH2AfterOpen(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CkNaoAgruparClick(Sender: TObject);
  private
    { Private declarations }
    (*FCopDocCtrl, *)FCopiaCH1, FCopiaCH2: String;

  public
    { Public declarations }
    FDBG: TDBGrid;
    FQrLct: TmySQLQuery;
    procedure ConfiguracoesIniciais(DBG: TDBGrid; QrLct: TmySQLQuery);
  end;

  var
  FmCopiaDoc: TFmCopiaDoc;

implementation

uses UCreate, ModuleGeral, UMySQLModule, UnMyObjects, UnInternalConsts;

{$R *.DFM}

procedure TFmCopiaDoc.BtImprimeClick(Sender: TObject);
begin
  //frxCopiaDOC.DesignReport();
  if RGItensPagina.ItemIndex > 1 then
    MyObjects.frxMostra(frxCopiaDOC2, 'C�pia de lan�amentos')
  else
    MyObjects.frxMostra(frxCopiaDOC, 'C�pia de lan�amentos');
end;

procedure TFmCopiaDoc.BtPesquisaClick(Sender: TObject);
  function VerificaSeTextoExiste(Texto, NovoTexto: String): String;
  begin
    if Pos(NovoTexto, Texto) > 0 then
      Result := Texto
    else
      Result := Texto + '; ' + NovoTexto;
  end;
var
  Documento: Double;
  Ctrls: String;
  //Controle, Sub, Carteira,
  Tipo, i: Integer;
  Data, SerieCH: String;
  //
  Genero, NotaFiscal, Banco1, Agencia1, ID_Pgto, TipoDoc, ID_Copia, QtdDocs: Integer;
  NOMEEMPRESA, Duplicata, Conta1, NOMEFORNECE, Descricao, NOMEBANCO, Controles: String;
  Debito: Double;
begin
  MyObjects.Informa(LaAviso, True, 'Criando tabelas tempor�rias');
  FCopiaCH1 := UCriar.RecriaTempTableNovo(ntrttCopiaCH1, DmodG.QrUpdPID1, False);
  FCopiaCH2 := UCriar.RecriaTempTableNovo(ntrttCopiaCH2, DmodG.QrUpdPID1, False);
  MyObjects.Informa(LaAviso, True, 'Buscando dados selecionados');
  Ctrls := '';
  if FDBG.SelectedRows.Count > 1 then
  begin
    with FDBG.DataSource.DataSet do
    for i:= 0 to FDBG.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(FDBG.SelectedRows.Items[i]));
      Ctrls := Ctrls + FormatFloat('0', FQrLct.FieldByName('Controle').AsInteger) + ',';
    end;
    Ctrls := Copy(Ctrls, 1, Length(Ctrls)-1);
  end else Ctrls := FormatFloat('0', FQrLct.FieldByName('Controle').AsInteger);
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FCopiaCH1);
  DModG.QrUpdPID1.SQL.Add('SELECT car.Banco1, car.Conta1, car.Agencia1,');
  DModG.QrUpdPID1.SQL.Add('lan.SerieCH,');
  case RGDataTipo.ItemIndex of
    0:   DModG.QrUpdPID1.SQL.Add('lan.Data,');
    else DModG.QrUpdPID1.SQL.Add('lan.Vencimento,');
  end;
  DmodG.QrUpdPID1.SQL.Add('lan.Documento, lan.Debito,');
  DModG.QrUpdPID1.SQL.Add('IF(lan.Fornecedor=0,"",IF(frn.Tipo=0,');
  DModG.QrUpdPID1.SQL.Add('frn.RazaoSocial,frn.Nome)) NOMEFORNECE,');
  DModG.QrUpdPID1.SQL.Add('lan.Descricao, lan.Controle, lan.ID_Pgto,');
  DModG.QrUpdPID1.SQL.Add('ban.Nome NOMEBANCO, car.TipoDoc, car.Tipo, ');
  DModG.QrUpdPID1.SQL.Add('0 ID_Copia, ');
  DModG.QrUpdPID1.SQL.Add('IF(lan.CliInt=0,"",IF(emp.Tipo=0,');
  DModG.QrUpdPID1.SQL.Add('emp.RazaoSocial,emp.Nome)) NOMEEMPRESA,');
  DModG.QrUpdPID1.SQL.Add('lan.NotaFiscal, lan.Duplicata, lan.Genero,');
  DModG.QrUpdPID1.SQL.Add('0 Protocolo, 0 EANTem');
  DModG.QrUpdPID1.SQL.Add('FROM '+TMeuDB+'.lanctos lan');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades frn ON frn.Codigo=lan.Fornecedor');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades emp ON emp.Codigo=lan.CliInt');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.carteiras car ON car.Codigo=lan.Carteira');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.bancos    ban ON ban.Codigo=car.Banco1 AND ban.Codigo<>0');
  DModG.QrUpdPID1.SQL.Add('WHERE lan.Sub=0 AND Debito>0');
  DModG.QrUpdPID1.SQL.Add('AND lan.Controle IN (' + Ctrls + ')');
  DModG.QrUpdPID1.ExecSQL;
  //
  MyObjects.Informa(LaAviso, True, 'Verificando carteiras de origem');
  QrCorrige.Close;
  QrCorrige.SQL.Clear;
  QrCorrige.SQL.Add('SELECT DISTINCT car.TipoDoc, cc1.ID_Pgto');
  QrCorrige.SQL.Add('FROM copiach1 cc1');
  QrCorrige.SQL.Add('LEFT JOIN '+ TMeuDB + '.lanctos lan ON lan.Controle=cc1.ID_Pgto');
  QrCorrige.SQL.Add('LEFT JOIN '+ TMeuDB + '.carteiras car ON car.Codigo=lan.Carteira');
  QrCorrige.SQL.Add('WHERE cc1.ID_Pgto <> 0');
  QrCorrige.SQL.Add('');
  QrCorrige.SQL.Add('');
  QrCorrige.SQL.Add('');
  QrCorrige.SQL.Add('');
  QrCorrige.Open;
  if QrCorrige.RecordCount > 0 then
  begin
    QrCorrige.First;
    while not QrCorrige.Eof do
    begin
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stUpd, FCopiaCH1, False, [
      'TipoDoc'], ['ID_Pgto'], [QrCorrigeTipoDoc.Value], [QrCorrigeID_Pgto.Value
      ], False);
      //
      QrCorrige.Next;
    end;  
  end;
  MyObjects.Informa(LaAviso, True, 'Separando itens sem documento');
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + FCopiaCH1);
  DModG.QrUpdPID1.SQL.Add('SET ID_Copia=Controle ');
  DModG.QrUpdPID1.SQL.Add('WHERE Documento=0');
  DModG.QrUpdPID1.ExecSQL;
  MyObjects.Informa(LaAviso, True, 'Criando c�pias');
  QrCopiaCH1.Close;
  QrCopiaCH1.SQL.Clear;
  if CkNaoAgrupar.Checked then
  begin
    QrCopiaCH1.SQL.Add('SELECT 1 Itens, Banco1, Conta1,');
    QrCopiaCH1.SQL.Add('Agencia1, SerieCH, Data, Documento, NOMEBANCO,');
    QrCopiaCH1.SQL.Add('TipoDoc, Tipo, ID_Copia, Debito DEBITO,');
    QrCopiaCH1.SQL.Add('NOMEEMPRESA, NotaFiscal, Duplicata, Genero,');
    QrCopiaCH1.SQL.Add('NOMEFORNECE, Controle, Descricao');
    QrCopiaCH1.SQL.Add('FROM copiach1 cc1');
  end else
  begin
    QrCopiaCH1.SQL.Add('SELECT COUNT(cc1.Debito) Itens, Banco1, Conta1,');
    QrCopiaCH1.SQL.Add('Agencia1, SerieCH, Data, Documento, NOMEBANCO,');
    QrCopiaCH1.SQL.Add('TipoDoc, Tipo, ID_Copia, SUM(Debito) DEBITO,');
    QrCopiaCH1.SQL.Add('NOMEEMPRESA, NotaFiscal, Duplicata, Genero,');
    QrCopiaCH1.SQL.Add('NOMEFORNECE, Controle, Descricao');
    QrCopiaCH1.SQL.Add('FROM copiach1 cc1');
    QrCopiaCH1.SQL.Add('GROUP BY Banco1, Conta1,');
    QrCopiaCH1.SQL.Add('Agencia1, SerieCH, Data, Documento, NOMEBANCO,');
    QrCopiaCH1.SQL.Add('TipoDoc, Tipo, ID_Copia');
  end;
  QrCopiaCH1.Open;
  QrCopiaCH1.First;
  while not QrCopiaCH1.Eof do
  begin
    Banco1        := QrCopiaCH1Banco1.Value;
    Conta1        := QrCopiaCH1Conta1.Value;
    ID_Pgto       := 0;
    TipoDoc       := QrCopiaCH1TipoDoc.Value;
    Tipo          := QrCopiaCH1Tipo.Value;
    ID_Copia      := QrCopiaCH1ID_Copia.Value;
    QtdDocs       := QrCopiaCH1Itens.Value;
    Agencia1      := QrCopiaCH1Agencia1.Value;
    SerieCH       := QrCopiaCH1SerieCH.Value;
    Documento     := QrCopiaCH1Documento.Value;
    NOMEBANCO     := QrCopiaCH1NOMEBANCO.Value;
    Debito        := QrCopiaCH1DEBITO.Value;
    Data          := Geral.FDT(QrCopiaCH1Data.Value, 1);
    NOMEEMPRESA   := QrCopiaCH1NOMEEMPRESA.Value;
    NotaFiscal    := QrCopiaCH1NotaFiscal.Value;
    Duplicata     := QrCopiaCH1Duplicata.Value;
    Genero        := QrCopiaCH1Genero.Value;
    //
    QrItens.Close;
    QrItens.Params[00].AsInteger := Banco1;
    QrItens.Params[01].AsString  := Conta1;
    QrItens.Params[02].AsInteger := Agencia1;
    QrItens.Params[03].AsString  := SerieCH;
    QrItens.Params[04].AsString  := Data;
    QrItens.Params[05].AsFloat   := Documento;
    QrItens.Params[06].AsString  := NOMEBANCO;
    QrItens.Params[07].AsInteger := TipoDoc;
    QrItens.Params[08].AsInteger := Tipo;
    QrItens.Params[09].AsInteger := ID_Copia;
    QrItens.Open;
    //if QrItens.RecordCount > 0 then
    if (QrItens.RecordCount > 0) and (not CkNaoAgrupar.Checked) then
    begin
      QrItens.First;
      NOMEFORNECE   := '';
      Descricao     := '';
      Controles     := '';
      while not QrItens.Eof do
      begin
        NOMEFORNECE   := VerificaSeTextoExiste(NOMEFORNECE, QrItensNOMEFORNECE.Value);
        Descricao     := VerificaSeTextoExiste(Descricao, QrItensDescricao.Value);
        Controles     := Controles   + FormatFloat('0', QrItensControle.Value) + '; ';
        //
        QrItens.Next;
      end;
    end else begin
      NOMEFORNECE   := QrCopiaCH1NOMEFORNECE.Value;
      Descricao     := QrCopiaCH1Descricao.Value;
      Controles     := FormatFloat('0', QrCopiaCH1Controle.Value);
    end;
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'copiach2', False, [
    'Banco1', 'Conta1', 'Agencia1',
    'SerieCH', 'Data', 'Documento',
    'Debito', 'NOMEFORNECE', 'Descricao',
    'Controles', 'ID_Pgto', 'NOMEBANCO',
    'TipoDoc', 'Tipo', 'ID_Copia',
    'QtdDocs', 'NOMEEMPRESA', 'NotaFiscal',
    'Duplicata', 'Genero'], [
    ], [
    Banco1, Conta1, Agencia1,
    SerieCH, Data, Documento,
    Debito, NOMEFORNECE, Descricao,
    Controles, ID_Pgto, NOMEBANCO,
    TipoDoc, Tipo, ID_Copia,
    QtdDocs, NOMEEMPRESA, NotaFiscal,
    Duplicata, Genero], [
    ], False);
    QrCopiaCH1.Next;
  end;
  QrCopiaCH2.Close;
  QrCopiaCH2.Open;
{
SELECT Banco1, Conta1, Agencia1, SerieCH, Data,
Documento, SUM(Debito) Debito, CONCAT(NOMEFORNECE),
CONCAT(Descricao), CONCAT(Controle), ID_Pgto, NOMEBANCO
FROM _aaaa_
WHERE Documento <> 0
GROUP BY Banco1, Conta1, Agencia1, SerieCH, Data, Documento
}
  MyObjects.Informa(LaAviso, False, '...');
end;

procedure TFmCopiaDoc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCopiaDoc.CkNaoAgruparClick(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmCopiaDoc.ConfiguracoesIniciais(DBG: TDBGrid; QrLct: TmySQLQuery);
var
  Ctrls: String;
{
  Documento: Double;
  I, Errados: Integer;
  SerieCH, Controles, Sep1: String;
}
  //Controle, Sub, Tipo, Carteira, Documento,
  i: Integer;
  //Data, SerieCH: String;
begin
  //FCopDocCtrl := UCriar.RecriaTempTable('CopDocCtrl', DModG.QrUpdPID1, False);
  FCopiaCH1 := UCriar.RecriaTempTable('CopiaCH', DModG.QrUpdPID1, False, 0, 'CopiaCH1');
  FCopiaCH2 := UCriar.RecriaTempTable('CopiaCH', DModG.QrUpdPID1, False, 0, 'CopiaCH2');
  Ctrls := '';
  if DBG.SelectedRows.Count > 1 then
  begin
    with DBG.DataSource.DataSet do
    for i:= 0 to DBG.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(DBG.SelectedRows.Items[i]));
      {
      Controle  := QrLct.FieldByName('Controle').AsInteger;
      Data      := Geral.FDT(QrLct.FieldByName('Data').AsDateTime, 1);
      Tipo      := QrLct.FieldByName('Tipo').AsInteger;
      Carteira  := QrLct.FieldByName('Carteira').AsInteger;
      Documento := QrLct.FieldByName('Documento').AsInteger;
      SerieCH   := QrLct.FieldByName('SerieCH').AsString;
      //
      UMyMod.SQLIns_ON_DUPLICATE_KEY(DmodG.QrUpdPID1, FCopDocCtrl, False,
      ['Controle'],
      ['Data', 'Tipo', 'Carteira', 'Documento', 'SerieCH'],
      ['Data', 'Tipo', 'Carteira', 'Documento', 'SerieCH'],
      [Controle],
      [Data, Tipo, Carteira, Documento, SerieCH],
      [Data, Tipo, Carteira, Documento, SerieCH],
      True);
      }
      Ctrls := Ctrls + FormatFloat('0', QrLct.FieldByName('Controle').AsInteger) + ',';
      {
      if (Documento <> FmPrincipal.QrLctDocumento.Value) or
         (SerieCH <> FmPrincipal.QrLctSerieCH.Value) then
      Errados := Errados + 1
      else begin
        Controles := Controles + Sep1 +
          dmkPF.FFP(FmPrincipal.QrLctControle.Value, 0);
        FTextoCopia := FTextoCopia + Sep1 +
          FmPrincipal.QrLctDescricao.Value;
        //
        Sep1 := ', ';
        //Sep2 := #13#10;
      end;
      }
    end;
    Ctrls := Copy(Ctrls, 1, Length(Ctrls)-1);
  end else Ctrls := FormatFloat('0', QrLct.FieldByName('Controle').AsInteger);
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FCopiaCH1);
  DModG.QrUpdPID1.SQL.Add('SELECT car.Banco1, car.Conta1, car.Agencia1,');
  DModG.QrUpdPID1.SQL.Add('lan.SerieCH, lan.Data, lan.Documento, lan.Debito,');
  DModG.QrUpdPID1.SQL.Add('IF(lan.Fornecedor=0,"",IF(frn.Tipo=0,');
  DModG.QrUpdPID1.SQL.Add('frn.RazaoSocial,frn.Nome)) NOMEFORNECE,');
  DModG.QrUpdPID1.SQL.Add('lan.Descricao, lan.Controle, lan.ID_Pgto,');
  DModG.QrUpdPID1.SQL.Add('ban.Nome NOMEBANCO');
  DModG.QrUpdPID1.SQL.Add('FROM '+TMeuDB+'.lanctos lan');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades frn ON frn.Codigo=lan.Fornecedor');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.carteiras car ON car.Codigo=lan.Carteira');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.bancos    ban ON ban.Codigo=car.Banco1 AND ban.Codigo<>0');
  DModG.QrUpdPID1.SQL.Add('WHERE lan.Sub=0');
  DModG.QrUpdPID1.ExecSQL;
  //
(*
SELECT Banco1, Conta1, Agencia1, SerieCH, Data,
Documento, SUM(Debito) Debito, CONCAT(NOMEFORNECE),
CONCAT(Descricao), CONCAT(Controle), ID_Pgto, NOMEBANCO
FROM _aaaa_
WHERE Documento <> 0
GROUP BY Banco1, Conta1, Agencia1, SerieCH, Data, Documento
*)
{
  Errados   := 0;
  Documento := FmPrincipal.QrLctDocumento.Value;
  SerieCH   := FmPrincipal.QrLctSerieCH.Value;
  //
  if FmCondGer.dmkDBGLct.SelectedRows.Count > 0 then
  begin
    FTextoCopia := '';
    Sep1 := '';
    with FmCondGer.dmkDBGLct.DataSource.DataSet do
    for i:= 0 to FmCondGer.dmkDBGLct.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(FmCondGer.dmkDBGLct.SelectedRows.Items[i]));
      if (Documento <> FmPrincipal.QrLctDocumento.Value) or
         (SerieCH <> FmPrincipal.QrLctSerieCH.Value) then
      Errados := Errados + 1
      else begin
        Controles := Controles + Sep1 +
          dmkPF.FFP(FmPrincipal.QrLctControle.Value, 0);
        FTextoCopia := FTextoCopia + Sep1 +
          FmPrincipal.QrLctDescricao.Value;
        //
        Sep1 := ', ';
        //Sep2 := #13#10;
      end;
    end;
  end else begin
    FTextoCopia := FmPrincipal.QrLctDescricao.Value;
    Controles   := dmkPF.FFP(FmPrincipal.QrLctDocumento.Value, 0);
  end;
  if Errados > 0 then
    Application.MessageBox(PChar('Foram localizados ' + IntToStr(Errados) +
    ' lan�amentos que n�o tem a S�rie ou Documento igual ao primeiro lan�amento' +
    'do itens selecionados! C�pia cancelada!'), 'Aviso', MB_OK+MB_ICONWARNING)
  else begin
    QrCopiaVC.Close;
    QrCopiaVC.SQL.Clear;
    QrCopiaVC.SQL.Add('SELECT SUM(lan.Debito) Debito, LPAD(car.Banco1, 3, "0") Banco1,');
    QrCopiaVC.SQL.Add('LPAD(car.Agencia1, 4, "0") Agencia1, car.Conta1,');
    QrCopiaVC.SQL.Add('lan.SerieCH, IF(lan.Documento=0, "",');
    QrCopiaVC.SQL.Add('LPAD(lan.Documento, 6, "0")) Documento, lan.Data,');
    QrCopiaVC.SQL.Add('lan.Fornecedor, IF(lan.Fornecedor=0, "",');
    QrCopiaVC.SQL.Add('CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial');
    QrCopiaVC.SQL.Add('ELSE frn.Nome END) NOMEFORNECE,');
    QrCopiaVC.SQL.Add('ban.Nome NOMEBANCO, LPAD(lan.Controle, 6, "0") Controle,');
    QrCopiaVC.SQL.Add('lan.TipoCH');
    QrCopiaVC.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrCopiaVC.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrCopiaVC.SQL.Add('LEFT JOIN entidades frn ON frn.Codigo=lan.Fornecedor');
    QrCopiaVC.SQL.Add('LEFT JOIN bancos ban ON ban.Codigo=car.Banco1');
    QrCopiaVC.SQL.Add('WHERE lan.Controle in (' + Controles + ')');
    QrCopiaVC.SQL.Add('GROUP BY lan.SerieCH, lan.Documento');
    QrCopiaVC.Open;
    //
    ReacertaFrxDataSets();
    frxCopiaVC.EnabledDataSets.Add(DCond.frxDsCond);
    frxCopiaVC.EnabledDataSets.Add(frxDsCopiaVC);
    MyObjects.frxMostra(frxCopiaVC, 'C�pia de cheque (M�ltiplos lan�amentos)');
  end;
}
end;

procedure TFmCopiaDoc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCopiaDoc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Geral.WriteAppKeyCU(
    'PageIts', Application.Title + '\CopiaDoc', RGItensPagina.ItemIndex, ktInteger);
  Geral.WriteAppKeyCU(
    'DataTip', Application.Title + '\CopiaDoc', RGDataTipo.ItemIndex, ktInteger);
  Geral.WriteAppKeyCU(
    'NomeEmp', Application.Title + '\CopiaDoc', CkDono.Checked, ktBoolean);
end;

procedure TFmCopiaDoc.FormCreate(Sender: TObject);
begin
  RGItensPagina.ItemIndex := Geral.ReadAppKeyCU('PageIts',
    Application.Title + '\CopiaDoc', ktInteger, 0);
  RGDataTipo.ItemIndex    := Geral.ReadAppKeyCU('DataTip',
    Application.Title + '\CopiaDoc', ktInteger, 0);
  CkDono.Checked          := Geral.ReadAppKeyCU('NomeEmp',
    Application.Title + '\CopiaDoc', ktBoolean, 0);
end;

procedure TFmCopiaDoc.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCopiaDoc.frxCopiaDOCGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EHDEBITO'   then Value := QrCopiaCH2TipoDoc.Value <> 1 else
  if VarName = 'VARF_ITENS'      then Value := RGItensPagina.ItemIndex + 1 else
  if VarName = 'VAR_IMPDONO'     then Value := CkDono.Checked;
  if VarName = 'VARF_EXTENSO'    then Value :=
    dmkPF.ExtensoMoney(Geral.FFT(QrCopiaCH2Debito.Value, 2, siPositivo)) else
  // .... terminando abaixo
  if QrCopiaCH2TipoDoc.Value = 1 then
  begin
    if VarName = 'TIT_COPIA'     then Value := 'C�PIA DE CHEQUE' else
    if VarName = 'TIT_DOC'       then Value := 'Cheque N�' else
    if VarName = 'TXT_EXTENSO'   then Value := 'Pague por este cheque a quantia de ' else
    if VarName = 'TXT_PARA'      then Value := 'a ' else
    if VarName = 'TXT_OUPARA'    then Value := ' ou a sua ordem.' else
    if VarName = 'CANHOTO'       then Value := 'C A N H O T O ' else
    if VarName = 'VARF_CONTRATO' then Value := ' ' else
    if VarName = 'TIT_RESPONSABILIDADE' then Value := 'CHEQUE ASSINADO POR' else
  end else begin
    if VarName = 'TIT_COPIA'     then Value := 'C�PIA DE D�BITO EM CONTA' else
    if VarName = 'TIT_DOC'       then Value := 'Documento' else
    if VarName = 'TXT_EXTENSO'   then Value := 'Foi debitado na conta corrente acima o valor de ' else
    if VarName = 'TXT_PARA'      then Value := 'referente ao pagamento do fornecedor ' else
    if VarName = 'TXT_OUPARA'    then Value := '.' else
    if VarName = 'CANHOTO'       then Value := ' ' else
    if VarName = 'VARF_CONTRATO' then
    begin
      if QrCopiaCH2Genero.Value <> 0 then
      begin
        QrContasEnt.Close;
        QrContasEnt.Params[0].AsInteger := QrCopiaCH2Genero.Value;
        QrContasEnt.Open;
        Value := QrContasEntCntrDebCta.Value;
      end else Value := ' ';
    end else
    if VarName = 'TIT_RESPONSABILIDADE' then Value := 'Lan�amento ratificado por' else
  end;
  // n�o colocar nada aqui
end;

procedure TFmCopiaDoc.QrCopiaCH2AfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := QrCopiaCH2.RecordCount > 0;
end;

procedure TFmCopiaDoc.QrCopiaCH2BeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

end.
